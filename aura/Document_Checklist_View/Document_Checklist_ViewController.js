({
	doInit: function(component, event, helper) {
		helper.getLoanApplications(component);
	},
    
    inlineEditName : function(component,event,helper){   
        // show the name edit field popup 
        component.set("v.nameEditMode", true); 
        // after the 100 millisecond set focus to input field   
        setTimeout(function(){ 
            component.find("inputId").focus();
        }, 100);
    },
    
    handleClickNew : function(component, event, helper){
    	var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Document_Checklist__c"
            
                
        });
        createRecordEvent.fire();
	}
})