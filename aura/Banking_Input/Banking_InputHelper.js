({
    //init
    getMonthlyBankingList : function(component,event) {
        //alert('--init---');
        var recordid = component.get("v.custRecordId");
        console.log('RECORD '+component.get("v.custRecordId"));
        //Get customer detail 
        var action1 = component.get("c.getCustDetailInfo");
        action1.setParams({
            'CDrecID' : component.get("v.custRecordId"),            
        })
        action1.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {  
                console.log('RES '+response.getReturnValue());
                component.set("v.CustomerDetail", response.getReturnValue());               
                var status = component.get("v.CustomerDetail.Banking_Status__c");
                if(status == 'Completed') {
                    //var isverified = component.find("isChecked").get("v.value");
                    component.set("v.isverified",true);
                } else {
                    component.set("v.isverified",false); 
                }
                console.log('--CustomerDetail-- banking status--' + JSON.stringify(component.get("v.CustomerDetail")));
            }
        });
        $A.enqueueAction(action1); 
        
        //Get List of Month        
        console.log('component.get("v.custRecordId") '+component.get("v.custRecordId"));
        var action11 = component.get("c.getPerfiosbankMasterDetail");
        action11.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'getBankNames' : true 
        })
        action11.setCallback(this, function(response){
            var state = response.getState();
            console.log('STATE IS '+state);
            if (state === "SUCCESS") {   
                console.log('RESIIII '+response.getReturnValue()); 
                var x1 = response.getReturnValue();
                
                console.log('Here in this'+JSON.stringify(x1)); 
                console.log(x1 +x1);
                if(x1 != null) {
                    var optionsList1 = [];    
                    for (var key1 in x1) {
                        optionsList1.push({value: key1, label: x1[key1]});
                    };
                    console.log('optionsList1::::'+JSON.stringify(optionsList1));
                    component.set("v.bankNameList", optionsList1);
                } else {
                    component.set("v.disableBank1" , true);
                    //component.set("v.disableBank2" , true);
                    //component.set("v.disableBank3" , true);
                }
            }
        });
        $A.enqueueAction(action11);
        //var bnknameMain = component.get("v.BankName1");
        //console.log(bnknameMain+'sharukh');
        //if(bnknameMain != undefined && bnknameMain != '' && bnknameMain != null) {
        
        console.log('component.get("v.custRecordId") '+component.get("v.custRecordId"));
        var action12 = component.get("c.getPerfiosbankMasterDetail");
        action12.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'getBankNames' : false 
        })
        action12.setCallback(this, function(response){
            var state = response.getState();
            console.log('STATE IS '+state);
            if (state === "SUCCESS") {   
                console.log('RESIIII '+response.getReturnValue()); 
                var x1 = response.getReturnValue();  
                var optionsList1 = [];    
                for (var key1 in x1) {
                    optionsList1.push({value: key1, label: x1[key1]});
                };
                console.log('optionsList1::::'+JSON.stringify(optionsList1));
                component.set("v.accountNumberList", optionsList1);
            }
        });
        $A.enqueueAction(action12); 
        //}
        //ADDED BY SHARAD for account number fetch with bank name
        
        
        
        
        //Get List of Month        
        var action = component.get("c.fetchMonthlyBanking");
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'bnkname' : component.get("v.BankName"),
            'accountNo': component.get("v.Accountno"),
            'getBRERecord' : true,
            'perfiosBankDetails' : true
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('In this method '+state);
            if (state === "SUCCESS") {   
                component.set("v.MonthList", {});
                var storeResponse = response.getReturnValue();   
                console.log('--obj values--'+ JSON.stringify(response.getReturnValue()));                
                component.set("v.MonthList1", response.getReturnValue());// set list 1                
                component.set("v.MonthList2", storeResponse);// set default list   
                component.set("v.MonthList3", storeResponse);
            }
        });
        $A.enqueueAction(action); 
        
        console.log('Calling fetchBRERecordsUpdated');
        this.fetchBRERecordsUpdated(component,helper,event);
        
    },
    
    docStatusPickListVal: function(component,helper,event) {
        
        
        var action = component.get("c.getselectOptions");
       
        action.setParams({
            "objObject": component.get("v.objInfoForPicklistValues"),
            "fld": 'Banking_Account_Type__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.docStatus", allValues);
            }
        });
        $A.enqueueAction(action);
    },
    
    savePanelHelper: function(component, event, helper, bankName, accountNumber, perfiosBankDetails, abc) {
        var isChecked = component.find("isChecked").get("v.value");
        console.log('In savePanelHelper');
        var action1 = component.get("c.savePanel1");
        action1.setParams({
            'isChecked' : isChecked, 
            'CDrecID' : component.get("v.custRecordId") ,
             completed: true
        })
        /*action1.setCallback(this, function(response){
            var state = response.getState();
            console.log('--SAVE return value--'+response.getReturnValue());
            if (response.getReturnValue() != "Success") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": "Please complete missing information in section 1."
                });
                toastEvent.fire();
            } else {
                console.log('Here in output');
                component.set("v.disableInput",true);
            }
        });*/
        $A.enqueueAction(action1);
    }, 
    doSave: function(component, event, helper, bankName ,accountNumber,accountType,sectionno, perfiosBankDetails, perfiosButtonClicked, abc){
        console.log('abc '+abc);
        console.log('v.DisableScreen '+component.get("v.DisableScreen"));
        console.log('v.disableBank1 '+component.get("v.disableBank1"));
        console.log('v.disableBankSection1 '+component.get("v.disableBankSection1"));
        console.log('accountType '+accountType);
        console.log(component.find("isChecked").get("v.value"));
        var list;
        if(sectionno == "1"){
            list = component.get("v.MonthList1");
            console.log('list '+JSON.stringify(list));
        }else if(sectionno == "2"){
            list = component.get("v.MonthList2");
        }else if(sectionno == "3"){
            list = component.get("v.MonthList3");
        }             
        var action = component.get("c.SaveMonthlyInOutRecord");
        action.setParams({
            'wrappedObjList1' : JSON.stringify(list),
            'CDrecID':component.get("v.custRecordId"),
            'bnkname' : bankName,
            'accountNo': accountNumber,
            'acctType' : accountType,
            'SavePartially': component.find("isChecked").get("v.value")==true ? false : true,
            'perfiosBankDetails' : perfiosBankDetails,
            'perfiosButtonClicked' : perfiosButtonClicked,
            'rowsToVerify' : abc,
            'sectionno' : sectionno
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('======STATE====='+state);
            console.log('--SAVE return value--'+response.getReturnValue());
            if (state === "SUCCESS") { 
            if (response.getReturnValue().includes("SUCCESS")) {
                var abb = response.getReturnValue().split("-");
                console.log('abb is '+abb);
                console.log('sectionno '+sectionno);
                if(sectionno == "1"){
                    component.set("v.disableInput",true); 
                    //component.set("v.disableBankSection1", true);
                    component.set("v.averageABB1",abb[1]);
                    console.log('component.get(abb1 )'+component.get("v.averageABB1"));
                }else if(sectionno == "2"){
                    component.set("v.disableInput2",true);
                    //component.set("v.disableBankSection2", true);
                    component.set("v.averageABB2",abb[1]);
                }else if(sectionno == "3"){
                    component.set("v.disableInput3",true); 
                    //component.set("v.disableBankSection3", true);
                    component.set("v.averageABB3",abb[1]);
                }
                console.log('disableInput '+ component.get("v.disableInput")); 
                console.log('disableInput3 '+ component.get("v.disableInput3"));
                console.log('disableInput2 '+ component.get("v.disableInput2"));
                console.log('disableBank2 '+ component.get("v.disableBank2"));
                console.log('disableBank21 '+ component.get("v.disableBank21"));
                console.log('disableBank3 '+ component.get("v.disableBank3"));
                console.log('disableBank31 '+ component.get("v.disableBank31"));
                console.log('BankName3 '+ component.get("v.BankName3"));
                console.log('Accountno3 '+ component.get("v.Accountno3"));
                console.log('BankName2 '+ component.get("v.BankName2"));
                console.log('Accountno2 '+ component.get("v.Accountno2"));
                if(component.get("v.disableInput") == true && (component.get("v.disableInput2") == true || (component.get("v.disableBank2") == false && component.get("v.disableBank21") == false)) && (component.get("v.disableInput3") == true || (component.get("v.disableBank3") == false && component.get("v.disableBank31") == false))) {
                	console.log('TOTAL save');
                    this.savePanelHelper(component, event, helper);    
                } else {
                    var isChecked = component.find("isChecked").get("v.value");
         
                    var action1 = component.get("c.savePanel1");
                    action1.setParams({
                        'isChecked' : isChecked, 
                        'CDrecID' : component.get("v.custRecordId") ,
                        completed: false
                    })
                    $A.enqueueAction(action1);
                }                    
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "success",
                    "message": "successfully saved section "+sectionno
                });
                toastEvent.fire();
            }else if (response.getReturnValue() === "MISSING_FIELDS") { 
                console.log('sectionno))) '+sectionno);
                //alert('Fill all mandatory fields'); 
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": "Please complete missing information in section "+sectionno 
                });
                toastEvent.fire();
            }else if (response.getReturnValue() === "BANKNAME_NOT_FOUND") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": "Bank Name not found in section "+sectionno
                });
                toastEvent.fire();
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": "Unknown error occurred while saving section "+sectionno+", please contact Admin!!"
                });
                toastEvent.fire();
            }
        }
        });
        $A.enqueueAction(action); 
        // }
        
    },
    
    
    fetchBRERecords: function(component, event, helper, bankName, accountNumber, sectionno, perfiosBankDetails){
        var recordid = component.get("v.custRecordId");
        console.log('>> record ID--'+ recordid );
        var action = component.get("c.fetchMonthlyBanking");
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"), 
            'bnkname' : bankName ,
            'accountNo': accountNumber,
            'getBRERecord' :true,
            'perfiosBankDetails' : perfiosBankDetails
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {   
                component.set("v.MonthList", {});
                //console.log('--if loop--');
                var storeResponse = response.getReturnValue();//response.getReturnValue()
                if(sectionno == "1"){
                    component.set("v.perfiosButtonClicked" ,false);
                    //alert('set list 1--');
                    component.set("v.MonthList1", response.getReturnValue());                  
                }else if(sectionno == "2"){
                    component.set("v.perfiosButtonClicked2" ,false);
                    //alert('set list 2--');
                    component.set("v.MonthList2", response.getReturnValue());                  
                }else if(sectionno == "3"){
                    component.set("v.perfiosButtonClicked3" ,false);
                    //alert('set list 3--');
                    component.set("v.MonthList3", response.getReturnValue());                  
                }
                
                
            }
        });
        $A.enqueueAction(action);         
    },
    fetchBRERecordsUpdated: function(component, event, helper, bankName, accountNumber, accountType,sectionno, perfiosBankDetails){
        console.log('IN fetchBRERecordsUpdated');
        var recordid = component.get("v.custRecordId");
        console.log('>> record ID--'+ recordid );
        var action = component.get("c.fetchMonthlyBankingUpdateds");
        action.setParams({
            'CDrecID' : component.get("v.custRecordId")
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            console.log('TTTTTT1 '+response.getReturnValue()[0].averageABB1);
            console.log('TTTTTT2 '+response.getReturnValue()[0].averageABB2);
            console.log('TTTTTT3 '+response.getReturnValue()[0].averageABB3);
            console.log('TTTTTT4 '+response.getReturnValue()[0].showSection2);
            if (state === "SUCCESS") {   
                component.set("v.MonthList", {});
                console.log('--if loop--');
                var storeResponse = response.getReturnValue();//response.getReturnValue()
                
                //alert('set list 1--');
                if(response.getReturnValue()[0].MonthList1 != null && response.getReturnValue()[0].MonthList1 != ''){
                    component.set("v.perfiosButtonClicked" ,false);
                    component.set("v.MonthList1", response.getReturnValue()[0].MonthList1);
                    component.set("v.averageABB1", response.getReturnValue()[0].averageABB1);
                    //alert('set list 1-- '+response.getReturnValue()[0].pdcmonthList1+' '+response.getReturnValue()[0].accountMonthList1);
                    if(response.getReturnValue()[0].pdcmonthList1 != null && response.getReturnValue()[0].pdcmonthList1 != ''){
                        component.set("v.disableBank1" ,true);
                        component.set("v.isaccountType11" ,true);
                        component.set("v.BankName11" ,response.getReturnValue()[0].pdcmonthList1);
                        component.set("v.Accountno11" ,response.getReturnValue()[0].accountMonthList1);
                        component.set("v.AccountType11" ,response.getReturnValue()[0].accountTypeList1);
                        console.log('BANKKKKKKKKKKKKKKKKKKKKKKKKKK11 '+component.get("v.BankName11"));
                            console.log('ACCCOUNNTTTTTTTTTTTTTTTTTTT11'+component.get("v.Accountno11"));
                            console.log('ACCOUNTTTTTTTTTTTTTTTTTTTYPE1'+component.get("v.AccountType11"));
                         //component.set('v.disableBankSection1',true);
                    }
                    else{
                     	if(response.getReturnValue()[0].perfiosBankName1 != null && response.getReturnValue()[0].perfiosBankName1 != ''){
                            component.set("v.disableBank11" ,true);
                            component.set("v.isaccountType1" ,true);
                            console.log('BANKKKKKKKKKKKKKKPREVIOUS '+response.getReturnValue()[0].perfiosBankName1);
                            console.log('ACCCOUNNTTTTTTPREVIOUS'+response.getReturnValue()[0].perfiosAccount1);
                            component.set("v.BankName1" ,response.getReturnValue()[0].perfiosBankName1);
                            component.set("v.Accountno1" ,response.getReturnValue()[0].perfiosAccount1);
                            component.set("v.AccountType1" ,response.getReturnValue()[0].perfiosAccountType1);
                            console.log('BANKKKKKKKKKKKKKKKKKKKKKKKKKK1 '+component.get("v.BankName1"));
                            console.log('ACCCOUNNTTTTTTTTTTTTTTTTTTT1'+component.get("v.Accountno1"));
                            console.log('ACCOUNTTTTTTTTTTTTTTTTTTTYPE1'+component.get("v.AccountType1"));
                            component.set("v.showOtherPicklist",true);
                             //component.set('v.disableBankSection1',true);
                        }   
 
                        
                    }
                   
                }
                
                if(response.getReturnValue()[0].MonthList2 != null && response.getReturnValue()[0].MonthList2 != ''){
                    //alert('set list 1--'+response.getReturnValue()[0].MonthList2);
                    console.log('CHECKING SECOND SECTION');
                    component.set("v.showSection2",response.getReturnValue()[0].showSection2);
                    component.set("v.sectionCount", 2)
                    console.log('SEC 2 COUNT '+component.get("v.sectionCount"));
                    component.set("v.perfiosButtonClicked2" ,false);
                    component.set("v.MonthList2", response.getReturnValue()[0].MonthList2);
                     component.set("v.averageABB2", response.getReturnValue()[0].averageABB2);
                    if(response.getReturnValue()[0].pdcmonthList2 != null && response.getReturnValue()[0].pdcmonthList2 != ''){
                        component.set("v.disableBank2" ,true);
                        component.set("v.isaccountType21" ,true);
                        component.set("v.BankName21" ,response.getReturnValue()[0].pdcmonthList2);
                        component.set("v.Accountno21" ,response.getReturnValue()[0].accountMonthList2);
                        component.set("v.AccountType21" ,response.getReturnValue()[0].accountTypeList2);
                    	console.log('BANKKKKKKKKKKKKKKKKKKKKKKKKKK21 '+component.get("v.BankName21"));
                        console.log('ACCCOUNNTTTTTTTTTTTTTTTTTTT21'+component.get("v.Accountno21"));
                         //component.set('v.disableBankSection2',true);
                    }                     
                    else{
                        if(response.getReturnValue()[0].perfiosBankName2 != null && response.getReturnValue()[0].perfiosBankName2 != ''){
                            component.set("v.disableBank21" ,true);
                            component.set("v.isaccountType2" ,true);
                            component.set("v.BankName2" ,response.getReturnValue()[0].perfiosBankName2);
                            component.set("v.Accountno2" ,response.getReturnValue()[0].perfiosAccount2);
                            component.set("v.AccountType2" ,response.getReturnValue()[0].perfiosAccountType2);
                            console.log('BANKKKKKKKKKKKKKKKKKKKKKKKKKK2 '+component.get("v.BankName2"));
                            console.log('ACCCOUNNTTTTTTTTTTTTTTTTTTT2'+component.get("v.Accountno2"));
                             //component.set('v.disableBankSection2',true);
                        }
                 
                        
                    } 
                }
                console.log('OKKKK '+response.getReturnValue()[0].MonthList3);
                if(response.getReturnValue()[0].MonthList3 != null && response.getReturnValue()[0].MonthList3 != ''){
                    console.log('IN THIS SECTION 3');
                    component.set("v.sectionCount",3);
                    component.set("v.perfiosButtonClicked3" ,false);
                    //alert('set list 1--'+response.getReturnValue()[0].MonthList3);
                    component.set("v.MonthList3", response.getReturnValue()[0].MonthList3);
                    console.log('AVERAGE ABB3'+response.getReturnValue()[0].averageABB3);
                    if(response.getReturnValue()[0].showSection2) {
                        component.set("v.averageABB3", response.getReturnValue()[0].averageABB3);
                    } else {
                     	component.set("v.averageABB3", response.getReturnValue()[0].averageABB2);
                    }
                    if(response.getReturnValue()[0].pdcmonthList3 != null && response.getReturnValue()[0].pdcmonthList3 != ''){
                        component.set("v.disableBank3" ,true);
                        component.set("v.isaccountType31" ,true);
                        component.set("v.BankName31" ,response.getReturnValue()[0].pdcmonthList3);
                        component.set("v.Accountno31" ,response.getReturnValue()[0].accountMonthList3);
                        component.set("v.AccountType31" ,response.getReturnValue()[0].accountTypeList3);
                   		 console.log('BANKKKKKKKKKKKKKKKKKKKKKKKKKK31 '+component.get("v.BankName31"));
                            console.log('ACCCOUNNTTTTTTTTTTTTTTTTTTT31'+component.get("v.Accountno31"));
                         //component.set('v.disableBankSection3',true);
                    }                     
                    else{
                        console.log('VALLLLL' +response.getReturnValue()[0].perfiosBankName3);
                        console.log('VALLLLL' +response.getReturnValue()[0].perfiosAccount3);
                        if(response.getReturnValue()[0].perfiosBankName3 != null && response.getReturnValue()[0].perfiosBankName3 != ''){
                            component.set("v.disableBank31" ,true);
                            component.set("v.isaccountType3" ,true);
                            component.set("v.BankName3" ,response.getReturnValue()[0].perfiosBankName3);
                            component.set("v.Accountno3" ,response.getReturnValue()[0].perfiosAccount3);
                            component.set("v.AccountType3" ,response.getReturnValue()[0].perfiosAccountType3);
                        	 console.log('BANKKKKKKKKKKKKKKKKKKKKKKKKKK3 '+component.get("v.BankName3"));
                            console.log('ACCCOUNNTTTTTTTTTTTTTTTTTTT3'+component.get("v.Accountno3"));
                             //component.set('v.disableBankSection3',true);
                        }
                        
                    }
                }
                //component.set("v.MonthList3", response.getReturnValue()[0].MonthList3);
                console.log('sriram '+response.getReturnValue()[0].MonthList1);
        		}
         });
        $A.enqueueAction(action);
        //component.set("v.Accountno1" ,'123');
        //console.log('ACCCOUNNT1UPDATEDDDDDDDDD'+component.get("v.Accountno1"));
    },
    fetchPerfiosData: function(component, event, helper, bankName, accountNumber, sectionno, perfiosBankDetails){
        var recordid = component.get("v.custRecordId");
        console.log('>> record ID--'+ recordid );
        var action = component.get("c.fetchMonthlyBanking");
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"), 
            'bnkname' : bankName ,
            'accountNo': accountNumber,
            'getBRERecord' : false,
            'perfiosBankDetails' : perfiosBankDetails
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {   
                component.set("v.MonthList", {});
                
                var storeResponse = response.getReturnValue();//response.getReturnValue()
                console.log(JSON.stringify(response.getReturnValue()));
                console.log('--> check resp--'+storeResponse);
                console.log('--typeof storeResponse--'+ typeof storeResponse--);
                if(storeResponse==null || storeResponse.length==0|| storeResponse==''|| JSON.stringify(response.getReturnValue())=='[]'){
                    //alert('No perfios data found');
                    component.set("v.showConfirmDialog",true);
                    component.set("v.AskforPerfiosData",false);
                    component.set("v.perfiosButtonClicked" ,false);
                } else{
                    if(sectionno == "1"){
                        //alert('set list 1--');
                        component.set("v.perfiosButtonClicked" ,true);
                        component.set("v.MonthList1", response.getReturnValue());                  
                    }else if(sectionno == "2"){
                        //alert('set list 2--');
                        component.set("v.perfiosButtonClicked2" ,true);
                        component.set("v.MonthList2", response.getReturnValue());                  
                    }else if(sectionno == "3"){
                        //alert('set list 3--');
                        component.set("v.perfiosButtonClicked3" ,true);
                        component.set("v.MonthList3", response.getReturnValue());                  
                    }
                }
                
            }
        });            
        $A.enqueueAction(action); 
        //alert('fetch perfios data- complete');
        
        /* save the perfios data as BRE records*/
        //this.doSave(component, event, helper, bnkname, accountNo, sectionno);
    },
    
    getdatafromperfios : function(component, event, helper) {
        
        var d = new Date();
        var n = d.getFullYear();
        component.set("v.currentyear", n);                
        var custid = component.get("v.Customerid");
        var action = component.get("c.GetPerfiosData");
        action.setParams({
            'conId':custid
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                console.log('in success'+ response.getReturnValue());
                component.set("v.gotperfios", response.getReturnValue());    
                if(response.getReturnValue() === false)
                {
                    component.set('v.gotperfios', false);
                    component.set('v.showConfirmDialog', true);
                    console.log('in success get return value false::::'+response.getReturnValue());
                    
                }
                else{
                    component.set('v.showConfirmDialog', false);
                }
                
                
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    closeMe : function(comp, event, helper)  { 
        var custid = comp.get("v.custRecordId");
        var recordIdNumber = '';
        var testLoanAppId = comp.get("v.loanAppId");
        
        var evt = $A.get("e.c:callCreateCamScreenEvent");
        evt.setParams({
            "recordIdNumber" : custid,
            "isOpen" : true,
            "testLoanAppId" : testLoanAppId
        });
        evt.fire();
    },
    fetchBRERecordsHelper : function(component, event, helper, disableBank1, disableBank11, sectionno)  {
    	if(disableBank1 == false && disableBank11 == false) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Please enter bank and account number'
            });
            toastEvent.fire(); 
        } else if(disableBank1 && disableBank11) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Some error occurred, Please contact system admin'
            });
            toastEvent.fire(); 
        } else if(disableBank1) {
            if(sectionno == "1") {
                var bankName = component.get("v.BankName11");
                var accountNumber = component.get("v.Accountno11");  
                
            } else if (sectionno == "2") {
				var bankName = component.get("v.BankName21");
                var accountNumber = component.get("v.Accountno21");  
                               
            } else if(sectionno == "3") {
               	var bankName = component.get("v.BankName31");
                var accountNumber = component.get("v.Accountno31");  
                 
            }
            console.log('accountNumber'+accountNumber);
            console.log('bankName '+bankName);
            if(bankName == undefined || bankName == null || bankName =='' || accountNumber == undefined || accountNumber == null || accountNumber == '') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Please enter PDC Bank Master and Account Number'
                });
                toastEvent.fire();    
            } else {
                this.fetchBRERecords(component, event, helper,bankName ,accountNumber,sectionno, false);
            }
        } else if(disableBank11) {
            if(sectionno == "1") {  
                var bankName = component.get("v.BankName1");
                var accountNumber = component.get("v.Accountno1");
            } else if (sectionno == "2") {
				var bankName = component.get("v.BankName2");
                var accountNumber = component.get("v.Accountno2");             
            } else if (sectionno == "3") {  
            	var bankName = component.get("v.BankName3");
                var accountNumber = component.get("v.Accountno3");
            }
            console.log('bankName '+bankName);
            console.log('accountNumber '+accountNumber);
            
            if(bankName == undefined || bankName == null || bankName == '' || accountNumber == undefined || accountNumber == null || accountNumber == '') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Please enter Perfios Bank and Account Number'
                });
                toastEvent.fire();     
            } else {
                this.fetchBRERecords(component, event, helper,bankName ,accountNumber,sectionno, true);
            }
        }    
    },
    
    fetchPerfiosDataHelper: function(component, event, helper, disableBank1, disableBank11, sectionno)  {
    	if(disableBank1 == false && disableBank11 == false) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Please enter bank and account number'
            });
            toastEvent.fire(); 
        } else if(disableBank1 && disableBank11) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Some error occurred, Please contact system admin'
            });
            toastEvent.fire(); 
        } else if(disableBank1) {
            if(sectionno == "1") {
                var bankName = component.get("v.BankName11");
                var accountNumber = component.get("v.Accountno11");  
            } else if(sectionno == "2") {
            	var bankName = component.get("v.BankName21");
                var accountNumber = component.get("v.Accountno21");     
            } else if (sectionno == "3") {
            	var bankName = component.get("v.BankName31");
                var accountNumber = component.get("v.Accountno31");     
            }   
            console.log('accountNumber'+accountNumber);
            console.log('bankName '+bankName);
            
            if(bankName == undefined || bankName == null || bankName =='' || accountNumber == undefined || accountNumber == null || accountNumber == '') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Please enter PDC Bank Master and Account Number'
                });
                toastEvent.fire();    
            } else {
                component.set("v.FetchPerfiosSectionNo",sectionno);
                component.set("v.showConfirmDialog",true);
            }
        } else if(disableBank11) {
            if(sectionno == "1") {
                var bankName = component.get("v.BankName1");
                var accountNumber = component.get("v.Accountno1");
            } else if(sectionno == "2") {
                var bankName = component.get("v.BankName2");
                var accountNumber = component.get("v.Accountno2");
            } else if(sectionno == "3") {
                var bankName = component.get("v.BankName3");
                var accountNumber = component.get("v.Accountno3");
            }
            console.log('bankName '+bankName);
            console.log('accountNumber '+accountNumber);
            
            if(bankName == undefined || bankName == null || bankName == '' || accountNumber == undefined || accountNumber == null || accountNumber == '') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Please enter Perfios Bank and Account Number'
                });
                toastEvent.fire();     
            } else {
                component.set("v.FetchPerfiosSectionNo",sectionno);
                component.set("v.showConfirmDialog",true);
            }
        }    
    },
    saveHelper: function(component, event, helper, disableBank1, disableBank11, perfiosButtonClicked, sectionno,saveAll)  {    
        //console.log('$ save 1 $'+ JSON.stringify(component.get("v.MonthList1")));
        var isverified = component.find("isChecked").get("v.value");
        //console.log('is verified val--'+ isverified);
        //console.log('disableBank1 '+disableBank1);
        //console.log('disableBank11 '+disableBank11);  
        var errorMsg = false;
        if (sectionno == "2") {
            var list = component.get("v.MonthList2");
            var valueFoundin2 = false;
            console.log('list HERE'+JSON.stringify(list));
            if(list != '') {
                for(var key in list) {
                    console.log('PRINTING THE LIST '+JSON.stringify(list[key]));
                    console.log('PRINTING VAL '+JSON.stringify(list[key].inoutrec));
                    if(JSON.stringify(list[key].inoutrec) != '{}') {
                        console.log('PRINTING THE LIST-->>'+JSON.stringify(list[key]));
                        console.log('PRINTING VAL-->>'+JSON.stringify(list[key].inoutrec));
                        console.log('NO BLANK BLANK');
                        valueFoundin2 = true;
                    }
                }
            }
            console.log('valueFoundin2 '+valueFoundin2);
        } else if (sectionno == "3") {
            var list = component.get("v.MonthList3");
            var valueFoundin3 = false;
            console.log('list HERE'+JSON.stringify(list));
            if(list != '') {
                for(var key in list) {
                    console.log('PRINTING THE LIST '+JSON.stringify(list[key]));
                    console.log('PRINTING VAL '+JSON.stringify(list[key].inoutrec));
                    if(JSON.stringify(list[key].inoutrec) != '{}') {
                        console.log('NO BLANK BLANK');
                        valueFoundin3 = true;
                    }
                }
            }
            console.log('valueFoundin3 '+valueFoundin3);
        }
        if(disableBank1 == false && disableBank11 == false ) {
            errorMsg = true;
            
            if(saveAll == "false" || (saveAll == "true" && (sectionno =="1" || (sectionno =="2" && valueFoundin2) || (sectionno =="3" && valueFoundin3)))) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Please enter bank details in section '+sectionno
                });
                toastEvent.fire();
            }
        } else if(disableBank1 && disableBank11 ) {
            errorMsg = true;
            //if(saveAll == "false" || (saveAll == "true" && sectionno =="1")) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Some error occurred, Please contact system admin in section '+sectionno
                });
                toastEvent.fire(); 
            //}
        } else if(disableBank1) {
            
            if(sectionno == "1") {
                var bankName = component.get("v.BankName11");
                var accountNumber = component.get("v.Accountno11");  
                var accountType = component.get("v.AccountType11");
                
            } else if(sectionno == "2") {
            	var bankName = component.get("v.BankName21");
                var accountNumber = component.get("v.Accountno21"); 
                var accountType = component.get("v.AccountType21");
                
            } else if (sectionno == "3") {
            	var bankName = component.get("v.BankName31");
                var accountNumber = component.get("v.Accountno31");
                var accountType = component.get("v.AccountType31");
                
            }
            console.log('acc1 '+component.get("v.Accountno11"));
            console.log('acc2 '+component.get("v.Accountno21"));
            console.log('acc3 '+component.get("v.Accountno31"));
            console.log('accountNumber'+accountNumber);
            console.log('bankName '+bankName);
                        
            if(bankName == undefined || bankName == null || bankName =='' || accountNumber == undefined || accountNumber == null || accountNumber == '' || accountType == undefined || accountType == null || accountType=='') {
                errorMsg = true;
                if(saveAll == "false" || (saveAll == "true" && (sectionno =="1" || (sectionno =="2" && valueFoundin2) || (sectionno =="3" && valueFoundin3)))) {
                    
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "",
                        "type": "Error",
                        "message": 'Please enter PDC Bank Master, Account Number, Account Type in section '+sectionno
                    });
                    toastEvent.fire();  
                }
            }
            
            
        } else if(disableBank11 ) {
            if(sectionno == "1") {
                var bankName = component.get("v.BankName1");
                var accountNumber = component.get("v.Accountno1");
                var accountType = component.get("v.AccountType1");
            } else if(sectionno == "2") {
                var bankName = component.get("v.BankName2");
                var accountNumber = component.get("v.Accountno2");
                var accountType = component.get("v.AccountType2");
                
            } else if(sectionno == "3") {
                var bankName = component.get("v.BankName3");
                var accountNumber = component.get("v.Accountno3");
                var accountType = component.get("v.AccountType3");
            }
            console.log('acc1 '+component.get("v.Accountno1"));
            console.log('acc2 '+component.get("v.Accountno2"));
            console.log('acc3 '+component.get("v.Accountno3"));
            console.log('bankName '+bankName);
            console.log('accountNumber '+accountNumber);
            
            if(bankName == undefined || bankName == null || bankName == '' || accountNumber == undefined || accountNumber == null || accountNumber == '' || accountType == undefined || accountType == null || accountType =='') {
                errorMsg = true;
                if(saveAll == "false" || (saveAll == "true" && (sectionno =="1" || (sectionno =="2" && valueFoundin2) || (sectionno =="3" && valueFoundin3)))) {
                    
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "",
                        "type": "Error",
                        "message": 'Please enter Perfios Bank, Account Number, Account Type in section '+sectionno
                    });
                    toastEvent.fire();  
                }
            } 
        }
        console.log('errorMsg in saveHelper '+errorMsg);
        if(errorMsg == false) {
            if(isverified === false){
                console.log('false isVerified');
                var abc = component.get("v.RowCountToVerify");
                this.doSave(component, event, helper,bankName ,accountNumber,accountType, sectionno,disableBank11, perfiosButtonClicked, abc);            
            }else{
                //Error Handling on save button   
                console.log('true isVerified');
                
                var abc = component.get("v.RowCountToVerify");
                if(sectionno == "1") {
                    var childComp = component.find('childComp1');  
                    childComp.slice(0,abc).reduce(function(componentlist, currentComponent){
                        currentComponent.CheckChildValidity();},childComp[0]);        
                    
                    var allValid = component.find('fieldId1').reduce(function (validSoFar, inputCmp) {
                        inputCmp.showHelpMessageIfInvalid();
                        return validSoFar && inputCmp.get('v.validity').valid;
                    }, true); 
                } else if(sectionno == "2") {
                        
                    var childComp = component.find('childComp2');  
                    console.log('childComp for section 2'+childComp);
                    //if(childComp != undefined) {
                    childComp.slice(0,abc).reduce(function(componentlist, currentComponent){
                        currentComponent.CheckChildValidity();},childComp[1]);        
                    
                    var allValid = component.find('fieldId2').reduce(function (validSoFar, inputCmp) {
                        inputCmp.showHelpMessageIfInvalid();
                        return validSoFar && inputCmp.get('v.validity').valid;
                    }, true); 
                    //}
                } else if(sectionno == "3") {
                    
                    var childComp = component.find('childComp3');  
                    //if(childComp != undefined) {
                    childComp.slice(0,abc).reduce(function(componentlist, currentComponent){
                        currentComponent.CheckChildValidity();},childComp[2]);        
                    
                    var allValid = component.find('fieldId3').reduce(function (validSoFar, inputCmp) {
                        inputCmp.showHelpMessageIfInvalid();
                        return validSoFar && inputCmp.get('v.validity').valid;
                    }, true); 
                    //}
                }    
                
                //do save logic if valid
                if(allValid){ 
                    //alert('ALl fields valid !!');
                    this.doSave(component, event, helper,bankName ,accountNumber,accountType, sectionno,disableBank11, perfiosButtonClicked , abc);   
                }
            }
        }
    
    } ,
    save1Helper : function(component, event, helper, saveAll){
      	var disableBank1 = component.get("v.disableBank1");
        var disableBank11 = component.get("v.disableBank11");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked");
        console.log('perfiosButtonClicked in save1Helper'+perfiosButtonClicked);
        this.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"1", "Save",saveAll);
        
    } ,
    save2Helper : function(component, event, helper, saveAll){
        var disableBank1 = component.get("v.disableBank2");
        var disableBank11 = component.get("v.disableBank21");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked2");
        console.log('perfiosButtonClicked in save1Helper2'+perfiosButtonClicked);
        this.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"2", "Save",saveAll);
        
    }, 
	save3Helper : function(component, event, helper, saveAll){    
        var disableBank1 = component.get("v.disableBank3");
        var disableBank11 = component.get("v.disableBank31");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked3");
        console.log('perfiosButtonClicked in save1Helper3'+perfiosButtonClicked);
        this.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"3", "Save",saveAll);
        
    },
    checkDuplicates : function(component, event, helper, disableBank1, disableBank11, perfiosButtonClicked, sectionno, action, saveAll)   {  
        if(sectionno == "1") {
            if( 
              ( (((component.get("v.BankName11") == component.get("v.BankName21")) && (component.get("v.BankName11") != undefined) && (component.get("v.BankName11") != null) && (component.get("v.BankName11") != '') && (component.get("v.BankName21") != undefined) && (component.get("v.BankName21") != null) && (component.get("v.BankName21") != '')) || ((component.get("v.BankName11") == component.get("v.BankName2")) && (component.get("v.BankName11") != undefined) && (component.get("v.BankName11") != null) && (component.get("v.BankName11") != '') && (component.get("v.BankName2") != undefined) && (component.get("v.BankName2") != null) && (component.get("v.BankName2") != '')) || ((component.get("v.BankName1") == component.get("v.BankName2")) && (component.get("v.BankName1") != undefined) && (component.get("v.BankName1") != null) && (component.get("v.BankName1") != '') && (component.get("v.BankName2") != undefined) && (component.get("v.BankName2") != null) && (component.get("v.BankName2") != '')) || ((component.get("v.BankName1") == component.get("v.BankName21")) && (component.get("v.BankName1") != undefined) && (component.get("v.BankName1") != null) && (component.get("v.BankName1") != '') && (component.get("v.BankName21") != undefined) && (component.get("v.BankName21") != null) && (component.get("v.BankName21") != '') ) )  &&
                (((component.get("v.Accountno11") == component.get("v.Accountno21"))  && (component.get("v.Accountno11") != undefined) && (component.get("v.Accountno11") != null) && (component.get("v.Accountno11") != '') && (component.get("v.Accountno21") != undefined) && (component.get("v.Accountno21") != null) && (component.get("v.Accountno21") != '' )) || ((component.get("v.Accountno11") == component.get("v.Accountno2"))  && (component.get("v.Accountno11") != undefined) && (component.get("v.Accountno11") != null) && (component.get("v.Accountno11") != '') && (component.get("v.Accountno2") != undefined) && (component.get("v.Accountno2") != null) && (component.get("v.Accountno2") != '' ))|| ((component.get("v.Accountno1") == component.get("v.Accountno2"))  && (component.get("v.Accountno1") != undefined) && (component.get("v.Accountno1") != null) && (component.get("v.Accountno1") != '') && (component.get("v.Accountno2") != undefined) && (component.get("v.Accountno2") != null) && (component.get("v.Accountno2") != '' )) || ((component.get("v.Accountno1") == component.get("v.Accountno21"))  && (component.get("v.Accountno1") != undefined) && (component.get("v.Accountno1") != null) && (component.get("v.Accountno1") != '') && (component.get("v.Accountno21") != undefined) && (component.get("v.Accountno21") != null) && (component.get("v.Accountno21") != '') ) )) ||
                    
              ( (((component.get("v.BankName31") == component.get("v.BankName11")) && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '' && component.get("v.BankName11") != undefined && component.get("v.BankName11") != null && component.get("v.BankName11") != '') || ((component.get("v.BankName31") == component.get("v.BankName1")) && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '' && component.get("v.BankName1") != undefined && component.get("v.BankName1") != null && component.get("v.BankName1") != '') || ((component.get("v.BankName3") == component.get("v.BankName11")) && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' && component.get("v.BankName11") != undefined && component.get("v.BankName11") != null && component.get("v.BankName11") != '') || ((component.get("v.BankName3") == component.get("v.BankName1")) && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' && component.get("v.BankName1") != undefined && component.get("v.BankName1") != null && component.get("v.BankName1") != '' ) )  &&
                (((component.get("v.Accountno31") == component.get("v.Accountno11"))  && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' && component.get("v.Accountno11") != undefined && component.get("v.Accountno11") != null && component.get("v.Accountno11") != '' ) || ((component.get("v.Accountno31") == component.get("v.Accountno1"))  && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' && component.get("v.Accountno1") != undefined && component.get("v.Accountno1") != null && component.get("v.Accountno1") != '' )|| ((component.get("v.Accountno3") == component.get("v.Accountno11"))  && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' && component.get("v.Accountno11") != undefined && component.get("v.Accountno11") != null && component.get("v.Accountno11") != '' ) || ((component.get("v.Accountno3") == component.get("v.Accountno1"))  && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' && component.get("v.Accountno1") != undefined && component.get("v.Accountno1") != null && component.get("v.Accountno1") != '' ) ))    
            ){
                //errorMsg = true;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Duplicate Bank Details entered in section 1'
                });
                toastEvent.fire();    
            } else {
                if(action == "Save") {
                	this.saveHelper(component, event, helper, disableBank1, disableBank11,perfiosButtonClicked, "1",saveAll);   
                } else if(action == "Perfios") {
                    this.fetchPerfiosDataHelper(component, event, helper,disableBank1, disableBank11, "1");
                } else if(action == "Banking") {
                    this.fetchBRERecordsHelper(component, event, helper, disableBank1, disableBank11, "1");
                }
            }
        } else if(sectionno== "2") {
            if( 
              ( (((component.get("v.BankName11") == component.get("v.BankName21")) && (component.get("v.BankName11") != undefined) && (component.get("v.BankName11") != null) && (component.get("v.BankName11") != '') && (component.get("v.BankName21") != undefined) && (component.get("v.BankName21") != null) && (component.get("v.BankName21") != '')) || ((component.get("v.BankName11") == component.get("v.BankName2")) && (component.get("v.BankName11") != undefined) && (component.get("v.BankName11") != null) && (component.get("v.BankName11") != '') && (component.get("v.BankName2") != undefined) && (component.get("v.BankName2") != null) && (component.get("v.BankName2") != '')) || ((component.get("v.BankName1") == component.get("v.BankName2")) && (component.get("v.BankName1") != undefined) && (component.get("v.BankName1") != null) && (component.get("v.BankName1") != '') && (component.get("v.BankName2") != undefined) && (component.get("v.BankName2") != null) && (component.get("v.BankName2") != '')) || ((component.get("v.BankName1") == component.get("v.BankName21")) && (component.get("v.BankName1") != undefined) && (component.get("v.BankName1") != null) && (component.get("v.BankName1") != '') && (component.get("v.BankName21") != undefined) && (component.get("v.BankName21") != null) && (component.get("v.BankName21") != '') ) )  &&
                (((component.get("v.Accountno11") == component.get("v.Accountno21"))  && (component.get("v.Accountno11") != undefined) && (component.get("v.Accountno11") != null) && (component.get("v.Accountno11") != '') && (component.get("v.Accountno21") != undefined) && (component.get("v.Accountno21") != null) && (component.get("v.Accountno21") != '' )) || ((component.get("v.Accountno11") == component.get("v.Accountno2"))  && (component.get("v.Accountno11") != undefined) && (component.get("v.Accountno11") != null) && (component.get("v.Accountno11") != '') && (component.get("v.Accountno2") != undefined) && (component.get("v.Accountno2") != null) && (component.get("v.Accountno2") != '' ))|| ((component.get("v.Accountno1") == component.get("v.Accountno2"))  && (component.get("v.Accountno1") != undefined) && (component.get("v.Accountno1") != null) && (component.get("v.Accountno1") != '') && (component.get("v.Accountno2") != undefined) && (component.get("v.Accountno2") != null) && (component.get("v.Accountno2") != '' )) || ((component.get("v.Accountno1") == component.get("v.Accountno21"))  && (component.get("v.Accountno1") != undefined) && (component.get("v.Accountno1") != null) && (component.get("v.Accountno1") != '') && (component.get("v.Accountno21") != undefined) && (component.get("v.Accountno21") != null) && (component.get("v.Accountno21") != '') ) )) ||
                
              ( (((component.get("v.BankName21") == component.get("v.BankName31")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '') || ((component.get("v.BankName21") == component.get("v.BankName3")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '') || ((component.get("v.BankName2") == component.get("v.BankName31")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '') || ((component.get("v.BankName2") == component.get("v.BankName3")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' ) )  &&
                (((component.get("v.Accountno21") == component.get("v.Accountno31"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' ) || ((component.get("v.Accountno21") == component.get("v.Accountno3"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' )|| ((component.get("v.Accountno2") == component.get("v.Accountno31"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' ) || ((component.get("v.Accountno2") == component.get("v.Accountno3"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' ) ))
                
            ){
                //errorMsg = true;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Duplicate Bank Details entered in section 2'
                });
                toastEvent.fire();    
            } else {
                 if(action == "Save") {
                	this.saveHelper(component, event, helper,disableBank1, disableBank11,perfiosButtonClicked, "2",saveAll); 
                 } else if(action == "Perfios") {
                     this.fetchPerfiosDataHelper(component, event, helper,disableBank1, disableBank11, "2");
                 } else if(action == "Banking") {
                     this.fetchBRERecordsHelper(component, event, helper, disableBank1, disableBank11, "2");
                 }
            }

        } else if(sectionno =="3") {
             console.log('BANK 2-3 compare '+(((component.get("v.BankName21") == component.get("v.BankName31")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '') || ((component.get("v.BankName21") == component.get("v.BankName3")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '') || ((component.get("v.BankName2") == component.get("v.BankName31")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '') || ((component.get("v.BankName2") == component.get("v.BankName3")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' )  ));
			  console.log('ACCOUNT 2-3 compare '+ (((component.get("v.Accountno21") == component.get("v.Accountno31"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' ) || ((component.get("v.Accountno21") == component.get("v.Accountno3"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' )|| ((component.get("v.Accountno2") == component.get("v.Accountno31"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' ) || ((component.get("v.Accountno2") == component.get("v.Accountno3"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' ) ));
			  console.log('BANK 1-3 compare '+(((component.get("v.BankName31") == component.get("v.BankName11")) && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '' && component.get("v.BankName11") != undefined && component.get("v.BankName11") != null && component.get("v.BankName11") != '') || ((component.get("v.BankName31") == component.get("v.BankName1")) && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '' && component.get("v.BankName1") != undefined && component.get("v.BankName1") != null && component.get("v.BankName1") != '') || ((component.get("v.BankName3") == component.get("v.BankName11")) && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' && component.get("v.BankName11") != undefined && component.get("v.BankName11") != null && component.get("v.BankName11") != '') || ((component.get("v.BankName3") == component.get("v.BankName1")) && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' && component.get("v.BankName1") != undefined && component.get("v.BankName1") != null && component.get("v.BankName1") != '' ) ));
			  console.log('ACCOUNT 1-3 compare '+(((component.get("v.Accountno31") == component.get("v.Accountno11"))  && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' && component.get("v.Accountno11") != undefined && component.get("v.Accountno11") != null && component.get("v.Accountno11") != '' ) || ((component.get("v.Accountno31") == component.get("v.Accountno1"))  && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' && component.get("v.Accountno1") != undefined && component.get("v.Accountno1") != null && component.get("v.Accountno1") != '' )||  ((component.get("v.Accountno3") == component.get("v.Accountno11"))  && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' && component.get("v.Accountno11") != undefined && component.get("v.Accountno11") != null && component.get("v.Accountno11") != '' ) || ((component.get("v.Accountno3") == component.get("v.Accountno1"))  && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' && component.get("v.Accountno1") != undefined && component.get("v.Accountno1") != null && component.get("v.Accountno1") != '' ) ));

            if( 
                              
              ( (((component.get("v.BankName21") == component.get("v.BankName31")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '') || ((component.get("v.BankName21") == component.get("v.BankName3")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '') || ((component.get("v.BankName2") == component.get("v.BankName31")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '') || ((component.get("v.BankName2") == component.get("v.BankName3")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' ) )  &&
                (((component.get("v.Accountno21") == component.get("v.Accountno31"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' ) || ((component.get("v.Accountno21") == component.get("v.Accountno3"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' )|| ((component.get("v.Accountno2") == component.get("v.Accountno31"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' ) || ((component.get("v.Accountno2") == component.get("v.Accountno3"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' ) )) ||
                
              ( (((component.get("v.BankName31") == component.get("v.BankName11")) && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '' && component.get("v.BankName11") != undefined && component.get("v.BankName11") != null && component.get("v.BankName11") != '') || ((component.get("v.BankName31") == component.get("v.BankName1")) && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '' && component.get("v.BankName1") != undefined && component.get("v.BankName1") != null && component.get("v.BankName1") != '') || ((component.get("v.BankName3") == component.get("v.BankName11")) && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' && component.get("v.BankName11") != undefined && component.get("v.BankName11") != null && component.get("v.BankName11") != '') || ((component.get("v.BankName3") == component.get("v.BankName1")) && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' && component.get("v.BankName1") != undefined && component.get("v.BankName1") != null && component.get("v.BankName1") != '' ) )  &&
                (((component.get("v.Accountno31") == component.get("v.Accountno11"))  && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' && component.get("v.Accountno11") != undefined && component.get("v.Accountno11") != null && component.get("v.Accountno11") != '' ) || ((component.get("v.Accountno31") == component.get("v.Accountno1"))  && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' && component.get("v.Accountno1") != undefined && component.get("v.Accountno1") != null && component.get("v.Accountno1") != '' )|| ((component.get("v.Accountno3") == component.get("v.Accountno11"))  && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' && component.get("v.Accountno11") != undefined && component.get("v.Accountno11") != null && component.get("v.Accountno11") != '' ) || ((component.get("v.Accountno3") == component.get("v.Accountno1"))  && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' && component.get("v.Accountno1") != undefined && component.get("v.Accountno1") != null && component.get("v.Accountno1") != '' ) ))
                
            ){
                //errorMsg = true;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Duplicate Bank Details entered in section 3'
                });
                toastEvent.fire();    
            } else {
                 if(action == "Save") {
                	this.saveHelper(component, event, helper,disableBank1, disableBank11,perfiosButtonClicked, "3",saveAll);
                 } else if(action == "Perfios") {
                    this.fetchPerfiosDataHelper(component, event, helper,disableBank1, disableBank11, "3"); 
                 }  else if(action == "Banking") {
                     helper.fetchBRERecordsHelper(component, event, helper, disableBank1, disableBank11, "3");
                 }  
            }
        }
    } ,       
    excludeSection : function(component,event,helper, sectionno) {

        var action = component.get("c.removeBankSection");
        action.setParams({
            'CDrecID' : component.get("v.custRecordId") , 
            'sectionno' : sectionno
        })
        $A.enqueueAction(action); 
    }
})