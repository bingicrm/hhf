({
    initRecord: function(component, event, helper) {
        // call the apex class method and fetch the monthly inflow outflow related to customer intergration record 
        //alert('--init working fine');
        console.log('recordId(((((( ');
        var isverified = component.find("isChecked").get("v.value");
        
        component.set("v.isverified",isverified);
        console.log('VERIFIED STATUS '+component.get("v.isverified"));
        helper.getMonthlyBankingList(component,helper,event);
        helper.docStatusPickListVal(component,helper, event);
        //helper.getMonthlyBankUpdated(component,helper,event);
        
    },
    disableBankName11: function (component, event, helper) {
        var bnkname = component.get("v.BankName1");
        var Accountno1 = component.get("v.Accountno1");
        component.set("v.perfiosButtonClicked" ,false);
        if(bnkname != undefined && bnkname != null && bnkname != '') {
            component.set("v.disableBank11",true);
        } else {
            if(Accountno1 == undefined || Accountno1 == '' || Accountno1 == null) {
                component.set("v.disableBank11",false);
            }
        } 
        console.log(component.get("v.disableBank11"));
    },
    disableBankName21: function (component, event, helper) {
        var bnkname2 = component.get("v.BankName2");
        var Accountno2 = component.get("v.Accountno2");
        component.set("v.perfiosButtonClicked2" ,false);
        if(bnkname2 != undefined && bnkname2 != null && bnkname2 != '') {
            component.set("v.disableBank21",true);
        } else {
            if(Accountno2 == undefined || Accountno2 == '' || Accountno2 == null) {
                component.set("v.disableBank21",false);
            }
        } 
        console.log(component.get("v.disableBank21"));
    },
    disableBankName31: function (component, event, helper) {
        var bnkname3 = component.get("v.BankName3");
        var Accountno3 = component.get("v.Accountno3");
        component.set("v.perfiosButtonClicked3" ,false);
        if(bnkname3 != undefined && bnkname3 != null && bnkname3 != '') {
            component.set("v.disableBank31",true);
        } else {
            if(Accountno3 == undefined || Accountno3 == '' || Accountno3 == null) {
                component.set("v.disableBank31",false);
            }
        } 
        console.log(component.get("v.disableBank31"));
    },
    disableBankName1 : function (component, event, helper) {
        console.log('Hi');
        var bnkName11 = component.get("v.BankName11");
        var Accountno11 = component.get("v.Accountno11");
        component.set("v.perfiosButtonClicked" ,false);
        if(bnkName11 != undefined && bnkName11 != null && bnkName11 != '') {
            component.set("v.disableBank1",true);
        } else {
            if(Accountno11 == undefined || Accountno11 == '' || Accountno11 == null) {
                
                
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    console.log('STATE IS '+state);
                    if (state === "SUCCESS") {   
                        console.log('RESIIII '+response.getReturnValue()); 
                        var x1 = response.getReturnValue();
                        console.log('Here in this'+JSON.stringify(x1)); 
                        if(x1 == null) {
                            component.set("v.disableBank1" , true);
                        } else {
                            component.set("v.disableBank1",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
        console.log(component.get("v.disableBank1"));
    },
    disableBankName2 : function (component, event, helper) {
        console.log('Hi');
        var bnkName21 = component.get("v.BankName21");
        var Accountno21 = component.get("v.Accountno21");
        component.set("v.perfiosButtonClicked2" ,false);
        if(bnkName21 != undefined && bnkName21 != null && bnkName21 != '') {
            component.set("v.disableBank2",true);
        } else {
            if(Accountno21 == undefined || Accountno21 == '' || Accountno21 == null) {
                
                
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    console.log('STATE IS '+state);
                    if (state === "SUCCESS") {   
                        console.log('RESIIII '+response.getReturnValue()); 
                        var x1 = response.getReturnValue();
                        console.log('Here in this'+JSON.stringify(x1)); 
                        if(x1 == null) {
                            component.set("v.disableBank2" , true);
                        } else {
                            component.set("v.disableBank2",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
        console.log(component.get("v.disableBank2"));
    },
    
    disableBankName3 : function (component, event, helper) {
        console.log('Hi');
        var bnkName31 = component.get("v.BankName31");
        var Accountno31 = component.get("v.Accountno31");
        component.set("v.perfiosButtonClicked3" ,false);
        if(bnkName31 != undefined && bnkName31 != null && bnkName31 != '') {
            component.set("v.disableBank3",true);
        } else {
            if(Accountno31 == undefined || Accountno31 == '' || Accountno31 == null) {
                
                
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    console.log('STATE IS '+state);
                    if (state === "SUCCESS") {   
                        console.log('RESIIII '+response.getReturnValue()); 
                        var x1 = response.getReturnValue();
                        console.log('Here in this'+JSON.stringify(x1)); 
                        if(x1 == null) {
                            component.set("v.disableBank3" , true);
                        } else {
                            component.set("v.disableBank3",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
        console.log(component.get("v.disableBank3"));
    },
    
    disable1BankName11: function (component, event, helper) {
        var Accountno1 = component.get("v.Accountno1");
        var bnkname = component.get("v.BankName1");
        component.set("v.perfiosButtonClicked" ,false);
        if(Accountno1 != undefined && Accountno1 != null && Accountno1 != '') {
            component.set("v.disableBank11",true);
        } else {
            if(bnkname == undefined || bnkname == '' ||bnkname== null) {
                component.set("v.disableBank11",false);
            }
        } 
        console.log(component.get("v.disableBank11"));
    },
    disable2BankName11: function (component, event, helper) {
        var AccountType1 = component.get("v.AccountType1");
        var bnkname = component.get("v.BankName1");
        component.set("v.perfiosButtonClicked" ,false);
        if(AccountType1 != undefined && AccountType1 != null && AccountType1 != '') {
            component.set("v.disableBank11",true);
        } else {
            if(bnkname == undefined || bnkname == '' ||bnkname== null) {
                component.set("v.disableBank11",false);
            }
        } 
        console.log(component.get("v.disableBank11"));
    },
    disable1BankName21: function (component, event, helper) {
        var Accountno2 = component.get("v.Accountno2");
        var bnkname2 = component.get("v.BankName2");
        component.set("v.perfiosButtonClicked2" ,false);
        if(Accountno2 != undefined && Accountno2 != null && Accountno2 != '') {
            component.set("v.disableBank21",true);
        } else {
            if(bnkname2 == undefined || bnkname2 == '' ||bnkname2== null) {
                component.set("v.disableBank21",false);
            }
        } 
        console.log(component.get("v.disableBank21"));
    },
    disable2BankName21: function (component, event, helper) {
        var AccountType2 = component.get("v.AccountType2");
        var bnkname = component.get("v.BankName2");
        component.set("v.perfiosButtonClicked" ,false);
        if(AccountType2 != undefined && AccountType2 != null && AccountType2 != '') {
            component.set("v.disableBank21",true);
        } else {
            if(bnkname == undefined || bnkname == '' ||bnkname== null) {
                component.set("v.disableBank21",false);
            }
        } 
        console.log(component.get("v.disableBank21"));
    },
    disable1BankName31: function (component, event, helper) {
        var Accountno3 = component.get("v.Accountno3");
        var bnkname3 = component.get("v.BankName3");
        component.set("v.perfiosButtonClicked3" ,false);
        if(Accountno3 != undefined && Accountno3 != null && Accountno3 != '') {
            component.set("v.disableBank31",true);
        } else {
            if(bnkname3 == undefined || bnkname3 == '' ||bnkname3== null) {
                component.set("v.disableBank31",false);
            }
        } 
        console.log(component.get("v.disableBank31"));
    },
    disable2BankName31: function (component, event, helper) {
        var AccountType3 = component.get("v.AccountType3");
        var bnkname = component.get("v.BankName3");
        component.set("v.perfiosButtonClicked" ,false);
        if(AccountType3 != undefined && AccountType3 != null && AccountType3 != '') {
            component.set("v.disableBank31",true);
        } else {
            if(bnkname == undefined || bnkname == '' ||bnkname== null) {
                component.set("v.disableBank31",false);
            }
        } 
        console.log(component.get("v.disableBank31"));
    },
    disable1BankName1: function (component, event, helper) {
        console.log('Hi');
        var Accountno11 = component.get("v.Accountno11");
        var bnkName11 = component.get("v.BankName11");
        component.set("v.perfiosButtonClicked" ,false);
        if(Accountno11 != undefined && Accountno11 != null && Accountno11 != '') {
            component.set("v.disableBank1",true);
        } else {
            if(bnkName11 == undefined || bnkName11 == '' || bnkName11 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    console.log('STATE IS '+state);
                    if (state === "SUCCESS") {   
                        console.log('RESIIII '+response.getReturnValue());  
                        var x1 = response.getReturnValue();
                        console.log('Here in this'+JSON.stringify(x1)); 
                        if(x1 == null) {
                            component.set("v.disableBank1" , true);
                        } else {
                            component.set("v.disableBank1",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
        console.log(component.get("v.disableBank1"));
    },
    disable2BankName1: function (component, event, helper) {
        console.log('Hi');
        var Accounttype11 = component.get("v.AccountType11");
        var bnkName11 = component.get("v.BankName11");
        component.set("v.perfiosButtonClicked" ,false);
        if(Accounttype11 != undefined && Accounttype11 != null && Accounttype11 != '') {
            component.set("v.disableBank1",true);
        } else {
            if(bnkName11 == undefined || bnkName11 == '' || bnkName11 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    console.log('STATE IS '+state);
                    if (state === "SUCCESS") {   
                        console.log('RESIIII '+response.getReturnValue());  
                        var x1 = response.getReturnValue();
                        console.log('Here in this'+JSON.stringify(x1)); 
                        if(x1 == null) {
                            component.set("v.disableBank1" , true);
                        } else {
                            component.set("v.disableBank1",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
        console.log(component.get("v.disableBank1"));
    },
    disable1BankName2: function (component, event, helper) {
        console.log('Hi');
        var Accountno21 = component.get("v.Accountno21");
        var bnkName21 = component.get("v.BankName21");
        component.set("v.perfiosButtonClicked2" ,false);
        if(Accountno21 != undefined && Accountno21 != null && Accountno21 != '') {
            component.set("v.disableBank2",true);
        } else {
            if(bnkName21 == undefined || bnkName21 == '' || bnkName21 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    console.log('STATE IS '+state);
                    if (state === "SUCCESS") {   
                        console.log('RESIIII '+response.getReturnValue());  
                        var x1 = response.getReturnValue();
                        console.log('Here in this'+JSON.stringify(x1)); 
                        if(x1 == null) {
                            component.set("v.disableBank2" , true);
                        } else {
                            component.set("v.disableBank2",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
        console.log(component.get("v.disableBank2"));
    },
    disable2BankName2: function (component, event, helper) {
        console.log('Hi');
        var Accounttype21 = component.get("v.AccountType21");
        var bnkName21 = component.get("v.BankName21");
        component.set("v.perfiosButtonClicked" ,false);
        if(Accounttype21 != undefined && Accounttype21 != null && Accounttype21 != '') {
            component.set("v.disableBank2",true);
        } else {
            if(bnkName21 == undefined || bnkName21 == '' || bnkName21 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    console.log('STATE IS '+state);
                    if (state === "SUCCESS") {   
                        console.log('RESIIII '+response.getReturnValue());  
                        var x1 = response.getReturnValue();
                        console.log('Here in this'+JSON.stringify(x1)); 
                        if(x1 == null) {
                            component.set("v.disableBank2" , true);
                        } else {
                            component.set("v.disableBank2",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
        console.log(component.get("v.disableBank2"));
    },
    disable1BankName3: function (component, event, helper) {
        console.log('Hi');
        var Accountno31 = component.get("v.Accountno31");
        var bnkName31 = component.get("v.BankName31");
        component.set("v.perfiosButtonClicked3" ,false);
        if(Accountno31 != undefined && Accountno31 != null && Accountno31 != '') {
            component.set("v.disableBank3",true);
        } else {
            if(bnkName31 == undefined || bnkName31 == '' || bnkName31 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    console.log('STATE IS '+state);
                    if (state === "SUCCESS") {   
                        console.log('RESIIII '+response.getReturnValue());  
                        var x1 = response.getReturnValue();
                        console.log('Here in this'+JSON.stringify(x1)); 
                        if(x1 == null) {
                            component.set("v.disableBank3" , true);
                        } else {
                            component.set("v.disableBank3",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
        console.log(component.get("v.disableBank3"));
    },
    disable2BankName3: function (component, event, helper) {
        console.log('Hi');
        var Accounttype31 = component.get("v.AccountType31");
        console.log('Accounttype31 '+Accounttype31);
        var bnkName31 = component.get("v.BankName31");
        console.log('bnkName31 '+bnkName31);
        component.set("v.perfiosButtonClicked" ,false);
        if(Accounttype31 != undefined && Accounttype31 != null && Accounttype31 != '') {
            component.set("v.disableBank3",true);
        } else {
            if(bnkName31 == undefined || bnkName31 == '' || bnkName31 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    console.log('STATE IS '+state);
                    if (state === "SUCCESS") {   
                        console.log('RESIIII '+response.getReturnValue());  
                        var x1 = response.getReturnValue();
                        console.log('Here in this'+JSON.stringify(x1)); 
                        if(x1 == null) {
                            component.set("v.disableBank3" , true);
                        } else {
                            component.set("v.disableBank3",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
        console.log(component.get("v.disableBank3"));
    },
    savePanel: function(component, event, helper) {
        component.set("v.disableInput", false);
        component.set("v.disableInput1",false)
        component.set("v.disableInput2",false);
        helper.save1Helper(component, event, helper, "true"); 
        helper.save2Helper(component, event, helper, "true"); 
        helper.save3Helper(component, event, helper, "true"); 
        /*var disableBank1 = component.get("v.disableBank1");
        var disableBank11 = component.get("v.disableBank11");
        var abc = component.get("v.RowCountToVerify");
        if(disableBank1 == false && disableBank11 == false) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Please enter bank and account number'
            });
            toastEvent.fire(); 
        } else if(disableBank1 && disableBank11) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Some error occurred, Please contact system admin'
            });
            toastEvent.fire(); 
        } else if(disableBank1) {
            var bankName = component.get("v.BankName11");
            var accountNumber = component.get("v.Accountno11");  
            console.log('accountNumber'+accountNumber);
            console.log('bankName '+bankName);
            
            if(bankName == undefined || bankName == null || bankName =='' || accountNumber == undefined || accountNumber == null || accountNumber == '') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Please enter PDC Bank Master and Account Number'
                });
                toastEvent.fire();    
            } else {
                helper.savePanelHelper(component, event, helper,bankName ,accountNumber, false, abc);
            }
        } else if(disableBank11) {
            var bankName = component.get("v.BankName1");
            var accountNumber = component.get("v.Accountno1");
            console.log('bankName '+bankName);
            console.log('accountNumber '+accountNumber);
            
            if(bankName == undefined || bankName == null || bankName == '' || accountNumber == undefined || accountNumber == null || accountNumber == '') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Please enter Perfios Bank and Account Number'
                });
                toastEvent.fire();     
            } else {
                helper.savePanelHelper(component, event, helper,bankName ,accountNumber, true, abc);
            }
        }*/
    },   
    fetchRecords_section1 : function(component, event, helper) {
        var disableBank1 = component.get("v.disableBank1");
        var disableBank11 = component.get("v.disableBank11");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked");
        helper.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"1", "Banking");
        //helper.fetchBRERecordsHelper(component, event, helper, disableBank1, disableBank11, "1");
    },
    
    
    fetchRecords_section2 : function(component, event, helper) {
        var disableBank1 = component.get("v.disableBank2");
        var disableBank11 = component.get("v.disableBank21");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked2");
        helper.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"2", "Banking");
        //helper.fetchBRERecordsHelper(component, event, helper, disableBank1, disableBank11, "2");
    },
    fetchRecords_section3 : function(component, event, helper) {
        var disableBank1 = component.get("v.disableBank3");
        var disableBank11 = component.get("v.disableBank31");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked3");
        helper.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"3", "Banking");
        //helper.fetchBRERecordsHelper(component, event, helper, disableBank1, disableBank11, "3");
    },
    fetchPerfiosData_section1:function(component, event, helper) {
        
        var disableBank1 = component.get("v.disableBank1");
        var disableBank11 = component.get("v.disableBank11");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked");
        helper.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"1", "Perfios");
        //helper.fetchPerfiosDataHelper(component, event, helper,disableBank1, disableBank11, "1");
        
        
        /*var bnkname = component.get("v.BankName1");
        var accountNo = component.get("v.Accountno1");
        alert(bnkname+'  '+accountNo);
        if(bnkname==undefined || accountNo==undefined){            
          var allValid = component.find('fieldId1').reduce(function (validSoFar, inputCmp) {
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid;
            }, true);
        }else{
            var bnkname = component.get("v.BankName1");
        	var accountNo = component.get("v.Accountno1");
        	var bnkName11 = component.get("v.BankName11");
            var accountNo11 = component.get("v.Accountno11");
            console.log('bnkname '+bnkname);
            console.log('accountNo '+accountNo);
            console.log('accountNo11'+accountNo11);
            console.log('bnkName11 '+bnkName11);
            
            var errorMsg = false;
            if(((bnkname == undefined || bnkname == null || bnkname == '') && (bnkName11 == undefined || bnkName11 == null || bnkName11 =='')) || ((bnkname != undefined && bnkname != null && bnkname != '') && (bnkName11 != undefined && bnkName11 != null && bnkName11 != ''))) {
                errorMsg = true;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Please enter Perfios Bank Name or PDC Bank Master'
                });
                toastEvent.fire();
            }
            if(((accountNo == undefined || accountNo == null || accountNo == '') && (accountNo11 == undefined || accountNo11 == null || accountNo11 == '')) || ((accountNo != undefined && accountNo != null && accountNo != '') && (accountNo11 != undefined && accountNo11 != null && accountNo11 != ''))) {
                errorMsg = true;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Please enter Perfios Account Number or Account Number'
                });
                toastEvent.fire();
            }
            if(errorMsg == false) {
                component.set("v.FetchPerfiosSectionNo","1");
            	component.set("v.showConfirmDialog",true);
            }	
        }*/
        
    },
    fetchPerfiosData_section2:function(component, event, helper) {
        var disableBank1 = component.get("v.disableBank2");
        var disableBank11 = component.get("v.disableBank21");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked2");
        helper.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"2", "Perfios");
        //helper.fetchPerfiosDataHelper(component, event, helper,disableBank1, disableBank11, "2");
        
    },
    fetchPerfiosData_section3:function(component, event, helper) {
        var disableBank1 = component.get("v.disableBank3");
        var disableBank11 = component.get("v.disableBank31");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked3");
        helper.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"3", "Perfios");
        //helper.fetchPerfiosDataHelper(component, event, helper,disableBank1, disableBank11, "3");
    },    
    toggleSection: function(component, event, helper){
        // dynamically get aura:id name from 'data-auraId' attribute
        var sectionAuraId = event.target.getAttribute("data-auraId");
        // get section Div element using aura:id
        var sectionDiv = component.find(sectionAuraId).getElement();
        /* The search() method searches for 'slds-is-open' class, and returns the position of the match.
         * This method returns -1 if no match is found.
        */
        var sectionState = sectionDiv.getAttribute('class').search('slds-is-open'); 
        
        // -1 if 'slds-is-open' class is missing...then set 'slds-is-open' class else set slds-is-close class to element
        if(sectionState == -1){
            sectionDiv.setAttribute('class' , 'slds-section slds-is-open');
        }else{
            sectionDiv.setAttribute('class' , 'slds-section slds-is-close');
        }
    },
    
    AddNewSection: function(component, event, helper){
        console.log(component.get("v.sectionCount"));
        console.log('MonthList1'+JSON.stringify(component.get("v.MonthList1")));
        console.log('MonthList2'+JSON.stringify(component.get("v.MonthList2")));
        console.log('MonthList3'+JSON.stringify(component.get("v.MonthList3")));
        console.log('bankname '+component.get("v.BankName"));
        console.log('accountNo '+component.get("v.Accountno"));
        
        console.log('v.disableInput2'+ component.get('v.disableInput2'));
        console.log('v.DisableScreen'+ component.get('v.DisableScreen'));
        console.log('v.disableBank2 '+ component.get('v.disableBank2'));
        console.log('v.disableBankSection2 '+component.get('v.disableBankSection2')); 
        if(component.get("v.sectionCount")==1){
            
            var action = component.get("c.fetchMonthlyBanking");
            action.setParams({
                'CDrecID' : component.get("v.custRecordId"),
                'bnkname' : component.get("v.BankName"),
                'accountNo': component.get("v.Accountno"),
                'getBRERecord' : true,
                'perfiosBankDetails' : true
            })
            action.setCallback(this, function(response){
                var state = response.getState();
                console.log('In this method '+state);
                if (state === "SUCCESS") {   
                    component.set("v.MonthList", {});
                    var storeResponse = response.getReturnValue();   
                    console.log('--obj values--'+ JSON.stringify(response.getReturnValue()));                
                    //component.set("v.MonthList1", response.getReturnValue());// set list 1                
                    component.set("v.MonthList2", storeResponse);// set default list   
                    //component.set("v.MonthList3", storeResponse);
                }
            });
            $A.enqueueAction(action); 
            component.set("v.sectionCount",2);
            component.set("v.showSection2", true);
        }else if(component.get("v.sectionCount")==2){
            
            component.set("v.sectionCount",3);
            var action = component.get("c.fetchMonthlyBanking");
            action.setParams({
                'CDrecID' : component.get("v.custRecordId"),
                'bnkname' : component.get("v.BankName"),
                'accountNo': component.get("v.Accountno"),
                'getBRERecord' : true,
                'perfiosBankDetails' : true
            })
            action.setCallback(this, function(response){
                var state = response.getState();
                console.log('In this method '+state);
                if (state === "SUCCESS") {   
                    component.set("v.MonthList", {});
                    var storeResponse = response.getReturnValue();   
                    console.log('--obj values--'+ JSON.stringify(response.getReturnValue()));                
                    //component.set("v.MonthList1", response.getReturnValue());// set list 1                
                    //component.set("v.MonthList2", storeResponse);// set default list   
                    component.set("v.MonthList3", storeResponse);
                }
            });
            $A.enqueueAction(action); 
        }else if(component.get("v.sectionCount")==3){
            if(component.get("v.showSection2") == false) {
                var action11 = component.get("c.fetchMonthlyBanking");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'bnkname' : component.get("v.BankName"),
                    'accountNo': component.get("v.Accountno"),
                    'getBRERecord' : true,
                    'perfiosBankDetails' : true
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    console.log('In this method '+state);
                    if (state === "SUCCESS") {   
                        component.set("v.MonthList", {});
                        var storeResponse = response.getReturnValue();   
                        console.log('--obj values--'+ JSON.stringify(response.getReturnValue()));                
                        //component.set("v.MonthList1", response.getReturnValue());// set list 1                
                        //component.set("v.MonthList2", storeResponse);// set default list   
                        component.set("v.MonthList2", storeResponse);
                    }
                });
                $A.enqueueAction(action11);
                component.set("v.showSection2", true);
            } else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Only 3 banking sections can be added !!'
                });
                toastEvent.fire(); 
            }
            //alert('Only 3 banking sections can be added !!');
        }
        
    },   
    
    Save1: function(component, event, helper){
        helper.save1Helper(component, event, helper, "false");  
    },
    Edit1: function(component, event, helper){
        console.log('v.DisableScreen '+component.get("v.DisableScreen"));
        console.log('v.disableBank1 '+component.get("v.disableBank1"));
        console.log('v.disableBankSection1 '+component.get("v.disableBankSection1"));
        component.set("v.disableInput",false);
        //component.set('v.disableBankSection1',false);
        console.log('v.disableInput '+component.get("v.disableInput"));
    },    
    Save2: function(component, event, helper){
        helper.save2Helper(component, event, helper, "false");          
    },
    Edit2: function(component, event, helper){
        component.set("v.disableInput2",false);
        //component.set('v.disableBankSection2',false);
    },
    Save3: function(component, event, helper){
        helper.save3Helper(component, event, helper,"false");              
    }, 
    Edit3: function(component, event, helper){
        component.set("v.disableInput3",false);
        // component.set('v.disableBankSection3',false);
    },
    
    Spinnertoggle: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
    },    
    handleConfirmDialog : function(component, event, helper) {
        component.set('v.showConfirmDialog', true);
    },
    
    handleConfirmDialogYes : function(component, event, helper) {
        console.log('Yes');
        component.set('v.showConfirmDialog', false);
        if(component.get("v.FetchPerfiosSectionNo") =="1"){
            
            var disableBank1 = component.get("v.disableBank1");
            var disableBank11 = component.get("v.disableBank11");
            if(disableBank1) {
                var bankName = component.get("v.BankName11");
                var accountNumber = component.get("v.Accountno11"); 
                console.log('accountNumber'+accountNumber);
                console.log('bankName '+bankName);
                helper.fetchPerfiosData(component, event, helper,bankName ,accountNumber,"1", false);
            }  else if(disableBank11) {
                var bankName = component.get("v.BankName1");
                var accountNumber = component.get("v.Accountno1");
                console.log('bankName '+bankName);
                console.log('accountNumber '+accountNumber);
                helper.fetchPerfiosData(component, event, helper,bankName ,accountNumber,"1", true);
            }    
        }
        else if(component.get("v.FetchPerfiosSectionNo") =="2"){
            
            var disableBank1 = component.get("v.disableBank2");
            var disableBank11 = component.get("v.disableBank21");
            if(disableBank1) {
                var bankName = component.get("v.BankName21");
                var accountNumber = component.get("v.Accountno21"); 
                console.log('accountNumber'+accountNumber);
                console.log('bankName '+bankName);
                helper.fetchPerfiosData(component, event, helper,bankName ,accountNumber,"2", false);
            }  else if(disableBank11) {
                var bankName = component.get("v.BankName2");
                var accountNumber = component.get("v.Accountno2");
                console.log('bankName '+bankName);
                console.log('accountNumber '+accountNumber);
                helper.fetchPerfiosData(component, event, helper,bankName ,accountNumber,"2", true);
            }
        }
            else if(component.get("v.FetchPerfiosSectionNo") =="3"){
                
                var disableBank1 = component.get("v.disableBank3");
                var disableBank11 = component.get("v.disableBank31");
                if(disableBank1) {
                    var bankName = component.get("v.BankName31");
                    var accountNumber = component.get("v.Accountno31"); 
                    console.log('accountNumber'+accountNumber);
                    console.log('bankName '+bankName);
                    helper.fetchPerfiosData(component, event, helper,bankName ,accountNumber,"3", false);
                }  else if(disableBank11) {
                    var bankName = component.get("v.BankName3");
                    var accountNumber = component.get("v.Accountno3");
                    console.log('bankName '+bankName);
                    console.log('accountNumber '+accountNumber);
                    helper.fetchPerfiosData(component, event, helper,bankName ,accountNumber,"3", true);
                } 
            }
    },
    
    handleConfirmDialogNo : function(component, event, helper) {
        console.log('No');
        component.set('v.showConfirmDialog', false);
    },
    // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
    
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },
    closePanel2 : function(component,event,helper){
        /*console.log('DISABLED SECTION (((( '+component.get("v.disableBank2")); 
        console.log('DISABLED SECTION (((( '+component.get("v.disableBank21"));*/
        
        if(component.get("v.disableBank2")==true){
            var accntno = component.get("v.Accountno21");
            var bnkname = component.get("v.BankName21");
        }
        if(component.get("v.disableBank21")==true){
            var accntnoper = component.get("v.Accountno2");
            var bnknameper = component.get("v.BankName2");
        }
        /*console.log('accntno....' + component.get("v.Accountno21"));
        console.log('bnkname...' +  component.get("v.BankName21"));
        console.log('accntnoper...' + component.get("v.Accountno2"));
        console.log('bnknameper....' + component.get("v.BankName2"));*/
        //helper.excludeSection(component, event, helper,accntno,bnkname,accntnoper,bnknameper);
        if((component.get("v.disableBank3") == false && component.get("v.disableBank31") == false)) {
            component.set("v.sectionCount",1);
        } else {
            component.set("v.sectionCount",3);
        }
        component.set("v.showSection2", false);
        //component.set("v.MonthList2", '');
        component.set("v.BankName2", '');
        component.set("v.BankName21", '');
        component.set("v.Accountno2", '');
        component.set("v.Accountno21", '');
        component.set("v.disableBank2", false);
        component.set("v.disableBank21", false);
        helper.excludeSection(component, event, helper, "2");
        
        
    },
    closePanel3 : function(component,event,helper){
        //console.log('DISABLED SECTION (((( '+component.get("v.disableBank3")); 
        //console.log('DISABLED SECTION (((( '+component.get("v.disableBank31"));
        if(component.get("v.disableBank3")==true){
            var accntno = component.get("v.Accountno31");
            var bnkname = component.get("v.BankName31");
        }
        if(component.get("v.disableBank31")==true){
            var accntnoper = component.get("v.Accountno3");
            var bnknameper = component.get("v.BankName3");
        }
        /*console.log('accntno....' + component.get("v.Accountno31"));
        console.log('bnkname...' +  component.get("v.BankName31"));
        console.log('accntnoper...' + component.get("v.Accountno3"));
        console.log('bnknameper....' + component.get("v.BankName3"));*/
        //helper.excludeSection(component, event, helper,accntno,bnkname,accntnoper,bnknameper);
        if((component.get("v.disableBank2") == false && component.get("v.disableBank21") == false)) {
            console.log('SECTION 2 EMPTY FOUND');
            component.set("v.showSection2", false);
            component.set("v.sectionCount",1);
        } else {
            component.set("v.sectionCount",2);  
        }
        //component.set("v.MonthList3", '')
        component.set("v.BankName3", '');
        component.set("v.BankName31", '');
        component.set("v.Accountno3", '');
        component.set("v.Accountno31", '');
        component.set("v.disableBank3", false);
        component.set("v.disableBank31", false);
        helper.excludeSection(component, event, helper,"3");
        
    },
    closeMe : function(component, event, helper){
        if(component.get("v.disableInput") == true &&  component.get("v.disableInput2") == true && component.get("v.disableInput3") == true) {
            helper.closeMe(component, event, helper);        
        }  else {   
            component.set("v.ConfirmClose", true);
        }    
        
    },
    handleConfirmDialogboxYes : function(component, event, helper) {
        //component.set("v.ConfirmClose", true);
        helper.closeMe(component, event, helper);
    },
    handleConfirmDialogboxNo : function(component, event, helper) {
        component.set("v.ConfirmClose", false);        
    },
    onCheck : function(component, event, helper) { 
        var val1 = component.find("isChecked").get("v.value");
        alert('is verified js contrllr '+val1);
        if(val1==false || val1 == undefined || val1 == null)
            component.set("v.isverified", true);
        else
            component.set("v.isverified", false); 
        
    },
    // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
    
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    }
})