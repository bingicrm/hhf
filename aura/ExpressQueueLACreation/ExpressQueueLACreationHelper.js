({
	
    getCD : function(component, event, helper) {
        	var action = component.get('c.getPrimaryApplicant');
            var wrapval = JSON.stringify(component.get("v.laVal"));
            console.log('wrapval '+wrapval);
            action.setParams({
             laID : component.get("v.loanAppID"),
            });
            action.setCallback(this,function(a){
                var response = JSON.stringify(a.getReturnValue());
                console.log("response::::",response);
                if(a.getState() == 'SUCCESS'){
                if(response != null){
                    if(response.errorMsg != null){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "type": "Error",
                            "message": response.errorMsg
                        });
                        toastEvent.fire();
                    }
                    else{
                        component.set("v.isDisplay",true);
                        component.set("v.CDfetched",true);
                        component.set("v.CDWrap",a.getReturnValue().lstCD);
                        console.log('a.getReturnValue().lstCD:::'+JSON.stringify(a.getReturnValue().lstCD));
                        this.hideSpinner(component, event, helper);
                        //console.log("response.CD::::",response.lstCD);
                    }
                }
                }
                else{
				this.hideSpinner(component, event, helper);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "Error",
                        "message": "Error: Some error occured. Please contact System Admin."
                    });
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
    },
    addCDRecord : function(component, event, helper) {
        console.log('I am in ADd'+JSON.stringify(component.get("v.CDWrap")));
        	var action = component.get('c.addCDRow');
        	var index = component.get("v.CDWrap").length;
        	console.log('index is '+index);
        	component.set('v.LoanConId', index);
        	console.log('LoanConId '+component.get('v.LoanConId'));
            action.setParams({
                cdWrap : JSON.stringify(component.get("v.CDWrap"))
            });
            action.setCallback(this,function(a){
                var response = JSON.stringify(a.getReturnValue());
                console.log('response:::'+response);
                if(response != null){
                component.set("v.CDWrap",a.getReturnValue());
                console.log('component.get:::'+component.get("v.CDWrap"));
                component.set('v.disableAddButton',true);   
                console.log('disable button status'+ component.get('v.disableAddButton'));
                this.hideSpinner(component, event, helper);    
                }
            });
        	$A.enqueueAction(action);
    },
    delCDRecord: function(component, event, helper) {
        var index = event.target.id;
        console.log('index::'+ index);
        var wrapValue = [];
		wrapValue = component.get("v.CDWrap");
        console.log('wrapVal:::'+ JSON.stringify(wrapValue));
        wrapValue.splice(index,1);    
        component.set("v.CDWrap",wrapValue);  
        component.set('v.disableAddButton',false);
		this.hideSpinner(component,event,helper);
    },
     showSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
     
    hideSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-show");
        $A.util.addClass(spinner, "slds-hide");
    },
     getCustomerDetails : function (component, event, helper) {
        var customerId = component.get('v.laVal').customer;
        var customerName = component.get('v.laVal').customerName;
        console.log('CustomerId is '+customerId);
        console.log('CustomerName is '+customerName);
        var action = component.get('c.fetchCustomer');
            action.setParams({
                CustomerId : customerId,
                CustomerName : customerName
            });
            action.setCallback(this,function(a){
                component.set("v.customer",a.getReturnValue());    
            }); 
            $A.enqueueAction(action);
     },
     getProductDetails: function(component, event, helper) {
        var productId = component.get('v.laVal').product;
        var productName = component.get('v.laVal').productName;
        console.log('productId is '+productId);
        console.log('productName is '+productName);
        var action = component.get('c.fetchProduct');
            action.setParams({
                productId : productId,
                productName : productName
            });
            action.setCallback(this,function(a){
                component.set("v.prod",a.getReturnValue());    
            }); 
            $A.enqueueAction(action);    
     },
     getPolicyDetails: function(component, event, helper) {
        var policyId = component.get('v.laVal').localpolicyValue;
        var policyName = component.get('v.laVal').localpolicyName;
        console.log('policyId is '+policyId);
        console.log('policyName is '+policyName);
        var action = component.get('c.fetchLALocalPolicy');
            action.setParams({
                policyId : policyId,
                policyName : policyName
            });
            action.setCallback(this,function(a){
                component.set("v.localPol",a.getReturnValue()); 
            });
            $A.enqueueAction(action);    
     },
     filterSalesDivisonValues1 : function(component, event, helper) {
             component.set('v.showErrorMessage' , false);
             var index = component.get('v.LoanConId');
         console.log('From filterSalesDivisonValues1,Index is '+index);
         var wrap = [];
         
         wrap = JSON.parse(component.get('v.CDWrap'));
         console.log('From filterSalesDivisonValues1, Wrapper is : ' +JSON.stringify(component.get('v.CDWrap')));
         
        //var dynamicCmp = component.find("InputSelectDynamic1");
        //var consti = dynamicCmp.get("v.value");
        //console.log('consti '+consti);
        var consti = wrap[index].constitution;
        console.log('From filterSalesDivisonValues1,consti is '+consti);
        var action = component.get('c.fetchConstitutionList');
            action.setParams({
                "ValueOfArg" : consti,
                "type" : "CustomerSegment"
            });
            action.setCallback(this,function(a){
                var cons = a.getReturnValue();
                console.log('From filterSalesDivisonValues1,cons::::'+JSON.stringify(cons));
                var consOptionsList = [];
                for (var key in cons) {
                            consOptionsList.push({value: key, label: cons[key]});
                            if(cons[key] == wrap[index].CustSegmentName) {
                                wrap[index].CustSegment = key;
                            }
                    };
                    console.log('From filterSalesDivisonValues1, consOptionsList::::'+JSON.stringify(consOptionsList));
                    wrap[index].mapCustSegment1 = consOptionsList;
                    component.set("v.CDWrap",wrap);
                component.set("v.lstCustSegment",consOptionsList);
                this.hideSpinner(component, event, helper);
            });
        $A.enqueueAction(action); 
    },
    filterSalesDivisonValues : function(component, event, helper) {
         component.set('v.showErrorMessage' , false);
         var index = component.get('v.LoanConId');
         console.log('From filterSalesDivisonValues, Index is '+index);
         var wrap = [];
         wrap = component.get('v.CDWrap');
         console.log('From filterSalesDivisonValues,Wrapper is : ' +JSON.stringify(component.get('v.CDWrap')));
         
        //var dynamicCmp = component.find("InputSelectDynamic");
        //console.log('dynamicCmp '+dynamicCmp);
        //var borrower = dynamicCmp.get("v.value");
        var borrower = wrap[index].BorrowerType;
        console.log('From filterSalesDivisonValues,borrower '+borrower);
        //console.log('borrowerT '+borrowerT);
        var action = component.get('c.fetchConstitutionList');
            action.setParams({
                "ValueOfArg" : borrower,
                "type" : "Constitution"
            });
            action.setCallback(this,function(a){
                var cons = a.getReturnValue();
                console.log('From filterSalesDivisonValues,cons::::'+JSON.stringify(cons));
                
                var consOptionsList = [];
                for (var key in cons) {
                            if(cons[key] == wrap[index].constitutionName) {
                                wrap[index].constitution = key;
                            }
                            consOptionsList.push({value: key, label: cons[key]});
                    };
                    console.log('From filterSalesDivisonValues,consOptionsList::::'+JSON.stringify(consOptionsList));
                    wrap[index].mapConstitution1 = consOptionsList;
                    console.log('From filterSalesDivisonValues,wrap index val '+ JSON.stringify(wrap[index]));
                    component.set("v.CDWrap",JSON.stringify(wrap));
                    console.log('From filterSalesDivisonValues,wrap '+ JSON.stringify(component.get('v.CDWrap')));
                    
                component.set("v.lstconstitution",consOptionsList);
                this.filterSalesDivisonValues1(component, event, helper);
            });
        $A.enqueueAction(action); 
    } ,
    filterSalesDivisonValues3 : function(component, event, helper) {
             component.set('v.showErrorMessage' , false);
             var index = component.get('v.LoanConId');
         console.log('From filterSalesDivisonValues1,Index is '+index);
         var wrap = [];
         
         wrap = component.get('v.CDWrap');
         console.log('From filterSalesDivisonValues1, Wrapper is : ' +JSON.stringify(component.get('v.CDWrap')));
         
        //var dynamicCmp = component.find("InputSelectDynamic1");
        //var consti = dynamicCmp.get("v.value");
        //console.log('consti '+consti);
        var consti = wrap[index].constitution;
        console.log('From filterSalesDivisonValues1,consti is '+consti);
        var action = component.get('c.fetchConstitutionList');
            action.setParams({
                "ValueOfArg" : consti,
                "type" : "CustomerSegment"
            });
            action.setCallback(this,function(a){
                var cons = a.getReturnValue();
                console.log('From filterSalesDivisonValues1,cons::::'+JSON.stringify(cons));
                var consOptionsList = [];
                for (var key in cons) {
                            consOptionsList.push({value: key, label: cons[key]});
                            if(cons[key] == wrap[index].CustSegmentName) {
                                wrap[index].CustSegment = key;
                            }
                    };
                    console.log('From filterSalesDivisonValues1, consOptionsList::::'+JSON.stringify(consOptionsList));
                    wrap[index].mapCustSegment1 = consOptionsList;
                    component.set("v.CDWrap",wrap);
                component.set("v.lstCustSegment",consOptionsList);
                this.hideSpinner(component, event, helper);
            });
        $A.enqueueAction(action); 
    },
    filterSalesDivisonValues2: function(component, event, helper) {
         component.set('v.showErrorMessage' , false);
         var index = component.get('v.LoanConId');
         console.log('From filterSalesDivisonValues, Index is '+index);
         var wrap = [];
         wrap = component.get('v.CDWrap');
         console.log('From filterSalesDivisonValues,Wrapper is : ' +JSON.stringify(component.get('v.CDWrap')));
         
        //var dynamicCmp = component.find("InputSelectDynamic");
        //console.log('dynamicCmp '+dynamicCmp);
        //var borrower = dynamicCmp.get("v.value");
        var borrower = wrap[index].BorrowerType;
        console.log('From filterSalesDivisonValues,borrower '+borrower);
        //console.log('borrowerT '+borrowerT);
        var action = component.get('c.fetchConstitutionList');
            action.setParams({
                "ValueOfArg" : borrower,
                "type" : "Constitution"
            });
            action.setCallback(this,function(a){
                var cons = a.getReturnValue();
                console.log('From filterSalesDivisonValues,cons::::'+JSON.stringify(cons));
                
                var consOptionsList = [];
                for (var key in cons) {
                            if(cons[key] == wrap[index].constitutionName) {
                                wrap[index].constitution = key;
                            }
                            consOptionsList.push({value: key, label: cons[key]});
                    };
                    console.log('From filterSalesDivisonValues,consOptionsList::::'+JSON.stringify(consOptionsList));
                    wrap[index].mapConstitution1 = consOptionsList;
                    console.log('From filterSalesDivisonValues,wrap index val '+ JSON.stringify(wrap[index]));
                    component.set("v.CDWrap",wrap);
                    console.log('From filterSalesDivisonValues,wrap '+ JSON.stringify(component.get('v.CDWrap')));
                    
                component.set("v.lstconstitution",consOptionsList);
                this.filterSalesDivisonValues1(component, event, helper);
            });
        $A.enqueueAction(action); 
    } ,
     filterSalesDivisonValues4 : function(component, event, helper) {
         component.set('v.showErrorMessage' , false);
         var index = component.get('v.LoanConId');
         console.log('From filterSalesDivisonValues, Index is '+index);
         var wrap = [];
         wrap = component.get('v.CDWrap');
         console.log('From filterSalesDivisonValues,Wrapper is : ' +JSON.stringify(component.get('v.CDWrap')));
         
        //var dynamicCmp = component.find("InputSelectDynamic");
        //console.log('dynamicCmp '+dynamicCmp);
        //var borrower = dynamicCmp.get("v.value");
        var borrower = wrap[index].BorrowerType;
        
        console.log('From filterSalesDivisonValues,borrower '+borrower);
        //console.log('borrowerT '+borrowerT);
        var action = component.get('c.fetchConstitutionList');
            action.setParams({
                "ValueOfArg" : borrower,
                "type" : "Constitution"
            });
            action.setCallback(this,function(a){
                var cons = a.getReturnValue();
                console.log('From filterSalesDivisonValues,cons::::'+JSON.stringify(cons));
                
                var consOptionsList = [];
                for (var key in cons) {
                            consOptionsList.push({value: key, label: cons[key]});
                    };
                    console.log('From filterSalesDivisonValues,consOptionsList::::'+JSON.stringify(consOptionsList));
                    wrap[index].mapConstitution1 = consOptionsList;
                    if(borrower == '1') { 
                        wrap[index].constitution = '20';
                    }
                    console.log('From filterSalesDivisonValues,wrap index val '+ JSON.stringify(wrap[index]));
                    component.set("v.CDWrap",wrap);
                    console.log('From filterSalesDivisonValues4,wrap '+ JSON.stringify(component.get('v.CDWrap')));
                    
                component.set("v.lstconstitution",consOptionsList);
                if(borrower == '1' || borrower == '') { 
                    this.filterSalesDivisonValues3(component, event, helper);
                }
            });
        $A.enqueueAction(action); 
    },
     pensionFlagChange : function(component, event, helper) {
        component.set('v.showErrorMessage' , false);
         var index = component.get('v.LoanConId');
         console.log('Index is '+index);
         var wrap = [];
         wrap = component.get('v.CDWrap');
         console.log('Wrapper is : ' +JSON.stringify(component.get('v.CDWrap')));
         console.log('WRAP '+JSON.stringify(wrap[index]));
         var pensionFlag = wrap[index].PensionIncomeFlag;
         console.log('Pension flag '+pensionFlag);
         if(pensionFlag) {
              wrap[index].IncomeConsideredFlag = true;
              //wrap[index].idType = 'PAN'; 
              component.set('v.CDWrap',wrap);
              this.incomeFlagChange(component, event, helper);
         }
    },
     incomeFlagChange : function(component, event, helper) {
         console.log('Income flag change method');
        component.set('v.showErrorMessage' , false);
         var index = component.get('v.LoanConId');
         console.log('Index is '+index);
         var wrap = [];
         wrap = component.get('v.CDWrap');
         console.log('Wrapper is : ' +JSON.stringify(component.get('v.CDWrap')));
         console.log('WRAP '+JSON.stringify(wrap[index]));
         var incomeFlag = wrap[index].IncomeConsideredFlag;
         console.log('incomeFlag '+incomeFlag);
         if(incomeFlag) {
              wrap[index].idType = 'PAN'; 
              component.set('v.CDWrap',wrap);
              this.idTypeChange(component, event, helper);
         } //else {
            //wrap[index].idType = '';
              //component.set('v.CDWrap',wrap); 
         //}
    },
    idTypeChange : function (component, event,helper) {
       component.set('v.showErrorMessage' , false);
         var index = component.get('v.LoanConId');
         console.log('Index is '+index);
         var wrap = [];
         wrap = component.get('v.CDWrap');
         console.log('Wrapper is : ' +JSON.stringify(component.get('v.CDWrap')));
         var idType = wrap[index].idType;
         if(idType == 'PAN') {
              console.log('SELECTED IS PAN '+ idType);
             var customerID = wrap[index].customer;
             console.log('Customer Id Sent by Vaishali in high state'+customerID);
             console.log('SELECTED IS PAN '+ idType);
             var actionV = component.get('c.populatePAN');
             
            actionV.setParams({
                "customerID" :customerID
            });
            actionV.setCallback(this,function(a){
                var PAN  = a.getReturnValue();
                console.log('PAN '+ PAN);
                if(PAN != null) {
                    wrap[index].PAN = PAN;
                    component.set('v.CDWrap', wrap);
                    console.log(component.get('v.PAN'));
                }
            });
            $A.enqueueAction(actionV);
         } else {
             wrap[index].PAN = '';
            component.set('v.CDWrap', wrap);
         }
    }
})