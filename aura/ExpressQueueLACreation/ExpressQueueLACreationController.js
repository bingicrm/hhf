({
    doInit : function(component, event, helper) {
        var navigateBack = component.get('v.navigateBack');
        console.log('navigateBack '+navigateBack);
        console.log('QDEMode',component.get('v.QDEMode'));
        console.log('laVal',  component.get('v.laVal'));
        console.log('calledFromVFPage CHILD'+component.get('v.calledFromVFPage'));
        var laVAlue = component.get('v.laVal'); 
        var QDEMode = component.get('v.QDEMode');
         component.set('v.showErrorMessage' , false)
        if(QDEMode == true && navigateBack == false) {
            helper.showSpinner(component);
            console.log('QDE '+QDEMode);
            var recordId = component.get('v.recordId');
            console.log('recordId '+recordId);
            var action11 = component.get('c.getLA');
            action11.setParams({
                "recordId" :recordId
            });
            action11.setCallback(this,function(a){
                var response = a.getReturnValue();
                console.log('response:::'+ JSON.stringify(response));
                console.log('a.getState:::'+ a.getState());
                if(a.getState() == 'SUCCESS'){
                    if(response.laWrapped.errorMsg != '') {
                        component.set('v.errorLA', true);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "",
                            "type": "Error",
                            "message": response.laWrapped.errorMsg
                        });
                        toastEvent.fire();
                    }    
                    //if(response.isInserted){
                    component.set("v.loanAppID",response.laID);   
                    component.set("v.isInserted",response.isInserted);
                    component.set("v.isEditableLA", false);  
                    component.set("v.LaNum",response.laNum);
                    component.set("v.laVal",response.laWrapped);
                    console.log(component.get('v.LaNum'));
                    
                     var res= response.laWrapped;
                    var x = res.mapCustSegment;
                    console.log('x::::'+JSON.stringify(x));
                    var optionsList = [];    
                    for (var key in x) {
                            optionsList.push({value: key, label: x[key]});
                    };
                    console.log('optionsList::::'+optionsList);
                component.set("v.lstCustSegment",optionsList);
                var br = res.mapBorrowerType;
                var borrowerOptionsList = [];
                for (var key in br) {
                            borrowerOptionsList.push({value: key, label: br[key]});
                    };
                console.log('borrowerOptionsList::::'+JSON.stringify(borrowerOptionsList));
                component.set("v.lstBorrowerType",borrowerOptionsList);
                var cons = res.mapConstitution;
                console.log('cons::::'+JSON.stringify(cons));
                var consOptionsList = [];
                for (var key in cons) {
                            consOptionsList.push({value: key, label: cons[key]});
                    };
                console.log('consOptionsList::::'+JSON.stringify(consOptionsList));
                component.set("v.lstconstitution",consOptionsList);
                var ids = res.idTypes;
                console.log('ids::::'+JSON.stringify(ids));
                var idsOptionsList = [];
                for (var key in ids) {
                            idsOptionsList.push({value: key, label: ids[key]});
                    };
                console.log('idsOptionsList::::'+JSON.stringify(idsOptionsList));
                component.set("v.idTypes",idsOptionsList);
                    /*setTimeout(function() {   
                        
                        helper.getCD(component, event, helper);
                    },5000);*/
                    helper.getCustomerDetails(component, event, helper);
                    helper.getProductDetails(component, event, helper);
                    helper.getPolicyDetails(component, event, helper);
                    console.log('component.get(v.customer)' + component.get('v.customer'));
                    console.log('component.get(v.prod)' + component.get('v.prod'));
                    console.log('component.get(v.localPol)' + component.get('v.localPol'));
                    helper.hideSpinner(component, event, helper);  
                }
                else{
                    helper.hideSpinner(component, event, helper);  
                    var calledFromVFPage = component.get('v.calledFromVFPage');
                    if(calledFromVFPage == false) {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "",
                            "type": "Error",
                            "message": "Error: Some error occured. Please contact System Admin."
                        });
                        toastEvent.fire();
                    } else {
                        component.set('v.showErrorMessage' , true);
                        component.set('v.errorMessage','Error: Some error occured. Please contact System Admin.');
                    }
                }

            });
            $A.enqueueAction(action11); 
            
            
        } 
        if(navigateBack == true) {
        var paramSet = component.get('c.parseWrapper');
        paramSet.setParams({
                serializedString : component.get('v.laVal')
            });
            paramSet.setCallback(this,function(response){
                component.set("v.laVal",response.getReturnValue());
            });
            $A.enqueueAction(paramSet); 
        }
        
        

        if(navigateBack == false && QDEMode == false) {
            console.log('Hi');
		var action = component.get('c.initial');
            
            action.setCallback(this,function(a){
                //this.handleResponse(a,component,helper);
                component.set("v.laVal",a.getReturnValue());
                console.log('a.getReturnValue()::'+JSON.stringify(a.getReturnValue()));
                    var res=a.getReturnValue();
                    var x = res.mapCustSegment;
                    console.log('x::::'+JSON.stringify(x));
                    var optionsList = [];    
                    for (var key in x) {
                            optionsList.push({value: key, label: x[key]});
                    };
                    console.log('optionsList::::'+optionsList);
                component.set("v.lstCustSegment",optionsList);
                var br = res.mapBorrowerType;
                var borrowerOptionsList = [];
                for (var key in br) {
                            borrowerOptionsList.push({value: key, label: br[key]});
                    };
                console.log('borrowerOptionsList::::'+JSON.stringify(borrowerOptionsList));
                component.set("v.lstBorrowerType",borrowerOptionsList);
                var cons = res.mapConstitution;
                console.log('cons::::'+JSON.stringify(cons));
                var consOptionsList = [];
                for (var key in cons) {
                            consOptionsList.push({value: key, label: cons[key]});
                    };
                    console.log('consOptionsList::::'+JSON.stringify(consOptionsList));
                component.set("v.lstconstitution",consOptionsList);
                var ids = res.idTypes;
                console.log('ids::::'+JSON.stringify(ids));
                var idsOptionsList = [];
                for (var key in ids) {
                            idsOptionsList.push({value: key, label: ids[key]});
                    };
                console.log('idsOptionsList::::'+JSON.stringify(idsOptionsList));
                component.set("v.idTypes",idsOptionsList);
            });
            $A.enqueueAction(action); 
        } 
        var action21 = component.get("c.getlookupWrapperObj");  
            action21.setParams({
            });
            action21.setCallback(this, function(result11){
                var state11 = result11.getState();
                if (component.isValid() && state11 === "SUCCESS"){
                    component.set('v.lookupWrapperObj',result11.getReturnValue());
                    console.log('v.lookupWrapperObj' +component.get('v.lookupWrapperObj'));
                }
            });
            $A.enqueueAction(action21);
	},
	clickCreate : function(component, event, helper) {
        
        component.set('v.showErrorMessage' , false);
        console.log('LAWrapper'+ JSON.stringify(component.get("v.laVal")));
        console.log('component.get("v.customer")'+ JSON.stringify(component.get("v.customer")));
        console.log('component.get("v.prod")'+ JSON.stringify(component.get("v.prod")));
        console.log('component.get("v.localPol")'+ JSON.stringify(component.get("v.localPol")));
		if(component.get("v.prod") != null ){ /***** Added by Saumya For Null handling *****/
        var prodID = component.get("v.prod").val;
        var prodName = component.get("v.prod").text;
        component.set("v.laVal.product",prodID);
        component.set("v.laVal.productName",prodName);
    	}
        if(component.get("v.customer") != null ){ /***** Added by Saumya For Null handling *****/
        var customerID = component.get("v.customer").val;
        var customerName = component.get("v.customer").text;
        component.set("v.laVal.customer",customerID);
        component.set("v.laVal.customerName",customerName);
        }
        if(component.get("v.localPol") != null && component.get("v.localPol") != undefined){ /***** Added by Saumya For Null handling *****/
        var policyID = component.get("v.localPol").val;
        var policyName = component.get("v.localPol").text;
        component.set("v.laVal.localpolicyValue",policyID);
		component.set("v.laVal.localpolicyName",policyName);
        }
        var la =  component.get("v.laVal");
        console.log('la:::'+la);
        var amount = component.get("v.laVal.approvedLoanAmt");
        var loannumber = component.get("v.laVal.loanApplicationNumber");
        var tenure = component.get("v.laVal.requestedTenure");
        var customer = component.get("v.customer");
        var product = component.get("v.prod");
        var policy = component.get("v.localPol");
        var laId = component.get('v.loanAppID');
        
        console.log('laWrapValues.approvedLoanAmt:::'+component.get("v.laVal.approvedLoanAmt"));
        console.log('laWrapValues.loanApplicationNumber:::'+component.get("v.laVal.loanApplicationNumber"));
        console.log('laWrapValues.requestedTenure:::'+component.get("v.laVal.requestedTenure"));
        console.log('component.get("v.customer")'+ component.get("v.customer"));
        console.log('component.get("v.prod")'+component.get("v.prod"));
        console.log('component.get("v.localPol") '+JSON.stringify(component.get("v.localPol")));
        console.log('v.loanAppID'+ component.get('v.loanAppID'));
        if(amount == '' || loannumber == '' || customer == undefined || product == undefined || policy==undefined || amount == undefined || loannumber == undefined || customer == '' || product == '' || policy=='' ||tenure == undefined ) {
            
            var errorFields = 'Please enter ';
            if(customer == undefined || customer == '') {
                errorFields = errorFields+'Customer, ';
            } if(product == undefined || product == '') {
                errorFields = errorFields +'Product, ';
            } if(policy==undefined || policy=='') {
                errorFields = errorFields + 'Local Policy, ';
            } if(amount == '' || amount == undefined) {
                errorFields = errorFields+ 'Requested Loan Amount, ';
            } if(loannumber == '' || loannumber == undefined) {
                errorFields = errorFields+ 'Loan application number, ';
            } if(tenure == undefined) {
                errorFields = errorFields+ 'Requested tenure, ';
            }
            
            if(errorFields.endsWith(', ')) { 
                console.log('Last char is comma'); 
                errorFields = errorFields.substring(0, errorFields.length - 2);
            }
            if(errorFields != 'Please enter ') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": errorFields
                });
                toastEvent.fire();
            helper.hideSpinner(component, event, helper);
            }    
        }else {
            if(!$A.util.isEmpty(la) && !$A.util.isUndefined(la)){
            helper.showSpinner(component);
    		var action = component.get('c.saveLA');
                //action.setStorable();
                console.log('LAWrapper'+ JSON.stringify(component.get("v.laVal")));
                var wrapval = JSON.stringify(component.get("v.laVal"));
                
                action.setParams({
                    serializedWrapper : JSON.stringify(component.get("v.laVal"))
                });
                action.setCallback(this,function(a){
                    var response = a.getReturnValue();
                    console.log('response:::'+ JSON.stringify(response));
                    console.log('a.getState:::'+ a.getState());
                    if(a.getState() == 'SUCCESS'){
                        
                        helper.hideSpinner(component, event, helper); 
                        console.log('response wrapper '+JSON.stringify(response.laWrapped));
                        console.log("v.loanAppID"+response.laID);  
                        if(response.laWrapped == null) {
                            
                            var calledFromVFPage = component.get('v.calledFromVFPage');
                            if(calledFromVFPage == false) {
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "",
                                    "type": "Error",
                                    "message": 'Some error occurred, Please contact system admin'
                            });
                            toastEvent.fire();
                            } else {
                                 component.set('v.showErrorMessage' , true);
                                component.set('v.errorMessage','Some error occurred, Please contact system admin');
                                }  
                        }
                        else if(response.laWrapped.errorMsg != null && response.laWrapped.errorMsg != '') {
                            console.log('response wrapper error '+response.laWrapped.errorMsg);
                           var calledFromVFPage = component.get('v.calledFromVFPage');
                            if(calledFromVFPage == false) {
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "",
                                    "type": "Error",
                                    "message": response.laWrapped.errorMsg
                            });
                            toastEvent.fire();
                            } else {
                                 component.set('v.showErrorMessage' , true);
                                component.set('v.errorMessage',response.laWrapped.errorMsg);
                                }  
                        }
                        //if(response.isInserted){
                        component.set("v.loanAppID",response.laID);  
                        console.log('lA id '+component.get('v.loanAppID'));
                        component.set("v.isInserted",response.isInserted);
                        console.log('v.isInserted'+response.isInserted);
                        component.set("v.isEditableLA", false);  
                        component.set("v.LaNum",response.laNum);
                        component.set("v.laVal",response.laWrapped);
                        console.log(component.get('v.LaNum'));
                        console.log('v.isEditableLA'+component.get('v.isEditableLA'));
                        /*setTimeout(function() {   
                            
                            helper.getCD(component, event, helper);
                        },5000);*/
                    }
                    else{
                    helper.hideSpinner(component, event, helper);    
                        component.set("v.isInserted",false);
                        component.set("v.isEditableLA", false);
                        if(response.laWrapped == null) {
                            var calledFromVFPage = component.get('v.calledFromVFPage');
                            if(calledFromVFPage == false) {
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "",
                                    "type": "Error",
                                    "message": 'Some error occurred, Please contact system admin'
                            });
                            toastEvent.fire();
                            } else {
                                 component.set('v.showErrorMessage' , true);
                                component.set('v.errorMessage','Some error occurred, Please contact system admin');
                                }  
                        }
                        else if(response.laWrapped.errorMessage != null && response.laWrapped.errorMessage != '') {
                           var calledFromVFPage = component.get('v.calledFromVFPage');
                            if(calledFromVFPage == false) {
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "",
                                    "type": "Error",
                                    "message": response.laWrapped.errorMsg
                            });
                            toastEvent.fire();
                            } else {
                                 component.set('v.showErrorMessage' , true);
                                component.set('v.errorMessage',response.laWrapped.errorMsg);
                                }  
                        }
                        console.log('v.isEditableLA'+component.get('v.isEditableLA'));
                    }
    
                });
                $A.enqueueAction(action); 
            }
        }
	},
    getCD : function(component, event, helper) {
        component.set('v.showErrorMessage' , false);
        helper.showSpinner(component);
        helper.getCD(component, event, helper);
    },
    handleEdit : function(component, event, helper) {
        component.set('v.showErrorMessage' , false);
       
        var index = component.get('v.LoanConId');
		//var index = event.target.id;
		if(index == null || index == undefined) {
		    var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Please select customer'
            });
            toastEvent.fire();    
		} else {
		helper.showSpinner(component);
        console.log('index::'+ index);
        var wrap = [];
        wrap = component.get("v.CDWrap");
        
        
        
        wrap[index].isEditable = true;
        console.log('CDWrap::'+JSON.stringify(wrap));
        console.log('wrap[index]::'+JSON.stringify(wrap[index])+ 'wrap[index]. '+ wrap[index].isEditable);
		component.set("v.CDWrap", wrap); 
		
		component.set('v.disableEditButton',true);
		
        var oldWrap = JSON.stringify(wrap[index]);
        component.set('v.oldWrapString',oldWrap);
        console.log('OLDWRAP '+component.get('v.oldWrapString'));
		
		
		helper.filterSalesDivisonValues(component, event, helper);
		
        //helper.filterSalesDivisonValues1(component, event, helper);
		//component.set("v.isEditable", true);   
		}
    },
    handleLAEdit : function(component, event, helper) {
         component.set('v.showErrorMessage' , false);
        console.log('LAWrapper'+ JSON.stringify(component.get("v.laVal")));
        helper.showSpinner(component);
        var policyValue = JSON.stringify(component.get("v.localPol"));
        console.log('policyValue '+policyValue);
        console.log('localpolicyvalue '+component.get("v.laVal.localpolicyValue"));
        
        if(component.get("v.laVal.localpolicyValue") == undefined) {
            console.log('NULL VALUE FOR POL VAL');
            component.set("v.localPol", undefined);
        }
        
        component.set("v.isEditableLA", true);  
        console.log('isEditableLA '+component.get('v.isEditableLA'));
        helper.hideSpinner(component);
        
    },    
     handleSave : function(component, event, helper) {
         
          component.set('v.showErrorMessage' , false);
         var index = event.target.id;
        console.log('index::'+ index);

        var wrap = [];  
        wrap = component.get("v.CDWrap"); 
        console.log('customer:::::'+wrap[index].customer); 
        console.log('WRap value '+JSON.stringify(wrap[index]));
          var eligibleForSaveflag = true;
         if(wrap[index].customer == null) {
           eligibleForSaveflag = false;
             var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Please enter customer'
            });
            toastEvent.fire();   
         } 
         else {
              
             if(wrap[index].customer != null && wrap[index].isPrimary){
                 var customerID = wrap[index].customer;
                 console.log('customerID:::::'+customerID); 
             }
             
             else if(wrap[index].customer.val != null && wrap[index].customer.val != 'undefined'){
                 console.log('wrap:::::'+wrap[index].customer.val); 
                 var customerID = wrap[index].customer.val;
                 console.log('customerID:::::'+customerID);     
             }
                 else{
                     var customerID = wrap[index].customer;
                     console.log('customerID:::::'+customerID);   
                 }
                 var wrapVal = [];
                 wrapVal = wrap;
                 wrapVal[index].customer = customerID;
         
         
         if(wrap[index].ApplicantType =='' || wrap[index].BorrowerType =='' || wrap[index].constitution == '' || wrap[index].CustSegment == '') {
            eligibleForSaveflag = false;
             var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Please enter Applicant Type/Borrower Type/Constitution/Customer Segment for this customer'
            });
            toastEvent.fire();        
         }
          else if(wrapVal[index].idType == '' && wrapVal[index].mobileNum == '') {
              eligibleForSaveflag = false;
             var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Please enter Mobile or Id Details'
            });
            toastEvent.fire(); 
        }
        else if(wrapVal[index].idType != '' && (wrapVal[index].PAN == '' || wrapVal[index].PAN == undefined) ) {
            eligibleForSaveflag = false;
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Please enter Id number corresponding to selected ID type'
            });
            toastEvent.fire();  
        } 
        else if(wrapVal[index].idType == '' && wrapVal[index].PAN != '') {
            eligibleForSaveflag = false;
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Please enter Id type'
            });
            toastEvent.fire();  
        }
        
       if(eligibleForSaveflag ) {
           console.log('ADD BUTTON' +component.get('v.disableAddButton'));
           console.log('Edit BUTTON' +component.get('v.disableEditButton'));
       // component.set("v.wrap[index].customer",customerID);
         console.log('wrap[index]:::::'+JSON.stringify(wrapVal[index]));
         var action = component.get('c.saveCD');
            action.setParams({
                serializedWrapper : JSON.stringify(wrapVal[index]),
                loanAppId : component.get("v.loanAppID")
            });
            action.setCallback(this,function(a){
                var response = a.getReturnValue();
                console.log('response Value ky hai ::'+JSON.stringify(response));
                
                
                var wrapValues = component.get("v.CDWrap");
                //wrapValues[index] = response;
                wrapValues[index].isEditable = false;
                component.set("v.CDWrap",wrapValues);
                
                if(response.errorMsg != null && response.errorMsg != ''){
                    console.log('Error message'); 
                    wrapValues[index].isEditable = true;
                    component.set("v.CDWrap",wrapValues);
                    console.log('WRap ' +JSON.stringify(component.get("v.CDWrap")));
                    
                    var calledFromVFPage = component.get('v.calledFromVFPage');
                    if(calledFromVFPage == false) {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "",
                            "type": "Error",
                            "message": response.errorMsg
                        });
                        toastEvent.fire();
                    } else {
                         component.set('v.showErrorMessage' , true);
                        component.set('v.errorMessage',response.errorMsg);
                        } 
                        
                    }
                else{
                        console.log('In else statement');
                        console.log('wrap[index]:::::'+JSON.stringify(wrapVal[index]));
                       helper.showSpinner(component, event, helper); 
                        if(wrap[index].CDId == null || wrap[index].CDId == '') {
                            console.log('CD ID' +wrap[index].CDId);
                            component.set('v.disableAddButton',false);
                        } else {
                            component.set('v.disableEditButton',false);
                        }
                    }
                    var wrapValue = [];
                    wrapValue = component.get("v.CDWrap");
                    console.log('wrapVal:::'+ JSON.stringify(wrapValue));
                    wrapValue[index] = a.getReturnValue(); 
                    if(response.errorMsg != null && response.errorMsg != ''){
                        var pinObj = component.get('v.lookupWrapperObj');
                        console.log('pinObj '+pinObj);
                        pinObj.val = wrapValue[index].customer;
                        pinObj.text = wrapValue[index].customerName;
                        pinObj.objName = 'Account';
                        wrapValue[index].customer = pinObj;
                    }
                    console.log('indexed val '+JSON.stringify(wrapValue[index]));
                    var lstBorr = component.get("v.lstBorrowerType");
                    for (var value in lstBorr) {
                        console.log('::value::'+value);
                        if(lstBorr[value].value == wrapVal[index].BorrowerType){
                            wrapValue[index].BorrowerTypeName = lstBorr[value].label;
                        }
                    };
                    var lstCustSeg = component.get("v.lstCustSegment");
                    for (var value in lstCustSeg) {
                        console.log('::value::'+value);
                        if(lstCustSeg[value].value  == wrapVal[index].CustSegment){
                            wrapValue[index].CustSegmentName = lstCustSeg[value].label;
                        }  
                    };
                    var lstCosnti = component.get("v.lstconstitution");
                    console.log('::lstCosnti::'+JSON.stringify(lstCosnti));
                    for (var value in lstCosnti) {
                        console.log('::Consvalue::'+value + ':;wrapVal[index].constitution::'+wrapVal[index].constitution);
                        if(lstCosnti[value].value == wrapVal[index].constitution){
                            wrapValue[index].constitutionName = lstCosnti[value].label;
                        }
                    };
                    component.set("v.CDWrap",wrapValue);
                    console.log('response in Save:::'+ JSON.stringify(component.get('v.CDWrap')));
                    helper.hideSpinner(component, event, helper);
            });
            $A.enqueueAction(action); 
        } 
         }    
     },
    addRow: function(component, event, helper) {
         component.set('v.showErrorMessage' , false);
        helper.showSpinner(component);
        helper.addCDRecord(component, event);
    },
    deleteRow: function(component, event, helper) {
         component.set('v.showErrorMessage' , false);
         component.set('v.LoanConId',undefined);
        helper.showSpinner(component);
        helper.delCDRecord(component, event);
    },
    addAddress: function(component, event, helper) {
         component.set('v.showErrorMessage' , false);
         var index = component.get('v.LoanConId');
         if(index == null || index == undefined) {
		    var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Please select customer'
            });
            toastEvent.fire();    
		} else {
        //var index = event.target.id;
        console.log('ADD address method');
        console.log('index::'+ index);
        var wrap = [];
        wrap = component.get("v.CDWrap"); 
        var customerDetailId = wrap[index].CDId;
        var customerDetailNumber = wrap[index].customerName;
        console.log('customerDetailId' +customerDetailId);
        console.log('laVAL '+ component.get('v.laVal'));
        console.log('loanAppID '+component.get('v.loanAppID'));
        console.log('ID TYPES '+component.get('v.idTypes'));
        component.set("v.addAddress",'true');
        var evt = $A.get("e.c:callAddressDetailEvent");
        evt.setParams(
        {
        "customerDetailNumber" : customerDetailNumber,    
        "customerDetail": customerDetailId,
        "laVal" : JSON.stringify(component.get('v.laVal')),
        "CDWrap" : wrap,
        "lstBorrowerType" : component.get('v.lstBorrowerType'),
        "lstconstitution" : component.get('v.lstconstitution'),
        "lstCustSegment" : component.get('v.lstCustSegment'),
        "loanAppID" : component.get('v.loanAppID'),
        "prod" : component.get('v.prod'),
        "localPol" : component.get('v.localPol'),
        "customer" : component.get('v.customer'),
        "isDisplay" : component.get('v.isDisplay') ,
        "isEditable" : component.get('v.isEditable'),
        "isInserted" : component.get('v.isInserted'),
        "isDisable" : component.get('v.isDisable'),
        "addAddress" : component.get('v.addAddress'),
        "CDfetched" : component.get('v.CDfetched'),
        "data" : component.get('v.data'),
        "columns" : component.get('v.columns'),
        "borrowerTy" : component.get('v.borrowerTy'),
        "LaNum" : component.get('v.LaNum'),
        "isEditableLA" : component.get('v.isEditableLA'),
        "QDEMode" : component.get('v.QDEMode'),
        "calledFromVFPage" : component.get('v.calledFromVFPage'),
        "errorMessage" : component.get('v.errorMessage'),
        "showErrorMessage" :component.get('v.showErrorMessage'),
        "LoanConId" : component.get('v.LoanConId'),
        "idTypes" : component.get('v.idTypes'),
        "oldWrap" : component.get('v.oldWrap'),
        "oldWrapString" : component.get('v.oldWrapString'),
        "disableAddButton" : component.get('v.disableAddButton'),
        "disableEditButton" : component.get('v.disableEditButton')
        } 
    );
        evt.fire();
        /*helper.showSpinner(component);
        helper.delCDRecord(component, event);*/
		}
    },
    addPAN :function(component, event, helper) {
         component.set('v.showErrorMessage' , false);
          var index = component.get('v.LoanConId');
          if(index == null || index == undefined) {
		    var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Please select customer'
            });
            toastEvent.fire();    
		} else {
        //var index = event.target.id;
        console.log('ADD address method');
        console.log('index::'+ index);
        var fromLAComponent = true;
        console.log('Setting fromLAComponent'+fromLAComponent);
        var wrap = [];
        wrap = component.get("v.CDWrap"); 
        var customerDetailId = wrap[index].CDId;
        console.log('customerDetailId' +customerDetailId);
        var evt = $A.get("e.c:callPANValidation");
        evt.setParams(
            { 
                "recordId": customerDetailId,
                "fromLAComponent" : fromLAComponent,
                "index" : index, 
                "customerDetail": customerDetailId,
                "laVal" : JSON.stringify(component.get('v.laVal')),
                "CDWrap" : wrap,
                "lstBorrowerType" : component.get('v.lstBorrowerType'),
                "lstconstitution" : component.get('v.lstconstitution'),
                "lstCustSegment" : component.get('v.lstCustSegment'),
                "loanAppID" : component.get('v.loanAppID'),
                "prod" : component.get('v.prod'),
                "localPol" : component.get('v.localPol'),
                "customer" : component.get('v.customer'),
                "isDisplay" : component.get('v.isDisplay') ,
                "isEditable" : component.get('v.isEditable'),
                "isInserted" : component.get('v.isInserted'),
                "isDisable" : component.get('v.isDisable'),
                "addAddress" : component.get('v.addAddress'),
                "CDfetched" : component.get('v.CDfetched'),
                "data" : component.get('v.data'),
                "columns" : component.get('v.columns'),
                "borrowerTy" : component.get('v.borrowerTy'),
                "LaNum" : component.get('v.LaNum'),
                "isEditableLA" : component.get('v.isEditableLA'),
                 "QDEMode" : component.get('v.QDEMode'),
                 "calledFromVFPage" : component.get('v.calledFromVFPage'),
                 "errorMessage" : component.get('v.errorMessage'),
                "showErrorMessage" :component.get('v.showErrorMessage'),
                "LoanConId" : component.get('v.LoanConId'),
                "idTypes" : component.get('v.idTypes'),
                "oldWrap" : component.get('v.oldWrap'),
                "oldWrapString" : component.get('v.oldWrapString'),
                "disableAddButton" : component.get('v.disableAddButton'),
                "disableEditButton" : component.get('v.disableEditButton')
            }
        );
        evt.fire();
		}    
    },
    addCIBIL : function(component, event, helper) {
         component.set('v.showErrorMessage' , false);
          var index = component.get('v.LoanConId');
          if(index == null || index == undefined) {
		    var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Please select customer'
            });
            toastEvent.fire();    
		} else {
         //var index = event.target.id;
        console.log('ADD CBIL method');
        console.log('index::'+ index);
        var fromLAComponent = true;
        console.log('Setting fromLAComponent'+fromLAComponent);
        var wrap = [];
        wrap = component.get("v.CDWrap"); 
        var customerDetailId = wrap[index].CDId;
        console.log('customerDetailId' +customerDetailId);
        var evt = $A.get("e.c:callCIBIL");
        evt.setParams(
            { 
                "recordId": customerDetailId,
                "fromLAComponent" : fromLAComponent,
                "index" : index, 
                "customerDetail": customerDetailId,
                "laVal" : JSON.stringify(component.get('v.laVal')),
                "CDWrap" : wrap,
                "lstBorrowerType" : component.get('v.lstBorrowerType'),
                "lstconstitution" : component.get('v.lstconstitution'),
                "lstCustSegment" : component.get('v.lstCustSegment'),
                "loanAppID" : component.get('v.loanAppID'),
                "prod" : component.get('v.prod'),
                "localPol" : component.get('v.localPol'),
                "customer" : component.get('v.customer'),
                "isDisplay" : component.get('v.isDisplay') ,
                "isEditable" : component.get('v.isEditable'),
                "isInserted" : component.get('v.isInserted'),
                "isDisable" : component.get('v.isDisable'),
                "addAddress" : component.get('v.addAddress'),
                "CDfetched" : component.get('v.CDfetched'),
                "data" : component.get('v.data'),
                "columns" : component.get('v.columns'),
                "borrowerTy" : component.get('v.borrowerTy'),
                "LaNum" : component.get('v.LaNum'),
                "isEditableLA" : component.get('v.isEditableLA'),
                 "QDEMode" : component.get('v.QDEMode'),
                 "calledFromVFPage" : component.get('v.calledFromVFPage'),
                 "errorMessage" : component.get('v.errorMessage'),
                "showErrorMessage" :component.get('v.showErrorMessage'),
                "LoanConId" : component.get('v.LoanConId'),
                "idTypes" : component.get('v.idTypes'),
                "oldWrap" : component.get('v.oldWrap'),
                "oldWrapString" : component.get('v.oldWrapString'),
                "disableAddButton" : component.get('v.disableAddButton'),
                "disableEditButton" : component.get('v.disableEditButton')
            }
        );
        evt.fire();
		}    
    }, 
    addDocuments : function(component, event, helper) {
         component.set('v.showErrorMessage' , false);
        console.log('ADD Documents method');
        var fromLAComponent = true;
        console.log('Setting fromLAComponent'+fromLAComponent);
        var laId = component.get('v.loanAppID');
        var wrap = [];
        wrap = component.get("v.CDWrap"); 
        var evt = $A.get("e.c:callUploadDocument");
        evt.setParams(
            { 
                "recordId": laId,
                "fromLAComponent" : fromLAComponent,
                "laVal" : JSON.stringify(component.get('v.laVal')),
                "CDWrap" : wrap,
                "lstBorrowerType" : component.get('v.lstBorrowerType'),
                "lstconstitution" : component.get('v.lstconstitution'),
                "lstCustSegment" : component.get('v.lstCustSegment'),
                "loanAppID" : component.get('v.loanAppID'),
                "prod" : component.get('v.prod'),
                "localPol" : component.get('v.localPol'),
                "customer" : component.get('v.customer'),
                "isDisplay" : component.get('v.isDisplay') ,
                "isEditable" : component.get('v.isEditable'),
                "isInserted" : component.get('v.isInserted'),
                "isDisable" : component.get('v.isDisable'),
                "addAddress" : component.get('v.addAddress'),
                "CDfetched" : component.get('v.CDfetched'),
                "data" : component.get('v.data'),
                "columns" : component.get('v.columns'),
                "borrowerTy" : component.get('v.borrowerTy'),
                "LaNum" : component.get('v.LaNum'),
                "isEditableLA" : component.get('v.isEditableLA'),
                 "QDEMode" : component.get('v.QDEMode'),
                 "calledFromVFPage" : component.get('v.calledFromVFPage'),
                 "errorMessage" : component.get('v.errorMessage'),
                "showErrorMessage" :component.get('v.showErrorMessage'),
                "LoanConId" : component.get('v.LoanConId'),
                "idTypes" : component.get('v.idTypes'),
                "oldWrap" : component.get('v.oldWrap'),
                "oldWrapString" : component.get('v.oldWrapString'),
                "disableAddButton" : component.get('v.disableAddButton'),
                "disableEditButton" : component.get('v.disableEditButton')
            }
        );
        evt.fire();
    }, 
    filterSalesDivisonValues : function(component, event, helper) {
          
        console.log('Changing borrower type valeus');
         component.set('v.showErrorMessage' , false);
         var index = component.get('v.LoanConId');
         console.log('Index is '+index);
         var wrap = [];
         wrap = component.get('v.CDWrap');
         console.log('Wrapper is : ' +JSON.stringify(component.get('v.CDWrap')));
         
        //var dynamicCmp = component.find("InputSelectDynamic");
        //console.log('dynamicCmp '+dynamicCmp);
        //var borrower = dynamicCmp.get("v.value");
        
         if(wrap[index].customer != null && wrap[index].isPrimary){
         var customerID = wrap[index].customer;
         console.log('customerID:::::'+customerID); 
         }
         else if(wrap[index].customer.val != null && wrap[index].customer.val != 'undefined'){
             var customerID = wrap[index].customer.val;
             console.log('customerID:::::'+customerID);     
         }
             else{
                 var customerID = wrap[index].customer;
                 console.log('customerID:::::'+customerID);   
             }
         var wrapVal = [];
         wrapVal = wrap;
         wrapVal[index].customer = customerID;
        console.log('WRAP '+JSON.stringify(wrap[index]));
        
        var action11 = component.get('c.populateCustomerDetails');
        action11.setParams({
                "serializedWrapper" :JSON.stringify(wrap[index])
            });
            action11.setCallback(this,function(a){
                var status  = a.getReturnValue();
                console.log('status '+status);
                if(status) {
                      var toastEvent = $A.get("e.force:showToast");
                       toastEvent.setParams({
                        "title": "",
                        "type": "Error",
                        "message": "The borrower type and customer type should be same."
                    });  
                    toastEvent.fire();
                } else {  
                    
                    helper.filterSalesDivisonValues4(component, event, helper);
                }    
            });
            $A.enqueueAction(action11);
         
    },
    pensionFlagChange : function(component, event, helper) {
        helper.pensionFlagChange(component, event, helper);
    },
     incomeFlagChange : function(component, event, helper) {
        helper.incomeFlagChange(component, event, helper);
         //else {
            //wrap[index].idType = '';
              //component.set('v.CDWrap',wrap); 
         //}
    },
       filterSalesDivisonValues1 : function(component, event, helper) {
           console.log('In filterSalesDivisonValues1');
        helper.filterSalesDivisonValues3(component, event, helper);
    },
    close: function(component, event, helper) {
        component.set('v.showErrorMessage' , false);
        //var errorOcc = false;
        var QDEMode = component.get('v.QDEMode');
        console.log('QDEMode in close button'+QDEMode);
        console.log('WRap:: '+JSON.stringify(component.get("v.CDWrap")));
        var action11 = component.get('c.checkPANCIBILStatus');
            action11.setParams({
                "serializedWrapper" :JSON.stringify(component.get("v.CDWrap"))
            });
            action11.setCallback(this,function(a){
                var errorOcc  = a.getReturnValue();
                console.log('errorOcc '+errorOcc);
        //if(errorOcc) {
          //  var toastEvent = $A.get("e.force:showToast");
            //toastEvent.setParams({
              //  "title": "Error!",
            //    "type": "Error",
              //  "message": "PAN/CIBIL integration is not hit for all the users"
            //});
        //    toastEvent.fire();
       // } else {
          
            
            if(QDEMode == true) {
                if(component.get('v.disableAddButton') == true || component.get('v.disableEditButton') == true || (component.get('v.isInserted') == false || component.get('v.isEditableLA') == true)) {
                   //var result = confirm('There are some unsaved changes in Loan application, Are you sure you want to close the screen?'); 
                    //console.log('result is '+result);
                    //if(result == true){
                       //$A.get("e.force:closeQuickAction").fire();
                        //$A.get('e.force:refreshView').fire();   
                        //window.location.reload(); 
                    //}
                    component.set('v.showConfirmDialog', true); 
                } else {
					 /*** Added by Saumya For Close Refresh BRE Record ***/
                    console.log('I am in Else');
                    var recordId = component.get('v.recordId');
                    var action = component.get('c.uncheckBRERecord');
                    action.setParams({
                        "LAId" :recordId
                    });
                    action.setCallback(this,function(a){
                       console.log('I got rsponse'); 
                    });
                   
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();  
                    window.location.reload();
                    $A.enqueueAction(action); 
                    /*** Added by Saumya For Close Refresh BRE Record ***/
                }
            }else {
              //  console.log('Sending data back to VF');
            //var evt = $A.get("e.c:callPassDataToVF");
            //console.log('lightningAppExternalEvent '+evt);
            //evt.setParams({
              //  'appId':component.get('v.loanAppID')
        //    });
          //  evt.fire(); 
            //console.log('Check where i am ');
                if(component.get('v.disableAddButton') == true || component.get('v.disableEditButton') == true || (component.get('v.isInserted') == false || component.get('v.isEditableLA') == true)) {
                       //var result = confirm('There are some unsaved changes in Loan application, Are you sure you want to close the screen?'); 
                        //console.log('result is '+result);
                        //if(result == true){
                    
                          //  var idx = component.get('v.loanAppID');
                        //    console.log('idx'+idx);
                          //  var navEvt = $A.get("e.force:navigateToSObject");
                        //    navEvt.setParams({
                          //  "recordId": idx,
                            //"slideDevName": "detail"
                            
                             //});
                              //navEvt.fire(); 
                        //}
                        component.set('v.showConfirmDialog', true); 
                } else {
					/*** Added by Saumya For Close Refresh BRE Record ***/
                    console.log('I am in Else');

                    var action = component.get('c.uncheckBRERecord');
                    action.setParams({
                        "LAId" : component.get('v.loanAppID')
                    });
                    action.setCallback(this,function(a){
                       console.log('I got rsponse'); 
                    });
                    /*** Added by Saumya For Close Refresh BRE Record ***/
                       var idx = component.get('v.loanAppID');
                        console.log('idx'+idx);
                        var navEvt = $A.get("e.force:navigateToSObject");
                        navEvt.setParams({
                        "recordId": idx,
                        "slideDevName": "detail"
                        
                         });
                         navEvt.fire(); 
					/*** Added by Saumya For Close Refresh BRE Record ***/
                        $A.enqueueAction(action);
                    /*** Added by Saumya For Close Refresh BRE Record ***/
                }    
            }     
       // }
                
        });
         $A.enqueueAction(action11);     
        
    } ,
    handleConfirmDialogNo: function(component, event, helper) {
        component.set('v.showConfirmDialog', false); 
    },
    handleConfirmDialogYes: function(component, event, helper) {
        var QDEMode = component.get('v.QDEMode');
        if(QDEMode == true) {
				/*** Added by Saumya For Close Refresh BRE Record ***/
            var recordId = component.get('v.recordId');
            var action = component.get('c.uncheckBRERecord');
            action.setParams({
                "LAId" :recordId
            });
            action.setCallback(this,function(a){
                console.log('I got response'); 
            });
            /*** Added by Saumya For Close Refresh BRE Record ***/
                $A.get("e.force:closeQuickAction").fire();
                $A.get('e.force:refreshView').fire();   
                window.location.reload(); 
                component.set('v.showConfirmDialog', false); 
				$A.enqueueAction(action);

            }else {
				 /*** Added by Saumya For Close Refresh BRE Record ***/
                var action = component.get('c.uncheckBRERecord');
                action.setParams({
                    "LAId" : component.get('v.loanAppID')
                });
                action.setCallback(this,function(a){
                    console.log('I got rsponse'); 
                });
                $A.enqueueAction(action);
                /*** Added by Saumya For Close Refresh BRE Record ***/
              //  console.log('Sending data back to VF');
            //var evt = $A.get("e.c:callPassDataToVF");
            //console.log('lightningAppExternalEvent '+evt);
            //evt.setParams({
              //  'appId':component.get('v.loanAppID')
        //    });
          //  evt.fire(); 
            //console.log('Check where i am ');
                var idx = component.get('v.loanAppID');
                console.log('idx'+idx);
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                "recordId": idx,
                "slideDevName": "detail"
                
                 });
                 navEvt.fire(); 
                component.set('v.showConfirmDialog', false); 
            } 
    },
    //Added EditRecord by Aman for Edit button functionality on table
    EditRecord : function (component,event,helper)
    {
        var index = event.target.id;
        console.log('index::'+ index);
        var wrap = [];
        wrap = component.get("v.CDWrap"); 
        var customerDetailId = wrap[index].CDId;
        //component.set('v.ShowEdit',true);
         var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": customerDetailId
        });
        editRecordEvent.fire();
     
    },
     setLoanConid: function (component, event,helper) {
        
        var loanContactid = event.getSource().get("v.text");
        console.log('LoanConId: '+loanContactid);
        component.set('v.LoanConId', loanContactid);
    },
    handleUndo : function (component, event,helper) {
        console.log('OLD'+ component.get('v.oldWrapString'));
        var obj = JSON.parse(component.get('v.oldWrapString'));
        console.log('obj '+obj);
        component.set('v.showErrorMessage' , false);
         var index = event.target.id;
        console.log('index::'+ index);
        var wrap = [];
        wrap = component.get("v.CDWrap");
        wrap[index] = obj;
        wrap[index].isEditable = false;
        component.set("v.CDWrap",wrap);
        if(wrap[index].CDId == null || wrap[index].CDId == '') {
            console.log('CD ID' +wrap[index].CDId);
            component.set('v.disableAddButton',false);
        } else {
            component.set('v.disableEditButton',false);
        }
    },
    idTypeChange : function (component, event,helper) {
       helper.idTypeChange(component, event,helper)
    }
})