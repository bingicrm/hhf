({
	onAccept : function(component, event, helper) {
		 var loanApp =  component.get("v.recordId") ;
        console.log(loanApp);
       
        var action = component.get('c.setOwner');
        action.setParams({
           
            "oppId": component.get("v.recordId")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                console.log('==result===' + actionResult.getReturnValue());
              
            }
            $A.get("e.force:closeQuickAction").fire();
            $A.get('e.force:refreshView').fire(); 
        });
        $A.enqueueAction(action);
    
      }
})