({
    doInitHelper: function(component, event, helper)
    {
        component.set("v.spinner", true);
        var loanApp =  component.get("v.recordId");
        
        //Call Apex Method starts.
        var action = component.get("c.loadLoanApplicationStageInfo");
        action.setParams({ loadApplicationID : component.get("v.recordId") });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state==="SUCCESS")
            {
                console.log('Response state ++ '+state);
                var returnedWrapper = response.getReturnValue(); 
                
                if(returnedWrapper!=null)
                {
                    console.log('returnedWrapper++ '+JSON.stringify(returnedWrapper));
                    var returnedWrapperStringified = JSON.parse(JSON.stringify(returnedWrapper));
                    console.log('returnedWrapperStringified++ '+returnedWrapperStringified.message);
                    //console.log('Calling tryFun + '+helper.tryFun());
                    
                    if(this.checkBlankString(returnedWrapperStringified.message) && 
                       returnedWrapperStringified.message==='Loan Application Found' && //Move the Message to Label
                       this.checkBlankString(returnedWrapperStringified.currentStage) &&
                       this.checkBlankString(returnedWrapperStringified.currentSubStage) &&
                       this.checkBlankString(returnedWrapperStringified.nextStage) &&
                       this.checkBlankString(returnedWrapperStringified.nextSubStage))
                    {
                        console.log('Setting current and next Stage, Sub Stage Values for disp ');
                        var currentStage = returnedWrapperStringified.currentStage;
                        var currentSubStage = returnedWrapperStringified.currentSubStage;
                        var nextStage = returnedWrapperStringified.nextStage;
                        var nextSubStage = returnedWrapperStringified.nextSubStage;
                        
                        component.set("v.currentStage",currentStage);
                        component.set("v.currentSubStage",currentSubStage);
                        component.set("v.nextStage",nextStage);
                        component.set("v.nextSubStage",nextSubStage);
                        component.set("v.spinner", false);
                    }
                    else
                    {
                        console.log('Data issue with Loan Application');
                        component.set("v.spinner", false);
                    }
                }
            }
            else
            {
                console.log('Error encountered.');
            }
        });
        
        //enqueue apex call
        $A.enqueueAction(action);
        
        //Call Apex Method ends.
    },
    onAcceptHelper : function(component, event, helper)
    {
        component.set("v.spinner", true);
        console.log('record ID+ '+component.get("v.recordId"));
        var action = component.get("c.loanApplicationStageChange");
        action.setParams({ loadApplicationID : component.get("v.recordId") });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            //var returnedWrapper = response.getReturnValue();
            //console.log('returnedWrapper +'+returnedWrapper);
              //  var returnedWrapperStringified = JSON.parse(JSON.stringify(returnedWrapper));
            //console.log('returnedWrapperStringified++ '+returnedWrapperStringified);
            if(state==="SUCCESS")
            {
                console.log('Success');
                var returnedWrapper = response.getReturnValue();
                var returnedWrapperStringified = JSON.parse(JSON.stringify(returnedWrapper));
                console.log('returnedWrapperStringified++ '+returnedWrapperStringified.message);
                if(this.checkBlankString(returnedWrapperStringified.message) && 
                   returnedWrapperStringified.message=='Stage and Sub-Stage Successfully Changed to Tranche and Tranche Processing')
                {
                    console.log('Stage and Sub Stage Change successful.');
                    component.set("v.spinner", false);
                    this.showToast(component, event, helper,'Your application has been moved to the following stage: Tranche Processing.','success');
                    $A.get('e.force:refreshView').fire();
                    $A.get("e.force:closeQuickAction").fire();
                }
                else
                {
                    console.log('Loan Application Stage and Sub Stage not changed');
                    component.set("v.spinner", false);
                    this.showToast(component, event, helper,'Error moving Loan Application to Tranche - Contact Administrator','error');
                }
            }
            else
            {
                console.log('Issue with the controller method call/processing');
                component.set("v.spinner", false);
                this.showToast(component, event, helper,'Loan Application Stage and Sub Stage not changed','error');
            }
        }); 
        $A.enqueueAction(action);
        
    },
    onRejectHelper : function(component, event, helper)
    {
    	$A.get("e.force:closeQuickAction").fire();    
    },
    checkBlankString : function(str)
    {
        //console.log('str val+ '+str+' '+str.length);
        if (str == null ||  str == undefined || str.length == 0)
        {
            console.log('Blank String');
            return false;  
        }
        return true;
    },
    showToast : function(component, event, helper, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": type,
            "message": message,
            "type" : type,
        });
        toastEvent.fire();
    },
})