({
    doinit : function(component, event, helper) 
    {
        helper.doInitHelper(component, event, helper);
    },
    onAccept : function(component, event, helper)
    {
        helper.onAcceptHelper(component, event, helper);
    },
    doneNoAction : function(component, event, helper)
    {
        helper.onRejectHelper(component, event, helper);
    }
    
})