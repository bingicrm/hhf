({
	doInit : function(component, event, helper) {
		var action = component.get("c.getProjectInfo");
        action.setParams({"recordId" : component.get("v.recordId")});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                component.set("v.lapp", response.getReturnValue());
                console.log('APF Type'+response.getReturnValue().APF_Type__c);
                console.log('showBanner before if'+component.get("v.showBanner"));
                /*
                 * Commented based on input received from Shirish, to initiate Technical TPV for Deemed APF's : TIL - 2168
                if(response.getReturnValue().APF_Type__c == 'Deemed') {
                    var resultsToast = $A.get("e.force:showToast");
                
                    
                        resultsToast.setParams({
                            "title" : "Action not required",
                            "message" : "Deemed APF does not require Third Party Verification."
                        });
                    
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                }
                */
                if($A.util.isUndefined(response.getReturnValue().APF_Type__c)) {
                    var resultsToast1 = $A.get("e.force:showToast");
                
                    
                        resultsToast1.setParams({
                            "title" : "Action not required",
                            "message" : "Please specify APF Type on Project before initiating the Verifications."
                        });
                    
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast1.fire();
                    $A.get("e.force:refreshView").fire();
                }
                else {
                    console.log('showBanner from else'+component.get("v.showBanner"));
                    component.set("v.showBanner", true);
                    console.log('showBanner from else after update'+component.get("v.showBanner"));
                }
            }else{
                console.log('Problem getting Project Information, response state : '+state);
            }
        });
        $A.enqueueAction(action);
	},
    requestCreation: function(component, event, helper) {
        console.log('1111');
        helper.showSpinner(component);
        var action = component.get('c.getMessage');
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.hideSpinner(component);
            }
        });
        $A.enqueueAction(action);
        
        var action = component.get("c.thirdPartyCall");
		var loan = component.get("v.lapp");
        console.log(loan);
        action.setParams({
            "objProject" : loan
        });
        console.log('2222');
        action.setCallback(this, function(actionResult) {
            console.log('3333');
            var state = actionResult.getState();
            if(state === "SUCCESS"){
                console.log('4444');
                var resultsToast = $A.get("e.force:showToast");
                
                if(actionResult.getReturnValue() == null){
                	resultsToast.setParams({
                    	"title" : "Records Created",
                    	"message" : "Third Party verification records have been created."
                	});
            	}
            	else{
                	resultsToast.setParams({
                    	"title" : "Error",
                    	"message" : actionResult.getReturnValue()
                	});
            	}
 
				$A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
                console.log('5555');
            }else if(state === "ERROR"){
                console.log('Problem creating records, response state '+state);
                console.log('6666');
            }else{
                console.log('Unknown problem: '+state);
                console.log('7777');
            }
        });
        $A.enqueueAction(action);
    },
    cancelCreation: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})