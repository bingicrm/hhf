({   

doInit : function(component, event) {       

// Call the Apex Class Method from Javascript       
var recordId = component.get("v.recordId");
var action = component.get("c.rejectLead");       
                  
// Place the Action in the Queue    
action.setParams({
        "idRecordId":recordId
    });
action.setCallback(this, function(response) {
            var state = response.getState();
    		
            if(component.isValid() && state === "SUCCESS") {
                if (response.getReturnValue().includes('Unsuccessfull')) {
                    component.set("v.message", 'Lead not rejected, please fill Rejection reason. <br/> ' );
                      		
                }
                //Below else if block added by Abhilekh on 7th August 2019 for TIl-1290
                else if(response.getReturnValue().includes('Already Rejected')){
                    component.set("v.message", 'Lead has already been rejected.' );
                }
                else {
                   		component.set("v.message", "Lead rejected successfully! ");
                         $A.get('e.force:refreshView').fire();
                         
                }    
            } else {
                alert("Failed with state: " + state);
                
                component.set("v.message", "An error occurred, please contact System Administrator");
            }
        });    
$A.enqueueAction(action);   

},
    isRefreshed: function(component, event, helper) {
        location.reload();
    }

})