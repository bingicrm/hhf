({
    initRecord: function(component, event, helper) {
        var isverified = component.find("isChecked").get("v.value");
        
        component.set("v.isverified",isverified);
        helper.getMonthlyBankingList(component,helper,event);
        helper.docStatusPickListVal(component,helper, event);
    },
    
    disableBankName11: function (component, event, helper) {
        var bnkname = component.get("v.BankName1");
        var Accountno1 = component.get("v.Accountno1");
        component.set("v.perfiosButtonClicked" ,false);
        if(bnkname != undefined && bnkname != null && bnkname != '') {
            component.set("v.disableBank11",true);
        } else {
            if(Accountno1 == undefined || Accountno1 == '' || Accountno1 == null) {
                component.set("v.disableBank11",false);
            }
        }
    },
    disableBankName21: function (component, event, helper) {
        var bnkname2 = component.get("v.BankName2");
        var Accountno2 = component.get("v.Accountno2");
        component.set("v.perfiosButtonClicked2" ,false);
        if(bnkname2 != undefined && bnkname2 != null && bnkname2 != '') {
            component.set("v.disableBank21",true);
        } else {
            if(Accountno2 == undefined || Accountno2 == '' || Accountno2 == null) {
                component.set("v.disableBank21",false);
            }
        }
    },
    disableBankName31: function (component, event, helper) {
        var bnkname3 = component.get("v.BankName3");
        var Accountno3 = component.get("v.Accountno3");
        component.set("v.perfiosButtonClicked3" ,false);
        if(bnkname3 != undefined && bnkname3 != null && bnkname3 != '') {
            component.set("v.disableBank31",true);
        } else {
            if(Accountno3 == undefined || Accountno3 == '' || Accountno3 == null) {
                component.set("v.disableBank31",false);
            }
        }
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    disableBankName41: function (component, event, helper) {
        var bnkname4 = component.get("v.BankName4");
        var Accountno4 = component.get("v.Accountno4");
        component.set("v.perfiosButtonClicked4" ,false);
        if(bnkname4 != undefined && bnkname4 != null && bnkname4 != '') {
            component.set("v.disableBank41",true);
        } else {
            if(Accountno4 == undefined || Accountno4 == '' || Accountno4 == null) {
                component.set("v.disableBank41",false);
            }
        }
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    disableBankName51: function (component, event, helper) {
        var bnkname5 = component.get("v.BankName5");
        var Accountno5 = component.get("v.Accountno5");
        component.set("v.perfiosButtonClicked5" ,false);
        if(bnkname5 != undefined && bnkname5 != null && bnkname5 != '') {
            component.set("v.disableBank51",true);
        } else {
            if(Accountno5 == undefined || Accountno5 == '' || Accountno5 == null) {
                component.set("v.disableBank51",false);
            }
        }
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    disableBankName61: function (component, event, helper) {
        var bnkname6 = component.get("v.BankName6");
        var Accountno6 = component.get("v.Accountno6");
        component.set("v.perfiosButtonClicked6" ,false);
        if(bnkname6 != undefined && bnkname6 != null && bnkname6 != '') {
            component.set("v.disableBank61",true);
        } else {
            if(Accountno6 == undefined || Accountno6 == '' || Accountno6 == null) {
                component.set("v.disableBank61",false);
            }
        }
    },
    
    disableBankName1 : function (component, event, helper) {
        var bnkName11 = component.get("v.BankName11");
        var Accountno11 = component.get("v.Accountno11");
        component.set("v.perfiosButtonClicked" ,false);
        if(bnkName11 != undefined && bnkName11 != null && bnkName11 != '') {
            component.set("v.disableBank1",true);
        } else {
            if(Accountno11 == undefined || Accountno11 == '' || Accountno11 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var x1 = response.getReturnValue(); 
                        if(x1 == null) {
                            component.set("v.disableBank1" , true);
                        } else {
                            component.set("v.disableBank1",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
    },
    disableBankName2 : function (component, event, helper) {
        var bnkName21 = component.get("v.BankName21");
        var Accountno21 = component.get("v.Accountno21");
        component.set("v.perfiosButtonClicked2" ,false);
        if(bnkName21 != undefined && bnkName21 != null && bnkName21 != '') {
            component.set("v.disableBank2",true);
        } else {
            if(Accountno21 == undefined || Accountno21 == '' || Accountno21 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") { 
                        var x1 = response.getReturnValue(); 
                        if(x1 == null) {
                            component.set("v.disableBank2" , true);
                        } else {
                            component.set("v.disableBank2",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
    },
    
    disableBankName3 : function (component, event, helper) {
        var bnkName31 = component.get("v.BankName31");
        var Accountno31 = component.get("v.Accountno31");
        component.set("v.perfiosButtonClicked3" ,false);
        if(bnkName31 != undefined && bnkName31 != null && bnkName31 != '') {
            component.set("v.disableBank3",true);
        } else {
            if(Accountno31 == undefined || Accountno31 == '' || Accountno31 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var x1 = response.getReturnValue();
                        if(x1 == null) {
                            component.set("v.disableBank3" , true);
                        } else {
                            component.set("v.disableBank3",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    disableBankName4 : function (component, event, helper) {
        var bnkName41 = component.get("v.BankName41");
        var Accountno41 = component.get("v.Accountno41");
        component.set("v.perfiosButtonClicked4" ,false);
        if(bnkName41 != undefined && bnkName41 != null && bnkName41 != '') {
            component.set("v.disableBank4",true);
        } else {
            if(Accountno41 == undefined || Accountno41 == '' || Accountno41 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var x1 = response.getReturnValue();
                        if(x1 == null) {
                            component.set("v.disableBank4" , true);
                        } else {
                            component.set("v.disableBank4",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    disableBankName5 : function (component, event, helper) {
        var bnkName51 = component.get("v.BankName51");
        var Accountno51 = component.get("v.Accountno51");
        component.set("v.perfiosButtonClicked5" ,false);
        if(bnkName51 != undefined && bnkName51 != null && bnkName51 != '') {
            component.set("v.disableBank5",true);
        } else {
            if(Accountno51 == undefined || Accountno51 == '' || Accountno51 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var x1 = response.getReturnValue();
                        if(x1 == null) {
                            component.set("v.disableBank5" , true);
                        } else {
                            component.set("v.disableBank5",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    disableBankName6 : function (component, event, helper) {
        var bnkName61 = component.get("v.BankName61");
        var Accountno61 = component.get("v.Accountno61");
        component.set("v.perfiosButtonClicked6" ,false);
        if(bnkName61 != undefined && bnkName61 != null && bnkName61 != '') {
            component.set("v.disableBank6",true);
        } else {
            if(Accountno61 == undefined || Accountno61 == '' || Accountno61 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var x1 = response.getReturnValue();
                        if(x1 == null) {
                            component.set("v.disableBank6" , true);
                        } else {
                            component.set("v.disableBank6",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
    },
    
    disable1BankName11: function (component, event, helper) {
        var Accountno1 = component.get("v.Accountno1");
        var bnkname = component.get("v.BankName1");
        component.set("v.perfiosButtonClicked" ,false);
        if(Accountno1 != undefined && Accountno1 != null && Accountno1 != '') {
            component.set("v.disableBank11",true);
        } else {
            if(bnkname == undefined || bnkname == '' ||bnkname== null) {
                component.set("v.disableBank11",false);
            }
        }
    },
    disable2BankName11: function (component, event, helper) {
        var AccountType1 = component.get("v.AccountType1");
        var bnkname = component.get("v.BankName1");
        component.set("v.perfiosButtonClicked" ,false);
        if(AccountType1 != undefined && AccountType1 != null && AccountType1 != '') {
            component.set("v.disableBank11",true);
            
            //Below if and else block added by Abhilekh for BRE2 Enhancements
            if(AccountType1 == 'OD' || AccountType1 == 'CC'){
                component.set("v.disableLimitAmount1",false);
            }
            else{
                component.set("v.disableLimitAmount1",true);
            }
        } else {
            component.set("v.disableLimitAmount1",true); //Added by Abhilekh for BRE2 Enhancements
            if(bnkname == undefined || bnkname == '' ||bnkname== null) {
                component.set("v.disableBank11",false);
            }
        }
    },
    disable1BankName21: function (component, event, helper) {
        var Accountno2 = component.get("v.Accountno2");
        var bnkname2 = component.get("v.BankName2");
        component.set("v.perfiosButtonClicked2" ,false);
        if(Accountno2 != undefined && Accountno2 != null && Accountno2 != '') {
            component.set("v.disableBank21",true);
        } else {
            if(bnkname2 == undefined || bnkname2 == '' ||bnkname2== null) {
                component.set("v.disableBank21",false);
            }
        }
    },
    disable2BankName21: function (component, event, helper) {
        var AccountType2 = component.get("v.AccountType2");
        var bnkname = component.get("v.BankName2");
        component.set("v.perfiosButtonClicked" ,false);
        if(AccountType2 != undefined && AccountType2 != null && AccountType2 != '') {
            component.set("v.disableBank21",true);
            
            //Below if and else block added by Abhilekh for BRE2 Enhancements
            if(AccountType2 == 'OD' || AccountType2 == 'CC'){
                component.set("v.disableLimitAmount2",false);
            }
            else{
                component.set("v.disableLimitAmount2",true);
            }
        } else {
            component.set("v.disableLimitAmount2",true); //Added by Abhilekh for BRE2 Enhancements
            if(bnkname == undefined || bnkname == '' ||bnkname== null) {
                component.set("v.disableBank21",false);
            }
        }
    },
    disable1BankName31: function (component, event, helper) {
        var Accountno3 = component.get("v.Accountno3");
        var bnkname3 = component.get("v.BankName3");
        component.set("v.perfiosButtonClicked3" ,false);
        if(Accountno3 != undefined && Accountno3 != null && Accountno3 != '') {
            component.set("v.disableBank31",true);
        } else {
            if(bnkname3 == undefined || bnkname3 == '' ||bnkname3== null) {
                component.set("v.disableBank31",false);
            }
        }
    },
    disable2BankName31: function (component, event, helper) {
        var AccountType3 = component.get("v.AccountType3");
        var bnkname = component.get("v.BankName3");
        component.set("v.perfiosButtonClicked" ,false);
        if(AccountType3 != undefined && AccountType3 != null && AccountType3 != '') {
            component.set("v.disableBank31",true);
            
            //Below if and else block added by Abhilekh for BRE2 Enhancements
            if(AccountType3 == 'OD' || AccountType3 == 'CC'){
                component.set("v.disableLimitAmount3",false);
            }
            else{
                component.set("v.disableLimitAmount3",true);
            }
        } else {
            component.set("v.disableLimitAmount3",true); //Added by Abhilekh for BRE2 Enhancements
            if(bnkname == undefined || bnkname == '' ||bnkname== null) {
                component.set("v.disableBank31",false);
            }
        }
    },
    
    /*******Below code added by Abhilekh for BRE2 Enhancements************/
    disable1BankName41: function (component, event, helper) {
        var Accountno4 = component.get("v.Accountno4");
        var bnkname4 = component.get("v.BankName4");
        component.set("v.perfiosButtonClicked4" ,false);
        if(Accountno4 != undefined && Accountno4 != null && Accountno4 != '') {
            component.set("v.disableBank41",true);
        } else {
            if(bnkname4 == undefined || bnkname4 == '' ||bnkname4 == null) {
                component.set("v.disableBank41",false);
            }
        }
    },
    disable2BankName41: function (component, event, helper) {
        var AccountType4 = component.get("v.AccountType4");
        var bnkname = component.get("v.BankName4");
        component.set("v.perfiosButtonClicked" ,false);
        if(AccountType4 != undefined && AccountType4 != null && AccountType4 != '') {
            component.set("v.disableBank41",true);
            
            //Below if and else block added by Abhilekh for BRE2 Enhancements
            if(AccountType4 == 'OD' || AccountType4 == 'CC'){
                component.set("v.disableLimitAmount4",false);
            }
            else{
                component.set("v.disableLimitAmount4",true);
            }
        } else {
            component.set("v.disableLimitAmount4",true); //Added by Abhilekh for BRE2 Enhancements
            if(bnkname == undefined || bnkname == '' || bnkname== null) {
                component.set("v.disableBank41",false);
            }
        }
    },
    
    disable1BankName51: function (component, event, helper) {
        var Accountno5 = component.get("v.Accountno5");
        var bnkname5 = component.get("v.BankName5");
        component.set("v.perfiosButtonClicked5" ,false);
        if(Accountno5 != undefined && Accountno5 != null && Accountno5 != '') {
            component.set("v.disableBank51",true);
        } else {
            if(bnkname5 == undefined || bnkname5 == '' ||bnkname5== null) {
                component.set("v.disableBank51",false);
            }
        }
    },
    disable2BankName51: function (component, event, helper) {
        var AccountType5 = component.get("v.AccountType5");
        var bnkname = component.get("v.BankName5");
        component.set("v.perfiosButtonClicked" ,false);
        if(AccountType5 != undefined && AccountType5 != null && AccountType5 != '') {
            component.set("v.disableBank51",true);
            
            //Below if and else block added by Abhilekh for BRE2 Enhancements
            if(AccountType5 == 'OD' || AccountType5 == 'CC'){
                component.set("v.disableLimitAmount5",false);
            }
            else{
                component.set("v.disableLimitAmount5",true);
            }
        } else {
            component.set("v.disableLimitAmount5",true); //Added by Abhilekh for BRE2 Enhancements
            if(bnkname == undefined || bnkname == '' ||bnkname== null) {
                component.set("v.disableBank51",false);
            }
        }
    },
    
    disable1BankName61: function (component, event, helper) {
        var Accountno6 = component.get("v.Accountno6");
        var bnkname6 = component.get("v.BankName6");
        component.set("v.perfiosButtonClicked6" ,false);
        if(Accountno6 != undefined && Accountno6 != null && Accountno6 != '') {
            component.set("v.disableBank61",true);
        } else {
            if(bnkname6 == undefined || bnkname6 == '' ||bnkname6 == null) {
                component.set("v.disableBank61",false);
            }
        }
    },
    disable2BankName61: function (component, event, helper) {
        var AccountType6 = component.get("v.AccountType6");
        var bnkname = component.get("v.BankName6");
        component.set("v.perfiosButtonClicked" ,false);
        if(AccountType6 != undefined && AccountType6 != null && AccountType6 != '') {
            component.set("v.disableBank61",true);
            
            //Below if and else block added by Abhilekh for BRE2 Enhancements
            if(AccountType6 == 'OD' || AccountType6 == 'CC'){
                component.set("v.disableLimitAmount5",false);
            }
            else{
                component.set("v.disableLimitAmount5",true);
            }
        } else {
            component.set("v.disableLimitAmount5",true);
            if(bnkname == undefined || bnkname == '' ||bnkname== null) {
                component.set("v.disableBank61",false);
            }
        }
    },
    /*******Above code added by Abhilekh for BRE2 Enhancements************/
    
    disable1BankName1: function (component, event, helper) {
        var Accountno11 = component.get("v.Accountno11");
        var bnkName11 = component.get("v.BankName11");
        component.set("v.perfiosButtonClicked" ,false);
        if(Accountno11 != undefined && Accountno11 != null && Accountno11 != '') {
            component.set("v.disableBank1",true);
        } else {
            if(bnkName11 == undefined || bnkName11 == '' || bnkName11 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") { 
                        var x1 = response.getReturnValue();
                        if(x1 == null) {
                            component.set("v.disableBank1" , true);
                        } else {
                            component.set("v.disableBank1",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
    },
    disable2BankName1: function (component, event, helper) {
        var Accounttype11 = component.get("v.AccountType11");
        var bnkName11 = component.get("v.BankName11");
        component.set("v.perfiosButtonClicked" ,false);
        if(Accounttype11 != undefined && Accounttype11 != null && Accounttype11 != '') {
            component.set("v.disableBank1",true);
            
            //Below if and else block added by Abhilekh for BRE2 Enhancements
            if(Accounttype11 == 'OD' || Accounttype11 == 'CC'){
                component.set("v.disableLimitAmount1",false);
            }
            else{
                component.set("v.disableLimitAmount1",true);
            }
        } else {
            component.set("v.disableLimitAmount1",true); //Added by Abhilekh for BRE2 Enhancements
            if(bnkName11 == undefined || bnkName11 == '' || bnkName11 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") { 
                        var x1 = response.getReturnValue();
                        if(x1 == null) {
                            component.set("v.disableBank1" , true);
                        } else {
                            component.set("v.disableBank1",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
    },
    disable1BankName2: function (component, event, helper) {
        var Accountno21 = component.get("v.Accountno21");
        var bnkName21 = component.get("v.BankName21");
        component.set("v.perfiosButtonClicked2" ,false);
        if(Accountno21 != undefined && Accountno21 != null && Accountno21 != '') {
            component.set("v.disableBank2",true);
        } else {
            if(bnkName21 == undefined || bnkName21 == '' || bnkName21 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var x1 = response.getReturnValue();
                        if(x1 == null) {
                            component.set("v.disableBank2" , true);
                        } else {
                            component.set("v.disableBank2",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
    },
    disable2BankName2: function (component, event, helper) {
        var Accounttype21 = component.get("v.AccountType21");
        var bnkName21 = component.get("v.BankName21");
        component.set("v.perfiosButtonClicked" ,false);
        if(Accounttype21 != undefined && Accounttype21 != null && Accounttype21 != '') {
            component.set("v.disableBank2",true);
            
            //Below if and else block added by Abhilekh for BRE2 Enhancements
            if(Accounttype21 == 'OD' || Accounttype21 == 'CC'){
                component.set("v.disableLimitAmount2",false);
            }
            else{
                component.set("v.disableLimitAmount2",true);
            }
        } else {
            component.set("v.disableLimitAmount2",true); //Added by Abhilekh for BRE2 Enhancements
            if(bnkName21 == undefined || bnkName21 == '' || bnkName21 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") { 
                        var x1 = response.getReturnValue();
                        if(x1 == null) {
                            component.set("v.disableBank2" , true);
                        } else {
                            component.set("v.disableBank2",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
    },
    disable1BankName3: function (component, event, helper) {
        var Accountno31 = component.get("v.Accountno31");
        var bnkName31 = component.get("v.BankName31");
        component.set("v.perfiosButtonClicked3" ,false);
        if(Accountno31 != undefined && Accountno31 != null && Accountno31 != '') {
            component.set("v.disableBank3",true);
        } else {
            if(bnkName31 == undefined || bnkName31 == '' || bnkName31 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var x1 = response.getReturnValue();
                        if(x1 == null) {
                            component.set("v.disableBank3" , true);
                        } else {
                            component.set("v.disableBank3",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
    },
    disable2BankName3: function (component, event, helper) {
        var Accounttype31 = component.get("v.AccountType31");
        var bnkName31 = component.get("v.BankName31");
        component.set("v.perfiosButtonClicked" ,false);
        if(Accounttype31 != undefined && Accounttype31 != null && Accounttype31 != '') {
            component.set("v.disableBank3",true);
            
            //Below if and else block added by Abhilekh for BRE2 Enhancements
            if(Accounttype31 == 'OD' || Accounttype31 == 'CC'){
                component.set("v.disableLimitAmount3",false);
            }
            else{
                component.set("v.disableLimitAmount3",true);
            }
        } else {
            component.set("v.disableLimitAmount3",true); //Added by Abhilekh for BRE2 Enhancements
            if(bnkName31 == undefined || bnkName31 == '' || bnkName31 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {  
                        var x1 = response.getReturnValue();
                        if(x1 == null) {
                            component.set("v.disableBank3" , true);
                        } else {
                            component.set("v.disableBank3",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
    },
    
    /*******Below code added by Abhilekh for BRE2 Enhancements************/
    disable1BankName4: function (component, event, helper) {
        var Accountno41 = component.get("v.Accountno41");
        var bnkName41 = component.get("v.BankName41");
        component.set("v.perfiosButtonClicked4" ,false);
        if(Accountno41 != undefined && Accountno41 != null && Accountno41 != '') {
            component.set("v.disableBank4",true);
        } else {
            if(bnkName41 == undefined || bnkName41 == '' || bnkName41 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var x1 = response.getReturnValue();
                        if(x1 == null) {
                            component.set("v.disableBank4" , true);
                        } else {
                            component.set("v.disableBank4",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
    },
    disable2BankName4: function (component, event, helper) {
        var Accounttype41 = component.get("v.AccountType41");
        var bnkName41 = component.get("v.BankName41");
        component.set("v.perfiosButtonClicked" ,false);
        if(Accounttype41 != undefined && Accounttype41 != null && Accounttype41 != '') {
            component.set("v.disableBank4",true);
            
            //Below if and else block added by Abhilekh for BRE2 Enhancements
            if(Accounttype41 == 'OD' || Accounttype41 == 'CC'){
                component.set("v.disableLimitAmount4",false);
            }
            else{
                component.set("v.disableLimitAmount4",true);
            }
        } else {
            component.set("v.disableLimitAmount4",true);
            if(bnkName41 == undefined || bnkName41 == '' || bnkName41 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {  
                        var x1 = response.getReturnValue();
                        if(x1 == null) {
                            component.set("v.disableBank4" , true);
                        } else {
                            component.set("v.disableBank4",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
    },
    
    disable1BankName5: function (component, event, helper) {
        var Accountno51 = component.get("v.Accountno51");
        var bnkName51 = component.get("v.BankName51");
        component.set("v.perfiosButtonClicked5" ,false);
        if(Accountno51 != undefined && Accountno51 != null && Accountno51 != '') {
            component.set("v.disableBank5",true);
        } else {
            if(bnkName51 == undefined || bnkName51 == '' || bnkName51 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var x1 = response.getReturnValue();
                        if(x1 == null) {
                            component.set("v.disableBank5" , true);
                        } else {
                            component.set("v.disableBank5",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
    },
    disable2BankName5: function (component, event, helper) {
        var Accounttype51 = component.get("v.AccountType51");
        var bnkName51 = component.get("v.BankName51");
        component.set("v.perfiosButtonClicked" ,false);
        if(Accounttype51 != undefined && Accounttype51 != null && Accounttype51 != '') {
            component.set("v.disableBank5",true);
            
            //Below if and else block added by Abhilekh for BRE2 Enhancements
            if(Accounttype51 == 'OD' || Accounttype51 == 'CC'){
                component.set("v.disableLimitAmount5",false);
            }
            else{
                component.set("v.disableLimitAmount5",true);
            }
        } else {
            component.set("v.disableLimitAmount5",true); //Added by Abhilekh for BRE2 Enhancements
            if(bnkName51 == undefined || bnkName51 == '' || bnkName51 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {  
                        var x1 = response.getReturnValue();
                        if(x1 == null) {
                            component.set("v.disableBank5" , true);
                        } else {
                            component.set("v.disableBank5",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
    },
    
    disable1BankName6: function (component, event, helper) {
        var Accountno61 = component.get("v.Accountno61");
        var bnkName61 = component.get("v.BankName61");
        component.set("v.perfiosButtonClicked6" ,false);
        if(Accountno61 != undefined && Accountno61 != null && Accountno61 != '') {
            component.set("v.disableBank6",true);
        } else {
            if(bnkName61 == undefined || bnkName61 == '' || bnkName61 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var x1 = response.getReturnValue();
                        if(x1 == null) {
                            component.set("v.disableBank6" , true);
                        } else {
                            component.set("v.disableBank6",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
    },
    disable2BankName6: function (component, event, helper) {
        var Accounttype61 = component.get("v.AccountType61");
        var bnkName61 = component.get("v.BankName61");
        component.set("v.perfiosButtonClicked" ,false);
        if(Accounttype61 != undefined && Accounttype61 != null && Accounttype61 != '') {
            component.set("v.disableBank6",true);
            
            //Below if and else block added by Abhilekh for BRE2 Enhancements
            if(Accounttype61 == 'OD' || Accounttype61 == 'CC'){
                component.set("v.disableLimitAmount6",false);
            }
            else{
                component.set("v.disableLimitAmount6",true);
            }
        } else {
            component.set("v.disableLimitAmount6",true); //Added by Abhilekh for BRE2 Enhancements
            if(bnkName61 == undefined || bnkName61 == '' || bnkName61 == null) {
                var action11 = component.get("c.getPerfiosbankMasterDetail");
                action11.setParams({
                    'CDrecID' : component.get("v.custRecordId"),
                    'getBankNames' : true 
                })
                action11.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {  
                        var x1 = response.getReturnValue();
                        if(x1 == null) {
                            component.set("v.disableBank6" , true);
                        } else {
                            component.set("v.disableBank6",false);
                        }
                    }
                });
                $A.enqueueAction(action11); 
            }
        }
    },
    /*******Above code added by Abhilekh for BRE2 Enhancements************/
    
    savePanel: function(component, event, helper) {
        helper.showSpinner(component, event, helper); //Added by Abhilekh for BRE2 Enhancements
        component.set("v.disableInput", false);
        //component.set("v.disableInput1",false); //Commented by Abhilekh for BRE2 Enhancements
        component.set("v.disableInput2",false);
        component.set("v.disableInput3",false); //Added by Abhilekh for BRE2 Enhancements
        component.set("v.disableInput4",false); //Added by Abhilekh for BRE2 Enhancements
        component.set("v.disableInput5",false); //Added by Abhilekh for BRE2 Enhancements
        component.set("v.disableInput6",false); //Added by Abhilekh for BRE2 Enhancements
        var sectionCount = component.get("v.sectionCount");
        var showSection2 = component.get("v.showSection2");
        var showSection3 = component.get("v.showSection3"); //Added by Abhilekh for BRE2 Enhancements
        var showSection4 = component.get("v.showSection4"); //Added by Abhilekh for BRE2 Enhancements
        var showSection5 = component.get("v.showSection5"); //Added by Abhilekh for BRE2 Enhancements
        
        if(sectionCount > 0){
        	helper.save1Helper(component, event, helper, "true");    
        }
        if((sectionCount == 2) || showSection2){
            helper.save2Helper(component, event, helper, "true");
        }
        if((sectionCount == 3) || showSection3){ //showSection3 added by Abhilekh for BRE2 Enhancements
            helper.save3Helper(component, event, helper, "true");
        }
        
        /****************Below code added by Abhilekh for BRE2 Enhancements**************/
        if((sectionCount == 4) || showSection4){
            helper.save4Helper(component, event, helper, "true");
        }
        if((sectionCount == 5) || showSection5){
            helper.save5Helper(component, event, helper, "true");
        }
        if(sectionCount == 6){
            helper.save6Helper(component, event, helper, "true");
        }
        /****************Above code added by Abhilekh for BRE2 Enhancements**************/
    },  
    fetchRecords_section1 : function(component, event, helper) {
        //helper.showSpinner(component, event, helper); //Added by Abhilekh for BRE2 Enhancements
        var disableBank1 = component.get("v.disableBank1");
        var disableBank11 = component.get("v.disableBank11");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked");
        helper.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"1", "Banking");
    },
    fetchRecords_section2 : function(component, event, helper) {
        //helper.showSpinner(component, event, helper); //Added by Abhilekh for BRE2 Enhancements
        var disableBank1 = component.get("v.disableBank2");
        var disableBank11 = component.get("v.disableBank21");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked2");
        helper.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"2", "Banking");
    },
    fetchRecords_section3 : function(component, event, helper) {
        //helper.showSpinner(component, event, helper); //Added by Abhilekh for BRE2 Enhancements
        var disableBank1 = component.get("v.disableBank3");
        var disableBank11 = component.get("v.disableBank31");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked3");
        helper.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"3", "Banking");
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    fetchRecords_section4 : function(component, event, helper) {
        //helper.showSpinner(component, event, helper);
        var disableBank1 = component.get("v.disableBank4");
        var disableBank11 = component.get("v.disableBank41");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked4");
        helper.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"4", "Banking");
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    fetchRecords_section5 : function(component, event, helper) {
        //helper.showSpinner(component, event, helper);
        var disableBank1 = component.get("v.disableBank5");
        var disableBank11 = component.get("v.disableBank51");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked5");
        helper.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"5", "Banking");
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    fetchRecords_section6 : function(component, event, helper) {
        //helper.showSpinner(component, event, helper);
        var disableBank1 = component.get("v.disableBank6");
        var disableBank11 = component.get("v.disableBank61");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked6");
        helper.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"6", "Banking");
    },
    
    fetchPerfiosData_section1:function(component, event, helper) {
        //helper.showSpinner(component, event, helper); //Added by Abhilekh for BRE2 Enhancements
        var disableBank1 = component.get("v.disableBank1");
        var disableBank11 = component.get("v.disableBank11");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked");
        helper.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"1", "Perfios");
    },
    fetchPerfiosData_section2:function(component, event, helper) {
        //helper.showSpinner(component, event, helper); //Added by Abhilekh for BRE2 Enhancements
        var disableBank1 = component.get("v.disableBank2");
        var disableBank11 = component.get("v.disableBank21");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked2");
        helper.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"2", "Perfios");
    },
    fetchPerfiosData_section3:function(component, event, helper) {
        //helper.showSpinner(component, event, helper); //Added by Abhilekh for BRE2 Enhancements
        var disableBank1 = component.get("v.disableBank3");
        var disableBank11 = component.get("v.disableBank31");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked3");
        helper.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"3", "Perfios");
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    fetchPerfiosData_section4:function(component, event, helper) {
        //helper.showSpinner(component, event, helper);
        var disableBank1 = component.get("v.disableBank4");
        var disableBank11 = component.get("v.disableBank41");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked4");
        helper.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"4", "Perfios");
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    fetchPerfiosData_section5:function(component, event, helper) {
        //helper.showSpinner(component, event, helper);
        var disableBank1 = component.get("v.disableBank5");
        var disableBank11 = component.get("v.disableBank51");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked5");
        helper.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"5", "Perfios");
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    fetchPerfiosData_section6:function(component, event, helper) {
        //helper.showSpinner(component, event, helper);
        var disableBank1 = component.get("v.disableBank6");
        var disableBank11 = component.get("v.disableBank61");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked6");
        helper.checkDuplicates (component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"6", "Perfios");
    },
    
    toggleSection: function(component, event, helper){
        // dynamically get aura:id name from 'data-auraId' attribute
        var sectionAuraId = event.target.getAttribute("data-auraId");
        //var myCmp = component.find(sectionAuraId);
        //var isExpandable = $A.util.hasClass(myCmp, "slds-section slds-is-open");
        //console.log('isExpandable==>'+isExpandable);
        //var isNotExpandable = $A.util.hasClass(component.find(sectionAuraId), "slds-section slds-is-close");
        //console.log('isNotExpandable==>'+isNotExpandable);
        
        // get section Div element using aura:id
        var sectionDiv = component.find(sectionAuraId).getElement();
        /* The search() method searches for 'slds-is-open' class, and returns the position of the match.
             * This method returns -1 if no match is found.
            */
        var sectionState = sectionDiv.getAttribute('class').search('slds-is-open'); 
        
        // -1 if 'slds-is-open' class is missing...then set 'slds-is-open' class else set slds-is-close class to element
        if(sectionState == -1){
            sectionDiv.setAttribute('class' , 'slds-section slds-is-open');
        }else{
            sectionDiv.setAttribute('class' , 'slds-section slds-is-close');
        }
    },
    
    AddNewSection: function(component, event, helper){
        //helper.hideSpinner(component, event, helper);
        if(component.get("v.sectionCount") == 1){
            helper.fetchMonthlyBankingForMonthList2(component, event, helper);
            component.set("v.sectionCount",2);
            component.set("v.showSection2", true);
        }
        else if(component.get("v.sectionCount") == 2){
            helper.fetchMonthlyBankingForMonthList3(component, event, helper);
            component.set("v.sectionCount",3);
            component.set("v.showSection3", true); //Added by Abhilekh for BRE2 Enhancements
        }
            else if(component.get("v.sectionCount") == 3){
                if(component.get("v.showSection2") == false) {
                    helper.fetchMonthlyBankingForMonthList2(component, event, helper);
                    component.set("v.showSection2", true);
                }
                else {
                    helper.fetchMonthlyBankingForMonthList4(component, event, helper); //Added by Abhilekh for BRE2 Enhancements
                    component.set("v.sectionCount",4); //Added by Abhilekh for BRE2 Enhancements
                    component.set("v.showSection4", true); //Added by Abhilekh for BRE2 Enhancements
                }
            }
        //Below 3 else if's block added by Abhilekh for BRE2 Enhancements
                else if(component.get("v.sectionCount") == 4){
                    if(component.get("v.showSection2") == false) {
                        helper.fetchMonthlyBankingForMonthList2(component, event, helper);
                        component.set("v.showSection2", true);
                    }
                    else if(component.get("v.showSection3") == false) {
                        helper.fetchMonthlyBankingForMonthList3(component, event, helper);
                        component.set("v.showSection3", true);
                    }
                        else{
                            component.set("v.sectionCount",5);
                            helper.fetchMonthlyBankingForMonthList5(component, event, helper);
                            component.set("v.sectionCount",5);
                            component.set("v.showSection5", true);
                        }
                }
                    else if(component.get("v.sectionCount") == 5){
                        if(component.get("v.showSection2") == false) {
                            helper.fetchMonthlyBankingForMonthList2(component, event, helper);
                            component.set("v.showSection2", true);
                        }
                        else if(component.get("v.showSection3") == false) {
                            helper.fetchMonthlyBankingForMonthList3(component, event, helper);
                            component.set("v.showSection3", true);
                        }
                            else if(component.get("v.showSection4") == false) {
                                helper.fetchMonthlyBankingForMonthList4(component, event, helper);
                                component.set("v.showSection4", true);
                            }
                                else{
                                    component.set("v.sectionCount",6);
                                    helper.fetchMonthlyBankingForMonthList6(component, event, helper);
                                }
                    }
                        else if(component.get("v.sectionCount") == 6){
                            if(component.get("v.showSection2") == false) {
                                helper.fetchMonthlyBankingForMonthList2(component, event, helper);
                                component.set("v.showSection2", true);
                            }
                            else if(component.get("v.showSection3") == false) {
                                helper.fetchMonthlyBankingForMonthList3(component, event, helper);
                                component.set("v.showSection3", true);
                            }
                                else if(component.get("v.showSection4") == false) {
                                    helper.fetchMonthlyBankingForMonthList4(component, event, helper);
                                    component.set("v.showSection4", true);
                                }
                                    else if(component.get("v.showSection5") == false) {
                                        helper.fetchMonthlyBankingForMonthList5(component, event, helper);
                                        component.set("v.showSection5", true);
                                    }
                                        else{
                                            var toastEvent = $A.get("e.force:showToast");
                                            toastEvent.setParams({
                                                "title": "",
                                                "type": "Error",
                                                "message": 'Only 6 Banking Sections can be added.'
                                            });
                                            toastEvent.fire();
                                        }
                        }
    },   
    
    Save1: function(component, event, helper){
        helper.showSpinner(component, event, helper); //Added by Abhilekh for BRE2 Enhancements
        helper.save1Helper(component, event, helper, "false");  
    },
    Edit1: function(component, event, helper){
        component.set("v.disableInput",false);
        //component.set('v.disableBankSection1',false);
        console.log('v.disableInput '+component.get("v.disableInput"));
    },    
    Save2: function(component, event, helper){
        helper.showSpinner(component, event, helper); //Added by Abhilekh for BRE2 Enhancements
        helper.save2Helper(component, event, helper, "false");          
    },
    Edit2: function(component, event, helper){
        component.set("v.disableInput2",false);
        //component.set('v.disableBankSection2',false);
    },
    Save3: function(component, event, helper){
        helper.showSpinner(component, event, helper); //Added by Abhilekh for BRE2 Enhancements
        helper.save3Helper(component, event, helper,"false");              
    }, 
    Edit3: function(component, event, helper){
        component.set("v.disableInput3",false);
        // component.set('v.disableBankSection3',false);
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    Edit4 : function(component, event, helper){
        component.set("v.disableInput4",false);
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    Save4: function(component, event, helper){
        helper.showSpinner(component, event, helper);
        helper.save4Helper(component, event, helper,"false");              
    }, 
    
    //Added by Abhilekh for BRE2 Enhancements
    Edit5 : function(component, event, helper){
        component.set("v.disableInput5",false);
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    Save5: function(component, event, helper){
        helper.showSpinner(component, event, helper);
        helper.save5Helper(component, event, helper,"false");              
    }, 
    
    //Added by Abhilekh for BRE2 Enhancements
    Edit6 : function(component, event, helper){
        component.set("v.disableInput6",false);
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    Save6: function(component, event, helper){
        helper.showSpinner(component, event, helper);
        helper.save6Helper(component, event, helper,"false");              
    }, 
    
    Spinnertoggle: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
    },    
    handleConfirmDialog : function(component, event, helper) {
        component.set('v.showConfirmDialog', true);
    },
    
    handleConfirmDialogYes : function(component, event, helper) {
        component.set('v.showConfirmDialog', false);
        
        if(component.get("v.FetchPerfiosSectionNo") =="1"){
            var disableBank1 = component.get("v.disableBank1");
            var disableBank11 = component.get("v.disableBank11");
            var startMonth = component.get("v.selectedStartMonth1"); //Added by Abhilekh for BRE2 Enhancements
            if(disableBank1) {
                var bankName = component.get("v.BankName11");
                var accountNumber = component.get("v.Accountno11");
                helper.fetchPerfiosData(component, event, helper,bankName ,accountNumber,"1", false,startMonth); //startMonth added by Abhilekh for BRE2 Enhancements
            }  else if(disableBank11) {
                var bankName = component.get("v.BankName1");
                var accountNumber = component.get("v.Accountno1");
                helper.fetchPerfiosData(component, event, helper,bankName ,accountNumber,"1", true,startMonth); //startMonth added by Abhilekh for BRE2 Enhancements
            }    
        }
        else if(component.get("v.FetchPerfiosSectionNo") =="2"){
            var disableBank1 = component.get("v.disableBank2");
            var disableBank11 = component.get("v.disableBank21");
            var startMonth = component.get("v.selectedStartMonth2"); //Added by Abhilekh for BRE2 Enhancements
            if(disableBank1) {
                var bankName = component.get("v.BankName21");
                var accountNumber = component.get("v.Accountno21");
                helper.fetchPerfiosData(component, event, helper,bankName ,accountNumber,"2", false,startMonth); //startMonth added by Abhilekh for BRE2 Enhancements
            }  else if(disableBank11) {
                var bankName = component.get("v.BankName2");
                var accountNumber = component.get("v.Accountno2");
                helper.fetchPerfiosData(component, event, helper,bankName ,accountNumber,"2", true,startMonth); //startMonth added by Abhilekh for BRE2 Enhancements
            }
        }
            else if(component.get("v.FetchPerfiosSectionNo") =="3"){
                var disableBank1 = component.get("v.disableBank3");
                var disableBank11 = component.get("v.disableBank31");
                var startMonth = component.get("v.selectedStartMonth3"); //Added by Abhilekh for BRE2 Enhancements
                if(disableBank1) {
                    var bankName = component.get("v.BankName31");
                    var accountNumber = component.get("v.Accountno31");
                    helper.fetchPerfiosData(component, event, helper,bankName ,accountNumber,"3", false,startMonth); //startMonth added by Abhilekh for BRE2 Enhancements
                }  else if(disableBank11) {
                    var bankName = component.get("v.BankName3");
                    var accountNumber = component.get("v.Accountno3");
                    helper.fetchPerfiosData(component, event, helper,bankName ,accountNumber,"3", true,startMonth); //startMonth added by Abhilekh for BRE2 Enhancements
                } 
            }
        
        //Added by Abhilekh for BRE2 Enhancements
                else if(component.get("v.FetchPerfiosSectionNo") =="4"){
                    var disableBank1 = component.get("v.disableBank4");
                    var disableBank11 = component.get("v.disableBank41");
                    var startMonth = component.get("v.selectedStartMonth4");
                    if(disableBank1) {
                        var bankName = component.get("v.BankName41");
                        var accountNumber = component.get("v.Accountno41");
                        helper.fetchPerfiosData(component, event, helper,bankName ,accountNumber,"4", false,startMonth);
                    }  else if(disableBank11) {
                        var bankName = component.get("v.BankName4");
                        var accountNumber = component.get("v.Accountno4");
                        helper.fetchPerfiosData(component, event, helper,bankName ,accountNumber,"4", true,startMonth);
                    } 
                }
        
        //Added by Abhilekh for BRE2 Enhancements
                    else if(component.get("v.FetchPerfiosSectionNo") =="5"){
                        var disableBank1 = component.get("v.disableBank5");
                        var disableBank11 = component.get("v.disableBank51");
                        var startMonth = component.get("v.selectedStartMonth5");
                        if(disableBank1) {
                            var bankName = component.get("v.BankName51");
                            var accountNumber = component.get("v.Accountno51");
                            helper.fetchPerfiosData(component, event, helper,bankName ,accountNumber,"5", false,startMonth);
                        }  else if(disableBank11) {
                            var bankName = component.get("v.BankName5");
                            var accountNumber = component.get("v.Accountno5");
                            helper.fetchPerfiosData(component, event, helper,bankName ,accountNumber,"5", true,startMonth);
                        } 
                    }
        
        //Added by Abhilekh for BRE2 Enhancements
                        else if(component.get("v.FetchPerfiosSectionNo") =="6"){
                            var disableBank1 = component.get("v.disableBank6");
                            var disableBank11 = component.get("v.disableBank61");
                            var startMonth = component.get("v.selectedStartMonth6");
                            if(disableBank1) {
                                var bankName = component.get("v.BankName61");
                                var accountNumber = component.get("v.Accountno61");
                                helper.fetchPerfiosData(component, event, helper,bankName ,accountNumber,"6", false,startMonth);
                            }  else if(disableBank11) {
                                var bankName = component.get("v.BankName6");
                                var accountNumber = component.get("v.Accountno6");
                                helper.fetchPerfiosData(component, event, helper,bankName ,accountNumber,"6", true,startMonth);
                            } 
                        }
    },
    
    handleConfirmDialogNo : function(component, event, helper) {
        console.log('No');
        component.set('v.showConfirmDialog', false);
    },
    // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
    
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },
    
    //Modified by Abhilekh for BRE2 Enhancements
    closePanel2 : function(component,event,helper){
        if(component.get("v.sectionCount") == 2){
            component.set("v.sectionCount",1)
        }
        component.set("v.showSection2", false);
        component.set("v.BankName2", '');
        component.set("v.BankName21", '');
        component.set("v.Accountno2", '');
        component.set("v.Accountno21", '');
        component.set("v.disableBank2", false);
        component.set("v.disableBank21", false);
        helper.excludeSection(component, event, helper, "2");
    },
    
    //Modified by Abhilekh for BRE2 Enhancements
    closePanel3 : function(component,event,helper){
        if(component.get("v.sectionCount") == 3){
            if(component.get("v.showSection2") == false){
                component.set("v.sectionCount",1)
            }
            else{
                component.set("v.sectionCount",2)
            }
        }
        component.set("v.showSection3", false);
        component.set("v.BankName3", '');
        component.set("v.BankName31", '');
        component.set("v.Accountno3", '');
        component.set("v.Accountno31", '');
        component.set("v.disableBank3", false);
        component.set("v.disableBank31", false);
        helper.excludeSection(component, event, helper,"3");
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    closePanel4 : function(component,event,helper){
        if(component.get("v.sectionCount") == 4){
            if(component.get("v.showSection3") == true){
                component.set("v.sectionCount",3)
            }
            else if(component.get("v.showSection2") == true){
                component.set("v.sectionCount",2)
            }
                else{
                    component.set("v.sectionCount",1)
                }
        }
        component.set("v.showSection4", false);
        component.set("v.BankName4", '');
        component.set("v.BankName41", '');
        component.set("v.Accountno4", '');
        component.set("v.Accountno41", '');
        component.set("v.disableBank4", false);
        component.set("v.disableBank41", false);
        helper.excludeSection(component, event, helper,"4");
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    closePanel5 : function(component,event,helper){
        if(component.get("v.sectionCount") == 5){
            if(component.get("v.showSection4") == true){
                component.set("v.sectionCount",4)
            }
            else if(component.get("v.showSection3") == true){
                component.set("v.sectionCount",3)
            }
                else if(component.get("v.showSection2") == true){
                    component.set("v.sectionCount",2)
                }
                    else{
                        component.set("v.sectionCount",1)
                    }
        }
        component.set("v.showSection5", false);
        component.set("v.BankName5", '');
        component.set("v.BankName51", '');
        component.set("v.Accountno5", '');
        component.set("v.Accountno51", '');
        component.set("v.disableBank5", false);
        component.set("v.disableBank51", false);
        helper.excludeSection(component, event, helper,"5");
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    closePanel6 : function(component,event,helper){
        if(component.get("v.sectionCount") == 6){
            if(component.get("v.showSection5") == true){
                component.set("v.sectionCount",5)
            }
            else if(component.get("v.showSection4") == true){
                component.set("v.sectionCount",4)
            }
                else if(component.get("v.showSection3") == true){
                    component.set("v.sectionCount",3)
                }
                    else if(component.get("v.showSection2") == true){
                        component.set("v.sectionCount",2)
                    }
                        else{
                            component.set("v.sectionCount",1)
                        }
        }
        //component.set("v.showSection3", false);
        component.set("v.BankName6", '');
        component.set("v.BankName61", '');
        component.set("v.Accountno6", '');
        component.set("v.Accountno61", '');
        component.set("v.disableBank6", false);
        component.set("v.disableBank61", false);
        helper.excludeSection(component, event, helper,"6");
    },
    
    closeMe : function(component, event, helper){
        if(component.get("v.disableInput") == true && 
           component.get("v.disableInput2") == true && 
           component.get("v.disableInput3") == true && 
           component.get("v.disableInput4") == true && 
           component.get("v.disableInput5") == true && 
           component.get("v.disableInput6") == true) { //Modified by Abhilekh for BRE2 Enhancements
            helper.closeMe(component, event, helper);        
        }  else {   
            component.set("v.ConfirmClose", true);
        }    
        
    },
    handleConfirmDialogboxYes : function(component, event, helper) {
        helper.closeMe(component, event, helper);
    },
    handleConfirmDialogboxNo : function(component, event, helper) {
        component.set("v.ConfirmClose", false);        
    },
    onCheck : function(component, event, helper) {
        var val1 = component.find("isChecked").get("v.value");
        alert('is verified js contrllr '+val1);
        if(val1==false || val1 == undefined || val1 == null)
            component.set("v.isverified", true);
        else
            component.set("v.isverified", false);
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    /*changeStartMonth1 : function(component, event, helper) {
        var action = component.get("c.changeStartingMonth");
        
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'bnkname' : component.get("v.BankName"),
            'accountNo': component.get("v.Accountno"),
            'getBRERecord' : true,
            'perfiosBankDetails' : true,
            'newStartMonth' : component.get("v.selectedStartMonth1")
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {   
                component.set("v.MonthList", {});
                var storeResponse = response.getReturnValue();   
                console.log('--obj values--'+ JSON.stringify(response.getReturnValue()));                
                component.set("v.MonthList1", storeResponse);
            }
        });
        $A.enqueueAction(action);
    },*/
    
    //Added by Abhilekh for BRE2 Enhancements
    changeStartMonth1 : function(component, event, helper) {
        var disableBank1 = component.get("v.disableBank1");
        var disableBank11 = component.get("v.disableBank11");
        //var perfiosButtonClicked = component.get("v.perfiosButtonClicked");
        
        if(disableBank1){
            var bankName = component.get("v.BankName11");
            var accountNumber = component.get("v.Accountno11");
        }
        else if(disableBank11){
            var bankName = component.get("v.BankName1");
            var accountNumber = component.get("v.Accountno1");
        }
        
        var startMonth = component.get("v.selectedStartMonth1");
        
        var action = component.get("c.changeStartingMonth");
        
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'bnkname' : component.get("v.BankName"),
            'accountNo': component.get("v.Accountno"),
            'getBRERecord' : true,
            'perfiosBankDetails' : disableBank11,
            'newStartMonth' : component.get("v.selectedStartMonth1")
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {   
                component.set("v.MonthList", {});
                var storeResponse = response.getReturnValue();                
                component.set("v.MonthList1", storeResponse);
                
                if(bankName != null && bankName != '' && bankName != undefined && accountNumber != null && accountNumber != '' && accountNumber != undefined){
                    helper.fetchBRERecords(component, event, helper,bankName ,accountNumber,"1", disableBank11,startMonth);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    changeStartMonth2 : function(component, event, helper) {
        var disableBank2 = component.get("v.disableBank2");
        var disableBank21 = component.get("v.disableBank21");
        //var perfiosButtonClicked = component.get("v.perfiosButtonClicked");
        
        if(disableBank2){
            var bankName = component.get("v.BankName21");
            var accountNumber = component.get("v.Accountno21");
        }
        else if(disableBank21){
            var bankName = component.get("v.BankName2");
            var accountNumber = component.get("v.Accountno2");
        }
        
        var startMonth = component.get("v.selectedStartMonth2");
        
        var action = component.get("c.changeStartingMonth");
        
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'bnkname' : component.get("v.BankName"),
            'accountNo': component.get("v.Accountno"),
            'getBRERecord' : true,
            'perfiosBankDetails' : disableBank21,
            'newStartMonth' : component.get("v.selectedStartMonth2")
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {   
                component.set("v.MonthList2", {});
                var storeResponse = response.getReturnValue();                
                component.set("v.MonthList2", storeResponse);
                
                if(bankName != null && bankName != '' && bankName != undefined && accountNumber != null && accountNumber != '' && accountNumber != undefined){
                    helper.fetchBRERecords(component, event, helper,bankName ,accountNumber,"2", disableBank21,startMonth);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    changeStartMonth3 : function(component, event, helper) {
        var disableBank3 = component.get("v.disableBank3");
        var disableBank31 = component.get("v.disableBank31");
        //var perfiosButtonClicked = component.get("v.perfiosButtonClicked");
        
        if(disableBank3){
            var bankName = component.get("v.BankName31");
            var accountNumber = component.get("v.Accountno31");
        }
        else if(disableBank31){
            var bankName = component.get("v.BankName3");
            var accountNumber = component.get("v.Accountno3");
        }
        
        var startMonth = component.get("v.selectedStartMonth3");
        
        var action = component.get("c.changeStartingMonth");
        
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'bnkname' : component.get("v.BankName"),
            'accountNo': component.get("v.Accountno"),
            'getBRERecord' : true,
            'perfiosBankDetails' : disableBank31,
            'newStartMonth' : component.get("v.selectedStartMonth3")
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {   
                component.set("v.MonthList3", {});
                var storeResponse = response.getReturnValue();                
                component.set("v.MonthList3", storeResponse);
                
                if(bankName != null && bankName != '' && bankName != undefined && accountNumber != null && accountNumber != '' && accountNumber != undefined){
                    helper.fetchBRERecords(component, event, helper,bankName ,accountNumber,"3", disableBank31,startMonth);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    changeStartMonth4 : function(component, event, helper) {
        var disableBank4 = component.get("v.disableBank4");
        var disableBank41 = component.get("v.disableBank41");
        //var perfiosButtonClicked = component.get("v.perfiosButtonClicked");
        
        if(disableBank4){
            var bankName = component.get("v.BankName41");
            var accountNumber = component.get("v.Accountno41");
        }
        else if(disableBank41){
            var bankName = component.get("v.BankName4");
            var accountNumber = component.get("v.Accountno4");
        }
        
        var startMonth = component.get("v.selectedStartMonth4");
        
        var action = component.get("c.changeStartingMonth");
        
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'bnkname' : component.get("v.BankName"),
            'accountNo': component.get("v.Accountno"),
            'getBRERecord' : true,
            'perfiosBankDetails' : disableBank41,
            'newStartMonth' : component.get("v.selectedStartMonth4")
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {   
                component.set("v.MonthList4", {});
                var storeResponse = response.getReturnValue();                
                component.set("v.MonthList4", storeResponse);
                
                if(bankName != null && bankName != '' && bankName != undefined && accountNumber != null && accountNumber != '' && accountNumber != undefined){
                    helper.fetchBRERecords(component, event, helper,bankName ,accountNumber,"4", disableBank41,startMonth);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    changeStartMonth5 : function(component, event, helper) {
        var disableBank5 = component.get("v.disableBank5");
        var disableBank51 = component.get("v.disableBank51");
        //var perfiosButtonClicked = component.get("v.perfiosButtonClicked");
        
        if(disableBank5){
            var bankName = component.get("v.BankName51");
            var accountNumber = component.get("v.Accountno51");
        }
        else if(disableBank51){
            var bankName = component.get("v.BankName5");
            var accountNumber = component.get("v.Accountno5");
        }
        
        var startMonth = component.get("v.selectedStartMonth5");
        
        var action = component.get("c.changeStartingMonth");
        
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'bnkname' : component.get("v.BankName"),
            'accountNo': component.get("v.Accountno"),
            'getBRERecord' : true,
            'perfiosBankDetails' : disableBank51,
            'newStartMonth' : component.get("v.selectedStartMonth5")
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {   
                component.set("v.MonthList5", {});
                var storeResponse = response.getReturnValue();                
                component.set("v.MonthList5", storeResponse);
                
                if(bankName != null && bankName != '' && bankName != undefined && accountNumber != null && accountNumber != '' && accountNumber != undefined){
                    helper.fetchBRERecords(component, event, helper,bankName ,accountNumber,"5", disableBank51,startMonth);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    changeStartMonth6 : function(component, event, helper) {
        var disableBank6 = component.get("v.disableBank6");
        var disableBank61 = component.get("v.disableBank61");
        //var perfiosButtonClicked = component.get("v.perfiosButtonClicked");
        
        if(disableBank6){
            var bankName = component.get("v.BankName61");
            var accountNumber = component.get("v.Accountno61");
        }
        else if(disableBank61){
            var bankName = component.get("v.BankName6");
            var accountNumber = component.get("v.Accountno6");
        }
        
        var startMonth = component.get("v.selectedStartMonth6");
        
        var action = component.get("c.changeStartingMonth");
        
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'bnkname' : component.get("v.BankName"),
            'accountNo': component.get("v.Accountno"),
            'getBRERecord' : true,
            'perfiosBankDetails' : disableBank61,
            'newStartMonth' : component.get("v.selectedStartMonth6")
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {   
                component.set("v.MonthList6", {});
                var storeResponse = response.getReturnValue();                
                component.set("v.MonthList6", storeResponse);
                
                if(bankName != null && bankName != '' && bankName != undefined && accountNumber != null && accountNumber != '' && accountNumber != undefined){
                    helper.fetchBRERecords(component, event, helper,bankName ,accountNumber,"6", disableBank61,startMonth);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
    
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    }
})