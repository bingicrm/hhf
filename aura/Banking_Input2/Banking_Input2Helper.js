({
    //init
    getMonthlyBankingList : function(component,event) {
        var recordid = component.get("v.custRecordId");
        //Get customer detail 
        var action1 = component.get("c.getCustDetailInfo");
        action1.setParams({
            'CDrecID' : component.get("v.custRecordId"),            
        })
        action1.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.CustomerDetail", response.getReturnValue());               
                var status = component.get("v.CustomerDetail.Banking_Status__c");
                if(status == 'Completed') {
                    //var isverified = component.find("isChecked").get("v.value");
                    component.set("v.isverified",true);
                } else {
                    component.set("v.isverified",false); 
                }
            }
        });
        $A.enqueueAction(action1);
        
        //Get List of Month
        var action11 = component.get("c.getPerfiosbankMasterDetail");
        action11.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'getBankNames' : true 
        })
        action11.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var x1 = response.getReturnValue();
                if(x1 != null) {
                    var optionsList1 = [];    
                    for (var key1 in x1) {
                        optionsList1.push({value: key1, label: x1[key1]});
                    };
                    component.set("v.bankNameList", optionsList1);
                } else {
                    component.set("v.disableBank1" , true);
                    //component.set("v.disableBank2" , true);
                    //component.set("v.disableBank3" , true);
                    //component.set("v.disableBank4" , true); //Added by Abhilekh for BRE2 Enhacements
                    //component.set("v.disableBank5" , true); //Added by Abhilekh for BRE2 Enhacements
                    //component.set("v.disableBank6" , true); //Added by Abhilekh for BRE2 Enhacements
                }
            }
        });
        $A.enqueueAction(action11);
        
        var action12 = component.get("c.getPerfiosbankMasterDetail");
        action12.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'getBankNames' : false 
        })
        action12.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var x1 = response.getReturnValue();  
                var optionsList1 = [];    
                for (var key1 in x1) {
                    optionsList1.push({value: key1, label: x1[key1]});
                };
                component.set("v.accountNumberList", optionsList1);
            }
        });
        $A.enqueueAction(action12); 
        //}
        //ADDED BY SHARAD for account number fetch with bank name

        //Get List of Month        
        var action = component.get("c.fetchMonthlyBanking");
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'bnkname' : component.get("v.BankName"),
            'accountNo': component.get("v.Accountno"),
            'getBRERecord' : true,
            'perfiosBankDetails' : true,
            'startMonth' : ''
        }) //startMonth added by Abhilekh for BRE2 Enhancements
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {   
                component.set("v.MonthList", {});
                var storeResponse = response.getReturnValue();                
                component.set("v.MonthList1", response.getReturnValue());// set list 1
                component.set("v.MonthList2", storeResponse);// set default list   
                component.set("v.MonthList3", storeResponse);
                component.set("v.MonthList4", storeResponse); //Added by Abhilekh for BRE2 Enhacements
                component.set("v.MonthList5", storeResponse); //Added by Abhilekh for BRE2 Enhacements
                component.set("v.MonthList6", storeResponse); //Added by Abhilekh for BRE2 Enhacements
            }
        });
        $A.enqueueAction(action); 
        
        this.fetchBRERecordsUpdated(component,helper,event);
        this.getStartingMonthsList(component,helper,event); //Added by Abhilekh for BRE2 Enhancements
        this.getSubStageAndProfile(component,helper,event); //Added by Abhilekh for BRE2 Enhancements
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    getSubStageAndProfile : function(component,helper,event){
        var action = component.get("c.getSubStageAndProfile");
        
        action.setParams({
            'CDrecID' : component.get("v.custRecordId")
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS") {
                var res = response.getReturnValue();
                component.set("v.substage",res.substage);
                
                if(res.profilename == 'Credit Team' && (res.substage == 'Credit Review' || res.substage == 'Re Credit' || res.substage == 'Re-Look')){
                    component.set("v.disableBankConsidered",false);
                    component.set("v.disableForBCM",true);
                }
                else if((res.profilename == 'Financial Data Entry Maker' || res.profilename == 'Data Entry Team') && res.substage == 'COPS: Credit Operations Entry'){
                    component.set("v.disableBankConsidered",true);
                    component.set("v.disableForBCM",false);
                }
                else{
                    component.set("v.disableBankConsidered",true);
                    component.set("v.disableForBCM",true);
                }
            }
            else{
                console.log('An error occurred.');
            }
        });
        $A.enqueueAction(action);
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    getStartingMonthsList : function(component,helper,event){        
        var action = component.get("c.getStartingMonthsList");
    	   
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
            	var storeResponse = response.getReturnValue();
                component.set("v.lstStartMonths",storeResponse);
            }
            else{
                console.log('Error fetching starting months list.');
            }
        });
        $A.enqueueAction(action);
    },
    
    //Added by Abhilekh for BRE2
    fetchMonthlyBankingForMonthList2 : function(component,helper,event){
        var action = component.get("c.fetchMonthlyBanking");
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'bnkname' : component.get("v.BankName"),
            'accountNo': component.get("v.Accountno"),
            'getBRERecord' : true,
            'perfiosBankDetails' : true,
            'startMonth' : component.get("v.selectedStartMonth2")
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {   
                component.set("v.MonthList", {});          
                component.set("v.MonthList2", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    //Added by Abhilekh for BRE2
    fetchMonthlyBankingForMonthList3 : function(component,helper,event){
        var action = component.get("c.fetchMonthlyBanking");
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'bnkname' : component.get("v.BankName"),
            'accountNo': component.get("v.Accountno"),
            'getBRERecord' : true,
            'perfiosBankDetails' : true,
            'startMonth' : component.get("v.selectedStartMonth3")
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {   
                component.set("v.MonthList", {});          
                component.set("v.MonthList3", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    //Added by Abhilekh for BRE2
    fetchMonthlyBankingForMonthList4 : function(component,helper,event){
        var action = component.get("c.fetchMonthlyBanking");
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'bnkname' : component.get("v.BankName"),
            'accountNo': component.get("v.Accountno"),
            'getBRERecord' : true,
            'perfiosBankDetails' : true,
            'startMonth' : component.get("v.selectedStartMonth4")
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {   
                component.set("v.MonthList", {});          
                component.set("v.MonthList4", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    //Added by Abhilekh for BRE2
    fetchMonthlyBankingForMonthList5 : function(component,helper,event){
        var action = component.get("c.fetchMonthlyBanking");
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'bnkname' : component.get("v.BankName"),
            'accountNo': component.get("v.Accountno"),
            'getBRERecord' : true,
            'perfiosBankDetails' : true,
            'startMonth' : component.get("v.selectedStartMonth5")
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {   
                component.set("v.MonthList", {});          
                component.set("v.MonthList5", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    //Added by Abhilekh for BRE2
    fetchMonthlyBankingForMonthList6 : function(component,helper,event){
        var action = component.get("c.fetchMonthlyBanking");
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'bnkname' : component.get("v.BankName"),
            'accountNo': component.get("v.Accountno"),
            'getBRERecord' : true,
            'perfiosBankDetails' : true,
            'startMonth' : component.get("v.selectedStartMonth6")
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {   
                component.set("v.MonthList", {});          
                component.set("v.MonthList6", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    docStatusPickListVal: function(component,helper,event) {
        var action = component.get("c.getselectOptions");
       
        action.setParams({
            "objObject": component.get("v.objInfoForPicklistValues"),
            "fld": 'Banking_Account_Type__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.docStatus", allValues);
            }
        });
        $A.enqueueAction(action);
    },
    
    savePanelHelper: function(component, event, helper, bankName, accountNumber, perfiosBankDetails, abc) {
        var isChecked = component.find("isChecked").get("v.value");

        var action1 = component.get("c.savePanel1");
        action1.setParams({
            'isChecked' : isChecked, 
            'CDrecID' : component.get("v.custRecordId") ,
             completed: true
        })
        $A.enqueueAction(action1);
    }, 
    doSave: function(component, event, helper, bankName ,accountNumber,accountType,sectionno, perfiosBankDetails, perfiosButtonClicked, abc,limitAmount,bankConsidered,startMonth){ //limitAmount,bankConsidered,startMonth added by Abhilekh for BRE2 Enhancements
        var list;
        if(sectionno == "1"){
            list = component.get("v.MonthList1");
        }else if(sectionno == "2"){
            list = component.get("v.MonthList2");
        }else if(sectionno == "3"){
            list = component.get("v.MonthList3");
        }
        
        //Added by Abhilekh for BRE2 Enhancements
        else if(sectionno == "4"){
            list = component.get("v.MonthList4");
        }
        
        //Added by Abhilekh for BRE2 Enhancements
        else if(sectionno == "5"){
            list = component.get("v.MonthList5");
        }
        
        //Added by Abhilekh for BRE2 Enhancements
        else if(sectionno == "6"){
            list = component.get("v.MonthList6");
        }
        
        var validate = true; //Added by Abhilekh for BRE2 Enhancements
        
        //Below for loop added by Abhilekh for BRE2 Enhancements
        for(var i=0; i<list.length; i++){
            if(list[i].inoutrec.totalLoanDisbursal__c === ""){
                list[i].inoutrec.totalLoanDisbursal__c = null;
            }
            if(list[i].inoutrec.Balance_on_5__c === ""){
                list[i].inoutrec.Balance_on_5__c = null;
            }
            if(list[i].inoutrec.Total_Inflow_Count__c === ""){
                list[i].inoutrec.Total_Inflow_Count__c = null;
            }
            if(list[i].inoutrec.Balance_on_25__c === ""){
                list[i].inoutrec.Balance_on_25__c = null;
            }
            if(list[i].inoutrec.Total_Inflow_Value__c === ""){
                list[i].inoutrec.Total_Inflow_Value__c = null;
            }
            if(list[i].inoutrec.Total_Outflow_Value__c === ""){
                list[i].inoutrec.Total_Outflow_Value__c = null;
            }
            if(list[i].inoutrec.Total_Outflow_Count__c === ""){
                list[i].inoutrec.Total_Outflow_Count__c = null;
            }
            if(list[i].inoutrec.Balance_on_15__c === ""){
                list[i].inoutrec.Balance_on_15__c = null;
            }
            if(list[i].inoutrec.inwChqBounces__c === ""){
                list[i].inoutrec.inwChqBounces__c = null;
            }
            if(list[i].inoutrec.outwChqBounces__c === ""){
                list[i].inoutrec.outwChqBounces__c = null;
            }
            if(list[i].inoutrec.Total_Outflow_Count__c < 0){
                validate = false;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": "Negative value found for DEBIT# in section "+sectionno
                });
                toastEvent.fire();
                this.hideSpinner(component,event,helper);
            }
            if(list[i].inoutrec.Total_Inflow_Count__c < 0){
                validate = false;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": "Negative value found for CREDIT# in section "+sectionno
                });
                toastEvent.fire();
                this.hideSpinner(component,event,helper);
            }
            if(list[i].inoutrec.inwChqBounces__c < 0){
                validate = false;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": "Negative value found for I/W Bounce# in section "+sectionno 
                });
                toastEvent.fire();
                this.hideSpinner(component,event,helper);
            }
            if(list[i].inoutrec.outwChqBounces__c < 0){
                validate = false;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": "Negative value found for O/W Bounce# in section "+sectionno 
                });
                toastEvent.fire();
                this.hideSpinner(component,event,helper);
            }
        }
		        
        if(validate){
            var action = component.get("c.SaveMonthlyInOutRecord");
            action.setParams({
                'wrappedObjList1' : JSON.stringify(list),
                'CDrecID':component.get("v.custRecordId"),
                'bnkname' : bankName,
                'accountNo': accountNumber,
                'acctType' : accountType,
                'SavePartially': component.find("isChecked").get("v.value")==true ? false : true,
                'perfiosBankDetails' : perfiosBankDetails,
                'perfiosButtonClicked' : perfiosButtonClicked,
                'rowsToVerify' : abc,
                'sectionno' : sectionno,
                'limitAmount' : limitAmount,
                'bankConsidered' : bankConsidered,
                'startMonth' : startMonth
            }); //limitAmount,bankConsidered,startMonth added by Abhilekh for BRE2 Enhancements
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") { 
                    if (response.getReturnValue().includes("SUCCESS")) {
                        this.fetchBRERecordsUpdatedSectionWise(component,helper,event,sectionno); //Added by Abhilekh for BRE2 Enhancements
                        
                        var abb = response.getReturnValue().split("-");
                        if(sectionno == "1"){
                            component.set("v.disableInput",true);
                            component.set("v.averageABB1",abb[1]);
                        }else if(sectionno == "2"){
                            component.set("v.disableInput2",true);
                            component.set("v.averageABB2",abb[1]);
                        }else if(sectionno == "3"){
                            component.set("v.disableInput3",true);
                            component.set("v.averageABB3",abb[1]);
                        }
                        
                        //Added by Abhilekh for BRE2 Enhancements
                            else if(sectionno == "4"){
                                component.set("v.disableInput4",true);
                                component.set("v.averageABB4",abb[1]);
                            }
                        
                        //Added by Abhilekh for BRE2 Enhancements
                                else if(sectionno == "5"){
                                    component.set("v.disableInput5",true);
                                    component.set("v.averageABB5",abb[1]);
                                }
                        
                        //Added by Abhilekh for BRE2 Enhancements
                                    else if(sectionno == "6"){
                                        component.set("v.disableInput6",true);
                                        component.set("v.averageABB6",abb[1]);
                                    }
                        
                        if(component.get("v.disableInput") == true && (component.get("v.disableInput2") == true || (component.get("v.disableBank2") == false && component.get("v.disableBank21") == false)) 
                           && (component.get("v.disableInput3") == true || (component.get("v.disableBank3") == false && component.get("v.disableBank31") == false)) 
                           && (component.get("v.disableInput4") == true || (component.get("v.disableBank4") == false && component.get("v.disableBank41") == false)) 
                           && (component.get("v.disableInput5") == true || (component.get("v.disableBank5") == false && component.get("v.disableBank51") == false)) 
                           && (component.get("v.disableInput6") == true || (component.get("v.disableBank6") == false && component.get("v.disableBank61") == false))) { //Condition related to section 4,5,and 6 added by Abhilekh for BRE2 Enhancements
                            this.savePanelHelper(component, event, helper);    
                        } else {
                            var isChecked = component.find("isChecked").get("v.value");
                            
                            var action1 = component.get("c.savePanel1");
                            action1.setParams({
                                'isChecked' : isChecked, 
                                'CDrecID' : component.get("v.custRecordId") ,
                                completed: false
                            })
                            $A.enqueueAction(action1);
                        }                    
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "",
                            "type": "success",
                            "message": "successfully saved section "+sectionno
                        });
                        toastEvent.fire();
                    }else if (response.getReturnValue() === "MISSING_FIELDS") {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "",
                            "type": "Error",
                            "message": "Please complete missing information in section "+sectionno 
                        });
                        toastEvent.fire();
                    }else if (response.getReturnValue() === "BANKNAME_NOT_FOUND") {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "",
                            "type": "Error",
                            "message": "Bank Name not found in section "+sectionno
                        });
                        toastEvent.fire();
                    }
                    
                    //Below else if block added by Abhilekh for BRE2 Enhancements
                        else if(response.getReturnValue() === "BALANCE_AMOUNT_GREATER"){
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "",
                                "type": "Error",
                                "message": "The amount is greater than previous amount in 5th Bal, 15th Bal and 25th Bal in section "+sectionno
                            });
                            toastEvent.fire();
                        }
                    
                            else{
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "",
                                    "type": "Error",
                                    "message": "Unknown error occurred while saving section "+sectionno+", please contact Admin!!"
                                });
                                toastEvent.fire();
                            }
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    fetchBRERecords: function(component, event, helper, bankName, accountNumber, sectionno, perfiosBankDetails,startMonth){ //startMonth added by Abhilekh for BRE2 Enhancements
        var recordid = component.get("v.custRecordId");
        
        var action = component.get("c.fetchMonthlyBanking");
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"), 
            'bnkname' : bankName ,
            'accountNo': accountNumber,
            'getBRERecord' :true,
            'perfiosBankDetails' : perfiosBankDetails,
            'startMonth' : startMonth
        }) //startMonth added by Abhilekh for BRE2 Enhancements
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.MonthList", {});
                var storeResponse = response.getReturnValue();//response.getReturnValue()
                if(sectionno == "1"){
                    component.set("v.perfiosButtonClicked" ,false);
                    component.set("v.MonthList1", response.getReturnValue());  
                }else if(sectionno == "2"){
                    component.set("v.perfiosButtonClicked2" ,false);
                    component.set("v.MonthList2", response.getReturnValue());                  
                }else if(sectionno == "3"){
                    component.set("v.perfiosButtonClicked3" ,false);
                    component.set("v.MonthList3", response.getReturnValue());                  
                }
                
                //Added by Abhilekh for BRE2 Enhancements
                else if(sectionno == "4"){
                    component.set("v.perfiosButtonClicked4" ,false);
                    component.set("v.MonthList4", response.getReturnValue());                  
                }
                
                //Added by Abhilekh for BRE2 Enhancements
                else if(sectionno == "5"){
                    component.set("v.perfiosButtonClicked5" ,false);
                    component.set("v.MonthList5", response.getReturnValue());                  
                }
                
                //Added by Abhilekh for BRE2 Enhancements
                else if(sectionno == "6"){
                    component.set("v.perfiosButtonClicked6" ,false);
                    component.set("v.MonthList6", response.getReturnValue());                  
                }
                //this.hideSpinner(component, event, helper);
            }
        });
        $A.enqueueAction(action);         
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    fetchBRERecordsUpdatedSectionWise: function(component, event, helper,sectionno){
        var action = component.get("c.fetchMonthlyBankingUpdatedsSectionWise");
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"),
            'sectionNo' : sectionno
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {   
                component.set("v.MonthList", {});
                var storeResponse = response.getReturnValue();
                
                if(storeResponse.MonthList != null && storeResponse.MonthList != '' && sectionno === '1'){
                    component.set("v.perfiosButtonClicked" ,false);
                    component.set("v.MonthList1", storeResponse.MonthList);
                    component.set("v.averageABB1", storeResponse.averageABB);
                    
                    if(storeResponse.limitAmount != null){
                        component.set("v.limitAmount1", storeResponse.limitAmount);
                    }
                    if(storeResponse.stMth != null){
                        component.set("v.selectedStartMonth1", storeResponse.stMth);
                    }
                	component.set("v.bankConsidered1",storeResponse.bankConsidered);
                    
                    if(storeResponse.pdcmonthList != null && storeResponse.pdcmonthList != ''){
                        component.set("v.disableBank1" ,true);
                        component.set("v.isaccountType11" ,true);
                        component.set("v.BankName11" ,storeResponse.pdcmonthList);
                        component.set("v.Accountno11" ,storeResponse.accountMonthList);
                        component.set("v.AccountType11" ,storeResponse.accountTypeList);
                    }
                    else{
                        if(storeResponse.perfiosBankName != null && storeResponse.perfiosBankName != ''){
                            component.set("v.disableBank11" ,true);
                            component.set("v.isaccountType1" ,true);
                            component.set("v.BankName1",storeResponse.perfiosBankName);
                            component.set("v.Accountno1",storeResponse.perfiosAccount);
                            component.set("v.AccountType1",storeResponse.perfiosAccountType);
                            component.set("v.showOtherPicklist",true);
                        }
                    }
                }
                
                if(storeResponse.MonthList != null && storeResponse.MonthList != '' && sectionno === '2'){
                    component.set("v.showSection2",storeResponse.showSection2);
                    component.set("v.sectionCount", 2)
                    component.set("v.perfiosButtonClicked2" ,false);
                    component.set("v.MonthList2", storeResponse.MonthList);
                    component.set("v.averageABB2", storeResponse.averageABB);
                    
                    if(storeResponse.limitAmount != null){
                        component.set("v.limitAmount2", storeResponse.limitAmount);
                    }
                    if(storeResponse.stMth != null){
                        component.set("v.selectedStartMonth2", storeResponse.stMth);
                    }
                	component.set("v.bankConsidered2",storeResponse.bankConsidered);
                    
                    if(storeResponse.pdcmonthList != null && storeResponse.pdcmonthList != ''){
                        component.set("v.disableBank2" ,true);
                        component.set("v.isaccountType21" ,true);
                        component.set("v.BankName21" ,storeResponse.pdcmonthList);
                        component.set("v.Accountno21" ,storeResponse.accountMonthList);
                        component.set("v.AccountType21" ,storeResponse.accountTypeList);
                    }                     
                    else{
                        if(storeResponse.perfiosBankName != null && storeResponse.perfiosBankName != ''){
                            component.set("v.disableBank21" ,true);
                            component.set("v.isaccountType2" ,true);
                            component.set("v.BankName2" ,storeResponse.perfiosBankName);
                            component.set("v.Accountno2" ,storeResponse.perfiosAccount);
                            component.set("v.AccountType2" ,storeResponse.perfiosAccountType);
                        }
                    } 
                }
                if(storeResponse.MonthList != null && storeResponse.MonthList != '' && sectionno === '3'){
                    component.set("v.showSection3",storeResponse.showSection3);
                    component.set("v.sectionCount",3);
                    component.set("v.perfiosButtonClicked3" ,false);
                    component.set("v.MonthList3", storeResponse.MonthList);
                    component.set("v.averageABB3", storeResponse.averageABB);
                    
                    if(storeResponse.limitAmount != null){
                        component.set("v.limitAmount3",storeResponse.limitAmount);
                    }
                    if(storeResponse.stMth != null){
                        component.set("v.selectedStartMonth3", storeResponse.stMth);
                    }
                	component.set("v.bankConsidered3",storeResponse.bankConsidered);
                    
                    if(storeResponse.pdcmonthList != null && storeResponse.pdcmonthList != ''){
                        component.set("v.disableBank3" ,true);
                        component.set("v.isaccountType31" ,true);
                        component.set("v.BankName31" ,storeResponse.pdcmonthList);
                        component.set("v.Accountno31" ,storeResponse.accountMonthList);
                        component.set("v.AccountType31" ,storeResponse.accountTypeList);
                    }                     
                    else{
                        if(storeResponse.perfiosBankName != null && storeResponse.perfiosBankName != ''){
                            component.set("v.disableBank31" ,true);
                            component.set("v.isaccountType3" ,true);
                            component.set("v.BankName3" ,storeResponse.perfiosBankName);
                            component.set("v.Accountno3" ,storeResponse.perfiosAccount);
                            component.set("v.AccountType3" ,storeResponse.perfiosAccountType);
                        }
                    }
                }
                if(storeResponse.MonthList != null && storeResponse.MonthList != '' && sectionno === '4'){
                    component.set("v.showSection4",storeResponse.showSection4);
                    component.set("v.sectionCount",4);
                    component.set("v.perfiosButtonClicked4" ,false);
                    component.set("v.MonthList4", storeResponse.MonthList);
                    component.set("v.averageABB4", storeResponse.averageABB);
                    
                    if(storeResponse.limitAmount != null){
                        component.set("v.limitAmount4", storeResponse.limitAmount);
                    }
                    if(storeResponse.stMth != null){
                        component.set("v.selectedStartMonth4", storeResponse.stMth);
                    }
                	component.set("v.bankConsidered4",storeResponse.bankConsidered);
                    
                    if(storeResponse.pdcmonthList != null && storeResponse.pdcmonthList != ''){
                        component.set("v.disableBank4" ,true);
                        component.set("v.isaccountType41" ,true);
                        component.set("v.BankName41" ,storeResponse.pdcmonthList);
                        component.set("v.Accountno41" ,storeResponse.accountMonthList);
                        component.set("v.AccountType41" ,storeResponse.accountTypeList);
                    }                     
                    else{
                        if(storeResponse.perfiosBankName != null && storeResponse.perfiosBankName != ''){
                            component.set("v.disableBank41" ,true);
                            component.set("v.isaccountType4" ,true);
                            component.set("v.BankName4" ,storeResponse.perfiosBankName);
                            component.set("v.Accountno4" ,storeResponse.perfiosAccount);
                            component.set("v.AccountType4" ,storeResponse.perfiosAccountType);
                        }
                    }
                }
                if(storeResponse.MonthList != null && storeResponse.MonthList != '' && sectionno === '5'){
                    component.set("v.showSection5",storeResponse.showSection5);
                    component.set("v.sectionCount",5);
                    component.set("v.perfiosButtonClicked5" ,false);
                    component.set("v.MonthList5", storeResponse.MonthList);
                    component.set("v.averageABB5", storeResponse.averageABB);
                    
                    if(storeResponse.limitAmount != null){
                        component.set("v.limitAmount5", storeResponse.limitAmount);
                    }
                    if(storeResponse.stMth != null){
                        component.set("v.selectedStartMonth5", storeResponse.stMth);
                    }
                	component.set("v.bankConsidered5",storeResponse.bankConsidered);
                    
                    if(storeResponse.pdcmonthList != null && storeResponse.pdcmonthList != ''){
                        component.set("v.disableBank5" ,true);
                        component.set("v.isaccountType51" ,true);
                        component.set("v.BankName51" ,storeResponse.pdcmonthList);
                        component.set("v.Accountno51" ,storeResponse.accountMonthList);
                        component.set("v.AccountType51" ,storeResponse.accountTypeList);
                    }                     
                    else{
                        if(storeResponse.perfiosBankName != null && storeResponse.perfiosBankName != ''){
                            component.set("v.disableBank51" ,true);
                            component.set("v.isaccountType5" ,true);
                            component.set("v.BankName5" ,storeResponse.perfiosBankName);
                            component.set("v.Accountno5" ,storeResponse.perfiosAccount);
                            component.set("v.AccountType5" ,storeResponse.perfiosAccountType);
                        }
                    }
                }
                if(storeResponse.MonthList != null && storeResponse.MonthList != '' && sectionno === '6'){
                    component.set("v.sectionCount",6);
                    component.set("v.perfiosButtonClicked6" ,false);
                    component.set("v.MonthList6", storeResponse.MonthList);
                    component.set("v.averageABB6", storeResponse.averageABB);
                    
                    if(storeResponse.limitAmount != null){
                        component.set("v.limitAmount6", storeResponse.limitAmount);
                    }
                    if(storeResponse.stMth != null){
                        component.set("v.selectedStartMonth6", storeResponse.stMth);
                    }
                	component.set("v.bankConsidered6",storeResponse.bankConsidered);
                    
                    if(storeResponse.pdcmonthList != null && storeResponse.pdcmonthList != ''){
                        component.set("v.disableBank6" ,true);
                        component.set("v.isaccountType61" ,true);
                        component.set("v.BankName61" ,storeResponse.pdcmonthList);
                        component.set("v.Accountno61" ,storeResponse.accountMonthList);
                        component.set("v.AccountType61" ,storeResponse.accountTypeList);
                    }                     
                    else{
                        if(storeResponse.perfiosBankName != null && storeResponse.perfiosBankName != ''){
                            component.set("v.disableBank61" ,true);
                            component.set("v.isaccountType6" ,true);
                            component.set("v.BankName6" ,storeResponse.perfiosBankName);
                            component.set("v.Accountno6" ,storeResponse.perfiosAccount);
                            component.set("v.AccountType6" ,storeResponse.perfiosAccountType);
                        }
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    fetchBRERecordsUpdated: function(component, event, helper, bankName, accountNumber, accountType,sectionno, perfiosBankDetails){
        var recordid = component.get("v.custRecordId");
        
        var action = component.get("c.fetchMonthlyBankingUpdateds");
        action.setParams({
            'CDrecID' : component.get("v.custRecordId")
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.MonthList", {});
                var storeResponse = response.getReturnValue();
                
                if(response.getReturnValue()[0].MonthList1 != null && response.getReturnValue()[0].MonthList1 != ''){
                    component.set("v.perfiosButtonClicked" ,false);
                    component.set("v.MonthList1", response.getReturnValue()[0].MonthList1);
                    component.set("v.averageABB1", response.getReturnValue()[0].averageABB1);
                    
                    //Added by Abhilekh for BRE2 Enhancements
                    if(response.getReturnValue()[0].limitAmount1 != null){
                        component.set("v.limitAmount1", response.getReturnValue()[0].limitAmount1);
                    }
                    //Added by Abhilekh for BRE2 Enhancements
                    if(response.getReturnValue()[0].stMth1 != null){
                        component.set("v.selectedStartMonth1", response.getReturnValue()[0].stMth1);
                    }
                	component.set("v.bankConsidered1",response.getReturnValue()[0].bankConsidered1); //Added by Abhilekh for BRE2 Enhancements
                    
                    if(response.getReturnValue()[0].pdcmonthList1 != null && response.getReturnValue()[0].pdcmonthList1 != ''){
                        component.set("v.disableBank1" ,true);
                        component.set("v.isaccountType11" ,true);
                        component.set("v.BankName11" ,response.getReturnValue()[0].pdcmonthList1);
                        component.set("v.Accountno11" ,response.getReturnValue()[0].accountMonthList1);
                        component.set("v.AccountType11" ,response.getReturnValue()[0].accountTypeList1);
                    }
                    else{
                        if(response.getReturnValue()[0].perfiosBankName1 != null && response.getReturnValue()[0].perfiosBankName1 != ''){
                            component.set("v.disableBank11" ,true);
                            component.set("v.isaccountType1" ,true);
                            component.set("v.BankName1" ,response.getReturnValue()[0].perfiosBankName1);
                            component.set("v.Accountno1" ,response.getReturnValue()[0].perfiosAccount1);
                            component.set("v.AccountType1" ,response.getReturnValue()[0].perfiosAccountType1);
                            component.set("v.showOtherPicklist",true);
                        }
                    }
                }
                
                if(response.getReturnValue()[0].MonthList2 != null && response.getReturnValue()[0].MonthList2 != ''){
                    component.set("v.showSection2",response.getReturnValue()[0].showSection2);
                    component.set("v.sectionCount", 2)
                    component.set("v.perfiosButtonClicked2" ,false);
                    component.set("v.MonthList2", response.getReturnValue()[0].MonthList2);
                    component.set("v.averageABB2", response.getReturnValue()[0].averageABB2);
                    
                    //Added by Abhilekh for BRE2 Enhancements
                    if(response.getReturnValue()[0].limitAmount2 != null){
                        component.set("v.limitAmount2", response.getReturnValue()[0].limitAmount2);
                    }
                    //Added by Abhilekh for BRE2 Enhancements
                    if(response.getReturnValue()[0].stMth2 != null){
                        component.set("v.selectedStartMonth2", response.getReturnValue()[0].stMth2);
                    }
                	component.set("v.bankConsidered2",response.getReturnValue()[0].bankConsidered2); //Added by Abhilekh for BRE2 Enhancements
                    
                    if(response.getReturnValue()[0].pdcmonthList2 != null && response.getReturnValue()[0].pdcmonthList2 != ''){
                        component.set("v.disableBank2" ,true);
                        component.set("v.isaccountType21" ,true);
                        component.set("v.BankName21" ,response.getReturnValue()[0].pdcmonthList2);
                        component.set("v.Accountno21" ,response.getReturnValue()[0].accountMonthList2);
                        component.set("v.AccountType21" ,response.getReturnValue()[0].accountTypeList2);
                    }                     
                    else{
                        if(response.getReturnValue()[0].perfiosBankName2 != null && response.getReturnValue()[0].perfiosBankName2 != ''){
                            component.set("v.disableBank21" ,true);
                            component.set("v.isaccountType2" ,true);
                            component.set("v.BankName2" ,response.getReturnValue()[0].perfiosBankName2);
                            component.set("v.Accountno2" ,response.getReturnValue()[0].perfiosAccount2);
                            component.set("v.AccountType2" ,response.getReturnValue()[0].perfiosAccountType2);
                        }
                    } 
                }
                if(response.getReturnValue()[0].MonthList3 != null && response.getReturnValue()[0].MonthList3 != ''){
                    component.set("v.showSection3",response.getReturnValue()[0].showSection3); //Added by Abhilekh for BRE2 Enhancements
                    component.set("v.sectionCount",3);
                    component.set("v.perfiosButtonClicked3" ,false);
                    component.set("v.MonthList3", response.getReturnValue()[0].MonthList3);
                    component.set("v.averageABB3", response.getReturnValue()[0].averageABB3);
                    
                    //Added by Abhilekh for BRE2 Enhancements
                    if(response.getReturnValue()[0].limitAmount3 != null){
                        component.set("v.limitAmount3", response.getReturnValue()[0].limitAmount3);
                    }
                    //Added by Abhilekh for BRE2 Enhancements
                    if(response.getReturnValue()[0].stMth3 != null){
                        component.set("v.selectedStartMonth3", response.getReturnValue()[0].stMth3);
                    }
                	component.set("v.bankConsidered3",response.getReturnValue()[0].bankConsidered3); //Added by Abhilekh for BRE2 Enhancements
                    
                    if(response.getReturnValue()[0].pdcmonthList3 != null && response.getReturnValue()[0].pdcmonthList3 != ''){
                        component.set("v.disableBank3" ,true);
                        component.set("v.isaccountType31" ,true);
                        component.set("v.BankName31" ,response.getReturnValue()[0].pdcmonthList3);
                        component.set("v.Accountno31" ,response.getReturnValue()[0].accountMonthList3);
                        component.set("v.AccountType31" ,response.getReturnValue()[0].accountTypeList3);
                    }                     
                    else{
                        if(response.getReturnValue()[0].perfiosBankName3 != null && response.getReturnValue()[0].perfiosBankName3 != ''){
                            component.set("v.disableBank31" ,true);
                            component.set("v.isaccountType3" ,true);
                            component.set("v.BankName3" ,response.getReturnValue()[0].perfiosBankName3);
                            component.set("v.Accountno3" ,response.getReturnValue()[0].perfiosAccount3);
                            component.set("v.AccountType3" ,response.getReturnValue()[0].perfiosAccountType3);
                        }
                    }
                }
                
                /**********Below code added by Abhilekh for BRE2 Enhancements**********/
                if(response.getReturnValue()[0].MonthList4 != null && response.getReturnValue()[0].MonthList4 != ''){
                    component.set("v.showSection4",response.getReturnValue()[0].showSection4);
                    component.set("v.sectionCount",4);
                    component.set("v.perfiosButtonClicked4" ,false);
                    component.set("v.MonthList4", response.getReturnValue()[0].MonthList4);
                    component.set("v.averageABB4", response.getReturnValue()[0].averageABB4);
                    
                    if(response.getReturnValue()[0].limitAmount4 != null){
                        component.set("v.limitAmount4", response.getReturnValue()[0].limitAmount4);
                    }
                    if(response.getReturnValue()[0].stMth4 != null){
                        component.set("v.selectedStartMonth4", response.getReturnValue()[0].stMth4);
                    }
                	component.set("v.bankConsidered4",response.getReturnValue()[0].bankConsidered4);
                    
                    if(response.getReturnValue()[0].pdcmonthList4 != null && response.getReturnValue()[0].pdcmonthList4 != ''){
                        component.set("v.disableBank4" ,true);
                        component.set("v.isaccountType41" ,true);
                        component.set("v.BankName41" ,response.getReturnValue()[0].pdcmonthList4);
                        component.set("v.Accountno41" ,response.getReturnValue()[0].accountMonthList4);
                        component.set("v.AccountType41" ,response.getReturnValue()[0].accountTypeList4);
                    }                     
                    else{
                        if(response.getReturnValue()[0].perfiosBankName4 != null && response.getReturnValue()[0].perfiosBankName4 != ''){
                            component.set("v.disableBank41" ,true);
                            component.set("v.isaccountType4" ,true);
                            component.set("v.BankName4" ,response.getReturnValue()[0].perfiosBankName4);
                            component.set("v.Accountno4" ,response.getReturnValue()[0].perfiosAccount4);
                            component.set("v.AccountType4" ,response.getReturnValue()[0].perfiosAccountType4);
                        }
                    }
                }
                
                if(response.getReturnValue()[0].MonthList5 != null && response.getReturnValue()[0].MonthList5 != ''){
                    component.set("v.showSection5",response.getReturnValue()[0].showSection5);
                    component.set("v.sectionCount",5);
                    component.set("v.perfiosButtonClicked5" ,false);
                    component.set("v.MonthList5", response.getReturnValue()[0].MonthList5);
                    component.set("v.averageABB5", response.getReturnValue()[0].averageABB5);
                    
                    if(response.getReturnValue()[0].limitAmount5 != null){
                        component.set("v.limitAmount5", response.getReturnValue()[0].limitAmount5);
                    }
                    if(response.getReturnValue()[0].stMth5 != null){
                        component.set("v.selectedStartMonth5", response.getReturnValue()[0].stMth5);
                    }
                	component.set("v.bankConsidered5",response.getReturnValue()[0].bankConsidered5);
                    
                    if(response.getReturnValue()[0].pdcmonthList5 != null && response.getReturnValue()[0].pdcmonthList5 != ''){
                        component.set("v.disableBank5" ,true);
                        component.set("v.isaccountType51" ,true);
                        component.set("v.BankName51" ,response.getReturnValue()[0].pdcmonthList5);
                        component.set("v.Accountno51" ,response.getReturnValue()[0].accountMonthList5);
                        component.set("v.AccountType51" ,response.getReturnValue()[0].accountTypeList5);
                    }                     
                    else{
                        if(response.getReturnValue()[0].perfiosBankName5 != null && response.getReturnValue()[0].perfiosBankName5 != ''){
                            component.set("v.disableBank51" ,true);
                            component.set("v.isaccountType5" ,true);
                            component.set("v.BankName5" ,response.getReturnValue()[0].perfiosBankName5);
                            component.set("v.Accountno5" ,response.getReturnValue()[0].perfiosAccount5);
                            component.set("v.AccountType5" ,response.getReturnValue()[0].perfiosAccountType5);
                        }
                    }
                }
                
                if(response.getReturnValue()[0].MonthList6 != null && response.getReturnValue()[0].MonthList6 != ''){
                    component.set("v.sectionCount",6);
                    component.set("v.perfiosButtonClicked6" ,false);
                    component.set("v.MonthList6", response.getReturnValue()[0].MonthList6);
                    component.set("v.averageABB6", response.getReturnValue()[0].averageABB6);
                    
                    if(response.getReturnValue()[0].limitAmount6 != null){
                        component.set("v.limitAmount6", response.getReturnValue()[0].limitAmount6);
                    }
                    if(response.getReturnValue()[0].stMth6 != null){
                        component.set("v.selectedStartMonth6", response.getReturnValue()[0].stMth6);
                    }
                	component.set("v.bankConsidered6",response.getReturnValue()[0].bankConsidered6);
                    
                    if(response.getReturnValue()[0].pdcmonthList6 != null && response.getReturnValue()[0].pdcmonthList6 != ''){
                        component.set("v.disableBank6" ,true);
                        component.set("v.isaccountType61" ,true);
                        component.set("v.BankName61" ,response.getReturnValue()[0].pdcmonthList6);
                        component.set("v.Accountno61" ,response.getReturnValue()[0].accountMonthList6);
                        component.set("v.AccountType61" ,response.getReturnValue()[0].accountTypeList6);
                    }                     
                    else{
                        if(response.getReturnValue()[0].perfiosBankName6 != null && response.getReturnValue()[0].perfiosBankName6 != ''){
                            component.set("v.disableBank61" ,true);
                            component.set("v.isaccountType6" ,true);
                            component.set("v.BankName6" ,response.getReturnValue()[0].perfiosBankName6);
                            component.set("v.Accountno6" ,response.getReturnValue()[0].perfiosAccount6);
                            component.set("v.AccountType6" ,response.getReturnValue()[0].perfiosAccountType6);
                        }
                    }
                }
                /**********Above code added by Abhilekh for BRE2 Enhancements**********/
                this.checkAccountType(component,helper, event); //Added by Abhilekh for BRE2 Enhancements
            }
        });
        $A.enqueueAction(action);
    },
    fetchPerfiosData: function(component, event, helper, bankName, accountNumber, sectionno, perfiosBankDetails,startMonth){ //startMonth added by Abhilekh for BRE2 Enhancements
        var recordid = component.get("v.custRecordId");
        
        var action = component.get("c.fetchMonthlyBanking");
        action.setParams({
            'CDrecID' : component.get("v.custRecordId"), 
            'bnkname' : bankName ,
            'accountNo': accountNumber,
            'getBRERecord' : false,
            'perfiosBankDetails' : perfiosBankDetails,
            'startMonth' : startMonth
        }) //startMonth added by Abhilekh for BRE2 Enhancements
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {   
                component.set("v.MonthList", {});
                var storeResponse = response.getReturnValue();//response.getReturnValue()
                if(storeResponse==null || storeResponse.length==0|| storeResponse==''|| JSON.stringify(response.getReturnValue())=='[]'){
                    component.set("v.showConfirmDialog",true);
                    component.set("v.AskforPerfiosData",false);
                    component.set("v.perfiosButtonClicked" ,false);
                } else{
                    if(sectionno == "1"){
                        component.set("v.perfiosButtonClicked" ,true);
                        component.set("v.MonthList1", response.getReturnValue());
                    }else if(sectionno == "2"){
                        component.set("v.perfiosButtonClicked2" ,true);
                        component.set("v.MonthList2", response.getReturnValue());                  
                    }else if(sectionno == "3"){
                        component.set("v.perfiosButtonClicked3" ,true);
                        component.set("v.MonthList3", response.getReturnValue());                  
                    }
                    
                    //Added by Abhilekh for BRE2 Enhancements
                    else if(sectionno == "4"){
                        component.set("v.perfiosButtonClicked4" ,true);
                        component.set("v.MonthList4", response.getReturnValue());                  
                    }
                    
                    //Added by Abhilekh for BRE2 Enhancements
                    else if(sectionno == "5"){
                        component.set("v.perfiosButtonClicked5" ,true);
                        component.set("v.MonthList5", response.getReturnValue());                  
                    }
                    
                    //Added by Abhilekh for BRE2 Enhancements
                    else if(sectionno == "6"){
                        component.set("v.perfiosButtonClicked6" ,true);
                        component.set("v.MonthList6", response.getReturnValue());                  
                    }
                }
            }
        });            
        $A.enqueueAction(action);
    },
    
    getdatafromperfios : function(component, event, helper) {
        var d = new Date();
        var n = d.getFullYear();
        component.set("v.currentyear", n);                
        var custid = component.get("v.Customerid");
        var action = component.get("c.GetPerfiosData");
        action.setParams({
            'conId':custid
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                console.log('in success'+ response.getReturnValue());
                component.set("v.gotperfios", response.getReturnValue());    
                if(response.getReturnValue() === false)
                {
                    component.set('v.gotperfios', false);
                    component.set('v.showConfirmDialog', true);
                    console.log('in success get return value false::::'+response.getReturnValue());
                    
                }
                else{
                    component.set('v.showConfirmDialog', false);
                }
                
                
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    closeMe : function(comp, event, helper)  { 
        var custid = comp.get("v.custRecordId");
        var recordIdNumber = '';
        var testLoanAppId = comp.get("v.loanAppId");
        var fromScreenName = comp.get("v.fromScreenName"); //Added by Abhilekh for CAM Summary BRE2 Enhancements
        
        //Below if block and else block only added by Abhilekh for CAM Summary BRE2 Enhancements
        if(fromScreenName === 'CAMSummary'){
            var evt = $A.get("e.c:callCAMSummaryEvent");
            evt.setParams({
                "loanAppId" : testLoanAppId
            });
            evt.fire();
        }
        else{
            var evt = $A.get("e.c:callCreateCamScreenEvent");
            evt.setParams({
                "recordIdNumber" : custid,
                "isOpen" : true,
                "testLoanAppId" : testLoanAppId
            });
            evt.fire();
        }
    },
    fetchBRERecordsHelper : function(component, event, helper, disableBank1, disableBank11, sectionno)  {
    	if(disableBank1 == false && disableBank11 == false) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Please enter bank and account number'
            });
            toastEvent.fire(); 
        } else if(disableBank1 && disableBank11) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Some error occurred, Please contact system admin'
            });
            toastEvent.fire(); 
        } else if(disableBank1) {
            if(sectionno == "1") {
                var bankName = component.get("v.BankName11");
                var accountNumber = component.get("v.Accountno11");
                var startMonth = component.get("v.selectedStartMonth1"); //Added by Abhilekh for BRE2 Enhancements
                
            } else if (sectionno == "2") {
				var bankName = component.get("v.BankName21");
                var accountNumber = component.get("v.Accountno21");
                var startMonth = component.get("v.selectedStartMonth2"); //Added by Abhilekh for BRE2 Enhancements
                               
            } else if(sectionno == "3") {
               	var bankName = component.get("v.BankName31");
                var accountNumber = component.get("v.Accountno31");  
                var startMonth = component.get("v.selectedStartMonth3"); //Added by Abhilekh for BRE2 Enhancements
            }
            
            //Added by Abhilekh for BRE2 Enhancements
            else if(sectionno == "4") {
               	var bankName = component.get("v.BankName41");
                var accountNumber = component.get("v.Accountno41");  
                var startMonth = component.get("v.selectedStartMonth4");
            }
            
            //Added by Abhilekh for BRE2 Enhancements
            else if(sectionno == "5") {
               	var bankName = component.get("v.BankName51");
                var accountNumber = component.get("v.Accountno51");  
                var startMonth = component.get("v.selectedStartMonth5");
            }
            
            //Added by Abhilekh for BRE2 Enhancements
            else if(sectionno == "6") {
               	var bankName = component.get("v.BankName61");
                var accountNumber = component.get("v.Accountno61");  
                var startMonth = component.get("v.selectedStartMonth6");
            }
            
            if(bankName == undefined || bankName == null || bankName =='' || accountNumber == undefined || accountNumber == null || accountNumber == '') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Please enter PDC Bank Master and Account Number'
                });
                toastEvent.fire();    
            } else {
                this.fetchBRERecords(component, event, helper,bankName ,accountNumber,sectionno, false,startMonth); //startMonth added by Abhilekh for BRE2 Enhancements
            }
        } else if(disableBank11) {
            if(sectionno == "1") {  
                var bankName = component.get("v.BankName1");
                var accountNumber = component.get("v.Accountno1");
                var startMonth = component.get("v.selectedStartMonth1"); //Added by Abhilekh for BRE2 Enhancements
            } else if (sectionno == "2") {
				var bankName = component.get("v.BankName2");
                var accountNumber = component.get("v.Accountno2");
                var startMonth = component.get("v.selectedStartMonth2"); //Added by Abhilekh for BRE2 Enhancements
            } else if (sectionno == "3") {  
            	var bankName = component.get("v.BankName3");
                var accountNumber = component.get("v.Accountno3");
                var startMonth = component.get("v.selectedStartMonth3"); //Added by Abhilekh for BRE2 Enhancements
            }
            
            //Added by Abhilekh for BRE2 Enhancements
            else if (sectionno == "4") {  
            	var bankName = component.get("v.BankName4");
                var accountNumber = component.get("v.Accountno4");
                var startMonth = component.get("v.selectedStartMonth4");
            }
            
            //Added by Abhilekh for BRE2 Enhancements
            else if (sectionno == "5") {  
            	var bankName = component.get("v.BankName5");
                var accountNumber = component.get("v.Accountno5");
                var startMonth = component.get("v.selectedStartMonth5");
            }
            
            //Added by Abhilekh for BRE2 Enhancements
            else if (sectionno == "6") {  
            	var bankName = component.get("v.BankName6");
                var accountNumber = component.get("v.Accountno6");
                var startMonth = component.get("v.selectedStartMonth6");
            }
            
            if(bankName == undefined || bankName == null || bankName == '' || accountNumber == undefined || accountNumber == null || accountNumber == '') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Please enter Perfios Bank and Account Number'
                });
                toastEvent.fire();     
            } else {
                this.fetchBRERecords(component, event, helper,bankName ,accountNumber,sectionno, true,startMonth); //startMonth added by Abhilekh for BRE2 Enhancements
            }
        }    
    },
    
    fetchPerfiosDataHelper: function(component, event, helper, disableBank1, disableBank11, sectionno)  {
    	if(disableBank1 == false && disableBank11 == false) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Please enter bank and account number'
            });
            toastEvent.fire(); 
        } else if(disableBank1 && disableBank11) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": 'Some error occurred, Please contact system admin'
            });
            toastEvent.fire(); 
        } else if(disableBank1) {
            if(sectionno == "1") {
                var bankName = component.get("v.BankName11");
                var accountNumber = component.get("v.Accountno11");  
            } else if(sectionno == "2") {
            	var bankName = component.get("v.BankName21");
                var accountNumber = component.get("v.Accountno21");     
            } else if (sectionno == "3") {
            	var bankName = component.get("v.BankName31");
                var accountNumber = component.get("v.Accountno31");     
            }
            
            //Added by Abhilekh for BRE2 Enhancements
            else if (sectionno == "4") {
            	var bankName = component.get("v.BankName41");
                var accountNumber = component.get("v.Accountno41");     
            }
            
            //Added by Abhilekh for BRE2 Enhancements
            else if (sectionno == "5") {
            	var bankName = component.get("v.BankName51");
                var accountNumber = component.get("v.Accountno51");     
            }
            
            //Added by Abhilekh for BRE2 Enhancements
            else if (sectionno == "6") {
            	var bankName = component.get("v.BankName61");
                var accountNumber = component.get("v.Accountno61");     
            }
            
            if(bankName == undefined || bankName == null || bankName =='' || accountNumber == undefined || accountNumber == null || accountNumber == '') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Please enter PDC Bank Master and Account Number'
                });
                toastEvent.fire();    
            } else {
                component.set("v.FetchPerfiosSectionNo",sectionno);
                component.set("v.showConfirmDialog",true);
            }
        } else if(disableBank11) {
            if(sectionno == "1") {
                var bankName = component.get("v.BankName1");
                var accountNumber = component.get("v.Accountno1");
            } else if(sectionno == "2") {
                var bankName = component.get("v.BankName2");
                var accountNumber = component.get("v.Accountno2");
            } else if(sectionno == "3") {
                var bankName = component.get("v.BankName3");
                var accountNumber = component.get("v.Accountno3");
            }
            
            //Added by Abhilekh for BRE2 Enhancements
            else if(sectionno == "4") {
                var bankName = component.get("v.BankName4");
                var accountNumber = component.get("v.Accountno4");
            }
            
            //Added by Abhilekh for BRE2 Enhancements
            else if(sectionno == "5") {
                var bankName = component.get("v.BankName5");
                var accountNumber = component.get("v.Accountno5");
            }
            
            //Added by Abhilekh for BRE2 Enhancements
            else if(sectionno == "6") {
                var bankName = component.get("v.BankName6");
                var accountNumber = component.get("v.Accountno6");
            }
            
            if(bankName == undefined || bankName == null || bankName == '' || accountNumber == undefined || accountNumber == null || accountNumber == '') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Please enter Perfios Bank and Account Number'
                });
                toastEvent.fire();     
            } else {
                component.set("v.FetchPerfiosSectionNo",sectionno);
                component.set("v.showConfirmDialog",true);
            }
        }    
    },
    saveHelper: function(component, event, helper, disableBank1, disableBank11, perfiosButtonClicked, sectionno,saveAll)  {
        var isverified = component.find("isChecked").get("v.value");
        var lstStartMonths = component.get("v.lstStartMonths"); //Added by Abhilekh for BRE2 Enhancements
        
        var errorMsg = false;
        
        if (sectionno == "2") {
            var list = component.get("v.MonthList2");
            var valueFoundin2 = false;
            
            if(list != '') {
                for(var key in list) {
                    if(JSON.stringify(list[key].inoutrec) != '{}') {
                        valueFoundin2 = true;
                    }
                }
            }
        } else if (sectionno == "3") {
            var list = component.get("v.MonthList3");
            var valueFoundin3 = false;
            
            if(list != '') {
                for(var key in list) {
                    if(JSON.stringify(list[key].inoutrec) != '{}') {
                        valueFoundin3 = true;
                    }
                }
            }
        }
        
        /*******Below code added by Abhilekh for BRE2 Enhancements********/
        else if (sectionno == "4") {
            var list = component.get("v.MonthList4");
            var valueFoundin4 = false;
            
            if(list != '') {
                for(var key in list) {
                    if(JSON.stringify(list[key].inoutrec) != '{}') {
                        valueFoundin4 = true;
                    }
                }
            }
        }
        else if (sectionno == "5") {
            var list = component.get("v.MonthList5");
            var valueFoundin5 = false;
            
            if(list != '') {
                for(var key in list) {
                    if(JSON.stringify(list[key].inoutrec) != '{}') {
                        valueFoundin5 = true;
                    }
                }
            }
        }
        else if (sectionno == "6") {
            var list = component.get("v.MonthList6");
            var valueFoundin6 = false;
            
            if(list != '') {
                for(var key in list) {
                    if(JSON.stringify(list[key].inoutrec) != '{}') {
                        valueFoundin6 = true;
                    }
                }
            }
        }
        /*******Above code added by Abhilekh for BRE2 Enhancements********/
        
        if(disableBank1 == false && disableBank11 == false ) {
            errorMsg = true;
            if(saveAll == "false" || (saveAll == "true" && (sectionno =="1" || (sectionno =="2" && valueFoundin2) || (sectionno =="3" && valueFoundin3) || (sectionno =="4" && valueFoundin4) || (sectionno =="5" && valueFoundin5) || (sectionno =="6" && valueFoundin6)))){
                //section4,5 and 6 added by Abhilekh for BRE2 Enhancements
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Please enter bank details in section '+sectionno
                });
                toastEvent.fire();
            }
        } else if(disableBank1 && disableBank11 ) {
            errorMsg = true;
            //if(saveAll == "false" || (saveAll == "true" && sectionno =="1")) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Some error occurred, Please contact system admin in section '+sectionno
                });
                toastEvent.fire(); 
            //}
        } else if(disableBank1) {
            if(sectionno == "1") {
                var bankName = component.get("v.BankName11");
                var accountNumber = component.get("v.Accountno11");  
                var accountType = component.get("v.AccountType11");
                var limitAmount = component.get("v.limitAmount1"); //Added by Abhilekh for BRE2 Enhancements
                var bankConsidered = component.get("v.bankConsidered1"); //Added by Abhilekh for BRE2 Enhancements
                var startMonth = component.get("v.selectedStartMonth1"); //Added by Abhilekh for BRE2 Enhancements
            } else if(sectionno == "2") {
            	var bankName = component.get("v.BankName21");
                var accountNumber = component.get("v.Accountno21"); 
                var accountType = component.get("v.AccountType21");
                var limitAmount = component.get("v.limitAmount2"); //Added by Abhilekh for BRE2 Enhancements
                var bankConsidered = component.get("v.bankConsidered2"); //Added by Abhilekh for BRE2 Enhancements
                var startMonth = component.get("v.selectedStartMonth2"); //Added by Abhilekh for BRE2 Enhancements
            } else if (sectionno == "3") {
            	var bankName = component.get("v.BankName31");
                var accountNumber = component.get("v.Accountno31");
                var accountType = component.get("v.AccountType31");
                var limitAmount = component.get("v.limitAmount3"); //Added by Abhilekh for BRE2 Enhancements
                var bankConsidered = component.get("v.bankConsidered3"); //Added by Abhilekh for BRE2 Enhancements
                var startMonth = component.get("v.selectedStartMonth3"); //Added by Abhilekh for BRE2 Enhancements
            }
            
            //Added by Abhilekh for BRE2 Enhancements
            else if (sectionno == "4") {
            	var bankName = component.get("v.BankName41");
                var accountNumber = component.get("v.Accountno41");
                var accountType = component.get("v.AccountType41");
                var limitAmount = component.get("v.limitAmount4");
                var bankConsidered = component.get("v.bankConsidered4");
                var startMonth = component.get("v.selectedStartMonth4");
            }
        
        	//Added by Abhilekh for BRE2 Enhancements
        	else if (sectionno == "5") {
            	var bankName = component.get("v.BankName51");
                var accountNumber = component.get("v.Accountno51");
                var accountType = component.get("v.AccountType51");
                var limitAmount = component.get("v.limitAmount5");
                var bankConsidered = component.get("v.bankConsidered5");
                var startMonth = component.get("v.selectedStartMonth5");
            }
 			
 			//Added by Abhilekh for BRE2 Enhancements
 			else if (sectionno == "6") {
            	var bankName = component.get("v.BankName61");
                var accountNumber = component.get("v.Accountno61");
                var accountType = component.get("v.AccountType61");
                var limitAmount = component.get("v.limitAmount6");
                var bankConsidered = component.get("v.bankConsidered6");
                var startMonth = component.get("v.selectedStartMonth6");
            }           
            
            if(bankName == undefined || bankName == null || bankName =='' || accountNumber == undefined || accountNumber == null || accountNumber == '' || accountType == undefined || accountType == null || accountType=='') {
                errorMsg = true;
                if(saveAll == "false" || (saveAll == "true" && (sectionno =="1" || (sectionno =="2" && valueFoundin2) || (sectionno =="3" && valueFoundin3) || (sectionno =="4" && valueFoundin4) || (sectionno =="5" && valueFoundin5) || (sectionno =="6" && valueFoundin6)))){
                //section4,5 and 6 added by Abhilekh for BRE2 Enhancements
                
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "",
                        "type": "Error",
                        "message": 'Please enter PDC Bank Master, Account Number, Account Type in section '+sectionno
                    });
                    toastEvent.fire();  
                }
            }
            
            
        } else if(disableBank11 ) {
            if(sectionno == "1") {
                var bankName = component.get("v.BankName1");
                var accountNumber = component.get("v.Accountno1");
                var accountType = component.get("v.AccountType1");
                var limitAmount = component.get("v.limitAmount1"); //Added by Abhilekh for BRE2 Enhancements
                var bankConsidered = component.get("v.bankConsidered1"); //Added by Abhilekh for BRE2 Enhancements
                var startMonth = component.get("v.selectedStartMonth1"); //Added by Abhilekh for BRE2 Enhancements
            } else if(sectionno == "2") {
                var bankName = component.get("v.BankName2");
                var accountNumber = component.get("v.Accountno2");
                var accountType = component.get("v.AccountType2");
                var limitAmount = component.get("v.limitAmount2"); //Added by Abhilekh for BRE2 Enhancements
                var bankConsidered = component.get("v.bankConsidered2"); //Added by Abhilekh for BRE2 Enhancements
                var startMonth = component.get("v.selectedStartMonth2"); //Added by Abhilekh for BRE2 Enhancements
            } else if(sectionno == "3") {
                var bankName = component.get("v.BankName3");
                var accountNumber = component.get("v.Accountno3");
                var accountType = component.get("v.AccountType3");
                var limitAmount = component.get("v.limitAmount3"); //Added by Abhilekh for BRE2 Enhancements
                var bankConsidered = component.get("v.bankConsidered3"); //Added by Abhilekh for BRE2 Enhancements
                var startMonth = component.get("v.selectedStartMonth3"); //Added by Abhilekh for BRE2 Enhancements
            }
            
            //Added by Abhilekh for BRE2 Enhancements
            else if(sectionno == "4") {
                var bankName = component.get("v.BankName4");
                var accountNumber = component.get("v.Accountno4");
                var accountType = component.get("v.AccountType4");
                var limitAmount = component.get("v.limitAmount4");
                var bankConsidered = component.get("v.bankConsidered4");
                var startMonth = component.get("v.selectedStartMonth4");
            }
            
            //Added by Abhilekh for BRE2 Enhancements
            else if(sectionno == "5") {
                var bankName = component.get("v.BankName5");
                var accountNumber = component.get("v.Accountno5");
                var accountType = component.get("v.AccountType5");
                var limitAmount = component.get("v.limitAmount5");
                var bankConsidered = component.get("v.bankConsidered5");
                var startMonth = component.get("v.selectedStartMonth5");
            }
            
            //Added by Abhilekh for BRE2 Enhancements
            else if(sectionno == "6") {
                var bankName = component.get("v.BankName6");
                var accountNumber = component.get("v.Accountno6");
                var accountType = component.get("v.AccountType6");
                var limitAmount = component.get("v.limitAmount6");
                var bankConsidered = component.get("v.bankConsidered6");
                var startMonth = component.get("v.selectedStartMonth6");
            }
            
            if(bankName == undefined || bankName == null || bankName == '' || accountNumber == undefined || accountNumber == null || accountNumber == '' || accountType == undefined || accountType == null || accountType =='') {
                errorMsg = true;
                if(saveAll == "false" || (saveAll == "true" && (sectionno =="1" || (sectionno =="2" && valueFoundin2) || (sectionno =="3" && valueFoundin3) || (sectionno =="4" && valueFoundin4) || (sectionno =="5" && valueFoundin5) || (sectionno =="6" && valueFoundin6)))){
                //section4,5 and 6 added by Abhilekh for BRE2 Enhancements
                
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "",
                        "type": "Error",
                        "message": 'Please enter Perfios Bank, Account Number, Account Type in section '+sectionno
                    });
                    toastEvent.fire();  
            	}
            } 
        }
        
        if(errorMsg == false) {
            //Below if block added by Abhilekh for BRE2 Enhancements
            if(!lstStartMonths.includes(startMonth)){
                startMonth = lstStartMonths[0];
            }
            
            if(isverified === false){
                var abc = component.get("v.RowCountToVerify");
                this.doSave(component, event, helper,bankName ,accountNumber,accountType, sectionno,disableBank11, perfiosButtonClicked, abc,limitAmount,bankConsidered,startMonth); //limitAmount,bankConsidered,startMonth added by Abhilekh for BRE2 Enhancements
            }else{
                //Error Handling on save button                
                var abc = component.get("v.RowCountToVerify");
                if(sectionno == "1") {
                    var childComp = component.find('childComp1');  
                    childComp.slice(0,abc).reduce(function(componentlist, currentComponent){
                        currentComponent.CheckChildValidity();},childComp[0]);        
                    
                    var allValid = component.find('fieldId1').reduce(function (validSoFar, inputCmp) {
                        inputCmp.showHelpMessageIfInvalid();
                        return validSoFar && inputCmp.get('v.validity').valid;
                    }, true); 
                } else if(sectionno == "2") {
                    var childComp = component.find('childComp2');  
                    childComp.slice(0,abc).reduce(function(componentlist, currentComponent){
                        currentComponent.CheckChildValidity();},childComp[1]);        
                    
                    var allValid = component.find('fieldId2').reduce(function (validSoFar, inputCmp) {
                        inputCmp.showHelpMessageIfInvalid();
                        return validSoFar && inputCmp.get('v.validity').valid;
                    }, true); 
                } else if(sectionno == "3") {
                    var childComp = component.find('childComp3');  
                    childComp.slice(0,abc).reduce(function(componentlist, currentComponent){
                        currentComponent.CheckChildValidity();},childComp[2]);        
                    
                    var allValid = component.find('fieldId3').reduce(function (validSoFar, inputCmp) {
                        inputCmp.showHelpMessageIfInvalid();
                        return validSoFar && inputCmp.get('v.validity').valid;
                    }, true); 
                }
                
                //Added by Abhilekh for BRE2 Enhancements
                else if(sectionno == "4") {
                    var childComp = component.find('childComp4');  
                    childComp.slice(0,abc).reduce(function(componentlist, currentComponent){
                        currentComponent.CheckChildValidity();},childComp[3]);        
                    
                    var allValid = component.find('fieldId4').reduce(function (validSoFar, inputCmp) {
                        inputCmp.showHelpMessageIfInvalid();
                        return validSoFar && inputCmp.get('v.validity').valid;
                    }, true); 
                }
                
                //Added by Abhilekh for BRE2 Enhancements
                else if(sectionno == "5") {
                    var childComp = component.find('childComp5');  
                    childComp.slice(0,abc).reduce(function(componentlist, currentComponent){
                        currentComponent.CheckChildValidity();},childComp[4]);        
                    
                    var allValid = component.find('fieldId5').reduce(function (validSoFar, inputCmp) {
                        inputCmp.showHelpMessageIfInvalid();
                        return validSoFar && inputCmp.get('v.validity').valid;
                    }, true); 
                }
                
                //Added by Abhilekh for BRE2 Enhancements
                else if(sectionno == "6") {
                    var childComp = component.find('childComp6');  
                    childComp.slice(0,abc).reduce(function(componentlist, currentComponent){
                        currentComponent.CheckChildValidity();},childComp[5]);        
                    
                    var allValid = component.find('fieldId6').reduce(function (validSoFar, inputCmp) {
                        inputCmp.showHelpMessageIfInvalid();
                        return validSoFar && inputCmp.get('v.validity').valid;
                    }, true); 
                }
                
                //do save logic if valid
                if(allValid){ 
                    //alert('ALl fields valid !!');
                    this.doSave(component, event, helper,bankName ,accountNumber,accountType, sectionno,disableBank11, perfiosButtonClicked , abc,limitAmount,bankConsidered,startMonth); //limitAmount,bankConsidered,startMonth added by Abhilekh for BRE2 Enhancements
                }
            }
        }
    },
    save1Helper : function(component, event, helper, saveAll){
      	var disableBank1 = component.get("v.disableBank1");
        var disableBank11 = component.get("v.disableBank11");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked");
        this.checkDuplicates(component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"1", "Save",saveAll);
    },
    save2Helper : function(component, event, helper, saveAll){
        var disableBank1 = component.get("v.disableBank2");
        var disableBank11 = component.get("v.disableBank21");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked2");
        this.checkDuplicates(component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"2", "Save",saveAll);
    }, 
	save3Helper : function(component, event, helper, saveAll){    
        var disableBank1 = component.get("v.disableBank3");
        var disableBank11 = component.get("v.disableBank31");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked3");
        this.checkDuplicates(component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"3", "Save",saveAll);
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    save4Helper : function(component, event, helper, saveAll){    
        var disableBank1 = component.get("v.disableBank4");
        var disableBank11 = component.get("v.disableBank41");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked4");
        this.checkDuplicates(component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"4", "Save",saveAll);
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    save5Helper : function(component, event, helper, saveAll){    
        var disableBank1 = component.get("v.disableBank5");
        var disableBank11 = component.get("v.disableBank51");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked5");
        this.checkDuplicates(component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"5", "Save",saveAll);
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    save6Helper : function(component, event, helper, saveAll){    
        var disableBank1 = component.get("v.disableBank6");
        var disableBank11 = component.get("v.disableBank61");
        var perfiosButtonClicked = component.get("v.perfiosButtonClicked6");
        this.checkDuplicates(component, event, helper, disableBank1, disableBank11,perfiosButtonClicked,"6", "Save",saveAll);
    },
    checkDuplicates : function(component, event, helper, disableBank1, disableBank11, perfiosButtonClicked, sectionno, action, saveAll){
        if(sectionno == "1") {
            if( 
                ( (((component.get("v.BankName11") == component.get("v.BankName21")) && (component.get("v.BankName11") != undefined) && (component.get("v.BankName11") != null) && (component.get("v.BankName11") != '') && (component.get("v.BankName21") != undefined) && (component.get("v.BankName21") != null) && (component.get("v.BankName21") != '')) || ((component.get("v.BankName11") == component.get("v.BankName2")) && (component.get("v.BankName11") != undefined) && (component.get("v.BankName11") != null) && (component.get("v.BankName11") != '') && (component.get("v.BankName2") != undefined) && (component.get("v.BankName2") != null) && (component.get("v.BankName2") != '')) || ((component.get("v.BankName1") == component.get("v.BankName2")) && (component.get("v.BankName1") != undefined) && (component.get("v.BankName1") != null) && (component.get("v.BankName1") != '') && (component.get("v.BankName2") != undefined) && (component.get("v.BankName2") != null) && (component.get("v.BankName2") != '')) || ((component.get("v.BankName1") == component.get("v.BankName21")) && (component.get("v.BankName1") != undefined) && (component.get("v.BankName1") != null) && (component.get("v.BankName1") != '') && (component.get("v.BankName21") != undefined) && (component.get("v.BankName21") != null) && (component.get("v.BankName21") != '') ) )  &&
                 (((component.get("v.Accountno11") == component.get("v.Accountno21"))  && (component.get("v.Accountno11") != undefined) && (component.get("v.Accountno11") != null) && (component.get("v.Accountno11") != '') && (component.get("v.Accountno21") != undefined) && (component.get("v.Accountno21") != null) && (component.get("v.Accountno21") != '' )) || ((component.get("v.Accountno11") == component.get("v.Accountno2"))  && (component.get("v.Accountno11") != undefined) && (component.get("v.Accountno11") != null) && (component.get("v.Accountno11") != '') && (component.get("v.Accountno2") != undefined) && (component.get("v.Accountno2") != null) && (component.get("v.Accountno2") != '' ))|| ((component.get("v.Accountno1") == component.get("v.Accountno2"))  && (component.get("v.Accountno1") != undefined) && (component.get("v.Accountno1") != null) && (component.get("v.Accountno1") != '') && (component.get("v.Accountno2") != undefined) && (component.get("v.Accountno2") != null) && (component.get("v.Accountno2") != '' )) || ((component.get("v.Accountno1") == component.get("v.Accountno21"))  && (component.get("v.Accountno1") != undefined) && (component.get("v.Accountno1") != null) && (component.get("v.Accountno1") != '') && (component.get("v.Accountno21") != undefined) && (component.get("v.Accountno21") != null) && (component.get("v.Accountno21") != '') ) )) 
                ||   
                ( (((component.get("v.BankName31") == component.get("v.BankName11")) && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '' && component.get("v.BankName11") != undefined && component.get("v.BankName11") != null && component.get("v.BankName11") != '') || ((component.get("v.BankName31") == component.get("v.BankName1")) && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '' && component.get("v.BankName1") != undefined && component.get("v.BankName1") != null && component.get("v.BankName1") != '') || ((component.get("v.BankName3") == component.get("v.BankName11")) && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' && component.get("v.BankName11") != undefined && component.get("v.BankName11") != null && component.get("v.BankName11") != '') || ((component.get("v.BankName3") == component.get("v.BankName1")) && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' && component.get("v.BankName1") != undefined && component.get("v.BankName1") != null && component.get("v.BankName1") != '' ) )  &&
                 (((component.get("v.Accountno31") == component.get("v.Accountno11"))  && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' && component.get("v.Accountno11") != undefined && component.get("v.Accountno11") != null && component.get("v.Accountno11") != '' ) || ((component.get("v.Accountno31") == component.get("v.Accountno1"))  && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' && component.get("v.Accountno1") != undefined && component.get("v.Accountno1") != null && component.get("v.Accountno1") != '' )|| ((component.get("v.Accountno3") == component.get("v.Accountno11"))  && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' && component.get("v.Accountno11") != undefined && component.get("v.Accountno11") != null && component.get("v.Accountno11") != '' ) || ((component.get("v.Accountno3") == component.get("v.Accountno1"))  && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' && component.get("v.Accountno1") != undefined && component.get("v.Accountno1") != null && component.get("v.Accountno1") != '' ) ))
                ||
                ( (((component.get("v.BankName11") == component.get("v.BankName41")) && (component.get("v.BankName11") != undefined) && (component.get("v.BankName11") != null) && (component.get("v.BankName11") != '') && (component.get("v.BankName41") != undefined) && (component.get("v.BankName41") != null) && (component.get("v.BankName41") != '')) || ((component.get("v.BankName11") == component.get("v.BankName4")) && (component.get("v.BankName11") != undefined) && (component.get("v.BankName11") != null) && (component.get("v.BankName11") != '') && (component.get("v.BankName4") != undefined) && (component.get("v.BankName4") != null) && (component.get("v.BankName4") != '')) || ((component.get("v.BankName1") == component.get("v.BankName4")) && (component.get("v.BankName1") != undefined) && (component.get("v.BankName1") != null) && (component.get("v.BankName1") != '') && (component.get("v.BankName4") != undefined) && (component.get("v.BankName2") != null) && (component.get("v.BankName2") != '')) || ((component.get("v.BankName1") == component.get("v.BankName41")) && (component.get("v.BankName1") != undefined) && (component.get("v.BankName1") != null) && (component.get("v.BankName1") != '') && (component.get("v.BankName41") != undefined) && (component.get("v.BankName41") != null) && (component.get("v.BankName41") != '') ) )  &&
                 (((component.get("v.Accountno11") == component.get("v.Accountno41"))  && (component.get("v.Accountno11") != undefined) && (component.get("v.Accountno11") != null) && (component.get("v.Accountno11") != '') && (component.get("v.Accountno41") != undefined) && (component.get("v.Accountno41") != null) && (component.get("v.Accountno41") != '' )) || ((component.get("v.Accountno11") == component.get("v.Accountno4"))  && (component.get("v.Accountno11") != undefined) && (component.get("v.Accountno11") != null) && (component.get("v.Accountno11") != '') && (component.get("v.Accountno4") != undefined) && (component.get("v.Accountno4") != null) && (component.get("v.Accountno4") != '' ))|| ((component.get("v.Accountno1") == component.get("v.Accountno4"))  && (component.get("v.Accountno1") != undefined) && (component.get("v.Accountno1") != null) && (component.get("v.Accountno1") != '') && (component.get("v.Accountno4") != undefined) && (component.get("v.Accountno4") != null) && (component.get("v.Accountno4") != '' )) || ((component.get("v.Accountno1") == component.get("v.Accountno41"))  && (component.get("v.Accountno1") != undefined) && (component.get("v.Accountno1") != null) && (component.get("v.Accountno1") != '') && (component.get("v.Accountno41") != undefined) && (component.get("v.Accountno41") != null) && (component.get("v.Accountno41") != '') ) ))
                ||
                ( (((component.get("v.BankName11") == component.get("v.BankName51")) && (component.get("v.BankName11") != undefined) && (component.get("v.BankName11") != null) && (component.get("v.BankName11") != '') && (component.get("v.BankName51") != undefined) && (component.get("v.BankName51") != null) && (component.get("v.BankName51") != '')) || ((component.get("v.BankName11") == component.get("v.BankName5")) && (component.get("v.BankName11") != undefined) && (component.get("v.BankName11") != null) && (component.get("v.BankName11") != '') && (component.get("v.BankName5") != undefined) && (component.get("v.BankName5") != null) && (component.get("v.BankName5") != '')) || ((component.get("v.BankName1") == component.get("v.BankName5")) && (component.get("v.BankName1") != undefined) && (component.get("v.BankName1") != null) && (component.get("v.BankName1") != '') && (component.get("v.BankName5") != undefined) && (component.get("v.BankName5") != null) && (component.get("v.BankName5") != '')) || ((component.get("v.BankName1") == component.get("v.BankName51")) && (component.get("v.BankName1") != undefined) && (component.get("v.BankName1") != null) && (component.get("v.BankName1") != '') && (component.get("v.BankName51") != undefined) && (component.get("v.BankName51") != null) && (component.get("v.BankName51") != '') ) )  &&
                 (((component.get("v.Accountno11") == component.get("v.Accountno51"))  && (component.get("v.Accountno11") != undefined) && (component.get("v.Accountno11") != null) && (component.get("v.Accountno11") != '') && (component.get("v.Accountno51") != undefined) && (component.get("v.Accountno51") != null) && (component.get("v.Accountno51") != '' )) || ((component.get("v.Accountno11") == component.get("v.Accountno5"))  && (component.get("v.Accountno11") != undefined) && (component.get("v.Accountno11") != null) && (component.get("v.Accountno11") != '') && (component.get("v.Accountno5") != undefined) && (component.get("v.Accountno5") != null) && (component.get("v.Accountno5") != '' ))|| ((component.get("v.Accountno1") == component.get("v.Accountno5"))  && (component.get("v.Accountno1") != undefined) && (component.get("v.Accountno1") != null) && (component.get("v.Accountno1") != '') && (component.get("v.Accountno5") != undefined) && (component.get("v.Accountno5") != null) && (component.get("v.Accountno5") != '' )) || ((component.get("v.Accountno1") == component.get("v.Accountno51"))  && (component.get("v.Accountno1") != undefined) && (component.get("v.Accountno1") != null) && (component.get("v.Accountno1") != '') && (component.get("v.Accountno51") != undefined) && (component.get("v.Accountno51") != null) && (component.get("v.Accountno51") != '') ) ))
                ||
                ( (((component.get("v.BankName11") == component.get("v.BankName61")) && (component.get("v.BankName11") != undefined) && (component.get("v.BankName11") != null) && (component.get("v.BankName11") != '') && (component.get("v.BankName61") != undefined) && (component.get("v.BankName61") != null) && (component.get("v.BankName61") != '')) || ((component.get("v.BankName11") == component.get("v.BankName6")) && (component.get("v.BankName11") != undefined) && (component.get("v.BankName11") != null) && (component.get("v.BankName11") != '') && (component.get("v.BankName6") != undefined) && (component.get("v.BankName6") != null) && (component.get("v.BankName6") != '')) || ((component.get("v.BankName1") == component.get("v.BankName6")) && (component.get("v.BankName1") != undefined) && (component.get("v.BankName1") != null) && (component.get("v.BankName1") != '') && (component.get("v.BankName6") != undefined) && (component.get("v.BankName6") != null) && (component.get("v.BankName6") != '')) || ((component.get("v.BankName1") == component.get("v.BankName61")) && (component.get("v.BankName1") != undefined) && (component.get("v.BankName1") != null) && (component.get("v.BankName1") != '') && (component.get("v.BankName61") != undefined) && (component.get("v.BankName61") != null) && (component.get("v.BankName61") != '') ) )  &&
                 (((component.get("v.Accountno11") == component.get("v.Accountno61"))  && (component.get("v.Accountno11") != undefined) && (component.get("v.Accountno11") != null) && (component.get("v.Accountno11") != '') && (component.get("v.Accountno61") != undefined) && (component.get("v.Accountno61") != null) && (component.get("v.Accountno61") != '' )) || ((component.get("v.Accountno11") == component.get("v.Accountno6"))  && (component.get("v.Accountno11") != undefined) && (component.get("v.Accountno11") != null) && (component.get("v.Accountno11") != '') && (component.get("v.Accountno6") != undefined) && (component.get("v.Accountno6") != null) && (component.get("v.Accountno6") != '' ))|| ((component.get("v.Accountno1") == component.get("v.Accountno6"))  && (component.get("v.Accountno1") != undefined) && (component.get("v.Accountno1") != null) && (component.get("v.Accountno1") != '') && (component.get("v.Accountno6") != undefined) && (component.get("v.Accountno6") != null) && (component.get("v.Accountno6") != '' )) || ((component.get("v.Accountno1") == component.get("v.Accountno61"))  && (component.get("v.Accountno1") != undefined) && (component.get("v.Accountno1") != null) && (component.get("v.Accountno1") != '') && (component.get("v.Accountno61") != undefined) && (component.get("v.Accountno61") != null) && (component.get("v.Accountno61") != '') ) ))
            ){ //Conditions added by Abhilekh for BRE2 Enhancements
                //errorMsg = true;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Duplicate Bank Details entered in section 1'
                });
                toastEvent.fire();
                this.hideSpinner(component,event,helper); //Added by Abhilekh for BRE2 Enhancements
            } else {
                if(action == "Save") {
                	this.saveHelper(component, event, helper, disableBank1, disableBank11,perfiosButtonClicked, "1",saveAll);   
                } else if(action == "Perfios") {
                    this.fetchPerfiosDataHelper(component, event, helper,disableBank1, disableBank11, "1");
                } else if(action == "Banking") {
                    this.fetchBRERecordsHelper(component, event, helper, disableBank1, disableBank11, "1");
                }
            }
        } else if(sectionno== "2") {
            if( 
                ( (((component.get("v.BankName11") == component.get("v.BankName21")) && (component.get("v.BankName11") != undefined) && (component.get("v.BankName11") != null) && (component.get("v.BankName11") != '') && (component.get("v.BankName21") != undefined) && (component.get("v.BankName21") != null) && (component.get("v.BankName21") != '')) || ((component.get("v.BankName11") == component.get("v.BankName2")) && (component.get("v.BankName11") != undefined) && (component.get("v.BankName11") != null) && (component.get("v.BankName11") != '') && (component.get("v.BankName2") != undefined) && (component.get("v.BankName2") != null) && (component.get("v.BankName2") != '')) || ((component.get("v.BankName1") == component.get("v.BankName2")) && (component.get("v.BankName1") != undefined) && (component.get("v.BankName1") != null) && (component.get("v.BankName1") != '') && (component.get("v.BankName2") != undefined) && (component.get("v.BankName2") != null) && (component.get("v.BankName2") != '')) || ((component.get("v.BankName1") == component.get("v.BankName21")) && (component.get("v.BankName1") != undefined) && (component.get("v.BankName1") != null) && (component.get("v.BankName1") != '') && (component.get("v.BankName21") != undefined) && (component.get("v.BankName21") != null) && (component.get("v.BankName21") != '') ) )  &&
                 (((component.get("v.Accountno11") == component.get("v.Accountno21"))  && (component.get("v.Accountno11") != undefined) && (component.get("v.Accountno11") != null) && (component.get("v.Accountno11") != '') && (component.get("v.Accountno21") != undefined) && (component.get("v.Accountno21") != null) && (component.get("v.Accountno21") != '' )) || ((component.get("v.Accountno11") == component.get("v.Accountno2"))  && (component.get("v.Accountno11") != undefined) && (component.get("v.Accountno11") != null) && (component.get("v.Accountno11") != '') && (component.get("v.Accountno2") != undefined) && (component.get("v.Accountno2") != null) && (component.get("v.Accountno2") != '' ))|| ((component.get("v.Accountno1") == component.get("v.Accountno2"))  && (component.get("v.Accountno1") != undefined) && (component.get("v.Accountno1") != null) && (component.get("v.Accountno1") != '') && (component.get("v.Accountno2") != undefined) && (component.get("v.Accountno2") != null) && (component.get("v.Accountno2") != '' )) || ((component.get("v.Accountno1") == component.get("v.Accountno21"))  && (component.get("v.Accountno1") != undefined) && (component.get("v.Accountno1") != null) && (component.get("v.Accountno1") != '') && (component.get("v.Accountno21") != undefined) && (component.get("v.Accountno21") != null) && (component.get("v.Accountno21") != '') ) )) 
                ||          
                ( (((component.get("v.BankName21") == component.get("v.BankName31")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '') || ((component.get("v.BankName21") == component.get("v.BankName3")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '') || ((component.get("v.BankName2") == component.get("v.BankName31")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '') || ((component.get("v.BankName2") == component.get("v.BankName3")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' ) )  &&
                 (((component.get("v.Accountno21") == component.get("v.Accountno31"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' ) || ((component.get("v.Accountno21") == component.get("v.Accountno3"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' )|| ((component.get("v.Accountno2") == component.get("v.Accountno31"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' ) || ((component.get("v.Accountno2") == component.get("v.Accountno3"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' ) ))
                ||
                ( (((component.get("v.BankName21") == component.get("v.BankName41")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName41") != undefined && component.get("v.BankName41") != null && component.get("v.BankName41") != '') || ((component.get("v.BankName21") == component.get("v.BankName4")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName4") != undefined && component.get("v.BankName4") != null && component.get("v.BankName4") != '') || ((component.get("v.BankName2") == component.get("v.BankName41")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName41") != undefined && component.get("v.BankName41") != null && component.get("v.BankName41") != '') || ((component.get("v.BankName2") == component.get("v.BankName4")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName4") != undefined && component.get("v.BankName4") != null && component.get("v.BankName4") != '' ) )  &&
                 (((component.get("v.Accountno21") == component.get("v.Accountno41"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno41") != undefined && component.get("v.Accountno41") != null && component.get("v.Accountno41") != '' ) || ((component.get("v.Accountno21") == component.get("v.Accountno4"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno4") != undefined && component.get("v.Accountno4") != null && component.get("v.Accountno4") != '' )|| ((component.get("v.Accountno2") == component.get("v.Accountno41"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno41") != undefined && component.get("v.Accountno41") != null && component.get("v.Accountno41") != '' ) || ((component.get("v.Accountno2") == component.get("v.Accountno4"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno4") != undefined && component.get("v.Accountno4") != null && component.get("v.Accountno4") != '' ) ))
                ||
                ( (((component.get("v.BankName21") == component.get("v.BankName51")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName51") != undefined && component.get("v.BankName51") != null && component.get("v.BankName51") != '') || ((component.get("v.BankName21") == component.get("v.BankName5")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName5") != undefined && component.get("v.BankName5") != null && component.get("v.BankName5") != '') || ((component.get("v.BankName2") == component.get("v.BankName51")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName51") != undefined && component.get("v.BankName51") != null && component.get("v.BankName51") != '') || ((component.get("v.BankName2") == component.get("v.BankName5")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName5") != undefined && component.get("v.BankName5") != null && component.get("v.BankName5") != '' ) )  &&
                 (((component.get("v.Accountno21") == component.get("v.Accountno51"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno51") != undefined && component.get("v.Accountno51") != null && component.get("v.Accountno51") != '' ) || ((component.get("v.Accountno21") == component.get("v.Accountno5"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno5") != undefined && component.get("v.Accountno5") != null && component.get("v.Accountno5") != '' )|| ((component.get("v.Accountno2") == component.get("v.Accountno51"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno51") != undefined && component.get("v.Accountno51") != null && component.get("v.Accountno51") != '' ) || ((component.get("v.Accountno2") == component.get("v.Accountno5"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno5") != undefined && component.get("v.Accountno5") != null && component.get("v.Accountno5") != '' ) ))
                ||
                ( (((component.get("v.BankName21") == component.get("v.BankName61")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName61") != undefined && component.get("v.BankName61") != null && component.get("v.BankName61") != '') || ((component.get("v.BankName21") == component.get("v.BankName6")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName6") != undefined && component.get("v.BankName6") != null && component.get("v.BankName6") != '') || ((component.get("v.BankName2") == component.get("v.BankName61")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName61") != undefined && component.get("v.BankName61") != null && component.get("v.BankName61") != '') || ((component.get("v.BankName2") == component.get("v.BankName6")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName6") != undefined && component.get("v.BankName6") != null && component.get("v.BankName6") != '' ) )  &&
                 (((component.get("v.Accountno21") == component.get("v.Accountno61"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno61") != undefined && component.get("v.Accountno61") != null && component.get("v.Accountno61") != '' ) || ((component.get("v.Accountno21") == component.get("v.Accountno6"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno6") != undefined && component.get("v.Accountno6") != null && component.get("v.Accountno6") != '' )|| ((component.get("v.Accountno2") == component.get("v.Accountno61"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno61") != undefined && component.get("v.Accountno61") != null && component.get("v.Accountno61") != '' ) || ((component.get("v.Accountno2") == component.get("v.Accountno6"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno6") != undefined && component.get("v.Accountno6") != null && component.get("v.Accountno6") != '' ) ))
            ){ //Conditions added by Abhilekh for BRE2 Enhancements
                //errorMsg = true;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Duplicate Bank Details entered in section 2'
                });
                toastEvent.fire();
                this.hideSpinner(component,event,helper); //Added by Abhilekh for BRE2 Enhancements
            } else {
                 if(action == "Save") {
                	this.saveHelper(component, event, helper,disableBank1, disableBank11,perfiosButtonClicked, "2",saveAll); 
                 } else if(action == "Perfios") {
                     this.fetchPerfiosDataHelper(component, event, helper,disableBank1, disableBank11, "2");
                 } else if(action == "Banking") {
                     this.fetchBRERecordsHelper(component, event, helper, disableBank1, disableBank11, "2");
                 }
            }

        } else if(sectionno =="3") {
            if(              
                ( (((component.get("v.BankName21") == component.get("v.BankName31")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '') || ((component.get("v.BankName21") == component.get("v.BankName3")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '') || ((component.get("v.BankName2") == component.get("v.BankName31")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '') || ((component.get("v.BankName2") == component.get("v.BankName3")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' ) )  &&
                 (((component.get("v.Accountno21") == component.get("v.Accountno31"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' ) || ((component.get("v.Accountno21") == component.get("v.Accountno3"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' )|| ((component.get("v.Accountno2") == component.get("v.Accountno31"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' ) || ((component.get("v.Accountno2") == component.get("v.Accountno3"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' ) )) 
                ||            
                ( (((component.get("v.BankName31") == component.get("v.BankName11")) && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '' && component.get("v.BankName11") != undefined && component.get("v.BankName11") != null && component.get("v.BankName11") != '') || ((component.get("v.BankName31") == component.get("v.BankName1")) && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '' && component.get("v.BankName1") != undefined && component.get("v.BankName1") != null && component.get("v.BankName1") != '') || ((component.get("v.BankName3") == component.get("v.BankName11")) && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' && component.get("v.BankName11") != undefined && component.get("v.BankName11") != null && component.get("v.BankName11") != '') || ((component.get("v.BankName3") == component.get("v.BankName1")) && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' && component.get("v.BankName1") != undefined && component.get("v.BankName1") != null && component.get("v.BankName1") != '' ) )  &&
                 (((component.get("v.Accountno31") == component.get("v.Accountno11"))  && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' && component.get("v.Accountno11") != undefined && component.get("v.Accountno11") != null && component.get("v.Accountno11") != '' ) || ((component.get("v.Accountno31") == component.get("v.Accountno1"))  && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' && component.get("v.Accountno1") != undefined && component.get("v.Accountno1") != null && component.get("v.Accountno1") != '' )|| ((component.get("v.Accountno3") == component.get("v.Accountno11"))  && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' && component.get("v.Accountno11") != undefined && component.get("v.Accountno11") != null && component.get("v.Accountno11") != '' ) || ((component.get("v.Accountno3") == component.get("v.Accountno1"))  && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' && component.get("v.Accountno1") != undefined && component.get("v.Accountno1") != null && component.get("v.Accountno1") != '' ) ))
                ||
                ( (((component.get("v.BankName31") == component.get("v.BankName41")) && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '' && component.get("v.BankName41") != undefined && component.get("v.BankName41") != null && component.get("v.BankName41") != '') || ((component.get("v.BankName31") == component.get("v.BankName4")) && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '' && component.get("v.BankName4") != undefined && component.get("v.BankName4") != null && component.get("v.BankName4") != '') || ((component.get("v.BankName3") == component.get("v.BankName41")) && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' && component.get("v.BankName41") != undefined && component.get("v.BankName41") != null && component.get("v.BankName41") != '') || ((component.get("v.BankName3") == component.get("v.BankName4")) && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' && component.get("v.BankName4") != undefined && component.get("v.BankName4") != null && component.get("v.BankName4") != '' ) )  &&
                 (((component.get("v.Accountno31") == component.get("v.Accountno41"))  && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' && component.get("v.Accountno41") != undefined && component.get("v.Accountno41") != null && component.get("v.Accountno41") != '' ) || ((component.get("v.Accountno31") == component.get("v.Accountno4"))  && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' && component.get("v.Accountno4") != undefined && component.get("v.Accountno4") != null && component.get("v.Accountno4") != '' )|| ((component.get("v.Accountno3") == component.get("v.Accountno41"))  && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' && component.get("v.Accountno41") != undefined && component.get("v.Accountno41") != null && component.get("v.Accountno41") != '' ) || ((component.get("v.Accountno3") == component.get("v.Accountno4"))  && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' && component.get("v.Accountno4") != undefined && component.get("v.Accountno4") != null && component.get("v.Accountno4") != '' ) ))
                ||
                ( (((component.get("v.BankName31") == component.get("v.BankName51")) && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '' && component.get("v.BankName51") != undefined && component.get("v.BankName51") != null && component.get("v.BankName51") != '') || ((component.get("v.BankName31") == component.get("v.BankName5")) && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '' && component.get("v.BankName5") != undefined && component.get("v.BankName5") != null && component.get("v.BankName5") != '') || ((component.get("v.BankName3") == component.get("v.BankName51")) && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' && component.get("v.BankName51") != undefined && component.get("v.BankName51") != null && component.get("v.BankName51") != '') || ((component.get("v.BankName3") == component.get("v.BankName5")) && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' && component.get("v.BankName5") != undefined && component.get("v.BankName5") != null && component.get("v.BankName5") != '' ) )  &&
                 (((component.get("v.Accountno31") == component.get("v.Accountno51"))  && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' && component.get("v.Accountno51") != undefined && component.get("v.Accountno51") != null && component.get("v.Accountno51") != '' ) || ((component.get("v.Accountno31") == component.get("v.Accountno5"))  && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' && component.get("v.Accountno5") != undefined && component.get("v.Accountno5") != null && component.get("v.Accountno5") != '' )|| ((component.get("v.Accountno3") == component.get("v.Accountno51"))  && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' && component.get("v.Accountno51") != undefined && component.get("v.Accountno51") != null && component.get("v.Accountno51") != '' ) || ((component.get("v.Accountno3") == component.get("v.Accountno5"))  && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' && component.get("v.Accountno5") != undefined && component.get("v.Accountno5") != null && component.get("v.Accountno5") != '' ) ))
                ||
                ( (((component.get("v.BankName31") == component.get("v.BankName61")) && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '' && component.get("v.BankName61") != undefined && component.get("v.BankName61") != null && component.get("v.BankName61") != '') || ((component.get("v.BankName31") == component.get("v.BankName6")) && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '' && component.get("v.BankName6") != undefined && component.get("v.BankName6") != null && component.get("v.BankName6") != '') || ((component.get("v.BankName3") == component.get("v.BankName61")) && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' && component.get("v.BankName61") != undefined && component.get("v.BankName61") != null && component.get("v.BankName61") != '') || ((component.get("v.BankName3") == component.get("v.BankName6")) && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' && component.get("v.BankName6") != undefined && component.get("v.BankName6") != null && component.get("v.BankName6") != '' ) )  &&
                 (((component.get("v.Accountno31") == component.get("v.Accountno61"))  && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' && component.get("v.Accountno61") != undefined && component.get("v.Accountno61") != null && component.get("v.Accountno61") != '' ) || ((component.get("v.Accountno31") == component.get("v.Accountno6"))  && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' && component.get("v.Accountno6") != undefined && component.get("v.Accountno6") != null && component.get("v.Accountno6") != '' )|| ((component.get("v.Accountno3") == component.get("v.Accountno61"))  && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' && component.get("v.Accountno61") != undefined && component.get("v.Accountno61") != null && component.get("v.Accountno61") != '' ) || ((component.get("v.Accountno3") == component.get("v.Accountno6"))  && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' && component.get("v.Accountno6") != undefined && component.get("v.Accountno6") != null && component.get("v.Accountno6") != '' ) ))
            ){ //Conditions added by Abhilekh for BRE2 Enhancements
                //errorMsg = true;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Duplicate Bank Details entered in section 3'
                });
                toastEvent.fire();
                this.hideSpinner(component,event,helper); //Added by Abhilekh for BRE2 Enhancements
            } else {
                 if(action == "Save") {
                	this.saveHelper(component, event, helper,disableBank1, disableBank11,perfiosButtonClicked, "3",saveAll);
                 } else if(action == "Perfios") {
                    this.fetchPerfiosDataHelper(component, event, helper,disableBank1, disableBank11, "3"); 
                 }  else if(action == "Banking") {
                     helper.fetchBRERecordsHelper(component, event, helper, disableBank1, disableBank11, "3");
                 }  
            }
        }
        
        //Added by Abhilekh for BRE2 Enhancements
        else if(sectionno =="4") {
            if(              
                ( (((component.get("v.BankName21") == component.get("v.BankName41")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName41") != undefined && component.get("v.BankName41") != null && component.get("v.BankName41") != '') || ((component.get("v.BankName21") == component.get("v.BankName4")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName4") != undefined && component.get("v.BankName4") != null && component.get("v.BankName4") != '') || ((component.get("v.BankName2") == component.get("v.BankName41")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName41") != undefined && component.get("v.BankName41") != null && component.get("v.BankName41") != '') || ((component.get("v.BankName2") == component.get("v.BankName4")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName4") != undefined && component.get("v.BankName4") != null && component.get("v.BankName4") != '' ) )  &&
                 (((component.get("v.Accountno21") == component.get("v.Accountno41"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno41") != undefined && component.get("v.Accountno41") != null && component.get("v.Accountno41") != '' ) || ((component.get("v.Accountno21") == component.get("v.Accountno4"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno4") != undefined && component.get("v.Accountno4") != null && component.get("v.Accountno4") != '' )|| ((component.get("v.Accountno2") == component.get("v.Accountno41"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno41") != undefined && component.get("v.Accountno41") != null && component.get("v.Accountno41") != '' ) || ((component.get("v.Accountno2") == component.get("v.Accountno4"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno4") != undefined && component.get("v.Accountno4") != null && component.get("v.Accountno4") != '' ) )) 
                ||            
                ( (((component.get("v.BankName41") == component.get("v.BankName11")) && component.get("v.BankName41") != undefined && component.get("v.BankName41") != null && component.get("v.BankName41") != '' && component.get("v.BankName11") != undefined && component.get("v.BankName11") != null && component.get("v.BankName11") != '') || ((component.get("v.BankName41") == component.get("v.BankName1")) && component.get("v.BankName41") != undefined && component.get("v.BankName41") != null && component.get("v.BankName41") != '' && component.get("v.BankName1") != undefined && component.get("v.BankName1") != null && component.get("v.BankName1") != '') || ((component.get("v.BankName4") == component.get("v.BankName11")) && component.get("v.BankName4") != undefined && component.get("v.BankName4") != null && component.get("v.BankName4") != '' && component.get("v.BankName11") != undefined && component.get("v.BankName11") != null && component.get("v.BankName11") != '') || ((component.get("v.BankName4") == component.get("v.BankName1")) && component.get("v.BankName4") != undefined && component.get("v.BankName4") != null && component.get("v.BankName4") != '' && component.get("v.BankName1") != undefined && component.get("v.BankName1") != null && component.get("v.BankName1") != '' ) )  &&
                 (((component.get("v.Accountno41") == component.get("v.Accountno11"))  && component.get("v.Accountno41") != undefined && component.get("v.Accountno41") != null && component.get("v.Accountno41") != '' && component.get("v.Accountno11") != undefined && component.get("v.Accountno11") != null && component.get("v.Accountno11") != '' ) || ((component.get("v.Accountno41") == component.get("v.Accountno1"))  && component.get("v.Accountno41") != undefined && component.get("v.Accountno41") != null && component.get("v.Accountno41") != '' && component.get("v.Accountno1") != undefined && component.get("v.Accountno1") != null && component.get("v.Accountno1") != '' )|| ((component.get("v.Accountno4") == component.get("v.Accountno11"))  && component.get("v.Accountno4") != undefined && component.get("v.Accountno4") != null && component.get("v.Accountno4") != '' && component.get("v.Accountno11") != undefined && component.get("v.Accountno11") != null && component.get("v.Accountno11") != '' ) || ((component.get("v.Accountno4") == component.get("v.Accountno1"))  && component.get("v.Accountno4") != undefined && component.get("v.Accountno4") != null && component.get("v.Accountno4") != '' && component.get("v.Accountno1") != undefined && component.get("v.Accountno1") != null && component.get("v.Accountno1") != '' ) ))
                ||
                ( (((component.get("v.BankName41") == component.get("v.BankName31")) && component.get("v.BankName41") != undefined && component.get("v.BankName41") != null && component.get("v.BankName41") != '' && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '') || ((component.get("v.BankName41") == component.get("v.BankName3")) && component.get("v.BankName41") != undefined && component.get("v.BankName41") != null && component.get("v.BankName41") != '' && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '') || ((component.get("v.BankName4") == component.get("v.BankName31")) && component.get("v.BankName4") != undefined && component.get("v.BankName4") != null && component.get("v.BankName4") != '' && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '') || ((component.get("v.BankName4") == component.get("v.BankName3")) && component.get("v.BankName4") != undefined && component.get("v.BankName4") != null && component.get("v.BankName4") != '' && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' ) )  &&
                 (((component.get("v.Accountno41") == component.get("v.Accountno31"))  && component.get("v.Accountno41") != undefined && component.get("v.Accountno41") != null && component.get("v.Accountno41") != '' && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' ) || ((component.get("v.Accountno41") == component.get("v.Accountno3"))  && component.get("v.Accountno41") != undefined && component.get("v.Accountno41") != null && component.get("v.Accountno41") != '' && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' )|| ((component.get("v.Accountno4") == component.get("v.Accountno31"))  && component.get("v.Accountno4") != undefined && component.get("v.Accountno4") != null && component.get("v.Accountno4") != '' && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' ) || ((component.get("v.Accountno4") == component.get("v.Accountno3"))  && component.get("v.Accountno4") != undefined && component.get("v.Accountno4") != null && component.get("v.Accountno4") != '' && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' ) ))
                ||
                ( (((component.get("v.BankName41") == component.get("v.BankName51")) && component.get("v.BankName41") != undefined && component.get("v.BankName41") != null && component.get("v.BankName41") != '' && component.get("v.BankName51") != undefined && component.get("v.BankName51") != null && component.get("v.BankName51") != '') || ((component.get("v.BankName41") == component.get("v.BankName5")) && component.get("v.BankName41") != undefined && component.get("v.BankName41") != null && component.get("v.BankName41") != '' && component.get("v.BankName5") != undefined && component.get("v.BankName5") != null && component.get("v.BankName5") != '') || ((component.get("v.BankName4") == component.get("v.BankName51")) && component.get("v.BankName4") != undefined && component.get("v.BankName4") != null && component.get("v.BankName4") != '' && component.get("v.BankName51") != undefined && component.get("v.BankName51") != null && component.get("v.BankName51") != '') || ((component.get("v.BankName4") == component.get("v.BankName5")) && component.get("v.BankName4") != undefined && component.get("v.BankName4") != null && component.get("v.BankName4") != '' && component.get("v.BankName5") != undefined && component.get("v.BankName5") != null && component.get("v.BankName5") != '' ) )  &&
                 (((component.get("v.Accountno41") == component.get("v.Accountno51"))  && component.get("v.Accountno41") != undefined && component.get("v.Accountno41") != null && component.get("v.Accountno41") != '' && component.get("v.Accountno51") != undefined && component.get("v.Accountno51") != null && component.get("v.Accountno51") != '' ) || ((component.get("v.Accountno41") == component.get("v.Accountno5"))  && component.get("v.Accountno41") != undefined && component.get("v.Accountno41") != null && component.get("v.Accountno41") != '' && component.get("v.Accountno5") != undefined && component.get("v.Accountno5") != null && component.get("v.Accountno5") != '' )|| ((component.get("v.Accountno4") == component.get("v.Accountno51"))  && component.get("v.Accountno4") != undefined && component.get("v.Accountno4") != null && component.get("v.Accountno4") != '' && component.get("v.Accountno51") != undefined && component.get("v.Accountno51") != null && component.get("v.Accountno51") != '' ) || ((component.get("v.Accountno4") == component.get("v.Accountno5"))  && component.get("v.Accountno4") != undefined && component.get("v.Accountno4") != null && component.get("v.Accountno4") != '' && component.get("v.Accountno5") != undefined && component.get("v.Accountno5") != null && component.get("v.Accountno5") != '' ) ))
                ||
                ( (((component.get("v.BankName41") == component.get("v.BankName61")) && component.get("v.BankName41") != undefined && component.get("v.BankName41") != null && component.get("v.BankName41") != '' && component.get("v.BankName61") != undefined && component.get("v.BankName61") != null && component.get("v.BankName61") != '') || ((component.get("v.BankName41") == component.get("v.BankName6")) && component.get("v.BankName41") != undefined && component.get("v.BankName41") != null && component.get("v.BankName41") != '' && component.get("v.BankName6") != undefined && component.get("v.BankName6") != null && component.get("v.BankName6") != '') || ((component.get("v.BankName4") == component.get("v.BankName61")) && component.get("v.BankName4") != undefined && component.get("v.BankName4") != null && component.get("v.BankName4") != '' && component.get("v.BankName61") != undefined && component.get("v.BankName61") != null && component.get("v.BankName61") != '') || ((component.get("v.BankName4") == component.get("v.BankName6")) && component.get("v.BankName4") != undefined && component.get("v.BankName4") != null && component.get("v.BankName4") != '' && component.get("v.BankName6") != undefined && component.get("v.BankName6") != null && component.get("v.BankName6") != '' ) )  &&
                 (((component.get("v.Accountno41") == component.get("v.Accountno61"))  && component.get("v.Accountno41") != undefined && component.get("v.Accountno41") != null && component.get("v.Accountno41") != '' && component.get("v.Accountno61") != undefined && component.get("v.Accountno61") != null && component.get("v.Accountno61") != '' ) || ((component.get("v.Accountno41") == component.get("v.Accountno6"))  && component.get("v.Accountno41") != undefined && component.get("v.Accountno41") != null && component.get("v.Accountno41") != '' && component.get("v.Accountno6") != undefined && component.get("v.Accountno6") != null && component.get("v.Accountno6") != '' )|| ((component.get("v.Accountno4") == component.get("v.Accountno61"))  && component.get("v.Accountno4") != undefined && component.get("v.Accountno4") != null && component.get("v.Accountno4") != '' && component.get("v.Accountno61") != undefined && component.get("v.Accountno61") != null && component.get("v.Accountno61") != '' ) || ((component.get("v.Accountno4") == component.get("v.Accountno6"))  && component.get("v.Accountno4") != undefined && component.get("v.Accountno4") != null && component.get("v.Accountno4") != '' && component.get("v.Accountno6") != undefined && component.get("v.Accountno6") != null && component.get("v.Accountno6") != '' ) ))
            ){
                //errorMsg = true;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Duplicate Bank Details entered in section 4'
                });
                toastEvent.fire();
                this.hideSpinner(component,event,helper);
            } else {
                 if(action == "Save") {
                	this.saveHelper(component, event, helper,disableBank1, disableBank11,perfiosButtonClicked, "4",saveAll);
                 } else if(action == "Perfios") {
                    this.fetchPerfiosDataHelper(component, event, helper,disableBank1, disableBank11, "4"); 
                 }  else if(action == "Banking") {
                     helper.fetchBRERecordsHelper(component, event, helper, disableBank1, disableBank11, "4");
                 }  
            }
        }
        
        //Added by Abhilekh for BRE2 Enhancements
        else if(sectionno =="5") {
            if(              
                ( (((component.get("v.BankName21") == component.get("v.BankName51")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName51") != undefined && component.get("v.BankName51") != null && component.get("v.BankName51") != '') || ((component.get("v.BankName21") == component.get("v.BankName5")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName5") != undefined && component.get("v.BankName5") != null && component.get("v.BankName5") != '') || ((component.get("v.BankName2") == component.get("v.BankName51")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName51") != undefined && component.get("v.BankName51") != null && component.get("v.BankName51") != '') || ((component.get("v.BankName2") == component.get("v.BankName5")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName5") != undefined && component.get("v.BankName5") != null && component.get("v.BankName5") != '' ) )  &&
                 (((component.get("v.Accountno21") == component.get("v.Accountno51"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno51") != undefined && component.get("v.Accountno51") != null && component.get("v.Accountno51") != '' ) || ((component.get("v.Accountno21") == component.get("v.Accountno5"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno5") != undefined && component.get("v.Accountno5") != null && component.get("v.Accountno5") != '' )|| ((component.get("v.Accountno2") == component.get("v.Accountno51"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno51") != undefined && component.get("v.Accountno51") != null && component.get("v.Accountno51") != '' ) || ((component.get("v.Accountno2") == component.get("v.Accountno5"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno5") != undefined && component.get("v.Accountno5") != null && component.get("v.Accountno5") != '' ) )) 
                ||            
                ( (((component.get("v.BankName51") == component.get("v.BankName11")) && component.get("v.BankName51") != undefined && component.get("v.BankName51") != null && component.get("v.BankName51") != '' && component.get("v.BankName11") != undefined && component.get("v.BankName11") != null && component.get("v.BankName11") != '') || ((component.get("v.BankName51") == component.get("v.BankName1")) && component.get("v.BankName51") != undefined && component.get("v.BankName51") != null && component.get("v.BankName51") != '' && component.get("v.BankName1") != undefined && component.get("v.BankName1") != null && component.get("v.BankName1") != '') || ((component.get("v.BankName5") == component.get("v.BankName11")) && component.get("v.BankName5") != undefined && component.get("v.BankName5") != null && component.get("v.BankName5") != '' && component.get("v.BankName11") != undefined && component.get("v.BankName11") != null && component.get("v.BankName11") != '') || ((component.get("v.BankName5") == component.get("v.BankName1")) && component.get("v.BankName5") != undefined && component.get("v.BankName5") != null && component.get("v.BankName5") != '' && component.get("v.BankName1") != undefined && component.get("v.BankName1") != null && component.get("v.BankName1") != '' ) )  &&
                 (((component.get("v.Accountno51") == component.get("v.Accountno11"))  && component.get("v.Accountno51") != undefined && component.get("v.Accountno51") != null && component.get("v.Accountno51") != '' && component.get("v.Accountno11") != undefined && component.get("v.Accountno11") != null && component.get("v.Accountno11") != '' ) || ((component.get("v.Accountno51") == component.get("v.Accountno1"))  && component.get("v.Accountno51") != undefined && component.get("v.Accountno51") != null && component.get("v.Accountno51") != '' && component.get("v.Accountno1") != undefined && component.get("v.Accountno1") != null && component.get("v.Accountno1") != '' )|| ((component.get("v.Accountno5") == component.get("v.Accountno11"))  && component.get("v.Accountno5") != undefined && component.get("v.Accountno5") != null && component.get("v.Accountno5") != '' && component.get("v.Accountno11") != undefined && component.get("v.Accountno11") != null && component.get("v.Accountno11") != '' ) || ((component.get("v.Accountno5") == component.get("v.Accountno1"))  && component.get("v.Accountno5") != undefined && component.get("v.Accountno5") != null && component.get("v.Accountno5") != '' && component.get("v.Accountno1") != undefined && component.get("v.Accountno1") != null && component.get("v.Accountno1") != '' ) ))
                ||
                ( (((component.get("v.BankName51") == component.get("v.BankName41")) && component.get("v.BankName51") != undefined && component.get("v.BankName51") != null && component.get("v.BankName51") != '' && component.get("v.BankName41") != undefined && component.get("v.BankName41") != null && component.get("v.BankName41") != '') || ((component.get("v.BankName51") == component.get("v.BankName4")) && component.get("v.BankName51") != undefined && component.get("v.BankName51") != null && component.get("v.BankName51") != '' && component.get("v.BankName4") != undefined && component.get("v.BankName4") != null && component.get("v.BankName4") != '') || ((component.get("v.BankName5") == component.get("v.BankName41")) && component.get("v.BankName5") != undefined && component.get("v.BankName5") != null && component.get("v.BankName5") != '' && component.get("v.BankName41") != undefined && component.get("v.BankName41") != null && component.get("v.BankName41") != '') || ((component.get("v.BankName5") == component.get("v.BankName4")) && component.get("v.BankName5") != undefined && component.get("v.BankName5") != null && component.get("v.BankName5") != '' && component.get("v.BankName4") != undefined && component.get("v.BankName4") != null && component.get("v.BankName4") != '' ) )  &&
                 (((component.get("v.Accountno51") == component.get("v.Accountno41"))  && component.get("v.Accountno51") != undefined && component.get("v.Accountno51") != null && component.get("v.Accountno51") != '' && component.get("v.Accountno41") != undefined && component.get("v.Accountno41") != null && component.get("v.Accountno41") != '' ) || ((component.get("v.Accountno51") == component.get("v.Accountno4"))  && component.get("v.Accountno51") != undefined && component.get("v.Accountno51") != null && component.get("v.Accountno51") != '' && component.get("v.Accountno4") != undefined && component.get("v.Accountno4") != null && component.get("v.Accountno4") != '' )|| ((component.get("v.Accountno5") == component.get("v.Accountno41"))  && component.get("v.Accountno5") != undefined && component.get("v.Accountno5") != null && component.get("v.Accountno5") != '' && component.get("v.Accountno41") != undefined && component.get("v.Accountno41") != null && component.get("v.Accountno41") != '' ) || ((component.get("v.Accountno5") == component.get("v.Accountno4"))  && component.get("v.Accountno5") != undefined && component.get("v.Accountno5") != null && component.get("v.Accountno5") != '' && component.get("v.Accountno4") != undefined && component.get("v.Accountno4") != null && component.get("v.Accountno4") != '' ) ))
                ||
                ( (((component.get("v.BankName51") == component.get("v.BankName31")) && component.get("v.BankName51") != undefined && component.get("v.BankName51") != null && component.get("v.BankName51") != '' && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '') || ((component.get("v.BankName51") == component.get("v.BankName3")) && component.get("v.BankName51") != undefined && component.get("v.BankName51") != null && component.get("v.BankName51") != '' && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '') || ((component.get("v.BankName5") == component.get("v.BankName31")) && component.get("v.BankName5") != undefined && component.get("v.BankName5") != null && component.get("v.BankName5") != '' && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '') || ((component.get("v.BankName5") == component.get("v.BankName3")) && component.get("v.BankName5") != undefined && component.get("v.BankName5") != null && component.get("v.BankName5") != '' && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' ) )  &&
                 (((component.get("v.Accountno51") == component.get("v.Accountno31"))  && component.get("v.Accountno51") != undefined && component.get("v.Accountno51") != null && component.get("v.Accountno51") != '' && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' ) || ((component.get("v.Accountno51") == component.get("v.Accountno3"))  && component.get("v.Accountno51") != undefined && component.get("v.Accountno51") != null && component.get("v.Accountno51") != '' && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' )|| ((component.get("v.Accountno5") == component.get("v.Accountno31"))  && component.get("v.Accountno5") != undefined && component.get("v.Accountno5") != null && component.get("v.Accountno5") != '' && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' ) || ((component.get("v.Accountno5") == component.get("v.Accountno3"))  && component.get("v.Accountno5") != undefined && component.get("v.Accountno5") != null && component.get("v.Accountno5") != '' && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' ) ))
                ||
                ( (((component.get("v.BankName51") == component.get("v.BankName61")) && component.get("v.BankName51") != undefined && component.get("v.BankName51") != null && component.get("v.BankName51") != '' && component.get("v.BankName61") != undefined && component.get("v.BankName61") != null && component.get("v.BankName61") != '') || ((component.get("v.BankName51") == component.get("v.BankName6")) && component.get("v.BankName51") != undefined && component.get("v.BankName51") != null && component.get("v.BankName51") != '' && component.get("v.BankName6") != undefined && component.get("v.BankName6") != null && component.get("v.BankName6") != '') || ((component.get("v.BankName5") == component.get("v.BankName61")) && component.get("v.BankName5") != undefined && component.get("v.BankName5") != null && component.get("v.BankName5") != '' && component.get("v.BankName61") != undefined && component.get("v.BankName61") != null && component.get("v.BankName61") != '') || ((component.get("v.BankName5") == component.get("v.BankName6")) && component.get("v.BankName5") != undefined && component.get("v.BankName5") != null && component.get("v.BankName5") != '' && component.get("v.BankName6") != undefined && component.get("v.BankName6") != null && component.get("v.BankName6") != '' ) )  &&
                 (((component.get("v.Accountno51") == component.get("v.Accountno61"))  && component.get("v.Accountno51") != undefined && component.get("v.Accountno51") != null && component.get("v.Accountno51") != '' && component.get("v.Accountno61") != undefined && component.get("v.Accountno61") != null && component.get("v.Accountno61") != '' ) || ((component.get("v.Accountno51") == component.get("v.Accountno6"))  && component.get("v.Accountno51") != undefined && component.get("v.Accountno51") != null && component.get("v.Accountno51") != '' && component.get("v.Accountno6") != undefined && component.get("v.Accountno6") != null && component.get("v.Accountno6") != '' )|| ((component.get("v.Accountno5") == component.get("v.Accountno61"))  && component.get("v.Accountno5") != undefined && component.get("v.Accountno5") != null && component.get("v.Accountno5") != '' && component.get("v.Accountno61") != undefined && component.get("v.Accountno61") != null && component.get("v.Accountno61") != '' ) || ((component.get("v.Accountno5") == component.get("v.Accountno6"))  && component.get("v.Accountno5") != undefined && component.get("v.Accountno5") != null && component.get("v.Accountno5") != '' && component.get("v.Accountno6") != undefined && component.get("v.Accountno6") != null && component.get("v.Accountno6") != '' ) ))
            ){
                //errorMsg = true;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Duplicate Bank Details entered in section 5'
                });
                toastEvent.fire();
                this.hideSpinner(component,event,helper);
            } else {
                 if(action == "Save") {
                	this.saveHelper(component, event, helper,disableBank1, disableBank11,perfiosButtonClicked, "5",saveAll);
                 } else if(action == "Perfios") {
                    this.fetchPerfiosDataHelper(component, event, helper,disableBank1, disableBank11, "5"); 
                 }  else if(action == "Banking") {
                     helper.fetchBRERecordsHelper(component, event, helper, disableBank1, disableBank11, "5");
                 }  
            }
        }
        
        //Added by Abhilekh for BRE2 Enhancements
        else if(sectionno =="6") {
            if(              
                ( (((component.get("v.BankName21") == component.get("v.BankName61")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName61") != undefined && component.get("v.BankName61") != null && component.get("v.BankName61") != '') || ((component.get("v.BankName21") == component.get("v.BankName6")) && component.get("v.BankName21") != undefined && component.get("v.BankName21") != null && component.get("v.BankName21") != '' && component.get("v.BankName6") != undefined && component.get("v.BankName6") != null && component.get("v.BankName6") != '') || ((component.get("v.BankName2") == component.get("v.BankName61")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName61") != undefined && component.get("v.BankName61") != null && component.get("v.BankName61") != '') || ((component.get("v.BankName2") == component.get("v.BankName6")) && component.get("v.BankName2") != undefined && component.get("v.BankName2") != null && component.get("v.BankName2") != '' && component.get("v.BankName6") != undefined && component.get("v.BankName6") != null && component.get("v.BankName6") != '' ) )  &&
                 (((component.get("v.Accountno21") == component.get("v.Accountno61"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno61") != undefined && component.get("v.Accountno61") != null && component.get("v.Accountno61") != '' ) || ((component.get("v.Accountno21") == component.get("v.Accountno6"))  && component.get("v.Accountno21") != undefined && component.get("v.Accountno21") != null && component.get("v.Accountno21") != '' && component.get("v.Accountno6") != undefined && component.get("v.Accountno6") != null && component.get("v.Accountno6") != '' )|| ((component.get("v.Accountno2") == component.get("v.Accountno61"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno61") != undefined && component.get("v.Accountno61") != null && component.get("v.Accountno61") != '' ) || ((component.get("v.Accountno2") == component.get("v.Accountno6"))  && component.get("v.Accountno2") != undefined && component.get("v.Accountno2") != null && component.get("v.Accountno2") != '' && component.get("v.Accountno6") != undefined && component.get("v.Accountno6") != null && component.get("v.Accountno6") != '' ) )) 
                ||            
                ( (((component.get("v.BankName61") == component.get("v.BankName11")) && component.get("v.BankName61") != undefined && component.get("v.BankName61") != null && component.get("v.BankName61") != '' && component.get("v.BankName11") != undefined && component.get("v.BankName11") != null && component.get("v.BankName11") != '') || ((component.get("v.BankName61") == component.get("v.BankName1")) && component.get("v.BankName61") != undefined && component.get("v.BankName61") != null && component.get("v.BankName61") != '' && component.get("v.BankName1") != undefined && component.get("v.BankName1") != null && component.get("v.BankName1") != '') || ((component.get("v.BankName6") == component.get("v.BankName11")) && component.get("v.BankName6") != undefined && component.get("v.BankName6") != null && component.get("v.BankName6") != '' && component.get("v.BankName11") != undefined && component.get("v.BankName11") != null && component.get("v.BankName11") != '') || ((component.get("v.BankName6") == component.get("v.BankName1")) && component.get("v.BankName6") != undefined && component.get("v.BankName6") != null && component.get("v.BankName6") != '' && component.get("v.BankName1") != undefined && component.get("v.BankName1") != null && component.get("v.BankName1") != '' ) )  &&
                 (((component.get("v.Accountno61") == component.get("v.Accountno11"))  && component.get("v.Accountno61") != undefined && component.get("v.Accountno61") != null && component.get("v.Accountno61") != '' && component.get("v.Accountno11") != undefined && component.get("v.Accountno11") != null && component.get("v.Accountno11") != '' ) || ((component.get("v.Accountno61") == component.get("v.Accountno1"))  && component.get("v.Accountno61") != undefined && component.get("v.Accountno61") != null && component.get("v.Accountno61") != '' && component.get("v.Accountno1") != undefined && component.get("v.Accountno1") != null && component.get("v.Accountno1") != '' )|| ((component.get("v.Accountno6") == component.get("v.Accountno11"))  && component.get("v.Accountno6") != undefined && component.get("v.Accountno6") != null && component.get("v.Accountno6") != '' && component.get("v.Accountno11") != undefined && component.get("v.Accountno11") != null && component.get("v.Accountno11") != '' ) || ((component.get("v.Accountno6") == component.get("v.Accountno1"))  && component.get("v.Accountno6") != undefined && component.get("v.Accountno6") != null && component.get("v.Accountno6") != '' && component.get("v.Accountno1") != undefined && component.get("v.Accountno1") != null && component.get("v.Accountno1") != '' ) ))
                ||
                ( (((component.get("v.BankName61") == component.get("v.BankName41")) && component.get("v.BankName61") != undefined && component.get("v.BankName61") != null && component.get("v.BankName61") != '' && component.get("v.BankName41") != undefined && component.get("v.BankName41") != null && component.get("v.BankName41") != '') || ((component.get("v.BankName61") == component.get("v.BankName4")) && component.get("v.BankName61") != undefined && component.get("v.BankName61") != null && component.get("v.BankName61") != '' && component.get("v.BankName4") != undefined && component.get("v.BankName4") != null && component.get("v.BankName4") != '') || ((component.get("v.BankName6") == component.get("v.BankName41")) && component.get("v.BankName6") != undefined && component.get("v.BankName6") != null && component.get("v.BankName6") != '' && component.get("v.BankName41") != undefined && component.get("v.BankName41") != null && component.get("v.BankName41") != '') || ((component.get("v.BankName6") == component.get("v.BankName4")) && component.get("v.BankName6") != undefined && component.get("v.BankName6") != null && component.get("v.BankName6") != '' && component.get("v.BankName4") != undefined && component.get("v.BankName4") != null && component.get("v.BankName4") != '' ) )  &&
                 (((component.get("v.Accountno61") == component.get("v.Accountno41"))  && component.get("v.Accountno61") != undefined && component.get("v.Accountno61") != null && component.get("v.Accountno61") != '' && component.get("v.Accountno41") != undefined && component.get("v.Accountno41") != null && component.get("v.Accountno41") != '' ) || ((component.get("v.Accountno61") == component.get("v.Accountno4"))  && component.get("v.Accountno61") != undefined && component.get("v.Accountno61") != null && component.get("v.Accountno61") != '' && component.get("v.Accountno4") != undefined && component.get("v.Accountno4") != null && component.get("v.Accountno4") != '' )|| ((component.get("v.Accountno6") == component.get("v.Accountno41"))  && component.get("v.Accountno6") != undefined && component.get("v.Accountno6") != null && component.get("v.Accountno6") != '' && component.get("v.Accountno41") != undefined && component.get("v.Accountno41") != null && component.get("v.Accountno41") != '' ) || ((component.get("v.Accountno6") == component.get("v.Accountno4"))  && component.get("v.Accountno6") != undefined && component.get("v.Accountno6") != null && component.get("v.Accountno6") != '' && component.get("v.Accountno4") != undefined && component.get("v.Accountno4") != null && component.get("v.Accountno4") != '' ) ))
                ||
                ( (((component.get("v.BankName61") == component.get("v.BankName51")) && component.get("v.BankName61") != undefined && component.get("v.BankName61") != null && component.get("v.BankName61") != '' && component.get("v.BankName51") != undefined && component.get("v.BankName51") != null && component.get("v.BankName51") != '') || ((component.get("v.BankName61") == component.get("v.BankName5")) && component.get("v.BankName61") != undefined && component.get("v.BankName61") != null && component.get("v.BankName61") != '' && component.get("v.BankName5") != undefined && component.get("v.BankName5") != null && component.get("v.BankName5") != '') || ((component.get("v.BankName6") == component.get("v.BankName51")) && component.get("v.BankName6") != undefined && component.get("v.BankName6") != null && component.get("v.BankName6") != '' && component.get("v.BankName51") != undefined && component.get("v.BankName51") != null && component.get("v.BankName51") != '') || ((component.get("v.BankName6") == component.get("v.BankName5")) && component.get("v.BankName6") != undefined && component.get("v.BankName6") != null && component.get("v.BankName6") != '' && component.get("v.BankName5") != undefined && component.get("v.BankName5") != null && component.get("v.BankName5") != '' ) )  &&
                 (((component.get("v.Accountno61") == component.get("v.Accountno51"))  && component.get("v.Accountno61") != undefined && component.get("v.Accountno61") != null && component.get("v.Accountno61") != '' && component.get("v.Accountno51") != undefined && component.get("v.Accountno51") != null && component.get("v.Accountno51") != '' ) || ((component.get("v.Accountno61") == component.get("v.Accountno5"))  && component.get("v.Accountno61") != undefined && component.get("v.Accountno61") != null && component.get("v.Accountno61") != '' && component.get("v.Accountno5") != undefined && component.get("v.Accountno5") != null && component.get("v.Accountno5") != '' )|| ((component.get("v.Accountno6") == component.get("v.Accountno51"))  && component.get("v.Accountno6") != undefined && component.get("v.Accountno6") != null && component.get("v.Accountno6") != '' && component.get("v.Accountno51") != undefined && component.get("v.Accountno51") != null && component.get("v.Accountno51") != '' ) || ((component.get("v.Accountno6") == component.get("v.Accountno5"))  && component.get("v.Accountno6") != undefined && component.get("v.Accountno6") != null && component.get("v.Accountno6") != '' && component.get("v.Accountno5") != undefined && component.get("v.Accountno5") != null && component.get("v.Accountno5") != '' ) ))
                ||
                ( (((component.get("v.BankName61") == component.get("v.BankName31")) && component.get("v.BankName61") != undefined && component.get("v.BankName61") != null && component.get("v.BankName61") != '' && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '') || ((component.get("v.BankName61") == component.get("v.BankName3")) && component.get("v.BankName61") != undefined && component.get("v.BankName61") != null && component.get("v.BankName61") != '' && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '') || ((component.get("v.BankName6") == component.get("v.BankName31")) && component.get("v.BankName6") != undefined && component.get("v.BankName6") != null && component.get("v.BankName6") != '' && component.get("v.BankName31") != undefined && component.get("v.BankName31") != null && component.get("v.BankName31") != '') || ((component.get("v.BankName6") == component.get("v.BankName3")) && component.get("v.BankName6") != undefined && component.get("v.BankName6") != null && component.get("v.BankName6") != '' && component.get("v.BankName3") != undefined && component.get("v.BankName3") != null && component.get("v.BankName3") != '' ) )  &&
                 (((component.get("v.Accountno61") == component.get("v.Accountno31"))  && component.get("v.Accountno61") != undefined && component.get("v.Accountno61") != null && component.get("v.Accountno61") != '' && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' ) || ((component.get("v.Accountno61") == component.get("v.Accountno3"))  && component.get("v.Accountno61") != undefined && component.get("v.Accountno61") != null && component.get("v.Accountno61") != '' && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' )|| ((component.get("v.Accountno6") == component.get("v.Accountno31"))  && component.get("v.Accountno6") != undefined && component.get("v.Accountno6") != null && component.get("v.Accountno6") != '' && component.get("v.Accountno31") != undefined && component.get("v.Accountno31") != null && component.get("v.Accountno31") != '' ) || ((component.get("v.Accountno6") == component.get("v.Accountno3"))  && component.get("v.Accountno6") != undefined && component.get("v.Accountno6") != null && component.get("v.Accountno6") != '' && component.get("v.Accountno3") != undefined && component.get("v.Accountno3") != null && component.get("v.Accountno3") != '' ) ))
            ){
                //errorMsg = true;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Duplicate Bank Details entered in section 6'
                });
                toastEvent.fire();
                this.hideSpinner(component,event,helper);
            } else {
                 if(action == "Save") {
                	this.saveHelper(component, event, helper,disableBank1, disableBank11,perfiosButtonClicked, "6",saveAll);
                 } else if(action == "Perfios") {
                    this.fetchPerfiosDataHelper(component, event, helper,disableBank1, disableBank11, "6"); 
                 }  else if(action == "Banking") {
                     helper.fetchBRERecordsHelper(component, event, helper, disableBank1, disableBank11, "6");
                 }  
            }
        }
    },
    
    excludeSection : function(component,event,helper, sectionno) {
        var action = component.get("c.removeBankSection");
        action.setParams({
            'CDrecID' : component.get("v.custRecordId") , 
            'sectionno' : sectionno
        })
        $A.enqueueAction(action); 
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    checkAccountType : function(component,event,helper){
        if(component.get("v.AccountType1") == 'OD' || component.get("v.AccountType1") == 'CC' || component.get("v.AccountType11") == 'OD' || component.get("v.AccountType11") == 'CC'){
            component.set("v.disableLimitAmount1",false);
        }
        else{
            component.set("v.disableLimitAmount1",true);
        }
        
        if(component.get("v.AccountType2") == 'OD' || component.get("v.AccountType2") == 'CC' || component.get("v.AccountType21") == 'OD' || component.get("v.AccountType21") == 'CC'){
            component.set("v.disableLimitAmount2",false);
        }
        else{
            component.set("v.disableLimitAmount2",true);
        }
        
        if(component.get("v.AccountType3") == 'OD' || component.get("v.AccountType3") == 'CC' || component.get("v.AccountType31") == 'OD' || component.get("v.AccountType31") == 'CC'){
            component.set("v.disableLimitAmount3",false);
        }
        else{
            component.set("v.disableLimitAmount3",true);
        }
        
        if(component.get("v.AccountType4") == 'OD' || component.get("v.AccountType4") == 'CC' || component.get("v.AccountType41") == 'OD' || component.get("v.AccountType41") == 'CC'){
            component.set("v.disableLimitAmount4",false);
        }
        else{
            component.set("v.disableLimitAmount4",true);
        }
        
        if(component.get("v.AccountType5") == 'OD' || component.get("v.AccountType5") == 'CC' || component.get("v.AccountType51") == 'OD' || component.get("v.AccountType51") == 'CC'){
            component.set("v.disableLimitAmount5",false);
        }
        else{
            component.set("v.disableLimitAmount5",true);
        }
        
        if(component.get("v.AccountType6") == 'OD' || component.get("v.AccountType6") == 'CC' || component.get("v.AccountType61") == 'OD' || component.get("v.AccountType61") == 'CC'){
            component.set("v.disableLimitAmount6",false);
        }
        else{
            component.set("v.disableLimitAmount6",true);
        }
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    hideSpinner : function(component,event,helper){
       // make Spinner attribute to false for hiding loading spinner    
       component.set("v.spinner", false);
    }
})