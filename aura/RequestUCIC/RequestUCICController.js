({
    doInit : function(component, event, helper) {
        var title = '';
        var showMsg = 'Message';
        var loanAppId = component.get("v.recordId");
        var actionUCIC = component.get("c.checkEligibility");
        actionUCIC.setParams({
            "loanId" : loanAppId
        });
        actionUCIC.setCallback(this,function(response){
            var state = response.getState();
            console.log('state' + state);
            if(state === "SUCCESS"){
                var resp = response.getReturnValue();
                console.log('resp' + response.getReturnValue());
                if(resp == 'Eligible for Callout'){
                    title = "Success";
                    showMsg = "Request Initiated";
                }
                else if(resp == 'Not Eligible for Callout'){
                    title = "Id Exist";
                    showMsg = "UCICid already exist for all the customers";
                    
                }
                else if(resp == 'Operation not allowed : You cannot generate Dedupe Results at this stage.') {
                        title = "Operation not allowed";
                        showMsg = "Dedupe Results cannot be initiated at this stage";
                }
                else if(resp == 'Operation not allowed : You are not allowed to generate Dedupe Results.'){
                    title = "Operation not allowed";
                    showMsg = "You are not allowed to generate Dedupe Results";
                }
                //Below else if block added by Abhilekh on 11th September 2020 for UCIC Phase II
                else if(resp == 'Dedupe Results has already been initiated for response, please try after some time.'){
                    title = "Operation not allowed";
                     showMsg = "Dedupe Results has already been initiated for response, please try after some time.";
                }
                
            }
            else{
                title = "Error!";
                showMsg = "Internal Error, please refresh.";
            }
            console.log('showMsg   ' + showMsg);
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": title,
                "message": showMsg
            });
            toastEvent.fire();
            $A.get("e.force:closeQuickAction").fire();
        });
        $A.enqueueAction(actionUCIC);
    }
})