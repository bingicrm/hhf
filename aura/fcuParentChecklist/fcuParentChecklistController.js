({
	initRecords: function(component, event, helper) {
        // call the apex class method and fetch sub stage of loan appliction record 
       	 //alert('Modification in progress');
         // call the apex class method and fetch account list
         
        //All code related to action2 and action3 var is added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
        var action2 = component.get("c.checkLAFCUBRDExemption");
        action2.setParams({
            'tpvId': component.get("v.recordId")
        });
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                var storeResponse = response.getReturnValue();
                
                if(storeResponse == true){
                    component.set("v.exemptedFromFCUBRD",true);
                    
                    var action3 = component.get("c.fetchDocList1");
                    action3.setParams({
                        'tpvId': component.get("v.recordId")
                    });
                    action3.setCallback(this, function(response) {
                        var state = response.getState();
                        //alert(state);
                        if (state === "SUCCESS") {
                            var storeResponse = response.getReturnValue();
                            console.log('List received==>'+storeResponse);
                            
                            console.log('List received====='+JSON.stringify(storeResponse));
                            // set AccountList list with return value from server.
                            component.set("v.DocList2", storeResponse);
                        }
                    });
                    $A.enqueueAction(action3);
                }
                else{
                    component.set("v.exemptedFromFCUBRD",false);
                    
                    var action = component.get("c.fetchDocList");
                    action.setParams({
                        'tpvId': component.get("v.recordId")
                    });
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        //alert(state);
                        if (state === "SUCCESS") {
                            var storeResponse = response.getReturnValue();
                            console.log('List received=====>'+storeResponse);
                            
                            console.log('In Init==>'+JSON.stringify(storeResponse));
                            // set AccountList list with return value from server.
                            component.set("v.DocList1", storeResponse);
                        }
                    });
                    $A.enqueueAction(action);
                    
                    //Below code added by Abhilekh on 18th October 2019 for FCU BRD
                    var action1 = component.get("c.fetchProfileName");
                    
                    action1.setCallback(this, function(response) {
                        var state = response.getState();
                        if (state === "SUCCESS") {
                            var storeResponse = response.getReturnValue();
                            console.log('storeResponse==>'+storeResponse);
                            
                            console.log('In Init==>'+JSON.stringify(storeResponse));
                            component.set("v.profileName", storeResponse);
                        }
                    });
                    $A.enqueueAction(action1);
                }
            }
            else{
                var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({
                    'type': 'ERROR',
                    'message' : 'An error has occurred. Please contact system admin.'
                }); 
                showToast.fire();
            }
        });
        $A.enqueueAction(action2);
    },
    Save : function(component, event, helper) {
        var exemptedFromFCUBRD = component.get("v.exemptedFromFCUBRD"); //Added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
        console.log('exemptedFromFCUBRD==>'+exemptedFromFCUBRD);
        
        //Below if block only and else block added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
        if(exemptedFromFCUBRD == false){
            var spinner = component.find("mySpinner");
            $A.util.removeClass(spinner, "slds-hide");
            console.log('DocList1==>'+JSON.stringify(component.get("v.DocList1")));
            var action = component.get("c.saveLoan");

            action.setParams({
                "paramJSONList": JSON.stringify(component.get("v.DocList1")),
                "exemptedFromFCUBRD": exemptedFromFCUBRD, //Added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
                "tpvId" : component.get("v.recordId")
            });
            action.setCallback(this, function(response) {
                console.log('Response==>'+response.getReturnValue());
                console.log('Response state==>'+response.getState());
                if (response.getState() === "SUCCESS" && component.isValid()) {
                    component.set("v.DocList1", response.getReturnValue());
                    component.set("v.showSaveCancelBtn",false);
                }
                else{
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({
                        'type': 'ERROR',
                        'message' : 'An error has occurred. Please contact system admin.'
                    }); 
                    showToast.fire();
                }
                window.location.reload(true);
            });
            $A.enqueueAction(action);
        }
        else{
            var spinner = component.find("mySpinner");
            $A.util.removeClass(spinner, "slds-hide");
            
            //All code related to action1 var is added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
            var action1 = component.get("c.saveLoan");
            
            action1.setParams({
                "paramJSONList": JSON.stringify(component.get("v.DocList2")),
                "exemptedFromFCUBRD": exemptedFromFCUBRD //Added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
            });
            action1.setCallback(this, function(response1) {
                if (response1.getState() === "SUCCESS" && component.isValid()) {
                    component.set("v.DocList2", response1.getReturnValue());
                    component.set("v.showSaveCancelBtn",false);
                }
                else{
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({
                        'type': 'ERROR',
                        'message' : 'An error has occurred. Please contact system admin.'
                    }); 
                    showToast.fire();
                }
                window.location.reload(true);
            });
            $A.enqueueAction(action1);
        }
    },
	cancel : function(component,event,helper){
        component.set("v.showSaveCancelBtn",false);
        $A.get('e.force:refreshView').fire(); 
    },
    fetchAllDocuments : function(component, event, helper)
    {
        //alert('In Progress');
        /*
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
        */
        
        var exemptedFromFCUBRD = component.get("v.exemptedFromFCUBRD"); //Added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
        
        var propertyId = component.get("v.recordId");
        //alert(propertyId);
        //component.set("v.files", []);
        if (!propertyId) {
            return;
        }
        var action = component.get("c.queryAttachments");
        action.setParams({
            "docId": propertyId,
            "exemptedFromFCUBRD": exemptedFromFCUBRD //"exemptedFromFCUBRD": exemptedFromFCUBRD added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
        });
        //alert('Before callback');
        action.setCallback(this, function (response) {
            //alert('inside callback1');
            var files = response.getReturnValue();
            console.log(files);
            if(files != null) {
                var cusrecordIds = files.join();
                console.log('recordIds'+cusrecordIds);
                $A.get('e.lightning:openFiles').fire({
                    //recordIds: ['069p0000001E6ag'],
                    recordIds: files,
                });
            }
            else {
                var cmpTarget = component.find('Modalbox');
                var cmpBack = component.find('Modalbackdrop');
                $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
            }
            //component.set("v.files", files);
        });
        $A.enqueueAction(action);
    },
    closeModalDoc:function(component,event,helper){   
        //alert('Close');
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    
    //Added by Abhilekh on 5th December 2019 for FCU BRD
    fillScreened : function(component,event,helper){
		var screenedValue = component.find("screened").get("v.value");
        
        var docList = component.get("v.DocList1");
        var updatedDocList = [];
        
        for(var i=0; i < docList.length; i++){
            if(screenedValue == 'Yes'){
                docList[i].Screened__c = 'Yes';
            }
            else if(screenedValue == 'No'){
                docList[i].Screened__c = 'No';
            }
            else{
            	docList[i].Screened__c = '';	        
            }
            updatedDocList.push(docList[i]);
        }
        component.set("v.DocList1",updatedDocList);
        component.set("v.showSaveCancelBtn",true);
    },
    
    //Added by Abhilekh on 5th December 2019 for FCU BRD
    fillSampled : function(component,event,helper){
		var sampledValue = component.find("sampled").get("v.value");
        
        var docList = component.get("v.DocList1");
        var updatedDocList = [];
        
        for(var i=0; i < docList.length; i++){
            if(sampledValue == 'Yes'){
                docList[i].Sampled__c = 'Yes';
            }
            else if(sampledValue == 'No'){
                docList[i].Sampled__c = 'No';
            }
            else{
            	docList[i].Sampled__c = '';	        
            }
            updatedDocList.push(docList[i]);
        }
        component.set("v.DocList1",updatedDocList);
        component.set("v.showSaveCancelBtn",true);
    },
    
    //Added by Abhilekh on 5th December 2019 for FCU BRD
    fillDecision : function(component,event,helper){
		var decisionValue = component.find("decision").get("v.value");
        
        var docList = component.get("v.DocList1");
        var updatedDocList = [];
        
        for(var i=0; i < docList.length; i++){
            if(decisionValue == 'Negative'){
                docList[i].FCU_Decision__c = 'Negative';
            }
            else if(decisionValue == 'Could Not Verify'){
                docList[i].FCU_Decision__c = 'Could Not Verify';
            }
            else if(decisionValue == 'Refer to Credit'){
                docList[i].FCU_Decision__c = 'Refer to Credit';
            }
            else if(decisionValue == 'Positive'){
                docList[i].FCU_Decision__c = 'Positive';
            }
            else{
            	docList[i].FCU_Decision__c = '';	        
            }
            updatedDocList.push(docList[i]);
        }
        component.set("v.DocList1",updatedDocList);
        component.set("v.showSaveCancelBtn",true);
    }
})