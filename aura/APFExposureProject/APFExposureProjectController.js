({
	doInit : function(component, event, helper) {
		var action = component.get("c.checkEligibility");
        
        action.setParams({ projectId : component.get("v.recordId") });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state==>'+state);
            
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                console.log('res==>'+res);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success',
                    message: res,
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
               
                toastEvent.fire();
                
            }
            $A.get("e.force:closeQuickAction").fire();
        });
        $A.enqueueAction(action);
	}
})