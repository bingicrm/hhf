({
	doInit: function(component, event, helper)  
    { 
        
		var act = component.get("c.checkCibilEligibility");        
        act.setParams({
            strRecordId : component.get("v.recordId")
        });  
        act.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                var responseValue = response.getReturnValue();
            	console.log('Response Received'+responseValue);
                if(responseValue == 'CORPORATE') {
                    component.set('v.performCorporateCibil',true);
                }
                else if(responseValue == 'INDIVIDUAL') {
                    component.set('v.performIndividualCibil',true);
                }
                else {
                    if(responseValue == 'Corporate CIBIL Not Applicable for this Customer, based on Constitution.') {
                    	var showToast = $A.get("e.force:showToast"); 
                                        showToast.setParams({ 
                                            'type': 'ALERT',
                                            'message' : 'Corporate CIBIL Not Applicable for this Customer, based on Constitution.',
                                            'duration' : '10000ms'
                                        }); 
                                        showToast.fire(); 	
                                        
                                        $A.get("e.force:closeQuickAction").fire();
                    }
                    else if(responseValue == 'CIBIL has already been hit for response. Please try after some time.') {
                        var showToast = $A.get("e.force:showToast"); 
                                        showToast.setParams({ 
                                            'type': 'ALERT',
                                            'message' : 'CIBIL has already been hit for response. Please try after some time.',
                                            'duration' : '10000ms'
                                        }); 
                                        showToast.fire(); 	
                                        
                                        $A.get("e.force:closeQuickAction").fire();
                    }
                    else if(responseValue == 'Please fill City, Pin and State in Address before proceeding.') {
                        var showToast = $A.get("e.force:showToast"); 
                                        showToast.setParams({ 
                                            'type': 'ALERT',
                                            'message' : 'Please fill City, Pin and State in Address before proceeding.',
                                            'duration' : '10000ms'
                                        }); 
                                        showToast.fire(); 	
                                        
                                        $A.get("e.force:closeQuickAction").fire();
                    }
                    else if(responseValue == 'Please validate PAN Card before validating CIBIL.') {
                        var showToast = $A.get("e.force:showToast"); 
                                        showToast.setParams({ 
                                            'type': 'ALERT',
                                            'message' : 'Please validate PAN Card before validating CIBIL.',
                                            'duration' : '10000ms'
                                        }); 
                                        showToast.fire(); 	
                                        
                                        $A.get("e.force:closeQuickAction").fire();
                    }
                    else if(responseValue == 'Address not present. Could Not Initiate Corporate Cibil.') {
                        var showToast = $A.get("e.force:showToast"); 
                                        showToast.setParams({ 
                                            'type': 'ALERT',
                                            'message' : 'Address not present. Could Not Initiate Corporate Cibil.',
                                            'duration' : '10000ms'
                                        }); 
                                        showToast.fire(); 	
                                        
                                        $A.get("e.force:closeQuickAction").fire();
                    }
                    
                }
            }
        });
        $A.enqueueAction(act);
	}
})