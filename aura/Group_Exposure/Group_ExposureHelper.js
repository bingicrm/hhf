({
	showSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
     
    hideSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    
    /*saveSelectedHelper: function (component, event, saveRec) {
        var action = component.get('c.saveGERecords');
        action.setParams({
            "GERecords" : JSON.stringify(saveRec)
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("State==>"+state);
                /*if (response.getReturnValue() != '') {
                    alert('The following error has occurred while saving record(s)-->' + response.getReturnValue());
                } else {
                    console.log('check it--> save successful');
                }*/  
                //this.onLoad(component, event);
            /*}
        });
        
        $A.enqueueAction(action);
    }*/

})