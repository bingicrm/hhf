({
	doInit : function(component, event, helper) {
        
        helper.showSpinner(component);

		var action = component.get("c.getLoanApp");
        action.setParams({"lappId" : component.get("v.recordId")});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var res = response.getReturnValue();
                console.log('response---->'+res.StageName__c);
		//Below if and else block added by Abhilekh on 5th October 2019 for FCU BRD,&& result.Exempted_from_FCU_BRD__c == false added by Abhilekh on 23rd December 2019 for FCU BRD Exemption
                if(res.Overall_FCU_Status__c == 'Negative' && res.FCU_Decline_Count__c > 0 && (res.Sub_Stage__c == 'Credit Review' || res.Sub_Stage__c == 'Re Credit' || res.Sub_Stage__c == 'Re-Look') && result.Exempted_from_FCU_BRD__c == false){
                    var resultsToast = $A.get("e.force:showToast");
                     resultsToast.setParams({
                    	"title" : "Error!",
                        "type" : 'error',
                    	"message" : 'You cannot fetch Group Exposure as this application is FCU Decline.'
                	});
					helper.hideSpinner(component);
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                }
		//Below else if block added by Abhilekh on 24th January 2020 for Hunter BRD
                else if(res.Hunter_Decline_Count__c > 0 && (res.Sub_Stage__c == 'Credit Review' || res.Sub_Stage__c == 'Re Credit' || res.Sub_Stage__c == 'Re-Look')){
                    var resultsToast = $A.get("e.force:showToast");
                     resultsToast.setParams({
                    	"title" : "Error!",
                        "type" : 'error',
                    	"message" : 'Group Exposure cannot be fetched as Hunter analysis has not been recommended. Please reject or assign the application to Hunter Manager..'
                	});
					helper.hideSpinner(component);
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                }
                else if(res.StageName__c == 'Credit Decisioning'){
                    
 				component.set("v.lapp", response.getReturnValue());
                console.log('data here---->'+component.get("v.lapp")); 
 				component.set("v.showOption", true);
                    helper.hideSpinner(component);
                }
                else{
 				//component.set("v.showOption", False);
                	$A.get("e.force:closeQuickAction").fire();
        			var resultsToast = $A.get("e.force:showToast");
                     resultsToast.setParams({
                    	"title" : "Error!",
                        "type" : 'error',
                    	"message" : 'Group Exposure can not be calulated at this Stage.'
                	});
                    helper.hideSpinner(component);
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                }
               
            }else{
                var resultsToast = $A.get("e.force:showToast");
                 resultsToast.setParams({
                    	"title" : "Error!",
                        "type" : 'error',
                    	"message" : 'Contact System Administrator.'
                	});
                    helper.hideSpinner(component);
                	$A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
            
            }
        });
        $A.enqueueAction(action);
    },
    requestCreation: function(component, event, helper) {
        component.set("v.showBlock",true);
        console.log('requestCreation1---->');

        component.set("v.showBlockDef",false);
        console.log('requestCreation2---->');
        helper.showSpinner(component);
        
        var action = component.get("c.checkGroupID");
		var loanApp = component.get("v.lapp");
        action.setParams({
            "lAppID" : loanApp
        });
        var resultsToast = $A.get("e.force:showToast");
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.allRecs",result);
                
                if(actionResult.getReturnValue() == null){
                	resultsToast.setParams({
                    	"title" : "SUCCESS!",
                        "type" : 'success',
                    	"message" : "Group Exposure has been fetched."
                	});
                    helper.hideSpinner(component);
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                    
            	}
                
                else if(actionResult.getReturnValue().length == 1){
                    component.set("v.allRecs",result);
                    resultsToast.setParams({
                    	"title" : "SUCCESS!",
                        "type" : 'success',
                    	"message" : "Records Fetched."
                	});
                    helper.hideSpinner(component);
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                }
                else if(actionResult.getReturnValue().length > 1){
                    resultsToast.setParams({
                    	"title" : "SUCCESS!",
                        "type" : 'success',
                        "message" : "Records Fetched."
                	});
                    helper.hideSpinner(component);
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                }
            	else{
                	resultsToast.setParams({
                    	"title" : "SUCCESS!",
                        "type" : 'success',
                        "message" : "Records Fetched."
                	});
                    helper.hideSpinner(component);
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                    
            	}
            }else if(state === "ERROR"){
                resultsToast.setParams({
                    	"title" : "Error!",
                        "type" : 'error',
                    	"message" : 'Contact System Administrator.'
                	});
                    helper.hideSpinner(component);
                	$A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
            }else{
            
            }
        });
        $A.enqueueAction(action);
        
    },
    
    cancelCreation: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    
    
    /*saveSelected: function(component, event, helper) {
        var savId = [];
        var getAllId = component.find("chkboxes");
        
        if(! Array.isArray(getAllId)){
            if (getAllId.get("v.value") == true) {
                savId.push(getAllId.get("v.text"));
            }
        }
        else{
            for (var i = 0; i < getAllId.length; i++) {
                if (getAllId[i].get("v.value") == true) {
                    savId.push(getAllId[i].get("v.text"));
                }
            }
        }
        
        console.log('savId==>'+savId);
        
        if(component.get("v.selectedCount") == 0){
            alert('Select atleast one record.');
        }
        else{
            helper.saveSelectedHelper(component, event, savId);
        }
        $A.get("e.force:closeQuickAction").fire();
        $A.get("e.force:refreshView").fire();
    },*/
    
})