({
    doInit : function(component, event, helper) {
        console.log('customerdetailId adrress method '+component.get('v.customerDetailId'));
        console.log('laVal adrress method '+component.get('v.laVal'));
        console.log('loanAppID adrress method '+component.get('v.loanAppID'));
        
        var action1 = component.get("c.getPicklistOptions");
        action1.setParams ({
            "fieldName" : "Type_of_address__c",
            "objectName" : "Address__c"
        });
        action1.setCallback(this, function(a){
            var x = a.getReturnValue();
            var optionsList = [];    
            for (var key in x) {
                optionsList.push({value: key, label: x[key]});
            };
            console.log('optionsList::::'+JSON.stringify(optionsList));
            component.set("v.typeOfAddressValues", optionsList);
        });  
        $A.enqueueAction(action1);
        
        var action2 = component.get("c.getPicklistOptions");
        action2.setParams ({
            "fieldName" : "Residence_Type__c",
            "objectName" : "Address__c"
        });
        action2.setCallback(this, function(a1){
            var x1 = a1.getReturnValue();
            var optionsList1 = [];    
            for (var key1 in x1) {
                optionsList1.push({value: key1, label: x1[key1]});
            };
            console.log('optionsList1::::'+JSON.stringify(optionsList1));
            component.set("v.resdientTypeValues", optionsList1);
        });  
        $A.enqueueAction(action2);
        
        var action = component.get("c.getAddresses");
        action.setParams({
            "customerDetailId" : component.get('v.customerDetailId')
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                component.set("v.lstOfWrapper",result.getReturnValue());
                console.log('list'+ component.get('v.lstOfWrapper'));
                for (var index = 0; index < result.getReturnValue().length; index++) { 
                    console.log('LSTOFADDRESS1 ' + result.getReturnValue()[index]); 
                    if(result.getReturnValue()[index].saveMode == true) {
                        component.set('v.disableAdd',false);
                    }
                } 
            }
            else
            {
                console.log('failure');
            }
        });
        $A.enqueueAction(action);
        
        var action11 = component.get("c.getPincode");  
            action11.setParams({
            });
            action11.setCallback(this, function(result11){
                var state11 = result11.getState();
                if (component.isValid() && state11 === "SUCCESS"){
                    component.set('v.pincodeObj',result11.getReturnValue());
                    console.log('v.pincodeObj' +component.get('v.pincodeObj'));
                }
            });
            $A.enqueueAction(action11);
},    
    addRow : function(component, event, helper) { 
        var lstOfWrapper = component.get('v.lstOfWrapper');
        console.log(lstOfWrapper);
        console.log(lstOfWrapper[0]);
        var action = component.get("c.addRowToTable");
        action.setParams({
            "listOfWrapper" : JSON.stringify(component.get('v.lstOfWrapper')) 
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            console.log(state);
            if (component.isValid() && state === "SUCCESS"){
                component.set("v.lstOfWrapper",result.getReturnValue());
                console.log('list'+ component.get('v.lstOfWrapper'));
                for (var index = 0; index < result.getReturnValue().length; index++) { 
                    console.log('LSTOFADDRESS2 ' +result.getReturnValue()[index]); 
                } 
                component.set('v.disableAdd',true);
            }
            else
            {
                console.log('failure');
            }
        });
        $A.enqueueAction(action);
},
    
    deleteRow: function(component, event,helper) { 
            var index = event.target.id;
            console.log('index '+index);
            var AllRowsList = component.get('v.lstOfWrapper');
            console.log(AllRowsList);
            AllRowsList.splice(index, 1);
            // set the contactList after remove selected row element  
            component.set("v.lstOfWrapper", AllRowsList);
            component.set('v.disableAdd',false);
    },
    
    editRow: function(component, event,helper) {
        var index = event.target.id;
            console.log('index '+index);
            
        var abc= component.get('v.lstOfWrapper');
        console.log('wrapper' +abc);
        console.log('wrapper' +abc[0].addressType);
        console.log('wrapper' +abc[0].resdientType);
        console.log('wrapper' +abc[0].addressLine1);
        console.log('wrapper' +abc[0].addressLine2);
        console.log('wrapper' +abc[0].pincode);
        console.log('wrapper' +abc[0].loanContact);
        console.log('wrapper' +abc[0].addressId);
        
        var action = component.get("c.editAddress");  
            action.setParams({
                "listOfWrapper" : JSON.stringify(component.get('v.lstOfWrapper')),
                "index" : index
            });
            action.setCallback(this, function(result){
                var state = result.getState();
                if (component.isValid() && state === "SUCCESS"){
                    console.log('result.getReturnValue()'+JSON.stringify(result.getReturnValue()));
                    if(!result.getReturnValue().includes('Error')) {
                        var wrappedobj = result.getReturnValue();
                        var pinObj = component.get('v.pincodeObj');
                        console.log('pinObj '+pinObj);
                        pinObj.val = wrappedobj[index].pincode;
                        pinObj.text = wrappedobj[index].pincodeVal;
                        wrappedobj[index].pincode = pinObj;
                        component.set("v.lstOfWrapper",result.getReturnValue());
                        component.set('v.disableAdd',true);
                    }
                    else {
                        var calledFromVFPage = component.get('v.calledFromVFPage');
                        if(calledFromVFPage == false) {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "type": "Error",
                                "title": "",
                                "message": "Unexpected error occurred"
                            });
                            toastEvent.fire(); 
                        }else {
                            component.set('v.showErrorMessage' , true);
                            component.set('v.errorMessage','Error: Some error occured. Please contact System Admin.');
                        }
                    }
                }
                else
                {
                    var calledFromVFPage = component.get('v.calledFromVFPage');
                    if(calledFromVFPage == false) {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type": "Error",
                            "title": "",
                            "message": "Unexpected error occurred"
                        });
                        toastEvent.fire();  
                    } else {    
                        component.set('v.showErrorMessage' , true);
                        component.set('v.errorMessage','Error: Some error occured. Please contact System Admin.');
                    }
                }
            });
            $A.enqueueAction(action);
},
    saveRow: function(component, event,helper) { 
            var index = event.target.id;
            console.log('index '+index);
            var wrap = [];
            wrap = component.get("v.lstOfWrapper"); 
            var pincodeVar = wrap[index].pincode;
            var typeOfAddVar = wrap[index].addressType;
            var typeOfResVar =wrap[index].resdientType;
            var addline1Var = wrap[index].addressLine1;
            var addline2Var = wrap[index].addressLine2;
            if(pincodeVar == '' || pincodeVar == null || pincodeVar == undefined || typeOfAddVar == '' || typeOfAddVar == null || typeOfAddVar == undefined || typeOfResVar == '' || typeOfResVar == null || typeOfResVar == undefined || addline1Var == '' || addline1Var == null || addline1Var == undefined || addline2Var == '' || addline2Var == null || addline2Var == undefined) {
                var errorFields = 'Please enter ';
                if(typeOfAddVar == '' || typeOfAddVar == null || typeOfAddVar == undefined) {
                    errorFields = errorFields + 'Type Of Address, ';
                } if(typeOfResVar == '' || typeOfResVar == null || typeOfResVar == undefined) {
                    errorFields = errorFields + 'Residence Type, ';
                }if(addline1Var == '' || addline1Var == null || addline1Var == undefined) {
                    errorFields = errorFields + 'Address Line 1, ';
                } if(addline2Var == '' || addline2Var == null || addline2Var == undefined) {
                    errorFields = errorFields + 'Address Line 2, ';
                }if(pincodeVar == '' || pincodeVar == null || pincodeVar == undefined) {
                    errorFields = errorFields + 'Pincode, ';
                } 
                if(errorFields.endsWith(', ')) { 
                    console.log('Last char is comma'); 
                    errorFields = errorFields.substring(0, errorFields.length - 2);
                }
                if(errorFields != 'Please enter ') {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "",
                        "type": "Error",
                        "message": errorFields
                    });
                    toastEvent.fire();
                }
            } else {    
                console.log('wrap:::::'+JSON.stringify(wrap[index].pincode)); 
                if(wrap[index].pincode.val != null && wrap[index].pincode.val != 'undefined'){
                    var pincodeId = wrap[index].pincode.val;
                    var pincodeVal = wrap[index].pincode.text;
                } else{
                     var pincodeId = wrap[index].pincode;
                     console.log('pincode:::::'+pincodeId);   
                 }
                var wrapVal = [];
                wrapVal = wrap;
                wrapVal[index].pincode = pincodeId;
                if(pincodeVal != null && pincodeVal!= 'undefined') {
                    wrapVal[index].pincodeVal = pincodeVal;
                }    
                console.log('customerDetailId'+ component.get('v.customerDetailId'));
                console.log('pincodeId '+pincodeId);
                console.log('pincodeVal'+pincodeVal);
                
                var action = component.get("c.saveAddress");  
                action.setParams({
                    "listOfWrapper" : JSON.stringify(wrapVal[index]),
                    "customerDetailId" : component.get('v.customerDetailId'),
                });
                action.setCallback(this, function(result){
                    var state = result.getState();
                    if (component.isValid() && state === "SUCCESS"){
                        if(!result.getReturnValue().message.includes('Error')) {
                            console.log(result.getReturnValue().lstOfWrapper);
                            var wrapValue11 = [];
                            wrapValue11 = component.get("v.lstOfWrapper");
                            console.log('wrapValue11:::'+ JSON.stringify(wrapValue11));
                            wrapValue11[index] = result.getReturnValue().lstOfWrapper;
                            
                            var lstTypeOfAddressValues = component.get("v.typeOfAddressValues");
                            
                           console.log('::lstTypeOfAddressValues::'+JSON.stringify(lstTypeOfAddressValues)); 
                           console.log('index '+index);
                            for (var value in lstTypeOfAddressValues) {
                                console.log('::value::'+value);
                                console.log('::lstTypeOfAddressValues[value].value'+lstTypeOfAddressValues[value].value);
                                console.log('::wrapVal[index].addressType '+wrapVal[index].addressType)
                                if(lstTypeOfAddressValues[value].value == wrapVal[index].addressType){
                                    wrapValue11[index].addressTypeName = lstTypeOfAddressValues[value].label;
                                }
                            };
                            var lstResdientTypeValues = component.get("v.resdientTypeValues");
                            for (var value in lstResdientTypeValues) {
                                console.log('::value::'+value);
                                
                                if(lstResdientTypeValues[value].value == wrapVal[index].resdientType){
                                    wrapValue11[index].resdientTypeName = lstResdientTypeValues[value].label;
                                }
                            }
                            component.set("v.lstOfWrapper",wrapValue11);
                            component.set('v.disableAdd',false);
                             //var QDEMode = component.get('v.QDEMode');
                             //if(QDEMode == true) {
                               //  var toastEvent = $A.get("e.force:showToast");
                                 //toastEvent.setParams({
                                   // "type": "Success",
                                //    "title": "Success!",
                                  //  "message": result.getReturnValue().message
                                // });
                                //toastEvent.fire();
                             //} else {    
                               // component.set('v.showErrorMessage' , true);
                                //component.set('v.errorMessage',result.getReturnValue().message);
                             //}    
                        }
                        else {
                           var calledFromVFPage = component.get('v.calledFromVFPage');
                            if(calledFromVFPage == false) {
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "type": "Error",
                                    "title": "",
                                    "message": result.getReturnValue().message
                                });
                                toastEvent.fire(); 
                            } else {
                                component.set('v.showErrorMessage' , true);
                                component.set('v.errorMessage',result.getReturnValue().message);
                            }
                        }
                    }
                    else
                    {
                        var calledFromVFPage = component.get('v.calledFromVFPage');
                        if(calledFromVFPage == false) {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "type": "Error",
                                "title": "",
                                "message": result.getReturnValue().message
                            });
                            toastEvent.fire(); 
                        } else {
                            component.set('v.showErrorMessage' , true);
                            component.set('v.errorMessage',result.getReturnValue().message);
                        }
                    }
                });
                $A.enqueueAction(action);
            }    
},
    
    saveAddresses: function(component, event, helper) { 
        console.log(component.get('v.lstOfWrapper'));
        var action = component.get("c.saveAllAddresses");  
        action.setParams({
            "listOfWrapper" : JSON.stringify(component.get('v.lstOfWrapper')),
            "customerDetailId" : component.get('v.customerDetailId')
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                if(!result.getReturnValue().message.includes('Error')) {
                    var calledFromVFPage = component.get('v.calledFromVFPage');
                    if(calledFromVFPage == false) {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type": "Success",
                            "title": "",
                            "message": result.getReturnValue().message
                        });
                        toastEvent.fire();
                    } else {    
                        component.set('v.showErrorMessage' , true);
                        component.set('v.errorMessage',result.getReturnValue().message);
                    }
                    component.set("v.lstOfWrapper",result.getReturnValue().lstOfWrapper);
                    //$A.get("e.force:closeQuickAction").fire() ;
                }
                else {
                    var calledFromVFPage = component.get('v.calledFromVFPage');
                    if(calledFromVFPage == false) {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                           "type": "Error",
                            "title": "",
                            "message": result.getReturnValue().message
                        });
                        toastEvent.fire(); 
                    } else {    
                        component.set('v.showErrorMessage' , true);
                        component.set('v.errorMessage',result.getReturnValue().message);
                    }
                }
            }
            else
            {
                var calledFromVFPage = component.get('v.calledFromVFPage');
                    if(calledFromVFPage == false) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "Error",
                        "title": "",
                        "message": result.getReturnValue().message
                    });
                    toastEvent.fire();  
                } else {
                    component.set('v.showErrorMessage' , true);
                    component.set('v.errorMessage',result.getReturnValue().message);
                }
            }
        });
        $A.enqueueAction(action);
},

    cancel : function(component, event, helper){
        // helper.getGSTList(component);
        //$A.get("e.force:closeQuickAction").fire() ;
        console.log('In cancel method');
        console.log('laval'+ component.get('v.laVal'));
        if(component.get('v.disableAdd') == true) {
            component.set('v.showConfirmDialog', true); 
            /*var result = confirm('There are some unsaved changes in address, Are you sure you want to close this screen?'); 
            console.log('result is '+result);
            if(result == true){
                var evt = $A.get("e.c:callCreateLAEvent");
                evt.setParams(
                { 
                    "laVal" : component.get('v.laVal'),
                    "CDWrap" : component.get('v.CDWrap'),
                    "lstBorrowerType" : component.get('v.lstBorrowerType'),
                    "lstconstitution" : component.get('v.lstconstitution'),
                    "lstCustSegment" : component.get('v.lstCustSegment'),
                    "loanAppID" : component.get('v.loanAppID'),
                    "prod" : component.get('v.prod'),
                    "localPol" : component.get('v.localPol'),
                    "customer" : component.get('v.customer'),
                    "isDisplay" : component.get('v.isDisplay') ,
                    "isEditable" : component.get('v.isEditable'),
                    "isInserted" : component.get('v.isInserted'),
                    "isDisable" : component.get('v.isDisable'),
                    "addAddress" : component.get('v.addAddress'),
                    "CDfetched" : component.get('v.CDfetched'),
                    "data" : component.get('v.data'),
                    "columns" : component.get('v.columns'),
                    "borrowerTy" : component.get('v.borrowerTy'),
                    "LaNum" : component.get('v.LaNum'),
                    "isEditableLA" : component.get('v.isEditableLA'),
                    "QDEMode" : component.get('v.QDEMode'),
                    "calledFromVFPage" : component.get('v.calledFromVFPage'),
                    "errorMessage" : component.get('v.errorMessage'),
                    "showErrorMessage" :component.get('v.showErrorMessage'),
                    "LoanConId" : component.get('v.LoanConId'),
                    "idTypes" : component.get('v.idTypes'),
                    "oldWrap" : component.get('v.oldWrap'),
                    "oldWrapString" : component.get('v.oldWrapString'),
                    "disableAddButton" : component.get('v.disableAddButton'),
                    "disableEditButton" : component.get('v.disableEditButton')
                }
                );
                evt.fire();
            }*/
            
        } else {
            var evt = $A.get("e.c:callCreateLAEvent");
            evt.setParams(
            { 
                "laVal" : component.get('v.laVal'),
                "CDWrap" : component.get('v.CDWrap'),
                "lstBorrowerType" : component.get('v.lstBorrowerType'),
                "lstconstitution" : component.get('v.lstconstitution'),
                "lstCustSegment" : component.get('v.lstCustSegment'),
                "loanAppID" : component.get('v.loanAppID'),
                "prod" : component.get('v.prod'),
                "localPol" : component.get('v.localPol'),
                "customer" : component.get('v.customer'),
                "isDisplay" : component.get('v.isDisplay') ,
                "isEditable" : component.get('v.isEditable'),
                "isInserted" : component.get('v.isInserted'),
                "isDisable" : component.get('v.isDisable'),
                "addAddress" : component.get('v.addAddress'),
                "CDfetched" : component.get('v.CDfetched'),
                "data" : component.get('v.data'),
                "columns" : component.get('v.columns'),
                "borrowerTy" : component.get('v.borrowerTy'),
                "LaNum" : component.get('v.LaNum'),
                "isEditableLA" : component.get('v.isEditableLA'),
                "QDEMode" : component.get('v.QDEMode'),
                "calledFromVFPage" : component.get('v.calledFromVFPage'),
                "errorMessage" : component.get('v.errorMessage'),
                "showErrorMessage" :component.get('v.showErrorMessage'),
                "LoanConId" : component.get('v.LoanConId'),
                "idTypes" : component.get('v.idTypes'),
                "oldWrap" : component.get('v.oldWrap'),
                "oldWrapString" : component.get('v.oldWrapString'),
                "disableAddButton" : component.get('v.disableAddButton'),
                "disableEditButton" : component.get('v.disableEditButton')
            }
            );
            evt.fire();
        }
},
    handleConfirmDialogNo: function(component, event, helper) {
        component.set('v.showConfirmDialog', false); 
    },
    handleConfirmDialogYes: function(component, event, helper) {
         var evt = $A.get("e.c:callCreateLAEvent");
                evt.setParams(
                { 
                    "laVal" : component.get('v.laVal'),
                    "CDWrap" : component.get('v.CDWrap'),
                    "lstBorrowerType" : component.get('v.lstBorrowerType'),
                    "lstconstitution" : component.get('v.lstconstitution'),
                    "lstCustSegment" : component.get('v.lstCustSegment'),
                    "loanAppID" : component.get('v.loanAppID'),
                    "prod" : component.get('v.prod'),
                    "localPol" : component.get('v.localPol'),
                    "customer" : component.get('v.customer'),
                    "isDisplay" : component.get('v.isDisplay') ,
                    "isEditable" : component.get('v.isEditable'),
                    "isInserted" : component.get('v.isInserted'),
                    "isDisable" : component.get('v.isDisable'),
                    "addAddress" : component.get('v.addAddress'),
                    "CDfetched" : component.get('v.CDfetched'),
                    "data" : component.get('v.data'),
                    "columns" : component.get('v.columns'),
                    "borrowerTy" : component.get('v.borrowerTy'),
                    "LaNum" : component.get('v.LaNum'),
                    "isEditableLA" : component.get('v.isEditableLA'),
                    "QDEMode" : component.get('v.QDEMode'),
                    "calledFromVFPage" : component.get('v.calledFromVFPage'),
                    "errorMessage" : component.get('v.errorMessage'),
                    "showErrorMessage" :component.get('v.showErrorMessage'),
                    "LoanConId" : component.get('v.LoanConId'),
                    "idTypes" : component.get('v.idTypes'),
                    "oldWrap" : component.get('v.oldWrap'),
                    "oldWrapString" : component.get('v.oldWrapString'),
                    "disableAddButton" : component.get('v.disableAddButton'),
                    "disableEditButton" : component.get('v.disableEditButton')
                }
                );
                evt.fire();
        component.set('v.showConfirmDialog', false); 
    },
    
handleComponentEvent : function(cmp, event) { 
var message = event.getParam("disabledVar"); 
// set the handler attributes based on event data 
cmp.set("v.disabledVariable", message); 
console.log('EVENTVAR '+cmp.get('v.disabledVariable'));
}

})