({
	doInit : function(component, event, helper) {
		var action = component.get("c.getCriteriaList");//This method will return a list of Project Builder and Promoter Records
        action.setParams({
            'projectId': component.get("v.recordId")//Customer Detail Records associated with the Loan Application
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('Response Status'+state);
            
            if (state === "SUCCESS") {
                var capturedResponse = response.getReturnValue();
                console.log('Response received'+capturedResponse);
                console.log(JSON.stringify(capturedResponse));
                if(capturedResponse.length >0) {
                    component.set("v.RecordsList", capturedResponse);
                    component.set("v.selectedCount" , 0);
                    var totalRecordsList = capturedResponse;
                    var totalLength = totalRecordsList.length;
                    component.set("v.totalRecordsCount",totalLength);
                }
                else {
                    component.set("v.bNoRecordsFound" , true);
                }
            }
        });
        $A.enqueueAction(action);
	},
    getSelectedRecords: function(component, event, helper) {
    	helper.refresh(component, event);
        helper.showSpinner(component, event);
        
        var allRecords = component.get("v.RecordsList");
        var recordId = component.get("v.selectedID");
        var selectedRecords = [];
        var name;
        
        for (var i = 0; i < allRecords.length; i++) {
            console.log('ID == ' + allRecords[i].strRecordId);
            if (recordId == allRecords[i].strRecordId) {
                console.log('Id Match');
                selectedRecords.push(allRecords[i]);
                name = allRecords[i].obj_RecordDetails.strBuilderName;
            }
        }
        console.log('name' + name);
        var GSTINAuthCallout = component.get("c.makeGSTINAuthCallout");
        GSTINAuthCallout.setParams({
            'paramJSONList': JSON.stringify(selectedRecords)//JSON.stringify(component.get("v.CustomerList"))
        });
        GSTINAuthCallout.setCallback(this, function(response) {
            //alert('In Progress...');
            console.log('Inner CallBack');
            var GSTINErrorMessage = component.get("c.getErrorMessage");
            GSTINErrorMessage.setParams({
                'paramJSONList': JSON.stringify(selectedRecords)//component.get("v.CustomerList")
            });
            GSTINErrorMessage.setCallback(this, function(response) {
                console.log('set CallBack');
                var state = response.getState();
                console.log('State  ==> ' + state);
                console.log('response.getReturnValue()  ' + response.getReturnValue());
                if (state === "SUCCESS") {
                    console.log('SUCCESS...');
                    
                    console.log('SIZE' + response.getReturnValue().length);
                    if(response.getReturnValue().length > 0){
                        component.set("v.isShow" , true);
                        component.set("v.ErrorList" , response.getReturnValue());
                        console.log(component.get("v.isShow"));
                        console.log(component.get("v.ErrorList"));
                        //console.log('response.getReturnValue()  ' + response.getReturnValue());
                    }
                    if(!component.get("v.isShow")){
                        var resultsToast = $A.get("e.force:showToast");
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "GST INITIATED SUCCESSFULLY!!!",
                            "message" : "FOR : " + name,
                            "type" : "Success"
                        });
                        toastEvent.fire();
                    }
                    /*else{
                        var resultsToast = $A.get("e.force:showToast");
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "No Response !!!",
                            "message" : "From SERVER",
                            "type" : "ERROR"
                        });
                        toastEvent.fire();
                    }*/
                }
                else if (state === "INCOMPLETE") {
                    // do something
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " +errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            $A.enqueueAction(GSTINErrorMessage)
            helper.hideSpinner(component, event);
        });
        $A.enqueueAction(GSTINAuthCallout);
    },
     onGroup: function(cmp, evt) {
		 var selectedId = evt.getSource().get("v.text");
         cmp.set("v.selected" , true);
         cmp.set("v.selectedID" , selectedId);
         console.log('selectedId '+ selectedId);
	 },
    validateEmail : function(component, event, helper) {
        var emailFieldValue = event.getSource().get("v.value");
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;  
        // check if Email field in not blank,
        // and if Email field value is valid then set error message to null, 
        // and remove error CSS class.
        // ELSE if Email field value is invalid then add Error Style Css Class.
        // and set the error Message.  
        // and set isValidEmail boolean flag value to false.
        //alert('');
        if(!$A.util.isEmpty(emailFieldValue)){   
            if(emailFieldValue.match(regExpEmailformat)){
                event.getSource().set("v.class",'slds-input green');
                event.getSource().set("v.errors",[]);
            }
            else{
            console.log('---'+event.getSource());
            event.getSource().set("v.value",'');
            event.getSource().set("v.errors",[{message:"Invalid Email Address."}]);
            event.getSource().set("v.class",'slds-input red');
            //alert('Please Enter a Valid Email Address');
        }
        }
         else{
            event.getSource().set("v.value",'');
            event.getSource().set("v.errors",[{message:"Enter Email Address."}]);
            event.getSource().set("v.class",'slds-input red');
         }
    },
    validatePhone : function(component, event, helper)
    {
        var PhoneFieldValue = event.getSource().get("v.value");
        if (/^[6789]\d{9}$/.test(PhoneFieldValue)) {  
            // value is ok, use it //old regex used:/^\d{10}$/
            event.getSource().set("v.class",'slds-input green');
            event.getSource().set("v.errors",[]);
        } else {
           /* var resultsToast = $A.get("e.force:showToast");
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error",
                "message" : "Invalid Mobile Number...",
                "type" : "ERROR"
            });
            toastEvent.fire();*/
            event.getSource().set("v.value",'');
            event.getSource().set("v.class",'slds-input red');
            event.getSource().set("v.errors",[{message:"Invalid Mobile Number...."}]);
        }
    }
})