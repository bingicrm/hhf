({
	refresh : function(component, event){
        component.set("v.isShow" , false);
        console.log('Refresh View');
    	$A.get('e.force:refreshView').fire();
    },
    
    showSpinner: function(component, event) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
    hideSpinner : function(component,event){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    }
})