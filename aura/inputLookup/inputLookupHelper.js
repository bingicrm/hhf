({
	loadFirstValue : function(component) {
    //this is necessary to avoid multiple initializations (same event fired again and again)
    if(this.typeaheadInitStatus[component.getGlobalId()]){ 
        return;
    }
    this.typeaheadInitStatus[component.getGlobalId()] = true;
    this.loadValue(component);

},

loadValue : function(component, skipTypeaheadLoading){
    this.typeaheadOldValue[component.getGlobalId()] = component.get('v.value');
    var action = component.get("c.getCurrentValue");
    var self = this;
    action.setParams({
        'type' : component.get('v.type'),
        'value' : component.get('v.value'),
    });

    action.setCallback(this, function(a) {
        if(a.error && a.error.length){
            //return $A.error('Unexpected error: '+a.error[0].message);
            return null;
        }
        var result = a.getReturnValue();
        component.set('v.isLoading',false);
        component.set('v.nameValue',result);
        if(!skipTypeaheadLoading) self.createTypeaheadComponent(component);
    });
    $A.enqueueAction(action);
}
})