({
  savePartialDownsize : function(cmp, event, helper) {
      console.log('Partial Downsize helper');
      
      //Show Spinner
      this.toggleSpinner(cmp,event,"show");
      
      var action = cmp.get("c.partialDownsize");
      action.setParams({
          "recordId" : cmp.get('v.recordId'),
          "dType" : cmp.get('v.selectedValue'),
          "dAmt" : cmp.get('v.downsizeamount'),
          "creationRemarks" : cmp.get("v.creationRemark")
      });
      
      action.setCallback(this, function(response) {
          var state = response.getState();
          var downSizingResult = response.getReturnValue();
          var pendingDisburseAmount = JSON.parse(JSON.stringify(cmp.get("v.openLoanApplication")));
          
          if(state === "SUCCESS")
          {
              console.log('DML response ' + response.getReturnValue());
              if(downSizingResult == 'Downsizing request initiated successfully.')
              {
                console.log('Downsizing request initiated successfully.');  
                //Hide Spinner
                  this.toggleSpinner(cmp,event,"hide");
                  this.showToast(cmp, event, 'Downsizing request initiated successfully.', 'success');
                  $A.get('e.force:refreshView').fire();
                  $A.get("e.force:closeQuickAction").fire();
              }
              else if(downSizingResult == 'Issue with Inserting Loan Downsizing Request')
              {
                console.log('Issue with Inserting Loan Downsizing Request');  
                //Hide Spinner
                  this.toggleSpinner(cmp,event,"hide");
                  this.showToast(cmp, event, 'Error creating Loan Downsizing request - Contact Administrator.', 'error');

                  this.showToast(cmp, event, downSizingResult, 'error');
                  //$A.get('e.force:refreshView').fire();
                  //$A.get("e.force:closeQuickAction").fire();
              }
                  else if(downSizingResult == 'Invalid Values for type and Amount')
                  {
                      console.log('Invalid Values for type and Amount');
                      //hide Spinner
                      this.toggleSpinner(cmp,event,"hide");
                      this.showToast(cmp, event, 'Error creating Loan Downsizing request - Contact Administrator.', 'error');
                      //$A.get('e.force:refreshView').fire(); 
                      //$A.get("e.force:closeQuickAction").fire();
                  }
                      else
                      {
                          console.log('Unkonwn Error Message');
                          ///Hide Spinner
                          this.toggleSpinner(cmp,event,"hide");
                          document.getElementById("Error").innerHTML ="Note: There is an in progress tranche on this application, which will be cancelled once you initiate downsizing.";
                          //this.showToast(cmp, event, helper, 'Error: '+ downSizingResult, 'error');
                          //this.showToast(cmp, event, helper, 'Unkonwn Error - Contact Administrator.', 'error');
                      }
          }
          else
          {
              console.log('Log Error');
              //Hide Spinner
              this.toggleSpinner(cmp,event,"hide");
              this.showToast(cmp, event,'Server error - Contact Administrator.', 'error');
          }
      });
      $A.enqueueAction(action);
  },
  saveFullDownSize : function(cmp,event,helper)
  {
      //Show Spinner
      this.toggleSpinner(cmp,event,"show");
      var pendingDisburseAmount = JSON.parse(JSON.stringify(cmp.get("v.openLoanApplication")));
      var action = cmp.get("c.fullDownsize");
      
      action.setParams({
          "recordId" : cmp.get('v.recordId'),
          "dType" : cmp.get('v.selectedValue'),
          "creationRemarks" : cmp.get("v.creationRemark")
      });
      
      action.setCallback(this, function(response) {
          
          console.log('Inside Response - Full Downsize..');
          
          var state = response.getState();
          var downSizingResult;
          
          if(state === "SUCCESS")
          {
              downSizingResult = response.getReturnValue();
              
              console.log('DML response -- ' + response.getReturnValue());
              console.log('++++ Test1 +++ ');
              if(downSizingResult=='Downsizing request initiated successfully.')
              {
                  console.log('++++ Test2 +++ ');
                  //hide Spinner
                  this.toggleSpinner(cmp,event,"hide");
                  this.showToast(cmp, event, helper, 'Loan Downsizing request created successfully.', 'success');
                  $A.get('e.force:refreshView').fire();
                  $A.get("e.force:closeQuickAction").fire();
              }
              else if(downSizingResult=='Issue with Inserting Loan Downsizing Request')
              {
                  console.log('++++ Test3 +++ ');
                  //Hide Spinner
                  this.toggleSpinner(cmp,event,"hide");
                  this.showToast(cmp, event, helper, 'Error creating Loan Downsizing request - Contact Administrator.', 'error');
                  //$A.get('e.force:refreshView').fire();
                  //$A.get("e.force:closeQuickAction").fire();
              }
                 /* else((downSizingResult='Invalid Values for type and Amount')&&((pendingDisburseAmount.Status_c!='Cancelled')||(pendingDisburseAmount.Status_c!='Disbursed')||(pendingDisburseAmount.Status_c!='Rejected')))
                  {
                      //Hide Spinner
                      this.toggleSpinner(cmp,event,"hide");
                      //this.showToast(cmp, event, helper, 'Error creating Loan Downsizing request - Contact Administrator.', 'error');
                      //document.getElementById("Error").innerHTML ="Note: There is an in progress tranche on this application, which will be cancelled once you initiate downsizing.";
                      //document.getElementById("Error").style.display = "none";
                      //document.getElementById("Error").style.display = "block";
                      //$A.get('e.force:refreshView').fire();
                      //$A.get("e.force:closeQuickAction").fire();
                  }*/
                      else
                      {
                          console.log('++++ Test3 +++ ');
                          console.log('Unkonwn Error Message');
                          //Hide Spinner
                          this.toggleSpinner(cmp,event,"hide");
                          this.showToast(cmp, event, helper, 'Error: '+ downSizingResult, 'error');

                          //this.showToast(cmp, event, helper, 'Unkonwn Error - Contact Administrator.', 'error');
                      }
          }
          else
          {
              console.log('++++ Test4 +++ ');
              // Add toast message
              console.log('Log Error');
              //Hide Spinner
              this.toggleSpinner(cmp,event,"hide");
              this.showToast(cmp, event, helper, 'Server error - Contact Administrator.', 'error');
          }
          
      });
      $A.enqueueAction(action);
  },
  //Function to toggle spinner
  toggleSpinner : function(component,event,str)
  {
      var findSpinner = component.find("mySpinner");
      if(str=="show")
      {
          $A.util.removeClass(findSpinner, "slds-hide");
          $A.util.addClass(findSpinner, "slds-show");   
      }
      else
      {
          $A.util.removeClass(findSpinner, "slds-show");
          $A.util.addClass(findSpinner, "slds-hide");   
      }
      
  },
  //Function to show toast message:
  showToast : function(component, event,message,type)
  {
      var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
          "title": type.toUpperCase(),
          "message": message,
          "type" : type,
      });
      toastEvent.fire();
  },
  //Method to check blank String..
  checkBlankString : function(str)
  {
      //console.log('str val+ '+str+' '+str.length);
      if (str == null ||  str == undefined || str.length == 0)
      {
          console.log('Blank String');
          return false;  
      }
      console.log('non null string');
      return true;
  },
  
})