({
    doInit : function(cmp, event, helper) {
        console.log('Initiated');
        //show spinner
        //helper.toggleSpinner(component,event,helper,"show");
        //helper.toggle(component, event, 'show');
        
        //Show Spinner
        /*helper.toggleSpinner(cmp, event, "show");
        var action1 = cmp.get("c.checkSalesUser");
        action1.setParams({ lappId : cmp.get("v.recordId") });
        
        action1.setCallback(this, function(response) {
            var state1 = response.getState();
            console.log('inside Callback 1');
            if(state1 === "SUCCESS"){
                console.log(response.getReturnValue());
                if(response.getReturnValue() == true){
                    var action = cmp.get("c.getLoanApp");
                    action.setParams({ lappId : cmp.get("v.recordId") });
                    
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        console.log('inside Callback');
                        if(state === "SUCCESS")
                        {
                            //console log
                            console.log('response 1+ '+response.getReturnValue());
                            
                            if(helper.checkBlankString(JSON.parse(JSON.stringify(response.getReturnValue())).Loan_Downsizings__r))
                            {
                                console.log('Disable UI');
                                
                                cmp.set("v.isDisable",true);
                                
                                helper.showToast(component, event,'Initated or Approved Loan Downsizing exists.','error');
                                
                                
                            }
                            cmp.set("v.openLoanApplication",response.getReturnValue());
                            cmp.set("v.tranches",response.getReturnValue().Tranches__r);
                            console.log('no downsizing');
                            var jsonObj = JSON.parse(JSON.stringify(response.getReturnValue()));
                            //alert(jsonObj.Tranches__r[0].Status__c);
                            //if(jsonObj.Tranches__r != null){
                            console.log(jsonObj.Tranches__r);
                            var attVal= jsonObj.Tranches__r;
                            if(!($A.util.isUndefinedOrNull(attVal))){
                                cmp.set("v.trunc",jsonObj.Tranches__r[0].Status__c);
                            }
                            console.log('HERE'+cmp.get("v.trunc"));
                            //}
                            //alert(jsonObj.Tranches__r[0].Status__c);
                            
                            //cmp.set("v.ErrorApplication",response.getReturnValue());
                            
                            //Hide Spinner
                            helper.toggleSpinner(cmp, event,"hide");  
                            console.log('Loan Application '+JSON.stringify(cmp.get("v.openLoanApplication")));
                        }
                        else
                        {
                            //Hide Sinner
                            helper.toggleSpinner(cmp, event,"hide");  
                            helper.showToast(cmp, event, 'Server Error - Contact Administrator');
                            console.log('error');  
                        }
                        //Hide Spinner
                        //helper.toggleSpinner(component, event, helper, 'hide');
                    });
                }
                else if(response.getReturnValue() == false){
                    helper.toggleSpinner(cmp, event,"hide");  
                    helper.showToast(cmp, event, 'You are not authorized to initiate downsizing.');
                    console.log('sales user does not match');
                }
            }
            else
            {
                //Hide Sinner
                helper.toggleSpinner(cmp, event,"hide");  
                helper.showToast(cmp, event, 'Server Error - Contact Administrator');
                console.log('error');  
            }
        });*/
        
        
        var action = cmp.get("c.getLoanApp");
        action.setParams({ lappId : cmp.get("v.recordId") });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('inside Callback');
            if(state === "SUCCESS")
            {
                //console log
                console.log('response 1+ '+response.getReturnValue());
                
                
                if(helper.checkBlankString(JSON.parse(JSON.stringify(response.getReturnValue())).Loan_Downsizings__r))
                {
                    console.log('Disable UI');
                    
                    cmp.set("v.isDisable",true);
                    
                    helper.showToast(component, event,'Initated or Approved Loan Downsizing exists.','error');
                    
                    
                }
                cmp.set("v.openLoanApplication",response.getReturnValue());
                cmp.set("v.tranches",response.getReturnValue().Tranches__r);
                console.log('no downsizing');
                var jsonObj = JSON.parse(JSON.stringify(response.getReturnValue()));
                //alert(jsonObj.Tranches__r[0].Status__c);
                //if(jsonObj.Tranches__r != null){
                console.log(jsonObj.Tranches__r);
                var attVal= jsonObj.Tranches__r;
                if(!($A.util.isUndefinedOrNull(attVal))){
                	cmp.set("v.trunc",jsonObj.Tranches__r[0].Status__c);
                }
                console.log('HERE'+cmp.get("v.trunc"));
            	//}
                //alert(jsonObj.Tranches__r[0].Status__c);
                
                //cmp.set("v.ErrorApplication",response.getReturnValue());
                
                //Hide Spinner
                helper.toggleSpinner(cmp, event,"hide");  
                console.log('Loan Application '+JSON.stringify(cmp.get("v.openLoanApplication")));
                           
        	}
            else
            {
                //Hide Sinner
                helper.toggleSpinner(cmp, event,"hide");  
                helper.showToast(cmp, event, 'Server Error - Contact Administrator');
                console.log('error');  
            }
            //Hide Spinner
            //helper.toggleSpinner(component, event, helper, 'hide');
        });
        $A.enqueueAction(action);
    },
    
    activeButton : function(component, event, helper){
        var allValid = component.find('inpcreation').get("v.validity").valid; 
        
        if(!allValid){
          component.set('v.isButtonActive',true);  
        }else{
           component.set('v.isButtonActive',false);  
        }
        if(document.getElementById("Error") != null)
        {
        document.getElementById("Error").style.display = "block";
        }
       },
    
    selectType : function(cmp, event, helper){
        var pickValue = cmp.get("v.selectedValue");
        console.log('pickValue+ '+pickValue);
        
        if(pickValue == 'Partial Downsize')
        {
            cmp.set("v.showBlockDef",true);
        }
        else
        {
            cmp.set("v.showBlockDef",false);
        }
    },
    saveRecord : function(cmp, event, helper){
        //var spinner = cmp.find("mySpinner");
        //$A.util.toggleClass(spinner, "slds-hide");
        //helper.toggleSpinner(component, event, helper, 'show');
        
        console.log('Inside saveRecord Method');
        
        //Show Spinner
        helper.toggleSpinner(cmp, event, "show");
        
        var showBlockDef = cmp.get("v.showBlockDef");
        var sel = cmp.get('v.selectedValue');
        var dAmt = parseFloat(cmp.get('v.downsizeamount'));
        var creationReason = cmp.get('v.creationRemark');
        
        //var recID = cmp.get('v.recordId');
        var downSizeLimit = parseFloat($A.get("$Label.c.Tranche_Limit"));
        
        if(helper.checkBlankString(creationReason))
        {
            if(showBlockDef === true && sel ==='Partial Downsize')
            {
                var pendingDisburseAmount = JSON.parse(JSON.stringify(cmp.get("v.openLoanApplication")));
                if(dAmt === null || dAmt === "")
                {
                    ///Hide Spinner
                    helper.toggleSpinner(cmp, event, "hide");
                    helper.showToast(cmp, event, 'Downsize Amount cannot be blank.', 'error');
                }
                else if(pendingDisburseAmount.Pending_Amount_for_Disbursement__c === undefined || pendingDisburseAmount.Pending_Amount_for_Disbursement__c === null)
                {
                    //hide Spinner
                    helper.toggleSpinner(cmp, event, "hide");
                    helper.showToast(cmp, event, 'Pending Amount for Disbursement on Loan Application is Invalid.', 'error');  
                }
                /*
                else if(dAmt < downSizeLimit)
                {
                    console.log('Compare downsize limit and downsize amount-- '+(dAmt < downSizeLimit));
                    helper.toggleSpinner(component, event, helper, 'hide');
                    helper.showToast(component, event, helper, 'Downsize Limit cannot be less than Tranche Limit--'+downSizeLimit, 'error');
                }*/
                    else if((pendingDisburseAmount.Pending_Amount_for_Disbursement__c - dAmt) < downSizeLimit)
                    {
                        console.log('Check on Remaining Amount after the downsizing');
                        //hide Spinner
                        helper.toggleSpinner(cmp, event, "hide"); 
                        helper.showToast(cmp, event, "Remaining disbursement amount after Partial downsizing cannot be less than INR "+downSizeLimit, "error");
                        
                    }
                        else
                        {
                            console.log('Calling Save-Partial Helper..');
                            //hide Spinner
                            helper.toggleSpinner(cmp, event, "hide");                 
                            helper.savePartialDownsize(cmp,event,helper);   
                        }
            }
            else if(showBlockDef===false && sel === 'Full Downsize')
            {
                console.log('Calling Full Downsize helper-- '+ cmp.get('v.recordId'));
                //Sjow Spinner
                helper.toggleSpinner(cmp, event, "show");
                helper.saveFullDownSize(cmp,event,helper);
            }  
        }
        else
        {
            //hide Spinner
            helper.toggleSpinner(cmp, event, "hide");
            helper.showToast(cmp, event, 'Creation Remarks is mandatory to create Loan Downsize.', 'error');
        }
        //$A.util.toggleClass(spinner, "slds-hide");
    }
})