({
	 doInit : function(component, event, helper) {
    	var action = component.get("c.getLAInfo");
    	
        action.setParams({
            "laId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var res =  response.getReturnValue();
                var warning = false;
                
                if((res.la.Sub_Stage__c == 'Credit Review' || res.la.Sub_Stage__c == 'Re Credit' || res.la.Sub_Stage__c == 'Re-Look') && (res.profileName == 'Credit Team' || res.profileName == 'System Administrator')){
                    if(res.la.OwnerId == res.la.Assigned_Credit_Review__c && res.lstUD != null){
                        for(var i=0; i<res.lstUD.length; i++){
                            if(res.lstUD[i].recommendersDecision == '' || res.lstUD[i].recommendersDecision == undefined || res.lstUD[i].approversDecision == '' || res.lstUD[i].approversDecision == undefined){
                                warning = true;
                            }
                        }
                    }
                    else{
                        for(var i=0; i<res.lstUD.length; i++){
                            if(res.lstUD[i].recommendersDecision == '' || res.lstUD[i].recommendersDecision == undefined){
                                warning = true;
                            }
                        }
                    }
                }
                else if(res.la.Sub_Stage__c == 'L1 Credit Approval' && res.lstUD != null && (res.profileName == 'Credit Team' || res.profileName == 'System Administrator')){
                    if(res.la.OwnerId != res.currentUserId && res.currentUserId == res.la.Assigned_Credit_Review__c){
                        for(var i=0; i<res.lstUD.length; i++){
                            if(res.lstUD[i].approversDecision == '' || res.lstUD[i].approversDecision == undefined){
                                warning = true;
                            }
                        }
                    }
                }
                else if(res.la.Sub_Stage__c == 'Credit Authority Approval' && res.lstUD != null && (res.profileName == 'Credit Team' || res.profileName == 'System Administrator')){
                	for(var i=0; i<res.lstUD.length; i++){
                    	if(res.lstUD[i].approversDecision == '' || res.lstUD[i].approversDecision == undefined){
                            warning = true;
                        }
                    }
                }
                
                if(warning){
                    component.set("v.showWarning",true);
                }
                else{
                    component.set("v.showWarning",false);
                }
            }
        });
        $A.enqueueAction(action);
    }
})