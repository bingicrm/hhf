({
    //doinit addedf by Aman for --> getting RecTypeID for customer recordForm  
    doInit : function(component, event, helper) {
        //server call to get the Record type
        var opt= []; 
        var action = component.get("c.getRecordType");
        action.setCallback(this,function(response) {
            var status = response.getState();
            
            if(status === "SUCCESS"){
                console.log(response.getReturnValue());
                var arr = [];
                var result =response.getReturnValue();
                var IndvId =result.Individual;
                var CorpvId =result.Corporate;
                console.log(IndvId +' '+CorpvId)
                component.set('v.IndvId',IndvId);
                component.set('v.CorpvId',CorpvId);
                opt.push({label:'Individual',value:'Individual'});
                opt.push({label:'Corporate',value:'Corporate'});
                component.set("v.options", opt);  
            }
        });
        $A.enqueueAction(action);
    },
    itemSelected : function(component, event, helper) {
        helper.itemSelected(component, event, helper);
    }, 
    serverCall :  function(component, event, helper) {
        helper.serverCall(component, event, helper);
    },
    
    clearSelection : function(component, event, helper){
               /* Patch to resolve the clearing of lookup field on press of Enter key --- added by Aman*/	
        	
        var isFocused= false;	
        var custlkupval = document.getElementById('btnid');	
        isFocused = ((document.activeElement.localName === 'button'));// check if the btn is pressed and element is in focus	
        if (isFocused) {	
            helper.clearSelection(component, event, helper);	
            component.set('v.OnlyForNewLaCheck',false);  	
        }	
        else	
        {	
            event.preventDefault();// this stops the form page to relaod after pressing enter key	
        }	
        /* end of Patch to resolve the clearing of lookup field on press of Enter key --- added by Aman*/ 
    } ,
    // added by Aman for handling the Customer creation  
    CallNewRecordField: function(component, event, helper){
        // console.log('called');
        component.set('v.addAccount','true');
        component.set('v.showRadioGroup','true');
        component.set('v.showForm','false'); 
    },
    closeModal: function(component, event, helper){
        component.set('v.addAccount','false');
    },
    onCancel : function(component, event, helper){
        component.set('v.addAccount','false');
        component.set('v.showRadioGroup','false');
        component.set('v.showForm','false'); 
        component.set('v.OnlyForNewLaCheck','false');
        component.set("v.dupeBlocker", 'false');
        component.set("v.tableData",[]);
        component.set("v.isAllowedToSave", false);
        component.set('v.isScriptThrownError',false);
    },
    AddOption : function(component, event, helper){
        console.log('In add  option');
        var dropTrigger = component.find('sldsopen');	
        $A.util.addClass(dropTrigger,'slds-is-open');	
        $A.util.removeClass(dropTrigger,'slds-is-close')
        
        var valueFrom = component.get('v.OnlyForNewLA');
        if(valueFrom)
        {
            component.set('v.OnlyForNewLaCheck',true);
            var searchText = event.target.value; 
            console.log('searchTextfocus::'+searchText);
        }
    },
    handleClick :function(component, event, helper) {
        component.set("v.showForm", true);
        component.set("v.showRadioGroup", false);
        component.set("v.dupeBlocker", false);
        component.set("v.isAllowedToSave", false);
        component.set("v.tableData",[]);
        component.set('v.isScriptThrownError',false);
        //document.getElementById("modal-content-id-1").style.height = "70%";
    },
    changeFields : function(component, event, helper) {
        component.set("v.disableRadioBtn",false);
        var value= component.get("v.value");
        if(value=="Individual")
        {
            component.set('v.finalRecType',component.get('v.IndvId'));
        }
        if(value=="Corporate")
        {
            component.set('v.finalRecType',component.get('v.CorpvId'));
        }                        
        
    },
    onsubmit :function(component, event, helper) {
        console.log('HIIIIIIIIIIIIIIIIII');
        component.set('v.notloaded',true);
        /* Patch to Show Full name for Individual Account --added by Aman*/
        event.preventDefault();      
        var fields = event.getParam('fields');
        component.set('v.isScriptThrownError',false);
        if(component.get('v.finalRecType')==component.get('v.IndvId'))
        {
            //get data from the input form
            var NameCmp =component.find('indVName');
            var salutation=NameCmp.get('v.salutation');
            var firstName=NameCmp.get('v.firstName');
            var middleName=NameCmp.get('v.middleName');
            var lastName=NameCmp.get('v.lastName');
            
            //Assign value to RecordEditFormFields
            fields.Salutation=salutation;
            fields.FirstName=firstName;
            fields.MiddleName=middleName;
            fields.LastName=lastName;
            
        }
        //submit the Form
        //duplicateDatatoSave 
        component.set('v.duplicateDatatoSave',fields);
        //component.find('myRecordForm').submit(fields);
        if(component.get('v.isAllowedToSave') && component.find('ProceedAnyway').get('v.label')=='Proceed Anyway')
        {
           helper.SaveDupeData(component, event, helper,fields);   
        }
        else
        {
            console.log('INNNNNNNNN THIS TAG');
         component.find('myRecordForm').submit(fields);  
        }
        
        
        /* End of Patch to Show Full name for Individual Account --added by Aman*/
        
    },
    handleSuccess :function(component, event, helper){
        var params = event.getParams();
        var updatedRecord = (JSON.stringify(event.getParams()));
        var response = event.getParam("response");
        var fields = response.fields; // event.getParam("response");
        var txtvalue ='';
        if(component.get('v.finalRecType')==component.get('v.CorpvId'))
        {
            txtvalue=fields.Name.value;
        }
        if(component.get('v.finalRecType')==component.get('v.IndvId'))
        {
            var midVal=''
            if($A.util.isUndefinedOrNull(fields.MiddleName.value))
            {
                txtvalue=fields.Salutation.value+' '+fields.FirstName.value+' '+fields.LastName.value;
            }
            else
            {
                txtvalue=fields.Salutation.value+' '+fields.FirstName.value+' '+fields.MiddleName.value+' '+fields.LastName.value;
            }
            
        }
        var accResultobj = {
            val:response.id,
            text:txtvalue,
            objName:response.apiName
        }
        component.set('v.selItem',accResultobj);
        console.log(JSON.stringify( component.get('v.selItem')));
        component.set('v.addAccount','false');
        component.set("v.server_result",null);
        component.set('v.OnlyForNewLaCheck','false');
        component.set('v.showRadioGroup','false');
        component.set('v.showForm','false');
        component.set('v.notloaded','false');
    } , 
    //End of the block added by Aman
    handleError: function(component, event, helper){
        var fields= component.get('v.duplicateDatatoSave');
        component.set('v.notloaded',false);
        /* Patch to Show ErrorBased on duplicate record for Individual Account --added by Aman*/
        var message = '';
        var errors = event.getParams();
        console.log('Errors '+ JSON.stringify(errors));
        var matchResult=[];
        var errormessages = errors.output;
        component.set("v.dupeBlocker", false);
        if ($A.util.isEmpty(errormessages.errors) === false) {
            if (errormessages.errors.length > 0) {
                for (var j = 0; errormessages.errors.length > j; j++) {
                    var fielderror = errormessages.errors[j];
                    
                    if (fielderror.errorCode === 'DUPLICATES_DETECTED' ) {
                        component.set("v.msgFromDup", fielderror.message);
                        component.set("v.isAllowedToSave", fielderror.duplicateRecordError.matchResults[j].isAllowSave);
                        component.set("v.dupeBlocker", true);
                        component.set("v.dataisLoading",true);
                        console.log(fielderror.duplicateRecordError);
                        matchResult=fielderror.duplicateRecordError.matchResults[j].matchRecordIds;
                        //console.log(matchResult);
                    }
                    else { 
                        event.preventDefault();
                        if(fielderror.errorCode === 'DUPLICATE_VALUE') {
                            component.set("v.msgError", 'Duplicate Record exists');
                        }
                        //other error
                        component.set("v.msgError", fielderror.message);
                        component.set("v.showMsgError", true);
                        component.set("v.dupeBlocker", false);
                    }
                }
                if($A.util.isEmpty(matchResult) === false)
                {
                    //console.log('Set '+matchResult);
                    
                    component.set('v.columns', [
                        {label: 'Customer name',fieldName: 'Name', type: 'text'},
                        {label: 'Date Of Birth' ,fieldName: 'Date_of_Birth__c', type: 'date'},
                        {label: 'PAN' ,fieldName: 'PAN__c', type: 'text'},
                        {label: 'Mobile' ,fieldName: 'PersonMobilePhone', type: 'phone'},
                        {label: 'Phone' ,fieldName: 'Phone', type: 'phone'}]);
                    helper.getAccRecord(component, event, helper,matchResult); 
                }
            }
            
        }
        else
        {
            component.set("v.dupeBlocker", false);
        }
        
        
        
    },
    
    /* End of Patch to Show ErrorBased on duplicate record for Individual Account --added by Aman*/
    backToRadioButtons: function(component, event, helper){
        component.set("v.showForm", false);
        component.set("v.showRadioGroup", true);
        component.set("v.dupeBlocker", false);
        component.set("v.isAllowedToSave", false);
        component.set("v.tableData",[]);
        component.set('v.isScriptThrownError',false);
        //document.dupeBlocker("modal-content-id-1").style.height = "20%";
    },
    mobilChange : function(component, event, helper){
        console.log('Mobile number changed');
       component.set("v.isAllowedToSave", false); 
    },
    handleFocusOut :function(component, event, helper){	
         var dropTrigger = component.find('sldsopen');	
        $A.util.removeClass(dropTrigger,'slds-is-open')	
        $A.util.addClass(dropTrigger,'slds-is-close');	
    }
    
})