({
    itemSelected : function(component, event, helper) {
        component.set('v.OnlyForNewLaCheck',false); //Added by aman
        var target = event.target;   
        var SelIndex = helper.getIndexFrmParent(target,helper,"data-selectedIndex");  
        if(SelIndex){
            var serverResult = component.get("v.server_result");
            var selItem = serverResult[SelIndex];
            if(selItem.val){
                component.set("v.selItem",selItem);
                component.set("v.last_ServerResult",serverResult);
            } 
            
            component.set("v.server_result",null); 
            var cmpEvent = component.getEvent("cmpEvent");
            cmpEvent.setParams({
                "message" : selItem.val
            });
            cmpEvent.fire();
        } 
    }, 
    serverCall : function(component, event, helper) {  
        var target = event.target;  
        console.log('event.target::'+event.target); //Added by Aman
        var searchText = target.value; 
        // Added by aman
        var valueFrom = component.get('v.OnlyForNewLA');
        if(valueFrom)
        {
            component.set('v.OnlyForNewLaCheck',true);
            console.log('searchText::'+searchText);
            if($A.util.isEmpty(searchText))
            {
                component.set('v.OnlyForNewLaCheck',false);  
            }
        }
        // End of the block Added by aman
        var last_SearchText = component.get("v.last_SearchText");
        //Escape button pressed 
        if (event.keyCode == 27 || !searchText.trim()) { 
            helper.clearSelection(component, event, helper);
        }
        //Modified by aman
        else if(searchText.trim() != last_SearchText  && /\s+$/.test(searchText) && searchText.trim() != null && searchText.trim() != ''){ 
            //End of the patch -Modified by aman
            //Save server call, if last text not changed
            //Search only when space character entered
            console.log('I am in else If::'); //Added by aman
            var objectName = component.get("v.objectName");
            var field_API_text = component.get("v.field_API_text");
            var field_API_val = component.get("v.field_API_val");
            var field_API_search = component.get("v.field_API_search");
            var field_API_PAN = component.get("v.field_API_PAN");//// added by Aman
            var limit = component.get("v.limit");
            var loanApp = component.get("v.loanApp");
            var scheme = component.get("v.scheme");
            
            var action = component.get('c.searchDB');
            //action.setStorable();
            
            action.setParams({
                objectName : objectName,
                fld_API_Text : field_API_text,
                fld_API_Val : field_API_val,
                lim : limit, 
                fld_API_Search : field_API_search,
                field_API_PAN :field_API_PAN,// added by Aman
                searchText : searchText,
                loanApp: loanApp,
                scheme: scheme
            });
            
            action.setCallback(this,function(a){
                this.handleResponse(a,component,helper);
            });
            
            component.set("v.last_SearchText",searchText.trim());
            console.log('Server call made');
            console.log('In add  option');
            var dropTrigger = component.find('sldsopen');	
            $A.util.addClass(dropTrigger,'slds-is-open');	
            $A.util.removeClass(dropTrigger,'slds-is-close')
            
            var valueFrom = component.get('v.OnlyForNewLA');
            if(valueFrom)
            {
                component.set('v.OnlyForNewLaCheck',true);
                var searchText = event.target.value; 
                console.log('searchTextfocus::'+searchText);
            }
            $A.enqueueAction(action); 
        }else if(searchText && last_SearchText && searchText.trim() == last_SearchText.trim()){ 
            component.set("v.server_result",component.get("v.last_ServerResult"));
            var dropTrigger = component.find('sldsopen');	
            $A.util.addClass(dropTrigger,'slds-is-open');	
            $A.util.removeClass(dropTrigger,'slds-is-close')
            
            var valueFrom = component.get('v.OnlyForNewLA');
            if(valueFrom)
            {
                component.set('v.OnlyForNewLaCheck',true);
                var searchText = event.target.value; 
                console.log('searchTextfocus::'+searchText);
            }
            console.log('Server call saved');
        }         
    },
    handleResponse : function (res,component,helper){
        if (res.getState() === 'SUCCESS') {
            var retObj = JSON.parse(res.getReturnValue());
            if(retObj.length <= 0){
                var noResult = JSON.parse('[{"text":"No Results Found"}]');
                component.set("v.server_result",noResult); 
                component.set("v.last_ServerResult",noResult);
            }else{
                component.set("v.server_result",retObj); 
                component.set("v.last_ServerResult",retObj);
            }  
        }else if (res.getState() === 'ERROR'){
            var errors = res.getError();
            if (errors) {
                if (errors[0] && errors[0].message) {
                    alert(errors[0].message);
                }
            } 
        }
    },
    getIndexFrmParent : function(target,helper,attributeToFind){
        //User can click on any child element, so traverse till intended parent found
        var SelIndex = target.getAttribute(attributeToFind);
        while(!SelIndex){
            target = target.parentNode ;
            SelIndex = helper.getIndexFrmParent(target,helper,attributeToFind);           
        }
        return SelIndex;
    },
    clearSelection: function(component, event, helper){
        component.set("v.selItem",null);
        component.set("v.server_result",null);
        
    } ,
    /* Patch to Show ErrorBased on duplicate record for Individual Account --added by Aman*/
    getAccRecord : function(component, event, helper,matchResult)
    {
        var action = component.get('c.getDupRecord');
        action.setParams({'AccID' :matchResult});
        action.setCallback(this,function(response){
            var state=response.getState();
            var result= response.getReturnValue();
            component.set('v.tableData',result);
            component.set('v.duplicateRecordData',result); 
            component.set("v.dataisLoading",false);
            
        },'SUCCESS');
        $A.enqueueAction(action); 
    },
    SaveDupeData :function(component, event, helper,fields)
    {
        console.log('e3y7237237723723');
        var insertDupRecord = fields;
        console.log('insertDupRecord '+JSON.stringify(insertDupRecord));
        var txtvalue='';
        var action = component.get('c.insertDupRecord');
        action.setParams({'AccID' :fields});
        action.setCallback(this,function(response){
            var state=response.getState();
            var result= response.getReturnValue();
            console.log('insertDupRecord '+JSON.stringify(result));
            component.set('v.notloaded','false');
            if(component.get('v.finalRecType')==component.get('v.CorpvId'))
            {
                txtvalue=fields.Name.value;
            }
            if(component.get('v.finalRecType')==component.get('v.IndvId'))
            {
                var midVal=''
                if($A.util.isUndefinedOrNull(fields.MiddleName.value))
                {
                    txtvalue=fields.Salutation+' '+fields.FirstName+' '+fields.LastName;
                }
                else
                {
                    txtvalue=fields.Salutation+' '+fields.FirstName+' '+fields.MiddleName+' '+fields.LastName;
                }
                
            }
            if(result.Id != undefined)
            {
                var accResultobj = {
                    val:result.Id,
                    text:txtvalue,
                    objName:'Account'
                }
                 console.log('In data '+result.Id);
                component.set('v.selItem',accResultobj);
                component.set('v.addAccount','false');
                component.set("v.server_result",null);
                component.set('v.OnlyForNewLaCheck','false');
                component.set('v.showRadioGroup','false');
                component.set('v.showForm','false');
                component.set('v.notloaded','false'); 
            }
            
        },'SUCCESS');
        $A.enqueueAction(action); 
    }
    /*End of Patch to Show ErrorBased on duplicate record for Individual Account --added by Aman*/
})