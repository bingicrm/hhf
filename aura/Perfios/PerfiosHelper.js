({
    MAX_FILE_SIZE: 4500000,
    CHUNK_SIZE: 750000, 
    uploadHelper: function(component, event) {
           
        component.set("v.showLoadingSpinner", true);
        
        var fileInput = component.find("fileId").get("v.files");
        
        var file = fileInput[0];
        var self = this;
        var objFileReader = new FileReader();
        
        objFileReader.onload = $A.getCallback(function() {
            var fileContents = objFileReader.result;
            var base64 = 'base64,';
            var dataStart = fileContents.indexOf(base64) + base64.length;
 
            fileContents = fileContents.substring(dataStart);
            self.uploadProcess(component, file, fileContents);
        });
 
        objFileReader.readAsDataURL(file);
    },
 
    uploadProcess: function(component, file, fileContents) {
        
        var startPosition = 0;
        
        var endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);
 
        
        this.uploadInChunk(component, file, fileContents, startPosition, endPosition, '');
    },
 
 
    uploadInChunk: function(component, file, fileContents, startPosition, endPosition, attachId) {
        
        var getchunk = fileContents.substring(startPosition, endPosition);
        var action = component.get("c.saveChunk");
        action.setParams({
            parentId: component.get("v.recordId"),
            fileName: file.name,
            base64Data: encodeURIComponent(getchunk),
            contentType: file.type,
            fileId: attachId
        });
 
        
        action.setCallback(this, function(response) {
             
            attachId = response.getReturnValue();
            var state = response.getState();
            if (state === "SUCCESS") {
                
                startPosition = endPosition;
                endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);
                
                if (startPosition < endPosition) {
                    this.uploadInChunk(component, file, fileContents, startPosition, endPosition, attachId);
                } else {
                    alert('your File is uploaded successfully');
                    /*
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "The file has been uploaded successfully."
                    });
                    toastEvent.fire();
                    */
                    component.set("v.showLoadingSpinner", false);
                }
                   
            } else if (state === "INCOMPLETE") {
                alert("From server: " + response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    fetchrecordtype: function(component, event) {
    
        var action = component.get("c.getrecordid");
        //console.log(component.get('v.recordId'));
        action.setParams({
            "recordId" : component.get('v.recordId')
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                console.log(result.getReturnValue());
                component.set("v.recordtype",result.getReturnValue());
                console.log('id'+ component.get('v.recordtype'));
            }
            else
            {
                console.log('failure');
            }
        });
        $A.enqueueAction(action);
        
    },
    mailSent: function(component, event) {
        component.set("v.spinner", true); 
        var fromdate = component.get("v.fromdate");
        var todate = component.get("v.todate");
        var bankvalue = component.get('v.selected');
        var mobile = component.get('v.loanConMobile');
        var email = component.get('v.loanConEmail');
        if(bankvalue!='ITR') {
            var perfiosBankInformationId = component.find("Scheme").get("v.value");
        }
        console.log('wddwdwd1212');
        var action = component.get("c.sendEmail");
        //var selectedmain = 'Banking';
        if(component.get("v.selected") !=''){
        var selectedmain = component.get("v.selected");
            }
        else if(component.get("v.selected1") !=''){
        	var selectedmain = component.get("v.selected1");
        }
            else{
                var selectedmain = component.get("v.selected2");
            }
        action.setParams({
            "type" : selectedmain,
            "strFromDt" : fromdate,
            "strTodt" : todate,
             "strLoanConId" : component.get("v.LoanConId"),
            "strRecordId" : component.get("v.recordId"),   
            "bankId" : component.get("v.bankId"),
            "selectedNumberOfAssesmentvalue": component.get("v.selectedNumberOfAssesmentvalue"),
            "perfiosBankInformationId": perfiosBankInformationId,
            "mobile" : mobile,
            "email" : email
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                console.log(result.getReturnValue());
                console.log('Success');
                //component.set("v.ApplicantList",result.getReturnValue());
                //console.log('list'+ component.get('v.ApplicantList'));
                var storeResponse = result.getReturnValue();
                console.log('STORERESPONSE '+storeResponse);
                if(storeResponse.includes('Error')) {
                    component.set("v.errorMessage1",storeResponse); 
                    console.log(component.get("v.errorMessage1"));
                } else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type" :"Success",
                    "message": "URL has been sent to the customer."
                });
                toastEvent.fire();
                }
            }
            else
            {
                console.log('failure');
                component.set("v.errorMessage1",storeResponse);
            }
            component.set("v.spinner", false);
        });
        $A.enqueueAction(action);
        
        
    },
    setDocumentList: function (component,event) {
       component.set("v.spinner", true);
       
        var action = component.get("c.fetchDocumentCheckList");
        action.setParams({
            "customerDetailId": component.get('v.LoanConId'),
            "selectedOption" : component.get('v.selected')
        });       
        
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") { 
                var storeResponse = actionResult.getReturnValue();
                console.log('List received'+storeResponse);
                console.log(JSON.stringify(storeResponse));
                if(storeResponse != null) {
                    component.set("v.docCheckListWrapperList", storeResponse);
                } else {
                    component.set("v.errorMessage1", 'No matching document Found');
                }  
                component.set("v.spinner", false);
            }
        });
        $A.enqueueAction(action);      
    },
    showSpinner: function(component, event) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
      
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    }
})