({
    doInit : function(component, event, helper) {
        var action = component.get("c.getLoanAppStage");
            
            action.setParams({
                "recordId" : component.get('v.recordId')
            });
            action.setCallback(this, function(result){
                var state = result.getState();
                if (component.isValid() && state === "SUCCESS"){
                    if(result.getReturnValue().includes('Application Initiation')) {
                        component.set('v.showSendLinkButton',true);
                        component.set('v.showContinueJourneyButton',false);
                    } else {
                        component.set('v.showSendLinkButton',false);
                        component.set('v.showContinueJourneyButton',true);
                    }
                }
            });
             $A.enqueueAction(action);
        //alert('Development in Progress..');
        if(component.get('v.recordId') != null){
            var stati = component.get('v.stat');
            if(stati == 'Banking'){
                
                component.set('v.main',false);
                component.set('v.rad',true);
                component.set('v.typeofbanking','Banking');
            }  
            else if(stati == 'ITR'){
                
                component.set('v.main',false);
                component.set('v.rad',true);
                component.set('v.typeofbanking','ITR');
            }
                else if(stati == 'Financial'){
                    
                    component.set('v.main',false);
                    component.set('v.rad',true);
                    component.set('v.typeofbanking','Financial');
                }
                    else if(stati == 'Both'){
                        
                        component.set('v.main',false);
                        component.set('v.rad',true);
                        component.set('v.typeofbanking','ALL');
                    }
            
                        else{
                            
                            //component.set('v.main',false);
                            //component.set('v.rad',true);
                            component.set('v.typeofbanking','ALL');
                        }
            
            console.log(component.get('v.recordId')); 
            //component.set('v.displaycomponent',true);
            
            var action = component.get("c.getapplicants");
            
            action.setParams({
                "recordId" : component.get('v.recordId')
            });
            action.setCallback(this, function(result){
                var state = result.getState();
                if (component.isValid() && state === "SUCCESS"){
                    component.set("v.ApplicantList",result.getReturnValue());
                    console.log('list'+ component.get('v.ApplicantList'));
                    for (var index = 0; index < result.getReturnValue().length; index++) { 
                        console.log(result.getReturnValue()[index].Constitution); 
                    } 
                }
                else
                {
                    console.log('failure');
                }
            });
            $A.enqueueAction(action);
            
            var action = component.get("c.getFromDate");
            action.setCallback(this, function(result){
                var state = result.getState();
                if (component.isValid() && state === "SUCCESS"){
                    component.set("v.fromdate",result.getReturnValue());     
                }
                else
                {
                    console.log('failure');
                }
            });
            $A.enqueueAction(action);
            
            var action = component.get("c.getToDate");
            action.setCallback(this, function(result){
                var state = result.getState();
                if (component.isValid() && state === "SUCCESS"){
                    component.set("v.todate",result.getReturnValue());     
                }
                else
                {
                    console.log('failure');
                }
            });
            $A.enqueueAction(action);    
        }
        
        var items = [{
            "label": "Banking",
            "name": "1",
            "disabled": false,
            "expanded": true,
            "items": [{
                "label": "Net Banking",
                "name": "2",
                "disabled": false,
                "expanded": false,
                "items": []
            },{
                "label": "Scanned PDF",
                "name": "4",
                "disabled": false,
                "expanded": false,
                "items": []
            }]
        }];
        
        component.set('v.items', items);
        
        var items1 = [{
            "label": "ITR",
            "name": "1",
            "disabled": false,
            "expanded": false,
            "items": [{
                "label": "ITR",
                "name": "2",
                "disabled": false,
                "expanded": false,
                "items": []
            }]
        }];
        
        component.set('v.items1', items1);
        
        var items2 = [{
            "label": "BOTH",
            "name": "1",
            "disabled": false,
            "expanded": false,
            "items": []
        }];
        
        component.set('v.items2', items2);
        
        var items3 = [{
            "label": "Financial Analysis",
            "name": "1",
            "disabled": false,
            "expanded": true,
            "items": [{
                "label": "Financial Upload",
                "name": "2",
                "disabled": false,
                "expanded": true,
                "items": []
            }]
        }];
        
        component.set('v.items3', items3);
        
    }, 
    onProceedFinancial : function(component, event, helper) {
        //alert(component.get('v.LoanConId'));
        var action = component.get("c.addFinancialYearAPI");           
            action.setParams({
                "strFY": component.get('v.financialYear'),
                "strLoanConId" : component.get('v.LoanConId'),
                "finYear" : component.get('v.finYear')
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState();
                console.log('inside apex call result --'+state);
                if (state === "SUCCESS") { 
                    var storeResponse = actionResult.getReturnValue();
                    if(storeResponse != null) {
                        if(storeResponse.includes('Error')) { 
                            component.set('v.errorMessage1',actionResult.getReturnValue());
                            component.set('v.rad',true);
                        } 
                        else if(storeResponse.includes('URL Retrieved Successfully')) {
                            component.set('v.errorMessage1','');
                            component.set('v.main',false);
                            component.set('v.rad',false);
                            component.set('v.upload',true);
                            helper.setDocumentList(component, event);        
                        }
                    }
                }
            });
            $A.enqueueAction(action);
    },
    onSubmit : function(component, event, helper) { 
        component.set('v.errorMessage1','');
        var fromdate = component.get('v.fromdate');
        var todate = component.get('v.todate');
        var bankvalue = component.get('v.selected');
        component.set('v.showCancel',true);
        if(bankvalue == 'ScannedPDFBanking' || bankvalue == 'EstatementBanking') {
            var perfiosBankInformationId=component.find("Scheme").get("v.value"); 
            if(component.get('v.perfiosBankInformationId')==='') {
                component.set('v.bankId',''); 
            }
        }
        
        //bankvalue = 'ITR';
        console.log('from date--->'+fromdate);
        console.log('Bank Value---->'+bankvalue);
        console.log('To Date---->'+todate);
        console.log('Record Id--->'+component.get('v.recordId'));
        console.log('Bank Id ----->' +component.get('v.bankId'));
        console.log('perfiosBankInformationId----->' +component.get('v.perfiosBankInformationId'));
        console.log('FINYEAR '+component.get('v.finYear'));
        console.log('financialYear '+component.get('v.financialYear'));
        /*
        if(bankvalue == 'ScannedPDFBanking'){
        	component.set('v.rad',false);
        }
        */
            console.log('inside apex call');
            var action = component.get("c.callPerfios1");           
            action.setParams({
                "strRecordId": component.get('v.recordId'), 
                "strLoanConId" : component.get('v.LoanConId'),
                "strIntType": bankvalue,
                "strFromDt" : fromdate,
                "strTodt" : todate,
                "bankId" : component.get('v.bankId'),
                "financialYear": component.get('v.financialYear'),
                "finYear" : component.get('v.finYear'),
                "selectedNumberOfAssesmentvalue": component.get('v.selectedNumberOfAssesmentvalue'),
                "perfiosBankInformationId": perfiosBankInformationId
            });            
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState();
                console.log('inside apex call result --'+state);
                if (state === "SUCCESS") { 
                    var storeResponse = actionResult.getReturnValue();
                    if(storeResponse != null) {
                        if(storeResponse.includes('Error')) { 
                            component.set('v.errorMessage1',actionResult.getReturnValue());
                            component.set('v.rad',true);
                        } 
                        
                        else if((bankvalue == 'ScannedPDFBanking' || bankvalue == 'EstatementBanking' ) && storeResponse.includes('Success')){                         
                            console.log('if Success --'+state);
                            component.set('v.main',false);
                            component.set('v.rad',false);
                            component.set('v.upload',true);
                            helper.setDocumentList(component, event);
                        }
                        else if(bankvalue == 'Financial' && storeResponse.includes('URL Retrieved Successfully')) {
                            component.set('v.main',false);
                            component.set('v.rad',false);
                            component.set('v.upload',true);
                            helper.setDocumentList(component, event);        
                        }
                        else{
                           var newURL = actionResult.getReturnValue();
                            console.log('Else Success --'+newURL);
                            var n = newURL.search("http");
                            if(n !='-1'){
                                window.open(newURL, '_blank');   
                                //location.href = newURL;
                                /*
                        var urlEvent = $A.get("e.force:navigateToURL");
                        
                        urlEvent.setParams({
                                "url": newURL
                            });
                        urlEvent.fire();
                          */  
                            }
                            else{
                                console.log(newURL);
                                component.set('v.newURL',newURL);
                            } 
                        }   
                    }
                   
                }
            });
            $A.enqueueAction(action);
        if(bankvalue == 'ScannedPDFBanking' || bankvalue == 'EstatementBanking'){
            component.set('v.main',false);
            component.set('v.rad',false);
            var recordinitial = component.get('v.recordId').substring(0, 3);
            if(recordinitial != "a0I"){
                component.set('v.bankselect1',false);}
            else{
                component.set('v.bankselect1',false);
            }
            
        }else if(bankvalue == 'FinancialUpload'){
            component.set('v.main',false);
            component.set('v.rad',false);
            component.set('v.upload',true);
        }      
        //$A.get('e.force:refreshView').fire();
    },    
    onSubmitbank : function(component, event, helper) {
        
        console.log('hello');
        var fromdate = component.get('v.fromdate');
        var todate = component.get('v.todate');
        var bankvalue = component.get('v.selected');
        //var firstName = component.get("v.selectedLookUpRecord").Id;
        var firstName = "a0G0w000000LPS1EAO";
        
        
        console.log(firstName);
        console.log(bankvalue);
        if(fromdate != '' && fromdate !=null  && todate != '' && todate != null && bankvalue != '' && bankvalue != null ){
            
            var action = component.get("c.callPerfios1");
            action.setParams({
                "strRecordId": component.get('v.recordId'),
                "strLoanConId" : component.get('v.LoanConId'),
                "strIntType": bankvalue,
                "strFromDt" : fromdate,
                "strTodt" : todate,
                "bankname" : firstName
            });
            
            action.setCallback(this, function(actionResult) {
                var state = actionResult.getState();
                if (state === "SUCCESS") {
                    if(bankvalue != 'ScannedPDFBanking' ){
                        
                        var newURL = actionResult.getReturnValue();
                        var n = newURL.search("http");
                        if(n !='-1'){
                            window.open(newURL, '_blank');   
                            //location.href = newURL;
                            /*
                    var urlEvent = $A.get("e.force:navigateToURL");
                    
                    urlEvent.setParams({
                            "url": newURL
                        });
                    urlEvent.fire();
                      */  
                        }
                        else{
                            console.log(newURL);
                            component.set('v.newURL',newURL);
                        }
                    }
                    else{
                        component.set('v.main',false);
                        component.set('v.rad',false);
                        component.set('v.bankselect',false);
                        component.set('v.bankselect1',false);
                        component.set('v.upload',true);
                    }
                }
            });
            $A.enqueueAction(action);
        }
        
        
        
        //$A.get('e.force:refreshView').fire();
    },    
    handleFilesChange: function(component, event, helper) {
        var fileName = 'No File Selected..';
        if (event.getSource().get("v.files").length > 0) {
            fileName = event.getSource().get("v.files")[0]['name'];
        }
        component.set("v.fileName", fileName);
    },
    doSave: function(component, event, helper) {
        if (component.find("fileId").get("v.files").length > 0) {
            helper.uploadHelper(component, event);
        } else {
            alert('Please Select a Valid File');
        }
    },
    cancel : function(component, event, helper){
        // helper.getGSTList(component);
        console.log('In Cancel method');
        var action = component.get("c.createCIRecords");
        action.setParams({
            "strLoanConId": component.get('v.LoanConId'),
            "strRecordId": component.get('v.recordId')
        });
        
        action.setCallback(this, function(actionResult) {    
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                var storeResponse = actionResult.getReturnValue();
                 var message = JSON.stringify(storeResponse);
                if(message.includes('Error')) {
                    console.log('Error occurred');
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "Error",
                        "title": "Error!",
                        "message": message
                    });
                    toastEvent.fire();
                } else {
                    console.log('Success!');
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire();  
                    $A.get("e.force:closeQuickAction").fire();
                }    
            }
        });
        $A.enqueueAction(action);
    },
    oncomplete : function(component, event, helper) {
        component.set('v.showProceedButton',false);
        component.set('v.showSubmitButton',true);
        helper.showSpinner(component, event);
        var items = [{
            "label": "Banking",
            "name": "1",
            "disabled": false,
            "expanded": true,
            "items": [{
                "label": "Scanned Statement Upload",
                "name": "4",
                "disabled": false,
                "expanded": false,
                "items": []
            },{
                "label": "E Statement Upload",
                "name": "2",
                "disabled": false,
                "expanded": false,
                "items": []
            }]
        }];
        
        component.set('v.items', items);
        //var main = component.get('v.MainRadioValue');
        
        //if(main != '' && main != null){
        
        var action = component.get("c.setVisibilityOfTree");
        action.setParams({
            "strRecordId": component.get('v.recordId'),
            "loanConId" : component.get('v.LoanConId')
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            var storeResponse = actionResult.getReturnValue();
   
            if (state === "SUCCESS") {
                if(storeResponse != 'Not applicable') {
                    var array = storeResponse.split("-");        
                    component.set('v.showBankingTree',array[0]);
                    component.set('v.showITRTree',array[1]);
                    component.set('v.showFinancicalTree',array[2]); 
                    if(array[0] ==='false' && array[1]==='false' && array[2] ==='false') {
                         component.set('v.Message','Perfios Analysis is not availiable for this option');
                    } else {
                        component.set('v.Message',''); 
                    }
                } else {
                    component.set('v.showBankingTree',false);
                    component.set('v.showITRTree',false);
                    component.set('v.showFinancicalTree',false);
                    component.set('v.Message','Perfios Analysis is not availiable for this option');
                } 
                helper.hideSpinner(component, event);
            }
        });
        component.set('v.rad',true);
        component.set('v.main',false); 
        
        $A.enqueueAction(action);

        
        //}        
        /*
        if(bankvalue == 'ScannedPDFBanking'){
        	component.set('v.rad',false);
        }
        */
    },
    
    onbacktransacFinancial : function(component, event, helper) {
        component.set('v.showProceedButton', true);
        component.set('v.showSubmitButton',false);
        component.set('v.rad',true);
        component.set('v.main',false);   
        component.set('v.upload',false); 
        component.set('v.errorMessage1','');
            if(component.get('v.selected') == 'Financial') {
                 var opts =   
            [
                { "class": "optionClass", label: "2019-20", value: "2019-20", selected: "true"},
                { "class": "optionClass", label: "2018-19", value: "2018-19" },
                { "class": "optionClass", label: "2017-18", value: "2017-18" },
                { "class": "optionClass", label: "2016-17", value: "2016-17" },
                { "class": "optionClass", label: "2015-16", value: "2015-16" },
                { "class": "optionClass", label: "2014-15", value: "2014-15" }
            ];
                
            component.find("InputSelectDynamic1").set("v.options", opts);
           
                component.set("v.financialYear", '2019');    
                
                component.set("v.finYear", '2019-20'); //Added by Vaishali for BREII
            } else {
                component.find("Scheme").set("v.value",component.get('v.perfiosBankInformationId'));
            }
    },
    
    onstoptransac : function(component, event, helper) {
        
        helper.showSpinner(component, event);
        var fromdate = '';
        var todate = '';
        
        var action = component.get("c.callScannedPDFComplete");
        action.setParams({
            "strLoanConId": component.get('v.LoanConId'),
            "strRecordId": component.get('v.recordId'),
            "bankValue" : component.get('v.selected')
            
        });
        
        action.setCallback(this, function(actionResult) {    
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                
                var newURL = actionResult.getReturnValue();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type": "Success",
                    "title": "Success!",
                    "message": "The transaction has been successfully completed."
                });
                toastEvent.fire();
                //$A.get("e.force:closeQuickAction").fire() ;      
            }
            helper.hideSpinner(component, event);         
        });
        $A.enqueueAction(action);
        component.set('v.showProceedButton', false);
        component.set('v.showSubmitButton',true);
        
        component.set('v.rad',true);
        component.set('v.main',false); 
        component.set('v.upload',false); 
        component.set('v.errorMessage1','');
            if(component.get('v.selected') == 'Financial') {
                 var opts =   
            [
                { "class": "optionClass", label: "2019-20", value: "2019-20", selected: "true"},
                { "class": "optionClass", label: "2018-19", value: "2018-19" },
                { "class": "optionClass", label: "2017-18", value: "2017-18" },
                { "class": "optionClass", label: "2016-17", value: "2016-17" },
                { "class": "optionClass", label: "2015-16", value: "2015-16" },
                { "class": "optionClass", label: "2014-15", value: "2014-15" }
            ];
                
            component.find("InputSelectDynamic1").set("v.options", opts);
           
                component.set("v.financialYear", '2019');    
                component.set("v.finYear", '2019-20'); //Added by Vaishali for BREII
            } else {
                component.find("Scheme").set("v.value",component.get('v.perfiosBankInformationId'));
            }
            
    },
    
    
    onCanceltransac : function(component, event, helper) {
        
        helper.showSpinner(component, event);
        var fromdate = '';
        var todate = '';
        
        var action = component.get("c.callScannedPDFComplete");
        action.setParams({
            "strLoanConId": component.get('v.LoanConId'),
            "strRecordId": component.get('v.recordId'),
            "bankValue" : component.get('v.selected')
            
        });
        
        action.setCallback(this, function(actionResult) {    
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                
                var newURL = actionResult.getReturnValue();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type": "Success",
                    "title": "Success!",
                    "message": "The transaction has been successfully completed."
                });
                toastEvent.fire();
                //$A.get("e.force:closeQuickAction").fire() ;      
            }
            helper.hideSpinner(component, event);         
        });
        $A.enqueueAction(action);
        component.set('v.showProceedButton', false);
        component.set('v.showSubmitButton',true);
        
        component.set('v.rad',true);
        component.set('v.main',false); 
        component.set('v.upload',false); 
        component.set('v.errorMessage1','');
            if(component.get('v.selected') == 'Financial') {
                 var opts =   
            [
                { "class": "optionClass", label: "2019-20", value: "2019-20", selected: "true"},
                { "class": "optionClass", label: "2018-19", value: "2018-19" },
                { "class": "optionClass", label: "2017-18", value: "2017-18" },
                { "class": "optionClass", label: "2016-17", value: "2016-17" },
                { "class": "optionClass", label: "2015-16", value: "2015-16" },
                { "class": "optionClass", label: "2014-15", value: "2014-15" }
            ];
                
            component.find("InputSelectDynamic1").set("v.options", opts);
           
                component.set("v.financialYear", '2019');   
                component.set("v.finYear", '2019-20'); //Added by Vaishali for BREII
            } else {
                component.find("Scheme").set("v.value",component.get('v.perfiosBankInformationId'));
            }
            $A.get("e.force:closeQuickAction").fire();
    },
    
    cancelLink : function(component, event, helper) {
        console.log('In CancelDirect method');
        $A.get("e.force:closeQuickAction").fire();
    },
    
    sendlink : function(component, event, helper) {
        
        //component.set('v.main',false);
        
        
        /*
        var action = component.get("c.sendEmail");
        
        action.setParams({
            "url" : component.get('v.recordId')
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                console.log(result.getReturnValue());
                console.log('Success');
                //component.set("v.ApplicantList",result.getReturnValue());
                //console.log('list'+ component.get('v.ApplicantList'));
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "THANK YOU AND url has been sent to customer."
                });
                toastEvent.fire();
                $A.get("e.force:closeQuickAction").fire() ;
            }
            else
            {
                console.log('failure');
            }
        });
        $A.enqueueAction(action);
       */ 
        
    },
    handleSelectAllApplicants: function(component, event, helper) {
        var getID = component.get("v.ApplicantList");
        var checkvalue = component.find("selectAll").get("v.value");        
        var checkContact = component.find("checkContact"); 
        if(checkvalue == true){
            for(var i=0; i<checkContact.length; i++){
                checkContact[i].set("v.value",true);
            }
        }
        else{ 
            for(var i=0; i<checkContact.length; i++){
                checkContact[i].set("v.value",false);
            }
        }
    },
    handleSelectedAllApplicants: function(component, event, helper) {
        helper.showSpinner(component, event);
        var selectedContacts = [];
        var checkvalue = component.find("checkContact");
        
        if(!Array.isArray(checkvalue)){
            if (checkvalue.get("v.value") == true) {
                //selectedContacts.push(checkvalue.get("v.text"));
                console.log('tommy1');
                console.log(checkvalue.get("v.text"));
                var person = checkvalue.get("v.text");
                component.set('v.personId',person);
                console.log('perosnid'+ component.get('v.personId'));
                //helper.mailSent(component, event);
                component.set('v.mailsent',true);
                component.set('v.main',false);
            }
            else{
                //checkvalue.set("v.value",true);
                checkvalue.set("disabled", "true");
            }
        }else{
            for (var i = 0; i < checkvalue.length; i++) {
                if (checkvalue[i].get("v.value") == true) {
                    //selectedContacts.push(checkvalue[i].get("v.text"));
                    console.log('tommy2');
                    console.log(checkvalue[i].get("v.text"));
                    var person = checkvalue[i].get("v.text");
                    component.set('v.personId',person);
                    console.log('perosnid'+ component.get('v.personId'));
                    //helper.mailSent(component, event);
                    component.set('v.main',false);
                    component.set('v.mailsent',true);
                }
                else{
                    //checkvalue[i].set("v.value",true);'
                    //checkvalue[i].set("disabled", "true");
                }
            }
        }
        
        var items = [{
            "label": "Banking",
            "name": "1",
            "disabled": false,
            "expanded": false,
            "items": [{
                "label": "Net Banking",
                "name": "2",
                "disabled": false,
                "expanded": false,
                "items": []
            }]
        }];
        component.set('v.items',items);
        var action = component.get("c.setVisibilityOfTree");
        action.setParams({
            "strRecordId": component.get('v.recordId'),
            "loanConId" : component.get('v.LoanConId')
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            var storeResponse = actionResult.getReturnValue();
            if (state === "SUCCESS") {
                
                if(storeResponse != 'Not applicable') {
                    var array = storeResponse.split("-");        
                    component.set('v.showBankingTree',array[0]);
                    component.set('v.showITRTree',array[1]);
                    component.set('v.showFinancicalTree',array[2]); 
                    if(array[0] ==='false' && array[1]==='false' && array[2] ==='false') {
                         component.set('v.Message','Perfios Analysis is not availiable for this option');
                    } else {
                        component.set('v.Message',''); 
                    }
                } else {
                    component.set('v.showBankingTree',false);
                    component.set('v.showITRTree',false);
                    component.set('v.showFinancicalTree',false);
                    component.set('v.Message','Perfios Analysis is not availiable for this option');
                } 
            }
            helper.hideSpinner(component, event);
        });
        $A.enqueueAction(action);
        //console.log('selectedContacts-' + selectedContacts);
        
    },
    backto: function(component, event, helper) {
        
        component.set('v.main',true);
        component.set('v.rad',false);
        //var loanContactid = event.getSource().get("v.text");
        // component.set('v.LoanConId', component.get('v.LoanConId'));
        console.log('BACKTO-LoanConId: '+component.get('v.LoanConId'));
        component.set('v.errorMessage1','');
        component.set('v.Message','');
        component.set('v.showNumberOfAssessmentYears',false);
        component.set('v.showAssessmentDate',false);
        component.set('v.ShowDate',false);
        component.set('v.ShowBank',false);
    },
    back: function(component, event, helper) {
        
        component.set('v.main',true);
        component.set('v.mailsent',false);
        //var loanContactid = event.getSource().get("v.text");
        // component.set('v.LoanConId', component.get('v.LoanConId'));
        component.set('v.errorMessage1','');
        component.set('v.Message','');
        component.set('v.showNumberOfAssessmentYears',false);
        component.set('v.showAssessmentDate',false);
        component.set('v.ShowDate',false);
        component.set('v.ShowBank',false);
    },
    handleSelect: function (component, event) {
        //component.set('v.SelectItem' , true);
        //var name = event.getParam('name');
        
        event.preventDefault();
        var options = component.get('v.rad');
        if(options == true){
            var mapping = { '1' : 'Banking', '2' : 'EstatementBanking', '3' : 'NativePDFBanking',
                           '4' : 'ScannedPDFBanking'};
            var mappingName = { '1' : 'Banking', '2' : 'E Statement Upload', '3' : 'NativePDFBanking',
                           '4' : 'Scanned Statement Upload'};
           component.set('v.selected', mapping[event.getParam('name')]);
           component.set('v.showSubmitButton',true);
           var naming = mappingName[event.getParam('name')];
           component.set('v.ItemSelected', naming);
                              
            //var shiv = component.set('v.selected', mapping[event.getParam('name')]);
            console.log(component.get('v.selected'));
            //if(mapping[event.getParam('name')] ==='ScannedPDFBanking')  {
                component.set('v.ShowBank',true);
                component.find("Scheme").set("v.value",component.get('v.perfiosBankInformationId'));
            //} else {
              //  component.set('v.ShowBank',false);
        //    }
        }
        else
        {
            var mapping = { '1' : 'Banking', '2' : 'Banking', '3' : 'Banking',
                           '4' : 'Banking'};
            component.set('v.selected', mapping[event.getParam('name')]);
            component.set('v.ItemSelected',mapping[event.getParam('name')]);
            //var shiv = component.set('v.selected', mapping[event.getParam('name')]);
            console.log(component.get('v.selected'));
            //if(mapping[event.getParam('name')] ==='ScannedPDFBanking')  {
                component.set('v.ShowBank',true);
                component.find("Scheme").set("v.value",component.get('v.perfiosBankInformationId'));
            //} else {
            //    component.set('v.ShowBank',false);
        //    }
        }
        component.set('v.showAssessmentDate',false);
        component.set('v.showNumberOfAssessmentYears',false);
        component.set('v.errorMessage1','');
        component.set('v.ShowDate',true);
    },
    handleSelect1: function (component, event) {
        event.preventDefault();
        var options = component.get('v.rad');
        if(options == true){
            var mapping = { '1' : 'ITR','2' : 'ITR'};
            //component.set('v.selected1', mapping[event.getParam('name')]);
            component.set('v.selected', 'ITR');
            component.set('v.ItemSelected', 'ITR');
            console.log('if' + component.get('v.selected1'));
        }
        else{
            var mapping = { '1' : 'ITR','2' : 'ITR'};
            //component.set('v.selected1', mapping[event.getParam('name')]);
            component.set('v.selected', 'ITR');
            component.set('v.ItemSelected', 'ITR');
            console.log('else' + component.get('v.selected1'));
        }
        component.set('v.ShowDate',false);
        component.set('v.ShowBank',false);
        component.set('v.showAssessmentDate',false);
        component.set('v.errorMessage1','');
        component.set('v.showNumberOfAssessmentYears',true);
        var opts = 
            [
                { "class": "optionClass", label: "1", value: "1", selected: "true"},
                { "class": "optionClass", label: "2", value: "2" },
                { "class": "optionClass", label: "3", value: "3" }
            ];
                
        component.find("InputSelectDynamic").set("v.options", opts);
    },
    
    handleSelect2: function (component, event) {
        event.preventDefault();
        var mapping = { '1' : 'Both'};
        component.set('v.selected2', mapping[event.getParam('name')]);
    },
    handleSelect3: function (component, event) {
        event.preventDefault();
        var options = component.get('v.rad');
        if(options == true){
            var mapping = { '1' : 'FinancialAnalysis', '2' : 'Financial', '3' : 'FinancialUpload'};
            component.set('v.selected', mapping[event.getParam('name')]);
            component.set('v.ItemSelected', mapping[event.getParam('name')]);
            console.log(component.get('v.selected'));
            if(mapping[event.getParam('name')] ==='FinancialUpload')  {
                component.set('v.ShowDate',true);
                component.set('v.ShowBank',true);
                component.set('v.showAssessmentDate',false);
                component.set('v.showNumberOfAssessmentYears',false);
            } else {
                component.set('v.ShowDate',false);
                component.set('v.ShowBank',false);
                component.set('v.showAssessmentDate',true);
                component.set('v.showNumberOfAssessmentYears',false);
                var opts = 
            [
                { "class": "optionClass", label: "2019-20", value: "2019-20", selected: "true"},
                { "class": "optionClass", label: "2018-19", value: "2018-19" },
                { "class": "optionClass", label: "2017-18", value: "2017-18" },
                { "class": "optionClass", label: "2016-17", value: "2016-17" },
                { "class": "optionClass", label: "2015-16", value: "2015-16" },
                { "class": "optionClass", label: "2014-15", value: "2014-15" }
            ];
                
                component.find("InputSelectDynamic1").set("v.options", opts);
                component.set("v.financialYear", '2019');
                component.set("v.finYear", '2019-20'); //Added by Vaishali for BREII
            }
            component.set('v.errorMessage1','');
        }
        else
        {
            
        } 
    },     
    sendlinkall: function (component, event,helper) {
        console.log('wddwdwd');
        component.set('v.errorMessage1','');
        if(component.get('v.perfiosBankInformationId')==='') {
            console.log('Hi');
            component.set('v.bankId','');
        }
        helper.mailSent(component, event);
    },
    selectedlookup: function (component, event,helper) {
        console.log('wddwdwd');
        var firstName = component.find("myAtt").get("v.value");
        console.log(firstName);
        
    },
    setLoanConid: function (component, event,helper) {
        
        var loanContact = event.getSource().get("v.text");
        component.set('v.loanContact',loanContact);
        component.set('v.LoanConId', loanContact.Id);
        component.set('v.loanConMobile',loanContact.Mobile);
        component.set('v.loanConEmail',loanContact.Email);
        console.log('LoanCon: '+loanContact);
        console.log('loanName'+loanContact.Name);
        console.log('loanMobile'+loanContact.Mobile);
        console.log('loanEmail'+loanContact.Email);
    },
    getMobile : function (component, event,helper) {
        var loanContact = component.get('v.loanContact');
        component.set('v.loanConMobile',loanContact.Mobile);
        console.log('LoanCon: '+loanContact);
        console.log('loanMobile'+loanContact.Mobile);
    },
    getEmail : function (component, event,helper) {
        var loanContact = component.get('v.loanContact');
        component.set('v.loanConEmail',loanContact.Email);
        console.log('LoanCon: '+loanContact);
        console.log('loanEmail' +loanContact.Email);
    },
    doSendThisDocument: function(component, event, helper) {
        helper.showSpinner(component, event);
        var loanCon= component.get('v.LoanConId');
        var fromdate = component.get('v.fromdate');
        var todate = component.get('v.todate');
        var recordId= component.get('v.recordId');
        var docWrapper = component.get('v.docCheckListWrapperList');
        var bankvalue = component.get('v.selected');
        //alert(bankvalue);
        var action = component.get("c.sendDocument"); 
        action.setParams({
            "lstOfDocumentWrapper": docWrapper,
            "strRecordId": recordId,
            "strLoanConId" : loanCon,
            "strFromDt" : fromdate,   
            "strTodt" : todate,
            "bankvalue" : bankvalue,
            "financialYear": component.get('v.financialYear')
        }); 
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            console.log('inside apex call result --'+state);
            if (state === "SUCCESS") {
                var storeResponse = actionResult.getReturnValue();
                
                var message=JSON.stringify(storeResponse);
                if(storeResponse == null) {
                    var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Error!",  
                                "type": "Error",
                                "message": "Some error occurred, Please contact system admin"
                            });
                    toastEvent.fire();
                } else {    
                    if((storeResponse.message).includes("Error")) {
                        alert(storeResponse.message);      
                       // var toastEvent = $A.get("e.force:showToast");
                         //       toastEvent.setParams({
                           //         "title": "Error!",  
                             //       "type": "Error",
                               //     "message": storeResponse.message
                                //});
                        //toastEvent.fire();
                        component.set('v.docCheckListWrapperList',storeResponse.lstOfDocWrapper);
                    } else {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "type": "Success",
                            "message": storeResponse.message
                        });
                        toastEvent.fire();
                        component.set('v.docCheckListWrapperList',storeResponse.lstOfDocWrapper);
                        //helper.fetchCurrentDocList(component, event);
                    }
                }
            } 
            else{
                if(storeResponse == null) {
                    var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Error!",  
                                "type": "Error",
                                "message": "Some error occurred, Please contact system admin"
                            });
                    toastEvent.fire();
                } else {
                    var error = actionResult.getReturnValue();
                    var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "Error!",
                                    "type": "Error",
                                    "message": error.message
                                });
                    toastEvent.fire();
                }
            }
            helper.hideSpinner(component, event);
        });
        $A.enqueueAction(action);

    },
    openmodalDoc: function(component, event, helper) {
        var contentDocId= event.getSource().get("v.name");
        var contentDoc = new Array(contentDocId);
        $A.get('e.lightning:openFiles').fire({
                    //recordIds: ['0690w0000005qsQAAQ'],
                    recordIds: contentDoc,
        });
    },
    getSelectedBankName: function(component, event, helper) {
        var perfiosBankInformationId = component.find("Scheme").get("v.value");
        component.set('v.perfiosBankInformationId',perfiosBankInformationId);
        helper.showSpinner(component, event);
        var action = component.get("c.getBankID"); 
        action.setParams({
            "perfiosBankInformationId": perfiosBankInformationId
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                var storeResponse = actionResult.getReturnValue();
                if(storeResponse.includes("Error")) {
                    component.set('v.bankMessage',storeResponse);    
                } else {
                    component.set('v.bankId',storeResponse);
                }
            } else{
                component.set('v.bankMessage',storeResponse);
            }   
        });
        $A.enqueueAction(action);
        console.log('v.perfiosBankInformationId'+ component.get('v.perfiosBankInformationId'));
        console.log('v.bankId' + component.get('v.bankId'));
        helper.hideSpinner(component, event);
    },
    setTree: function(component, event, helper) {
        var items = component.get('v.items');
        var items1 = component.get('v.items1');
        var items3 = component.get('v.items3');
        if(items[0]['expanded'] === true) {
            items1[0]['expanded'] = false;
            items3[0]['expanded'] = false;
        }
        component.set('v.items',items);
        component.set('v.items1',items1);
        component.set('v.items3',items3);
    },
    setTree1: function(component, event, helper) {
        var items = component.get('v.items');
        var items1 = component.get('v.items1');
        var items3 = component.get('v.items3');
        if(items1[0]['expanded'] === true) {
            items[0]['expanded'] = false;
            items3[0]['expanded'] = false;
        }
        component.set('v.items',items);
        component.set('v.items1',items1);
        component.set('v.items3',items3);
    },
    setTree3: function(component, event, helper) {
        var items = component.get('v.items');
        var items1 = component.get('v.items1');
        var items3 = component.get('v.items3');
        if(items3[0]['expanded'] === true) {
            items1[0]['expanded'] = false;
            items[0]['expanded'] = false;
        }
        component.set('v.items',items);
        component.set('v.items1',items1);
        component.set('v.items3',items3);
    },
    onChange: function(component, event) {
        var dynamicCmp = component.find("InputSelectDynamic");
        component.set("v.selectedNumberOfAssesmentvalue", dynamicCmp.get("v.value"));
    },
    onFinancialYearChange: function(component, event) {
        var dynamicCmp = component.find("InputSelectDynamic1");
        var a=dynamicCmp.get("v.value");
        var financialYear=a.substring(0, 4);
        component.set("v.financialYear", financialYear);
        component.set("v.finYear",a);
    }

})