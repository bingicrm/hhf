({
    doInit : function(component, event, helper) {
        var action = component.get("c.checkPanDetails");
        
        action.setParams({ loanContactId : component.get("v.recordId") });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state==>'+state);
            
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                console.log('res==>'+res);
                if(response.getReturnValue().includes('PAN has been verified for this customer! Are you sure you want to verify again?')){
                    component.set("v.message",res);
                    component.set("v.showYesNo",true);
                }
                else if(response.getReturnValue().includes('PAN has been already hit for verification. Please try after some time.')){
                    component.set("v.message",res);
                    component.set("v.showYesNo",false);
                }
		//Below else if block added by Abhilekh on 11th October 2019 for FCU BRD
                else if(response.getReturnValue().includes('You cannot perform this action as the Loan Application is FD Decline.')){
                	component.set("v.message",res);
                    component.set("v.showYesNo",false);
                }
		//Below else if block added by Abhilekh on 24th January 2020 for Hunter BRD
                else if(response.getReturnValue().includes('You cannot perform this action as Hunter analysis has not been recommended.')){
                	component.set("v.message",res);
                    component.set("v.showYesNo",false);
                }
                else if(response.getReturnValue().includes('OK')){
                    component.set("v.showPANValidationCmp",true);
                    component.set("v.showMsg",false);
                    component.set("v.showYesNo",false);
                }
            }  
        });
        $A.enqueueAction(action);
    },
    
    validatePANagain : function(component, event, helper) {
        component.set("v.showPANValidationCmp",true);
        component.set("v.showMsg",false);
        component.set("v.showYesNo",false);
    },
    
    cancelCreation : function(component, event, helper) {
        //Added By Vaishali Mehta for BRE
         var calledFromLAComponent = component.get('v.fromLAComponent');
         console.log('fromLAComponent: Yes method '+calledFromLAComponent);
         if(calledFromLAComponent === true) {
            console.log('laval'+ component.get('v.laVal'));
            console.log('Hi PAN Success');
            var wrap = [];
            wrap = component.get('v.CDWrap');
            var index = component.get('v.index');
            console.log('wrap'+wrap);
            console.log('index'+index);
            wrap[index].PANVerified = true;
            console.log('wrap'+wrap);
            component.set('v.CDWrap',wrap);
        
            var evt = $A.get("e.c:callCreateLAEvent");
            evt.setParams(
                { 
                    "laVal" : component.get('v.laVal'),
                    "CDWrap" : component.get('v.CDWrap'),
                    "lstBorrowerType" : component.get('v.lstBorrowerType'),
                    "lstconstitution" : component.get('v.lstconstitution'),
                    "lstCustSegment" : component.get('v.lstCustSegment'),
                    "loanAppID" : component.get('v.loanAppID'),
                    "prod" : component.get('v.prod'),
                    "localPol" : component.get('v.localPol'),
                    "customer" : component.get('v.customer'),
                    "isDisplay" : component.get('v.isDisplay') ,
                    "isEditable" : component.get('v.isEditable'),
                    "isInserted" : component.get('v.isInserted'),
                    "isDisable" : component.get('v.isDisable'),
                    "addAddress" : component.get('v.addAddress'),
                    "CDfetched" : component.get('v.CDfetched'),
                    "data" : component.get('v.data'),
                    "columns" : component.get('v.columns'),
                    "borrowerTy" : component.get('v.borrowerTy'),
                    "LaNum" : component.get('v.LaNum'),
                    "isEditableLA" : component.get('v.isEditableLA'),
                    "QDEMode" : component.get('v.QDEMode'),
                    "calledFromVFPage" : component.get('v.calledFromVFPage'),
                    "errorMessage" : component.get('v.errorMessage'),
                    "showErrorMessage" :component.get('v.showErrorMessage'),
                    "LoanConId" : component.get('v.LoanConId'),
                    "idTypes" : component.get('v.idTypes'),
                    "oldWrap" : component.get('v.oldWrap'),
                    "oldWrapString" : component.get('v.oldWrapString'),
                    "disableAddButton" : component.get('v.disableAddButton'),
                    "disableEditButton" : component.get('v.disableEditButton')
                }
            );
            evt.fire();  
         }else {
             //End : Addition by Vaishali Mehta for BRE
             $A.get("e.force:closeQuickAction").fire();
             $A.get('e.force:refreshView').fire(); 
         } 
    }
})