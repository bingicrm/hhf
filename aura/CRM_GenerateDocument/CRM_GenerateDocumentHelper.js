({
    handleSubmit : function(cmp, event, helper) {
        var action = cmp.get('c.generateDocument');
        var msValue = cmp.get("v.msValue");
        var attachName;
        var attachmentOptionSelected;
        var GICfsValue = cmp.get("v.GICfsValue");
        var caseFromDate = new Date(cmp.get("v.caseFromDate"));
        var caseToDate = new Date(cmp.get("v.caseToDate"));
        var optionSelected = cmp.get("v.msValue");
        var loanStatus = cmp.get("v.SimpleRecord").LD_Loan_Status__c;
        var email = cmp.get("v.SimpleRecord").Email__c;
        
        console.log('loanStatus-->'+loanStatus);
        console.log('caseFromDate-->'+caseFromDate);
        console.log('caseToDate-->'+caseToDate);
        
        console.log('caseFromDate-->'+new Date(cmp.get("v.caseFromDate")));
        console.log('caseToDate-->'+new Date(cmp.get("v.caseToDate")));
        
        if(cmp.get("v.GRSfsValue")=='Download pdf to Case' || cmp.get("v.GicAttachmentOptionsValue")=='Download pdf to Case'){
            attachmentOptionSelected = 'save';
        }
        else if(cmp.get("v.GRSfsValue")=='Download pdf to Case and Send email to customer' || cmp.get("v.GicAttachmentOptionsValue")=='Download pdf to Case and Send email to customer'){
            attachmentOptionSelected = 'saveAndEmail';
        }
        
        if(msValue=='Generate Repayment Schedule'){
            optionSelected = 'Repayment';
            attachName = 'RPS_'+cmp.get("v.SimpleRecord").LMS_Application_ID__c+'_';
            action.setParams(
                {
                    "pdfDetailsMap": {
                        "caseId": cmp.get("v.recordId"),
                        "attachName": attachName,
                        "attachmentOptionSelected": attachmentOptionSelected,
                        "email": email
                    },
                    "optionSelected": optionSelected
                }
            );
        }
        else if(msValue=='Generate IT Ceritficate'){
            optionSelected = 'ITC';
            attachName = 'IT-{'+GICfsValue+'}-'+cmp.get("v.SimpleRecord").LMS_Application_ID__c+'_';
            action.setParams(
                {
                    "pdfDetailsMap": {
                        "caseId": cmp.get("v.recordId"),
                        "attachName": attachName,
                        "attachmentOptionSelected": attachmentOptionSelected,
                        "SubType": cmp.get("v.GICfsValue"),
                        "loanStatus": loanStatus,
                        "caseFromDate": new Date(cmp.get("v.caseFromDate")),
                        "caseToDate": new Date(cmp.get("v.caseToDate")),
                        "email": email
                    },
                    "optionSelected": optionSelected
                }
            );
        }
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                $A.get('e.force:refreshView').fire();
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
            }
            else if (state === "INCOMPLETE") {
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                            
                            let toastParams = {
                                title: "Error",
                                type: "error"
                            };
                            
                            if (errors && Array.isArray(errors) && errors.length > 0) {
                                toastParams.message = errors[0].message;
                            }
                            
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            var dismissActionPanel = $A.get("e.force:closeQuickAction");
                            
                            toastEvent.fire();
                            dismissActionPanel.fire();
                            $A.get('e.force:refreshView').fire();
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    setFiscalYearDates : function(cmp, event, helper) {
        var action = cmp.get('c.setFiscalYearDates');
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.dateMap",response.getReturnValue());
                var fromDate = $A.localizationService.formatDate(new Date(), cmp.get("v.dateMap").FromDate);
                var toDate = $A.localizationService.formatDate(new Date(), cmp.get("v.dateMap").toDate);
                cmp.set("v.caseFromDate",fromDate);
                cmp.set("v.caseToDate",toDate);
                cmp.set("v.fiscalYearEndDate",toDate);
                
                console.log('fromDate--> '+cmp.get("v.caseFromDate"));
                console.log('toDate--> '+cmp.get("v.caseToDate"));
                console.log('fiscalYearEndDate--> '+cmp.get("v.fiscalYearEndDate"));
            }
            else if (state === "INCOMPLETE") {
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            let toastParams = {
                                title: "Error",
                                message: "There is some technical error, please try again later.",
                                type: "error"
                            };
                            
                            if (errors && Array.isArray(errors) && errors.length > 0) {
                                toastParams.message = errors[0].message;
                            }
                            
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            toastEvent.fire();
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
})