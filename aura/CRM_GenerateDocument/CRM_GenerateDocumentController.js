({
    onFirstRadio: function(component, event, helper) {
        $A.util.removeClass(component.find("GRSGroups"), "slds-hide");
        $A.util.addClass(component.find("GICGroups"), "slds-hide");
        $A.util.addClass(component.find("dates"), "slds-hide");
        $A.util.addClass(component.find("GicAttachmentOptionsId"), "slds-hide");
        component.set("v.GICfsValue","");
        component.set("v.GicAttachmentOptionsValue","");
        component.set("v.nonValidation","false");
        component.set("v.noLMSAppId","false");
        component.set("v.sValidation","false");
        component.set("v.noEmail","false");
		component.set("v.invalidOptError","false");
        component.set("v.invalidDate","false");
    },
    
    onFirstRadioOptionsFucus: function(component, event, helper) {
        component.set("v.nonValidation","false");
        component.set("v.noLMSAppId","false");
        component.set("v.sValidation","false");
        component.set("v.noEmail","false");
		component.set("v.invalidOptError","false");
        component.set("v.GICfsValue","");
    },
    
    onSecondRadio: function(component, event, helper) {
        $A.util.addClass(component.find("GRSGroups"), "slds-hide");
        $A.util.removeClass(component.find("GICGroups"), "slds-hide");
        component.set("v.GRSfsValue","");
        component.set("v.nonValidation","false");
        component.set("v.noLMSAppId","false");
        component.set("v.noEmail","false");
		component.set("v.invalidOptError","false");
        component.set("v.sValidation","false");
    },
    
    onSecondRadioOptionsFucus: function(component, event, helper) {
        component.set("v.nonValidation","false");
        component.set("v.noLMSAppId","false");
        component.set("v.sValidation","false");
        component.set("v.noEmail","false");
		component.set("v.invalidOptError","false");
        component.set("v.GRSfsValue","");
        $A.util.removeClass(component.find("dates"), "slds-hide");
        $A.util.removeClass(component.find("GicAttachmentOptionsId"), "slds-hide");
    },
    
    doInit : function(component, event, helper) {
        $A.util.addClass(component.find("GRSGroups"), "slds-hide");
        $A.util.addClass(component.find("GICGroups"), "slds-hide");
        $A.util.addClass(component.find("GICGroups"), "slds-hide");
        $A.util.addClass(component.find("dates"), "slds-hide");
        $A.util.addClass(component.find("GicAttachmentOptionsId"), "slds-hide");
        console.log('<----------------doInit called!------------------->');
        helper.setFiscalYearDates(component, event, helper);
    },
    
    handleSubmit : function(component, event, helper) {
        var msValue = component.get("v.msValue");
        var GRSfsValue = component.get("v.GRSfsValue");
        var GICfsValue = component.get("v.GICfsValue");
        var GicAttachmentOptionsValue = component.get("v.GicAttachmentOptionsValue");
        var loanStatus = component.get("v.SimpleRecord").LD_Loan_Status__c;
        
        var caseFromDate = new Date(component.get("v.caseFromDate"));
        var caseToDate = new Date(component.get("v.caseToDate"));
        var fiscalYearEndDate = new Date(component.get("v.fiscalYearEndDate"));
        
        var days = (caseToDate-caseFromDate)/8.64e7;
        var futureDays = (fiscalYearEndDate-caseToDate)/8.64e7;
        
        console.log('Difference        without trunk--> '+ days);
        console.log('Future difference without trunk--> '+ futureDays);
        console.log('Difference        with    trunk--> '+ Math.trunc(days));
        console.log('Future difference without trunk--> '+ Math.trunc(futureDays));
        
        if(!msValue){
            console.log('<----------------Not selected anything!------------------->');
            component.set("v.sValidation","false");
            component.set("v.noLMSAppId","false");
            component.set("v.noEmail","false");
			component.set("v.invalidOptError","false");
            component.set("v.nonValidation","true");
        }
        else if(!GRSfsValue && !GICfsValue){
            console.log('<----------------Final value Not selected!------------------->');
            component.set("v.nonValidation","false");
            component.set("v.noLMSAppId","false");
            component.set("v.noEmail","false");
			component.set("v.invalidOptError","false");
            component.set("v.sValidation","true");
        }
            else if(msValue=='Generate IT Ceritficate' && (!component.get("v.caseFromDate") || !component.get("v.caseToDate"))){
                console.log('<----------------Generate IT Ceritficate------------------->');
                console.log('<----------------Date not present!------------------->');
                component.set("v.sValidation","false");
                component.set("v.nonValidation","false");
                component.set("v.noEmail","false");
                component.set("v.noLMSAppId","false");
				component.set("v.invalidOptError","false");
                component.set("v.noDate","true");
            }
                else if(msValue=='Generate IT Ceritficate' && (Math.trunc(days)<=0 || Math.trunc(futureDays)<0)){
                    console.log('<----------------Generate IT Ceritficate------------------->');
                    console.log('<----------------Date not present!------------------->');
                    component.set("v.sValidation","false");
                    component.set("v.noDate","false");
                    component.set("v.nonValidation","false");
                    component.set("v.noLMSAppId","false");
                    component.set("v.noEmail","false");
					component.set("v.invalidOptError","false");
                    component.set("v.invalidDate","true");
                }
                    else if(msValue=='Generate IT Ceritficate' && !component.get("v.GicAttachmentOptionsValue")){
                        console.log('<----------------Generate IT Ceritficate------------------->');
                        console.log('<----------------GicAttachmentOptionsValue not present!------------------->');
                        component.set("v.noDate","false");
                        component.set("v.nonValidation","false");
                        component.set("v.noLMSAppId","false");
                        component.set("v.noEmail","false");
						component.set("v.invalidOptError","false");
                        component.set("v.sValidation","true");
                    }
                        else if(loanStatus=='C' && GICfsValue=='Provisional'){
							component.set("v.sValidation","false");
                            component.set("v.noDate","false");
                            component.set("v.nonValidation","false");
                            component.set("v.noLMSAppId","false");
                            component.set("v.noEmail","false");
							component.set("v.invalidOptError","true");
						}
						else if(!component.get("v.SimpleRecord").Email__c && (GRSfsValue=='Download pdf to Case and Send email to customer' || GicAttachmentOptionsValue=='Download pdf to Case and Send email to customer')){
                            console.log('<----------------GicAttachmentOptionsValue not present!------------------->');
                            component.set("v.sValidation","false");
                            component.set("v.noDate","false");
                            component.set("v.nonValidation","false");
                            component.set("v.noLMSAppId","false");
							component.set("v.invalidOptError","false");
                            component.set("v.noEmail","true");
                        }
                            else if(!component.get("v.SimpleRecord").LMS_Application_ID__c){
                                console.log('<----------------LMS_Application_ID not present!------------------->');
								component.set("v.invalidOptError","false");
                                component.set("v.noLMSAppId","true");
                            }     
                                else{
                                    helper.handleSubmit(component, event, helper);
                                    console.log('<--No ERROR-->');
                                }
    },
    
    handleDateChange: function(component, event, helper) {
        if(component.get("v.caseFromDate") && component.get("v.caseToDate")){
            console.log('<----------------Date present!------------------->');
			component.set("v.invalidOptError","false");
            component.set("v.noDate","false");
            component.set("v.invalidDate","false");}
    },
    
    handleClose : function(component, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
        $A.get('e.force:refreshView').fire();
    },
    
    // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
    
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    }
})