({
	doInit : function(component, event, helper) {
		var action = component.get("c.dateSync");
//        action.setParams({"lappId" : component.get("v.recordId")});
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(state === "SUCCESS"){
                component.set("v.LMSDate", response.getReturnValue());
            }else{
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title" : "Error",
                    "message" : "Cannot fetch the LMS Id."
                });
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            }
        });
        $A.enqueueAction(action);
	},
})