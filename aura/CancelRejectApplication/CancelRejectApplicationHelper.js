({
	getPickListValues: function(component,event,helper) {
        var action = component.get("c.getPickListValuesIntoList");
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state is : " + state);            
            if(state === "SUCCESS" && response.getReturnValue() != null){
                console.log("Returned list as : " + JSON.stringify(response.getReturnValue())); 
                component.set("v.lstPickListVal", response.getReturnValue());				
            } 
        });
        $A.enqueueAction(action);
    }
})