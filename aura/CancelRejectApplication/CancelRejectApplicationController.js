({
	doInit : function(component, event, helper) {
		var action = component.get("c.getLoanApp");
        action.setParams({"lappId" : component.get("v.recordId")});
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(state === "SUCCESS"){
                component.set("v.showBlockDef",false);/* added by Saumya for TIL-00000843**/

                console.log('response.getReturnValue().Insurance_Loan_Application__c::'+response.getReturnValue().Insurance_Loan_Application__c);
                console.log('response.getReturnValue().Loan_Application_Number__c::'+response.getReturnValue().Loan_Application_Number__c);
                if(response.getReturnValue().Insurance_Loan_Application__c == true){
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title" : "Error",
                    "message" : "You can not cancel Insurance Loan Application."
                });
                $A.get("e.force:closeQuickAction").fire();                
   
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
                    
                }
                else if(response.getReturnValue().Loan_Application_Number__c != null && response.getReturnValue().Loan_Application_Number__c != '' && ! $A.util.isUndefined(response.getReturnValue().Loan_Application_Number__c) && ! $A.util.isEmpty(response.getReturnValue().Loan_Application_Number__c)){ /* added by Saumya for TIL-00000843**/
                $A.get("e.force:closeQuickAction").fire();                
                console.log('response.getReturnValue().Loan_Application_Number__c::'+response.getReturnValue().Loan_Application_Number__c);
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title" : "Error",
                    "message" : "You can not cancel Disbursed Loan Application."
                });
   
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
                }
                else{
                component.set("v.showBlockDef",true);/* added by Saumya for TIL-00000843**/

                component.set("v.lapp", response.getReturnValue());
                helper.getPickListValues(component,event,helper);
                }
            }else{
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title" : "Error",
                    "message" : "Cannot fetch the Loan Application."
                });
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
                
            }
        });
        $A.enqueueAction(action);
	},
    requestCreation: function(component, event, helper) {        
        component.set("v.showBlock",true);
        component.set("v.showBlockDef",false);
    },
    requestSave: function(component, event, helper) {
        var action = component.get("c.cancelApplication");
		var loan = component.get("v.lapp");
        console.log(JSON.stringify(loan));
        action.setParams({
            "la" : loan            
        });
        
        action.setCallback(this, function(actionResult) {
            
            var state = actionResult.getState();
            console.log('actionResult.getReturnValue()::'+actionResult.getReturnValue());
            if(state === "SUCCESS"){
                
                var resultsToast = $A.get("e.force:showToast");
                
                if(actionResult.getReturnValue() == null){
                	resultsToast.setParams({
                    	"title" : "Cancelled",
                    	"message" : "Loan Application has been cancelled."
                	});
                    $A.get("e.force:closeQuickAction").fire();                
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
            	}
                else if(actionResult.getReturnValue() == 'Submit For Approval'){// Added by Saumya For Reject Revive BRD
                  component.set('v.submitforApprovalcheck',true);
                  component.set('v.showBlock',false);       
                    console.log('I am here');
                }
            	else{
                	resultsToast.setParams({
                    	"title" : "Error",
                    	"message" : actionResult.getReturnValue(),
                         "type" : "error"//Added by Chitransh for TIL-840 [27-09-2019]//
                	});
                    $A.get("e.force:closeQuickAction").fire();                
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
            	}
 
				
                
            }
        });
        $A.enqueueAction(action);
    },
    callApproval: function(component, event, helper) {// Added by Saumya For Reject Revive BRD 
        var action = component.get("c.callApprovalProcess");
		var loan = component.get("v.lapp");
        console.log(JSON.stringify(loan));
        action.setParams({
            "la" : loan            
        });
        
        action.setCallback(this, function(actionResult) {
            
            var state = actionResult.getState();
            if(state === "SUCCESS"){
                
                var resultsToast = $A.get("e.force:showToast");
                
                if(actionResult.getReturnValue() == null || actionResult.getReturnValue() == ''){
                	resultsToast.setParams({
                    	"title" : "Cancelled",
                    	"message" : "Loan Application has been successfully sent for approval."
                	});
            	}
            	else{
                	resultsToast.setParams({
                    	"title" : "Error",
                    	"message" : actionResult.getReturnValue(),
                        "type" : "error"
                	});
            	}
 
				$A.get("e.force:closeQuickAction").fire();                
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
                
            }
        });
        $A.enqueueAction(action);
    },
    cancelCreation: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    
})