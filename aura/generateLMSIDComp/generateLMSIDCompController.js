({
    doInit : function(component, event, helper) {
        var loanId = component.get("v.recordId");
        var actionLMS = component.get("c.LMSCheck");
        actionLMS.setParams({
            "loanId" : loanId
        });
        actionLMS.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var checkResp = response.getReturnValue();
                if(checkResp == "updated"){
                    var checkAction = component.get("c.checkId");
                    checkAction.setParams({
                        "loanId" : loanId
                    });
                    checkAction.setCallback(this, function(response){
                        var state = response.getState();
                        if (state === "SUCCESS") {
                            var Checkesp = response.getReturnValue();
                            if(Checkesp=='ID not found'){                   
                                var action = component.get("c.generateId");
                                action.setParams({
                                    "loanId" : loanId
                                });
                                action.setCallback(this, function(response){
                                    var state = response.getState();
                                    if (state === "SUCCESS") {
                                        var resp = response.getReturnValue();
                                        if(resp == "Success"){
                                            component.find('notifLib').showNotice({
                                                "variant": "info",
                                                "header": "LMS ID Generated",
                                                "message": "The request for LMS ID has been initiated. Please refresh the page after a few minutes.",
                                                closeCallback: function() {
                                                }
                                            });
                                            component.set("v.response","Please click on Close button to exit.");
                                        }
                                        else{
                                            component.find('notifLib').showNotice({
                                                "variant": "error",
                                                "header": "Error Occured!",
                                                "message": "LMS ID for the Loan Application has not been generated.",
                                                closeCallback: function() {
                                                }
                                            });
                                            component.set("v.response","Please click on Close button to exit.");
                                        }
                                    }
                                    else if(state === "ERROR"){
                                        component.find('notifLib').showNotice({
                                            "variant": "error",
                                            "header": "Error Occured!",
                                            "message": "Internal Error. Please contact Admin",
                                            closeCallback: function() {
                                            }
                                        });
                                        component.set("v.response","Internal Error. Please click on Close button to exit.");
                                    }
                                });
                                $A.enqueueAction(action);                   
                            }
                            if(Checkesp!='ID not found'){
                                component.find('notifLib').showNotice({
                                    "variant": "error",
                                    "header": "Error Occured!",
                                    "message": "LMS ID already exists.",
                                    closeCallback: function() {
                                    }
                                });
                                component.set("v.response","Please click on Close button to exit.");
                                //$A.get("e.force:closeQuickAction").fire();
                                //$A.get('e.force:refreshView').fire();
                            }
                            //$A.get('e.force:refreshView').fire();
                            
                        }
                        else if(state === "ERROR"){
                            component.find('notifLib').showNotice({
                                "variant": "error",
                                "header": "Error Occured!",
                                "message": "Internal Error. Please contact Admin",
                                closeCallback: function() {
                                }
                            });
                            component.set("v.response","Internal Error. Please click on Close button to exit.");
                        }
                    });
                    $A.enqueueAction(checkAction); 
                }
                else if(checkResp == "checked" ){
                    component.find('notifLib').showNotice({
                        "variant": "error",
                        "header": "Error Occured!",
                        "message": "Request Initiated. Please try again after 15 minutes.",
                        closeCallback: function() {
                        }
                    });
                    component.set("v.response","Please click on Close button to exit.");
                }
            }
            else{
                component.find('notifLib').showNotice({
                    "variant": "error",
                    "header": "Error Occured!",
                    "message": "Internal Error. Please contact Admin",
                    closeCallback: function() {
                    }
                });
                component.set("v.response","Internal Error. Please click on Close button to exit.");
            }
        });
        $A.enqueueAction(actionLMS);
    },
    
    handleClick : function(component){
        $A.get("e.force:closeQuickAction").fire();
        window.location.reload(true);        
    }
})