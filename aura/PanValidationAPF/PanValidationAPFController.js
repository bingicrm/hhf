({
    doInit: function(component, event, helper) {
       var objectName = component.get("c.returnObjectName");
       objectName.setParams({strRecordId:component.get("v.recordId")});
       objectName.setCallback(this, function(objectNameResponse) {
       		var state = objectNameResponse.getState();
        	console.log('state '+state);
        	var resultsToast = $A.get("e.force:showToast");
        	if (state === "SUCCESS") {
				console.log('RESULT '+objectNameResponse.getReturnValue());    
                if(objectNameResponse.getReturnValue() != null){
                    if(objectNameResponse.getReturnValue() == 'Builder') {
                        component.set("v.showBuilderBlock",true);
                        var action1 = component.get("c.getCustomerDetails");
                        action1.setParams({ loanContactId : component.get("v.recordId") });
            
                        action1.setCallback(this, function(response) {
                            var state1 = response.getState();
                            if (state1 === "SUCCESS") {
                                //showBlock = True;
                                console.log('INIT MEthod');
                                component.set("v.showBlock",true);
                                if(response.getReturnValue() != null){
                                    component.set("v.custDetails",response.getReturnValue());
                                    console.log('CUSTDETAILS' +JSON.stringify(component.get("v.custDetails")));
                                }
                                
                            }
                             //$A.get('e.force:refreshView').fire();  //Commented this line by Vaishali for BRE
                        });
                        $A.enqueueAction(action1);
                    }
                    else if(objectNameResponse.getReturnValue() == 'Promoter') {
                        component.set("v.showPromoterBlock",true);
                    }
                }
            }
       });
       $A.enqueueAction(objectName);
    
       console.log('RecordId' +component.get("v.recordId"));
       var action = component.get("c.validationPanDetails");
       action.setParams({ strRecordId : component.get("v.recordId") });
       
        action.setCallback(this, function(response) {
                
                var state = response.getState();
                console.log('state '+state);
                var resultsToast = $A.get("e.force:showToast");
                if (state === "SUCCESS") {
                    //showBlock = True;
                    component.set("v.showBlock",true);
                    console.log('==show=='+component.get("v.showBlock"));
                   	console.log('RESULT '+response.getReturnValue());
                   	if(response.getReturnValue() != null){
                   		component.set("v.panDetails",response.getReturnValue());
                        var panStatus = component.get('v.panStatuses');
                        console.log('Pan Status '+panStatus);
                        console.log('pan status value '+panStatus[response.getReturnValue().PAN_Status__c]);
                        
                        component.set("v.panStatus",panStatus[response.getReturnValue().PAN_Status__c]);
                   	}
                    
                    else if(response.getReturnValue() == null){
                         
                    }
                }
                
                else if (state === "INCOMPLETE") {
                    
                }
                else if (state === "ERROR") {
                   
                }
            });
            $A.enqueueAction(action);
    },
    close :function(component,event,helper){
        $A.get("e.force:closeQuickAction").fire();
        $A.get('e.force:refreshView').fire(); 
    },
    
    doneAction:function(component, event, helper){
        var action3 = component.get("c.copyPanDetails");
        action3.setParams({"idLoanContact" : component.get("v.recordId") });
        action3.setCallback(this, function(response) {
            var state3 = response.getState();
            console.log('state3'+state3);
            if (state3 === "SUCCESS") {
                //showBlock = True;
                console.log('response.getReturnValue()'+response.getReturnValue());
                component.set("v.showBlock",true);
                if(response.getReturnValue() != null){
                    component.set("v.boolSuccess",response.getReturnValue());
                }
                
            }
            $A.get('e.force:refreshView').fire(); 
        });
        $A.enqueueAction(action3);
        $A.get("e.force:closeQuickAction").fire();
        $A.get('e.force:refreshView').fire(); 
    },
    
    doneNoAction:function(component, event, helper){
        $A.get("e.force:closeQuickAction").fire();
        $A.get('e.force:refreshView').fire(); 
    }
})