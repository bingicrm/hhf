({ 
        doInit: function(component, event, helper) 
    	{    
                var action = component.get("c.sendLMSRepaymentRequest");
                action.setParams({ disburseId : component.get("v.recordId") });   
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                       console.log(response.getReturnValue());
                       component.set("v.message",response.getReturnValue());  
                    }
                    else if (state === "INCOMPLETE") {
                         
                    }
                    else if (state === "ERROR") {
                       
                    }
                });
                $A.enqueueAction(action);       
        } 
})