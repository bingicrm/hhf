({
    getPickListValues: function(component,event,helper) {
        var action = component.get("c.getPickListValuesIntoList");
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state is : " + state);            
            if(state === "SUCCESS" && response.getReturnValue() != null){
                console.log("Returned list as : " + JSON.stringify(response.getReturnValue())); 
                component.set("v.lstPickListVal", response.getReturnValue());   
                //component.find("pickListReason").set("v.options",response.getReturnValue());      
            } 
        });
        $A.enqueueAction(action);
    },
    
    getPickListValues2: function(component,event,helper) {
        var action = component.get("c.getPickListValuesIntoList2");
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state is : " + state);            
            if(state === "SUCCESS" && response.getReturnValue() != null){
                console.log("Returned list as : " + JSON.stringify(response.getReturnValue())); 
                component.set("v.lstPickListVal2", response.getReturnValue());  
                //component.find("pickListReason2").set("v.options",response.getReturnValue());             
            } 
        });
        $A.enqueueAction(action);
    },
    
    getpicklistAllvalues : function(component,event,helper)
    {
        this.getPickListValues(component,event,helper);
        this.getPickListValues2(component,event,helper);
    },
    
    saveApproveandCredit : function(component,event,helper){
        var action = component.get("c.setApprovalStatus");
            action.setParams({
                "recordId" : component.get('v.recordId'),
                "approvalStatus" : component.get('v.selectedValue') ,
                "comments" : component.get('v.comments') 
            });
            action.setCallback(this, function(response) 
            {
                var state = response.getState();
                var resResult =  response.getReturnValue();
                if (state === "SUCCESS") {
                    if(resResult.success) {
                        var showToast = $A.get("e.force:showToast"); 
                        showToast.setParams({ 
                            'type': 'SUCCESS',
                            'message' : resResult.errorMessage
                        }); 
                        showToast.fire();
                        $A.get('e.force:refreshView').fire();
                    }
                    else {
                        alert(resResult.errorMessage);  
                    }
                }
               //$A.get('e.force:refreshView').fire();
            });
            $A.enqueueAction(action);  
    },
    
    saveRejectResponse: function(component,event,helper) {
        console.log('11---->>');
        var action = component.get("c.rejectApplication");
        action.setParams({
            "recordID" : component.get('v.recordId'),
            "reason" : component.get('v.Rejection_Reason__c'),
            "remark" : component.get('v.Remarks_Rejection__c'),
            "level" : component.get('v.Severity_Level__c')
        });
        $A.enqueueAction(action);  
        $A.get('e.force:refreshView').fire();
    }
})