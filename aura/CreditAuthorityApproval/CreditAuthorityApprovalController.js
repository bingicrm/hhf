({
    doInit : function(component, event, helper) {
       //Code in this doInit added by Abhilekh on 18th September 2019 for FCU BRD
    	var action = component.get("c.getLAStatus");
    	
        action.setParams({
            "laId" : component.get('v.recordId'),
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var resResult =  response.getReturnValue();
            console.log('resResult==>'+JSON.stringify(resResult));
            if (state === "SUCCESS") {
                if(resResult.FCU_Decline_Count__c > 0 && resResult.Overall_FCU_Status__c == 'Negative'){
                    component.set("v.showWarning",true);
                }
            }
        });
        $A.enqueueAction(action);
    },

    callApproval : function(component, event, helper) {
        var error = false;
        var sel = component.get('v.selectedValue');
        var comment = component.get('v.comments');
        var remark = component.get('v.Rejection_Reason__c');
        var reason = component.get('v.Remarks_Rejection__c');
        var sev = component.get('v.Severity_Level__c');
        var showBlockDef = component.get("v.showBlockDef");
        if(showBlockDef == true)
        {
            if((sel == 'Refer back to Credit' || sel == 'Approve') && (comment == null || comment.trim() == ''))
            {
                alert('Please fill in the mandatory fields.');
                
            }
            else
            {
                helper.saveApproveandCredit(component,event,helper);
            }
        }
        else
        {
            if(sel == 'Reject' && (reason == 'None' || sev == 'None' || remark == 'undefined' || remark==null))
            {
                alert('Please fill in the mandatory fields.');
            }
            else{
                console.log('Start-->>');
                helper.saveRejectResponse(component,event,helper);
            }
        }
        
    },
    
    showComment : function(component,event,helper)
    {
        var pickValue = component.get("v.selectedValue");
        if(pickValue == 'Approve' || pickValue=='Refer back to Credit')
        {
            component.set("v.showBlockDef",true);
            component.set("v.showBlock",false);
            if(component.get("v.showBlock") == false)
            {
                component.set("v.lstPickListVal",null);
                component.set("v.lstPickListVal2",null);
            }
        }
        else
        {
            component.set("v.showBlock",true);
            component.set("v.showBlockDef",false);
            if(component.get("v.showBlock") == true)
            {
                helper.getpicklistAllvalues(component,event,helper);
            }
            
            
        }
    }
})