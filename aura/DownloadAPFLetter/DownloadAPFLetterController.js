({
    doInit : function(component, event, helper) {
        var projectId = component.get("v.recordId");
        
        var action = component.get('c.downloadAPFLetter');
        action.setParams({  
            "projectId": component.get("v.recordId")
        });
        
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") 
            {
                
                console.log(actionResult.getReturnValue());
                var returnValue = actionResult.getReturnValue();                
                if(returnValue == 'Success')
                {
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                        'type': 'SUCCESS',
                        'message' : 'APF Letter has been saved.'
                    }); 
                    showToast.fire();
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                }
                else if(returnValue == 'Invalid Id'){
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                        'type': 'ERROR',
                        'message' : 'APF Letter cannot be downloaded at this sub stage.'
                    }); 
                    showToast.fire();
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                }
                else{
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                        'type': 'ERROR',
                        'message' : 'An error has occurred.'
                    }); 
                    showToast.fire();
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                }
            }
        });
        $A.enqueueAction(action);
    }
})