({
    // Fetch the accounts from the Apex controller
    getTPVList: function(component) {
        var action = component.get('c.getTPV');
        // Set up the callback
        /*var self = this;
        action.setCallback(this, function(actionResult) {
            component.set('v.tpvList', actionResult.getReturnValue());
            
        });*/
        action.setCallback(this, function(actionResult){
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                var records =actionResult.getReturnValue();
                console.log('Response'+actionResult.getReturnValue());
                records.forEach(function(record){
                    console.log('Inside foreach');
                    //Below if and else if block added by Abhilekh on 19th September 2019 for FCU BRD
                    if(record.Owner__r.Profile.Name == 'FCU Manager'){
                        record.linkName = '/'+record.Id;
                    }
                    else if(record.Owner__r.Profile.Name == 'System Administrator'){
                        record.linkName = '/'+record.Id;
                    }
                    else if(record.Owner__r.Profile.Name == 'Third Party Vendor'){
                        console.log('Third Party Vendor');
                    	record.linkName = '/hhfltpv/s/detail/'+record.Id;   
                    }
                    console.log('record after linkName'+record);
                    record.laName = record.Project__r.Name; //Added by Abhilekh on 19th September 2019 for FCU BRD
                    
                    //Below if and else if block added by Abhilekh on 19th September 2019 for FCU BRD
                    if(record.Owner__r.Profile.Name == 'FCU Manager'){
                        record.lalink = '/'+record.Project__c;
                    }
                    else if(record.Owner__r.Profile.Name == 'System Administrator'){
                        record.lalink = '/'+record.Project__c;
                    }
                    else if(record.Owner__r.Profile.Name == 'Third Party Vendor'){
                    	record.lalink = '/hhfltpv/s/detail/'+record.Project__c;   
                    }
                    console.log('record after laLink'+record);
                    //record.laOverallFCUReportStatus = record.Loan_Application__r.Overall_FCU_Status__c; //Added by Abhilekh on 19th September 2019 for FCU BRD
                    //record.laReAppealedCase = record.Loan_Application__r.Re_Appealed_Case__c ? 'Yes' : 'No'; //Added by Abhilekh on 19th September 2019 for FCU BRD    
                });
                console.log('Records'+records);
                component.set("v.tpvList", records);
            }
        });
        $A.enqueueAction(action);
    },
    
    //Below method added by Abhilekh on 30th October 2019 for FCU BRD
    sortData : function(component,fieldName,sortDirection){
        var data = component.get("v.tpvList");
        //function to return the value stored in the field
        var key = function(a) { return a[fieldName]; }
        var reverse = sortDirection == 'asc' ? 1: -1;
        
        // to handel number/currency type fields 
        /*if(fieldName == 'linkName'){ 
            data.sort(this.sortBy('Name', reverse))
        }*/
        //else{// to handel text type fields 
            data.sort(function(a,b){
                var a = key(a) ? key(a).toLowerCase() : '';//To handle null values , uppercase records during sorting
                var b = key(b) ? key(b).toLowerCase() : '';
                return reverse * ((a>b) - (b>a));
            });    
        //}
        //set sorted data to tpvList attribute
        component.set("v.tpvList",data);
    }
})