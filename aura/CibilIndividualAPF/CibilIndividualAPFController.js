({
	doInit : function(component, event, helper) {
		component.set('v.hitCIBILAgain',false);
        helper.callCIBIL(component);
	},
    callCIBILagain: function(component, event, helper)  
    {      
        component.set('v.hitCIBILAgain',true);
        helper.callCIBIL(component);
    },
    cancelCreation : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})