({
    toggle1 : function(component, event, helper) {
        var toggleText = component.find("PLENTRY");
        $A.util.toggleClass(toggleText, "toggle");
    },
    toggle2 : function(component, event, helper) {
        var toggleText = component.find("BALANCE");
        $A.util.toggleClass(toggleText, "toggle");
    },
    toggle3 : function(component, event, helper) {
        var toggleText = component.find("RATIOS");
        $A.util.toggleClass(toggleText, "toggle");
    },
    
    // common reusable function for toggle sections
    toggleSection : function(component, event, helper) {
    	/*    
        // dynamically get aura:id name from 'data-auraId' attribute
        var sectionAuraId = event.target.getAttribute("data-auraId");
        // get section Div element using aura:id
        var sectionDiv = component.find(sectionAuraId).getElement();
        // The search() method searches for 'slds-is-open' class, and returns the position of the match.
        // This method returns -1 if no match is found.
        
        var sectionState = sectionDiv.getAttribute('class').search('slds-is-open'); 
        
        // -1 if 'slds-is-open' class is missing...then set 'slds-is-open' class else set slds-is-close class to element
        if(sectionState == -1){
            sectionDiv.setAttribute('class' , 'slds-section slds-is-open');
        }else{
            sectionDiv.setAttribute('class' , 'slds-section slds-is-close');
        }
        */
        
    },
	doInit : function(component, event, helper) {
        helper.doInitisedit(component, event, helper);
        helper.doInitcustdetail(component, event, helper);
	    helper.doInit(component, event, helper, true, true, true);
        
	},
    save : function(component, event, helper) {  
        var ischecked = component.find("isChecked").get("v.value");
        var checkit = component.get("v.Financials");
        var custid = component.get("v.Customerid");
		var action = component.get("c.SaveFinancials");
        var Financiallist = component.get("v.Financials");
        var loanAppId = component.get("v.loanAppId");  
        var InterestPaid1 = Financiallist[0].Interest_paid__c;
        console.log('InterestPaid1 '+InterestPaid1);
        var InterestPaid2 = Financiallist[1].Interest_paid__c;
        console.log('InterestPaid2 '+InterestPaid2);
        var InterestPaid3 = Financiallist[2].Interest_paid__c;
        console.log('InterestPaid3 '+InterestPaid3);
        //Paramteres of action InterestPaid1, InterestPaid2,InterestPaid3 Added by Vaishali for BREIIEnchancement
        if (ischecked === false || ischecked == '') {//|| isChecked    
            action.setParams({
                "Financials":JSON.stringify(Financiallist),
                        'conId':custid,
                        'loanAppId' : loanAppId,
                        'InterestPaid1' : InterestPaid1,  
                        'InterestPaid2' : InterestPaid2,
                        'InterestPaid3' : InterestPaid3
            });
            action.setCallback(this, function(response){
                
                var state = response.getState();
                if(state === "SUCCESS"){
                    //Added if condition, else block by Vaishali for BREIIEnchancement
                    if(response.getReturnValue() === "Success") {
                        component.set("v.iseditable",false); 
                        console.log("response.getReturnValue()"+response.getReturnValue());
                        component.set("v.Financials",response.getReturnValue());
                        if(InterestPaid1 == '' && InterestPaid2 == '' && InterestPaid3 == '') {
                            helper.doInit(component, event, helper,true, true, true);
                        }else if (InterestPaid1 == '' && InterestPaid2 == '' && InterestPaid3 != '') {
                            helper.doInit(component, event, helper,true, true, false);
                        }else if (InterestPaid1 == '' && InterestPaid2 != '' && InterestPaid3 == '') {
                            helper.doInit(component, event, helper,true, false, true);
                        }else if (InterestPaid1 != '' && InterestPaid2 == '' && InterestPaid3 == '') {
                            helper.doInit(component, event, helper,false, true, true);
                        } else {
                            helper.doInit(component, event, helper,false, false, false);
                        }
                    } else {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "",
                            "type": "Error",
                            "message": response.getReturnValue()
                        });
                        toastEvent.fire();
                        component.set("v.message", response.getReturnValue());
                    }
                //End of the patch- Added by Vaishali for BREIIEnchancement
                }else if (response.getState() === "ERROR"){
                    var errors = action.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            component.set("v.message", errors[0].message);
                        }
                    }
                }
            });
            $A.enqueueAction(action);
        }else if(ischecked){
            var allValid = component.find('fieldId').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        	}, true);
            if(allValid){
               action.setParams({
                "Financials":Financiallist,
                        'conId':custid,
                        'loanAppId' : loanAppId,
                        'InterestPaid1' : InterestPaid1,  
                        'InterestPaid2' : InterestPaid2,
                        'InterestPaid3' : InterestPaid3
            
               });
                action.setCallback(this, function(response){
                    
                    var state = response.getState();
                    if(state === "SUCCESS"){
                        //Added if condition, else block by Vaishali for BREIIEnchancement
                        if(response.getReturnValue() === "Success") {
                            component.set("v.iseditable",false);
                            console.log("response.getReturnValue()"+response.getReturnValue());
                            component.set("v.Financials",response.getReturnValue());
                            if(InterestPaid1 == '' && InterestPaid2 == '' && InterestPaid3 == '') {
                                helper.doInit(component, event, helper,true, true, true);
                            }else if (InterestPaid1 == '' && InterestPaid2 == '' && InterestPaid3 != '') {
                                helper.doInit(component, event, helper,true, true, false);
                            }else if (InterestPaid1 == '' && InterestPaid2 != '' && InterestPaid3 == '') {
                                helper.doInit(component, event, helper,true, false, true);
                            }else if (InterestPaid1 != '' && InterestPaid2 == '' && InterestPaid3 == '') {
                                helper.doInit(component, event, helper,false, true, true);
                            } else {
                                helper.doInit(component, event, helper,false, false, false);
                            }
                        }
                    else {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "",
                            "type": "Error",
                            "message": response.getReturnValue()
                        });
                        toastEvent.fire();
                        component.set("v.message", response.getReturnValue());
                    }
                //End of the patch- Added by Vaishali for BREIIEnchancement    
                    }else if (response.getState() === "ERROR"){
                        var errors = action.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                component.set("v.message", errors[0].message);
                            }
                        }
                    }
                });
                $A.enqueueAction(action); 
                }
        }
        else {
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": "Please complete missing information."
                });
                toastEvent.fire();
        //alert('Please update the invalid form entries and try again.');
        }
        console.log("cover helper");
	},
    edit : function(component, event, helper) {
		component.set("v.iseditable",true);
	},
	handleConfirmDialogbox : function(component, event, helper) {
        component.set('v.showConfirmDialogbox', true);
    },
    handleConfirmDialogboxYes : function(component, event, helper) {
        helper.closeMe(component, event, helper);
    },
    handleConfirmDialogboxNo : function(component, event, helper) {
        component.set('v.showConfirmDialogbox', false);
    },
    checkperfiosdata : function(component, event, helper) {
        helper.Checkdatafromperfios(component, event, helper);
    },
    handleConfirmDialog : function(component, event, helper) {
        component.set('v.showConfirmDialog', true);
    },
    handleConfirmDialogYes : function(component, event, helper) {
        helper.getdatafromperfios(component, event, helper);
    },
    handleConfirmDialogNo : function(component, event, helper) {
        component.set('v.showConfirmDialog', false);
    },
       // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
     
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    }
})