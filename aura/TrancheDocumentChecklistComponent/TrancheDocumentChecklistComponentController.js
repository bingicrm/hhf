({
    initRecords: function(component, event, helper) {
        //call the apex class method and fetch the loan Id for the record.
        var action = component.get("c.getLoanId");
        action.setParams({
            'recordId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state : " + JSON.stringify(response.getState()));
            if(state === "SUCCESS"){
                var loanId = response.getReturnValue();
                //Set loanId with the return value of response.
                component.set("v.loanId", loanId);                
            }
        });
        $A.enqueueAction(action);
        
        // call the apex class method and fetch sub stage of loan application record        	
        var action = component.get("c.fetchSubStage");
        action.setParams({
            'recordId': component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();            
            if (state === "SUCCESS") {
                var capturedResponse = response.getReturnValue();                
                // set SubStage attribute with return value from server.
                component.set("v.subStage", capturedResponse);
                if(capturedResponse != 'Tranche File Check' && capturedResponse != 'Tranche Scan Checker') {
                    component.set("v.showforOtherStages", true);
                }
            }
        });
        $A.enqueueAction(action);
        
        // call the apex class method and fetch Document list  
        var action = component.get("c.fetchDocuments");        
        action.setParams({            
            'tranId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();            
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();                
                component.set("v.DocList1", storeResponse);
            }
        });
        $A.enqueueAction(action);
    },
    
    Save : function(component, event, helper) {        
        var actionValidation = component.get("c.validateChkList");
        
        actionValidation.setParams({
            "paramJSONList": JSON.stringify(component.get("v.DocList1"))
        });
        actionValidation.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS" && component.isValid()) {
                var resultMsg = response.getReturnValue();                
                if(resultMsg == "Successful"){
                    var action = component.get("c.saveLoan");                    
                    action.setParams({
                        "paramJSONList": JSON.stringify(component.get("v.DocList1"))
                    });
                    action.setCallback(this, function(response) {                        
                        if (response.getState() === "SUCCESS" && component.isValid()) {
                            component.set("v.DocList1", response.getReturnValue());
                            component.set("v.showSaveCancelBtn",false);
                        }
                        window.location.reload(true);
                    });
                    $A.enqueueAction(action);
                }
                else {                    
                    component.set("v.showSaveCancelBtn",false);
                    component.set("v.showError",true);
                    var cmpTarget = component.find('ModalboxError');
                    var cmpBack = component.find('ModalbackdropError');
                    $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                    $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
                    
                }
            }
        });
        $A.enqueueAction(actionValidation);
    },    
    
    cancel : function(component,event,helper){
        component.set("v.showSaveCancelBtn",false);
        $A.get('e.force:refreshView').fire(); 
    },
    
    createDocRecord : function(component, event, helper){	
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Document_Checklist__c",
            "defaultFieldValues":{
                "Loan_Applications__c" : component.get("v.loanId"),
                "Tranche__c" : component.get("v.recordId")
            },
            "panelOnDestroyCallback": function(event) {
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": component.get("v.recordId"),
                    "slideDevName": "related"
                });
                navEvt.fire();
            }
        });
        createRecordEvent.fire();
    },
    
    closeModalDoc:function(component,event,helper){   
        //alert('Close');
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    
    closeModalDocError:function(component,event,helper){
        component.set("v.showError",false);
        var cmpTarget = component.find('ModalboxError');
        var cmpBack = component.find('ModalbackdropError');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    }
})