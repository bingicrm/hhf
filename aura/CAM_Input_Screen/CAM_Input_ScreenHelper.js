({
    getLoanAppDataSet : function(component, event, helper){
        var action = component.get("c.getLoanAppDetails");
        var recordId = component.get("v.testLoanAppId");
        console.log('recordId '+recordId);
        action.setParams({
            'recId':recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                console.log('in success'+response.getReturnValue());
                console.log('in succ'+response.getReturnValue().loanobject)
                component.set("v.LoanAppRec", response.getReturnValue().loanobject);                
                component.set("v.proptype", response.getReturnValue().propertytype);                
                component.set("v.technicalval1", response.getReturnValue().technicalinput1);                
                component.set("v.technicalval2", response.getReturnValue().technicalinput2);
			console.log('hey Loan_Purpose'+response.getReturnValue().Loan_Purpose);                
            component.set("v.Loan_Purpose", response.getReturnValue().Loan_Purpose);                
            component.set("v.Transaction_type", response.getReturnValue().Transaction_type);                
            
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getCDdataSet : function(component, event, helper){
        var action = component.get("c.getCustDetails");
        var recordId = component.get("v.testLoanAppId");
        console.log('recordId '+recordId);
        action.setParams({
            'loanAppId':recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                console.log('ash test'+response.getReturnValue()[0].Id);
                
                component.set("v.custDetailsRec", response.getReturnValue());  
                var ri = component.get("v.recordIdNumber");
                if(ri != null && ri != ''){
                    console.log('ashish in first if::'+ ri);
                    //var custlist = component.get("v.custDetailsRec");
                    console.log('ashish in custlist length::'+ response.getReturnValue().length);
                    
                    //for(var act in response.getReturnValue()){
                    for (let i = 0; i < response.getReturnValue().length; i++){
            
                    console.log('ashish in for ri::'+ ri+'  act.id::'+response.getReturnValue()[i].Id);
                        if(response.getReturnValue()[i].Id === ri){
                            console.log('ri === act.id');
                            component.set("v.btnCheckRec", response.getReturnValue()[i]);    
                        }
                        
                    }
                }        
                
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    doInitisedit : function(component, event, helper) {
        
		var recordId = component.get("v.testLoanAppId");
        
        var action = component.get("c.GetIseditable");
        action.setParams({
            'recId':recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                component.set("v.iseditable", response.getReturnValue());    
                //console.log('in success getting editable::'+response.getReturnValue());
                
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    doInitiseditNw : function(component, event, helper) {
        
		var recordId = component.get("v.testLoanAppId");
        
        var action = component.get("c.GetIseditableNw");
        action.setParams({
            'recId':recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                component.set("v.iseditablenew", response.getReturnValue());    
                //console.log('in success getting editable::'+response.getReturnValue());
                
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    handleloanappstatus : function(component, event, helper) {
        
		var recordId = component.get("v.LoanAppRec");
        
        var action = component.get("c.saveloanstatus");
        action.setParams({
            'recId':recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                //console.log("response.getReturnValue()"+response.getReturnValue()[0].Loan_Type__c);
                
                //component.set("v.iseditable", response.getReturnValue());    
                console.log('in success getting editable::');
                helper.getLoanAppDataSet(component, event, helper);
                //component.set("v.showEdit", response.getReturnValue());                
                
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
        
    }
})