({
    doInit : function(component, event, helper){
        helper.getLoanAppDataSet(component, event, helper);
        helper.getCDdataSet(component, event, helper);
        helper.doInitisedit(component, event, helper);
        helper.doInitiseditNw(component, event, helper);
        var va = component.get("v.btnCheckRec");
        console.log(va);
        //console.log('ashish here'+ri);
    },
    
    getCD : function(component, event, helper){
        component.set("v.isOpen",true);
    },
    
    setLoanConid : function(component, event, helper){
        var loanContactid = event.getSource().get("v.text");
        console.log('LoanConId: '+loanContactid);
        component.set('v.LoanConId', loanContactid);
        var act = component.get("v.custDetailsRec");
        console.log(act);
        var str = act[loanContactid];
        component.set("v.btnCheckRec", str);
        console.log(str);
    },
    
    handleFinancials : function(component, event, helper){
        var recordIdNumber = component.get("v.btnCheckRec.Id");
        var loanAppId = component.get("v.testLoanAppId");
        var evt = $A.get("e.c:callFinancialDetailsEvent");
        evt.setParams({
            "recordIdNumber" : recordIdNumber,
            "loanAppId" : loanAppId
        })
        evt.fire();
    },
    handleLIP : function(component, event, helper){
        var recordIdNumber = component.get("v.btnCheckRec.Id");
        var loanAppId = component.get("v.testLoanAppId");
        var evt = $A.get("e.c:callLIPInputDetailsEvent");
        evt.setParams({
            "recordIdNumber" : recordIdNumber,
            "loanAppId" : loanAppId
        })
        evt.fire();
    },
    handleBanking : function(component, event, helper){
        console.log('BANKING ');
        var recordIdNumber = component.get("v.btnCheckRec.Id");
        var loanAppId = component.get("v.testLoanAppId");
        var evt = $A.get("e.c:callBankingInputDetailsEvent");
        evt.setParams({
            "recordIdNumber" : recordIdNumber,
            "loanAppId" : loanAppId
        })
        evt.fire();
    },
    handleAIP : function(component, event, helper){
        var recordIdNumber = component.get("v.btnCheckRec.Id");
        var loanAppId = component.get("v.testLoanAppId");
        console.log('calling AIP screen');
        console.log('recordIdNumber '+recordIdNumber);
        console.log('loanAppId '+loanAppId);
        var evt = $A.get("e.c:callAIPInputDetailsEvent");
        evt.setParams({
            "recordIdNumber" : recordIdNumber,
            "loanAppId" : loanAppId
        })
        evt.fire();
    },
    handleObligations : function(component, event, helper){
        var recordIdNumber = component.get("v.btnCheckRec.Id");
        var loanAppId = component.get("v.testLoanAppId");
        var evt = $A.get("e.c:callObligationsDetailsEvent");
        evt.setParams({
            "recordIdNumber" : recordIdNumber,
            "loanAppId" : loanAppId
        })
        evt.fire();
    },
    handleOtherIncome : function(component, event, helper){
        var recordIdNumber = component.get("v.btnCheckRec.Id");
        var loanAppId = component.get("v.testLoanAppId");
        var evt = $A.get("e.c:callOtherIncomeDetailsEvent");
        evt.setParams({
            "recordIdNumber" : recordIdNumber,
            "loanAppId" : loanAppId
        })
        evt.fire();
    },
    handleSalaried : function(component, event, helper){
        var recordIdNumber = component.get("v.btnCheckRec.Id");
        var loanAppId = component.get("v.testLoanAppId");
        var evt = $A.get("e.c:callSalariedDetailsEvent");
        evt.setParams({
            "recordIdNumber" : recordIdNumber,
            "loanAppId" : loanAppId
        })
        evt.fire();
    },        
    
    close : function(component, event, helper){
		$A.get('e.force:refreshView').fire(); //Added by Abhilekh for BRE2 Enhancements
        $A.get("e.force:closeQuickAction").fire();
    },
       // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
     
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },
    saveloanappstatus : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        //component.set("v.spinner", false);
        console.log('in saveloanstatus');
        helper.handleloanappstatus(component,event,helper);
    }
    
})