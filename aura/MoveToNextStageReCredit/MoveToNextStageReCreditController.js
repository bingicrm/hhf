({
	doInit : function(component, event, helper) 
    {
        var loanApplicationId = component.get("v.recordId");
        
        var action = component.get('c.moveToReCredit');
        action.setParams({  
            "loanApplicationId": component.get("v.recordId")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") 
            {
                
                console.log(actionResult.getReturnValue());
                var returnValue = actionResult.getReturnValue();

                component.set('v.operationAllowed',returnValue.success);
                component.set('v.previousStage',returnValue.prevStage);
                component.set('v.errorMsges',returnValue.errorMsg);
                console.log(component.get('v.errorMsges'));
               
                if(returnValue.success && returnValue.prevStage)
                {
                    component.set('v.showBlock',true);
                }
            }
        });
        $A.enqueueAction(action);
	},
    
    onAccept : function(component, event, helper) {
		var loanApp =  component.get("v.recordId") ;
        
        console.log(loanApp);
       
        var action = component.get('c.movePreviousToReCredit');
        console.log('1');
        action.setParams({
           
            "oppId": loanApp ,
            "comments": component.get("v.comments")
        });
        console.log('2');
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") 
            {
                var result = actionResult.getReturnValue();
                console.log('==result===' + actionResult.getReturnValue());
                if(result.error){
                    var showToast = $A.get("e.force:showToast"); 
					showToast.setParams({ 
					 					'type': 'ERROR',
										'message' : result.errorMsg
										}); 
					showToast.fire(); 	
                    
                }
                else
                {
                    var msg;
                    //console.log('why hwere');
                    var navEvent = $A.get("e.force:navigateToObjectHome");
                    navEvent.setParams({
               					"scope": "Loan_Application__c"
            		});
            		navEvent.fire();
                    
                    msg = 'Your application has been moved to Re Credit';  
            		var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                    	'type': 'Success',
                		'message' : msg
                    }); 
                	showToast.fire();
                }
            } 
            else 
            {
                console.log('==result===' + actionResult.getState());
                var errors = actionResult.getError();
                var errorMess;
                console.log('==error'+errors[0]);
               
               	if (errors[0] && errors[0].message) 
                {
                        errorMess = "Error message: " + errors[0].message;
                    
                } 
                else 
                {
                    errorMess ="Please contact System admin"
                    console.log("Unknown error");
                }
            
                var showToast = $A.get("e.force:showToast"); 
					showToast.setParams({ 
					'title' : errorMess, 
                    'type': 'ERROR',
					'message' : errorMess
					}); 
				showToast.fire(); 		
            }
            $A.get("e.force:closeQuickAction").fire();
            $A.get('e.force:refreshView').fire(); 
        });
        $A.enqueueAction(action);
    
    }
})