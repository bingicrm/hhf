({
    doInit : function(component, event, helper) {
        var recordId = component.get('v.recordId');
        var action = component.get("c.getLoanEligibility");
        action.setParams({
            'recId':recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var check = response.getReturnValue();
                //alert(check);
                if(check === true){
                	//if(recordId != undefined) {
                        console.log('REC '+component.get('v.recordId'));
                        $A.createComponent(
                            "c:CAM_Input_Screen",
                            {
                                "testLoanAppId" :recordId
                            },
                            function(newCmp){
                                if (component.isValid()) {
                                    component.set("v.body", newCmp);
                                }
                            }
                        );
                    //}
                }
                else{
                	component.set("v.isEligible", response.getReturnValue());
                }
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
        /*if(recordId != undefined) {
            console.log('REC '+component.get('v.recordId'));
            $A.createComponent(
                "c:CAM_Input_Screen",
                {
                    "testLoanAppId" :component.get('v.recordId')
                },
                function(newCmp){
                    if (component.isValid()) {
                        component.set("v.body", newCmp);
                    }
                }
            );
        }*/
    },
    
    NavigateToFinancialComponent : function(component, event, helper){
        $A.createComponent(
            "c:Financial_Inputs",
            {
                "Customerid" :event.getParam("recordIdNumber"),
                "loanAppId" : event.getParam("loanAppId")
            },
            function(newCmp){
                if (component.isValid()) {
                    component.set("v.body", newCmp);
                }
            }
        );
    },
    NavigateToLIPScreenComponent : function(component, event, helper){
        $A.createComponent(
            "c:LIP_Inputs",
            {
                "Customerid" :event.getParam("recordIdNumber"),
                "loanAppId" : event.getParam("loanAppId")
            },
            function(newCmp){
                if (component.isValid()) {
                    component.set("v.body", newCmp);
                }
            }
        );
    },
    NavigateToSalariedScreenComponent : function(component, event, helper){
        $A.createComponent(
            "c:Salaried",
            {
                "Customerid" :event.getParam("recordIdNumber"),
                "loanAppId" : event.getParam("loanAppId")
            },
            function(newCmp){
                if (component.isValid()) {
                    component.set("v.body", newCmp);
                }
            }
        );
    },
    NavigateToAIPScreenComponent : function(component, event, helper){
        console.log('AIP Screen event handled');
        $A.createComponent(
            "c:AIPInputs",
            {
                "Customerid" :event.getParam("recordIdNumber"),
                "loanAppId" : event.getParam("loanAppId")
            },
            function(newCmp){
                if (component.isValid()) {
                    component.set("v.body", newCmp);
                }
            }
        );
    },
    NavigateToObligationsScreenComponent : function(component, event, helper){
        $A.createComponent(
            "c:Obligations",
            {
                "Customerid" :event.getParam("recordIdNumber"),
                "loanAppId" : event.getParam("loanAppId")
            },
            function(newCmp){
                if (component.isValid()) {
                    component.set("v.body", newCmp);
                }
            }
        );
    },
    NavigateToOtherIncomeScreenComponent : function(component, event, helper){
        $A.createComponent(
            "c:OtherIncomecomp",
            {
                "Customerid" :event.getParam("recordIdNumber"),
                "loanAppId" : event.getParam("loanAppId")
            },
            function(newCmp){
                if (component.isValid()) {
                    component.set("v.body", newCmp);
                }
            }
        );
        
    },
    NavigateToBankingInputScreenComponent : function(component, event, helper){
        $A.createComponent(
            "c:Banking_Input2", //c:Banking_Input changed to c:Banking_Input2 by Abhilekh for BRE2 Enhancements
            {
                "custRecordId" :event.getParam("recordIdNumber"),
                "loanAppId" : event.getParam("loanAppId")
            },
            function(newCmp){
                if (component.isValid()) {
                    component.set("v.body", newCmp);
                } else {
                    console.log('ERROR FOUND');
                }
            }
        );
    },
    
    NavigateToCamScreenComponent : function(component, event, helper){
        $A.createComponent(
            "c:CAM_Input_Screen",
            {
                "recordIdNumber" :event.getParam("recordIdNumber"),
                "isOpen" : event.getParam("isOpen"),
                "testLoanAppId" : event.getParam("testLoanAppId")
            },
            function(newCmp){
                if (component.isValid()) {
                    component.set("v.body", newCmp);
                }
            }
        );
    }
})