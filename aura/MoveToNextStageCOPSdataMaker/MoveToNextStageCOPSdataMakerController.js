({
	onAccept : function(component, event, helper) //Modified Method name from doinit to onAccept
    {
		/************Added by Chitransh for TIL-1259***************/
        var validate = true;
        
        var commField = component.find("comm"); 
        var comm = commField.get("v.value"); 
        if($A.util.isEmpty(comm)){
            validate = false;
            commField.set("v.errors", [{message:"Comments can't be blank."}]);
        }
        else{
            commField.set("v.errors", null);
        }
        /******End of Patch added by Chitrannsh for TIL-1259**********/
        if(validate){  //Added by Chitransh for TIL-1259									
        var loanApplicationId = component.get("v.recordId");
        
        var action = component.get('c.moveToCOPS');
        action.setParams({  
            "loanApplicationId": component.get("v.recordId"),
			"comment": component.get("v.comments")/*** Added by Saumya for TIL-00002317 ***/
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") 
            {
                component.set("v.Spinner", true); 
                console.log('actionResult::'+JSON.stringify(actionResult.getReturnValue()));
                var returnValue = actionResult.getReturnValue();

                component.set('v.operationAllowed',returnValue.success);
                component.set('v.previousStage',returnValue.prevStage);
                component.set('v.errorMsges',returnValue.errorMsg);
                console.log(component.get('v.errorMsges'));
                /*if(returnValue.errorMsg == '' || returnValue.errorMsg == null || $A.util.isUndefined(returnValue.errorMsg)){
 				$A.get("e.force:closeQuickAction").fire();                
                }*/
                if(returnValue.success && returnValue.prevStage)
                {
                    component.set("v.Spinner", false); 
                    var showToast = $A.get("e.force:showToast"); 
					showToast.setParams({ 
                        'type': 'SUCCESS',
						'message' : returnValue.rtrnMsg
					}); 
                    showToast.fire(); 	
                    var navEvent = $A.get("e.force:navigateToList");
                            navEvent.setParams({
                        "listViewId": loanApplicationId,
                        "listViewName": "All",
                        "scope": "Loan_Application__c"
                    });
                    
                    navEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
            		$A.get('e.force:refreshView').fire();
                }
                //Below else block added by Abhilekh on 11th October 2019 for FCU BRD
                else{
                    component.set("v.Spinner", false); 
                    var showToast = $A.get("e.force:showToast"); 
					showToast.setParams({ 
                        'type': 'ERROR',
						'message' : returnValue.rtrnMsg
					}); 
                    showToast.fire();
                    $A.get("e.force:closeQuickAction").fire();
                }
            }
        });
        $A.enqueueAction(action);
		}
	},
   /* <!-- Added by Saumya for Move to next Stage -->*/
     showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
    
    doneNoAction : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
      /* <!-- Added by Saumya for Move to next Stage -->*/
})