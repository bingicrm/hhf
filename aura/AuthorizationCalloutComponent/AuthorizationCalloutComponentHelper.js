({
    doInitHelper : function(component, event, helper) 
    {
        //this.toggleSpinner(component, event, helper, "show");
        
        var recordId = component.get("v.recordId");
        console.log('recordID++ '+recordId)
        var action = component.get("c.loadLoanDownsizingInfo");
        action.setParams({ loanApplicationID : recordId });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS")
            {
                console.log('Inital Load'+response.getReturnValue());
                var responseBody = response.getReturnValue();
                if(this.checkBlankString(responseBody))
                {
                    component.set("v.loanDownsizingID",JSON.parse(JSON.stringify(responseBody)).Id);
                    console.log('LD ID++ '+component.get("v.loanDownsizingID"));
                    component.set("v.loanDownsizing", responseBody);
                    this.toggleSpinner(component, event, helper, "hide"); // remove spinner
                    
                    console.log('attribute-loanDownsizing1++ '+component.get("v.loanDownsizing"));
                    
                    var parsedResponse = JSON.parse(JSON.stringify(responseBody));
                    var remmaingAmount =  parsedResponse.Loan_Application__r.Pending_Amount_for_Disbursement__c - parsedResponse.Downsizing_Amount__c;
                    var revisedLoanAmount = parsedResponse.Loan_Application__r.Approved_Loan_Amount__c - parsedResponse.Downsizing_Amount__c;
                    console.log('revisedLoanAmount++'+revisedLoanAmount);

                    component.set("v.remaningAmount", remmaingAmount);
                    component.set("v.revisedLoanAmount", revisedLoanAmount);
                    //component.set("v.showLdDetails",true);
                }
                else
                {
                    this.showToast(component, event, helper, 'No Approved Loan Downsizing record Found', 'error')
                    this.toggleSpinner(component, event, helper, "hide");
                    $A.get("e.force:closeQuickAction").fire();                    
                }
            }
            else
            {
                console.log('Contact Administrator - Error has Occurred.');
                this.showToast(component, event, helper, 'Contact Administrator - Error has Occurred.', 'error')
                this.toggleSpinner(component, event, helper, "hide");
                $A.get("e.force:closeQuickAction").fire(); 
            }
        });
        
        $A.enqueueAction(action);   
    },
    //Method makes call to apex method for Callout to LMS.
    handleLmsCalloutHelper : function(component, event, helper)
    {
        //this.toggleSpinner(component, event, helper, "show");
        
        console.log("Inside handleLmsCalloutHelper");
        var recordId = component.get("v.recordId");
        var loanDownsizing = component.get("v.loanDownsizing");
        var stringLoanDownsizing = JSON.stringify(loanDownsizing);
        console.log('recordId++ '+recordId);
        
        if(this.checkBlankString(stringLoanDownsizing))
        {
            var parsedDownsizing = JSON.parse(JSON.stringify(loanDownsizing));
            console.log('loanDownsizing+ '+JSON.stringify(loanDownsizing));
            console.log('parsedDownsizing'+parsedDownsizing); 
            
            if(this.checkBlankString(parsedDownsizing) && this.checkBlankString(parsedDownsizing.Authorization_remark__c))
            {
                console.log('parsedDownsizing++ '+parsedDownsizing);
                if(parsedDownsizing.Case_is_not_delinquent__c && parsedDownsizing.NACH_is_registered__c)
                {
                    // call apex controller
                    console.log('Remarks is not blank');
                    //this.toggleSpinner(component, event, helper, "show");
                    
                    var action = component.get("c.sendForLmsDownsizingAuthorization");
                    
                    action.setParams({ "loanApplicationID" : recordId,
                                     "authorizationRemarks" : parsedDownsizing.Authorization_remark__c });
                    action.setCallback(this, function(response){
                        if(response.getState()==="SUCCESS")
                        {
                            console.log('inside response - authorization');
                            var responsebody = response.getReturnValue();
                            console.log('responsebody++ '+responsebody);
                            console.log('responsebody-Stringify '+JSON.stringify(responsebody));
                            
                            if(this.checkBlankString(responsebody))
                            {
                                console.log(' Inside Blank Check responsebody++ '+responsebody);
                                if(responsebody=="Loan Downsizing Successfull..")
                                {
                                    this.showToast(component,event,helper,responsebody,'success');
                                    this.toggleSpinner(component, event, helper, "hide");
                                    $A.get("e.force:closeQuickAction").fire(); 
                                }
                                else
                                {
                                    console.log('Inside error block');
                                    this.showToast(component,event,helper,responsebody,'failure'); 
                                    this.toggleSpinner(component, event, helper, "hide");
                                    //$A.get("e.force:closeQuickAction").fire();
                                }
                            }
                        }
                    });
                    $A.enqueueAction(action);
                }
                else
                {
                    console.log('NACH is unchecked++');
                    this.showToast(component, event, helper,'Downsizing authorization details are incomplete.', 'error');
                    this.toggleSpinner(component, event, helper, "hide"); 
                }
            }
            /*else if(this.showHelpMessageIfInvalid(parsedDownsizing.Authorization_remark__c))
            {
               this.showToast(component, event, helper,'Authorization Remark should not contain special characters', 'error');
                this.toggleSpinner(component, event, helper, "hide");   
            }*/
            
            else
            {
                console.log('Remarks is blank++');
                this.showToast(component, event, helper,'Authorization Remark is mandatory before making a Downsizing Request to LMS', 'error');
                this.toggleSpinner(component, event, helper, "hide");  
            }
        }
        else
        {
            this.showToast(component, event, helper,'Unexpected error', 'error');
            
            this.toggleSpinner(component, event, helper, "hide");
        }
    },
    // method for checking blank strings
    checkBlankString : function(str)
    {
        //console.log('str val+ '+str+' '+str.length);
        if (str == null ||  str == undefined || str.length == 0 || str.pattern == "[a-zA-Z0-9*()>&lt;!@$^, ]")
        {
            console.log('Blank String');
            return false;  
        }
        console.log('non null string');
        return true;
    },
    
    /*showHelpMessageIfInvalid : function(str)
    {
        if (str == "[a-zA-Z0-9#*()>&lt;!@$^, ]*")
        {
           return false; 
        }
    },*/
    //method to show toast message
    showToast : function(component, event, helper, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": type.toUpperCase(),
            "message": message,
            "type" : type,
        });
        toastEvent.fire();
    },
    
    //Method to toggle spinner
    toggleSpinner : function(component, event, helper, str)
    {
        var findSpinner = component.find("mySpinner");
        if(str=="show")
        {
            $A.util.removeClass(findSpinner, "slds-hide");
            $A.util.addClass(findSpinner, "slds-show");   
        }
        else
        {
            $A.util.removeClass(findSpinner, "slds-show");
            $A.util.addClass(findSpinner, "slds-hide");   
        }
        
    }
    
})