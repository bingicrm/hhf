({
    doinit : function(component, event, helper) 
    {
        helper.toggleSpinner(component, event, helper, "show");
        helper.doInitHelper(component, event, helper);
    },
    
    activeButton : function(component, event, helper){
        var allValid = component.find('inpauthority').get("v.validity").valid; 
        if(!allValid){
          component.set('v.isButtonActive',true);  
        }else{
           component.set('v.isButtonActive',false);  
        }
       },
    
    handleLmsCallout : function(component, event, helper)
    {
        helper.toggleSpinner(component, event, helper, "show");
        helper.handleLmsCalloutHelper(component, event, helper);
    },
   /* handlerSpin : function(component, event, helper)
    {
        helper.toggleSpinner(component,event,helper,"show");
    }*/
})