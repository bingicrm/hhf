({
   doInit : function(component, event, helper) {
       console.log('Creating LA Creation component');
       console.log('PA recordId'+component.get('v.recordId'));
       console.log('calledFromVFPage '+component.get('v.calledFromVFPage'));
       var recordId = component.get('v.recordId');
       var calledFromVFPage = component.get('v.calledFromVFPage');
       if(recordId != undefined) {
          $A.createComponent(
             "c:ExpressQueueLACreation",
             {
                "recordId" :component.get('v.recordId'),
               "QDEMode" : true,
               "isInserted" : true,
			   
			   
             },
             function(newCmp){
                if (component.isValid()) {
                   component.set("v.body", newCmp);
                }
             }
          );
       } else {
           $A.createComponent(
             "c:ExpressQueueLACreation",
             {
                "calledFromVFPage" : calledFromVFPage,
                "loanAppID" : ''
             },
             function(newCmp){
                if (component.isValid()) {
                   component.set("v.body", newCmp);
                }
             }
          );
           
       }       
   },
   NavigateToAddressComponent : function(component,event,helper) {
       console.log('Creating address component');
       console.log('ADD ID TYPES '+JSON.stringify(event.getParam('idTypes')));
      $A.createComponent(
         "c:AddressDetails",
         {
            "customerDetailNumber" :event.getParam("customerDetailNumber"),
           "customerDetailId" : event.getParam("customerDetail"),
           "laVal" : event.getParam("laVal"),
            "CDWrap" : event.getParam("CDWrap"),
            "lstBorrowerType" : event.getParam("lstBorrowerType"),
            "lstconstitution" : event.getParam("lstconstitution"),
            "lstCustSegment" : event.getParam("lstCustSegment"),
            "loanAppID" : event.getParam("loanAppID"),
            "prod" : event.getParam("prod"),
            "localPol" : event.getParam("localPol"),
            "customer" : event.getParam("customer"),
            "isDisplay" : event.getParam("isDisplay") ,
            "isEditable" : event.getParam("isEditable"),
            "isInserted" : event.getParam("isInserted"),
            "isDisable" : event.getParam("isDisable"),
            "addAddress" : event.getParam("addAddress"),
            "CDfetched" : event.getParam("CDfetched"),
            "data" : event.getParam("data"),
            "columns" : event.getParam("columns"),
            "borrowerTy" : event.getParam("borrowerTy"),
            "LaNum" : event.getParam("LaNum"),
            "isEditableLA" : event.getParam("isEditableLA"),
             "QDEMode" : event.getParam("QDEMode"),
             "calledFromVFPage" : event.getParam("calledFromVFPage"),
             "errorMessage" : event.getParam("errorMessage"),
            "showErrorMessage" :event.getParam("showErrorMessage"),
            "LoanConId" : event.getParam("LoanConId"),
            "idTypes" : event.getParam("idTypes"),
            "oldWrap" : event.getParam("oldWrap"),
            "oldWrapString" : event.getParam("oldWrapString"),
            "disableAddButton" : event.getParam("disableAddButton"),
            "disableEditButton" : event.getParam("disableEditButton")
         },
         function(newCmp){
            if (component.isValid()) {
                component.set("v.body", newCmp);
            }
         }
      );
   },
   NavigateBackToLAComponent : function(component,event,helper) {
       console.log('Creating LA component');
       console.log('QDEMOde while navigating back'+event.getParam("QDEMode"));
       var navigateBack = true;
      $A.createComponent(
         "c:ExpressQueueLACreation",
         {
            "laVal" : event.getParam("laVal"),
            "CDWrap" : event.getParam("CDWrap"),
            "lstBorrowerType" : event.getParam("lstBorrowerType"),
            "lstconstitution" : event.getParam("lstconstitution"),
            "lstCustSegment" : event.getParam("lstCustSegment"),
            "loanAppID" : event.getParam("loanAppID"),
            "prod" : event.getParam("prod"),
            "localPol" : event.getParam("localPol"),
            "customer" : event.getParam("customer"),
            "isDisplay" : event.getParam("isDisplay") ,
            "isEditable" : event.getParam("isEditable"),
            "isInserted" : event.getParam("isInserted"),
            "isDisable" : event.getParam("isDisable"),
            "addAddress" : event.getParam("addAddress"),
            "CDfetched" : event.getParam("CDfetched"),
            "data" : event.getParam("data"),
            "columns" : event.getParam("columns"),
            "borrowerTy" : event.getParam("borrowerTy"),
            "LaNum" : event.getParam("LaNum"),
            "isEditableLA" : event.getParam("isEditableLA"),
            "navigateBack" : navigateBack,
            "QDEMode" : event.getParam("QDEMode"),
            "calledFromVFPage" : event.getParam("calledFromVFPage"),
            "errorMessage" : event.getParam("errorMessage"),
            "showErrorMessage" :event.getParam("showErrorMessage"),
            "LoanConId" : event.getParam("LoanConId"),
            "idTypes" : event.getParam("idTypes"),
            "oldWrap" : event.getParam("oldWrap"),
            "oldWrapString" : event.getParam("oldWrapString"),
            "disableAddButton" : event.getParam("disableAddButton"),
            "disableEditButton" : event.getParam("disableEditButton")
         },
         function(newCmp){
            if (component.isValid()) {
                component.set("v.body", newCmp);
            }
         }
      );
   },
   NaviateToPANComponent: function(component,event,helper) {
        console.log('Creating PAN component'); 
        console.log('RecordId '+event.getParam("recordId"));
        console.log('fromLAComponent'+event.getParam("fromLAComponent"));
        console.log('event.getParam("index") '+event.getParam("index"));
        console.log('event.getParam("customerDetail") '+event.getParam("customerDetail"));
        console.log('event.getParam("laVal") '+event.getParam("laVal"));
        console.log('event.getParam("CDWrap") '+event.getParam("CDWrap"));
        console.log('event.getParam("lstBorrowerType") '+event.getParam("lstBorrowerType"));
        console.log('event.getParam("lstconstitution") '+event.getParam("lstconstitution"));
        console.log('event.getParam("lstCustSegment") '+event.getParam("lstCustSegment"));
        console.log('event.getParam("loanAppID") '+event.getParam("loanAppID"));
        console.log('event.getParam("prod") '+event.getParam("prod"));
        console.log('event.getParam("localPol") '+event.getParam("localPol"));
        console.log('event.getParam("customer") '+event.getParam("customer"));
        console.log('event.getParam("isDisplay") '+event.getParam("isDisplay"));
        console.log('event.getParam("isEditable") '+event.getParam("isEditable"));
        console.log('event.getParam("isInserted") '+event.getParam("isInserted"));
        console.log('event.getParam("isDisable") '+event.getParam("isDisable"));
        console.log('event.getParam("addAddress") '+event.getParam("addAddress"));
        console.log('event.getParam("CDfetched") '+event.getParam("CDfetched"));
        console.log('event.getParam("data") '+event.getParam("data"));
        console.log('event.getParam("columns") '+event.getParam("columns"));
        console.log('event.getParam("borrowerTy") '+event.getParam("borrowerTy"));
        console.log('event.getParam("LaNum") '+event.getParam("LaNum"));
        console.log('event.getParam("isEditableLA") '+event.getParam("isEditableLA"));
        console.log('event.getParam("QDEMode") '+event.getParam("QDEMode"));
        console.log('event.getParam("calledFromVFPage") '+event.getParam("calledFromVFPage"));
        console.log('event.getParam("errorMessage") '+event.getParam("errorMessage"));
        console.log('event.getParam("showErrorMessage") '+event.getParam("showErrorMessage"));
        console.log('event.getParam("LoanConId") '+event.getParam("LoanConId"));
        console.log('event.getParam("idTypes") '+event.getParam("idTypes"));
        console.log('event.getParam("oldWrap") '+event.getParam("oldWrap"));
        console.log('event.getParam("oldWrapString") '+event.getParam("oldWrapString"));
        console.log('event.getParam("disableAddButton") '+event.getParam("disableAddButton"));
        console.log('event.getParam("disableEditButton") '+event.getParam("disableEditButton"));
        $A.createComponent(
         "c:PanValidationCheck",
         {
            "recordId" : event.getParam("recordId"),
            "fromLAComponent" :event.getParam("fromLAComponent"),
            "index" : event.getParam("index"),
            "customerDetailId" : event.getParam("customerDetail"),
            "laVal" : event.getParam("laVal"),
            "CDWrap" : event.getParam("CDWrap"),
            "lstBorrowerType" : event.getParam("lstBorrowerType"),
            "lstconstitution" : event.getParam("lstconstitution"),
            "lstCustSegment" : event.getParam("lstCustSegment"),
            "loanAppID" : event.getParam("loanAppID"),
            "prod" : event.getParam("prod"),
            "localPol" : event.getParam("localPol"),
            "customer" : event.getParam("customer"),
            "isDisplay" : event.getParam("isDisplay") ,
            "isEditable" : event.getParam("isEditable"),
            "isInserted" : event.getParam("isInserted"),
            "isDisable" : event.getParam("isDisable"),
            "addAddress" : event.getParam("addAddress"),
            "CDfetched" : event.getParam("CDfetched"),
            "data" : event.getParam("data"),
            "columns" : event.getParam("columns"),
            "borrowerTy" : event.getParam("borrowerTy"),
            "LaNum" : event.getParam("LaNum"),
            "isEditableLA" : event.getParam("isEditableLA"),
            "QDEMode" : event.getParam("QDEMode"),
            "calledFromVFPage" : event.getParam("calledFromVFPage"),
             "errorMessage" : event.getParam("errorMessage"),
            "showErrorMessage" :event.getParam("showErrorMessage"),
            "LoanConId" : event.getParam("LoanConId"),
            "idTypes" : event.getParam("idTypes"),
            "oldWrap" : event.getParam("oldWrap"),
            "oldWrapString" : event.getParam("oldWrapString"),
            "disableAddButton" : event.getParam("disableAddButton"),
            "disableEditButton" : event.getParam("disableEditButton")
        },
         function(newCmp){
            console.log('Befocomponent.isValid()'+component.isValid());
            if (component.isValid()) {
                console.log('After component.isValid()'+component.isValid());
                //var body = component.get("v.body");
                component.set("v.body", newCmp);
            }
         }
      );
   } ,
   NaviateToCIBILComponent :function(component,event,helper) {
        console.log('Creating CIBIL component'); 
        console.log('RecordId '+event.getParam("recordId"));
        console.log('fromLAComponent'+event.getParam("fromLAComponent"));
        $A.createComponent(
         "c:CibilIndividual",
         {
            "recordId" : event.getParam("recordId"),
            "fromLAComponent" :event.getParam("fromLAComponent"),
            "index" : event.getParam("index"),
            "customerDetailId" : event.getParam("customerDetail"),
            "laVal" : event.getParam("laVal"),
            "CDWrap" : event.getParam("CDWrap"),
            "lstBorrowerType" : event.getParam("lstBorrowerType"),
            "lstconstitution" : event.getParam("lstconstitution"),
            "lstCustSegment" : event.getParam("lstCustSegment"),
            "loanAppID" : event.getParam("loanAppID"),
            "prod" : event.getParam("prod"),
            "localPol" : event.getParam("localPol"),
            "customer" : event.getParam("customer"),
            "isDisplay" : event.getParam("isDisplay") ,
            "isEditable" : event.getParam("isEditable"),
            "isInserted" : event.getParam("isInserted"),
            "isDisable" : event.getParam("isDisable"),
            "addAddress" : event.getParam("addAddress"),
            "CDfetched" : event.getParam("CDfetched"),
            "data" : event.getParam("data"),
            "columns" : event.getParam("columns"),
            "borrowerTy" : event.getParam("borrowerTy"),
            "LaNum" : event.getParam("LaNum"),
            "isEditableLA" : event.getParam("isEditableLA"),
            "QDEMode" : event.getParam("QDEMode"),
            "calledFromVFPage" : event.getParam("calledFromVFPage"),
             "errorMessage" : event.getParam("errorMessage"),
            "showErrorMessage" :event.getParam("showErrorMessage"),
            "LoanConId" : event.getParam("LoanConId"),
            "idTypes" : event.getParam("idTypes"),
            "oldWrap" : event.getParam("oldWrap"),
            "oldWrapString" : event.getParam("oldWrapString"),
            "disableAddButton" : event.getParam("disableAddButton"),
            "disableEditButton" : event.getParam("disableEditButton")
        },
         function(newCmp){
            if (component.isValid()) {
                component.set("v.body", newCmp);
            }
         }
      );    
   },
   
   NaviateToUploadDocumentComponent : function(component,event,helper) {
        console.log('Creating Upload Document component'); 
        console.log('RecordId '+event.getParam("recordId"));
        console.log('fromLAComponent'+event.getParam("fromLAComponent"));
        console.log('QDEMode '+event.getParam("QDEMode"));
        $A.createComponent(
         "c:DocumentChecklistCompnent",
         {
            "recordId" : event.getParam("recordId"),
            "fromLAComponent" :event.getParam("fromLAComponent"),
            "laVal" : event.getParam("laVal"),
            "CDWrap" : event.getParam("CDWrap"),
            "lstBorrowerType" : event.getParam("lstBorrowerType"),
            "lstconstitution" : event.getParam("lstconstitution"),
            "lstCustSegment" : event.getParam("lstCustSegment"),
            "loanAppID" : event.getParam("loanAppID"),
            "prod" : event.getParam("prod"),
            "localPol" : event.getParam("localPol"),
            "customer" : event.getParam("customer"),
            "isDisplay" : event.getParam("isDisplay") ,
            "isEditable" : event.getParam("isEditable"),
            "isInserted" : event.getParam("isInserted"),
            "isDisable" : event.getParam("isDisable"),
            "addAddress" : event.getParam("addAddress"),
            "CDfetched" : event.getParam("CDfetched"),
            "data" : event.getParam("data"),
            "columns" : event.getParam("columns"),
            "borrowerTy" : event.getParam("borrowerTy"),
            "LaNum" : event.getParam("LaNum"),
            "isEditableLA" : event.getParam("isEditableLA"),
            "QDEMode" : event.getParam("QDEMode"),
            "calledFromVFPage" : event.getParam("calledFromVFPage"),
            "errorMessage" : event.getParam("errorMessage"),
            "showErrorMessage" :event.getParam("showErrorMessage"),
            "LoanConId" : event.getParam("LoanConId"),
            "idTypes" : event.getParam("idTypes"),
            "oldWrap" : event.getParam("oldWrap"),
            "oldWrapString" : event.getParam("oldWrapString"),
            "disableAddButton" : event.getParam("disableAddButton"),
            "disableEditButton" : event.getParam("disableEditButton")
        },
         function(newCmp){
            console.log('PAN Component Valid?'+component.isValid());
            if (component.isValid()) {
                component.set("v.body", newCmp);
            }
         }
      );    
   }
})