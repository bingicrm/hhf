({
	doInit : function(component, event, helper) {
        var controllingFieldAPI = component.get("v.controllingFieldAPI");
        var dependingFieldAPI = component.get("v.dependingFieldAPI");
        var objDetails = component.get("v.lapp");
        
		var action = component.get("c.getLoanApp");
        action.setParams({"lappId" : component.get("v.recordId")});
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(state === "SUCCESS"){
                component.set("v.lapp", response.getReturnValue());
                helper.getPickListValues(component,event,helper);
                helper.getPickListValues2(component,event,helper);
            }else{
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title" : "Error",
                    "message" : "Cannot fetch the Loan Application."
                });
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            }
        });
        $A.enqueueAction(action);
        helper.fetchPicklistValues(component,objDetails,controllingFieldAPI, dependingFieldAPI);
	},
    requestCreation: function(component, event, helper) {
         var la = component.get("v.lapp"); //Added by Abhilekh on 25th September 2019 for FCU BRD
        
        //Below if and else block added by Abhilekh on 25th September 2019 for FCU BRD
        if(la.Sub_Stage__c == 'FCU Review'){
            var action = component.get("c.rejectApplication");
            
            action.setParams({
                "la" : la
            });
            action.setCallback(this, function(actionResult) {
                console.log('3333'+actionResult.getReturnValue());
                var state = actionResult.getState();
                if(state === "SUCCESS"){
                    console.log('4444');
                    var resultsToast = $A.get("e.force:showToast");
                    if(actionResult.getReturnValue() == null || actionResult.getReturnValue() == ''){
                        resultsToast.setParams({
                            "title" : "Rejected",
                            "message" : "Loan Application has been rejected."
                        });
                    }
                    else{
                        resultsToast.setParams({
                            "title" : "Error",
                            "message" : actionResult.getReturnValue()
                        });
                    }
                    
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                    console.log('5555');
                }else if(state === "ERROR"){
                    console.log('Problem creating records, response state '+state);
                    console.log('6666');
                }else{
                    console.log('Unknown problem: '+state);
                    console.log('7777');
                }
            });
            $A.enqueueAction(action);
        }
	//Below else if block added by Abhilekh on 29th January 2020 for Hunter BRD
        else if(la.Sub_Stage__c == 'Hunter Review'){
            var action = component.get("c.rejectApplication");
            
            action.setParams({
                "la" : la
            });
            action.setCallback(this, function(actionResult) {
                console.log('3333'+actionResult.getReturnValue());
                var state = actionResult.getState();
                if(state === "SUCCESS"){
                    console.log('4444');
                    var resultsToast = $A.get("e.force:showToast");
                    if(actionResult.getReturnValue() == null || actionResult.getReturnValue() == ''){
                        resultsToast.setParams({
                            "title" : "Rejected",
                            "message" : "Loan Application has been rejected."
                        });
                    }
                    else{
                        resultsToast.setParams({
                            "title" : "Error",
                            "message" : actionResult.getReturnValue()
                        });
                    }
                    
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                    console.log('5555');
                }else if(state === "ERROR"){
                    console.log('Problem creating records, response state '+state);
                    console.log('6666');
                }else{
                    console.log('Unknown problem: '+state);
                    console.log('7777');
                }
            });
            $A.enqueueAction(action);
        }
        else{
            component.set("v.showBlock",true);
            component.set("v.showBlockDef",false);
            console.log('1111');
        }
    },
    requestSave: function(component, event, helper){
        var severityVar = component.get("v.lapp.Severity_Level__c");
        var RejectionVar = component.get("v.lapp.Rejection_Reason__c");
        console.log('severityVar::'+severityVar+'RejectionVar::'+RejectionVar);
        if(severityVar == '--- None ---'|| RejectionVar == '--- None ---' || severityVar == '' || RejectionVar == ''){
                  var resultToast = $A.get("e.force:showToast");
            if(RejectionVar == '--- None ---' || RejectionVar == ''){
                	resultToast.setParams({
                    	"title" : "Rejected",
                    	"message" : "Please select Values Rejection Reason."
                	});
            }
            else if(severityVar == '--- None ---' || severityVar == '' ){
                resultToast.setParams({
                    	"title" : "Rejected",
                    	"message" : "Please select Values Severity Reason."
                	});
                
            }
				$A.get("e.force:closeQuickAction").fire();
                resultToast.fire();
            
        }
        else{
            	var action = component.get("c.rejectApplication");
		var loan = component.get("v.lapp");
        console.log(JSON.stringify(loan));
        console.log('SeverityVal:::'+component.get("v.lapp.Severity_Level__c"));

        action.setParams({
            "la" : loan
        });
        console.log('2222');
        action.setCallback(this, function(actionResult) {
            console.log('3333');
            var state = actionResult.getState();
            if(state === "SUCCESS"){
                console.log('4444');
                var resultsToast = $A.get("e.force:showToast");
                
                if(actionResult.getReturnValue() == null || actionResult.getReturnValue() == ''){ //|| actionResult.getReturnValue() == '' added by Abhilekh on 28th January 2020 for Hunter BRD
                	resultsToast.setParams({
                    	"title" : "Rejected",
                    	"message" : "Loan Application has been rejected."
                	});
            	}
            	else{
                	resultsToast.setParams({
                    	"title" : "Error",
                    	"message" : actionResult.getReturnValue(),
                        "type" : "error" // Added by Chitransh for TIL-840 [27-09-2019]
                	});
            	}
 
				$A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
                console.log('5555');
            }else if(state === "ERROR"){
                console.log('Problem creating records, response state '+state);
                console.log('6666');
            }else{
                console.log('Unknown problem: '+state);
                console.log('7777');
            }
        });
        $A.enqueueAction(action);
            
        }
    
  
    },
    cancelCreation: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    
    onControllerFieldChange: function(component, event, helper) {     
        var controllerValueKey = event.getSource().get("v.value"); // get selected controller field value
        var depnedentFieldMap = component.get("v.depnedentFieldMap");
        
        if (controllerValueKey != '--- None ---') {
            var ListOfDependentFields = depnedentFieldMap[controllerValueKey];
            
            if(ListOfDependentFields.length > 0){
                component.set("v.bDisabledDependentFld" , false);  
                helper.fetchDepValues(component, ListOfDependentFields);    
            }else{
                component.set("v.bDisabledDependentFld" , true); 
                component.set("v.listDependingValues", ['--- None ---']);
            }  
            
        } else {
            component.set("v.listDependingValues", ['--- None ---']);
            component.set("v.bDisabledDependentFld" , true);
        }
    },
    onControllerFieldSelect: function(component, event, helper) {  
        var x= component.get("v.lapp.Severity_Level__c");
        console.log('x::'+x);
            component.set("v.lapp.Severity_Level__c", x);

    }
})