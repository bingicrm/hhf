({
        doInit: function(component, event, helper) 
    	{
        	// Added by Vaishali for BRE		        	
        	console.log('RecordId' +component.get("v.recordId"));		
        	console.log('fromLAComponent: PAN Validation controller '+component.get("v.fromLAComponent"));		
        	// End of the patch Added by Vaishali for BRE
			var action1 = component.get("c.getCustomerDetails");
        	action1.setParams({ loanContactId : component.get("v.recordId") });

            action1.setCallback(this, function(response) {
                var state1 = response.getState();
                if (state1 === "SUCCESS") {
                    //showBlock = True;
                    component.set("v.showBlock",true);
                   	if(response.getReturnValue() != null){
                   		component.set("v.custDetails",response.getReturnValue());
                    }
                    
                }
                 //$A.get('e.force:refreshView').fire();  //Commented this line by Vaishali for BRE
            });
            $A.enqueueAction(action1);

			
            var action = component.get("c.validationPanDetails");
            console.log('heellow world');
        	action.setParams({ loanContactId : component.get("v.recordId") });

            action.setCallback(this, function(response) {
                var state = response.getState();
                var resultsToast = $A.get("e.force:showToast");
                if (state === "SUCCESS") {
                    //showBlock = True;
                    component.set("v.showBlock",true);
                    console.log('==show=='+component.get("v.showBlock"));
                   	console.log(response.getReturnValue());
                   	if(response.getReturnValue() != null){
                   		component.set("v.panDetails",response.getReturnValue());
                        var panStatus = component.get('v.panStatuses');
                        console.log(panStatus);
                        console.log(panStatus[response.getReturnValue().PAN_Status__c]);
                        
                        component.set("v.panStatus",panStatus[response.getReturnValue().PAN_Status__c]);
                   	}
                    else if(response.getReturnValue() == null){
                        console.log(response.getReturnValue());
                             //Added by Vaishali Mehta for BRE
                        var calledFromVFPage = component.get('v.calledFromVFPage');
                        var fromLAComponent = component.get('v.fromLAComponent');
                        if(fromLAComponent == false || fromLAComponent == true && calledFromVFPage == false) {
                            var resultsToast = $A.get("e.force:showToast");
                                resultsToast.setParams({
                                "title" : "Exception",
                                "message" : "You either do not have the access to perform this action or the PAN number entered is incorrect."
                            });
                            resultsToast.fire();
                        } else {
                            component.set('v.showErrorMessage' , true);
                            component.set('v.errorMessage','You either do not have the access to perform this action or the PAN number entered is incorrect.');
                        }
                        var calledFromLAComponent = component.get('v.fromLAComponent'); 
                        console.log('fromLAComponent: NO method '+calledFromLAComponent);
                         if(calledFromLAComponent === true) {
                            console.log('laval'+ component.get('v.laVal'));
                            var evt = $A.get("e.c:callCreateLAEvent");
                            evt.setParams(
                                { 
                                    "customerDetail": component.get('v.customerDetail') ,
                                    "laVal" : component.get('v.laVal'),
                                    "CDWrap" : component.get('v.CDWrap'),
                                    "lstBorrowerType" : component.get('v.lstBorrowerType'),
                                    "lstconstitution" : component.get('v.lstconstitution'),
                                    "lstCustSegment" : component.get('v.lstCustSegment'),
                                    "loanAppID" : component.get('v.loanAppID'),
                                    "prod" : component.get('v.prod'),
                                    "localPol" : component.get('v.localPol'),
                                    "customer" : component.get('v.customer'),
                                    "isDisplay" : component.get('v.isDisplay') ,
                                    "isEditable" : component.get('v.isEditable'),
                                    "isInserted" : component.get('v.isInserted'),
                                    "isDisable" : component.get('v.isDisable'),
                                    "addAddress" : component.get('v.addAddress'),
                                    "CDfetched" : component.get('v.CDfetched'),
                                    "data" : component.get('v.data'),
                                    "columns" : component.get('v.columns'),
                                    "borrowerTy" : component.get('v.borrowerTy'),
                                    "LaNum" : component.get('v.LaNum'),
                                    "isEditableLA" : component.get('v.isEditableLA'),
                                    "QDEMode" : component.get('v.QDEMode'),
                                    "calledFromVFPage" : component.get('v.calledFromVFPage'),
                                    "errorMessage" : component.get('v.errorMessage'),
                                    "showErrorMessage" :component.get('v.showErrorMessage'),
                                    "LoanConId" : component.get('v.LoanConId'),
                                    "idTypes" : component.get('v.idTypes'),
                                    "oldWrap" : component.get('v.oldWrap'),
                                    "oldWrapString" : component.get('v.oldWrapString'),
                                    "disableAddButton" : component.get('v.disableAddButton'),
                                    "disableEditButton" : component.get('v.disableEditButton')
                                }
                            );
                            evt.fire(); 
                         } else {
                            $A.get("e.force:closeQuickAction").fire();
                            $A.get('e.force:refreshView').fire(); 
                         }    
                     // End of Code Added by Vaishali Mehta for BRE  
                        
                    }
                }
                
                else if (state === "INCOMPLETE") {
                    
                }
                else if (state === "ERROR") {
                   
                }
            });
            $A.enqueueAction(action);
            
            
        },
    doneAction:function(component, event, helper)
    {	
         //if yes
          var action3 = component.get("c.copyPanDetails");
          var customerID = component.get("v.custDetails.Customer_r.Id");
          
        	
        	 action3.setParams({ "customerId" : customerID,"idLoanContact" : component.get("v.recordId") });
            
            action3.setCallback(this, function(response) {
                var state3 = response.getState();
                if (state3 === "SUCCESS") {
                    //showBlock = True;
                    component.set("v.showBlock",true);
                   	if(response.getReturnValue() != null){
                   		component.set("v.boolSuccess",response.getReturnValue());
                    }
                    
                }
                 $A.get('e.force:refreshView').fire(); 
            });
            $A.enqueueAction(action3);
         //Added by Vaishali Mehta for BRE
         var calledFromLAComponent = component.get('v.fromLAComponent');
         console.log('fromLAComponent: Yes method '+calledFromLAComponent);
         if(calledFromLAComponent === true) {
            console.log('laval'+ component.get('v.laVal'));
            console.log('Hi PAN Success');
            var wrap = [];
            wrap = component.get('v.CDWrap');
            var index = component.get('v.index');
            console.log('wrap'+wrap);
            console.log('index'+index);
            wrap[index].PANVerified = true;
            console.log('wrap'+wrap);
            component.set('v.CDWrap',wrap);
        
            var evt = $A.get("e.c:callCreateLAEvent");
            evt.setParams(
                { 
                    "laVal" : component.get('v.laVal'),
                    "CDWrap" : component.get('v.CDWrap'),
                    "lstBorrowerType" : component.get('v.lstBorrowerType'),
                    "lstconstitution" : component.get('v.lstconstitution'),
                    "lstCustSegment" : component.get('v.lstCustSegment'),
                    "loanAppID" : component.get('v.loanAppID'),
                    "prod" : component.get('v.prod'),
                    "localPol" : component.get('v.localPol'),
                    "customer" : component.get('v.customer'),
                    "isDisplay" : component.get('v.isDisplay') ,
                    "isEditable" : component.get('v.isEditable'),
                    "isInserted" : component.get('v.isInserted'),
                    "isDisable" : component.get('v.isDisable'),
                    "addAddress" : component.get('v.addAddress'),
                    "CDfetched" : component.get('v.CDfetched'),
                    "data" : component.get('v.data'),
                    "columns" : component.get('v.columns'),
                    "borrowerTy" : component.get('v.borrowerTy'),
                    "LaNum" : component.get('v.LaNum'),
                    "isEditableLA" : component.get('v.isEditableLA'),
                    "QDEMode" : component.get('v.QDEMode'),
                    "calledFromVFPage" : component.get('v.calledFromVFPage'),
                    "errorMessage" : component.get('v.errorMessage'),
                    "showErrorMessage" :component.get('v.showErrorMessage'),
                    "LoanConId" : component.get('v.LoanConId'),
                    "idTypes" : component.get('v.idTypes'),
                    "oldWrap" : component.get('v.oldWrap'),
                    "oldWrapString" : component.get('v.oldWrapString'),
                    "disableAddButton" : component.get('v.disableAddButton'),
                    "disableEditButton" : component.get('v.disableEditButton')
                }
            );
            evt.fire();  
         }else {
             //End of code Added by Vaishali Mehta for BRE
             $A.get("e.force:closeQuickAction").fire();
             $A.get('e.force:refreshView').fire(); 
         } 
    },
    
    doneNoAction:function(component, event, helper)
    {	
         //Added by Vaishali Mehta for BRE
        var calledFromLAComponent = component.get('v.fromLAComponent'); 
        console.log('fromLAComponent: NO method '+calledFromLAComponent);
         if(calledFromLAComponent === true) {
            console.log('laval'+ component.get('v.laVal'));
            var evt = $A.get("e.c:callCreateLAEvent");
            evt.setParams(
                { 
                    "customerDetail": component.get('v.customerDetail') ,
                    "laVal" : component.get('v.laVal'),
                    "CDWrap" : component.get('v.CDWrap'),
                   "lstBorrowerType" : component.get('v.lstBorrowerType'),
                    "lstconstitution" : component.get('v.lstconstitution'),
                    "lstCustSegment" : component.get('v.lstCustSegment'),
                    "loanAppID" : component.get('v.loanAppID'),
                    "prod" : component.get('v.prod'),
                    "localPol" : component.get('v.localPol'),
                    "customer" : component.get('v.customer'),
                    "isDisplay" : component.get('v.isDisplay') ,
                    "isEditable" : component.get('v.isEditable'),
                    "isInserted" : component.get('v.isInserted'),
                    "isDisable" : component.get('v.isDisable'),
                    "addAddress" : component.get('v.addAddress'),
                    "CDfetched" : component.get('v.CDfetched'),
                    "data" : component.get('v.data'),
                    "columns" : component.get('v.columns'),
                    "borrowerTy" : component.get('v.borrowerTy'),
                    "LaNum" : component.get('v.LaNum'),
                    "isEditableLA" : component.get('v.isEditableLA'),
                    "QDEMode" : component.get('v.QDEMode'),
                    "calledFromVFPage" : component.get('v.calledFromVFPage'),
                    "errorMessage" : component.get('v.errorMessage'),
                    "showErrorMessage" :component.get('v.showErrorMessage'),
                    "LoanConId" : component.get('v.LoanConId'),
                    "idTypes" : component.get('v.idTypes'),
                    "oldWrap" : component.get('v.oldWrap'),
                    "oldWrapString" : component.get('v.oldWrapString'),
                    "disableAddButton" : component.get('v.disableAddButton'),
                    "disableEditButton" : component.get('v.disableEditButton')
                }
            );
            evt.fire(); 
         }else {
             //End of code Added by Vaishali Mehta for BRE
             $A.get("e.force:closeQuickAction").fire();
             $A.get('e.force:refreshView').fire(); 
         }  
    },
	//Added by Vaishali Mehta for BRE
    close :function(component,event,helper){
        var calledFromLAComponent = component.get('v.fromLAComponent'); 
        console.log('fromLAComponent: NO method '+calledFromLAComponent);
         if(calledFromLAComponent === true) {
            console.log('laval'+ component.get('v.laVal'));
            var evt = $A.get("e.c:callCreateLAEvent");
            evt.setParams(
                { 
                    "laVal" : component.get('v.laVal'),
                    "CDWrap" : component.get('v.CDWrap'),
                    "lstBorrowerType" : component.get('v.lstBorrowerType'),
                    "lstconstitution" : component.get('v.lstconstitution'),
                    "lstCustSegment" : component.get('v.lstCustSegment'),
                    "loanAppID" : component.get('v.loanAppID'),
                    "prod" : component.get('v.prod'),
                    "localPol" : component.get('v.localPol'),
                    "customer" : component.get('v.customer'),
                    "isDisplay" : component.get('v.isDisplay') ,
                    "isEditable" : component.get('v.isEditable'),
                    "isInserted" : component.get('v.isInserted'),
                    "isDisable" : component.get('v.isDisable'),
                    "addAddress" : component.get('v.addAddress'),
                    "CDfetched" : component.get('v.CDfetched'),
                    "data" : component.get('v.data'),
                    "columns" : component.get('v.columns'),
                    "borrowerTy" : component.get('v.borrowerTy'),
                    "LaNum" : component.get('v.LaNum'),
                    "isEditableLA" : component.get('v.isEditableLA'),
                    "QDEMode" : component.get('v.QDEMode'),
                    "calledFromVFPage" : component.get('v.calledFromVFPage'),
                    "errorMessage" : component.get('v.errorMessage'),
                    "showErrorMessage" :component.get('v.showErrorMessage'),
                    "LoanConId" : component.get('v.LoanConId'),
                    "idTypes" : component.get('v.idTypes'),
                    "oldWrap" : component.get('v.oldWrap'),
                    "oldWrapString" : component.get('v.oldWrapString'),
                    "disableAddButton" : component.get('v.disableAddButton'),
                    "disableEditButton" : component.get('v.disableEditButton')
                }
            );
            evt.fire();  
         } else {
             $A.get("e.force:closeQuickAction").fire();
             $A.get('e.force:refreshView').fire(); 
         }
    }
    //End of the code Added by Vaishali Mehta for BRE
})