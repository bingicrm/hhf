({
	myAction : function(component, event, helper) {
		
	},
    openSingleFile: function(cmp, event, helper) {
		$A.get('e.lightning:openFiles').fire({
		    recordIds: ['00Pp0000009N8dV'],
            
		});
	}, 
	openMultipleFiles: function(cmp, event, helper) {
		$A.get('e.lightning:openFiles').fire({
		    recordIds: ['069p0000001E6ag', '069p0000001EGqZ', '069p0000001EGqU'], 
		    selectedRecordId: '069p0000001E6ag'
		});
	}, 
	handleOpenFiles: function(cmp, event, helper) {
		//alert('Opening files: ' + event.getParam('recordIds').join(', ') 
			//+ ', selected file is ' + event.getParam('selectedRecordId')); 
	}
})