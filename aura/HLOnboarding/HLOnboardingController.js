({
	doInit : function(component, event, helper) {
        //alert('Ongoing development.');
        var allowtoOnboard = component.get("c.checkDisbursementEligibility");
        allowtoOnboard.setParams({loanAppId : component.get("v.recordId")});
        allowtoOnboard.setCallback(this,function(responseFromController) {
            var stateResult = responseFromController.getState();
        	console.log('Repayment Status'+stateResult);
        	console.log('Response received'+responseFromController.getReturnValue());
            if(responseFromController.getReturnValue() == 'true') { //Modified by Saumya for Insurance Loan
                var checkRepaymentStatus = component.get("c.checkRepaymentEligibility");
                checkRepaymentStatus.setParams({loanAppId : component.get("v.recordId")});
                checkRepaymentStatus.setCallback(this,function(responseStatus) {
                    var stateResult = responseStatus.getState();
                    console.log('Repayment Status'+stateResult);
                    console.log('Response received'+responseStatus.getReturnValue());
                    if(responseStatus.getReturnValue()) {
                        component.set("v.repaymentDetailsValid",responseStatus.getReturnValue());
                        var action = component.get("c.onboardCallout");
                        console.log('AAAA'+component.get("v.recordId"));
                        action.setParams({ loanAppId : component.get("v.recordId") });
                        action.setCallback(this, function(response) {
                            var state = response.getState();
                            console.log('state'+state);
                            console.log('Response rec'+response.getReturnValue());
                            console.log('Response'+JSON.stringify(response.getReturnValue()));
                            component.set("v.singleRec",response.getReturnValue());
                            
                        });
                        $A.enqueueAction(action);
                    }
                    else {
                        
                        var cmpTarget = component.find('Modalbox');
                        var cmpBack = component.find('Modalbackdrop');
                        //alert(cmpTarget);
                        //alert(cmpBack);
                        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
                        
                        var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Error!",
                                "type": "Error",
                                "message": "Error: Repayment Schedule must be drawn on current Date before boarding the Loan. Please generate Repayment Schedule first."
                            });
                            toastEvent.fire();
                        component.set("v.repaymentDetailsValid",responseStatus.getReturnValue());
                    }
                });
             $A.enqueueAction(checkRepaymentStatus);
            }
            
			else if(responseFromController.getReturnValue() == 'Insurance Loan'){ //Added by Saumya for Insurance Loan
                component.set("v.repaymentDetailsValid",false);
                $A.get("e.force:closeQuickAction").fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "Error",
                    "message": "Please On board Parent Loan Application before moving Insurance Loan to disbursment."
                });
                toastEvent.fire();
                
                
            }
			
			//Below else if block added by Abhilekh on 23rd October 2019 for Post Sanction FCU BRD
            else if(responseFromController.getReturnValue() == 'FD plus FD'){
                component.set("v.repaymentDetailsValid",false);
                $A.get("e.force:closeQuickAction").fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "Error",
                    "message": "Please move the Loan Application to Disbursement Maker as it is FD Decline."
                });
                toastEvent.fire();
            }
            
            //Below else if block added by Abhilekh on 23rd October 2019 for Post Sanction FCU BRD
            else if(responseFromController.getReturnValue() == 'CNV'){
                component.set("v.repaymentDetailsValid",false);
                $A.get("e.force:closeQuickAction").fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "Error",
                    "message": "Please move the Loan Application to Disbursement Maker for CNV FCU Approval."
                });
                toastEvent.fire();
            }
            
            //Below else if block added by Abhilekh on 23rd October 2019 for Post Sanction FCU BRD
            else if(responseFromController.getReturnValue() == 'RTC'){
                component.set("v.repaymentDetailsValid",false);
                $A.get("e.force:closeQuickAction").fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "Error",
                    "message": "Please move the Loan Application to Disbursement Maker for RTC FCU Approval."
                });
                toastEvent.fire();
            }
            
            else {
                component.set("v.repaymentDetailsValid",false);
                $A.get("e.force:closeQuickAction").fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "Error",
                    "message": "Error: Verify disbursement details before Loan Onboarding."
                });
                toastEvent.fire();
                
                
            }
        });
        $A.enqueueAction(allowtoOnboard);
        
	},
    closeModalDoc:function(component,event,helper){   
        //alert('Close');
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    }
})