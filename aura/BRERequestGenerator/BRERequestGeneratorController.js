({
	doInit : function(component, event, helper) {
		
		component.set('v.showOptions',true);
		var action = component.get("c.getMessage");
            
            action.setParams({
                "loanApplicationID" : component.get('v.recordId')
            });
            action.setCallback(this, function(result){
                var state = result.getState();
                if (component.isValid() && state === "SUCCESS"){ 
                    component.set('v.message',result.getReturnValue());
                }
            });
            $A.enqueueAction(action);
		//helper.showSpinner(component, event);
		/*var action = component.get("c.createIntegration");
            
            action.setParams({
                "loanApplicationID" : component.get('v.recordId')
            });
            action.setCallback(this, function(result){
                var state = result.getState();
                if (component.isValid() && state === "SUCCESS"){ 
                    if(result.getReturnValue().includes('Error')) {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                        "type": "Error",
                        "title": "Error!",
                        "message": result.getReturnValue()
                        });
                        toastEvent.fire();
                        component.set('v.showOptions',false);
                    } 
                } else {
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                        "type": "Error",
                        "title": "Error!",
                        "message": 'Error occurred while creating CI, Please contact system administrator'
                        });
                        toastEvent.fire();
                        component.set('v.showOptions',false);
                }
            });
             $A.enqueueAction(action);
             */
             
	},
	handleYes: function(component, event, helper) {
		component.set('v.message','Generating Request');
		component.set('v.showOptions',false);
		helper.showSpinner(component, event);
		var action = component.get("c.generateRequest");
            
            action.setParams({
                "loanApplicationID" : component.get('v.recordId')
            });
            action.setCallback(this, function(result){
                var state = result.getState();
                if (component.isValid() && state === "SUCCESS"){ 
                    if(result.getReturnValue().includes('Error')) {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                        "type": "Error",
                        "title": "Error!",
                        "message": result.getReturnValue()
                        });
                        toastEvent.fire();
                    } else {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                        "type": "Success",
                        "title": "Success!",
                        "message": "Successful"
                        });
                        toastEvent.fire();
                        window.location.reload(); 
                    }
                }
                helper.hideSpinner(component, event);
                 $A.get("e.force:closeQuickAction").fire();
            });
             $A.enqueueAction(action);
	},
	handleNo: function(component, event, helper) {
	    $A.get("e.force:closeQuickAction").fire();
	}
})