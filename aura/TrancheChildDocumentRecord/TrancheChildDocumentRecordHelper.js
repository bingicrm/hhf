({
    MAX_FILE_SIZE: 9500000, //Max file size 4.5 MB 
    CHUNK_SIZE: 750000,      //Chunk Max size 750Kb 
    
      // Fetch the documents from the Apex controller
      getDocumentList: function(component,event,helper) {
        var action = component.get('c.getDocuments');
        // Set up the callback
        var self = this;
        action.setCallback(this, function(actionResult) {
         component.set('v.documents', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
      },

    uploadHelper: function(component, event) {
        var fileInput = event.getSource().get("v.files");
       var file = fileInput[0];
        var self = this;
        if (file.size > self.MAX_FILE_SIZE) {
            component.set("v.showLoadingSpinner", false);
            component.set("v.fileName", 'Alert : File size cannot exceed ' + self.MAX_FILE_SIZE + ' bytes.\n' + ' Selected file size: ' + file.size);
            return;
        }
        

        var objFileReader = new FileReader();
        objFileReader.onload = $A.getCallback(function() {
            var fileContents = objFileReader.result;
            var base64 = 'base64,';
            var dataStart = fileContents.indexOf(base64) + base64.length;

            fileContents = fileContents.substring(dataStart);
            // call the uploadProcess method 
            self.uploadProcess(component, file, fileContents);
        });

        objFileReader.readAsDataURL(file);
    },

    uploadProcess: function(component, file, fileContents) {
        var startPosition = 0;
        var endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);
        this.uploadInChunk(component, file, fileContents, startPosition, endPosition, '');
    },

     uploadInChunk: function(component, file, fileContents, startPosition, endPosition, attachId) {
        // call the apex method 'saveChunk'
        var getchunk = fileContents.substring(startPosition, endPosition);
         
         var spinner = component.find("spinner");
        $A.util.removeClass(spinner, 'slds-hide');
        $A.util.addClass(spinner, 'slds-show');
         
        var action = component.get("c.saveChunk");
        action.setParams({
            parentId: component.get("v.singleRec.Id"),
            fileName: file.name,
            base64Data: encodeURIComponent(getchunk),
            contentType: file.type,
            fileId: attachId
        });

 
        // set call back 
        action.setCallback(this, function(response) {
            // store the response / Attachment Id   
            attachId = response.getReturnValue();
            var state = response.getState();
            //console.log('==state=='+state);
            if (state === "SUCCESS") {
                // update the start position with end postion
                startPosition = endPosition;
                endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);
               
                if (startPosition < endPosition) {
                    this.uploadInChunk(component, file, fileContents, startPosition, endPosition, attachId);
                     //alert('here1');
                } else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": "success",
                        "message": "Your File is Uploaded Successfully."
                    });
                    toastEvent.fire();
                     //alert('here');
    
                }
                
                var createEvent = component.getEvent("refreshView");
                 //alert('here11');
        		createEvent.fire();
                
                // handel the response errors        
            } else if (state === "INCOMPLETE") {
                var error = response.getReturnValue();
                var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "Error",
                        "message": "Internal Server Error."
                    });
                    toastEvent.fire();
                
               // alert("From server: " + response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "Error",
                        "message": "Server Error:"+ errors
                    });
                    toastEvent.fire();
                    console.log("Error message: " + errors[0].message);
                    } 
                } else {
                    console.log("Unknown Error");
                }
            }
        var spinner = component.find("spinner");
        $A.util.removeClass(spinner, 'slds-show');
        $A.util.addClass(spinner, 'slds-hide');  
        });
        // enqueue the action 
        $A.enqueueAction(action);
    },
   // fetch picklist values dynamic from apex controller 
    docStatusPickListVal: function(component, event,helper) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.objInfoForPicklistValues"),
            "fld": 'Status__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            //alert(response.getState());
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
 
                component.set("v.docStatus", allValues);
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchPickListVal: function(component, fieldName, picklistOptsAttributeName) {
        var substage = component.get("v.subStage");
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.objInfoForPicklistValues"),
            "fld": fieldName,
            "subStage": substage
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
 
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.set("v." + picklistOptsAttributeName, opts);
            }
        });
        $A.enqueueAction(action);
    },
})