({
	doInit: function(component, event, helper) {
      // call the fetchPickListVal(component, field_API_Name, aura_attribute_name_for_store_options) -
      // method for get picklist values dynamic 
      
      	var capturedResponse = component.get("v.subStage");
      	if(capturedResponse != 'Tranche File Check' && capturedResponse != 'Tranche Scan Checker' ) {
            component.set("v.showforOtherStages", true);
        }
        helper.fetchPickListVal(component, 'Status__c','docPicklistOpts');
		helper.fetchPickListVal(component, 'Document_Collection_Mode__c','docCollectionModePicklistOpts');
       // alert();
    },
    editFileCheck : function(component,event,helper) {
		component.set("v.showSaveCancelBtn",true);
	},
    editDataEntryReq : function(component,event,helper) {
		component.set("v.showSaveCancelBtn",true);
	},
	editScanCheck : function(component,event,helper) {
		component.set("v.showSaveCancelBtn",true);
	},
	editScanCheckCompleted : function(component,event,helper) {
		component.set("v.showSaveCancelBtn",true);
	},
    inlineEditStatus : function(component,event,helper){   
        // show the rating edit field popup 
        component.set("v.photocopyEditMode", true); 
        // after set ratingEditMode true, set picklist options to picklist field 
        component.find("accRating").set("v.options" , component.get("v.docPicklistOpts"));
        // after the 100 millisecond set focus to input field   
        setTimeout(function(){ 
            component.find("accRating").focus();
        }, 100);
    },
    
    onphotocopyChange : function(component,event,helper){ 
        // if picklist value change,
        // then show save and cancel button by set attribute to true
        component.set("v.showSaveCancelBtn",true);
    },     
    
    closePhotocopyBox : function (component, event, helper) {
       // on focus out, close the input section by setting the 'ratingEditMode' att. as false
        component.set("v.photocopyEditMode", false); 
    },
    
    inlineEditdocStatus : function(component,event,helper){   
        // show the rating edit field popup 
        component.set("v.docStatusEditMode", true); 
        // after set ratingEditMode true, set picklist options to picklist field 
        //component.find("accRating").set("v.options" , component.get("v.ratingPicklistOpts"));
        // after the 100 millisecond set focus to input field   
        setTimeout(function(){ 
            component.find("statusDoc").focus();
        }, 100);
    },
    
    ondocStatusChange : function(component,event,helper){ 
        // if picklist value change,
        // then show save and cancel button by set attribute to true
        component.set("v.showSaveCancelBtn",true);
    },     
    
    closedocStatusBox : function (component, event, helper) {
       // on focus out, close the input section by setting the 'ratingEditMode' att. as false
        component.set("v.docStatusEditMode", false); 
    },
    
    handleFilesChange: function(component, event, helper) {
            var fileName = 'No File Selected.';
            if (event.getSource().get("v.files").length > 0) {
                fileName = event.getSource().get("v.files")[0]['name'];
                //alert('fileName: '+fileName);
                helper.uploadHelper(component, event);
            }  
            component.set("v.fileName", fileName);
     },
     EditName : function(component,event,helper){  
        component.set("v.nameEditMode", true);
         
            component.set("v.showSaveCancelBtn",true);
        
        /*setTimeout(function(){
            component.find("inputId").focus();
        }, 100);*/
    },
    inlineEditRating : function(component,event,helper){
        component.set("v.StatusEditMode", true);
        component.set("v.showSaveCancelBtn",true);
        component.find("inputId").set("v.options",component.get("v.docPicklistOpts"));
    },
	inlineEditDocCollectionStatus : function(component,event,helper){
        component.set("v.DocumentCollectionEditMode", true);
        component.set("v.showSaveCancelBtn",true);
        component.find("docCollectionModeID").set("v.options",component.get("v.docCollectionModePicklistOpts"));
    },
    Editbutton : function(component,event,helper){  
        var subStage = component.get("v.subStage");
        //alert(subStage);
        component.set("v.StatusEditMode", true);
        if(subStage != "Tranche File Check" && subStage != 'Tranche Scan Checker') {
            component.find("inputId").set("v.options",component.get("v.docPicklistOpts"));
        }
        component.set("v.nameEditMode", true);
        component.set("v.showSaveCancelBtn",true);
    },
    closeNameBox : function (component, event, helper) {
        component.set("v.nameEditMode", false);
        if(event.getSource().get("v.value") == ''){
            component.set("v.showErrorClass",true);
        }else{
            component.set("v.showErrorClass",false);
        }
        
    },
    closeStatusBox : function (component, event, helper) {
        component.set("v.StatusEditMode", false);
    },
	closeStatusBoxDocCollection : function (component, event, helper) {
        component.set("v.DocumentCollectionEditMode", false);
    },
    changevalue : function (component, event, helper) {
       var lookupval = component.find("inputlookup").get("v.value");
       console.log('lookupval--> '+lookupval);
       component.find("inputfield").set("v.value",lookupval);
    },
    handleComponentEvent: function(component, event, helper) {
      var messageId = event.getParam("message");
        component.set("v.IdFromLookup",messageId);
        //alert('test...'+messageId);
         var lookupval = component.find("inputlookup").get("v.value");
       console.log('lookupval--> '+lookupval);
       component.find("inputfield").set("v.value",messageId);
     
    },
    closeModalDoc:function(component,event,helper){   
        //alert('Close');
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    openmodalDoc: function(component,event,helper) {
        //alert('Open');
        /*var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); */
        var propertyId = component.get("v.singleRec.Id");
        //component.set("v.files", []);
        if (!propertyId) {
            return;
        }
        var action = component.get("c.queryAttachments");
        action.setParams({
            "propertyId": propertyId,
        });
        action.setCallback(this, function (response) {
            var files = response.getReturnValue();
            console.log(files);
            if(files != null) {
                var cusrecordIds = files.join();
                console.log('recordIds'+cusrecordIds);
                $A.get('e.lightning:openFiles').fire({
                    //recordIds: ['069p0000001E6ag'],
                    recordIds: files,
                });
            }
            else {
                var cmpTarget = component.find('Modalbox');
                var cmpBack = component.find('Modalbackdrop');
                $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                $A.util.addClass(cmpBack, 'slds-backdrop--open');
            }
            //component.set("v.files", files);
        });
        $A.enqueueAction(action);
        
        
    }
})