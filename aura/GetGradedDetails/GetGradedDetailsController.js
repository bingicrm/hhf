({
	doInit: function(component, event, helper) 
    {      
        var gradedDetails = component.get("v.gradedDetails");
        gradedDetails.push({
            'sobjectType': 'Graded_Details__c',
            'Slab_and_EMI_Seq_No__c  ': 1,
            'Slab_and_EMI_EMI__c ': '',
            'Slab_and_EMI_Instl_From__c ': '',
            'Slab_and_EMI_Instl_To__c ': '',
            'Slab_and_EMI_Recovery__c ':''
        });
        // set the updated list to attribute (contactList) again    
        component.set("v.gradedDetails", gradedDetails);
        
    },
    addNewRow: function(component, event, helper) 
    {
        var gradedDetails = component.get("v.gradedDetails");
        
        gradedDetails.push({
            'sobjectType': 'Graded_Details__c',
            'Slab_and_EMI_Seq_No__c  ': gradedDetails.length+1,
            'Slab_and_EMI_EMI__c ': '',
            'Slab_and_EMI_Instl_From__c ': '',
            'Slab_and_EMI_Instl_To__c ': '',
            'Slab_and_EMI_Recovery__c ':''
        });
        // set the updated list to attribute (contactList) again    
        component.set("v.gradedDetails", gradedDetails);
    },
    removeDeletedRow: function(component, event, helper) 
    {
        var index = event.getParam("indexVar");
        // get the all List (contactList attribute) and remove the Object Element Using splice method    
        var AllRowsList = component.get("v.gradedDetails");
        AllRowsList.splice(index, 1);
        // set the contactList after remove selected row element  
        component.set("v.gradedDetails", AllRowsList);
    }
    
})