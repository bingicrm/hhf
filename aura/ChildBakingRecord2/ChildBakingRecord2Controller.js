({
 doValidityCheck : function(component, event){
     var f1 = component.find("v.field1");
     if(f1=null|| f1==undefined){
        //alert('fill mandatries');
        return false;
     }else {
          return true;
     }
   
  } ,
    ChildCheckValidityMethod : function(component, event, helper) {        
        component.find('fieldId').reduce(function (validSoFar, inputField) {
            inputField.showHelpMessageIfInvalid();           
        },component.find('fieldId')[0] );
    },   
    
    //Added by Abhilekh for BRE2 Enhancements
    verifyFifthBal: function(component, event, helper){
        var inp = event.getSource();
        var val=inp.get('v.value');
        var rec = component.get("v.singleRec");
        var substage = component.get("v.substage");
        
        if(rec.Balance_on_5_At_Data_Entry__c < val && (substage === 'Credit Review' || substage === 'Re Credit' || substage === 'Re-Look')){
            inp.setCustomValidity(rec.Balance_on_5_At_Data_Entry__c+" is the limit.");
            inp.reportValidity();
        }
        else{
            inp.setCustomValidity('');
            inp.reportValidity();
        }
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    verifyFifteenthBal: function(component, event, helper){
        var inp = event.getSource();
        var val=inp.get('v.value');
        var rec = component.get("v.singleRec");
        var substage = component.get("v.substage");
        
        if(rec.Balance_on_15_At_Data_Entry__c < val && (substage === 'Credit Review' || substage === 'Re Credit' || substage === 'Re-Look')){
            inp.setCustomValidity(rec.Balance_on_15_At_Data_Entry__c+" is the limit.");
            inp.reportValidity();
        }
        else{
            inp.setCustomValidity('');
            inp.reportValidity();
        }
    },
    
    //Added by Abhilekh for BRE2 Enhancements
    verifyTwentyFifthBal: function(component, event, helper){
        var inp = event.getSource();
        var val=inp.get('v.value');
        var rec = component.get("v.singleRec");
        var substage = component.get("v.substage");
        
        if(rec.Balance_on_25_At_Data_Entry__c < val && (substage === 'Credit Review' || substage === 'Re Credit' || substage === 'Re-Look')){
            inp.setCustomValidity(rec.Balance_on_25_At_Data_Entry__c+" is the limit.");
            inp.reportValidity();
        }
        else{
            inp.setCustomValidity('');
            inp.reportValidity();
        }
    }
})