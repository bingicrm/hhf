({ 
        doInit: function(component, event, helper)  
    	{   
             
            component.set('v.hitCIBILAgain',false);
            helper.callCIBIL(component);
        },   
        callCIBILagain: function(component, event, helper)  
    	{      
           	component.set('v.hitCIBILAgain',true);
            helper.callCIBIL(component);
        },
    cancelCreation : function(component, event, helper) {
        //Added By Vaishali Mehta for BRE
        var calledFromLAComponent = component.get('v.fromLAComponent');
    
         console.log('fromLAComponent: Yes method '+calledFromLAComponent);
         if(calledFromLAComponent === true) {
            console.log('laval'+ component.get('v.laVal'));
            console.log('Hi PAN Success');
            var wrap = [];
            wrap = component.get('v.CDWrap');
            var index = component.get('v.index');
            console.log('wrap'+wrap);
            console.log('index'+index);
            wrap[index].PANVerified = true;
            console.log('wrap'+wrap);
            component.set('v.CDWrap',wrap);
        
            var evt = $A.get("e.c:callCreateLAEvent");
            evt.setParams(
                { 
                    "laVal" : component.get('v.laVal'),
                    "CDWrap" : component.get('v.CDWrap'),
                   "lstBorrowerType" : component.get('v.lstBorrowerType'),
                    "lstconstitution" : component.get('v.lstconstitution'),
                    "lstCustSegment" : component.get('v.lstCustSegment'),
                    "loanAppID" : component.get('v.loanAppID'),
                    "prod" : component.get('v.prod'),
                    "localPol" : component.get('v.localPol'),
                    "customer" : component.get('v.customer'),
                    "isDisplay" : component.get('v.isDisplay') ,
                    "isEditable" : component.get('v.isEditable'),
                    "isInserted" : component.get('v.isInserted'),
                    "isDisable" : component.get('v.isDisable'),
                    "addAddress" : component.get('v.addAddress'),
                    "CDfetched" : component.get('v.CDfetched'),
                    "data" : component.get('v.data'),
                    "columns" : component.get('v.columns'),
                    "borrowerTy" : component.get('v.borrowerTy'),
                    "LaNum" : component.get('v.LaNum'),
                    "isEditableLA" : component.get('v.isEditableLA'),
                    "QDEMode" : component.get('v.QDEMode'),
                    "calledFromVFPage" : component.get('v.calledFromVFPage'),
                    "errorMessage" : component.get('v.errorMessage'),
                    "showErrorMessage" :component.get('v.showErrorMessage'),
                    "LoanConId" : component.get('v.LoanConId'),
                    "idTypes" : component.get('v.idTypes'),
                    "oldWrap" : component.get('v.oldWrap'),
                    "oldWrapString" : component.get('v.oldWrapString'),
                    "disableAddButton" : component.get('v.disableAddButton'),
                    "disableEditButton" : component.get('v.disableEditButton')
                    
                }
            );
            evt.fire();  
         } else {
             // End of code added by Vaishali 
			$A.get("e.force:closeQuickAction").fire();
		}	
    },
	//Added By Vaishali Mehta for BRE
    close :function(component,event,helper){ 
        var calledFromLAComponent = component.get('v.fromLAComponent');
    
         console.log('fromLAComponent: Yes method '+calledFromLAComponent);
         if(calledFromLAComponent === true) {
            var evt = $A.get("e.c:callCreateLAEvent");
            evt.setParams(
                { 
                    "laVal" : component.get('v.laVal'),
                    "CDWrap" : component.get('v.CDWrap'),
                   "lstBorrowerType" : component.get('v.lstBorrowerType'),
                    "lstconstitution" : component.get('v.lstconstitution'),
                    "lstCustSegment" : component.get('v.lstCustSegment'),
                    "loanAppID" : component.get('v.loanAppID'),
                    "prod" : component.get('v.prod'),
                    "localPol" : component.get('v.localPol'),
                    "customer" : component.get('v.customer'),
                    "isDisplay" : component.get('v.isDisplay') ,
                    "isEditable" : component.get('v.isEditable'),
                    "isInserted" : component.get('v.isInserted'),
                    "isDisable" : component.get('v.isDisable'),
                    "addAddress" : component.get('v.addAddress'),
                    "CDfetched" : component.get('v.CDfetched'),
                    "data" : component.get('v.data'),
                    "columns" : component.get('v.columns'),
                    "borrowerTy" : component.get('v.borrowerTy'),
                    "LaNum" : component.get('v.LaNum'),
                    "isEditableLA" : component.get('v.isEditableLA'),
                    "QDEMode" : component.get('v.QDEMode'),
                    "calledFromVFPage" : component.get('v.calledFromVFPage'),
                    "errorMessage" : component.get('v.errorMessage'),
                    "showErrorMessage" :component.get('v.showErrorMessage'),
                    "LoanConId" : component.get('v.LoanConId'),
                    "idTypes" : component.get('v.idTypes'),
                    "oldWrap" : component.get('v.oldWrap'),
                    "oldWrapString" : component.get('v.oldWrapString'),
                    "disableAddButton" : component.get('v.disableAddButton'),
                    "disableEditButton" : component.get('v.disableEditButton')
                    
                }
            );
            evt.fire(); 
         } else{
             $A.get("e.force:closeQuickAction").fire();
         }
    }
	// End of code Added By Vaishali Mehta for BRE
})