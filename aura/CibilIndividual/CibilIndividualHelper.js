({
	callCIBIL : function(component, event) {
		var act = component.get("c.createCustomerIntegration");
                act.setParams({ loanContactId : component.get("v.recordId"),
                                hitCIBILAgain : component.get("v.hitCIBILAgain")
                              });   
                act.setCallback(this, function(response) 
                {
                    var state = response.getState(); 
                    var resultsToast = $A.get("e.force:showToast");
                    if (state === "SUCCESS") 
                    {
                        console.log(response.getReturnValue());
                        if(response.getReturnValue().failure)
                        {
                            component.set("v.recordIntegrationId",response.getReturnValue().respString);
                            var action = component.get("c.integrateCibil");
                            action.setParams({ 
                                "loanContactId": component.get("v.recordId"),
                                "customerIntegrationId": component.get("v.recordIntegrationId")
                            });
                            
                            action.setCallback(this, function(response) {
                                
                                var state = response.getState(); 
                                if (state === "SUCCESS") 
                                {
                                	if(response.getReturnValue().failure)
                                    {
                                         // Added by Vaishali for BRE
                                        var calledFromVFPage = component.get('v.calledFromVFPage');
                                        var fromLAComponent = component.get('v.fromLAComponent');
                                        if(fromLAComponent == false || fromLAComponent == true && calledFromVFPage == false){
                                            console.log('Hi');
                                            var showToast = $A.get("e.force:showToast"); 
                                            console.log('showToast '+showToast);
                                            showToast.setParams({ 
                                                'type': 'SUCCESS',
                                                'message' : 'Cibil Request Submitted'
                                            }); 
                                            showToast.fire(); 
                                        } else {
                                            component.set('v.showErrorMessage' , true);
                                            component.set('v.errorMessage','Cibil Request Submitted');
                                        }
                                        var calledFromLAComponent = component.get('v.fromLAComponent');
                                        var calledFromVFPage = component.get('v.calledFromVFPage'); 
                                         console.log('fromLAComponent: Yes method '+calledFromLAComponent);
                                         if(calledFromLAComponent === true) {
                                            console.log('laval'+ component.get('v.laVal'));
                                            console.log('Hi CIBIL Success');
                                            //var wrap = [];
                                            //wrap = component.get('v.CDWrap');
                                            //var index = component.get('v.index');
                                            //console.log('wrap'+wrap);
                                            //console.log('index'+index);
                                            //wrap[index].CIBILVerified = true;
                                            //console.log('wrap'+wrap);
                                            //component.set('v.CDWrap',wrap);
                                        
                                            var evt = $A.get("e.c:callCreateLAEvent");
                                            evt.setParams(
                                                { 
                                                    "customerDetail": component.get('v.customerDetail') ,
                                                    "laVal" : component.get('v.laVal'),
                                                    "CDWrap" : component.get('v.CDWrap'),
                                                    "lstBorrowerType" : component.get('v.lstBorrowerType'),
                                                    "lstconstitution" : component.get('v.lstconstitution'),
                                                    "lstCustSegment" : component.get('v.lstCustSegment'),
                                                    "loanAppID" : component.get('v.loanAppID'),
                                                    "prod" : component.get('v.prod'),
                                                    "localPol" : component.get('v.localPol'),
                                                    "customer" : component.get('v.customer'),
                                                    "isDisplay" : component.get('v.isDisplay') ,
                                                    "isEditable" : component.get('v.isEditable'),
                                                    "isInserted" : component.get('v.isInserted'),
                                                    "isDisable" : component.get('v.isDisable'),
                                                    "addAddress" : component.get('v.addAddress'),
                                                    "CDfetched" : component.get('v.CDfetched'),
                                                    "data" : component.get('v.data'),
                                                    "columns" : component.get('v.columns'),
                                                    "borrowerTy" : component.get('v.borrowerTy'),
                                                    "LaNum" : component.get('v.LaNum'),
                                                    "isEditableLA" : component.get('v.isEditableLA'),
                                                    "QDEMode" : component.get('v.QDEMode'),
                                                    "calledFromVFPage" : component.get('v.calledFromVFPage'),
                                                    "errorMessage" : component.get('v.errorMessage'),
                                                    "showErrorMessage" :component.get('v.showErrorMessage'),
                                                    "LoanConId" : component.get('v.LoanConId'),
                                                    "idTypes" : component.get('v.idTypes'),
                                                    "oldWrap" : component.get('v.oldWrap'),
                                                    "oldWrapString" : component.get('v.oldWrapString'),
                                                    "disableAddButton" : component.get('v.disableAddButton'),
                                                    "disableEditButton" : component.get('v.disableEditButton')
                                                }
                                            );
                                            evt.fire(); 
                                         } else {
                                             // End of the code added by Vaishali 	
											$A.get("e.force:closeQuickAction").fire();
										}	
                                    }
                                    else
                                    {
                                        component.set('v.message',response.getReturnValue().respString);
                                        if(component.get("v.showYesNo") === true){
                                        component.set("v.showYesNo",false);
                                        }
                                    }
                                }
                                else if (state === "INCOMPLETE") {
                                    component.set('v.message','Some error occured');
                                }
                                else if (state === "ERROR") {
                                   component.set('v.message','Some error occured');
                                }
                            });
                            $A.enqueueAction(action);
                        }
                        else
                        {
                            component.set('v.message',response.getReturnValue().respString);
                            if ( response.getReturnValue().respString.includes('Are you sure you want to initiate again') ){
                                            component.set('v.showYesNo',true);
                            }
                            else
                                component.set('v.showYesNo',false);
                        }
						//Commented this line by Vaishali for BRE
                        //$A.get('e.force:refreshView').fire();  
                           
                    }
                   
                });
                $A.enqueueAction(act);
	}
})