({
    doInit : function(component, event, helper){
        
        component.set("v.url", window.location.origin);
        helper.getLoanDetails(component, event); 
        helper.docStatusPickListVal(component, event);
        helper.getDeviations(component, event);
        helper.getConditions(component, event);
        helper.getObligations(component, event);
        //obligationsLists
        //toogle open
        //
        
        
        
    },
    Close : function(component, event, helper) {
        
        if(component.get("v.isEditInsurance") == false && component.get("v.isEditProcessing") == false && component.get("v.isEditAppcon") == false && component.get("v.isEditApplicant") == false && component.get("v.isEditApplican") == false && component.get("v.isEditApp") == false && component.get("v.isEditObligations") == false && component.get("v.isEditDeviations") == false && component.get("v.isEditConditions") == false && component.get("v.isEditExtra") == false){
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            dismissActionPanel.fire();  
            $A.get("e.force:closeQuickAction").fire();
        }
        else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Warning!",
                "message": "Please complete the work on the screen before closing",
                "type" : "error"
            });
            toastEvent.fire();
            
        }
        
    },
    panelOne : function(component, event, helper) {
        helper.toggleAction(component, event, 'panelOne');
    },
    
    panelTwo : function(component, event, helper) {
        helper.toggleAction(component, event, 'panelTwo');
    },
    
    panelThree : function(component, event, helper) {
        helper.toggleAction(component, event, 'panelThree');
    },
    
    panelFour : function(component, event, helper) {
        helper.toggleAction(component, event, 'panelFour');
    },
    
    panelFive : function(component, event, helper) {
        helper.toggleAction(component, event, 'panelFive');
    },
    panelSix : function(component, event, helper) {
        helper.toggleAction(component, event, 'panelSix');
    },
    panelSeven : function(component, event, helper) {
        helper.toggleAction(component, event, 'panelSeven');
    },
    panelEight : function(component, event, helper) {
        helper.toggleAction(component, event, 'panelEight');
    },
    panelNine : function(component, event, helper) {
        helper.toggleAction(component, event, 'panelNine');
    },
    panelTen : function(component, event, helper) {
        helper.toggleAction(component, event, 'panelTen');
    },
    panelEleven : function(component, event, helper) {
        helper.toggleAction(component, event, 'panelEleven');
    },
    panelTwelve : function(component, event, helper) {
        helper.toggleAction(component, event, 'panelTwelve');
    },
    panelThirteen : function(component, event, helper) {
        helper.toggleAction(component, event, 'panelThirteen');
    },
    panelFourteen : function(component, event, helper) {
        helper.toggleAction(component, event, 'panelFourteen');
    },
    panelFiveteen : function(component, event, helper) {
        helper.toggleAction(component, event, 'panelFiveteen');
    },
    panelSixteen : function(component, event, helper) {
        helper.toggleAction(component, event, 'panelSixteen');
    },
    panelSeventeen : function(component, event, helper) {
        helper.toggleAction(component, event, 'panelSeventeen');
    },
    panelEighteen : function(component, event, helper) {
        helper.toggleAction(component, event, 'panelEighteen');
    },
    isEditInsurance : function(component, event, helper) {
        
        component.set("v.isEditInsurance",true);
    },
    isEditDetailss : function(component, event, helper) {
        
        component.set("v.isEditDetails",true);
    },
    isEditThirds : function(component, event, helper) {
        
        component.set("v.isEditThird",true);
    },
    cancelTheRecord : function(component, event, helper) {
        
        var msg = event.getSource().get('v.value');
        console.log(msg); 
        component.set("v.cancelRecord",msg);
        console.log('aim'+component.get("v.cancelRecord"));
        component.set("v.isOpen",true);
        
    },
    cancelTheWholePage : function(component, event, helper) {
        
        var msg = event.getSource().get('v.value');
        console.log(msg); 
        component.set("v.cancelRecord",msg);
        console.log('aim'+component.get("v.cancelRecord"));
        if(component.get("v.isEditInsurance") == false && component.get("v.isEditProcessing") == false && component.get("v.isEditAppcon") == false && component.get("v.isEditApplicant") == false && component.get("v.isEditApplican") == false && component.get("v.isEditApp") == false && component.get("v.isEditObligations") == false && component.get("v.isEditDeviations") == false && component.get("v.isEditConditions") == false && component.get("v.isEditExtra") == false && component.get("v.isEditDetails") == false && component.get("v.isEditProperty") == false && component.get("v.isEditRemarks") == false && component.get("v.isEditThird") == false){
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            dismissActionPanel.fire();  
            $A.get("e.force:closeQuickAction").fire();
            $A.get('e.force:refreshView').fire();  
            window.location.reload();
            $A.enqueueAction(action); 
        }
        else{
            component.set("v.isOpen",true);
            
        }
        
        
    },
    cancelInsurance : function(component, event, helper) {
        
        helper.getLoanDetails(component, event); 
        component.set("v.isOpen",false); 
        if(component.get("v.cancelRecord") == 1){
            component.set("v.isEditInsurance",false);
        } 
        else if(component.get("v.cancelRecord") == 2){
            component.set("v.isEditProcessing",false);
        } 
            else if(component.get("v.cancelRecord") == 3){
                component.set("v.isEditAppcon",false);
            }
                else if(component.get("v.cancelRecord") == 4){
                    component.set("v.isEditApplicant",false);
                } 
                    else if(component.get("v.cancelRecord") == 5){
                        component.set("v.isEditApplican",false);
                    } 
                        else if(component.get("v.cancelRecord") == 6){
                            component.set("v.isEditApp",false);
                        } 
                            else if(component.get("v.cancelRecord") == 7){
                                component.set("v.isEditObligations",false);
                            }
                                else if(component.get("v.cancelRecord") == 8){
                                    helper.getDeviations(component, event);
                                    component.set("v.isEditDeviations",false);
                                }
                                    else if(component.get("v.cancelRecord") == 9){
                                        helper.getConditions(component, event);
                                        component.set("v.isEditConditions",false);
                                    }
                                        else if(component.get("v.cancelRecord") == 10){
                                            component.set("v.isEditExtra",false);
                                        }
                                            else if(component.get("v.cancelRecord") == 11){
                                                component.set("v.isEditDetails",false);
                                            }
                                                else if(component.get("v.cancelRecord") == 12){
                                                    component.set("v.isEditProperty",false);
                                                }
                                                    else if(component.get("v.cancelRecord") == 13){
                                                        component.set("v.isEditRemarks",false);
                                                    }
                                                        else if(component.get("v.cancelRecord") == 14){
                                                            component.set("v.isEditThird",false);
                                                        }
                                                            else if(component.get("v.cancelRecord") == 15){
                                                                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                                                                dismissActionPanel.fire();  
                                                                $A.get("e.force:closeQuickAction").fire();
                                                                $A.get('e.force:refreshView').fire();  
                                                                window.location.reload();
                                                                $A.enqueueAction(action);
                                                            }
        
    },
    stayInsurance : function(component, event, helper) {
        
        component.set("v.isOpen",false);
    },
    isEditProcessing : function(component, event, helper) {
        
        component.set("v.isEditProcessing",true);
    },
    
    isEditApplicants : function(component, event, helper) {
        
        component.set("v.isEditApplicant",true);
    },
    
    isEditAppl : function(component, event, helper) {
        
        component.set("v.isEditApp",true);
    },
    isEditDeviation : function(component, event, helper) {
        
        component.set("v.isEditDeviations",true);
    },
    isEditCondition : function(component, event, helper) {
        
        component.set("v.isEditConditions",true);
    },
    isEditExtraa : function(component, event, helper) {
        
        component.set("v.isEditExtra",true);
    },
    isEditApplicans : function(component, event, helper) {
        
        component.set("v.isEditApplican",true);
    },
    isEditAppcons : function(component, event, helper) {
        
        component.set("v.isEditAppcon",true);
    },
    isEditObligationss : function(component, event, helper) {
        
        component.set("v.isEditObligations",true);
    },
    isEditPropertys : function(component, event, helper) {
        
        component.set("v.isEditProperty",true);
    },
    isEditRemarkss : function(component, event, helper) {
        
        component.set("v.isEditRemarks",true);
    },
    
    saveTheRecord : function(component, event, helper) {
        
        var msg = event.getSource().get('v.value');
        console.log(msg);
        component.set("v.saveIndividual",msg);
        
        
        component.set('v.loaded', !component.get('v.loaded'));
        helper.saveInsurance(component, event);
        /*
        component.set("v.isEditInsurance",false);
        component.set("v.isEditProcessing",false);
        component.set("v.isEditApplicant",false);
        component.set("v.isEditApp",false);
        component.set("v.isEditExtra",false);
        component.set("v.isEditApplican",false);
        component.set("v.isEditAppcon",false);
        */
    },
    saveTheDeviations : function(component, event, helper) {
        
        component.set('v.loaded', !component.get('v.loaded'));
        helper.saveDeviations(component, event, helper);
        //component.set("v.isEditDeviations",false);
        
    },
    saveTheObligations : function(component, event, helper) {
        
        component.set('v.loaded', !component.get('v.loaded'));
        helper.saveObligations(component, event, helper);
        //component.set("v.isEditObligations",false);
        
    },
    saveTheConditions : function(component, event, helper) {
        
        component.set('v.loaded', !component.get('v.loaded'));
        helper.saveConditions(component, event, helper);
        //component.set("v.isEditConditions",false);
        
    },
    
    delete : function(component, event,helper) {
    
    component.set('v.loaded', !component.get('v.loaded'));
    var action = component.get("c.deleteDeviation");
    action.setParams({
    'AccountId':event.target.id,
    'recordId' : component.get("v.recordId")
});
action.setCallback(this, function(response) {
    //component.set("v.deviationList",response.getReturnValue());
    component.set('v.loaded', !component.get('v.loaded'));
    if(response.getReturnValue() == 'Success'){
        helper.getDeviations(component, event);    
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "Authority Approval Norm is deleted successfully.",//Modified by Saumya for Applicable Deviation Label Change
            "type" : "success"
        });
        toastEvent.fire();
    }
    else{
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error!",
            "message": response.getReturnValue(),
            "type" : "error"
        });
        toastEvent.fire();    
        
    }
});
$A.enqueueAction(action);
//console.log('event.target.id '+event.target.id);
},
    addRowDeviations : function(component, event) {
        
        var action = component.get("c.addRowDeviation");
        action.setParams({
            'recordId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            component.set("v.deviationList",response.getReturnValue());
            component.set("v.isEditDeviations",true);
        });
        $A.enqueueAction(action);
    },
        
        addRowConditions : function(component, event) {
            
            console.log('1');
            var action = component.get("c.addRowConditionss");
            action.setParams({
                'recordId' : component.get("v.recordId")
            });
            action.setCallback(this, function(response) {
                console.log('2');
                component.set("v.conditionList",response.getReturnValue());
                component.set("v.isEditConditions",true);
            });
            $A.enqueueAction(action);
            
        },
            addRowObligations : function(component, event) {
                
                console.log('1');
                var action = component.get("c.addRowObligationss");
                action.setParams({
                    'recordId' : component.get("v.recordId")
                });
                action.setCallback(this, function(response) {
                    console.log('2');
                    component.set("v.obligationList",response.getReturnValue());
                    component.set("v.isEditObligations",true);
                });
                $A.enqueueAction(action);
                
            },
                
                deleteConditions : function(component, event) {
                    var action = component.get("c.deleteCondition");
                    action.setParams({
                        'AccountId':event.target.id,
                        'recordId' : component.get("v.recordId")
                    });
                    action.setCallback(this, function(response) {
                        component.set("v.conditionList",response.getReturnValue());
                    });
                    $A.enqueueAction(action);
                },
                    deleteObligations : function(component, event) {
                        var action = component.get("c.deleteObligation");
                        action.setParams({
                            'AccountId':event.target.id,
                            'recordId' : component.get("v.recordId")
                        });
                        action.setCallback(this, function(response) {
                            component.set("v.obligationList",response.getReturnValue());
                        });
                        $A.enqueueAction(action);
                    },
})