({
    toggleAction : function(component, event, secId) {
        var acc = component.find(secId);
        for(var cmp in acc) {
            $A.util.toggleClass(acc[cmp], 'slds-show');  
            $A.util.toggleClass(acc[cmp], 'slds-hide');  
        }
    },
    
    getLoanDetails : function(component, event){
        
        component.set('v.loaded', !component.get('v.loaded'));        
        var action = component.get("c.getloandetails");
        console.log('sharad1');
        action.setParams({
            'recordId' : component.get("v.recordId")
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                component.set('v.loaded', !component.get('v.loaded'));
                console.log('sharad2');
                component.set("v.lstRcds",result.getReturnValue());
                console.log('sharad'+ component.get("v.lstRcds.property"));
            }
        });
        $A.enqueueAction(action);
        
        
    },
    getDeviations : function(component, event){
        
        var action = component.get("c.deviationLists");
        console.log('sharad1');
        action.setParams({
            'recordId' : component.get("v.recordId")
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                
                console.log('sharad2 getDeviations '+JSON.stringify(result.getReturnValue()));
                component.set("v.deviationList",result.getReturnValue());
                //console.log('sharad'+ component.get("v.lstRcds.property"));
            }
        });
        $A.enqueueAction(action);
        
        
    },
    getConditions : function(component, event){
        
        var action = component.get("c.conditionLists");
        console.log('sharad1');
        action.setParams({
            'recordId' : component.get("v.recordId")
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                
                console.log('sharad2');
                component.set("v.conditionList",result.getReturnValue());
                //console.log('sharad'+ component.get("v.lstRcds.property"));
            }
        });
        $A.enqueueAction(action);
        
        
    },
    getObligations : function(component, event){
        
        var action = component.get("c.obligationsLists");
        console.log('sharad1');
        action.setParams({
            'recordId' : component.get("v.recordId")
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                
                console.log('sharad2');
                component.set("v.obligationList",result.getReturnValue());
                //console.log('sharad'+ component.get("v.lstRcds.property"));
            }
        });
        $A.enqueueAction(action);
        
        
    },
    saveInsurance : function(component, event){
        
        var action = component.get("c.saveInsuranceDetial");
        console.log('sharadss1');
        action.setParams({
            "loanlist" : component.get('v.lstRcds'),
            'type' : component.get("v.saveIndividual")
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                component.set('v.loaded', !component.get('v.loaded'));
                console.log('sharadss2');
                var allValues = result.getReturnValue();
                console.log('allValues simooo '+allValues);
                if(allValues == 'Success'){
                    this.getLoanDetails(component, event);
                    
                    if(component.get("v.saveIndividual") == 1){
                        component.set("v.isEditInsurance",false);
                    } 
                    else if(component.get("v.saveIndividual") == 2){
                        component.set("v.isEditProcessing",false);
                    } 
                        else if(component.get("v.saveIndividual") == 4){
                            component.set("v.isEditApplicant",false);
                        }
                            else if(component.get("v.saveIndividual") == 5){
                                component.set("v.isEditApplican",false);
                            }
                                else if(component.get("v.saveIndividual") == 6){
                                    component.set("v.isEditApp",false);
                                } 
                                    else if(component.get("v.saveIndividual") == 10){
                                        component.set("v.isEditExtra",false);
                                    }
                                        else if(component.get("v.saveIndividual") == 11){
                                            component.set("v.isEditDetails",false);
                                        }
                                            else if(component.get("v.saveIndividual") == 12){
                                                component.set("v.isEditProperty",false);
                                            }
                                                else if(component.get("v.saveIndividual") == 13){
                                                    component.set("v.isEditRemarks",false);
                                                }
                                                    else if(component.get("v.saveIndividual") == 14){
                                                        component.set("v.isEditThird",false);
                                                    }
                    
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Changes have been saved successfully!",
                        "type" : "success"
                    });
                    toastEvent.fire();
                    
                }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": allValues,
                        "type" : "error"
                    });
                    toastEvent.fire();
                }
                
                //console.log('sharad'+ component.get("c.loandetails"));
            }
            else{
                component.set('v.loaded', !component.get('v.loaded'));
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": result.getReturnValue()+' '+state,
                    "type" : "error"
                });
                toastEvent.fire();
                
            }
        });
        $A.enqueueAction(action);
        
        
    },
    saveDeviations : function(component, event, helper){
        
        var action = component.get("c.saveDeviationListl");
        
        console.log('SIZE OFLIST '+component.get('v.deviationList').length);
        var abc = JSON.stringify(component.get('v.deviationList'));
        console.log('DEV LIst '+JSON.stringify(abc));
        var i;
        for (i = 0; i < (component.get('v.deviationList').length) * 3; i++) {
            if(abc.includes('[]')) {
                abc = abc.replace('[]', '"null" ');
                //abc = abc.replace('[]', '"null" ');
                //abc = abc.replace('[]', '"null" ');
            }
        }
        console.log('DEV LIst '+JSON.stringify(abc));
        action.setParams({
            "loanlist1" : abc,
            'recordId' : component.get("v.recordId")
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                component.set('v.loaded', !component.get('v.loaded'));
                
                console.log('sharad2');
                var allValues = result.getReturnValue();
                console.log('allValues simooo '+allValues);
                if(allValues == 'Success'){
                    
                    this.getDeviations(component, event);
                    component.set("v.isEditDeviations",false);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Changes have been saved successfully!",
                        "type" : "success"
                    });
                    toastEvent.fire();
                    
                }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": allValues,
                        "type" : "error"
                    });
                    toastEvent.fire();
                    
                }
                //console.log('sharad2'+allValues);
                //console.log('sharad'+ component.get("c.loandetails"));
            }
        });
        $A.enqueueAction(action);
        
        
    },
    
    saveConditions : function(component, event, helper){
        
        var action = component.get("c.saveConditionListl");
        var conditionListString = JSON.stringify(component.get('v.conditionList'));	
        console.log('sharad1'+component.get('v.conditionList'));
        action.setParams({
            "loanlist1" : conditionListString,
            'recordId' : component.get("v.recordId")
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                component.set('v.loaded', !component.get('v.loaded'));
                console.log('sharad2');
                var allValues = result.getReturnValue();
                console.log('allValues simooo '+allValues);
                if(allValues == 'Success'){
                    this.getConditions(component, event);
                    component.set("v.isEditConditions",false);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Changes have been saved successfully!",
                        "type" : "success"
                    });
                    toastEvent.fire();
                    
                }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": allValues,
                        "type" : "error"
                    });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
        
        
    },
    saveObligations : function(component, event, helper){
        
        var action = component.get("c.saveObligationsss");
        console.log('sharad1'+component.get('v.conditionList'));
        action.setParams({
            "loanlist" : component.get('v.obligationList'),
            'recordId' : component.get("v.recordId")
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                component.set('v.loaded', !component.get('v.loaded'));
                console.log('sharad2');
                var allValues = result.getReturnValue();
                console.log('allValues simooo '+allValues);
                if(allValues == 'Success'){
                    
                    this.getObligations(component, event);
                    component.set("v.isEditObligations",false);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Changes have been saved successfully!",
                        "type" : "success"
                    });
                    toastEvent.fire();
                    
                }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": allValues,
                        "type" : "error"
                    });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
        
        
    },
    
    docStatusPickListVal: function(component, event) {
        
        console.log('sid3');
        var action = component.get("c.getselectOptions");
        console.log('sid1');
        action.setParams({
            "objObject": component.get("v.objInfoForPicklistValues"),
            "fld": 'Type__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            //alert(response.getState());
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log('sid2');
                component.set("v.docStatus", allValues);
            }
        });
        $A.enqueueAction(action);
    }
})