({
	onAccept : function(component, event, helper) {
		 var loanApp =  component.get("v.recordId") ;
        console.log('===loanApp=='+loanApp);
       
        var action = component.get('c.setOwner');
        action.setParams({
           
            "oppId": loanApp 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                console.log('==result===' + actionResult.getReturnValue());
                if(actionResult.getReturnValue() == null){
                     var errorMess= 'You are not authorized to accept the application. Kindly contact the system admin.'
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                    
                        'type': 'ERROR',
                'message' : errorMess
                        }); 
                showToast.fire();   
                }
              
            }
            $A.get("e.force:closeQuickAction").fire();
            $A.get('e.force:refreshView').fire(); 
        });
        console.log('-------Hello----------');
        $A.enqueueAction(action);
        console.log('-------Hello 2----------');
    
      },
       showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }
})