({
	getOtherIncome : function(component, event, helper) {
        helper.doInit(component, event, helper);
        helper.doInitisedit(component, event, helper);
        helper.doInitcustdetail(component, event, helper);
    },
    save : function(component, event, helper) {
        var isChecked = component.find("isChecked").get("v.value");
         var allValid = component.find('fieldId').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        
        if (allValid || !isChecked) {//|| isChecked
        var action = component.get("c.SaveOtherIncome");
        var OtherIncomelist = component.get("v.CustDetails");
        var custid = component.get("v.Customerid");
        //console.log('hey ash'+OtherIncomelist.Rental_Income_Current_Year__c);
        action.setParams({
            "OtherIncDetails":OtherIncomelist,
            'conId':custid
        });
        action.setCallback(this, function(response){          
            var state = response.getState();
            if(state === "SUCCESS"){
                component.set("v.iseditable",false);
	            console.log("response.getReturnValue()"+response.getReturnValue());
                helper.doInit(component, event, helper);//update the value back on page
            }else if(response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('in error'+errors[0].message);
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
            //alert('All form entries look valid. Ready to submit!');
        }else {
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": "Please complete missing information."
                });
                toastEvent.fire();

        	//alert('Please update the invalid form entries and try again.');
        }
	},
    edit : function(component, event, helper) {
		component.set("v.iseditable",true);
	},
    
    handleConfirmDialogbox : function(component, event, helper) {
        component.set('v.showConfirmDialogbox', true);
    },
    handleConfirmDialogboxYes : function(component, event, helper) {
        helper.closeMe(component, event, helper);
    },
    handleConfirmDialogboxNo : function(component, event, helper) {
        component.set('v.showConfirmDialogbox', false);
    },
      // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
     
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    }
})