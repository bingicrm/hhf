({
    helperMethod : function(component, event, helper) {
        var custDetid = component.get("v.Customerid");
        var action = component.get("c.fetchAIP");//get data from controller
        action.setParams({
            'key':custDetid
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
               // alert('success');
                //console.log('In success');
                //console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                component.set("v.AIPInputs", response.getReturnValue());     
                var Apcheck= component.get("v.AIPInputs");
               // console.log('aip'+JSON.stringify(Apcheck));
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
        var action2 = component.get("c.sendMonths");
        action2.setParams({
            'key':custDetid
        });
        action2.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
               // alert('success');
                //console.log('In success');
                //console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                component.set("v.months", response.getReturnValue());   
                var check= component.get("v.months");
                //console.log("v.months"+JSON.stringify(check));
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });   
        $A.enqueueAction(action2);
    },
    doSave : function(component, event, helper) {
        
        var action = component.get("c.saveAIP");
        var AIPtoIU = component.get("v.AIPInputs");
        action.setParams({
             "AIPtoUpdateInsert":AIPtoIU,
              "custid" : component.get("v.Customerid")
        });
        action.setCallback(this, function(response){          
            var state = response.getState();
            if(state === "SUCCESS"){
                  component.set("v.iseditable", false);
                  helper.helperMethod(component, event, helper);
            }else if(response.getState() === "ERROR"){
                  var errors = action.getError();
                  if (errors) {
                     if (errors[0] && errors[0].message) {
                            console.log('in error'+errors[0].message);
                            component.set("v.message", errors[0].message);
                     }
                 }
            }
        });
        $A.enqueueAction(action);
    },
    doInitisedit : function(component, event, helper) {
		
        var custid = component.get("v.Customerid");
        var action = component.get("c.GetIseditable");
        action.setParams({
            'conId':custid
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                //console.log("response.getReturnValue()"+response.getReturnValue()[0].Loan_Type__c);
                
                component.set("v.iseditable", response.getReturnValue());     
                component.set("v.showEdit", response.getReturnValue());         
            
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
		
	},
    getCustomer : function(component, event, helper) {
        var custDetid = component.get("v.Customerid");
        var action = component.get("c.sendCustomerDetails");//get data from controller
        action.setParams({
            'key':custDetid
        });
           action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){

                component.set("v.CustDetails", response.getReturnValue());     
                var Apcheck= component.get("v.CustDetails");
                //console.log('cust details'+JSON.stringify(Apcheck));
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    closeMe : function(comp, event, helper)  { 
        var custid = comp.get("v.Customerid");
        var recordIdNumber = '';
        var testLoanAppId = comp.get("v.loanAppId");
        
        var evt = $A.get("e.c:callCreateCamScreenEvent");
        evt.setParams({
            "recordIdNumber" : custid,
            "isOpen" : true,
            "testLoanAppId" : testLoanAppId
        });
        evt.fire();
	}
})