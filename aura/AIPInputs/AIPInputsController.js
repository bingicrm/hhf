({
    myAction : function(component, event, helper) {
        
        console.log('In aip init Handler ');
        helper.helperMethod(component, event, helper);
        helper.getCustomer(component, event, helper);
        helper.doInitisedit(component, event, helper);
    },
    saveFunction : function(component, event, helper) {
        var ischecked = component.find("isChecked").get("v.value");
        //console.log('test'+ ischecked);
        if (ischecked === false || ischecked == '' || ischecked == undefined) {//|| isChecked    
          helper.doSave(component, event, helper);
        } else if(ischecked){
                var allValid = component.find('fieldId').reduce(function (validSoFar, inputCmp) {
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid;
                }, true);
            if(allValid){
                 helper.doSave(component, event, helper);
            }
        }
        else {
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": "Please complete missing information."
                });
                toastEvent.fire();
        }
    },
    
    edit : function(component, event, helper) {
		component.set("v.iseditable",true);
	},
    
    isRefreshed: function(component, event, helper) {
        location.reload();
       
        var check= component.get("v.iseditable");
        console.log('check last'+check);
    }, 
    handleConfirmDialogbox : function(component, event, helper) {
        component.set('v.showConfirmDialogbox', true);
    },
    handleConfirmDialogboxYes : function(component, event, helper) {
        helper.closeMe(component, event, helper);
    },
    handleConfirmDialogboxNo : function(component, event, helper) {
        component.set('v.showConfirmDialogbox', false);
    },
       // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
     
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    }
})