({
	onAccept : function(component, event, helper) {
		var objProj =  component.get("v.recordId") ;
        console.log('===objProj=='+objProj);
       
        var action = component.get('c.setOwner');
        action.setParams({
           
            "projectId": objProj 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                console.log('==result===' + actionResult.getReturnValue());
                if(actionResult.getReturnValue() == null){
                     var errorMess= 'You are not authorized to accept the Project. Kindly contact the system admin.'
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                    
                        'type': 'ERROR',
                'message' : errorMess
                        }); 
                showToast.fire();   
                }
              location.reload();
            }
             $A.get("e.force:closeQuickAction").fire();
            $A.get('e.force:refreshView').fire(); 
           
            
        });
        console.log('-------Hello----------');
        $A.enqueueAction(action);
        console.log('-------Hello 2----------');
	},
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   	},
    
 	// this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }
})