({
    reassignOriginalOwner : function(cmp, event, helper) {
        var action = cmp.get('c.reassignOriginalOwner');
        var caseId = cmp.get("v.recordId");
        action.setParams({
     caseId: cmp.get("v.recordId")
});
        console.log('HELPER CALLED'+action);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
             dismissActionPanel.fire();
                $A.get('e.force:refreshView').fire();
            }
            else if (state === "INCOMPLETE") {
            }
               else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
})