({   

doInit : function(component, event, helper) {       

// Call the Apex Class Method from Javascript       
 var recordId = component.get("v.recordId");
var action = component.get("c.SanctionLetter");       
                  
// Place the Action in the Queue    
action.setParams({
        "recordId":recordId
    });
action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                
                if ( response.getReturnValue() == "true" ) {
                    
                	component.set("v.message", "Sanction form generated successfully !! <br/> Redirecting to Loan Application... ");
                    setTimeout(function() { var urlEvent = $A.get("e.force:navigateToURL");
                                           if (window.location.pathname.includes('hhfllos')) {
                                           		urlEvent.setParams({
                                                "url": "/hhfllos/s/detail/" + recordId
                                                });
                                           } else {
                                               urlEvent.setParams({
                                                "url": "/" + recordId
                                                });
                                           }
                                                urlEvent.fire();
                                              
                                              }, 1000);
                } else if (response.getReturnValue() == "false") {
                    component.set("v.message", "Sanction form not generated. Please contact System Administrator");
                }
		//Below else if block added by Abhilekh on 11th October 2019 for FCU BRD
                else if(response.getReturnValue() == "Sancion letter cannot be generated because this application is FD Decline"){
                    component.set("v.message","Sancion letter cannot be generated because this application is FD Decline.");
                }
		//Below else if block added by Abhilekh on 24th January 2020 for Hunter BRD
                else if(response.getReturnValue() == "Sancion letter cannot be generated as Hunter analysis has not been recommended. Please reject or assign the application to Hunter Manager."){
                	component.set("v.message","Sancion letter cannot be generated as Hunter analysis has not been recommended. Please reject or assign the application to Hunter Manager.");
                }	
		 else if (response.getReturnValue() == "repayment"){
					component.set("v.message", "Please generate repayment by clicking the button below. ");
                    component.set("v.showYesNo", true);                
                }
				/** Added by saumya for Insurance Loan **/

                else if (response.getReturnValue() == "Insurance Loan Application"){
					component.set("v.message", "This action is not valid for Insurance Loan. ");
                    component.set("v.showYesNo", false);                
                }
                /** Added by saumya for Insurance Loan **/
				/** Added by saumya for Physical Documents TIL 1150**/

                else if (response.getReturnValue() == "Physical Documents not received"){
					component.set("v.message", "Physical Cheque is mandatory for application sourced through DSA before performing this action.");
                    component.set("v.showYesNo", false);                
                }
                /** Added by saumya for Physical Documents TIL 1150**/
            } else {
                console.log("Failed with state: " + state);
                component.set("v.message", "Sanction form not generated due to some internal error. Please contact System Administrator");
            }
        });    
$A.enqueueAction(action);   

},
    
    cancelCreation : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
 
 	callRepayment : function(component, event, helper) {
		         helper.showSpinner(component);//Added by Saumya  For Loading Spinner

        var recordId = component.get("v.recordId");
		var action = component.get("c.generateRepayment"); 
        action.setParams({
        	"idLoanApp":recordId
    	});
    	action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                setTimeout(function() {
                   component.set("v.message", "Repayment Generated Successfully, please generate sanction letter again");   
                   component.set("v.showYesNo", false);	
                },4000);
            } else {
              component.set("v.message", "Some internal error occurred in repayment generation. Please contact system admin.");   
            }
    	});    
		$A.enqueueAction(action);   


         setTimeout(function() {
         helper.hideSpinner(component);//Added by Saumya  For Loading Spinner
    
         },20000);		
	}
})