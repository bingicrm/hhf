({
    getFinDet1 : function(component, event, helper) {
        var action = component.get("c.getFinDet1");
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.data1", response.getReturnValue());
            }
        });
        //var growthP = (("v.data1.Share_Capital_CY__c" - "v.data1.Share_Capital_CY_1__c")/"v.data1.Share_Capital_CY_1__c")*100;
        //component.set("v.growth", growthP);
        //alert(growthP);
        $A.enqueueAction(action);
    },
    getFinDet2 : function(component, event, helper) {
        var action = component.get("c.getFinDet2");
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.data2", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    getFinDet3 : function(component, event, helper) {
        var action = component.get("c.getFinDet3");
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.data3", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    getFinDet4 : function(component, event, helper) {
        var action = component.get("c.getFinDet4");
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.data4", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },

    
    saveDataTable1 : function(component, event, helper) {
        var editedRecords =  component.find("financialDetailSheet1").get("v.draftValues");
        var totalRecordEdited = editedRecords.length;
        var action = component.get("c.updateFinDets");
        action.setParams({
            'editedCFList' : editedRecords
        });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //if update is successful
                if(response.getReturnValue() === true){
                    helper.showToast({
                        "title": "Record Update",
                        "type": "success",
                        "message": totalRecordEdited+" Financial Detail(s) updated."
                    });
                    console.log('1111');                    
                    //$A.get('e.force:refreshView').fire();
                    
                    helper.reloadDataTable();
                    console.log("Event Fired");
                } else{ //if update got failed
                    helper.showToast({
                        "title": "Error!!",
                        "type": "error",
                        "message": "Error in update"
                    });
                    //$A.get('e.force:refreshView').fire();
                    helper.reloadDataTable();
                }
                //$A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    saveDataTable2 : function(component, event, helper) {
        var editedRecords =  component.find("financialDetailSheet2").get("v.draftValues");
        var totalRecordEdited = editedRecords.length;
        var action = component.get("c.updateFinDets");
        action.setParams({
            'editedCFList' : editedRecords
        });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //if update is successful
                if(response.getReturnValue() === true){
                    helper.showToast({
                        "title": "Record Update",
                        "type": "success",
                        "message": totalRecordEdited+" Financial Detail(s) updated."
                    });
                    console.log('2222');
                    //$A.get('e.force:refreshView').fire();
                    helper.reloadDataTable();
                } else{ //if update got failed
                    helper.showToast({
                        "title": "Error!!",
                        "type": "error",
                        "message": "Error in update"
                    });
                    //$A.get('e.force:refreshView').fire();
                    helper.reloadDataTable();
                }
            }
        });
        $A.enqueueAction(action);
    },
    saveDataTable3 : function(component, event, helper) {
        var editedRecords =  component.find("financialDetailSheet3").get("v.draftValues");
        var totalRecordEdited = editedRecords.length;
        var action = component.get("c.updateFinDets");
        action.setParams({
            'editedCFList' : editedRecords
        });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //if update is successful
                if(response.getReturnValue() === true){
                    helper.showToast({
                        "title": "Record Update",
                        "type": "success",
                        "message": totalRecordEdited+" Financial Detail(s) updated."
                    });
                    console.log('2222');
                    //$A.get('e.force:refreshView').fire();
                    helper.reloadDataTable();
                } else{ //if update got failed
                    helper.showToast({
                        "title": "Error!!",
                        "type": "error",
                        "message": "Error in update"
                    });
                    //$A.get('e.force:refreshView').fire();
                    helper.reloadDataTable();
                }
            }
        });
        $A.enqueueAction(action);
    },
    saveDataTable4 : function(component, event, helper) {
        var editedRecords =  component.find("financialDetailSheet4").get("v.draftValues");
        var totalRecordEdited = editedRecords.length;
        var action = component.get("c.updateFinDets");
        action.setParams({
            'editedCFList' : editedRecords
        });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //if update is successful
                if(response.getReturnValue() === true){
                    helper.showToast({
                        "title": "Record Update",
                        "type": "success",
                        "message": totalRecordEdited+" Financial Detail(s) updated."
                    });
                    console.log('2222');
                    //$A.get('e.force:refreshView').fire();
                    helper.reloadDataTable();
                } else{ //if update got failed
                    helper.showToast({
                        "title": "Error!!",
                        "type": "error",
                        "message": "Error in update"
                    });
                    //$A.get('e.force:refreshView').fire();
                    helper.reloadDataTable();
                }
            }
        });
        $A.enqueueAction(action);
    },

    
    showToast : function(params){
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(params);
            toastEvent.fire();
        } else{
            alert(params.message);
        }
    },

    /*
     * reload data table
     * */
    reloadDataTable : function(component,event,helper){
    var refreshEvent = $A.get('e.force:refreshView');
        console.log('3333'+refreshEvent);
        if(refreshEvent){
            console.log('4444');
            refreshEvent.fire();
            console.log('Refresh Event Fired');
        }
    },
    
    insertNewRecord : function(component,event,helper){
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Financial_Details__c"
            
        });
        createRecordEvent.fire();
        
    },
    saveRecordData : function(component, event, helper){
        //var saveRecordEvent = $A.get("e.force:saveRecord");
        var action = component.get("c.updateFinDets");
        action.setParams({
            "fdata1": component.get("v.data1")
        });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //if update is successful
                if(response.getReturnValue() === true){
                    helper.showToast({
                        "title": "Record(s) Updated",
                        "type": "success",
                        "message":"Financial Detail(s) updated."
                    });
                    console.log('2222');
                    //$A.get('e.force:refreshView').fire();
                    helper.reloadDataTable();
                } else{ //if update got failed
                    helper.showToast({
                        "title": "Error!!",
                        "type": "error",
                        "message": "Error in update"
                    });
                    //$A.get('e.force:refreshView').fire();
                    helper.reloadDataTable();
                }
            }
        });
        $A.enqueueAction(action);
        //saveRecordEvent.fire();
    },
})