({
    
doInit : function(component, event, helper) {        
        component.set('v.sampleShare',[{label: 'Share Capital CY', fieldName: 'Share_Capital_CY__c', editable:'true', type: 'currency'}]);
    	component.set('v.columns1', [
            {label: 'Name', fieldName: 'Name', editable:'false', type: 'text'},
            {label: 'Share Capital CY', fieldName: 'Share_Capital_CY__c', editable:'true', type: 'currency'},
            {label: 'Share Capital CY-1', fieldName: 'Share_Capital_CY_1__c', editable:'true', type: 'currency'},
            {label: 'Share Capital CY-2', fieldName: 'Share_Capital_CY_2__c', editable:'true', type: 'currency'},
            {label: 'Share Capital Total', fieldName: 'Share_Capital_Total__c', editable:'false', type: 'currency'},
            {label: 'Reserves CY', fieldName: 'Reserves_CY__c', editable:'true', type: 'currency'},
            {label: 'Reserves CY-1', fieldName: 'Reserves_CY_1__c', editable:'true', type: 'currency'},
            {label: 'Reserves CY-2', fieldName: 'Reserves_CY_2__c', editable:'true', type: 'currency'},
            {label: 'Reserves Total', fieldName: 'Reserves_Total__c', editable:'false', type: 'currency'},
            {label: 'Liabilities CY', fieldName: 'Liabilities_CY__c', editable:'true', type: 'currency'},
            {label: 'Liabilities CY-1', fieldName: 'Liabilities_CY_1__c', editable:'true', type: 'currency'},
            {label: 'Liabilities CY-2', fieldName: 'Liabilities_CY_2__c', editable:'true', type: 'currency'},
            {label: 'Liabilities Total', fieldName: 'Liabilities_Total__c', editable:'false', type: 'currency'}
        ]);
    	component.set('v.columns2', [
            {label: 'Name', fieldName: 'Name', editable:'false', type: 'text'},
            {label: 'Fixed Assets CY', fieldName: 'Fixed_Assets_CY__c', editable:'true', type: 'currency'},
            {label: 'Fixed Assets CY-1', fieldName: 'Fixed_Assets_CY_1__c', editable:'true', type: 'currency'},
            {label: 'Fixed Assets CY-2', fieldName: 'Fixed_Assets_CY_2__c', editable:'true', type: 'currency'},
            {label: 'Fixed Assets Total', fieldName: 'Fixed_Assets_Total__c', editable:'false', type: 'currency'},
            {label: 'Current Assets CY', fieldName: 'Current_Assets_CY__c', editable:'true', type: 'currency'},
            {label: 'Current Assets CY-1', fieldName: 'Current_Assets_CY_1__c', editable:'true', type: 'currency'},
            {label: 'Current Assets CY-2', fieldName: 'Current_Assets_CY_2__c', editable:'true', type: 'currency'},
            {label: 'Current Assets Total', fieldName: 'Current_Assets_Total__c', editable:'false', type: 'currency'},
            {label: 'Debtors CY', fieldName: 'Debtors_CY__c', editable:'true', type: 'currency'},
            {label: 'Debtors CY-1', fieldName: 'Debtors_CY_1__c', editable:'true', type: 'currency'},
            {label: 'Debtors CY-2', fieldName: 'Debtors_CY_2__c', editable:'true', type: 'currency'},
            {label: 'Debtors Total', fieldName: 'Debtors_Total__c', editable:'false', type: 'currency'}
        ]);
    	component.set('v.columns3', [
            {label: 'Name', fieldName: 'Name', editable:'false', type: 'text'},
            {label: 'Direct Expenses CY', fieldName: 'Direct_Expenses_CY__c', editable:'true', type: 'currency'},
            {label: 'Direct Expenses CY-1', fieldName: 'Direct_Expenses_CY_1__c', editable:'true', type: 'currency'},
            {label: 'Direct Expenses CY-2', fieldName: 'Direct_Expenses_CY_2__c', editable:'true', type: 'currency'},
            {label: 'Direct Expenses Total', fieldName: 'Direct_Expenses_Total__c', editable:'false', type: 'currency'},
            {label: 'Indirect Expenses CY', fieldName: 'Indirect_Expenses_CY__c', editable:'true', type: 'currency'},
            {label: 'Indirect Expenses CY-1', fieldName: 'Indirect_Expenses_CY_1__c', editable:'true', type: 'currency'},
            {label: 'Indirect Expenses CY-2', fieldName: 'Indirect_Expenses_CY_2__c', editable:'true', type: 'currency'},
            {label: 'Indirect Expenses Total', fieldName: 'Indirect_Expenses_Total__c', editable:'false', type: 'currency'},
            {label: 'Misc Expenses CY', fieldName: 'Misc_Expenses_CY__c', editable:'true', type: 'currency'},
            {label: 'Misc Expenses CY-1', fieldName: 'Misc_Expenses_CY_1__c', editable:'true', type: 'currency'},
            {label: 'Misc Expenses CY-2', fieldName: 'Misc_Expenses_CY_2__c', editable:'true', type: 'currency'},
            {label: 'Misc Expenses Total', fieldName: 'Misc_Expenses_Total__c', editable:'false', type: 'currency'}
        ]);
    	component.set('v.columns4', [
            {label: 'Name', fieldName: 'Name', editable:'false', type: 'text'},
            {label: 'Sales CY', fieldName: 'Sales_CY__c', editable:'true', type: 'currency'},
            {label: 'Sales CY-1', fieldName: 'Sales_CY_1__c', editable:'true', type: 'currency'},
            {label: 'Sales CY-2', fieldName: 'Sales_CY_2__c', editable:'true', type: 'currency'},
            {label: 'Sales Total', fieldName: 'Sales_Total__c', editable:'false', type: 'currency'},
            {label: 'Other Income CY', fieldName: 'Other_Income_CY__c', editable:'true', type: 'currency'},
            {label: 'Other Income CY-1', fieldName: 'Other_Income_CY_1__c', editable:'true', type: 'currency'},
            {label: 'Other Income CY-2', fieldName: 'Other_Income_CY_2__c', editable:'true', type: 'currency'},
            {label: 'Other Income Total', fieldName: 'Other_Income_Total__c', editable:'false', type: 'currency'},
            //{label: 'Total CY', fieldName: 'Total_CY__c', editable:'false', type: 'currency'},
            //{label: 'Total CY-1', fieldName: 'Total_CY_1__c', editable:'false', type: 'currency'},
            //{label: 'Total CY-2', fieldName: 'Total_CY_2__c', editable:'false', type: 'currency'}
        ]);
        helper.getFinDet1(component, helper);
    	helper.getFinDet2(component, helper);
    	helper.getFinDet3(component, helper);
    	helper.getFinDet4(component, helper);
    },

    
    onSave1 : function (component, event, helper) {
        helper.saveDataTable1(component, event, helper); 
    },
    onSave2 : function (component, event, helper) {
        helper.saveDataTable2(component, event, helper);
    },
    onSave3 : function (component, event, helper) {
        helper.saveDataTable3(component, event, helper);
    },
    onSave4 : function (component, event, helper) {
        helper.saveDataTable4(component, event, helper);
    },
    handleNew : function (component, event, helper){
        helper.insertNewRecord(component, event, helper);
    },
    
            saveRecord : function(component, event, helper){
            	helper.saveRecordData(component, event, helper);
            }
})