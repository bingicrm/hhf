({
    initRecords: function(component, event, helper) {
        // call the apex class method and fetch sub stage of loan application record 
        //alert('Modification in progress');
        var action = component.get("c.fetchSubStage");
        action.setParams({
            'loanId': component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            //alert(state);
            if (state === "SUCCESS") {
                var Response = response.getReturnValue();
                console.log('Response received'+JSON.stringify(Response));
															  
                // set SubStage attribute with return value from server.
                var capturedResponse = Response.Sub_Stage__c;
                component.set("v.subStage", Response.Sub_Stage__c);
                component.set("v.InsuranceLoan", Response.Insurance_Loan_Application__c);
                //component.set("v.subStage", "Scan Maker");
                if(capturedResponse != 'Application Initiation' && capturedResponse != 'Scan Maker' && capturedResponse != 'File Check' 
                   && capturedResponse != 'COPS:Data Maker' && capturedResponse != 'COPS:Data Checker' && capturedResponse != 'Scan: Data Maker' 
                   && capturedResponse != 'Scan: Data Checker' && capturedResponse != 'Scan Checker' && capturedResponse != 'Docket Checker' 
                   && capturedResponse != 'Hand Sight' && capturedResponse != 'OTC Receiving' && capturedResponse != 'Customer Negotiation' 
                   && capturedResponse != 'Sales Approval' && capturedResponse != 'Disbursement Maker' && capturedResponse != 'COPS: Credit Operations Entry' 
                   && capturedResponse != 'Disbursement Checker' && capturedResponse !='Document Approval' && capturedResponse != 'CPC Scan Maker' 
                   && capturedResponse != 'CPC Scan Checker') {
                    component.set("v.showforOtherStages", true);
                }
                if(capturedResponse != 'Scan: Data Checker' && capturedResponse != 'COPS: Credit Operations Entry' && capturedResponse != 'Docket Checker' && capturedResponse != 'COPS:Data Maker' && capturedResponse != 'COPS:Data Checker' && capturedResponse != 'Scan Maker' && capturedResponse != 'Hand Sight'  && capturedResponse != 'Scan Checker' && capturedResponse != 'Disbursement Maker'  && capturedResponse != 'Disbursement Checker' ) {
                    component.set("v.showNewButton", true);
                }
            }
        });
        $A.enqueueAction(action);
        
        // call the apex class method and fetch account list  
        var action = component.get("c.fetchDocuments");
        action.setParams({
            'loanId': component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            //alert(state);
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('List received'+storeResponse);
                //component.set("v.DocList", storeResponse);
                console.log(JSON.stringify(storeResponse));
                // set AccountList list with return value from server.
                component.set("v.DocList1", storeResponse);
            }
        });
        $A.enqueueAction(action);
    },
    
    Save : function(component, event, helper) {
        //console.log('DocList'+component.get("v.DocList1")[0].lstDocumentCheckList[0].Id); 
        var actionValidation = component.get("c.validateChkList");
        console.log('Param Sent'+JSON.stringify(component.get("v.DocList1")));
        actionValidation.setParams({
            "paramJSONList": JSON.stringify(component.get("v.DocList1"))
        });
        actionValidation.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS" && component.isValid()) {
                var resultMsg = response.getReturnValue();
                console.log('Result Message'+resultMsg);
                if(resultMsg == "Successful"){
                    var action = component.get("c.saveLoan");
                    console.log('Param Sent'+JSON.stringify(component.get("v.DocList1")));
                    action.setParams({
                        "paramJSONList": JSON.stringify(component.get("v.DocList1"))
                    });
                    action.setCallback(this, function(response) {
                        if (response.getState() === "SUCCESS" && component.isValid()) {
                            var errorString = response.getReturnValue();
                            console.log('Error Msg :'+errorString[0].errorMsg);
                            if( errorString[0].errorMsg != ""){
                                component.set("v.ErrorMsg", errorString[0].errorMsg);
                                component.set("v.showLockedError",true);
                                var cmpTarget = component.find('ModalboxError1');
                                var cmpBack = component.find('ModalbackdropError1');
                                $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                                $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
                                console.log("Boolean"+component.get("v.showLockedError"));
                            }                                                               
                            if( errorString[0].errorMsg == "" ){
                                component.set("v.DocList1", response.getReturnValue());
                                component.set("v.showSaveCancelBtn",false);
                                var calledFromLAComponent = component.get('v.fromLAComponent'); 
                                console.log('fromLAComponent: NO method '+calledFromLAComponent);
                                 if(calledFromLAComponent === true) {
                                    console.log('laval'+ component.get('v.laVal'));
                                    var evt = $A.get("e.c:callCreateLAEvent");
                                    evt.setParams(
                                        { 
                                            "laVal" : component.get('v.laVal'),
                                            "CDWrap" : component.get('v.CDWrap'),
                                            "lstBorrowerType" : component.get('v.lstBorrowerType'),
                                            "lstconstitution" : component.get('v.lstconstitution'),
                                            "lstCustSegment" : component.get('v.lstCustSegment'),
                                            "loanAppID" : component.get('v.loanAppID'),
                                            "prod" : component.get('v.prod'),
                                            "localPol" : component.get('v.localPol'),
                                            "customer" : component.get('v.customer'),
                                            "isDisplay" : component.get('v.isDisplay') ,
                                            "isEditable" : component.get('v.isEditable'),
                                            "isInserted" : component.get('v.isInserted'),
                                            "isDisable" : component.get('v.isDisable'),
                                            "addAddress" : component.get('v.addAddress'),
                                            "CDfetched" : component.get('v.CDfetched'),
                                            "data" : component.get('v.data'),
                                            "columns" : component.get('v.columns'),
                                            "borrowerTy" : component.get('v.borrowerTy'),
                                            "LaNum" : component.get('v.LaNum'),
                                            "isEditableLA" : component.get('v.isEditableLA'),
                                            "QDEMode" : component.get('v.QDEMode'),
                                            "calledFromVFPage" : component.get('v.calledFromVFPage'),
                                            "errorMessage" : component.get('v.errorMessage'),
                                            "showErrorMessage" :component.get('v.showErrorMessage'),
                                            "LoanConId" : component.get('v.LoanConId'),
                                            "idTypes" : component.get('v.idTypes'),
                                            "oldWrap" : component.get('v.oldWrap'),
                                            "oldWrapString" : component.get('v.oldWrapString'),
                                            "disableAddButton" : component.get('v.disableAddButton'),
                                            "disableEditButton" : component.get('v.disableEditButton')
                                        }
                                    );
                                    evt.fire();  
                                 } else { 
                                    window.location.reload(true);
                                 }  
                            }
                        }
                        /*if(response.getReturnValue() != null) {
                                window.location.reload(true);
                            }*/
                        else {
                            //Begin : Added by Vaishali Mehta for BRE
                            var fromLAComponent = component.get('v.fromLAComponent');
                            var calledFromVFPage = component.get('v.calledFromVFPage');
                   
                            if(fromLAComponent == false || fromLAComponent == true && calledFromVFPage == false) {
                            //End : Added by Vaishali Mehta for BRE
								var error = response.getReturnValue();
								var toastEvent = $A.get("e.force:showToast");
								toastEvent.setParams({
									"title": "",
									"type": "Error",
									"message": "Error Occured During Update. Please validate your changes."
								});
								toastEvent.fire();
							//Begin : Added by Vaishali Mehta for BRE    
                            } else {
                                component.set('v.showErrorMessage' , true);
                                component.set('v.errorMessage','Error Occured During Update. Please validate your changes.');
                            }
                            //End : Added by Vaishali Mehta for BRE
                        }
                        
                    });
                    $A.enqueueAction(action);
                }
                else {
					//Begin : Added by Vaishali Mehta for BRE
                    var fromLAComponent = component.get('v.fromLAComponent');
                    var calledFromVFPage = component.get('v.calledFromVFPage');
           
                    if(fromLAComponent == false || fromLAComponent == true && calledFromVFPage == false) {
                    //End : Added by Vaishali Mehta for BRE
						var error = response.getReturnValue();
						var toastEvent = $A.get("e.force:showToast");
						toastEvent.setParams({
							"title": "",
							"type": "Error",
							"message": "Error Occured During Update. Please validate your changes."
						});
						toastEvent.fire();
					//Begin : Added by Vaishali Mehta for BRE    
                    } else {
                        component.set('v.showErrorMessage' , true);
                        component.set('v.errorMessage','Error Occured During Update. Please validate your changes.');
                    }
                    //End : Added by Vaishali Mehta for BRE
					
                    //alert('Cannot Update the Status when there is no file associated with the record.');
                    component.set("v.showSaveCancelBtn",false);
                    component.set("v.showError",true);
                    var cmpTarget = component.find('ModalboxError');
                    var cmpBack = component.find('ModalbackdropError');
                    $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                    $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
                    
                }
            }
        });
        $A.enqueueAction(actionValidation);
    },  
    
    cancel : function(component,event,helper){
        component.set("v.showSaveCancelBtn",false);
        $A.get('e.force:refreshView').fire(); 
    },
    
    createDocRecord : function(component, event, helper){	
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Document_Checklist__c",
            "defaultFieldValues":{
                "Loan_Applications__c" : component.get("v.recordId")
            },
            "panelOnDestroyCallback": function(event) {
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": component.get("v.recordId"),
                    "slideDevName": "related"
                });
                navEvt.fire();
            }
        });
        createRecordEvent.fire();
    },
    
    fetchAllDocuments : function(component, event, helper){
        //alert('In Progress');
        /*
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
        */
        var propertyId = component.get("v.recordId");
        //component.set("v.files", []);
        if (!propertyId) {
            return;
        }
        var action = component.get("c.queryAttachments");
        action.setParams({
            "propertyId": propertyId,
        });
        action.setCallback(this, function (response) {
            var files = response.getReturnValue();
            console.log(files);
            if(files != null) {
                var cusrecordIds = files.join();
                console.log('recordIds'+cusrecordIds);
                $A.get('e.lightning:openFiles').fire({
                    //recordIds: ['069p0000001E6ag'],
                    recordIds: files,
                });
            }
            else {
                var cmpTarget = component.find('Modalbox');
                var cmpBack = component.find('Modalbackdrop');
                $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
            }
            //component.set("v.files", files);
        });
        $A.enqueueAction(action);
    },
    
    closeModalDoc:function(component,event,helper){   
        //alert('Close');
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    
    closeModalDocError:function(component,event,helper){
        component.set("v.showError",false);
        component.set("v.showLockedError",false);
        var cmpTarget = component.find('ModalboxError');
        var cmpBack = component.find('ModalbackdropError');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        
        var cmpTarget1 = component.find('ModalboxError1');
        var cmpBack1 = component.find('ModalbackdropError1');
        $A.util.removeClass(cmpBack1,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget1, 'slds-fade-in-open');
    },
	// Added by Vaishali for BRE
    close :function(component,event,helper){
		console.log('QDEMode in cancel doc'+ component.get('v.QDEMode'));
		var calledFromLAComponent = component.get('v.fromLAComponent');
    
         console.log('fromLAComponent: Yes method '+calledFromLAComponent);
         if(calledFromLAComponent === true) {
            var evt = $A.get("e.c:callCreateLAEvent");
            evt.setParams(
                { 
                    "laVal" : component.get('v.laVal'),
                    "CDWrap" : component.get('v.CDWrap'),
                    "lstBorrowerType" : component.get('v.lstBorrowerType'),
                    "lstconstitution" : component.get('v.lstconstitution'),
                    "lstCustSegment" : component.get('v.lstCustSegment'),
                    "loanAppID" : component.get('v.loanAppID'),
                    "prod" : component.get('v.prod'),
                    "localPol" : component.get('v.localPol'),
                    "customer" : component.get('v.customer'),
                    "isDisplay" : component.get('v.isDisplay') ,
                    "isEditable" : component.get('v.isEditable'),
                    "isInserted" : component.get('v.isInserted'),
                    "isDisable" : component.get('v.isDisable'),
                    "addAddress" : component.get('v.addAddress'),
                    "CDfetched" : component.get('v.CDfetched'),
                    "data" : component.get('v.data'),
                    "columns" : component.get('v.columns'),
                    "borrowerTy" : component.get('v.borrowerTy'),
                    "LaNum" : component.get('v.LaNum'),
                    "isEditableLA" : component.get('v.isEditableLA'),
                    "QDEMode" : component.get('v.QDEMode'),
                    "calledFromVFPage" : component.get('v.calledFromVFPage'),
                    "errorMessage" : component.get('v.errorMessage'),
                    "showErrorMessage" :component.get('v.showErrorMessage'),
                    "LoanConId" : component.get('v.LoanConId'),
                    "idTypes" : component.get('v.idTypes'),
                    "oldWrap" : component.get('v.oldWrap'),
                    "oldWrapString" : component.get('v.oldWrapString'),
                    "disableAddButton" : component.get('v.disableAddButton'),
                    "disableEditButton" : component.get('v.disableEditButton')
                }
            );
            evt.fire();  
         } else {
             $A.get("e.force:closeQuickAction").fire();
         }
    }
    // End of the patch- Added by Vaishali for BRE
})