({
	doInit : function(component, event, helper) {
		var action = component.get("c.getLoanApp");
        action.setParams({"lappId" : component.get("v.recordId")});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                component.set("v.lapp", response.getReturnValue());
            }else{
                console.log('Problem getting Loan Application, response state : '+state);
            }
        });
        $A.enqueueAction(action);
	},
     requestCreation: function(component, event, helper) {
        console.log('1111');
        helper.showSpinner(component);
        var action = component.get("c.propertyTrueOperations");
		var loan = component.get("v.lapp");
         console.log('loan::'+loan);
        action.setParams({
            "la" : loan
        });
        console.log('2222');
        action.setCallback(this, function(actionResult) {
            console.log('3333');
            var state = actionResult.getState();
            if(state === "SUCCESS"){
                console.log('4444');
                var resultsToast = $A.get("e.force:showToast");
                
                if(actionResult.getReturnValue() == 'Error'){
                    resultsToast.setParams({
                    	"title" : "Please provide mandatory property details before proceeding..",
                    	"message" : actionResult.getReturnValue()
                	});
           
            	}
                else if(actionResult.getReturnValue() == 'Initiate All the TPV'){
                    resultsToast.setParams({
                    	"title" : "Error",
                    	"message" : 'Please create all the Third Party Verifications.',
                        "type" : 'error'
                	});
           
            	}
            	else if(actionResult.getReturnValue() == 'Success'){
                	resultsToast.setParams({
                    	"title" : "Records Created",
                    	"message" : "Third Party verification records have been created.",
                        "type" : 'success'
                	});
            	}
                else if(actionResult.getReturnValue() == 'Already Initiated'){
                	resultsToast.setParams({
                    	"title" : "Records Created",
                    	"message" : "Legal and Technical verification already assigned.",
                        "type" : 'error'
                	});
            	}
 
				$A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
                console.log('5555');
            }else if(state === "ERROR"){
                console.log('Problem creating records, response state '+state);
                console.log('6666');
            }else{
                console.log('Unknown problem: '+state);
                console.log('7777');
            }
        });
        $A.enqueueAction(action);
 
 
    },
    cancelCreation: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
     
})