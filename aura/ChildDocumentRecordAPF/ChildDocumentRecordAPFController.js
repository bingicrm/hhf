({
	doInit: function(component, event, helper) {
        // call the fetchPickListVal(component, field_API_Name, aura_attribute_name_for_store_options) -
        // method for get picklist values dynamic 
        
        var capturedResponse = component.get("v.subStage");
        if(capturedResponse != 'File Checker' && capturedResponse != 'Scan Maker' && capturedResponse != 'Scan Checker') {
            component.set("v.showforOtherStages", true);
        }
        //component.set("v.subStage", 'Scan Checker');
        helper.fetchPickListVal(component, 'Status__c','docPicklistOpts');
    },
    closeModalDoc:function(component,event,helper){   
        //alert('Close');
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    inlineEditRating : function(component,event,helper){
        var locked = component.get("v.singleRec.Locked__c");
        if(!locked){
            component.set("v.singleRec.Edited__c", true);
            component.set("v.StatusEditMode", true);
            component.set("v.showSaveCancelBtn",true);
            component.find("inputId").set("v.options",component.get("v.docPicklistOpts"));
        }
    },
    closeStatusBox : function (component, event, helper) {
        component.set("v.StatusEditMode", false);
        //component.set("v.dateEditMode", false);
    },
    inlineEditNotes : function(component){
        var locked = component.get("v.singleRec.Locked__c");
        if(!locked){
            component.set("v.singleRec.Edited__c", true);
            component.set("v.noteEditMode", true);
            component.set("v.showSaveCancelBtn",true);
        }        
    },
    handleUploadFinished : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "type": "success",
            "message": "Your File is Uploaded Successfully."
        });
        toastEvent.fire();
        component.set("v.singleRec.Document_Uploaded__c", true);
       // alert('File Uploaded Successfully.');
    },
    Editbutton : function(component,event,helper){  
        var subStage = component.get("v.subStage");
        console.log('subStage::'+subStage);
        //alert('Here to Edit');
        //alert(subStage);
        //
        var locked = component.get("v.singleRec.Locked__c");
        //alert('Locking status'+locked);
        if(!locked){
            var isNameEditableTrue = component.get("v.singleRec.Document_Type__r.Document_Name_Editable__c");
            var stageName = component.get("v.singleRec.Project__r.Stage__c");
            //alert('Name is editable??'+isNameEditableTrue);
            //if(isNameEditableTrue && (stageName == 'Customer Onboarding' || stageName == 'Customer Acceptance')) {
        	if(isNameEditableTrue) { 
        		component.set("v.nameEditMode", true);
            }
            component.set("v.singleRec.Edited__c", true);
            component.set("v.StatusEditMode", true);
            //if(subStage != "File Check" && subStage != 'Scan: Data Checker' && subStage != 'Docket Checker') {
                component.find("inputId").set("v.options",component.get("v.docPicklistOpts"));
            //}
        }

        
        
        component.set("v.showSaveCancelBtn",true);
    },
    openmodalDoc: function(component,event,helper) {
        //alert('Open');
        /*var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); */
        var propertyId = component.get("v.singleRec.Id");
        //component.set("v.files", []);
        if (!propertyId) {
            return;
        }
        var action = component.get("c.queryAttachments");
        action.setParams({
            "propertyId": propertyId,
        });
        action.setCallback(this, function (response) {
            var files = response.getReturnValue();
            console.log(files);
            if(files != null) {
                var cusrecordIds = files.join();
                console.log('recordIds'+cusrecordIds);
                $A.get('e.lightning:openFiles').fire({
                    //recordIds: ['069p0000001E6ag'],
                    recordIds: files,
                });
            }
            else {
                var cmpTarget = component.find('Modalbox');
                var cmpBack = component.find('Modalbackdrop');
                $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                $A.util.addClass(cmpBack, 'slds-backdrop--open');
            }
            //component.set("v.files", files);
        });
        $A.enqueueAction(action);       
    },
    editFileCheck : function(component,event,helper) {
        component.set("v.singleRec.Edited__c", true);
        component.set("v.showSaveCancelBtn",true);
    },
    editScanCheckCompleted : function(component,event,helper) {
        component.set("v.singleRec.Edited__c", true);
        component.set("v.showSaveCancelBtn",true);
    }
})