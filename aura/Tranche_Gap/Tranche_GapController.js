({
	doInit : function(component, event, helper) {
		/*var action = component.get("c.getTrancheRec");
        alert('doInit---'+component.get("v.recordId"));
        action.setParams({
            "trancheId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                component.set("v.tranche", response.getReturnValue());
            }else{
            
            }
        });
        alert('doInit');
        //console.log('record ID'+component.get("v.recordId"));
        $A.enqueueAction(action);  */
	},
    
    requestCreation : function(component, event, helper) {
		helper.showSpinner(component);
        var action = component.get("c.makeAPICallout");
        var traID = component.get("v.recordId");
        console.log('tranche ID-->'+traID);
        action.setParams({
            "tranID" : traID
        });
        //$A.enqueueAction(action);
        //var resultsToast = $A.get("e.force:showToast");
        action.setCallback(this, function(actionResult){
            var state = actionResult.getState();
            if(state === "SUCCESS"){
                var resultsToast = $A.get("e.force:showToast");
                if(actionResult.getReturnValue() == null){
                    resultsToast.setParams({
                        "title" : "SUCCESS",
                        "message" : "PEMI Amount has been calculated."
                    });
                }
                else{
                    resultsToast.setParams({
                        "title" : "ERROR",
                        "message" : actionResult.getReturnValue()
                    });
                }
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            }
            else if(state === "ERROR"){
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                        "title" : "Error",
                        "message" : "Contact System Administrator."
                    });
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            }
        });
        $A.enqueueAction(action);
        //$A.get("e.force:closeQuickAction").fire();
        //$A.get("e.force:refreshView").fire();

	}, 
    
    cancelCreation : function(component, event, helper) {
		 $A.get("e.force:closeQuickAction").fire();
	}
})