({
	doInit : function(component, event, helper) {
		
        var custid = component.get("v.Customerid");
        var action = component.get("c.GetFinancials");
        action.setParams({
            'conId':custid
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                console.log("response.getReturnValue()"+response.getReturnValue());
                
                component.set("v.Financials", response.getReturnValue());                
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
		
	},
    doInittotalemi : function(component, event, helper) {
		
        var custid = component.get("v.Customerid");
        var action = component.get("c.GetFinancialsTotal");
        action.setParams({
            'conId':custid
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                //console.log("response.getReturnValue()"+response.getReturnValue()[0].Loan_Type__c);
                
                component.set("v.TotalEmi", response.getReturnValue());                
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
		
	},
    
    doInitisedit : function(component, event, helper) {
		
        var custid = component.get("v.Customerid");
        var action = component.get("c.GetIseditable");
        action.setParams({
            'conId':custid
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                //console.log("response.getReturnValue()"+response.getReturnValue()[0].Loan_Type__c);
                
                component.set("v.iseditable", response.getReturnValue());     
                component.set("v.showEdit", response.getReturnValue());                
            
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
		
	},
    doInitcustdetail : function(component, event, helper) {
		
        var custid = component.get("v.Customerid");
        var action = component.get("c.GetCustdetails");
        action.setParams({
            'conId':custid
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                //console.log("response.getReturnValue()"+response.getReturnValue()[0].Loan_Type__c);
                
                //component.set("v.iseditable", response.getReturnValue());     
                component.set("v.contobj", response.getReturnValue());                
            
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
		
	},
	closeMe : function(comp, event, helper)  { 
        var custid = comp.get("v.Customerid");
        var recordIdNumber = '';
        var testLoanAppId = comp.get("v.loanAppId");
        
        var evt = $A.get("e.c:callCreateCamScreenEvent");
        evt.setParams({
            "recordIdNumber" : custid,
            "isOpen" : true,
            "testLoanAppId" : testLoanAppId
        });
        evt.fire();
	}
})