({
    
    // common reusable function for toggle sections
    doInit : function(component, event, helper) {
        console.log('In obligation js');
        helper.doInit(component, event, helper);
        helper.doInittotalemi(component, event, helper);
	    helper.doInitisedit(component, event, helper);
        helper.doInitcustdetail(component, event, helper);
	
	
    },
   
    save : function(component, event, helper) {
        var checkit = component.get("v.Financials");
        var custid = component.get("v.Customerid");
        var dateValidationSt = component.get("v.dateValidationErrorSt");
        var dateValidationEn = component.get("v.dateValidationErrorEn");
        var action = component.get("c.SaveFinancials");
        var Financiallist = component.get("v.Financials")
        console.log('VAL '+checkit[0].isVerified__c);
        if ((checkit[0].isVerified__c == ''  || checkit[0].isVerified__c === false ) && !dateValidationEn && !dateValidationSt) { //|| !isChecked
            action.setParams({
                "Financials":Financiallist,
                        'conId':custid
            
            });
            action.setCallback(this, function(response){
                
                var state = response.getState();
                if(state === "SUCCESS"){
                    component.set("v.iseditable",false);
                    console.log("response.getReturnValue()"+response.getReturnValue());
                    component.set("v.Financials",response.getReturnValue());
                    helper.doInit(component, event, helper);
                    helper.doInittotalemi(component, event, helper);
        
                }else if (response.getState() === "ERROR"){
                    var errors = action.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            component.set("v.message", errors[0].message);
                        }
                    }
                }
            });
            $A.enqueueAction(action);
        }
        else if(checkit[0].isVerified__c && !dateValidationEn && !dateValidationSt) {
            var allValid = component.find('fieldId').reduce(function (validSoFar, inputCmp) {
            	 inputCmp.showHelpMessageIfInvalid();
                 return validSoFar && inputCmp.get('v.validity').valid;
       		}, true);
            console.log('allValid'+allValid);
            if(allValid){
                action.setParams({
                    "Financials":JSON.stringify(Financiallist),
                            'conId':custid
                });
                action.setCallback(this, function(response){
                    var state = response.getState();
                    if(state === "SUCCESS"){
                        component.set("v.iseditable",false);
                        console.log("response.getReturnValue()"+response.getReturnValue());
                        component.set("v.Financials",response.getReturnValue());
                        helper.doInit(component, event, helper);
                        helper.doInittotalemi(component, event, helper);
            
                    }else if (response.getState() === "ERROR"){
                        var errors = action.getError();
                        console.log('errors '+errors);
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                component.set("v.message", errors[0].message);
                            }
                        }
                    }
                });
                $A.enqueueAction(action); 
            }
        }
        else {
            console.log('Here i am ');
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": "Please complete missing information."
                });
                toastEvent.fire();
        }
	},
    addnew : function(component, event, helper) {
		
        var custid = component.get("v.Customerid");
        var action = component.get("c.NewObligation");
        var Financiallist = component.get("v.Financials")
        action.setParams({
            'conId':custid,
            "Financials":JSON.stringify(Financiallist)
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                	component.set("v.iseditable",true);
                component.set("v.Financials", response.getReturnValue());                
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
		
	},
    removeRow: function(component, event, helper){        
        var finList = component.get("v.Financials");//Get the account list
        //console.log("finList",finList);       
        var selectedItem = event.currentTarget;//Get the target object
        var index = selectedItem.dataset.record;//Get the selected item index
        //console.log("index",index);
        finList.splice(index, 1);
        component.set("v.Financials", finList);
    },
    edit : function(component, event, helper) {
		component.set("v.iseditable",true);
	},
       // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
    handleConfirmDialogbox : function(component, event, helper) {
        component.set('v.showConfirmDialogbox', true);
    },
    handleConfirmDialogboxYes : function(component, event, helper) {
        helper.closeMe(component, event, helper);
    },
    handleConfirmDialogboxNo : function(component, event, helper) {
        component.set('v.showConfirmDialogbox', false);
    },
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },
    dateUpdateStart : function(component, event, helper) {
        //alert('hi');
        var today = new Date();        
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
     // if date is less then 10, then append 0 before date   
        if(dd < 10){
            dd = '0' + dd;
        } 
    // if month is less then 10, then append 0 before date    
        if(mm < 10){
            mm = '0' + mm;
        }
        var checkit = component.get("v.Financials");
        var indexvar = event.getSource().get("v.label");
        //console.log('value: '+indexvar);
        //console.log('value123: '+checkit[indexvar].Date_of_Loan_Taken__c);
        var todayFormattedDate = yyyy+'-'+mm+'-'+dd;
        //Added by Vaishali for BREII-Enhancement
        if(checkit[indexvar].Date_of_Loan_Taken__c != '' && checkit[indexvar].Date_of_Loan_Taken__c > todayFormattedDate){
            component.set("v.dateValidationErrorSt" , true);
        }else{
            component.set("v.dateValidationErrorSt" , false);
        }
        
     },
     dateUpdateEnd : function(component, event, helper) {
        var checkit = component.get("v.Financials");
        var indexvar = event.getSource().get("v.label");
        //Added by Vaishali for BREII-Enhancement
        if((checkit[indexvar].Date_of_Loan_Taken__c > checkit[indexvar].Date_of_Closure__c) && checkit[indexvar].Date_of_Loan_Taken__c!='' && checkit[indexvar].Date_of_Closure__c!=''){
            component.set("v.dateValidationErrorEn" , true);
        }else{
            component.set("v.dateValidationErrorEn" , false);
        }
        
     }
     
})