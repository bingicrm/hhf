({
    CopyData: function(component, event, helper)
    {
        console.log('Record Id::'+ component.get("v.recordId"));
        var getStatus = component.get("c.copyApplicantsValues");
        getStatus.setParams({
            "loanContactId": component.get("v.recordId")
        });
        getStatus.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                var response = actionResult.getReturnValue();
                if(response == 'SUCCESS') {
                    $A.get("e.force:closeQuickAction").fire();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": "success",
                        "message": "Data have been successfully copied."
                    });
                    toastEvent.fire();
                         
                }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "Error",
                        "message": actionResult.getReturnValue()
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();     
                    /*component.set("v.errorMessage",actionResult.getReturnValue());
                    component.set("v.showForm",false);*/
                }
            }
            else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "Error",
                    "message": "Error: Some error occured. Please contact System Admin."
                });
                toastEvent.fire();
                $A.get("e.force:closeQuickAction").fire();     
            }
        });
        $A.enqueueAction(getStatus);	
    },
     cancelCreation: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})