({
    initMethod: function(component, event, helper)
    {
        //Below code added by Abhilekh on 24th Janaury 2020 for Hunter BRD
        var getHunterDeclineStatus = component.get("c.checkHunterDecline");
        getHunterDeclineStatus.setParams({
            "laId": component.get("v.recordId")
        });
        getHunterDeclineStatus.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                var response = actionResult.getReturnValue();
                if(response == true) {
                    var errorMessage =[];
                    errorMessage.push('Repayment Schedule cannot be fetched as Hunter Analysis has not been recommended. Please reject or assign the application to Hunter Manager.');
                    
                    component.set("v.error",true);
                    component.set("v.errorMessage",errorMessage);
                    component.set("v.showForm",false);
                }
                else{
                    //Below code added by Abhilekh on 5th October 2019 for FCU BRD
                    var getFCUDeclineStatus = component.get("c.checkFDDecline");   
                    getFCUDeclineStatus.setParams({
                        "laId": component.get("v.recordId")
                    });
                    getFCUDeclineStatus.setCallback(this, function(actionResult) {
                        var state = actionResult.getState();
                        if (state === "SUCCESS") {
                            var response = actionResult.getReturnValue();
                            if(response == true) {
                                var errorMessage =[];
                                errorMessage.push('Repayment Schedule cannot be fetched as this application is FCU Decline');
                                
                                component.set("v.error",true);
                                component.set("v.errorMessage",errorMessage);
                                component.set("v.showForm",false);
                            }
                            else{
                                component.set("v.showForm",true);
                                
                                //Below code was already there                    
                                var action = component.get("c.validateUser");
                                action.setParams({
                                    "recordId": component.get("v.recordId")
                                });
                                action.setCallback(this, function(actionResult) {
                                    var state = actionResult.getState();
                                    if (state === "SUCCESS") {
                                        var response = actionResult.getReturnValue();
                                        console.log('Response'+response);
                                        if(response == true) {
                                            
                                            helper.getRepaymentRecord(component); 
                                        }
                                        else {
                                            component.set("v.eligibilityFailed",true);
                                            var cmpTarget = component.find('Modalbox');
                                            var cmpBack = component.find('Modalbackdrop');
                                            //alert(cmpTarget);
                                            //alert(cmpBack);
                                            $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                                            $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
                                            
                                        }
                                        
                                    }
                                    else {
                                        var toastEvent = $A.get("e.force:showToast");
                                        toastEvent.setParams({
                                            "title": "Error!",
                                            "type": "Error",
                                            "message": "Error: Some error occured. Please contact System Admin."
                                        });
                                        toastEvent.fire();
                                    }
                                });
                                $A.enqueueAction(action);
                                
                                var getDisType = component.get("c.getDisbursementType");
                                
                                getDisType.setParams({
                                    "recordId": component.get("v.recordId")
                                });
                                getDisType.setCallback(this, function(actionResult) {
                                    var state = actionResult.getState();
                                    
                                    if (state === "SUCCESS") {
                                        var responseValue = actionResult.getReturnValue();
                                        //alert(responseValue);
                                        component.set("v.isMultipleDisbursal",responseValue);
                                    }
                                });
                                $A.enqueueAction(getDisType);
                                //Above code was already there                    
                            }
                        }
                        else {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Error!",
                                "type": "Error",
                                "message": "Error: Some error occured. Please contact System Admin."
                            });
                            toastEvent.fire();
                        }
                    });
                    $A.enqueueAction(getFCUDeclineStatus);
                }
            }
            else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "Error",
                    "message": "Error: Some error occured. Please contact System Admin."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(getHunterDeclineStatus);
    },
    doInit: function(component, event, helper) 
    {   
        var gradedDetails;
        var repayment = component.get("v.loanRepayment");
        if(repayment.Installment_Plan__c == 'Graded Instl')
        {
            gradedDetails = component.get("v.gradedDetails");
            gradedDetails.push({
                'sobjectType': 'Graded_Details__c',
                'Slab_and_EMI_Seq_No__c': 1,
                'Slab_and_EMI_EMI__c': '',
                'Slab_and_EMI_Instl_From__c': 1,
                'Slab_and_EMI_Instl_To__c': '',
                'Slab_and_EMI_Recovery__c':''
            });
            
        }
        else
        {
            gradedDetails = [];
        }
        component.set("v.gradedDetails", gradedDetails);
        
    },
    onSubmit : function(component, event, helper) 
    {
        
        var error = false;
        var errorMessage =[];
        var repayment = component.get("v.loanRepayment");
        var la = component.get("v.simpleRecord");
        
        component.set("v.error",error);
        component.set("v.errorMessage",errorMessage);    
        
        if(repayment.Rate_Type__c == 'EMI')
        {
            component.set('v.loanRepayment.Interest_Rate__c','0');
            if(component.get('v.loanRepayment.EMI_Amount__c') == null || component.get('v.loanRepayment.EMI_Amount__c') < 0.1 )
            {
                //alert('invalid EMI AMT');
                errorMessage.push('invalid EMI AMT');
                error =  true;
            }
        }
        else if(repayment.Rate_Type__c == 'Rate')
        {
            component.set('v.loanRepayment.EMI_Amount__c','0');
            if(component.get('v.loanRepayment.Interest_Rate__c') == null || component.get('v.loanRepayment.Interest_Rate__c') < 0.1 )
            {
                //alert('invalid ROI');
                errorMessage.push('invalid ROI');
                error =  true;
            }
        }
        
        if ( repayment.Frequency__c == 'Quarterly' && (( repayment.Moratorium_Flag__c == 'Yes' && repayment.Moratorium_period__c % 3 != 0 )  || repayment.Tenure__c % 3 != 0)) {
            errorMessage.push('Loan tenure should in multiple of 3 for Quarterly frequency.');
            error =  true;
        }
        if ( repayment.Frequency__c == 'Half-yearly' && (( repayment.Moratorium_Flag__c == 'Yes' && repayment.Moratorium_period__c % 6 != 0 )  || repayment.Tenure__c % 6 != 0)) {
            errorMessage.push('Loan tenure should in multiple of 6 for Half-yearly frequency.');
            error =  true;
        }
        if ( repayment.Frequency__c == 'Annually' && (( repayment.Moratorium_Flag__c == 'Yes' && repayment.Moratorium_period__c % 12 != 0 )  || repayment.Tenure__c % 12 != 0)) {
            errorMessage.push('Loan tenure should in multiple of 12 for Annual frequency.');
            error =  true;
        }
        
        if (component.get("v.loanRepayment.Rate_Type__c") == 'EMI' && component.get("v.isMultipleDisbursal") == true) {
            errorMessage.push('For multiple disbursal, Rate Type can only be Rate.');
            error =  true;
        }
        
        if (component.get("v.loanRepayment.Installment_Plan__c") != 'Equated Instl' && component.get("v.isMultipleDisbursal") == true) {
            errorMessage.push('For multiple disbursal, Instalment Plan can only be Equated.');
            error =  true;
        }
        if (component.get("v.loanRepayment.Installment_Plan__c") == 'Bullet' && component.get("v.loanRepayment.Rate_Type__c") != 'Rate' && component.get("v.isMultipleDisbursal") == false) {
            errorMessage.push('Rate Type must be Rate, when Instalment Plan is set to Bullet.');
            error =  true;
        }
        if (component.get("v.loanRepayment.Installment_Plan__c") == 'Balloon' && component.get("v.loanRepayment.Rate_Type__c") != 'Rate' && component.get("v.isMultipleDisbursal") == false) {
            errorMessage.push('Rate Type must be Rate, when Instalment Plan is set to Balloon.');
            error =  true;
        }
        console.log('Hello World size 0');
        if(repayment.Loan_Application__r.Insurance_Loan_Application__c){/** Added by saumay For Insurance Loan **/
            if (component.get("v.loanRepayment.Broken_Period_Interest_Handing__c") != 'Add to schedule' ) {
                errorMessage.push('Broken Period Interest Handling can not be Return as Charge for Insurance Loan. Please change.');
                error =  true;
            }
        }
        var tt = component.get("v.gradedDetails")[0];
        
        if( repayment.Installment_Plan__c == 'Graded Instl' )
        {
            var gradedDetails = component.get("v.gradedDetails");
            console.log(gradedDetails);
            if( gradedDetails[gradedDetails.length-1].Slab_and_EMI_Instl_To__c == '' ||
               (gradedDetails[gradedDetails.length-1].Slab_and_EMI_Recovery__c == '' &&
                gradedDetails[gradedDetails.length-1].Slab_and_EMI_EMI__c == ''))
            {
                //alert('Please fill in all the details before adding new row');
                errorMessage.push('Please fill in all the details before adding new row');
                error =  true;
            }
            else if( Number(gradedDetails[gradedDetails.length-1].Slab_and_EMI_Instl_To__c) > Number(repayment.Tenure__c) )
            {
                //alert('Installment cannot exceed total approved tenure');
                errorMessage.push('Installment cannot exceed total approved tenure');
                error =  true;
            }
            
            var sumOfInstallment = 0;
            for( var i = 0; i < gradedDetails.length; i++)
            {
                sumOfInstallment = sumOfInstallment +  Number(gradedDetails[i].Slab_and_EMI_Recovery__c);
            }
            
            if(sumOfInstallment != 100 && sumOfInstallment > 0 )
            {
                errorMessage.push('Total recovery percentage should be 100');
                error =  true;
            }
        }
        
        if(repayment.Installment_Plan__c == 'Balloon')
        {
            if( (repayment.Baloon_Amount__c == null || !(repayment.Baloon_Amount__c > 0)) && component.get("v.isMultipleDisbursal") == false)
            {
                errorMessage.push('Balloon Amount mandatory, when Instalment Plan is set to Balloon.');
                error =  true;
            }
            
        }else
        {
            repayment.Baloon_Amount__c = 0;
        }
        
        if( repayment.Moratorium_Flag__c == 'Yes')
        {
            if( repayment.Moratorium_period__c == null || !(repayment.Moratorium_period__c > 0) )
            {
                errorMessage.push('Invalid Morotorium period');
                error =  true;
            }     
        }
        
        console.log('Error');
        console.log(error);
        helper.showSpinner(component);//Added by Saumya  For Loading Spinner
        if(!error)
        {
            
            console.log('In Error');
            console.log(error);
            console.log('component.get("v.loanRepayment"):::Saumya'+JSON.stringify(component.get("v.loanRepayment")));
            var recordId = '';
            var action = component.get('c.saveRepayment');
            action.setParams({
                "la": component.get("v.simpleRecord"),
                "lr": component.get("v.loanRepayment")
            });
            action.setCallback(this, function(actionResult) {
                var state = actionResult.getState();
                if (state === "SUCCESS") {
                    recordId = actionResult.getReturnValue();
                    if(component.get("v.loanRepayment").Installment_Plan__c == 'Graded Instl')
                    {
                        helper.helperMethod(component,recordId,null,la.Id);
                    }
                    else
                    {
                        helper.generatePaymentSchedule(component,recordId,la.Id);
                    }
                    
                }
            });
            $A.enqueueAction(action);
            
        }
        else
        {
            console.log('Hello this is error');
            console.log(errorMessage);
            component.set("v.error",error);
            component.set("v.errorMessage",errorMessage);
            helper.hideSpinner(component);//Added by Saumya  For Loading Spinner
            console.log(component.get("v.errorMessage"));
        }
        
        
    },
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            // record is loaded (render other component which needs record data value)
            console.log("Record is loaded successfully.");
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }
    },
    addNewRow: function(component, event, helper) 
    {
        var lr = component.get("v.loanRepayment");
        var gradedDetails = component.get("v.gradedDetails");
        var error = false;
        var errorMessage =[];
        component.set("v.error",error);
        component.set("v.errorMessage",errorMessage);    
        //alert(Number(gradedDetails[gradedDetails.length-1].Slab_and_EMI_Instl_To__c));
        if( gradedDetails[gradedDetails.length-1].Slab_and_EMI_Instl_To__c == '' ||
           (gradedDetails[gradedDetails.length-1].Slab_and_EMI_Recovery__c == '' &&
            gradedDetails[gradedDetails.length-1].Slab_and_EMI_EMI__c == ''))
        {
            
            //alert('Please fill in all the details before adding new row');
            errorMessage.push('Please fill in all the details before adding new row');
            error = true;
        }
        else if( Number(gradedDetails[gradedDetails.length-1].Slab_and_EMI_Instl_To__c) >= Number(lr.Tenure__c) )
        {
            //alert('Installment cannot exceed total approved tenure');
            errorMessage.push('Installment cannot exceed total approved tenure');
            error = true;
        }
        if(!error)
        {
            
            var startSeq = gradedDetails[gradedDetails.length-1].Slab_and_EMI_Instl_To__c;
            
            gradedDetails.push({
                'sobjectType': 'Graded_Details__c',
                'Slab_and_EMI_Seq_No__c': gradedDetails.length+1,
                'Slab_and_EMI_EMI__c': '',
                'Slab_and_EMI_Instl_From__c': Number(startSeq) + Number(1),
                'Slab_and_EMI_Instl_To__c': '',
                'Slab_and_EMI_Recovery__c':''
            });
            // set the updated list to attribute (contactList) again    
            component.set("v.gradedDetails", gradedDetails);
        }
        else
        {
            component.set("v.error",error);
            component.set("v.errorMessage",errorMessage);    
        }
        
    },
    removeDeletedRow: function(component, event, helper) 
    {
        var index = event.getParam("indexVar");
        // get the all List (contactList attribute) and remove the Object Element Using splice method    
        var AllRowsList = component.get("v.gradedDetails");
        AllRowsList.splice(index, 1);
        // set the contactList after remove selected row element  
        component.set("v.gradedDetails", AllRowsList);
    },
    closeModalDoc:function(component,event,helper){   
        //alert('Close');
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    }
})