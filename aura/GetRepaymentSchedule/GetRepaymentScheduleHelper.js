({
    helperMethod : function(component, recordId, gradedDetails,laId) 
    {
        
        //console.log(component.get("v.gradedDetails"));
        var action = component.get('c.insertGradedDetails');
        action.setParams({
            "allGraded": component.get("v.gradedDetails"),
            "parentId": recordId
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                this.generatePaymentSchedule(component,recordId,laId);
            }
        });
        $A.enqueueAction(action);
    },
    generatePaymentSchedule : function(component,recordId,laId)
    {
        console.log(recordId);
        var action2 = component.get('c.getSchedule');
        var error = false;
        var errorMessage =[];
        component.set("v.error",error);
        component.set("v.errorMessage",errorMessage);  
        
        
        action2.setParams({
            "lrId": recordId,
            "LaId": laId
        });
        action2.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            var resResult =  JSON.parse(actionResult.getReturnValue());
            console.log(actionResult.getReturnValue());
            
            if (state === "SUCCESS") 
            {
                
                if(resResult.isError)
                {
                    //alert(resResult.errorMessage);
                    errorMessage.push('Error in generating repayment Schedule ' + resResult.errorMessage);
                    error =  true;
                    component.set("v.error",error);
                    component.set("v.errorMessage",errorMessage);  
                }
                else
                {
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": recordId,
                        "slideDevName": "related"
                    });
                    navEvt.fire();
                }
                this.hideSpinner(component, event, helper);//Added by Saumya  For Loading Spinner
                
            }
        });
        $A.enqueueAction(action2);
        
    },
    getDueDate : function(component)
    {
        var laId = component.get('v.recordId');
        var action = component.get('c.getDueDayMaster');
        action.setParams({
            "LaId": laId
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            //console.log(resResult);
            if (state === "SUCCESS") 
            {
                var resResult =  actionResult.getReturnValue();
                var custs = [];
                for(var key in resResult){
                    custs.push({value:parseInt(resResult[key]), key:parseInt(key)});
                }
                component.set('v.dueDate',custs);
                console.log(custs[0].key);
                //('got it');
                //alert(component.get(v.loanRepayment.dueDay__c));
                if(component.get('v.loanRepayment.dueDay__c') == null )
                   component.set('v.loanRepayment.dueDay__c',custs[0].key);
            }
        });
        $A.enqueueAction(action);
    },
    
    getRepaymentRecord : function(component)
    {
        var laId = component.get('v.recordId');
        //alert(laId);
        var action = component.get('c.getRepaymentRecord');
        action.setParams({
            "LaId": laId
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            
            if (state === "SUCCESS") 
            {
                var repayment = actionResult.getReturnValue();
                console.log('repayment::'+ repayment);
                //alert('Stage'+repayment.Loan_Application__r.StageName__c);
                /*if(!$A.util.isUndefined( repayment) || !$A.util.isUndefined( repayment.Loan_Application__c) || !$A.util.isUndefined( repayment.Loan_Application__r.StageName__c) || repayment.Loan_Application__r.StageName__c != 'Undefined'){
                  if( repayment.Loan_Application__r.StageName__c == 'Loan Disbursal') {
                    component.set('v.boolIsDisb',true);
                }  
                }*/
                
                if(!$A.util.isUndefined( repayment) && repayment != null){
                    if(!$A.util.isUndefined( repayment.Loan_Application__c) && repayment.Loan_Application__c != null){
                        if(!$A.util.isUndefined( repayment.Loan_Application__r.StageName__c)){
                            if( repayment.Loan_Application__r.StageName__c == 'Loan Disbursal') {
                                component.set('v.boolIsDisb',true);
                            } 
                        }
                    }
                }
                
                if(!$A.util.isUndefined( repayment) && repayment != null){ /** Added by Saumya for Insurance Loan ***/
                    if(!$A.util.isUndefined( repayment.Loan_Application__c) && repayment.Loan_Application__c != null){
                        if( repayment.Loan_Application__r.Insurance_Loan_Application__c) {
                            component.set('v.boolMonatorium',true);
                        }
                    }
                }
                if(!$A.util.isUndefined( repayment) && repayment != null){
                    if(!$A.util.isUndefined( repayment.Loan_Application__c) && repayment.Loan_Application__c != null){
                        var date = new Date();
                        date = repayment.Loan_Application__r.Business_Date_Modified__c;
                        console.log('date::'+date);
                        if(date != null){
                            var day =date.split("-");
                            console.log('day:::'+day[2]);
                            if(/*day[2] == '08' &&*//*** Added by Saumya for New IMGC  Enhancements ***/ repayment.Loan_Application__r.Insurance_Loan_Application__c){ /** Added by Saumya for Insurance Loan**/
                                repayment.Moratorium_Flag__c ='No';
                                repayment.Moratorium_period__c =0;
                                repayment.Moratorium_impact_on_tenure__c ='Exclusive';
                            }
                            /*** Added by Saumya for  IMGC Enhancements ***/
                            //if(day[2] != '08' && repayment.Loan_Application__r.Insurance_Loan_Application__c){ /** Added by Saumya for Insurance Loan**/
                            /*  repayment.Moratorium_Flag__c ='Yes';
                                repayment.Moratorium_period__c =1;
                                repayment.Moratorium_impact_on_tenure__c ='Exclusive';
                            }*/
                            /*** Added by Saumya for IMGC Enhancements ***/
                        }
                    }
                }
                
                component.set('v.loanRepayment',repayment);
                this.getDueDate(component);
                if(!$A.util.isUndefined(repayment) && repayment != null){
                    if(repayment.Installment_Plan__c != null){
                        if(repayment.Installment_Plan__c == 'Graded Instl')
                        {
                            component.set("v.gradedDetails",repayment.Graded_Details__r);
                        } 
                    }
                    
                }
            }
        });
        $A.enqueueAction(action);
    },
    showSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    hideSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-show");
    }
    
});