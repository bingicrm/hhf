({
    doInit : function(component, event, helper) {
        console.log('I am in doInit');
        helper.fetchPickListVal(component, 'Recommender_s_Decision__c','recommendersDecisionOptions');
        helper.fetchPickListVal(component, 'Approver_s_Decision__c','approversDecisionOptions');
        helper.fetchPickListVal(component, 'Overall_UCIC_Decision__c','overallDecisionOptions');
        var action = component.get("c.getUCICData");
        action.setParams({
            'laId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var res = response.getReturnValue();
                
                if(res.currentUserProfile == 'Credit Team' && (res.la.Sub_Stage__c == 'Credit Review' || res.la.Sub_Stage__c == 'Re Credit' || res.la.Sub_Stage__c == 'Re-Look') && (res.UCICResponseStatus == false || res.UCICNegativeAPIResponse == false)){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "",
                        "type": "Error",
                        "message": "Please wait for latest Dedupe Results or re-initiate Dedupe Results."
                    });
                    toastEvent.fire();
                }
                
                if(res.currentUserProfile != 'System Administrator' && res.currentUserProfile != 'Credit Team'){
                    //component.set("v.bothDecisionEditable",false);
                    //component.set("v.bothDecisionNonEditable",true);
                    component.set("v.recommendersDecisionEditable",false);
                    component.set("v.approversDecisionEditable",false);
                }
                else if(res.la.Sub_Stage__c != 'Credit Review' && res.la.Sub_Stage__c != 'Re Credit' && res.la.Sub_Stage__c != 'Re-Look' && res.la.Sub_Stage__c != 'L1 Credit Approval' && res.la.Sub_Stage__c != 'Credit Authority Approval'){
                    //component.set("v.bothDecisionEditable",false);
                    //component.set("v.bothDecisionNonEditable",true);
                    component.set("v.recommendersDecisionEditable",false);
                    component.set("v.approversDecisionEditable",false);
                }
                else if(res.la.Sub_Stage__c == 'Credit Review' || res.la.Sub_Stage__c == 'Re Credit' || res.la.Sub_Stage__c == 'Re-Look'){
                	if(res.la.Assigned_Credit_Review__c == res.la.OwnerId && res.la.OwnerId == res.currentUserId){
                        //component.set("v.bothDecisionEditable",true);
                        //component.set("v.bothDecisionNonEditable",false);
                        component.set("v.recommendersDecisionEditable",true);
                        component.set("v.approversDecisionEditable",true);
                    }
                    else if(res.la.Assigned_Credit_Review__c != res.la.OwnerId && res.la.OwnerId == res.currentUserId){
                        //component.set("v.bothDecisionEditable",false);
                        //component.set("v.bothDecisionNonEditable",false);
                        component.set("v.recommendersDecisionEditable",true);
                        component.set("v.approversDecisionEditable",false);
                    }
                    else{
                        //component.set("v.bothDecisionEditable",false);
                        //component.set("v.bothDecisionNonEditable",true);
                          component.set("v.recommendersDecisionEditable",false);
                          component.set("v.approversDecisionEditable",false);
                    }
                }
                else if(res.la.Sub_Stage__c == 'L1 Credit Approval'){
                    if(res.la.OwnerId != res.currentUserId && res.currentUserId == res.la.Assigned_Credit_Review__c){
                        //component.set("v.bothDecisionEditable",false);
                        //component.set("v.bothDecisionNonEditable",false);
                        component.set("v.recommendersDecisionEditable",false);
                        component.set("v.approversDecisionEditable",true);
                    }
                    else{
                        //component.set("v.bothDecisionEditable",false);
                        //component.set("v.bothDecisionNonEditable",true);
                        component.set("v.recommendersDecisionEditable",false);
                        component.set("v.approversDecisionEditable",false);
                    }
                }
                else if(res.la.Sub_Stage__c == 'Credit Authority Approval'){
                	if(res.la.OwnerId != res.currentUserId && res.currentUserId == res.la.Assigned_Credit_Review__c){
                	    //component.set("v.bothDecisionEditable",false);
                        //component.set("v.bothDecisionNonEditable",false);
                        component.set("v.recommendersDecisionEditable",false);
                        component.set("v.approversDecisionEditable",true);
                    }
                    else{
                    	//component.set("v.bothDecisionEditable",false);
                        //component.set("v.bothDecisionNonEditable",true);
                        component.set("v.recommendersDecisionEditable",false);
                        component.set("v.approversDecisionEditable",false);
                    }
                }
                else{
                	//component.set("v.bothDecisionEditable",false);
                    //component.set("v.bothDecisionNonEditable",true);
                    component.set("v.recommendersDecisionEditable",false);
                    component.set("v.approversDecisionEditable",false);
                }
                
                component.set("v.UCICData",res);
            }
            else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": "Technical error occured, please contact System Administrator!"
                });
                toastEvent.fire();
                
                $A.get("e.force:closeQuickAction").fire();
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    handleSave : function(component, event, helper){
        var validate = true;
        var lstUD = component.get("v.UCICData.lstUD");
        var laVal = component.get("v.UCICData.la");
        //var bothEditable = component.get("v.bothDecisionEditable");
        //var bothNonEditable = component.get("v.bothDecisionNonEditable");
        var recommendersEditable = component.get("v.recommendersDecisionEditable");
        var approversEditable = component.get("v.approversDecisionEditable");
        
        if(recommendersEditable == true || approversEditable == true){
            for(var i=0; i<lstUD.length; i++){
                /*if(bothEditable == true){
                            if(lstUD[i].Recommender_s_Decision__c != '' && $A.util.isUndefinedOrNull(lstUD[i].Recommender_s_Remarks__c)){
                                validate = false;
                            }
                            if(lstUD[i].Approver_s_Decision__c != '' && $A.util.isUndefinedOrNull(lstUD[i].Approver_s_Remarks__c)){
                                validate = false;
                            }
                        }*/
                if(recommendersEditable == true){
                    if(lstUD[i].Recommender_s_Decision__c != '' && ($A.util.isUndefinedOrNull(lstUD[i].Recommender_s_Remarks__c) || lstUD[i].Recommender_s_Remarks__c == '')){
                        validate = false;
                    }   
                }
                if(approversEditable == true){
                    if(lstUD[i].Approver_s_Decision__c != '' && ($A.util.isUndefinedOrNull(lstUD[i].Approver_s_Remarks__c) || lstUD[i].Approver_s_Remarks__c == '')){
                        validate = false;
                    }
                }
            }
            if(laVal != null && recommendersEditable == true){
                if(laVal.Overall_UCIC_Decision__c != '' && ($A.util.isUndefinedOrNull(laVal.Overall_UCIC_Decision_Remarks__c) || laVal.Overall_UCIC_Decision_Remarks__c == '')){
                    validate = false;
                }    
            }
        }
        
        if(validate == true){
            var action = component.get("c.saveUCICDatabaseDecision");
            action.setParams({
                'UCICData' : JSON.stringify(component.get("v.UCICData"))
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if(state === "SUCCESS"){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "",
                        "type": "Success",
                        "message": "Data saved successfully!"
                    });
                    toastEvent.fire();
                    
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "",
                        "type": "Error",
                        "message": "Unable to save data, please contact System Administrator!"
                    });
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
        }
        else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "",
                "type": "Error",
                "message": "Please fill Remarks before saving the data."
            });
            toastEvent.fire();
        }
    },
    
    close : function(component, event, helper){
        $A.get("e.force:closeQuickAction").fire();
    },
    
    // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
    
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    }
})