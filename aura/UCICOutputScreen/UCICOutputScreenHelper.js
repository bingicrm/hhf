({
    fetchPickListVal: function(component, fieldName, picklistOptsAttributeName) {
        var action = component.get("c.getselectOptions");
        if(fieldName != 'Overall_UCIC_Decision__c'){
            action.setParams({
                "objObject": component.get("v.objInfoForPicklistValues"),
                "fld": fieldName
            });
        }
        else{
            action.setParams({
                "objObject": component.get("v.objInfoForlaPicklistValues"),
                "fld": fieldName
            });    
        }
        //var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                
                /*if (allValues != undefined && allValues.length > 0) {
                        opts.push({
                            class: "optionClass",
                            label: "--- None ---",
                            value: ""
                        });
                    }
                    for (var i = 0; i < allValues.length; i++) {
                        opts.push({
                            class: "optionClass",
                            label: allValues[i],
                            value: allValues[i]
                        });
                    }*/
                //console.log('opts==>'+JSON.stringify(opts));
                //console.log('allValues::fieldName::'+allValues)
                
                component.set("v." + picklistOptsAttributeName, allValues);
            }
        });
        $A.enqueueAction(action);
    }
})