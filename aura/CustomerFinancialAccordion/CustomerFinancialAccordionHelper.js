({
    getUserProfile : function(component, event, helper){
        var action = component.get("c.getProfileName");
        //action.setParams({'custDet': component.get("v.recordId")});
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.LIPView", response.getReturnValue());
                
                if(response.getReturnValue() == true){
                    helper.getCustFins1(component, event, helper);
                    helper.getCustFins2(component, event, helper);
                    helper.getCustFins3(component, event, helper);
                    helper.getCustFins4(component, event, helper);
                    helper.getCustFins5(component, event, helper);
                }
                else{
                    helper.getCustFins1(component, event, helper);
                    helper.getCustFins2(component, event, helper);
                    helper.getCustFins3(component, event, helper);
                    helper.getCustFins4(component, event, helper);
                    helper.getCustFins5(component, event, helper);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getCustFins1 : function(component, event, helper) {
        var action = component.get("c.getCustFins1");
        action.setParams({'custDet': component.get("v.recordId")});
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.data1", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    getCustFins2 : function(component, event, helper) {
        var action = component.get("c.getCustFins2");
        action.setParams({'custDet': component.get("v.recordId")});
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.data2", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    getCustFins3 : function(component, event, helper) {
        var action = component.get("c.getCustFins3");
        action.setParams({'custDet': component.get("v.recordId")});
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.data3", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    getCustFins4 : function(component, event, helper) {
        var action = component.get("c.getCustFins4");
        action.setParams({'custDet': component.get("v.recordId")});
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.data4", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    getCustFins5 : function(component, event, helper) {
        var action = component.get("c.getCustFins5");
        action.setParams({'custDet': component.get("v.recordId")});
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.data5", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    
    saveDataTable1 : function(component, event, helper) {
        //var editedRecords =  component.find("customerFinancialSheet1").get("v.draftValues");
        var editedRecords = component.get("v.data1");
        //var totalRecordEdited = editedRecords.length;
        var action = component.get("c.updateCustFins");
        action.setParams({
            'editedCFList' : editedRecords
        });
        action.setCallback(this,function(response) {
            //alert(response.getState());
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() === true){
                    helper.showToast({
                        "title": "Record Update",
                        "type": "success",
                        "message": " Customer Financial(s) Records Updated"
                    });
                    console.log('1111');                    
                    //$A.get('e.force:refreshView').fire();
                    
                    //helper.reloadDataTable();
                    component.set('v.readonly',true);
                    console.log("Event Fired");
                } else{ 
                    helper.showToast({
                        "title": "Error!!",
                        "type": "error",
                        "message": "Error in update"
                    });
                    //$A.get('e.force:refreshView').fire();
                    helper.reloadDataTable();
                }
                //$A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    saveDataTable2 : function(component, event, helper) {
        //var editedRecords =  component.find("customerFinancialSheet2").get("v.draftValues");
        var editedRecords = component.get("v.data2");
        //var totalRecordEdited = editedRecords.length;
        var action = component.get("c.updateCustFins");
        action.setParams({
            'editedCFList' : editedRecords
        });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                if(response.getReturnValue() === true){
                    helper.showToast({
                        "title": "Record Update",
                        "type": "success",
                        "message": " Customer Financial(s) Records Updated"
                    });
                    console.log('2222');
                    component.set('v.readonly2',true);
                    //$A.get('e.force:refreshView').fire();
                    //helper.reloadDataTable();
                } else{ 
                    helper.showToast({
                        "title": "Error!!",
                        "type": "error",
                        "message": "Error in update"
                    });
                    //$A.get('e.force:refreshView').fire();
                    helper.reloadDataTable();
                }
            }
        });
        $A.enqueueAction(action);
    },
    saveDataTable3 : function(component, event, helper) {
        //var editedRecords =  component.find("customerFinancialSheet3").get("v.draftValues");
        var editedRecords = component.get("v.data3");
        //var totalRecordEdited = editedRecords.length;
        var action = component.get("c.updateCustFins");
        action.setParams({
            'editedCFList' : editedRecords
        });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                if(response.getReturnValue() === true){
                    helper.showToast({
                        "title": "Record Update",
                        "type": "success",
                        "message": " Customer Financial(s) Records Updated"
                    });
                    console.log('2222');
                    component.set('v.readonly3',true);
                    //$A.get('e.force:refreshView').fire();
                    //helper.reloadDataTable();
                } else{ 
                    helper.showToast({
                        "title": "Error!!",
                        "type": "error",
                        "message": "Error in update"
                    });
                    //$A.get('e.force:refreshView').fire();
                    helper.reloadDataTable();
                }
            }
        });
        $A.enqueueAction(action);
    },
    saveDataTable4 : function(component, event, helper) {
        //var editedRecords =  component.find("customerFinancialSheet4").get("v.draftValues");
        var editedRecords = component.get("v.data4");
        //var totalRecordEdited = editedRecords.length;
        var action = component.get("c.updateCustFins");
        action.setParams({
            'editedCFList' : editedRecords
        });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                if(response.getReturnValue() === true){
                    helper.showToast({
                        "title": "Record Update",
                        "type": "success",
                        "message": " Customer Financial(s) Records Updated"
                    });
                    console.log('2222');
                    component.set('v.readonly4',true);
                    //$A.get('e.force:refreshView').fire();
                    //helper.reloadDataTable();
                    
                } else{ 
                    helper.showToast({
                        "title": "Error!!",
                        "type": "error",
                        "message": "Error in update"
                    });
                    //$A.get('e.force:refreshView').fire();
                    helper.reloadDataTable();
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    
    showToast : function(params){
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(params);
            toastEvent.fire();
        } else{
            alert(params.message);
        }
    },
    
    reloadDataTable : function(component,event,helper){
        var refreshEvent = $A.get('e.force:refreshView');
        console.log('3333'+refreshEvent);
        if(refreshEvent){
            console.log('4444');
            refreshEvent.fire();
            console.log('Refresh Event Fired');
            
        }
    },
    
    insertNewRecord : function(component,event,helper){
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Customer_Financials__c"
            
        });
        createRecordEvent.fire();
    },
})