({
    
doInit : function(component, event, helper) {        
        helper.getUserProfile(component, event, helper);
    	/*component.set('v.columns1', [
            {label: 'Financial Type', fieldName: 'Finance_Type__c', type: 'text'},
            {label: 'CY', fieldName: 'Current_Year__c', editable:'true', type: 'currency'},
            {label: '% Growth', fieldName: 'Growth_1__c', type: 'number'},
            {label: 'CY-1', fieldName: 'Current_Year_1__c', editable:'true', type: 'currency'},
            {label: '% Growth', fieldName: 'Growth_2__c', type: 'number'},
            {label: 'CY-2', fieldName: 'Current_Year_2__c', editable:'true', type: 'currency'}
        ]);
    	component.set('v.columns2', [
            {label: 'Financial Type', fieldName: 'Finance_Type__c', type: 'text'},
            {label: 'CY', fieldName: 'Current_Year__c', editable:'true', type: 'currency'},
            {label: 'CY-1', fieldName: 'Current_Year_1__c', editable:'true', type: 'currency'},
            {label: 'CY-2', fieldName: 'Current_Year_2__c', editable:'true', type: 'currency'}
        ]);
    	component.set('v.columns3', [
            {label: 'Financial Type', fieldName: 'Finance_Type__c', type: 'text'},
            {label: 'CY', fieldName: 'Current_Year__c', type: 'currency'},
            {label: 'Assessed CY', fieldName: 'Assessed_CY__c', editable:'true', type: 'currency'},
            {label: '% Growth', fieldName: 'Growth_1__c', type: 'number'},
            {label: 'Assessed % Growth', fieldName: 'Assessed_Growth_1__c', type: 'number'},
            {label: 'CY-1', fieldName: 'Current_Year_1__c', type: 'currency'},
            {label: 'Assessed CY-1', fieldName: 'Assessed_CY_1__c', editable:'true', type: 'currency'},
            {label: '% Growth', fieldName: 'Growth_2__c', type: 'number'},
            {label: 'Assessed % Growth', fieldName: 'Assessed_Growth_2__c', type: 'number'},
            {label: 'CY-2', fieldName: 'Current_Year_2__c', type: 'currency'},
            {label: 'Assessed CY-2', fieldName: 'Assessed_CY_2__c', editable:'true', type: 'currency'}
        ]);
    	component.set('v.columns4', [
            {label: 'Financial Type', fieldName: 'Finance_Type__c', type: 'text'},
            {label: 'CY', fieldName: 'Current_Year__c', type: 'currency'},
            {label: 'Assessed CY', fieldName: 'Assessed_CY__c', editable:'true', type: 'currency'},
            {label: 'CY-1', fieldName: 'Current_Year_1__c', type: 'currency'},
            {label: 'Assessed CY-1', fieldName: 'Assessed_CY_1__c', editable:'true', type: 'currency'},
            {label: 'CY-2', fieldName: 'Current_Year_2__c', type: 'currency'},
            {label: 'Assessed CY-2', fieldName: 'Assessed_CY_2__c', editable:'true', type: 'currency'}
        ]);*/
        
    	/*helper.getCustFins1(component, event, helper);
    	helper.getCustFins2(component, event, helper);
    	helper.getCustFins3(component, event, helper);
    	helper.getCustFins4(component, event, helper);
    	helper.getCustFins5(component, event, helper);*/
    },

    
    onSave1 : function (component, event, helper) {
        helper.saveDataTable1(component, event, helper); 
    },
    onSave2 : function (component, event, helper) {
        helper.saveDataTable2(component, event, helper);
    },
    onSave3 : function (component, event, helper) {
        helper.saveDataTable3(component, event, helper);
    },
    onSave4 : function (component, event, helper) {
        helper.saveDataTable4(component, event, helper);
    },
    onEdit : function (component, event, helper) {
        component.set('v.readonly',false);
    },
    onEdit2 : function (component, event, helper) {
        component.set('v.readonly2',false);
    },
 	onEdit3 : function (component, event, helper) {
        component.set('v.readonly3',false);
    },
	onEdit4 : function (component, event, helper) {
        component.set('v.readonly4',false);
    },
	onEdit5 : function (component, event, helper) {
        component.set('v.readonly5',false);
    },
    onCancel : function (component, event, helper) {
        component.set('v.readonly',true);
    },
    onCancel2 : function (component, event, helper) {
        component.set('v.readonly2',true);
    },
    onCancel3 : function (component, event, helper) {
        component.set('v.readonly3',true);
    },
    onCancel4 : function (component, event, helper) {
        component.set('v.readonly4',true);
    },
    onCancel5 : function (component, event, helper) {
        component.set('v.readonly5',true);
    }

})