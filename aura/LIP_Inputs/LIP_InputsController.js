({
    toggle1 : function(component, event, helper) {
        var toggleText = component.find("PLENTRY");
        $A.util.toggleClass(toggleText, "toggle");
    },
    toggle2 : function(component, event, helper) {
        var toggleText = component.find("BALANCE");
        $A.util.toggleClass(toggleText, "toggle");
    },
    toggle3 : function(component, event, helper) {
        var toggleText = component.find("RATIOS");
        $A.util.toggleClass(toggleText, "toggle");
    },
    
    // common reusable function for toggle sections
    toggleSection : function(component, event, helper) {
    	/*    
        // dynamically get aura:id name from 'data-auraId' attribute
        var sectionAuraId = event.target.getAttribute("data-auraId");
        // get section Div element using aura:id
        var sectionDiv = component.find(sectionAuraId).getElement();
        // The search() method searches for 'slds-is-open' class, and returns the position of the match.
        // This method returns -1 if no match is found.
        
        var sectionState = sectionDiv.getAttribute('class').search('slds-is-open'); 
        
        // -1 if 'slds-is-open' class is missing...then set 'slds-is-open' class else set slds-is-close class to element
        if(sectionState == -1){
            sectionDiv.setAttribute('class' , 'slds-section slds-is-open');
        }else{
            sectionDiv.setAttribute('class' , 'slds-section slds-is-close');
        }
        */
        
    },
	doInit : function(component, event, helper) {
        helper.doInit(component, event, helper);
	    helper.doInitisedit(component, event, helper);
	    helper.doInitiseditEstimateCol(component, event, helper);
        helper.doInitcustdetail(component, event, helper);
	},
    save : function(component, event, helper) {        
        var ischecked = component.find("isChecked").get("v.value");
        var checkit = component.get("v.Financials");
        var custid = component.get("v.Customerid");
        var action = component.get("c.SaveFinancials");
        var Financiallist = component.get("v.Financials");
        var loanAppId = component.get("v.loanAppId");
        if (ischecked === false || ischecked == '') {//|| isChecked
            action.setParams({
                "Financials":JSON.stringify(Financiallist),
                    'conId':custid,
                'loanAppId' : loanAppId
            });
            action.setCallback(this, function(response){
                
                var state = response.getState();
                if(state === "SUCCESS"){
                    component.set("v.iseditable",false);
                    component.set("v.Financials",response.getReturnValue());
                    helper.doInit(component, event, helper);
    	
                }else if (response.getState() === "ERROR"){
                    var errors = action.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            component.set("v.message", errors[0].message);
                        }
                    }
                }
            });
            $A.enqueueAction(action);
        }else if(ischecked) {
            var allValid = component.find('fieldId').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
       		}, true);
            if(allValid){
                action.setParams({
                    "Financials":Financiallist,
                        'conId':custid,
                    'loanAppId' : loanAppId
                });
                action.setCallback(this, function(response){
                    
                    var state = response.getState();
                    if(state === "SUCCESS"){
                        component.set("v.iseditable",false);
                        component.set("v.Financials",response.getReturnValue());
                        helper.doInit(component, event, helper);
            
                    }else if (response.getState() === "ERROR"){
                        var errors = action.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                component.set("v.message", errors[0].message);
                            }
                        }
                    }
                });
                $A.enqueueAction(action); 
            }
        }
        else {
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": "Please complete missing information."
                });
                toastEvent.fire();
        }
	},
    saveEst : function(component, event, helper) {
        
        var isChecked = component.find("isChecked").get("v.value");
        var checkit = component.get("v.Financials");
        var custid = component.get("v.Customerid");
        var allValid = component.find('fieldId').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        if (allValid || !isChecked) {//|| isChecked
            var action = component.get("c.SaveFinancials");
            var Financiallist = component.get("v.Financials")
            action.setParams({
                "Financials":JSON.stringify(Financiallist),
                    'conId':custid
            });
            action.setCallback(this, function(response){
                
                var state = response.getState();
                if(state === "SUCCESS"){
                    component.set("v.iseditableEst",false);
                    component.set("v.Financials",response.getReturnValue());
                    helper.doInit(component, event, helper);
    	
                }else if (response.getState() === "ERROR"){
                    var errors = action.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            component.set("v.message", errors[0].message);
                        }
                    }
                }
            });
            $A.enqueueAction(action);
        }else {
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": "Please complete missing information."
                });
                toastEvent.fire();
        }
	},
    edit : function(component, event, helper) {
		component.set("v.iseditable",true);
	},
    editEst : function(component, event, helper) {
		component.set("v.iseditableEst",true);
	},
    checkperfiosdata : function(component, event, helper) {
        helper.Checkdatafromperfios(component, event, helper);
    },
    handleConfirmDialog : function(component, event, helper) {
        component.set('v.showConfirmDialog', true);
    },
     
    handleConfirmDialogYes : function(component, event, helper) {
        console.log('Yes');
        helper.getdatafromperfios(component, event, helper);
    },
     
    handleConfirmDialogNo : function(component, event, helper) {
        console.log('No');
        component.set('v.showConfirmDialog', false);
    },
    handleConfirmDialogbox : function(component, event, helper) {
        component.set('v.showConfirmDialogbox', true);
    },
    handleConfirmDialogboxYes : function(component, event, helper) {
        helper.closeMe(component, event, helper);
    },
    handleConfirmDialogboxNo : function(component, event, helper) {
        component.set('v.showConfirmDialogbox', false);
    },
       // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
     
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    }
})