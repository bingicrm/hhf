({
	doInit : function(component, event, helper) {
        
        var action1 = component.get("c.GetYears");
        action1.setParams({
            
        });
        action1.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                if(response.getReturnValue() == null) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "", 
                        "type": "Error",
                        "message": 'Unknown Error occurred, Please contact system admin'
                    });
                    toastEvent.fire();
                } else {
                    component.set("v.CY",response.getReturnValue()[0]);
                    component.set("v.PY",response.getReturnValue()[1]);
                    component.set("v.BPY",response.getReturnValue()[2]);
                }
            }else if (response.getState() === "ERROR"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": 'Unknown Error occurred, Please contact system admin'
                });
                toastEvent.fire();
            }    
        });
        $A.enqueueAction(action1);
        
        
        var d = new Date();
        var n = d.getFullYear();
        component.set("v.currentyear", n);                
        var custid = component.get("v.Customerid");
        var loanAppId = component.get("v.loanAppId");
        
        var action = component.get("c.GetFinancials");
        action.setParams({
            'conId':custid,
            'loanAppId' : loanAppId,
        });
        action.setCallback(this, function(response){
            console.log('CHECK IS EDITABLE '+component.get("v.showEdit"));
            var state = response.getState();
            if(state === "SUCCESS"){
                if(response.getReturnValue() == null) {
                    console.log('NULL RESPONSE RECEIVED '+response.getReturnValue());
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "",
                            "type": "Error",
                            "message": 'Unknown Error occurred, Please contact system admin'
                        });
                        toastEvent.fire();
                    
                } else {
                    console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                    
                    component.set("v.Financials", response.getReturnValue()); 
                }    
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    
    doInitisedit : function(component, event, helper) {
		
        var custid = component.get("v.Customerid");
        var action = component.get("c.GetIseditable");
        action.setParams({
            'conId':custid
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                //console.log("response.getReturnValue()"+response.getReturnValue()[0].Loan_Type__c);
                
                component.set("v.iseditable", response.getReturnValue());     
                component.set("v.showEdit", response.getReturnValue());                
            
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
		
	},
	doInitiseditEstimateCol : function(component, event, helper) {
        var action = component.get("c.GetIseditableEst");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //console.log("response.getReturnValue()"+response.getReturnValue());
                component.set("v.showEstimate", response.getReturnValue());   
                component.set("v.iseditableEst", response.getReturnValue()); 
            
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
		
	},
    doInitcustdetail : function(component, event, helper) {
		
        var custid = component.get("v.Customerid");
        var action = component.get("c.GetCustdetails");
        action.setParams({
            'conId':custid
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                //console.log("response.getReturnValue()"+response.getReturnValue()[0].Loan_Type__c);
                
                //component.set("v.iseditable", response.getReturnValue());     
                component.set("v.contobj", response.getReturnValue());                
            
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
		
	},
    getdatafromperfios : function(component, event, helper) {
        
        var d = new Date();
        var n = d.getFullYear();
        component.set("v.currentyear", n);                
        var custid = component.get("v.Customerid");
        var loanAppId = component.get("v.loanAppId");
        var action = component.get("c.GetPerfiosData");
        action.setParams({
            'conId':custid,
            'loanAppId' :loanAppId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                console.log('in success'+ response.getReturnValue());
                component.set("v.gotperfios", response.getReturnValue());    
                if(response.getReturnValue() === false)
                {
                    //component.set('v.gotperfios', false);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                            "title": "",
                            "type": "Error",
                            "message": 'Unknown Error occurred, Please contact system admin'
                    });
                    toastEvent.fire();
                    component.set('v.showConfirmDialog', true);
                    console.log('in success get return value false::::'+response.getReturnValue());
    			
                }
                else{
                    component.set('v.showConfirmDialog', false);
                        helper.doInit(component, event, helper);
	
    			}
                    
                
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
        
    }
    ,
    Checkdatafromperfios : function(component, event, helper) {
        
        var d = new Date();
        var n = d.getFullYear();
        component.set("v.currentyear", n);                
        var custid = component.get("v.Customerid");
        var loanAppId = component.get("v.loanAppId");
        var action = component.get("c.CheckPerfiosData");
        action.setParams({
            'conId':custid,
            'loanAppId' : loanAppId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                console.log('in success'+ response.getReturnValue());
                component.set("v.gotperfios", response.getReturnValue());    
                if(response.getReturnValue() === false)
                {
                    component.set('v.gotperfios', false);
                        component.set('v.showConfirmDialog', true);
                    console.log('in success get return value false::::'+response.getReturnValue());
    			
                }
                else{
                    component.set('v.showConfirmDialog', true);
                        helper.doInit(component, event, helper);
	
    			}
                    
                
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
        
    },
     closeMe : function(comp, event, helper)  { 
        var custid = comp.get("v.Customerid");
        var recordIdNumber = '';
        var testLoanAppId = comp.get("v.loanAppId");
        
        var evt = $A.get("e.c:callCreateCamScreenEvent");
        evt.setParams({
            "recordIdNumber" : custid,
            "isOpen" : true,
            "testLoanAppId" : testLoanAppId
        });
        evt.fire();
	},
})