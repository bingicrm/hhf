({
	doInit : function(component, event, helper) {
		var action = component.get("c.getLoanCon");
        action.setParams({"lconId" : component.get("v.recordId")});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                if(actionResult.getReturnValue() != null){
                    component.set("v.lcon", response.getReturnValue());
                }
                else{
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title" : "ERROR!",
                        "type" : 'error',
                        "message" : "Operation Not Allowed."
                    });
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();                    
                }
            }else{
                
            }
        });
        $A.enqueueAction(action);
	},
    requestCreation: function(component, event, helper) {
        console.log('1111');
        helper.showSpinner(component);
         
        var action = component.get("c.checkGroupID");
		var loanCon = component.get("v.lcon");
        //console.log(loan);
        action.setParams({
            "lcID" : loanCon
        });
        console.log('2222');
        action.setCallback(this, function(actionResult) {
            console.log('3333');
            var state = actionResult.getState();
            if(state === "SUCCESS"){
                console.log('4444');
                var resultsToast = $A.get("e.force:showToast");
                
                if(actionResult.getReturnValue() != null){
                	resultsToast.setParams({
                    	"title" : "SUCCESS!",
                        "type" : 'success',
                    	"message" : "Customer has been added to LMS."
                	});
            	}
                else if(actionResult.getReturnValue() == null){
                    resultsToast.setParams({
                    	"title" : "ERROR!",
                        "type" : 'error',
                    	"message" : "Customer already exists in LMS."
                	});
                }
            	else{
                	resultsToast.setParams({
                    	"title" : "Error!",
                        "type" : 'success',
                    	"message" : actionResult.getReturnValue()
                	});
            	}
 
				$A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
                
            }else if(state === "ERROR"){
                
            }else{
            
            }
        });
        $A.enqueueAction(action);
    },
    
    cancelCreation: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})