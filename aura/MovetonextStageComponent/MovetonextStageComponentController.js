({
    doinit : function(component, event, helper) { // Added by saumya for TIL-00000747
        console.log('I am in doinit');
        component.set("v.Spinner", true); 

        var loanApp =  component.get("v.recordId") ;
        console.log(loanApp);
       
        var action = component.get('c.NextStage');
        action.setParams({
           
            "oppId": component.get("v.recordId")
        });
        action.setCallback(this, function(actionResult) {
            var msg;
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                var result = actionResult.getReturnValue();
                console.log('result::'+JSON.stringify(result));
                component.set('v.currentSubStage',result.currentStage);
                var nextStage = result.NextStage;
                console.log('NextStage '+nextStage);
                if(nextStage.includes('---')) {
                    component.set('v.isReasonOfMovement',true);
                    console.log('v.isReasonOfMovement' +component.get('v.isReasonOfMovement'));
                    var res = nextStage.split("---");
                    console.log('0-- ' +res[0]);   
                    console.log('1-- ' + res[1]);
                    component.set('v.NextSubStage', res[0]);
                    component.set('v.reasonOfMovement', 'Reason: '+res[1]);
                } else {
                    component.set('v.isReasonOfMovement',false);
                   component.set('v.NextSubStage',result.NextStage);
                   component.set('v.reasonOfMovement', '');
                }
               
                component.set("v.Spinner", false); 

            } 
            else 
            {
        		component.set("v.Spinner", true); 
            }
        });
        $A.enqueueAction(action);
    
      },
    onAccept : function(component, event, helper) {
         var loanApp =  component.get("v.recordId") ;
        console.log(loanApp);
       
        var action = component.get('c.moveNext');
        action.setParams({
           
            "oppId": component.get("v.recordId")
        });
        action.setCallback(this, function(actionResult) {
            var msg;
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                var result = actionResult.getReturnValue();
                if(result.error)
                {
                    console.log('Errror occurred');
                    var errorMess= actionResult.getReturnValue();
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                        'type': 'ERROR',
                		'message' : result.msg
                        }); 
                	showToast.fire();  
                	console.log('RELOADING SCREEN');
                	if(result.msg == 'Bureau response is pending') {
                	    window.location.reload();
                	}
                } 
                else
                {
                    console.log('why hwere');
                    var navEvent = $A.get("e.force:navigateToObjectHome");
                    navEvent.setParams({
                       "scope": "Loan_Application__c"
                    });
                    navEvent.fire();
                    msg = 'Your application has been moved to the following stage: '+ result.msg;
            		var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                        'type': 'Success',
                        'message' : msg
                     }); 
                	showToast.fire();
                }
                
            } 
            else 
            {
                var errors = actionResult.getError();
              
                var errorMess;

               	if (errors[0] && errors[0].message) 
                {
                        errorMess = "Error message: " + errors[0].message;
                    
                } 
                else 
                {
                    errorMess ="Please contact System admin"
                    console.log("Unknown error");
                }
            
                var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                    'title' : errorMess, 
                        'type': 'ERROR',
                'message' : errorMess
                        }); 
                showToast.fire();       
            }
                 var listviews = actionResult.getReturnValue();
            $A.get("e.force:closeQuickAction").fire();
            $A.get('e.force:refreshView').fire(); 
           
        });
        $A.enqueueAction(action);
    
      },
    
    doneNoAction : function(component, event, helper) {
        console.log(component.get('v.reasonOfMovement'));
        if(component.get('v.reasonOfMovement') == 'Reason: Bureau response is pending') {
            window.location.reload();
        }
        $A.get("e.force:closeQuickAction").fire();
    },
        showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }
})