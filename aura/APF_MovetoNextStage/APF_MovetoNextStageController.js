({
	doInit : function(component, event, helper) {
		var varAPFApplication =  component.get("v.recordId") ;
        console.log(varAPFApplication);
        
        var getWarningsAction = component.get('c.showWarningMsgs');
        getWarningsAction.setParams({
            "strAPFApplicationId": varAPFApplication
        });
        getWarningsAction.setCallback(this,function(getWarningsActionResult) {
            var state = getWarningsActionResult.getState();
            if (state === "SUCCESS") {
            	var result = getWarningsActionResult.getReturnValue();
                console.log('>>Warning Msgs'+result);
                if(result) {
                    console.log('Result Warning Msgs'+result);
                    if(result != null && result != ''){
                        component.set('v.WarningMsges',result);
                    	component.set('v.ShowWarnings',true);
                    }
                    else {
                        console.log('I am in Else'+result);
                        helper.onProceed(component,event,helper);
                        component.set("v.StageNotify", true);
                    }
                }
                else {
                    helper.onProceed(component,event,helper);
                }
            }
        });
        $A.enqueueAction(getWarningsAction);
    },
    
    onProceed : function(component, event, helper) {
        component.set("v.ShowWarnings", false);
        var action = component.get('c.initialSubStageNotification');
        var varAPFApplication =  component.get("v.recordId") ;
        action.setParams({
            "strAPFApplicationId": varAPFApplication
        });
        action.setCallback(this, function(actionResult) {
            var msg;
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                var result = actionResult.getReturnValue();
                console.log('result::'+JSON.stringify(result));
                if(result.currentStage != 'No Movement' && result.NextStage != 'No Movement'){
                console.log('I am in IF::'+JSON.stringify(result));
                component.set('v.currentSubStage',result.currentStage);
                component.set('v.NextSubStage',result.NextStage);
               	component.set("v.StageNotify", true); 
                component.set("v.Spinner", false); 
                }
                else{
                    component.set("v.StageNotify", false);
                    console.log('I am in Else::'+JSON.stringify(result));
                    //helper.initialValidation(component, event, helper);
                    component.set("v.Spinner", false); 
                }

            } 
            else 
            {
        		component.set("v.Spinner", true); 
            }
        });
        $A.enqueueAction(action);
    },
    
    onAccept : function(component, event, helper) {
        var varAPFApplication = component.get("v.recordId");
        component.set("v.StageNotify", false);
        console.log('I am in Accept');
        var action = component.get('c.saveProject');

        action.setParams({  
            "strAPFApplicationId": component.get("v.recordId")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") 
            {
                
                console.log('returnValue::'+actionResult.getReturnValue());
                var returnValue = actionResult.getReturnValue();
                //console.log('length::::'+returnValue.errorMsg.length);
                if(!$A.util.isUndefined(returnValue.errorMsg)){
                if(returnValue.errorMsg.length > 0){ //Added by Saumya for Submit for approval Issue
                console.log('I am in If'+returnValue.errorMsg.length);
                    component.set('v.operationAllowed',returnValue.success);
                    component.set("v.showErrors", true);
                component.set('v.errorMsges',returnValue.errorMsg);
                }
                else if(returnValue.errorMsg.length <= 0){
                console.log('I am in Else'+returnValue.errorMsg.length);
                component.set('v.operationAllowed',true);
                }
                }
                console.log('v.operationAllowed::'+component.get('v.operationAllowed'));
                component.set('v.previousStage',returnValue.prevStage);
                component.set('v.errorMsges',returnValue.errorMsg);
                console.log('errorMsges::'+component.get('v.errorMsges'));
                console.log('returnValue.success::'+returnValue.success +'returnValue.prevStage::'+returnValue.prevStage);
                if(returnValue.success && returnValue.prevStage)
                {
                    var showToast = $A.get("e.force:showToast"); 
					showToast.setParams({ 
                        'type': 'SUCCESS',
						'message' : returnValue.rtrnMsg
					}); 
                    showToast.fire(); 	
                    var navEvent = $A.get("e.force:navigateToList");
                            navEvent.setParams({
                        "listViewId": "00BO0000002RNQW",
                        "listViewName": "All",
                        "scope": "Project__c"
                    });
                    
                    navEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
            		$A.get('e.force:refreshView').fire();
                }
                
              
            }
        });
        $A.enqueueAction(action);
        
    },
    doneNoAction : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
})