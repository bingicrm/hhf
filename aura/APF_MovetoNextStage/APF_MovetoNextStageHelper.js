({
	onProceed : function(component, event, helper) {
        component.set("v.ShowWarnings", false);
        var action = component.get('c.initialSubStageNotification');
        var varAPFApplication =  component.get("v.recordId") ;
        action.setParams({
            "strAPFApplicationId": varAPFApplication
        });
        action.setCallback(this, function(actionResult) {
            var msg;
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                var result = actionResult.getReturnValue();
                console.log('result::'+JSON.stringify(result));
                if(result.currentStage != 'No Movement' && result.NextStage != 'No Movement'){
                console.log('I am in IF::'+JSON.stringify(result));
                component.set('v.currentSubStage',result.currentStage);
                component.set('v.NextSubStage',result.NextStage);
               	component.set("v.StageNotify", true); 
                component.set("v.Spinner", false); 
                }
                else{
                    component.set("v.StageNotify", false);
                    console.log('I am in Else::'+JSON.stringify(result));
                    //helper.initialValidation(component, event, helper);
                    component.set("v.Spinner", false); 
                }

            } 
            else 
            {
        		component.set("v.Spinner", true); 
            }
        });
        $A.enqueueAction(action);
    }
})