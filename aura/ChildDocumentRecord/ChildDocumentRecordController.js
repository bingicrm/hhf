({
    doInit: function(component, event, helper) {
        // call the fetchPickListVal(component, field_API_Name, aura_attribute_name_for_store_options) -
        // method for get picklist values dynamic 
        
        var capturedResponse = component.get("v.subStage");
        if(capturedResponse != 'Application Initiation' && capturedResponse != 'Scan Maker' 
           && capturedResponse != 'File Check' && capturedResponse != 'COPS:Data Maker' 
           && capturedResponse != 'COPS:Data Checker' && capturedResponse != 'Scan: Data Maker' 
           && capturedResponse != 'Scan: Data Checker' && capturedResponse != 'Scan Checker' 
           && capturedResponse != 'Docket Checker' && capturedResponse != 'Hand Sight' 
           && capturedResponse != 'OTC Receiving' && capturedResponse != 'Customer Negotiation' 
           && capturedResponse != 'Sales Approval' && capturedResponse != 'Disbursement Maker' 
           && capturedResponse != 'COPS: Credit Operations Entry' && capturedResponse != 'Disbursement Checker' 
           && capturedResponse !='Document Approval' && capturedResponse != 'CPC Scan Maker' && capturedResponse != 'CPC Scan Checker') {
            component.set("v.showforOtherStages", true);
        }
        helper.fetchPickListVal(component, 'Status__c','docPicklistOpts');
        helper.fetchPickListVal(component, 'Document_Collection_Mode__c','docCollectionModePicklistOpts');
        // alert();
    },
    
    editFileCheck : function(component,event,helper) {
        component.set("v.singleRec.Edited__c", true);
        component.set("v.showSaveCancelBtn",true);
    },
    
    handleUploadFinished : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "type": "success",
            "message": "Your File is Uploaded Successfully."
        });
        toastEvent.fire();
       // alert('File Uploaded Successfully.');
		/*** Added by Saumya For Green Highlight***/
            component.set("v.singleRec.Document_Uploaded__c", true);// Added by Saumya For Green Highlight
        console.log(' component.get("v.singleRec.Id")::'+ component.get("v.singleRec.Id"));
        var recId = component.get("v.singleRec.Id");
        var action = component.get("c.saveDocUploadCheckbox");
        action.setParams({
            "RecordId": recId
        });
        action.setCallback(this, function (response) {
            console.log('I have recieved Response');
        })
         /*** Added by Saumya For Green Highlight***/
	$A.enqueueAction(action);   
    },
    
    editDataEntryReq : function(component,event,helper) {
        //component.set("v.singleRec.Edited__c", true);
        component.set("v.showSaveCancelBtn",true);
    },
    
    editScanCheck : function(component,event,helper) {
        //component.set("v.singleRec.Edited__c", true);
        component.set("v.showSaveCancelBtn",true);
    },
    
    editScanCheckCompleted : function(component,event,helper) {
        component.set("v.singleRec.Edited__c", true);
        component.set("v.showSaveCancelBtn",true);
    },
    
    inlineEditStatus : function(component,event,helper){   
        // show the rating edit field popup 
        component.set("v.photocopyEditMode", true); 
        // after set ratingEditMode true, set picklist options to picklist field 
        component.find("accRating").set("v.options" , component.get("v.docPicklistOpts"));
        // after the 100 millisecond set focus to input field   
        setTimeout(function(){ 
            component.find("accRating").focus();
        }, 100);
    },
    
    onphotocopyChange : function(component,event,helper){ 
        // if picklist value change,
        // then show save and cancel button by set attribute to true
        component.set("v.showSaveCancelBtn",true);
    },     
    
    closePhotocopyBox : function (component, event, helper) {
        // on focus out, close the input section by setting the 'ratingEditMode' att. as false
        component.set("v.photocopyEditMode", false); 
    },
    
    inlineEditdocStatus : function(component,event,helper){   
        // show the rating edit field popup 
        component.set("v.docStatusEditMode", true); 
        // after set ratingEditMode true, set picklist options to picklist field 
        //component.find("accRating").set("v.options" , component.get("v.ratingPicklistOpts"));
        // after the 100 millisecond set focus to input field   
        setTimeout(function(){ 
            component.find("statusDoc").focus();
        }, 100);
    },
    
    ondocStatusChange : function(component,event,helper){ 
        // if picklist value change,
        // then show save and cancel button by set attribute to true
        component.set("v.showSaveCancelBtn",true);
    },     
    
    closedocStatusBox : function (component, event, helper) {
        // on focus out, close the input section by setting the 'ratingEditMode' att. as false
        component.set("v.docStatusEditMode", false); 
    },
    
    handleFilesChange: function(component, event, helper) {
        var fileName = 'No File Selected.';
        if (event.getSource().get("v.files").length > 0) {
            fileName = event.getSource().get("v.files")[0]['name'];
            //alert('fileName: '+fileName);
            helper.uploadHelper(component, event);
        }  
        component.set("v.fileName", fileName);
    },
    
    EditName : function(component,event,helper){  
        component.set("v.nameEditMode", true);
        
        component.set("v.showSaveCancelBtn",true);
        
        /*setTimeout(function(){
            component.find("inputId").focus();
        }, 100);*/
    },
    
    inlineEditRating : function(component,event,helper){
        var locked = component.get("v.singleRec.Logged__c");
        if(!locked){
            component.set("v.singleRec.Edited__c", true);
            component.set("v.StatusEditMode", true);
            component.set("v.showSaveCancelBtn",true);
            component.find("inputId").set("v.options",component.get("v.docPicklistOpts"));
        }
        else{
            
        }
    },
    
    inlineEditOTC : function(component){
        var locked = component.get("v.singleRec.Logged__c");
        if(!locked){
            component.set("v.singleRec.Edited__c", true);
            component.set("v.dateEditMode", true);
            component.set("v.showSaveCancelBtn",true);  
        }
    },
    
    inlineEditNotes : function(component){
        var locked = component.get("v.singleRec.Logged__c");
        if(!locked){
            component.set("v.singleRec.Edited__c", true);
            component.set("v.noteEditMode", true);
            component.set("v.showSaveCancelBtn",true);
        }        
    },
    
    inlineEditDocCollectionStatus : function(component,event,helper){
        component.set("v.singleRec.Edited__c", true);
        component.set("v.DocumentCollectionEditMode", true);
        component.set("v.showSaveCancelBtn",true);
        component.find("docCollectionModeID").set("v.options",component.get("v.docCollectionModePicklistOpts"));
    },
    
    Editbutton : function(component,event,helper){  
        var subStage = component.get("v.subStage");
        console.log('subStage::'+subStage);
        //alert('Here to Edit');
        //alert(subStage);
        //
        var locked = component.get("v.singleRec.Logged__c");
        //alert('Locking status'+locked);
        if(!locked){
            var isNameEditableTrue = component.get("v.singleRec.Document_Type__r.Document_Name_Editable__c");
            var stageName = component.get("v.singleRec.Loan_Applications__r.StageName__c");
            //alert('Name is editable??'+isNameEditableTrue);
            if(isNameEditableTrue && (stageName == 'Customer Onboarding' || stageName == 'Customer Acceptance')) {
                component.set("v.nameEditMode", true);
            }
            component.set("v.singleRec.Edited__c", true);
            component.set("v.StatusEditMode", true);
            //if(subStage != "File Check" && subStage != 'Scan: Data Checker' && subStage != 'Docket Checker') {Added by Ankit for TIL-00002496
            if(subStage != "File Check" && subStage != 'Scan: Data Checker') {	//Added by Ankit for TIL-00002496
                component.find("inputId").set("v.options",component.get("v.docPicklistOpts"));
            }
        }

        
        
        component.set("v.showSaveCancelBtn",true);
    },
    
    closeNameBox : function (component, event, helper) {
        component.set("v.nameEditMode", false);
        if(event.getSource().get("v.value") == ''){
            component.set("v.showErrorClass",true);
        }else{
            component.set("v.showErrorClass",false);
        }
        
    },
    
    closeStatusBox : function (component, event, helper) {
        component.set("v.StatusEditMode", false);
        component.set("v.dateEditMode", false);
    },
    
    closeStatusBoxDocCollection : function (component, event, helper) {
        component.set("v.DocumentCollectionEditMode", false);
    },
    
    changevalue : function (component, event, helper) {
        var lookupval = component.find("inputlookup").get("v.value");
        console.log('lookupval--> '+lookupval);
        component.find("inputfield").set("v.value",lookupval);
    },
    
    handleComponentEvent: function(component, event, helper) {
        var messageId = event.getParam("message");
        component.set("v.IdFromLookup",messageId);
        //alert('test...'+messageId);
        var lookupval = component.find("inputlookup").get("v.value");
        console.log('lookupval--> '+lookupval);
        component.find("inputfield").set("v.value",messageId);
        
    },
    
    closeModalDoc:function(component,event,helper){   
        //alert('Close');
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    updateDocMaster : function(component,event,helper){
        var docMaster = component.find("docMaster").get("v.value");
        component.set("v.singleRec.Document_Master__c",docMaster);
    },
    updateDocType : function(component,event,helper){
        var docType = component.find("docType").get("v.value");
        component.set("v.singleRec.Document_Type__c",docType);
    },
    inlineEditName : function(component,event,helper){  
        var locked = component.get("v.singleRec.Logged__c");
        var isNameEditableTrue = component.get("v.singleRec.Document_Type__r.Document_Name_Editable__c");
        var stageName = component.get("v.singleRec.Loan_Applications__r.StageName__c");
        console.log('StageName'+stageName);
        if(!locked && isNameEditableTrue && (stageName == 'Customer Onboarding' || stageName == 'Customer Acceptance')){
            console.log('Setting true from inlineEditName method.');
            component.set("v.singleRec.Edited__c", true);
            component.set("v.nameEditMode", true);
            component.set("v.showSaveCancelBtn",true);
        }
    },
    
    inlineEditType : function(component,event,helper){
        var locked = component.get("v.singleRec.Logged__c");
        if(!locked){
            component.set("v.singleRec.Edited__c", true);
            component.set("v.typeEditMode", true);
            component.set("v.showSaveCancelBtn",true);
        }
    },
    
    openmodalDoc: function(component,event,helper) {
        //alert('Open');
        /*var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); */
        var propertyId = component.get("v.singleRec.Id");
        //component.set("v.files", []);
        if (!propertyId) {
            return;
        }
        var action = component.get("c.queryAttachments");
        action.setParams({
            "propertyId": propertyId,
        });
        action.setCallback(this, function (response) {
            var files = response.getReturnValue();
            console.log(files);
            if(files != null) {
                var cusrecordIds = files.join();
                console.log('recordIds'+cusrecordIds);
                $A.get('e.lightning:openFiles').fire({
                    //recordIds: ['069p0000001E6ag'],
                    recordIds: files,
                });
            }
            else {
                var cmpTarget = component.find('Modalbox');
                var cmpBack = component.find('Modalbackdrop');
                $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                $A.util.addClass(cmpBack, 'slds-backdrop--open');
            }
            //component.set("v.files", files);
        });
        $A.enqueueAction(action);       
    }
})