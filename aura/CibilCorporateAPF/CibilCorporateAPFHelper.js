({
	callCIBIL : function(component, event) {
		var act = component.get("c.createCustomerIntegration");
                act.setParams({ strRecordId : component.get("v.recordId"),
                                hitCIBILAgain : component.get("v.hitCIBILAgain")
                              });   
                act.setCallback(this, function(response) 
                {
                    var state = response.getState(); 
                    console.log('response state'+state);
                    var resultsToast = $A.get("e.force:showToast");
                    if (state === "SUCCESS") 
                    {
                        console.log(response.getReturnValue());
                        if(response.getReturnValue().failure)
                        {
                            component.set("v.recordIntegrationId",response.getReturnValue().respString);
                            var action = component.get("c.integrateCibil");
                            action.setParams({ 
                                "strRecordId": component.get("v.recordId"),
                                "customerIntegrationId": component.get("v.recordIntegrationId")
                            });
                            
                            action.setCallback(this, function(response) {
                                
                                var state = response.getState(); 
                                if (state === "SUCCESS") 
                                {
                                	if(response.getReturnValue().failure)
                                    {
                                        var showToast = $A.get("e.force:showToast"); 
                                        showToast.setParams({ 
                                            'type': 'SUCCESS',
                                            'message' : 'Cibil Request Submitted'
                                        }); 
                                        showToast.fire(); 	
                                        
                                        $A.get("e.force:closeQuickAction").fire();
                                    }
                                    else
                                    {
                                        component.set('v.message',response.getReturnValue().respString);
                                        if(component.get("v.showYesNo") === true){
                                        component.set("v.showYesNo",false);
                                        }
                                    }
                                }
                                else if (state === "INCOMPLETE") {
                                    component.set('v.message','Some error occured');
                                }
                                else if (state === "ERROR") {
                                   component.set('v.message','Some error occured');
                                }
                            });
                            $A.enqueueAction(action);
                        }
                        else
                        {
                            component.set('v.message',response.getReturnValue().respString);
                            if ( response.getReturnValue().respString.includes('Are you sure you want to initiate again') ){
                                            component.set('v.showYesNo',true);
                            }
                            else
                                component.set('v.showYesNo',false);
                        }
                 
                        $A.get('e.force:refreshView').fire();  
                           
                    }
                   
                });
                $A.enqueueAction(act);
	}
})