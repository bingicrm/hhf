({
    doInitHelper : function(component, event, helper) 
    {
        var recordId = component.get("v.recordId");	
        
        var action = component.get("c.getLoanDownsizeInfo");
        action.setParams({loanApplicationID : recordId});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                
               // var response = JSON.stringify(response.getReturnValue());
                
                console.log('response '+response);
                if(response.getReturnValue())
                {
                    var response = JSON.stringify(response.getReturnValue());
                    console.log('Non Blank - Inside IF');
                    var parsedResponse =  JSON.parse(response);
                    console.log('parsedResponse+++ '+parsedResponse);
                    if( this.checkBlankString(parsedResponse) )
                    {
                        if(this.checkBlankString(parsedResponse.Status__c))
                        {
                          if(parsedResponse.Status__c=="Initiated" || parsedResponse.Status__c=="Approved")
                          {
                            component.set("v.buttonVisibility", false);
                            component.set("v.loanDownsizingID", parsedResponse.Id);
                            if(this.checkBlankString(parsedResponse.Remarks__c))
                            {
                                component.set("v.remarks",parsedResponse.Remarks__c);
                            }
                            this.toggleSpinner(component, event, helper, "hide");
                          }
                          else if(this.checkBlankString(parsedResponse.Status__c) && /*parsedResponse.Status__c=="Approved" ||*/ (parsedResponse.Status__c=="Rejected" || parsedResponse.Status__c=="Authorized"))
                        {
                            this.showToast(component, event, helper,'Loan Dowsizing is either Rejected or Authorized', 'error');
                            //hide the approve / reject button.
                            this.toggleSpinner(component, event, helper, "hide");
                        }
                        //console.log('Loan Downsizing not eligibile to get approved.');
                        }
                        
                    }
                   
                }
                else
                {
                    console.log('Show Toast - No or Multiple Loan Downsizing at Initiated Stage')
                    this.showToast(component, event, helper,'No or Multiple Loan Downsizing with status Initiated or approved.', 'error');
                    this.toggleSpinner(component, event, helper, "hide");
                }
            }
            else
            {
                console.log('Server Error');
                this.showToast(component, event, helper,'Server Error', 'error');
                    this.toggleSpinner(component, event, helper, "hide");
            }
            
        });
        $A.enqueueAction(action);
        
    },
    saveRecordHelper : function(component, event, helper)
    {
     	var recordId = component.get("v.loanDownsizingID");
        var selectedValue = component.get("v.selectedValue");
        var remarks = component.get("v.remarks");
        console.log('++recordId++ '+recordId);

        if(this.checkBlankString(remarks))
        {
			  console.log('Remarks not blank');
            // Call apex controller
            var action = component.get("c.approveRejectLoanDownsizing");
            action.setParams({ loanDownsizingID : recordId, statusValue : selectedValue, creditRemarks : remarks});
            action.setCallback(this, function(response){
                 var state = response.getState();
                if(state==='SUCCESS')
                {
                    var serverResponse = response.getReturnValue(); 
                    if(this.checkBlankString(serverResponse))
                    {
                        if(serverResponse=="Loan Downsizing updated successfully.")
                        {
                            this.showToast(component, event, helper, 'Loan Downsizing updated successfully.', "success");
                            this.toggleSpinner(component, event, helper, "hide");
                            $A.get('e.force:refreshView').fire();
                            $A.get("e.force:closeQuickAction").fire()
                        }
                        else
                        {
                          this.showToast(component, event, helper, serverResponse, "error");
                            this.toggleSpinner(component, event, helper, "hide");
                        }
                    }
                    else
                    {
                     this.showToast(component, event, helper, "Unexpected Error", "error"); 
                        this.toggleSpinner(component, event, helper, "hide");
                    }
                }
                else
                {
                 this.showToast(component, event, helper, "Unexpected Error", "error"); 
                    this.toggleSpinner(component, event, helper, "hide");
                }
            });
            $A.enqueueAction(action);
        }
        else
        {
            this.showToast(component, event, helper, "Credit Remarks Cannot be Blank","error");
            this.toggleSpinner(component, event, helper, "hide");
        }
        
    }, 
    checkBlankString : function(str)
    {
        console.log('str++ '+str);
        //console.log('str val+ '+str+' '+str.length);
        if (str == null ||  str == undefined || str.length == 0)
        {
            console.log('Blank String');
            return false;  
        }
        console.log('non null string');
        return true;
    },
    //method to show toast message
    showToast : function(component, event, helper, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": type.toUpperCase(),
            "message": message,
            "type" : type,
        });
        toastEvent.fire();
    },
    //Method to toggle spinner
    toggleSpinner : function(component, event, helper, str)
    {
        var findSpinner = component.find("mySpinner");
        if(str=="show")
        {
            $A.util.removeClass(findSpinner, "slds-hide");
            $A.util.addClass(findSpinner, "slds-show");   
        }
        else
        {
            $A.util.removeClass(findSpinner, "slds-show");
            $A.util.addClass(findSpinner, "slds-hide");   
        }
    },
})