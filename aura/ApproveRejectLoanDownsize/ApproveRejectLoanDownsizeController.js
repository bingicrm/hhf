({
    doInit : function(component, event, helper) 
    {
        helper.toggleSpinner(component, event, helper, "show");
        helper.doInitHelper(component, event, helper);	
    },
    saveRecord : function(component, event, helper)
    {
        helper.toggleSpinner(component, event, helper, "show");
        helper.saveRecordHelper(component, event, helper);
    },
})