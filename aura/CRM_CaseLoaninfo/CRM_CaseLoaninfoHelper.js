({
    getCaseLoanInfo : function(cmp, event, helper) {
        console.log('<----------------HELPER------------------->');
        
        var appId = cmp.get("v.SimpleRecord").LMS_Application_ID__c;
        var appNumber = cmp.get("v.SimpleRecord").Loan_Application_Number__c;
        var customerId = cmp.get("v.SimpleRecord").LD_Customer_ID__c;
        
        var action = cmp.get('c.getCaseLoanInfo');
        action.setParams(
            {
                "caseId": cmp.get("v.recordId"),
                "appId": appId,
                "appNumber": appNumber,
                "customerId": customerId
            }
        );
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set('v.calloutCheck',true);
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
                $A.get('e.force:refreshView').fire();
            }
            else if (state === "INCOMPLETE") {
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            let toastParams = {
                                title: "Error",
                                message: $A.get("$Label.c.CRM_CaseGenDocGenericCalloutError"),
                                type: "error"
                            };
                            
                            if (errors && Array.isArray(errors) && errors.length > 0) {
                                toastParams.message = errors[0].message;
                            }
                            
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams(toastParams);
                            var dismissActionPanel = $A.get("e.force:closeQuickAction");
                            
                            toastEvent.fire();
                            dismissActionPanel.fire();
                            $A.get('e.force:refreshView').fire();
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
})