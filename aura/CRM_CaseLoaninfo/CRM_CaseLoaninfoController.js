({
    doInit : function(component, event, helper) {
        console.log('<----------------doInit called!------------------->');
        var appId = component.get("v.SimpleRecord").LMS_Application_ID__c;
        var calloutCheck = component.get("v.calloutCheck");
        var CRM_CaseGenDocLAError = $A.get("$Label.c.CRM_CaseGenDocLAError");
        
        if(!appId){
            let toastParams = {
                title: "Error",
                message: CRM_CaseGenDocLAError,
                type: "error"
            };
            
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            
            toastEvent.fire();
            dismissActionPanel.fire();
            $A.get('e.force:refreshView').fire();
        }
        else if(appId && calloutCheck==false){
            helper.getCaseLoanInfo(component, event, helper);
        }
    },
    
    // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
    
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },
})