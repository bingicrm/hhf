({
	doInit : function(component, event, helper) {
		var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            workspaceAPI.setTabLabel({
                tabId: focusedTabId,
                label: "Hunter Verifications"
            });
            /*workspaceAPI.setTabIcon({
                tabId: focusedTabId,
                icon: "utility:", //set icon you want to set
                iconAlt: "FCU Verifications" //set label tooltip you want to set
            });*/
        })
        .catch(function(error) {
            console.log('Error==>'+error);
        });
        
        component.set('v.mycolumns', [
            {label: 'Third Party Verification Name', fieldName: 'linkName', type: 'url', 
             typeAttributes: {label: { fieldName: 'Name' }, target: '_self'}},
            {label: 'Customer Name', fieldName: 'Name_of_Applicant__c', type: 'text', sortable : true},
            {label: 'Loan Application Name', fieldName: 'lalink', type: 'url',
             typeAttributes: { label: { fieldName: 'laName' }, target: '_self'}, sortable : true},
            {label: 'Initiation Date', fieldName: 'Initiation_Date__c', type: 'date', typeAttributes: {  
                day: 'numeric',  
                month: 'short',  
                year: 'numeric',  
                hour: '2-digit',  
                minute: '2-digit',  
                second: '2-digit',  
                hour12: true}, sortable : true},
            {label: 'Owner Name', fieldName: 'Owner_Name__c', type: 'Text',sortable : true},
            {label: 'Requested Loan Amount', fieldName: 'Requested_Loan_Amount__c', type: 'currency'},
            {label: 'Product Type', fieldName: 'Product_Type__c', type: 'Text'},
            {label: 'Branch', fieldName: 'Branch__c', type: 'Text', sortable : true},
            {label: 'Record Type', fieldName: 'Record_Type_Name__c', type: 'Text'}
        ]);
            
        helper.getTPVList(component);
	},
    
    handleSort : function(component,event,helper){
        //Returns the field which has to be sorted
        var sortBy = event.getParam("fieldName");
        //returns the direction of sorting like asc or desc
        var sortDirection = event.getParam("sortDirection");
        //Set the sortBy and SortDirection attributes
        component.set("v.sortBy",sortBy);
        component.set("v.sortDirection",sortDirection);
        // call sortData helper function
        helper.sortData(component,sortBy,sortDirection);
    }
})