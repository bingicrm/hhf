({
	getTPVList: function(component) {
        var action = component.get('c.getHunterTPV');

        action.setCallback(this, function(actionResult){
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                var records =actionResult.getReturnValue();
                
                records.forEach(function(record){
                    record.linkName = '/'+record.Id;
                    record.laName = record.Loan_Application__r.Name;
                    record.lalink = '/'+record.Loan_Application__c;
                });
                component.set("v.tpvList", records);
            }
        });
        $A.enqueueAction(action);
    },
    
    //Below method added by Abhilekh on 30th October 2019 for FCU BRD
    sortData : function(component,fieldName,sortDirection){
        var data = component.get("v.tpvList");
        //function to return the value stored in the field
        var key = function(a) { return a[fieldName]; }
        var reverse = sortDirection == 'asc' ? 1: -1;
        
        // to handel number/currency type fields 
        /*if(fieldName == 'linkName' || fieldName == 'lalink'){ 
            data.sort(this.sortBy('Name', reverse))
        }*/
        //else{// to handel text type fields 
            data.sort(function(a,b){ 
                var a = key(a) ? key(a).toLowerCase() : '';//To handle null values , uppercase records during sorting
                var b = key(b) ? key(b).toLowerCase() : '';
                return reverse * ((a>b) - (b>a));
            });    
        //}
        //set sorted data to tpvList attribute
        component.set("v.tpvList",data);
    }
})