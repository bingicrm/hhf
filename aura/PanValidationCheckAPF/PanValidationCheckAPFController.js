({
	doInit : function(component, event, helper) {
        var action = component.get("c.checkPanDetails");
        
        action.setParams({ strRecordId : component.get("v.recordId") });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state==>'+state);
            
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                console.log('res==>'+res);
                if(response.getReturnValue().includes('PAN has been verified for this Builder! Are you sure you want to verify again?')){
                    component.set("v.message",res);
                    component.set("v.showYesNo",true);
                }
                else if(response.getReturnValue().includes('PAN has been verified for this Promoter! Are you sure you want to verify again?')){
                    component.set("v.message",res);
                    component.set("v.showYesNo",true);
                }
                else if(response.getReturnValue().includes('PAN has been already hit for verification. Please try after some time.')){
                    component.set("v.message",res);
                    component.set("v.showYesNo",false);
                }
                else if(response.getReturnValue().includes('Please specify PAN before initiating PAN Verification')){
                    component.set("v.message",res);
                    component.set("v.showYesNo",false);
                }
                else if(response.getReturnValue().includes('OK')){
                    component.set("v.showPANValidationCmp",true);
                    component.set("v.showMsg",false);
                    component.set("v.showYesNo",false);
                }
            }  
        });
        $A.enqueueAction(action);
    },
    cancelCreation : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
        $A.get('e.force:refreshView').fire();
    },
    validatePANagain : function(component, event, helper) {
        component.set("v.showPANValidationCmp",true);
        component.set("v.showMsg",false);
        component.set("v.showYesNo",false);
    }
})