({
    doInit : function(component, event, helper) { // Modified by saumya for TIL-00001205
        component.set("v.Spinner", true); 

        var loanApp =  component.get("v.recordId") ;
        console.log(loanApp);
       
        var action = component.get('c.initialSubStageNotification');
        action.setParams({
           
            "loanApplicationId": component.get("v.recordId")
        });
        action.setCallback(this, function(actionResult) {
            var msg;
            var state = actionResult.getState();
            console.log('State '+state );
            if (state === "SUCCESS") {
                var result = actionResult.getReturnValue();
                console.log('result::'+JSON.stringify(result));
                if(result.currentStage != 'No Movement' && result.NextStage != 'No Movement'){
                console.log('I am in IF::'+JSON.stringify(result));
                component.set('v.currentSubStage',result.currentStage);
                component.set('v.NextSubStage',result.NextStage);
               	component.set("v.StageNotify", true); 
                component.set("v.Spinner", false); 
                
                //Added by Vaishali for BRE2
                //var nextStage = result.NextStage;
                //console.log('NextStage '+nextStage);
                  //  if(nextStage.includes('---')) {
                    //    var res = nextStage.split("---");
                      //  component.set('v.NextSubStage',res[0]);
                        //component.set('v.reasonOfMovement', 'Reason: '+res[1]);
                    //} else {
                      //  component.set('v.NextSubStage',result.NextStage);
                    //    component.set('v.reasonOfMovement', '');
                    //} 
                //End of patch- Added by Vaishali for BRE2    
                }
                else{
                    	component.set("v.StageNotify", false);
                     console.log('I am in Else::'+JSON.stringify(result));
                    helper.initialValidation(component, event, helper);
                    component.set("v.Spinner", false); 
                }

            } 
            else 
            {
        		component.set("v.Spinner", true); 
            }
        });
        $A.enqueueAction(action);
    
      },
	/*doInit : function(component, event, helper) 
    {
        var loanApplicationId = component.get("v.recordId");
        
        var action = component.get('c.initialValidation');
        action.setParams({  
            "loanApplicationId": component.get("v.recordId")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") 
            {
                
                console.log(actionResult.getReturnValue());
                var returnValue = actionResult.getReturnValue();
                //console.log('length::::'+returnValue.errorMsg.length);
                if(returnValue.errorMsg.length > 0){ //Added by Saumya for Submit for approval Issue
                component.set('v.operationAllowed',returnValue.success);
                }
                else{
                component.set('v.operationAllowed',true);
                }
                component.set('v.previousStage',returnValue.prevStage);
                component.set('v.errorMsges',returnValue.errorMsg);
                console.log(component.get('v.errorMsges'));
               
                if(returnValue.success && returnValue.prevStage)
                {
                    var showToast = $A.get("e.force:showToast"); 
					showToast.setParams({ 
                        'type': 'SUCCESS',
						'message' : returnValue.rtrnMsg
					}); 
                    showToast.fire(); 	
                    var navEvent = $A.get("e.force:navigateToList");
                            navEvent.setParams({
                        "listViewId": loanApplicationId,
                        "listViewName": "All",
                        "scope": "Loan_Application__c"
                    });
                    
                    navEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
            		$A.get('e.force:refreshView').fire();
                }
                
              
            }
        });
        $A.enqueueAction(action);
	},*/
	callApproval : function(component, event, helper) 
    {
        
        var approvalAction = component.get('v.selectedValue');
        if(approvalAction == 'Approve')
        {
			var loanApplicationId = component.get("v.recordId");
 			component.set('v.forApproval',false);
            var action = component.get('c.moveToNextStage');
            action.setParams
            ({
                "loanApplicationId": component.get("v.recordId")
            });
			action.setCallback(this, function(actionResult) 
            {
            	var state = actionResult.getState();
				var result = actionResult.getReturnValue();
				console.log('result '+JSON.stringify(result));
                if (state === "SUCCESS") 
                {
                    var navEvent = $A.get("e.force:navigateToObjectHome");
                    navEvent.setParams({
                       "scope": "Loan_Application__c"
                    });
                    navEvent.fire();
                    
                    var msg = result.rtrnMsg;
					var successmsg = result.success; // Added by Saumya For L1 Credit Issue
                    console.log('msg '+msg + 'successmsg ' + successmsg);

                    if(successmsg == true){  
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                        'type': 'SUCCESS',
                        'message' : msg
                    }); 
                    showToast.fire();
					}
                    else{
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                        'type': 'ERROR',
                        'message' : msg
                    }); 
                    showToast.fire();    
                    }
                    //window.location.reload(); 
                    
                    

                    /*var navEvent = $A.get("e.force:navigateToList");
                    navEvent.setParams({
                        "listViewId": loanApplicationId,
                        "listViewName": "All",
                        "scope": "Loan_Application__c"
                    });
                    
                    navEvent.fire();*/
                    
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                    
                }
            });
            $A.enqueueAction(action);            
        }
        else
        {
            var loanApplicationId = component.get("v.recordId");
            component.set('v.forApproval',false);
            var action = component.get('c.assignToSalesUser');
            action.setParams({
                
                "loanApplicationId": component.get("v.recordId"),
                "comments":component.get("v.comments")
            });
            action.setCallback(this, function(actionResult) {
                var state = actionResult.getState();
                if (state === "SUCCESS") {   
                    var navEvent = $A.get("e.force:navigateToObjectHome");
                    navEvent.setParams({
                       "scope": "Loan_Application__c"
                    });
                    navEvent.fire();

                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                        'type': 'SUCCESS',
                        'message' : 'Application Rejected'
                    }); 
                    showToast.fire(); 	
                    /*var navEvent = $A.get("e.force:navigateToList");
                    navEvent.setParams({
                        "listViewId": loanApplicationId,
                        "listViewName": "All",
                        "scope": "Loan_Application__c"
                    });
                    
                    navEvent.fire();*/
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                    
                }
            });
            $A.enqueueAction(action);                
        }
        
	},
    doneNoAction : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
      onAccept : function(component, event, helper) {
          component.set("v.StageNotify", false);
          console.log('I am in Accept');
        helper.initialValidation(component, event, helper);
        //$A.get("e.force:closeQuickAction").fire();
    }
})