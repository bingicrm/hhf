({
    initialValidation : function(component, event, helper) { // Modified by saumya for TIL-00001205
        console.log('I am in Initial Validation');
        var loanApplicationId = component.get("v.recordId");
        console.log('loanApplicationId::'+loanApplicationId);
        var action = component.get('c.initialValidation');
        action.setParams({  
            "loanApplicationId": component.get("v.recordId")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") 
            {
                
                console.log('returnValue::'+ JSON.stringify(actionResult.getReturnValue()));
                var returnValue = actionResult.getReturnValue();
                //console.log('length::::'+returnValue.errorMsg.length);
                if(!$A.util.isUndefined(returnValue.errorMsg)){
                if(returnValue.errorMsg.length > 0){ //Added by Saumya for Submit for approval Issue
                console.log('I am in If'+returnValue.errorMsg.length);
                    component.set('v.operationAllowed',returnValue.success);
                     component.set("v.showErrors", true);
                    component.set('v.errorMsges',returnValue.errorMsg);
                }
                    else if(returnValue.errorMsg.length <= 0){
                        component.set('v.operationAllowed',true);
                    }
                }
                /*else{
                console.log('I am in else');
                component.set('v.operationAllowed',true);
                component.set('v.previousStage',true);    
                }*/
                else{
                component.set('v.operationAllowed',true);
                }
                console.log('v.operationAllowed::'+component.get('v.operationAllowed'));
                component.set('v.previousStage',returnValue.prevStage);
                component.set('v.errorMsges',returnValue.errorMsg);
                console.log('errorMsges::'+component.get('v.errorMsges'));
                console.log('returnValue.success::'+returnValue.success +'returnValue.prevStage::'+returnValue.prevStage);
                if(returnValue.success && returnValue.prevStage)
                {
                    var showToast = $A.get("e.force:showToast"); 
					showToast.setParams({ 
                        'type': 'SUCCESS',
						'message' : returnValue.rtrnMsg
					}); 
                    showToast.fire(); 	
                    var navEvent = $A.get("e.force:navigateToList");
                            navEvent.setParams({
                        "listViewId": loanApplicationId,
                        "listViewName": "All",
                        "scope": "Loan_Application__c"
                    });
                    
                    navEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
            		$A.get('e.force:refreshView').fire();
                }
                
              
            }
        });
        $A.enqueueAction(action);
	},
      
	checkMinRequirments : function(component) 
    {
		var loanApplicationId = component.get("v.recordId");
        var error = false; 
        component.set('v.forApproval',false);
        component.set('v.hasError',false);
        
        var action = component.get('c.getThirdPartyVerificationStatus');
        action.setParams({ 
            "loanApplicationId": component.get("v.recordId")
        });
        
        action.setCallback(this, function(actionResult) 
        {
            
            var state = actionResult.getState();
            if (state === "SUCCESS") 
            {     
                if(actionResult.getReturnValue() != null && actionResult.getReturnValue() == 'Some of third party verification has expired')
                {
                   	console.log('TPS error');
                    component.set('v.hasError',true);
                    component.set('v.errorTPS',actionResult.getReturnValue());
                }
            }
        });
        $A.enqueueAction(action);
        
        var action2 = component.get('c.getPersonalDiscussionDetails');
        action2.setParams({ 
            "loanApplicationId": component.get("v.recordId")
        });
        action2.setCallback(this, function(actionResult) 
        {
            var state = actionResult.getState();
            if (state === "SUCCESS") 
            {     
                if(actionResult.getReturnValue() != null && actionResult.getReturnValue() == 'PD has not been completed for all the customer in loan application')
                {
                    console.log('PD Error');
                    component.set('v.hasError',true);
                    component.set('v.errorPD',actionResult.getReturnValue());
                }
            }
        });
       
        $A.enqueueAction(action2);
        
	}
})