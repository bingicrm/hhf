({
	doInit : function(component, event, helper) 
    {
        component.set("v.Spinner", true); 
        
        var loanApp =  component.get("v.recordId") ;
        console.log(loanApp);
        
        var action = component.get('c.beforeMovingToHunterReview');
        action.setParams({
            "laId": loanApp
        });
        action.setCallback(this, function(actionResult) {
            var msg;
            var state = actionResult.getState();
            if (state === "SUCCESS"){
                var result = actionResult.getReturnValue();
                console.log('result::'+JSON.stringify(result));
                component.set('v.currentSubStage',result.currentStage);
                component.set('v.NextSubStage',result.NextStage);
                component.set("v.Spinner", false); 
            } 
            else{
            	component.set("v.Spinner", true); 
            }
        });
        $A.enqueueAction(action);
	},
    
    onAccept : function(component, event, helper) {
        component.set('v.showBlockOption',false);
        component.set('v.showBlock',true);
    },
    
    onSave : function(component, event, helper) {
        var validate = true;
        var loanApp =  component.get("v.recordId") ;
        console.log(loanApp);
        
        var commField = component.find("comm");
        var comm = commField.get("v.value");
        
        if($A.util.isEmpty(comm)){
            validate = false;
            commField.set("v.errors", [{message:"Comments can't be blank."}]);
        }
        else{
            commField.set("v.errors", null);
        }
        
        if(validate){
            var action = component.get('c.moveToHunterReview');
            action.setParams({
                "laId": loanApp,
                "comments": component.get("v.comments")
            });
            action.setCallback(this, function(actionResult) {
                var msg;
                var state = actionResult.getState();
                if (state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    if(result.error){
                        var errorMess= actionResult.getReturnValue();
                        var showToast = $A.get("e.force:showToast"); 
                        showToast.setParams({ 
                            'type': 'ERROR',
                            'message' : result.msg
                        }); 
                        showToast.fire();   
                    } 
                    else{
                        console.log('======');
                        var navEvent = $A.get("e.force:navigateToObjectHome");
                        navEvent.setParams({
                            "scope": "Loan_Application__c"
                        });
                        navEvent.fire();
                        msg = 'Your application has been moved to the following stage: '+ result.msg;
                        var showToast = $A.get("e.force:showToast"); 
                        showToast.setParams({ 
                            'type': 'Success',
                            'message' : msg
                        }); 
                        showToast.fire();
                    }
                } 
                else{
                    var errors = actionResult.getError();
                    var errorMess;
                    
                    if (errors[0] && errors[0].message) {
                        errorMess = "Error message: " + errors[0].message;
                    } 
                    else {
                        errorMess ="Please contact System admin"
                        console.log("Unknown error");
                    }
                    
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                        'title' : errorMess, 
                        'type': 'ERROR',
                        'message' : errorMess
                    }); 
                    showToast.fire();       
                }
                var listviews = actionResult.getReturnValue();
                $A.get("e.force:closeQuickAction").fire();
                $A.get('e.force:refreshView').fire(); 
                
            });
            $A.enqueueAction(action);
        }
    },
    
    doneNoAction : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    
    showSpinner: function(component, event, helper) {
        //Make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    //This function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        //Make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    }
})