({
    getLastPaymentDetails : function(cmp) {
    var action = cmp.get("c.getResponse");
    action.setParams({
        "caseId" : cmp.get("v.recordId")
    });
    action.setCallback(this, function(response){
        var state = response.getState();
        if (state === "SUCCESS") {
            console.log('response.getReturnValue()',response.getReturnValue());
            if(response.getReturnValue()!=null){
              cmp.set("v.LastPaymentDetails", response.getReturnValue());
            cmp.set("v.isCaseDetail", true);
            }
            else{
                cmp.set("v.noCaseDetail", true);
            }
        }
        else{
            cmp.set("v.serverError", true);
        }
    });
    $A.enqueueAction(action);
}
})