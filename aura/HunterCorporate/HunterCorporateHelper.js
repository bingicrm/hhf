({
    callHunter : function(component, event) {
        var action = component.get("c.checkHunterDeclineAndFCUDecline");
        action.setParams({
            loanAppId : component.get("v.recordId")
        })
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var message = response.getReturnValue();
                console.log('message==>'+message);
                
                if(message == '' || message == null){
                    console.log('Inside hunter');
                    var act = component.get("c.checkCorporateHunterEligibility");
                    act.setParams({ loanAppId : component.get("v.recordId") });   
                    act.setCallback(this, function(response) {
                        var state = response.getState(); 
                        //var resultsToast = $A.get("e.force:showToast");
                        if (state === "SUCCESS") 
                        {
                            console.log('response==>'+response.getReturnValue());
                            if(response.getReturnValue().includes('Online Hunter is not applicable'))
                            {
                                var message = response.getReturnValue();
                                console.log('message==>'+message);
                                var showToast = $A.get("e.force:showToast"); 
                                showToast.setParams({ 
                                    'type': 'Error',
                                    'message' : message
                                });
                                showToast.fire(); 	
                                
                                $A.get("e.force:closeQuickAction").fire();
                                
                            }
                            else if(response.getReturnValue() == '' || response.getReturnValue() == null)
                            {
                                var message = response.getReturnValue();
                                var showToast = $A.get("e.force:showToast"); 
                                showToast.setParams({ 
                                    'type': 'Success',
                                    'message' : 'Hunter Analysis has been initiated successfully.'
                                }); 
                                showToast.fire(); 	
                                
                                $A.get("e.force:closeQuickAction").fire();
                            }
                                else{
                                    var showToast = $A.get("e.force:showToast"); 
                                    showToast.setParams({ 
                                        'type': 'Error',
                                        'message' : 'An error has occurred. Please contact System Admin.'
                                    }); 
                                    showToast.fire();
                                }
                            $A.get('e.force:refreshView').fire();
                            
                        }
                        else{
                            var showToast = $A.get("e.force:showToast"); 
                            showToast.setParams({ 
                                'type': 'Error',
                                'message' : 'An error has occurred. Please contact System Administrator.'
                            }); 
                            showToast.fire(); 	
                            
                            $A.get("e.force:closeQuickAction").fire();
                        }
                    });
                    $A.enqueueAction(act);
                }
                else{
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                        'type': 'Error',
                        'message' : message
                    });
                    showToast.fire(); 	
                    
                    $A.get("e.force:closeQuickAction").fire();
                }
            }
            else{
                var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({ 
                    'type': 'Error',
                    'message' : 'An error has occurred. Please contact System Administrator.'
                }); 
                showToast.fire(); 	
                
                $A.get("e.force:closeQuickAction").fire();
            }
        });
        $A.enqueueAction(action);
    }
})