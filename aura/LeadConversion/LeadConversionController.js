({   

doInit : function(component, event) {       

// Call the Apex Class Method from Javascript       
var recordId = component.get("v.recordId");
var action = component.get("c.convertLead");       
                  
// Place the Action in the Queue    
action.setParams({
        "idRecordId":recordId
    });
action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                if (response.getReturnValue().includes('Unsuccessfull')) {
                    if(response.getReturnValue().split("$")[1].includes('DUPLICATES_DETECTED')){
                		component.set("v.message", "Duplicate customer detected<br/> Redirecting to opportunity...");
                        setTimeout(function() { var urlEvent = $A.get("e.force:navigateToURL");
                                               
                                               if(window.location.pathname.includes('hhfllos')){
                                                   
                                                urlEvent.setParams({
                                                    "url": "/hhfllos/s/detail/" + response.getReturnValue().split("$")[2]
                                                     
                                                });
                                                   
                                               } else {
                                                   urlEvent.setParams({
                                                    "url": "/" + response.getReturnValue().split("$")[2]
                                                    });
                                               }   
                                                urlEvent.fire();
                                              
                                              }, 2000);
                    } else {
                        component.set("v.message", 'Lead not converted, please contact System Admin. <br/> ' + response.getReturnValue().split("$")[1]);
                    }
                } else {
                   		component.set("v.message", "Lead converted successfully! <br/> Redirecting to Customer...");
                        setTimeout(function() { var urlEvent = $A.get("e.force:navigateToURL");
                                                if(window.location.pathname.includes('hhfllos')){
                                                   
                                                urlEvent.setParams({
                                                    "url": "/hhfllos/s/detail/" + response.getReturnValue()
                                                     
                                                });
                                                   
                                               } else {
                                                   urlEvent.setParams({
                                                    "url": "/" + response.getReturnValue()
                                                    });
                                               } 
                                                urlEvent.fire();
                                              
                                              }, 2000);
                }    
            } else {
                alert("Failed with state: " + state);
                
                component.set("v.message", "An error occurred, please contact System Administrator");
            }
        });    
$A.enqueueAction(action);   

}

})