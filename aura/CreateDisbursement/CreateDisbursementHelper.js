({
	createRecord : function(component, event) {
        var createRecordEvent = $A.get("e.force:createRecord");
        var recId = component.get("v.recTypeId");
        console.log("loanId : " + component.get("v.loanId"));
        console.log("recordId : " + component.get("v.recordId"));
        console.log('Changed');
        createRecordEvent.setParams({
            "entityApiName": "Disbursement__c",
            "recordTypeId": recId,
            "defaultFieldValues" :{
                "Loan_Application__c" : component.get("v.loanId"),
                "Tranche__c" : component.get("v.recordId")
            }
        });
        createRecordEvent.fire();  
	}
})