({
    doInit : function(component, event, helper){
        var recordId = component.get("v.recordId");
        var actiongetRT = component.get("c.getRecTypeId");
        actiongetRT.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS" && response.getReturnValue()!= null){
                component.set("v.recTypeId", response.getReturnValue());

                var action  =  component.get("c.getLoanId");
                action.setParams({
                    "recordId" : recordId
                });
                action.setCallback(this, function(response1){
                    var stateIN = response1.getState();
                    if(stateIN == "SUCCESS" && response1.getReturnValue()!= null){                              
                        
                        component.set("v.loanId", response1.getReturnValue());
                        
                        var recordId = component.get("v.recordId");
                        var loanId = component.get("v.loanId");
                        helper.createRecord(component, event);
                    }
                });
                $A.enqueueAction(action);
            }
        });
        $A.enqueueAction(actiongetRT);

        
    },
})