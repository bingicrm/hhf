({
	initRecords: function(component, event, helper) {
		var action = component.get("c.fetchSubStage");
        action.setParams({
            'strRecordId': component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            //alert(state);
            if (state === "SUCCESS") {
                var Response = response.getReturnValue();
                console.log('Response received'+JSON.stringify(Response));
															  
                // set SubStage attribute with return value from server.
                var capturedResponse = Response.Sub_Stage__c;
                component.set("v.subStage", Response.Sub_Stage__c);
                //component.set("v.subStage", 'Scan Checker');
                if(capturedResponse != 'File Checker' && capturedResponse != 'Scan Maker'  && capturedResponse != 'Scan Checker') {
                    component.set("v.showforOtherStages", true);
                }
                
                component.set("v.showNewButton", true);
                
                var action1 = component.get("c.fetchDocuments");
                action1.setParams({
                    'strRecordId': component.get("v.recordId")
                });
                action1.setCallback(this, function(response) {
                    var state = response.getState();
                    //alert(state);
                    if (state === "SUCCESS") {
                        var storeResponse = response.getReturnValue();
                        console.log('List received'+storeResponse);
                        //component.set("v.DocList", storeResponse);
                        console.log(JSON.stringify(storeResponse));
                        // set AccountList list with return value from server.
                        component.set("v.DocList1", storeResponse);
                    }
                });
                $A.enqueueAction(action1);
            }
        });
        $A.enqueueAction(action);
	},
    createDocRecord : function(component, event, helper){	
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Project_Document_Checklist__c",
            "defaultFieldValues":{
                "Project__c" : component.get("v.recordId")
            },
            "panelOnDestroyCallback": function(event) {
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": component.get("v.recordId"),
                    "slideDevName": "related"
                });
                navEvt.fire();
            }
        });
        createRecordEvent.fire();
    },
    cancel : function(component,event,helper){
        component.set("v.showSaveCancelBtn",false);
        $A.get('e.force:refreshView').fire(); 
    },
    Save : function(component, event, helper) {
        //console.log('DocList'+component.get("v.DocList1")[0].lstDocumentCheckList[0].Id); 
        var actionValidation = component.get("c.validateChkList");
        console.log('Param Sent'+JSON.stringify(component.get("v.DocList1")));
        actionValidation.setParams({
            "paramJSONList": JSON.stringify(component.get("v.DocList1"))
        });
        actionValidation.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS" && component.isValid()) {
                var resultMsg = response.getReturnValue();
                console.log('Result Message from validateChkList'+resultMsg);
                if(resultMsg == "Successful"){
                    var action = component.get("c.saveLoan");
                    console.log('Param Sent'+JSON.stringify(component.get("v.DocList1")));
                    action.setParams({
                        "paramJSONList": JSON.stringify(component.get("v.DocList1"))
                    });
                    action.setCallback(this, function(response) {
                        if (response.getState() === "SUCCESS" && component.isValid()) {
                            var errorString = response.getReturnValue();
                            console.log('Response msg'+errorString);
                            console.log('Error Msg :'+errorString[0].errorMsg);
                            if( errorString[0].errorMsg != ""){
                                component.set("v.ErrorMsg", errorString[0].errorMsg);
                                component.set("v.showLockedError",true);
                                var cmpTarget = component.find('ModalboxError1');
                                var cmpBack = component.find('ModalbackdropError1');
                                $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                                $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
                                console.log("Boolean"+component.get("v.showLockedError"));
                            }                                                               
                            if( errorString[0].errorMsg == "" ){
                                component.set("v.DocList1", response.getReturnValue());
                                component.set("v.showSaveCancelBtn",false);
                                window.location.reload(true);
                            }
                        }
                        /*if(response.getReturnValue() != null) {
                                window.location.reload(true);
                            }*/
                        else {
                                component.set('v.showErrorMessage' , true);
                                component.set('v.errorMessage','Error Occured During Update. Please validate your changes.');
                        }
                    });
                    $A.enqueueAction(action);
                }
                else {
                    //alert('Cannot Update the Status when there is no file associated with the record.');
                    component.set("v.showSaveCancelBtn",false);
                    component.set("v.showError",true);
                    var cmpTarget = component.find('ModalboxError');
                    var cmpBack = component.find('ModalbackdropError');
                    $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                    $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
                    
                }
            }
        });
        $A.enqueueAction(actionValidation);
    },
    closeModalDocError:function(component,event,helper){
        component.set("v.showError",false);
        component.set("v.showLockedError",false);
        var cmpTarget = component.find('ModalboxError');
        var cmpBack = component.find('ModalbackdropError');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        
        var cmpTarget1 = component.find('ModalboxError1');
        var cmpBack1 = component.find('ModalbackdropError1');
        $A.util.removeClass(cmpBack1,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget1, 'slds-fade-in-open');
    },
    close :function(component,event,helper){
        $A.get("e.force:closeQuickAction").fire();
    }
})