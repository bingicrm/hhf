({
	initRecords: function(component, event, helper) {
         var action = component.get("c.fetchDocList");
         action.setParams({
                    'tpvId': component.get("v.recordId")
                  });
             action.setCallback(this, function(response) {
              var state = response.getState();
              if (state === "SUCCESS") {
                  //alert(response.getReturnValue());
                  var capturedResponse = response.getReturnValue();
                  //console.log('Response received:'+capturedResponse);
                  component.set("v.docCheckList", capturedResponse);
                  console.log("Doc Checklist value : " + JSON.stringify(component.get("v.docCheckList")));
            }
        });
        $A.enqueueAction(action);
        helper.fetchPickListVal(component, 'Document_Collection_Mode__c', 'modeOptions');
        helper.fetchPickListVal(component, 'Screened_p__c', 'scrOptions');
        helper.fetchPickListVal(component, 'Sampled_p__c', 'samOptions');
        //helper.docStatusPickListVal(component, event, helper);
        //helper.docSampledPickListVal(component, event, helper);
        //helper.docScreenedPickListVal(component, event, helper);
    },
    
    Save : function(component, event, helper) {
        	var action = component.get("c.saveLoan");
        	console.log('Param Sent'+JSON.stringify(component.get("v.docCheckList")));
            action.setParams({
                "paramJSONList": JSON.stringify(component.get("v.docCheckList"))
            });
            action.setCallback(this, function(response) {
                //alert('12121');
                if (response.getState() === "SUCCESS" && component.isValid()) {
                    component.set("v.DocList1", response.getReturnValue());
                    component.set("v.showSaveCancelBtn",false);
                }
                window.location.reload(true);
            });
            $A.enqueueAction(action);
        
    },
    cancel : function(component,event,helper){
        component.set("v.showSaveCancelBtn",false);
        component.set("v.StatusEditMode", false);
        component.set("v.StatusEditScr", false);
        component.set("v.StatusEditSam", false);
        $A.get('e.force:refreshView').fire(); 
    },
    fetchAllDocuments : function(component, event, helper){
        var docId = component.get("v.recordId");
        if (!docId) {
            return;
        }
        var action = component.get("c.queryAttachments");
        action.setParams({
            "docId": docId,
        });
        action.setCallback(this, function (response) {
            var files = response.getReturnValue();
            console.log(files);
            if(files != null) {
                var cusrecordIds = files.join();
                console.log('recordIds'+cusrecordIds);
                $A.get('e.lightning:openFiles').fire({
                    recordIds: files,
                });
            }
            else {
                var cmpTarget = component.find('Modalbox');
                var cmpBack = component.find('Modalbackdrop');
                $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
            }
            //component.set("v.files", files);
        });
        $A.enqueueAction(action);
    },
    closeStatusBox : function (component, event, helper) {
        var selected = component.get("v.data.Screened_p__c");
        console.log('selection'+selected);
        component.set("v.StatusEditMode", false);
    },
    inlineEditMode : function(component,event,helper){
        //console.log('called');
        //alert('called');
        component.set("v.StatusEditMode", true);
        component.set("v.showSaveCancelBtn",true);
        console.log(component.get("v.modeOptions"));
        //component.find("inputMode").set("v.options",component.get("v.modeOptions"));
        
        //component.find("inputScr").set("v.options",component.get("v.scrOptions"));
        //component.find("inputSam").set("v.options",component.get("v.samOptions"));
    },
    inlineEditScr : function(component,event,helper){
        //console.log('called');
        //alert('called');
        component.set("v.StatusEditScr", true);
        component.set("v.showSaveCancelBtn",true);
        console.log(component.get("v.scrOptions"));
        //component.find("inputMode").set("v.options",component.get("v.modeOptions"));
        //component.find("inputScr").set("v.options",component.get("v.scrOptions"));
        
        //component.find("inputSam").set("v.options",component.get("v.samOptions"));
    },
    inlineEditSam : function(component,event,helper){
        //console.log('called');
        //alert('called');
        component.set("v.StatusEditSam", true);
        component.set("v.showSaveCancelBtn",true);
        console.log(component.get("v.samOptions"));
        //component.find("inputMode").set("v.options",component.get("v.modeOptions"));
        //component.find("inputScr").set("v.options",component.get("v.scrOptions"));
        //component.find("inputSam").set("v.options",component.get("v.samOptions"));
        
    },
    Editbutton : function(component,event,helper){  
        
        component.set("v.StatusEditMode", true);
        component.set("v.StatusEditScr", true);
        component.set("v.StatusEditSam", true);
        component.set("v.showSaveCancelBtn",true);
        
        //component.find("inputMode").set("v.options",component.get("v.modeOptions"));
        //component.find("inputScr").set("v.options",component.get("v.scrOptions"));
        //component.find("inputSam").set("v.options",component.get("v.samOptions"));
        console.log(component.get("v.modeOptions"));
        console.log(component.get("v.scrOptions"));
        console.log(component.get("v.samOptions"));
    },
    onPicklistChange: function(component, event, helper) {
        // get the value of select option
        alert(event.getSource().get("v.value"));
    },
    closeModalDoc:function(component,event,helper){   
        //alert('Close');
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    openmodalDoc: function(component,event,helper) {
        //alert('Open');
        /*var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); */
        var propertyId = component.get("v.singleRec.Id");
        //component.set("v.files", []);
        if (!propertyId) {
            return;
        }
        var action = component.get("c.queryAttachments");
        action.setParams({
            "propertyId": docId,
        });
        action.setCallback(this, function (response) {
            var files = response.getReturnValue();
            console.log(files);
            if(files != null) {
                var cusrecordIds = files.join();
                console.log('recordIds'+cusrecordIds);
                $A.get('e.lightning:openFiles').fire({
                    //recordIds: ['069p0000001E6ag'],
                    recordIds: files,
                });
            }
            else {
                var cmpTarget = component.find('Modalbox');
                var cmpBack = component.find('Modalbackdrop');
                $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                $A.util.addClass(cmpBack, 'slds-backdrop--open');
            }
            //component.set("v.files", files);
        });
        $A.enqueueAction(action);
        
        
    }
})