({
	fetchPickListVal: function(component, fieldName, elementId) {
        //alert(component.get("v.objInfo")+'---'+fieldName);
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.objInfo"),
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            //alert(response.getState());
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
 
               /* if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }*/
                //alert(elementId);
                //component.find(elementId).set("v.options", opts);
                console.log('allValues---'+allValues);
                component.set('v.'+elementId,allValues);
                console.log('---'+component.get("v."+elementId));
            }
        });
        $A.enqueueAction(action);
    },
    docStatusPickListVal: function(component, event, helper) {
        var action = component.get("c.getMode");
        var mode = component.find("inputMode");
        var opts=[];
        action.setCallback(this, function(a) {
            opts.push({
                class: "optionClass",
                label: "--- None ---",
                value: ""
            });
            for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            mode.set("v.options", opts);
             
        });
        $A.enqueueAction(action);
    },
    docScreenedPickListVal: function(component, event, helper) {
        var action = component.get("c.getScreened");
        var scr = component.find("inputScr");
        var opts=[];
        action.setCallback(this, function(a) {
            opts.push({
                class: "optionClass",
                label: "--- None ---",
                value: ""
            });
            for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            scr.set("v.options", opts);
             
        });
        $A.enqueueAction(action);
    },
    docSampledPickListVal: function(component, event, helper) {
        var action = component.get("c.getSampled");
        var sam = component.find("inputSam");
        var opts=[];
        action.setCallback(this, function(a) {
            opts.push({
                class: "optionClass",
                label: "--- None ---",
                value: ""
            });
            for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            sam.set("v.options", opts);
             
        });
        $A.enqueueAction(action);
    }
})