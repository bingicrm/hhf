({
	helperMethod : function(component, event, helper) {
		console.log('In getsalaryincome');
        var custDetid = component.get("v.Customerid");
        var action = component.get("c.getSalariedIncome");//get data from controller
        action.setParams({
            'cusDtId':custDetid
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //console.log('In success');
                //console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                component.set("v.CustDetails", response.getReturnValue());    
                console.log('custDetails '+JSON.stringify(component.get("v.CustDetails")));
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
	},
    doInitisedit : function(component, event, helper) {
        
        var custid = component.get("v.Customerid");
        var action = component.get("c.GetIseditable");
        action.setParams({
            'conId':custid
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                //console.log("response.getReturnValue()"+response.getReturnValue()[0].Loan_Type__c);
                
                component.set("v.iseditable", response.getReturnValue());     
                component.set("v.showEdit", response.getReturnValue());                
                
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    closeMe : function(comp, event, helper)  { 
        var custid = comp.get("v.Customerid");
        var testLoanAppId = comp.get("v.loanAppId");
        
        console.log('custid '+custid);
        var recordIdNumber = '';
        var evt = $A.get("e.c:callCreateCamScreenEvent");
        evt.setParams({
            "recordIdNumber" : custid,
            "isOpen" : true,
            "testLoanAppId" : testLoanAppId
        });
        evt.fire();
	},
})