({
    toggle1 : function(component, event, helper) {
        var toggleText = component.find("PLENTRY");
        $A.util.toggleClass(toggleText, "toggle");
    },
    toggle2 : function(component, event, helper) {
        var toggleText = component.find("BALANCE");
        $A.util.toggleClass(toggleText, "toggle");
    },
    toggle3 : function(component, event, helper) {
        var toggleText = component.find("RATIOS");
        $A.util.toggleClass(toggleText, "toggle");
    },
    handleConfirmDialogbox : function(component, event, helper) {
        component.set('v.showConfirmDialogbox', true);
    },
    handleConfirmDialogboxYes : function(component, event, helper) {
        helper.closeMe(component, event, helper);
    },
    handleConfirmDialogboxNo : function(component, event, helper) {
        component.set('v.showConfirmDialogbox', false);
    },
	getSalaryIncome : function(component, event, helper) {
        helper.helperMethod(component, event, helper);
        helper.doInitisedit(component, event, helper);
    },
    save : function(component, event, helper) {
        var ischecked = component.find("isChecked").get("v.value");
        var customerSegment = component.get("v.CustDetails");
        var custid = component.get("v.Customerid");
		var action = component.get("c.SaveSalary");
        var salarylist = component.get("v.CustDetails");
        console.log('test'+ ischecked);
        if (ischecked === false || ischecked == '') {//checkit[0].isVerified__c
            action.setParams({
                "SalariedDetails":salarylist,
                'conId':custid
            });
            action.setCallback(this, function(response){          
                var state = response.getState();
                if(state === "SUCCESS"){
                    component.set("v.iseditable",false);
                    console.log("response.getReturnValue()"+response.getReturnValue());
                    helper.helperMethod(component, event, helper);
                    //component.set("v.CustDetails",response.getReturnValue());
                }else if(response.getState() === "ERROR"){
                    var errors = action.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log('in error'+errors[0].message);
                            component.set("v.message", errors[0].message);
                        }
                    }
                }
            });
            $A.enqueueAction(action);
        }
        else if(ischecked){
            console.log('customerSegment[0].Income_Program_Type__c '+customerSegment[0].Income_Program_Type__c);
        	if(customerSegment[0].Income_Program_Type__c == 'Normal Salaried' || customerSegment[0].Income_Program_Type__c == 'FLIP' ){
                var allValid = component.find('fieldId').reduce(function (validSoFar, inputCmp) {
                    inputCmp.showHelpMessageIfInvalid();
                    return validSoFar && inputCmp.get('v.validity').valid;
                }, true);
            }else if(customerSegment[0].Income_Program_Type__c == 'Cash Salary' || customerSegment[0].Income_Program_Type__c == 'Salary Cat-B'){
                var allValid = component.find('fieldId2').reduce(function (validSoFar, inputCmp) {
                    inputCmp.showHelpMessageIfInvalid();
                    return validSoFar && inputCmp.get('v.validity').valid;
                }, true);
            }
        	/*var allValid1 = component.find('fieldId').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar &&  !inputCmp.get('v.validity').rangeUnderflow;
       		}, true);
        
        	var allValid2 = component.find('fieldId2').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            //return validSoFar && inputCmp.get('v.validity').valid;
           	return validSoFar &&  !inputCmp.get('v.validity').rangeUnderflow;
        	}, true);*/
        	//&& allValid1 && allValid2
        	console.log('allValid '+allValid);
            if(allValid){
                  action.setParams({
                  "SalariedDetails":JSON.stringify(salarylist) ,
                  'conId':custid
                  });
                action.setCallback(this, function(response){          
                    var state = response.getState();
                    if(state === "SUCCESS"){
                        component.set("v.iseditable",false);
                        console.log("response.getReturnValue()"+response.getReturnValue());
                        helper.helperMethod(component, event, helper);
                        //component.set("v.CustDetails",response.getReturnValue());
                    }else if(response.getState() === "ERROR"){
                        var errors = action.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log('in error'+errors[0].message);
                                component.set("v.message", errors[0].message);
                            }
                        }
                    }
                });
                $A.enqueueAction(action);
            }else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": "Please complete missing information."
                });
                toastEvent.fire();
            }    
        }
        else {
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "type": "Error",
                    "message": "Please complete missing information."
                });
                toastEvent.fire();
        	//alert('Please update the invalid form entries and try again.');
        }
	},
    edit : function(component, event, helper) {
		component.set("v.iseditable",true);
	},
       // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
     
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    }
})