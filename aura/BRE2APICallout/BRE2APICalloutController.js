({
	doInit : function(component, event, helper) {
		
		component.set('v.showOptions',true);
		var action = component.get("c.getMessage");
            
            action.setParams({
                "loanApplicationID" : component.get('v.recordId')
            });
            action.setCallback(this, function(result){
                var state = result.getState();
                if (component.isValid() && state === "SUCCESS"){
                    if(result.getReturnValue().includes("Error")) {
                        component.set('v.showOptions1' , false); 
                    } else {
                       component.set('v.showOptions1' , true); 
                    }
                    component.set('v.message',result.getReturnValue());
                }
            });
            $A.enqueueAction(action);
	},
	handleYes: function(component, event, helper) {
		component.set('v.message','Generating Request');
		component.set('v.showOptions',false);
		helper.showSpinner(component, event);
		var action = component.get("c.generateRequest");
            console.log(component.get('v.recordId'));
            action.setParams({
                "loanApplicationID" : component.get('v.recordId')
            });
            action.setCallback(this, function(result){
                var state = result.getState();
                if (component.isValid() && state === "SUCCESS"){ 
                    if(result.getReturnValue().includes('Error')) {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                        "type": "Error",
                        "title": "Error!",
                        "message": result.getReturnValue()
                        });
                        toastEvent.fire();
                    } else {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                        "type": "Success",
                        "title": "Success!",
                        "message": "Successful"
                        });
                        toastEvent.fire();
                        window.location.reload(); 
                    }
                }
                helper.hideSpinner(component, event);
                 $A.get("e.force:closeQuickAction").fire();
            });
             $A.enqueueAction(action);
	},
	handleNo: function(component, event, helper) {
	    $A.get("e.force:closeQuickAction").fire();
	}
})