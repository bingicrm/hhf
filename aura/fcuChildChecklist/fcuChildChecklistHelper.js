({
    MAX_FILE_SIZE: 9500000, //Max file size 4.5 MB 
    CHUNK_SIZE: 750000,      //Chunk Max size 750Kb 
    
      // Fetch the documents from the Apex controller
      getDocumentList: function(component,event,helper) {
        var action = component.get('c.getDocuments');
        // Set up the callback
        var self = this;
        action.setCallback(this, function(actionResult) {
         component.set('v.documents', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
     },

   // fetch picklist values dynamic from apex controller 
    docStatusPickListVal: function(component, event,helper) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.objInfoForPicklistValues"),
            "fld": 'Status__c'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            //alert(response.getState());
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
 
                component.set("v.docStatus", allValues);
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchPickListVal: function(component, fieldName, picklistOptsAttributeName) {
        var exemptedFromFCUBRD = component.get("v.exemptedFromFCUBRD"); //Added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
        
        //Below if block only and else block added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
        if(exemptedFromFCUBRD == false){
            var action = component.get("c.getselectOptions");
            action.setParams({
                "objObject": component.get("v.objInfoForPicklistValues"),
                "fld": fieldName
            });
            var opts = [];
            action.setCallback(this, function(response) {
                if (response.getState() == "SUCCESS") {
                    var allValues = response.getReturnValue();
                    
                    if (allValues != undefined && allValues.length > 0) {
                        opts.push({
                            class: "optionClass",
                            label: "--- None ---",
                            value: ""
                        });
                    }
                    for (var i = 0; i < allValues.length; i++) {
                        opts.push({
                            class: "optionClass",
                            label: allValues[i],
                            value: allValues[i]
                        });
                    }
                    component.set("v." + picklistOptsAttributeName, opts);
                }
            });
            $A.enqueueAction(action);
        }
        else{
            //All code related to action1 var is added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
            var action1 = component.get("c.getselectOptions");
            action1.setParams({
                "objObject": component.get("v.objInfoForPicklistValues1"),
                "fld": fieldName
            });
            var opts = [];
            action1.setCallback(this, function(response) {
                if (response.getState() == "SUCCESS") {
                    var allValues = response.getReturnValue();
                    
                    if (allValues != undefined && allValues.length > 0) {
                        opts.push({
                            class: "optionClass",
                            label: "--- None ---",
                            value: ""
                        });
                    }
                    for (var i = 0; i < allValues.length; i++) {
                        opts.push({
                            class: "optionClass",
                            label: allValues[i],
                            value: allValues[i]
                        });
                    }
                    component.set("v." + picklistOptsAttributeName, opts);
                }
            });
            $A.enqueueAction(action1);
        }
    }
})