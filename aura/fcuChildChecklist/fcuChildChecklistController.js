({
	doInit: function(component, event, helper) {
        var exemptedFromFCUBRD = component.get("v.exemptedFromFCUBRD"); //Added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
        
        //Below if block only and else block added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
        if(exemptedFromFCUBRD == false){
            helper.fetchPickListVal(component, 'FCU_Decision__c','docCollectionModePicklistOpts'); //Decision_Component__c added by Abhilekh on 6th September 2019 for FCU BRD
            helper.fetchPickListVal(component, 'Screened__c','scrOptions');
            helper.fetchPickListVal(component, 'Sampled__c','samOptions');
            
            var screenedField = component.find("inputScr");
            var screenedTextField = component.find("screened");
            var sRecord = component.get("v.singleRec");
            
            if(component.get("v.scrEditMode") == true){
                console.log('screenedField.get("v.value")==>'+screenedField.get("v.value"));
                console.log('$A.util.isEmpty(screenedField.get("v.value"))==>'+$A.util.isEmpty(screenedField.get("v.value")));
                if(screenedField.get("v.value") == 'No' || $A.util.isEmpty(screenedField.get("v.value"))){
                    component.set("v.validateDecision",false);
                }
            }
        }
        else{
            helper.fetchPickListVal(component, 'Document_Collection_Mode__c','docCollectionModePicklistOpts');
            helper.fetchPickListVal(component, 'Screened_p__c','scrOptions');
            helper.fetchPickListVal(component, 'Sampled_p__c','samOptions');
        }
    },
    
    inlineEditScreened : function(component,event,helper){   
        // show the rating edit field popup 
        component.set("v.scrEditMode", true); 
		component.set("v.showSaveCancelBtn",true);
        // after set ratingEditMode true, set picklist options to picklist field 
        component.find("inputScr").set("v.options" , component.get("v.scrOptions"));
        // after the 100 millisecond set focus to input field   
        setTimeout(function(){ 
            component.find("inputScr").focus();
        }, 100);
    },
    
	inlineEditSampled : function(component,event,helper){   
        // show the rating edit field popup 
        component.set("v.samEditMode", true);
		component.set("v.showSaveCancelBtn",true);
        // after set ratingEditMode true, set picklist options to picklist field 
        component.find("inputSam").set("v.options" , component.get("v.samOptions"));
        // after the 100 millisecond set focus to input field   
        setTimeout(function(){ 
            component.find("inputSam").focus();
        }, 100);
    },
    
    onphotocopyChange : function(component,event,helper){ 
        // if picklist value change,
        // then show save and cancel button by set attribute to true
        component.set("v.showSaveCancelBtn",true);
    },     
    
    closePhotocopyBox : function (component, event, helper) {
       // on focus out, close the input section by setting the 'ratingEditMode' att. as false
        component.set("v.scrEditMode", false);
		component.set("v.samEditMode", false); 
    },
    
	inlineEditDocCollectionStatus : function(component,event,helper){
        //alert('Inline1');
        component.set("v.DocumentCollectionEditMode", true);
        //alert('Inline2');
        component.set("v.showSaveCancelBtn",true);
        //alert('Inline3');
        component.find("inputMode").set("v.options",component.get("v.docCollectionModePicklistOpts"));
        //alert('Inline4');
    },
    
    Editbutton : function(component,event,helper){  
        component.set("v.DocumentCollectionEditMode", true);
		component.set("v.scrEditMode", true); 
		component.set("v.samEditMode", true);
        //alert('Edit1');
        component.find("inputMode").set("v.options",component.get("v.docCollectionModePicklistOpts"));
        //alert('Edit2');
        component.find("inputScr").set("v.options",component.get("v.scrOptions"));
        //alert('Edit3');
        component.find("inputSam").set("v.options",component.get("v.samOptions"));
		 
        
        component.set("v.showSaveCancelBtn",true);
    },
    
    closeStatusBoxDocCollection : function (component, event, helper) {
        component.set("v.DocumentCollectionEditMode", false);
        
    },
    
	closeScrInput : function (component, event, helper) {
        component.set("v.scrEditMode", false);
    },
    
	closeSamInput : function (component, event, helper) {
        component.set("v.samEditMode", false);
    },
    
    closeModalDoc:function(component,event,helper){   
        //alert('Close');
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    
    openmodalDoc: function(component,event,helper) {
        //alert('Open');
        /*var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); */
        
        var exemptedFromFCUBRD = component.get("v.exemptedFromFCUBRD"); //Added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
        
        //Below if block only and else block added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
        if(exemptedFromFCUBRD == false){
            var propertyId = component.get("v.singleRec.Document_Checklist__c");
            //component.set("v.files", []);
            if (!propertyId) {
                return;
            }
            var action = component.get("c.queryAttachments");
            action.setParams({
                "docId": propertyId,
            });
            action.setCallback(this, function (response) {
                var files = response.getReturnValue();
                console.log(files);
                if(files != null) {
                    var cusrecordIds = files.join();
                    console.log('recordIds'+cusrecordIds);
                    $A.get('e.lightning:openFiles').fire({
                        //recordIds: ['069p0000001E6ag'],
                        recordIds: files,
                    });
                }
                else {
                    var cmpTarget = component.find('Modalbox');
                    var cmpBack = component.find('Modalbackdrop');
                    $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                    $A.util.addClass(cmpBack, 'slds-backdrop--open');
                }
                //component.set("v.files", files);
            });
            $A.enqueueAction(action);
        }
        else{
            var propertyId = component.get("v.singleRec1.Id");
            //component.set("v.files", []);
            if (!propertyId) {
                return;
            }
            
            //All code related to action1 var is added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
            var action1 = component.get("c.queryAttachments");
            action1.setParams({
                "docId": propertyId,
            });
            action1.setCallback(this, function (response) {
                var files = response.getReturnValue();
                console.log(files);
                if(files != null) {
                    var cusrecordIds = files.join();
                    console.log('recordIds'+cusrecordIds);
                    $A.get('e.lightning:openFiles').fire({
                        //recordIds: ['069p0000001E6ag'],
                        recordIds: files,
                    });
                }
                else {
                    var cmpTarget = component.find('Modalbox');
                    var cmpBack = component.find('Modalbackdrop');
                    $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                    $A.util.addClass(cmpBack, 'slds-backdrop--open');
                }
                //component.set("v.files", files);
            });
            $A.enqueueAction(action1);
        }
    },
    
    /*onDecisionChange: function(component,event,helper) {
        var singleResultField = component.find("singleResult");
        var screenedField = component.find("inputScr");
        var screenedTextField = component.find("screened");
        //console.log('screenedTextField.get()==>'+screenedTextField.get("v.value"));
        
        if(screenedTextField.get("v.value") == 'No' || $A.util.isEmpty(screenedTextField.get("v.value"))){
            console.log('Inside if==>');
            component.set("v.validateDecision",false);
            singleResultField.set("v.value", "Document screening should be complete before taking decision.");
        }
        else{
            component.set("v.validateDecision",true);
        }
        console.log('validateDecision==>'+component.get("v.validateDecision"));
    },*/
    
    //Added by Abhilekh on 6th September 2019 for FCU BRD
    onScreenedChange: function(component,event,helper) {
        var exemptedFromFCUBRD = component.get("v.exemptedFromFCUBRD"); //Added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
        
        //Below if block only added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
        if(exemptedFromFCUBRD == false){
            var screenedField = component.find("inputScr");
            var screenedOutputField = component.find("scrRes");
            
            if(screenedField.get("v.value") == 'Yes'){
                component.set("v.validateDecision",true);
                screenedOutputField.set("v.value","");
            }
            else{
                component.set("v.validateDecision",false);
                screenedOutputField.set("v.value","Decision cannot be saved if Screening has not been done.");
            } 
        }
    }
})