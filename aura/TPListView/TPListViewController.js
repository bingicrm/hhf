({
    doInit: function(component, event, helper) {
        //Added by Abhilekh on 30th October 2019 for FCU BRD
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            workspaceAPI.setTabLabel({
                tabId: focusedTabId,
                label: "FCU Verifications"
            });
            /*workspaceAPI.setTabIcon({
                tabId: focusedTabId,
                icon: "utility:", //set icon you want to set
                iconAlt: "FCU Verifications" //set label tooltip you want to set
            });*/
        })
        .catch(function(error) {
            console.log('Error==>'+error);
        });
        //End of code by Abhilekh on 30th October 2019 for FCU BRD

        var act = component.get("c.getUserType");

        act.setCallback(this, function(actionResult1) {
            var state = actionResult1.getState();
            if (state === "SUCCESS"){
                var result = actionResult1.getReturnValue();
                console.log('result==>'+JSON.stringify(result));

                //Below if and else block added by Abhilekh on 30th October 2019 for FCU BRD
                if(result.FCU__c == true){
                    var action = component.get("c.getProfileName");

                    action.setCallback(this, function(actionResult) {
                        var state = actionResult.getState();
                        if (state === "SUCCESS"){
                            var result = actionResult.getReturnValue();

                            if(result === 'FCU Manager' || result === 'System Administrator'){
                                component.set('v.mycolumns', [
                                    {label: 'Third Party Verification Name', fieldName: 'linkName', type: 'url', 
                                     typeAttributes: {label: { fieldName: 'Name' }, target: '_self'}},
                                    {label: 'Customer Name', fieldName: 'Name_of_Applicant__c', type: 'text', sortable : true},
                                    {label: 'Loan Application Name', fieldName: 'lalink', type: 'url',
                                     typeAttributes: { label: { fieldName: 'laName' }, target: '_self'}, sortable : true}, //Edited by Abhilekh on 19th September 2019 for FCU BRD
                                    {label: 'Initiation Date', fieldName: 'Initiation_Date__c', type: 'date', typeAttributes: {  
                                        day: 'numeric',  
                                        month: 'short',  
                                        year: 'numeric',  
                                        hour: '2-digit',  
                                        minute: '2-digit',  
                                        second: '2-digit',  
                                        hour12: true}, sortable : true},//Added by saumya For Third Party view Change
                                    
                                    {label: 'Requested Loan Amount', fieldName: 'Requested_Loan_Amount__c', type: 'currency'}, //Added by saumya For Third Party view Change
                                    {label: 'Product Type', fieldName: 'Product_Type__c', type: 'Text'},
                                    {label: 'Branch', fieldName: 'Branch__c', type: 'Text'},
                                    {label: 'Record Type', fieldName: 'Record_Type_Name__c', type: 'Text'}, //Added by Abhilekh on 20th January 2020 for Hunter BRD
                                    {label: 'FCU Type', fieldName: 'FCU_Type__c', type: 'Text'}, //Added by Abhilekh on 19th September 2019 for FCU BRD
                                    {label: 'Agency Name', fieldName: 'agencyName', type: 'Text'},
                                    //{label: 'Status', fieldName: 'Status__c', type: 'Text'},
                                    {label: 'Re-Appealed Case', fieldName: 'laReAppealedCase', type: 'Text', sortable : true}, //Added by Abhilekh on 19th September 2019 for FCU BRD
                                    //{label: 'Overall Report Status', fieldName: 'laOverallFCUReportStatus', type: 'Text'} //Added by Abhilekh on 19th September 2019 for FCU BRD
                                ]);
                                    }
                                    else{
                                    component.set('v.mycolumns', [
                                    {label: 'Third Party Verification Name', fieldName: 'linkName', type: 'url', 
                                    typeAttributes: {label: { fieldName: 'Name' }, target: '_blank'}},
                                    {label: 'Customer Name', fieldName: 'Name_of_Applicant__c', type: 'text'},
                                    {label: 'Loan Application Name', fieldName: 'lalink', type: 'url',
                                    typeAttributes: { label: { fieldName: 'laName' }, target: '_blank' }}, //Edited by Abhilekh on 19th September 2019 for FCU BRD
                                    {label: 'Initiation Date', fieldName: 'Initiation_Date__c', type: 'date', typeAttributes: {  
                                    day: 'numeric',  
                                    month: 'short',  
                                    year: 'numeric',  
                                    hour: '2-digit',  
                                    minute: '2-digit',  
                                    second: '2-digit',  
                                    hour12: true}, sortable : true},//Added by saumya For Third Party view Change
                                    
                                    {label: 'Requested Loan Amount', fieldName: 'Requested_Loan_Amount__c', type: 'currency'}, //Added by saumya For Third Party view Change
                                    {label: 'Product Type', fieldName: 'Product_Type__c', type: 'Text'},
                                    {label: 'Branch', fieldName: 'Branch__c', type: 'Text'},
                                    {label: 'FCU Type', fieldName: 'FCU_Type__c', type: 'Text'}, //Added by Abhilekh on 19th September 2019 for FCU BRD
                                    {label: 'Owner Name', fieldName: 'Owner_Name__c', type: 'Text'},
                                    {label: 'Status', fieldName: 'Status__c', type: 'Text'},
                                    //{label: 'Re-Appealed Case', fieldName: 'laReAppealedCase', type: 'Text'}, //Added by Abhilekh on 19th September 2019 for FCU BRD
                                    //{label: 'Overall Report Status', fieldName: 'laOverallFCUReportStatus', type: 'Text'} //Added by Abhilekh on 19th September 2019 for FCU BRD
                                ]);
                            }
                        }
                    });
                    $A.enqueueAction(action);
                }
                else{
                    console.log('Inside else');
                    component.set('v.mycolumns', [
                        {label: 'Third Party Verification Name', fieldName: 'linkName', type: 'url', 
                         typeAttributes: {label: { fieldName: 'Name' }, target: '_blank'}},
                        {label: 'Customer Name', fieldName: 'Name_of_Applicant__c', type: 'text'},
                        {label: 'Loan Application', fieldName: 'Loan_Application_Name__c', type: 'Text'},
                        {label: 'Initiation Date', fieldName: 'Initiation_Date__c', type: 'date', typeAttributes: {  
                            day: 'numeric',  
                            month: 'short',  
                            year: 'numeric',  
                            hour: '2-digit',  
                            minute: '2-digit',  
                            second: '2-digit',  
                            hour12: true}},//Added by saumya For Third Party view Change
                        
                        {label: 'Requested Loan Amount', fieldName: 'Requested_Loan_Amount__c', type: 'currency'}, //Added by saumya For Third Party view Change
                        {label: 'Product Type', fieldName: 'Product_Type__c', type: 'Text'},
                        {label: 'Branch', fieldName: 'Branch__c', type: 'Text'},
                        {label: 'Verification Type', fieldName: 'Record_Type_Name__c', type: 'Text'},
                        {label: 'Owner Name', fieldName: 'Owner_Name__c', type: 'Text'},
                        {label: 'Status', fieldName: 'Status__c', type: 'Text'}
                        //{label: 'Re-Appealed Case', fieldName: 'laReAppealedCase', type: 'Text'}, //Added by Abhilekh on 19th September 2019 for FCU BRD
                        //{label: 'Overall Report Status', fieldName: 'laOverallFCUReportStatus', type: 'Text'} //Added by Abhilekh on 19th September 2019 for FCU BRD
                    ]);
                }
            }
        });
        $A.enqueueAction(act);

        helper.getTPVList(component);
    },
    
    //Below method added by Abhilekh on 30th October 2019 for FCU BRD
    handleSort : function(component,event,helper){
        //Returns the field which has to be sorted
        var sortBy = event.getParam("fieldName");
        //returns the direction of sorting like asc or desc
        var sortDirection = event.getParam("sortDirection");
        //Set the sortBy and SortDirection attributes
        component.set("v.sortBy",sortBy);
        component.set("v.sortDirection",sortDirection);
        // call sortData helper function
        helper.sortData(component,sortBy,sortDirection);
    }
})