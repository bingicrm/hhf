({
    doInit : function(component, event, helper) {
        //All code related to action1 var is added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
        var action1 = component.get("c.checkLAFCUBRDExemption1");

        action1.setParams({
            'laId': component.get("v.recordId")
        });
        
        action1.setCallback(this, function(response) {
            var state = response.getState();

            if(state === "SUCCESS"){
            	var storeResponse = response.getReturnValue();

                if(storeResponse == true){
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({
                        'type': 'ERROR',
                        'message' : 'This action is not allowed for old Loan Applications.'
                    }); 
                    showToast.fire();
                    $A.get("e.force:closeQuickAction").fire();
                }
                else{
                    var action2 = component.get("c.checkUserRole");
                    
                    action2.setCallback(this, function(response) {
                        var state = response.getState();
                        
                        if (state === "SUCCESS") {
                            var storeResponse = response.getReturnValue();
                            
                            if(storeResponse == 'FCU Head'){
                                var showToast = $A.get("e.force:showToast"); 
                                showToast.setParams({
                                    'type': 'ERROR',
                                    'message' : 'You are not allowed to create fresh FCU Verifications.'
                                }); 
                                showToast.fire();
                                $A.get("e.force:closeQuickAction").fire();
                            }
                            else{
                                var action = component.get("c.fetchAllDocs");
                                action.setParams({
                                    'laId': component.get("v.recordId")
                                });
                                action.setCallback(this, function(response) {
                                    var state = response.getState();
                                    if (state === "SUCCESS") {
                                        var storeResponse = response.getReturnValue();
                                        console.log('List received'+storeResponse);
                                        console.log(JSON.stringify(storeResponse));
                                        component.set("v.docList", storeResponse);
                                    }
                                    else{
                                        var showToast = $A.get("e.force:showToast"); 
                                        showToast.setParams({
                                            'type': 'ERROR',
                                            'message' : 'An error has occurred. Please contact system admin.'
                                        }); 
                                        showToast.fire();
                                        $A.get("e.force:closeQuickAction").fire();
                                    }
                                });
                                $A.enqueueAction(action);
                            }
                        }
                        else{
                            var showToast = $A.get("e.force:showToast"); 
                            showToast.setParams({
                                'type': 'ERROR',
                                'message' : 'An error has occurred. Please contact system admin.'
                            }); 
                            showToast.fire();
                            $A.get("e.force:closeQuickAction").fire();
                        }
                    });
                    $A.enqueueAction(action2);
                }
            }
            else{
                var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({
                    'type': 'ERROR',
                    'message' : 'An error has occurred. Please contact system admin.'
                }); 
                showToast.fire();
                $A.get("e.force:closeQuickAction").fire();
            }
        });
        $A.enqueueAction(action1);
    },
    
    onCheck : function(component, event, helper){
        var checkedValue = event.getSource().get("v.value");
        var getAllId = component.find("chkboxes");
        
        if(! Array.isArray(getAllId)){
            if(checkedValue == true){ 
                component.find("chkboxes").set("v.value", true);
                component.set("v.selectedCount", 1);
            }else{
                component.find("chkboxes").set("v.value", false);
                component.set("v.selectedCount", 0);
            }
        }
        else{
            if (checkedValue == true) {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("chkboxes")[i].set("v.value", true);
                    component.set("v.selectedCount", getAllId.length);
                }
            } else {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("chkboxes")[i].set("v.value", false);
                    component.set("v.selectedCount", 0);
                }
            } 
        }  
    },
    
    checkboxSelect: function(component, event, helper) {
        var selectedRec = event.getSource().get("v.value");
        var getSelectedNumber = component.get("v.selectedCount");
        
        if (selectedRec == true) {
            getSelectedNumber++;
        } else {
            getSelectedNumber--;
        }
        
        component.set("v.selectedCount", getSelectedNumber);
        component.find("v.checkboxid");
        component.set("v.checkboxid",false);
    },
    
    Save : function(component,event,helper){
        var savId = [];
        var getAllId = component.find("chkboxes");
        
        if(!Array.isArray(getAllId)){
            if (getAllId.get("v.value") == true) {
                savId.push(getAllId.get("v.text"));
            }
        }
        else{
            for (var i = 0; i < getAllId.length; i++) {
                if (getAllId[i].get("v.value") == true) {
                    savId.push(getAllId[i].get("v.text"));
                }
            }
        }
        
        console.log('savId==>'+savId);
        
        if(component.get("v.selectedCount") == 0){
            alert('Select atleast one document before assigning FCU.');
        }
        else{
            var spinner = component.find("mySpinner");
        	$A.util.removeClass(spinner, "slds-hide");
            
        	var action = component.get("c.createFreshFCUTPV");
            action.setParams({
                'docIds': JSON.stringify(savId),
                'laId': component.get("v.recordId")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS"){
                    var storeResponse = response.getReturnValue();
                    
                    if(storeResponse === "SUCCESS"){
                        var showToast = $A.get("e.force:showToast"); 
                        showToast.setParams({ 
                            'type': 'SUCCESS',
                            'message' : 'Fresh FCU Third Party Verification has been created.'
                        });
                        showToast.fire();
                    }
                    else{
                        var showToast = $A.get("e.force:showToast"); 
                        showToast.setParams({
                            'type': 'ERROR',
                            'message' : 'An error has occurred. Please contact system admin.'
                        }); 
                        showToast.fire();
                    }
                }
                else{
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({
                        'type': 'ERROR',
                        'message' : 'An error has occurred. Please contact system admin.'
                    }); 
                    showToast.fire();
                }
                $A.get("e.force:closeQuickAction").fire();
                $A.get('e.force:refreshView').fire();
            });
            $A.enqueueAction(action);
        }
    },
    
    openmodalDoc: function(component,event,helper) {
        var ctarget = event.currentTarget;
        var propertyId = ctarget.dataset.value;
        
        if (!propertyId) {
            return;
        }
        var action = component.get("c.queryAttachments");
        action.setParams({
            "docId": propertyId,
        });
        action.setCallback(this, function (response) {
            var files = response.getReturnValue();
            console.log(files);
            if(files != null) {
                var cusrecordIds = files.join();
                console.log('recordIds'+cusrecordIds);
                $A.get('e.lightning:openFiles').fire({
                    recordIds: files,
                });
            }
            else {
                var cmpTarget = component.find('Modalbox');
                var cmpBack = component.find('Modalbackdrop');
                $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                $A.util.addClass(cmpBack, 'slds-backdrop--open');
            }
        });
        $A.enqueueAction(action);
    }
})