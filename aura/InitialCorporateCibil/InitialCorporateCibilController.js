({
	doInit: function(component, event, helper)  
    {   
		var act = component.get("c.checkCorporateCibilEligibility");        
        act.setParams({
            loanContactId : component.get("v.recordId")
        });  
        act.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                var responseValue = response.getReturnValue();
            	console.log('Response Received'+responseValue);
                if(responseValue == 'SUCCESS') {
                    component.set('v.performCorporateCibil',true);
                }
                else {
                    if(responseValue == 'Corporate CIBIL Not Applicable for this Customer, based on Constitution.') {
                    	var showToast = $A.get("e.force:showToast"); 
                                        showToast.setParams({ 
                                            'type': 'ALERT',
                                            'message' : 'Corporate CIBIL Not Applicable for this Customer, based on Constitution.',
                                            'duration' : '10000ms'
                                        }); 
                                        showToast.fire(); 	
                                        
                                        $A.get("e.force:closeQuickAction").fire();
                    }
                    else if(responseValue == 'Operation Not Allowed at this Stage/Sub-Stage.') {
                        var showToast = $A.get("e.force:showToast"); 
                                        showToast.setParams({ 
                                            'type': 'ALERT',
                                            'message' : 'Operation Not Allowed at this Stage/Sub-Stage.',
                                            'duration' : '10000ms'
                                        }); 
                                        showToast.fire(); 	
                                        
                                        $A.get("e.force:closeQuickAction").fire();
                    }
                    //Below else if block added by Abhilekh on 14th August 2019 for TIL-1297
                    else if(responseValue == 'Individual check false') {
                        var showToast = $A.get("e.force:showToast"); 
                                        showToast.setParams({ 
                                            'type': 'ALERT',
                                            'message' : 'Please add PAN details in atleast one of the individual customers.',
                                            'duration' : '10000ms'
                                        }); 
                                        showToast.fire(); 	
                                        
                                        $A.get("e.force:closeQuickAction").fire();
                    }
                    
                }
            }
        });
        $A.enqueueAction(act);
    }
})