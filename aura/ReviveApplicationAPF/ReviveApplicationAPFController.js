({
	doInit : function(component, event, helper) {
		
		var action = component.get("c.getProjectDetails");
        action.setParams({"projectId" : component.get("v.recordId")});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
				component.set("v.project", response.getReturnValue());
            }else{
                console.log('Problem getting Project Details, response state : '+state);
            }
        });
        $A.enqueueAction(action);
	},
    requestCreation: function(component, event, helper) {
        component.set("v.showBlock",true);
        component.set("v.showBlockDef",false);
        console.log('1111');
     },
    cancelCreation: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    requestSave: function(component, event, helper){
    	var action = component.get("c.reviveApplication");
		var project = component.get("v.project");
        console.log(project);
        action.setParams({
            "la" : project
        });
        console.log('2222');
        action.setCallback(this, function(actionResult) {
            console.log('3333');
            var state = actionResult.getState();
            if(state === "SUCCESS"){
                console.log('4444'+actionResult.getReturnValue());
                var resultsToast = $A.get("e.force:showToast");
                
                if(actionResult.getReturnValue() == null || actionResult.getReturnValue() == ''){
                	resultsToast.setParams({
                    	"title" : "Application Revival Approval",// Added by Saumya For Reject Revive BRD
                    	"message" : "Your application has been sent for Approval."// Added by Saumya For Reject Revive BRD
                	});
            	}
                else if(actionResult.getReturnValue() == 'Application Successfully revived'){
					resultsToast.setParams({
                    	"title" : "Success",// Added by Saumya For Reject Revive BRD
                    	"message" : "Your application successfully revived."// Added by Saumya For Reject Revive BRD
                	});                
                }
            	else{
                	resultsToast.setParams({
                    	"title" : "Error",
                    	"message" : actionResult.getReturnValue()
                	});
            	}
 
				$A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
                console.log('5555');
            }else if(state === "ERROR"){
                console.log('Problem creating records, response state '+state);
                console.log('6666');
            }else{
                console.log('Unknown problem: '+state);
                console.log('7777');
            }
        });
        $A.enqueueAction(action);
    }
})