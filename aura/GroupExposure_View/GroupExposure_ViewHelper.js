({
    saveSelectedHelper : function (component, event, saveRec, remRec) {
        var action = component.get('c.saveGERecords');
        action.setParams({
            "GERecords" : JSON.stringify(saveRec),
            "UGERecords" : JSON.stringify(remRec)
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('>>>>'+state);
            if (state === "SUCCESS") {           
                console.log('Helper State'+state);
                console.log("State==>"+state);
                if (response.getReturnValue() === 'The Remarks cannot be blank.') {
                    /*var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "The Remarks cannot be blank."
                    });
                    toastEvent.fire();*/
                    component.find('notifLib').showNotice({
                        "variant": "error",
                        "header": "Error!",
                        "message": "Remarks cannot be blank.",
                        closeCallback: function() {
                        }
                    });
                    console.log('Helper' +'1111'+'<<<<>>>>'+response.getReturnValue());
                } 
                else {
                    console.log('check it--> save successful');
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get("e.force:refreshView").fire();
                }  
                //this.onLoad(component, event);
            }
            else{
                console.log('Failure');
            }
        });        
        $A.enqueueAction(action);
        console.log('Action fired');        
    },     
    doInitisedit : function(component, event, helper) {
        
		var recordId = component.get("v.recordId");
        
        var action = component.get("c.GetIseditable");
        action.setParams({
            'recId':recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                //console.log("response.getReturnValue()"+response.getReturnValue()[4].Customer_Integration__c);
                //console.log("response.getReturnValue()"+response.getReturnValue()[0].Loan_Type__c);
                
                component.set("v.iseditable", response.getReturnValue());    
                console.log('in success getting editable::'+response.getReturnValue());
                //component.set("v.showEdit", response.getReturnValue());                
                
            }else if (response.getState() === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.message", errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
        
    }
})