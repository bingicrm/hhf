({
    doInit : function(component, event, helper) {
        helper.doInitisedit(component, event, helper);
        var action = component.get("c.getGroupExp");
        action.setParams({"loanAppId" : component.get("v.recordId")});
        action.setCallback(this, function(response){
            var state = response.getState();
            var result = response.getReturnValue();
            console.log('result::'+result);  
            if(state === "SUCCESS"){
                if(response.getReturnValue() != null){
                    var res = response.getReturnValue();
                    console.log('res::::'+JSON.stringify(res));
                    if(res.StageName != 'Credit Decisioning'){
                    $A.get("e.force:closeQuickAction").fire();
        			var resultsToast = $A.get("e.force:showToast");
                     resultsToast.setParams({
                    	"title" : "Error!",
                        "type" : 'error',
                    	"message" : 'Group Exposure can not be viewed at this Stage.'
                	});
                    resultsToast.fire();
                   // $A.get("e.force:refreshView").fire();
                        
                    }
                    else{
                        component.set("v.IsDisplay", true);
                    //component.set("v.gExp", response.getReturnValue());
                    component.set("v.gExp", res.lstGroupExp);
                    component.set("v.AppID", res.AppId);
                    var grpExp = component.get("v.gExp");
                    console.log('grpExp:::::'+JSON.stringify(grpExp));
                    component.set("v.reviewed",res.ExposureReviewed);// Added by Saumya For BRE 2
                    component.set("v.profile",res.loggedInUserProfile);// Added by Saumya For BRE 2
                    for(var i=0; i<grpExp.length; i++){
                        if(grpExp[i].Status__c == 'Not Considered'){
                            component.set("v.selectAll",false);
                            break;
                        }
                    }
                    
                    var action1 = component.get("c.calculateSumPO");
                    action1.setParams({"loanAppId" : component.get("v.recordId")});
                    action1.setCallback(this, function(response1){
                        var state1 = response1.getState();
                        if(state1 === "SUCCESS"){
                            component.set("v.sumPO", response1.getReturnValue());
                            console.log('Sum PO'+response1.getReturnValue());
                            /*** Added by Saumya For Group Exposure **/
                            var grpExp = response1.getReturnValue();
                            console.log('grpExp[6]'+grpExp[6]+':::grpExp[7]'+grpExp[7]);
                            component.set("v.groupExpAmt", grpExp[6]);
                            component.set("v.adjustAmt", grpExp[7]);
                            component.set("v.sumPO[6]", parseInt(grpExp[6])+parseInt(grpExp[7]));
                            console.log('component.set("v.groupExpAmt"'+component.get("v.groupExpAmt"));
                            console.log('component.set("v.adjustAmt"'+component.get("v.adjustAmt"));
                            
                            /*** Added by Saumya For Group Exposure **/
                            
                        }else{
                            console.log('Error on Sum PO');
                        }
                    });
                    $A.enqueueAction(action1);
                	}
                }
                else if(response.getReturnValue() == null){
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title" : "Error!",
                        "type" : 'error',
                        "message" : 'Please fill in Approved Loan Amount before viewing Group Exposure.'
                    });
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                }
            }else{
                
            }
        });
        $A.enqueueAction(action);
    },    
    /*** Added by Saumya For BRE 2 ***/
    onCheckBRE: function(component, event, helper){
        console.log('I am inonCheckBRE'+component.get("v.reviewed"));
        var action = component.get("c.reviewedGroupExposure");
        action.setParams({"loanAppId" : component.get("v.recordId"),
                          "reviewed" : component.get("v.reviewed")});
        action.setCallback(this, function(response){
            var state = response.getState();
            var result = response.getReturnValue();
            if(state === "SUCCESS"){
            }
        });
        $A.enqueueAction(action);
    },
     /*** Added by Saumya For BRE 2 ***/					 
    saveSelected: function(component, event, helper){
         var x =0; /*** Added by Saumya For Group Exposure **/

        var savId = [];
        var remId = [];
        var getAllId = component.find("chkboxes");
        if(!Array.isArray(getAllId)){
            if (getAllId.get("v.value") == true) {
                savId.push(getAllId.get("v.text"));
            }
            else{
                remId.push(getAllId.get("v.text"));
            }
        }
        else{
            for (var i = 0; i < getAllId.length; i++) {
                if (getAllId[i].get("v.value") == true) {
                    savId.push(getAllId[i].get("v.text"));
                }
                else{
                    remId.push(getAllId[i].get("v.text"));
                }           
            }
        }
        
/*** Added by Saumya For Group Exposure **/
        var adjustAmt = component.get("v.sumPO[6]");
            
           console.log('remId.length:::'+remId.length); 
        if(remId.length > 0){
            console.log('remId.length:::'+remId.length); 
                for(var i=0; i<remId.length ; i++){
            	console.log('I am in for:::'); 
                x = x+parseInt(remId[i].Amount_Sanctioned_but_not_Disbursed__c) +parseInt(remId[i].Principal_Outstanding__c);
                }
            }
        console.log('x:::'+x);
            adjustAmt = adjustAmt - x;
        console.log('adjustAmt:::'+adjustAmt);
       /*** Added by Saumya For Group Exposure **/ 
            var adjustAmtupdate = component.get("v.adjustAmtupdate");
            var recId = component.get("v.recordId");
            var action = component.get("c.saveGEAdjustment");
            action.setParams({
                "loanId": recId,
                "adjustAmt": adjustAmt,
                "adjustAmtupdate": adjustAmtupdate            /*** Added by Saumya For Group Exposure **/    
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") { 
                    var res = response.getReturnValue();
                }
                else{
                    alert('Error');
                }
            })
            $A.enqueueAction(action); 
        
        helper.saveSelectedHelper(component, event, savId, remId);
        
        
        
    },    
    
    onCheck: function(component, event, helper){
        console.log('I am here');
        var checkedValue = event.getSource().get("v.value");
        var getAllId = component.find("chkboxes");
        
        if(! Array.isArray(getAllId)){
            if(checkedValue == true){ 
                component.find("chkboxes").set("v.value", true);
                component.set("v.selectedCount", 1);
            }else{
                component.find("chkboxes").set("v.value", false);
                component.set("v.selectedCount", 0);
            }
        }
        else{
            if (checkedValue == true) {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("chkboxes")[i].set("v.value", true);
                    component.set("v.selectedCount", getAllId.length);
                }
            } else {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("chkboxes")[i].set("v.value", false);
                    component.set("v.selectedCount", 0);
                }
            } 
        }
    },    
    
    checkboxSelect : function(component, event, helper) {
        var getAllChkBx = component.find("chkboxes");
        
        for (var i = 0; i < getAllChkBx.length; i++) {
            if (getAllChkBx[i].get("v.value") == false) {
                component.find("checkboxid").set("v.value", false);
            }
        }
        //var selectedRec = event.getSource().get("v.value");
        //var getSelectedNumber = component.get("v.selectedCount");
        
        /*if (selectedRec == true) {
            component.set("v.selectAll",false);
        } else {
            getSelectedNumber--;
        }
        component.set("v.selectedCount", getSelectedNumber);*/
        //component.find("checkboxid").set("v.value", false);
    },
    
    adjustExposure : function(component, event, helper){
        var adjustAmt = component.get("v.adjustAmt");
        if(adjustAmt <= 0){
            component.find('notifLib').showNotice({
                "variant": "error",
                "header": "Values Incorrect!",
                "message": "The Group Exposure adjustments cannot be Zero or Negetive amount.",
                closeCallback: function() {
                }
            });
        }
        else if(adjustAmt > 0){
            var totalExpCons = component.get("v.sumPO[6]"); /*** Added by Saumya For Group Exposure **/
            var grpExp = component.get("v.groupExpAmt");
            var adjustAmtupdate = component.get("v.adjustAmtupdate");
            var totalExpConsider = parseInt(grpExp)+parseInt(adjustAmt);
            console.log('totalExpConsider'+totalExpConsider);
            /*** Commented by Saumya For Group Exposure **/
            //if(adjustAmtupdate == null)
            var adjAmtupdate = parseInt(adjustAmt);
            /*else
            var adjAmtupdate = parseInt(adjustAmtupdate)+parseInt(adjustAmt);*/
            console.log('adjAmtupdate'+adjAmtupdate);
            component.set("v.sumPO[6]",totalExpConsider); 
            component.set("v.adjustAmtupdate",adjAmtupdate);
            component.find('notifLib').showNotice({
                "variant": "info",
                "header": "Values Saved!",
                "message": "The Group Exposure adjustments have been made.",
                closeCallback: function() {
                }
            });
                       
        }
    } 
})