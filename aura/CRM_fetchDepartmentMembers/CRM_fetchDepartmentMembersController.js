({
	fetchDepartmentMembers : function(component, event, helper) {
         component.set('v.mycolumns', [
            	
                {label: 'Department', fieldName: 'Department__c', type: 'text'},
             {label: 'Members', fieldName: 'L1_Name__c', type: 'text'}
            ]);
        
        var action = component.get("c.fetchDepartmentMembersmet");
        action.setParams({ 
            //dep :component.get("v.RecordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.metadataList", response.getReturnValue());
            }
        });
        $A.enqueueAction(action); 
    }
})