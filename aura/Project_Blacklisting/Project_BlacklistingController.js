({
	doInit : function(component, event, helper) {
        
        var checkErrorMsg = component.get("c.showErrorMsg");
        checkErrorMsg.setCallback(this, function(errorMsgResponse) {
            var responsestate = errorMsgResponse.getState();
            if(responsestate === "SUCCESS") {
                if(response.getReturnValue() == "Success") {
                    var action = component.get("c.checkEligibility");
                    action.setParams({"lappId" : component.get("v.recordId")});
                    action.setCallback(this, function(response){
                        var state = response.getState();
                        
                        if(state === "SUCCESS"){
                            
                            if(response.getReturnValue().APF_Status__c != "APF Inventory Approved") {
                                var resultsToast = $A.get("e.force:showToast");
                                resultsToast.setParams({
                                    "title" : "Error",
                                    "message" : "APF Status must be APF Inventory Approved, for initiating Blacklisting."
                                });
                                resultsToast.fire();
                                $A.get("e.force:closeQuickAction").fire();
                            }
                            if(response.getReturnValue().Blacklisting_Status__c == "Blacklisting Initiated") {
                                var resultsToast = $A.get("e.force:showToast");
                                resultsToast.setParams({
                                    "title" : "Error",
                                    "message" : "Blacklisting Already Initiated for this Project."
                                });
                                resultsToast.fire();
                                $A.get("e.force:closeQuickAction").fire();
                            }
                            else {
                                component.set("v.lapp", response.getReturnValue());
                                component.set("v.showBlockDef",true);
                            }
                        }else{
                            var resultsToast = $A.get("e.force:showToast");
                            resultsToast.setParams({
                                "title" : "Error",
                                "message" : "Cannot fetch the Project."
                            });
                            resultsToast.fire();
                            $A.get("e.force:refreshView").fire();
                        }
                    });
                    $A.enqueueAction(action);
                }
                
                else if(response.getReturnValue() == "You are not authorized to initiate Project Blacklisting") {
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title" : "Error",
                        "message" : "You are not authorized to initiate Project Blacklisting."
                    });
                    resultsToast.fire();
                    $A.get("e.force:closeQuickAction").fire();
                }
            }
        });
        $A.enqueueAction(checkErrorMsg);
        
		
	},
    requestCreation: function(component, event, helper) {
        component.set("v.showBlock",true);
        component.set("v.showBlockDef",false);
        console.log('1111');
         
    },
  
    cancelCreation: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    requestSave : function(component, event, helper) {
		var loanApp =  component.get("v.recordId") ;
        
        console.log(loanApp);
       
        var action = component.get('c.moveprevious');
        console.log('1');
        action.setParams({
           
            "oppId": loanApp ,
            "comments": component.get("v.comments")
        });
        console.log('2');
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") 
            {
                var result = actionResult.getReturnValue();
                //result = '';
                console.log('==result===' + actionResult.getReturnValue());
                if(result.error){
                    //var errorMess= actionResult.getReturnValue();
                    var showToast = $A.get("e.force:showToast"); 
					showToast.setParams({ 
					 					'type': 'ERROR',
										'message' : result.msg
										}); 
					showToast.fire(); 	
                    
                }
                else
                {
                    var msg;
                    //console.log('why hwere');
                    var navEvent = $A.get("e.force:navigateToObjectHome");
                    navEvent.setParams({
               					"scope": "Project__c"
            		});
            		navEvent.fire();
                    msg = 'Project Submitted for Blacklisting Approval.';  
            		var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                    	'type': 'Success',
                		'message' : msg
                    }); 
                	showToast.fire();
                    //helper.UpdateFTR(component);
                }
            } 
            else 
            {
                console.log('==result===' + actionResult.getState());
                var errors = actionResult.getError();
                var errorMess;
                console.log('==error'+errors[0]);
               
               	if (errors[0] && errors[0].message) 
                {
                        errorMess = "Error message: " + errors[0].message;
                    
                } 
                else 
                {
                    errorMess ="Please contact System admin"
                    console.log("Unknown error");
                }
            
                var showToast = $A.get("e.force:showToast"); 
					showToast.setParams({ 
					'title' : errorMess, 
                    'type': 'ERROR',
					'message' : errorMess
					}); 
				showToast.fire(); 		
            }
            $A.get("e.force:closeQuickAction").fire();
            $A.get('e.force:refreshView').fire(); 
        });
        $A.enqueueAction(action);
    
    }
})