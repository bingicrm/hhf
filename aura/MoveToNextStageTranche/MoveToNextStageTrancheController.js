({
	  doinit : function(component, event, helper) {  // Added by saumya for TIL-00000795 
        component.set("v.Spinner", true); 

        var loanApp =  component.get("v.recordId") ;
        console.log(loanApp);
       
        var action = component.get('c.moveNextStage');
        action.setParams({
           
            "oppId": component.get("v.recordId")
        });
        action.setCallback(this, function(actionResult) {
            var msg;
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                var result = actionResult.getReturnValue();
                console.log('result::'+JSON.stringify(result));
                component.set('v.currentSubStage',result.currentStage);
                component.set('v.NextSubStage',result.NextStage);
               
                component.set("v.Spinner", false); 

            } 
            else 
            {
        		component.set("v.Spinner", true); 
            }
        });
        $A.enqueueAction(action);
    
      },
    
    doneNoAction : function(component, event, helper) { // Added by saumya for TIL-00000795 
        $A.get("e.force:closeQuickAction").fire();
    },
    onAccept : function(component, event, helper) {
        var tranId =  component.get("v.recordId") ;
        console.log(tranId);
        
        var action = component.get('c.moveNext');
        action.setParams({            
            "oppId": tranId
        });
        action.setCallback(this, function(actionResult) {
            var msg;
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                var str = actionResult.getReturnValue();
                var res ;
                console.log('==result===' + actionResult.getReturnValue());
                if(actionResult.getReturnValue() != null && str.includes("-")) {
                    res = str.split("-");		 
                if(res[0] != 'Tranche'){
                    msg= actionResult.getReturnValue();
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({
                        'type': 'Error',
                        'message' : msg
                    }); 
                    showToast.fire(); 	
                } else{                    
                    var navEvent = $A.get("e.force:navigateToList");
                    navEvent.setParams({
                        "listViewId": tranId,
                        "listViewName": "All",
                        "scope": "Tranche__c"
                    });
                    navEvent.fire();
                    msg = 'Your application has been moved to the Next Stage :'+res[1];  
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({
                        'type': 'Success',
                        'message' : msg
                    }); 
                    showToast.fire();
                }
				}
                
                else if(actionResult.getReturnValue() != null){
                    msg= actionResult.getReturnValue();
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({
                        'type': 'Error',
                        'message' : msg
                    }); 
                    showToast.fire(); 
                    
                }
                console.log('==result===' + actionResult.getReturnValue());           
            }           
			else {
                console.log('==result===' + actionResult.getState());
                var errors = actionResult.getError();                                
                console.log('==error'+errors[0]);                
                if (errors[0] && errors[0].message) {
                    msg = "Error message: " + errors[0].message;                    
                } else {
                    msg ="Please contact System admin"
                    console.log("Unknown error");
                }
                
                var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({ 
                    'title' : msg, 
                    'type': 'ERROR',
                    'message' : msg
                }); 
                showToast.fire(); 		
            }
            var listviews = actionResult.getReturnValue();
            $A.get("e.force:closeQuickAction").fire();
            $A.get('e.force:refreshView').fire(); 
            
        });
        $A.enqueueAction(action); 
	},
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    }
})