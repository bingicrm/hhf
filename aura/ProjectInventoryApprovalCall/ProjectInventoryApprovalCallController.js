({
	doInit : function(component, event, helper) {
		var action = component.get("c.countUnApprovedInventories");
        action.setParams({"projectId" : component.get("v.recordId")});
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(state === "SUCCESS"){
                if(response.getReturnValue() == 0) {
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title" : "Error",
                        "message" : "No recently added inventories for Approval."
                    });
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                }
                else {
                    component.set("v.recordCount", response.getReturnValue());
                }
            }else{
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title" : "Error",
                    "message" : "Cannot fetch the Details."
                });
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            }
        });
        $A.enqueueAction(action);
	},
    cancelCreation: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    requestSave : function(component, event, helper) {
		var loanApp =  component.get("v.recordId") ;
        
        console.log(loanApp);
       
        var action = component.get('c.sendForApproval');
        console.log('1');
        action.setParams({
            "projectId": loanApp 
        });
        console.log('2');
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") 
            {
                var result = actionResult.getReturnValue();
                //result = '';
                console.log('==result===' + actionResult.getReturnValue());
                if(result == "" || result == "Error"){
                    //var errorMess= actionResult.getReturnValue();
                    var showToast = $A.get("e.force:showToast"); 
					showToast.setParams({ 
					 					'type': 'ERROR',
										'message' : result.msg
										}); 
					showToast.fire(); 	
                    
                }
                else
                {
                    var msg;
                    //console.log('why hwere');
                    var navEvent = $A.get("e.force:navigateToObjectHome");
                    navEvent.setParams({
               					"scope": "Project__c"
            		});
            		navEvent.fire();
                    msg = 'Approval Initiated Successfully.';  
            		var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                    	'type': 'Success',
                		'message' : msg
                    }); 
                	showToast.fire();
                    //helper.UpdateFTR(component);
                }
            } 
            else 
            {
                console.log('==result===' + actionResult.getState());
                var errors = actionResult.getError();
                var errorMess;
                console.log('==error'+errors[0]);
               
               	if (errors[0] && errors[0].message) 
                {
                        errorMess = "Error message: " + errors[0].message;
                    
                } 
                else 
                {
                    errorMess ="Please contact System admin"
                    console.log("Unknown error");
                }
            
                var showToast = $A.get("e.force:showToast"); 
					showToast.setParams({ 
					'title' : errorMess, 
                    'type': 'ERROR',
					'message' : errorMess
					}); 
				showToast.fire(); 		
            }
            $A.get("e.force:closeQuickAction").fire();
            $A.get('e.force:refreshView').fire(); 
        });
        $A.enqueueAction(action);
    
    }
})