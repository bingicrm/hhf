({
    updateLoanApp : function(component, event, helper) {
         var loanApp =  component.get("v.recordId") ;
        console.log(loanApp);
       
        var action = component.get('c.updateLoanApplication');
        action.setParams({
           
            "laID": component.get("v.recordId")
        });
        action.setCallback(this, function(actionResult) {
            var msg;
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                var result = actionResult.getReturnValue();
                        console.log(result);

                if(result.error)
                {
                    var errorMess= actionResult.getReturnValue();
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                        'type': 'ERROR',
                		'message' : result.msg
                        }); 
                	showToast.fire();   
                } 
                else
                {
                    console.log('why hwere');
                    var navEvent = $A.get("e.force:navigateToObjectHome");
                    navEvent.setParams({
                       "scope": "Loan_Application__c"
                    });
                    navEvent.fire();
                    msg = 'Your application has been moved to the following stage: '+ result.msg;
            		var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                        'type': 'Success',
                        'message' : msg
                     }); 
                	showToast.fire();
                }
            } 
            else 
            {
                var errors = actionResult.getError();
              
                var errorMess;

               	if (errors[0] && errors[0].message) 
                {
                        errorMess = "Error message: " + errors[0].message;
                    
                } 
                else 
                {
                    errorMess ="Please contact System admin"
                    console.log("Unknown error");
                }
            
                var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                    'title' : errorMess, 
                        'type': 'ERROR',
                'message' : errorMess
                        }); 
                showToast.fire();       
            }
                 var listviews = actionResult.getReturnValue();
            $A.get("e.force:closeQuickAction").fire();
            $A.get('e.force:refreshView').fire(); 
           
        });
        $A.enqueueAction(action);
    
      },
        showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }
})