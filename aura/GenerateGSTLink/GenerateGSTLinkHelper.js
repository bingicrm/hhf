({
    refresh : function(component, event){
        component.set("v.isShow" , false);
        console.log('Refresh View');
    	$A.get('e.force:refreshView').fire();
    },
    
    showSpinner: function(component, event) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
      
    hideSpinner : function(component,event){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    }
	/*fetchData: function (cmp,event,helper) {
        var action = cmp.get("c.getAllAccounts");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                cmp.set('v.data',data);
            }
            // error handling when state is "INCOMPLETE" or "ERROR"
        });
        $A.enqueueAction(action);
    }*/
})