({
    doInit : function(component, event, helper) {
        var action = component.get("c.getCriteriaList");//This method will return a list of Customer Detail Records
        action.setParams({
            'loanApplicationId': component.get("v.recordId")//Customer Detail Records associated with the Loan Application
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('Response Status'+state);
            
            if (state === "SUCCESS") {
                var capturedResponse = response.getReturnValue();
                console.log('Response received'+capturedResponse);
                console.log(JSON.stringify(capturedResponse));
                if(capturedResponse.length >0) {
                    component.set("v.CustomerList", capturedResponse);
                    component.set("v.selectedCount" , 0);
                    var totalRecordsList = capturedResponse;
                    var totalLength = totalRecordsList.length;
                    component.set("v.totalRecordsCount",totalLength);
                }
                else {
                    component.set("v.bNoRecordsFound" , true);
                }
            }
        });
        $A.enqueueAction(action);
	},
     onGroup: function(cmp, evt) {
		 var selectedId = evt.getSource().get("v.text");
         cmp.set("v.selected" , true);
         cmp.set("v.selectedID" , selectedId);
         console.log('selectedId '+ selectedId);
	 },

    
    /*selectAllCheckbox: function(component, event, helper) {
        var selectedHeaderCheck = event.getSource().get("v.value");
        var updatedAllRecords = [];
        var CustomerList = component.get("v.CustomerList");
        // play a for loop on all records list 
        for (var i = 0; i < CustomerList.length; i++) {
            // check if header checkbox is 'true' then update all checkbox with true and update selected records count
            // else update all records with false and set selectedCount with 0  
            if (selectedHeaderCheck == true) {
                CustomerList[i].isChecked = true;
                component.set("v.selectedCount", CustomerList.length);
            } else {
                CustomerList[i].isChecked = false;
                component.set("v.selectedCount", 0);
            }
            updatedAllRecords.push(CustomerList[i]);
        }
        component.set("v.CustomerList", updatedAllRecords);
    },
    
    checkboxSelect: function(component, event, helper) {
        // on each checkbox selection update the selected record count 
        var selectedRec = event.getSource().get("v.value");
        var getSelectedNumber = component.get("v.selectedCount");
        if (selectedRec == true) {
            getSelectedNumber++;
        } else {
            getSelectedNumber--;
            component.find("selectAllId").set("v.value", false);
        }
        component.set("v.selectedCount", getSelectedNumber);
        // if all checkboxes are checked then set header checkbox with true   
        if (getSelectedNumber == component.get("v.totalRecordsCount")) {
            component.find("selectAllId").set("v.value", true);
        }
    },*/
 
    getSelectedRecords: function(component, event, helper) {
        helper.refresh(component, event);
        helper.showSpinner(component, event);
        var allRecords = component.get("v.CustomerList");
        var recordId = component.get("v.selectedID");
        var selectedRecords = [];
        var name;
        for (var i = 0; i < allRecords.length; i++) {
            //console.log('ID == ' + allRecords[i].objLoanContact.Id);
            if (recordId == allRecords[i].objLoanContact.Id) {
                console.log('Id Match');
                selectedRecords.push(allRecords[i]);
                name = allRecords[i].objLoanContact.Name;
            }
        }
        console.log('name' + name);
        var GSTINAuthCallout = component.get("c.makeGSTINAuthCallout");
        GSTINAuthCallout.setParams({
            'paramJSONList': JSON.stringify(selectedRecords)//JSON.stringify(component.get("v.CustomerList"))
        });
        GSTINAuthCallout.setCallback(this, function(response) {
            console.log('Inner CallBack');
            var GSTINErrorMessage = component.get("c.getErrorMessage");
            GSTINErrorMessage.setParams({
                'paramJSONList': JSON.stringify(selectedRecords)//component.get("v.CustomerList")
            });
            GSTINErrorMessage.setCallback(this, function(response) {
                console.log('set CallBack');
                var state = response.getState();
                console.log('State  ==> ' + state);
                console.log('response.getReturnValue()  ' + response.getReturnValue());
                if (state === "SUCCESS") {
                    console.log('SUCCESS...');
                    
                    console.log('SIZE' + response.getReturnValue().length);
                    if(response.getReturnValue().length > 0){
                        component.set("v.isShow" , true);
                        component.set("v.ErrorList" , response.getReturnValue());
                        console.log(component.get("v.isShow"));
                        console.log(component.get("v.ErrorList"));
                        //console.log('response.getReturnValue()  ' + response.getReturnValue());
                    }
                    if(!component.get("v.isShow")){
                        var resultsToast = $A.get("e.force:showToast");
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "GST INITIATED SUCCESSFULLY!!!",
                            "message" : "FOR : " + name,
                            "type" : "Success"
                        });
                        toastEvent.fire();
                    }
                    /*else{
                        var resultsToast = $A.get("e.force:showToast");
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "No Response !!!",
                            "message" : "From SERVER",
                            "type" : "ERROR"
                        });
                        toastEvent.fire();
                    }*/
                }
                else if (state === "INCOMPLETE") {
                    // do something
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            $A.enqueueAction(GSTINErrorMessage)
            helper.hideSpinner(component, event);
        });
        /*if(component.get("v.isShow")){
            var resultsToast = $A.get("e.force:showToast");
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "GST initiated !!!",
                "message" : "SUCCESSFULLY"
            });
            toastEvent.fire();
        }*/
        $A.enqueueAction(GSTINAuthCallout);
        //var dismissActionPanel = $A.get("e.force:closeQuickAction"); 
		//dismissActionPanel.fire();
        
    },
    
    validatePhone : function(component, event, helper)
    {
        var PhoneFieldValue = event.getSource().get("v.value");
        if (/^[6789]\d{9}$/.test(PhoneFieldValue)) {  
            // value is ok, use it //old regex used:/^\d{10}$/
            event.getSource().set("v.class",'slds-input green');
            event.getSource().set("v.errors",[]);
        } else {
           /* var resultsToast = $A.get("e.force:showToast");
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error",
                "message" : "Invalid Mobile Number...",
                "type" : "ERROR"
            });
            toastEvent.fire();*/
            event.getSource().set("v.value",'');
            event.getSource().set("v.class",'slds-input red');
            event.getSource().set("v.errors",[{message:"Invalid Mobile Number...."}]);
        }
    },
     validateEmail : function(component, event, helper) {
        var emailFieldValue = event.getSource().get("v.value");
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;  
        // check if Email field in not blank,
        // and if Email field value is valid then set error message to null, 
        // and remove error CSS class.
        // ELSE if Email field value is invalid then add Error Style Css Class.
        // and set the error Message.  
        // and set isValidEmail boolean flag value to false.
        //alert('');
        if(!$A.util.isEmpty(emailFieldValue)){   
            if(emailFieldValue.match(regExpEmailformat)){
                event.getSource().set("v.class",'slds-input green');
                event.getSource().set("v.errors",[]);
            }
            else{
            console.log('---'+event.getSource());
            event.getSource().set("v.value",'');
            event.getSource().set("v.errors",[{message:"Invalid Email Address."}]);
            event.getSource().set("v.class",'slds-input red');
            //alert('Please Enter a Valid Email Address');
        }
        }
         else{
             event.getSource().set("v.value",'');
            event.getSource().set("v.errors",[{message:"Enter Email Address."}]);
            event.getSource().set("v.class",'slds-input red');
         }
        

    }
   
})