<!--**********************************************************************************
* VisualForce Page: edInitiateLargeFilesUpload
* Created by Suresh Meghnathi: 11/09/2019

* - This is used to used to initiate the large file uploads.
* 
* - Modifications:
* - Suresh Meghnathi, 11/09/2019 - Initial Development
***********************************************************************************-->
<apex:page controller="edInitiateLargeFilesUploadController" sidebar="false" action="{!init}" title="Move Documents in Amazon S3" tabStyle="Loan_Application__c">
    <apex:stylesheet value="{!URLFOR($Resource.NEILON__Jquery, 'jquery/css/ui-smoothness/jquery-ui.min.css')}" />
    <apex:includeScript value="{!$Resource.NEILON__Jquery}/jquery/js/jquery-2.2.4.min.js"></apex:includeScript>
    <apex:includeScript value="{!$Resource.NEILON__Jquery}/jquery/js/jquery-migrate-1.3.0.min.js"></apex:includeScript>
    <apex:includeScript value="{!$Resource.NEILON__Jquery}/jquery/js/jquery-ui-1.11.4.min.js"></apex:includeScript>
    <apex:includeScript value="{!$Resource.NEILON__AppurinUtils}/appurin-util.min.js"></apex:includeScript>
    <apex:includeScript value="{!$Resource.NEILON__AppurinUtils}/appurin-prototype.min.js"></apex:includeScript>
    <apex:includeScript value="{!$Resource.NEILON__Jquery}/jquery/easytooltip/easyTooltip.min.js"></apex:includeScript>
    <apex:includeScript value="/lightning/lightning.out.js" />
    <apex:includeScript value="{!$Resource.NEILON__AppurinUtils}/appurin-lightning.min.js" />
    <apex:includeScript value="/support/console/44.0/integration.js"/>
    <apex:includeScript value="{!$Resource.NEILON__AppurinUtils}/appurin-console.min.js"></apex:includeScript>
    
    <apex:stylesheet value="{!URLFOR($Resource.NEILON__AppurinUtils, 'resources/css/appurin.min.css')}" />  
    <apex:stylesheet value="{!URLFOR($Resource.NEILON__SLDS, 'assets/styles/salesforce-lightning-design-system-vf.min.css')}" />  
    <apex:stylesheet value="{!URLFOR($Resource.NEILON__AppurinUtils, 'resources/css/appurin-lightning.min.css')}" />
    
    <script type="text/javascript">
        var j$ = jQuery.noConflict();
        // Set salesforce ui theme
        Appurin.lightning.setUITheme('{!$User.UIThemeDisplayed}');
        Appurin.lightning.setSLDSPath('{!URLFOR($Resource.NEILON__SLDS, '')}');
    </script>
    
    <style>
         .slds-nubbin--left-top:after {
            margin-top: -.9rem !important;
        }
        .slds-nubbin--left-top:before {
            margin-top: -.9rem !important;
        }
        .slds-section__content{
            padding: .75rem !important;
        }
        .slds-spinner_container{
            z-index: 9002;
        }
        .slds .slds-notify ul {
            list-style-type: disc;
            padding-left: 40px;
        }
        .slds .slds-notify ol {
            list-style-type: decimal;
            padding-left: 40px;
        }
        .slds .slds-page-header{
            border: 0px;
            border-bottom: 1px solid #dddbda !important;
            border-radius: 0px;
            box-shadow: none;
        }
        .slds-card__body{
            margin-bottom: 1.4rem !important;
        }
        .slds .slds-button-group{
            display: flex;
        }
    </style>
    <apex:outputPanel rendered="{!$User.UIThemeDisplayed == 'Theme4t'}">
        <style>
            #contentWrapper{
                min-width: 0px !important;
            }
        </style>
    </apex:outputPanel>
    
    <!-- Use Lightning background -->
    <script>
        if(!Appurin.lightning.isLightningExperience()){
            j$("table[id$='bodyTable']").css('padding', '0px').find("td.noSidebarCell").css('padding', '0px');
        } else {
            var htmlDOM = window.document.getElementsByTagName('HTML')[0];
            htmlDOM.style.backgroundColor = 'rgba(176, 196, 223, 1.0)';
            htmlDOM.style.height = '100%';
            var bodyDOM = window.document.body;
            bodyDOM.style.padding = '0px';
            bodyDOM.style.margin = '0px';
        }
    </script>
    
     <style>
        .apLightningOneTile{
            margin: 1.5rem !important;
            margin-right: 15% !important;
            margin-left: 15% !important;
        }
    </style>
    
    <script>
        // Folder infos
        var folderInfos = [];
        
        // Window ready
        j$(document).ready(function() {
            // Folder info string
            folderInfos = JSON.parse('{!JSENCODE(foldersInformation)}');
            
            // Upload files
            uploadFiles(folderInfos);
        });
        
        /*
        This method is used to upload files.
        */
        function uploadFiles(folderInfos){
            // Set easy tooltip
            easyToolTipForAll();
            
            // Show wait cursor
            Appurin.startSplash();
            
            // Show list of folders
            for(var i = 0; i < folderInfos.length; i++){
                // Show folder detail
                showFolderDetail(i);
                
                // Start upload
                var folderInfo = folderInfos[i];
                
                // Get URL
                var uploadURL = '/apex/NEILON__edUploadSalesforceFiles';
                
                // Get file ids
                var fileIds = folderInfo.documents.join(',');
                uploadURL = uploadURL + '?sfFileIds='+fileIds
                
                // Get folder id
                uploadURL = uploadURL + '&folderId='+folderInfo.id;
                
                // Open window for download
                window.open(uploadURL, '_blank');
            }
            
            // Hide wait cursor
            Appurin.endSplash();
        }
        
        /*
        This method is used to show easy tooltip for components.
        */
        function easyToolTipForAll() {
            j$('.apHelpText').easyTooltip({width: '200px', isLightning:true, isFormatted:true});
            j$('.apHelpTextSimple').easyTooltip({width: '200px', isLightning:true, isFormatted:true, toolTipType:'TEXT'});
            return false;
        }
        
        /*
        This method is used to show detail of folders.
        */
        function showFolderDetail(index){
            // Add table row
            var folderInfoSection = createFolderInfo(folderInfos[index], index);
            j$("table[id$='foldersListTable']").find("tbody").append(folderInfoSection);
            
            // Make table visible
            j$("div[id$='foldersList']").css('display', '');
        }
        
        /*
        This method is used to create file information.
        */
        function createFolderInfo(folderData, index){
            // File row
            var folderInfoRow = j$("<tr id='fileDetail"+index+"' class='slds-hint-parent'/>");
            var fileNameColumn = j$("<td/>");
            var fileNameInput = j$("<label/>").text(folderData.name).attr('id', 'fileName'+index);
            fileNameColumn.append(fileNameInput);
            folderInfoRow.append(fileNameColumn);
            
            var fileSizeColumn = j$("<td/>");
            var fileSizeLabel = j$("<label/>").text(folderData.count);
            fileSizeColumn.append(fileSizeLabel);
            folderInfoRow.append(fileSizeColumn);
            
            return folderInfoRow;
        }
        
        /*
        This method is used to retry.
        */
        function retry(foldersInformation){
            // Folder info string
            folderInfos = JSON.parse(foldersInformation);
            
            // Upload files
            uploadFiles(folderInfos);
        }
        
        /*
        This method is used to close window.
        */
        function closeWindow(){
            var retURL = '{!JSENCODE($CurrentPage.parameters.retURL)}';
            var loanApplicationId = '{!loanApplicationId}';
            if(Appurin.lightning.isLightningExperience()){
                if(lightningBackRecordId != ''){
                    if(!lightningBackRecordId.startsWith('00B')){
                        sforce.one.navigateToSObject(lightningBackRecordId);
                    } else{
                        sforce.one.navigateToList(lightningBackRecordId, null, 'Loan_Application__c');
                    }
                } else{
                    Appurin.lightning.back(true);
                }
            } else{
                if(sforce.console.isInConsole()){
                    Appurin.console.closeConsoleTab();
                } else if(retURL != ''){
                    window.location = retURL;;
                } else if(loanApplicationId){
                    window.location = '/' + loanApplicationId;
                }
            }
            return false;
        }
    </script>
        
    <apex:actionStatus id="splashStatus" onstart="Appurin.startSplash();" onstop="Appurin.endSplash();" />
    <div class="slds">
        <div id="splashDiv" class="apInitiallyDivDisplayNone" style="z-index:9998;">
            <div class="slds-spinner_container apLightningSpinnerContainer">
                <div role="status" class="slds-spinner slds-spinner--medium slds-spinner--brand">
                    <span class="slds-assistive-text">Loading</span>
                    <div class="slds-spinner__dot-a"></div>
                    <div class="slds-spinner__dot-b"></div>
                </div>
            </div>
        </div>
    </div>
    
    <apex:form id="form">
        <apex:actionFunction name="refresh" action="{!init}" rerender="form" onComplete="easyToolTipForAll(); return retry('{!JSENCODE(foldersInformation)}');"/>
        <div class="apLightningForm">
            <div style="background:#f4f6f9; padding:1rem;" class="apLightningTransparent">
                <div class="slds-panel slds-nowrap apLightningTransparent" style="background:#f4f6f9;">
                    <div class="{!IF($User.UIThemeDisplayed == 'Theme4t', '', 'apLightningOneTile')}" style="padding:0px;">
                        <apex:outputPanel rendered="{!!isLoadSuccess}">
                            <apex:pagemessages id="pageLoadErrorMessage"/>
                            <script>
                                Appurin.lightning.createLightningPageMessage({'classicPageMessageId' : '{!$Component.pageLoadErrorMessage}'});   
                            </script>
                        </apex:outputPanel>
                        <apex:outputPanel styleClass="slds" layout="block" rendered="{!isLoadSuccess}">
                            <div class="slds-grid apLightningTransparent" style="background:#f4f6f9;padding: 0px;">
                                <div class="slds-panel slds-grid slds-grid--vertical slds-nowrap slds-is-editing" style="box-shadow: none;">
                                    <div class="slds-form--stacked slds-grow slds-scrollable--y">
                                        <div class="slds-panel__section slds-has-divider--bottom">
                                            <div class="slds-media">
                                                <div class="slds-media__figure">
                                                    <span class="slds-icon_container">
                                                        <apex:image styleClass="slds-icon slds-page-header__icon" value="{!URLFOR($Resource.NEILON__AppurinUtils, 'resources/images/fileIcons/attachment_60.png')}"/>
                                                    </span>
                                                </div>
                                                <div class="slds-media__body">
                                                    <h1 class="slds-page-header__title slds-truncate slds-align-middle">Move Documents in Amazon S3</h1>
                                                    <p class="slds-text-body_small slds-line-height_reset">Folders will be created and documents will be uploaded in different folders in Amazon S3.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slds-panel__section">
                                            <apex:outputPanel id="errorSection">
                                                <apex:pageMessages escape="false" id="errorMessages"/>
                                                <script>
                                                    Appurin.lightning.createLightningPageMessage({'classicPageMessageId' : '{!$Component.errorMessages}'});   
                                                </script>
                                            </apex:outputPanel>
                                            <div id="fileDetailSection">
                                                <div id="foldersList" style="display:none;">
                                                    <div style="padding-bottom:5px;" id="successMessageDiv"></div>
                                                    <table id="foldersListTable" class="slds-table slds-table--bordered slds-table--cell-buffer">
                                                        <thead>
                                                            <tr class="slds-text-heading--label">
                                                                <th scope="col">
                                                                    <div class="slds-truncate">{!$ObjectType.NEILON__Folder__c.fields.Name.Label}</div>
                                                                </th>
                                                                <th scope="col">
                                                                    <div class="slds-truncate">Total Documents</div>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slds-panel__actions slds-has-divider--top" style="background-color:white;">
                                <div class="slds-grid slds-grid--align-center">
                                    <button type="button" id="retryButton" class="slds-button slds-button--brand" onClick="return refresh();">Retry</button>
                                    <button type="button" id="cancelButton" class="slds-button slds-button--neutral" onClick="return closeWindow('{!loanApplicationId}');">Cancel</button>
                                </div>
                            </div>
                        </apex:outputPanel>
                    </div>
                </div>
            </div>
        </div>
    </apex:form>
</apex:page>