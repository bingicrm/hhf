<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_FI_Combination</fullName>
        <field>Combination__c</field>
        <formula>Scheme_Lookup__c + TEXT(Customer_Segment__c) + TEXT(Transaction_Type__c) + TEXT(Type_of_Address__c) + TEXT(Loan_Contact_Type__c) + IF(FI__c == TRUE, &apos;TRUE&apos;, &apos;FALSE&apos;)</formula>
        <name>Update FI Combination</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate FI Combination</fullName>
        <actions>
            <name>Update_FI_Combination</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Scheme_Lookup__c &lt;&gt; NULL &amp;&amp; TEXT(Customer_Segment__c) &lt;&gt; NULL &amp;&amp;  TEXT(Transaction_Type__c) &lt;&gt; NULL &amp;&amp;  TEXT(Type_of_Address__c) &lt;&gt; NULL &amp;&amp;  TEXT(Loan_Contact_Type__c) &lt;&gt; NULL</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
