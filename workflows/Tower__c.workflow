<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Construction_Status_Update_Date</fullName>
        <field>Construction_Status_Update_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Construction Status Update Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Construction Status Update Date</fullName>
        <actions>
            <name>Update_Construction_Status_Update_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Construction_Status_Percentage__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
