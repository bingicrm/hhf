<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_BRE_Record_on_Address</fullName>
        <field>BRE_Record__c</field>
        <literalValue>0</literalValue>
        <name>Update BRE Record on Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update BRE Check on Address</fullName>
        <actions>
            <name>Update_BRE_Record_on_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( BRE_Record__c = TRUE ,ISCHANGED(BRE_Check__c) ,BRE_Check__c = TRUE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
