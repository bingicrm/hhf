<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Duplicate_Flat_Number</fullName>
        <field>Duplicate_Flat_Number__c</field>
        <formula>Project__c &amp;  Flat_House_No__c &amp;  Floor__c &amp;  Tower__c</formula>
        <name>Populate Duplicate Flat Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Duplicate Detection</fullName>
        <actions>
            <name>Populate_Duplicate_Flat_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project_Inventory__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
