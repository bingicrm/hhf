<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_an_email_alert_to_customer_for_PERFIOS_ITR_analysis</fullName>
        <description>Send an email alert to customer for PERFIOS ITR analysis</description>
        <protected>false</protected>
        <recipients>
            <field>PerfiosEmail__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Perfios_ITR</template>
    </alerts>
    <alerts>
        <fullName>Send_an_email_alert_to_customer_for_PERFIOS_NetBanking_analysis</fullName>
        <description>Send an email alert to customer for PERFIOS NetBanking analysis</description>
        <protected>false</protected>
        <recipients>
            <field>PerfiosEmail__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Perfios_Net_Banking</template>
    </alerts>
    <fieldUpdates>
        <fullName>CIBIL_CheckBox_Check</fullName>
        <field>CIBIL_Completed__c</field>
        <literalValue>1</literalValue>
        <name>CIBIL CheckBox Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Loan_Applications__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CIBIL_Corporate_Check</fullName>
        <field>CIBIL_Verified__c</field>
        <literalValue>1</literalValue>
        <name>CIBIL Corporate Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CIBIL_Verified_false</fullName>
        <field>CIBIL_Verified__c</field>
        <literalValue>0</literalValue>
        <name>CIBIL Verified false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CIBIL_verified_true</fullName>
        <field>CIBIL_Verified__c</field>
        <literalValue>1</literalValue>
        <name>CIBIL verified true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Corporate_Flag</fullName>
        <field>Corporate_Check__c</field>
        <literalValue>1</literalValue>
        <name>Check Corporate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Loan_Applications__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Customer_segment_on_app</fullName>
        <field>Applicant_Customer_segment__c</field>
        <formula>TEXT(Customer_segment__c)</formula>
        <name>Customer segment on app</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Loan_Applications__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EQ_Maker_Mandatory_Field</fullName>
        <field>EQ_Maker_Mandatory_Field__c</field>
        <literalValue>1</literalValue>
        <name>EQ Maker Mandatory Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FalseCorpCibil</fullName>
        <field>Retrigger_Corp_CIBIL__c</field>
        <literalValue>0</literalValue>
        <name>FalseCorpCibil</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Income_Considered</fullName>
        <field>Income_Considered__c</field>
        <literalValue>1</literalValue>
        <name>Income Considered</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pan_Validated_to_false</fullName>
        <field>Pan_Card_Validated__c</field>
        <literalValue>0</literalValue>
        <name>Pan Validated to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Loan_Applications__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Checkbox_to_True</fullName>
        <field>Mandatory_Fields_Completed__c</field>
        <literalValue>1</literalValue>
        <name>Set Checkbox to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Cibil_Remarks</fullName>
        <field>CIBIL_Remarks__c</field>
        <formula>&apos;Not Applicable, based on Constitution&apos;</formula>
        <name>Set Cibil Remarks</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Cibil_Report_Status</fullName>
        <field>CIBIL_Report_Status__c</field>
        <literalValue>Positive</literalValue>
        <name>Set Cibil Report Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Cibil_Verified_to_True</fullName>
        <field>CIBIL_Verified__c</field>
        <literalValue>1</literalValue>
        <name>Set Cibil Verified to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_BRE_Record_on_CD</fullName>
        <field>BRE_Record__c</field>
        <literalValue>0</literalValue>
        <name>Update BRE Record on CD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CIBIL_Response_on_Customer</fullName>
        <field>CIBIL_Response__c</field>
        <literalValue>0</literalValue>
        <name>Update CIBIL Response on Customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CheckBox_for_Data_Maker</fullName>
        <field>Mandatory_Fields_at_Data_Maker__c</field>
        <literalValue>1</literalValue>
        <name>Update CheckBox for Data Maker</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Duplicate_Detector_of_Loan_Contac</fullName>
        <field>Duplicate_Loan_Contact__c</field>
        <formula>Loan_Applications__c &amp;  Customer__c</formula>
        <name>Update Duplicate Detector of Loan Contac</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Has_Gurrantor_flag_on_LA</fullName>
        <field>HasGurrantorCustomer__c</field>
        <literalValue>1</literalValue>
        <name>Update Has Gurrantor flag on LA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Loan_Applications__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PAN_CheckBox_Check</fullName>
        <field>Pan_Card_Validated__c</field>
        <literalValue>1</literalValue>
        <name>Update PAN CheckBox Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Loan_Applications__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PAN_Response_on_Customer_Detail</fullName>
        <field>PAN_Response__c</field>
        <literalValue>0</literalValue>
        <name>Update PAN Response on Customer Detail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_mail_LA</fullName>
        <field>Customer_Email__c</field>
        <formula>Email__c</formula>
        <name>Update mail LA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Loan_Applications__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>approved_foreclosure_charges</fullName>
        <field>Approved_Foreclosure_Charges__c</field>
        <formula>0.02</formula>
        <name>approved  foreclosure charges</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Loan_Applications__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>approved_individual_foreclosure_charges</fullName>
        <field>Approved_Foreclosure_Charges__c</field>
        <formula>0.0</formula>
        <name>approved individual  foreclosure charges</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Loan_Applications__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>CIBIL Response check on Customer</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Contact__c.CIBIL_Response__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_CIBIL_Response_on_Customer</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Loan_Contact__c.CIBIL_Response_Time__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Corporate CIBIL</fullName>
        <actions>
            <name>CIBIL_Corporate_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Contact__c.CIBIL_Report_Status__c</field>
            <operation>equals</operation>
            <value>Positive,Negative</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Contact__c.CIBIL_Remarks__c</field>
            <operation>notEqual</operation>
            <value>NULL</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Corporate Tag Update</fullName>
        <actions>
            <name>Check_Corporate_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Borrower__c, &apos;2&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Duplicate Detection for Loan Contact</fullName>
        <actions>
            <name>Update_Duplicate_Detector_of_Loan_Contac</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EQ Maker Mandatory Field</fullName>
        <actions>
            <name>EQ_Maker_Mandatory_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>And(!ISBLANK( Gross_Salary__c ), !ISBLANK( Net_Salary__c ), !ISBLANK( Profit_After_tax__c ), !ISBLANK( Depreciation__c ), !ISBLANK( Net_Income__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Foreclosure Charges on Corporate Customer</fullName>
        <actions>
            <name>approved_foreclosure_charges</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Contact__c.Applicant_Type__c</field>
            <operation>equals</operation>
            <value>Applicant,Co- Applicant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Contact__c.Borrower__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Application__c.Insurance_Loan_Application__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>The value of Approved Foreclosure Charges are dependent on customer type of the Applicant and Co-Applicant.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Foreclosure charges on Individual</fullName>
        <actions>
            <name>approved_individual_foreclosure_charges</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Loan_Contact__c.Applicant_Type__c</field>
            <operation>equals</operation>
            <value>Applicant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Contact__c.Borrower__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <description>the value of foreclosure charges are dependent on customer type of the applicant is individual</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Income considered</fullName>
        <actions>
            <name>Income_Considered</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Contact__c.Applicant_Type__c</field>
            <operation>equals</operation>
            <value>Applicant</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LoanContact</fullName>
        <actions>
            <name>FalseCorpCibil</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Contact__c.Borrower__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Contact__c.Retrigger_Corp_CIBIL__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Mandatory Fields Check</fullName>
        <actions>
            <name>Set_Checkbox_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3) OR (2 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Loan_Contact__c.Applicant_Status__c</field>
            <operation>equals</operation>
            <value>1,2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Contact__c.Customer_segment__c</field>
            <operation>equals</operation>
            <value>1,3,4,9,10,11,12,13,15,14</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Contact__c.Borrower__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Contact__c.Borrower__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Mandatory fields at Data Maker</fullName>
        <actions>
            <name>Update_CheckBox_for_Data_Maker</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3 ) OR (4 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Loan_Contact__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Salaried</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Contact__c.Gross_Salary__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Contact__c.Count_of_Customer_Financial__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Contact__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Others,Others DC</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PAN Response check on Customer</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Contact__c.PAN_Response__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_PAN_Response_on_Customer_Detail</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Loan_Contact__c.CIBIL_Response_Time__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Populate mail on LA</fullName>
        <actions>
            <name>Update_mail_LA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Application__c.Sub_Stage__c</field>
            <operation>equals</operation>
            <value>File Check</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Contact__c.Email__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Contact__c.Applicant_Type__c</field>
            <operation>equals</operation>
            <value>Applicant</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send ITR Link to Customer</fullName>
        <actions>
            <name>Send_an_email_alert_to_customer_for_PERFIOS_ITR_analysis</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>PRIORVALUE(ITR_URL__c)   &lt;&gt;  ITR_URL__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send NetBanking Link to Customer</fullName>
        <actions>
            <name>Send_an_email_alert_to_customer_for_PERFIOS_NetBanking_analysis</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>PRIORVALUE(NetBanking_URl__c)   &lt;&gt;  NetBanking_URl__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Cibil Verified to True</fullName>
        <actions>
            <name>Set_Cibil_Remarks</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Cibil_Report_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Cibil_Verified_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Contact__c.Borrower__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Contact__c.Constitution__c</field>
            <operation>notEqual</operation>
            <value>18,19,25</value>
        </criteriaItems>
        <description>This WF Rule will set Cibil Verified to True, if Borrower Type is Corporate, and Constitution is not any of these values : Public Ltd, Pvt. Ltd., LLP.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update BRE Check on Customer Detail</fullName>
        <actions>
            <name>Update_BRE_Record_on_CD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( BRE_Record__c = TRUE ,ISCHANGED(BRE_Check__c) ,BRE_Check__c = TRUE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update CIBIL CheckBox</fullName>
        <actions>
            <name>CIBIL_CheckBox_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( (Loan_Applications__r.CIBIL_Validaion_Check_Count__c = Loan_Applications__r.All_Loan_Contact_Count__c), Loan_Applications__r.All_Loan_Contact_Count__c &lt;&gt; 0 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update CIBIL verified to false for inc considered</fullName>
        <actions>
            <name>CIBIL_Verified_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Loan_Contact__c.Income_Considered__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Contact__c.Count_of_CIBIL_Records__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update CIBIL verified to true for inc considered</fullName>
        <actions>
            <name>CIBIL_verified_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Loan_Contact__c.Income_Considered__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Cibil verified to false for Corporate</fullName>
        <actions>
            <name>CIBIL_Verified_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Borrower__c,&apos;2&apos;) &amp;&amp; 
(ISPICKVAL(Constitution__c,&apos;18&apos;) || 
ISPICKVAL(Constitution__c,&apos;19&apos;) || 
ISPICKVAL(Constitution__c,&apos;25&apos;) ) &amp;&amp; 
ISPICKVAL(Loan_Applications__r.Sub_Stage__c,&apos;COPS:Data Maker&apos;) &amp;&amp; 
( ISBLANK(CIBIL_Remarks__c) || ISNULL(CIBIL_Remarks__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Cibil verified to true for Corporate</fullName>
        <actions>
            <name>CIBIL_verified_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Contact__c.Borrower__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Application__c.Sub_Stage__c</field>
            <operation>equals</operation>
            <value>Application Initiation,Express Queue: Data Maker,Express Queue: Data Checker,Document collection,File Check,Scan: Data Maker,Scan: Data Checker,Customer Negotiation</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Has Gurrantor Flag on LA</fullName>
        <actions>
            <name>Update_Has_Gurrantor_flag_on_LA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Contact__c.Applicant_Type__c</field>
            <operation>equals</operation>
            <value>Guarantor</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update PAN CheckBox</fullName>
        <actions>
            <name>Update_PAN_CheckBox_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND((Loan_Applications__r.PAN_Customer_Detail__c = Loan_Applications__r.All_Loan_Contact_Count__c),Loan_Applications__r.All_Loan_Contact_Count__c &lt;&gt; 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update PAN CheckBox False</fullName>
        <actions>
            <name>Pan_Validated_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR((Loan_Applications__r.PAN_Customer_Detail__c &lt;&gt; Loan_Applications__r.All_Loan_Contact_Count__c),Loan_Applications__r.All_Loan_Contact_Count__c = 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update PAN CheckBox True</fullName>
        <actions>
            <name>Update_PAN_CheckBox_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND((Loan_Applications__r.PAN_Customer_Detail__c = Loan_Applications__r.All_Loan_Contact_Count__c),Loan_Applications__r.All_Loan_Contact_Count__c &lt;&gt; 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update applicant customer segment on la</fullName>
        <actions>
            <name>Customer_segment_on_app</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 
ISPICKVAL(Applicant_Type__c , &quot;Applicant&quot;), 
OR( 
ISPICKVAL( Customer_segment__c, &quot;1&quot;), 
ISPICKVAL( Customer_segment__c, &quot;3&quot;), 
ISPICKVAL( Customer_segment__c, &quot;4&quot;), 
ISPICKVAL( Customer_segment__c, &quot;9&quot;), 
ISPICKVAL( Customer_segment__c, &quot;10&quot;), 
ISPICKVAL( Customer_segment__c, &quot;11&quot;), 
ISPICKVAL( Customer_segment__c, &quot;12&quot;), 
ISPICKVAL( Customer_segment__c, &quot;13&quot;), 
ISPICKVAL( Customer_segment__c, &quot;15&quot;), 
ISPICKVAL( Customer_segment__c, &quot;14&quot;) 
), 
ISCHANGED(Customer_segment__c) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
