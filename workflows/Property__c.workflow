<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>approved_crosssell_amount_value</fullName>
        <field>Approved_cross_sell_amount__c</field>
        <formula>Loan_Application__r.Approved_Loan_Amount__c * 0.01</formula>
        <name>approved crosssell amount value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>processing_fee1</fullName>
        <field>Approved_Processing_Fee__c</field>
        <formula>Loan_Application__r.Approved_Loan_Amount__c * 0.01</formula>
        <name>applicableprocessing fee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
</Workflow>
