<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Unique_Filed</fullName>
        <field>Unique_Value__c</field>
        <formula>CCM__r.Id +  BCM__r.Id +  ZCM__r.Id +  ZCM_Salaried__r.Id +  NCM__r.Id + BH__r.Id + CRO__r.Id</formula>
        <name>Unique Filed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Unique Filed Value</fullName>
        <actions>
            <name>Unique_Filed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Hierarchy__c.Active__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
