<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>User_Sourcing_Update</fullName>
        <field>User_Sourcing_Servicing_Combination__c</field>
        <formula>User__c +  Sourcing_Branch__c +  Servicing_Branch__c</formula>
        <name>User Sourcing Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate User Sourcing Servicing Branch Combinations</fullName>
        <actions>
            <name>User_Sourcing_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
