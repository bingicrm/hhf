<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Activated_Date_Time</fullName>
        <field>Activated_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Populate Activated Date/ Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Deactivated_Date_Time</fullName>
        <field>Deactivated_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Populate Deactivated Date/ Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Capture Deactivated Date%2F Time</fullName>
        <actions>
            <name>Populate_Deactivated_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IsActive = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Activated Date</fullName>
        <actions>
            <name>Populate_Activated_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IsActive = True</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
