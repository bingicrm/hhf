<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_TPV_Owner_APF</fullName>
        <description>Email to TPV Owner - APF</description>
        <protected>false</protected>
        <recipients>
            <field>Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Assignment_of_TP_Verification_APF</template>
    </alerts>
    <fieldUpdates>
        <fullName>Check_Re_Initiated_APF</fullName>
        <field>Re_Initiated__c</field>
        <literalValue>1</literalValue>
        <name>Check Re Initiated - APF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Expired_APF</fullName>
        <field>Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Set Status to Expired - APF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Third_Party_Expiry_APF</fullName>
        <field>Expiry_Date__c</field>
        <formula>Date_of_Report__c + 10</formula>
        <name>Update Third Party Expiry - APF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Times_Re_Initiated_APF</fullName>
        <field>Times_Re_Initiated__c</field>
        <formula>Times_Re_Initiated__c + 1</formula>
        <name>Update Times Re-Initiated - APF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CheckReinitiatedAndUpdateCounter - APF</fullName>
        <actions>
            <name>Check_Re_Initiated_APF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Times_Re_Initiated_APF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISPICKVAL(Status__c,&quot;Initiated&quot;), OR( ISPICKVAL(PRIORVALUE(Status__c),&quot;Completed&quot;), ISPICKVAL(PRIORVALUE(Status__c),&quot;Expired&quot;) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TPV Status is Initiated - APF</fullName>
        <actions>
            <name>Email_to_TPV_Owner_APF</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>OR( AND(ISPICKVAL( Status__c , &apos;Initiated&apos;), ISCHANGED( Owner__c )), AND( ISPICKVAL( Status__c , &apos;Initiated&apos;), NOT(ISPICKVAL(PRIORVALUE(Status__c ), &apos;Initiated&apos;)) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Third Party 60 days expiration - APF</fullName>
        <actions>
            <name>Set_Status_to_Expired_APF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Third_Party_Verification_APF__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Third_Party_Verification_APF__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>FCU,Legal,Technical</value>
        </criteriaItems>
        <criteriaItems>
            <field>Project__c.Project_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Expiry Date - APF</fullName>
        <actions>
            <name>Update_Third_Party_Expiry_APF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Third_Party_Verification_APF__c.Date_of_Report__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
