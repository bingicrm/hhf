<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Capture_Day_Count</fullName>
        <field>Days_Gap__c</field>
        <formula>Loan_Application__r.Business_Date_Modified__c  - Business_Date__c</formula>
        <name>Capture Day Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_the_Box</fullName>
        <field>Consider_for_Insurance__c</field>
        <literalValue>1</literalValue>
        <name>Check the Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LMTD_False</fullName>
        <field>LMTD__c</field>
        <literalValue>0</literalValue>
        <name>LMTD False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LMTD_True</fullName>
        <field>LMTD__c</field>
        <literalValue>1</literalValue>
        <name>LMTD True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Evaluate Consider for Insurance</fullName>
        <actions>
            <name>Check_the_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(
Active__c = TRUE,
OR(
Insurance__c = TRUE,
AND(
Approved_Cross_Sell__c &gt; 0,
Insurance_Loan_Created__c = FALSE
)
)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LMTD Check</fullName>
        <actions>
            <name>LMTD_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Checks the LMTD checkbox on Achievements</description>
        <formula>AND(
 OR(Month_Difference__c = 1, Month_Difference__c = -1),
 DAY(Business_Date__c) &lt;= DAY(TODAY()) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LMTD Uncheck</fullName>
        <actions>
            <name>LMTD_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Unchecks the LMTD checkbox on Achievements</description>
        <formula>AND(
 OR(Month_Difference__c = 2, Month_Difference__c = -2),
 LMTD__c = TRUE
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Days Gap</fullName>
        <actions>
            <name>Capture_Day_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To capture the number of days between current date and achievement&apos;s business date</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
