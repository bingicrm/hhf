<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Disbursal_Amount_update</fullName>
        <field>Disbursal_Amount__c</field>
        <formula>Tranche__r.Approved_Disbursal_Amount__c</formula>
        <name>Disbursal Amount update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Repayment_on_disbursement_checker</fullName>
        <field>Repayment_schedule_at_Disbursement_Check__c</field>
        <literalValue>1</literalValue>
        <name>Repayment on disbursement checker</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PEMI</fullName>
        <field>PEMI__c</field>
        <formula>Tranche__r.PEMI_Amount__c</formula>
        <name>Update PEMI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Value_for_DxROI</fullName>
        <field>Disbursement_DxROI__c</field>
        <formula>(Disbursal_Amount__c * Loan_Application__r.Approved_ROI__c )*100</formula>
        <name>Update Value for DxROI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Value_for_DxROI_Tranche</fullName>
        <field>Disbursement_DxROI_Tranche__c</field>
        <formula>( Cheque_Amount__c * Loan_Application__r.Approved_ROI__c )*100</formula>
        <name>Update Value for DxROI Tranche</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>disbursement_checked</fullName>
        <field>Disbursal_Details_entered__c</field>
        <literalValue>1</literalValue>
        <name>disbursement checked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Disbursement details entered</fullName>
        <actions>
            <name>disbursement_checked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Populate DxROI on LA</fullName>
        <actions>
            <name>Update_Value_for_DxROI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISBLANK( Tranche__c ), OR( ISNEW(), PRIORVALUE(Disbursal_Amount__c) &lt;&gt;  Disbursal_Amount__c  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate DxROI on Tranche</fullName>
        <actions>
            <name>Update_Value_for_DxROI_Tranche</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  NOT(ISBLANK( Tranche__c )),  OR(  ISNEW(),  PRIORVALUE( Cheque_Amount__c ) &lt;&gt; Cheque_Amount__c   )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Repayment on disbursement checker</fullName>
        <actions>
            <name>Repayment_on_disbursement_checker</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Application__c.Sub_Stage__c</field>
            <operation>equals</operation>
            <value>Disbursement Maker</value>
        </criteriaItems>
        <description>repayment schedule must be initiated at disbursement checker</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Disbursal Amount</fullName>
        <actions>
            <name>Disbursal_Amount_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_PEMI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Disbursement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Others</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
