<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>APF_Status_APF_Inventory_Approved</fullName>
        <field>APF_Status__c</field>
        <literalValue>APF Inventory Approved</literalValue>
        <name>APF Status : APF Inventory Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>APF_Status_Blacklisted</fullName>
        <field>APF_Status__c</field>
        <literalValue>Blacklisted</literalValue>
        <name>APF Status : Blacklisted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Blacklisting_Status_Blacklisted</fullName>
        <field>Blacklisting_Status__c</field>
        <literalValue>Project Blacklisted</literalValue>
        <name>Blacklisting Status : Blacklisted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Blacklisting_Status_Blacklisting_Remov</fullName>
        <field>Blacklisting_Status__c</field>
        <literalValue>Blacklisting Removed</literalValue>
        <name>Blacklisting Status : Blacklisting Remov</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Blacklisting_Status_Update</fullName>
        <field>Blacklisting_Status__c</field>
        <name>Blacklisting Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Blacklisting_Status_Update_Approved</fullName>
        <field>Blacklisting_Status__c</field>
        <literalValue>Project Blacklisted</literalValue>
        <name>Blacklisting Status Update : Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Blacklisting_Status_Update_Rejected</fullName>
        <field>Blacklisting_Status__c</field>
        <literalValue>Blacklisting Rejected</literalValue>
        <name>Blacklisting Status Update : Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Inventory_Approval_Status_Approved</fullName>
        <field>Inventory_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Inventory Approval Status : Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Inventory_Approval_Status_Pending</fullName>
        <field>Inventory_Approval_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Inventory Approval Status : Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Inventory_Approval_Status_Rejected</fullName>
        <field>Inventory_Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Inventory Approval Status - Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Project_Status_APF_Inventory_Approved</fullName>
        <field>APF_Status__c</field>
        <literalValue>APF Inventory Approved</literalValue>
        <name>Project Status - APF Inventory Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recently_Added_Inventory_False</fullName>
        <field>Inventories_Recently_Added__c</field>
        <literalValue>0</literalValue>
        <name>Recently Added Inventory - False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_APF_Status_to_APF_Approved</fullName>
        <field>APF_Status__c</field>
        <literalValue>APF Approved</literalValue>
        <name>Set APF Status to APF Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Approval_Communication</fullName>
        <field>Stage__c</field>
        <literalValue>Approval Communication</literalValue>
        <name>Set Stage to Approval Communication</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Sub_Stage_to_Approval_Communication</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Approval Communication</literalValue>
        <name>Set Sub Stage to Approval Communication</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Update_Apf_Initiation</fullName>
        <field>Stage__c</field>
        <literalValue>APF Initiation</literalValue>
        <name>Stage Update Apf Initiation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_APF_Inventory_Approved</fullName>
        <field>APF_Status__c</field>
        <literalValue>APF Inventory Approved</literalValue>
        <name>Status - APF Inventory Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sub_Stage_APF_Inventory_Approval</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>APF Inventory Approval</literalValue>
        <name>Sub Stage - APF Inventory Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sub_Stage_Update_PDC</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Project Data Capture</literalValue>
        <name>Sub Stage Update PDC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_APF_Status</fullName>
        <field>APF_Status__c</field>
        <literalValue>APF Initiated</literalValue>
        <name>Update APF Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Exposure_Limits_Updated_Flag</fullName>
        <field>Exposure_Details_Modified__c</field>
        <literalValue>0</literalValue>
        <name>Update Exposure Limits Updated Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_APF_Initiation</fullName>
        <field>RecordTypeId</field>
        <lookupValue>APF_Initiation</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to APF Initiation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ZCM_Approval_Status_Approved</fullName>
        <field>ZCM_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>ZCM Approval Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ZCM_Approval_Status_In_Progress</fullName>
        <field>ZCM_Approval_Status__c</field>
        <literalValue>In Progress</literalValue>
        <name>ZCM Approval Status In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ZCM_Approval_Status_Rejected</fullName>
        <field>ZCM_Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>ZCM Approval Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
