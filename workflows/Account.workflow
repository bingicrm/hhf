<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Combination_Populated</fullName>
        <field>Combination__c</field>
        <formula>Name+TEXT(Date_of_Birth__c)</formula>
        <name>Combination Populated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Customer_ID_on_Account</fullName>
        <field>Customer_ID__c</field>
        <formula>Customer_ID_Onboarding__c</formula>
        <name>Populate Customer ID on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Individual_Edit</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Account Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Corporate_Account_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Corporate_Edit</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Corporate Account Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Corporate Account - Update Record Type</fullName>
        <actions>
            <name>Update_Corporate_Account_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.IsPersonAccount</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Builder - Corporate,Builder - Individual</value>
        </criteriaItems>
        <description>Last Modified by Shobhit Saxena to exempt the actions if the Account Record Type is Builder Individual, Builder Corporate (APF)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Person Account - Update Record Type</fullName>
        <actions>
            <name>Update_Account_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.IsPersonAccount</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Populate Customer ID on Account</fullName>
        <actions>
            <name>Populate_Customer_ID_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Customer_ID_Onboarding__c</field>
            <operation>notEqual</operation>
            <value>NULL</value>
        </criteriaItems>
        <description>This WF Rule will populate the Customer Id on Account.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Combination on Account</fullName>
        <actions>
            <name>Combination_Populated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISBLANK(Date_of_Birth__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
