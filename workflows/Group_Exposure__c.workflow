<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Amt_Changed_False</fullName>
        <field>Amount_Changed__c</field>
        <literalValue>0</literalValue>
        <name>Amt Changed False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>LoanApplication__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Make Amt Changed False</fullName>
        <actions>
            <name>Amt_Changed_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
