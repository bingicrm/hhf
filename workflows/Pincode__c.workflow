<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_City</fullName>
        <field>City__c</field>
        <formula>City_LP__r.Name</formula>
        <name>Update City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Country</fullName>
        <field>Country_Text__c</field>
        <formula>TEXT( City_LP__r.Country__c )</formula>
        <name>Update Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_State</fullName>
        <field>State_Text__c</field>
        <formula>TEXT( City_LP__r.State__c )</formula>
        <name>Update State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update City%2C State and Country</fullName>
        <actions>
            <name>Update_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_State</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISNEW() || ISCHANGED( City_LP__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
