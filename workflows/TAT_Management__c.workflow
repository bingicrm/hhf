<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>TAT_end_time</fullName>
        <field>End_TAT_calculation__c</field>
        <formula>ROUND(24*(
(5*FLOOR((DATEVALUE(End_Time__c)-DATE(1996,1,1))/7)+
MIN(5,
MOD(DATEVALUE(End_Time__c)-DATE(1996,1,1),7)+
MIN(1,MOD(End_Time__c-DATETIMEVALUE(St__c),1))
)
)
-
(5*FLOOR((DATEVALUE(Start_Time__c)-DATE(1996,1,1))/7)+
MIN(5,
MOD(DATEVALUE(Start_Time__c)-DATE(1996,1,1),7)+
MIN(1,MOD(Start_Time__c-DATETIMEVALUE(St__c),1))
)
)
),
0)</formula>
        <name>TAT end time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TAT end time</fullName>
        <actions>
            <name>TAT_end_time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TAT_Management__c.End_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
