<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Duplicate_Document_Field_Data</fullName>
        <field>Duplicate_Document__c</field>
        <formula>Project__c &amp; Project_Builder__c &amp;  Document_Master__c &amp;  Document_Type__c &amp;  Document_Type__r.Name</formula>
        <name>Populate Duplicate Document Field Data</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Duplicate Document Field</fullName>
        <actions>
            <name>Populate_Duplicate_Document_Field_Data</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(     NOT(ISBLANK(Project_Builder__c)), 				Document_Type__r.Name  = &apos;Approved APF Letter&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
