<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_on_Loan_Disbursal</fullName>
        <description>Email Alert on Loan Disbursal</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Loan_Disbursed_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_on_movement_to_Re_credit</fullName>
        <description>Email Alert on movement to Re credit</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Customer_Negotiation_to_Recredit_movement</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_Customer_on_Customer_Onboarding</fullName>
        <description>Email Alert to Customer on Customer Onboarding</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Customer_Onboarding_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Insurance_Loan_has_been_created</fullName>
        <description>Insurance Loan has been created</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Insurance_Loan_Notification</template>
    </alerts>
    <alerts>
        <fullName>Loan_Application_Approved</fullName>
        <description>Loan Application Approved</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Credit_Review__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Credit_Manager_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Loan_application_approved</template>
    </alerts>
    <alerts>
        <fullName>Loan_Application_Customer_Acceptance_process</fullName>
        <description>Loan Application - Customer Acceptance process</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Sales_User__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Credit_Manager_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Positive_Status_Notification</template>
    </alerts>
    <alerts>
        <fullName>Loan_Application_Rejected</fullName>
        <description>Loan Application Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>Credit_Manager_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Loan_request_has_been_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Mail_on_Document_Approval</fullName>
        <description>Mail on Document Approval</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Document_Approval_Template_Customer</template>
    </alerts>
    <alerts>
        <fullName>email_on_approval</fullName>
        <description>email on approval</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Sales_User__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Current_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Credit_Manager_Approval_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>APproval_Initiated</fullName>
        <field>Approval_requested__c</field>
        <literalValue>1</literalValue>
        <name>APproval Initiated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Amount_Changed_c</fullName>
        <field>Amount_Changed__c</field>
        <literalValue>1</literalValue>
        <name>Amount_Changed__c</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_requested_false</fullName>
        <field>Approval_requested__c</field>
        <literalValue>0</literalValue>
        <name>Approval requested false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_required_negative</fullName>
        <field>Cus_neg_approval_required__c</field>
        <literalValue>0</literalValue>
        <name>Approval required negative</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_required_negative_ZCM</fullName>
        <field>Approval_Required_for_OTC__c</field>
        <literalValue>0</literalValue>
        <name>Approval required negativeZCM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_Processing_Fee</fullName>
        <field>Processing_Fee_Percentage__c</field>
        <formula>IF( Processing_Fee_Percentage__c &gt; Requested_Processing_Fee_Percent__c , Processing_Fee_Percentage__c, Requested_Processing_Fee_Percent__c)</formula>
        <name>Approved Processing Fee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_crossell</fullName>
        <field>Approved_cross_sell_amount__c</field>
        <formula>Requested_Cross_Sell_Amount__c</formula>
        <name>Approved crossell</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BHApprovalRequiredToFalse</fullName>
        <field>BH_Approval_Required__c</field>
        <literalValue>0</literalValue>
        <name>BHApprovalRequiredToFalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BHApprovalRequiredToTrue</fullName>
        <field>BH_Approval_Required__c</field>
        <literalValue>1</literalValue>
        <name>BHApprovalRequiredToTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cancelled_Rejected_Staus</fullName>
        <field>Rejected_Cancelled_Status__c</field>
        <name>Cancelled Rejected Staus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Requested_Loan_Amount_into_Approved</fullName>
        <field>Approved_Loan_Amount__c</field>
        <formula>Requested_Amount__c</formula>
        <name>Copy Requested Loan Amount into Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Requested_Loan_Tenure_into_Approved</fullName>
        <field>Approved_Loan_Tenure__c</field>
        <formula>Requested_Loan_Tenure__c</formula>
        <name>Copy Requested Loan Tenure into Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Credit_Approval_required</fullName>
        <field>Credit_Approval_required__c</field>
        <literalValue>1</literalValue>
        <name>Credit Approval required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Credit_Approval_required_to_False</fullName>
        <field>Credit_Approval_required__c</field>
        <literalValue>0</literalValue>
        <name>Credit Approval required to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Credit_Reject_True</fullName>
        <field>Credit_Rejected__c</field>
        <literalValue>1</literalValue>
        <name>Credit Reject True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cus_neg_field_update</fullName>
        <field>Cus_neg_approval_required__c</field>
        <literalValue>1</literalValue>
        <name>Cus neg field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Of_Cancellation</fullName>
        <field>Date_of_Cancellation__c</field>
        <formula>NOW()</formula>
        <name>Date Of Cancellation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FCU_Head_Decline_At_OTC_Receiving_True</fullName>
        <field>FCU_Head_Decline_At_OTC_Receiving__c</field>
        <literalValue>1</literalValue>
        <name>FCU Head Decline At OTC Receiving True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FCU_Revive_to_False</fullName>
        <field>FCU_Revive__c</field>
        <literalValue>0</literalValue>
        <name>FCU Revive to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FTNR_Check_To_True</fullName>
        <field>FTNR_Check__c</field>
        <literalValue>1</literalValue>
        <name>FTNR Check To True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FTNR_at_Docket_To_True</fullName>
        <field>FTNR_at_Docket__c</field>
        <literalValue>1</literalValue>
        <name>FTNR at Docket To True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fill_Date_of_Rejection</fullName>
        <field>Date_of_Rejection__c</field>
        <formula>NOW()</formula>
        <name>Fill Date of Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fill_Rejected_Cancelled_Stage</fullName>
        <field>Rejected_Cancelled_Stage__c</field>
        <formula>&apos;FCU Review&apos;</formula>
        <name>Fill Rejected Cancelled Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fill_Soft_Approved_in_TPV</fullName>
        <field>Third_Party_Approval_Status__c</field>
        <name>Fill Soft Approved in TPV</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fixed_For_Update</fullName>
        <field>Fixed_For_Months__c</field>
        <formula>Scheme__r.Default_Fixed_For_Months__c</formula>
        <name>Fixed For Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Full_to_Partial_Disbursed</fullName>
        <field>Disbursement_Status__c</field>
        <literalValue>Partially Disbursed</literalValue>
        <name>Full to Partial Disbursed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GE_Amount_update</fullName>
        <field>Group_Exposure_Amount__c</field>
        <formula>Group_Exposure_Amount__c + ( Approved_cross_sell_amount__c + Approved_Loan_Amount__c - Approved_Loan_Amount_at_GE__c )</formula>
        <name>GE Amount update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Generated_Repayment</fullName>
        <field>Repayment_Generated_for_Documents__c</field>
        <literalValue>0</literalValue>
        <name>Generated Repayment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IMD_approval_done</fullName>
        <field>IMD_approval_required__c</field>
        <literalValue>0</literalValue>
        <name>IMD approval done</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Is_Customer_Acceptance_set_to_True</fullName>
        <field>Is_Customer_Acceptance__c</field>
        <literalValue>1</literalValue>
        <name>Is Customer Acceptance set to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Is_Scan_Checked_set_to_True</fullName>
        <field>Is_Scan_Checked__c</field>
        <literalValue>1</literalValue>
        <name>Is Scan Checked set to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LA_Sub_Stage_to_Re_Look</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Re-Look</literalValue>
        <name>LA Sub Stage to Re Look</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Level_2_Approved</fullName>
        <field>Level_2_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Level 2 Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LoanApplicationLMSUnckeck</fullName>
        <field>LMSCheck__c</field>
        <literalValue>0</literalValue>
        <name>LoanApplicationLMSUnckeck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Login_date</fullName>
        <field>Login_Date__c</field>
        <formula>Business_Date_Modified__c</formula>
        <name>Login date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Overall_Post_Sanction_FCU_Status_to_ve</fullName>
        <field>Overall_Post_Sanction_FCU_Status__c</field>
        <literalValue>Negative</literalValue>
        <name>Overall Post Sanction FCU Status to -ve</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Override_Recall_To_True</fullName>
        <field>Override_Recall__c</field>
        <literalValue>1</literalValue>
        <name>Override Recall To True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PD_Check_to_False</fullName>
        <field>PD_Check__c</field>
        <literalValue>0</literalValue>
        <name>PD Check to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PD_Check_to_True</fullName>
        <field>PD_Check__c</field>
        <literalValue>1</literalValue>
        <name>PD Check to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Partial_to_Full_Disbursed</fullName>
        <field>Disbursement_Status__c</field>
        <literalValue>Fully Disbursed</literalValue>
        <name>Partial to Full Disbursed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PopulateDisbursementSystemDateTime</fullName>
        <field>Disbursement_System_DateTime__c</field>
        <formula>NOW()</formula>
        <name>PopulateDisbursementSystemDateTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PopulateLoginSystemDateTime</fullName>
        <field>Login_System_DateTime__c</field>
        <formula>NOW()</formula>
        <name>PopulateLoginSystemDateTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Inventory_Updation_Date</fullName>
        <field>APF_Inventory_Modification_Date__c</field>
        <formula>TODAY()</formula>
        <name>Populate Inventory Updation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Previous_Sub_Stage</fullName>
        <field>Previous_Sub_Stage__c</field>
        <formula>TEXT(PRIORVALUE(Sub_Stage__c))</formula>
        <name>Populate Previous Sub Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Profile</fullName>
        <field>Sales_Profile__c</field>
        <formula>CreatedBy.Profile.Name</formula>
        <name>Populate Profile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Processing_fee</fullName>
        <field>Approved_Processing_Fee__c</field>
        <formula>Requested_Processing_Fees1__c</formula>
        <name>Processing fee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ROI</fullName>
        <field>Approved_ROI__c</field>
        <formula>Approved_ROI__c</formula>
        <name>ROI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Re_Appealed_Case_to_False</fullName>
        <field>Re_Appealed_Case__c</field>
        <literalValue>0</literalValue>
        <name>Re-Appealed Case to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Cancelled_Rejected</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_Update_Disb_Checker</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Disbursement_Checker</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type Update Disb Checker</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_to_Custom_Negotiation</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Customer_Negotiations</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type to Custom Negotiation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_type_update_nego</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Customer_Negotiations</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record type update_nego</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reject_Revive_Reopen_Status</fullName>
        <field>Revive_Reopen_Status__c</field>
        <formula>&apos;Rejected&apos;</formula>
        <name>Reject Revive/Reopen Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_Cancelled_Status</fullName>
        <field>Rejected_Cancelled_Status__c</field>
        <formula>&quot;Cancelled&quot;</formula>
        <name>Rejected/Cancelled Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejection_Reason_FCU_Head_Decline</fullName>
        <field>Rejection_Reason__c</field>
        <literalValue>FCU Head Decline</literalValue>
        <name>Rejection Reason FCU Head Decline</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejection_Reason_Hunter_Head_Decline</fullName>
        <field>Rejection_Reason__c</field>
        <literalValue>Hunter Head Decline</literalValue>
        <name>Rejection Reason Hunter Head Decline</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remark</fullName>
        <field>Rejection_Reason__c</field>
        <literalValue>Bounced IMD</literalValue>
        <name>Remarks</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remarks</fullName>
        <field>Rejection_Reason__c</field>
        <literalValue>Criteria not met</literalValue>
        <name>Remarks</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reopen_Check</fullName>
        <field>Reopen_Application__c</field>
        <literalValue>0</literalValue>
        <name>Reopen Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reopen_Counter</fullName>
        <field>Reopen_Count__c</field>
        <formula>IF( Reopen_Application__c ,  Reopen_Count__c + 1, Reopen_Count__c)</formula>
        <name>Reopen Counter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Revive_Application</fullName>
        <field>Revive_Application__c</field>
        <literalValue>0</literalValue>
        <name>Revive Application</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Revive_Reopen_Status</fullName>
        <field>Revive_Reopen_Status__c</field>
        <name>Revive Reopen Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SUD_Cancellation_Count_Update</fullName>
        <field>SUD_Cancellation_Count__c</field>
        <formula>SUD_Cancellation_Count__c+1</formula>
        <name>SUD Cancellation Count Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetCustNegoApprovalCompletedToFalse</fullName>
        <field>Customer_Negotiation_Approval_Completed__c</field>
        <literalValue>0</literalValue>
        <name>SetCustNegoApprovalCompletedToFalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetCustNegoApprovalCompletedToTrue</fullName>
        <field>Customer_Negotiation_Approval_Completed__c</field>
        <literalValue>1</literalValue>
        <name>SetCustNegoApprovalCompletedToTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetFCUHeadApprovedToTrue</fullName>
        <field>FCU_Head_Approved__c</field>
        <literalValue>1</literalValue>
        <name>SetFCUHeadApprovedToTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Loan_Status_to_Active</fullName>
        <field>Loan_Status__c</field>
        <literalValue>Active</literalValue>
        <name>Set Loan Status to Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lock_Check_to_False</fullName>
        <field>Lock_Check__c</field>
        <literalValue>0</literalValue>
        <name>Set Lock Check to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lock_Check_to_True</fullName>
        <field>Lock_Check__c</field>
        <literalValue>1</literalValue>
        <name>Set Lock Check to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Severity_Level_to_1</fullName>
        <field>Severity_Level__c</field>
        <literalValue>1</literalValue>
        <name>Severity Level to 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Change_To_Loan_Reject</fullName>
        <field>StageName__c</field>
        <literalValue>Loan Rejected</literalValue>
        <name>Stage Change To Loan Reject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Update_Disb_Checker</fullName>
        <field>StageName__c</field>
        <literalValue>Loan Disbursal</literalValue>
        <name>Stage_Update_Disb_Checker</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_to_Credit_Decisioning</fullName>
        <field>StageName__c</field>
        <literalValue>Credit Decisioning</literalValue>
        <name>Stage to Credit Decisioning</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_to_Loan_Disbursal</fullName>
        <field>StageName__c</field>
        <literalValue>Loan Disbursal</literalValue>
        <name>Stage to Loan Disbursal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_to_Loan_Rejected</fullName>
        <field>StageName__c</field>
        <literalValue>Loan Rejected</literalValue>
        <name>Stage to Loan Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_updation</fullName>
        <field>StageName__c</field>
        <literalValue>Customer Onboarding</literalValue>
        <name>Stage updation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_updation1</fullName>
        <field>StageName__c</field>
        <literalValue>Loan Rejected</literalValue>
        <name>Stage updation1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_updation_acceptance</fullName>
        <field>StageName__c</field>
        <literalValue>Customer Acceptance</literalValue>
        <name>Stage updation_acceptance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_updation_nego</fullName>
        <field>StageName__c</field>
        <literalValue>Customer Acceptance</literalValue>
        <name>Stage updation_nego</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_is_ReCredit</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Re Credit</literalValue>
        <name>Status is ReCredit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SubStage_Updation</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Application Initiation</literalValue>
        <name>SubStage Updation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SubStage_Updation_nego</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Customer Negotiation</literalValue>
        <name>SubStage Updation_nego</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SubStg_Update_Disb_Checker</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Disbursement Checker</literalValue>
        <name>SubStg_Update_Disb_Checker</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sub_Stage_Change_to_LoanReject</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Loan reject</literalValue>
        <name>Sub Stage Change to LoanReject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sub_Stage_to_Disbursement_Maker</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Disbursement Maker</literalValue>
        <name>Sub Stage to Disbursement Maker</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sub_Stage_to_Loan_Reject</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Loan reject</literalValue>
        <name>Sub Stage to Loan Reject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sub_Stage_to_Negotiation</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Customer Negotiation</literalValue>
        <name>Sub Stage to Negotiation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sub_Stage_to_OTC_Receiving</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>OTC Receiving</literalValue>
        <name>Sub Stage to OTC Receiving</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sub_Stage_to_Re_Credit</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Re Credit</literalValue>
        <name>Sub Stage to Re Credit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sub_stage_updation_to_negotiation</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Customer Negotiation</literalValue>
        <name>Sub stage updation to negotiation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Substage_rejected</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Loan reject</literalValue>
        <name>Substage_rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SubtractApprovedCrossSellFrmApprovedLoan</fullName>
        <field>Approved_Loan_Amount__c</field>
        <formula>Approved_Loan_Amount__c - Approved_cross_sell_amount__c</formula>
        <name>SubtractApprovedCrossSellFrmApprovedLoan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>True</fullName>
        <field>Repayment_Generated_for_Documents__c</field>
        <literalValue>1</literalValue>
        <name>True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Amount</fullName>
        <field>Approved_Loan_Amount_Exclusive__c</field>
        <formula>Approved_Loan_Amount__c</formula>
        <name>Update Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Application_Bureau_Status_Login</fullName>
        <field>Application_Bureau_Status_at_Login__c</field>
        <formula>TEXT(Application_Bureau_Status__c)</formula>
        <name>Update Application Bureau Status Login</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_to_Initiated</fullName>
        <field>BRE_Approval_Status__c</field>
        <literalValue>Initiated</literalValue>
        <name>Update Approval Status to Initiated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approved_Loan_Amount_on_GE</fullName>
        <field>Approved_Loan_Amount_at_GE__c</field>
        <formula>Approved_cross_sell_amount__c  +  Approved_Loan_Amount__c</formula>
        <name>Update Approved Loan Amount on GE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_BRE_Approval_Status_to_Rejected</fullName>
        <field>BRE_Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update BRE Approval Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_BRE_Approval_Status_to_Unapproved</fullName>
        <field>BRE_Approval_Status__c</field>
        <literalValue>Unapproved</literalValue>
        <name>Update BRE Approval Status to Unapproved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_BRE_Approval_Status_to_approved</fullName>
        <field>BRE_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update BRE Approval Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_BRE_Approval_status_L1_approved</fullName>
        <field>BRE_Approval_Status__c</field>
        <literalValue>L1 Approved</literalValue>
        <name>Update BRE Approval status- L1 approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_BRE_Record</fullName>
        <field>BRE_Record__c</field>
        <literalValue>0</literalValue>
        <name>Update BRE Record</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Cancelled_Rejected_Stage</fullName>
        <field>Rejected_Cancelled_Stage__c</field>
        <formula>&apos;Customer Onboarding&apos;</formula>
        <name>Update Cancelled/Rejected Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Cancelled_Rejected_Substage</fullName>
        <field>Rejected_Cancelled_Sub_Stage__c</field>
        <formula>&apos;Application Initiation&apos;</formula>
        <name>Update Cancelled/Rejected Substage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Checkbox</fullName>
        <field>Amount_Changed__c</field>
        <literalValue>0</literalValue>
        <name>Update Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_DM_Flag</fullName>
        <field>DMDownloaded__c</field>
        <literalValue>0</literalValue>
        <name>Update DM Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_of_Rejection</fullName>
        <field>Date_of_Rejection__c</field>
        <formula>TODAY()</formula>
        <name>Update Date of Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Final_Approved</fullName>
        <field>Third_Party_Approval_Status__c</field>
        <literalValue>Final Approved</literalValue>
        <name>Update Final Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Fixed_Floating_For</fullName>
        <field>Fixed_For_Months__c</field>
        <name>Update Fixed Floating For</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_IPT_at_Login</fullName>
        <field>Income_Program_Type_at_Login__c</field>
        <formula>TEXT(Income_Program_Type__c)</formula>
        <name>Update IPT at Login</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Insurance_Loan_Created</fullName>
        <field>Insurance_Loan_Created__c</field>
        <literalValue>0</literalValue>
        <name>Update Insurance Loan Created</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Insurance_Loan_Number</fullName>
        <field>Insurance_Loan_Number__c</field>
        <name>Update Insurance Loan Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_LA_RecordType_to_App_Initiation</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Application_Initiation</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update LA RecordType to App Initiation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_LA_RecordType_to_Read_only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>App_Initiation_BRE_approval</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update LA RecordType to Read only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_LA_Stage</fullName>
        <field>StageName__c</field>
        <literalValue>Loan Cancelled</literalValue>
        <name>Update LA Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Loan_Engine_2_Output</fullName>
        <field>Loan_Engine_2_Output__c</field>
        <literalValue>Non STP</literalValue>
        <name>Update Loan Engine 2 Output</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Overall_Bureau_Status_at_Login</fullName>
        <field>Overall_Application_status_at_Login__c</field>
        <formula>TEXT(Overall_Application_status__c )</formula>
        <name>Update Overall Bureau Status at Login</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Overall_application_status</fullName>
        <field>Overall_Application_status__c</field>
        <literalValue>Amber</literalValue>
        <name>Update Overall application status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Property_Identified_to_False</fullName>
        <field>Property_Identified__c</field>
        <literalValue>0</literalValue>
        <name>Update Property Identified to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Property_Identified_to_True</fullName>
        <field>Property_Identified__c</field>
        <literalValue>1</literalValue>
        <name>Update Property Identified to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Property_Indentified_Date</fullName>
        <field>Property_Identification_Date__c</field>
        <formula>Now()</formula>
        <name>Update Property Indentified Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Rejected_Cancelled_Status</fullName>
        <field>Rejected_Cancelled_Status__c</field>
        <formula>&quot;Rejected&quot;</formula>
        <name>Update Rejected Cancelled Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Repayment_Check_False</fullName>
        <field>Repayment_schedule_at_Disbursement_Check__c</field>
        <literalValue>0</literalValue>
        <name>Update Repayment Check False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Requested_Amount_at_Login</fullName>
        <field>Requested_Loan_Amount_on_Login__c</field>
        <formula>Requested_Amount__c</formula>
        <name>Update Requested Amount at Login</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Revive_Reopen_Status</fullName>
        <field>Revive_Reopen_Status__c</field>
        <formula>&apos;Approved&apos;</formula>
        <name>Update Revive/Reopen Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sanction_Date</fullName>
        <field>Sanction_Date__c</field>
        <formula>Business_Date_Modified__c</formula>
        <name>Update Sanction Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Severity_Level</fullName>
        <field>Severity_Level__c</field>
        <literalValue>3</literalValue>
        <name>Update Severity Level</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Soft_Approved</fullName>
        <field>Third_Party_Approval_Status__c</field>
        <literalValue>Soft Approved</literalValue>
        <name>Update Soft Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Sales Approval</literalValue>
        <name>Update Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Loan_Reject</fullName>
        <field>StageName__c</field>
        <literalValue>Loan Rejected</literalValue>
        <name>Update Stage to Loan Reject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sub_Stage</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Loan Cancel</literalValue>
        <name>Update Sub Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sub_Stage_to_Credit_Authority_App</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Credit Authority Approval</literalValue>
        <name>Update Sub Stage to Credit Authority App</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Substage_to_CIBIL_reject</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>CIBIL Reject</literalValue>
        <name>Update Substage to CIBIL reject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_approved_ROI</fullName>
        <field>Approved_ROI__c</field>
        <formula>Requested_ROI__c</formula>
        <name>Update approved ROI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_cus_OK</fullName>
        <description>Update customer ok with the parameters as false</description>
        <field>Customer_OK_with_All__c</field>
        <literalValue>0</literalValue>
        <name>Update cus OK</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Custom_Negotiation</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Customer Negotiation</literalValue>
        <name>Update to Custom  Negotiation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>approval_required_false</fullName>
        <field>Cus_neg_approval_required__c</field>
        <literalValue>0</literalValue>
        <name>approval required false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>approved_Foreclosure</fullName>
        <field>Approved_Foreclosure_Charges__c</field>
        <formula>Requested_Foreclosure_Charges__c</formula>
        <name>approved Foreclosure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>approved_LA</fullName>
        <description>The approved loan amount should change to requested loan amount</description>
        <field>Approved_Loan_Amount__c</field>
        <formula>Requested_Amount__c</formula>
        <name>approved LA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>approved_loan_tenure</fullName>
        <field>Approved_Loan_Tenure__c</field>
        <formula>Requested_Loan_Tenure__c</formula>
        <name>approved loan tenure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>approved_processing_fees</fullName>
        <field>Processing_Fee_Percentage__c</field>
        <formula>Requested_Processing_Fee_Percent__c</formula>
        <name>approved processing fees</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>crosssell</fullName>
        <field>Approved_cross_sell_amount__c</field>
        <formula>Approved_cross_sell_amount__c</formula>
        <name>crosssell</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>cus_negotiation_approval</fullName>
        <field>Cus_neg_approval_required__c</field>
        <literalValue>0</literalValue>
        <name>cus negotiation approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>date_update</fullName>
        <field>FTNR_date__c</field>
        <formula>TODAY()</formula>
        <name>date update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ftnr_docket_date_update</fullName>
        <field>FTNR_date_at_Docket__c</field>
        <formula>TODAY()</formula>
        <name>ftnr docket date update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>loanappSanctionRequestedq</fullName>
        <field>sanction_waiver_requested__c</field>
        <literalValue>0</literalValue>
        <name>loanappSanctionRequestedq</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>processing_fee_percent</fullName>
        <field>Requested_Processing_Fee_Percent__c</field>
        <formula>IF( Processing_Fee_Percentage__c &gt; Requested_Processing_Fee_Percent__c , Processing_Fee_Percentage__c, Requested_Processing_Fee_Percent__c)</formula>
        <name>processing fee %</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>record_type_change</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Sales_Approval</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>record type change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>requested_ROI</fullName>
        <field>Requested_ROI__c</field>
        <formula>Approved_ROI__c</formula>
        <name>requested ROI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>requested_foreclosure</fullName>
        <field>Requested_Foreclosure_Charges__c</field>
        <formula>Approved_Foreclosure_Charges__c</formula>
        <name>requested foreclosure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>requested_loan_amount</fullName>
        <field>Requested_Amount__c</field>
        <formula>Approved_Loan_Amount__c</formula>
        <name>requested loan amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>requested_tenure</fullName>
        <field>Requested_Loan_Tenure__c</field>
        <formula>Approved_Loan_Tenure__c</formula>
        <name>requested tenure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>sales_approval_false</fullName>
        <field>Sales_Approval_required__c</field>
        <literalValue>0</literalValue>
        <name>sales approval false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>sales_approval_falsee</fullName>
        <field>Sales_Approval_required__c</field>
        <literalValue>0</literalValue>
        <name>sales approval falsee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>stage_to_loan_dispersal</fullName>
        <field>StageName__c</field>
        <literalValue>Loan Disbursal</literalValue>
        <name>stage to loan dispersal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>sub_stage</fullName>
        <field>Sub_Stage__c</field>
        <literalValue>Customer Negotiation</literalValue>
        <name>sub stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Insurance_Loan_Status</fullName>
        <field>Insurance_Loan_Status__c</field>
        <name>update Insurance Loan Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_OTC_waiver_approval</fullName>
        <field>OTC_waiver_approval_action__c</field>
        <literalValue>1</literalValue>
        <name>update OTC/waiver approval action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_OTC_waiver_approval_action</fullName>
        <field>OTC_waiver_approval_action__c</field>
        <literalValue>1</literalValue>
        <name>update OTC/waiver approval action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_waiver_approval_action</fullName>
        <field>waiver_approval_action__c</field>
        <literalValue>1</literalValue>
        <name>update waiver approval action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Application Initiation</fullName>
        <actions>
            <name>Fixed_For_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Stage_updation</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SubStage_Updation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK( TEXT(StageName__c) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BHApprovalTrueWhenCrossSellChangedAtSalesApproval</fullName>
        <actions>
            <name>BHApprovalRequiredToTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow sets BH_Approval_Required__c field to true if Approved_cross_sell_amount__c OR Approved_Foreclosure_Charges__c OR Approved_ROI__c is changed at Sales Approval sub stage. This is done because of TIL-1234.</description>
        <formula>AND(     ISPICKVAL(Sub_Stage__c,&quot;Sales Approval&quot;), 				OR( 				    ISCHANGED(Approved_cross_sell_amount__c), 								ISCHANGED(Approved_Foreclosure_Charges__c), 								ISCHANGED(Approved_ROI__c) 				) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Capture Previous Sub Stage</fullName>
        <actions>
            <name>Populate_Previous_Sub_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TEXT(Sub_Stage__c) &lt;&gt; TEXT(PRIORVALUE(Sub_Stage__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Capture Property Identified Date%2FTime</fullName>
        <actions>
            <name>Update_Property_Indentified_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Loan_Application__c.Property_Identified__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Capture Sales Profile</fullName>
        <actions>
            <name>Populate_Profile</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule will capture the profile of Assigned Sales User whenever it is changed</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Check Is Customer Acceptance for first time</fullName>
        <actions>
            <name>Is_Customer_Acceptance_set_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Application__c.StageName__c</field>
            <operation>equals</operation>
            <value>Customer Acceptance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Application__c.Is_Customer_Acceptance__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Check Is Scan Checked for first time</fullName>
        <actions>
            <name>Is_Scan_Checked_set_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Login_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PopulateLoginSystemDateTime</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Application_Bureau_Status_Login</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_IPT_at_Login</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Overall_Bureau_Status_at_Login</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Requested_Amount_at_Login</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Loan_Application__c.Sub_Stage__c</field>
            <operation>equals</operation>
            <value>Scan: Data Maker</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Application__c.Is_Scan_Checked__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Application__c.Login_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Application__c.Login_System_DateTime__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Check for COPS Data Maker Status</fullName>
        <active>false</active>
        <formula>PRIORVALUE(Sub_Stage__c) == &apos;Document Collection&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Copy Requested Loan Amount and Tenure into Approved</fullName>
        <actions>
            <name>Copy_Requested_Loan_Amount_into_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_Requested_Loan_Tenure_into_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Application__c.Sub_Stage__c</field>
            <operation>equals</operation>
            <value>Document Approval</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Customer Negotiation approvals</fullName>
        <actions>
            <name>Cus_neg_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_cus_OK</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>or(  Requested_Amount__c &gt; Approved_Loan_Amount__c ,  Requested_Loan_Tenure__c &gt; Approved_Loan_Tenure__c ,  Approved_ROI__c &lt;&gt; Requested_ROI__c ,  Approved_Foreclosure_Charges__c &gt; Requested_Foreclosure_Charges__c ,   Processing_Fee_Percentage__c  &gt;  Requested_Processing_Fee_Percent__c  ,  Document_marked_OTC__c &gt; 0 ,  Document_Waiver__c &gt; 0 , sanction_waiver_requested__c = true  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Customer OK</fullName>
        <actions>
            <name>approval_required_false</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>processing_fee_percent</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>requested_ROI</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>requested_foreclosure</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>requested_loan_amount</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>requested_tenure</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Application__c.Customer_OK_with_All__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Application__c.Sub_Stage__c</field>
            <operation>equals</operation>
            <value>Customer Negotiation,Sales Approval</value>
        </criteriaItems>
        <description>If the customer is ok with all the approving parameters, then copy approved value to the requested value</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Document Approval Mail</fullName>
        <actions>
            <name>Mail_on_Document_Approval</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(  NOT(ISPICKVAL(PRIORVALUE(Sub_Stage__c),&apos;Document Approval&apos;)),  ISPICKVAL((Sub_Stage__c),&apos;Document Approval&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Download Disbursal Memo Flag false at DC to DM</fullName>
        <actions>
            <name>Update_DM_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(AND(TEXT(PRIORVALUE(Sub_Stage__c)) = &apos;Disbursement Checker&apos;, ISPICKVAL(Sub_Stage__c, &apos;Disbursement Maker&apos;)), AND(TEXT(PRIORVALUE(Sub_Stage__c)) = &apos;Tranche Disbursement Checker&apos;, ISPICKVAL(Sub_Stage__c, &apos;Tranche Disbursement Maker&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email alert on movement to Re Credit</fullName>
        <actions>
            <name>Email_Alert_on_movement_to_Re_credit</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(  ISPICKVAL(PRIORVALUE(Sub_Stage__c),&apos;Customer Negotiation&apos;),  ISPICKVAL((Sub_Stage__c),&apos;Re Credit&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email on Loan Disbursal</fullName>
        <actions>
            <name>Email_Alert_on_Loan_Disbursal</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(  NOT(ISPICKVAL(PRIORVALUE(Sub_Stage__c),&apos;Loan Disbursed&apos;)),  ISPICKVAL((Sub_Stage__c),&apos;Loan Disbursed&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email to Customer on Onboarding</fullName>
        <actions>
            <name>Email_Alert_to_Customer_on_Customer_Onboarding</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(   ISCHANGED( Sub_Stage__c ), ISPICKVAL( Sub_Stage__c , &quot;File Check&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FTNR Check Update LA</fullName>
        <actions>
            <name>FTNR_Check_To_True</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>date_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(   ISPICKVAL( PRIORVALUE( Sub_Stage__c ) , &quot;File Check&quot;),  ISPICKVAL( Sub_Stage__c , &quot;Application Initiation&quot;), ISBLANK( Login_Date__c )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FTNR at Docket Update at LA</fullName>
        <actions>
            <name>FTNR_at_Docket_To_True</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ftnr_docket_date_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  ISPICKVAL( PRIORVALUE( Sub_Stage__c ) , &quot;Docket Checker&quot;),  ISPICKVAL( Sub_Stage__c , &quot;Document Approval&quot;),  ISBLANK( First_Time_ScanMaker_System_DateTime__c )   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Final Approved to Soft Approved</fullName>
        <actions>
            <name>Update_Soft_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>IF( 
ISPICKVAL(Sub_Stage__c, &quot;Customer Negotiation&quot;), 
IF( 
Property_Identified__c=TRUE, 
IF( 
AND( 
Verification_Count_All__c != Verification_Count_Completed__c, 
Verification_Count__c &lt;&gt; 0, 
ISPICKVAL(Sub_Stage__c, &quot;Customer Negotiation&quot;) 
), 
TRUE, 
FALSE 
), 
TRUE 
), 
FALSE 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Generated Repayment Flag false</fullName>
        <actions>
            <name>Generated_Repayment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Application__c.Sub_Stage__c</field>
            <operation>equals</operation>
            <value>Disbursement Maker</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Initiate Customer Acceptance</fullName>
        <actions>
            <name>Loan_Application_Customer_Acceptance_process</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND((Verification_Count_All__c = Verification_Count_Completed__c), Verification_Count_Completed__c &lt;&gt;0)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LoanApplicationLMSUnckeckRule</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Application__c.LMSCheck__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>LoanApplicationLMSUnckeck</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Loan_Application__c.Formula_for_Time_Based_Workflow_POC__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Move to previous on Ins Loan creation</fullName>
        <actions>
            <name>Update_Insurance_Loan_Created</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Insurance_Loan_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_Insurance_Loan_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Insurance_Loan_Created__c == True, OR(AND(ISPICKVAL(PRIORVALUE(Sub_Stage__c),&apos;Disbursement Maker&apos;),ISPICKVAL( (Sub_Stage__c) , &apos;Re Credit&apos;)), AND(ISPICKVAL(PRIORVALUE(Sub_Stage__c),&apos;Document Approval&apos;),ISPICKVAL( (Sub_Stage__c) , &apos;Customer Negotiation&apos;))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Parent loan about Insurance Loan</fullName>
        <actions>
            <name>Insurance_Loan_has_been_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Application__c.Insurance_Loan_Application__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notify Parent loan about Insurance Loan creation</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Populate APF Inventory Modification Date</fullName>
        <actions>
            <name>Populate_Inventory_Updation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule will populate the APF Inventory Modification Date once the Inventory gets updated on an APF Loan Applicaiton.</description>
        <formula>APF_Loan__c = true &amp;&amp; ISCHANGED(Project_Inventory__c) &amp;&amp; NOT(ISBLANK(PRIORVALUE(Project_Inventory__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Disbursement Date on DM to DC</fullName>
        <actions>
            <name>PopulateDisbursementSystemDateTime</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Loan_Status_to_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISBLANK(Loan_Application_Number__c)) &amp;&amp;  ISBLANK(PRIORVALUE(Loan_Application_Number__c)) &amp;&amp;
ISPICKVAL(Sub_Stage__c,&apos;Disbursement Checker&apos;) &amp;&amp;
ISPICKVAL(PRIORVALUE(Sub_Stage__c),&apos;Disbursement Checker&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Property Identified for APF Loans</fullName>
        <actions>
            <name>Update_Property_Identified_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 				  ISCHANGED(APF_Loan__c), 				  APF_Loan__c = true 				)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Property Identified for APF Loans - 2</fullName>
        <actions>
            <name>Update_Property_Identified_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 				  ISCHANGED(APF_Loan__c), 				  APF_Loan__c = false, 				  NOT(ISPICKVAL(Sub_Stage__c,&apos;COPS:Data Maker&apos;)), 				  NOT(ISPICKVAL(Sub_Stage__c,&apos;COPS: Credit Operations Entry&apos;)), 				  NOT(ISPICKVAL(Sub_Stage__c,&apos;Credit Review&apos;)), 				  NOT(ISPICKVAL(Sub_Stage__c,&apos;Credit:Review and Decision&apos;)), 				  NOT(ISPICKVAL(Sub_Stage__c,&apos;Re-Look&apos;)), 			  	NOT(ISPICKVAL(Sub_Stage__c,&apos;Re Credit&apos;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PopulateThirdPartyApprovalStatus</fullName>
        <actions>
            <name>Fill_Soft_Approved_in_TPV</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 
OR( 
ISPICKVAL(StageName__c,&quot;Credit Decisioning&quot;), 
ISPICKVAL(StageName__c,&quot;Operation Control&quot;), 
ISPICKVAL(StageName__c,&quot;Customer Onboarding&quot;) 
), 
NOT(ISPICKVAL(Third_Party_Approval_Status__c,&quot;&quot;)) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Repayment Flag false at DC to DM</fullName>
        <actions>
            <name>Update_Repayment_Check_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TEXT(PRIORVALUE(Sub_Stage__c)) = &apos;Disbursement Checker&apos; &amp;&amp;
ISPICKVAL(Sub_Stage__c, &apos;Disbursement Maker&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Soft Approved to Final Approved</fullName>
        <actions>
            <name>Update_Final_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(  				Verification_Count_All__c =  Verification_Count_Completed__c,  				Verification_Count__c &lt;&gt; 0, 				ISPICKVAL(Sub_Stage__c, &quot;Customer Negotiation&quot;), 				Property_Identified__c=True )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SubtractApprovedCrossSellOnMovementFromDisbMakerToReCredit</fullName>
        <actions>
            <name>SubtractApprovedCrossSellFrmApprovedLoan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule subtracts Approved_cross_sell_amount__c from Approved_Loan_Amount__c when an LA moves from Disbursement Maker to Re Credit and Insurance_Loan_Created__c is false.</description>
        <formula>AND(     ISPICKVAL(Sub_Stage__c,&quot;Re Credit&quot;), 				ISPICKVAL(PRIORVALUE(Sub_Stage__c),&quot;Disbursement Maker&quot;), 				Insurance_Loan_Created__c = FALSE, 				PRIORVALUE(Insurance_Loan_Created__c) = FALSE  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ThirdPartyApprovalStatusToBeBlankOnLoanCancelReject</fullName>
        <actions>
            <name>Fill_Soft_Approved_in_TPV</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR (2 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Loan_Application__c.Sub_Stage__c</field>
            <operation>equals</operation>
            <value>Loan reject</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Application__c.Sub_Stage__c</field>
            <operation>equals</operation>
            <value>Loan Cancel</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Application__c.Loan_Application_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>This rule sets Third Party Approval Status field in LA to blank when LA is cancelled and LAN No. is blank OR LA is rejected.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Approved Loan Amount Exclusive</fullName>
        <actions>
            <name>Update_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Loan_Application__c.Sub_Stage__c</field>
            <operation>equals</operation>
            <value>Document Approval</value>
        </criteriaItems>
        <description>Store Approved loan amount in a dummy field that will be use in movement of application from handsight to disbursement maker - Moved to loanApplicationTriggerHandler with comment containing workflow name</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Approved Loan Amount for GE</fullName>
        <actions>
            <name>Update_Approved_Loan_Amount_on_GE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>Amount_Changed__c  = true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Approved Processing Fee When Requested is Greater</fullName>
        <actions>
            <name>approved_processing_fees</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This workflow rule updates Approved Processing Fee Percentage (Processing_Fee_Percentage__c) with Requested Processing Fee Percent (Requested_Processing_Fee_Percent__c) when LA moves from Customer Negotiation to Document Approval.</description>
        <formula>AND(     Requested_Processing_Fee_Percent__c &gt; Processing_Fee_Percentage__c, 				ISPICKVAL(Sub_Stage__c,&quot;Document Approval&quot;), 				ISPICKVAL(PRIORVALUE(Sub_Stage__c),&quot;Customer Negotiation&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update BRE Check on Loan Application</fullName>
        <actions>
            <name>Update_BRE_Record</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( BRE_Record__c = TRUE ,ISCHANGED(BRE_Check__c) ,BRE_Check__c = TRUE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Disbursement Status</fullName>
        <actions>
            <name>Partial_to_Full_Disbursed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Partially Disbursed to Fully Disbursed when Pending amount gets 0</description>
        <formula>AND(  ISCHANGED(Pending_Amount_for_Disbursement__c ),  Pending_Amount_for_Disbursement__c = 0 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Disbursement Status Reverse</fullName>
        <actions>
            <name>Full_to_Partial_Disbursed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Fully Disbursed to Partially Disbursed when Pending amount gets non 0 from 0</description>
        <formula>AND(  ISCHANGED(Pending_Amount_for_Disbursement__c ),  PRIORVALUE(Pending_Amount_for_Disbursement__c) = 0,  Pending_Amount_for_Disbursement__c &lt;&gt; 0 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Floating Months on interest type update</fullName>
        <actions>
            <name>Update_Fixed_Floating_For</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  ISPICKVAL(StageName__c, &apos;Credit Decisioning&apos; ) , ISCHANGED( Interest_Type__c ), ISPICKVAL(Interest_Type__c, &apos;Fixed&apos; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Loan Engine 2 Output</fullName>
        <actions>
            <name>Update_Loan_Engine_2_Output</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR(ISPICKVAL( Sub_Stage__c , &apos;Credit Review&apos; ), ISPICKVAL( Sub_Stage__c , &apos;Re Credit&apos; ), ISPICKVAL( Sub_Stage__c , &apos;Re-credit&apos; )), ISPICKVAL(PRIORVALUE (Sub_Stage__c ),  &apos;COPS:Data Checker&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>update Approved Loan Amount at GE on change</fullName>
        <actions>
            <name>Amount_Changed_c</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>GE_Amount_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(OR( ISCHANGED( Approved_Loan_Amount__c ),  ISCHANGED( Approved_cross_sell_amount__c )),  ISPICKVAL(  StageName__c  , &apos;Credit Decisioning&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>xxyz</fullName>
        <actions>
            <name>True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Loan_Application__c.Name</field>
            <operation>equals</operation>
            <value>LA-00003138</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Application__c.Repayment_Generated_for_Documents__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
