<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>DSA_DST_name</fullName>
        <field>Sourcing_DSA_DST__c</field>
        <formula>if(
				!isBlank( DST__c ), 
				DST__r.FirstName +&apos; &apos;+ DST__r.LastName, 
				if(
								!isBlank( Channel_Partner_Name__c ), 
								Channel_Partner_Name__r.Name ,
								if(
												!isBlank( DSA_Name__c ),  
												DSA_Name__r.DSA_Name__c ,
												if(
												    !isBlank( Hero_Dealer_DSA__c ),
																Hero_Dealer_DSA__r.DSA_Name__c,
																&apos;&apos;
												)
								)
				)
)</formula>
        <name>DSA DST name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_RSM</fullName>
        <field>RSM__c</field>
        <formula>IF(NOT(ISBLANK(Sales_User_Name__c)), Sales_User_Name__r.Manager.FirstName + &apos; &apos;+Sales_User_Name__r.Manager.MiddleName + &apos; &apos;+ 
Sales_User_Name__r.Manager.LastName, Loan_Application__r.CreatedBy.Manager.Manager.FirstName +&apos; &apos;+Loan_Application__r.CreatedBy.Manager.Manager.MiddleName+ &apos; &apos;+Loan_Application__r.CreatedBy.Manager.Manager.LastName )</formula>
        <name>Populate RSM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SM_ID</fullName>
        <field>SM_CBM_ID__c</field>
        <formula>IF(NOT(ISBLANK(Sales_User_Name__c)), Sales_User_Name__r.Id, NULL)</formula>
        <name>SM ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_manager</fullName>
        <field>Sourcing_Sales_Manager__c</field>
        <formula>IF(NOT(ISBLANK(Sales_User_Name__c)), Sales_User_Name__r.FirstName + &apos; &apos;+Sales_User_Name__r.MiddleName + &apos; &apos;+ 
Sales_User_Name__r.LastName, Loan_Application__r.Sales_Manager_CreatedByManager__c )</formula>
        <name>Sales manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sourcing_Duplicate</fullName>
        <field>Duplicate_Sourcing__c</field>
        <formula>Loan_Application__c</formula>
        <name>Sourcing Duplicate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Channel_Partner</fullName>
        <field>Channel_Partner__c</field>
        <formula>if(!isBlank( DSA_Name__c), DSA_Name__r.Name, if(!isBlank( Connector_Name__c), Connector_Name__r.Name ,
if(!isBlank( Hero_Dealer__c), Hero_Dealer__r.Name ,if(!isBlank( Hero_Dealer_DSA__c), Hero_Dealer_DSA__r.Name ,if(!isBlank( Digital_Connector__c),Digital_Connector__r.Name,&apos;&apos;)))))</formula>
        <name>Update Channel Partner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Source_Code</fullName>
        <field>Source_Code__c</field>
        <formula>IF( NOT( ISBLANK( TEXT(Source_Code__c) ) ) , RecordType.Name  , &apos;&apos;)</formula>
        <name>Update Source Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Duplicate Detection for Sourcing Detail</fullName>
        <actions>
            <name>Sourcing_Duplicate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Channel Partner and Source Code on Loan Application</fullName>
        <actions>
            <name>Update_Channel_Partner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Source_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate manager %26 DST</fullName>
        <actions>
            <name>DSA_DST_name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_RSM</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SM_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sales_manager</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
