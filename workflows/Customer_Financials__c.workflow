<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Growth_1</fullName>
        <field>Growth_1__c</field>
        <formula>IF(Current_Year_1__c &lt;&gt; 0, ((Current_Year__c - Current_Year_1__c)/ Current_Year_1__c), 0)</formula>
        <name>Populate Growth 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Growth_2</fullName>
        <field>Growth_2__c</field>
        <formula>IF(Current_Year_2__c &lt;&gt; 0,((Current_Year_1__c - Current_Year_2__c)/ Current_Year_2__c ), 0)</formula>
        <name>Populate Growth 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Growth</fullName>
        <actions>
            <name>Populate_Growth_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Growth_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
