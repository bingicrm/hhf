<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Tech_Combination</fullName>
        <field>Combination__c</field>
        <formula>Scheme_Lookup__c + TEXT(Transaction_Type__c) + TEXT(Type_of_Property__c) + TEXT(Threshold_Amount__c) + TEXT(Evaluation_Count__c)</formula>
        <name>Update Tech Combination</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Technical Combination</fullName>
        <actions>
            <name>Update_Tech_Combination</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Scheme_Lookup__c &lt;&gt; NULL &amp;&amp; TEXT(Transaction_Type__c) &lt;&gt; NULL &amp;&amp; TEXT(Type_of_Property__c) &lt;&gt; NULL &amp;&amp; TEXT(Threshold_Amount__c) &lt;&gt; NULL &amp;&amp; TEXT(Evaluation_Count__c) &lt;&gt; NULL</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
