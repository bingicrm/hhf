<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Dedupe_Combination_Field</fullName>
        <field>Dedupe_Combination__c</field>
        <formula>Name_of_Promoter__c&amp;TEXT(DOB__c)&amp;Project_Builder__c&amp; Project_Builder__r.Project__c</formula>
        <name>Update Dedupe Combination Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Duplicate_Mobile_Number_Field</fullName>
        <field>Duplicate_Mobile_Number__c</field>
        <formula>Contact_No_Mobile__c&amp;Project_Builder__c&amp;Project_Builder__r.Project__c</formula>
        <name>Update Duplicate Mobile Number Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Duplicate_PAN_Number_Field</fullName>
        <field>Duplicate_PAN_Number__c</field>
        <formula>Project_Builder__c&amp;PAN_Number__c&amp;Project_Builder__r.Project__c</formula>
        <name>Update Duplicate PAN Number Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Deduplication of Promoters</fullName>
        <actions>
            <name>Update_Dedupe_Combination_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Duplicate_Mobile_Number_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Duplicate_PAN_Number_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project_Builder__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
