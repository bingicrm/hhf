<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Duplicate_Detector</fullName>
        <field>Duplicate_Detector__c</field>
        <formula>Loan_Application__c &amp;  Name_of_Bank__c &amp; Account_Number__c</formula>
        <name>Update Duplicate Detector</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Duplicate Detection for Bank Detail</fullName>
        <actions>
            <name>Update_Duplicate_Detector</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
