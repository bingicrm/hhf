<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Dedupe_Combination_Field</fullName>
        <field>Dedupe_Combination__c</field>
        <formula>Name_of_Promoter__c&amp; PAN_Number__c&amp;TEXT(DOB__c)&amp; Contact_No_Mobile__c</formula>
        <name>Update Dedupe Combination Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Deduplication of Promoter Detail</fullName>
        <actions>
            <name>Update_Dedupe_Combination_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Promoter_Detail__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
