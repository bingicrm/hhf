<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Mark_Expired_True</fullName>
        <field>Document_expired__c</field>
        <literalValue>1</literalValue>
        <name>Mark Expired True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OTC_checked</fullName>
        <field>OTC_PDD_check__c</field>
        <literalValue>1</literalValue>
        <name>OTC checked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Current_Status</fullName>
        <field>Status_at_OTC_Receiving__c</field>
        <formula>TEXT(Status__c)</formula>
        <name>Populate Current Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>lock_the_record</fullName>
        <field>Logged__c</field>
        <literalValue>1</literalValue>
        <name>lock the record</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Mark Expired</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Document_Checklist__c.Status__c</field>
            <operation>equals</operation>
            <value>Uploaded</value>
        </criteriaItems>
        <criteriaItems>
            <field>Document_Checklist__c.Expiration_Possible__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Mark_Expired_True</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>OTC check</fullName>
        <actions>
            <name>OTC_checked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Document_Checklist__c.Status__c</field>
            <operation>equals</operation>
            <value>PDD,OTC Requested,Lawyer OTC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Document_Checklist__c.REquest_Date_for_OTC__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Record Locked</fullName>
        <actions>
            <name>lock_the_record</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Document_Checklist__c.Status__c</field>
            <operation>equals</operation>
            <value>Waived Off</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status Update at CPC Scan Checker</fullName>
        <actions>
            <name>Populate_Current_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Scan Check is true for any non Uploaded document at CPC Scan Checker, update the value of field Status at OTC Receiving with the current Status</description>
        <formula>AND( 
ISPICKVAL(Loan_Applications__r.Sub_Stage__c, &apos;CPC Scan Checker&apos;), 
Scan_Check_Completed__c = TRUE, 
PRIORVALUE(Scan_Check_Completed__c) = FALSE, 
NOT(ISPICKVAL(Status__c, &apos;Uploaded&apos;)) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
