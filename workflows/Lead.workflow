<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Lead_Assignment_Email</fullName>
        <description>Lead Assignment Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_Owner_Change</template>
    </alerts>
    <fieldUpdates>
        <fullName>Capture_Lead_Verified_Date_Time</fullName>
        <field>Verified_Date__c</field>
        <formula>NOW()</formula>
        <name>Capture Lead Verified Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_sent_to_ATS_Update</fullName>
        <field>Lead_Send_to_ATS__c</field>
        <literalValue>0</literalValue>
        <name>Lead sent to ATS Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Owner</fullName>
        <field>OwnerId</field>
        <name>Update Lead Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_to_Cold</fullName>
        <field>Rating</field>
        <literalValue>Cold</literalValue>
        <name>Update Lead to Cold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_to_Hot</fullName>
        <field>Rating</field>
        <literalValue>Hot</literalValue>
        <name>Update Lead to Hot</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_to_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Update Lead to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_to_Warm</fullName>
        <field>Rating</field>
        <literalValue>Warm</literalValue>
        <name>Update Lead to Warm</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Nurturing</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Update Status to Nurturing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Verified_Lead_Update</fullName>
        <field>Verified_Lead__c</field>
        <literalValue>1</literalValue>
        <name>Verified Lead Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Capture Lead Verified Date%2FTime</fullName>
        <actions>
            <name>Capture_Lead_Verified_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Verified_Lead__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Interested Lead</fullName>
        <actions>
            <name>Update_Lead_to_Hot</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Digital Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Call_Status__c</field>
            <operation>equals</operation>
            <value>Interested</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Sent To ATS</fullName>
        <actions>
            <name>Lead_sent_to_ATS_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Lead_Send_to_ATS__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_ATS_Response__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead rejected based on Call status</fullName>
        <actions>
            <name>Update_Lead_to_Cold</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Lead_to_Rejected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Digital Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Call_Status__c</field>
            <operation>equals</operation>
            <value>Not Interested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Call_Status__c</field>
            <operation>equals</operation>
            <value>Invalid number</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Less Interested Lead</fullName>
        <actions>
            <name>Update_Lead_to_Warm</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Digital Direct</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Call_Status__c</field>
            <operation>equals</operation>
            <value>Call Later</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Call_Status__c</field>
            <operation>equals</operation>
            <value>Not Contactable</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SendEmailToLeadOwner</fullName>
        <actions>
            <name>Lead_Assignment_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>This workflow rule sends email to Lead Owner whenever a lead is created or its owner is changed.</description>
        <formula>OR(     AND( 				    ISNEW(), 								NOT(ISBLANK(OwnerId)) 				), 				AND( 				    ISCHANGED(OwnerId), 								NOT(ISBLANK(OwnerId)) 				) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Owner for Inbound</fullName>
        <actions>
            <name>Lead_Assignment_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Source_Detail__c,&apos;Inbound Call&apos;) &amp;&amp;  OwnerId &lt;&gt; LastModifiedById</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Status to Nurturing for Website</fullName>
        <actions>
            <name>Update_Status_to_Nurturing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Source_Detail__c</field>
            <operation>equals</operation>
            <value>Website</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Verified Lead Flag</fullName>
        <actions>
            <name>Verified_Lead_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL( Call_Status__c , &apos;Verified Lead&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
