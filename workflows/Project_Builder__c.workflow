<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Duplicate_Info_Field</fullName>
        <field>Duplicate_Information__c</field>
        <formula>Builder_Name__r.Name &amp; TEXT(Date_Of_Birth__c)&amp; Project__c</formula>
        <name>Populate Duplicate Info Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Duplicate_Mobile_Number_Field</fullName>
        <field>Duplicate_Mobile_Number__c</field>
        <formula>Project__c &amp; Contact_Number_Mobile__c</formula>
        <name>Populate Duplicate Mobile Number Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Duplicate_PAN_Number_Field</fullName>
        <field>Duplicate_PAN_Number__c</field>
        <formula>Project__c &amp;  PAN_Number__c</formula>
        <name>Populate Duplicate PAN Number Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Cibil_Verified_to_True</fullName>
        <field>CIBIL_Verified__c</field>
        <literalValue>1</literalValue>
        <name>Set Cibil Verified to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Deduplication of Project Builders</fullName>
        <actions>
            <name>Populate_Duplicate_Info_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Duplicate_Mobile_Number_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Duplicate_PAN_Number_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project_Builder__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
