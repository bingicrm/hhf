<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Status_Update</fullName>
        <description>Updating status as approved</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Repayment_Start_Date_on_LA</fullName>
        <field>Repayment_Start_Date__c</field>
        <formula>Repayment_Start_Date__c</formula>
        <name>Update Repayment Start Date on LA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Modified Repayment Start Date</fullName>
        <actions>
            <name>Update_Repayment_Start_Date_on_LA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  PRIORVALUE(Repayment_Start_Date__c ) &lt;&gt; Repayment_Start_Date__c, Repayment_Start_Date__c &lt;&gt;  Loan_Application__r.Repayment_Start_Date__c   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
