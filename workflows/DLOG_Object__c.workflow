<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>DLOG_Update_Log_Record_Type_Integration</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Integration_Log</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>DLOG: Update Log Record Type-Integration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DLOG_Update_Log_Record_Type_Transaction</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Transaction_Log</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>DLOG: Update Log Record Type-Transaction</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Update_Log_Record_Type_Error</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Error_Log</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>DLOG: Update Log Record Type - Error</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>DLOG%3A Update Log Record Type - Error</fullName>
        <actions>
            <name>Task_Update_Log_Record_Type_Error</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>DLOG_Object__c.Type__c</field>
            <operation>equals</operation>
            <value>Error</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>DLOG%3A Update Log Record Type - Integration</fullName>
        <actions>
            <name>DLOG_Update_Log_Record_Type_Integration</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>DLOG_Object__c.Type__c</field>
            <operation>equals</operation>
            <value>Integration</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>DLOG%3A Update Log Record Type - Transaction</fullName>
        <actions>
            <name>DLOG_Update_Log_Record_Type_Transaction</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>DLOG_Object__c.Type__c</field>
            <operation>equals</operation>
            <value>Transaction</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
