<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>references_entered</fullName>
        <field>References_entered__c</field>
        <literalValue>1</literalValue>
        <name>references entered</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Enter references</fullName>
        <actions>
            <name>references_entered</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
