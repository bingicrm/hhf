<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_To_IMGC_TPV_Owner</fullName>
        <description>Email To IMGC TPV Owner</description>
        <protected>false</protected>
        <recipients>
            <field>Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Assignment_of_TP_Verification</template>
    </alerts>
    <alerts>
        <fullName>Email_for_Customer_Acceptance</fullName>
        <description>Email for Customer Acceptance</description>
        <protected>false</protected>
        <recipients>
            <field>LA_Credit_Manager_User__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>LA_Sales_Manager_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Positive_Status_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_on_FI_Sensitive_Fields_Change</fullName>
        <description>Email on FI Sensitive Fields Change</description>
        <protected>false</protected>
        <recipients>
            <field>LA_Credit_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Reinitiation_of_FI</template>
    </alerts>
    <alerts>
        <fullName>Email_to_CreditSales_Manager_on_Negative_FCU_Verification</fullName>
        <description>Email to CreditSales Manager on Negative FCU Verification</description>
        <protected>false</protected>
        <recipients>
            <field>LA_Credit_Manager_User__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>LA_Sales_Manager_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/FCU_Verification_Negative_CreditSales</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Credit_Manager_on_Verification_Completion</fullName>
        <description>Email to Credit Manager on Verification Completion</description>
        <protected>false</protected>
        <recipients>
            <field>LA_Credit_Manager_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Verification_Completed</template>
    </alerts>
    <alerts>
        <fullName>Email_to_FCU_Manager_on_Negative_FCU_Verification</fullName>
        <description>Email to FCU Manager on Negative FCU Verification</description>
        <protected>false</protected>
        <recipients>
            <field>Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/FCU_Verification_Negative</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Sales_Manager_on_Verification_Completion</fullName>
        <description>Email to Sales Manager on Verification Completion</description>
        <protected>false</protected>
        <recipients>
            <field>LA_Sales_Manager_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Verification_Completed_Sales_Team</template>
    </alerts>
    <alerts>
        <fullName>Email_to_TPV_Owner</fullName>
        <description>Email to TPV Owner</description>
        <protected>false</protected>
        <recipients>
            <field>Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Assignment_of_TP_Verification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Check_Re_Initiated</fullName>
        <field>Re_Initiated__c</field>
        <literalValue>1</literalValue>
        <name>Check Re-Initiated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Dummy_False</fullName>
        <field>Dummy__c</field>
        <literalValue>0</literalValue>
        <name>Dummy False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IMGCPremiumFees_ToNull</fullName>
        <field>IMGC_Premium_Fees__c</field>
        <name>IMGCPremiumFees%ToNull</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IMGC_Decision_Pending</fullName>
        <field>IMGC_Decision__c</field>
        <literalValue>Decision Pending</literalValue>
        <name>IMGC Decision Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Initiate_Verification</fullName>
        <field>Status__c</field>
        <literalValue>Initiated</literalValue>
        <name>Initiate Verification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Latest_Record_to_False</fullName>
        <field>Latest_Record__c</field>
        <literalValue>0</literalValue>
        <name>Latest Record to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ReportStatusChangesByDocsToFalse</fullName>
        <field>Report_Status_Changed_by_Docs__c</field>
        <literalValue>0</literalValue>
        <name>ReportStatusChangesByDocsToFalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Expired</fullName>
        <field>Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Set Status to Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Completed</fullName>
        <field>Status__c</field>
        <literalValue>Completed</literalValue>
        <name>Status Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Revert</fullName>
        <field>Status__c</field>
        <literalValue>Assigned</literalValue>
        <name>Status Revert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update</fullName>
        <field>Status__c</field>
        <literalValue>Waived Off</literalValue>
        <name>Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Credit_Manager_Status</fullName>
        <field>Credit_Manager_Decision__c</field>
        <literalValue>Approve</literalValue>
        <name>Update Credit Manager Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Credit_Manager_Status_Rejected</fullName>
        <field>Credit_Manager_Decision__c</field>
        <literalValue>Reject</literalValue>
        <name>Update Credit Manager Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Email_Credit</fullName>
        <field>LA_Credit_Manager_Email__c</field>
        <formula>Loan_Application__r.Credit_Manager_User__r.Email</formula>
        <name>Update Email_Credit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_IMGC_Preminum_Fee</fullName>
        <field>IMGC_Premium_Fee__c</field>
        <name>Update IMGC Preminum Fee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Report_Submitted_Date</fullName>
        <field>Report_Received_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Report Submitted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Third_Party_Expiry</fullName>
        <field>Expiry_Date__c</field>
        <formula>Date_of_Report__c + 10</formula>
        <name>Update Third Party Expiry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Times_Re_Initiated</fullName>
        <field>Times_Re_Initiated__c</field>
        <formula>Times_Re_Initiated__c + 1</formula>
        <name>Update Times Re-Initiated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_Status_to_Expired</fullName>
        <field>Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Update the Status to Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>approval_required_true</fullName>
        <field>Approval_Required_for_OTC__c</field>
        <literalValue>1</literalValue>
        <name>approval required true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>CheckReinitiatedAndUpdateCounter</fullName>
        <actions>
            <name>Check_Re_Initiated</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Times_Re_Initiated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule checks Re-initiated field in TPV and increases value of Times Re-Initiated by 1 every time a TPV record is re-initiated.</description>
        <formula>AND(      				ISPICKVAL(Status__c,&quot;Initiated&quot;), 				OR( 				    ISPICKVAL(PRIORVALUE(Status__c),&quot;Completed&quot;), 								ISPICKVAL(PRIORVALUE(Status__c),&quot;Expired&quot;) 				)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IMGC Initiated</fullName>
        <actions>
            <name>IMGCPremiumFees_ToNull</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IMGC_Decision_Pending</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_IMGC_Preminum_Fee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Third_Party_Verification__c.Verification_Type__c</field>
            <operation>equals</operation>
            <value>IMGC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Third_Party_Verification__c.Status__c</field>
            <operation>equals</operation>
            <value>Initiated</value>
        </criteriaItems>
        <description>Set Status on LA as Decision Pending when IMGC verification is Initiated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IMGCTPVInitiated</fullName>
        <actions>
            <name>Email_To_IMGC_TPV_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(
    RecordType.Name = &apos;Others&apos;,
				ISPICKVAL(Verification_Type__c,&quot;IMGC&quot;),
				OR(  				
				    AND(ISPICKVAL(Status__c , &apos;Initiated&apos;),ISCHANGED( Owner__c )),  				
				    AND(ISPICKVAL(Status__c , &apos;Initiated&apos;),NOT(ISPICKVAL(PRIORVALUE(Status__c ),&apos;Initiated&apos;))), 				
				    AND(ISNEW(), ISPICKVAL(Status__c,&apos;Initiated&apos;)) 
    )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>NotifySalesCreditFCUManagersOfFDStatus</fullName>
        <actions>
            <name>Email_to_CreditSales_Manager_on_Negative_FCU_Verification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_to_FCU_Manager_on_Negative_FCU_Verification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Third_Party_Verification__c.Report_Status__c</field>
            <operation>equals</operation>
            <value>Negative</value>
        </criteriaItems>
        <criteriaItems>
            <field>Third_Party_Verification__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>FCU</value>
        </criteriaItems>
        <criteriaItems>
            <field>Third_Party_Verification__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Third_Party_Verification__c.Exempted_from_FCU_BRD__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This workflow rule sends an email to Sales Manager, Credit Manager and FCU Manager when Report_Status__c of a FCU TPV comes as Negative.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OTC doc marked - approval req</fullName>
        <actions>
            <name>approval_required_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>if the 3rd party is marked OTC then  approval is required from ZCM on theloan app</description>
        <formula>and(OTC_Report__c = true, priorvalue(OTC_Report__c )= OTC_Report__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Report Submitted Date</fullName>
        <actions>
            <name>Email_to_Credit_Manager_on_Verification_Completion</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_to_Sales_Manager_on_Verification_Completion</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Report_Submitted_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Status__c) &amp;&amp; ISPICKVAL(Status__c, &apos;Completed&apos;) &amp;&amp; TEXT(Report_Status__c) &lt;&gt; NULL</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ReportStatusByDocsToFalse</fullName>
        <actions>
            <name>ReportStatusChangesByDocsToFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule makes the Report_Status_Changed_by_Docs__c to false if it becomes true.</description>
        <formula>AND(     ISCHANGED(Report_Status__c), 				Report_Status_Changed_by_Docs__c = true )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TPV Status is Initiated</fullName>
        <actions>
            <name>Email_to_TPV_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule sends out an email alert to the third party verification owner for the loan application and updates the Initiated Date</description>
        <formula>AND(
    RecordType.Name &lt;&gt; &apos;Others&apos;,
				OR(  				
				    AND(ISPICKVAL(Status__c , &apos;Initiated&apos;),ISCHANGED( Owner__c )),  				
				    AND(ISPICKVAL(Status__c , &apos;Initiated&apos;),NOT(ISPICKVAL(PRIORVALUE(Status__c ),&apos;Initiated&apos;))), 				
				    AND(ISNEW(), ISPICKVAL(Status__c,&apos;Initiated&apos;)) 
    )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Third Party 30 days Expiration</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Third_Party_Verification__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Third_Party_Verification__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>FI,Vendor PD</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Application__c.Loan_Application_Number__c</field>
            <operation>equals</operation>
            <value>null</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Status_to_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Third_Party_Verification__c.Report_Received_Date__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Third Party 60 days expiration</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Third_Party_Verification__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Third_Party_Verification__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>FCU,LIP,Legal,Legal Vetting,Technical</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Application__c.Loan_Application_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Status_to_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Third_Party_Verification__c.Report_Received_Date__c</offsetFromField>
            <timeLength>60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Third Party 999 days Expiration</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Third_Party_Verification__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Others</value>
        </criteriaItems>
        <criteriaItems>
            <field>Third_Party_Verification__c.Verification_Type__c</field>
            <operation>equals</operation>
            <value>IMGC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Third_Party_Verification__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Application__c.Loan_Application_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Status_to_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Third_Party_Verification__c.Report_Received_Date__c</offsetFromField>
            <timeLength>999</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Expiry Date</fullName>
        <actions>
            <name>Update_Third_Party_Expiry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Third_Party_Verification__c.Date_of_Report__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update User Fields on TPV Creation</fullName>
        <actions>
            <name>Update_Email_Credit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>True</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
