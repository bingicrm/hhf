<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>LA_repayment_check</fullName>
        <description>repayment schedule at LD need to be check if the user has created a new record</description>
        <field>Repayment_at_LD__c</field>
        <literalValue>1</literalValue>
        <name>LA repayment check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Loan_Application__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Repayment schedule at LD</fullName>
        <actions>
            <name>LA_repayment_check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Application__c.StageName__c</field>
            <operation>equals</operation>
            <value>Loan Disbursal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Application__c.Sub_Stage__c</field>
            <operation>equals</operation>
            <value>Disbursement Maker</value>
        </criteriaItems>
        <description>repayment schedule needs to be calculated at loan disbursal</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
