<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Status_to_Initiated</fullName>
        <field>Status__c</field>
        <literalValue>Initiated</literalValue>
        <name>Set Status to Initiated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Amount</fullName>
        <field>Downsizing_Amount__c</field>
        <formula>Loan_Application__r.Pending_Amount_for_Disbursement__c</formula>
        <name>Update Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Partial to Full Downsize</fullName>
        <actions>
            <name>Update_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Downsizing Amount on Type Change</description>
        <formula>AND( ISPICKVAL(PRIORVALUE( Downsize_Type__c ), &apos;Partial Downsize&apos;), ISPICKVAL( Downsize_Type__c, &apos;Full Downsize&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reset Status on Approved Edited LD</fullName>
        <actions>
            <name>Set_Status_to_Initiated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( PRIORVALUE( Creation_Remark__c ) &lt;&gt; Creation_Remark__c , TEXT(PRIORVALUE(Downsize_Type__c) ) &lt;&gt; TEXT(Downsize_Type__c) , PRIORVALUE( Downsizing_Amount__c ) &lt;&gt; Downsizing_Amount__c  ), ISPICKVAL(Status__c, &apos;Approved&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
