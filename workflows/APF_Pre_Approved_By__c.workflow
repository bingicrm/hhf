<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Duplicate_Detector_field</fullName>
        <field>Duplicate_Pre_Approving_Bank__c</field>
        <formula>Project__c&amp; Pre_Approving_Bank__c</formula>
        <name>Update Duplicate Detector field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Deduplication of Approving Banks</fullName>
        <actions>
            <name>Update_Duplicate_Detector_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>APF_Pre_Approved_By__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
