<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Approved_By_Update</fullName>
        <field>Approved_By__c</field>
        <formula>$User.Full_Name__c</formula>
        <name>Approved By Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_Date_Update</fullName>
        <field>Approved_Date__c</field>
        <formula>NOW()</formula>
        <name>Approved Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck</fullName>
        <field>CAM_Check__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>locked</fullName>
        <field>Locked__c</field>
        <literalValue>1</literalValue>
        <name>locked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Lock and Approve sanction conditions</fullName>
        <actions>
            <name>Approved_By_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Approved_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>locked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Sanction_Condition__c.Status__c</field>
            <operation>equals</operation>
            <value>Verified</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck CAM Check</fullName>
        <actions>
            <name>Uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>CAM_Check__c = TRUE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>lock sanction conditions</fullName>
        <actions>
            <name>locked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Sanction_Condition__c.Status__c</field>
            <operation>equals</operation>
            <value>Waived Off,Verified</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
