<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_on_Match_Found_in_Certain_Data_Sources</fullName>
        <ccEmails>hhfl.ucic@herohfl.com</ccEmails>
        <description>Email Alert on Match Found in Certain Data Sources</description>
        <protected>false</protected>
        <recipients>
            <recipient>rajneesh.sharma@herohfl.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>CRO__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>NCM__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Policy_Head__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Dedupe_Result</template>
    </alerts>
    <fieldUpdates>
        <fullName>Expire_CIBIL_Check</fullName>
        <field>PAN_Expired__c</field>
        <literalValue>Yes</literalValue>
        <name>Expire CIBIL Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Latest_False</fullName>
        <field>Latest_Record__c</field>
        <literalValue>0</literalValue>
        <name>Latest False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PAN_Expired_Yes</fullName>
        <field>PAN_Expired__c</field>
        <literalValue>Yes</literalValue>
        <name>PAN Expired Yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_CIBIL_Last_Updated</fullName>
        <field>CIBIL_Last_Updated__c</field>
        <formula>NOW()</formula>
        <name>Populate CIBIL Last Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PAN_Date</fullName>
        <field>PAN_Last_Updated__c</field>
        <formula>NOW()</formula>
        <name>Update PAN Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Email on Match Found in Certain UCIC DB</fullName>
        <actions>
            <name>Email_Alert_on_Match_Found_in_Certain_Data_Sources</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Customer_Integration__c.Email_Required__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Expire CIBIL Check</fullName>
        <active>true</active>
        <formula>NOT(ISNULL(CIBIL_Last_Updated__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Expire_CIBIL_Check</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Customer_Integration__c.CIBIL_Last_Updated__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Expire PAN Check</fullName>
        <active>true</active>
        <formula>NOT(ISNULL(PAN_Last_Updated__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>PAN_Expired_Yes</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Customer_Integration__c.PAN_Last_Updated__c</offsetFromField>
            <timeLength>999</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update CIBIL Last Updated Date%2F Time</fullName>
        <actions>
            <name>Populate_CIBIL_Last_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(CIBIL_Score__c) &amp;&amp; CIBIL_Score__c &lt;&gt; NULL</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update PAN Updated Date%2F Time</fullName>
        <actions>
            <name>Update_PAN_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(PAN_Status__c) &amp;&amp; ISPICKVAL(PAN_Status__c, &apos;E&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
