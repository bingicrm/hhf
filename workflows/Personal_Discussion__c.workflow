<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Personal_Discussion_Email_to_Customer</fullName>
        <description>Personal Discussion Email to Customer</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Conduct_Personal_Discussion</template>
    </alerts>
    <rules>
        <fullName>Personal Discussion Alert</fullName>
        <actions>
            <name>Personal_Discussion_Email_to_Customer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Personal_Discussion__c.PD_Initiated_On__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Personal_Discussion__c.Send_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
