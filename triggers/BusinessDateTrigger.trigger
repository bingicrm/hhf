trigger BusinessDateTrigger on LMS_Date_Sync__c (after insert) {
    if(trigger.isAfter && trigger.isInsert){
        List<LMS_Date_Sync__c> dateList = new List<LMS_Date_Sync__c>();
        dateList = [Select Id, Current_Date__c from LMS_Date_Sync__c WHERE ID NOT IN: Trigger.new ORDER BY CreatedDate DESC limit 1];
        for(LMS_Date_Sync__c busDate: trigger.New){
            if(!dateList.isEmpty() && dateList[0].Current_Date__c != busDate.Current_Date__c){
                database.executebatch(new BatchUpdateMonthlyTarget(),200);
            }
            
             if(System.Label.LMS_Business_Date != null && System.Label.LMS_Business_Date == 'true') {
                /**** Added by Saumya For TIL TIL 1299 ********/
                List<LMS_Bus_Date__c> objLMSBusDate =[SELECT id, Current_Date__c FROM LMS_Bus_Date__c limit 1];
                if(objLMSBusDate.size() > 0){
                System.debug('busDate.Current_Date__c'+ busDate.Current_Date__c);    
                objLMSBusDate[0].Current_Date__c = busDate.Current_Date__c.addDays(1);
                update objLMSBusDate[0];
                }
                else{
                LMS_Bus_Date__c objLMS= new LMS_Bus_Date__c(); 
                objLMS.Name ='Current Business Date';
                objLMS.Current_Date__c = busDate.Current_Date__c.addDays(1);
                insert objLMS;
               }
                /**** Added by Saumya For TIL TIL 1299 ********/
              }
        }
        BusinessDateTriggerHandler.processAccountDatachievement();
        // Below block checks M-2 on Achievements
        /*List<Achievement__c> achList = new List<Achievement__c>();
        List<Achievement__c> achToUpdate = new List<Achievement__c>();
        achList = [Select Id, M_2__c, Business_Date__c, Month_Value__c, Month_Difference__c from Achievement__c where Month_Difference__c = 2 OR Month_Difference__c = -2];
        for(Achievement__c ach : achList){
            ach.M_2__c = TRUE;
            achToUpdate.add(ach);
        }
        system.debug('--M-2 SIZE--:'+achToUpdate.size());
        if(achToUpdate.size()>0){
            database.update(achToUpdate);
        }*/
        if(System.Label.ActivityTriggeronBusinessDate != null && System.Label.ActivityTriggeronBusinessDate == 'true') {
        BusinessDateTriggerHandler.checkM2();
        }
        // Below block checks and unchecks FTD on achievements
        /*LMS_Date_Sync__c bDate = trigger.new[0];
        List<Achievement__c> achFTDList = new List<Achievement__c>();
        List<Achievement__c> achFTDUpdate = new List<Achievement__c>();
        achFTDList = [Select Id, M_2__c, FTD__c, Business_Date__c, Month_Value__c, Month_Difference__c from Achievement__c where Month_Difference__c = 0];
        for(Achievement__c ach : achFTDList){
            if(bDate.Current_Date__c.addDays(1) == ach.Business_Date__c){
                ach.FTD__c = TRUE;
                ach.M_2__c = FALSE;
                achFTDUpdate.add(ach);
            }
            else if(bDate.Current_Date__c.addDays(1) != ach.Business_Date__c && ach.FTD__c == TRUE){
                ach.FTD__c = FALSE;
                achFTDUpdate.add(ach);
            }
        }
        system.debug('--FTD SIZE--:'+achFTDUpdate.size());
        if(achFTDUpdate.size()>0){
            database.update(achFTDUpdate);
        }*/
        if(System.Label.ActivityTriggeronBusinessDate != null && System.Label.ActivityTriggeronBusinessDate == 'true') {
        BusinessDateTriggerHandler.checkFTD(trigger.new);
        }
        //Below block runs a dummy update on previous month's achievements to run their WF rules to evaluate LMTD
        /*List<Achievement__c> achLMTDList = new List<Achievement__c>();
        achLMTDList = [Select Id, Month_Difference__c from Achievement__c where Month_Difference__c = 1 OR Month_Difference__c = -1];
        system.debug('--M-1 SIZE to Trigger Workflow for LMTD Days Gap--:'+achLMTDList.size());
        if(achLMTDList.size()>0){
            database.update(achLMTDList);
        }*/
        if(System.Label.ActivityTriggeronBusinessDate != null && System.Label.ActivityTriggeronBusinessDate == 'true') {
        BusinessDateTriggerHandler.dummyUpdate();
        }
        // Below block unchecks M-2 flag for Achievements not having 2 months difference
        /*List<Achievement__c> achNewList = new List<Achievement__c>();
        List<Achievement__c> achUncheck = new List<Achievement__c>();
        achNewList = [Select Id, M_2__c, Business_Date__c, Month_Value__c, Month_Difference__c from Achievement__c where Month_Difference__c != 2 OR Month_Difference__c != -2];
        for(Achievement__c ach : achNewList){
            ach.M_2__c = FALSE;
            achUncheck.add(ach);
        }
        system.debug('--Uncheck FTD, M-2 SIZE--:'+achUncheck.size());
        if(achUncheck.size()>0){
            database.update(achUncheck);
        }*/
        if(System.Label.ActivityTriggeronBusinessDate != null && System.Label.ActivityTriggeronBusinessDate == 'true') {
        BusinessDateTriggerHandler.uncheckM2();
        }
    }

}