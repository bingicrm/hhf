trigger AccountTrigger on Account (after insert, after update, after delete, before insert, before update) {

   // AccountTriggerHandler handler = AccountTriggerHandler.getInstance();
    
    if(trigger.isBefore && trigger.isInsert){
         AccountTriggerHandler.beforeInsert(trigger.New,trigger.newMap);
       
    }
    else if(trigger.isBefore && trigger.isUpdate){
       AccountTriggerHandler.beforeUpdate(trigger.new,trigger.oldMap);
    }
    else if(trigger.isBefore && trigger.isDelete){
        
    }
    else if(trigger.isAfter && trigger.isInsert){
       
    }
    else if(trigger.isAfter && trigger.isUpdate){
        //handler.afterUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
    }
    else if(trigger.isAfter && trigger.isDelete){
    
   
}
}