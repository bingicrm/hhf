/*=====================================================================
 * Deloitte India
 * Name:TrancheTrigger
 * Description: This is the trigger on the Tranche object
 * Created Date: [12/Oct/2018]
 * Created By:Asit Porwal (Deloitte India)
 * Events : Before : insert, update
 *          After  : insert, update
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/


trigger TrancheTrigger on Tranche__c (before insert, before update, after insert, after update, before delete, after Delete) {
    if(trigger.isBefore && trigger.isInsert){        
        TrancheTriggerHandler.beforeInsert(trigger.new);
    }
    else if(trigger.isBefore && trigger.isUpdate){
        TrancheTriggerHandler.beforeUpdate(trigger.new,trigger.oldMap);      
    }
    else if(trigger.isBefore && trigger.isDelete){
        TrancheTriggerHandler.beforeDelete(trigger.oldMap);
    }
    else if(trigger.isAfter && trigger.isInsert){
        TrancheTriggerHandler.afterInsert(trigger.new);
    }
    else if(trigger.isAfter && trigger.isUpdate){
        TrancheTriggerHandler.afterUpdate(trigger.new,trigger.oldMap);
    }
    else if(trigger.isAfter && trigger.isDelete){
    
    }

}