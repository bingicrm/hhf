/**=====================================================================
 * Deloitte India
 * Name: Address_Trigger
 * Description: This is a trigger on Address.
 * Created Date: [10/15/2018]
 * Created By: Gaurav Nawal (Deloitte India)
 *
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/ 
trigger Address_Trigger on Address__c (before insert, after insert, before update, after update, before delete, after delete) {
    if(Trigger.isInsert && Trigger.isBefore) {
        Address_TriggerHandler.beforeInsert(Trigger.new);
    }
    if(Trigger.isInsert && Trigger.isAfter) {
        Address_TriggerHandler.afterInsert(Trigger.new, Trigger.newMap);
    }
    if(Trigger.isUpdate && Trigger.isBefore) {
        Address_TriggerHandler.beforeUpdate(Trigger.new,Trigger.newMap, Trigger.oldMap);
    }
    if(Trigger.isUpdate && Trigger.isAfter) {
        Address_TriggerHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);
    }
}