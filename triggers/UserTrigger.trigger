trigger UserTrigger on User (after insert, after update) {
    if(trigger.isAfter && trigger.isInsert){
        UserTriggerHandler.afterInsert(trigger.new);
    }
     if(trigger.isAfter && trigger.isUpdate){
        UserTriggerHandler.afterUpdate(trigger.new, trigger.oldMap);
    }
}