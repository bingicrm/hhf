trigger BREDecisionTrigger on Application_CIBIL_Decision_Detail__c (before insert, after insert) {
    if(trigger.isAfter && trigger.isInsert){
        BREDecisionTriggerHandler.afterInsert(trigger.new);
    }
    if(trigger.isBefore && trigger.isInsert){
        BREDecisionTriggerHandler.beforeInsert(trigger.new);
    }
}