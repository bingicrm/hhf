trigger TaskTrigger on Task (before insert, before update, after insert, after update) {
    if(Trigger.isAfter && Trigger.isInsert) {
    	TaskTriggerHandler.afterInsert(trigger.new);
    }
    if(trigger.isBefore && trigger.isUpdate){
        TaskTriggerHandler.beforeUpdate(trigger.new);
    }
    if(trigger.isAfter && trigger.isUpdate){
        TaskTriggerHandler.afterUpdate(trigger.new);
    }
}