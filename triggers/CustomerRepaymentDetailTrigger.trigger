trigger CustomerRepaymentDetailTrigger on PDC__c (before insert) {
    /* 
        Created By      : Shobhit Saxena(Deloitte)
        Created On      : 26 December, 2018
        Requested By    : Gaurav Khurana(HHFL)
        Requirement     : Each Loan Application can have one and only one Customer Repayment Detail Record
    
    */
    if(trigger.isBefore && trigger.isInsert) {
        CustomerRepaymentDetailTriggerHandler.onBeforeInsert(Trigger.new);
    }
}