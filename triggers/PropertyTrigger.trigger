//Added by Vaishali for BRE2 (Check after Update argument while deployment)
trigger PropertyTrigger on Property__c (before insert,before update,after update) {
    if(Trigger.isBefore && Trigger.isInsert) {
        PropertyTriggerHandler.onBeforeInsert(Trigger.new);
    }
    
    //Below if block added by Abhilekh on 20th August 2019 for TIL-855
    if(Trigger.isBefore && Trigger.isUpdate){
        PropertyTriggerHandler.onBeforeUpdate(Trigger.new,trigger.old,trigger.newMap,trigger.oldMap);
    }
	// Added by Vaishali for BRE2
    if(Trigger.isAfter && Trigger.isUpdate){
        PropertyTriggerHandler.afterUpdate(Trigger.New,Trigger.oldMap);
    }
    // Added by Vaishali for BRE2
}