trigger CustIntegrationTrigger on Customer_Integration__c (after insert, after update, after delete, before insert, before update) {

    /* Date Modified                Modified By                  Description of the update

        24/07/2019                  Shobhit Saxena              Handling added for Before Update Event
        
        =====================================================================*/
      
    if(trigger.isBefore && trigger.isInsert){
        CustIntegrationTriggerHandler.beforeInsert(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
    }
    else if(trigger.isBefore && trigger.isUpdate){
        // Added By Vaishali
        CustIntegrationTriggerHandler.onBeforeUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
    }
    else if(trigger.isBefore && trigger.isDelete){
        
    }
    else if(trigger.isAfter && trigger.isInsert){
         CustIntegrationTriggerHandler.afterInsert(trigger.New,trigger.newMap);
    }
    else if(trigger.isAfter && trigger.isUpdate){
        // Added By Vaishali
        CustIntegrationTriggerHandler.onAfterUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);    
    }
    else if(trigger.isAfter && trigger.isDelete){
    
    }
       
}