trigger monthlyInflowOutflow on Monthly_Inflow_Outflow__c (before Insert,after Insert,before Update,after Update) {
    
    //Below if block added by Abhilekh for BRE2 Enhancements
    if(trigger.isBefore && trigger.isInsert) {
        monthlyInflowOutflowTriggerHandler.beforeInsert(Trigger.New);
    }
    
    if(trigger.isAfter && trigger.isInsert) {
        monthlyInflowOutflowTriggerHandler.afterInsert(Trigger.New);
    }
    
    //Below if block added by Abhilekh for BRE2 Enhancements
    if(trigger.isBefore && trigger.isUpdate) {
        monthlyInflowOutflowTriggerHandler.beforeUpdate(Trigger.New,Trigger.oldMap);
    }
    
    if(trigger.isAfter && trigger.isUpdate ) {
        monthlyInflowOutflowTriggerHandler.afterUpdate(Trigger.New,Trigger.oldMap);
    }

}