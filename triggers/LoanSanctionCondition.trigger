trigger LoanSanctionCondition on Loan_Sanction_Condition__c ( before update, after insert, after update , before insert) 
{
    if(Trigger.isBefore && Trigger.isUpdate )
    {
    //system.assert(false, 'here1');
        LoanSanctionConditionHandler.beforeUdpateHandler(trigger.new,trigger.oldMap);
    }
    if(Trigger.isAfter && Trigger.isUpdate )
    {
       // system.assert(false, 'here');
        LoanSanctionConditionHandler.AfterUdpateHandler(trigger.new,trigger.oldMap);
    }
    if(Trigger.isAfter && Trigger.isInsert )
    {
        LoanSanctionConditionHandler.afterInsertHandler(trigger.new);
    }
    
}