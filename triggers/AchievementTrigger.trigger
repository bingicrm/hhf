trigger AchievementTrigger on Achievement__c (before insert, before update) {
    if ( trigger.isInsert && trigger.isbefore ) {
        AchievementTriggerHandler.beforeInsert(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
    }
    if ( trigger.isUpdate && trigger.isbefore ) {
        AchievementTriggerHandler.beforeUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
    }

}