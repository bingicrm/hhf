trigger CustomerObligationTrigger on Customer_Obligations__c (after insert, after update) {
    if(trigger.isAfter && trigger.isInsert){
        CustomerObligationTriggerHandler.afterInsert(trigger.new);
    }
    if(trigger.isAfter && trigger.isUpdate){
        CustomerObligationTriggerHandler.afterUpdate(trigger.new, trigger.old, trigger.newMap, trigger.oldMap);
    }
}