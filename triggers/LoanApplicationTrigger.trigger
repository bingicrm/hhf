/*=====================================================================
 * Deloitte India
 * Name:LoanApplicationTrigger
 * Description: This is the trigger on the loan application object
 * Created Date: [05/07/2018]
 * Created By:Rahul Kumar(Deloitte India)
 *
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/
trigger LoanApplicationTrigger on Loan_Application__c (before insert, before update, after insert, after update, after delete) {
    /*Added by Ankit for TL-Datafix*/
	TriggerActivation__c taObj = TriggerActivation__c.getValues('LoanApplicationTrigger');
	if(taObj == null || (taObj != null && taObj.Active__c)){/*Added by Ankit for TL-Datafix*/
	
		if(trigger.isBefore && trigger.isInsert){
			LoanApplicationTriggerHandler.beforeInsert(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
		}
		else if(trigger.isBefore && trigger.isUpdate && LoanApplicationTriggerHandler.runInsLoan){	//Added By Ankit for Code Optimization 21-12-20
			system.debug('==in here===loan application');
			LoanApplicationTriggerHandler.beforeUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);  
			LoanApplicationTriggerHandler2.beforeUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);// New class created for code optimization purpose. APF Handling done here.
		}
		else if(trigger.isBefore && trigger.isDelete){
			
		}
		else if(trigger.isAfter && trigger.isInsert){
			LoanApplicationTriggerHandler.afterInsert(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);      
		}
		else if(trigger.isAfter && trigger.isUpdate && LoanApplicationTriggerHandler.runInsLoan){	//Added By Ankit for Code Optimization 21-12-20
			LoanApplicationTriggerHandler.afterUpdate(trigger.new,trigger.old,trigger.newMap,trigger.oldMap);  
			LoanApplicationTriggerHandler2.afterUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);// New class created for code optimization purpose. APF Handling done here.
		}
		else if(trigger.isAfter && trigger.isDelete){
		
		}
	}	/*Added by Ankit for TL-Datafix*/
}