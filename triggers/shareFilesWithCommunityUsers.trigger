trigger shareFilesWithCommunityUsers on ContentDocumentLink(before insert, after insert, before Delete){
    if(Trigger.isBefore && Trigger.isInsert) {
        Schema.DescribeSObjectResult r = document_checklist__c.sObjectType.getDescribe(); //for attachments on document checklist
        Schema.DescribeSObjectResult l = loan_application__c.sObjectType.getDescribe(); //for attachments on loan application
        Schema.DescribeSObjectResult t = task.sObjectType.getDescribe(); //for attachments on task
        Schema.DescribeSObjectResult tpv = third_party_verification__c.sObjectType.getDescribe(); //for attachments on third party

        String keyPrefix1 = r.getKeyPrefix();
        String keyPrefix2 = t.getKeyPrefix();
        String keyPrefix3 = l.getKeyPrefix();
        String keyPrefix4 = tpv.getKeyPrefix();

        
		List<Id> cdIds = new  List<Id>(); //Added by Chitransh Porwal on 27th June 2019 for TIL-1244
        for(ContentDocumentLink cdl:trigger.new){
            system.debug('@@cdl.LinkedEntityId' + cdl.LinkedEntityId);
            if((String.valueOf(cdl.LinkedEntityId)).startsWith(keyPrefix1) || (String.valueOf(cdl.LinkedEntityId)).startsWith(keyPrefix2) || (String.valueOf(cdl.LinkedEntityId)).startsWith(keyPrefix3) || (String.valueOf(cdl.LinkedEntityId)).startsWith(keyPrefix4)){
                    cdl.ShareType = 'I';
                    cdl.Visibility = 'AllUsers';
                }
                cdIds.add(cdl.ContentDocumentId); //Added by Chitransh Porwal on 27th June 2019 for TIL-1244
            
                system.debug('LINE 16:'+cdl);
            }
					//code snippet to restrict uploading of files with extension exe and php//
        /***********************************Added by Chitransh Porwal on 27 June 2019 for TIL-1244**********************************/
        List<ContentDocument> listCD = new List<ContentDocument>();
        if(!cdIds.isEmpty()){
             listCD = [SELECT Id, FileExtension FROM ContentDocument where id = : cdIds];
        }
        
        for(ContentDocumentLink cdl: trigger.new){
            if(!listCD.isEmpty()){
                for(ContentDocument cd: listCD){
                 if(cdl.ContentDocumentId == cd.Id && (cd.FileExtension == 'exe' || cd.FileExtension == 'php')){
                     system.debug('ERROR!!');
                     cdl.addError('EXE or PHP files are not allowed.');
                 }
             }
            }
             
         }
        /****************************End of Patch added by Chitransh on 27 June 2019 for TIL-1244****************************************************/
    }
    
    if(Trigger.isAfter && Trigger.isInsert) {
        Schema.DescribeSObjectResult r = document_checklist__c.sObjectType.getDescribe();
        String keyPrefix1 = r.getKeyPrefix();
        Set<Id> setLinkedEntityIds = new Set<Id>();
        List<ContentDocumentLink> lstContentDocumentLink = new List<ContentDocumentLink>();
        List<File_Upload_Status__c> lstFileUploadStatuses = new List<File_Upload_Status__c>();
        if(!trigger.new.isEmpty()) {
            for(ContentDocumentLink objCDL : trigger.new) {
                if(String.isNotBlank(objCDL.LinkedEntityId)) {
                    if((String.valueOf(objCDL.LinkedEntityId)).startsWith(keyPrefix1)) {
                        System.debug('>>>>Linked Entity Id'+objCDL.LinkedEntityId);
                        setLinkedEntityIds.add(objCDL.LinkedEntityId);
                    }
                }
            }
        }
        System.debug('Debug Log for setLinkedEntityIds'+setLinkedEntityIds.size());
        if(!setLinkedEntityIds.isEmpty()) {
            lstContentDocumentLink = [SELECT Id, LinkedEntityId, ContentDocument.Title, ContentDocument.FileType, ContentDocument.ContentSize From ContentDocumentLink WHERE LinkedEntityId IN: setLinkedEntityIds AND Id IN: Trigger.new];
        }
        System.debug('Debug Log for lstContentDocumentLink'+lstContentDocumentLink.size());
        
        if(!lstContentDocumentLink.isEmpty()) {
            for(ContentDocumentLink cdl:lstContentDocumentLink){
                
                    System.debug('File Title'+cdl.ContentDocument.Title);
                    System.debug('FileType'+cdl.ContentDocument.FileType);
                    File_Upload_Status__c objFileUploadStatus = new File_Upload_Status__c();
                        objFileUploadStatus.Document_Checklist__c = cdl.LinkedEntityId;
                        objFileUploadStatus.File_Type__c = cdl.ContentDocument.FileType;
                        objFileUploadStatus.File_Name__c = cdl.ContentDocument.Title;
                        objFileUploadStatus.File_Size__c = String.valueOf(cdl.ContentDocument.ContentSize/1000)+' KB';
                        lstFileUploadStatuses.add(objFileUploadStatus);
                
                
                if(!lstFileUploadStatuses.isEmpty()) {
                    Database.SaveResult[] dbSaveResult = Database.insert(lstFileUploadStatuses, false);
                    for(Database.SaveResult objSaveResult : dbSaveResult) {
                        if(!objSaveResult.isSuccess()) {
                            System.debug('Error : '+objSaveResult.getErrors());
                        }
                        else {
                            System.debug('Record Inserted Successfully with Id'+objSaveResult.getId());
                        }
                    }
                }
            }
        }
    }
}