trigger UCICDatabaseTrigger on UCIC_Database__c (before insert, after insert, before update, after update) {
    if(trigger.isBefore && trigger.isUpdate){
        UCICDatabaseTriggerHandler.beforeUpdate(trigger.new,trigger.oldMap);
    }
    if(trigger.isAfter && trigger.isInsert){
        UCICDatabaseTriggerHandler.afterInsert(trigger.new);
    }
    if(trigger.isAfter && trigger.isUpdate){
        UCICDatabaseTriggerHandler.afterUpdate(trigger.new,trigger.oldMap);
    }  
}