/*=====================================================================
* Persistent Systems Ltd
* Name: CRM_CaseTrigger
* Description: This is a case trigger implemented for CRM.
* Created Date: [01/02/2021]
* Created By: Pavani Duggirala
*
* Date Modified                Modified By                  Description of the update

    
=====================================================================*/
trigger CRM_CaseTrigger on Case (before insert,before update,after insert,after update) {
    
    APF_Triggers_Configuration__mdt configMDT = [SELECT Id, DeveloperName, isTriggerActive__c,Trigger_Bypass__c,  Enable_Before_Insert__c, Enable_After_Insert__c, Enable_Before_Update__c, Enable_After_Update__c, Enable_Before_Delete__c, Enable_After_Delete__c, Object_API_Name__c From APF_Triggers_Configuration__mdt Where Object_API_Name__c = 'Case' LIMIT 1];
    
    if(configMDT != NULL && configMDT.isTriggerActive__c && !System.isBatch() && !System.isScheduled()){
        try {
            CRM_CaseTriggerHandler.isInsertContext = Trigger.isInsert;
            
            if(Trigger.isInsert && Trigger.isBefore){ 
                if(configMDT.Trigger_Bypass__c){
                    CRM_CaseTriggerHandler.checkDuplicateCases(Trigger.New, null);
                    CRM_CaseTriggerHandler.populateCaseOwnerRoundRobin(Trigger.New);
                    CRM_CaseTriggerHandler.setCasePriority(Trigger.New);
                }
                CRM_CaseTriggerHandler.populateTAT(Trigger.New);
                CRM_CaseTriggerHandler.setDueDateAndNetUsedDays(Trigger.New, null);
                CRM_CaseTriggerHandler.populateCustomerDetails(Trigger.New);
                CRM_CaseTriggerHandler.populateCustomerFromLoan(Trigger.New);
            }
            
            if(Trigger.isInsert  && Trigger.isAfter){
                CRM_CaseTriggerHandler.trackDepartmentChanges(Trigger.New, null);
                CRM_CaseTriggerHandler.createEmailTrackerRecords(Trigger.New, null);
                
                if(configMDT.Trigger_Bypass__c){
                    CRM_CaseTriggerHandler.sendEmailToCustomerOnCaseCreateOrUpdate(Trigger.New, null);
                    CRM_CaseTriggerHandler.sendEmailToCustomerOnCaseClosure(Trigger.New, null);
                    CRM_CaseTriggerHandler.sendSMSonCaseCreationOrUpdate(Trigger.New, null);
                    CRM_CaseTriggerHandler.sendSMSonCaseClosure(Trigger.New, null);
					CRM_CaseTriggerHandler.syncCaseDetailsOnInsert(Trigger.New, null);
					//SyncCaseDetails
                }
                
            }
            if(Trigger.isBefore && Trigger.isUpdate){
                if(configMDT.Trigger_Bypass__c){
                	CRM_CaseTriggerHandler.checkDuplicateCases(Trigger.New, Trigger.oldMap);
                	CRM_CaseTriggerHandler.validateCaseClosure(Trigger.New,Trigger.oldMap);
                }
                CRM_CaseTriggerHandler.populateTAT(Trigger.New);
                CRM_CaseTriggerHandler.setDueDateAndNetUsedDays(Trigger.New, Trigger.oldMap);
                CRM_CaseTriggerHandler.populateCustomerDetails(Trigger.New);
                CRM_CaseTriggerHandler.populateCustomerFromLoan(Trigger.New);
                CRM_CaseTriggerHandler.validateCaseClosure(Trigger.New, Trigger.oldMap);
            }
       
            if(Trigger.isUpdate && Trigger.isAfter){
                CRM_CaseTriggerHandler.trackDepartmentChanges(Trigger.New, Trigger.oldMap);
                CRM_CaseTriggerHandler.UpdateDepartmentChangesExitDateWhenClosed(Trigger.New);
                CRM_CaseTriggerHandler.createDepartmentChangesWhenCaseReopen(Trigger.New, Trigger.oldMap); 
                
                
                if(configMDT.Trigger_Bypass__c){
                    CRM_CaseTriggerHandler.createEmailTrackerRecords(Trigger.New, Trigger.oldMap);
                    CRM_CaseTriggerHandler.sendEmailToCustomerOnCaseCreateOrUpdate(Trigger.New, Trigger.oldMap);
                    CRM_CaseTriggerHandler.sendEmailToCustomerOnCaseClosure(Trigger.New, Trigger.oldMap);
                    CRM_CaseTriggerHandler.sendSMSonCaseCreationOrUpdate(Trigger.New, Trigger.oldMap);
                    CRM_CaseTriggerHandler.sendSMSonCaseClosure(Trigger.New,  Trigger.oldMap);
					CRM_CaseTriggerHandler.syncCaseDetailsOnUpdate(Trigger.New, Trigger.oldMap);
					//SyncCaseDetails
                }
            }
            
        } catch(Exception e){
            system.debug('Exception in Case trigger '+e.getStackTraceString());
        }
        finally {
            System.debug('CaseCheck Aggregate:' + System.limits.getAggregateQueries() + '/' + System.limits.getLimitAggregateQueries() );
            System.debug('CaseCheck CPU Time:' + System.limits.getCpuTime() + '/' + System.limits.getLimitCpuTime() );
            System.debug('CaseCheck DML Rows:' + System.limits.getDMLRows() + '/' + System.limits.getLimitDMLRows() );
            System.debug('CaseCheck DML Statements:' + System.limits.getDMLStatements() + '/' + System.limits.getLimitDMLStatements() );
            System.debug('CaseCheck heap size:' + System.limits.getHeapSize() + '/' + System.limits.getLimitHeapSize() );
            System.debug('CaseCheck queries:' + System.limits.getQueries() + '/' + System.limits.getLimitQueries() );
            System.debug('CaseCheck Query Rows:' + System.limits.getQueryRows() + '/' + System.limits.getLimitQueryRows() );
        }
    }
}