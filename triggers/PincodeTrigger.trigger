trigger PincodeTrigger on Pincode__c (before insert,before update) {
    if (trigger.isBefore && trigger.isInsert) {
        PincodeTriggerHandler.beforeInsert(trigger.new);
    }
    if (trigger.isBefore && trigger.isUpdate) {
        PincodeTriggerHandler.beforeUpdate(trigger.new);
    }
}