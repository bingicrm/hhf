/*
*   Executed:   After insert
*   Purpose:    Update fields in S3-File
*/
trigger S3LinkFileTrigger on NEILON__File__c (after insert) {
    S3LinkFileTriggerHandler objFileTriggerHandler = new S3LinkFileTriggerHandler();
    if (Trigger.isInsert && Trigger.isAfter) {
        objFileTriggerHandler.onAfterInsert(Trigger.new);
    }
}