trigger AddressDetailTrigger on Address_Detail__c (before insert, before update) {
    if(trigger.isBefore && Trigger.isInsert) {
        AddressDetailTriggerHandler.onBeforeInsert(Trigger.New);
    }
}