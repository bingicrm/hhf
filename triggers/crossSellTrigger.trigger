/*=====================================================================
 * Deloitte India
 * Name:crossSellTrigger
 * Description: This is the trigger on Cross Sell object
 * Created Date: [26/02/2020]
 * Created By: Vaishali Mehta(Deloitte India)
 *
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/

trigger crossSellTrigger on Cross_Sell__c (after update, before update){
    													  
    if(trigger.isAfter && trigger.isUpdate){
        crossSellTriggerHandler.afterUpdate(trigger.new, trigger.oldMap);
         
    }
    if(trigger.isBefore && trigger.isUpdate){
        crossSellTriggerHandler.beforeUpdate(trigger.new, trigger.oldMap);
    }
}