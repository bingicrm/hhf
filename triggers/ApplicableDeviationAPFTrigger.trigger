trigger ApplicableDeviationAPFTrigger on Applicable_Deviation_APF__c (after insert) {
    if(Trigger.isAfter && Trigger.isInsert) {
        List<Applicable_Deviation_APF__c> lstAPFDeviation = [Select Id, Name, Project__c, Credit_Deviation_Master_APF__c, Credit_Deviation_Master_APF__r.Name From Applicable_Deviation_APF__c WHERE ID IN: Trigger.New];
        for(Applicable_Deviation_APF__c objADAPF : lstAPFDeviation) {
            if(String.isNotBlank(objADAPF.Credit_Deviation_Master_APF__c)) {
                Project__c objProject = [Select Id, Name, NCM_Approval_Required__c, CRO_Approval_Required__c From Project__c Where Id =: objADAPF.Project__c];
                if(objADAPF.Credit_Deviation_Master_APF__r.Name == 'APF Norms Not Met' || objADAPF.Credit_Deviation_Master_APF__r.Name == 'Legal Deviation in APF' || 
                    objADAPF.Credit_Deviation_Master_APF__r.Name == 'Technical Deviation in APF' || objADAPF.Credit_Deviation_Master_APF__r.Name == 'Disbursement upto 10% higher than standard CLP') {
                    
                    objProject.NCM_Approval_Required__c = true;
                    objProject.CRO_Approval_Required__c = true;
                }
                else if(objADAPF.Credit_Deviation_Master_APF__r.Name == 'Deemed APF Approval other than ICICI/HDFC' || objADAPF.Credit_Deviation_Master_APF__r.Name == 'Any Other Deviation') {
                    objProject.NCM_Approval_Required__c = true;
                }
                System.debug('Debug Log for objProject'+objProject);
                update objProject;
            }
        }
    }
}