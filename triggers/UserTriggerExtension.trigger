trigger UserTriggerExtension on User (after insert, after update, before insert, before update) {
    if(trigger.isAfter && trigger.isInsert){
        UserTriggerHandler.afterInsert(trigger.new);
    }
     if(trigger.isBefore && trigger.isInsert){
        UserTriggerHandler.beforeInsert(trigger.new);
    }
}