trigger ProjectInventoryTrigger on Project_Inventory__c (before insert, before update, before delete) {
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            ProjectInventoryTriggerHandler.onBeforeInsert(Trigger.new);
        }
        if(Trigger.isUpdate) {
            List<Project_Inventory__c> lstProjectInvUpdated = new List<Project_Inventory__c>();
            for(Project_Inventory__c objProjectInventory : Trigger.new) {
                if(objProjectInventory.Flat_House_No__c != Trigger.oldMap.get(objProjectInventory.Id).Flat_House_No__c 
                || objProjectInventory.Floor__c != Trigger.oldMap.get(objProjectInventory.Id).Floor__c
                || objProjectInventory.Tower__c != Trigger.oldMap.get(objProjectInventory.Id).Tower__c
                ) {
                    lstProjectInvUpdated.add(objProjectInventory);
                }
            }
            if(!lstProjectInvUpdated.isEmpty()) {
                ProjectInventoryTriggerHandler.onBeforeInsert(Trigger.new);
            }
            ProjectInventoryTriggerHandler.onBeforeUpdate(Trigger.newMap, Trigger.oldMap);
        }
        
        if(Trigger.isDelete) {
            ProjectInventoryTriggerHandler.onBeforeDelete(Trigger.Old);
        }
    }
}