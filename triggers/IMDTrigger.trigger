//Added by Vaishali for BRE2 (Check before Update argument while deployment)
trigger IMDTrigger on IMD__c (after insert, after update, after delete, before insert, before update) {
    //This Trigger will call the API for obtaining the Cheque Id after creation of record.
    
    if(Trigger.isBefore && Trigger.isInsert) {
        System.debug('Trigger.new size'+Trigger.new.size());
        IMDTriggerHandler.onBeforeInsert(Trigger.new);
    }


    if(Trigger.isAfter && Trigger.isInsert) {
        System.debug('Trigger.new size'+Trigger.new.size());
        IMDTriggerHandler.onAfterInsert(Trigger.new);
    }

    if(Trigger.isAfter && Trigger.isUpdate) {
        System.debug('Trigger.new size'+Trigger.new.size());
        IMDTriggerHandler.onAfterUpdate(Trigger.new,trigger.oldmap);
    }
    
    if(Trigger.isAfter && Trigger.isDelete) {
       
        IMDTriggerHandler.onAfterDelete(Trigger.old);
    }
	
	// Added by Vaishali for BRE2
    if(Trigger.isBefore && Trigger.isUpdate) {
        System.debug('Trigger.new size'+Trigger.new.size());
        IMDTriggerHandler.onBeforeUpdate(Trigger.new,trigger.oldmap);
    }
    //End of patch- Added by Vaishali for BRE2
}