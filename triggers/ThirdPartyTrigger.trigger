/*=====================================================================
 * Deloitte India
 * Name:ThirdPartyTrigger
 * Description: This is the trigger on Third Party Verification object
 * Created Date: [09/07/2018]
 * Created By:Kashish Krishna(Deloitte India)
 *
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/

trigger ThirdPartyTrigger on Third_Party_Verification__c (before insert, after update, after insert, before update,before delete,after delete,after undelete){
    													  //before delete,after delete,after undelete added by Abhilekh on 1st October 2019 for FCU BRD
    if(trigger.isBefore && trigger.isInsert){
        ThirdPartyTriggerHandler.beforeInsert(trigger.new);
    }

    if(trigger.isAfter && trigger.isInsert){
        ThirdPartyTriggerHandler.afterInsert(trigger.new);
    }
    
    if(trigger.isAfter && trigger.isUpdate){
        ThirdPartyTriggerHandler.afterUpdate(trigger.new, trigger.oldMap);
        //ThirdPartyTriggerHandler.afterUpdateSMS(trigger.new, trigger.oldMap);
    }
    if(trigger.isBefore && trigger.isUpdate){
        ThirdPartyTriggerHandler.beforeUpdate(trigger.new, trigger.oldMap);
    }
    
    //Below if block added by Abhilekh on 1st October 2019 for FCU BRD
    if(trigger.isAfter && trigger.isDelete){
        ThirdPartyTriggerHandler.afterDelete(trigger.old,trigger.oldMap);
    }
    
    //Below if block added by Abhilekh on 1st October 2019 for FCU BRD
    if(trigger.isAfter && trigger.isUndelete){
        ThirdPartyTriggerHandler.afterUndelete(trigger.new, trigger.newMap);
    }
}