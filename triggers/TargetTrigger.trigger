/*
@@author : Deloitte Team
@@Created Date : 17th June 2019
@@Description : Trigger on Target Object
*/

trigger TargetTrigger on Targets__c (before insert, after insert) {
    if ( trigger.isInsert && trigger.isAfter ) {
        TargetTriggerHandler.afterInsert(trigger.new, trigger.oldMap);
    }
}