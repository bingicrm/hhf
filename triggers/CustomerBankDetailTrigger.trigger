/*=====================================================================
 * Deloitte India
 * Name: CustomerBankDetailTrigger
 * Description: This is the trigger on the Customer Bank Detail (Bank_Detail__c) object
 * Created Date: [17/02/2020]
 * Created By: Abhilekh Anand(Deloitte India)
 *
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/
trigger CustomerBankDetailTrigger on Bank_Detail__c (before insert, before update, after insert, after update, after delete) {
	
    if(trigger.isBefore && trigger.isUpdate){
        CustomerBankDetailTriggerHandler.beforeUpdate(trigger.New,trigger.oldMap);
    }
}