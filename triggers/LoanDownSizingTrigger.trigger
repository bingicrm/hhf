trigger LoanDownSizingTrigger on Loan_Downsizing__c (after update, after insert, before insert, before update) 
{
  //Before Trigger(s) Starts\
    if (trigger.isAfter ){
        if(trigger.isUpdate) 
        {
            LoanDownSizingTriggerHandler.changeLoanApplicationStage(trigger.new);
            
            if(trigger.new.size()==1)
            {
                LoanDownSizingTriggerHandler.changeDocumentChecklistStatus(trigger.new[0]);   
            }
        }
        if(trigger.isInsert){
            LoanDownSizingTriggerHandler.newDownsizingOperations(trigger.new);
        }
      }
    
  //Before Trigger(s) Ends
  //After Trigger Starts
    if(trigger.isBefore)
    {
        if(trigger.isInsert)
        {
            system.debug('Inside Before Insert');
           // LoanDownSizingTriggerHandler.checkValidLoanDownsizing(trigger.new); 
        }       
    }
  //After Trigger Ends
    
}