trigger leadTrigger on Lead (after insert, after update, after delete, before insert, before update) {
   // leadTriggerHandler handler = leadTriggerHandler.getInstance();
    
    if(trigger.isBefore && trigger.isInsert){
    
       LeadTriggerHandler.beforeInsert(trigger.New, trigger.newMap);
       
    }
    else if(trigger.isBefore && trigger.isUpdate){
       system.debug('@@ before update');
        LeadTriggerHandler.beforeUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
    }
    else if(trigger.isBefore && trigger.isDelete){
        
    }
    else if(trigger.isAfter && trigger.isInsert){
       LeadTriggerHandler.afterInsert(trigger.new);
    }
    else if(trigger.isAfter && trigger.isUpdate){
       LeadTriggerHandler.afterUpdate(trigger.new, trigger.oldMap);
    }
    else if(trigger.isAfter && trigger.isDelete){
    
    }
}