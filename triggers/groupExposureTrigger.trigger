trigger groupExposureTrigger on Group_Exposure__c (before insert, before update, after insert, after update, after delete) {
    
    if(trigger.isBefore && trigger.isInsert){
       groupExposureTriggerHandler.beforeInsert(trigger.new, trigger.newMap);
    }
    else if(trigger.isBefore && trigger.isUpdate){
    
    }
    else if(trigger.isBefore && trigger.isDelete){
        
    }
    else if(trigger.isAfter && trigger.isInsert){
        //groupExposureTriggerHandler.afterInsert(trigger.new); 
    }
    else if(trigger.isAfter && trigger.isUpdate){
    
    }
    else if(trigger.isAfter && trigger.isDelete){
    
    }
}