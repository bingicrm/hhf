// Before Delete argument added by Vaishali for BREII
trigger ApplicableDeviationTrigger on Applicable_Deviation__c (before insert, before update, after insert, after update, after delete, before Delete) {
    if(trigger.isAfter && trigger.isInsert){
        Applicable_Deviation__c app = new Applicable_Deviation__c();
        ApplicableDeviationTriggerHandler.afterInsert(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
    }
    else if(trigger.isAfter && trigger.isUpdate){
        Applicable_Deviation__c app = new Applicable_Deviation__c();
        ApplicableDeviationTriggerHandler.afterUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
    }
    //Added by Vaishali for BREII
    else if(trigger.isBefore && trigger.isUpdate){
        ApplicableDeviationTriggerHandler.beforeUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
    }
    else if(trigger.isBefore && trigger.isDelete) {
        system.debug('Here in DELETION');
        ApplicableDeviationTriggerHandler.beforeDelete(trigger.old, trigger.oldMap);
    }
	else if(trigger.isAfter && trigger.isDelete){/**** Added by Saumya For L1 Credit Issue ****/
       // Applicable_Deviation__c app = new Applicable_Deviation__c();
        ApplicableDeviationTriggerHandler.afterDelete(trigger.old,trigger.oldMap);
    }
    //End of patch- Added by Vaishali for BREII	
}