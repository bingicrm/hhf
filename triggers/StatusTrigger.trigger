trigger StatusTrigger on Status_Tracking__c (before insert, after insert) {
    if(trigger.isBefore && trigger.isInsert){
        StatusTriggerHandler.beforeInsert(trigger.new);
    }
    else if(trigger.isAfter && trigger.isInsert){
        StatusTriggerHandler.afterInsert(trigger.new);
    }
}