trigger KYCDetailTrigger on OCR_Information__c (after insert, after update, after delete, before insert, before update) {

      
    if(trigger.isBefore && trigger.isInsert){
        KYCDetailTriggerHandler.beforeInsert(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
    }
    else if(trigger.isBefore && trigger.isUpdate){
        
    }
    else if(trigger.isBefore && trigger.isDelete){
        
    }
    else if(trigger.isAfter && trigger.isInsert){
        
    }
    else if(trigger.isAfter && trigger.isUpdate){
        
    }
    else if(trigger.isAfter && trigger.isDelete){
    
    }
       
}