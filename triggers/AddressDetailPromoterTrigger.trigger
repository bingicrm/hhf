trigger AddressDetailPromoterTrigger on Address_Detail_Promoter__c (before insert, before update) {
    if(trigger.isBefore && Trigger.isInsert) {
        AddressDetailPromoterTriggerHandler.onBeforeInsert(Trigger.New);
    }
}