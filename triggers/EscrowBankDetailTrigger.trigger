trigger EscrowBankDetailTrigger on Escrow_Bank_Details__c (before insert, after insert, before update, after update, before delete, after delete) {
    List<APF_Triggers_Configuration__mdt> configMDT = [SELECT Id, DeveloperName, NamespacePrefix, Enable_Before_Insert__c, Enable_After_Insert__c, Enable_Before_Update__c, Enable_After_Update__c, Enable_Before_Delete__c, Enable_After_Delete__c, Object_API_Name__c From APF_Triggers_Configuration__mdt Where Object_API_Name__c = 'Escrow_Bank_Details__c' LIMIT 1];
    if(!configMDT.isEmpty()) {
        if(Trigger.isBefore && Trigger.isInsert) {
            if(configMDT[0].Enable_Before_Insert__c) {
                EscrowBankDetailTriggerHandler.onBeforeInsert(Trigger.new);
            }
        }
        else if(Trigger.isAfter && Trigger.isInsert) {
            if(configMDT[0].Enable_After_Insert__c) {
                EscrowBankDetailTriggerHandler.onAfterInsert(Trigger.new);
            }
        }
    }
}