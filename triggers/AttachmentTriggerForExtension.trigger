trigger AttachmentTriggerForExtension on Attachment (before insert, before Delete) {
                    //code snippet to restrict uploading of files with extension exe and php//
    /***********************************Added by Chitransh Porwal on 27 June 2019**********************************/
    if(Trigger.isBefore && Trigger.isInsert){
        List<Attachment> lstAttachementTobeRestricted = new List<Attachment>();
        
        for(Attachment at: trigger.New){
            
            String fileName =  at.Name;
            if(String.isNotBlank(fileName)){
                List<String> fileNameithExtension = fileName.split('\\.');
                String fileExtension = fileNameithExtension[fileNameithExtension.size() - 1];
                
                if(fileExtension == 'exe' || fileExtension == 'php'){
                    lstAttachementTobeRestricted.add(at);
                }
            }
            
            
        }
        if(!lstAttachementTobeRestricted.isEmpty()){
            for(Attachment att : lstAttachementTobeRestricted){
                att.addError('EXE or PHP not allowed');
            }
        }
        
    }
    
    /****************************End of Patch added by Chitransh on 27 June 2019****************************************************/
    
    /****************************Added by Shobhit on 8 July 2019******************************************************************/
    if(Trigger.isBefore && Trigger.isDelete) {
        // addError logic added by KK for TIL-1400
        User u = [Select Id, Profile.Name from User where Id =: UserInfo.getUserId()];
        String profile = u.Profile.Name;
        if(profile != 'System Administrator'){
            List<Attachment> lstAttachementTobeRestricted = new List<Attachment>();
            for(Attachment at: trigger.old){
                at.addError('You are not allowed to delete this file.');
            }
        }
        else{
            AttachmentTriggerForExtensionHandler.onBeforeDelete(Trigger.oldMap);
        }
    }
    
    
    /****************************End of Patch added by Shobhit on 8 July 2019****************************************************/
    
}