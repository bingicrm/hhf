trigger FCUDocumentChecklistTrigger on FCU_Document_Checklist__c (before insert,after insert,before update,after update) {
	
    if(Trigger.isAfter && Trigger.isInsert){
        FCUDocumentChecklistTriggerHandler.afterInsert(Trigger.New);
    }
    
    if(Trigger.isAfter && Trigger.isUpdate){
        FCUDocumentChecklistTriggerHandler.afterUpdate(Trigger.New,Trigger.Old,Trigger.newMap,Trigger.oldMap);
    }
}