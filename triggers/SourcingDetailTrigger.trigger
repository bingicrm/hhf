trigger SourcingDetailTrigger on Sourcing_Detail__c (before insert, before update, after update, after insert) {
	/*Added by Ankit for TL-Datafix*/
    TriggerActivation__c taObj = TriggerActivation__c.getValues('SourcingDetailTrigger');
    if(taObj == null || (taObj != null && taObj.Active__c)){/*Added by Ankit for TL-Datafix*/
		if(Trigger.isBefore && Trigger.isInsert){
			//for(Sourcing_Detail__c sd: Trigger.New)
			SourcingDetailTriggerHandler.beforeInsert(Trigger.New);
		}
		else if(trigger.isAfter && trigger.isUpdate){
			system.debug('Debug log for after update');
			SourcingDetailTriggerHandler.afterUpdate(trigger.newMap,trigger.oldMap);
		}
		else if(trigger.isBefore && trigger.isUpdate){
            system.debug('Debug log in Before Trigger');
            SourcingDetailTriggerHandler.beforeUpdate(trigger.New,trigger.oldMap);
        }
        else if(trigger.isAfter && trigger.isInsert){
            system.debug('Debug log in After Trigger');
            SourcingDetailTriggerHandler.afterInsert(trigger.New);
        }
    }	/*Added by Ankit for TL-Datafix*/
}