trigger ProjectBuilderTrigger on Project_Builder__c (before insert, before update, after insert, after update, before delete, after delete) {
    List<APF_Triggers_Configuration__mdt> configMDT = [SELECT Id, DeveloperName, NamespacePrefix, Enable_Before_Insert__c, Enable_After_Insert__c, Enable_Before_Update__c, Enable_After_Update__c, Enable_Before_Delete__c, Enable_After_Delete__c, Object_API_Name__c From APF_Triggers_Configuration__mdt Where Object_API_Name__c = 'Project_Builder__c' LIMIT 1];
    if(!configMDT.isEmpty()) {
        if(Trigger.isBefore && Trigger.isInsert) {
            if(configMDT[0].Enable_Before_Insert__c) {
                ProjectBuilderTriggerHandler.onBeforeInsert(Trigger.new);
            }
        }
        if(Trigger.isAfter && Trigger.isInsert) {
            System.debug('After Insert from Project Builder Trigger Handler Before Check');
            if(configMDT[0].Enable_After_Insert__c) {
                System.debug('After Insert from Project Builder Trigger Handler Check Passed');
                ProjectBuilderTriggerHandler.onAfterInsert(Trigger.newMap);
            }
        }
        if(Trigger.isBefore && Trigger.isUpdate) {
            if(configMDT[0].Enable_Before_Update__c) {
                ProjectBuilderTriggerHandler.onBeforeUpdate(Trigger.newMap, Trigger.oldMap);
            }
        }
        if(Trigger.isAfter && Trigger.isUpdate) {
            if(configMDT[0].Enable_After_Update__c) {
                ProjectBuilderTriggerHandler.onAfterUpdate(Trigger.newMap, Trigger.oldMap);
            }
        }
    }
}