trigger DocumentChecklistTrigger on Document_Checklist__c(after insert, after update, after delete, before insert, before update) {
    
    DocumentChecklistTriggerHandler handler = DocumentChecklistTriggerHandler.getInstance();
    
    if(trigger.isBefore && trigger.isInsert) {
         DocumentChecklistTriggerHandler.beforeInsert(trigger.new);   
    } else if(trigger.isBefore && trigger.isUpdate) {
        handler.beforeUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
    } else if(trigger.isBefore && trigger.isDelete) {
        
    } else if(trigger.isAfter && trigger.isInsert) {
       DocumentChecklistTriggerHandler.afterInsert(trigger.new, trigger.old, trigger.newMap, trigger.oldMap);
    } else if(trigger.isAfter && trigger.isUpdate) {
        handler.afterUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
    } else if(trigger.isAfter && trigger.isDelete) {
    
    }
    
}