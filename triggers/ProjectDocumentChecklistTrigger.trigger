trigger ProjectDocumentChecklistTrigger on Project_Document_Checklist__c (before insert, before update, before delete, after insert, after update, after delete) {
    List<APF_Triggers_Configuration__mdt> configMDT = [SELECT Id, DeveloperName, NamespacePrefix, Enable_Before_Insert__c, Enable_After_Insert__c, Enable_Before_Update__c, Enable_After_Update__c, Enable_Before_Delete__c, Enable_After_Delete__c, Object_API_Name__c From APF_Triggers_Configuration__mdt Where Object_API_Name__c = 'Project_Document_Checklist__c' LIMIT 1];
    if(!configMDT.isEmpty()) {
        if(Trigger.isAfter && Trigger.isInsert) {
            if(configMDT[0].Enable_After_Insert__c) {
                ProjectDocumentChecklistTriggerHandler.onAfterInsert(Trigger.new);
            }
        }
        if(Trigger.isBefore && Trigger.isUpdate) {
            if(configMDT[0].Enable_Before_Update__c) {
                ProjectDocumentChecklistTriggerHandler.onBeforeUpdate(Trigger.newMap, Trigger.oldMap);
            }
        }
        if(Trigger.isAfter && Trigger.isUpdate) {
            if(configMDT[0].Enable_After_Update__c) {
                ProjectDocumentChecklistTriggerHandler.onAfterUpdate(Trigger.newMap, Trigger.oldMap);
            }
        }
    }
}