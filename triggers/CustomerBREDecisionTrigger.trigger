trigger CustomerBREDecisionTrigger on CIBIL_Decision_Detail__c (before insert, after insert) {
    if(trigger.isAfter && trigger.isInsert){
        CustomerBREDecisionTriggerHandler.afterInsert(trigger.new);
    }
    if(trigger.isBefore && trigger.isInsert){
        CustomerBREDecisionTriggerHandler.beforeInsert(trigger.new);
    }
}