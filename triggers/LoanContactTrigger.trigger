trigger LoanContactTrigger on Loan_Contact__c (after insert, after update, after delete, before insert, before update) {

      
    if(trigger.isBefore && trigger.isInsert){
       //  leadTriggerHandler.beforeInsert(trigger.New,trigger.newMap);
        LoanContactTriggerHandler.beforeInsert(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);

       
    }
    else if(trigger.isBefore && trigger.isUpdate){
        LoanContactTriggerHandler.beforeUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
      
    }
    else if(trigger.isBefore && trigger.isDelete){
        
    }
    else if(trigger.isAfter && trigger.isInsert){
        LoanContactTriggerHandler.AfterInsert(trigger.New,trigger.newMap);
       
    }
    else if(trigger.isAfter && trigger.isUpdate){
        LoanContactTriggerHandler.afterUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);

    }
    else if(trigger.isAfter && trigger.isDelete){
		//Added by Vaishali for BRE
        LoanContactTriggerHandler.afterDelete(trigger.oldMap);
    }
    
}