trigger Disbursement_repayment_details_Trigger on Disbursement_payment_details__c(after insert, after update, after delete, before insert, before update) {

   // AccountTriggerHandler handler = AccountTriggerHandler.getInstance();
    
    if(trigger.isBefore && trigger.isInsert){
       //  AccountTriggerHandler.beforeInsert(trigger.New,trigger.newMap);
       DisbursementRepaymentTriggerHandler.beforeInsert(trigger.new);
       
    }
    else if(trigger.isBefore && trigger.isUpdate){
      // AccountTriggerHandler.beforeUpdate(trigger.new);
      DisbursementRepaymentTriggerHandler.beforeUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
    }
    else if(trigger.isBefore && trigger.isDelete){
        
    }
    else if(trigger.isAfter && trigger.isInsert){
    DisbursementRepaymentTriggerHandler.afterInsert(trigger.new);
       
    }
    else if(trigger.isAfter && trigger.isUpdate){
        //handler.afterUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
    }
    else if(trigger.isAfter && trigger.isDelete){
    
   
}
}