trigger DisbursementTrigger on Disbursement__c (before insert, before update, after update/*Added by Chitransh [23-01-2020]*/) {
	/*Added by Ankit for TL-Datafix*/
    TriggerActivation__c taObj = TriggerActivation__c.getValues('DisbursementTrigger');
    if(taObj == null || (taObj != null && taObj.Active__c)){/*Added by Ankit for TL-Datafix*/
		if (trigger.isInsert && trigger.isBefore) {
			/* Modified by : Shobhit Saxena(Deloitte) on 16 Dec, 2k18
			   Modification Made : New method added in handler to validate charge to scheme mapping.*/
			DisbursementTriggerHandler.beforeInsert(trigger.new);
		} else if (trigger.isUpdate && trigger.isBefore) {
			DisbursementTriggerHandler.beforeUpdate(trigger.new, trigger.oldmap);
		}
		//*************Added by Chitransh to update Disburse_Amount__c on LA post Tranche Cancelled [23-01-2020]***************//
		else if(trigger.isUpdate && trigger.isAfter){
			DisbursementTriggerHandler.afterUpdate(trigger.new);
		}
		//*************Code Ends****************//
	}	/*Added by Ankit for TL-Datafix*/
}