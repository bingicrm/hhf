//Added by Vaishali for BRE2 (before Update, after Update)
trigger PersonalDiscussionTrigger on Personal_Discussion__c (before Insert,after insert, before Update, after Update) 
{
	if(trigger.isBefore && trigger.isInsert)
	{
        PersonalDiscussionTriggerHandler.beforeInsert(trigger.new);
    }
	if(trigger.isAfter && trigger.isInsert)
	{
        PersonalDiscussionTriggerHandler.afterInsert(trigger.new);
    }
	//Added by Vaishali for BRE2
    if(trigger.isAfter && trigger.isUpdate) {
        PersonalDiscussionTriggerHandler.afterUpdate(trigger.new, trigger.oldMap);    
    } 
    if(trigger.isBefore && trigger.isUpdate) {
        PersonalDiscussionTriggerHandler.beforeUpdate(trigger.new, trigger.oldMap);    
    }
    //End of patch- Added by Vaishali for BRE2
}