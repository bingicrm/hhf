trigger referenceTrigger on Reference__c (after insert, after update, after delete, before insert, before update) {      
    if(trigger.isBefore && trigger.isInsert){
        //leadTriggerHandler.beforeInsert(trigger.New,trigger.newMap);
        ReferenceTriggerHandler.beforeInsert(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
    }
    else if(trigger.isBefore && trigger.isUpdate){
		ReferenceTriggerHandler.beforeUpdate(trigger.New,trigger.oldMap); //Added by Abhilekh on 6th February 2020 for TIL-1942
    }
    else if(trigger.isBefore && trigger.isDelete){
        
    }
    else if(trigger.isAfter && trigger.isInsert){
        //LoanContactTriggerHandler.AfterInsert(trigger.New,trigger.newMap);
       
    }
    else if(trigger.isAfter && trigger.isUpdate){
        //LoanContactTriggerHandler.afterUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);

    }
    else if(trigger.isAfter && trigger.isDelete){
    
    }
}