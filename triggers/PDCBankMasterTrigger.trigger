trigger PDCBankMasterTrigger on PDC_Bank_Master__c (before insert, before update) {
    if(trigger.isInsert && trigger.isBefore){
       PDCBankMasterTriggerHandler.onBeforeInsert(trigger.new);
    }
    if(trigger.isUpdate && trigger.isBefore){
        PDCBankMasterTriggerHandler.onBeforeUpdate(trigger.new);
    }
}