trigger LoanRepaymentDuplicationDetectionTrigger on Loan_Repayment__c (before insert) {
    if(Trigger.isBefore && Trigger.isInsert) {
        Map<String, Integer> mapUniqueConstrainttoCount = new Map<String, Integer>();
        Map<String, String> mapLoanAppIdtoLoanRepyamentId = new Map<String, String>();
        List<Loan_Repayment__c> lstExistingLoanRepayment = new List<Loan_Repayment__c>();
        String strUniqueConstraint;
        if(Trigger.new.size() >1) {
            lstExistingLoanRepayment = [Select Id, Name, Loan_Application__c from Loan_Repayment__c];
            if(!lstExistingLoanRepayment.isEmpty()) {
                for(Loan_Repayment__c objExistingLoanRepayment : lstExistingLoanRepayment) {
                    if(!mapUniqueConstrainttoCount.containsKey(objExistingLoanRepayment.Loan_Application__c)) {
                        mapUniqueConstrainttoCount.put(objExistingLoanRepayment.Loan_Application__c, 1);
                    }
                    else {
                        //objLoanRepayment.addError('You are trying to create a duplicate record. A record already exists for the specified Loan Application.');
                        mapUniqueConstrainttoCount.put(objExistingLoanRepayment.Loan_Application__c,mapUniqueConstrainttoCount.get(objExistingLoanRepayment.Loan_Application__c)+1);
                    }
                }
            }
            for(Loan_Repayment__c objLoanRepayment : Trigger.new) {
                if(!mapUniqueConstrainttoCount.containsKey(objLoanRepayment.Loan_Application__c)) {
                    mapUniqueConstrainttoCount.put(objLoanRepayment.Loan_Application__c, 1);
                }
                else {
                    objLoanRepayment.addError('You are trying to create a duplicate record. A record already exists for the specified Loan Application.');
                    //mapUniqueConstrainttoCount.put(objLoanRepayment.Loan_Application__c,mapUniqueConstrainttoCount.get(objLoanRepayment.Loan_Application__c)+1);
                }
            }
        }
        else {
            lstExistingLoanRepayment = [Select Id, Name, Loan_Application__c from Loan_Repayment__c WHERE Loan_Application__c =: Trigger.new[0].Loan_Application__c];
            if(!lstExistingLoanRepayment.isEmpty()) {
                Trigger.new[0].addError('You are trying to create a duplicate record. A record already exists for the specified Loan Application.');
            }
        }
    }
}