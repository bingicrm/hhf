public class ContentDocumentLinkTriggerHelper {
	
    public static void updateFCUDoneInDocChecklist(Set<Id> setDocIds){
        List<Document_Checklist__c> lstDocChecklist = [SELECT Id,Name,FCU_Done__c,Exempted_from_FCU_BRD__c from Document_Checklist__c WHERE Id IN: setDocIds]; //Exempted_from_FCU_BRD__c added by Abhilekh on 18th December 2019 for FCU BRD Exemption
        List<Document_Checklist__c> lstDocChecklistToUpdate = new List<Document_Checklist__c>();
        
        for(Document_Checklist__c dc: lstDocChecklist){
            if(dc.FCU_Done__c == true && dc.Exempted_from_FCU_BRD__c == false){ //&& dc.Exempted_from_FCU_BRD__c == false added by Abhilekh on 18th December 2019 for FCU BRD Exemption
                dc.FCU_Done__c = false;
                lstDocChecklistToUpdate.add(dc);
            }
        }
        
        if(lstDocChecklistToUpdate.size() > 0){
            update lstDocChecklistToUpdate;
        }
    }
    
    public static void updateFCUReportUploadedInTPV(Set<Id> setTPVIds){
        List<Third_Party_Verification__c> lstTPV = [SELECT Id,Name,FCU_Report_Uploaded__c,RecordType.Name,Exempted_from_FCU_BRD__c ,Verification_Type__c from Third_Party_Verification__c WHERE Id IN: setTPVIds]; //Exempted_from_FCU_BRD__c added by Abhilekh on 18th December 2019 for FCU BRD Exemption
		//Verification_Type__c added by Saumya on 31st August 2020 for IMGC BRD						
        List<Third_Party_Verification__c> lstTPVToUpdate = new List<Third_Party_Verification__c>();
        
        for(Third_Party_Verification__c tpv: lstTPV){
            if((tpv.FCU_Report_Uploaded__c == false && tpv.RecordType.Name == Constants.FCU && tpv.Exempted_from_FCU_BRD__c == false) || (tpv.RecordType.Name == Constants.Others_RECORD && tpv.Verification_Type__c == Constants.IMGC)){ //&& tpv.Exempted_from_FCU_BRD__c == false added by Abhilekh on 18th December 2019 for FCU BRD Exemption
                tpv.FCU_Report_Uploaded__c = true;
                lstTPVToUpdate.add(tpv);
            }
        }
        
        if(lstTPVToUpdate.size() > 0){
            update lstTPVToUpdate;
        }
    }
}