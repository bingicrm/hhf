@isTest
public class TestGetRepaymentSchedulerController {
   
    public static testMethod void method1()
    {

         Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='9923453331',
                                  Date_of_Incorporation__c=Date.newInstance(2018,11,11));
         
         Database.insert(Cob);                
                            
         Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                     Scheme_ID__c=11,Product_Code__c='CF');
                            
         Database.insert(sc); 
        
         Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,
                                                           StageName__c='Customer Onboarding',
                                                           Transaction_type__c='PB',Requested_Amount__c=1000,
                                                           Applicant_Customer_segment__c='Salaried',
                                                           Branch__c='test',Line_Of_Business__c='Open Market',
                                                           Loan_Purpose__c='20',Sub_Stage__c='Application Initiation',
                                                           Requested_EMI_amount__c=100,Approved_ROI__c=10,
                                                           Requested_Loan_Tenure__c=5);
        
        Database.insert(LAob);
        String LoanApplicationID=String.valueOf(LAob.id);
        
        Loan_Application__c LAob2= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,
                                                           StageName__c='Customer Onboarding',
                                                           Transaction_type__c='PB',Requested_Amount__c=1000,
                                                           Applicant_Customer_segment__c='Salaried',
                                                           Branch__c='test',Line_Of_Business__c='Open Market',
                                                           Loan_Purpose__c='20',Sub_Stage__c='Application Initiation',
                                                           Requested_EMI_amount__c=100,Approved_ROI__c=10,
                                                           Requested_Loan_Tenure__c=5);
        
        Database.insert(LAob2);
        String LoanApplicationID2=String.valueOf(LAob2.id);
        
        
        Date duedate=date.today();
        duedate=duedate+20;
        Loan_Repayment__c lr=new Loan_Repayment__c(Moratorium_Flag__c = 'No',
                                                    Rate_Type__c = 'EMI',
                                                    Broken_Period_Interest_Handing__c = 'Add to Schedule',
                                                    Installment_Plan__c = 'Equated Instl',
                                                    Moratorium_period__c = 0,
                                                    Advanced_Installment_Flag__c = 'Charge upfront (reduce opening principal)',
                                                    Number_of_Advance_Installment__c = 0,
                                                    RepayType__c = 'Structured',
                                                    Repayment_Drawn_On__c = 'Sanctioned Amount',
                                                    Installment_Mode__c = 'Arrear',
                                                    Disbursement_Amount__c =100000,
                                                    Sanction_Amount__c =1000,
                                                    EMI_Amount__c = 0,
                                                    Interest_StartDate__c =Date.newInstance(2018,11,10),
                                                    Interest_Rate__c = 5,
                                                    Tenure__c = 3,
                                                    Interest_Round_off_parameter__c = 0,
                                                    Instalment_Round_off_parameter__c = 0,
                                                    IRR_calculation_parameters__c = String.valueOf('0'),
                                                    EMI_Start_date__c =Date.newInstance(2018,11,20),
                                                    Repayment_effective_date__c =Date.newInstance(2018,12,01),
                                                    Days_Per_Year__c = 'ACT',
                                                    Loan_Application__c = LAob.Id,
                                                    dueDay__c =5,
                                                    Baloon_Amount__c = 0);
    
        Database.insert(lr);
        String parentIdlr=String.valueOf(lr.id);
        lr.Disbursal_Date__c=lr.EMI_Start_date__c+10;
        Database.update(lr);
        
        
        Graded_Details__c GDob=new Graded_Details__c(Loan_Repayment__c=lr.id);
        Database.insert(GDob);
        
        
        
                  
        Scheme_Due_Date__c SDDob= new Scheme_Due_Date__c(Scheme__c=sc.id,Due_Date__c=5,Due_Date_Code__c='TST');
        Database.insert(SDDob);
        
        
        test.startTest();
        String returnedValue=GetRepaymentSchedulerController.saveRepayment(LAob,lr);
        Map<String,String> mapreturnedValue= GetRepaymentSchedulerController.getDueDayMaster(LoanApplicationID);
        String returnedvalue2= GetRepaymentSchedulerController.getSchedule(parentIdlr,LoanApplicationID);
        Date disbaledate=date.today()+25;
        Date reurnedvalueDate=GetRepaymentSchedulerController.getDate(1,disbaledate);
        Loan_Repayment__c returnedObject= GetRepaymentSchedulerController.getRepaymentRecord(LoanApplicationID);
        
        Loan_Repayment__c returnedObject2= GetRepaymentSchedulerController.getRepaymentRecord(LoanApplicationID2);
        Loan_Repayment__c returnedObject3= GetRepaymentSchedulerController.getRepaymentRecord('');
        
        test.stopTest();
    }
    
    
    public static testMethod void method2()
    {
        /* User u=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId 
                       from user where profile.Name='DSA' and isActive=true limit 1];*/
        
         Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='9992345333',
                                  Date_of_Incorporation__c=Date.newInstance(2018,11,11));
         
         Database.insert(Cob);                
                            
         Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                     Scheme_ID__c=11,Product_Code__c='CF');
                            
         Database.insert(sc); 
        
         Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,
                                                           StageName__c='Customer Onboarding',
                                                           Transaction_type__c='PB',Requested_Amount__c=1000,
                                                           Applicant_Customer_segment__c='Salaried',
                                                           Branch__c='test',Line_Of_Business__c='Open Market',
                                                           Loan_Purpose__c='20',Sub_Stage__c='Application Initiation',
                                                           Requested_EMI_amount__c=100,Approved_ROI__c=10,
                                                           Requested_Loan_Tenure__c=5);
        
        Database.insert(LAob);
        String LoanApplicationID=String.valueOf(LAob.id);
        
        Loan_Application__c LAob2= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,
                                                           StageName__c='Customer Onboarding',
                                                           Transaction_type__c='PB',Requested_Amount__c=1000,
                                                           Applicant_Customer_segment__c='Salaried',
                                                           Branch__c='test',Line_Of_Business__c='Open Market',
                                                           Loan_Purpose__c='20',Sub_Stage__c='Application Initiation',
                                                           Requested_EMI_amount__c=100,Approved_ROI__c=10,
                                                           Requested_Loan_Tenure__c=5);
        
        Database.insert(LAob2);
        String LoanApplicationID2=String.valueOf(LAob2.id);
        
        
        Date duedate=date.today();
        duedate=duedate+20;
        Loan_Repayment__c lr=new Loan_Repayment__c(Moratorium_Flag__c = 'No',
                                                    Rate_Type__c = 'EMI',
                                                    Broken_Period_Interest_Handing__c = 'Add to Schedule',
                                                    Installment_Plan__c = 'Equated Instl',
                                                    Moratorium_period__c = 0,
                                                    Advanced_Installment_Flag__c = 'Charge upfront (reduce opening principal)',
                                                    Number_of_Advance_Installment__c = 0,
                                                    RepayType__c = 'Structured',
                                                    Repayment_Drawn_On__c = 'Sanctioned Amount',
                                                    Installment_Mode__c = 'Arrear',
                                                    Disbursement_Amount__c =100000,
                                                    Sanction_Amount__c =1000,
                                                    EMI_Amount__c = 0,
                                                    Interest_StartDate__c =Date.newInstance(2018,11,10),
                                                    Interest_Rate__c = 5,
                                                    Tenure__c = 3,
                                                    Interest_Round_off_parameter__c = 0,
                                                    Instalment_Round_off_parameter__c = 0,
                                                    IRR_calculation_parameters__c = String.valueOf('0'),
                                                    EMI_Start_date__c =Date.newInstance(2018,11,20),
                                                    Repayment_effective_date__c =Date.newInstance(2018,12,01),
                                                    Days_Per_Year__c = 'ACT',
                                                    Loan_Application__c = LAob.Id,
                                                    dueDay__c =5,
                                                    Baloon_Amount__c = 0);
    
        Database.insert(lr);
        String parentIdlr=String.valueOf(lr.id);
        lr.Disbursal_Date__c=lr.EMI_Start_date__c+10;
        Database.update(lr);
        
        
        Graded_Details__c GDob=new Graded_Details__c();
        List<Graded_Details__c> listgrade= new List<Graded_Details__c>();
        listgrade.add(GDob);
        
                  
                
        Scheme_Due_Date__c SDDob= new Scheme_Due_Date__c(Scheme__c=sc.id,Due_Date__c=5,Due_Date_Code__c='TST');
        Database.insert(SDDob);
        
        LMSRepaymentSchedule.RepaymentResponse ty= new LMSRepaymentSchedule.RepaymentResponse();
        ty.ClosingPrinciple=12.4567;
        ty.Days='1';
        ty.Principal='12';
        ty.CustomerIRR='10';
        ty.InstlNo='10';
        //public String CustomerIRR;
        ty.BusinessIRR='10';
        //public String InstlNo;
        ty.OpeningPrinciple=10.1;
        ty.DueDate= date.today().format();
        ty.InstAmt='10';
        ty.AdvanceInstAmt='100';
        ty.BrokenPeriodInt='1';
        ty.Interest='1';
        ty.ClosingPrinciple=10.1;
        ty.Days='1';
        ty.ServiceTax='1';
        ty.ServiceTaxRate='1';
        ty.IntRate='1';
        ty.AdvncFlag='1';
        ty.DaysPerYear='1';
        ty.Tds='1';
        ty.Dsrate='1';
        ty.Excess_interest='1';
        ty.Principal='1';
        ty.CustomerIRR='10';
        
        test.startTest();
        GetRepaymentSchedulerController.insertGradedDetails(listgrade,parentIdlr);
        LMSRepaymentSchedule.getRepayment(ty,lr.id, LAob2.id);
        test.stopTest();
     }
     

}