public class TrancheGapAmountController {

    @AuraEnabled 
    public static Tranche__c getTrancheRec(Id trancheId){   
        return [SELECT Id, Loan_Application__c, Loan_Application__r.Loan_Number__c, Approved_Disbursal_Amount__c 
                FROM Tranche__c
                WHERE Id = :trancheId ];      
    }
    
    @AuraEnabled
    public static String makeAPICallout(Id tranID){
        Id loanAppId;
        String msg;
        DateTime dt = LMSDateUtility.lmsDateValue;
        List<LMS_Date_Sync__c> lmsDate = new List<LMS_Date_Sync__c>();
        msg = '';
        String monthName = dt.format('MMMMM');
        monthName = monthName.substring(0,3);
        Integer sday = dt.day();
        Integer syear = dt.year();
        String disbDt = String.valueOf(sday)+'-'+monthName+'-'+String.valueOf(syear);

        lmsDate = [SELECT Current_Date__c FROM LMS_Date_Sync__c ORDER BY Name DESC LIMIT 1];
        Rest_Service__mdt serviceDetails=[SELECT Client_Password__c, Client_Username__c, Request_Method__c, Time_Out_Period__c, Service_EndPoint__c 
                                          FROM Rest_Service__mdt 
                                          WHERE MasterLabel='TrancheGap'];
        Tranche__c tr = [SELECT Id, Loan_Application__c, Disbursal_Sequence__c, Loan_Application__r.Loan_Number__c, Approved_Disbursal_Amount__c
                         FROM Tranche__c
                         WHERE Id =: tranID];
        loanAppId = tr.Loan_Application__c; 
        Loan_Repayment__c loanRepay = [SELECT Id, Broken_Period_Interest_Handing__c, Loan_Application__c FROM Loan_Repayment__c WHERE Loan_Application__c =: loanAppId]; 
        if(loanRepay.Broken_Period_Interest_Handing__c == 'Return as charge'){
            if(serviceDetails != null && tr != null){
                Map<String,String> headerMap = new Map<String,String>();
                
                
                Blob headerValue = Blob.valueOf(serviceDetails.Client_Username__c + ':' + serviceDetails.Client_Password__c);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                headerMap.put('Content-Type','application/json');
                headerMap.put('Authorization',authorizationHeader);
                headerMap.put('Username',serviceDetails.Client_Username__c);
                headerMap.put('Password',serviceDetails.Client_Password__c);
                
                TrancheGapWrapper req = new TrancheGapWrapper(); 
                req.agreementId = tr.Loan_Application__r.Loan_Number__c;
                req.disbursalNo = String.valueOf(tr.Disbursal_Sequence__c);
                req.disbursedDate = disbDt;
                req.disbursedAmt = String.valueOf(tr.Approved_Disbursal_Amount__c);
                
                String jsonRequest = Json.serialize(req,true);

                TrancheGapResponseWrapper resp = new TrancheGapResponseWrapper();
                HttpResponse res   = Utility.sendRequest(serviceDetails.Service_EndPoint__c,serviceDetails.Request_Method__c, jsonRequest , headerMap, Integer.valueOf(serviceDetails.Time_Out_Period__c) );
                if( res != null && res.getStatusCode() == 200 ){
                    resp = (TrancheGapResponseWrapper)JSON.deserialize(res.getBody(), TrancheGapResponseWrapper.class);
                    Tranche__c tran = [SELECT Id, PEMI_Amount__c,Amount_Changed__c, Business_Date_PEMI__c FROM Tranche__c WHERE Id =: tranId];
                    system.debug('<<<<>>>>>'+resp.o_success_flag);
                    if(resp.o_success_flag == 'Y'){
                        system.debug('Flag Y');
                        tran.PEMI_Amount__c = Decimal.valueOf(resp.o_gap_interest);
                        tran.Business_Date_PEMI__c = Date.valueOf(dt);
                        tran.Amount_Changed__c = false;
                        update tran;
                    }
                    else if(resp.o_success_flag == 'N'){
                        system.debug('Flag N');
                        msg = String.valueOf(resp.o_error_msg);
                        tran.PEMI_Amount__c = 0;
                        tran.Business_Date_PEMI__c = Date.valueOf(dt);
                        tran.Amount_Changed__c = false;
                        update tran;
                    }
                    
                }
                else{
                    msg = 'Contact System Administrator.';
                }
            }
        }
        else{
            Tranche__c tran = [SELECT Id, PEMI_Amount__c,Amount_Changed__c, Business_Date_PEMI__c FROM Tranche__c WHERE Id =: tranId];
            tran.PEMI_Amount__c = 0;
            tran.Business_Date_PEMI__c = Date.valueOf(dt);
            tran.Amount_Changed__c = false;
            msg = 'This operation is not allowed as the Broken Period Handling was added to Schedule.';
            update tran;
        }
        return msg;
    }

    public class TrancheGapWrapper{
        public string agreementId {get; set;}
        public string disbursalNo {get; set;}
        public string disbursedDate {get; set;}
        public String disbursedAmt {get; set;}    
        
    }
    public class TrancheGapResponseWrapper{
        public string o_gapdays {get; set;}
        public string o_gap_interest {get; set;}
        public string o_gapdtl {get; set;}
        public String o_error_code {get; set;}    
        public string o_error_msg {get; set;}
        public String o_success_flag {get; set;}
    }
}