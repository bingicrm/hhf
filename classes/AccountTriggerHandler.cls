/*=====================================================================
* Deloitte India
* Name:AccountTriggerHandler
* Description: This class is a handler class and supports Lead trigger.
* Created Date: [22/06/2018]
* Created By:Sonali Kansal(Deloitte India)
*
* Date Modified                Modified By                  Description of the update
* []                              []                             []
=====================================================================*/

public class AccountTriggerHandler {
    public static void afterInsert(List < account > lstIncentiveAchievement) {}
    public static void beforeInsert(List <account> newList, Map <Id, account> newMap) {
        Date dt = LMSDateUtility.lmsDateValue;
        for(Account acc : newList){
            
            acc.Business_Date__c = (Date) dt;
        }
        nameToUpperCase(newList, null);
    }
    public static void beforeUpdate(List<account> newList, Map<Id,account> oldMap) {
        AccountTriggerHelper.checkPanValidated(newList, oldMap);
        nameToUpperCase(newList, oldMap);
        updatePhoneEmailAndMobile(newList, oldMap);
        AccountTriggerHelper.retriggerCorpCIBIL(newList, oldMap);
		AccountTriggerHelper.retriggerHunter(newList, oldMap); //Added by Abhilekh on 6th February 2020 for TIL-1942
    }
    public static void afterUpdate(Map < Id, account > newMap, Map < Id, account > oldMap) {}
    public static void beforeDelete(Map < Id, account > oldMap) {}
    public static void afterDelete(Map < Id, account > oldMap) {}
    
    private static void nameToUpperCase(List<Account> lstOfAccounts, Map<Id, Account> oldMap) {
        for(Account acc : lstOfAccounts){
            if(acc.IsPersonAccount) {
                if(acc.FirstName != null && (oldMap == null || !oldMap.containsKey(acc.Id) || acc.FirstName != oldMap.get(acc.Id).FirstName)) {
                    String fName = acc.FirstName;
                    acc.FirstName = fName.toUpperCase();
                }
                if(acc.MiddleName != null && (oldMap == null || !oldMap.containsKey(acc.Id) || acc.MiddleName != oldMap.get(acc.Id).MiddleName)){
                    String mName = acc.MiddleName;
                    acc.MiddleName = mName.toUpperCase();
                }
                if(acc.LastName != null && (oldMap == null || !oldMap.containsKey(acc.Id) || acc.LastName != oldMap.get(acc.Id).LastName)){
                    String lName = acc.LastName;
                    acc.LastName = lName.toUpperCase();
                }
                if(acc.PAN__c != null && (oldMap == null || !oldMap.containsKey(acc.Id) || acc.PAN__c != oldMap.get(acc.Id).PAN__c)) {
                    String panNum = acc.PAN__c;
                    acc.PAN__c = panNum.toUpperCase();
                }
                if( acc.Father_Husband_Name__c != null && (oldMap == null || !oldMap.containsKey(acc.Id) || acc.Father_Husband_Name__c != oldMap.get(acc.Id).Father_Husband_Name__c)) {
                    String FHName = acc.Father_Husband_Name__c;
                    acc.Father_Husband_Name__c = FHName.toUpperCase();
                }
            }
            else{
                if(acc.Name != null && (oldMap == null || !oldMap.containsKey(acc.Id) || acc.Name != oldMap.get(acc.Id).Name)) {
                    String accName = acc.Name;
                    acc.Name = accName.toUpperCase();
                }
                if(acc.PAN__c != null && (oldMap == null || !oldMap.containsKey(acc.Id) || acc.PAN__c != oldMap.get(acc.Id).PAN__c)) {
                    String panNumcust = acc.PAN__c; 
                    acc.PAN__c =  panNumcust.toUpperCase();
                }
            }
        }
    }

    private static void updatePhoneEmailAndMobile(List<Account> lstOfAccounts, Map<Id, Account> oldMap) {
        for(Account acc : lstOfAccounts) {
            if(acc.IsPersonAccount) {
                if(acc.Mobile__c != null && oldMap != null && oldMap.containsKey(acc.Id) && oldMap.get(acc.Id).Mobile__c != acc.Mobile__c) {
                    acc.PersonMobilePhone = acc.Mobile__c;
                    acc.Mobile__c = null;
                }
                if(acc.Email__c != null && String.isNotBlank(acc.Email__c) && oldMap != null && oldMap.containsKey(acc.Id) && oldMap.get(acc.Id).Email__c != acc.Email__c) {
                    acc.PersonEmail = acc.Email__c;
                    acc.Email__c = null;
                }
            } else {
                if(acc.Phone__c != null && oldMap != null && oldMap.containsKey(acc.Id) && oldMap.get(acc.Id).Phone__c != acc.Phone__c) {
                    acc.Phone = acc.Phone__c;
                    acc.Phone__c = null;
                }
            }
        }
    }
}