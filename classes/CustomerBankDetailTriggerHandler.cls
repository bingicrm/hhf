/*=====================================================================
 * Deloitte India
 * Name: CustomerBankDetailTriggerHandler
 * Description: This is the trigger handler for CustomerBankDetailTrigger
 * Created Date: [17/02/2020]
 * Created By: Abhilekh Anand(Deloitte India)
 *
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/
public class CustomerBankDetailTriggerHandler {
	
    public static void beforeUpdate(List<Bank_Detail__c> newList, Map<Id,Bank_Detail__c> oldMap) {
        Map<Id,Hunter_Required_Field__mdt> mapHunterFields= new Map<Id,Hunter_Required_Field__mdt>([Select Id, Field__c, Object__c, Critical_Field__c from Hunter_Required_Field__mdt where Object__c = 'Bank_Detail__c' and Critical_Field__c = true]);
        
        for(Bank_Detail__c bd: newList){
            for(Hunter_Required_Field__mdt objHunterFields: mapHunterFields.values()){
                if(bd.get(objHunterFields.Field__c) != oldMap.get(bd.Id).get(objHunterFields.Field__c)){
                    bd.Retrigger_Hunter__c = true;
                    break;
                }
            }
        }
    }
}