public class UCICDatabaseTriggerHelper {
    public static void updateLA(List<UCIC_Database__c> UCICDBList){
    List<Id> lstla = new List<Id>();
        for(UCIC_Database__c objDB: UCICDBList){
            lstla.add(objDB.Loan_Application__c);
        }
        List<Loan_Application__c> lstlatoUpdate = new List<Loan_Application__c>();
            for(Loan_Application__c objla:[Select Id, Overall_UCIC_Decision__c from Loan_Application__c where Id IN: lstla and Overall_UCIC_Decision__c != null and Overall_UCIC_Decision__c != '' ])  {
               objla.Overall_UCIC_Decision__c = '';
               lstlatoUpdate.add(objla) ;  
            } 
        if(lstlatoUpdate.size() > 0){
            update lstlatoUpdate;
        }
    }
 
}