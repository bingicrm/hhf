public class HunterCorporateResponseBody{
	public String path;	///online-match-sme
	public Integer statusCode;	//200
	public cls_payload payload;
	public String timestamp;	//2019-10-07 18:39:51.059
	public String acknowledgementId;	//5d9b391aab58d00fdfc793fd
	public class cls_payload {
		public cls_resultBlockObject resultBlockObject;
	}
	public class cls_resultBlockObject {
		public cls_matchSummary matchSummary;
		public cls_errorWarnings errorWarnings;
	}
	public class cls_matchSummary {
		public String matches;	//1
		public cls_totalMatchScore totalMatchScore;
		public cls_rules rules;
		public cls_matchSchemes matchSchemes;
	}
	public class cls_totalMatchScore {
		public String totalScore;	//1330
	}
	public class cls_rules {
		public Integer totalRuleCount;	//19
		public cls_rule[] rule;
	}
	public class cls_rule {
		public Integer ruleCount;	//19
		public String isGlobal;	//1
		public cls_ruleID ruleID;
		public cls_score score;
	}
	public class cls_ruleID {
		public String ruleID;	//NH_INCS_PAN_PPC
	}
	public class cls_score {
		public Integer score;	//1330
	}
	public class cls_matchSchemes {
		public Integer schemeCount;	//1
		public cls_scheme scheme;
	}
	public class cls_scheme {
		public cls_schemeID schemeID;
		public cls_score score;
	}
	public class cls_schemeID {
		public Integer schemeId;	//318
	}
	public class cls_errorWarnings {
		public cls_warnings warnings;
	}
	public class cls_warnings {
		public Integer warningCount;	//1
		public cls_warning warning;
	}
	public class cls_warning {
		public cls_number numr;
		public cls_message message;
		public cls_values values;
	}
	public class cls_number {
		public Integer num;	//102009
	}
	public class cls_message {
		public String message;	//Match schemes not used
	}
	public class cls_values {
		public cls_value[] value;
	}
	public class cls_value {
		public Integer value;	//28
	}
	public static HunterCorporateResponseBody parse(String json){
		return (HunterCorporateResponseBody) System.JSON.deserialize(json, HunterCorporateResponseBody.class);
	}
}