public class PerfiosITRStatementRetrieveResponseBody{
    public cls_itrvDetails[] itrvDetails;
    public class cls_itrvDetails {
        public String acknowledgementNumber;    //727879840070718
        public String assessmentYear;   //2018-19
        public String dateOfSubmission; //2018-07-07
        public String designationOfAO;  //WARD 2, PRODATTUR
        public String eFilingStatus;    //ITR Processed
        public String financialYear;    //2017-18
        public String formNumber;   //ITR-1
        public cls_incomeDetail incomeDetail;
        public String originalOrRevised;    //ORIGINAL
        public cls_personalInfo personalInfo;
    }
    public class cls_incomeDetail {
        public String currentYearLoss;  //0
        public String deductionsUnderChapter6A; //165000
        public cls_exemptIncome exemptIncome;
        public String grossTotalIncome; //561661
        public String interestPayable;  //0
        public String netTaxPayable;    //0
        public String refund;   //190
        public String taxPayable;   //0
        public cls_taxesPaid taxesPaid;
        public String totalIncome;  //396660
        public String totalTaxAndInterestPayable;   //7553
    }
    public class cls_exemptIncome {
        public String agriculture;  //0
        public String others;   //81266
    }
    public class cls_taxesPaid {
        public String advanceTax;   //0
        public String selfAssessmentTax;    //0
        public String tcs;  //0
        public String tds;  //7745
        public String totalTaxPaid; //7745
    }
    public class cls_personalInfo {
        public String address;  //Jammalamadugu Kadapa Kadapa 516434
        public String adhaarNumber; //XXXX XXXX 5623
        public String name; //RAMESH BANDORIPALLI
        public String pan;  //AQTPB0690A
        public String status;   //Individual
    }
}