@isTest
    public class TestPersonalDiscussionTriggerHandler {
    
    public static Testmethod void method1(){
        List<Personal_Discussion__c> lpd=new List<Personal_Discussion__c>();
        
        Account acc=new Account();
    acc.name='TestAc12';
    acc.phone='9732567891';
    acc.PAN__c='fjauy1936e';
    //acc.recordtypeid='0120l000000HtAN';
    //acc.PersonEmail='abcefh@gmail.com';
    acc.Date_of_Incorporation__c=date.today();
    insert acc;
    
    System.debug(acc.id);
    
    Scheme__c sc=new Scheme__c(); 
    sc.name='TestScheme';
    sc.Scheme_Code__c='testcode';
    sc.Scheme_Group_ID__c='23131';
    sc.Product_Code__c='HL';
    insert sc;
    System.debug(sc.id);
    
    Loan_Application__c lap=new Loan_Application__c ();
    lap.Customer__c=acc.id;
    lap.Transaction_type__c='DAA';
    lap.Scheme__c=sc.id;
    lap.Approved_Loan_Amount__c=5000000;
    lap.transaction_type__c='PB';
    insert lap;
    System.debug(lap.id);
    
    
    
    
    Personal_Discussion__c pd=new Personal_Discussion__c();
    pd.PD_Status__c='Positive';
    pd.Type_Of_PD__c='Telephonic';
    pd.Loan_Application__c=lap.id;
        pd.Send_SMS__c=true;
    List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
    pd.Customer_Detail__c=lstofcon[0].id;
    pd.PD_Completed_On__c=date.today();
    insert pd;
    lpd.add(pd);
    System.debug(pd);
    
    //PersonalDiscussionTriggerHandler pdth=new PersonalDiscussionTriggerHandler();
        PersonalDiscussionTriggerHandler.afterInsert(lpd);
        
        
    }
    
    public static Testmethod void method2(){
        
        //user 
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u1 = new User(
                     ProfileId =p.id,
                     MobilePhone='9232567891',
                     LastName = 'last',
                     Email = 'puser001@amamama.com',
                     Username = 'puser001@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
                     UserRoleId = r.id);
        insert u1;
    
        System.runAs(u1){
        //Test with Person Account
        List<Personal_Discussion__c> lpd=new List<Personal_Discussion__c>();
        Account acc=new Account();
         id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
    //acc.name='TestAccoun123t12';
    acc.phone='9232567891';
        acc.PersonMobilePhone='9232567891';
    acc.PAN__c='fjauy1936e';
    acc.recordtypeid=recTypeId;
    acc.PersonEmail='abcefh@gmail.com';
    acc.Date_of_Incorporation__c=date.today();
    acc.Firstname='testname';
    acc.Lastname='testlastname';
    acc.Salutation='Mr.';
    insert acc;
    System.debug(acc.id);
    
    Scheme__c sc=new Scheme__c();
    sc.name='TestScheme';
    sc.Scheme_Code__c='testcode';
    sc.Scheme_Group_ID__c='23131';
    sc.Product_Code__c='HL';
    insert sc;
    System.debug(sc.id);
    
    Loan_Application__c lap=new Loan_Application__c ();
    lap.Customer__c=acc.id;
    lap.Transaction_type__c='DAA';
    lap.Scheme__c=sc.id;
    lap.Approved_Loan_Amount__c=5000000;
    lap.Credit_Manager_User__c=u1.id;
    lap.transaction_type__c='PB';
    insert lap;
    System.debug(lap.id);
    
    
    
    
    Personal_Discussion__c pd=new Personal_Discussion__c();
    pd.PD_Status__c='Positive';
    pd.Type_Of_PD__c='Telephonic';
    pd.Loan_Application__c=lap.id;
    pd.Send_SMS__c=true;
    List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
    pd.Customer_Detail__c=lstofcon[0].id;
    pd.PD_Initiated_On__c=date.today();
    pd.PD_Completed_On__c=date.today();
    //pd.PD_Initiated_On__c=date.today();
    insert pd;
    lpd.add(pd);
    System.debug(pd);
    
    //PersonalDiscussionTriggerHandler pdth=new PersonalDiscussionTriggerHandler();
        PersonalDiscussionTriggerHandler.afterInsert(lpd);
        PersonalDiscussionTriggerHandler.beforeInsert(lpd);
        
        }
    }
   public static Testmethod void method3(){
        
        //user 
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u1 = new User(
                     ProfileId =p.id,
                     MobilePhone='9232567891',
                     LastName = 'last',
                     Email = 'puser001@amamama.com',
                     Username = 'puser001@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
                     UserRoleId = r.id);
        insert u1;
    
        System.runAs(u1){
        //Test with Person Account
        List<Personal_Discussion__c> lpd=new List<Personal_Discussion__c>();
        Account acc=new Account();
         id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();

    acc.phone='9292567891';
    acc.PersonMobilePhone='9292567891';
    acc.PAN__c='byyui5678e';
    acc.recordtypeid=recTypeId;
    acc.PersonEmail='ab6cfh@gmail.com';
    acc.Date_of_Incorporation__c=date.today();
    acc.Firstname='geetx';
    acc.Lastname='khnam';
    acc.Salutation='Mrs.';
    insert acc;
    System.debug(acc.id);
    
    Scheme__c sc=new Scheme__c();
    sc.name='TestScheme';
    sc.Scheme_Code__c='testcode';
    sc.Scheme_Group_ID__c='23131';
    sc.Product_Code__c='HL';
    insert sc;
    System.debug(sc.id);
    
    Loan_Application__c lap=new Loan_Application__c ();
    lap.Customer__c=acc.id;
    
    lap.Transaction_type__c='DAA';
    lap.Scheme__c=sc.id;
    lap.Approved_Loan_Amount__c=5000000;
    lap.Credit_Manager_User__c=u1.id;
    lap.transaction_type__c='PB';
    insert lap;
    System.debug(lap.id);
    
    Personal_Discussion__c pd=new Personal_Discussion__c();
    pd.PD_Status__c='Positive';
    pd.Type_Of_PD__c='Telephonic';
    pd.Loan_Application__c=lap.id;
    pd.Send_SMS__c=true;
    List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
    pd.Customer_Detail__c=lstofcon[0].id;
    pd.PD_Initiated_On__c=date.today();
    pd.PD_Completed_On__c=date.today();
    pd.reTriggerBRE2__c=false;
    insert pd;
    lpd.add(pd);
    pd.Business_Description__c='Yes';
    update pd;
    System.debug(pd);
        }
    }
   
    
    
    
    }