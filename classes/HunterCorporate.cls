public class HunterCorporate {
    
    @AuraEnabled
    public static String checkHunterDeclineAndFCUDecline(Id loanAppId){
        Loan_Application__c la = [SELECT Id,FCU_Decline_Count__c,Overall_FCU_Status__c,Exempted_from_FCU_BRD__c,Hunter_Decline_Count__c,Sub_Stage__c from Loan_Application__c WHERE Id =: loanAppId];
        
        UCIC_Negative_API__c ucg = UCIC_Negative_API__c.getInstance(); //Added by Abhilekh on 26th October 2020 for UCIC Phase II
        List<Customer_Integration__c> cust = new List<Customer_Integration__c>(); //Added by Abhilekh on 26th October 2020 for UCIC Phase II
        Id RecordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get(Constants.UCIC).getRecordTypeId(); //Added by Abhilekh on 26th October 2020 for UCIC Phase II
        
        cust = [SELECT Id, UCIC_Negative_API_Response__c, UCIC_Response_Status__c, Negative_UCIC_DB_Found__c FROM Customer_Integration__c WHERE RecordTypeId =: RecordTypeId AND Loan_Application__c =: la.Id ORDER BY CreatedDate DESC LIMIT 1]; //Added by Abhilekh on 26th October 2020 for UCIC Phase II
        
        if((la.Sub_Stage__c == Constants.Credit_Review || la.Sub_Stage__c == Constants.Re_Credit || la.Sub_Stage__c == Constants.Re_Look) && la.Overall_FCU_Status__c == Constants.NEGATIVE && la.FCU_Decline_Count__c > 0 && la.Exempted_from_FCU_BRD__c == false){
            return 'You cannot Initiate Hunter as this application is FCU Decline.';
        }
        else if((la.Sub_Stage__c == Constants.Credit_Review || la.Sub_Stage__c == Constants.Re_Credit || la.Sub_Stage__c == Constants.Re_Look) && la.Hunter_Decline_Count__c > 0){
            return 'You cannot Initiate Hunter as Hunter analysis has not been recommended. Please reject or assign the application to Hunter Manager.';
        }
        //Below else if block added by Abhilekh on 26th October 2020 for UCIC Phase II
        else if(cust.size() > 0 && cust[0].Negative_UCIC_DB_Found__c == true && ucg.UCIC_Active__c == true && (la.Sub_Stage__c == Constants.Credit_Review || la.Sub_Stage__c == Constants.Re_Credit || la.Sub_Stage__c == Constants.Re_Look)){
            return 'You cannot Initiate Hunter as Data Found in Negative Dedupe Database.';  
        }
        return '';
    }
    
    @AuraEnabled
    public static String checkCorporateHunterEligibility(Id loanAppId) {
        Loan_Application__c la = [SELECT Id,Branch__c,Assigned_Hunter_Manager__c,Hunter_Approval_Required__c from Loan_Application__c WHERE Id =: loanAppId]; //Added by Abhilekh on 5th February 2020 for TIL-1944
        
        List<Loan_Contact__c> lstCustDet = [SELECT Id, Name, Loan_Applications__r.Loan_Purpose__c, Borrower__c, Organization_Name__c,
                                            Industry__c,Constitution__c,TAN__c,Date_of_Incorporation__c,Company_Registration_No_CIN__c,Net_Annual_Income__c,
                                            Pan_Number__c,Customer__r.FirstName,Customer__r.MiddleName,Customer__r.LastName,Date_Of_Birth__c,Age__c,
                                            Gender__c,Qualification__c,Marital_Status__c,Total_Income__c,PP_Nationality__c,
                                            Relationship_Type_1__c, Relationship_Type_2__c, Corporate_Customer_1__r.Id, 
                                            Corporate_Customer_2__r.Id,Relationship_with_Applicant__c
                                            FROM Loan_Contact__c 
                                            where Loan_Applications__c =:loanAppId];
        
        Group hunterGroup = [SELECT Id,Name,(SELECT Id,UserOrGroupId from GroupMembers) from Group WHERE Name =: Constants.Hunter_Managers]; //Added by Abhilekh on 5th February 2020 for TIL-1944
        
        Set<Id> setUserIds = new Set<Id>(); //Added by Abhilekh on 5th February 2020 for TIL-1944
        
        //Below for loop added by Abhilekh on 5th February 2020 for TIL-1944
        for(GroupMember gm: hunterGroup.GroupMembers){
            if(String.valueOf(gm.UserOrGroupId).startsWith('005')){
            	setUserIds.add(gm.UserOrGroupId);
            }
        }
        
        List<User> lstUser = [SELECT Id,Name,Hunter__c,Role_Assigned_Date__c,No_Hunter_Assignment__c from User WHERE Id IN: setUserIds]; //Added by Abhilekh on 5th February 2020 for TIL-1944
        Id HunterRT = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('Hunter').getRecordTypeId(); //Added by Abhilekh on 5th February 2020 for TIL-1944
        
        List<Loan_Contact__c> lstOfCorpCust = new List<Loan_Contact__c>();  
        Set<Id> SetOfCorpCust = new Set<Id>();
        Set<Id> SetOfIndiCustifNoCorp = new Set<Id>();
        List<Id> ListOfIndiCustifNoCorp = new List<Id>();
        String strIndiCustifNoCorp = '';
        Set<Id> SetOfIndiCust = new Set<Id>();
        List<Id> ListOfIndiCust = new List<Id>();
        for(Loan_Contact__c objCD: lstCustDet){
            if(objCD.Borrower__c == '2'){
                lstOfCorpCust.add(objCD);
                SetOfCorpCust.add(objCD.Id);
            }
            else if(objCD.Borrower__c == '1'){
                SetOfIndiCustifNoCorp.add(objCD.Id);
                ListOfIndiCustifNoCorp.add(objCD.Id);
                strIndiCustifNoCorp = strIndiCustifNoCorp != null? strIndiCustifNoCorp+ ' - '+ objCD.Name : objCD.Name;
            }
        }
        if(lstOfCorpCust.size() > 0){
            if(lstOfCorpCust.size() > 2){
                /*Below code added by Abhilekh on 5th February 2020 for TIL-1944*/
                List<User> usersToUpdate = new List<User>();
                
                Third_Party_Verification__c tpv = new Third_Party_Verification__c();
                tpv.Loan_Application__c = la.Id;
                tpv.RecordTypeId = HunterRT;
                tpv.Status__c = Constants.INITIATED;
                tpv.Online_Hunter_Not_Applicatble__c = true;
                
                Id OwnerIdHunter;
                Datetime ladFCU = System.now();
                Datetime radFCU;
                
                for(User ub: lstUser){
                    if(ub.Hunter__c == TRUE && ub.No_Hunter_Assignment__c == false){
                        radFCU = ub.Role_Assigned_Date__c;
                        if(radFCU == null){
                            OwnerIdHunter = ub.Id;
                        }
                        else if(radFCU < ladFCU){
                            ladFCU = radFCU;
                            OwnerIdHunter = ub.Id;
                        }
                    }
                }
                
                if(la.Assigned_Hunter_Manager__c != null){
                    tpv.Owner__c = la.Assigned_Hunter_Manager__c;
                }
                else{
                    tpv.Owner__c = OwnerIdHunter;
                    
                    User u = new User();
                    u.Id = OwnerIdHunter;
                    u.Role_Assigned_Date__c = system.now();
                    usersToUpdate.add(u);
                    
                    la.Assigned_Hunter_Manager__c = OwnerIdHunter;
                }
                la.Hunter_Approval_Required__c = false;
                
                Loan_Application__Share laShare = new Loan_Application__Share();
                laShare.UserOrGroupId = hunterGroup.Id;
                laShare.AccessLevel = Constants.ACCESS_LEVEL_EDIT;
                laShare.RowCause = Schema.loan_application__Share.RowCause.ManualAccess__c;
                laShare.ParentId = la.Id;
                
                insert tpv;
                update la;
                insert laShare;
                
                if(usersToUpdate.size() > 0){
                    update usersToUpdate;
                }
                /*Above code added by Abhilekh on 5th February 2020 for TIL-1944*/
                
                return 'Online Hunter is not applicable for this application.';
            }
            Map<String, List<Loan_Contact__c>> mapofCorpwithPro_Co_Promoters = new Map<String, List<Loan_Contact__c>>();
            Map<String, Integer> mapofCorpwithPro_Co_PromoterCount = new Map<String, Integer>();
            Set<Id> setOfCorpCustomerwithProCoPro= new Set<Id>();
            for(Loan_Contact__c objCorpCD: lstOfCorpCust){
                for(Loan_Contact__c objCD: lstCustDet){
                    if((objCD.Relationship_Type_1__c == 'Promoter' || objCD.Relationship_Type_1__c == 'Co-Promoter') && objCD.Corporate_Customer_1__r.Id == objCorpCD.Id) {
                        string strCORPIdwithRelationshipId = objCorpCD.Id + ' - ' + objCD.Id;
                        if(objCD.Relationship_Type_1__c  == 'Promoter'){
                            if(mapofCorpwithPro_Co_Promoters.get(objCorpCD.Id) != null){
                                List<Loan_Contact__c> lstOfCD= new List<Loan_Contact__c>();
                                for(Loan_Contact__c objCon: mapofCorpwithPro_Co_Promoters.get(objCorpCD.Id)){
                                    lstOfCD.add(objCon);
                                }
                                lstOfCD.add(objCD);
                                setOfCorpCustomerwithProCoPro.add(objCD.Id);
                                setOfCorpCustomerwithProCoPro.add(objCorpCD.Id);
                                mapofCorpwithPro_Co_Promoters.put(objCorpCD.Id, lstOfCD);
                            }
                            else if(mapofCorpwithPro_Co_Promoters.get(objCorpCD.Id) == null){
                                List<Loan_Contact__c> lstOfCD= new List<Loan_Contact__c>();
                                lstOfCD.add(objCD);
                                setOfCorpCustomerwithProCoPro.add(objCD.Id);
                                setOfCorpCustomerwithProCoPro.add(objCorpCD.Id);
                                mapofCorpwithPro_Co_Promoters.put(objCorpCD.Id, lstOfCD);
                            }
                            /* if(lstOfPromoters.size() == 0){
lstOfPromoters.add(objCD);
}
else{
lstOfCoPromoters.add(objCD); 
}*/
                        }
                        else if(objCD.Relationship_Type_1__c  == 'Co-Promoter'){
                            if(mapofCorpwithPro_Co_Promoters.get(objCorpCD.Id) != null){
                                List<Loan_Contact__c> lstOfCD= new List<Loan_Contact__c>();
                                for(Loan_Contact__c objCon: mapofCorpwithPro_Co_Promoters.get(objCorpCD.Id)){
                                    lstOfCD.add(objCon);
                                }
                                lstOfCD.add(objCD);
                                setOfCorpCustomerwithProCoPro.add(objCD.Id);
                                setOfCorpCustomerwithProCoPro.add(objCorpCD.Id);
                                mapofCorpwithPro_Co_Promoters.put(objCorpCD.Id, lstOfCD);
                            }
                            else if(mapofCorpwithPro_Co_Promoters.get(objCorpCD.Id) == null){
                                List<Loan_Contact__c> lstOfCD= new List<Loan_Contact__c>();
                                lstOfCD.add(objCD);
                                setOfCorpCustomerwithProCoPro.add(objCD.Id);
                                setOfCorpCustomerwithProCoPro.add(objCorpCD.Id);
                                mapofCorpwithPro_Co_Promoters.put(objCorpCD.Id, lstOfCD);
                            }
                        }
                        
                        
                    }
                    else if((objCD.Relationship_Type_2__c == 'Promoter' || objCD.Relationship_Type_2__c == 'Co-Promoter') && objCD.Corporate_Customer_2__r.Id == objCorpCD.Id){
                        string strCORPIdwithRelationshipId = objCorpCD.Id + ' - ' + objCD.Id;
                        if(objCD.Relationship_Type_2__c  == 'Promoter'){
                            //mapofCorpwithPromoters.put(objCorpCD.Id, objCD);
                            /*if(lstOfPromoters.size() == 0){
lstOfPromoters.add(objCD);
Count2++;
}
else{
lstOfCoPromoters.add(objCD);    
Count2++;
}*/
                            if(mapofCorpwithPro_Co_Promoters.get(objCorpCD.Id) != null){
                                List<Loan_Contact__c> lstOfCD= new List<Loan_Contact__c>();
                                for(Loan_Contact__c objCon: mapofCorpwithPro_Co_Promoters.get(objCorpCD.Id)){
                                    lstOfCD.add(objCon);
                                }
                                lstOfCD.add(objCD);
                                setOfCorpCustomerwithProCoPro.add(objCD.Id);
                                setOfCorpCustomerwithProCoPro.add(objCorpCD.Id);
                                mapofCorpwithPro_Co_Promoters.put(objCorpCD.Id, lstOfCD);
                            }
                            else if(mapofCorpwithPro_Co_Promoters.get(objCorpCD.Id) == null){
                                List<Loan_Contact__c> lstOfCD= new List<Loan_Contact__c>();
                                lstOfCD.add(objCD);
                                setOfCorpCustomerwithProCoPro.add(objCD.Id);
                                setOfCorpCustomerwithProCoPro.add(objCorpCD.Id);
                                mapofCorpwithPro_Co_Promoters.put(objCorpCD.Id, lstOfCD);
                            }
                            
                        }
                        else if(objCD.Relationship_Type_2__c  == 'Co-Promoter'){
                            //mapofCorpwithCoPromoters.put(objCorpCD.Id, objCD);
                            //lstOfCoPromoters.add(objCD);
                            if(mapofCorpwithPro_Co_Promoters.get(objCorpCD.Id) != null){
                                List<Loan_Contact__c> lstOfCD= new List<Loan_Contact__c>();
                                for(Loan_Contact__c objCon: mapofCorpwithPro_Co_Promoters.get(objCorpCD.Id)){
                                    lstOfCD.add(objCon);
                                }
                                lstOfCD.add(objCD);
                                setOfCorpCustomerwithProCoPro.add(objCD.Id);
                                setOfCorpCustomerwithProCoPro.add(objCorpCD.Id);
                                mapofCorpwithPro_Co_Promoters.put(objCorpCD.Id, lstOfCD);
                            }
                            else if(mapofCorpwithPro_Co_Promoters.get(objCorpCD.Id) == null){
                                List<Loan_Contact__c> lstOfCD= new List<Loan_Contact__c>();
                                lstOfCD.add(objCD);
                                setOfCorpCustomerwithProCoPro.add(objCD.Id);
                                setOfCorpCustomerwithProCoPro.add(objCorpCD.Id);
                                mapofCorpwithPro_Co_Promoters.put(objCorpCD.Id, lstOfCD);
                            }
                        }
                        
                    }
                    
                }
            }
            
            for(Loan_Contact__c objCD: lstCustDet){
                if(!setOfCorpCustomerwithProCoPro.contains(objCD.Id)){
                    SetOfIndiCust.add(objCD.Id);
                    ListOfIndiCust.add(objCD.Id);
                }
            }
            
            integer totalCount= 0;
            Set<Id> setOFLC= new Set<Id>();
            for(Loan_Contact__c objCorpCust: lstOfCorpCust){
                if(mapofCorpwithPro_Co_Promoters.get(objCorpCust.Id) != null){
                    for(Loan_Contact__c objLC: mapofCorpwithPro_Co_Promoters.get(objCorpCust.Id)){
                    setOFLC.add(objLC.Id);
                    }
                   // mapofCorpwithPro_Co_PromoterCount.put(objCorpCust.Id, mapofCorpwithPro_Co_Promoters.get(objCorpCust.Id).size());
                   // totalCount = totalCount+ mapofCorpwithPro_Co_Promoters.get(objCorpCust.Id).size();
                }
            }
            totalCount = setOFLC.size();
            Boolean offlineHunterCheck = false;
            if(lstOfCorpCust.size() > 0){
                for(Loan_Contact__c objCorpCust: lstOfCorpCust){
                    if(mapofCorpwithPro_Co_PromoterCount.get(objCorpCust.Id) != null && mapofCorpwithPro_Co_PromoterCount.get(objCorpCust.Id) > 4){
                        offlineHunterCheck = true;
                    }
                }
                
                /*if(offlineHunterCheck == true){
                    //Below code added by Abhilekh on 5th February 2020 for TIL-1944
                    List<User> usersToUpdate = new List<User>();
                    
                    Third_Party_Verification__c tpv = new Third_Party_Verification__c();
                    tpv.Loan_Application__c = la.Id;
                    tpv.RecordTypeId = HunterRT;
                    tpv.Status__c = Constants.INITIATED;
                    tpv.Online_Hunter_Not_Applicatble__c = true;
                    
                    Id OwnerIdHunter;
                    Datetime ladFCU = System.now();
                    Datetime radFCU;
                    
                    for(User ub: lstUser){
                        if(ub.Hunter__c == TRUE && ub.No_Hunter_Assignment__c == false){
                            radFCU = ub.Role_Assigned_Date__c;
                            if(radFCU == null){
                                OwnerIdHunter = ub.Id;
                            }
                            else if(radFCU < ladFCU){
                                ladFCU = radFCU;
                                OwnerIdHunter = ub.Id;
                            }
                        }
                    }
                    
                    if(la.Assigned_Hunter_Manager__c != null){
                        tpv.Owner__c = la.Assigned_Hunter_Manager__c;
                    }
                    else{
                        tpv.Owner__c = OwnerIdHunter;
                        
                        User u = new User();
                        u.Id = OwnerIdHunter;
                        u.Role_Assigned_Date__c = system.now();
                        usersToUpdate.add(u);
                        
                        la.Assigned_Hunter_Manager__c = OwnerIdHunter;
                    }
                    la.Hunter_Approval_Required__c = false;
                    
                    Loan_Application__Share laShare = new Loan_Application__Share();
                    laShare.UserOrGroupId = hunterGroup.Id;
                    laShare.AccessLevel = Constants.ACCESS_LEVEL_EDIT;
                    laShare.RowCause = Schema.loan_application__Share.RowCause.ManualAccess__c;
                    laShare.ParentId = la.Id;
                    
                    insert tpv;
                    update la;
                    insert laShare;
                    
                    if(usersToUpdate.size() > 0){
                        update usersToUpdate;
                    }
                    //Above code added by Abhilekh on 5th February 2020 for TIL-1944
                    
                    return 'Online Hunter is not applicable for this application.'; 
                }*/
            }
            if(SetOfIndiCust.size() > 6){
                /*Below code added by Abhilekh on 5th February 2020 for TIL-1944*/
                List<User> usersToUpdate = new List<User>();
                
                Third_Party_Verification__c tpv = new Third_Party_Verification__c();
                tpv.Loan_Application__c = la.Id;
                tpv.RecordTypeId = HunterRT;
                tpv.Status__c = Constants.INITIATED;
                tpv.Online_Hunter_Not_Applicatble__c = true;
                
                Id OwnerIdHunter;
                Datetime ladFCU = System.now();
                Datetime radFCU;
                
                for(User ub: lstUser){
                    if(ub.Hunter__c == TRUE && ub.No_Hunter_Assignment__c == false){
                        radFCU = ub.Role_Assigned_Date__c;
                        if(radFCU == null){
                            OwnerIdHunter = ub.Id;
                        }
                        else if(radFCU < ladFCU){
                            ladFCU = radFCU;
                            OwnerIdHunter = ub.Id;
                        }
                    }
                }
                
                if(la.Assigned_Hunter_Manager__c != null){
                    tpv.Owner__c = la.Assigned_Hunter_Manager__c;
                }
                else{
                    tpv.Owner__c = OwnerIdHunter;
                    
                    User u = new User();
                    u.Id = OwnerIdHunter;
                    u.Role_Assigned_Date__c = system.now();
                    usersToUpdate.add(u);
                    
                    la.Assigned_Hunter_Manager__c = OwnerIdHunter;
                }
                la.Hunter_Approval_Required__c = false;
                
                Loan_Application__Share laShare = new Loan_Application__Share();
                laShare.UserOrGroupId = hunterGroup.Id;
                laShare.AccessLevel = Constants.ACCESS_LEVEL_EDIT;
                laShare.RowCause = Schema.loan_application__Share.RowCause.ManualAccess__c;
                laShare.ParentId = la.Id;
                
                insert tpv;
                update la;
                insert laShare;
                
                if(usersToUpdate.size() > 0){
                    update usersToUpdate;
                }
                /*Above code added by Abhilekh on 5th February 2020 for TIL-1944*/
                
                return 'Online Hunter is not applicable for this application.';
            }
            system.debug('totalCount::'+totalCount);
            //List<Customer_Integration__c> lstCIToInsert = new List<Customer_Integration__c>();
            if(totalCount > 4){
                Integer i = 0;
                for(Loan_Contact__c objLoanCon: lstOfCorpCust){
                    system.debug('Inside If::'+totalCount);
                    Customer_Integration__c integrations = new Customer_Integration__c();
                    integrations.Loan_Contact__c = lstOfCorpCust[i].Id;
                    integrations.Loan_Application__c = loanAppId;
                    integrations.recordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('Hunter Analysis').getRecordTypeId();
                    insert integrations;
                    i++;
                    //lstCIToInsert.add(integrations);
                    
                    Set<Id> setOfCorpId = new Set<Id>();   
                    setOfCorpId.add(objLoanCon.Id);
                    System.debug('First Corporate Callout=='+integrations.Id);
                    HunterCorporateIntegrationCallout.HunterCorporateIntegrationCallout(loanAppId,setOfCorpId,integrations.Id);
                }
                /**** Double ****/ 
                
                /*if(lstCIToInsert.size() > 0){
                    insert lstCIToInsert;
                }*/
            }
            else{
            system.debug('Inside Else::'+totalCount);
                Customer_Integration__c integrations = new Customer_Integration__c();
                integrations.Loan_Contact__c = lstOfCorpCust[0].Id;
                integrations.Loan_Application__c = loanAppId;
                integrations.recordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('Hunter Analysis').getRecordTypeId();
                insert integrations;
                System.debug('Second Corporate Callout=='+integrations.Id);
                HunterCorporateIntegrationCallout.HunterCorporateIntegrationCallout(loanAppId,SetOfCorpCust,integrations.Id);
            }
            
            
            system.debug('SetOfIndiCust::'+SetOfIndiCust);
            if(SetOfIndiCust.size()> 0){
            	system.debug('SetOfIndiCust I am getting called::'+SetOfIndiCust);
                Customer_Integration__c integrations = new Customer_Integration__c();
                integrations.Loan_Contact__c = ListOfIndiCust[0];
                integrations.Loan_Application__c = loanAppId;
                integrations.recordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('Hunter Analysis').getRecordTypeId();
                insert integrations;
            	system.debug('SetOfIndiCust after Integration'+integrations);
                System.debug('First Individual Callout=='+integrations.Id);
                HunterIndividualIntegrationCallout.makeHunterIndividualIntegrationCallout(loanAppId,SetOfIndiCust,integrations.Id);
            }
        }
        else if(SetOfIndiCustifNoCorp.size() > 0){
            system.debug('SetOfIndiCustifNoCorp I am getting called::'+SetOfIndiCustifNoCorp);
            Customer_Integration__c integrations = new Customer_Integration__c();
            integrations.Loan_Contact__c = ListOfIndiCustifNoCorp[0];
            integrations.Loan_Application__c = loanAppId;
            integrations.recordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('Hunter Analysis').getRecordTypeId();
            insert integrations;
            System.debug('Second Individual Callout=='+integrations.Id);
            HunterIndividualIntegrationCallout.makeHunterIndividualIntegrationCallout(loanAppId,SetOfIndiCustifNoCorp,integrations.Id);
        }
        return '';
    }
}