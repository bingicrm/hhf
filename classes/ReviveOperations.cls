public class ReviveOperations {
    
    @AuraEnabled
    public static Loan_Application__c getLoanApp(Id lappId){
        system.debug('=='+lappId);
         return [SELECT Id, Name, StageName__c, Sub_Stage__c, Rejected_Cancelled_Sub_Stage__c,Rejected_Cancelled_Stage__c,OwnerId/**** Added by Saumya For Reject Revive BRD *****/, Loan_Status__c/*** Added by Saumya for Loan Status ***/,Insurance_Loan_Application__c, Assigned_Sales_User__c, RecordTypeID, Date_of_Rejection__c, Date_of_Cancellation__c, Remarks_Revival__c,Rejection_Reason__c,Severity_Level__c,
				FCU_Decline_Count__c,Post_Sanction_FCU_Decline_Count__c,Moved_To_FCU_Review_From_Credit__c,Overall_FCU_Status__c,Reviewed_by_FCU_Manager__c,Overall_Post_Sanction_FCU_Status__c,Post_Sanction_FCU_Manager_Review__c,Exempted_from_FCU_BRD__c,Previous_Sub_Stage__c,
				(Select Id, Recordtype.DeveloperName, Recordtypeid, Applicant_Type__c /*** Added by Saumya for New UCIC II Enhancements ***/ from Loan_Contacts__r)           
				FROM Loan_Application__c
                WHERE Id = :lappId ]; //FCU_Decline_Count__c added by Abhilekh on 15th September 2019 for FCU BRD
									  //Post_Sanction_FCU_Decline_Count__c,Moved_To_FCU_Review_From_Credit__c,Overall_FCU_Status__c,Reviewed_by_FCU_Manager__c,Overall_Post_Sanction_FCU_Status__c,Post_Sanction_FCU_Manager_Review__c added by Abhilekh on 30th September 2019 for Post Sanction FCU BRD
									  //Exempted_from_FCU_BRD__c added by Abhilekh on 23rd December 2019 for FCU BRD Exemption
									  //Previous_Sub_Stage__c added by Abhilekh on 29th January 2020 for Hunter BRD
    }
    
    @AuraEnabled
    public static String reviveApplication(loan_application__c la){
        
        Savepoint sp = Database.setSavepoint();
        String msg;
        msg='';
       	List<Reject_Revive_Master__mdt> lstofRejectRevivemdt =[Select Id,Re_Open_Stage__c,Reopen__c,Re_Open_Sub_Stage__c,Revive_Stage__c,Revive_Sub_Stage__c,Cancelled_Reject_Stage__c from Reject_Revive_Master__mdt where IsActive__c=true and Cancelled_Reject_Stage__c =: la.Rejected_Cancelled_Sub_Stage__c];
        System.debug('lstofRejectRevivemdt:::'+lstofRejectRevivemdt);
        system.debug('UserInfo.getUserId()::'+UserInfo.getUserId()+'la.OwnerId::'+la.OwnerId);
		List<UCIC_Negative_Database__mdt> UCICdblst = [Select Id, Deviation__c, Reject_Data_Match_Count__c, Rejection_Reason__c, Severity_Level__c, UCIC_Database_Name__c from UCIC_Negative_Database__mdt where Active__c = true]; /*** Added by Saumya for New UCIC II Enhancements ***/
		LMS_Bus_Date__c BusinessDt = LMS_Bus_Date__c.getInstance();/*** Added by Saumya for New UCIC II Enhancements ***/
        UCIC_Global__c ucg = UCIC_Global__c.getInstance();/*** Added by Saumya for New UCIC II Enhancements ***/
		Customer_Integration__c cust = new Customer_Integration__c();/*** Added by Saumya for New UCIC II Enhancements ***/
        /*** Added by Saumya for New UCIC II Enhancements ***/
        if(ucg.UCIC_Active__c == true){
			Id RecordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get(Constants.UCIC).getRecordTypeId(); 
			Id primaryApplicant;
			for(Loan_Contact__c objCon: la.Loan_Contacts__r){
				if(objCon.Applicant_Type__c=='Applicant'){
					primaryApplicant =  objCon.Id; 
				}
				
			}
			system.debug('primaryApplicant '+primaryApplicant);
			List<Customer_Integration__c> lstcust = [SELECT Id, UCIC_Negative_API_Response__c, UCIC_Response_Status__c, Negative_UCIC_DB_Found__c FROM Customer_Integration__c WHERE RecordTypeId =: RecordTypeId AND Loan_Application__c =: la.Id  AND Loan_Contact__c =: primaryApplicant order by createdDate DESC LIMIT 1];
            if(lstcust.size() >0){
             cust = lstcust[0];   
            }
        }
        /*** Added by Saumya for New UCIC II Enhancements ***/
        try {
        if(Approval.isLocked(la) == true){
            msg = 'Application has already been submitted for Approval.';
        }
        else if(la.Severity_Level__c == '1'){
            msg = 'Revival is not allowed for application whose Severity Level is 1.';
        }
        else if(UserInfo.getUserId() != la.OwnerId && !Test.isRunningTest()){
            msg= 'You are not authorized for Revive.';
        }
        else if(la.Loan_Status__c == 'Cancelled'){ //Added By Saumya For Loan Status
            msg = 'Application with Cancelled Loan Status cannot be revived.';
        }
		else if(la.Insurance_Loan_Application__c == true){ //Added By Saumya For Insurance Loan App
            msg = 'Revival is not allowed for Insurance Loan application.';
        }
		/*** Added by Saumya for New UCIC II Enhancements ***/
		else if(cust.Negative_UCIC_DB_Found__c == true){
				msg = 'You can not revive this Application as Data Found in Negative UCIC Database.';	
			}
		/*** Added by Saumya for New UCIC II Enhancements ***/
        else{
            system.debug('@@la.Date_of_Cancellation__c' + la.Date_of_Cancellation__c);
            system.debug('@@la.Date_of_Rejection__c' + la.Date_of_Rejection__c);
            system.debug('@@la.Remarks_Revival__c' + la.Remarks_Revival__c);
            system.debug('@@la.' + la);
            if((la.Date_of_Cancellation__c != null && la.Date_of_Cancellation__c.daysBetween(System.Today()) < 30) || (la.Date_of_Rejection__c != null && la.Date_of_Rejection__c.daysBetween(System.Today()) < 30)){
                if(la.Remarks_Revival__c != NULL) {
                   /* if((la.Rejected_Cancelled_Stage__c == 'Credit Decisioning' || la.Rejected_Cancelled_Stage__c == 'Operation Control' || la.Rejected_Cancelled_Stage__c == 'Customer Onboarding' || (la.Rejected_Cancelled_Stage__c == 'Loan Rejected' && la.Rejected_Cancelled_Sub_Stage__c =='CIBIL Reject') || la.Rejected_Cancelled_Sub_Stage__c == 'Hunter Review')/* && la.SUD_count__c == 0*//*){// Added by Saumya For Reject Revive BRD
                        //la.Rejected_Cancelled_Sub_Stage__c == 'Hunter Review' added by Abhilekh on 29th January 2020 for Hunter BRD
                        for(Reject_Revive_Master__mdt objrejectRevive: lstofRejectRevivemdt){
                            if(objrejectRevive.Cancelled_Reject_Stage__c == la.Rejected_Cancelled_Sub_Stage__c ){
                                la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Application Initiation').getRecordTypeId();
                                la.StageName__c = objrejectRevive.Revive_Stage__c;
                            	la.Sub_Stage__c = objrejectRevive.Revive_Sub_Stage__c;
                                
                                //Below if and else if block added by Abhilekh on 29th January 2020 for Hunter BRD
                                if(la.Rejection_Reason__c == 'FCU Decline'){
                                    la.Re_Appealed_Case__c = true;
                                }
                                else if(la.Rejection_Reason__c == 'Hunter Decline'){
                                    la.Hunter_Reappeal__c = true;
                                }
                            }
                    	}
                        update la;
                    //Changes by Mehul to update CD record type
                    List<Loan_Contact__c> lstLoanCon = new List<Loan_Contact__c>(); 
                    for (Loan_Contact__c objLoanCon : [Select Id, Recordtype.DeveloperName, Recordtypeid from Loan_Contact__c where Loan_Applications__C = :la.id ]) {
                        system.debug('@@before objLoanCon.Recordtype.DeveloperName' + objLoanCon.Recordtype.DeveloperName);
                        if (objLoanCon.Recordtype.DeveloperName == 'Salaried_DC') {
                            objLoanCon.Recordtypeid = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByDeveloperName().get('Salaried').getRecordTypeId();
                            
                        } else if (objLoanCon.Recordtype.DeveloperName == 'Others_DC') {
                            objLoanCon.Recordtypeid = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByDeveloperName().get('Others').getRecordTypeId();
                        }
                        system.debug('@@after objLoanCon.Recordtype.DeveloperName' + objLoanCon.Recordtype.DeveloperName);
                        lstLoanCon.add(objLoanCon); 
                    }
                    
                    update lstLoanCon;
                    system.debug('@@@lstLoanCon' + lstLoanCon);
                    msg='Application Successfully revived';    
                    }
                    else{*/
                        la.Revive_Application__c = true;// Added by Saumya For Reject Revive BRD
                        la.Revive_Reopen_Status__c = 'Submitted for Approval';// Added by Saumya For Reject Revive BRD
                        
                        //Below if block added by Abhilekh on 29th January 2020 for Hunter BRD
                        if(la.Rejection_Reason__c == 'FCU Decline'){
                            la.Re_Appealed_Case__c = true;
                        }
                        
                        update la;// Added by Saumya For Reject Revive BRD
                        msg='';// Added by Saumya For Reject Revive BRD  
                   // }
                }
                else{
                    msg = 'Please fill in remarks before reviving the Loan Application.';
                }
                
            }
            else{
                msg = 'This application cannot be Revived as it has passed the 30 days limit.';
            }
        }

        
           /* if(Approval.isLocked(la) == true){
                Approval.UnlockResult urList = Approval.unlock(la, false);
                if (urList.isSuccess()){
                    system.debug('@@@ inside if locked');
                    update la;
                }
                else{
                }
            }
            else if(!Approval.isLocked(la) == true){
                system.debug('@@@ inside if ');
                update la;       
                system.debug('@@@ after inside if ' + la);         
            }
            system.debug('=========>>>' + la.recordtypeId);*/
            
        } catch (DMLException e) {
            system.debug('@@exception1' + e);
            Database.rollback(sp);
            
        } catch (Exception e) {
            system.debug('@@exception2' + e);
            Database.rollback(sp);
            
                
        }
        return msg;
    }
	
	//Below method added by Abhilekh on 25th September 2019 for FCU BRD
    @AuraEnabled
    public static String FCUReviveApplication(loan_application__c la){
        //Below if block only,else if block and else block added by Abhilekh on 29th January 2020 for Hunter BRD
        if(la.Previous_Sub_Stage__c == Constants.FCU_Review){
            Savepoint sp = Database.setSavepoint();
            String msg;
            msg='';
            //Reject_Revive_Master__mdt lstofRejectRevivemdt =[Select Id,Re_Open_Stage__c,Reopen__c,Re_Open_Sub_Stage__c,Revive_Stage__c,Revive_Sub_Stage__c,Cancelled_Reject_Stage__c from Reject_Revive_Master__mdt where IsActive__c=true and Cancelled_Reject_Stage__c =: la.Rejected_Cancelled_Sub_Stage__c];
            //System.debug('lstofRejectRevivemdt:::'+lstofRejectRevivemdt);
            if(Approval.isLocked(la) == true){
                msg = 'Application has already been submitted for Approval.';
            }
            else if(la.Severity_Level__c == '1'){
                msg = 'Revival is not allowed for application whose Severity Level is 1.';
            }
            else if(la.Insurance_Loan_Application__c == true){ //Added By Saumya For Insurance Loan App
                msg = 'Revival is not allowed for Insurance Loan application.';
            }
            //else if((la.FCU_Decline_Count__c == 0 || la.FCU_Decline_Count__c == null) && (la.Post_Sanction_FCU_Decline_Count__c == 0 || la.Post_Sanction_FCU_Decline_Count__c == null) && la.Rejection_Reason__c != 'FCU Decline' && (la.Overall_FCU_Status__c != Constants.NEGATIVE || la.Reviewed_by_FCU_Manager__c == false) && (la.Overall_Post_Sanction_FCU_Status__c != Constants.NEGATIVE || la.Post_Sanction_FCU_Manager_Review__c == false)){ //la.Post_Sanction_FCU_Decline_Count__c == 0 added by Abhilekh on 30th October 2019 for Post Sanction FCU BRD
            else if(la.Rejection_Reason__c != 'FCU Decline'){            
                msg = 'Reappeal is allowed only in case of FCU Reject or Hunter Reject.';
            }
            else if(UserInfo.getUserId() != la.OwnerId && !Test.isRunningTest()){
                msg= 'You are not authorized for Revive.';
            }
            else if(la.Loan_Status__c == 'Cancelled'){ //Added By Saumya For Loan Status
                msg = 'Application with Cancelled Loan Status cannot be revived.';
            }
            else{
                system.debug('@@la.Date_of_Cancellation__c' + la.Date_of_Cancellation__c);
                system.debug('@@la.Date_of_Rejection__c' + la.Date_of_Rejection__c);
                system.debug('@@la.Remarks_Revival__c' + la.Remarks_Revival__c);
                system.debug('@@la.' + la);
                if((la.Date_of_Cancellation__c != null && la.Date_of_Cancellation__c.daysBetween(System.Today()) < 30) || (la.Date_of_Rejection__c != null && la.Date_of_Rejection__c.daysBetween(System.Today()) < 30)){
                    if(la.Remarks_Revival__c != NULL) {
                        if(la.Moved_To_FCU_Review_From_Credit__c == true || la.Rejected_Cancelled_Sub_Stage__c == Constants.Re_Credit || la.Rejected_Cancelled_Sub_Stage__c == Constants.Credit_Review || la.Rejected_Cancelled_Sub_Stage__c == Constants.Re_Look){
                            //la.FCU_Revive__c = true;
                            la.Re_Appealed_Case__c = true;
                            la.StageName__c = Constants.Credit_Decisioning;
                            la.Sub_Stage__c = Constants.Re_Credit;
                            la.Moved_To_FCU_Review_From_Credit__c = false;
                            msg='Application successfully revived';
                        }
                        else{
                            la.Revive_Application__c = true;
                            la.Revive_Reopen_Status__c = 'Submitted for Approval';
                            la.Re_Appealed_Case__c = true;
                            la.Post_Sanction_FCU_Revive__c = true;
                            msg='';
                        }
                    }
                    else{
                        msg = 'Please fill in remarks before reviving the Loan Application.';
                    }
                    
                }
                else{
                    msg = 'This application cannot be Revived as it has passed the 30 days limit.';
                }
            }
            
            try {
                if(Approval.isLocked(la) == true){
                    Approval.UnlockResult urList = Approval.unlock(la, false);
                    if (urList.isSuccess()){
                        system.debug('@@@ inside if locked');
                        update la;
                    }
                    else{
                        
                    }
                }
                else if(!Approval.isLocked(la) == true){
                    system.debug('@@@ inside if ');
                    update la;       
                    system.debug('@@@ after inside if ' + la);         
                }
                system.debug('=========>>>' + la.recordtypeId);
                
            } catch (DMLException e) {
                system.debug('@@exception1' + e);
                Database.rollback(sp);
                
            } catch (Exception e) {
                system.debug('@@exception2' + e);
                Database.rollback(sp);
                
                
            }
            return msg;
        }
        
        else if(la.Previous_Sub_Stage__c == Constants.Hunter_Review){
            Savepoint sp = Database.setSavepoint();
            String msg;
            msg='';
            //Reject_Revive_Master__mdt lstofRejectRevivemdt =[Select Id,Re_Open_Stage__c,Reopen__c,Re_Open_Sub_Stage__c,Revive_Stage__c,Revive_Sub_Stage__c,Cancelled_Reject_Stage__c from Reject_Revive_Master__mdt where IsActive__c=true and Cancelled_Reject_Stage__c =: la.Rejected_Cancelled_Sub_Stage__c];
            //System.debug('lstofRejectRevivemdt:::'+lstofRejectRevivemdt);
            if(Approval.isLocked(la) == true){
                msg = 'Application has already been submitted for Approval.';
            }
            else if(la.Severity_Level__c == '1'){
                msg = 'Revival is not allowed for application whose Severity Level is 1.';
            }
            else if(la.Insurance_Loan_Application__c == true){ //Added By Saumya For Insurance Loan App
                msg = 'Revival is not allowed for Insurance Loan application.';
            }
            //else if((la.FCU_Decline_Count__c == 0 || la.FCU_Decline_Count__c == null) && (la.Post_Sanction_FCU_Decline_Count__c == 0 || la.Post_Sanction_FCU_Decline_Count__c == null) && la.Rejection_Reason__c != 'FCU Decline' && (la.Overall_FCU_Status__c != Constants.NEGATIVE || la.Reviewed_by_FCU_Manager__c == false) && (la.Overall_Post_Sanction_FCU_Status__c != Constants.NEGATIVE || la.Post_Sanction_FCU_Manager_Review__c == false)){ //la.Post_Sanction_FCU_Decline_Count__c == 0 added by Abhilekh on 30th October 2019 for Post Sanction FCU BRD
            else if(la.Rejection_Reason__c != 'Hunter Decline'){            
                msg = 'Reappeal is allowed only in case of FCU Reject or Hunter Reject.';
            }
            else if(UserInfo.getUserId() != la.OwnerId && !Test.isRunningTest()){
                msg= 'You are not authorized for Revive.';
            }
            else if(la.Loan_Status__c == 'Cancelled'){ //Added By Saumya For Loan Status
                msg = 'Application with Cancelled Loan Status cannot be revived.';
            }
            else{
                system.debug('@@la.Date_of_Cancellation__c' + la.Date_of_Cancellation__c);
                system.debug('@@la.Date_of_Rejection__c' + la.Date_of_Rejection__c);
                system.debug('@@la.Remarks_Revival__c' + la.Remarks_Revival__c);
                system.debug('@@la.' + la);
                if((la.Date_of_Cancellation__c != null && la.Date_of_Cancellation__c.daysBetween(System.Today()) < 30) || (la.Date_of_Rejection__c != null && la.Date_of_Rejection__c.daysBetween(System.Today()) < 30)){
                    if(la.Remarks_Revival__c != NULL) {
                        la.Hunter_Reappeal__c = true;
                        la.StageName__c = Constants.Credit_Decisioning;
                        la.Sub_Stage__c = Constants.Re_Credit;
                        msg='Application successfully revived';
                    }
                    else{
                        msg = 'Please fill in remarks before reviving the Loan Application.';
                    }
                    
                }
                else{
                    msg = 'This application cannot be Revived as it has passed the 30 days limit.';
                }
            }
            
            try {
                if(Approval.isLocked(la) == true){
                    Approval.UnlockResult urList = Approval.unlock(la, false);
                    if (urList.isSuccess()){
                        system.debug('@@@ inside if locked');
                        update la;
                    }
                    else{
                        
                    }
                }
                else if(!Approval.isLocked(la) == true){
                    system.debug('@@@ inside if ');
                    update la;       
                    system.debug('@@@ after inside if ' + la);         
                }
                system.debug('=========>>>' + la.recordtypeId);
                
            } catch (DMLException e) {
                system.debug('@@exception1' + e);
                Database.rollback(sp);
                
            } catch (Exception e) {
                system.debug('@@exception2' + e);
                Database.rollback(sp);
                
                
            }
            return msg;
        }
        else{
            String msg;
            msg = 'Reappeal is allowed only in case of FCU Reject or Hunter Reject.';
            return msg;
        }
    }
}