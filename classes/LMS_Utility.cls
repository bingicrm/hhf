public with sharing class LMS_Utility 
{
    @future(callout=true)
    public static void generateLMSID( String loanApplicationID ) 
    {
        Rest_Service__mdt restService = [ SELECT MasterLabel, QualifiedApiName , Service_EndPoint__c , 
                                                 Client_Password__c, Client_Username__c, Request_Method__c, Time_Out_Period__c  
                                          FROM   Rest_Service__mdt
                                          WHERE  MasterLabel = 'LMS Loan Application Id'
                                        ]; 
        List<Loan_Application__c> lstLoanApps = [select id,Name,Loan_Application_Number__c from Loan_Application__c WHERE id = :loanApplicationID];
        if(lstLoanApps.size() == 0) {
            return;
        }
        Loan_Application__c la = lstLoanApps[0];

        

        /*LMSIdRequest rqst = new LMSIdRequest();
        rqst.RequestID = la.Name.substring(3, la.Name.length()-1);
        rqst.customerId = loanApplicationID;
        rqst.applicantType = 'P';
        rqst.UserId = restService.Client_Username__c;
        rqst.Password = restService.Client_Password__c;
        rqst.applicationId = '';
        rqst.responseDateTime = '';
        rqst.ErrorMsg = '';
        */
        
        Map<String,String> headerMap = new Map<String,String>();
        //
        Blob headerValue = Blob.valueOf(restService.Client_Username__c +':' + restService.Client_Password__c);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        headerMap.put('Authorization',authorizationHeader);
        headerMap.put('Username',restService.Client_Username__c);
        headerMap.put('Password',restService.Client_Password__c);
        headerMap.put('Content-Type','application/json');

        //String jsonRequest = Json.serialize(rqst);
        String jsonRequest = IntegrationFrameWork.returnJSON('LMS Loan Application Id','Loan_Application__c',loanApplicationID);
        HttpResponse res   = Utility.sendRequest(restService.Service_EndPoint__c,restService.Request_Method__c, jsonRequest , headerMap, Integer.valueOf(restService.Time_Out_Period__c) );
        system.debug('Response before null'+res);
        if(res != null){
            system.debug('Response'+res);
            System.debug('Response'+res.getBody());
        
        if( res.getStatusCode() == 200 )
        {
            LMSIdRequest response = (LMSIdRequest)JSON.deserialize(res.getBody(), LMSIdRequest.class);
            system.debug(response.applicationId);
            if(String.isNotEmpty(response.applicationId))
            {
                la.Loan_Number__c = response.applicationId;
            }

            update la;
        }
        }

    }


    public class LMSIdRequest
    {
        public String RequestID;
        public String ErrorMsg;
        public String applicationId;
        public String customerId;
        public String applicantType;
        public String responseDateTime;
        public String UserId;
        public String Password;
    }
}