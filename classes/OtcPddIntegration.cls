public with sharing class OtcPddIntegration 
{
    @AuraEnabled
    public static void IntegrateOTC(id docid) 
    {
        String blob64Data = '';
        Rest_Service__mdt serviceDetails=[select Client_Password__c,Client_Username__c,
                                                 Request_Method__c,Time_Out_Period__c, Service_EndPoint__c 
                                                 from Rest_Service__mdt 
                                                where MasterLabel='PddOtc'];

        Document_Checklist__c DocChecklist = [Select Id,Loan_Applications__r.Loan_Number__c,OTC_PDD_Remarks__c,OTC_Received__c,
        Loan_Applications__r.Customer__r.Name, Loan_Applications__r.Customer__r.Customer_ID__c ,Loan_Contact__c,
        Loan_Contact__r.Customer__r.Name, Loan_Contact__r.Customer__c,  Loan_Contact__r.Customer__r.Customer_ID__c,Loan_Contact__r.LMS_Customer_ID__c,
        Document_master__r.Doc_Id__c,Document_master__r.Name, Document_Type__r.Document_Type_Id__c,Document_Type__r.Name, Document_Type__c,REquest_Date_for_OTC__c
                                                     from Document_Checklist__c where Id = :docid];

        loan_contact__c appCon = [select id, LMS_Customer_ID__c, Customer__c, Customer__r.Customer_ID__c from loan_contact__c where loan_applications__c = :DocChecklist.Loan_Applications__c and Applicant_Type__c = 'Applicant'];

        List<ContentDocumentLink> dcl =[SELECT ContentDocumentId 
                                      FROM ContentDocumentLink 
                                      WHERE LinkedEntityId =:docid ];
        if( dcl.size() > 0 )
        {
            List<ContentVersion> lstDocumentVersion = [SELECT Id,ContentDocumentId,FileExtension,Title,VersionData 
                                                 FROM ContentVersion 
                                                 WHERE ContentDocumentId = :dcl[0].ContentDocumentId];
            if( lstDocumentVersion.size() > 0 )
            {
                Blob blobfile = lstDocumentVersion[0].VersionData;            
                blob64Data = EncodingUtil.base64Encode(blobfile);
            }
        }
        
            
            


        if(serviceDetails != null)
        {


            Map<String,String> headerMap = new Map<String,String>();
            Blob headerValue = Blob.valueOf(serviceDetails.Client_Username__c + ':' + serviceDetails.Client_Password__c);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            headerMap.put('Content-Type','application/json');
            headerMap.put('Accept' , 'application/json');
            headerMap.put('Authorization',authorizationHeader);
            headerMap.put('Username',serviceDetails.Client_Username__c);
            headerMap.put('Password',serviceDetails.Client_Password__c);


            OTCPDDWrapper req = new OTCPDDWrapper(); 
            req.applicationId = DocChecklist.Loan_Applications__r.Loan_Number__c;
            if( DocChecklist.Loan_Contact__c != null && DocChecklist.Loan_Contact__r.Customer__c != null && DocChecklist.Loan_Contact__r.Customer__r.Customer_ID__c != null)
            {
                req.customerId =  DocChecklist.Loan_Contact__r.Customer__r.Customer_ID__c;
            }
            else if(appCon.Customer__c != null && appCon.Customer__r.Customer_ID__c != null)
            {
                req.customerId =  appCon.Customer__r.Customer_ID__c;
            }
            req.documentId = DocChecklist.Document_master__r.Doc_Id__c != null ? DocChecklist.Document_master__r.Doc_Id__c : '';
            req.documenttype = DocChecklist.Document_Type__c != null ? DocChecklist.Document_Type__r.Name : '';
            req.userId = 'cointrive';
            req.password = 'zqbAx8rZ0LvWMftg38eTatwjEANYAo/6';
            req.requestId = Utility.generateRandomRequestID();
            req.sourceId = 'SFDC-HFL';
            req.receivedDate = getUpdatedDate(system.today());
            req.stage = 'otc';
            req.subStage = '';
            //req.image = blob64Data;
            req.image = ''; // Changes as per Shirish - 07-01
            req.address = '';
            req.longitude = '';
            req.latitude = '';

            String jsonRequest = Json.serialize(req,true);
                 
            HttpResponse res   = Utility.sendRequest(serviceDetails.Service_EndPoint__c,serviceDetails.Request_Method__c, jsonRequest , headerMap, Integer.valueOf(serviceDetails.Time_Out_Period__c) );
            //system.debug('===response===='+res);
           
            if( res != null && res.getStatusCode() == 200 )
            {
                system.debug('===response===='+res.getBody());
                try
                {
                    system.debug('===response===='+res);
                    OTCResponse wrpr = new OTCResponse();
                    wrpr = (OTCResponse)JSON.deserialize(res.getBody(), OTCResponse.class);
                    system.debug('===wrapper===='+wrpr);
                    DocChecklist.OTC_PDD_Remarks__c = wrpr.result.remarks;
                    if(wrpr.result.remarks.contains('Data successfully updated'))
                    {
                        DocChecklist.OTC_Received__c = true;
                    }
                }
                catch( Exception e )
                {
                    system.debug('===Exception===='+e.getMessage());
                    DocChecklist.OTC_Received__c = false;
                }
            }
            system.debug('final doc checklist'+DocChecklist);
            if( DocChecklist != null )
            {
                update DocChecklist;
            } 
        }
    }

    public static String getUpdatedDate( Date toUpdate)
    {
        DateTime dt =  Datetime.newInstance(toUpdate.year(), toUpdate.month(), toUpdate.day());
        return dt.format('dd-MMM-yyyy');
    }
    public class OTCResponse
    {
        public Result result;
    }
    public class Result {
        public String applicationId;
        public String customerId;
        public Object dateCreated;
        public String documenttype;
        public String stage;
        public String subStage;
        public Object fileName;
        public Object imageName;
        public Object width;
        public Object image;
        public Object height;
        public String imagedbName;
        public String path;
        public String imageCount;
        public String syncFlag;
        public Integer imageCountIdentifier;
        public String documentId;
        public String latitude;
        public String longitude;
        public String address;
        public Object original;
        public Object dmsDocId;
        public Object ftrFNTR;
        public Object status;
        public Object targetDate;
        public String reason;
        public String remarks;
        public String userId;
        public String password;
        public Object errMsg;
        public Object errCode;
        public String receivedDate;
        public String sourceId;
        public String requestId;
        public Object fivReportStream;
    }
    public class OTCPDDWrapper
    {
        public string applicationId;
        public string customerId;
        public string documentId;
        public string documenttype;
        public string fileName;
        public string imageName;
        public string stage;
        public string subStage;
        public string image;
        public string address;
        public string longitude;
        public string latitude;
        public string userId;
        public string password;
        public string requestId;
        public string sourceId;
        public string receivedDate;
        public string remarks;
        public string errMsg;
        public String errCode;
    }
}