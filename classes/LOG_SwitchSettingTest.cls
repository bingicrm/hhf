@isTest(SeeAllData=false)
/*
*  Name           :   LOG_SwitchSetting
*  Created By     :   Shilpa Menghani
*  Created Date   :   May 9,2016
*  Description    :   Test class 
*/
private class LOG_SwitchSettingTest{
    private static User user;
    private static void setUpData(){
        Profile profile = [select Id,Name from Profile where Name =: 'System Administrator'];
        
        user = new User();
        user.Username = 'testUser87@deloitte.com';
        user.Email = 'testUser123@deloitte.com';
        user.LastName = 'Test87';
        user.Alias = 'Test123';
        user.ProfileID = profile.Id;
        user.LocaleSidKey = 'en_US';
        user.LanguageLocaleKey = 'en_US';
        user.Country = 'India';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.EmployeeNumber = '22';
        user.EmailEncodingKey='UTF-8';
        insert user;
        
        List<Controller_Settings__c> controllerSettingList = new List<Controller_Settings__c>();
                
        Controller_Settings__c controlSetting = new Controller_Settings__c();
        controlSetting.Name = profile.Name;
        controlSetting.SetupOwnerId = profile.Id;
        controlSetting.Logs_Enabled__c = true;
        controlSetting.Log_Integration_Errors__c = true;
        
        controllerSettingList.add(controlSetting);
        insert controllerSettingList;
    } 
    static testMethod void myUnitTestOne() {
        // TO DO: implement unit test
        setUpData();
        
        Test.startTest();
        
        System.runAs(user) { 
            LOG_SwitchSetting logSwitch = new LOG_SwitchSetting();
            //logSwitch.Next();
            //logSwitch.previous();
            
            //system.assertEquals(false,logSwitch.hasPrevious); 
           
            //system.assertEquals(true,logSwitch.hasNext);
            logSwitch.save();
            
        }
        Test.stopTest();
     }
}