public Class PersonalDiscussionTriggerHandler 
{

	public static void afterInsert(List<Personal_Discussion__c> newList)
	{

        List<Personal_Discussion__c> eventsToPass = new List<Personal_Discussion__c>();        
        for(Personal_Discussion__c pd: newList){
            if(pd.Customer_Detail__c != null)  
            {
                eventsToPass.add(pd);
            }
        }

        PersonalDiscussionTriggerHelper.sendSMStoCustomer(eventsToPass);
    }

    public static void beforeInsert(List<Personal_Discussion__c> newList)
    {
        Set<Id> custIDs = new Set<Id>();
        
        for( Personal_Discussion__c  pd : newList )
        {
            if( pd.Customer_Detail__c != null )
            {
                custIDs.add(pd.Customer_Detail__c);
            }
        }
        if( custIDs.size() > 0 )
        {
            Map<Id,Loan_Contact__c> allCust = new Map<Id,Loan_Contact__c>([select id,Customer__r.PersonEmail, Email__c from Loan_Contact__c where id in :custIDs]);
            for( Personal_Discussion__c  pd : newList )
            {
               if( allCust.get(pd.Customer_Detail__c).Customer__r.PersonEmail != null )
                    pd.Customer_Email_Address__c =  allCust.get(pd.Customer_Detail__c).Customer__r.PersonEmail;
                    
               
            }

        }
      
    }
	
	//Added by Vaishali for BREII
    public static void afterUpdate(List<Personal_Discussion__c> newList,Map<Id, Personal_Discussion__c> oldMap){
    
        Map<Id,BRE2_Retrigger_Fields__mdt> mapBRE2Retrigger = new Map<Id,BRE2_Retrigger_Fields__mdt	>([Select Id, Field__c, Object__c, isActive__c from BRE2_Retrigger_Fields__mdt where Object__c = 'Personal_Discussion__c' and isActive__c = true]);
        Set<Id> setOfAppIds = new Set<Id>();
        Map<Id,BRE2_Retrigger_Customer_Negotiation__mdt > mapBRE2RetriggerCustNegotiation = new Map<Id, BRE2_Retrigger_Customer_Negotiation__mdt  >([Select Id, Field__c, Object__c, isActive__c from BRE2_Retrigger_Customer_Negotiation__mdt where Object__c = 'Personal_Discussion__c' and isActive__c = true]);
        system.debug('mapBRE2Retrigger '+mapBRE2Retrigger);
        system.debug('mapBRE2RetriggerCustNegotiation '+mapBRE2RetriggerCustNegotiation);

        for( Personal_Discussion__c personalDisc : newList )
        {
            for ( BRE2_Retrigger_Fields__mdt objbreReset : mapBRE2Retrigger.values() ) {
                if( personalDisc.get(objbreReset.Field__c) != oldMap.get(personalDisc.id).get(objbreReset.Field__c)) {
                    setOfAppIds.add(personalDisc.Loan_Application__c); 
                    system.debug('Setting Financial_Eligibility_Valid__c to false');
                    break;
                }
            }
            
            for ( BRE2_Retrigger_Customer_Negotiation__mdt objbreReset : mapBRE2RetriggerCustNegotiation.values() ) {
                if(  personalDisc.get(objbreReset.Field__c) != oldMap.get(personalDisc.id).get(objbreReset.Field__c)) {
                    setOfAppIds.add(personalDisc.Loan_Application__c); 
                    system.debug('Setting Financial_Eligibility_Valid__c to false');
                    break;
                }
            }
        }
        if(!setOfAppIds.isEmpty()) {
            LoanContactHelper.updateBRE2RetriggerOnLA(setOfAppIds);
        }
    }
    
    public static void beforeUpdate(List<Personal_Discussion__c> newList, Map<Id,Personal_Discussion__c> oldMap) {
        system.debug('In before Update');
        Map<Id, BRE2_Retrigger_Fields__mdt	> mapBRE2Retrigger = new Map<Id,BRE2_Retrigger_Fields__mdt	>([Select Id, Field__c, Object__c, isActive__c from BRE2_Retrigger_Fields__mdt where Object__c = 'Personal_Discussion__c' and isActive__c = true]);
        Set<Id> setOfAppIds = new Set<Id>();
        Map<Id,BRE2_Retrigger_Customer_Negotiation__mdt > mapBRE2RetriggerCustNegotiation = new Map<Id, BRE2_Retrigger_Customer_Negotiation__mdt  >([Select Id, Field__c, Object__c, isActive__c from BRE2_Retrigger_Customer_Negotiation__mdt where Object__c = 'Personal_Discussion__c' and isActive__c = true]);
        system.debug('mapBRE2Retrigger '+mapBRE2Retrigger);
        system.debug('mapBRE2RetriggerCustNegotiation '+mapBRE2RetriggerCustNegotiation);
        
        for(Personal_Discussion__c personalDis : newList){
            for ( BRE2_Retrigger_Fields__mdt objbreReset : mapBRE2Retrigger.values() ) {
                if( personalDis.reTriggerBRE2__c == false && personalDis.get(objbreReset.Field__c) != oldMap.get(personalDis.id).get(objbreReset.Field__c)) {
                    setOfAppIds.add(personalDis.Loan_Application__c); 
                    system.debug('Setting Financial_Eligibility_Valid__c to false');
                    break;
                }
            }
            
            for ( BRE2_Retrigger_Customer_Negotiation__mdt objbreReset : mapBRE2RetriggerCustNegotiation.values() ) {
                if( personalDis.reTriggerBRE2__c == false && personalDis.get(objbreReset.Field__c) != oldMap.get(personalDis.id).get(objbreReset.Field__c)) {
                    setOfAppIds.add(personalDis.Loan_Application__c); 
                    system.debug('Setting Financial_Eligibility_Valid__c to false');
                    break;
                }
            }
        }
        
        system.debug('setOfAppIds '+setOfAppIds);
        Id recordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('BRE2').getRecordTypeId();
        List<Customer_Integration__c> lstOfBRE2SuccessfulCustInt= new List<Customer_Integration__c>();
        if(!setOfAppIds.isEmpty()) {
            lstOfBRE2SuccessfulCustInt = [SELECT Id, Loan_Application__c, BRE2_Success__c from Customer_Integration__c WHERE Loan_Application__c IN: setOfAppIds and BRE2_Recent__c = true and recordTypeId =: recordTypeId ]; 
        }
        Map<Id, Boolean> mapOfBRE2SuccessfulCustInt = new Map<Id, Boolean>();
        if(!lstOfBRE2SuccessfulCustInt.isEmpty()) {
            for(Customer_Integration__c cust : lstOfBRE2SuccessfulCustInt) {
                if(cust.BRE2_Success__c == true) {
                    mapOfBRE2SuccessfulCustInt.put(cust.Loan_Application__c, true);
                } else {
                    mapOfBRE2SuccessfulCustInt.put(cust.Loan_Application__c, false);    
                }
            }
        }
        system.debug('mapOfBRE2SuccessfulCustInt '+mapOfBRE2SuccessfulCustInt);

        for(Personal_Discussion__c personalDis : newList) {
            if (mapOfBRE2SuccessfulCustInt != null) {
                if(mapOfBRE2SuccessfulCustInt.containsKey(personalDis.Loan_Application__c)) {
                    if(mapOfBRE2SuccessfulCustInt.get(personalDis.Loan_Application__c)) {
                        personalDis.reTriggerBRE2__c = true;       
                    }
                }
            }
        }
        
    }
    //End of patch- Added by Vaishali for BREII
}