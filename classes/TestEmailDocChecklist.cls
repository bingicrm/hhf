@isTest
public class TestEmailDocChecklist {
  @TestSetup
    static void setup()
    {
        
        Account acc=new Account();
       // acc.name='TestAccount1';
        acc.phone='9234567667';
        acc.PAN__c='fjauy0916u';
        acc.PersonMobilePhone='9234234234';
        acc.PersonEmail='abcdea@gmail.com';
        acc.Date_of_Incorporation__c=date.today();
        acc.FirstName='Fname';
        acc.LastName='Lname';
        acc.Salutation='Mr.';
        insert acc;
        
        System.debug(acc.id);
        
        Scheme__c sc=new Scheme__c();
        sc.name='TestScheme';
        sc.Scheme_Code__c='testcode';
        sc.Scheme_Group_ID__c='23131';
        sc.Product_Code__c='HL';
        insert sc;
        System.debug(sc.id);
        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Transaction_type__c='PB';
        lap.Scheme__c=sc.id;
        insert lap;
        System.debug(lap.id);
        
        City__c city=new city__c();
        city.City_ID__c='100000';
        city.Name='TEZPOOR';
        city.Country__c='1';
        city.State__c='5';
        insert city;
        Pincode__c pin=new Pincode__c();
        pin.City_LP__c=city.id;
        pin.ZIP_ID__c='46172';
        insert pin;
        //Customer__c c=new Customer__c();
        list<Loan_Contact__c> lc=[select id, name, Customer__r.PersonEmail from Loan_Contact__c where Loan_Applications__c=:lap.id limit 1];
        system.debug('loan contact'+lc[0]);
        Address__c address=new Address__c();
        address.Address_Line_1__c='abc Street';
        address.Address_Line_2__c='abc Area';
        address.Pincode_LP__c=pin.id;
        address.Type_of_address__c='PERMNENT';
        address.Loan_Contact__c=lc[0].id;
        //insert address;
        Document_Master__c dm=new Document_Master__c();
        dm.name='MasterDoc';
        dm.Doc_Id__c= '67';
        insert dm;
        Document_Checklist__c dc=new Document_Checklist__c();
        dc.Status__c='Pending';
        dc.Loan_Applications__c=lap.id;
        dc.Loan_Contact__c=lc[0].id;
        dc.Document_Master__c=dm.id;
        insert dc;
        
    }
    @isTest
    public static void TestGetLoanApp(){
         list<Loan_Application__c> lc = [select  Id, Name, StageName__c, Sub_Stage__c, Assigned_Sales_User__c, RecordTypeID from Loan_Application__c limit 1];
        emailDocChecklist.getLoanApp(lc[0].id);
      //  emailDocChecklist.mailDocChecklist(lc[0]);
    }
      @isTest
    public static void TestMailDocChecklist(){
          list<Loan_Application__c> lc = [select  Id, Name, StageName__c, Sub_Stage__c, Assigned_Sales_User__c, RecordTypeID from Loan_Application__c limit 1];
    
     emailDocChecklist.mailDocChecklist(lc[0]);
    }
}