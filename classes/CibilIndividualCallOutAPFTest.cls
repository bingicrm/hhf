@isTest(SeeAllData = false)
private class CibilIndividualCallOutAPFTest {

	private static testMethod void test1() {
        
        
	    List<RecordType> lstRecordType = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Individual'];
        Id builderIndvRecTypeId;
        if(!lstRecordType.isEmpty()) {
            builderIndvRecTypeId = lstRecordType[0].Id;
        }
        
        Account acc = new Account();
            acc.RecordTypeId = builderIndvRecTypeId;
            acc.Name = 'Individual Test Acc';
            acc.PAN__c = 'AAAAA3333B';
            acc.GSTIN__c = '27AAHFG0551H1ZL';
        insert acc;
        
        List<RecordType> lstRecordType2 = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Corporate'];
        Id builderCorpRecTypeId;
        if(!lstRecordType2.isEmpty()) {
            builderCorpRecTypeId = lstRecordType2[0].Id;
        }
        
        List<RecordType> lstIndividualPBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Individual'];
        List<RecordType> lstCorporatePBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Corporate'];
        
        Account acc2 = new Account();
            acc2.RecordTypeId = builderCorpRecTypeId;
            acc2.Name = 'Test Corporate Account';
            acc2.PAN__c = 'AAAAB2222B';
            
        insert acc2;
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83123');
        insert objPincode;
        
        Project__c objProject = new Project__c();
        //objProject.Assigned_Sales_User__c = assignedSalesUser != null ? assignedSalesUser.Id : null;
        objProject.Account__c = acc.Id;
        objProject.Project_Name__c = 'Test Project';
        objProject.APF_Status__c = 'APF Initiated';
        objProject.Stage__c = 'APF Initiation';
        objProject.Sub_Stage__c = 'Project Data Capture';
        objProject.APF_Type__c = 'New';
        objProject.Address_Line_1__c = 'Test Address';
        objProject.Pincode__c = objPincode.Id;
        insert objProject;
        
        Project_Builder__c objProjectBuilderCorp = new Project_Builder__c();
        objProjectBuilderCorp.RecordTypeId = lstCorporatePBId[0].Id;
        objProjectBuilderCorp.Builder_Name__c = acc2.Id;
        objProjectBuilderCorp.Constitution__c = '24';
        objProjectBuilderCorp.Builder_Status__c = 'Resident';
        objProjectBuilderCorp.Contact_Number_Mobile__c = '9999444223';
        objProjectBuilderCorp.PAN_Number__c = 'EGNPS1111T';
        objProjectBuilderCorp.Project__c = objProject.Id; 
        objProjectBuilderCorp.Preferred_Communication_Address__c = 'CURRES'; 
        //objProjectBuilderCorp. =
        Database.insert(objProjectBuilderCorp,false);
        
        acc2.GSTIN__c = '27AAHFG0551H1ZL';
        update acc2;
        
        insert objProjectBuilderCorp;
        System.debug('Debug Log for GSTIN'+objProjectBuilderCorp);
        
        
        objProjectBuilderCorp.GSTIN_Number__c = '27AAHFG0551H1ZL';
        //objProjectBuilderCorp.Builder_Role__c = Constants.strPBConstructorMarketer;
        update objProjectBuilderCorp;
        
        Set<Id> setProjectId = new Set<Id>();
        setProjectId.add(objProject.Id);
        
        
        
        List<Project_Builder__c> lstProjectBuilder = [SELECT Id, Gender__c, RecordTypeId, Builder_Category__c, GSTIN_Available__c, Name, Date_Of_Birth__c, Builder_Name__r.RecordType.DeveloperName, All_Promoter_Count__c, Builder_Type__c, Class_Of_Activity__c, Builder_Name__c, Builder_Name__r.RecordTypeId, Builder_Status__c, Constitution__c, Builder_Role__c, Contact_Number_Mobile__c, Email__c, Preferred_Communication_Address__c, Address_Line_1__c, Address_Line_2__c, Pincode__c, Promoter_PAN_Validation_Count__c, Promoter_Cibil_Validation_Count__c, Project__c,(Select Id, Name, Name_of_Promoter__c, Email__c, Contact_No_Mobile__c, Preferred_Communication_Address__c, Promoter_Status__c, Address_Line_1__c, Address_Line_2__c, Pincode__c from Promoters__r) From Project_Builder__c Where Project__c =: objProject.Id];
        System.debug('Debug Log for lstProjectBuilder size'+lstProjectBuilder.size());
        String builderIndividualRecordId;
        List<Promoter__c> lstPromoter = new List<Promoter__c>();
        if(!lstProjectBuilder.isEmpty()) {
            for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                objProjectBuilder.GSTIN_Available__c = true;
                objProjectBuilder.GSTIN_Number__c = '27AAHFG0551H1ZL';
                if(objProjectBuilder.Promoters__r.isEmpty() && objProjectBuilder.Builder_Name__r.RecordType.DeveloperName != 'Builder_Individual'){
                    Promoter__c objPromoter = new Promoter__c(Project_Builder__c = objProjectBuilder.Id, Preferred_Communication_Address__c = 'CURRES', PAN_Number__c = 'AAARR1123L');
                    lstPromoter.add(objPromoter);
                }
                else if(objProjectBuilder.Builder_Name__r.RecordType.DeveloperName == 'Builder_Individual') {
                    builderIndividualRecordId = objProjectBuilder.Id;
                    objProjectBuilder.Gender__c = 'M';
                }
            }
            update lstProjectBuilder;
            System.debug('Debug Log for lstPromoter'+lstPromoter.size());
            if(!lstPromoter.isEmpty()) {
                insert lstPromoter;
            }
            System.debug('Debug Log for lstPromoter post insertion from Address Check Method'+lstPromoter);
            update lstPromoter;
            
        }
        
        CibilIndividualCallOutAPF.createCustomerIntegration(lstPromoter[0].Id, false);
        CibilIndividualCallOutAPF.createCustomerIntegration(builderIndividualRecordId, false);
        
        Project_Builder__c objPB = [Select Id, pan_verification_status__c,Address_Line_1__c, Address_Line_2__c, Date_of_Birth__c, Pincode__c From Project_Builder__c Where Id =: builderIndividualRecordId LIMIT 1];
        if(objPB != null) {
            if(objPB.pan_verification_status__c == false) {
                objPB.pan_verification_status__c = true;
            }
                objPB.Pincode__c = objPincode.ID;
                objPB.Address_Line_1__c = 'Test Test';
                objPB.Address_Line_2__c = 'Test Test';
                objPB.Date_of_Birth__c = Date.newInstance(1990, 9, 14);
            
            update objPB;
            CibilIndividualCallOutAPF.ResponseCIBILWrapper builderIntegration = CibilIndividualCallOutAPF.createCustomerIntegration(objPB.Id, false);
            
            Test.startTest();
            
            TestMockRequest req1=new TestMockRequest(200,'OK',JSON.serialize(''),null);
            Test.setMock(HttpCalloutMock.class, req1);
            CibilIndividualCallOutAPF.ResponseCIBILWrapper objRCibilWrap3 = CibilIndividualCallOutAPF.integrateCibil(objProjectBuilderCorp.Id, builderIntegration.respString);
            
            Test.stopTest();
        }
        
        Promoter__c objPromoter = [Select Id, pan_verification_status__c, Address_Line_1__c, Address_Line_2__c, Pincode__c, Gender__c From Promoter__c Where Id=: lstPromoter[0].Id LIMIT 1];
        if(objPromoter != null) {
            if(objPromoter.pan_verification_status__c == false) {
                objPromoter.pan_verification_status__c = true;
            }
                objPromoter.Pincode__c = objPincode.ID;
                objPromoter.Address_Line_1__c = 'Test Test';
                objPromoter.Address_Line_2__c = 'Test Test';
                objPromoter.Gender__c = 'M';
            update objPromoter;
            CibilIndividualCallOutAPF.ResponseCIBILWrapper promoterIntegration = CibilIndividualCallOutAPF.createCustomerIntegration(objPromoter.Id, false);
            
            TestMockRequest req1=new TestMockRequest(200,'OK',JSON.serialize(''),null);
            Test.setMock(HttpCalloutMock.class, req1);
            CibilIndividualCallOutAPF.ResponseCIBILWrapper objRCibilWrap4 = CibilIndividualCallOutAPF.integrateCibil(objProjectBuilderCorp.Id, promoterIntegration.respString);
            
        }
	    
	    
        
	}
	
		private static testMethod void test2() {
        
        
	    List<RecordType> lstRecordType = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Individual'];
        Id builderIndvRecTypeId;
        if(!lstRecordType.isEmpty()) {
            builderIndvRecTypeId = lstRecordType[0].Id;
        }
        
        Account acc = new Account();
            acc.RecordTypeId = builderIndvRecTypeId;
            acc.Name = 'Individual Test Acc';
            acc.PAN__c = 'AAAAA3333B';
            acc.GSTIN__c = '27AAHFG0551H1ZL';
        insert acc;
        
        List<RecordType> lstRecordType2 = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Corporate'];
        Id builderCorpRecTypeId;
        if(!lstRecordType2.isEmpty()) {
            builderCorpRecTypeId = lstRecordType2[0].Id;
        }
        
        List<RecordType> lstIndividualPBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Individual'];
        List<RecordType> lstCorporatePBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Corporate'];
        
        Account acc2 = new Account();
            acc2.RecordTypeId = builderCorpRecTypeId;
            acc2.Name = 'Test Corporate Account';
            acc2.PAN__c = 'AAAAB2222B';
            
        insert acc2;
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83124');
        insert objPincode;
        
        Document_Master__c docMasObj = new Document_Master__c();
        docMasObj.Name = 'Test GSTIN - Registration certificate';
        docMasObj.Doc_Id__c = '123344';
        docMasObj.Approving_authority__c = 'Business Head';
        docMasObj.Applicable_for_APF__c = true;
        Database.insert(docMasObj);
        
        Document_type__c docTypeObj = new Document_type__c();
        docTypeObj.Name = 'Builder - APF';
        docTypeObj.Approving_authority__c    = 'Business Head';
         docTypeObj.Applicable_for_APF__c = true;	
        Database.insert(docTypeObj);
        
        APF_Document_Checklist__c objAPF = new APF_Document_Checklist__c();
        objAPF.Active_del__c = true;
        objAPF.Document_Master__c = docMasObj.Id;
        objAPF.Document_Type__c = docTypeObj.id;
        objAPF.Mandatory__c = true;
        objAPF.Valid_for_Project_Type__c = 'New';
        insert objAPF;
        Project__c objProject = new Project__c();
        //objProject.Assigned_Sales_User__c = assignedSalesUser != null ? assignedSalesUser.Id : null;
        objProject.Account__c = acc.Id;
        objProject.Project_Name__c = 'Test Project';
        objProject.APF_Status__c = 'APF Initiated';
        objProject.Stage__c = 'APF Initiation';
        objProject.Sub_Stage__c = 'Project Data Capture';
        objProject.APF_Type__c = 'New';
        objProject.Address_Line_1__c = 'Test Address';
        objProject.Pincode__c = objPincode.Id;
        insert objProject;
        
        Project_Builder__c objProjectBuilderCorp = new Project_Builder__c();
        objProjectBuilderCorp.RecordTypeId = lstCorporatePBId[0].Id;
        objProjectBuilderCorp.Builder_Name__c = acc2.Id;
        objProjectBuilderCorp.Constitution__c = '24';
        objProjectBuilderCorp.Builder_Status__c = 'Resident';
        objProjectBuilderCorp.Contact_Number_Mobile__c = '9999444223';
        objProjectBuilderCorp.PAN_Number__c = 'EGNPS1111T';
        objProjectBuilderCorp.Project__c = objProject.Id; 
        objProjectBuilderCorp.Preferred_Communication_Address__c = 'CURRES'; 
        //objProjectBuilderCorp. =
        Database.insert(objProjectBuilderCorp,false);
        
        acc2.GSTIN__c = '27AAHFG0551H1ZL';
        update acc2;
        
        insert objProjectBuilderCorp;
        System.debug('Debug Log for GSTIN'+objProjectBuilderCorp);
        
        
        objProjectBuilderCorp.GSTIN_Number__c = '27AAHFG0551H1ZL';
        //objProjectBuilderCorp.Builder_Role__c = Constants.strPBConstructorMarketer;
        update objProjectBuilderCorp;
        
        Set<Id> setProjectId = new Set<Id>();
        setProjectId.add(objProject.Id);
        
        
        
        List<Project_Builder__c> lstProjectBuilder = [SELECT Id, Gender__c, RecordTypeId, Builder_Category__c, GSTIN_Available__c, Name, Date_Of_Birth__c, Builder_Name__r.RecordType.DeveloperName, All_Promoter_Count__c, Builder_Type__c, Class_Of_Activity__c, Builder_Name__c, Builder_Name__r.RecordTypeId, Builder_Status__c, Constitution__c, Builder_Role__c, Contact_Number_Mobile__c, Email__c, Preferred_Communication_Address__c, Address_Line_1__c, Address_Line_2__c, Pincode__c, Promoter_PAN_Validation_Count__c, Promoter_Cibil_Validation_Count__c, Project__c,(Select Id, Name, Name_of_Promoter__c, Email__c, Contact_No_Mobile__c, Preferred_Communication_Address__c, Promoter_Status__c, Address_Line_1__c, Address_Line_2__c, Pincode__c from Promoters__r) From Project_Builder__c Where Project__c =: objProject.Id];
        System.debug('Debug Log for lstProjectBuilder size'+lstProjectBuilder.size());
        String builderIndividualRecordId;
        List<Promoter__c> lstPromoter = new List<Promoter__c>();
        if(!lstProjectBuilder.isEmpty()) {
            for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                objProjectBuilder.GSTIN_Available__c = true;
                objProjectBuilder.GSTIN_Number__c = '27AAHFG0551H1ZL';
                if(objProjectBuilder.Promoters__r.isEmpty() && objProjectBuilder.Builder_Name__r.RecordType.DeveloperName != 'Builder_Individual'){
                    Promoter__c objPromoter = new Promoter__c(Project_Builder__c = objProjectBuilder.Id, Preferred_Communication_Address__c = 'CURRES', PAN_Number__c = 'AAARR1123L');
                    lstPromoter.add(objPromoter);
                }
                else if(objProjectBuilder.Builder_Name__r.RecordType.DeveloperName == 'Builder_Individual') {
                    builderIndividualRecordId = objProjectBuilder.Id;
                    objProjectBuilder.Gender__c = 'M';
                }
            }
            update lstProjectBuilder;
            System.debug('Debug Log for lstPromoter'+lstPromoter.size());
            if(!lstPromoter.isEmpty()) {
                insert lstPromoter;
            }
            System.debug('Debug Log for lstPromoter post insertion from Address Check Method'+lstPromoter);
            update lstPromoter;
            
        }
        List<RecordType> lstPromId = [Select Id, Name from RecordType Where sObjectType = 'Promoter_Integrations__c' AND DeveloperName = 'GSTIN'];
        Promoter_Integrations__c obj = new Promoter_Integrations__c();
        obj.GSTIN__c = '123456789900';
        obj.Promoter__c = lstPromoter[0].id;
        insert obj;
        
        obj.RecordTypeId =lstPromId[0].id;
        obj.Constitution__c = 'avx';
        obj.GST_Tax_payer_type__c = 'Corporate';
        obj.Current_Status_of_Registration_Under_GST__c= 'Registered';
        obj.VAT_Registration_Number__c='124';
        obj.Email__c='abc@g.com';
        obj.Mobile__c='9988776655';
        obj.PAN_Number__c='CHZPB6440A';
        obj.Trade_Name__c='abc';
        obj.Legal_Name__c='abc';
        obj.Filling_Frequency__c=' aaaa';
        obj.Business_Activity__c= 'abc';
        obj.GST_Date_Of_Registration__c= Date.valueOf(System.Now());
        obj.Cumulative_Sales_Turnover__c= 1200000;
        obj.Month_Count__c= 9;
        update obj;
        
        CibilIndividualCallOutAPF.createCustomerIntegration(lstPromoter[0].Id, false);
        CibilIndividualCallOutAPF.createCustomerIntegration(builderIndividualRecordId, false);
        
        Project_Builder__c objPB = [Select Id, pan_verification_status__c,Address_Line_1__c, Address_Line_2__c, Date_of_Birth__c, Pincode__c From Project_Builder__c Where Id =: builderIndividualRecordId LIMIT 1];
        if(objPB != null) {
            if(objPB.pan_verification_status__c == false) {
                objPB.pan_verification_status__c = true;
            }
                objPB.Pincode__c = objPincode.ID;
                objPB.Address_Line_1__c = 'Test Test';
                objPB.Address_Line_2__c = 'Test Test';
                objPB.Date_of_Birth__c = Date.newInstance(1990, 9, 14);
            
            update objPB;
            CibilIndividualCallOutAPF.ResponseCIBILWrapper builderIntegration = CibilIndividualCallOutAPF.createCustomerIntegration(objPB.Id, false);
            
            
        }
        
        Promoter__c objPromoter = [Select Id, pan_verification_status__c, Address_Line_1__c, Address_Line_2__c, DOB__c, Pincode__c, Gender__c From Promoter__c Where Id=: lstPromoter[0].Id LIMIT 1];
        if(objPromoter != null) {
            if(objPromoter.pan_verification_status__c == false) {
                objPromoter.pan_verification_status__c = true;
            }
                objPromoter.Pincode__c = objPincode.ID;
                objPromoter.Address_Line_1__c = 'Test Test';
                objPromoter.Address_Line_2__c = 'Test Test';
                objPromoter.Gender__c = 'M';
                objPromoter.DOB__c = Date.newInstance(1990, 9, 14);
            update objPromoter;
            CibilIndividualCallOutAPF.ResponseCIBILWrapper promoterIntegration = CibilIndividualCallOutAPF.createCustomerIntegration(objPromoter.Id, false);
            
            Test.startTest();
            
            TestMockRequest req1=new TestMockRequest(200,'OK',JSON.serialize(''),null);
            Test.setMock(HttpCalloutMock.class, req1);
            CibilIndividualCallOutAPF.ResponseCIBILWrapper objRCibilWrap4 = CibilIndividualCallOutAPF.integrateCibil(objPromoter.Id, promoterIntegration.respString);
            
            Test.stopTest();
        }
	    
	    
        
	}

}