public class ApplicableDeviationTriggerHandler {

    public static void afterInsert(List<Applicable_Deviation__c> newList,List<Applicable_Deviation__c> oldList,Map<Id,Applicable_Deviation__c> newMap,Map<Id,Applicable_Deviation__c> oldMap){
        chooseAppropriateUser(newList);
    }
    
    public static void afterUpdate(List<Applicable_Deviation__c> newList,List<Applicable_Deviation__c> oldList,Map<Id,Applicable_Deviation__c> newMap,Map<Id,Applicable_Deviation__c> oldMap){
        chooseAppropriateUser(newList);
    }
    
    public static void chooseAppropriateUser(List<Applicable_Deviation__c> lstAD){
        Set<Id> setLoanApp = new Set<Id>();
        List<Loan_Application__c> lstLAtoUpdate = new List<Loan_Application__c>();
        
        for(Applicable_Deviation__c ad: lstAD){
            setLoanApp.add(ad.Loan_Application__c);
        }

        List<Loan_Application__c> lstLoanApp = [SELECT Id,Name,StageName__c,Sub_Stage__c,Requested_Amount__c,
                                                Assigned_credit_at_CN__c,Credit_Manager_User__c,Credit_Approval_required__c
                                                from Loan_Application__c WHERE Id IN: setLoanApp];
        System.debug('lstLoanApp.size()==>'+lstLoanApp.size());
        
        for(Loan_Application__c la : lstLoanApp){
            if(la.StageName__c == Constants.Customer_Acceptance  || la.StageName__c == Constants.Credit_Decisioning){
                String creditAuthId = CreditAuthorityUtility.findCreditAuthority(la.id);
                
                if(String.isNotEmpty(creditAuthId)){
                    la.Assigned_credit_at_CN__c = creditAuthId;
					la.Assigned_Credit_Review__c = creditAuthId; //Added by Vaishali for L1 Approval Fix
                    //la.Credit_Approval_required__c = true;
                }
            }
            lstLAtoUpdate.add(la);
        }
        
        if(lstLAtoUpdate.size() > 0){
            Database.update(lstLAtoUpdate);
        }
    }
    
    //Added by Vaishali for BREII
    public static void beforeUpdate(List<Applicable_Deviation__c> newList,List<Applicable_Deviation__c> oldList,Map<Id,Applicable_Deviation__c> newMap,Map<Id,Applicable_Deviation__c> oldMap){
        for(Applicable_Deviation__c app : newList) {
            if(app.Approved__c == true && oldMap.get(app.Id).Approved__c == false) {
                app.Approver__c = UserInfo.getUserId();
                app.Approval_Time__c = System.now();
            }
        }
    }
    
    public static void beforeDelete(List<Applicable_Deviation__c> oldList,Map<Id,Applicable_Deviation__c> oldMap){
        system.debug('In beforeDelete Trigger: old List '+oldList);
        
        RunDeleteTriggerOnApplicableDeviation__c   runValidationCheck = RunDeleteTriggerOnApplicableDeviation__c.getInstance('RunDeleteTriggerOnApplicableDeviation');
        system.debug(runValidationCheck);
        
        if(runValidationCheck != null && !runValidationCheck.byPassTrigger__c) {
            system.debug('Yes checking');
            Id currentUserId = UserInfo.getUserId();
            User currentUser = [SELECT Id, Profile.Name from User Where Id =:currentUserId ];
            List<Applicable_Deviation__c> lstOfDeviations = [SELECT Id, Loan_Application__r.Credit_Manager_User__r.Id, Loan_Application__r.Sub_Stage__c, Auto_Deviation__c FROM Applicable_Deviation__c WHERE Id IN: oldMap.keySet()];
            for(Applicable_Deviation__c dev : lstOfDeviations) {
                if(oldMap.containsKey(dev.Id)) {
                    if(currentUser.Profile.Name != 'System Administrator' && dev.Auto_Deviation__c == true) {
                        system.debug('deviation '+dev);  
                         oldMap.get(dev.Id).addError('This Applicable Authority Approval Norm can not be deleted');/** Changed Applicable Deviation name to Applicable Authority Approval Norm **/
                    } else if(dev.Auto_Deviation__c == false){
                        if(currentUser.Profile.Name != 'System Administrator' && ((dev.Loan_Application__r.Credit_Manager_User__r.Id != currentUserId) || (dev.Loan_Application__r.Sub_Stage__c != Constants.Credit_Review && dev.Loan_Application__r.Sub_Stage__c != Constants.Re_Credit && dev.Loan_Application__r.Sub_Stage__c != Constants.Re_Look ))) {
                            oldMap.get(dev.Id).addError('BCM can delete Applicable Authority Approval Norm only for loan applications which are at Credit Review/ Re Credit/ Re Look stage.');   /** Changed Applicable Deviation name to Applicable Authority Approval Norm **/ 
                        }
                    }
                }    
            }
        }
    }
    public static void afterDelete(List<Applicable_Deviation__c> oldList,Map<Id,Applicable_Deviation__c> oldMap){/**** Added by Saumya For L1 Credit Issue ****/
        chooseAppropriateUser(oldList);
    }

    //End of patch - Added by Vaishali for BREII
    
}