public class PincodeTriggerHandler{

    public static void beforeInsert(List<Pincode__c> lstNewPincode){
        updateStateCountry(lstNewPincode);
    }
    
    public static void beforeUpdate(List<Pincode__c> lstNewPincode){
        updateStateCountry(lstNewPincode);
    }
    
    public static void updateStateCountry(List<Pincode__c> lstNewPincode) {
        Map<String,String>  mapState = new Map<String,String>();
        Map<String,String>  mapCountry = new Map<String,String>();
       
        Schema.DescribeFieldResult fieldResult = City__c.State__c.getDescribe();
        List<Schema.PicklistEntry> lstPicklist = fieldResult.getPicklistValues();
        String strPLValue;   
        List<Id> lstIds = new List<Id>();  
        for( Schema.PicklistEntry f : lstPicklist) {
           mapState.put(f.getValue(),f.getLabel());
        }
        
        fieldResult = City__c.Country__c.getDescribe();
        lstPicklist = fieldResult.getPicklistValues();
         
        for( Schema.PicklistEntry f : lstPicklist) {
           mapCountry.put(f.getValue(),f.getLabel());
        }
          
        for(Pincode__c objPin : lstNewPincode){
          lstIds.add(objPin.City_LP__c);    
        }
        
        Map<Id,City__c> mapIdToCity = new Map<Id,City__c>([Select Id, State__c, Country__c from City__c where Id IN :lstIds]);
        
        for(Pincode__c objPin : lstNewPincode){
            //system.debug('mapIdToCity.get(objPin.City_LP__c).State__c' + mapIdToCity.get(objPin.City_LP__c).State__c);
            //system.debug('mapState@@' + mapState);
            if(mapIdToCity != null && mapIdToCity.containsKey(objPin.City_LP__c)){
                objPin.State_Text__c = mapState.get(mapIdToCity.get(objPin.City_LP__c).State__c);
                objPin.Country_Text__c = mapCountry.get(mapIdToCity.get(objPin.City_LP__c).Country__c);
            }
        }
        
    }
}