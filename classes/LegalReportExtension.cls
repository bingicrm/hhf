public with sharing class LegalReportExtension {
    
    public Third_Party_Verification__c tpv{get;set;}
    public List<Property_Document_Checklist__c> docCheckList{get;set;}
    
    public LegalReportExtension(ApexPages.StandardController stdController) {
        this.tpv = (Third_Party_Verification__c)stdController.getRecord();
        tpv = [Select l.Id, l.Owner__r.FirstName, l.Owner__r.LastName, l.Branch__c, l.Customer_Name__c, l.Loan_Application__r.Name, l.Loan_Application__r.Scheme__r.Product_Code__c, l.Property_Address_Vendor__c,
              l.Approximate_Area_of_House__c, l.Proposed_Owner__c, l.Name_of_Applicant__c, l.Report_Status__c, l.Property_Area_Under_Consideration__c, l.Payment_to_be_made_in_favor_of__c, l.Flow_of_Title__c,
              l.X13_Years_Title_Search_Report_Required__c, l.Additional_Document_Required__c, l.Rent_Details__c, l.PDD_Docs_to_collect_Post_Disbursement__c, l.Must_to_Have_Documents_to_be_collected__c,
              l.Nice_to_have_documents__c, l.Steps_Docs_req_to_take_after_Disbursal__c, l.Report_Received_Date__c, l.Address_as_per_Inspection__c, l.Evidence_of_Title_of_Property__c,
              l.Urban_Land_Ceiling_Act_Applicable__c, l.Subject_to_Minor_s_Claim_Share__c, l.Affected_by_Revenue_Tenancy_Regulations__c, l.Land_Non_Agricultural_for_Residential__c,
              l.Tax_Land_Revenue_Paid__c, l.X13_Years_Original_Documents_Scrutinized__c, l.Documents_Available_for_Mortgage__c, l.Right_to_Transfer_Property__c, l.Proposed_Mortgage_Possible__c,
              l.Transferred_by_a_POA__c, l.POA_Holder_has_had_authority__c, l.POA_Registered__c, l.Land_Tenure_if_leased__c, l.Adivasi_Tribal_Land__c, l.Joint_Family_Property__c, l.Subject_to_any_Reservations__c,
              l.Search_Report_Obtained_and_Submitted__c, l.Search_Report_Receipt_Number__c, l.Search_Conducted_Years__c, l.Name_of_the_Advocate__c, l.Search_Report_Date__c, l.EC_Obtained__c,
              l.Number_of_Years_of_EC_Obtained__c, l.EC_Applied_By__c, l.Any_Encumbrance_to_Specify__c, l.Builder_is_Private_Limited_Company__c, l.ROC_Search_Report_Submitted__c, l.Any_Charge_Mortgage_Created__c,
              l.If_Mortgaged_Charged_Name_of_the_Bank_FI__c, l.NOC_Release_Letter_Obtained__c, l.Docs_to_be_collected_at_the_time__c, l.Remarks_51__c, l.Remarks_52__c,
              l.Remarks_53__c, l.Remarks_54__c, l.Remarks_55__c, l.Remarks_56__c, l.Remarks_57__c, l.Remarks_58__c, l.Remarks_59__c, l.Remarks_510__c, l.Remarks_511__c, l.Remarks_512__c, l.Remarks_514__c, l.Remarks_515__c, 
              l.Remarks_516__c, l.Remarks_517__c, l.Remarks_518__c, l.Remarks_519__c, l.Remarks_519a__c, l.Remarks_519b__c, l.Remarks_519d__c, Loan_Application__r.Scheme__r.Name from Third_Party_Verification__c 
              l where l.Id =: tpv.Id];
        docCheckList = [Select Id, Loan_Application__c, Document_Checklist__r.Document_Type__r.Name, Third_Party_Verification__c from Property_Document_Checklist__c where Third_Party_Verification__c =: tpv.Id];
    }
    public List<string> getOL(){
        
        List<String> listStr = new List<String>();
        
        liststr.add('element1');
        liststr.add('element2');
        liststr.add('element3');
        return listStr;
    }

}