public class ProjectInventoryTriggerHandler {
    public static void onBeforeInsert(List<Project_Inventory__c> lstTriggerNew) {
        Set<Id> setTowerIds = new Set<Id>();
        Set<Id> setProjectIds = new Set<Id>();
        Map<Id, Tower__c> mapTowerIdtoTowerInfo;
        Map<Id, Project__c> mapProjectIdtoProjectInfo;
        String strFlatInfo;
        String strFloorInfo;
        String strTowerInfo;
        
        for(Project_Inventory__c objProjectInventory : lstTriggerNew) {
            if(String.isNotBlank(objProjectInventory.Tower__c)) {
                setTowerIds.add(objProjectInventory.Tower__c);
            }
            if(String.isNotBlank(objProjectInventory.Project__c)) {
                setProjectIds.add(objProjectInventory.Project__c);
            }
        }
        
        if(!setProjectIds.isEmpty()) {
            mapProjectIdtoProjectInfo = new Map<Id,Project__c>([Select Id, Name, Pincode__c From Project__c Where Id IN: setProjectIds]);
        }        

        if(!setTowerIds.isEmpty()) {
            mapTowerIdtoTowerInfo = new Map<Id, Tower__c>([Select Id, Name from Tower__c Where Id IN: setTowerIds]);
        }
        
        for(Project_Inventory__c objProjectInventory : lstTriggerNew) {
            strFlatInfo = String.isNotBlank(objProjectInventory.Flat_House_No__c) ? objProjectInventory.Flat_House_No__c : '';
            strFloorInfo = String.isNotBlank(objProjectInventory.Floor__c) ? objProjectInventory.Floor__c : '';
            strTowerInfo = mapTowerIdtoTowerInfo != null && !mapTowerIdtoTowerInfo.isEmpty() && mapTowerIdtoTowerInfo.containsKey(objProjectInventory.Tower__c) && String.isNotBlank(mapTowerIdtoTowerInfo.get(objProjectInventory.Tower__c).Name) ? mapTowerIdtoTowerInfo.get(objProjectInventory.Tower__c).Name : '';
            
            objProjectInventory.Inventory_Detail__c = strFlatInfo + ' - ' + strFloorInfo + ' - ' + strTowerInfo;
            objProjectInventory.Pincode__c = mapProjectIdtoProjectInfo != null && !mapProjectIdtoProjectInfo.isEmpty() && mapProjectIdtoProjectInfo.containsKey(objProjectInventory.Project__c) && String.isNotBlank(mapProjectIdtoProjectInfo.get(objProjectInventory.Project__c).Pincode__c) ? mapProjectIdtoProjectInfo.get(objProjectInventory.Project__c).Pincode__c : null;
        }
        
    }
    
    public static void onBeforeUpdate(Map<Id,Project_Inventory__c> mapTriggerNew, Map<Id,Project_Inventory__c> mapTriggerOld) {
        
    }
    
    public static void onBeforeDelete(List<Project_Inventory__c> lstTriggerOld) {
        for(Project_Inventory__c objProjectInventory : lstTriggerOld) {
            if(String.isNotBlank(objProjectInventory.Inventory_Approval_Status__c) && objProjectInventory.Inventory_Approval_Status__c == 'Approved') {
                objProjectInventory.addError('Approved Inventories cannot be deleted.');
            }
        }
    }
        
}