public class SourcingDetailTriggerHandler {
    public static void beforeInsert(List<Sourcing_Detail__c> sd){
        //SourcingDetailTriggerHelper.checkSourceDetail(sd); //Commented by Chitransh on 6th August 2019 for TIL-1276
        SourcingDetailTriggerHelper.checkSourceDetailType(sd);
        SourcingDetailTriggerHelper.duplicationDetection(sd); //Added by Chitransh on 6th August 2019 for TIL-1276
		SourcingDetailTriggerHelper.updateConnectorDetails(sd,null);//Added by Abhishek for Sourcing BRD
        SourcingDetailTriggerHelper.checkSubDealerParent(sd);//Added by Abhishek for Sourcing BRD
		SourcingDetailTriggerHelper.populateHierarchy(sd,null);//Added by Abhishek for TIL-00002442
    }
	public static void beforeUpdate(List<Sourcing_Detail__c> sourcingDetailList,  Map<Id, Sourcing_Detail__c> oldMap ){
        SourcingDetailTriggerHelper.updateConnectorDetails(sourcingDetailList,oldMap);//Added by Abhishek for Sourcing BRD
        SourcingDetailTriggerHelper.checkSubDealerParent(sourcingDetailList);//Added by Abhishek for Sourcing BRD
		SourcingDetailTriggerHelper.populateHierarchy(sourcingDetailList,oldMap);//Added by Abhishek for TIL-00002442
    }
	public static void afterInsert(List<Sourcing_Detail__c> sourcingDetailList){
        SourcingDetailTriggerHelper.populateRSMUserAndBREOnLA(sourcingDetailList, null); /*Added by Ankit for Strategic Alliance*/
    }
	public static void afterUpdate(Map<Id, Sourcing_Detail__c> newMap, Map<Id, Sourcing_Detail__c> oldMap) {
        SourcingDetailTriggerHelper.resetBREFields(newMap, oldMap); //Added by Vaishali for BRE
		SourcingDetailTriggerHelper.populateRSMUserAndBREOnLA(newMap.values(), oldMap); /*Added by Ankit for Strategic Alliance*/
    } 
    
    
}