public  class UCICDBTemplate
{
    public Id loanId {get;set;}
    public List<UCIC_Database__c> getlstDB()
    {
        List<UCIC_Database__c> ucicDBList;
        ucicDBList = [SELECT Name, Customer_Detail__c,Customer_Detail__r.Borrower__c, Customer_Detail__r.Borrower_Type_record__c, Customer_Detail__r.Name, Customer_Detail__r.Applicant_Type__c ,Loan_Application__c FROM UCIC_Database__c where Loan_Application__c =: loanId];

        return ucicDBList;
    }
}