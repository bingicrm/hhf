@isTest
public class CRM_LMSRepaymentSchedulePdfTest {
    
    @testSetup static void setup(){
        CRM_Utility.insertRestService('repayment');
        
        Loan_Application__c loanApp = new Loan_Application__c();
        loanApp.Loan_Number__c = '1012871';
        loanApp.Loan_Application_Number__c = 'HHFLUCHOU20000003632';
        insert loanApp;
        
        Loan_Application__c loanAppTwo = new Loan_Application__c();
        loanAppTwo.Loan_Number__c = '';
        loanAppTwo.Loan_Application_Number__c = '';
        insert loanAppTwo;
        
        case theCase = new case();
        theCase.Loan_Application__c = loanApp.Id;
        theCase.LD_Branch_ID__c = '1';
        insert theCase;
        
        case theCaseTwo = new case();
        theCaseTwo.Loan_Application__c = loanAppTwo.Id;
        theCaseTwo.LD_Branch_ID__c = '2';
        insert theCaseTwo;
    }
    
    static testMethod void testRepaymentSchedulePdf(){
        case theCase = [Select Id from case where Loan_Application__r.Loan_Application_Number__c='HHFLUCHOU20000003632' limit 1];
        case theCaseTwo = [Select Id from case where Loan_Application__r.Loan_Application_Number__c='' limit 1];
        
        PageReference pdf = Page.CRM_LMSRepaymentSchedulePDF;
        Test.setCurrentPage(pdf);
        pdf.getParameters().put('id', String.valueOf(theCase.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(theCase);
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new CRM_LmsMock());
        CRM_LMSRepaymentSchedulePDFController pdfControllerOne = new CRM_LMSRepaymentSchedulePDFController(sc);
        pdfControllerOne.today = date.today();
        
        pdf.getParameters().put('id', String.valueOf(theCaseTwo.Id));
        Test.setMock(HttpCalloutMock.class, new CRM_LmsMock());
        ApexPages.StandardController scTwo = new ApexPages.StandardController(theCaseTwo);
        CRM_LMSRepaymentSchedulePDFController pdfControllerTwo = new CRM_LMSRepaymentSchedulePDFController(scTwo);
        pdfControllerOne.logUtility();
        test.stopTest();
    }
}