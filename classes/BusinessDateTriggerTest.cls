@isTest
private class BusinessDateTriggerTest {

	private static testMethod void test1() {
	    Targets__c tar = new Targets__c(LOB__c = 'Open Market',
                                SM_Category__c = 'DST',
                                Target_Count__c = 5);
        insert tar;
	    
	    Achievement__c objAch = new Achievement__c();
	    objAch.Target__c = tar.Id;
	    objAch.Active__c = true;
	    objAch.Insurance__c = true;
	    //objAch.Month_Difference__c=2;
	    objAch.Business_Date__c = Date.valueOf('2020-03-22');
	    insert objAch;
	    
	    /*Achievement__c objAch1 = new Achievement__c();
	    objAch1.Target__c = tar.Id;
	    objAch1.Active__c = true;
	    objAch1.Insurance__c = true;
	    //objAch.Month_Difference__c=2;
	    objAch1.Business_Date__c = Date.valueOf('2020-03-19');
	    insert objAch1;
	    
	    Achievement__c objAch2 = new Achievement__c();
	    objAch2.Target__c = tar.Id;
	    objAch2.Active__c = true;
	    objAch2.Insurance__c = true;
	    //objAch.Month_Difference__c=2;
	    objAch2.Business_Date__c = Date.valueOf('2020-02-19');
	    insert objAch2;
	    
	    Achievement__c objAch3 = new Achievement__c();
	    objAch3.Target__c = tar.Id;
	    objAch3.Active__c = true;
	    objAch3.Insurance__c = true;
	    //objAch.Month_Difference__c=2;
	    objAch3.Business_Date__c = Date.valueOf('2020-01-19');
	    insert objAch3;*/
	    

	    LMS_Bus_Date__c objL = new LMS_Bus_Date__c();
	    objL.Current_Date__c = Date.valueOf('2020-03-22');
	    insert objL;
	    
	    LMS_Date_Sync__c objLMS = new LMS_Date_Sync__c();
	    objLMS.Current_Date__c = Date.valueOf('2020-03-22');
	    insert objLMS;
	    List<LMS_Date_Sync__c> lstdate = new List<LMS_Date_Sync__c>();
	    lstdate.add(objLMS);
	    BusinessDateTriggerHandler.checkM2();
	    BusinessDateTriggerHandler.uncheckM2();
	    BusinessDateTriggerHandler.dummyUpdate();
	    BusinessDateTriggerHandler.checkFTD(lstdate);
	    
	 
	}
	private static testMethod void test2() {
	    Targets__c tar = new Targets__c(LOB__c = 'Open Market',
                                SM_Category__c = 'DST',
                                Target_Count__c = 5);
        insert tar;
	    
	    Achievement__c objAch = new Achievement__c();
	    objAch.Target__c = tar.Id;
	    objAch.Active__c = true;
	    objAch.Insurance__c = true;
	    //objAch.Month_Difference__c=2;
	    objAch.Business_Date__c = Date.valueOf('2020-02-22');
	    insert objAch;
	    
	    LMS_Bus_Date__c objL = new LMS_Bus_Date__c();
	    objL.Current_Date__c = Date.valueOf('2020-03-22');
	    insert objL;
	    
	    BusinessDateTriggerHandler.checkM2();
	    BusinessDateTriggerHandler.uncheckM2();
	    BusinessDateTriggerHandler.dummyUpdate();
	    //BusinessDateTriggerHandler.checkFTD(lstdate);
	    
	 
	}
	
		private static testMethod void test3() {
	    Targets__c tar = new Targets__c(LOB__c = 'Open Market',
                                SM_Category__c = 'DST',
                                Target_Count__c = 5);
        insert tar;
	    
	    Achievement__c objAch = new Achievement__c();
	    objAch.Target__c = tar.Id;
	    objAch.Active__c = true;
	    objAch.Insurance__c = true;
	    //objAch.Month_Difference__c=2;
	    objAch.Business_Date__c = Date.valueOf('2020-01-22');
	    insert objAch;
	    LMS_Date_Sync__c objLMS = new LMS_Date_Sync__c();
	    objLMS.Current_Date__c = Date.valueOf('2020-03-22');
	    insert objLMS;
	    
	    
	    BusinessDateTriggerHandler.checkM2();
	    BusinessDateTriggerHandler.uncheckM2();
	    BusinessDateTriggerHandler.dummyUpdate();
	    //BusinessDateTriggerHandler.checkFTD(lstdate);
	    
	 
	}
	
	


}