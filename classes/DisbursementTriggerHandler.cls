public class DisbursementTriggerHandler{
    public static void beforeInsert(List<Disbursement__c> newList) {
		DisbursementTriggerHelper.checkDisbursalAmount(newList);//Added by Chitransh for TIL-1472//	
        DisbursementTriggerHelper.validateChargetoSchemeMapping(newList);
        for(Disbursement__c db : newList){
            if(db.recordType.Name == 'Others'){
              DisbursementTriggerHelper.validateDisbursedAmount(newList);  
            }           
        }       
    }
    
    public static void beforeUpdate(List<Disbursement__c> newList, Map<Id,Disbursement__c> oldMap){
    	/* Modified by 				: Shobhit Saxena(Deloitte)
    	   Modification Purpose 	: Each Charge will be validated against the applied Scheme for Loan Application
    	*/
		DisbursementTriggerHelper.checkDisbursalAmount(newList);//Added by Chitransh for TIL-1472//
    	List<Disbursement__c> lstDisbursementforChargeValidation = new List<Disbursement__c>();
		set<id> loanAppIds = new set<id>();//Added by Chitransh for TIL-1464 on [16-10-2019]
		Set<Id> loanAppIdSet = new Set<Id>(); //Added by Abhishek for Sourcing BRD
        List<Disbursement__c> disbursedDisbusementList = new List<Disbursement__c>(); //Added by Abhishek for Sourcing BRD
    	for(Disbursement__c objDisbursement : newList) {
		
			//Added By Abhishek for Sourcing BRD - START
            if(objDisbursement.Tranche_Disbursal_Date__c != oldMap.get(objDisbursement.Id).Tranche_Disbursal_Date__c) {
                loanAppIdSet.add(objDisbursement.Loan_Application__c);
                disbursedDisbusementList.add(objDisbursement);
            }
            //Added By Abhishek for Sourcing BRD - END
			
			/*************Added by Chitransh for TIL-1464 on [16-10-2019]************************/
            if(objDisbursement.Disbursal_Amount__c != oldMap.get(objDisbursement.Id).Disbursal_Amount__c){
                loanAppIds.add(objDisbursement.Loan_Application__c);
            }
            /*********End of Patch Added by Chitransh for TIL-1464 on [16-10-2019]*****************/
    		if(oldMap.get(objDisbursement.Id).ROC_Charges__c != objDisbursement.ROC_Charges__c || 
    			oldMap.get(objDisbursement.Id).CERSAI_Charges__c != objDisbursement.CERSAI_Charges__c ||
    			oldMap.get(objDisbursement.Id).Legal_Vetting__c != objDisbursement.Legal_Vetting__c ||
    			oldMap.get(objDisbursement.Id).Stamp_Duty__c != objDisbursement.Stamp_Duty__c ||
    			oldMap.get(objDisbursement.Id).PEMI__c != objDisbursement.PEMI__c) {
    				lstDisbursementforChargeValidation.add(objDisbursement);
    			}
    	}
		/*************Added by Chitransh for TIL-1464 on [16-10-2019]************************/
        system.debug('loanAppIds  ' + loanAppIds);
        if(!loanAppIds.isEmpty()){
            list<Loan_Application__c> listToUpdateRepaymentStatus = new list<Loan_Application__c>();
            list<Loan_Application__c> loanApplicationRecords = [Select id,Repayment_schedule_at_Disbursement_Check__c from Loan_Application__c where id in :loanAppIds ];
            for(Loan_Application__c loanApp : loanApplicationRecords){
                loanApp.Repayment_schedule_at_Disbursement_Check__c = false;
                listToUpdateRepaymentStatus.add(loanApp);
            }
            if(!listToUpdateRepaymentStatus.isEmpty()){
                try{
                    update listToUpdateRepaymentStatus;
                }
                catch(DMLException exp){
                    System.debug('The following exception has occured : ' + exp.getMessage());
                }
            }
            
        }
		//Added By Abhishek for Sourcing BRD - START
        if(!disbursedDisbusementList.isEmpty()) {
            DisbursementTriggerHelper.copySourcingDataToDisbursement(disbursedDisbusementList,loanAppIdSet);
        }
        //Added By Abhishek for Sourcing BRD - END
         /*********End of Patch Added by Chitransh for TIL-1464 on [16-10-2019]*****************/
    	System.debug('Debug Log for criteria records'+lstDisbursementforChargeValidation.size());
    	if(!lstDisbursementforChargeValidation.isEmpty()) {
    		DisbursementTriggerHelper.validateChargetoSchemeMapping(lstDisbursementforChargeValidation);
    	}
    }
	
	 //*************Added by Chitransh to update Disburse_Amount__c on LA post Tranche Cancelled [23-01-2020]***************//
    public static void afterUpdate(List<Disbursement__c> newList){
        if(!newList.isEmpty()){
            List<Disbursement__c> disbList = new List<Disbursement__c>();
            for(Disbursement__c disb : newList){
                if(disb.Tranche_Cancelled__c == true){
                    disbList.add(disb);
                }
            }
            if(!disbList.isEmpty())
            DisbursementTriggerHelper.updateLADisburseAmount(disbList);
        }
      }
}