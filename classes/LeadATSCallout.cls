public class LeadATSCallout 
{
    @future (callout = true)
    public static void sendLeadDatatoATS( Id leadId )
    {
        HttpResponse res; 
       try
        {

            Rest_Service__mdt restService = [ SELECT MasterLabel, QualifiedApiName , Service_EndPoint__c , 
                                                         Client_Password__c, Client_Username__c, Request_Method__c, Time_Out_Period__c  
                                                  FROM   Rest_Service__mdt
                                                  WHERE  MasterLabel = 'LeadATSIntegration'
                                                ];
            Digital_Lead_API_Metadata__mdt LeadMtd = [Select Id, DeveloperName, MasterLabel, Source_Detail__c, User_Name__c, Vendor_Password__c From Digital_Lead_API_Metadata__mdt where MasterLabel ='ATS'];

            Map<String,String> headerMap = new Map<String,String>();
            
            Blob headerValue = Blob.valueOf(restService.Client_Username__c + ':' + restService.Client_Password__c);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            headerMap.put('Authorization',authorizationHeader);
            headerMap.put('Username',restService.Client_Username__c);
            headerMap.put('Password',restService.Client_Password__c);
            headerMap.put('Content-Type','application/json');
            headerMap.put('Accept' , 'application/json');
            LeadATSRequest leadReqst = new LeadATSRequest();
            List<ATSRequest> lstLeadRqst = new List<ATSRequest>();
            List<Lead> leadlst=[SELECT Address,Address_Line_1__c,Address_Line_2__c,Agent_Name__c,Channel_Partner_Detail__c,CreatedDate,lastName,Alternate_Number__c,AnnualRevenue,Annual_Income__c,Any_Part_payment_made__c,Best_Response__c,Branch__c,Builder_Project_Name__c,Call_Status__c,City,City_Bulk_Upload__c,Company,Connector_Master_Name__c,Connector_Name__c,Contacted_Date__c,ConvertedAccountId,ConvertedContactId,ConvertedDate,ConvertedOpportunityId,Country,Co_Applicant_Current_EMI_Details__c,Co_Applicant_FirstName__c,Co_Applicant_LastName__c,Co_Applicant_Nationality__c,Co_Applicant_Pincode__c,Co_Applicant_Relationship__c,Co_Applicant__c,Cross_Sell_Partner_Name__c,Current_EMI_Details__c,Current_Location__c,Customer_Segment__c,Date_of_Birth__c,Dealer_Executive_1__c,Dealer_Executive_2__c,Declared_Income__c,Declared_Obligations__c,Description,Document_Status__c,DoNotCall,DSA_Name__c,DST_Name__c,DST_SM_Name__c,Email,EmailBouncedDate,EmailBouncedReason,Existing_Tenure__c,Fax,FirstName,Gender__c,GeocodeAccuracy,Group_Company_Name__c,Group_Company_Office_Location__c,Group_Company__c,HasOptedOutOfEmail,HasOptedOutOfFax,Hero_Dealer_Type_of_Lead__c,Hero_Dealer__c,Id,Income_Considered__c,Income_Details__c,IndividualId,Industry,IsConverted,IsDeleted,IsUnreadByOwner,Jigsaw,JigsawContactId,Last_contacted_date__c,Last_Response__c,Latitude,LeadSource,Lead_Converted_Custom__c,Lead_Id__c,Line_Of_Business__c,Loan_Application__c,Longitude,MasterRecordId,MiddleName,MobilePhone,Monthly_Income__c,Name,Name_DOB__c,Nationality__c,NumberOfEmployees,Number_of_attempts_made__c,Offer_Type__c,Office_Address__c,Organization_Name__c,OwnerId,Ownership__c,PAN_Card_Number__c,Phone,PhotoUrl,Pincode__c,PostalCode,Preferred_Time_of_Contact__c,Product_Scheme__c,Product__c,City__c,Property_City__c,Property_Details__c,Property_Identified__c,Property_Pincode__c,Qualified_Status_Time__c,Rating,RecordTypeId,Referral_Partner__c,Rejection_Reason__c,Remarks_Nurturing__c,Remarks__c,Requested_Loan_Amount__c,RSM__c,Salutation,Sanctioned_Date__c,SM_CBM__c,Source_Detail__c,State,Status,Strategic_Partner_Branch__c,Strategic_Partner_Name__c,Street,Title,Transaction_type__c,Type_of_lead__c,Vendor_Lead_Id__c,Vendor_Name__c,Vendor_Password__c,Website FROM Lead where Id =: leadId];
            if(leadlst.size() >0){
                for(Lead objLead: leadlst){
                    ATSRequest rqst = new ATSRequest();
                    rqst.RequestId = Utility.GenerateRandomRequestId().substring(0,16);
                    rqst.SourceId = 'SFDC';   
                    rqst.leadID = objLead.Id;
                    rqst.firstName = objLead.FirstName;
                    rqst.lastName = objLead.lastName;
                    rqst.mobile = objLead.MobilePhone;
                    rqst.offerType = objLead.Offer_Type__c;
                    rqst.createdDate = String.valueOf(objLead.CreatedDate);
                    rqst.sourceDetail = objLead.Source_Detail__c;
                    rqst.channelPartnerDetail = objLead.Channel_Partner_Detail__c;
                    rqst.product = objLead.Product__c;
                    rqst.declaredIncome = String.valueOf(objLead.Declared_Income__c);
                    rqst.Gender = objLead.Gender__c;
                    
                    rqst.Nationality = objLead.Nationality__c;
                    rqst.Email = objLead.Email;
                    rqst.city = objLead.City_Bulk_Upload__c;
                    rqst.requestedLoanAmount = String.valueOf(objLead.Requested_Loan_Amount__c);
                    rqst.userId =LeadMtd.User_Name__c;
                    rqst.password =LeadMtd.Vendor_Password__c;
                    rqst.branch= objLead.Branch__c;
                    rqst.agentName = objLead.Agent_Name__c;
                    rqst.callstatus = objLead.Call_Status__c;
                    rqst.creationRemarks = objLead.Remarks__c;
                    rqst.reason = objLead.Rejection_Reason__c;
                    rqst.dob = String.valueOf(objLead.Date_of_Birth__c);
                    rqst.customerSegment = objLead.Customer_Segment__c ;
                    rqst.monthlyIncome = String.valueOf(objLead.Monthly_Income__c) ;
                    rqst.coAppIncome = String.valueOf(objLead.Co_Applicant__c);
                    rqst.declaredObligations = String.valueOf(objLead.Declared_Obligations__c);
                    rqst.organizationName =objLead.Organization_Name__c;
                    rqst.propertyCity = objLead.Property_City__c;
                    rqst.bestResponse = objLead.Best_Response__c;
                    rqst.lastResponse = objLead.Last_Response__c;
                    rqst.lastContactedDate = String.valueOf(objLead.Last_contacted_date__c) ;
                    rqst.propertyAddress = objLead.Address_Line_1__c;
                    rqst.propertyArea = objLead.Address_Line_2__c;
                    rqst.propertyPincode= objLead.Pincode__c;
                    lstLeadRqst.add(rqst);
                    
                }
            }
            leadReqst.lstATSRequest=lstLeadRqst;
            

            String jsonRequest = Json.serialize(leadReqst);
            system.debug('jsonRequest::' + jsonRequest);
            res   = Utility.sendRequest(restService.Service_EndPoint__c,restService.Request_Method__c, jsonRequest , headerMap, Integer.valueOf(restService.Time_Out_Period__c) );
            
            system.debug('res::::::' + res);
/*** Added by Saumya For Lead Changes ****/
            List<Lead> lstLeadToUpdate = new List<Lead>();
            for(Lead objLead: leadlst){
                if( res != null){
                                system.debug('res.getBody(::::::' + res.getBody());
                   objLead.Lead_Send_to_ATS__c= true;
                   objLead.Lead_ATS_Response__c= res.getBody();
                   lstLeadToUpdate.add(objLead);
                }
            }
                                system.debug('lstLeadToUpdate::::::' + lstLeadToUpdate);
            if(lstLeadToUpdate.size() >0){
                update lstLeadToUpdate;
            }
            /*** Added by Saumya For Lead Changes ****/                        
            ATSResponseWrapper allResponse;
            if( res != null && res.getStatusCode() == 200 )
            {
                system.debug(res.getBody());
                allResponse= (ATSResponseWrapper)JSON.deserialize(res.getBody(), ATSResponseWrapper.class);
                system.debug(allResponse);
               // return allResponse;            

            }
            //LogUtility.createIntegrationLogs(jsonRequest,res,restService.Service_EndPoint__c); 
        }
        Catch(Exception e)
        {
            //system.debug('res' + res);
            //system.debug('hello from Exception body' + res.getBody());
            system.debug(e.getStackTraceString());
            
            if ( e.getStackTraceString().contains('JSON.deserialize') ) {
                system.debug('inside deserialize' );
                ErrorWrapper errorResp= (ErrorWrapper)JSON.deserialize(res.getBody(), ErrorWrapper.class);
                system.debug('errorResp' + errorResp);
                //return new ATSResponseWrapper(true, errorResp.error.errorMessage);
            } else {
               // return new ATSResponseWrapper(true, 'Please contact System Admin');
            }    
        }
        
        //return null;
    }

    public class ErrorWrapper{
        public String status;   //failure
        public cls_error error;
    }
    public class cls_error {
        public String errorCode;    //400
        public String errorType;    //Application
        public String errorMessage; //Full Recovery not Possible with the specified EMIs.Kindly increse the EMIs.
    }
    public class LeadATSRequest{
        public List<ATSRequest> lstATSRequest;
    }
    public class ATSRequest 
    {
        public String RequestId;
        public String SourceId;
        public String RepayType;
        public String leadID;
        public String firstName;
        public String lastName;
        public String mobile;
        public String offerType;
        public String createdDate;
        public String sourceDetail;
        public String channelPartnerDetail;
        public String product;
        public String declaredIncome;
        public String Gender;
        public String Nationality;
        public String Email;
        public String city;
        public String requestedLoanAmount;
        public String agentName;
        public String callstatus;
        public String creationRemarks;
        public String reason;
        public String dob;
        public String customerSegment;
        public String monthlyIncome;
        public String coAppIncome;
        public String declaredObligations;
        public String organizationName;
        public String propertyArea;
        public String propertyCity;
        public String propertyPincode;
        public String currentAddress;
        public String branch;
        public String numberOfAttempts;
        public String bestResponse;
        public String lastResponse;
        public String lastContactedDate;
        public String userId;
        public String password;
        public String fileName;
        public String fileFormat;
        public String fileData;
        public String fileInformation;
        public String propertyAddress;

    }

    public class ATSResponse 
    {

        public String CustomerIRR;
        public String BusinessIRR;
        public String InstlNo;
        public Decimal OpeningPrinciple;
        public String DueDate;
        public String InstAmt;
        public String AdvanceInstAmt;
        public String BrokenPeriodInt;
        public String Interest;
        public Decimal ClosingPrinciple;
        public String Days;
        public String ServiceTax;
        public String ServiceTaxRate;
        public String IntRate;
        public String AdvncFlag;
        public String DaysPerYear;
        public String Tds;
        public String Dsrate;
        public String Excess_interest;
        public String Principal;

    }

    public class ATSResponseWrapper
    {
        public boolean isError;
        public String errorMessage;

        public ATSResponseWrapper( boolean isError, String errorMessage)
        {
            this.isError = isError;
            this.errorMessage = errorMessage;
        }
    }
    
    public Class RESPONSE{
        @AuraEnabled
        public String message; 
         
    }
}