@isTest
public class TestDocumentListController{
    public static testMethod void fetchSubstage(){
        Account custObj = new Account();
        custObj.Name = 'Test Customer';
        custObj.Phone = '9999999999';
        database.insert(custObj);
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        database.insert(schemeObj);
        
        Loan_Application__c loanAppObj = new Loan_Application__c();
        loanAppObj.customer__c = custObj.id;
        loanAppObj.Loan_Application_Number__c = 'LA00001';
        loanAppObj.Loan_Purpose__c = '11';
        loanAppObj.scheme__c = schemeObj.id;
        loanAppObj.Transaction_type__c = 'PB';
        loanAppObj.Requested_Amount__c = 50;
        loanAppObj.StageName__c = 'Customer Onboarding';
        loanAppObj.sub_stage__c = 'Application Initiation';
        Database.insert(loanAppObj);
        
        Third_Party_Verification__c tpvObj = new Third_Party_Verification__c();
        tpvObj.Loan_Application__c = loanAppObj.id;
        tpvObj.Status__c = 'New';
        //Database.insert(tpvObj);
        //
        
        Attachment attach=new Attachment();   	
    	attach.Name='Test';
    	Blob bodyBlob=Blob.valueOf('Testing Body of Attachment');
    	attach.body=bodyBlob;
        attach.parentId=custObj.id;
        insert attach;
        
        Test.startTest();
        Loan_contact__c loanConObj = [select id, name, loan_applications__c from loan_contact__c where loan_applications__c =: loanAppObj.id];
        loanConObj.Mandatory_Fields_Completed__c = true;
        update loanConObj;
        
        Document_Master__c docMasObj = new Document_Master__c();
        docMasObj.Name = 'Test';
        docMasObj.Doc_Id__c = '123344';
        docMasObj.Approving_authority__c = 'Business Head';
        Database.insert(docMasObj);
        
        Document_type__c docTypeObj = new Document_type__c();
        docTypeObj.Name = 'Test Document Type';
        docTypeObj.Approving_authority__c    = 'Business Head';
        Database.insert(docTypeObj);
        
        DocumentType_Document_Mapping__c mapObj = new DocumentType_Document_Mapping__c();
        mapObj.Document_Master__c = docMasObj.id;
        mapObj.Document_Type__c = docTypeObj.id;
        Database.insert(mapObj);
        
        Document_Checklist__c docObj = new Document_Checklist__c();
        docObj.Loan_Applications__c = loanAppObj.id;
        docObj.status__c = 'Pending';
        docObj.document_type__c = docTypeObj.id;
        docObj.Document_Master__c = docMasObj.id;
        docObj.Loan_Contact__c = loanConObj.id;
        docObj.Express_Queue_Mandatory__c = true;
        docObj.Document_Collection_Mode__c = 'Photocopy';
        docObj.Screened_p__c = 'Yes';
        docObj.Sampled_p__c = 'Yes';
        Database.insert(docObj); 
        
        
        
        List<Document_checklist__c> docList = new list<document_checklist__c>();
        docList.add(docObj); 
        
        DocumentListController.fetchSubStage(loanAppObj.id);
        DocumentListController.fetchDocuments(loanAppObj.id);
        
        DocumentLIstController.getselectOptions(docObj,'Status__c','Application Initiation',true);
        DocumentListController.getAttachment(loanAppObj.id);
        //DocumentListController.validateChkList('docList');
        DocumentListController.queryAttachments(loanAppObj.id);
        //DocumentListController.saveDocUploadCheckbox(docObj.id);
        DocumentListController.saveTheFile(docObj.id,'Test File','base64Data','contentType');
        DocumentListController.saveChunk(loanAppObj.id,'Test File','base64Data','contentType',attach.id);
        Test.stopTest();
    }
    
        public static testMethod void fetchSubstagetwo(){
        Account custObj = new Account();
        custObj.Name = 'Test Customer';
        custObj.Phone = '9999999999';
        database.insert(custObj);
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        database.insert(schemeObj);
            
       Attachment attach=new Attachment();   	
    	attach.Name='Test';
    	Blob bodyBlob=Blob.valueOf('Testing Body of Attachment');
    	attach.body=bodyBlob;
        attach.parentId=custObj.id;
        insert attach;
        
        Test.startTest();
        Loan_Application__c loanAppObj = new Loan_Application__c();
        loanAppObj.customer__c = custObj.id;
        loanAppObj.Loan_Application_Number__c = 'LA00001';
        loanAppObj.Loan_Purpose__c = '11';
        loanAppObj.scheme__c = schemeObj.id;
        loanAppObj.Transaction_type__c = 'PB';
        loanAppObj.Requested_Amount__c = 50;
        loanAppObj.StageName__c = 'Customer Onboarding';
        loanAppObj.sub_stage__c = 'Application Initiation';
        Database.insert(loanAppObj);
        
        system.debug('All_Loan_Contact_Count__c!!!'+loanAppObj.All_Loan_Contact_Count__c);
        system.debug('Loan_Contact_Count__c$$$$$$'+loanAppObj.Loan_Contact_Count__c);
        
        Third_Party_Verification__c tpvObj = new Third_Party_Verification__c();
        tpvObj.Loan_Application__c = loanAppObj.id;
        tpvObj.Status__c = 'New';
        //Database.insert(tpvObj);     
        
        
        Loan_contact__c loanConObj = [select id, name, loan_applications__c, Mandatory_Fields_Completed__c from loan_contact__c where loan_applications__c =: loanAppObj.id];
        /*loanConObj.Mandatory_Fields_Completed__c = true;
        update loanConObj;*/
        
        Document_Master__c docMasObj = new Document_Master__c();
        docMasObj.Name = 'Test';
        docMasObj.Doc_Id__c = '123344';
        docMasObj.Approving_authority__c = 'Business Head';
        Database.insert(docMasObj);
        
        Document_type__c docTypeObj = new Document_type__c();
        docTypeObj.Name = 'Test Document Type';
        docTypeObj.Approving_authority__c    = 'Business Head';
        Database.insert(docTypeObj);
        
        DocumentType_Document_Mapping__c mapObj = new DocumentType_Document_Mapping__c();
        mapObj.Document_Master__c = docMasObj.id;
        mapObj.Document_Type__c = docTypeObj.id;
        Database.insert(mapObj);
        
        Document_Checklist__c docObj = new Document_Checklist__c();
        docObj.Loan_Applications__c = loanAppObj.id;
        docObj.status__c = 'Pre Disbursal';
        docObj.document_type__c = docTypeObj.id;
        docObj.Document_Master__c = docMasObj.id;
        docObj.Loan_Contact__c = loanConObj.id;
        docObj.Express_Queue_Mandatory__c = true;
        docObj.Document_Collection_Mode__c = 'Photocopy';
        docObj.Screened_p__c = 'Yes';
        docObj.Sampled_p__c = 'Yes';
        docObj.File_Check_Completed__c = false;
        Database.insert(docObj);     
        
        List<Document_checklist__c> docList = new list<document_checklist__c>();
        docList.add(docObj); 
        
        
        
        DocumentListController.fetchSubStage(loanAppObj.id);
        DocumentListController.fetchDocuments(loanAppObj.id);
        loanAppObj.sub_stage__c = 'Scan: Data Maker';
        //update loanAppObj;
        
        DocumentListController.fetchDocuments(loanAppObj.id);
        
        loanAppObj.sub_stage__c = 'Scan: Data Checker';
        //update loanAppObj;
         DocumentListController.fetchDocuments(loanAppObj.id);
        
        DocumentLIstController.getselectOptions(docObj,'Status__c','Scan: Data Maker',true);
        DocumentLIstController.getselectOptions(docObj,'Status__c','Credit Review',true);
        DocumentLIstController.getselectOptions(docObj,'Status__c','Document Approval',true);
        DocumentLIstController.getselectOptions(docObj,'Status__c','Customer Negotiation',true);
        DocumentLIstController.getselectOptions(docObj,'Status__c','COPS: Credit Operations Entry',true);
        DocumentLIstController.getselectOptions(docObj,'Status__c','Sales Approval',true);
        DocumentLIstController.getselectOptions(docObj,'Status__c','Docket Checker',true);
        
        DocumentListController.getAttachment(loanAppObj.id);
        //DocumentListController.validateChkList('docList');
        DocumentListController.queryAttachments(loanAppObj.id);
        DocumentListController.saveTheFile(docObj.id,'Test File','base6dData','contentType');
        DocumentListController.saveChunk(loanAppObj.id,'Test File','base6dData','contentType',attach.id);
        String paramjson='[{"strContactName":"tesst","lstDocumentCheckList":[{"id":"'+docObj.id+'"}]}]';
        DocumentListController.validateChkList(paramjson);
        String paramjson1='[{"strContactName":"tesst","lstDocumentCheckList":[{"id":""}]}]';
        DocumentListController.saveLoan(paramjson);
        DocumentListController.validateChkList(paramjson);
        Test.stopTest();
    }
    
    
}