public class AddressDetailPromoterTriggerHandler {
    public static void onBeforeInsert(List<Address_Detail_Promoter__c> lstTriggerNew) {
        List<Promoter__c> lstPromoterwithAddresses = new List<Promoter__c>();
        Set<Id> setPromoterIds = new Set<Id>();
        for(Address_Detail_Promoter__c objAddressDetail : lstTriggerNew) {
            if(String.isNotBlank(objAddressDetail.Promoter__c)) {
                setPromoterIds.add(objAddressDetail.Promoter__c);
            }
        }
        System.debug('Debug Log for setPromoterIds'+setPromoterIds);
        if(!setPromoterIds.isEmpty()) {
            lstPromoterwithAddresses = [Select Id, Name, (Select Id, Name from Address_Details__r Where Id NOT IN: lstTriggerNew) From Promoter__c Where ID IN: setPromoterIds];
            System.debug('Debug Log for lstPromoterwithAddresses'+lstPromoterwithAddresses.size());
            if(!lstPromoterwithAddresses.isEmpty()) {
                for(Promoter__c objProjectBuilder : lstPromoterwithAddresses) {
                    for(Address_Detail_Promoter__c objAddressDetail : lstTriggerNew) {
                        if(objAddressDetail.Promoter__c == objProjectBuilder.Id && !objProjectBuilder.Address_Details__r.isEmpty()) {
                            objAddressDetail.addError('Address already specified for the Promoter.');
                        }
                    }
                }
            }
        }
    }
}