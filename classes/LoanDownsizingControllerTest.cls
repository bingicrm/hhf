@IsTest
public class LoanDownsizingControllerTest {
    
    public static testMethod void testLoanDownSizePositive(){
        Document_Checklist__c docObj1 = new Document_Checklist__c();
        String strLoaId ;
        String strStatus ;
        String strCreditRemarks ;
        User u1 = [select Id,Name from User where Id = :UserInfo.getUserId()];
        Profile p=[SELECT id,name from profile where name=:'Sales Team'];
        UserRole r = [Select Id From UserRole Where Name =: 'BCM - Delhi 1.1' limit 1];
        User u = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,
            FCU__c =true,Role_in_Sales_Hierarchy__c = 'SM'
        ); 
        insert u;
        
        system.runAs(u){
            //test.startTest();
            Account custObj = new Account();
            custObj.Name = 'Test Customer';
            custObj.Phone = '9999999999';
            database.insert(custObj);
            
            scheme__c schemeObj = new scheme__c();
            schemeObj.Name = 'Scheme Name';
            schemeObj.Scheme_Group_ID__c = '12345';
            schemeObj.Scheme_Code__c = '12345';
            database.insert(schemeObj);
            
            Loan_Application__c loanAppObj = new Loan_Application__c();
            loanAppObj.customer__c = custObj.id;
            loanAppObj.Loan_Application_Number__c = 'LA00001';
            loanAppObj.Loan_Purpose__c = '11';
            loanAppObj.scheme__c = schemeObj.id;
            loanAppObj.Transaction_type__c = 'PB';
            loanAppObj.Requested_Amount__c = 50; 
            loanAppObj.Assigned_Sales_User__c = u.id;
            loanAppObj.Pending_Amount_for_Disbursement__c = 56000;
            Database.insert(loanAppObj);
            test.startTest();
            Loan_Application__c loanAppObj1 = new Loan_Application__c();
            loanAppObj1.customer__c = custObj.id;
            loanAppObj1.Loan_Application_Number__c = 'LA00002';
            loanAppObj1.Loan_Purpose__c = '11';
            loanAppObj1.scheme__c = schemeObj.id;
            loanAppObj1.Transaction_type__c = 'PB';
            loanAppObj1.Requested_Amount__c = 90; 
            loanAppObj1.Pending_Amount_for_Disbursement__c = 52000;
            Database.insert(loanAppObj1);
            
            Document_Master__c docMasObj = new Document_Master__c();
            docMasObj.Name = 'Downsizing Agreement';
            docMasObj.Doc_Id__c = '123344';
            docMasObj.Approving_authority__c = 'Business Head';
            Database.insert(docMasObj);
            
            Document_Master__c docMasObj1 = new Document_Master__c();
            docMasObj1.Name = 'PTM';
            docMasObj1.Doc_Id__c = '123345';
            docMasObj1.Approving_authority__c = 'Business Head';
            Database.insert(docMasObj1);
            
            Document_type__c docTypeObj = new Document_type__c();
            docTypeObj.Name = 'Test Document Type';
            docTypeObj.Approving_authority__c = 'Business Head';
            Database.insert(docTypeObj);
            List<Document_Type__c> lstDocTyp = new List<Document_Type__c>();
            Document_Type__c DType=new Document_Type__c(Name='Others');
            Document_Type__c DType1=new Document_Type__c(Name=Constants.PROPERTYPAPERS);
            lstDocTyp.add(DType);
            lstDocTyp.add(DType1);
            insert lstDocTyp;
            
            List<Document_Checklist__c> lstofDocs = new List<Document_Checklist__c>();
            Document_Checklist__c docObj = new Document_Checklist__c();
            docObj.Loan_Applications__c = loanAppObj.id;
            docObj.status__c = 'Pending';
            docObj.document_type__c = DType1.id;
            docObj.Document_Master__c = docMasObj.id;
            docObj.Express_Queue_Mandatory__c = true;
            docObj.Document_Collection_Mode__c = 'Photocopy';
            docObj.Screened_p__c = 'Yes';
            docObj.Sampled_p__c = 'Yes';
            lstofDocs.add(docObj);
            docObj1.Loan_Applications__c = loanAppObj1.id;
            docObj1.status__c = 'Pending';
            docObj1.document_type__c = DType1.id;
            docObj1.Document_Master__c = docMasObj.id;
            docObj1.Express_Queue_Mandatory__c = true;
            docObj1.Document_Collection_Mode__c = 'Photocopy';
            docObj1.Screened_p__c = 'Yes';
            docObj1.Sampled_p__c = 'Yes';
            lstofDocs.add(docObj1);
            insert lstofDocs;
            //Database.insert(docObj);
            
            //Create Document
            ContentVersion cv = new ContentVersion();
            cv.Title = 'Test Document';
            cv.PathOnClient = 'TestDocument.pdf';
            cv.VersionData = Blob.valueOf('Test Content');
            cv.IsMajorVersion = true;
            Insert cv;
            
            //Get Content Documents
            Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
            
            //Create ContentDocumentLink 
            ContentDocumentLink cdl = New ContentDocumentLink();
            cdl.LinkedEntityId = docObj.Id;
            cdl.ContentDocumentId = conDocId;
            cdl.shareType = 'V';
            Insert cdl;
            
            ContentDocumentLink cdll = New ContentDocumentLink();
            cdll.LinkedEntityId = docObj1.Id;
            cdll.ContentDocumentId = conDocId;
            cdll.shareType = 'V';
            Insert cdll;
            
            //Create ContentDocumentLink 
            ContentDocumentLink cdl1 = New ContentDocumentLink();
            cdl1.LinkedEntityId = loanAppObj.Id;//objDocCheck.Id;
            cdl1.ContentDocumentId = conDocId;
            cdl1.shareType = 'V';
            Insert cdl1;
            
            ContentDocumentLink cdl2 = New ContentDocumentLink();
            cdl2.LinkedEntityId = loanAppObj1.Id;//objDocCheck.Id;
            cdl2.ContentDocumentId = conDocId;
            cdl2.shareType = 'V';
            Insert cdl2;
            
            Attachment att=new Attachment();
            att.ParentId = loanAppObj.Id ;
            att.Name='test';
            att.Body=Blob.valueOf('test');
            insert att;
            
            Attachment att1=new Attachment();
            att1.ParentId = loanAppObj1.Id ;
            att1.Name='test';
            att1.Body=Blob.valueOf('test');
            insert att1;
            
            
            
            List<Loan_Downsizing__c> lstofLoanSize = new List<Loan_Downsizing__c>();
            Loan_Downsizing__c loanDownSize = new Loan_Downsizing__c(Loan_Application__c=loanAppObj.id,Downsize_Type__c='Partial Downsize',Status__c='Initiated',Remarks__c='Test',Downsizing_Amount__c=207,PDD_s_Reviewed__c=true);
            Loan_Downsizing__c loanDownSize1 = new Loan_Downsizing__c(Loan_Application__c=loanAppObj1.id,Downsize_Type__c='Full Downsize',Status__c='Initiated',Remarks__c='Test',Downsizing_Amount__c=loanAppObj1.Pending_Amount_for_Disbursement__c,PDD_s_Reviewed__c=true);
            lstofLoanSize.add(loanDownSize);
            lstofLoanSize.add(loanDownSize1);
            Database.insert(lstofLoanSize);
            
            Decimal dAmt = lstofLoanSize[0].Downsizing_Amount__c;
            Decimal dAmt1 = lstofLoanSize[1].Downsizing_Amount__c;
            String dtypes = lstofLoanSize[0].Downsize_Type__c;
            String dtypes1 = lstofLoanSize[1].Downsize_Type__c;
            String creationRemarks = 'Test Remarks';
            strLoaId = String.valueOf(lstofLoanSize[1].Id);
            strStatus = lstofLoanSize[1].Status__c;
            strCreditRemarks = lstofLoanSize[1].Remarks__c;
            
            Loan_Downsizing__c newloanDownSize = new Loan_Downsizing__c(Downsizing_Amount__c = dAmt, Downsize_Type__c = dtypes,Loan_Application__c=loanAppObj.id,Status__c = 'Initiated');
            insert newloanDownSize;
            Loan_Downsizing__c newloanDownSize1 = new Loan_Downsizing__c(Downsizing_Amount__c = loanAppObj1.Pending_Amount_for_Disbursement__c , Downsize_Type__c = dtypes1,Loan_Application__c=loanAppObj1.id,Status__c = 'Initiated');
            insert newloanDownSize1;
            Id downsizeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Downsizing').getRecordTypeId();
            
            LoanDownsizingController.getLoanApp(loanAppObj.id);
            LoanDownsizingController.partialDownsize(loanAppObj1.id, dtypes , dAmt, creationRemarks);
            LoanDownsizingController.getLoanDownsizeInfo(lstofLoanSize[0].id);
            docObj.Status__c = 'Uploaded';
            update docObj;
            
            test.stopTest();
            docObj1.Status__c = 'Uploaded';
            update docObj1;
            loanAppObj1.StageName__c='Loan Disbursal';
            loanAppObj1.Sub_Stage__c='Scan Checker';
            update loanAppObj1;
            docObj1.Scan_Check_Completed__c = true;
            update docObj1;
            try
            {
              LoanDownsizingController.approveRejectLoanDownsizing(strLoaId,strStatus,strCreditRemarks);   
            }
            catch(Exception ex)
            {
                system.assert(false, 'PDDs Review Should be Checked before Approving or Rejecting Loan Downsizing.');
            }
            
        }
        
        
    }
    
    public static testMethod void testLoanDownSizeNegative(){
        User u1 = [select Id,Name from User where Id = :UserInfo.getUserId()];
        Profile p=[SELECT id,name from profile where name=:'Sales Team'];
        UserRole r = [Select Id From UserRole Where Name =: 'BCM - Delhi 1.1' limit 1];
        User u = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,
            FCU__c =true,Role_in_Sales_Hierarchy__c = 'SM'
        ); 
        insert u;
        
        
        system.runAs(u){
            
            Account custObj = new Account();
            custObj.Name = 'Test Customer';
            custObj.Phone = '9999999999';
            database.insert(custObj);
            
            scheme__c schemeObj = new scheme__c();
            schemeObj.Name = 'Scheme Name';
            schemeObj.Scheme_Group_ID__c = '12345';
            schemeObj.Scheme_Code__c = '12345';
            database.insert(schemeObj);
            
            Loan_Application__c loanAppObj = new Loan_Application__c();
            loanAppObj.customer__c = custObj.id;
            loanAppObj.Loan_Application_Number__c = 'LA00001';
            loanAppObj.Loan_Purpose__c = '11';
            loanAppObj.scheme__c = schemeObj.id;
            loanAppObj.Transaction_type__c = 'PB';
            loanAppObj.Requested_Amount__c = 50; 
            loanAppObj.Assigned_Sales_User__c = u.id;
            loanAppObj.Pending_Amount_for_Disbursement__c = 56000;
            Database.insert(loanAppObj);
            test.startTest();
            Loan_Application__c loanAppObj1 = new Loan_Application__c();
            loanAppObj1.customer__c = custObj.id;
            loanAppObj1.Loan_Application_Number__c = 'LA00002';
            loanAppObj1.Loan_Purpose__c = '11';
            loanAppObj1.scheme__c = schemeObj.id;
            loanAppObj1.Transaction_type__c = 'PB';
            loanAppObj1.Requested_Amount__c = 90; 
            loanAppObj1.Pending_Amount_for_Disbursement__c = 52000;
            Database.insert(loanAppObj1);
            
            Document_Master__c docMasObj = new Document_Master__c();
            docMasObj.Name = 'Downsizing Agreement';
            docMasObj.Doc_Id__c = '123344';
            docMasObj.Approving_authority__c = 'Business Head';
            Database.insert(docMasObj);
            
            Document_type__c docTypeObj = new Document_type__c();
            docTypeObj.Name = 'Test Document Type';
            docTypeObj.Approving_authority__c = 'Business Head';
            Document_type__c docTypeObj1 = new Document_type__c();
            docTypeObj1.Name = 'Others';
            docTypeObj1.Approving_authority__c = 'Business Head';
            Database.insert(docTypeObj1);
            
            Document_Type__c DType=new Document_Type__c(Name='Test');
            insert DType;
            
            List<Document_Checklist__c> lstofDocs = new List<Document_Checklist__c>();
            Document_Checklist__c docObj = new Document_Checklist__c();
            docObj.Loan_Applications__c = loanAppObj.id;
            docObj.status__c = 'Pending';
            docObj.document_type__c = docTypeObj.id;
            docObj.Document_Master__c = docMasObj.id;
            docObj.Express_Queue_Mandatory__c = true;
            docObj.Document_Collection_Mode__c = 'Photocopy';
            docObj.Screened_p__c = 'Yes';
            docObj.Sampled_p__c = 'Yes';
            docObj.Scan_Check_Completed__c=true;
            lstofDocs.add(docObj);
            Database.insert(lstofDocs);
            
            List<Loan_Downsizing__c> lstofLoanSize = new List<Loan_Downsizing__c>();
            Loan_Downsizing__c loanDownSize = new Loan_Downsizing__c(Loan_Application__c=loanAppObj.id,Downsize_Type__c='Partial Downsize',Status__c='Initiated',Remarks__c='Test',Downsizing_Amount__c=207,PDD_s_Reviewed__c=true);
            lstofLoanSize.add(loanDownSize);
            Database.insert(lstofLoanSize);
            
            Decimal dAmt = lstofLoanSize[0].Downsizing_Amount__c;
            String dtypes = lstofLoanSize[0].Downsize_Type__c;
            String creationRemarks = 'Test Remarks';
            String strStatus = lstofLoanSize[0].Status__c;
            String strCreditRemarks = lstofLoanSize[0].Remarks__c;
            String strLoaId = String.valueOf(lstofLoanSize[0].Id);
            Loan_Downsizing__c newloanDownSize = new Loan_Downsizing__c(Downsizing_Amount__c = dAmt, Downsize_Type__c = dtypes,Loan_Application__c=loanAppObj.id,Status__c = 'Initiated');           
            insert newloanDownSize;
            
            LoanDownsizingController.partialDownsize(loanAppObj1.id, dtypes , dAmt, creationRemarks);
            LoanDownsizingController.fullDownsize(loanAppObj1.id, dtypes, creationRemarks);
            LoanDownsizingController.approveRejectLoanDownsizing(null,strStatus,null); 
            test.stopTest();
        }
    }
    
    public static testMethod void testfullDownsize(){
        User u1 = [select Id,Name from User where Id = :UserInfo.getUserId()];
        Profile p=[SELECT id,name from profile where name=:'Sales Team'];
        UserRole r = [Select Id From UserRole Where Name =: 'BCM - Delhi 1.1' limit 1];
        User u = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,
            FCU__c =true,Role_in_Sales_Hierarchy__c = 'SM'
        ); 
        insert u;
        
        
        system.runAs(u){
            
            Account custObj = new Account();
            custObj.Name = 'Test Customer';
            custObj.Phone = '9999999999';
            database.insert(custObj);
            
            scheme__c schemeObj = new scheme__c();
            schemeObj.Name = 'Scheme Name';
            schemeObj.Scheme_Group_ID__c = '12345';
            schemeObj.Scheme_Code__c = '12345';
            database.insert(schemeObj);
            
            Loan_Application__c loanAppObj = new Loan_Application__c();
            loanAppObj.customer__c = custObj.id;
            loanAppObj.Loan_Application_Number__c = 'LA00001';
            loanAppObj.Loan_Purpose__c = '11';
            loanAppObj.scheme__c = schemeObj.id;
            loanAppObj.Transaction_type__c = 'PB';
            loanAppObj.Requested_Amount__c = 50; 
            loanAppObj.Assigned_Sales_User__c = u.id;
            loanAppObj.Pending_Amount_for_Disbursement__c = 56000;
            Database.insert(loanAppObj);
            test.startTest();
            Loan_Application__c loanAppObj1 = new Loan_Application__c();
            loanAppObj1.customer__c = custObj.id;
            loanAppObj1.Loan_Application_Number__c = 'LA00002';
            loanAppObj1.Loan_Purpose__c = '11';
            loanAppObj1.scheme__c = schemeObj.id;
            loanAppObj1.Transaction_type__c = 'PB';
            loanAppObj1.Requested_Amount__c = 90; 
            loanAppObj1.Pending_Amount_for_Disbursement__c = 52000;
            Database.insert(loanAppObj1);
            
            Document_Master__c docMasObj = new Document_Master__c();
            docMasObj.Name = 'Downsizing Agreement';
            docMasObj.Doc_Id__c = '123344';
            docMasObj.Approving_authority__c = 'Business Head';
            Database.insert(docMasObj);
            
            Document_type__c docTypeObj = new Document_type__c();
            docTypeObj.Name = 'Test Document Type';
            docTypeObj.Approving_authority__c = 'Business Head';
            Document_type__c docTypeObj1 = new Document_type__c();
            docTypeObj1.Name = 'Others';
            docTypeObj1.Approving_authority__c = 'Business Head';
            Database.insert(docTypeObj1);
            
            Document_Type__c DType=new Document_Type__c(Name='Test');
            insert DType;
            
            List<Document_Checklist__c> lstofDocs = new List<Document_Checklist__c>();
            Document_Checklist__c docObj = new Document_Checklist__c();
            docObj.Loan_Applications__c = loanAppObj.id;
            docObj.status__c = 'Pending';
            docObj.document_type__c = docTypeObj.id;
            docObj.Document_Master__c = docMasObj.id;
            docObj.Express_Queue_Mandatory__c = true;
            docObj.Document_Collection_Mode__c = 'Photocopy';
            docObj.Screened_p__c = 'Yes';
            docObj.Sampled_p__c = 'Yes';
            docObj.Scan_Check_Completed__c=true;
            lstofDocs.add(docObj);
            Document_Checklist__c docObj1 = new Document_Checklist__c();
            docObj1.Loan_Applications__c = loanAppObj1.id;
            docObj1.status__c = 'Pending';
            docObj1.document_type__c = docTypeObj.id;
            docObj1.Document_Master__c = docMasObj.id;
            docObj1.Express_Queue_Mandatory__c = true;
            docObj1.Document_Collection_Mode__c = 'Photocopy';
            docObj1.Screened_p__c = 'Yes';
            docObj1.Sampled_p__c = 'Yes';
            Database.insert(lstofDocs);
            
            List<Loan_Downsizing__c> lstofLoanSize = new List<Loan_Downsizing__c>();
            Loan_Downsizing__c loanDownSize = new Loan_Downsizing__c(Loan_Application__c=loanAppObj.id,Downsize_Type__c='Full Downsize',Status__c='Initiated',Remarks__c='Test',Downsizing_Amount__c=207,PDD_s_Reviewed__c=true);
            lstofLoanSize.add(loanDownSize);
            Database.insert(lstofLoanSize);
            
            Decimal dAmt = lstofLoanSize[0].Downsizing_Amount__c;
            String dtypes = lstofLoanSize[0].Downsize_Type__c;
            String creationRemarks = 'Test Remarks';
            String strStatus = lstofLoanSize[0].Status__c;
            String strCreditRemarks = lstofLoanSize[0].Remarks__c;
            String strLoaId = String.valueOf(lstofLoanSize[0].Id);
            Loan_Downsizing__c newloanDownSize = new Loan_Downsizing__c(Downsizing_Amount__c = loanAppObj.Pending_Amount_for_Disbursement__c, Downsize_Type__c = dtypes,Loan_Application__c=loanAppObj.id,Status__c = 'Initiated',Creation_Remark__c='creationRemarks');           
            insert newloanDownSize;
            LoanDownsizingController.fullDownsize(loanAppObj1.id, dtypes, creationRemarks);
            
            test.stopTest();
            
        }
    }
    
}