public class PanValidationControllerAPF {
    
    @AuraEnabled
    public static String returnObjectName(Id strRecordId) {
        if(String.isNotBlank(strRecordId)) {
            String sObjName = strRecordId.getSObjectType().getDescribe().getName();
            if(sObjName == Constants.strProjectBuilderAPI) {
                return 'Builder';
            }
            else if(sObjName == Constants.strPromoterAPI) {
                return 'Promoter';
            }
        }
        return '';
    }
    
    @Auraenabled
    public static Project_Builder__c getCustomerDetails( Id projectBuilderId )
    {  
        Id userId = UserInfo.getUserId();
        User u = new User();
        u = [SELECT Id, Profile.Name 
            FROM User 
            WHERE Id =: userId];

        system.debug('======>user'+u.Profile.Name);
        if(u.Profile.Name != 'Document Scanner and Checker'
          ){
            List<Project_Builder__c> lstLoan = [Select Id, Builder_Name__r.PAN__c,Builder_Name__r.Salutation, Builder_Name__r.LMS_Existing_Customer__c,/*<!-- Added by Saumya For TIL-00000417 -->*/ Builder_Name__r.FirstName, Builder_Name__r.LastName, Builder_Name__r.MiddleName from Project_Builder__c where id=: projectBuilderId];     
            system.debug('@@@lstLoan ' + lstLoan );
             system.debug('@@@lstLoan[0] ' + lstLoan[0].Builder_Name__r.PAN__c );
            if (!lstLoan.isEmpty())
                
                return lstLoan[0];
            else 
                return null;    
        }
        else {
            return null;
        }
    }
    
    
    @auraenabled
    public static sObject validationPanDetails( Id strRecordId )
    {  
        if(String.isNotBlank(strRecordId)) {
            String sObjName = strRecordId.getSObjectType().getDescribe().getName();
            Id userId = UserInfo.getUserId();
            User u = new User();
            u = [SELECT Id, Profile.Name 
                FROM User 
                WHERE Id =: userId];
            if(sObjName == Constants.strProjectBuilderAPI) {
                Builder_Integrations__c objBuilderIntegration = new Builder_Integrations__c();
                PanValidationCallOutAPF pan = new PanValidationCallOutAPF();
                objBuilderIntegration =  (Builder_Integrations__c)pan.validatePan(strRecordId);
                system.debug('Response after Pan Callout =====>'+objBuilderIntegration);
                return objBuilderIntegration;
                
            }
            else if(sObjName == Constants.strPromoterAPI) {
                Promoter_Integrations__c objPromoterIntegration = new Promoter_Integrations__c();
                PanValidationCallOutAPF pan = new PanValidationCallOutAPF();
                objPromoterIntegration =  (Promoter_Integrations__c)pan.validatePan(strRecordId);
                system.debug('Response after Pan Callout =====>'+objPromoterIntegration);
                return objPromoterIntegration;
            }
        }
        return null;
    }
    
    @auraenabled
    public static Boolean copyPanDetails(Id idLoanContact)
    {  
        Id userId = UserInfo.getUserId();
        User u = new User();
        u = [SELECT Id, Profile.Name 
            FROM User 
            WHERE Id =: userId];

        
        if(u.Profile.Name != 'Document Scanner and Checker' ){
            //List<Account> lstAcc= [Select Id, Salutation, FirstName, LastName, MiddleName from Account where id=: customerId LIMIT 1];     
            List<Builder_Integrations__c > lstCustIn = [Select Id, PAN_Number__c,Project_Builder__r.Builder_Name__c, PAN_First_Name__c, PAN_Middle_Name__c, Pan_Last_Name__c, Pan_Title__c FROM Builder_Integrations__c  where Project_Builder__c = :idLoanContact and RecordType.Name='PAN Integration' ORDER BY CreatedDate DESC LIMIT 1];
            List<Account> lstAcc= [Select Id, Pan_Validated__c, PAN__c,Salutation, FirstName, LastName, MiddleName from Account where id=: lstCustIn[0].Project_Builder__r.Builder_Name__c LIMIT 1]; 
            
            //lstAcc[0].Salutation = lstCustIn[0].Pan_Title__c ;
            lstAcc[0].FirstName= lstCustIn[0].PAN_First_Name__c;
            lstAcc[0].MiddleName = lstCustIn[0].PAN_Middle_Name__c;
            lstAcc[0].LastName= lstCustIn[0].Pan_Last_Name__c;
            //lstAcc[0].PAN__c = lstCustIn[0].PAN_Number__c;
            lstAcc[0].Pan_Validated__c = true;
            
            try{
                system.debug('lstAcc '+lstAcc);
                update lstAcc;
                system.debug('@@lstAcc' + lstAcc);
                return true;
            } catch(Exception e){
                return false;
            }
                
             
        }
        else {
            return null;
        }
    }
}