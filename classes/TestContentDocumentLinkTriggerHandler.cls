@isTest

public class TestContentDocumentLinkTriggerHandler {
      	public static User u1;
    @isTest
    public static void testMethod1(){
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        ContentVersion contentVersionSelect = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersionInsert.Id LIMIT 1];
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        System.assertEquals(documents.size(), 1);
        ContentDocument ob = documents[0];
         Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='9999966667',
                                  Date_of_Incorporation__c=Date.newInstance(2018,11,11));
         Database.insert(Cob); 
        Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,
                                                           StageName__c='Customer Onboarding',
                                                           Sub_Stage__c='Tranche_File_Check',
                                                           Transaction_type__c='SC',Requested_Amount__c=10000,
                                                           Applicant_Customer_segment__c='Salaried',
                                                           Branch__c='test',Line_Of_Business__c='Open Market',
                                                           Loan_Purpose__c='20',
                                                           Requested_EMI_amount__c=1000,Approved_ROI__c=5,
                                                           Requested_Loan_Tenure__c=5,
                                                           Government_Programs__c='PMAY',
                                                           Loan_Engine_2_Output__c='STP',
                                                           Property_Identified__c=True);
                                                     

        Database.insert(LAob);
        Document_Checklist__c Dc = new Document_Checklist__c(Notes__c='raja',Loan_Applications__c=LAob.Id);
        insert Dc;
        ContentDocumentLink oa = new ContentDocumentLink(linkedEntityId =Dc.Id,contentdocumentId=ob.Id );
        insert oa;
        //
        Pincode__c p = new Pincode__c(Name='Test',City__c='Test',ZIP_ID__c='00' );
        Database.insert(p);
        Address__c address = new Address__c(Type_of_address__c = 'PERMNENT', Residence_Type__c ='PG',Pincode_LP__c =p.Id,Address_Line_1__c='test',Address_Line_2__c='test2',GeoLimits__c ='WITHIN GEO-LIMITS');
		Database.insert(address);
        Loan_Contact__c lcc = [select id from Loan_Contact__c where Customer__c =: cob.id limit 1];
        Profile pr =[SELECT id,name from profile where name=:'Sales Team'];
  		UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        
        u1 = new User(
            ProfileId = pr.Id,
            firstname ='first',
            LastName = 'lastt',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,Role_in_Sales_Hierarchy__c = 'SM'
        );

        Database.insert(u1);

        

        Profile pp = [SELECT id,name from profile where name=:'Sales Team' limit 1];
        u1 = [select id,firstname,lastname from user where ProfileId =: pp.Id limit 1];
        String fullname = UserInfo.getName();
        DSA_Master__c dsa = new DSA_Master__c(Name = fullname, BrokerID__c ='8127');
        Database.insert(dsa);
        dsa = [select id,name from DSA_Master__c where Name =:fullname ];
        Sourcing_Detail__c sdc = new Sourcing_Detail__c(Loan_Application__c = LAob.Id,DSA_Name__c =dsa.Id,Connector_Name__c=dsa.id, Sales_User_Name__c =u1.id); 
        
        Database.insert(sdc); 
		LAob.StageName__c = 'Operation Control';
        Database.update(LAob);

 		Third_Party_Verification__c tpv = new Third_Party_Verification__c(Loan_Contact__c =lcc.id, Loan_Application__c=LAob.Id, Status__c ='New', Address__c =address.id);
		Database.insert(tpv);


        List<ContentDocumentLink> records = [select id,linkedEntityId from ContentDocumentLink where Id =: oa.id ];
        system.debug('id: '+oa.LinkedEntityId);
        ContentDocumentLinkTriggerHandler.afterInsert(records);
 		oa = new ContentDocumentLink(linkedEntityId =tpv.Id,contentdocumentId=ob.Id );

        insert oa;
        records = [select id,linkedEntityId from ContentDocumentLink where Id =: oa.id ];
        ContentDocumentLinkTriggerHandler.afterInsert(records);



               

    }

}