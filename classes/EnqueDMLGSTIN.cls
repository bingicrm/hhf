public class EnqueDMLGSTIN implements Queueable{
    public Customer_Integration__c ciRecord;
    public String strRequest;
    public String strResponse;
    public String strURL;
    public EnqueDMLGSTIN(Customer_Integration__c ciRecord, String strRequest, String strResponse, String strURL){
        system.debug('Debug for ciRecord' + ciRecord);
        system.debug('Debug for strRequest' + strRequest);
        system.debug('Debug for strResponse' + strResponse);
        system.debug('Debug for strURL' + strURL);
        this.ciRecord = ciRecord;
        this.strRequest = strRequest;
        this.strResponse = strResponse;
        this.strURL = strURL;
    }
    
    public void execute(QueueableContext context) {
        
        insert ciRecord;
        system.debug('ciRecord' + ciRecord.Id);
        if(ciRecord.Loan_Contact__c != null){
            Loan_Contact__c loanContact = [Select id,GST_Initiated__c from Loan_Contact__c where id =: ciRecord.Loan_Contact__c limit 1];
            if(loanContact.Id != null){
                loanContact.GST_Initiated__c = true;
                update loanContact;
            }
            system.debug('Debug Log for Update ' + loanContact);
        }
        
       HttpResponse res = new HttpResponse();
        res.setBody(strResponse);  
        LogUtility.createIntegrationLogs(strRequest,res,strURL);
        // System.debug('Debug Log for response'+httpRes);
                  //  System.debug('Debug Log for response body'+httpRes.getBody());
                   // System.debug('Debug Log for response status'+httpRes.getStatus());
                   // System.debug('Debug Log for response status code'+httpRes.getStatusCode());
    }

}