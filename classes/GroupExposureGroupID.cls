public class GroupExposureGroupID {
    @AuraEnabled
    public static Loan_Contact__c getLoanCon(Id lconId){
        Loan_Contact__c lc = [SELECT Id, Loan_Applications__c, Loan_Applications__r.Sub_Stage__c FROM Loan_Contact__c WHERE Id=: lconId];
        if(lc.Loan_Applications__r.Sub_Stage__c == 'Disbursement Checker'){
            return [SELECT Id, Customer__c, Customer__r.Group_Id__c,Loan_Applications__c FROM Loan_Contact__c WHERE Id = :lconId ];
        }
        else{
            return null;
        }
    }

    @AuraEnabled
    public static string checkGroupID(Id LoanApplicationID){  
        //HomeLoanOnboardingCallout obj1 = new HomeLoanOnboardingCallout();
        Set<String> setGrpId = new Set<String>();
        String msg;
        List<Loan_Contact__c> lstLoanContact = new List<Loan_Contact__c>();
        String LMSLoanNumber;
        String ApplicantName;
        //String lappID = lcID.Loan_Applications__c;
        system.debug('Loan Apllication ID------>'+LoanApplicationID);
        //String lappID = [SELECT Id, Name, Loan_Applications__c FROM Loan_Contact__c WHERE Id=: objCustDetailRecId LIMIT 1].Loan_Applications__c;      
        List<Account> lstCustomer = new List<Account>();
        if(String.isNotBlank(LoanApplicationID)){
            lstLoanContact = [SELECT id, Name, Applicant_Type__c, Customer__r.Group_Id__c, Customer__c, Borrower__c, Loan_Applications__c, Loan_Applications__r.Loan_Number__c, Loan_Applications__r.Customer__r.Name 
                                                FROM Loan_Contact__c 
                                                WHERE Loan_Applications__c  = : LoanApplicationID AND (Applicant_Type__c = 'Applicant' OR Applicant_Type__c = 'Co- Applicant')];

        }
        system.debug('Here--->'+lstLoanContact);
        if(!lstLoanContact.isEmpty() && lstLoanContact.size() > 0){
            for(Loan_Contact__c lc : lstLoanContact){
                if(lc.Applicant_Type__c == 'Applicant') {
                    LMSLoanNumber = lc.Loan_Applications__r.Loan_Number__c;
                    ApplicantName = lc.Loan_Applications__r.Customer__r.Name;
                }
                System.debug('Debug Log for groupID of'+lc.Name+'is'+lc.Customer__r.Group_Id__c);
                setGrpId.add(lc.Customer__r.Group_Id__c);
                lstCustomer.add(lc.Customer__r);
            }    
        }
        system.debug('Here val'+setGrpId);
        System.debug('Debug Log for LMSLoanNumber'+LMSLoanNumber);
        System.debug('Debug Log for ApplicantName'+ApplicantName);
        
        if(setGrpId.size() == 1 && setGrpId.contains(null) ){
            String groupName;
            groupName = ApplicantName != null && LMSLoanNumber != null ? ApplicantName.subString(0,2)+'0'+LMSLoanNumber : '';
            for(Account cust : lstCustomer){
                cust.Group_Id__c = groupName;
            }
            //update lstCustomer;
            Database.SaveResult[] dbSaveResult = Database.update(lstCustomer, false);
            for(Database.SaveResult objDSR : dbSaveResult) {
                System.debug('Errors Occured:'+objDSR.getErrors());
                System.debug('Record Id'+objDSR.getId());
            }
            msg = groupName;
                        
        }

        else if(!setGrpId.isEmpty() && setGrpId.size() == 1 && !setGrpId.contains(null)){
            System.debug('Coming under this criteria. Set Not Empty and Set Size is 1');
            List<String> valueList = new List<String>(setGrpId);
            System.debug('valueList[0]'+valueList[0]);
            msg = valueList[0];
            
        }
        else if(!setGrpId.isEmpty() && setGrpId.size() > 1 && setGrpId.contains(null)){
            String groupId;
            for(Account cust : lstCustomer){
                if(cust.Group_Id__c != null){
                    groupId = cust.Group_Id__c;
                }
            }
            for(Account cus : lstCustomer){
                cus.Group_Id__c = groupId;
            }
            //update lstCustomer;
             Database.SaveResult[] dbSaveResult1 = Database.update(lstCustomer, false);
             for(Database.SaveResult dsr: dbSaveResult1 ) {
                System.debug('>>>Error'+dsr.getErrors());
             }
            msg = groupId;
            
        }
        else if(!setGrpId.isEmpty() && setGrpId.size() > 1){
            system.debug('Case 4---'+setGrpId);
            String groupName;
            groupName = ApplicantName != null && LMSLoanNumber != null ? ApplicantName.subString(0,2)+'0'+LMSLoanNumber : '';
            for(Account cust : lstCustomer){
                cust.Group_Id__c = groupName;
            }  
            system.debug('Updated Number---->'+groupName);
            //update lstCustomer;
            Database.SaveResult[] dbSaveResult2 = Database.update(lstCustomer, false);
            for(Database.SaveResult objDSR : dbSaveResult2) {
                System.debug('Errors Occured:'+objDSR.getErrors());
                System.debug('Record Id'+objDSR.getId());
            }
            msg = groupName;
            UpdateCustomer(setGrpId,groupName);
        }
        System.debug('Returning'+msg);
        return msg;
    }

    public static void UpdateCustomer(Set<String> groupIdSet, String groupName){
        List<Loan_Contact__c> lstLoanCon = new List<Loan_Contact__c>();
        lstLoancon = [SELECT Id, Name, Customer__c, Customer__r.Group_Id__c FROM Loan_Contact__c WHERE Customer__r.Group_Id__c =: groupIdSet];
        system.debug('Loan Cons--->'+lstLoancon);
        Set<String> groupId = new Set<String>(); 
        for(Loan_Contact__c lc : lstLoancon){
            if(lc.Customer__r.Group_Id__c != null){
                groupId.add(lc.Customer__r.Group_Id__c);
            }
            
        }
        system.debug('1---->'+groupId);
        List<Account> lstcon = new List<Account>();
        lstcon = [SELECT Id, Group_Id__c
                  FROM Account
                  WHERE Group_Id__c =: groupId AND Group_Id__c != null];
        System.debug('Let me knoww----->'+lstcon);
        for(Account cust : lstcon){
            if(cust.Group_Id__c != null){
                cust.Group_Id__c = groupName;
            }
        }
        system.debug('Code Updated to..?'+groupName);
        //update lstcon;
        Database.SaveResult[] dbSaveResult3 = Database.update(lstcon, false);
        for(Database.SaveResult objDSR : dbSaveResult3) {
            System.debug('Errors Occured:'+objDSR.getErrors());
            System.debug('Record Id'+objDSR.getId());
        }
    }
}