@isTest(SeeAllData=true)

public class ExpressQueueLACreationController_Test {
    public static testMethod void methodTest()
    {
        Scheme__c sch=new Scheme__c();
        sch.Product_Code__c='HLAP'; 
        sch.Scheme_Code__c='ELAP';
        sch.Scheme_Group_ID__c='Test';
        insert sch;
        
        Scheme__c sch1=new Scheme__c();
        sch1.Product_Code__c='HL'; 
        sch1.Scheme_Code__c='EL';
        sch1.Scheme_Group_ID__c='Test1';
        insert sch1;
        
        Insurance_Loan_Application__mdt mdt=new Insurance_Loan_Application__mdt();
        mdt.Language='en_US';
        mdt.Label='Insurance Loan Application';
        mdt.Insurance_Application_Check__c=false;
        
        Account acc=new Account();
        acc.CountryCode__c='+91';
        acc.Email__c='puser001@amamama.com';
        acc.Gender__c='M';
        acc.Mobile__c='9800765432';
        acc.Phone__c='9800765434';
        //acc.Rejection_Reason__c='Rejected';
        acc.Name='Test Account';
        // acc.Type=;
        insert acc;
        
        Account acc1=new Account();
        acc1.CountryCode__c='+91';
        acc1.Email__c='puser001@amamama.com';
        acc1.Gender__c='M';
        acc1.Mobile__c='9800765432';
        acc1.Phone__c='9800765434';
        acc1.Name='Test Account';
        insert acc1;
        
        Loan_Application__c app=new Loan_Application__c();
        app.Branch__c='Test';
        app.Cancellation_Reason__c='Cancelled';
        app.Approved_ROI__c=2;
        app.Approval_Taken__c=true;
        app.Scheme__c=sch.id;
        app.STP__c='N';
        app.Insurance_Loan_Application__c=false;
        app.Processing_Fee_Percentage__c=0;
        //app.Region__c='UTTAR PRADESH';
        app.StageName__c='Credit Decisioning';
        insert app;
        
        Loan_Contact__c loan=new Loan_Contact__c();
        //loan.Age__c= 20;
        loan.Date_Of_Birth__c= Date.newInstance(2012, 12, 9);
        loan.Email__c='puser001@amamama.com';
        loan.Fax_Number__c= '8970090';
        loan.Gender__c='M';
        loan.Customer__c=acc.id;
        loan.Loan_Applications__c= app.id;
        insert loan;
        
        
        Loan_Contact__c loan1=new Loan_Contact__c();
        //loan.Age__c= 20;
        loan1.Date_Of_Birth__c= Date.newInstance(2012, 12, 9);
        loan1.Email__c='puser001@amamama.com';
        loan1.Fax_Number__c= '8970090';
        loan1.Applicant_Type__c='';
        loan1.Borrower__c='';
        loan1.Customer_segment__c='';
        loan1.Income_Considered__c=true;
        loan1.Constitution__c='';
        loan1.Business_Date_CoApplicant__c= Date.newInstance(2012, 12, 9);
        loan1.Gender__c='M';
        loan1.Customer__c=acc1.id;
        loan1.Loan_Applications__c= app.id;
        insert loan1;
        
        Product2 prod=new Product2();
        prod.Name='Product';
        prod.ProductCode='201';
        prod.Description='Testing Product';
        prod.IsActive=true;
        insert prod;
        
        Local_Policies__c lop=new Local_Policies__c();
        lop.Name='Mumbai_Vashi_Gaothan';
        lop.Branch__c='Mumbai - Vashi';
        lop.IsActive__c=true;
        lop.Product_Scheme__c='All Schemes';
        insert lop;
        
        ExpressQueueLACreationController.LAWrapper objLaWrapper=new ExpressQueueLACreationController.LAWrapper();
        ExpressQueueLACreationController.LAWrapper objLaWrapper11=new ExpressQueueLACreationController.LAWrapper();
        objLaWrapper11.laId = app.Id;
        objLaWrapper11.product=sch.Id;
        objLaWrapper11.productName = 'Home Loan';
        objLaWrapper11.localpolicyValue = lop.Id;
        objLaWrapper11.customer = acc1.Id;
        objLaWrapper.product='Product';
        objLaWrapper.productName = 'Home Loan';
        objLaWrapper.localpolicyValue='230';
        List<ExpressQueueLACreationController.LAWrapper> lstLaWrap = new List<ExpressQueueLACreationController.LAWrapper>();
        lstLaWrap.add(objLaWrapper);
        String jsonobj=JSON.serialize(objLaWrapper);
        String jsonobj123=JSON.serialize(objLaWrapper11);
        String jsonobjList=JSON.serialize(lstLaWrap);
        
        ExpressQueueLACreationController.LAWrapper objLaWrapper1=new ExpressQueueLACreationController.LAWrapper();
        objLaWrapper1.product='Product';
        objLaWrapper1.productName = 'Loan Against Property';
        objLaWrapper1.localpolicyValue='230';
        String jsonobj2=JSON.serialize(objLaWrapper1);
        
        ExpressQueueLACreationController.CDWrapper objCDWrapper=new ExpressQueueLACreationController.CDWrapper();
        ExpressQueueLACreationController.CDWrapper objCDWrapper11=new ExpressQueueLACreationController.CDWrapper();
        objCDWrapper11.customer = acc1.Id;
        String jsonobj11=JSON.serialize(objCDWrapper11);
        List<ExpressQueueLACreationController.CDWrapper> lstCDWrap = new List<ExpressQueueLACreationController.CDWrapper>();
        String jsonobj1=JSON.serialize(objCDWrapper);
        String jsonobjList1=JSON.serialize(lstCDWrap);
        
        Test.startTest();
         //PageReference pageRef = Page.url;
         Opportunity  testOppty = new Opportunity();
         testOppty.name='testOppty';
         testOppty.AccountId=acc.id;
         testOppty.StageName='Open';
         testOppty.CloseDate=System.today();
         insert testOppty;
         //Test.setCurrentPage(pageRef);
        //pageRef.getParameters().put('id',testOppty.id);
        //ApexPages.StandardSetController sc = new ApexPages.StandardSetController(app);
        //Myclass  controller = new Myclass(sc);
        //System.assertNotEquals(null,controller.autoRun());
        //ExpressQueueLACreationController testCon=new ExpressQueueLACreationController(); 
        //ExpressQueueLACreationController.getPicklistOptions();
        ExpressQueueLACreationController.getLA(app.id);
        ExpressQueueLACreationController.getPrimaryApplicant(app.id);
        ExpressQueueLACreationController.fetchProduct(prod.id,'Product');
        ExpressQueueLACreationController.fetchLALocalPolicy(lop.id,'Mumbai');
        ExpressQueueLACreationController.fetchCustomer(loan.id,'puser');
        ExpressQueueLACreationController.fetchCustomer(loan1.id,'puser');
        ExpressQueueLACreationController.initial();
        ExpressQueueLACreationController.parseWrapper(jsonobj);
        ExpressQueueLACreationController.populateCustomerDetails(jsonobj11);
        ExpressQueueLACreationController.saveCD(jsonobj11,app.id);
        ExpressQueueLACreationController.saveLA(jsonobj,loan.id);
        ExpressQueueLACreationController.saveLA(jsonobj123,loan.id);
        
        //ExpressQueueLACreationController.saveLA(jsonobj2,loan1.id);
        ExpressQueueLACreationController.checkPANCIBILStatus(jsonobjList);
        ExpressQueueLACreationController.getPicklistOptions('Name','Account');
        //ExpressQueueLACreationController.delCDRecord(jsonobj1,app.id);
        ExpressQueueLACreationController.addCDRow(jsonobjList1);
        ExpressQueueLACreationController.fetchConstitutionList('Argument','text');
        ExpressQueueLACreationController.fetchConstitutionList('Argument','Constitution');
        //ExpressQueueLACreationController.closePanel();
        //ExpressQueueLACreationController obj = new ExpressQueueLACreationController(sc);
        Test.stopTest();
    }
}