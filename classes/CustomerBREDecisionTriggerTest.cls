/*=====================================================================
* Deloitte India
* Name:BREDecisionTriggerTest
* Description: This class is a test class and supports BREDecisionTrigger and BREDecisionTriggerHandler.
* Created Date: [03/04/2020]
* Created By:Saumya Bhasin(Deloitte India)
*
=====================================================================*/
@isTest
private class CustomerBREDecisionTriggerTest {

	/*public static testMethod void methodTest()
    {
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
         insert u;                        
                           
                           
        User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId 
                       from user where ID=:UserInfo.getUserId()];
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=u.Id;
        loggedUser.ManagerID=u.Id;
        update loggedUser;
        
     
        system.runAs(loggedUser){   
            Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
             Account acc=new Account();
            acc.CountryCode__c='+91';
            acc.Email__c='puser001@amamama.com';
            acc.Gender__c='M';
            acc.Mobile__c='9800765432';
            acc.Phone__c='9800765434';
            acc.PersonMobilePhone = '9800765434';
            //acc.Rejection_Reason__c='Rejected';
            //acc.Name='Test Account';
            acc.FirstName = 'abc';
            acc.LastName='def';
            acc.Salutation='Mr';
            acc.RecordTypeId = RecordTypeIdCustomer;
            // acc.Type=;
            insert acc;
         
                                
             Scheme__c sch= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                         Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                         Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                         Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                         Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
            insert sch;    
            LMS_Date_Sync__c abc = new LMS_Date_Sync__c(EOD_BOD__c='N',Current_Date__c=Date.Today());
            insert abc;
            Id RecordTypeIdLoanApplication = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('COPS: Credit Operations Entry').getRecordTypeId();
        
            Loan_Application__c app = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=sch.Id,Loan_Number__c='124',
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',StageName__c='Customer Onboarding',
                                                                        RecordTypeId=RecordTypeIdLoanApplication,
                                                                        Business_Date_Created__c=System.today(),
                                                                        Approved_Loan_Amount__c=100000,
                                                                        Processing_Fee_Percentage__c=1,Mode_of_Repayment__c='NACH',
                                                                        Repayment_Start_Date__c=Date.Today().addDays(100),
                                                                        Sanctioned_Disbursement_Type__c='Multiple Tranche',Property_Identified__c = true);
            insert app;
            
            Loan_Contact__c lc =[SELECT Id,Borrower__c from Loan_Contact__c where Loan_Applications__c=:app.Id];
            lc.Borrower__c='1';
            //lc.Category__c='1';
            lc.Constitution__c = '20';
            lc.Customer_segment__c = '1';
            lc.BRE_Record__c = true;
            lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc.Marital_Status__c = 'M';
            lc.Gross_Salary__c = 1000;
            lc.Basic_Pay__c = 1000;
            lc.DA__c = 1000;
            //lc.Total_Fixed_Pay__c = 1000;
            //lc.Variable_Income_Monthly__c = 1000;
            lc.Net_Income__c = 1000;
            lc.PF__c = 1000;
            lc.TDS__c = 1000;
            lc.Current_Organization_Work_Ex_in_Months__c=12;
            lc.Total_Work_Experience__c = 12;
            lc.Customer_Details_Verified__c= true;
            lc.Voter_ID_Number__c = '123456789';
            lc.Passport_Number__c = '123456789';
            lc.PAN_Number__c = 'BAFPV6543A';
            lc.Aadhaar_Number__c = '898518762736';
            lc.Income_Considered__c = true;
            
            update lc;
            Pincode__c pin = new Pincode__c(Name='11044',ZIP_ID__c='45',City__c='Ludiana',State_Text__c='Punjab',Country_Text__c='India');
            insert pin;   
            system.debug('pin '+pin);
            Address__c add = new Address__c(Residence_Type__c= 'G',Type_of_address__c='OFFRES',Address_Line_1__c='11111111',Address_Line_2__c='127737',Pincode_LP__c=pin.Id,Loan_Contact__c=lc.Id);
            insert add;
            Customer_Integration__c cust = new Customer_Integration__c(CIBIL_Score__c='5',recordTypeId=Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId(), Loan_Application__c = app.Id, Loan_Contact__c=lc.Id,BRE_Recent__c=true);
            insert cust;
            Customer_Integration__c cust1 = new Customer_Integration__c(CIBIL_Score__c='5',recordTypeId=Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId(), Loan_Application__c = app.Id, Loan_Contact__c=lc.Id,BRE_Recent__c=true);
            insert cust1;
            Sourcing_Detail__c sc = new Sourcing_Detail__c(Loan_Application__c= app.Id, Source_Code__c= '19',Lead_Sourcing_Detail__c='HHFL Website',recordTypeId=Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('Digital Direct').getRecordTypeId());
            insert sc;
            app.StageName__c='Operation Control';
            app.Sub_Stage__c  ='Scan: Data Maker';
            //app.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('File check Scanner').getRecordTypeId();
            update app;
            //Property__c prop = new Property__c(First_Property_Owner__c=lc.Id, Loan_Application__c=app.Id,Address_Line_1__c='17827872',Area__c=12,Floor__c='1',Area_Unit__c='Sqft',Flat_No_House_No__c='1',Type_Property__c='Residential',Pincode_LP__c=pin.Id);
            //insert prop;
			//Sanction_Condition_Master__c objSCM = new Sanction_Condition_Master__c();
			//objSCM.Name = 'Others';
			//insert objSCM;
            //LMS_Bus_Date__c objL = [Select Id, Current_Date__c from LMS_Bus_Date__c];
            //objL.Current_Date__c = Date.valueOf(System.now());
            //update objL;
			
            Credit_Deviation_Master__c objCDM = new Credit_Deviation_Master__c();
            objCDM.Deviation_Code__c = '1';
            objCDM.Status__c = TRUE;
            objCDM.Product__c = 'CF';
            insert objCDM;
            
            Credit_Deviation_Master__c objCDM1 = new Credit_Deviation_Master__c();
            objCDM1.Deviation_Code__c = '0';
            objCDM1.Status__c = TRUE;
            objCDM.Product__c = 'CF';
            insert objCDM1;
            Mitigant__c objM = new Mitigant__c();
            objM.Active__c = true;
            objM.Mitigant_ID__c = '123';
            objM.Name = 'Others';
            insert objM;
            
            Applicable_Deviation__c objAD = new Applicable_Deviation__c();
            objAD.Credit_Deviation_Master__c = objCDM.Id;
            objAD.Mitigant__c = objM.Id;
            objAD.Loan_Application__c = app.Id;
            objAD.Customer_Integration__c = cust.Id;
            objAD.Deviation_Code__c ='0';
            objAD.Auto_Deviation__c =true;
            insert objAD;
            
            RunDeleteTriggerOnApplicableDeviation__c RunDeleteTriggerOnApplicableDeviation = new RunDeleteTriggerOnApplicableDeviation__c();
            RunDeleteTriggerOnApplicableDeviation.Name = 'RunDeleteTriggerOnApplicableDeviation' ;
            RunDeleteTriggerOnApplicableDeviation.byPassTrigger__c = false;
            insert RunDeleteTriggerOnApplicableDeviation;
            
			
			
			CIBIL_Decision_Detail__c objAppCIBIL = new CIBIL_Decision_Detail__c();
			objAppCIBIL.Reason_Code__c = '0';
			//objAppCIBIL.Product_Code__c = sch.Scheme_Code__c;
			objAppCIBIL.BRE_II__c = true;
			objAppCIBIL.Loan_Application__c = app.Id;
			objAppCIBIL.Customer_Integration__c = cust1.Id;

			insert objAppCIBIL;
			
		
            Test.startTest();
            
            Test.stopTest();
        }    
    }*/
    
    public static testMethod void methodTest1()
    {
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
         insert u;                        
                           
                           
        User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId 
                       from user where ID=:UserInfo.getUserId()];
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=u.Id;
        loggedUser.ManagerID=u.Id;
        update loggedUser;
        
     
        system.runAs(loggedUser){   
            Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
             Account acc=new Account();
            acc.CountryCode__c='+91';
            acc.Email__c='puser001@amamama.com';
            acc.Gender__c='M';
            acc.Mobile__c='9800765432';
            acc.Phone__c='9800765434';
            acc.PersonMobilePhone = '9800765434';
            //acc.Rejection_Reason__c='Rejected';
            //acc.Name='Test Account';
            acc.FirstName = 'abc';
            acc.LastName='def';
            acc.Salutation='Mr';
            acc.RecordTypeId = RecordTypeIdCustomer;
            // acc.Type=;
            insert acc;
         
                                
             Scheme__c sch= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                         Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                         Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                         Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                         Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
            insert sch;  
            Scheme__c sch1= new Scheme__c(Name='Hoem Loan',Scheme_Code__c='HL',Scheme_Group_ID__c='7',
                                         Scheme_ID__c=7,Product_Code__c='HL',Max_ROI__c=10,Min_ROI__c=5,
                                         Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                         Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                         Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
            insert sch1;  
            /*LMS_Date_Sync__c abc = new LMS_Date_Sync__c(EOD_BOD__c='N',Current_Date__c=Date.Today());
            insert abc;*/
            Id RecordTypeIdLoanApplication = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('COPS: Credit Operations Entry').getRecordTypeId();

            Loan_Application__c app = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=sch.Id,Loan_Number__c='124',
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',StageName__c='Customer Onboarding',
                                                                        RecordTypeId=RecordTypeIdLoanApplication,
                                                                        Business_Date_Created__c=System.today(),
                                                                        Approved_Loan_Amount__c=100000,
                                                                        Processing_Fee_Percentage__c=1,Mode_of_Repayment__c='NACH',
                                                                        Repayment_Start_Date__c=Date.Today().addDays(100),
                                                                        Sanctioned_Disbursement_Type__c='Multiple Tranche',Property_Identified__c = true);
            insert app;
            
            Loan_Application__c app1 = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=sch1.Id,Loan_Number__c='124',
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',StageName__c='Customer Onboarding',
                                                                        RecordTypeId=RecordTypeIdLoanApplication,
                                                                        Business_Date_Created__c=System.today(),
                                                                        Approved_Loan_Amount__c=100000,
                                                                        Processing_Fee_Percentage__c=1,Mode_of_Repayment__c='NACH',
                                                                        Repayment_Start_Date__c=Date.Today().addDays(100),
                                                                        Sanctioned_Disbursement_Type__c='Multiple Tranche',Property_Identified__c = true);
            insert app1;
            Test.startTest();
            Loan_Contact__c lc =[SELECT Id,Borrower__c from Loan_Contact__c where Loan_Applications__c=:app.Id];
            /*lc.Borrower__c='1';
            //lc.Category__c='1';
            lc.Constitution__c = '20';
            lc.Customer_segment__c = '1';
            lc.BRE_Record__c = true;
            lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc.Marital_Status__c = 'M';
            lc.Gross_Salary__c = 1000;
            lc.Basic_Pay__c = 1000;
            lc.DA__c = 1000;
            //lc.Total_Fixed_Pay__c = 1000;
            //lc.Variable_Income_Monthly__c = 1000;
            lc.Net_Income__c = 1000;
            lc.PF__c = 1000;
            lc.TDS__c = 1000;
            lc.Current_Organization_Work_Ex_in_Months__c=12;
            lc.Total_Work_Experience__c = 12;
            lc.Customer_Details_Verified__c= true;
            lc.Voter_ID_Number__c = '123456789';
            lc.Passport_Number__c = '123456789';
            lc.PAN_Number__c = 'BAFPV6543A';
            lc.Aadhaar_Number__c = '898518762736';
            lc.Income_Considered__c = true;
            
            update lc;*/
            
            Loan_Contact__c lc1 =[SELECT Id,Borrower__c from Loan_Contact__c where Loan_Applications__c=:app1.Id];
            lc1.Borrower__c='1';
            //lc.Category__c='1';
            lc1.Constitution__c = '20';
            lc1.Customer_segment__c = '1';
            lc1.BRE_Record__c = true;
            lc1.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc1.Marital_Status__c = 'M';
            lc1.Gross_Salary__c = 1000;
            lc1.Basic_Pay__c = 1000;
            lc1.DA__c = 1000;
            //lc.Total_Fixed_Pay__c = 1000;
            //lc.Variable_Income_Monthly__c = 1000;
            lc1.Net_Income__c = 1000;
            lc1.PF__c = 1000;
            lc1.TDS__c = 1000;
            lc1.Current_Organization_Work_Ex_in_Months__c=12;
            lc1.Total_Work_Experience__c = 12;
            lc1.Customer_Details_Verified__c= true;
            lc1.Voter_ID_Number__c = '123456789';
            lc1.Passport_Number__c = '123456789';
            lc1.PAN_Number__c = 'BAFPV6543A';
            lc1.Aadhaar_Number__c = '898518762736';
            lc1.Income_Considered__c = true;
            
            update lc1;
            /*Pincode__c pin = new Pincode__c(Name='11044',ZIP_ID__c='45',City__c='Ludiana',State_Text__c='Punjab',Country_Text__c='India');
            insert pin;   
            system.debug('pin '+pin);
            Address__c add = new Address__c(Residence_Type__c= 'G',Type_of_address__c='OFFRES',Address_Line_1__c='11111111',Address_Line_2__c='127737',Pincode_LP__c=pin.Id,Loan_Contact__c=lc.Id);
            insert add;
            Customer_Integration__c cust = new Customer_Integration__c(CIBIL_Score__c='5',recordTypeId=Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId(), Loan_Application__c = app.Id, Loan_Contact__c=lc.Id,BRE_Recent__c=true);
            insert cust;
            Sourcing_Detail__c sc = new Sourcing_Detail__c(Loan_Application__c= app.Id, Source_Code__c= '19',Lead_Sourcing_Detail__c='HHFL Website',recordTypeId=Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('Digital Direct').getRecordTypeId());
            insert sc;
            app.StageName__c='Operation Control';
            app.Sub_Stage__c  ='Scan: Data Maker';
            //app.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('File check Scanner').getRecordTypeId();
            update app;*/
            /*Property__c prop = new Property__c(First_Property_Owner__c=lc.Id, Loan_Application__c=app.Id,Address_Line_1__c='17827872',Area__c=12,Floor__c='1',Area_Unit__c='Sqft',Flat_No_House_No__c='1',Type_Property__c='Residential',Pincode_LP__c=pin.Id);
            insert prop;*/
			/*Sanction_Condition_Master__c objSCM = new Sanction_Condition_Master__c();
			objSCM.Name = 'Others';
			insert objSCM;
            LMS_Bus_Date__c objL = [Select Id, Current_Date__c from LMS_Bus_Date__c];
            objL.Current_Date__c = Date.valueOf(System.now());
            update objL;*/
			
			Customer_Integration__c cust = new Customer_Integration__c(CIBIL_Score__c='5',recordTypeId=Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId(), Loan_Application__c = app1.Id, Loan_Contact__c=lc1.Id,BRE_Recent__c=true);
            insert cust;
            Customer_Integration__c cust1 = new Customer_Integration__c(CIBIL_Score__c='5',recordTypeId=Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId(), Loan_Application__c = app.Id, Loan_Contact__c=lc.Id,BRE_Recent__c=true);
            insert cust1;
            Credit_Deviation_Master__c objCDM = new Credit_Deviation_Master__c();
            objCDM.Deviation_Code__c = '0';
            objCDM.Status__c = TRUE;
            objCDM.Product__c = 'CF';
            insert objCDM;
            
            Credit_Deviation_Master__c objCDM1 = new Credit_Deviation_Master__c();
            objCDM1.Deviation_Code__c = '0';
            objCDM1.Status__c = TRUE;
            objCDM1.Product__c = 'HL';
            insert objCDM1;
            
            Credit_Deviation_Master__c objCDM2 = new Credit_Deviation_Master__c();
            objCDM2.Deviation_Code__c = '1';
            objCDM2.Status__c = TRUE;
            objCDM2.Product__c = 'CF';
            insert objCDM2;

            
            Mitigant__c objM = new Mitigant__c();
            objM.Active__c = true;
            objM.Mitigant_ID__c = '123';
            objM.Name = 'Others';
            insert objM;
            
            Applicable_Deviation__c objAD = new Applicable_Deviation__c();
            objAD.Credit_Deviation_Master__c = objCDM.Id;
            objAD.Mitigant__c = objM.Id;
            objAD.Loan_Application__c = app1.Id;
            objAD.Deviation_Code__c ='0';
            objAD.Customer_Integration__c =cust.Id;
            objAD.Customer_Details__c = lc1.id;
            insert objAD;
            
            Applicable_Deviation__c objAD1 = new Applicable_Deviation__c();
            objAD1.Credit_Deviation_Master__c = objCDM.Id;
            objAD1.Mitigant__c = objM.Id;
            objAD1.Loan_Application__c = app.Id;
            objAD1.Deviation_Code__c ='0';
            objAD1.Customer_Integration__c =cust.Id;
            objAD1.Customer_Details__c = lc.id;
            insert objAD1;
            
			/*Application_CIBIL_Decision_Detail__c objAppCIBIL = new Application_CIBIL_Decision_Detail__c();
			objAppCIBIL.Deviation_Code__c = '0';
			objAppCIBIL.BRE_II__c = true;
			objAppCIBIL.Loan_Application__c = app.Id;
			objAppCIBIL.Customer_Integration__c = cust1.Id;*/
			
			BRE_Decision_Code__c bobj = new BRE_Decision_Code__c();

			bobj.Reason_Code__c = '1';
			bobj.Active__c = true;
			insert bobj;
			
			CIBIL_Decision_Detail__c objAppCIBIL = new CIBIL_Decision_Detail__c();
			objAppCIBIL.Reason_Code__c = '1';
			//objAppCIBIL.Product_Code__c = sch.Scheme_Code__c;
			objAppCIBIL.BRE_II__c = true;
			objAppCIBIL.Loan_Application__c = app.Id;
			objAppCIBIL.Customer_Integration__c = cust1.Id;
            objAppCIBIL.Customer_Detail__c = lc1.id;
			insert objAppCIBIL;
			
			
            
            
            
            Test.stopTest();
        } 
    }
        
        public static testMethod void methodTest2()
    {
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
         insert u;                        
                           
                           
        User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId 
                       from user where ID=:UserInfo.getUserId()];
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=u.Id;
        loggedUser.ManagerID=u.Id;
        update loggedUser;
        
     
        system.runAs(loggedUser){   
            Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
             Account acc=new Account();
            acc.CountryCode__c='+91';
            acc.Email__c='puser001@amamama.com';
            acc.Gender__c='M';
            acc.Mobile__c='9800765432';
            acc.Phone__c='9800765434';
            acc.PersonMobilePhone = '9800765434';
            //acc.Rejection_Reason__c='Rejected';
            //acc.Name='Test Account';
            acc.FirstName = 'abc';
            acc.LastName='def';
            acc.Salutation='Mr';
            acc.RecordTypeId = RecordTypeIdCustomer;
            // acc.Type=;
            insert acc;
         
                                
             Scheme__c sch= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                         Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                         Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                         Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                         Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
            insert sch;  
            Scheme__c sch1= new Scheme__c(Name='Hoem Loan',Scheme_Code__c='HL',Scheme_Group_ID__c='7',
                                         Scheme_ID__c=7,Product_Code__c='HL',Max_ROI__c=10,Min_ROI__c=5,
                                         Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                         Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                         Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
            insert sch1;  
            /*LMS_Date_Sync__c abc = new LMS_Date_Sync__c(EOD_BOD__c='N',Current_Date__c=Date.Today());
            insert abc;*/
            Id RecordTypeIdLoanApplication = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('COPS: Credit Operations Entry').getRecordTypeId();

            Loan_Application__c app = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=sch.Id,Loan_Number__c='124',
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',StageName__c='Customer Onboarding',
                                                                        RecordTypeId=RecordTypeIdLoanApplication,
                                                                        Business_Date_Created__c=System.today(),
                                                                        Approved_Loan_Amount__c=100000,
                                                                        Processing_Fee_Percentage__c=1,Mode_of_Repayment__c='NACH',
                                                                        Repayment_Start_Date__c=Date.Today().addDays(100),
                                                                        Sanctioned_Disbursement_Type__c='Multiple Tranche',Property_Identified__c = true);
            insert app;
            
            Loan_Application__c app1 = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=sch1.Id,Loan_Number__c='124',
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',StageName__c='Customer Onboarding',
                                                                        RecordTypeId=RecordTypeIdLoanApplication,
                                                                        Business_Date_Created__c=System.today(),
                                                                        Approved_Loan_Amount__c=100000,
                                                                        Processing_Fee_Percentage__c=1,Mode_of_Repayment__c='NACH',
                                                                        Repayment_Start_Date__c=Date.Today().addDays(100),
                                                                        Sanctioned_Disbursement_Type__c='Multiple Tranche',Property_Identified__c = true);
            insert app1;
            Test.startTest();
            Loan_Contact__c lc =[SELECT Id,Borrower__c from Loan_Contact__c where Loan_Applications__c=:app.Id];
            /*lc.Borrower__c='1';
            //lc.Category__c='1';
            lc.Constitution__c = '20';
            lc.Customer_segment__c = '1';
            lc.BRE_Record__c = true;
            lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc.Marital_Status__c = 'M';
            lc.Gross_Salary__c = 1000;
            lc.Basic_Pay__c = 1000;
            lc.DA__c = 1000;
            //lc.Total_Fixed_Pay__c = 1000;
            //lc.Variable_Income_Monthly__c = 1000;
            lc.Net_Income__c = 1000;
            lc.PF__c = 1000;
            lc.TDS__c = 1000;
            lc.Current_Organization_Work_Ex_in_Months__c=12;
            lc.Total_Work_Experience__c = 12;
            lc.Customer_Details_Verified__c= true;
            lc.Voter_ID_Number__c = '123456789';
            lc.Passport_Number__c = '123456789';
            lc.PAN_Number__c = 'BAFPV6543A';
            lc.Aadhaar_Number__c = '898518762736';
            lc.Income_Considered__c = true;
            
            update lc;*/
            
            Loan_Contact__c lc1 =[SELECT Id,Borrower__c from Loan_Contact__c where Loan_Applications__c=:app1.Id];
            lc1.Borrower__c='1';
            //lc.Category__c='1';
            lc1.Constitution__c = '20';
            lc1.Customer_segment__c = '1';
            lc1.BRE_Record__c = true;
            lc1.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc1.Marital_Status__c = 'M';
            lc1.Gross_Salary__c = 1000;
            lc1.Basic_Pay__c = 1000;
            lc1.DA__c = 1000;
            //lc.Total_Fixed_Pay__c = 1000;
            //lc.Variable_Income_Monthly__c = 1000;
            lc1.Net_Income__c = 1000;
            lc1.PF__c = 1000;
            lc1.TDS__c = 1000;
            lc1.Current_Organization_Work_Ex_in_Months__c=12;
            lc1.Total_Work_Experience__c = 12;
            lc1.Customer_Details_Verified__c= true;
            lc1.Voter_ID_Number__c = '123456789';
            lc1.Passport_Number__c = '123456789';
            lc1.PAN_Number__c = 'BAFPV6543A';
            lc1.Aadhaar_Number__c = '898518762736';
            lc1.Income_Considered__c = true;
            
            update lc1;
            /*Pincode__c pin = new Pincode__c(Name='11044',ZIP_ID__c='45',City__c='Ludiana',State_Text__c='Punjab',Country_Text__c='India');
            insert pin;   
            system.debug('pin '+pin);
            Address__c add = new Address__c(Residence_Type__c= 'G',Type_of_address__c='OFFRES',Address_Line_1__c='11111111',Address_Line_2__c='127737',Pincode_LP__c=pin.Id,Loan_Contact__c=lc.Id);
            insert add;
            Customer_Integration__c cust = new Customer_Integration__c(CIBIL_Score__c='5',recordTypeId=Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId(), Loan_Application__c = app.Id, Loan_Contact__c=lc.Id,BRE_Recent__c=true);
            insert cust;
            Sourcing_Detail__c sc = new Sourcing_Detail__c(Loan_Application__c= app.Id, Source_Code__c= '19',Lead_Sourcing_Detail__c='HHFL Website',recordTypeId=Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('Digital Direct').getRecordTypeId());
            insert sc;
            app.StageName__c='Operation Control';
            app.Sub_Stage__c  ='Scan: Data Maker';
            //app.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('File check Scanner').getRecordTypeId();
            update app;*/
            /*Property__c prop = new Property__c(First_Property_Owner__c=lc.Id, Loan_Application__c=app.Id,Address_Line_1__c='17827872',Area__c=12,Floor__c='1',Area_Unit__c='Sqft',Flat_No_House_No__c='1',Type_Property__c='Residential',Pincode_LP__c=pin.Id);
            insert prop;*/
			/*Sanction_Condition_Master__c objSCM = new Sanction_Condition_Master__c();
			objSCM.Name = 'Others';
			insert objSCM;
            LMS_Bus_Date__c objL = [Select Id, Current_Date__c from LMS_Bus_Date__c];
            objL.Current_Date__c = Date.valueOf(System.now());
            update objL;*/
			
			Customer_Integration__c cust = new Customer_Integration__c(CIBIL_Score__c='5',recordTypeId=Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId(), Loan_Application__c = app1.Id, Loan_Contact__c=lc1.Id,BRE_Recent__c=true);
            insert cust;
            Customer_Integration__c cust1 = new Customer_Integration__c(CIBIL_Score__c='5',recordTypeId=Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId(), Loan_Application__c = app.Id, Loan_Contact__c=lc.Id,BRE_Recent__c=true);
            insert cust1;
            Credit_Deviation_Master__c objCDM = new Credit_Deviation_Master__c();
            objCDM.Deviation_Code__c = '0';
            objCDM.Status__c = TRUE;
            objCDM.Product__c = 'CF';
            insert objCDM;
            
            Credit_Deviation_Master__c objCDM1 = new Credit_Deviation_Master__c();
            objCDM1.Deviation_Code__c = '0';
            objCDM1.Status__c = TRUE;
            objCDM1.Product__c = 'HL';
            insert objCDM1;
            
            Credit_Deviation_Master__c objCDM2 = new Credit_Deviation_Master__c();
            objCDM2.Deviation_Code__c = '1;2;';
            objCDM2.Status__c = TRUE;
            objCDM2.Product__c = 'CF';
            insert objCDM2;

            
            Mitigant__c objM = new Mitigant__c();
            objM.Active__c = true;
            objM.Mitigant_ID__c = '123';
            objM.Name = 'Others';
            insert objM;
            
            /*Applicable_Deviation__c objAD = new Applicable_Deviation__c();
            objAD.Credit_Deviation_Master__c = objCDM.Id;
            objAD.Mitigant__c = objM.Id;
            objAD.Loan_Application__c = app1.Id;
            objAD.Deviation_Code__c ='0';
            objAD.Customer_Integration__c =cust.Id;
            objAD.Customer_Details__c = lc1.id;
            insert objAD;
            
            Applicable_Deviation__c objAD1 = new Applicable_Deviation__c();
            objAD1.Credit_Deviation_Master__c = objCDM.Id;
            objAD1.Mitigant__c = objM.Id;
            objAD1.Loan_Application__c = app.Id;
            objAD1.Deviation_Code__c ='0';
            objAD1.Customer_Integration__c =cust.Id;
            objAD1.Customer_Details__c = lc.id;
            insert objAD1;*/
            
			/*Application_CIBIL_Decision_Detail__c objAppCIBIL = new Application_CIBIL_Decision_Detail__c();
			objAppCIBIL.Deviation_Code__c = '0';
			objAppCIBIL.BRE_II__c = true;
			objAppCIBIL.Loan_Application__c = app.Id;
			objAppCIBIL.Customer_Integration__c = cust1.Id;*/
			
			BRE_Decision_Code__c bobj = new BRE_Decision_Code__c();

			bobj.Reason_Code__c = '1';
			bobj.Active__c = true;
			insert bobj;
			
			CIBIL_Decision_Detail__c objAppCIBIL = new CIBIL_Decision_Detail__c();
			objAppCIBIL.Reason_Code__c = '1';
			//objAppCIBIL.Product_Code__c = sch.Scheme_Code__c;
			objAppCIBIL.BRE_II__c = true;
			objAppCIBIL.Loan_Application__c = app.Id;
			objAppCIBIL.Customer_Integration__c = cust1.Id;
            objAppCIBIL.Customer_Detail__c = lc1.id;
			insert objAppCIBIL;
			
			
            
            
            
            Test.stopTest();
        }    
    
    }
}