@IsTest(isParallel=false)

public class TestUserTriggerHelper{
	
	/*Public static Testmethod void method1(){

List<User> ulist=new List<User>();

  User u1=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,profile.name,isActive,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId , line_of_business__C
                       from user where profile.Name='DST' and isActive=false limit 1];
    u1.IsActive = true;
   ulist.add(u1);                    
  //UserTriggerHandler.afterInsert(ulist);
     update u1;

       Profile p=[SELECT id,name from profile where name=:'Sales Team'];
         UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
    List<User> lstBranchManager= new List<User>();                          
          User u = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,Role_in_Sales_Hierarchy__c = 'SM'
        );
        lstBranchManager.add(u);
        
        User us1 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
         lstBranchManager.add(us1);
        insert lstBranchManager;  
    User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId 
                       from user where ID=:UserInfo.getUserId()];
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=lstBranchManager[0].Id;
        loggedUser.ManagerID=lstBranchManager[0].Id;
        update loggedUser;
       
        
       
        u.ManagerId=UserInfo.getUserID();
       
        system.runAs(loggedUser){ 
    Branch_Master__c objBranchMaster = new Branch_Master__c(Name='Test Branch',Office_Code__c='Tes1OC');
    insert objBranchMaster;
    Branch_Master__c objBranchMaster1 = new Branch_Master__c(Name='Test Branch1',Office_Code__c='TestOC');
    insert objBranchMaster1;        
    List<User_Branch_Mapping__c> li=new List<User_Branch_Mapping__c>();
    
    User_Branch_Mapping__c ubrmap1= new User_Branch_Mapping__c(Name='DummyMap1',User__c=u1.id,Servicing_Branch__c=objBranchMaster.Id,Sourcing_Branch__c=objBranchMaster1.Id);
    li.add(ubrmap1);
    insert li;  
        }


}*/

Public static Testmethod void method5(){

List<User> ulist=new List<User>();
/*
User u=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,profile.name,isActive,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId 
                       from user where profile.Name='Vendor PD' and isActive=true limit 1];
   ulist.add(u);                    
  UserTriggerHandler.afterInsert(ulist);
  */
  /*User u1=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,profile.name,isActive,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId , line_of_business__C
                       from user where profile.Name='DST' and isActive=false limit 1];
    u1.IsActive = true;
   ulist.add(u1);                    
  //UserTriggerHandler.afterInsert(ulist);
     update u1;*/
  
  User u2=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,profile.name,isActive,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId ,line_of_business__C
                       from user where profile.Name='DSA' and isActive=false limit 1];
  
  u2.IsActive = true;
    ulist.add(u2);                    
//  UserTriggerHandler.afterInsert(ulist);
   update u2;
       Profile p=[SELECT id,name from profile where name=:'Sales Team'];
         UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
    List<User> lstBranchManager= new List<User>();                          
          User u = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,Role_in_Sales_Hierarchy__c = 'SM'
        );
        lstBranchManager.add(u);
        
        User us1 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
         lstBranchManager.add(us1);
        insert lstBranchManager;  
    User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId 
                       from user where ID=:UserInfo.getUserId()];
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=lstBranchManager[0].Id;
        loggedUser.ManagerID=lstBranchManager[0].Id;
        update loggedUser;
       
        
       
        u.ManagerId=UserInfo.getUserID();
       
        system.runAs(loggedUser){ 
    Branch_Master__c objBranchMaster = new Branch_Master__c(Name='Test Branch',Office_Code__c='TestOC');
    insert objBranchMaster;
    DSA_Master__c objDSAMaster = new DSA_Master__c(Name='A JAGAN MOHAN',Type__c='DSA', BrokerID__c = '1234');
    insert objDSAMaster;
    List<User_Branch_Mapping__c> li=new List<User_Branch_Mapping__c>();
    
    User_Branch_Mapping__c ubrmap1= new User_Branch_Mapping__c(Name='DummyMap1',User__c=u2.id,Servicing_Branch__c=objBranchMaster.Id,Sourcing_Branch__c=objBranchMaster.Id);
    li.add(ubrmap1);
    insert li;  
        }
	/*User u3=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,profile.name,isActive,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId ,line_of_business__C
                       from user where profile.Name='Sales Team' and isActive=false limit 1];
  u3.IsActive = true;
    ulist.add(u3);                    
  //UserTriggerHandler.afterInsert(ulist);
    update u3;
    Branch_Master__c objBranchMaster1 = new Branch_Master__c(Name='Test Branch2',Office_Code__c='TestOCR');
    insert objBranchMaster1;
    List<User_Branch_Mapping__c> li2=new List<User_Branch_Mapping__c>();
    
    User_Branch_Mapping__c ubrmap2= new User_Branch_Mapping__c(Name='DummyMap2',User__c=u3.id,Servicing_Branch__c=objBranchMaster1.Id,Sourcing_Branch__c=objBranchMaster1.Id);
    li2.add(ubrmap2);
    insert li2;  
    User u4=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,profile.name,isActive,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId ,line_of_business__C
                       from user where profile.Name='Credit Team' and isActive=false limit 1];
   u4.IsActive = true;
    ulist.add(u4);                    
  //UserTriggerHandler.afterInsert(ulist);
    update u4;
  User u5=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,profile.name,isActive,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId ,line_of_business__C
                       from user where profile.Name='Credit Team' and isActive=false limit 1];
   u5.IsActive = true;
   // ulist.add(u5);                    
  //UserTriggerHandler.afterInsert(ulist);
    update u5;*/

}
    
    
    Public static Testmethod void method4(){

List<User> ulist=new List<User>();
/*
User u=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,profile.name,isActive,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId 
                       from user where profile.Name='Vendor PD' and isActive=true limit 1];
   ulist.add(u);                    
  UserTriggerHandler.afterInsert(ulist);
  */
  User u1=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,profile.name,isActive,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId ,line_of_business__C, Location__c
                       from user where profile.Name='DST' and isActive=true and Location__c != 'Delhi' limit 1];
   u1.Location__c = 'Delhi';
   ulist.add(u1);                    
  
   //  update ulist;
  
  /*User u2=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,profile.name,isActive,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId ,line_of_business__C
                       from user where profile.Name='DSA' and isActive=true limit 1];
   //ulist.add(u2);*/                    
  //UserTriggerHandler.afterInsert(ulist);
  // update ulist;
    
  User u3=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,profile.name,isActive,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId ,line_of_business__C, Location__c
                       from user where profile.Name='Sales Team' and isActive=true and Location__c != 'Delhi - NSP' limit 1];
   u3.Location__c = 'Delhi - NSP';
   ulist.add(u3);                    
  //UserTriggerHandler.afterInsert(ulist);
 //   update ulist;
  
  User u4=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,profile.name,isActive,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId ,line_of_business__C, Location__c
                       from user where profile.Name='Credit Team' and isActive=true limit 1];
   u4.Location__c = 'Lucknow';
   ulist.add(u4);                    
  //UserTriggerHandler.afterInsert(ulist);
  //  update ulist;
    
  User u5=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,profile.name,isActive,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId ,line_of_business__C, Location__c
                       from user where profile.Name='Sales Team' and isActive=true and Location__c != 'Mumbai' limit 1];
   u5.Location__c = 'Mumbai';
   ulist.add(u5);                    
  UserTriggerHandler.afterInsert(ulist);
//update ulist;

}


}