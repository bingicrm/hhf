@isTest
private class TestLoanContactHelper {
    
    static testmethod void test1(){
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Name='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567890',RecordTypeId=RecordTypeIdAccount);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567891',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact cnt2 = new Contact (LastName = 'LNameLoanContact', AccountId = acc1.Id, MobilePhone = '9999667739');
        insert cnt2;
        
        Account acc2 = new Account(PAN__c='ABCDE1234H',Salutation='Mr',FirstName='Suresh',LastName='Nath',
                                   Date_of_Birth__c =Date.newInstance(1990,11,02),
                                   PersonMobilePhone='9234567892',PersonEmail='sn@gmail.com',RecordTypeId=RecordTypeIdAccount1);
        insert acc2;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Test.startTest();
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert objLoanApplication;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Guarantor').getRecordTypeId();
        
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc1.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='2',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='18',
                                                             Customer_segment__c='9',Applicant_Status__c='1',
                                                             GSTIN_Available__c=true, Contact__c = cnt2.Id,
                                                             GSTIN_Number__c = '27AAHFG0551H1ZL',
                                                             income_considered__c = false);
        insert objLoanContact;
        
        List<Borrower_Document_Checklist__c> listBCheck = new List<Borrower_Document_Checklist__c>();
        Borrower_Document_Checklist__c objBorrowDocCheck = new Borrower_Document_Checklist__c();
        objBorrowDocCheck.Applicant_Type__c = 'Co- Applicant';
        objBorrowDocCheck.Applicant_Status__c = '1';
        objBorrowDocCheck.Customer_Type__c = '2';
        objBorrowDocCheck.Customer_Segment__c = '3';
        objBorrowDocCheck.Income_program_typeq__c= '';
        objBorrowDocCheck.Constitution__c = '18';
        objBorrowDocCheck.income_considered__c = false;
        objBorrowDocCheck.GST_Status__c = true;
        objBorrowDocCheck.Stage__c = '';
        listBCheck.add(objBorrowDocCheck);
        
        Borrower_Document_Checklist__c objBorrowDocCheck2 = new Borrower_Document_Checklist__c();
        objBorrowDocCheck2.Applicant_Type__c = 'Co- Applicant';
        objBorrowDocCheck2.Applicant_Status__c = '1';
        objBorrowDocCheck2.Customer_Type__c = '2';
        objBorrowDocCheck2.Customer_Segment__c = '';
        objBorrowDocCheck2.Income_program_typeq__c= '';
        objBorrowDocCheck2.Constitution__c = '18';
        objBorrowDocCheck2.income_considered__c = false;
        objBorrowDocCheck2.GST_Status__c = true;
        objBorrowDocCheck2.Stage__c = '';
        listBCheck.add(objBorrowDocCheck2);
        
        insert listBCheck;
       
        List<Loan_Contact__c> lstLoanCon = new List<Loan_Contact__c>();
        lstLoanCon.add(objLoanContact);
        
        LoanContactHelper.DocumentChecklist(lstLoanCon);
        Test.stopTest();
    }    
    
    static testmethod void test2(){
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Name='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567890',RecordTypeId=RecordTypeIdAccount);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567891',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact cnt2 = new Contact (LastName = 'LNameLoanContact', AccountId = acc1.Id, MobilePhone = '9999667739');
        insert cnt2;
        
        Account acc2 = new Account(PAN__c='ABCDE1234H',Salutation='Mr',FirstName='Suresh',LastName='Nath',
                                   Date_of_Birth__c =Date.newInstance(1990,11,02),
                                   PersonMobilePhone='9234567892',PersonEmail='sn@gmail.com',RecordTypeId=RecordTypeIdAccount1);
        insert acc2;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Test.startTest();
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',Processing_Fee_Percentage__c=1);
        insert objLoanApplication;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Guarantor').getRecordTypeId();
        
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc1.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='2',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='18',
                                                             Customer_segment__c='9',Applicant_Status__c='1',
                                                             GSTIN_Available__c=true,GSTIN_Number__c = '27AAHFG0551H1ZL',
                                                              Contact__c = cnt2.Id
                                                             );
        //insert objLoanContact;
        
        Loan_Contact__c objLoanContact2 = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc2.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='1',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='20',
                                                             Customer_segment__c='3',Applicant_Status__c='1',
                                                             Category__c='1');
        //insert objLoanContact2;
        List<Loan_Contact__c> lstLC1 = new List<Loan_Contact__c>();
        lstLC1.add(objLoanContact);
        lstLC1.add(objLoanContact2);
        insert lstLC1;
        
        //System.debug('objLoanContact==>'+[SELECT Id,applicant_Type__c,Customer_segment__c,Applicant_Status__c,Constitution__c from Loan_Contact__c WHERE Id=:objLoanContact.Id]);
        
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=objLoanApplication.Id,
                                                                     Loan_Contact__c=objLoanContact.Id);
        insert objDocCheck;
        
        Borrower_Document_Checklist__c objBorrowDocCheck = new Borrower_Document_Checklist__c();
        objBorrowDocCheck.Applicant_Type__c = 'Co- Applicant';
        objBorrowDocCheck.Applicant_Status__c = '1';
        objBorrowDocCheck.Customer_Type__c = '2';
        objBorrowDocCheck.Customer_Segment__c = '3';
        objBorrowDocCheck.Income_program_typeq__c= '';
        objBorrowDocCheck.Customer_Type__c = '';
        objBorrowDocCheck.Constitution__c = '20';
        objBorrowDocCheck.income_considered__c = false;
        objBorrowDocCheck.GST_Status__c = true;
        objBorrowDocCheck.Stage__c = '';
        insert objBorrowDocCheck;
        
        Loan_Contact__c objLoanContact1 = [SELECT Id,Loan_Applications__c,Customer__c,Applicant_Type__c,Borrower__c, Mobile__c,
                                          Relationship_with_Applicant__c,Pan_Verification_status__c,Constitution__c,
                                          Customer_segment__c,Applicant_Status__c from Loan_Contact__c
                                          WHERE Loan_Applications__c =: objLoanApplication.Id AND Applicant_Type__c='Applicant'];
        
        List<Loan_Contact__c> lstLoanCon = new List<Loan_Contact__c>();
        lstLoanCon.add(objLoanContact);
        
        List<Loan_Contact__c> lstLoanCon1 = new List<Loan_Contact__c>();
        lstLoanCon1.add(objLoanContact1);
        
        List<Loan_Contact__c> lstLoanCon2 = new List<Loan_Contact__c>();
        
        List<Loan_Contact__c> lstLoanCon3 = new List<Loan_Contact__c>();
        lstLoanCon3.add(objLoanContact2);

       // LoanContactHelper.DocumentChecklist(lstLoanCon);
       // LoanContactHelper.insertCustomerFinancials(lstLoanCon);
        //LoanContactHelper.blockLoanConCreation(lstLoanCon1,lstLoanCon);
       // LoanContactHelper.validateRelationShip(lstLoanCon);
       // LoanContactHelper.validateRelationShip(lstLoanCon3);
       LoanContactHelper.blockLoanConCreation(lstLoanCon1,lstLoanCon2);
       // LoanContactHelper.sendSMStoApplicant(lstLoanCon1);
        
        Test.stopTest();
    }
    
    static testmethod void test3(){
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Name='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567890',RecordTypeId=RecordTypeIdAccount);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567891',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact cnt2 = new Contact (LastName = 'LNameLoanContact', AccountId = acc1.Id, MobilePhone = '9999667739');
        insert cnt2;
        
        Account acc2 = new Account(PAN__c='ABCDE1234H',Salutation='Mr',FirstName='Suresh',LastName='Nath',
                                   Date_of_Birth__c =Date.newInstance(1990,11,02),
                                   PersonMobilePhone='9234567892',PersonEmail='sn@gmail.com',RecordTypeId=RecordTypeIdAccount1);
        insert acc2;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Test.startTest();
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',Processing_Fee_Percentage__c=1);
        insert objLoanApplication;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Guarantor').getRecordTypeId();
        
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc1.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='2',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='18',
                                                             Customer_segment__c='9',Applicant_Status__c='1',
                                                             GSTIN_Available__c=true,GSTIN_Number__c = '27AAHFG0551H1ZL',
                                                              Contact__c = cnt2.Id
                                                             );
        //insert objLoanContact;
        
        Loan_Contact__c objLoanContact2 = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc2.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='1',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='20',
                                                             Customer_segment__c='3',Applicant_Status__c='1',
                                                             Category__c='1');
        //insert objLoanContact2;
        List<Loan_Contact__c> lstLC1 = new List<Loan_Contact__c>();
        lstLC1.add(objLoanContact);
        lstLC1.add(objLoanContact2);
        insert lstLC1;
        //System.debug('objLoanContact==>'+[SELECT Id,applicant_Type__c,Customer_segment__c,Applicant_Status__c,Constitution__c from Loan_Contact__c WHERE Id=:objLoanContact.Id]);
        
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=objLoanApplication.Id,
                                                                     Loan_Contact__c=objLoanContact.Id);
        insert objDocCheck;
        
        Borrower_Document_Checklist__c objBorrowDocCheck = new Borrower_Document_Checklist__c();
        objBorrowDocCheck.Applicant_Type__c = 'Co- Applicant';
        objBorrowDocCheck.Applicant_Status__c = '1';
        objBorrowDocCheck.Customer_Type__c = '2';
        objBorrowDocCheck.Customer_Segment__c = '3';
        objBorrowDocCheck.Income_program_typeq__c= '';
        objBorrowDocCheck.Customer_Type__c = '';
        objBorrowDocCheck.Constitution__c = '20';
        objBorrowDocCheck.income_considered__c = false;
        objBorrowDocCheck.GST_Status__c = true;
        objBorrowDocCheck.Stage__c = '';
        insert objBorrowDocCheck;
        
        Loan_Contact__c objLoanContact1 = [SELECT Id,Loan_Applications__c,Customer__c,Applicant_Type__c,Borrower__c, Mobile__c,
                                          Relationship_with_Applicant__c,Pan_Verification_status__c,Constitution__c,
                                          Customer_segment__c,Applicant_Status__c from Loan_Contact__c
                                          WHERE Loan_Applications__c =: objLoanApplication.Id AND Applicant_Type__c='Applicant'];
        
        List<Loan_Contact__c> lstLoanCon = new List<Loan_Contact__c>();
        lstLoanCon.add(objLoanContact);
        
        List<Loan_Contact__c> lstLoanCon1 = new List<Loan_Contact__c>();
        lstLoanCon1.add(objLoanContact1);
        
        List<Loan_Contact__c> lstLoanCon2 = new List<Loan_Contact__c>();
        
        List<Loan_Contact__c> lstLoanCon3 = new List<Loan_Contact__c>();
        lstLoanCon3.add(objLoanContact2);

       // LoanContactHelper.DocumentChecklist(lstLoanCon);
       // LoanContactHelper.insertCustomerFinancials(lstLoanCon);
       // LoanContactHelper.blockLoanConCreation(lstLoanCon1,lstLoanCon);
       // LoanContactHelper.validateRelationShip(lstLoanCon);
       // LoanContactHelper.validateRelationShip(lstLoanCon3);
       // LoanContactHelper.blockLoanConCreation(lstLoanCon1,lstLoanCon2);
        LoanContactHelper.sendSMStoApplicant(lstLoanCon1);
        
        Test.stopTest();
    }
    
    static testmethod void test4(){
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Name='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567890',RecordTypeId=RecordTypeIdAccount);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567891',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact cnt2 = new Contact (LastName = 'LNameLoanContact', AccountId = acc1.Id, MobilePhone = '9999667739');
        insert cnt2;
        
        Account acc2 = new Account(PAN__c='ABCDE1234H',Salutation='Mr',FirstName='Suresh',LastName='Nath',
                                   Date_of_Birth__c =Date.newInstance(1990,11,02),
                                   PersonMobilePhone='9234567892',PersonEmail='sn@gmail.com',RecordTypeId=RecordTypeIdAccount1);
        insert acc2;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Test.startTest();
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',Processing_Fee_Percentage__c=1);
        insert objLoanApplication;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Guarantor').getRecordTypeId();
        
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc1.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='2',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='18',
                                                             Customer_segment__c='9',Applicant_Status__c='1',
                                                             GSTIN_Available__c=true,GSTIN_Number__c = '27AAHFG0551H1ZL',
                                                              Contact__c = cnt2.Id
                                                             );
        //insert objLoanContact;
        
        Loan_Contact__c objLoanContact2 = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc2.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='1',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='20',
                                                             Customer_segment__c='3',Applicant_Status__c='1',
                                                             Category__c='1');
        //insert objLoanContact2;
        List<Loan_Contact__c> lstLC1 = new List<Loan_Contact__c>();
        lstLC1.add(objLoanContact);
        lstLC1.add(objLoanContact2);
        insert lstLC1;
        //System.debug('objLoanContact==>'+[SELECT Id,applicant_Type__c,Customer_segment__c,Applicant_Status__c,Constitution__c from Loan_Contact__c WHERE Id=:objLoanContact.Id]);
        
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=objLoanApplication.Id,
                                                                     Loan_Contact__c=objLoanContact.Id);
        insert objDocCheck;
        
        Borrower_Document_Checklist__c objBorrowDocCheck = new Borrower_Document_Checklist__c();
        objBorrowDocCheck.Applicant_Type__c = 'Co- Applicant';
        objBorrowDocCheck.Applicant_Status__c = '1';
        objBorrowDocCheck.Customer_Type__c = '2';
        objBorrowDocCheck.Customer_Segment__c = '3';
        objBorrowDocCheck.Income_program_typeq__c= '';
        objBorrowDocCheck.Customer_Type__c = '';
        objBorrowDocCheck.Constitution__c = '20';
        objBorrowDocCheck.income_considered__c = false;
        objBorrowDocCheck.GST_Status__c = true;
        objBorrowDocCheck.Stage__c = '';
        insert objBorrowDocCheck;
        
        Loan_Contact__c objLoanContact1 = [SELECT Id,Loan_Applications__c,Customer__c,Applicant_Type__c,Borrower__c, Mobile__c,
                                          Relationship_with_Applicant__c,Pan_Verification_status__c,Constitution__c,
                                          Customer_segment__c,Applicant_Status__c from Loan_Contact__c
                                          WHERE Loan_Applications__c =: objLoanApplication.Id AND Applicant_Type__c='Applicant'];
        
        List<Loan_Contact__c> lstLoanCon = new List<Loan_Contact__c>();
        lstLoanCon.add(objLoanContact);
        
        List<Loan_Contact__c> lstLoanCon1 = new List<Loan_Contact__c>();
        lstLoanCon1.add(objLoanContact1);
        
        List<Loan_Contact__c> lstLoanCon2 = new List<Loan_Contact__c>();
        
        List<Loan_Contact__c> lstLoanCon3 = new List<Loan_Contact__c>();
        lstLoanCon3.add(objLoanContact2);

       // LoanContactHelper.DocumentChecklist(lstLoanCon);
       // LoanContactHelper.insertCustomerFinancials(lstLoanCon);
       // LoanContactHelper.blockLoanConCreation(lstLoanCon1,lstLoanCon);
       // LoanContactHelper.validateRelationShip(lstLoanCon);
        //LoanContactHelper.validateRelationShip(lstLoanCon3);
       // LoanContactHelper.blockLoanConCreation(lstLoanCon1,lstLoanCon2);
       LoanContactHelper.sendSMStoApplicant(lstLoanCon1);
        
        Test.stopTest();
    }
    
    static testmethod void test5(){
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Name='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567890',RecordTypeId=RecordTypeIdAccount);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567891',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact cnt2 = new Contact (LastName = 'LNameLoanContact', AccountId = acc1.Id, MobilePhone = '9999667739');
        insert cnt2;
        
        Account acc2 = new Account(PAN__c='ABCDE1234H',Salutation='Mr',FirstName='Suresh',LastName='Nath',
                                   Date_of_Birth__c =Date.newInstance(1990,11,02),
                                   PersonMobilePhone='9234567892',PersonEmail='sn@gmail.com',RecordTypeId=RecordTypeIdAccount1);
        insert acc2;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Test.startTest();
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',Processing_Fee_Percentage__c=1);
        insert objLoanApplication;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Guarantor').getRecordTypeId();
        
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc1.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='2',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='18',
                                                             Customer_segment__c='9',Applicant_Status__c='1',
                                                             GSTIN_Available__c=true,GSTIN_Number__c = '27AAHFG0551H1ZL',
                                                              Contact__c = cnt2.Id
                                                             );
        //insert objLoanContact;
        
        Loan_Contact__c objLoanContact2 = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc2.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='1',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='20',
                                                             Customer_segment__c='3',Applicant_Status__c='1',
                                                             Category__c='1');
        //insert objLoanContact2;
        List<Loan_Contact__c> lstLC1 = new List<Loan_Contact__c>();
        lstLC1.add(objLoanContact);
        lstLC1.add(objLoanContact2);
        insert lstLC1;
        //System.debug('objLoanContact==>'+[SELECT Id,applicant_Type__c,Customer_segment__c,Applicant_Status__c,Constitution__c from Loan_Contact__c WHERE Id=:objLoanContact.Id]);
        
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=objLoanApplication.Id,
                                                                     Loan_Contact__c=objLoanContact.Id);
        insert objDocCheck;
        
        Borrower_Document_Checklist__c objBorrowDocCheck = new Borrower_Document_Checklist__c();
        objBorrowDocCheck.Applicant_Type__c = 'Co- Applicant';
        objBorrowDocCheck.Applicant_Status__c = '1';
        objBorrowDocCheck.Customer_Type__c = '2';
        objBorrowDocCheck.Customer_Segment__c = '3';
        objBorrowDocCheck.Income_program_typeq__c= '';
        objBorrowDocCheck.Customer_Type__c = '';
        objBorrowDocCheck.Constitution__c = '20';
        objBorrowDocCheck.income_considered__c = false;
        objBorrowDocCheck.GST_Status__c = true;
        objBorrowDocCheck.Stage__c = '';
        insert objBorrowDocCheck;
        
        Loan_Contact__c objLoanContact1 = [SELECT Id,Loan_Applications__c,Customer__c,Applicant_Type__c,Borrower__c, Mobile__c,
                                          Relationship_with_Applicant__c,Pan_Verification_status__c,Constitution__c,
                                          Customer_segment__c,Applicant_Status__c from Loan_Contact__c
                                          WHERE Loan_Applications__c =: objLoanApplication.Id AND Applicant_Type__c='Applicant'];
        
        List<Loan_Contact__c> lstLoanCon = new List<Loan_Contact__c>();
        lstLoanCon.add(objLoanContact);
        
        List<Loan_Contact__c> lstLoanCon1 = new List<Loan_Contact__c>();
        lstLoanCon1.add(objLoanContact1);
        
        List<Loan_Contact__c> lstLoanCon2 = new List<Loan_Contact__c>();
        
        List<Loan_Contact__c> lstLoanCon3 = new List<Loan_Contact__c>();
        lstLoanCon3.add(objLoanContact2);

       // LoanContactHelper.DocumentChecklist(lstLoanCon);
       // LoanContactHelper.insertCustomerFinancials(lstLoanCon);
       // LoanContactHelper.blockLoanConCreation(lstLoanCon1,lstLoanCon);
       // LoanContactHelper.validateRelationShip(lstLoanCon);
       // LoanContactHelper.validateRelationShip(lstLoanCon3);
       // LoanContactHelper.blockLoanConCreation(lstLoanCon1,lstLoanCon2);
        LoanContactHelper.sendSMStoApplicant(lstLoanCon1);
        
        Test.stopTest();
    }
    
    static testmethod void test6(){
        Test.startTest();
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Name='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567890',RecordTypeId=RecordTypeIdAccount);
        insert acc;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert objLoanApplication;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Guarantor').getRecordTypeId();

        Loan_Contact__c objLoanContact1 = [SELECT Id,Loan_Applications__c,Customer__c,Applicant_Type__c,Borrower__c, Mobile__c,
                                          Relationship_with_Applicant__c,Pan_Verification_status__c,Constitution__c,
                                          Customer_segment__c,Applicant_Status__c from Loan_Contact__c
                                          WHERE Loan_Applications__c =: objLoanApplication.Id AND Applicant_Type__c='Applicant'];

        List<Loan_Contact__c> lstLoanCon1 = new List<Loan_Contact__c>();
        lstLoanCon1.add(objLoanContact1);
        

        LoanContactHelper.sendSMStoApplicant(lstLoanCon1);
        
        Test.stopTest();
    }
    
    
    static testmethod void test7(){
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
        insert acc;
        
        
        //Contact con = new Contact(LastName = 'Test Contact', AccountId = acc.Id);
        //insert con;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
        insert con1;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
         
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert objLoanApplication;
        Loan_Contact__c lc = [SELECT Id, Name,Applicant_Type__c, Applicant_Status__c, Constitution__c, Customer_segment__c, Loan_Applications__c FROM Loan_Contact__c WHERE Loan_Applications__c =: objLoanApplication.Id];
        //lc.Applicant_Type__c='Co- Applicant';
        lc.Borrower__c='2';
        lc.Contact__c = con1.Id;
        lc.Constitution__c='18';
        lc.Applicant_Status__c='1';
        lc.Customer_segment__c='9';
        lc.Income_Program_Type__c = 'Liquid Income Program';
        //lc.Gender__c = 'M';
        //lc.Category__c ='1';
        //lc.Religion__c = '1';
        lc.Mobile__c='9876543210';
        lc.Email__c='testemail@gmail.com';
        //lc.Customer__c = acc.id;    
        update lc;
        test.startTest();
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Co-Applicant').getRecordTypeId();      
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc.Id,
                                                             Applicant_Type__c='Guarantor',Borrower__c='1',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='Brother',
                                                             Pan_Verification_status__c=true,Constitution__c='20',
                                                             Customer_segment__c='1',Applicant_Status__c='1',
                               Category__c='1',
                                                             Father_s_Husband_s_Name__c='ABC',
                                                             GSTIN_Number__c='04AATPB2258C1Z3',
                               Mobile__c='9978786756',Email__c='avd@g.com');
        insert objLoanContact;
        
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=objLoanApplication.Id,
                                                                     Loan_Contact__c=objLoanContact.Id);
        insert objDocCheck;
        
        Borrower_Document_Checklist__c objBorrowDocCheck = new Borrower_Document_Checklist__c();
        objBorrowDocCheck.Applicant_Type__c = 'Co- Applicant';
        objBorrowDocCheck.Applicant_Status__c = '1';
        objBorrowDocCheck.Customer_Type__c = '1';
        //objBorrowDocCheck.Customer_Segment__c = '3';
        objBorrowDocCheck.Income_program_typeq__c= '';
        objBorrowDocCheck.Customer_Type__c = '';
        objBorrowDocCheck.Constitution__c = '20';
        insert objBorrowDocCheck;
        
       
        
        Loan_Contact__c objLoanContactUpdated = new Loan_Contact__c(Id=objLoanContact.Id,Applicant_Type__c='Guarantor');
        update objLoanContactUpdated;
        
        List<Loan_Contact__c> lstLoanCon = new List<Loan_Contact__c>();
        lstLoanCon.add(objLoanContact);
        
        Map<Id,Loan_Contact__c> mapLoanCon = new Map<Id,Loan_Contact__c>();
        mapLoanCon.put(objLoanContact.Id,objLoanContact);
        
        List<Loan_Contact__c> newlstLoanCon = new List<Loan_Contact__c>();
        newlstLoanCon.add(objLoanContactUpdated);
        
        Map<Id,Loan_Contact__c> newmapLoanCon = new Map<Id,Loan_Contact__c>();
        newmapLoanCon.put(objLoanContactUpdated.Id,objLoanContactUpdated);
        LoanContactHelper.upCAMScreenStatus(lstLoanCon,newmapLoanCon);
        objLoanContact.Income_considered__c=true;
        //LoanContactHelper.upCAMScreenStatus(lstLoanCon,newmapLoanCon);
        test.stopTest();
              
    }
}