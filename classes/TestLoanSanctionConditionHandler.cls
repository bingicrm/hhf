@isTest
public class TestLoanSanctionConditionHandler
{
    public static testMethod void method1()
    {
        Profile fcuManagerProfile = [SELECT Id from Profile WHERE Name='FCU Manager' LIMIT 1];
        List<User> lstUser = new List<User>();
        Hierarchy__c objHierarchy = new Hierarchy__c();
        /*objHierarchy.BCM__c = u2.Id;
        objHierarchy.ZCM__c = u4.Id;
        objHierarchy.CCM__c = u2.Id;
        objHierarchy.CRO__c = u2.Id;*/
        insert objHierarchy;
        List<Hierarchy__c> lstHierarchy = [Select Id, Name, Active__c, BCM__c, BH__c, CCM__c, CRO__c, NCM__c, ZCM__c, ZCM_Salaried__c from Hierarchy__c where Active__c = true];
        String HierarchyName = lstHierarchy[0].Name; 
        User fcuManager = new User(Username ='fcumanager@test.com',ProfileId = fcuManagerProfile.Id,
                              Alias = 'fcum123',Email = 'fcumanager@test.com',EmailEncodingKey = 'UTF-8',
                              LastName = 'Manager',CommunityNickname = 'fcumanager12345',TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US',FCU__c = TRUE,Hierarchy__c = HierarchyName,
            	     Non_Salaried_Authority__c = 'A2',
            		 Salaried_Authority__c = 'A2',
            		 Role_in_Credit_Hierarchy__c = 'BCM');
        lstUser.add(fcuManager);
        insert lstUser;
        Sanction_Condition_Master__c scm = new Sanction_Condition_Master__c();
        scm.Sanction_Condition__c = 'Condition 123';
        scm.Customer_Segment__c = '1';
        insert scm ;
        
        Account acc=new Account();
        acc.name='Applicant123';
        acc.phone='9234509786';
        acc.PAN__c='ADGPW4078E';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        System.debug(acc.id);
        
        Scheme__c sc=new Scheme__c();
        sc.name='Construction Finance';
        sc.Scheme_Code__c='CF';
        sc.Scheme_Group_ID__c='6';
        sc.Product_Code__c='HL';
        insert sc;
        System.debug(sc.id);
        
        List<Loan_Application__c> listLA = new List<Loan_Application__c>();
        
        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Scheme__c=sc.id;
        lap.Requested_Amount__c = 50000000;
        lap.Approved_Loan_Amount__c=5000000;
        lap.Transaction_Type__c='PB';
		lap.Credit_Manager_User__c= fcuManager.Id;
        listLA.add(lap);
        
        Loan_Application__c lap1=new Loan_Application__c ();
        lap1.Customer__c=acc.id;
        lap1.Scheme__c=sc.id;
        lap1.Requested_Amount__c = 50000000;
        lap1.Transaction_Type__c='PB';
        lap1.Approved_Loan_Amount__c=5000000;
		lap.Credit_Manager_User__c= fcuManager.Id;
        listLA.add(lap1);
        
        insert listLA;
        
        
        List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:listLA[0].id];
        Loan_Sanction_Condition__c lsc = new Loan_Sanction_Condition__c();
        lsc.Customer_Details__c = lstofcon[0].id;
        lsc.Loan_Application__c = lap.id;
        lsc.Sanction_Condition__c = 'Sanction 123';
        lsc.Sanction_Condition_Master__c = scm.id;
        lsc.Status__c = 'Approved';
        lsc.Type_of_Query__c = 'Document with Data Entry';
        insert lsc;
        
       /*Account acc1=new Account();
        acc1.name='Applicant1234';
        acc1.phone='1234509787';
        acc1.PAN__c='ADGPW4076E';
        acc1.Date_of_Incorporation__c=date.today();
        insert acc1; */
        
        
        
        List<Loan_Contact__c> lstofconn=[select id from Loan_Contact__c where Loan_Applications__c=:listLA[1].id];
        
        try 
        {
            lsc.Customer_Details__c = lstofconn[0].id;
            update lsc;
        }
        catch(Exception e)
        {}
        
    }
    
    public static testMethod void method2()
    {
	
		Profile fcuManagerProfile = [SELECT Id from Profile WHERE Name='FCU Manager' LIMIT 1];
        List<User> lstUser = new List<User>();
        Hierarchy__c objHierarchy = new Hierarchy__c();
        /*objHierarchy.BCM__c = u2.Id;
        objHierarchy.ZCM__c = u4.Id;
        objHierarchy.CCM__c = u2.Id;
        objHierarchy.CRO__c = u2.Id;*/
        insert objHierarchy;
        List<Hierarchy__c> lstHierarchy = [Select Id, Name, Active__c, BCM__c, BH__c, CCM__c, CRO__c, NCM__c, ZCM__c, ZCM_Salaried__c from Hierarchy__c where Active__c = true];
        String HierarchyName = lstHierarchy[0].Name; 
        User fcuManager = new User(Username ='fcumanager@test.com',ProfileId = fcuManagerProfile.Id,
                              Alias = 'fcum123',Email = 'fcumanager@test.com',EmailEncodingKey = 'UTF-8',
                              LastName = 'Manager',CommunityNickname = 'fcumanager12345',TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US',FCU__c = TRUE,Hierarchy__c = HierarchyName,
            	     Non_Salaried_Authority__c = 'A1',
            		 Salaried_Authority__c = 'A1',
            		 Role_in_Credit_Hierarchy__c = 'BCM');
        lstUser.add(fcuManager);
        insert lstUser;
        Sanction_Condition_Master__c scm = new Sanction_Condition_Master__c();
        scm.Sanction_Condition__c = 'Condition 123';
        scm.Customer_Segment__c = '1';
        insert scm ;
        
        Account acc=new Account();
        acc.name='Applicant123';
        acc.phone='9234509786';
        acc.PAN__c='ADGPW4078E';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        System.debug(acc.id);
        
        Scheme__c sc=new Scheme__c();
        sc.name='Construction Finance';
        sc.Scheme_Code__c='CF';
        sc.Scheme_Group_ID__c='6';
        sc.Product_Code__c='HL';
        insert sc;
        System.debug(sc.id);
        
        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Scheme__c=sc.id;
        lap.Requested_Amount__c = 50000000;
        lap.Approved_Loan_Amount__c=5000000;
        lap.Transaction_Type__c='PB';
		lap.Credit_Manager_User__c= fcuManager.Id;

        insert lap;
        
        List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
        Loan_Sanction_Condition__c lsc = new Loan_Sanction_Condition__c();
        lsc.Customer_Details__c = lstofcon[0].id;
        lsc.Loan_Application__c = lap.id;
        lsc.Sanction_Condition__c = 'Sanction 123';
        lsc.Sanction_Condition_Master__c = scm.id;
        lsc.Status__c = 'Approved';
        lsc.Type_of_Query__c = 'Document with Data Entry';
        insert lsc ;
        
        lsc.Status__c = 'Waiver Requested';
        update lsc;
        
        
        
    }
    public static testMethod void method3()
    {
	Profile fcuManagerProfile = [SELECT Id from Profile WHERE Name='FCU Manager' LIMIT 1];
        List<User> lstUser = new List<User>();
        Hierarchy__c objHierarchy = new Hierarchy__c();
        /*objHierarchy.BCM__c = u2.Id;
        objHierarchy.ZCM__c = u4.Id;
        objHierarchy.CCM__c = u2.Id;
        objHierarchy.CRO__c = u2.Id;*/
        insert objHierarchy;
        List<Hierarchy__c> lstHierarchy = [Select Id, Name, Active__c, BCM__c, BH__c, CCM__c, CRO__c, NCM__c, ZCM__c, ZCM_Salaried__c from Hierarchy__c where Active__c = true];
        String HierarchyName = lstHierarchy[0].Name; 
        User fcuManager = new User(Username ='fcumanager@test.com',ProfileId = fcuManagerProfile.Id,
                              Alias = 'fcum123',Email = 'fcumanager@test.com',EmailEncodingKey = 'UTF-8',
                              LastName = 'Manager',CommunityNickname = 'fcumanager12345',TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US',FCU__c = TRUE,Hierarchy__c = HierarchyName,
            	     Non_Salaried_Authority__c = 'A1',
            		 Salaried_Authority__c = 'A1',
            		 Role_in_Credit_Hierarchy__c = 'BCM');
        lstUser.add(fcuManager);
        insert lstUser;
        Sanction_Condition_Master__c scm = new Sanction_Condition_Master__c();
        scm.Sanction_Condition__c = 'Condition 123';
        scm.Customer_Segment__c = '1';
        insert scm ;
        set<Id> scmId = new set<Id>();
        scmId.add(scm.id);
        
        Account acc=new Account();
        acc.name='Applicant123';
        acc.phone='9934509786';
        acc.PAN__c='ADGPW4078E';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        System.debug(acc.id);
        
        Scheme__c sc=new Scheme__c();
        sc.name='Construction Finance';
        sc.Scheme_Code__c='CF';
        sc.Scheme_Group_ID__c='6';
        sc.Product_Code__c='HL';
        insert sc;
        System.debug(sc.id);
        
        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Scheme__c=sc.id;
        lap.Requested_Amount__c = 50000000;
        lap.Approved_Loan_Amount__c=5000000;
        lap.Transaction_Type__c='PB';
				lap.Credit_Manager_User__c= fcuManager.Id;

        insert lap;
        List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
        Loan_Sanction_Condition__c lsc = new Loan_Sanction_Condition__c();
        lsc.Customer_Details__c = lstofcon[0].id;
        lsc.Loan_Application__c = lap.id;
        lsc.Sanction_Condition__c = 'Sanction 123';
        lsc.Sanction_Condition_Master__c = scm.id;
        lsc.Status__c = 'Approved';
        lsc.Type_of_Query__c = 'Document with Data Entry';
        insert lsc ;
        List<Loan_Sanction_Condition__c> lolsc = new List<Loan_Sanction_Condition__c>();
        lolsc.add(lsc);
        
        Document_Master__c dmc = new Document_Master__c();
        dmc.Sanction_Condition_Master__c = scm.id;
        dmc.Doc_Id__c = 'Test';
        insert dmc;
        
        LoanSanctionConditionHandler.insertDocuments(scmId, lolsc);
    }
    
}