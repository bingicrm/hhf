public class CopyOverCreditFields {
    
    @AuraEnabled
    public static String copyApplicantsValues(Id loanContactId) {
        Boolean updateValues = false;
        Id profileId = UserInfo.getProfileId();
        String profileName =[Select Id, Name from Profile where Id=:profileId].Name;
        system.debug('Profile Name'+profileName);
        system.debug('loanContactId:::'+loanContactId);
        Loan_Contact__c objCon = [Select Id, Loan_Applications__c, Applicant_Type__c, Loan_Applications__r.StageName__c, Loan_Applications__r.Applicant_Customer_segment__c, Customer_Segment__c, Industry__c, Nature_of_Business__c,Sub_Industry__c,Activity_Type__c, Trading_Type__c, Skill_Level__c,/*Service__c,*/Platform_Type__c, Service_Industry_Type__c from Loan_Contact__c where Id=: loanContactId ];
        
        if(objCon != null){
            
            if(objCon.Loan_Applications__r.StageName__c != 'Credit Decisioning' || profileName != 'Credit Team'){
              return 'BCM is allowed to copy values on at Credit Review / Re-Credit / Re-Look stages.';   
            }            
            if(objCon.Loan_Applications__r.Applicant_Customer_segment__c != objCon.Customer_Segment__c){
              return 'Applicant segment should be same to copy values.';   
            }
            if(objCon.Applicant_Type__c == 'Applicant'){
              return 'This operation is not possible for Applicants.';   
            }
            else if(objCon.Loan_Applications__r.Applicant_Customer_segment__c == objCon.Customer_Segment__c){
              
                Loan_Contact__c objApplicantCon = [Select Id, Loan_Applications__c, Loan_Applications__r.Applicant_Customer_segment__c, Customer_Segment__c, Industry__c, Sub_Industry__c, Nature_of_Business__c, /*Product__c,*/Activity_Type__c, Trading_Type__c, Skill_Level__c,/*Service__c,*/Platform_Type__c, Service_Industry_Type__c from Loan_Contact__c where Loan_Applications__c=: objCon.Loan_Applications__c and Applicant_Type__c = 'Applicant' ];
                if(objApplicantCon.Industry__c != null || objApplicantCon.Industry__c != ''){
                updateValues = true;    
                objCon.Industry__c = objApplicantCon.Industry__c; 
                }
                if(objApplicantCon.Nature_of_Business__c != null || objApplicantCon.Nature_of_Business__c != ''){
                updateValues = true;     
                objCon.Nature_of_Business__c = objApplicantCon.Nature_of_Business__c; 
                }
                if(objApplicantCon.Sub_Industry__c != null || objApplicantCon.Sub_Industry__c != ''){
                updateValues = true;     
                objCon.Sub_Industry__c = objApplicantCon.Sub_Industry__c;
                }
                if(objApplicantCon.Activity_Type__c != null || objApplicantCon.Activity_Type__c != ''){
                updateValues = true;     
                objCon.Activity_Type__c = objApplicantCon.Activity_Type__c; 
                }
                if(objApplicantCon.Trading_Type__c != null || objApplicantCon.Trading_Type__c != ''){
                updateValues = true;     
                objCon.Trading_Type__c = objApplicantCon.Trading_Type__c; 
                }
                if(objApplicantCon.Skill_Level__c != null || objApplicantCon.Skill_Level__c != ''){
                updateValues = true;     
                objCon.Skill_Level__c = objApplicantCon.Skill_Level__c;
                }
                /*if(objApplicantCon.Service__c != null || objApplicantCon.Service__c != ''){
                updateValues = true;     
                objCon.Service__c = objApplicantCon.Service__c;
                }*/
                if(objApplicantCon.Platform_Type__c != null || objApplicantCon.Platform_Type__c != ''){
                updateValues = true;     
                objCon.Platform_Type__c = objApplicantCon.Platform_Type__c; 
                }
                if(objApplicantCon.Service_Industry_Type__c != null || objApplicantCon.Service_Industry_Type__c != ''){
                updateValues = true;     
                objCon.Service_Industry_Type__c = objApplicantCon.Service_Industry_Type__c;
                }
                
                if(updateValues == true){
                    try{
                    update objCon;
                        return 'SUCCESS';
                    }
                    catch (DMLException e) {
                        return String.ValueOf(e);
                        
                    } catch (Exception e) {
                       return String.ValueOf(e); 
                    }
                }
                else{
                   
                    return 'Applicant has no values to be updated.';
                    
                }
                
            }
            
        }
        return null;
    }

}