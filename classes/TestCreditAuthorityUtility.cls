@isTest
public class TestCreditAuthorityUtility {
    public static testMethod void testdatamethod()
    {
        String str;
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        Profile p1 =[SELECT Id FROM Profile WHERE Name='Standard User'];
        

        Hierarchy__c objHierarchy = new Hierarchy__c();
        /*objHierarchy.BCM__c = u2.Id;
        objHierarchy.ZCM__c = u4.Id;
        objHierarchy.CCM__c = u2.Id;
        objHierarchy.CRO__c = u2.Id;*/
        insert objHierarchy;
        
        List<Hierarchy__c> lstHierarchy = [Select Id, Name, Active__c, BCM__c, BH__c, CCM__c, CRO__c, NCM__c, ZCM__c, ZCM_Salaried__c from Hierarchy__c where Active__c = true];
        String HierarchyName = lstHierarchy[0].Name;        
        User u1 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser001@amamama.com',
                     Username = 'puser001@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
            		 Hierarchy__c = HierarchyName,
            	     Non_Salaried_Authority__c = 'A11',
            		 Salaried_Authority__c = 'A11',
            		 Role_in_Credit_Hierarchy__c = 'NCM'
                     );
                     
        insert u1;
        
        User u2 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser002@amamama.com',
                     Username = 'puser002@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
             Non_Salaried_Authority__c = 'A2',
            		 Salaried_Authority__c = 'A2',
            		 Role_in_Credit_Hierarchy__c = 'BCM'
                     );
                     
        insert u2;
                
        User u3 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser003@amamama.com',
                     Username = 'puser003@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
             Non_Salaried_Authority__c = 'A2',
            		 Salaried_Authority__c = 'A2',
            		 Role_in_Credit_Hierarchy__c = 'BCM'
                     );
                     
        insert u3;
                
        User u4 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser004@amamama.com',
                     Username = 'puser004@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
            Non_Salaried_Authority__c = 'A13',
            		 Salaried_Authority__c = 'A13',
            		 Role_in_Credit_Hierarchy__c = 'ZCM'
                     );
                     
        insert u4;
               
       
        User u5 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser005@amamama.com',
                     Username = 'puser005@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
            		Hierarchy__c = HierarchyName,
            	     Non_Salaried_Authority__c = 'A1',
            		 Salaried_Authority__c = 'A1',
            		 Role_in_Credit_Hierarchy__c = 'BCM'
                     );
                     
        insert u5;
        
        User u6 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser001@amamama.com',
                     Username = 'puser001@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
            		 Hierarchy__c = HierarchyName,
            	     Non_Salaried_Authority__c = 'A5',
            		 Salaried_Authority__c = 'A5',
            		 Role_in_Credit_Hierarchy__c = 'CRO'
                     );
                     
        insert u6;
		
        objHierarchy.BCM__c = u2.Id;
        objHierarchy.ZCM__c = u4.Id;
        objHierarchy.NCM__c = u1.Id;
        objHierarchy.CRO__c = u6.Id;
        update objHierarchy;
       
        
        system.runAs(u5){
        
        Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='9923453331',Date_of_Incorporation__c=Date.newInstance(2018,11,11));
        insert Cob;
        
       /* 
        Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',Scheme_ID__c=11,Product_Code__c='HL');
        insert sc;*/
        
        /*Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,Transaction_type__c='PB',Requested_Amount__c=1000,Branch__c='test',Line_Of_Business__c='Open Market',Loan_Purpose__c='20');
        insert LAob;*/
        
                   
                            
         Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                     Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                     Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                     Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                     Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
                            
         Database.insert(sc); 
        
         Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,
                                                           //StageName__c='Credit Decisioning',
                                                           //Sub_Stage__c='Credit Review',
                                                           Transaction_type__c='SC',Requested_Amount__c=3000000,
                                                           Approved_Loan_Amount__c=3000000,
                                                           Applicant_Customer_segment__c='Salaried',
                                                           Branch__c='test',Line_Of_Business__c='Open Market',
                                                           //Assigned_Sales_User__c=u.id,
                                                           Loan_Purpose__c='20',
                                                           Requested_EMI_amount__c=1000,Approved_ROI__c=5,
                                                           Requested_Loan_Tenure__c=5,
                                                           Government_Programs__c='PMAY',
                                                           Loan_Engine_2_Output__c='STP',
                                                           Property_Identified__c=True,Group_Exposure_Amount__c = 6000000,ownerId=UserInfo.getUserID(),
                                                           Credit_Manager_User__c= u5.Id);
                                                           insert LAob;
            
        ID LoanID=LAob.Id;      
        str=String.valueof(LoanID);
        
        Credit_Deviation_Master__c CDMob1= new Credit_Deviation_Master__c(Name='Age',Approver_Level__c='BH',Status__c = True, Deviation_Level__c= 20);
        insert CDMob1;
        ID CredMasterID= CDMob1.Id;
        
        List<Loan_Contact__c> LCob=[select id from Loan_Contact__c where Loan_Applications__c=:LoanID];
        system.debug('*****loan Application ID'+LoanID);
        system.debug('*****Loan Conatctobject'+LCob); 
        //LAob.StageName__c='Credit Decisioning';
        //LAob.Sub_Stage__c='Credit: Data Checker';
        //update LAob;
                
        Applicable_Deviation__c  ADob= new Applicable_Deviation__c();
        ADob.Loan_Application__c=LAob.id;
        ADob.Customer_Details__c=LCob[0].id;
        ADob.Credit_Deviation_Master__c=CDMob1.id;
        insert ADob;   
       
               
        Branch_LOB_User_Mapping__c BLUMob= new Branch_LOB_User_Mapping__c(branch__c='test',Line_Of_Business__c='Open Market',BH__c=u1.id,NCH__c=u2.id,CCH__c=u3.id,ZCH__c=u4.id,CRO__c=u5.id,CEO__c=u5.id);
        insert BLUMob;
        
               
        String retrunedvalue= CreditAuthorityUtility.findCreditAuthority(str);
        }
    }     
    
    public static testMethod void testdatamethod1()
    {
        String str;
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        Profile p1 =[SELECT Id FROM Profile WHERE Name='Standard User'];
        

        Hierarchy__c objHierarchy = new Hierarchy__c();
        /*objHierarchy.BCM__c = u2.Id;
        objHierarchy.ZCM__c = u4.Id;
        objHierarchy.CCM__c = u2.Id;
        objHierarchy.CRO__c = u2.Id;*/
        insert objHierarchy;
        
        List<Hierarchy__c> lstHierarchy = [Select Id, Name, Active__c, BCM__c, BH__c, CCM__c, CRO__c, NCM__c, ZCM__c, ZCM_Salaried__c from Hierarchy__c where Active__c = true];
        String HierarchyName = lstHierarchy[0].Name;        
        User u1 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser001@amamama.com',
                     Username = 'puser001@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
            		 Hierarchy__c = HierarchyName,
            	     Non_Salaried_Authority__c = 'A2',
            		 Salaried_Authority__c = 'A2',
            		 Role_in_Credit_Hierarchy__c = 'NCM'
                     );
                     
        insert u1;
        
        User u2 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser002@amamama.com',
                     Username = 'puser002@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
             Non_Salaried_Authority__c = 'A2',
            		 Salaried_Authority__c = 'A2',
            		 Role_in_Credit_Hierarchy__c = 'BCM'
                     );
                     
        insert u2;
                
        User u3 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser003@amamama.com',
                     Username = 'puser003@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
             Non_Salaried_Authority__c = 'A2',
            		 Salaried_Authority__c = 'A2',
            		 Role_in_Credit_Hierarchy__c = 'BCM'
                     );
                     
        insert u3;
                
        User u4 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser004@amamama.com',
                     Username = 'puser004@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
            Non_Salaried_Authority__c = 'A1',
            		 Salaried_Authority__c = 'A1',
            		 Role_in_Credit_Hierarchy__c = 'ZCM'
                     );
                     
        insert u4;
               
       
        User u5 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser005@amamama.com',
                     Username = 'puser005@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
            		Hierarchy__c = HierarchyName,
            	     Non_Salaried_Authority__c = 'A1',
            		 Salaried_Authority__c = 'A1',
            		 Role_in_Credit_Hierarchy__c = 'BCM'
                     );
                     
        insert u5;
        
        User u6 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser001@amamama.com',
                     Username = 'puser001@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
            		 Hierarchy__c = HierarchyName,
            	     Non_Salaried_Authority__c = 'A5',
            		 Salaried_Authority__c = 'A5',
            		 Role_in_Credit_Hierarchy__c = 'CRO'
                     );
                     
        insert u6;
		
        objHierarchy.BCM__c = u2.Id;
        objHierarchy.ZCM__c = u4.Id;
        objHierarchy.NCM__c = u1.Id;
        objHierarchy.CRO__c = u6.Id;
        update objHierarchy;
       
        
        system.runAs(u5){
        
        Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='9923453331',Date_of_Incorporation__c=Date.newInstance(2018,11,11));
        insert Cob;
        
       /* 
        Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',Scheme_ID__c=11,Product_Code__c='HL');
        insert sc;*/
        
        /*Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,Transaction_type__c='PB',Requested_Amount__c=1000,Branch__c='test',Line_Of_Business__c='Open Market',Loan_Purpose__c='20');
        insert LAob;*/
        
                   
                            
         Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                     Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                     Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                     Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                     Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
                            
         Database.insert(sc); 
        
         Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,
                                                           //StageName__c='Credit Decisioning',
                                                           //Sub_Stage__c='Credit Review',
                                                           Transaction_type__c='SC',Requested_Amount__c=3000000,
                                                           Approved_Loan_Amount__c=3000000,
                                                           Applicant_Customer_segment__c='Salaried',
                                                           Branch__c='test',Line_Of_Business__c='Open Market',
                                                           //Assigned_Sales_User__c=u.id,
                                                           Loan_Purpose__c='20',
                                                           Requested_EMI_amount__c=1000,Approved_ROI__c=5,
                                                           Requested_Loan_Tenure__c=5,
                                                           Government_Programs__c='PMAY',
                                                           Loan_Engine_2_Output__c='STP',
                                                           Property_Identified__c=True,Group_Exposure_Amount__c = 6000000,ownerId=UserInfo.getUserID(),
                                                           Credit_Manager_User__c= u5.Id);
                                                           insert LAob;
            
        ID LoanID=LAob.Id;      
        str=String.valueof(LoanID);
        
        Credit_Deviation_Master__c CDMob1= new Credit_Deviation_Master__c(Name='Age',Approver_Level__c='BH',Status__c = True, Deviation_Level__c= 20);
        insert CDMob1;
        Credit_Deviation_Master__c CDMob2= new Credit_Deviation_Master__c(Name='Age Val',Approver_Level__c='NCH',Status__c = True, Deviation_Level__c= 30);
        insert CDMob2;
        ID CredMasterID= CDMob1.Id;
        
        List<Loan_Contact__c> LCob=[select id from Loan_Contact__c where Loan_Applications__c=:LoanID];
        system.debug('*****loan Application ID'+LoanID);
        system.debug('*****Loan Conatctobject'+LCob); 
        //LAob.StageName__c='Credit Decisioning';
        //LAob.Sub_Stage__c='Credit: Data Checker';
        //update LAob;
                
        Applicable_Deviation__c  ADob= new Applicable_Deviation__c();
        ADob.Loan_Application__c=LAob.id;
        ADob.Customer_Details__c=LCob[0].id;
        ADob.Credit_Deviation_Master__c=CDMob1.id;
        insert ADob;  
            
        Applicable_Deviation__c  ADob1= new Applicable_Deviation__c();
        ADob1.Loan_Application__c=LAob.id;
        ADob1.Customer_Details__c=LCob[0].id;
        ADob1.Credit_Deviation_Master__c=CDMob2.id;
        insert ADob1;
       
               
        Branch_LOB_User_Mapping__c BLUMob= new Branch_LOB_User_Mapping__c(branch__c='test',Line_Of_Business__c='Open Market',BH__c=u1.id,NCH__c=u2.id,CCH__c=u3.id,ZCH__c=u4.id,CRO__c=u5.id,CEO__c=u5.id);
        insert BLUMob;
        Branch_Wise_User_Hierarchy_Mapping__c objBracnh = new Branch_Wise_User_Hierarchy_Mapping__c();
        objBracnh.Hierachy__c = HierarchyName;
        objBracnh.User__c = u5.Id;
        objBracnh.Branch__c = 'test';
        insert objBracnh;
        test.startTest();
        String retrunedvalue= CreditAuthorityUtility.findCreditAuthority(str);
        test.stopTest();    
        }
    }   
    
    public static testMethod void testdatamethod3()
    {
        String str;
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        Profile p1 =[SELECT Id FROM Profile WHERE Name='Standard User'];
        

        Hierarchy__c objHierarchy = new Hierarchy__c();
        /*objHierarchy.BCM__c = u2.Id;
        objHierarchy.ZCM__c = u4.Id;
        objHierarchy.CCM__c = u2.Id;
        objHierarchy.CRO__c = u2.Id;*/
        insert objHierarchy;
        
        List<Hierarchy__c> lstHierarchy = [Select Id, Name, Active__c, BCM__c, BH__c, CCM__c, CRO__c, NCM__c, ZCM__c, ZCM_Salaried__c from Hierarchy__c where Active__c = true];
        String HierarchyName = lstHierarchy[0].Name;        
        User u1 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser001@amamama.com',
                     Username = 'puser001@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
            		 Hierarchy__c = HierarchyName,
            	     Non_Salaried_Authority__c = 'A11',
            		 Salaried_Authority__c = 'A11',
            		 Role_in_Credit_Hierarchy__c = 'NCM'
                     );
                     
        insert u1;
        
        User u2 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser002@amamama.com',
                     Username = 'puser002@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
             Non_Salaried_Authority__c = 'A2',
            		 Salaried_Authority__c = 'A2',
            		 Role_in_Credit_Hierarchy__c = 'BCM'
                     );
                     
        insert u2;
                
        User u3 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser003@amamama.com',
                     Username = 'puser003@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
             Non_Salaried_Authority__c = 'A2',
            		 Salaried_Authority__c = 'A2',
            		 Role_in_Credit_Hierarchy__c = 'BCM'
                     );
                     
        insert u3;
                
        User u4 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser004@amamama.com',
                     Username = 'puser004@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
            Non_Salaried_Authority__c = 'A13',
            		 Salaried_Authority__c = 'A13',
            		 Role_in_Credit_Hierarchy__c = 'ZCM'
                     );
                     
        insert u4;
               
       
        User u5 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser005@amamama.com',
                     Username = 'puser005@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
            		Hierarchy__c = HierarchyName,
            	     Non_Salaried_Authority__c = 'A1',
            		 Salaried_Authority__c = 'A1',
            		 Role_in_Credit_Hierarchy__c = 'BCM'
                     );
                     
        insert u5;
        
        User u6 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser001@amamama.com',
                     Username = 'puser001@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
            		 Hierarchy__c = HierarchyName,
            	     Non_Salaried_Authority__c = 'A5',
            		 Salaried_Authority__c = 'A5',
            		 Role_in_Credit_Hierarchy__c = 'CRO'
                     );
                     
        insert u6;
		
        objHierarchy.BCM__c = u2.Id;
        objHierarchy.ZCM__c = u4.Id;
        objHierarchy.NCM__c = u1.Id;
        objHierarchy.CRO__c = u6.Id;
        update objHierarchy;
       
        
        system.runAs(u5){
        
        Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='9923453331',Date_of_Incorporation__c=Date.newInstance(2018,11,11));
        insert Cob;
        
       /* 
        Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',Scheme_ID__c=11,Product_Code__c='HL');
        insert sc;*/
        
        /*Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,Transaction_type__c='PB',Requested_Amount__c=1000,Branch__c='test',Line_Of_Business__c='Open Market',Loan_Purpose__c='20');
        insert LAob;*/
        
                   
                            
         Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                     Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                     Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                     Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                     Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
                            
         Database.insert(sc); 
        
         Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,
                                                           //StageName__c='Credit Decisioning',
                                                           //Sub_Stage__c='Credit Review',
                                                           Transaction_type__c='SC',Requested_Amount__c=3000000,
                                                           Approved_Loan_Amount__c=3000000,
                                                           Applicant_Customer_segment__c='Salaried',
                                                           Branch__c='test',Line_Of_Business__c='Open Market',
                                                           //Assigned_Sales_User__c=u.id,
                                                           Loan_Purpose__c='20',
                                                           Requested_EMI_amount__c=1000,Approved_ROI__c=5,
                                                           Requested_Loan_Tenure__c=5,
                                                           Government_Programs__c='PMAY',
                                                           Loan_Engine_2_Output__c='STP',
                                                           Property_Identified__c=True,Group_Exposure_Amount__c = 6000000,ownerId=UserInfo.getUserID(),
                                                           Credit_Manager_User__c= u5.Id, Central_Salaried__c= true);
                                                           insert LAob;
            
        ID LoanID=LAob.Id;      
        str=String.valueof(LoanID);
        
        Credit_Deviation_Master__c CDMob1= new Credit_Deviation_Master__c(Name='Age',Approver_Level__c='BH',Status__c = True, Deviation_Level__c= 20);
        insert CDMob1;
        ID CredMasterID= CDMob1.Id;
        
        List<Loan_Contact__c> LCob=[select id from Loan_Contact__c where Loan_Applications__c=:LoanID];
        system.debug('*****loan Application ID'+LoanID);
        system.debug('*****Loan Conatctobject'+LCob); 
        //LAob.StageName__c='Credit Decisioning';
        //LAob.Sub_Stage__c='Credit: Data Checker';
        //update LAob;
                
        Applicable_Deviation__c  ADob= new Applicable_Deviation__c();
        ADob.Loan_Application__c=LAob.id;
        ADob.Customer_Details__c=LCob[0].id;
        ADob.Credit_Deviation_Master__c=CDMob1.id;
        insert ADob;   
       
               
        Branch_LOB_User_Mapping__c BLUMob= new Branch_LOB_User_Mapping__c(branch__c='test',Line_Of_Business__c='Open Market',BH__c=u1.id,NCH__c=u2.id,CCH__c=u3.id,ZCH__c=u4.id,CRO__c=u5.id,CEO__c=u5.id);
        insert BLUMob;
        
               
        String retrunedvalue= CreditAuthorityUtility.findCreditAuthority(str);
        }
    }
}