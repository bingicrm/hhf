public class CreditAuthorityApprovalController 
{
	//Below method added by Abhilekh on 18th September 2019 for FCU BRD
    @AuraEnabled
    public static Loan_Application__c getLAStatus(Id laId){
        Loan_Application__c la = [SELECT Id,Name,FCU_Decline_Count__c,Overall_FCU_Status__c from Loan_Application__c WHERE Id =: laId];
    	return la;
    }
	
    @auraenabled
    public static CreditAuthorityApprovalWrapper setApprovalStatus( String recordId,  String approvalStatus , String comments )
    {
        Loan_Application__c la = [Select Sub_stage__c,StageName__c,OwnerId,Owner_Manager__c,Assigned_Credit_Review__c,Assigned_Sales_User__c, Customer__r.PersonMobilePhone from Loan_Application__c where Id =:recordId];
        
        if(la.Sub_stage__c == 'L1 Credit Approval')
        {
            if( (String)Id.valueOf(UserInfo.getUserId()) != (String)Id.valueOf(la.Owner_Manager__c))
            {
                return new CreditAuthorityApprovalWrapper( false , 'User not authorized to perform this action');
            }
        }
        else if(la.Sub_stage__c == 'Credit Authority Approval')
        {
                if((String)Id.valueOf(UserInfo.getUserId()) != (String)Id.valueOf(la.Assigned_Credit_Review__c))
            {
                return new CreditAuthorityApprovalWrapper( false , 'User not authorized to perform this action');
            }
        }

        try
        {
            Loan_Application__c toUpdate = new Loan_Application__c();
            List<ProcessInstanceWorkitem> Piwi = [SELECT Id FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId= :recordId];
            
            if( approvalStatus == 'Approve')
            {
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments(comments);
                req2.setAction('Approve'); //This is the action that is approve in your case, you can set it to Reject also
                req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                req2.setWorkitemId(Piwi[0].Id);
                Approval.ProcessResult result2 =  Approval.process(req2);

                return new CreditAuthorityApprovalWrapper( true , 'Loan Application Approved');
            }
            /*else if( approvalStatus == 'Refer back to credit')
            {
                String msg = '';
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments(comments);
                req2.setAction('Reject'); //This is the action that is approve in your case, you can set it to Reject also
                req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                req2.setWorkitemId(Piwi[0].Id);         
                Approval.ProcessResult result2 =  Approval.process(req2);

                la.Sub_stage__c = 'Re-look';
                la.Credit_Status__c = 'FTNR';
                msg = 'Loan Application referred back to Credit.';
                List<FeedItem> items =  Utility.postToChatter(null,la.OwnerId,recordId,'Loan Application has been referred back.');
                if( items != null && items.size() > 0 )
                {
                    insert items;
                }
                update la;
                return new CreditAuthorityApprovalWrapper( true , msg);*/
            else if( approvalStatus == 'Reject' ||  approvalStatus == 'Refer back to credit')
            {
                //Loan_Application__c la = [Select Sub_stage__c,StageName__c,OwnerId,Assigned_Sales_User__c, Customer__r.PersonMobilePhone from Loan_Application__c where Id =:recordId];
                String msg = '';
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments(comments);
                req2.setAction('Reject'); //This is the action that is approve in your case, you can set it to Reject also
                req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                req2.setWorkitemId(Piwi[0].Id);         
                Approval.ProcessResult result2 =  Approval.process(req2);
 /***** Added by saumya for Code optimization *****/
        List<Loan_application__c> lstLoanApp = new List<Loan_application__c>(); 
                lstLoanApp.add(la);
                /***** Added by saumya for Code optimization *****/
                if( approvalStatus == 'Reject' )
                {
                    la.OwnerId = la.Assigned_Sales_User__c;
                    la.Sub_stage__c = 'Loan reject';
                    la.StageName__c = 'Loan Rejected';
                    la.Credit_Status__c = 'FTNR Rejected';
                    msg = 'Loan Application Rejected';
                    Communication_Trigger_Point__mdt ctp = [Select Id, Stage_Value__c, SMS_Template__c from Communication_Trigger_Point__mdt where Stage_Value__c =: Constants.Loan_Rejected];
                    SMSIntegration.SMSinit(la.Customer__r.PersonMobilePhone, ctp.SMS_Template__c);  
                    //List<FeedItem> items = Utility.postToChatter(null,la.Assigned_Sales_User__c,recordId,'Loan Application has been rejected by Credit');
					Set<Id> setLAIDs = new Set<Id>();
                    if(!lstLoanApp.isEmpty()) {
                        for(Loan_Application__c objLA : lstLoanApp) {
                            setLAIDs.add(objLA.Id);
                        }
                    }
                    if(!setLAIDs.isEmpty() && !Test.isRunningTest()) {
                        Utility.postToChatter(setLAIDs ,'Loan_Application__c');
                    }
                     // Added by Saumya for Code Optimization
                }

                if(approvalStatus == 'Refer back to credit')
                {
                    la.Sub_stage__c = 'Re-look';
                    la.Credit_Status__c = 'FTNR';
                    msg = 'Loan Application Referred back to Credit';
                    Set<Id> setLAIDs = new Set<Id>();
                    if(!lstLoanApp.isEmpty()) {
                        for(Loan_Application__c objLA : lstLoanApp) {
                            setLAIDs.add(objLA.Id);
                        }
                    }
                    if(!setLAIDs.isEmpty() && !Test.isRunningTest()) {
                        Utility.postToChatter(setLAIDs, 'Loan_Application__c');
                    }
                    //List<FeedItem> items =  Utility.postToChatter(null,la.OwnerId,recordId,'Loan Application has been Referred back'); // Commented by Saumya
                    //Utility.postToChatter(lstLoanApp); // Added by Saumya for Code Optimization
                    
                }
                update la;

                return new CreditAuthorityApprovalWrapper( true , msg);
            }
        }
        catch(DmlException e )
        {
            return new CreditAuthorityApprovalWrapper(false,e.getMessage());
        }
        return null;

    }

    public class CreditAuthorityApprovalWrapper
    {
        @auraenabled
        public boolean success;
        @auraenabled
        public String errorMessage;

        public CreditAuthorityApprovalWrapper(boolean success, String errorMessage)
        {
            this.success = success;
            this.errorMessage = errorMessage;
        }

    }
    
    @AuraEnabled
    public static List<String> getPickListValuesIntoList(){
       List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Loan_Application__c.Rejection_Reason__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }
        return pickListValuesList;
    }
    
    @AuraEnabled
    public static List<String> getPickListValuesIntoList2(){
       List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Loan_Application__c.Severity_Level__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }
        return pickListValuesList;
    }
    
    @AuraEnabled
    public static CreditAuthorityApprovalWrapper rejectApplication(String recordID, String reason, String remark, String level){
        Loan_Application__c la = [SELECT Id, recordTypeId, Severity_Level__c, Remarks_Rejection__c, Rejection_Reason__c, ownerId, Assigned_Sales_User__c, Date_of_Rejection__c, StageName__c, Sub_Stage__c 
                                  FROM Loan_Application__c
                                  WHERE Id =: recordID];
        la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Cancelled/Rejected').getRecordTypeId();
        la.ownerId = la.Assigned_Sales_User__c;
        la.Date_of_Rejection__c = System.TODAY();
        la.StageName__c = 'Loan Rejected';
        la.Sub_Stage__c = 'Loan reject';
        la.Severity_Level__c = level;
        la.Remarks_Rejection__c = remark;
        la.Rejection_Reason__c = reason;
        update la;
        return new CreditAuthorityApprovalWrapper( true , 'Loan Application Rejected.');
    }
}