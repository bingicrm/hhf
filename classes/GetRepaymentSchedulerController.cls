public class GetRepaymentSchedulerController {
	
	//Below method added by Abhilekh on 24th January 2020 for Hunter BRD
    @AuraEnabled
    public static Boolean checkHunterDecline(Id laId){
        String profileName = [SELECT Id,Name from Profile WHERE Id =: UserInfo.getProfileId()].Name;
        
        Loan_Application__c la = [SELECT Id,Name,StageName__c,Sub_Stage__c,Hunter_Decline_Count__c from Loan_Application__c WHERE Id =: laId];
        
        if(profileName == 'Credit Team' && (la.Sub_Stage__c == Constants.Credit_Review || la.Sub_Stage__c == Constants.Re_Credit || la.Sub_Stage__c == Constants.Re_Look) && la.Hunter_Decline_Count__c > 0){
            return true;
        }
        else{
            return false;
        }
    }
	
	//Below method added by Abhilekh on 5th October 2019 for FCU BRD
    @AuraEnabled
    public static Boolean checkFDDecline(Id laId){
        String profileName = [SELECT Id,Name from Profile WHERE Id =: UserInfo.getProfileId()].Name;
        
        Loan_Application__c la = [SELECT Id,Name,StageName__c,Sub_Stage__c,Overall_FCU_Status__c,FCU_Decline_Count__c,Overall_Post_Sanction_FCU_Status__c,Post_Sanction_FCU_Decline_Count__c,Exempted_from_FCU_BRD__c from Loan_Application__c WHERE Id =: laId];
        //Overall_Post_Sanction_FCU_Status__c,Post_Sanction_FCU_Decline_Count__c added by Abhilekh on 30th October 2019 for Post Sanction FCU BRD
        //Exempted_from_FCU_BRD__c added by Abhilekh on 23rd December 2019 for FCU BRD Exemption
        
        //&& la.Exempted_from_FCU_BRD__c == false added by Abhilekh on 23rd December 2019 for FCU BRD Exemption
        if(profileName == 'Credit Team' && (la.Sub_Stage__c == Constants.Credit_Review || la.Sub_Stage__c == Constants.Re_Credit || la.Sub_Stage__c == Constants.Re_Look) && la.FCU_Decline_Count__c > 0 && la.Overall_FCU_Status__c == Constants.NEGATIVE && la.Exempted_from_FCU_BRD__c == false){
            return true;
        }
        //Below else if block added by Abhilekh on 30th October 2019 for Post Sanction FCU BRD,&& la.Exempted_from_FCU_BRD__c == false added by Abhilekh on 23rd December 2019 for FCU BRD Exemption
        else if(profileName == 'Credit Team' && la.Sub_Stage__c == Constants.Disbursement_Maker && la.Post_Sanction_FCU_Decline_Count__c > 0 && la.Overall_Post_Sanction_FCU_Status__c == Constants.NEGATIVE && la.Exempted_from_FCU_BRD__c == false){
            return true;
        }
        else{
            return false;
        }
    }
	
    @auraEnabled
    public static String saveRepayment( Loan_Application__c la, Loan_Repayment__c lr )
    {
        system.debug(Lr);
        Date dt = Lr.Disbursal_Date__c; //Date.today();
        Lr.EMI_Start_date__c = lr.dueDay__c != null ? Date.newInstance(dt.year(),dt.month(),Integer.valueOf(lr.dueDay__c)) : Date.newInstance(dt.year(),dt.month(),8);
        system.debug('@@Lr.Disbursal_Date__c' + Lr.Disbursal_Date__c);
        if(Lr.EMI_Start_date__c < Lr.Disbursal_Date__c)
        {
            system.debug('in if @@Lr.EMI_Start_date__c' + Lr.EMI_Start_date__c);
            Lr.EMI_Start_date__c = lr.EMI_Start_date__c.addMonths(1);
        }
        //.Interest_StartDate__c = lr.Disbursal_Date__c;
        if(Lr.Moratorium_Flag__c == 'No')
        {
            Lr.Moratorium_charge_interest_adjust_in_sch__c = null;
            Lr.Moratorium_Charge_interest__c = null;
            Lr.Moratorium_impact_on_tenure__c = null;
        }
        Lr.Repayment_effective_date__c = Lr.Disbursal_Date__c;
        Lr.Interest_StartDate__c = Lr.Disbursal_Date__c;
        lr.Disbursement_Amount__c = lr.Sanction_Amount__c;

        update lr;
        List<Graded_Details__c> toDel = [select id from Graded_Details__c where Loan_Repayment__c = : lr.Id];
        system.debug('toDel @@' + toDel );
        if( toDel.size() > 0 )
        {
            delete toDel;
        }

        return lr.Id;
    }

    @auraEnabled
    public static void insertGradedDetails(List<Graded_Details__c> allGraded, String parentId )
    {
        for( Graded_Details__c gd: allGraded)
        {
            if ( gd.Loan_Repayment__c == null ) {
                gd.Loan_Repayment__c = parentId;
               
            }
             gd.id = null;
        }

        if(allGraded.size() > 0 )
        {
            insert allGraded;
        }
    }
    
    @auraEnabled
    public static Boolean validateUser(Id recordId){
        System.debug('>>>>'+recordId);
        List<Loan_Application__c> lstLoanApp = [Select Id, Sub_Stage__c from Loan_Application__c where Id =: recordId];
        if ( !lstLoanApp.isEmpty() && lstLoanApp[0].Sub_Stage__c == 'Hand Sight') {
            return false;
        } else {
            return true;
        }
    
    }

    @auraEnabled
    public static Boolean getDisbursementType(Id recordId) {
        System.debug('>>>>'+recordId);
        List<Loan_Application__c> lstLoanApp = [Select Id, Sub_Stage__c, Sanctioned_Disbursement_Type__c from Loan_Application__c where Id =: recordId];
        if ( !lstLoanApp.isEmpty() && lstLoanApp[0].Sanctioned_Disbursement_Type__c == 'Multiple Tranche') {
            return true;
        } else {
            return false;
        }
    }
    
    @auraEnabled
    public static Map<String,String> getDueDayMaster(String LaId)
    {
        Map<String,String> getMaster = new Map<String, String>();
        List<Loan_Application__c> loanApp = [Select Id,Scheme__c  from Loan_Application__c where id = :LaId];
        if(loanApp.size() > 0)
        {
            List<Scheme_Due_Date__c> allSchemes  = [Select Due_Date__c,Scheme__c From Scheme_Due_Date__c where Scheme__c = :loanApp[0].Scheme__c AND Active__c=true ORDER BY Due_Date__c desc];//Added by Ankit for TIL-00002471
            system.debug('@@allSchemes ' + allSchemes );
            for( Scheme_Due_Date__c dueDate  : allSchemes )
            {
                getMaster.put(String.valueOf(dueDate.Due_Date__c),String.valueOf(dueDate.Due_Date__c));
                
            }
        }
		//Added by Abhishek for TIL-00002444 - START
        for(Loan_Repayment__c LRNObj : [SELECT Id, dueDay__c FROM Loan_Repayment__c WHERE Loan_Application__c = :LaId]) {
            getMaster.put(String.valueOf(LRNObj.dueDay__c),String.valueOf(LRNObj.dueDay__c));
        }
        //Added by Abhishek for TIL-00002444 - END									  
         system.debug('@@getMaster' + getMaster);
        return getMaster;
    }   

    @auraEnabled
    public static String getSchedule( String lrId , String LaId)
    {
        LMSRepaymentSchedule.RepaymentResponseWrapper res = LMSRepaymentSchedule.pullRepaymentSchedule(lrId,LaId);
        system.debug('Debug');
        system.debug(res);
        return JSON.serialize(res);
    }

    public static Date getDate( Integer day , Date disbDate )
    {
        date myDate = date.newInstance(Date.today().year(), Date.today().month(), day);
        system.debug(disbDate.daysBetween(myDate));
        if( myDate < disbDate)
        {
            //myDate = date.newInstance(Date.today().year(), Date.today().addMonths(1).month(), day);
            myDate = myDate.addMonths(1);
        }
        system.debug('final - 1'+ myDate);
        if( (disbDate.daysBetween(myDate)) < 30)
        {

            //myDate = date.newInstance(Date.today().year(), myDate.addMonths(1).month(), day);
             myDate = myDate.addMonths(1);
            system.debug('final 0'+ myDate);
        }
        system.debug('final'+ myDate);
        return myDate;
    }

    @auraEnabled
    public static Loan_Repayment__c getRepaymentRecord( String LaId )
    {
        if( String.isNotBlank(laId) )
        {
            List<Loan_Repayment__c> repayments = [Select dueDay__c, Loan_Application__r.StageName__c, Tenure__c, Sanction_Amount__c, RescheduleFlag__c, RequestId__c, Repayment_effective_date__c, Repayment_Drawn_On__c, RepayType__c,
                                        Rate_Type__c, Number_of_Advance_Installment__c, Negative_Capitalization_Flag__c, Moratorium_period__c, Moratorium_impact_on_tenure__c, 
                                        Moratorium_charge_interest_adjust_in_sch__c, Moratorium_Flag__c, Moratorium_Charge_interest__c, Loan_Application__c, Interest_StartDate__c, Interest_Round_off_parameter__c, Interest_Round_off_parameter_Flag__c, Interest_Rate__c, Instalment_Round_off_parameter__c, Instalment_Round_off_parameter_Flag__c,
                                        Installment_Plan__c,Installment_Mode__c,Baloon_Amount__c, IRR_calculation_parameters__c, Frequency__c,EMI_Start_date__c,EMI_Amount__c,Disbursement_Amount__c, Disbursal_Date__c,Days_Per_Year__c,
                                        Bulk_refund__c,Broken_Period_Interest_Handing__c,Advanced_Installment_Flag__c,Loan_Application__r.Insurance_Loan_Application__c,Loan_Application__r.Business_Date_Modified__c,/*** Added by Saumya for Insurance Laon ***/
                                        (Select Slab_and_EMI_Seq_No__c, Slab_and_EMI_EMI__c, Slab_and_EMI_Recovery__c, Slab_and_EMI_Instl_From__c, Slab_and_EMI_Instl_To__c From Graded_Details__r order by Slab_and_EMI_Seq_No__c ASC)
                                        From Loan_Repayment__c 
                                        where Loan_Application__c = :laId
                                        order by createdDate DESC];
			System.debug('repayments::'+repayments);										
            
            List<Loan_Application__c> lstApp = [Select id,StageName__c,Sub_Stage__c,Disbursed_Amount__c , (Select Id, Disbursal_Amount__c from Disbursement__r WHERE Recordtype.Name = 'First Disbursement' OR Recordtype.Name = 'Others' ORDER BY CreatedDate DESC ),
                                            Approved_Loan_Amount__c,Requested_Amount__c,Approved_Loan_Tenure__c,Business_Date_Modified__c,Requested_EMI_amount__c,Approved_ROI__c,Requested_Loan_Tenure__c from Loan_Application__c where id = : laId];
            
            Loan_Application__c loanApp = new Loan_Application__c ();
            if ( lstApp != null)
                loanApp = lstApp[0]; 
            List<LMS_Date_Sync__c> lstLMS = [SELECT Id, LastModifiedDate, Current_Date__c FROM LMS_Date_Sync__c order by LastModifiedDate desc LIMIT 1];
                
            if(repayments.size() == 0 )
            {
                
                Loan_Repayment__c lr = new Loan_Repayment__c();
                Lr.Disbursal_Date__c = !lstLMS.isEMpty() ? lstLMS[0].Current_Date__c : null;                
                Lr.Moratorium_Flag__c = 'No';
                Lr.Rate_Type__c = 'Rate';// Added by Saumya For TIL-00000316
                lr.Broken_Period_Interest_Handing__c = loanApp.Sub_Stage__c == 'Disbursement Maker'  ? 'Return as charge' : 'Add to Schedule';
                lr.Installment_Plan__c = 'Equated Instl';
                lr.Moratorium_period__c = 0;
                lr.Advanced_Installment_Flag__c = 'Charge upfront (reduce opening principal)';
                lr.Number_of_Advance_Installment__c = 0;
                lr.RepayType__c = 'Structured';
                lr.Repayment_Drawn_On__c = 'Sanctioned Amount';
                lr.Installment_Mode__c = 'Arrear';
                lr.Disbursement_Amount__c = loanApp.Approved_Loan_Amount__c;
                lr.Sanction_Amount__c = loanApp.Sub_Stage__c == 'Disbursement Maker' && !loanApp.Disbursement__r.isEmpty() ?  loanApp.Disbursement__r[0].Disbursal_Amount__c: loanApp.Approved_Loan_Amount__c;
                lr.EMI_Amount__c = loanApp.Requested_EMI_amount__c !=null ? loanApp.Requested_EMI_amount__c : 0 ;
                Lr.Interest_StartDate__c = lr.Disbursal_Date__c;
                lr.Interest_Rate__c = loanApp.Approved_ROI__c;
               // lr.Tenure__c = loanApp.Requested_Loan_Tenure__c;
                lr.Tenure__c = loanApp.Approved_Loan_Tenure__c;// Added by Saumya For TIL-00000316  
                lr.Interest_Round_off_parameter__c = 0;
                lr.Instalment_Round_off_parameter__c = 0;
                Lr.IRR_calculation_parameters__c = String.valueOf('0');
                system.debug(lr.Disbursal_Date__c);
                Lr.EMI_Start_date__c = Lr.Interest_StartDate__c;
                Lr.Repayment_effective_date__c = Lr.Interest_StartDate__c;
                lr.Days_Per_Year__c = 'ACT';
                lr.Loan_Application__c = loanApp.Id;
                Lr.dueDay__c = null;
                Lr.Baloon_Amount__c = 0;
                insert lr;
                List<Loan_Repayment__c> lstLR = new List<Loan_Repayment__c>();
				lstLR =[SELECT Id, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, LastViewedDate, LastReferencedDate, Loan_Application__c, Additional_Disbursement__c, Advanced_Installment_Flag__c, Baloon_Amount__c, Broken_Period_Interest_Handing__c, Bulk_refund__c, Days_Per_Year__c, Disbursal_Date__c, Disbursement_Amount__c, EMI_Amount__c, EMI_Start_date__c, Frequency__c, IRR_calculation_parameters__c, Installment_Mode__c, Installment_Plan__c, Instalment_Round_off_parameter_Flag__c, Instalment_Round_off_parameter__c, Interest_Rate__c, Interest_Round_off_parameter_Flag__c, Interest_Round_off_parameter__c, Interest_StartDate__c, Moratorium_Charge_interest__c, Moratorium_Flag__c, Moratorium_charge_interest_adjust_in_sch__c, Moratorium_impact_on_tenure__c, Moratorium_period__c, Negative_Capitalization_Flag__c, Number_of_Advance_Installment__c, Rate_Type__c, RepayType__c, Repayment_Drawn_On__c, Repayment_effective_date__c, RequestId__c, RescheduleFlag__c, Sanction_Amount__c, Tenure__c, dueDay__c, Loan_Application__r.StageName__c , Loan_Application__r.Approved_Loan_Amount__c, Loan_Application__r.Sub_Stage__c, Loan_Application__r.Requested_EMI_amount__c, Loan_Application__r.Approved_ROI__c, Loan_Application__r.Requested_Loan_Tenure__c, Loan_Application__r.Disbursed_Amount__c, Loan_Application__r.Requested_Amount__c, Loan_Application__r.Business_Date_Modified__c, Loan_Application__r.Insurance_Loan_Application__c FROM Loan_Repayment__c where id= :Lr.Id];
				if(lstLR.size() > 0)
				return lstLR[0];/*** Added by Saumya Insurance Loan Issue ***/
            }
            else
            {
                repayments[0].Disbursal_Date__c = !lstLMS.isEMpty() ? lstLMS[0].Current_Date__c : null;   
                repayments[0].Sanction_Amount__c = loanApp.Sub_Stage__c == 'Disbursement Maker' && !loanApp.Disbursement__r.isEmpty() ? loanApp.Disbursement__r[0].Disbursal_Amount__c: loanApp.Approved_Loan_Amount__c;
                repayments[0].Broken_Period_Interest_Handing__c = loanApp.Sub_Stage__c == 'Disbursement Maker' ? 'Return as charge' : 'Add to Schedule';
                repayments[0].Interest_Rate__c = loanApp.Approved_ROI__c;
				repayments[0].Rate_Type__c = 'Rate';
				repayments[0].Tenure__c = loanApp.Approved_Loan_Tenure__c; //Added by Abhilekh on 2nd July 2019 for TIL-1230
                if (loanApp.Sub_Stage__c == 'Disbursement Maker') {
                    repayments[0].Installment_Plan__c = 'Equated Instl';
                } else{
                    repayments[0].Disbursement_Amount__c = loanApp.Approved_Loan_Amount__c;
                    
                }
                
                 system.debug('@@repayments' + repayments[0]);
                return repayments[0];
            }
           

        }
        return null;

    }

}