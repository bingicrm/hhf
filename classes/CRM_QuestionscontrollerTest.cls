@isTest
Public class CRM_QuestionscontrollerTest{

     static testMethod void Questionscontroller() {
     
     Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
    User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');
     CRM_Survey_Question__c q1 = new CRM_Survey_Question__c ( Answer_List__c = 'TestAns1#TestAns2#TestAns3', Is_Active__c= true,Order_Number__c=1, Question__c ='TestQuestion1'); 
     CRM_Survey_Question__c q2 = new CRM_Survey_Question__c ( Answer_List__c  = 'TestAns1#TestAns2#TestAns3', Is_Active__c= true,Order_Number__c=2, Question__c ='TestQuestion2');
     CRM_Survey_Question__c q3 = new CRM_Survey_Question__c ( Answer_List__c  = 'TestAns1#TestAns2#TestAns3', Is_Active__c= true,Order_Number__c=3, Question__c ='TestQuestion3');
     CRM_Survey_Question__c q4 = new CRM_Survey_Question__c ( Answer_List__c  = 'TestAns1#TestAns2#TestAns3', Is_Active__c= true,Order_Number__c=4, Question__c ='TestQuestion4');
     CRM_Survey_Question__c q5 = new CRM_Survey_Question__c ( Answer_List__c  = 'TestAns1#TestAns2#TestAns3', Is_Active__c= true,Order_Number__c=5, Question__c ='TestQuestion5');
     CRM_Survey_Question__c q6 = new CRM_Survey_Question__c ( Answer_List__c  = 'TestAns#TestAns#TestAns', Is_Active__c= true,Order_Number__c=6, Question__c ='TestQuestion6');
     CRM_Survey_Question__c q7 = new CRM_Survey_Question__c ( Answer_List__c  = 'TestAns#TestAns#TestAns', Is_Active__c= true,Order_Number__c=7, Question__c ='TestQuestion7');
     
     insert q1;
     insert q2;
     insert q3;
     insert q4;
     insert q5;
     insert q6;
     insert q7;
     
     
     Test.startTest();
     
    CRM_Questionscontroller qc = new CRM_Questionscontroller();
    qc.getQuestionOptions();
    qc.getQuestion2Options();
    qc.getQuestion3Options();
    qc.getQuestion4Options();
    qc.getQuestion5Options();
    qc.getQuestion6Options();
    
    PageReference pg= qc.save();
      Test.stopTest();
    //ScheduleTerritoryInactiveMemberCheck sh1 = new ScheduleTerritoryInactiveMemberCheck();

    //String sch = '0 0 23 * * ?'; system.schedule('Test Territory Check', sch, sh1); Test.stopTest();
    
    
     }
 
     
   
    
}