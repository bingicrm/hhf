public class EnqueDMLGSTINOne implements Queueable {
    public Customer_Integration__c ciRecord;
    public String strRequest;
    public String strResponse;
    public String fillingFreq;
    public String strURL;
    public EnqueDMLGSTINOne(Customer_Integration__c ciRecord,String fillingFreq, String strRequest, String strResponse, String strURL){
        system.debug('Debug for ciRecord1' + ciRecord);
        system.debug('Debug for strRequest1' + strRequest);
        system.debug('Debug for strResponse1' + strResponse);
        system.debug('Debug for strURL1' + strURL);
        this.ciRecord = ciRecord;
        this.strRequest = strRequest;
        this.strResponse = strResponse;
        this.strURL = strURL;
        this.fillingFreq = fillingFreq;
    }
    public void execute(QueueableContext context) {
      Customer_Integration_For_GSTIN__c passValidationCheck =  Customer_Integration_For_GSTIN__c.getInstance();
      
        if(ciRecord.Id != null){
            passValidationCheck.CI_GSTIN_Editable__c = true;
            upsert passValidationCheck; 
            ciRecord.Filling_Frequency__c = fillingFreq;
            update ciRecord;
            if(passValidationCheck.CI_GSTIN_Editable__c){
                passValidationCheck.CI_GSTIN_Editable__c = false;
            upsert passValidationCheck; 
            }
            system.debug('GSTRecord.FillingFreq  ' + ciRecord.Filling_Frequency__c  );
        }
        if(ciRecord.Loan_Contact__c != null){
            Loan_Contact__c loanContact = [Select id,GST_ReturnStatus__c,GST_Initiated__c from Loan_Contact__c where id =: ciRecord.Loan_Contact__c limit 1];
            if(loanContact.Id != null){ 
                loanContact.GST_ReturnStatus__c = true;
                update loanContact;
            }
        }
       
        HttpResponse res = new HttpResponse();
        res.setBody(strResponse);  
        LogUtility.createIntegrationLogs(strRequest,res,strURL);
    }
}