/*=====================================================================
 * Deloitte India
 * Name: PerfiosBankDetailsFetcher
 * Description: This class fetch bank details by interacting with perfios API & stores the data in custom setting
 * Created Date: [03/07/2019]
 * Created By: Vaishali Mehta(Deloitte India)
 *
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/
global class PerfiosBankDetailsFetcher implements Schedulable {
//  public class PerfiosBankDetailsFetcher{
    global void execute(SchedulableContext SC) {
    //  public PerfiosBankDetailsFetcher() {
        List<Rest_Service__mdt> lstOfIntegrationUserDetails = [SELECT Client_Password__c, Client_Username__c, Request_Method__c, 
        Time_Out_Period__c, Service_EndPoint__c, Content_Type__c, Request_Body__c FROM Rest_Service__mdt WHERE Label='PerfiosBankDetailsFetcher'];
        
        if(!lstOfIntegrationUserDetails.isEmpty()) {
            Map<String,String> headerMap = new Map<String,String>();
            Blob headerValue = Blob.valueOf(lstOfIntegrationUserDetails[0].Client_Username__c + ':' + lstOfIntegrationUserDetails[0].Client_Password__c);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            headerMap.put('Content-Type', lstOfIntegrationUserDetails[0].Content_Type__c);
            headerMap.put('vendorId', 'HEROFINCORPLTD');
            headerMap.put('Authorization', authorizationHeader);
            headerMap.put('Username', lstOfIntegrationUserDetails[0].Client_Username__c);
            headerMap.put('Password', lstOfIntegrationUserDetails[0].Client_Password__c);
            //String jsonRequest = lstOfIntegrationUserDetails[0].Request_Body__c;
            String jsonRequest = '{"apiVersion": "2.1", "destination": "statement"}';
            sendRequest(lstOfIntegrationUserDetails[0].Service_EndPoint__c, lstOfIntegrationUserDetails[0].Request_Method__c, jsonRequest, headerMap, Integer.valueOf(lstOfIntegrationUserDetails[0].Time_Out_Period__c) );
             
        }   
    }
    
    @future(callout=true)
     public static void sendRequest(String endPointURL, String method, String body , Map<String,String> header, Integer timeOut ){
        
        HTTP http = new HTTP();
        HTTPRequest httpReq = new HTTPRequest();
        HTTPResponse res;
        httpReq.setEndPoint(endPointURL);
        httpReq.setMethod(method);
        system.debug('==header=='+header);
        for(String headerKey : header.keySet()){
            httpReq.setHeader(headerKey, header.get(headerKey));
        }
        httpReq.setBody(body);
        httpReq.setTimeOut(50000);
        system.debug(body);
        try
        {
            res = http.send(httpReq);
            system.debug('@@@@in utility' + res );
            if( res != null && res.getStatusCode() == 200 )
            {
                system.debug('===response==== '+res.getBody());
                List<JSON2ApexSuccess> wrapper = new List<JSON2ApexSuccess>();
                try {
                    wrapper = (List<JSON2ApexSuccess>)JSON.deserialize(res.getBody(), List<JSON2ApexSuccess>.class);
                    system.debug('WRAPPER '+wrapper);
                }
                catch (Exception e) {
                    JSON2ApexError errorWrapper = (JSON2ApexError)JSON.deserialize(res.getBody(),JSON2ApexError.class);
                    system.debug('ERROR WRAPPER '+wrapper);
                }    
                
                List<Perfios_Bank_Information__c> lstOfBankDetailRecords = new List<Perfios_Bank_Information__c>();
                for(JSON2ApexSuccess wrappedRecord: wrapper) {
                    Perfios_Bank_Information__c bankDetailRecord = new Perfios_Bank_Information__c();
                    bankDetailRecord.Name = wrappedRecord.name;
                    bankDetailRecord.Bank_Name__c = wrappedRecord.id;
                    lstOfBankDetailRecords.add(bankDetailRecord);
                }
                system.debug('lstOfBankDetailRecords '+lstOfBankDetailRecords);
                try {
                    upsert lstOfBankDetailRecords name;
                }
                catch( Exception e ) {
                    system.debug('===Exception===='+e.getMessage());  
                }        
            }   
            //LogUtility.createLogs('Integration', 'Debug', 'Apex Class', 'URL : '+endPointURL,'', httpRes.getBody(), '', true);
        }
        catch(Exception ex)
        {
            LogUtility.createLogs('Error', 'Error', 'Apex Class', 'URL : '+endPointURL, ex.getStackTraceString(), ex.getMessage(), '', true);
        }
        
        LogUtility.createIntegrationLogs(body,res,endPointURL);
    }
    
    public class JSON2ApexSuccess {
        public String addressAvailable;
        public String form26AsAvailable;
        public String id;
        public String institutionType;
        public String itrVAvailable;
        public String name;
        public String originalStatementAvailable;    
    }
    
    public class JSON2ApexError {
        public String status;
        public Error error;
    }
    
    public class Error {
            public Integer errorCode;
            public String errorType;
            public String errorMessage;
    } 
}