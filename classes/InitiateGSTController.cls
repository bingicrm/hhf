public class InitiateGSTController {
  /* @Description :- This method fetch the list of Loan Contacts and return back to GenerateGSTLink Lightning Component.
* @param :- id of LoanApplication
* @author :- Chitransh Porwal
* @returns :- wrapper of type cls_LoanContactListWrapper
*/ 
    @AuraEnabled
    public static List<cls_LoanContactListWrapper> getCriteriaList(Id loanApplicationId) {
        List<cls_LoanContactListWrapper> lstLoanContactWrapper = new List<cls_LoanContactListWrapper>();
        
        List<Loan_Contact__c> lstRelatedLoanContacts = [Select Id,Name,Contact__r.Email,Contact__r.MobilePhone,Borrower__c,Customer__r.Name,Constitution__c,Applicant_Type__c, Customer_segment__c, GSTIN_Number__c, Pan_Number__c,Income_Considered__c,RecordTypeId,Email__c, Mobile__c,Loan_Applications__c from Loan_Contact__c where  Loan_Applications__c =: loanApplicationId and Income_Considered__c = true and GSTIN_Number__c != NULL];//: Income_Considered__c = true and GSTIN_Number__c != NULL and// loanApplicationId];//[Select Id, Name, Borrower__c, Applicant_Type__c, Constitution__c, Customer__r.Name, Pan_Number__c, GSTIN_Number__c From Loan_Contact__c Where Loan_Applications__c =: loanApplicationId];
        system.debug('lstRelatedLoanContacts' + lstRelatedLoanContacts);
        
        //Fetch the Values of Picklist named Customer_segment__c from it's API
        Map<String,String> SegmentlabelToValueMap = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult1 = Loan_Contact__c.Customer_segment__c.getDescribe();  
        List<Schema.PicklistEntry> ple = fieldResult1.getPicklistValues();  
        for (Schema.PicklistEntry f : ple) {  
            SegmentlabelToValueMap.put(f.getValue(),f.getLabel());
        } 
        System.debug('SegmentlabelToValueMap ' + SegmentlabelToValueMap);
        
        //Fetch the values of Picklist named Constitution__c from it's API
        Map<String,String> ConstitutionlabelToValueMap = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult2 = Loan_Contact__c.Constitution__c.getDescribe();  
        List<Schema.PicklistEntry> plentry = fieldResult2.getPicklistValues();  
        for (Schema.PicklistEntry f : plentry) {  
            ConstitutionlabelToValueMap.put(f.getValue(),f.getLabel());
        } 
        System.debug('ConstitutionlabelToValueMap ' + ConstitutionlabelToValueMap);
        
        
        if(!lstRelatedLoanContacts.isEmpty()) {
            for(Loan_Contact__c objLoanContact : lstRelatedLoanContacts) {
                String segmentLabel = '';
                String constitutionlabel = '';
                if(SegmentlabelToValueMap.KeySet().contains(objLoanContact.Customer_segment__c)){
                    segmentLabel = SegmentlabelToValueMap.get(objLoanContact.Customer_segment__c);
                }
                if(ConstitutionlabelToValueMap.keyset().contains(objLoanContact.Constitution__c)){
                    constitutionlabel = ConstitutionlabelToValueMap.get(objLoanContact.Constitution__c);
                }
                
                lstLoanContactWrapper.add(new cls_LoanContactListWrapper(false, objLoanContact,segmentLabel,constitutionlabel));
            }
            return lstLoanContactWrapper;
            
        }
        else {
            return new List<cls_LoanContactListWrapper>();
        }
    }
    /* @Description :- This method used to make a call to makeGstinAuthDetailsCallout METHOD of GstinAuthDetails CLASS
* @param :- String
* @author :- Chitransh Porwal
* @returns :- void
*/     
    @AuraEnabled
    public static void makeGSTINAuthCallout(String paramJSONList) {
        
        System.debug('Debug Log for param list authCallout'+paramJSONList);
        List<Loan_Contact__c> lstLoanContact = new List<Loan_Contact__c>();
        List<cls_LoanContactListWrapper> lstSerializedWrapper = (List<cls_LoanContactListWrapper>)JSON.deserialize(paramJSONList,List<cls_LoanContactListWrapper>.class);
        System.debug('Debug Log for deserialized List'+lstSerializedWrapper.size());
        System.debug('Debug Log for deserialized List'+lstSerializedWrapper);
        
        if(!lstSerializedWrapper.isEmpty()) {
            for(cls_LoanContactListWrapper objcls_LoanContactListWrapper : lstSerializedWrapper) {
                //if(objcls_LoanContactListWrapper.isChecked)
                    lstLoanContact.add(objcls_LoanContactListWrapper.objLoanContact);
                    
            }
            System.debug('Debug Log for lstLoanContact'+lstLoanContact.size());
            System.debug('Debug Log for lstLoanContact'+lstLoanContact);
            if(!lstLoanContact.isEmpty()) {
                Integer Counter = 0;
                for(Loan_Contact__c objLoanContact : lstLoanContact) {
                    if(String.isNotBlank(objLoanContact.GSTIN_Number__c)) {
                        Counter += 1;
                        System.debug('Borrower Type ' +objLoanContact.Borrower__c );
                        String mobNo = '';
                        String emailId = '';
                        if(objLoanContact.Borrower__c == '2'){
                            mobNo = objLoanContact.Contact__r.MobilePhone;
                            emailId = objLoanContact.Contact__r.Email;
                        }
                        else{
                            mobNo = objLoanContact.Mobile__c;
                            emailId = objLoanContact.Email__c;
                        }
                       // system.enqueueJob(new GSTAuthINDetailsCallout(objLoanContact.GSTIN_Number__c, objLoanContact.Id, objLoanContact.Loan_Applications__c,objLoanContact.Pan_Number__c,mobNo,emailId,objLoanContact.Constitution__c ));
                        GSTAuthINDetailsCallout.makeGstinAuthDetailsCallout(objLoanContact.GSTIN_Number__c, objLoanContact.Id, objLoanContact.Loan_Applications__c,objLoanContact.Pan_Number__c,mobNo,emailId,objLoanContact.Constitution__c );
                        system.debug('Counter ==>' + Counter);
                    }
                }
            }
        }
    }
    
    @AuraEnabled
    public static List<String> getErrorMessage(String paramJSONList) { //  map<String,String>
        set<id> lstLoanContactIds = new set<id>();
        List<Loan_Contact__c> lstLoanContact = new List<Loan_Contact__c>();
        List<cls_LoanContactListWrapper> lstSerializedWrapper = (List<cls_LoanContactListWrapper>)JSON.deserialize(paramJSONList,List<cls_LoanContactListWrapper>.class);
        System.debug('Debug Log for deserialized List'+lstSerializedWrapper.size());
        System.debug('Debug Log for deserialized List'+lstSerializedWrapper);
        
        if(!lstSerializedWrapper.isEmpty()) {
            for(cls_LoanContactListWrapper objcls_LoanContactListWrapper : lstSerializedWrapper) {
                //if(objcls_LoanContactListWrapper.isChecked)
                    lstLoanContact.add(objcls_LoanContactListWrapper.objLoanContact);
            }
        }
        system.debug('lstLoanContact Size ' + lstLoanContact.size());
        system.debug('lstLoanContact '+ lstLoanContact);
        if(!lstLoanContact.isEmpty()) {
            Integer Counter = 0;
            for(Loan_Contact__c objLoanContact : lstLoanContact) {
                lstLoanContactIds.add(objLoanContact.id);
            }
            system.debug('lstLoanContactIds' + lstLoanContactIds.size());
        }
        List<String> cdWithErrorMessage = new List<String>();
        //Map<String,String> cdToMessageMap = new Map<String,String>();
        List<Customer_Integration__c> cilist = new List<Customer_Integration__c>();
        List<Loan_Contact__c> cdList = [Select id,Name,(Select id,Loan_Contact__r.Name,GSTIN_Error_Message__c from Customer_Integrations__r where  RecordType.Name = 'GSTIN' ORDER BY  CreatedDate DESC LIMIT 1) from Loan_Contact__c  where id in : lstLoanContactIds];
        if(!cdList.isEmpty()){
            for(Loan_Contact__c cd : cdList){
                if(!cd.Customer_Integrations__r.isEmpty()){
                    cilist.add(cd.Customer_Integrations__r);
                }
                
            }
            system.debug('cilist '+ cilist);
            if(!cilist.isEmpty()){
                for(Customer_Integration__c ci : cilist){
                    //cdToMessageMap.put(ci.Loan_Contact__r.Name,ci.GSTIN_Error_Message__c);
                    if(ci.GSTIN_Error_Message__c != null){
                        String str = ci.Loan_Contact__r.Name + ' - ' + ci.GSTIN_Error_Message__c;
                        System.debug('str '+ str);
                        cdWithErrorMessage.add(str);
                    }
                    
                }
            }
        }
        //system.debug('cdToMessageMap ' + cdToMessageMap);
        system.debug('cdWithErrorMessage ' + cdWithErrorMessage);
        //return cdToMessageMap;
        return cdWithErrorMessage;
        
    }
    
    public class cls_LoanContactListWrapper {
        @AuraEnabled public Boolean isChecked;
        @AuraEnabled public String CustomerSegmentLabel;
        @AuraEnabled public Loan_Contact__c objLoanContact;
        @AuraEnabled public String Constitution;
        public cls_LoanContactListWrapper(Boolean isChecked, Loan_Contact__c objLoanContact, String CustomerSegmentLabel, String Constitution) {
            this.isChecked = isChecked;
            this.objLoanContact = objLoanContact;
            this.Constitution = Constitution;
            this.CustomerSegmentLabel = CustomerSegmentLabel;
        }
        
    }
    public class cls_loanContactDetail{
        public String PAN;
        public String GSTIN;
        public String contactId;
        public String loanAppId;
        public integer mobile;
        public string email;
        public String Constitution;
    }
    
}