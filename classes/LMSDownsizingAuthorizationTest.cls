@IsTest
public class LMSDownsizingAuthorizationTest {
    
    public static testMethod void LMSMethodTest(){
        
        Profile p=[SELECT id,name from profile where name=:'Sales Team'];
        UserRole r = [Select Id From UserRole Where Name =: 'BCM - Delhi 1.1' limit 1];
        User u = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,
            FCU__c =true,Role_in_Sales_Hierarchy__c = 'SM'
        ); 
        insert u;
        
        system.runAs(u){
            Account custObj = new Account();
            custObj.Name = 'Test Customer';
            custObj.Phone = '9999999999';
            database.insert(custObj);
            
            scheme__c schemeObj = new scheme__c();
            schemeObj.Name = 'Scheme Name';
            schemeObj.Scheme_Group_ID__c = '12345';
            schemeObj.Scheme_Code__c = '12345';
            database.insert(schemeObj);
            
            
            Loan_Application__c loanAppObj = new Loan_Application__c();
            loanAppObj.customer__c = custObj.id;
            loanAppObj.Loan_Application_Number__c = 'LA00001';
            loanAppObj.Loan_Purpose__c = '11';
            loanAppObj.scheme__c = schemeObj.id;
            loanAppObj.Transaction_type__c = 'PB';
            loanAppObj.Requested_Amount__c = 50;
            loanAppObj.Business_Date_Modified__c = date.newInstance(2019, 12, 15);
            Database.insert(loanAppObj);
            
            List<Loan_Downsizing__c> lstofLoanSize = new List<Loan_Downsizing__c>();
            Loan_Downsizing__c loanDownSize = new Loan_Downsizing__c(Loan_Application__c=loanAppObj.id,Downsize_Type__c='Full Downsize',Status__c='Approved',Remarks__c='Test Approved',Downsizing_Amount__c=207);
            lstofLoanSize.add(loanDownSize);
            Database.insert(lstofLoanSize);
            
            
            test.startTest();
            String authorizationRemarks = 'tesing remarks';
            LMSDownsizingAuthorization.loadLoanDownsizingInfo(loanAppObj.Id);
            LMSDownsizingAuthorization.loadLoanDownsizingInfo(null);
            LMSDownsizingAuthorization.generateHttpRquest(Null, authorizationRemarks);
            LMSDownsizingAuthorization.sendForLmsDownsizingAuthorization(loanAppObj.Id,authorizationRemarks);
            LMSDownsizingAuthorization.sendForLmsDownsizingAuthorization(loanAppObj.Id,null);
            test.stopTest();
        }
        
    }
    
    static testMethod void createRequestBodyTest(){
        
        Profile p=[SELECT id,name from profile where name=:'Sales Team'];
        UserRole r = [Select Id From UserRole Where Name =: 'BCM - Delhi 1.1' limit 1];
        User u = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,
            FCU__c =true,Role_in_Sales_Hierarchy__c = 'SM'
        ); 
        insert u;
        
        system.runAs(u){
            test.startTest();
            Account custObj = new Account();
            custObj.Name = 'Test Customer';
            custObj.Phone = '9999999999';
            database.insert(custObj);
            
            scheme__c schemeObj = new scheme__c();
            schemeObj.Name = 'Scheme Name';
            schemeObj.Scheme_Group_ID__c = '12345';
            schemeObj.Scheme_Code__c = '12345';
            database.insert(schemeObj);
            
            LMS_Date_Sync__c dt = new LMS_Date_Sync__c(Current_Date__c=system.today());
            insert dt;
            Decimal pendingAmount = 500;
            Decimal newSanctionedAmount = 100.2;
            Loan_Application__c loanAppObj = new Loan_Application__c();
            loanAppObj.customer__c = custObj.id;
            loanAppObj.Loan_Application_Number__c = 'LA00001';
            loanAppObj.Loan_Purpose__c = '11';
            loanAppObj.scheme__c = schemeObj.id;
            loanAppObj.Transaction_type__c = 'PB';
            loanAppObj.Requested_Amount__c = 50; 
            loanAppObj.Business_Date_Modified__c = date.newInstance(2019, 12, 15);
            loanAppObj.Approved_Loan_Amount__c =pendingAmount;
            loanAppObj.Pending_Amount_for_Disbursement__c=56000;
            loanAppObj.Date_of_Downsizing__c = dt.Current_Date__c;
            loanAppObj.Approved_cross_sell_amount__c = 788;
            loanAppObj.IMGC_Premium_Fees_GST_Inclusive1__c = 319;
            loanAppObj.StageName__c = 'Credit Decisioning';
            loanAppObj.Sub_Stage__c = 'Docket Checker';
            loanAppObj.Approved_Loan_Amount_Exclusive__c=pendingAmount;
            loanAppObj.Insurance_Loan_Created__c=false;
            Database.insert(loanAppObj);
            system.debug('loanAppObj++'+loanAppObj);
			Id RecordTypeIdTechnical = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('Technical').getRecordTypeId();            
            Third_Party_Verification__c objThrd = new Third_Party_Verification__c();
        	objThrd.Status__c = 'Completed';
            objThrd.Report_Status__c = 'Positive';
        	objThrd.Loan_Application__c = loanAppObj.Id;
        	objThrd.Final_Property_Valuation__c = 4343332;
        	objThrd.RecordTypeId = RecordTypeIdTechnical;
        	insert objThrd;
        
        	Third_Party_Verification__c objThrd1 = new Third_Party_Verification__c();
        	objThrd1.Status__c = 'Completed';
            objThrd1.Report_Status__c = 'Positive';
        	objThrd1.Loan_Application__c = loanAppObj.Id;
        	objThrd1.Final_Property_Valuation__c = 4343332;
        	objThrd1.RecordTypeId = RecordTypeIdTechnical;
        	insert objThrd1;
            
            system.debug('loanAppObj.Final_Property_Valuation__c +++' + loanAppObj.Final_Property_Valuation__c);
            String strauthorizationRemarks = 'Remarks test';
            List<Loan_Downsizing__c> lstofLoanSize = new List<Loan_Downsizing__c>();
            Loan_Downsizing__c loanDownSize = new Loan_Downsizing__c(Loan_Application__c=loanAppObj.id,Downsize_Type__c='Partial Downsize',Remarks__c=strauthorizationRemarks,Downsizing_Amount__c=207,Case_is_not_delinquent__c=true,NACH_is_registered__c=true);
            lstofLoanSize.add(loanDownSize);
            Database.insert(lstofLoanSize);
            Loan_Downsizing__c loanDownSizeNew = [Select Downsizing_Amount__c,Downsize_Type__c,Remarks__c, Loan_Application__r.Loan_Application_Number__c,Loan_Application__r.Loan_Number__c, 
                                                  Loan_Application__r.Business_Date_Modified__c, Loan_Application__r.Approved_Loan_Amount__c,Loan_Application__r.Date_of_Downsizing__c, 
                                                  Loan_Application__r.Pending_Amount_for_Disbursement__c,Creation_Remark__c,createdby.name,Case_is_not_delinquent__c,
                                                  NACH_is_registered__c, Loan_Application__r.LTV_amount__c, Authorization_remark__c, Loan_Application__r.Final_Property_Valuation__c, 
                                                  Loan_Application__r.Approved_cross_sell_amount__c, Loan_Application__r.StageName__c, Loan_Application__r.Insurance_Loan_Created__c, 
                                                  Loan_Application__r.Sub_Stage__c From Loan_Downsizing__c where Id =: lstofLoanSize[0].Id];
            loanAppObj = [Select Id,customer__c, Loan_Application_Number__c, Loan_Purpose__c,scheme__c, Transaction_type__c, Requested_Amount__c, Business_Date_Modified__c, Approved_Loan_Amount__c, Pending_Amount_for_Disbursement__c, Date_of_Downsizing__c,LTV_amount__c from Loan_Application__c where Id =: loanAppObj.Id];
            system.debug('loanAppObj++' + loanAppObj);
            LMSDownsizingAuthorization.SuccessResponseWrapper mockResponse = new LMSDownsizingAuthorization.SuccessResponseWrapper();
            
            Time myTime = Time.newInstance(0, 0, 0, 0);
            String dtv = DateTime.newInstance(loanDownSizeNew.Loan_Application__r.Business_Date_Modified__c,myTime).format('dd-MMM-yyyy');
            
            Decimal ltv = 0.00;
            LMSDownsizingAuthorization.RequestWrapper reqWrap = new LMSDownsizingAuthorization.RequestWrapper();
            reqWrap.requestid = System.now().format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
            reqWrap.source = Label.Source_Name;
            reqWrap.agreementId = loanDownSizeNew.Loan_Application__c;
            reqWrap.downSizingAmount = loanDownSizeNew.Downsizing_Amount__c; 
            reqWrap.downSizingDate = dtv;
            reqWrap.makerRemark = loanDownSizeNew.Creation_Remark__c;
            reqWrap.maker = loanDownSizeNew.createdby.name; 
            reqWrap.author = UserInfo.getName();
            reqWrap.grossLtv = ltv.setScale(4)*100;
            reqWrap.netLtv = ltv.setScale(4)*100;
            string serializedOutput = JSON.serializePretty(reqWrap);
            system.debug('serializedOutput > ' +serializedOutput);
            
            TestMockRequest req1=new TestMockRequest(200,'OK',JSON.serialize(mockResponse),null);
            Test.setMock(HttpCalloutMock.class, req1);
            LMSDownsizingAuthorization.createRequestBody(loanDownSizeNew, strauthorizationRemarks);
            LMSDownsizingAuthorization.generateHttpRquest(loanDownSizeNew, strauthorizationRemarks);
            LMSDownsizingAuthorization.updateLoandownsizingAndLoanApplication(loanDownSizeNew,pendingAmount,newSanctionedAmount,strauthorizationRemarks);
            test.stopTest();
        }
    }
    
    static testMethod void createRequestBodyTest2(){
        
        Profile p=[SELECT id,name from profile where name=:'Sales Team'];
        UserRole r = [Select Id From UserRole Where Name =: 'BCM - Delhi 1.1' limit 1];
        User u = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,
            FCU__c =true,Role_in_Sales_Hierarchy__c = 'SM'
        ); 
        insert u;
        
        system.runAs(u){
            test.startTest();
            Account custObj = new Account();
            custObj.Name = 'Test Customer';
            custObj.Phone = '9999999999';
            database.insert(custObj);
            
            scheme__c schemeObj = new scheme__c();
            schemeObj.Name = 'Scheme Name';
            schemeObj.Scheme_Group_ID__c = '12345';
            schemeObj.Scheme_Code__c = '12345';
            database.insert(schemeObj);
            
            LMS_Date_Sync__c dt = new LMS_Date_Sync__c(Current_Date__c=system.today());
            insert dt;
            Decimal pendingAmount = 500;
            Decimal newSanctionedAmount = 100.2;
            Loan_Application__c loanAppObj = new Loan_Application__c();
            loanAppObj.customer__c = custObj.id;
            loanAppObj.Loan_Application_Number__c = 'LA00001';
            loanAppObj.Loan_Purpose__c = '11';
            loanAppObj.scheme__c = schemeObj.id;
            loanAppObj.Transaction_type__c = 'PB';
            loanAppObj.Requested_Amount__c = 50; 
            loanAppObj.Business_Date_Modified__c = date.newInstance(2019, 12, 15);
            loanAppObj.Approved_Loan_Amount__c = pendingAmount;
            loanAppObj.Pending_Amount_for_Disbursement__c=56000;
            loanAppObj.Approved_cross_sell_amount__c = 788;
            loanAppObj.IMGC_Premium_Fees_GST_Inclusive1__c = 319;
            loanAppObj.Date_of_Downsizing__c = dt.Current_Date__c;
            loanAppObj.Loan_Number__c = '12345';
            Database.insert(loanAppObj);
            
            String strauthorizationRemarks = 'Remarks test';
            List<Loan_Downsizing__c> lstofLoanSize = new List<Loan_Downsizing__c>();
            Loan_Downsizing__c loanDownSize = new Loan_Downsizing__c(Loan_Application__c=loanAppObj.id,Downsize_Type__c='Full Downsize',Remarks__c=strauthorizationRemarks,Downsizing_Amount__c=207,Case_is_not_delinquent__c=true,NACH_is_registered__c=true);
            lstofLoanSize.add(loanDownSize);
            Database.insert(lstofLoanSize);
            //Loan_Downsizing__c loanDownSizeNew = [Select Id,Loan_Application__r.customer__c,Loan_Application__r.LTV_amount__c, Downsize_Type__c, Downsizing_Amount__c,Case_is_not_delinquent__c, NACH_is_registered__c, Loan_Application__r.Loan_Number__c,Loan_Application__r.Loan_Application_Number__c, Loan_Application__r.Loan_Purpose__c,Loan_Application__r.scheme__c, Loan_Application__r.Transaction_type__c, Loan_Application__r.Requested_Amount__c, Loan_Application__r.Business_Date_Modified__c, Loan_Application__r.Approved_Loan_Amount__c, Loan_Application__r.Pending_Amount_for_Disbursement__c, Loan_Application__r.Date_of_Downsizing__c from Loan_Downsizing__c where Id =: lstofLoanSize[0].Id];            
            Loan_Downsizing__c loanDownSizeNew = [Select Downsizing_Amount__c,Downsize_Type__c,Remarks__c, Loan_Application__r.Loan_Application_Number__c,Loan_Application__r.Loan_Number__c, 
                                                  Loan_Application__r.Business_Date_Modified__c, Loan_Application__r.Approved_Loan_Amount__c,Loan_Application__r.Date_of_Downsizing__c, 
                                                  Loan_Application__r.Pending_Amount_for_Disbursement__c,Creation_Remark__c,createdby.name,Case_is_not_delinquent__c,
                                                  NACH_is_registered__c, Loan_Application__r.LTV_amount__c, Authorization_remark__c, Loan_Application__r.Final_Property_Valuation__c, 
                                                  Loan_Application__r.Approved_cross_sell_amount__c, Loan_Application__r.StageName__c, Loan_Application__r.Insurance_Loan_Created__c, 
                                                  Loan_Application__r.Sub_Stage__c From Loan_Downsizing__c where Id =: lstofLoanSize[0].Id];
            loanAppObj = [Select Id,customer__c, Loan_Application_Number__c, Loan_Purpose__c,scheme__c, Transaction_type__c, Requested_Amount__c, Business_Date_Modified__c, Approved_Loan_Amount__c, Pending_Amount_for_Disbursement__c, Date_of_Downsizing__c from Loan_Application__c where Id =: loanAppObj.Id];
            
            LMSDownsizingAuthorization.SuccessResponseWrapper mockResponse = new LMSDownsizingAuthorization.SuccessResponseWrapper();
            
            LMSDownsizingAuthorization.ParsedResponseWrapper respWrapper = new LMSDownsizingAuthorization.ParsedResponseWrapper(pendingAmount,newSanctionedAmount);
            TestMockRequest req1=new TestMockRequest(200,'OK',JSON.serialize(mockResponse),null);
            Test.setMock(HttpCalloutMock.class, req1);
            LMSDownsizingAuthorization.createRequestBody(loanDownSizeNew, strauthorizationRemarks);
            LMSDownsizingAuthorization.generateHttpRquest(loanDownSizeNew, strauthorizationRemarks);
            LMSDownsizingAuthorization.updateLoandownsizingAndLoanApplication(loanDownSizeNew,pendingAmount,newSanctionedAmount,strauthorizationRemarks);
            test.stopTest();
        }
    }
}