public class HLOnboardingController {
  
  /*@AuraEnabled
    public static Boolean checkDisbursementEligibility(Id loanAppId) {
      Boolean b = [SELECT Id, Disbursement_Details_verified__c FROM Loan_Application__c WHERE Id =: loanAppId LIMIT 1].Disbursement_Details_verified__c;
        System.debug('>>>>>>'+b);
      if(b == true) {
            System.debug('>>>Returning true');
        return true;
      }
      else {
            System.debug('>>>Returning false');
        return false;
      }
	}*/
	//Modified By Saumya For Insurance Loan
    @AuraEnabled
    public static String checkDisbursementEligibility(Id loanAppId) {
      Loan_Application__c b = [SELECT Id, Disbursement_Details_verified__c, Insurance_Loan_Application__c, Original_Loan_Application__r.Loan_Application_Number__c,Post_Sanction_FCU_Decline_Count__c,Overall_Post_Sanction_FCU_Status__c,Post_Sanction_CNV_Count__c,Approve_Post_Sanction_CNV_FCU__c,Post_Sanction_RTC_Count__c,Approve_Post_Sanction_RTC_FCU__c,Exempted_from_FCU_BRD__c FROM Loan_Application__c WHERE Id =: loanAppId LIMIT 1]; // Added by Saumya Original_Loan_Application__r.Loan_Application_Number__c for Insurance Loan
								//Post_Sanction_FCU_Decline_Count__c,Overall_Post_Sanction_FCU_Status__c,Post_Sanction_CNV_Count__c,Approve_Post_Sanction_CNV_FCU__c,Post_Sanction_RTC_Count__c,Approve_Post_Sanction_RTC_FCU__c added by Abhilekh on 23rd October 2019 for Post Sanction FCU BRD
								//Exempted_from_FCU_BRD__c added by Abhilekh on 23rd December 2019 for FCU BRD Exemption
        System.debug('>>>>>>'+b);
      if(b.Disbursement_Details_verified__c == true) {
            System.debug('>>>Returning true');
			//Below if and 2 else if blocks added by Abhilekh on 23rd October 2019 for Post Sanction FCU BRD,&& b.Exempted_from_FCU_BRD__c == false added by Abhilekh on 23rd December 2019 for FCU BRD Exemption
            if(b.Overall_Post_Sanction_FCU_Status__c == Constants.NEGATIVE && b.Post_Sanction_FCU_Decline_Count__c > 0 && b.Exempted_from_FCU_BRD__c == false){
                return 'FD plus FD';
            }
            else if((b.Post_Sanction_CNV_Count__c > 0 || b.Overall_Post_Sanction_FCU_Status__c == Constants.CNV) && b.Approve_Post_Sanction_CNV_FCU__c == false && b.Exempted_from_FCU_BRD__c == false){ //&& b.Exempted_from_FCU_BRD__c == false added by Abhilekh on 23rd December 2019 for FCU BRD Exemption
                return 'CNV';
            }
            else if((b.Post_Sanction_RTC_Count__c > 0 || b.Overall_Post_Sanction_FCU_Status__c == Constants.RTC) && b.Approve_Post_Sanction_RTC_FCU__c == false && b.Exempted_from_FCU_BRD__c == false){ //&& b.Exempted_from_FCU_BRD__c == false added by Abhilekh on 23rd December 2019 for FCU BRD Exemption
                return 'RTC';
            }
      if(b.Insurance_Loan_Application__c == true){
                if(b.Original_Loan_Application__r.Loan_Application_Number__c != null){ // Added by Saumya for Insurance Loan
        return 'true';
                }
                else{
        return 'Insurance Loan';
                }
      
      }
      else
        return 'true';
      }
      else {
            System.debug('>>>Returning false');
        return 'false';
      }
    }
  
  @AuraEnabled
    public static Boolean checkRepaymentEligibility(Id loanAppId) {
      String businessDateLMS;
      List<LMS_Date_Sync__c> lstLMSDateSync;
      List<Loan_Repayment__c> lstLoanRepayment;
      Map<String,String> mapCreationDatetoBusinessDate = new Map<String,String>();
      
      if(String.isNotBlank(loanAppId)) {
        lstLMSDateSync = [SELECT ID, Name, Current_Date__c,CreatedDate FROM LMS_Date_Sync__c ORDER BY CreatedDate ASC];
          if(!lstLMSDateSync.isEmpty()) {
            for(LMS_Date_Sync__c objLMSDateSync : lstLMSDateSync) {
              mapCreationDatetoBusinessDate.put(String.valueOf(objLMSDateSync.CreatedDate.format('dd/MM/yyyy')),String.valueOf(Datetime.newInstance(objLMSDateSync.Current_Date__c,Time.newInstance(23,59,59,59)).format('dd/MM/yyyy')));
            }
            for(String d : mapCreationDatetoBusinessDate.keySet()) {
              System.debug('Key'+d);
              System.debug('Value'+mapCreationDatetoBusinessDate.get(d));
            }
            businessDateLMS = String.valueOf(Datetime.newInstance(lstLMSDateSync[lstLMSDateSync.size()-1].Current_Date__c,Time.newInstance(23,59,59,59)).format('dd/MM/yyyy'));
            System.debug('Debug Log for business Date'+businessDateLMS);
          }
            lstLoanRepayment = [SELECT 
                          CreatedDate,
                          LastModifiedDate,
                                            dueDay__c, 
                                            Tenure__c, 
                                            Sanction_Amount__c, 
                                            RescheduleFlag__c, 
                                            RequestId__c, 
                                            Repayment_effective_date__c, 
                                            Repayment_Drawn_On__c, 
                                            RepayType__c, 
                                            Rate_Type__c, 
                                            Number_of_Advance_Installment__c, 
                                            Negative_Capitalization_Flag__c, 
                                            Name, 
                                            Moratorium_period__c, 
                                            Moratorium_impact_on_tenure__c, 
                                            Moratorium_charge_interest_adjust_in_sch__c, 
                                            Moratorium_Flag__c, 
                                            Moratorium_Charge_interest__c, 
                                            Loan_Application__c, 
                                            Interest_StartDate__c, 
                                            Interest_Round_off_parameter__c, 
                                            Interest_Round_off_parameter_Flag__c, 
                                            Interest_Rate__c, 
                                            Instalment_Round_off_parameter__c, 
                                            Instalment_Round_off_parameter_Flag__c, 
                                            Installment_Plan__c, 
                                            Installment_Mode__c, 
                                            Id, 
                                            IRR_calculation_parameters__c, 
                                            Frequency__c, 
                                            EMI_Start_date__c, 
                                            EMI_Amount__c, 
                                            Disbursement_Amount__c, 
                                            Disbursal_Date__c, 
                                            Days_Per_Year__c, 
                                            Bulk_refund__c, 
                                            Broken_Period_Interest_Handing__c, 
                                            Baloon_Amount__c, 
                                            Advanced_Installment_Flag__c, 
                                            Additional_Disbursement__c 
                                    FROM 
                                            Loan_Repayment__c 
                                    WHERE
                                            Loan_Application__c =: loanAppId
                                    ORDER BY 
                                        CreatedDate DESC LIMIT 1
                                            ];
                                                
            System.debug('Debug Log for lstLoanRepayment'+lstLoanRepayment);
            if(!lstLoanRepayment.isEmpty()) {
              System.debug('Date of Repayment created'+String.valueOf(datetime.newInstance(lstLoanRepayment[0].Disbursal_Date__c, Time.newInstance(23,59,59,59)).format('dd/MM/yyyy')));
              //System.debug('Value against repayment created'+mapCreationDatetoBusinessDate.get(datetime.newInstance(lstLoanRepayment[0].Disbursal_Date__c, Time.newInstance(23,59,59,59)).format('dd/MM/yyyy')));
              //if(mapCreationDatetoBusinessDate.containsKey(String.valueOf(datetime.newInstance(lstLoanRepayment[0].Disbursal_Date__c, Time.newInstance(23,59,59,59)).format('dd/MM/yyyy'))) && businessDateLMS == mapCreationDatetoBusinessDate.get(datetime.newInstance(lstLoanRepayment[0].Disbursal_Date__c, Time.newInstance(23,59,59,59)).format('dd/MM/yyyy'))) {
                if(businessDateLMS == String.valueOf(datetime.newInstance(lstLoanRepayment[0].Disbursal_Date__c, Time.newInstance(23,59,59,59)).format('dd/MM/yyyy'))) {
                System.debug('Business Date found and matched');
                return true;
              }
              else {
                System.debug('Business Date not matched.');
                return false;
              }
            }
            else {
              return false;
            }
      }  
      return false;
    }
  
  
    @AuraEnabled
    public static Loan_Application__c onboardCallout(Id loanAppId) {
      if(String.isNotBlank(loanAppId)) {
        HomeLoanOnboardingCallout objHomeLoanOnboardingCallout = new HomeLoanOnboardingCallout();
        Loan_Application__c objLA = objHomeLoanOnboardingCallout.HLOnboardingCallout(loanAppId);
        System.debug('Debug Log from Lightning Component Controller'+objLA);
        return objLA;
      }
      return null;
    }
}