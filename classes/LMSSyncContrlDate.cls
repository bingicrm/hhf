public class LMSSyncContrlDate {
    @AuraEnabled
    public static String dateSync(){
    
        DateTime dt = LMSDateUtility.lmsDateValue;
        
        String monthName = dt.format('MMMMM');
        monthName = monthName.substring(0,3);
        Integer sday = dt.day();
        Integer syear = dt.year();
        String disbDt = 'Business Date : '+String.valueOf(sday)+'-'+monthName+'-'+String.valueOf(syear);
    
   
        return disbDt;
    }
}