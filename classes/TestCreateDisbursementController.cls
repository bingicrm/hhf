@IsTest
public class TestCreateDisbursementController {
    @IsTest
    public static void TestGetLoanId(){
        Account acc=new Account();
        acc.name='TestAccount';
        acc.phone='9988998899';
        acc.PAN__c='fjauy1916e';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        System.debug(acc.id);
        
        Scheme__c sc=new Scheme__c();
        sc.name='TestScheme';
        sc.Scheme_Code__c='testcode';
        sc.Scheme_Group_ID__c='23131';
        sc.Product_Code__c='HL';
        insert sc;
        System.debug(sc.id);
        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Transaction_type__c='PB';
        lap.Approved_Loan_Amount__c = 250000;
        lap.Scheme__c=sc.id;
        insert lap;
        System.debug(lap.id);
        Tranche__c tr=new Tranche__c();
        tr.Disbursal_Amount__c = 180000;
        tr.Loan_Application__c=lap.id;
        insert tr;
        System.debug(tr.id);
        CreateDisbursementController.getLoanId(tr.id);
    }
}