public class ContentDocumentTriggerHandler {
    //code snippet to create record for Document_Deletion_Log on deletion of any Attachment of Loan Application//
    /***********************************Added by Chitransh Porwal on 8 July 2019**********************************/
    public static void onBeforeDelete(Map<id,ContentDocument> triggerOldMap){
        system.debug('triggerOldMap '+triggerOldMap);
        if(!triggerOldMap.isEmpty()) {
            Map<Id,Document_Migration_Log__c> mapDMLIDtoDML = new Map<Id,Document_Migration_Log__c>([Select Id, Name, Document_Id__c, Successfully_Migrated__c, Loan_Application__c From Document_Migration_Log__c WHERE Document_Id__c IN: triggerOldMap.keySet()]);
            Map<Id, Id> mapAttachmentIdtoDMLId = new Map<Id, Id>();
            if(!mapDMLIDtoDML.isEmpty()) {
                for(Id DMLID : mapDMLIDtoDML.keySet()) {
                    mapAttachmentIdtoDMLId.put(mapDMLIDtoDML.get(DMLID).Document_Id__c,DMLID);
                }
            }
            
            Boolean flag = false;
            String objectApiName = '';
            String SourceLoanApp = '';
            String UserPrefix = '005';
            List<Id> contentDocId = new List<Id>();
            List<id> DocCheckListIds = new List<id>();
            Map<Id, Id> contDocLinkedMap = new Map<Id, Id>();
            Map<id,id> checklistToLoanAppMap = new Map<id,id>();
            List<Document_Deletion_Log__c> listDeletedAttachmentsofLoanApp = new List<Document_Deletion_Log__c>();
            
            
            for(ContentDocument con : triggerOldMap.values()){
                contentDocId.add(con.Id);
            }
            
            if(!contentDocId.isEmpty()){
                for(ContentDocumentLink cdl : [SELECT Id,ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE ContentDocumentId IN : contentDocId]){
                    String EntityId = cdl.LinkedEntityId;
                    if(!EntityId.contains(UserPrefix)){
                        Id parentId = cdl.LinkedEntityId;
                        String objectName = parentId.getSObjectType().getDescribe().getName();
                        if(objectName.equals('Document_Checklist__c')){
                            DocCheckListIds.add(cdl.LinkedEntityId);
                            contDocLinkedMap.put(cdl.ContentDocumentId, cdl.LinkedEntityId);
                        }
                        else if(objectName.equals('Loan_Application__c')){
                            contDocLinkedMap.put(cdl.ContentDocumentId, cdl.LinkedEntityId);
                        }
                        
                    }
                }
            }
            system.debug('contDocLinkedMap '+contDocLinkedMap.size());
            
            if(!DocCheckListIds.isEmpty()){
                for(Document_Checklist__c dc: [SELECT Id, Loan_Applications__c FROM Document_Checklist__c where id in : DocCheckListIds ]){
                    checklistToLoanAppMap.put(dc.id, dc.Loan_Applications__c);
                } 
            }
            system.debug('checklistToLoanAppMap '+checklistToLoanAppMap);
            for(ContentDocument objContentDocument : triggerOldMap.values()) {
                
                if(contDocLinkedMap.keySet().contains(objContentDocument.Id)){
                    
                    Id parentId = contDocLinkedMap.get(objContentDocument.Id);
                    String objectName = parentId.getSObjectType().getDescribe().getName(); 
                    system.debug(objectName);
                    if(objectName.equals('Loan_Application__c'))
                    {
                        flag = true;
                        SourceLoanApp = contDocLinkedMap.get(objContentDocument.Id);
                    }
                    if(objectName.equals('Document_Checklist__c')){
                        flag = true;
                        SourceLoanApp = checklistToLoanAppMap.get(contDocLinkedMap.get(objContentDocument.Id));
                    }
                    system.debug('flag'+flag);
                    system.debug('SourceLoanApp '+SourceLoanApp);
                    if(flag == true){
                        system.debug('Hello');
                        Document_Deletion_Log__c deletedAttachmentsofLoanApp = new Document_Deletion_Log__c();
                            deletedAttachmentsofLoanApp.Source_Loan_Application__c = String.isNotBlank(SourceLoanApp) ?  SourceLoanApp : '';
                            deletedAttachmentsofLoanApp.Document_Name__c = String.isNotBlank(objContentDocument.Title) ? objContentDocument.Title : '';
                            if( contDocLinkedMap != null && contDocLinkedMap.containsKey(objContentDocument.Id) && String.isNotBlank(contDocLinkedMap.get(objContentDocument.Id)) ) {
                                objectAPIName = contDocLinkedMap.get(objContentDocument.Id).getSObjectType().getDescribe().getName();
                                if(objectAPIName == 'Document_Checklist__c') {
                                    deletedAttachmentsofLoanApp.Source_Document_Checklist__c = contDocLinkedMap.get(objContentDocument.Id);
                                }
                            }
                            
                            deletedAttachmentsofLoanApp.Document_Size__c = String.valueOf(objContentDocument.ContentSize);
                            deletedAttachmentsofLoanApp.Document_Id__c = objContentDocument.Id;
                            deletedAttachmentsofLoanApp.Deletion_Status__c = 'This file has been deleted.';
                            deletedAttachmentsofLoanApp.Deletion_Initiated_as__c = (!mapAttachmentIdtoDMLId.isEmpty() && mapAttachmentIdtoDMLId.containsKey(objContentDocument.Id) && String.isNotBlank(mapAttachmentIdtoDMLId.get(objContentDocument.Id))) ? 'S3 Document Migration' : 'End User Operation';
                            System.debug('Debug Log for deletedAttachmentsofLoanApp'+deletedAttachmentsofLoanApp);
                        listDeletedAttachmentsofLoanApp.add(deletedAttachmentsofLoanApp);
                    }
                    System.debug('Debug Log for listDeletedAttachmentsofLoanApp'+listDeletedAttachmentsofLoanApp.size());
                    System.debug('Debug Log for listDeletedAttachmentsofLoanApp'+listDeletedAttachmentsofLoanApp.size());
                    if(!listDeletedAttachmentsofLoanApp.isEmpty()){
                        try{
                            Database.SaveResult[] lstDSR = Database.insert(listDeletedAttachmentsofLoanApp,false);
                            for(Database.SaveResult objDSR : lstDSR) {
                                if(objDSR.getErrors().isEmpty()) {
                                    System.debug('Record Created Successfully with ID'+objDSR.getId());
                                }
                                else {
                                    System.debug('The following error occured'+objDSR.getErrors());
                                }
                            }
                            
                        }
                        catch(DmlException e) {
                            System.debug('The following exception has occurred: ' + e.getMessage());
                        }
                    }
                    
                }
            }
        }
    }
    /****************************End of Patch added by Chitransh on 8 July 2019****************************************************/
}