public class CustomerIntegrationTriggerHandler {
	

	 public static void AfterInsert(list < customer_integration__c > newList, Map < Id, customer_integration__c > newMap) {
	 		  list<customer_integration__c> cusList = [select id, recordTypeId from customer_integration__c where id in:newList];
        

        list<id> recordIdList = new list<id>();


        for(customer_integration__c tp: cusList){

            recordIdList.add(tp.recordTypeId);



        }

        if(recordIdList.size() >0){
            system.debug('===');

            list<customer_integration__c> previousList = new list<customer_integration__c>();
            previousList = [ select id, Latest_Record__c, recordTypeId from customer_integration__c where
            						recordTypeId in :recordIdList and id not in: newList ];

            		for(customer_integration__c prev: previousList){

            			prev.Latest_Record__c = false;
            		}
            		update previousList;
        }

	 }
}