/*=====================================================================
 * Deloitte India
 * Name: TestclsCheckExistingCustomer
 * Description: This class is a test class for clsCheckExistingCustomer.
 * Created Date: [01/09/2019]
 * Created By: Gaurav Nawal (Deloitte India)
 *
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/
@isTest
public class TestclsCheckExistingCustomer {
    @testSetup
    public static void init() {
        Scheme__c sc = new Scheme__c(Name = 'Construction Finance', Scheme_Code__c = 'CF', Scheme_Group_ID__c = '5',
                                    Scheme_ID__c = 10,  Product_Code__c='CF', Max_ROI__c = 10, Min_ROI__c = 5,
                                    Min_Loan_Amount__c = 10000, Min_FOIR__c = 1, Max_Loan_Amount__c = 100000,
                                    Max_Tenure__c = 5, Min_Tenure__c = 1, Max_LTV__c = 5, Min_LTV__c = 1, Max_FOIR__c = 5);
        insert sc;
        Account corpCust = new Account(PAN__c='AXEGA3767M', Name='AccountTest', Phone='9992345933', 
                                        Date_of_Incorporation__c = Date.newInstance(2018,11,11), LMS_Existing_Customer__c = false);            
        insert corpCust;
        List<Loan_Application__c> lstLoanApps = new List<Loan_Application__c> ();
        Loan_Application__c la = new Loan_Application__c(Customer__c = corpCust.Id, Scheme__c = sc.Id, 
                                                        StageName__c = 'Customer Onboarding', 
                                                        Transaction_type__c = 'TOP', Requested_Amount__c = 10000,
                                                        Applicant_Customer_Segment__c = '1', Branch__c = 'test', 
                                                        Line_Of_Business__c = 'Open Market', Loan_Purpose__c = '20',
                                                        Requested_EMI_amount__c = 1000, Approved_ROI__c = 5, 
                                                        Requested_Loan_Tenure__c = 5, Government_Programs__c = 'PMAY',
                                                        Loan_Engine_2_Output__c = 'STP', Property_Identified__c = true, 
                                                        Existing_Loan_Application_Number__c = '12334', Outstanding_Loan_amount__c = 10000,
                                                        Name_of_BT_Bank__c = 'TST');

        lstLoanApps.add(la);
        insert lstLoanApps;
    }

    public static testMethod void testChkExistingCustomMethod1() {
        Loan_Contact__c lcRec = [SELECT Id, Customer__c, Customer__r.LMS_Existing_Customer__c from Loan_Contact__c LIMIT 1][0];
        Test.startTest();
        clsCheckExistingCustomer.existingCustomerCheck(lcRec);
        Account corpCust = [SELECT Id FROM Account][0];
        Scheme__c sc = [SELECT Id FROM Scheme__c][0];
        Loan_Application__c la2 = new Loan_Application__c(Customer__c = corpCust.Id, Scheme__c = sc.Id, 
                                                        StageName__c = 'Customer Onboarding', 
                                                        Transaction_type__c = 'TOP', Requested_Amount__c = 10000,
                                                        Applicant_Customer_Segment__c = '1', Branch__c = 'test', 
                                                        Line_Of_Business__c = 'Open Market', Loan_Purpose__c = '20',
                                                        Requested_EMI_amount__c = 1000, Approved_ROI__c = 5, 
                                                        Requested_Loan_Tenure__c = 5, Government_Programs__c = 'PMAY',
                                                        Loan_Engine_2_Output__c = 'STP', Property_Identified__c = true, 
                                                        Existing_Loan_Application_Number__c = '12335', Outstanding_Loan_amount__c = 10000,
                                                        Name_of_BT_Bank__c = 'TST', Loan_Number__c = '7876896978');

        insert la2;
        clsCheckExistingCustomer.existingCustomerCheck(lcRec);
        Test.stopTest();

    }
}