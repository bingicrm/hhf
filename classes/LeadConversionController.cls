public class LeadConversionController {
    public static Lead objLead;
    public static Account objAcc;
    
    @AuraEnabled
    public static String convertLead(Id idRecordId) {
        
        List<Lead> lstLead = [Select Id, Gender__c, Call_Status__c,Source_Detail__c ,DST_Name__r.Name,Connector_Master_Name__c, Status, Hero_Dealer__c,RecordType.Name,Dealer_Executive_1__c,Dealer_Executive_2__c, Requested_Loan_Amount__c,Product_Scheme__c, Lead_Id__c, Co_Applicant_FirstName__c, Co_Applicant_LastName__c, Transaction_Type__c, Product__c, Rating, Salutation, FirstName, LastName, 
                               MobilePhone, Email, Date_of_Birth__c, Property_Details__c,Lead_Converted_Custom__c, 
                               Connector_Name__c ,Cross_Sell_Partner_Name__c, DSA_Name__c,DST_Name__c,Hero_Sub_Dealer_Extension_Counter__c,
                               DST_SM_Name__c, DST_SM_Name__r.Name,Strategic_Partner_Branch__c,Strategic_Partner_Name__c,
                               Group_Company__c,Group_Company_Office_Location__c 
                              from Lead where id=: idRecordId ];//Hero_Sub_Dealer_Extension_Counter__c added in Query by Abhishek for Sourcing BRD
      
        
        Id profileId=userinfo.getProfileId();
        String strProfileName=[Select Id,Name from Profile where Id=:profileId].Name;                    
        if (!lstLead.isEmpty()) {
            objLead = lstLead[0];
        }
        Savepoint savepnt;
        try{  
            if (objLead.Status == 'New') {
                return 'Unsuccessfull$'+'You cannot convert a lead which is currently in New status!';
            } else if (objLead.Status == 'Rejected') {
                return 'Unsuccessfull$'+'You cannot convert a lead which is currently in Rejected status!';
            } 
            
            if ( objLead.RecordType.Name.contains('Digital') && (strProfileName == 'Marketing Team' ||  strProfileName == 'Sales Team') ) {
                return 'Unsuccessfull$'+'You do not have the access to convert a lead!';
            }
            //Added by Abhishek for Sourcing BRD - START
            if ( objLead.RecordType.Name.EqualsIgnoreCase('Hero Dealer') && String.isBlank(objLead.Hero_Sub_Dealer_Extension_Counter__c)) {
                return 'Unsuccessfull$'+'Please enter data in Hero Sub-Dealer/Extension Counter before proceeding.';
            }
            //Added by Abhishek for TIL-00002442 - START
            if (String.isBlank(objLead.DST_Name__c)) {
                return 'Unsuccessfull$'+'Please enter data in Sales User Name before proceeding.';
            }
            //Added by Abhishek for TIL-00002442 - END
            //Added by Abhishek for Sourcing BRD - END
            if (objLead.RecordType.Name == 'Open Market' || objLead.RecordType.Name == 'Stategic Partner' || objLead.RecordType.Name == 'Hero Dealer' || objLead.RecordType.Name == 'Connector' || objLead.RecordType.Name == 'Hero Dealer DSA' || objLead.RecordType.Name == 'Hero Employees' ) {
                if (objLead.Call_Status__c != 'Interested') {
                    return 'Unsuccessfull$'+'You can only convert a lead which is marked as Interested!';
                }
            
            }
            savepnt = Database.setSavepoint();
  
            system.debug('@@id' + idrecordid);
            Account objAcc = createAccount(true);
            system.debug('@@@Account' + objAcc);
            insert objAcc;
            
            
            Loan_Application__c objLoan = new Loan_Application__c();
            objLoan.Customer__c = objAcc.Id;
            objLoan.Scheme__c = objLead.Product_Scheme__c;
            objLoan.Requested_Amount__c = objLead.Requested_Loan_Amount__c;
            objLoan.Property_Identified__c = objLead.Property_Details__c != null ? true : false;
            objLoan.Lead_Id__c = objLead.Lead_Id__c;
           // objLoan.transaction_type__c= objLead.transaction_type__c;
            insert objLoan;
            
            if (objLead.Recordtype.Name != 'Open Market') {
                List<User> lstuser = [Select Id, Name from user where Name = :objLead.DST_SM_Name__r.Name];
                List<User> lstuserDST = [Select Id, Name from user where Name = :objLead.DST_Name__r.Name];
                Sourcing_Detail__c objSource = new Sourcing_Detail__c();
                objsource.Connector_Name__c= objLead.Connector_Master_Name__c != null ? objLead.Connector_Master_Name__c : null;
                objsource.Cross_Sell_Partner_Name__c = objLead.Cross_Sell_Partner_Name__c != null ? objLead.Cross_Sell_Partner_Name__c : null;
                //objsource.Dealer_Executive__c = objLead.Dealer_Executive_1__c != null ? objLead.Dealer_Executive_1__c : null;
                objsource.Dealer_Executive_1__c = objLead.Dealer_Executive_1__c != null ? objLead.Dealer_Executive_1__c : null;
                objsource.Dealer_Executive_2__c = objLead.Dealer_Executive_2__c != null ? objLead.Dealer_Executive_2__c : null;
                objsource.DSA_Name__c = objLead.DSA_Name__c != null ? objLead.DSA_Name__c : null;
                objsource.DST_Name__c = objLead.DST_Name__c != null ? objLead.DST_Name__c : null;
                objSource.DST__c = lstuserDST!= null && lstuserDST.size()>0 && lstuserDST[0].Id!= null ? lstuserDST[0].Id: null;
                objsource.DST_SM_Master__c = objLead.DST_SM_Name__c != null ? objLead.DST_SM_Name__c : null;
                objsource.Sales_User_Name__c =  !lstuser.isEmpty() ? lstuser[0].Id : null;
                objsource.Hero_Dealer__c = objLead.Hero_Dealer__c != null ? objLead.Hero_Dealer__c : null;
                objsource.Hero_Sub_Dealer_Extension_Counter__c = objLead.Hero_Sub_Dealer_Extension_Counter__c != null ? objLead.Hero_Sub_Dealer_Extension_Counter__c : null;//Added by Abhishek for Sourcing BRD
                objsource.Strategic_Partner_Branch__c = objLead.Strategic_Partner_Branch__c != null ? objLead.Strategic_Partner_Branch__c : null;
                objsource.Strategic_Partner_Name__c = objLead.Strategic_Partner_Name__c != null ? objLead.Strategic_Partner_Name__c : null;
                objSource.Group_Company__c = objLead.Group_Company__c != null ? objLead.Group_Company__c : null;
                objSource.Group_Company_Office_Location__c = objLead.Group_Company_Office_Location__c != null ? objLead.Group_Company_Office_Location__c : null;
                
                objSource.Loan_Application__c = objLoan.Id;
                objSource.RecordTypeId = getRecordType(objLead.RecordType.Name);
                if (objlead.Source_Detail__c == 'Website') {
                    objSource.Lead_Sourcing_Detail__c = 'HHFL Website';
                    objSource.Lead_Broker_ID__c= '8149';//Added by Saumya for Lead Changes
                } else if (objlead.Source_Detail__c == 'Inbound Call') {
                    objSource.Lead_Sourcing_Detail__c = 'Inbound Call';
                    objSource.Lead_Broker_ID__c= '7887';//Added by Saumya for Lead Changes    
                } else if (objlead.Source_Detail__c == 'PaisaBazaar.com' || objlead.Source_Detail__c == 'BankBazaar.com') {
                    objSource.Lead_Sourcing_Detail__c = 'PaisaBazaar.com';
                    objSource.Lead_Broker_ID__c= '8482';//Added by Saumya for Lead Changes        
                }
                else if (objlead.Source_Detail__c == 'Advertisement') {//Added by Saumya for Lead Changes
                    objSource.Lead_Sourcing_Detail__c = 'Advertisement';
                    objSource.Lead_Broker_ID__c= '7887';//Added by Saumya for Lead Changes
                }
                else if (objlead.Source_Detail__c == 'WishFin') {//Added by Saumya for Lead Changes
                    objSource.Lead_Sourcing_Detail__c = 'WishFin';
                    objSource.Lead_Broker_ID__c= '7887';
                }
                else if (objlead.Source_Detail__c == 'CIBIL') {//Added by Saumya for Lead Changes
                    objSource.Lead_Sourcing_Detail__c = 'CIBIL';
                    objSource.Lead_Broker_ID__c= '7887';
                }
                else if (objlead.Source_Detail__c == 'My Money Karma') {//Added by Saumya for Lead Changes
                    objSource.Lead_Sourcing_Detail__c = 'My Money Karma';
                    objSource.Lead_Broker_ID__c= '9796';//Added by Saumya for Lead Changes
                }
                else if (objlead.Source_Detail__c == 'My Money Mantra') {//Added by Saumya for Lead Changes
                    objSource.Lead_Sourcing_Detail__c = 'My Money Mantra';
                    objSource.Lead_Broker_ID__c= '9795';//Added by Saumya for Lead Changes
                } 
                else if (objlead.Source_Detail__c == 'Lending adda') {//Added by Saumya for Lead Changes 27/11
                    objSource.Lead_Sourcing_Detail__c = 'Lending adda';
                    objSource.Lead_Broker_ID__c= '10219';//Added by Saumya for Lead Changes 27/11
                }
                else if (objlead.Source_Detail__c == 'Referral Program') {//Added by Saumya for Lead Changes 13/01
                    objSource.Lead_Sourcing_Detail__c = 'Referral Program';
                    objSource.Lead_Broker_ID__c= '10578';//Added by Saumya for Lead Changes
                }                             
                insert objSource;
            }
            objLead.Lead_Converted_Custom__c = true;
            objLead.Loan_Application__c = objLoan.id;
            update objLead;
            system.debug('objLead1@@@' + objLead);  
            Database.LeadConvert lc = new database.LeadConvert();
             system.debug('objLead2@@@' + objLead);  
            lc.setLeadId(objLead.id);
            lc.setAccountId(objAcc.Id);
            
            lc.ConvertedStatus = 'Converted';
            lc.setDoNotCreateOpportunity(true);
            Database.LeadConvertResult lcr = Database.convertLead(lc); 
            system.debug('objLead3@@@' + objLead);  
           
            return objAcc.Id;
        }
        catch ( Exception e){ 
            system.debug(' @@ in catch' + e.getMessage());
            system.debug(' @@ in catch stack' + e.getStackTraceString());
            String strError = e.getMessage().substringAfter(',');
            Database.rollback(savepnt);
            return 'Unsuccessfull$'+strError;
        }
        
    }
    
    public static Id getSchemeId(String strProduct) {
        List<Scheme__c> lstScheme;
            
        if (strProduct != null) {
            system.debug('objLead.Product__c' + strProduct);
            Schema.DescribeFieldResult fieldResult = Lead.Product__c.getDescribe();
            List<Schema.PicklistEntry> lstPicklist = fieldResult.getPicklistValues();
            String strPLValue;     
            for( Schema.PicklistEntry f : lstPicklist) {
                if (f.getValue() == strProduct) {
                    strPLValue = f.getLabel();
                }
            }
            lstScheme = [Select Id, Name from Scheme__c where Name =:strPLValue ]; 
        } else {
            lstScheme = [Select Id, Name from Scheme__c where Name ='Home Loan' ];
        }
        
        return lstScheme[0].id; 
    }
    
    public static Account createAccount(Boolean boolApplicant) {
        Account objAcc;
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        objAcc = new Account(RecordTypeId=rtId); 
        objAcc.Salutation = objLead.Salutation;       
        objAcc.FirstName = boolApplicant ? objLead.FirstName : objLead.Co_Applicant_FirstName__c;
        objAcc.LastName = boolApplicant ? objLead.LastName : objLead.Co_Applicant_LastName__c;
         
        objAcc.PersonMobilePhone = objLead.MobilePhone != null ? objLead.MobilePhone : '';
        objAcc.PersonEmail = objLead.Email;
        objAcc.PersonBirthdate = objLead.Date_of_Birth__c;
        objAcc.Date_of_Birth__c = objLead.Date_of_Birth__c;
        objAcc.Rating = objLead.Rating;
        /*if (objLead.Gender__c == 'Male') {
            objAcc.Gender__c = 'M';
        } else if (objLead.Gender__c == 'Female') {
            objAcc.Gender__c = 'F';
        } else if (objLead.Gender__c == 'Other') {
            objAcc.Gender__c = 'T';
        } */
        
        
        
        return objAcc;
              
    }
    
    public static Id getRecordType(String strRecordType) {
        Id rtId;
        if (strRecordType == 'Stategic Partner') {
            strRecordType = 'Strategic Partner';
        } else if (strRecordType == 'Hero Employees') {
            strRecordType = 'Hero Employee';
        } else if (strRecordType == 'Open Market') {
            strRecordType = 'Direct (O)';
        }
        else if(strRecordType == 'Digital Alliances'){
            strRecordType = 'Digital Alliance';
        }
        else if(strRecordType == 'Digital Directs'){//Added by Saumya for Lead Changes 27/12
            strRecordType = 'Digital Direct';
        }
        rtId = Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get(strRecordType).getRecordTypeId();
        
        return rtId;
    }
    
    @AuraEnabled
    public static String rejectLead(Id idRecordId) {
        try{
            List<Lead> lstLead = [Select Id, Status from Lead where Id= :idRecordId LIMIT 1];
            if (!lstLead.isEmpty()) {
                //Below if block and else (block only) added by Abhilekh on 7th August 2019 for TIL-1290
                if(lstLead[0].Status == 'Rejected'){
                    return 'Already Rejected';
                }
                else{
                    lstLead[0].Status = 'Rejected';    
                }
            }
            update lstLead;
            
            
            return 'Successfull';
        }catch(Exception e){
            return 'Unsuccessfull';
        }
    }

}