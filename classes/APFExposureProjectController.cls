public class APFExposureProjectController {
    @AuraEnabled
    public static String checkEligibility(Id projectId) {
        if(projectId != null) {
                APFExposureCallout.makeAPFExposureCalloutProject(projectId);
                return 'APF Exposure request submitted.';
            
        }
        return 'Success';
    }
}