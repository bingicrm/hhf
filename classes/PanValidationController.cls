public class PanValidationController 
{
    @auraenabled
    public static Customer_Integration__c validationPanDetails( Id loanContactId )
    {  
        Id userId = UserInfo.getUserId();
        User u = new User();
        u = [SELECT Id, Profile.Name 
            FROM User 
            WHERE Id =: userId];
        Loan_Contact__c lc = [Select Id, Loan_Applications__r.StageName__c from Loan_Contact__c where Id =: loanContactId];

        system.debug('======>user'+u.Profile.Name);
        if(u.Profile.Name != 'Credit Team' && u.Profile.Name != 'Document Scanner and Checker' && lc.Loan_Applications__r.StageName__c != 'Customer Acceptance'){
            Customer_Integration__c response  = new Customer_Integration__c();
            PanValidationCallOut pan = new PanValidationCallOut();
            response =  pan.validatePan( loanContactId );
            system.debug('in here =====>'+response);
            return response;
        }
        else {
            return null;
        }
    }
    
    @auraenabled
    public static Loan_Contact__c getCustomerDetails( Id loanContactId )
    {  
        Id userId = UserInfo.getUserId();
        User u = new User();
        u = [SELECT Id, Profile.Name 
            FROM User 
            WHERE Id =: userId];

        system.debug('======>user'+u.Profile.Name);
        if(u.Profile.Name != 'Credit Team' && u.Profile.Name != 'Document Scanner and Checker'
          ){
            List<Loan_Contact__c> lstLoan = [Select Id, Customer__r.PAN__c,Customer__r.Salutation,Customer__r.LMS_Existing_Customer__c,/*<!-- Added by Saumya For TIL-00000417 -->*/ 
			Customer__r.FirstName, Customer__r.LastName, Customer__r.MiddleName from Loan_Contact__c where id=: loanContactId];     
            system.debug('@@@lstLoan ' + lstLoan );
             system.debug('@@@lstLoan[0] ' + lstLoan[0].Customer__r.PAN__c );
            if (!lstLoan.isEmpty())
                
                return lstLoan[0];
            else 
                return null;    
        }
        else {
            return null;
        }
    }
    
    @auraenabled
    public static Boolean copyPanDetails( String customerId, Id idLoanContact)
    {  
        Id userId = UserInfo.getUserId();
        User u = new User();
        u = [SELECT Id, Profile.Name 
            FROM User 
            WHERE Id =: userId];

        system.debug('======>customerId'+customerId);
        if(u.Profile.Name != 'Credit Team' && u.Profile.Name != 'Document Scanner and Checker' ){
            //List<Account> lstAcc= [Select Id, Salutation, FirstName, LastName, MiddleName from Account where id=: customerId LIMIT 1];     
            List<Customer_Integration__c > lstCustIn = [Select Id, PAN_Number__c,Loan_Contact__r.Customer__c, PAN_First_Name__c, PAN_Middle_Name__c, Pan_Last_Name__c, Pan_Title__c FROM Customer_Integration__c  where Loan_Contact__c = :idLoanContact and RecordType.Name='PAN Integration' ORDER BY CreatedDate DESC LIMIT 1];
            List<Account> lstAcc= [Select Id, Pan_Validated__c, PAN__c,Salutation, FirstName, LastName, MiddleName from Account where id=: lstCustIn[0].Loan_Contact__r.Customer__c LIMIT 1]; 
            
            //lstAcc[0].Salutation = lstCustIn[0].Pan_Title__c ;
            lstAcc[0].FirstName= lstCustIn[0].PAN_First_Name__c;
            lstAcc[0].MiddleName = lstCustIn[0].PAN_Middle_Name__c;
            lstAcc[0].LastName= lstCustIn[0].Pan_Last_Name__c;
            //lstAcc[0].PAN__c = lstCustIn[0].PAN_Number__c;
            lstAcc[0].Pan_Validated__c = true;
            
            try{
                update lstAcc;
                system.debug('@@lstAcc' + lstAcc);
                return true;
            } catch(Exception e){
                return false;
            }
                
             
        }
        else {
            return null;
        }
    }
    
}