@isTest
public class TestLookup{
    public static testMethod void searchDBTestMethod(){
        List<User> lstBranchManager= new List<User>();  
         Profile p=[SELECT id,name from profile where name=:'Sales Team'];
       /* List<User> BranchManager=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                           Title,Alias,TimeZoneSidKey,
                           EmailEncodingKey,LanguageLocaleKey,
                           LocaleSidKey,UserRoleId
                           from user where profile.name=:p.name AND isActive=true];*/
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
       
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
        lstBranchManager.add(u);
        insert lstBranchManager;                            
        
        
        User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                         Title,Alias,TimeZoneSidKey,
                         EmailEncodingKey,LanguageLocaleKey,
                         LocaleSidKey,UserRoleId 
                         from user where ID=:UserInfo.getUserId()];
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=lstBranchManager[0].Id;
        loggedUser.ManagerID=lstBranchManager[0].Id;
        loggedUser.Location__c='All';// Added by Ashwini
        update loggedUser;
        Test.startTest();
        try
        {
            system.runAs(loggedUser){
                Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='9772345333',
                                         Date_of_Incorporation__c=Date.newInstance(2018,11,11));
                
                Database.insert(Cob);                
                
                Scheme__c sch= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                             Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                             Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                             Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                             Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
                
                Database.insert(sch); 
                
                Branch_Master__c objBranchMaster = new Branch_Master__c(Name='Test Branch',Office_Code__c='TestOC');
                insert objBranchMaster;
                Local_Policies__c LocalPolicy= new Local_Policies__c(Branch_Master__c=objBranchMaster.Id,Scheme__c=sch.Id,Name='XYZ',Branch__c='All',Product_Scheme__c='All Scheme', IsActive__c=true);
                insert LocalPolicy;
                
                Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sch.id,
                                                                  Branch_Lookup__c=objBranchMaster.Id,
                                                                  StageName__c='Customer Onboarding',
                                                                  Sub_Stage__c='Application Initiation',
                                                                  Transaction_type__c='SC',Requested_Amount__c=3000000,
                                                                  Applicant_Customer_segment__c='Salaried',
                                                                  Branch__c='test',Line_Of_Business__c='Open Market',
                                                                  Assigned_Sales_User__c=u.id,
                                                                  Loan_Purpose__c='20',
                                                                  Requested_EMI_amount__c=1000,Approved_ROI__c=5,
                                                                  Requested_Loan_Tenure__c=5,
                                                                  Government_Programs__c='PMAY',
                                                                  Loan_Engine_2_Output__c='STP',
                                                                  Property_Identified__c=True,ownerId=loggedUser.id);
                Database.insert(LAob);
                string LoanAppId = LAob.Id;
                Lookup.searchDB('Account', 'Name', 'Email__c', 'PAN__c', 10, 'Name', 'ABC',LAob.Id,sch.Id);
                Lookup.searchDB('Scheme__c', 'Name', 'Scheme_Code__c', 'Product_Code__c', 10, 'Name', LAob.Id,'ABC',sch.Id);
                Lookup.searchDB('Local_Policies__c', 'Name', 'Product_Scheme__c', 'Branch__c', 10, 'Name','XYZ',LAob.Id,sch.Id);  //Added by Ashwini        
                
                List<Id> accIds = new List<Id>();
                accIds.add(Cob.Id);
                Lookup.getRecordType();
                Lookup.getDupRecord(accIds);
                Lookup.insertDupRecord(Cob);
                
            }
        }
        catch(exception e)
        {
        }     
        Test.stopTest();
    }
    
}