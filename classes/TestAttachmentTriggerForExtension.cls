@istest
public class TestAttachmentTriggerForExtension {
    public static testmethod void method1(){
        Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='9872345333',
                                  Date_of_Incorporation__c=Date.newInstance(2018,11,11));
         
         Database.insert(Cob); 

        Attachment attach = new Attachment();

        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=Cob.id;
        insert attach;

        
    
            delete attach;  
    
            
    }
}