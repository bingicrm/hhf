public class PromoterTriggerHandler {
    public static void onBeforeInsert(List<Promoter__c> lstTriggerNew) {
        /*********************************************Not Required since the object is now discarded*********************************************************
        Set<Id> setPromoterDetailId = new Set<Id>();
        Map<String,Promoter_Detail__c> mapIdtoPromoterDetail;
        if(!lstTriggerNew.isEmpty()) {
            for(Promoter__c objPromoter : lstTriggerNew) {
                if(String.isNotBlank(objPromoter.Promoter_Detail__c)) {
                    setPromoterDetailId.add(objPromoter.Promoter_Detail__c);
                }
            }
            if(!setPromoterDetailId.isEmpty()) {
                System.debug('Debug Log for setPromoterDetailId'+setPromoterDetailId.size());
                mapIdtoPromoterDetail = new Map<String,Promoter_Detail__c>([Select Id, Name, Address__c, Address_Line_1__c, Address_Line_2__c, Constitution__c, Constitution_of_Business__c, 
                                                                               Contact_No_Landline__c, Contact_No_Mobile__c, DOB__c, Date_of_Issue__c, Date_of_Liability__c, Email__c, 
                                                                               Gender__c, GSTIN_Number__c, Name_of_Promoter__c, Office_Email_Address__c, Office_Phone_Number__c, PAN_Number__c,
                                                                               Pincode__c, Preferred_Communication_Address__c, Promoter_Status__c, ITR_Aadhaar_Number__c, Passport_Number__c, Voter_ID_Number__c
                                                                        From
                                                                               Promoter_Detail__c Where Id IN: setPromoterDetailId]);
               if(!mapIdtoPromoterDetail.isEmpty()) {
                  System.debug('Debug Log for mapIdtoPromoterDetail'+mapIdtoPromoterDetail.size()); 
                  for(Promoter__c objPromoter : lstTriggerNew) {
                      if(mapIdtoPromoterDetail.containsKey(objPromoter.Promoter_Detail__c) && mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c) != null) {
                          objPromoter.Address__c = String.isNotBlank(mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Address__c) ? mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Address__c : '';
                          objPromoter.Address_Line_1__c = String.isNotBlank(mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Address_Line_1__c) ? mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Address_Line_1__c : '';
                          objPromoter.Address_Line_2__c = String.isNotBlank(mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Address_Line_2__c) ? mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Address_Line_2__c : '';
                          objPromoter.Constitution__c = String.isNotBlank(mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Constitution__c) ? mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Constitution__c : '';
                          objPromoter.Constitution_of_Business__c = String.isNotBlank(mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Constitution_of_Business__c) ? mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Constitution_of_Business__c : '';
                          objPromoter.Contact_No_Landline__c = String.isNotBlank(mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Contact_No_Landline__c) ? mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Contact_No_Landline__c : '';
                          objPromoter.Contact_No_Mobile__c = String.isNotBlank(mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Contact_No_Mobile__c) ? mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Contact_No_Mobile__c : '';
                          objPromoter.DOB__c = mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).DOB__c != null ? mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).DOB__c : null;
                          objPromoter.Date_of_Issue__c = mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Date_of_Issue__c != null ? mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Date_of_Issue__c : null;
                          objPromoter.Date_of_Liability__c = mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Date_of_Liability__c;
                          objPromoter.Email__c = mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Email__c;
                          objPromoter.Gender__c = mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Gender__c;
                          objPromoter.GSTIN_Number__c =  mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).GSTIN_Number__c;
                          objPromoter.Name_of_Promoter__c = mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Name_of_Promoter__c;
                          objPromoter.Office_Email_Address__c = mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Office_Email_Address__c;
                          objPromoter.Office_Phone_Number__c = mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Office_Phone_Number__c;
                          objPromoter.PAN_Number__c = mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).PAN_Number__c;
                          objPromoter.Pincode__c = mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Pincode__c;
                          objPromoter.Preferred_Communication_Address__c = mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Preferred_Communication_Address__c;
                          objPromoter.Promoter_Status__c = mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Promoter_Status__c;
                          objPromoter.ITR_Aadhaar_Number__c = String.isNotBlank(mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).ITR_Aadhaar_Number__c) ? mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).ITR_Aadhaar_Number__c : '';
                          objPromoter.Passport_Number__c = String.isNotBlank(mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Passport_Number__c) ? mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Passport_Number__c : '';
                          objPromoter.Voter_ID_Number__c = String.isNotBlank(mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Voter_ID_Number__c) ? mapIdtoPromoterDetail.get(objPromoter.Promoter_Detail__c).Voter_ID_Number__c : '';
                      }
                  }
               }
            }
        }
        *******************************************************************************************************************************************/
    }
    
    
}