@isTest(SeeAllData = false)
private class InitiateCibilCalloutAPFControllerTest {

	private static testMethod void testMethod1() {
        
	    List<RecordType> lstRecordType = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Individual'];
        Id builderIndvRecTypeId;
        if(!lstRecordType.isEmpty()) {
            builderIndvRecTypeId = lstRecordType[0].Id;
        }
        
        Account acc = new Account();
            acc.RecordTypeId = builderIndvRecTypeId;
            acc.Name = 'Individual Test Acc';
            acc.PAN__c = 'AAAAA3333B';
            acc.GSTIN__c = '27AAHFG0551H1ZL';
        insert acc;
        
        List<RecordType> lstRecordType2 = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Corporate'];
        Id builderCorpRecTypeId;
        if(!lstRecordType2.isEmpty()) {
            builderCorpRecTypeId = lstRecordType2[0].Id;
        }
        
        List<RecordType> lstIndividualPBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Individual'];
        List<RecordType> lstCorporatePBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Corporate'];
        
        Account acc2 = new Account();
            acc2.RecordTypeId = builderCorpRecTypeId;
            acc2.Name = 'Test Corporate Account';
            acc2.PAN__c = 'AAAAB2222B';
            
        insert acc2;
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83125');
        insert objPincode;
        
        Project__c objProject = new Project__c();
        //objProject.Assigned_Sales_User__c = assignedSalesUser != null ? assignedSalesUser.Id : null;
        objProject.Account__c = acc.Id;
        objProject.Project_Name__c = 'Test Project';
        objProject.APF_Status__c = 'APF Initiated';
        objProject.Stage__c = 'APF Initiation';
        objProject.Sub_Stage__c = 'Project Data Capture';
        objProject.APF_Type__c = 'New';
        objProject.Address_Line_1__c = 'Test Address';
        objProject.Pincode__c = objPincode.Id;
        insert objProject;
        
        Project_Builder__c objProjectBuilderCorp = new Project_Builder__c();
        objProjectBuilderCorp.RecordTypeId = lstCorporatePBId[0].Id;
        objProjectBuilderCorp.Builder_Name__c = acc2.Id;
        objProjectBuilderCorp.Constitution__c = '24';
        objProjectBuilderCorp.Builder_Status__c = 'Resident';
        objProjectBuilderCorp.Contact_Number_Mobile__c = '9999444223';
        objProjectBuilderCorp.PAN_Number__c = 'EGNPS1111T';
        objProjectBuilderCorp.Project__c = objProject.Id; 
        objProjectBuilderCorp.Preferred_Communication_Address__c = 'CURRES'; 
        //objProjectBuilderCorp. =
        Database.insert(objProjectBuilderCorp,false);
        
        acc2.GSTIN__c = '27AAHFG0551H1ZL';
        update acc2;
        
        insert objProjectBuilderCorp;
        System.debug('Debug Log for GSTIN'+objProjectBuilderCorp);
        
        
        objProjectBuilderCorp.GSTIN_Number__c = '27AAHFG0551H1ZL';
        //objProjectBuilderCorp.Builder_Role__c = Constants.strPBConstructorMarketer;
        update objProjectBuilderCorp;
        
        Set<Id> setProjectId = new Set<Id>();
        setProjectId.add(objProject.Id);
        
        
        
        List<Project_Builder__c> lstProjectBuilder = [SELECT Id, Gender__c, RecordTypeId, Builder_Category__c, GSTIN_Available__c, Name, Date_Of_Birth__c, Builder_Name__r.RecordType.DeveloperName, All_Promoter_Count__c, Builder_Type__c, Class_Of_Activity__c, Builder_Name__c, Builder_Name__r.RecordTypeId, Builder_Status__c, Constitution__c, Builder_Role__c, Contact_Number_Mobile__c, Email__c, Preferred_Communication_Address__c, Address_Line_1__c, Address_Line_2__c, Pincode__c, Promoter_PAN_Validation_Count__c, Promoter_Cibil_Validation_Count__c, Project__c,(Select Id, Name, Name_of_Promoter__c, Email__c, Contact_No_Mobile__c, Preferred_Communication_Address__c, Promoter_Status__c, Address_Line_1__c, Address_Line_2__c, Pincode__c from Promoters__r) From Project_Builder__c Where Project__c =: objProject.Id];
        System.debug('Debug Log for lstProjectBuilder size'+lstProjectBuilder.size());
        String builderIndividualRecordId;
        List<Promoter__c> lstPromoter = new List<Promoter__c>();
        if(!lstProjectBuilder.isEmpty()) {
            for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                objProjectBuilder.GSTIN_Available__c = true;
                objProjectBuilder.GSTIN_Number__c = '27AAHFG0551H1ZL';
                if(objProjectBuilder.Promoters__r.isEmpty() && objProjectBuilder.Builder_Name__r.RecordType.DeveloperName != 'Builder_Individual'){
                    Promoter__c objPromoter = new Promoter__c(Project_Builder__c = objProjectBuilder.Id, Preferred_Communication_Address__c = 'CURRES', PAN_Number__c = 'AAARR1123L');
                    lstPromoter.add(objPromoter);
                }
                else {
                    builderIndividualRecordId = objProjectBuilder.Id;
                    objProjectBuilder.Gender__c = 'M';
                }
            }
            update lstProjectBuilder;
            System.debug('Debug Log for lstPromoter'+lstPromoter.size());
            if(!lstPromoter.isEmpty()) {
                insert lstPromoter;
            }
            System.debug('Debug Log for lstPromoter post insertion from Address Check Method'+lstPromoter);
            update lstPromoter;
            
        }
        
        InitiateCibilCalloutAPFController.checkCibilEligibility(lstPromoter[0].Id);
        InitiateCibilCalloutAPFController.checkCibilEligibility(builderIndividualRecordId);
        InitiateCibilCalloutAPFController.checkCibilEligibility(objProjectBuilderCorp.Id);
        
        InitiateCibilCalloutAPFController.ResponseCIBILWrapper objRCibil1 = InitiateCibilCalloutAPFController.createCustomerIntegration(lstPromoter[0].Id, false);
        InitiateCibilCalloutAPFController.ResponseCIBILWrapper objRCibil2 = InitiateCibilCalloutAPFController.createCustomerIntegration(builderIndividualRecordId, false);
        InitiateCibilCalloutAPFController.ResponseCIBILWrapper objRCibil3 = InitiateCibilCalloutAPFController.createCustomerIntegration(objProjectBuilderCorp.Id, false);
        
        Builder_Integrations__c integrations = new Builder_Integrations__c();
                    integrations.Project_Builder__c = objProjectBuilderCorp.Id;
                    integrations.Project__c = objProject.Id;
                    integrations.recordTypeId = Schema.SObjectType.Builder_Integrations__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId();
        insert integrations;
        
        
        InitiateCibilCalloutAPFController.Header resultHeader = new InitiateCibilCalloutAPFController.Header();
            resultHeader.ApplicationId = objProjectBuilderCorp.Id;
            resultHeader.CustId = integrations.Id;
            resultHeader.RequestType = 'Test';
            resultHeader.RequestTime = 'Test';
            resultHeader.SFDCRecordId = integrations.ID;
        
        InitiateCibilCalloutAPFController.Resultresponse result = new InitiateCibilCalloutAPFController.Resultresponse();
            result.Header =  resultHeader;
            result.AcknowledgementId = 'Test';
            result.Status = 'SUCCESS';
        
        Test.startTest();
            TestMockRequest req1=new TestMockRequest(200,'OK',JSON.serialize(result),null);
            Test.setMock(HttpCalloutMock.class, req1);
            InitiateCibilCalloutAPFController.ResponseCIBILWrapper objRCibilWrap3 = InitiateCibilCalloutAPFController.integrateCibil(objProjectBuilderCorp.Id, integrations.Id);
            
        Test.stopTest();
        
        
	
	}

}