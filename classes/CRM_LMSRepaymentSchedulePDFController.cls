public class CRM_LMSRepaymentSchedulePDFController {
    
    public case theCase{get;set;}
    public map<string,Object> pdfDetailsMap;
    public date today {get{return date.today();}set;}
    public CRM_caseRepaymentResponse caseRepaymentResponse{get;set;}
    public String jsonString;
    public HttpResponse response;
    public String endpoint;
    
    public CRM_LMSRepaymentSchedulePDFController(ApexPages.StandardController stdController) {
        try{this.theCase = (Case)stdController.getRecord();
            Id caseId = ApexPages.currentPage().getParameters().get('id');
            theCase = [select LMS_Application_ID__c, Loan_Application_Number__c, LD_Branch_ID__c, account.Name, LD_Address_Line1__c, LD_Address_Line2__c, LD_Address_Line3__c, LD_GST_State__c, LD_Pincode__c, CLD_Mobile_Number__c from case where Id=: caseId];
            Rest_Service__mdt rs = [Select Client_Username__c, Client_Password__c, Service_EndPoint__c from Rest_Service__mdt where DeveloperName = 'CRM_Repayment_Schedule'];
            
            String username = rs.Client_Username__c;
            String password = rs.Client_Password__c;
            endpoint = rs.Service_EndPoint__c;
            Blob headerValue = Blob.valueOf(rs.Client_Username__c+ ':' +rs.Client_Password__c);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            
            Map<String, Object> jsonMap = new Map<String, Object>();
            jsonMap.put('agreementId', theCase.LMS_Application_ID__c);
            jsonMap.put('loanNumber', theCase.Loan_Application_Number__c);
            jsonMap.put('branchId', theCase.LD_Branch_ID__c);
            jsonMap.put('currency', 'INR');
            jsonMap.put('viewFlag', 'V');
            jsonMap.put('reportFlag', 'EMI');
            
            jsonString = JSON.Serialize(jsonMap);
            
            Http p=new Http();
            HttpRequest request =new HttpRequest();
            
            request.setHeader('Content-Type','application/json');
            request.setHeader('accept','application/json');
            
            request.setHeader('Authorization', authorizationHeader);
            request.setHeader('operation-flag','R');
            
            request.setEndPoint(endpoint);
            request.setMethod('POST');
            request.setBody(jsonString);
            
            response =p.send(request);
            caseRepaymentResponse = CRM_caseRepaymentResponse.parse(response.getBody());
            
            system.debug('@@@ response.getStatusCode()--> '+response.getStatusCode());
            if(response.getStatusCode() != 200) {
                throw new CalloutException('');
            }
           }
        catch(exception e){
            throw new AuraHandledException('');
        }
    }
    
    //Create integration logs
    public void logUtility(){
        try{
            system.debug('Repayment controller LOG UTILITY CALLED');
            LogUtility.createIntegrationLogs(jsonstring,response,endpoint);
        }
        catch(exception e){
            throw new AuraHandledException('');
        }
    }
}