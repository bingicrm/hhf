/* **************************************************************************
*
* Controller Class: apApplicationException
* Created by Anil Meghnathi: 04/10/2015
*
* Neilon application exception class. later can be expanded.
* 

* - Modifications:
* - Anil Meghnathi, 04/10/2015 – Initial Development
************************************************************************** */
public with sharing class apApplicationException extends Exception {
}