public class RejectOperationsAPF {
    @AuraEnabled
    public static Project__c getLoanApp(Id lappId){
        system.debug('=='+lappId);
        return [SELECT Id, Name, Stage__c, Sub_Stage__c, RecordTypeID, Comments__c
                FROM Project__c
                WHERE Id = :lappId ];
    }
    
    @AuraEnabled
    public static MoveNextWrapper moveprevious(id oppId, string comments) 
    {
        Project__c la = new Project__c();
        //Added by Shobhit Saxena for Comments Trail
        la = [select Id, Comments_Trail__c, Previous_Sub_Stage__c, Stage__c, Sub_Stage__c, APF_Status__c, Comments__c,Approval_Rejection_Comments__c from Project__c where Id =: oppId];
        
        List<Project_Builder__c> lstProjectBuilder = [Select Id, Name, RecordTypeId, RecordType.DeveloperName, (Select Id, Name, RecordTypeId From Promoters__r) From Project_Builder__c Where Project__c =: oppId];
        if(la != null) {
            la.Stage__c = 'APF Reject';
            la.Sub_Stage__c = 'APF Reject';
            la.APF_Status__c = 'APF Reject';
            la.Date_of_Rejection__c = System.TODAY();
            la.RecordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeRejected).getRecordTypeId();
            
        }
        
        List<Promoter__c> lstPromoter = new List<Promoter__c>();
        if(!lstProjectBuilder.isEmpty()) {
            for(Project_Builder__c objPB : lstProjectBuilder) {
                if(objPB.RecordType.DeveloperName == 'Builder_Corporate') {
                    objPB.RecordTypeId = Schema.SObjectType.Project_Builder__c.getRecordTypeInfosByName().get(Constants.strPBRecTypeReadOnlyCorp).getRecordTypeId();
                }
                else if(objPB.RecordType.DeveloperName == 'Builder_Individual') {
                    objPB.RecordTypeId = Schema.SObjectType.Project_Builder__c.getRecordTypeInfosByName().get(Constants.strPBRecTypeReadOnlyIndv).getRecordTypeId();
                }
                if(!objPB.Promoters__r.isEmpty()) {
                    for(Promoter__c objPromoter : objPB.Promoters__r) {
                        objPromoter.RecordTypeId = Schema.SObjectType.Promoter__c.getRecordTypeInfosByName().get('Read Only').getRecordTypeId();
                        lstPromoter.add(objPromoter);
                    }
                }
            }
            update lstProjectBuilder;
            if(!lstPromoter.isEmpty()) {
               update lstPromoter;
            }
        }
        
        try 
        {                
            System.debug('Debug Log for Sub Stage'+la.Sub_Stage__c);
            update la;
            return new MoveNextWrapper(false,la.Sub_Stage__c);
        } 
        catch (DMLException e) 
        {
            return new MoveNextWrapper(true,e.getDmlMessage(0));
        } 
        catch (Exception e) 
        {
            return new MoveNextWrapper(true,e.getMessage());
        } 
    }
    
    public class MoveNextWrapper
    {
        @AuraEnabled
        public Boolean error;
        @AuraEnabled
        public String msg;
        
        public MoveNextWrapper(Boolean error, String msg )
        {
            this.error = error;
            this.msg = msg;
        }
        
    }
}