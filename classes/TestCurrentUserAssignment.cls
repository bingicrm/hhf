@isTest
private class TestCurrentUserAssignment {
    
    static testmethod void test1(){
        User u;
        Account acc;
        GroupMember GM;
        Group g;
        
        User adminUser = new User(Id=UserInfo.getUserId());
        
        User thisUser = [Select Id from User where Name = 'Vinay Arora'];
        
        System.runAs(thisUser){
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                          Phone='9934567890',RecordTypeId=RecordTypeIdAccount);
        insert acc;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                     EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                     LocaleSidKey='en_US', ProfileId = p.Id, 
                     TimeZoneSidKey='America/Los_Angeles', UserName='standarduser8@testorg.com');
        //insert u;
        
        /*Group objGroup = new Group(Type='Queue',Name='Express Data Checker',DeveloperName='Express_Data_Checker');
insert objGroup;

QueueSObject q = new QueueSObject(SobjectType='Loan_Application__c', QueueId=objGroup.Id);
insert q;

GM = new GroupMember(GroupId=objGroup.Id,UserOrGroupId=thisUser.Id);
insert GM;

System.debug('objGroup==>'+objGroup.Id);*/
        
        //Group objGroup = [SELECT Id from Group WHERE Type='Queue' AND DeveloperName='Express_Data_Checker'];
        System.runAs(adminUser){
        Group objGroup = new Group(Name='test group', Type='Queue');
        insert objGroup;
            
            QueuesObject testQueue = new QueueSObject(QueueID = objGroup.id, SObjectType = 'Loan_Application__c');
            insert testQueue;
            
            g = [select Id from Group where Name='test group' AND Type = 'Queue'];
            
            GroupMember member = new GroupMember();
            member.UserOrGroupId = UserInfo.getUserId();
            member.GroupId = g.Id;
            insert member;
        }
        
        Branch_Master__c objBM = new Branch_Master__c(Name='Delhi',Office_Code__c='1');
        insert objBM;
        
        Id RecordTypeIdLoanApp = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('COPS:Data Checker').getRecordTypeId();
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                         Transaction_type__c='PB',
                                                                         Requested_Amount__c=100000,
                                                                         Loan_Purpose__c='11',
                                                                         Sub_Stage__c='Application Initiation',
                                                                         StageName__c='Customer Onboarding',
                                                                         RecordTypeId=RecordTypeIdLoanApp,
                                                                         Branch__c='All',Branch_Lookup__c=objBM.Id
                                                                         //Assigned_Sales_User__c=thisUser.Id,
                                                                         //OwnerId=objGroup.Id
                                                                        );
        insert objLoanApplication;
        
        Id RecordTypeISourceDetail = Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('DST').getRecordTypeId();
        
        DST_Master__c objDSTMaster = new DST_Master__c(Name='Test DST Master', Inspector_ID__c='1234');
        insert objDSTMaster;
        
        Sourcing_Detail__c objSourceDetail = new Sourcing_Detail__c(Loan_Application__c=objLoanApplication.Id,
                                                                    //Sales_User_Name__c=thisUser.Id,
                                                                    DST_Name__c=objDSTMaster.Id);
        insert objSourceDetail;
        
        Loan_Contact__c objLoanContact = [SELECT Id,Income_Considered__c from Loan_Contact__c WHERE Loan_Applications__c=:objLoanApplication.Id];
        objLoanContact.Borrower__c = '1';
        objLoanContact.Constitution__c = '20';
        objLoanContact.Applicant_Status__c = '1';
        objLoanContact.Customer_segment__c = '1';
        objLoanContact.Income_Program_Type__c = 'FLIP';
        objLoanContact.Mobile__c='9876543210';
        objLoanContact.Email__c='testemail@gmail.com';
        objLoanContact.Category__c='1';
        update objLoanContact;
        
        objLoanApplication.StageName__c = 'Operation Control';
        objLoanApplication.Sub_Stage__c = 'COPS:Data Checker';
        objLoanApplication.OwnerId = g.Id;
        update objLoanApplication;

            
        Test.startTest();
            
        CurrentUserAssignment.setOwner(objLoanApplication.Id);
       
         CurrentUserAssignment.setRecordTypeAndStoreAssignedQueueUser(objLoanApplication,'Queue');
        Test.stopTest();
        /*objLoanApplication = new Loan_Application__c(Id=objLoanApplication.Id,OwnerId=objGroup.Id);
Update objLoanApplication;

CurrentUserAssignment.setOwner(objLoanApplication.Id);*/    
        }
    }
}