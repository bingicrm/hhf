/*=====================================================================
* Persistent Systems Ltd
* Name: batchUpdateAccountsContacts
* Description: This batch class is used pick changed/new customer details and send to ameyo for sync.
* Created Date: [09/03/2021]
* Created By: Surekha Bharamgonda
*
* Date Modified                Modified By                  Description of the update

    
=====================================================================*/
global class batchUpdateAccountsContacts implements Database.Batchable <sObject>,Database.Stateful, Database.AllowsCallouts,Schedulable {   
    global batchUpdateAccountsContacts()
    {
    }
    String jsonData;
    Date today = system.today(); //variable to be used in SOQL
    Set<id> successRecord = new Set<id>();
    Set<id> failRecord = new Set<id>();  
    // Start Method of Batch class
 	global Database.QueryLocator start(Database.BatchableContext info)
    { 
    	String SOQL='Select id,name,phone from Account where phone!=null';
            //where CreatedDate =: today OR LastModifiedDate =:today';
            system.debug('SOQL '+SOQL);
       	return Database.getQueryLocator(SOQL);// Query the records
   	}     
    // Execute Method of Batch class
   	global void execute(Database.BatchableContext info, List<Account> scope)
    {
        
        try
        {
            if(!scope.isEmpty())
            {
                Rest_Service__mdt rs = [Select Client_Username__c, Client_Password__c, Service_EndPoint__c from Rest_Service__mdt where DeveloperName = 'CRM_AmeyoUploadContactAndAddCallback'];
                String username = rs.Client_Username__c;
                String password = rs.Client_Password__c;
                String endpoint = rs.Service_EndPoint__c;
                Blob headerValue = Blob.valueOf(rs.Client_Username__c+ ':' +rs.Client_Password__c);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
				JSONGenerator jsonGen = JSON.createGenerator(true);    
            	jsonGen.writeStartObject(); 
                jsonGen.writeFieldName('CustomerList');
                jsonGen.writeStartArray();            
                for(Account acc:scope)
                {
                    jsonGen.writeStartObject();
                    jsonGen.writeStringField('phone1', acc.Phone);
                    jsonGen.writeStringField('customerId',acc.Id); 
                    jsonGen.writeEndObject();
                }   
                jsonGen.writeEndArray();
                jsonGen.writeEndObject();                     
                jsonData = jsonGen.getAsString();
                System.debug('Json Data - ' + jsonData);  
                // Now make the HTTP callout
                Http http = new Http();
                HttpRequest req = new HttpRequest();
                HttpResponse res = new HttpResponse(); 
                req.setHeader('Content-Type','application/json');
            	req.setHeader('accept','application/json');            
                req.setHeader('Authorization', authorizationHeader);
                req.setHeader('operation-flag','R');            
                req.setEndPoint(endpoint);
                req.setMethod('POST');
                req.setBody(jsonData);
                req.setCompressed(true); 
                //send the request
                res = http.send(req);                
              //  caseRepaymentResponse = CRM_caseRepaymentResponse.parse(response.getBody());            
            	system.debug('@@@ res.getStatusCode()--> '+res.getStatusCode());
                //check the response
            	if(res.getStatusCode() != 200) 
                {
                	throw new CalloutException('');
            	}
                
                /*if (res.getStatusCode() == 200) 
                {
                    String jsonResponse = res.getBody();
                    System.debug('Response-' + jsonResponse);
				}*/
         	}            
        }
        catch(exception e){
            throw new AuraHandledException(label.CRM_CaseGenDocGenericCalloutError);
        }
       /* catch (Exception e) 
        {         
            System.debug('Error:' + e.getMessage() + 'LN:' + e.getLineNumber() );           
        }*/    	
	}     
   	global void finish(Database.BatchableContext info)
    { 
   		// Get the ID of the AsyncApexJob representing this batch job
   		// from Database.BatchableContext.
   		// Query the AsyncApexJob object to retrieve the current job's information.
   		AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
      	TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = :info.getJobId()];     
   		// Send an email to the Apex job's submitter notifying of job completion.
   		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();       
       	String[] toAddresses = new String[] {a.CreatedBy.Email};
       	mail.setToAddresses(toAddresses);
       	mail.setSubject('Account and contact update' + a.Status);
       	mail.setPlainTextBody('The batch Apex job processed ' +jsonData);
        //a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures.'+successRecord+'successRecordids: '+ 'failRecordids: '+ failRecord);
   		//Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
   	} 
    global void execute(SchedulableContext SC)
    {
    	database.executeBatch(new batchUpdateAccountsContacts(),100);
       	//for cron expression
       	// String cronexpression = ‘0 0 0 ? * * *’
       	// System.schedule(‘Testing’, cronexpression, testobj);        
    } 
}