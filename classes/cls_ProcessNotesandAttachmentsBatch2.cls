global class cls_ProcessNotesandAttachmentsBatch2 implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {  
          return Database.getQueryLocator('Select Id, Name, Loan_Number__c, Number_of_Applicant_Documents__c, Number_of_Co_App_Documents__c, Number_of_Application_Documents__c, Documents_in_Attachments__c, Applicant_Documents_Migrated__c, Co_App_Documents_Migrated__c, Application_Documents_Migrated__c, Attachment_Documents_Migrated__c, StageName__c, Sub_Stage__c, Disbursement_Date__c, Days_Since_Loan_Disbursed__c, Folders_Created_in_S3__c, Sanctioned_Disbursement_Type__c, Date_of_Cancellation__c, Days_Since_Loan_Cancelled__c, Date_of_Rejection__c, Days_Since_Loan_Rejected__c From Loan_Application__c Where (Sub_Stage__c = \'CPC Scan Checker\' OR Sub_Stage__c = \'Loan Disbursed\' OR Sub_Stage__c = \'Loan Disbursed\' OR Sub_Stage__c = \'Loan Cancel\' OR Sub_Stage__c = \'Loan reject\') AND Folders_Created_in_S3__c = true');
    
    }
    global void execute(Database.BatchableContext BC, List<Loan_Application__c > lstLoanApplications)
        {    
            list<Loan_Application__c> lstloan = new list<Loan_Application__c>();
            for (Loan_Application__c objLA : lstLoanApplications){
                
                if(String.isNotBlank(Label.Delete_Documents_for_Previous_Records) && Label.Delete_Documents_for_Previous_Records == 'true') {
                    if(objLA.Sanctioned_Disbursement_Type__c == 'Single Tranche'  && objLA.Sub_Stage__c == 'Loan Disbursed' && Label.Document_Movement_Disbursed_Loan != null && objLA.Days_Since_Loan_Disbursed__c >= Decimal.valueOf(Label.Document_Movement_Disbursed_Loan).setScale(0)) {
                        lstloan.add(objLA);
                    }
                    else if(objLA.Sanctioned_Disbursement_Type__c == 'Multiple Tranche' && objLA.Sub_Stage__c == 'Loan Disbursed') {
                        lstloan.add(objLA);
                    }
                    if(objLA.StageName__c == 'Loan Cancelled' && objLA.Sub_Stage__c == 'Loan Cancel' && objLA.Date_of_Cancellation__c != null && String.isNotBlank(Label.Document_Movement_Cancelled_Loan) && objLA.Days_Since_Loan_Cancelled__c >= Decimal.valueOf(Label.Document_Movement_Cancelled_Loan).setScale(0)) {
                        lstloan.add(objLA);
                    }
                    else if(objLA.StageName__c == 'Loan Rejected' && objLA.Sub_Stage__c == 'Loan reject' && objLA.Date_of_Rejection__c != null && String.isNotBlank(Label.Document_Movement_Rejected_Loan) && objLA.Days_Since_Loan_Rejected__c >= Decimal.valueOf(Label.Document_Movement_Rejected_Loan).setScale(0)) {
                        lstloan.add(objLA);
                    }
                }
                
                else {
                    if(objLA.Sanctioned_Disbursement_Type__c == 'Single Tranche' && objLA.Sub_Stage__c == 'Loan Disbursed' && Label.Document_Movement_Disbursed_Loan != null && objLA.Days_Since_Loan_Disbursed__c == Decimal.valueOf(Label.Document_Movement_Disbursed_Loan).setScale(0)) {
                        lstloan.add(objLA);
                    }
                    else if(objLA.Sanctioned_Disbursement_Type__c == 'Multiple Tranche' && objLA.Sub_Stage__c == 'Loan Disbursed') {
                        lstloan.add(objLA);
                    }
                    else if(objLA.StageName__c == 'Loan Cancelled' && objLA.Sub_Stage__c == 'Loan Cancel' && objLA.Date_of_Cancellation__c != null && String.isNotBlank(Label.Document_Movement_Cancelled_Loan) && objLA.Days_Since_Loan_Cancelled__c == Decimal.valueOf(Label.Document_Movement_Cancelled_Loan).setScale(0)) {
                        lstloan.add(objLA);
                    }
                    else if(objLA.StageName__c == 'Loan Rejected' && objLA.Sub_Stage__c == 'Loan reject' && objLA.Date_of_Rejection__c != null && String.isNotBlank(Label.Document_Movement_Rejected_Loan) && objLA.Days_Since_Loan_Rejected__c == Decimal.valueOf(Label.Document_Movement_Rejected_Loan).setScale(0)) {
                        lstloan.add(objLA);
                    }
                }
                
            }
            System.debug('Debug Log fo lstLoan'+lstLoan.size());
            System.debug('Debug Log fo lstLoan'+lstLoan);
            
            if(!lstLoan.isEmpty()) {
                for(Loan_Application__c objLA : lstLoan) {
                    cls_MoveDocumentstoS3Files.createS3FilesLoanKit2(objLA);
                }
            }
      } 
        
    global void finish(Database.BatchableContext BC)
    {
		 AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors,JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
          
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Notes & Attachments Document Migration Batch 2' + a.Status);
            mail.setPlainTextBody('During Notes & Attachments Migration, records processed ' + a.TotalJobItems +
           'with '+ a.NumberOfErrors + ' failures.');
        if(!test.isrunningTest()){
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        Database.executeBatch(new cls_RetryDocumentsMovementBatch(),1);
    }
}