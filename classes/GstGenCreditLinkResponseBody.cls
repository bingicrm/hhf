public class GstGenCreditLinkResponseBody {
    public cls_result result;
    public String requestId;    //71c0b669-2471-11e9-b70c-4719335f1e77
    public Integer statusCode;  //101
    public class cls_result {
        public String weblink;  //https://gst.karza.in/gstn-cred/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjozNTkwMTksImNsaWVudF9pZCI6Imhlcm9maW5fYXBpX3Byb2QiLCJjbGllbnRfbmFtZSI6IkhFUk8gRklOQ09SUCIsInJlZl9pZCI6IjU0MTM1NDEzIiwiZXhwIjoxNTUwMDUwMTI4LjQxNDcwNiwicmVxdWVzdF9pZCI6IjcyMzA0MTU0LTI0NzEtMTFlOS1hYzA2LTBiMDQ5YTEzNmUyOSIsImdzdGluIjoiMDJBQUFDUjUwNTVLMVpKIn0.kHqtGLgjkHT-v7968bD80R7octq0w-XAR3edZfPp9_c
    }
}