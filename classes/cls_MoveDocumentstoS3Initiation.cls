public class cls_MoveDocumentstoS3Initiation {

    //public static void moveDocumentstoS3(Id loanApplicationId) {
    public static void moveDocumentstoS3(Loan_Application__c objLA) {
        //The entire code block will be executed only when the loanApplicationId is not blank.
        //if(!String.isBlank(loanApplicationId)) {
            Set<Id> setApplicantRelatedDocs = new Set<Id>();
            Set<Id> setCoApplicantGuarantorDocs = new Set<Id>();
            Set<Id> setApplicationRelatedDocs = new Set<Id>();
            Set<Id> setAllDocuments = new Set<Id>();
            //List<ContentDocumentLink> lstContentDocLinks;
            //List<ContentVersion> lstContentVersion;
            Set<Id> setContentDocumentLinkIds = new Set<Id>();
            Set<Id> setContentDocIdsDocChkLst = new Set<Id>();
            Set<Id> setContentVersionId = new Set<Id>();
            Map<Id,Id> mapConDocLnktoDocChkLst = new Map<Id,Id>();
            Map<Id,Id> mapConDocLnktoDocChkLst1 = new Map<Id,Id>();
            
            /****************************************Beginning of Logic for Creation of S3 Folder for Loan Application************************************/
            List<NEILON__Folder__c> lstBucket;
            if(System.Label.S3_Folder_Search_Basis != null && System.Label.S3_Folder_Search_Basis == 'NEILON__Parent__c') {
                lstBucket = [Select Id, Name, NEILON__Bucket_Region__c From NEILON__Folder__c Where NEILON__Parent__c = null];
            }
            else if(System.Label.S3_Folder_Search_Basis != null && System.Label.S3_Folder_Search_Basis == 'NEILON__Bucket_Name__c') {//For UAT Only
                lstBucket = [Select Id, Name, NEILON__Bucket_Region__c From NEILON__Folder__c Where NEILON__Bucket_Name__c = Null];
            }
            else {
                lstBucket = [Select Id, Name, NEILON__Bucket_Region__c From NEILON__Folder__c Where NEILON__Parent__c = null];
            }
            //String loanAppName = [Select Id, Name From Loan_Application__c Where Id=: loanApplicationId].Name;
            String strApplicantId;
            Loan_Contact__c objApplicant = [Select Id, Name, Applicant_Type__c from Loan_Contact__c Where Loan_Applications__r.Name =: objLA.Name LIMIT 1];
            if(objApplicant != null) {
                strApplicantId = objApplicant.Id;
            }
            List<S3_Folders_Configuration__mdt> lstS3ConfigData = [SELECT Id, Delete_files__c, Folder_Name__c, MasterLabel, DeveloperName, NamespacePrefix FROM S3_Folders_Configuration__mdt];
            Map<String, S3_Folders_Configuration__mdt> mapDeveloperNametoRecord = new Map<String, S3_Folders_Configuration__mdt>();
            System.debug('Debug Log for lstS3ConfigData'+lstS3ConfigData.size());
            if(!lstS3ConfigData.isEmpty()) {
                for(S3_Folders_Configuration__mdt objS3ConfigData : lstS3ConfigData) {
                    mapDeveloperNametoRecord.put(objS3ConfigData.DeveloperName, objS3ConfigData);
                }
            }
            System.debug('Debug Log for mapDeveloperNametoRecord'+mapDeveloperNametoRecord.size());
            System.debug('Debug Log for lstBucket'+lstBucket.size());
            if(!lstBucket.isEmpty()) {
                // Create roo
                NEILON__Folder__c objS3Folder = NEILON.apGlobalUtils.buildFolderArchitecture(objLA.Id);
                if(String.isNotBlank(objS3Folder.Id)) {
                    /****************************************Beginning of Logic for Creation of S3 Folder for Applicant Related Documents************************************/
                    List<NEILON__Folder__c> lstParentFolder = [Select Id, Name From NEILON__Folder__c Where Id =: objS3Folder.Id];
                    List<NEILON__Folder__c> lstFolderstoCreate = new List<NEILON__Folder__c>();
                    if(!lstParentFolder.isEmpty()) {
                        NEILON__Folder__c objS3Folder1 = new NEILON__Folder__c();
                        objS3Folder1.Loan_Application__c = objLA.Id;
                        objS3Folder1.NEILON__Bucket_Name__c = lstBucket[0].Name;
                        objS3Folder1.NEILON__Bucket_Region__c = lstBucket[0].NEILON__Bucket_Region__c;
                        objS3Folder1.NEILON__Parent_Id__c = objLA.Id;
                        objS3Folder1.NEILON__Parent__c = lstParentFolder[0].Id; 
                        objS3Folder1.NEILON__Parent_Object_API_Name__c = Constants.strParentObjectAPIName;
                        if(strApplicantId != null) {
                            objS3Folder1.Loan_Contact__c = strApplicantId;
                        }
                        if(!mapDeveloperNametoRecord.isEmpty() && mapDeveloperNametoRecord.containsKey(Constants.strApplicantConstant) && String.isNotBlank(mapDeveloperNametoRecord.get(Constants.strApplicantConstant).Folder_Name__c)) {
                            objS3Folder1.Name = mapDeveloperNametoRecord.get(Constants.strApplicantConstant).Folder_Name__c + ' '+objLA.Name;
                        }
                        
                        lstFolderstoCreate.add(objS3Folder1);
                        
                        NEILON__Folder__c objS3Folder2 = new NEILON__Folder__c();
                        objS3Folder2.Loan_Application__c = objLA.Id;
                        objS3Folder2.NEILON__Bucket_Name__c = lstBucket[0].Name;
                        objS3Folder2.NEILON__Bucket_Region__c = lstBucket[0].NEILON__Bucket_Region__c;
                        objS3Folder2.NEILON__Parent_Id__c = objLA.Id;
                        objS3Folder2.NEILON__Parent__c = lstParentFolder[0].Id; 
                        objS3Folder2.NEILON__Parent_Object_API_Name__c = Constants.strParentObjectAPIName;
                        //objS3Folder2.Name = 'Co-Applicant Documents for '+objLA.Name;
                        if(!mapDeveloperNametoRecord.isEmpty() && mapDeveloperNametoRecord.containsKey('Co_Applicant') && String.isNotBlank(mapDeveloperNametoRecord.get('Co_Applicant').Folder_Name__c)) {
                            objS3Folder2.Name = mapDeveloperNametoRecord.get('Co_Applicant').Folder_Name__c+ ' '+objLA.Name;
                        }
                        lstFolderstoCreate.add(objS3Folder2);
                        
                        NEILON__Folder__c objS3Folder3 = new NEILON__Folder__c();
                        objS3Folder3.Loan_Application__c = objLA.Id;
                        objS3Folder3.NEILON__Bucket_Name__c = lstBucket[0].Name;
                        objS3Folder3.NEILON__Bucket_Region__c = lstBucket[0].NEILON__Bucket_Region__c;
                        objS3Folder3.NEILON__Parent_Id__c = objLA.Id;
                        objS3Folder3.NEILON__Parent__c = lstParentFolder[0].Id; 
                        objS3Folder3.NEILON__Parent_Object_API_Name__c = Constants.strParentObjectAPIName;
                        //objS3Folder3.Name = 'Application Documents for '+objLA.Name;
                        if(!mapDeveloperNametoRecord.isEmpty() && mapDeveloperNametoRecord.containsKey('Application') && String.isNotBlank(mapDeveloperNametoRecord.get('Application').Folder_Name__c)) {
                            objS3Folder3.Name = mapDeveloperNametoRecord.get('Application').Folder_Name__c+ ' '+objLA.Name;
                        }
                        lstFolderstoCreate.add(objS3Folder3);
                        
                        NEILON__Folder__c objS3Folder4 = new NEILON__Folder__c();
                        objS3Folder4.Loan_Application__c = objLA.Id;
                        objS3Folder4.NEILON__Bucket_Name__c = lstBucket[0].Name;
                        objS3Folder4.NEILON__Bucket_Region__c = lstBucket[0].NEILON__Bucket_Region__c;
                        objS3Folder4.NEILON__Parent_Id__c = objLA.Id;
                        objS3Folder4.NEILON__Parent__c = lstParentFolder[0].Id; 
                        objS3Folder4.NEILON__Parent_Object_API_Name__c = Constants.strParentObjectAPIName;
                        //objS3Folder4.Name = 'Notes & Attachments for '+objLA.Name;
                        if(!mapDeveloperNametoRecord.isEmpty() && mapDeveloperNametoRecord.containsKey('Notes_and_Attachments') && String.isNotBlank(mapDeveloperNametoRecord.get('Notes_and_Attachments').Folder_Name__c)) {
                            objS3Folder4.Name = mapDeveloperNametoRecord.get('Notes_and_Attachments').Folder_Name__c+ ' '+objLA.Name;
                        }
                        lstFolderstoCreate.add(objS3Folder4);
                        Database.SaveResult[] lstDSR= Database.insert(lstFolderstoCreate,false);
                        for(Database.SaveResult objDSR : lstDSR) {
                            if(objDSR.getID() != null) {
                                System.debug('Folder Creation Successful with Id'+objDSR.getId());
                                objLA.Folders_Created_in_S3__c = true;
                                objLA.Migration_Initiation_Date__c = System.Today();
                                update objLA;
                            }
                            else {
                                System.debug('Debug Log for Errors(if any), during folder creation'+objDSR.getErrors());
                            }
                        }
                        
                    }
                    /*******************************************End of Logic for Creation of S3 Folder for Co-Applicant Related Documents***************************************/
                }
            }
            
            List<Attachment> lstAttachment;
            List<ContentDocumentLink> lstContentDocLinks;
            List<ContentDocumentLink> lstContentDocDocChecklist;
            List<ContentVersion> lstContentVersion;
            List<ContentVersion> lstContentVersionDocChkLst;
            Map<Id,Id> mapConDocIdtoConDocLink = new Map<Id,Id>();
            List<Document_Migration_Log__c> lstDocumentMigrationLog = new List<Document_Migration_Log__c>();
            List<Document_Checklist__c> lstDocChkLst = [SELECT Id, Loan_Applications__c, Loan_Contact__c, Loan_Contact__r.Customer__r.Customer_ID__c, Document_Master__c, Document_Type__c, Customer_Type__c, Loan_Contact__r.Applicant_Type__c From Document_Checklist__c Where Loan_Applications__c =: objLA.Id];
            System.debug('Debug Log for lstDocChkLst'+lstDocChkLst.size());
            if(!lstDocChkLst.isEmpty()) {
                for(Document_Checklist__c objDC : lstDocChkLst) {
                    setAllDocuments.add(objDC.Id);
                }
            }
            System.debug('Debug Log for setAllDocuments'+setAllDocuments.size());
            
            if(!setAllDocuments.isEmpty()) {
                lstContentDocDocChecklist = [select id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where LinkedEntityId IN: setAllDocuments];
                System.debug('Debug Log for lstContentDocDocChecklist'+lstContentDocDocChecklist.size());
                
                for(ContentDocumentLink objCDL : lstContentDocDocChecklist) { 
                    setContentDocIdsDocChkLst.add(objCDL.ContentDocumentId); 
                    mapConDocLnktoDocChkLst1.put(objCDL.ContentDocumentId, objCDL.LinkedEntityId);
                }
                
                lstContentVersionDocChkLst = [SELECT Id, Title, CreatedDate, ContentSize, ContentDocumentId, FileExtension FROM ContentVersion WHERE ContentDocumentId IN :setContentDocIdsDocChkLst AND IsLatest=true];
                System.debug('Debug Log for lstContentVersionDocChkLst'+lstContentVersionDocChkLst.size());
                
                if(!lstContentVersionDocChkLst.isEmpty()) {
                    for(ContentVersion objContentVersion : lstContentVersionDocChkLst) {
                        Document_Migration_Log__c objDocumentMigrationLog = new Document_Migration_Log__c();
                        if(!mapConDocLnktoDocChkLst1.isEmpty() && mapConDocLnktoDocChkLst1.containsKey(objContentVersion.ContentDocumentId) && String.isNotBlank(mapConDocLnktoDocChkLst1.get(objContentVersion.ContentDocumentId))) {
                            objDocumentMigrationLog.Document_Checklist__c = mapConDocLnktoDocChkLst1.get(objContentVersion.ContentDocumentId);
                        }
                        objDocumentMigrationLog.Loan_Application__c = objLA.Id;
                        objDocumentMigrationLog.File_Name_If_Attachment__c = objContentVersion.Title;
                        objDocumentMigrationLog.Document_Id__c = objContentVersion.ContentDocumentId;
                        objDocumentMigrationLog.Size_of_the_Document__c = String.valueOf(objContentVersion.ContentSize);
                        lstDocumentMigrationLog.add(objDocumentMigrationLog);
                    }
                }
            }
            
            lstContentDocLinks = [select id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where LinkedEntityId =: objLA.Id];
            System.debug('Debug Log for lstContentDocLinks'+lstContentDocLinks.size());
            
            
            
            for(ContentDocumentLink objCDL : lstContentDocLinks) { 
                setContentDocumentLinkIds.add(objCDL.ContentDocumentId);
                mapConDocIdtoConDocLink.put(objCDL.ContentDocumentId, objCDL.Id);
                mapConDocLnktoDocChkLst.put(objCDL.ContentDocumentId, objCDL.LinkedEntityId);
            }
            System.debug('Debug Log for mapConDocIdtoConDocLink'+mapConDocIdtoConDocLink.size());
            System.debug('Debug Log for mapConDocIdtoConDocLink');
            for(Id keyId : mapConDocIdtoConDocLink.keySet()) {
                System.debug('Key'+keyId+' Value'+mapConDocIdtoConDocLink.get(keyId));
            }
            System.debug('Debug Log for mapConDocLnktoDocChkLst');
            for(Id keyId : mapConDocLnktoDocChkLst.keySet()) {
                System.debug('Key'+keyId+' Value'+mapConDocLnktoDocChkLst.get(keyId));
            }
            
            lstContentVersion = [SELECT Id, Title, CreatedDate, ContentDocumentId, ContentSize, FileExtension FROM ContentVersion WHERE ContentDocumentId IN :setContentDocumentLinkIds AND IsLatest=true];
            System.debug('Debug Log for lstContentVersion'+lstContentVersion.size());
            List<ContentVersion> lstContentDocsApplicant = new List<ContentVersion>();
            List<ContentVersion> lstContentDocsCoApplicants = new List<ContentVersion>();
            List<ContentVersion> lstContentDocsApplication = new List<ContentVersion>();
            if(!lstContentVersion.isEmpty()) {
                for(ContentVersion objContentVersion : lstContentVersion) {
                    if(mapConDocLnktoDocChkLst.containsKey(objContentVersion.ContentDocumentId) && String.isNotBlank(mapConDocLnktoDocChkLst.get(objContentVersion.ContentDocumentId))) {
                        
                        Document_Migration_Log__c objDocumentMigrationLog = new Document_Migration_Log__c();
                        //objDocumentMigrationLog.Document_Checklist__c = objDC.Id;
                        objDocumentMigrationLog.Loan_Application__c = objLA.Id;
                        objDocumentMigrationLog.File_Name_If_Attachment__c = objContentVersion.Title;
                        objDocumentMigrationLog.Document_Id__c = objContentVersion.ContentDocumentId;
                        objDocumentMigrationLog.Size_of_the_Document__c = String.valueOf(objContentVersion.ContentSize);
                        lstDocumentMigrationLog.add(objDocumentMigrationLog);
                    }
                }
            }
            
            lstAttachment = [SELECT Id, Name, Body, BodyLength, CreatedDate, Description, ParentId FROM Attachment Where ParentId =:objLA.Id];
            System.debug('Debug Log for lstAttachment'+lstAttachment);
            System.debug('Debug Log for lstAttachment'+lstAttachment.size());
            if(!lstAttachment.isEmpty()) {
                for(Attachment objAttachment : lstAttachment) {
                    Document_Migration_Log__c objDocumentMigrationLog = new Document_Migration_Log__c();
                    objDocumentMigrationLog.Loan_Application__c =objLA.Id;
                    objDocumentMigrationLog.File_Name_If_Attachment__c = objAttachment.Name;
                    objDocumentMigrationLog.Document_Id__c = objAttachment.Id;
                    objDocumentMigrationLog.Size_of_the_Document__c = String.valueOf(objAttachment.BodyLength);
                    lstDocumentMigrationLog.add(objDocumentMigrationLog);
                }
            }
            System.debug('Debug Log for lstDocumentMigrationLog'+lstDocumentMigrationLog.size());
            if(!lstDocumentMigrationLog.isEmpty()) {
                Database.SaveResult[] lstDSRInsertDML = Database.insert(lstDocumentMigrationLog, false);
                for(Database.SaveResult objDSR : lstDSRInsertDML) {
                  if(!objDSR.isSuccess()){//only look at failures, or NOT Successes
                    System.debug(objDSR.getErrors());
                    
                  }
                  else {
                    System.debug('Debug Log for saved Record'+objDSR.getId());
                  }
                }
            }
            
    }
}