/************************************************************************************
Test Class Name     : HunterCorporateResponseBody
Version             : 1.0
Created Date        : 18th Dec 2020
Function            : Test class for HunterCorporateResponseBody
Author              : Ashwini Gulbhile (Deloitte India)
Modification Log    :
* Developer                 Date                   Description
* ----------------------------------------------------------------------------                 
*************************************************************************************/
@isTest
public class HunterCorporateResponseBodyTest {
    
    static testMethod void testParse() {
        String json='{'+
            '"path": "/online-match-sme",'+
            '"statusCode": 200,'+
            '"payload": {'+
            '"resultBlockObject": {'+
            '"matchSummary": {'+
            '"matches": "1",'+
            '"totalMatchScore": {'+
            '"totalScore": "990"'+
            '},'+
            '"rules": {'+
            '"totalRuleCount": 11,'+
            '"rule": ['+
            '{'+
            '"ruleCount": 11,'+
            '"ruleID": {'+
            '"ruleID": "HFL_NC_SME"'+
            '},'+
            '"score": {'+
            '"score": 90'+
            '}'+
            '}'+
            ']'+
            '},'+
            '"matchSchemes": {'+
            '"schemeCount": 1,'+
            '"scheme": {'+
            '"schemeID": {'+
            '"schemeId": 362'+
            '},'+
            '"score": {'+
            '"score": 990'+
            '}'+
            '}'+
            '}'+
            '},'+
            '"errorWarnings": {'+
            '"warnings": {'+
            '"warningCount": 1,'+
            '"warning": {'+
            '"number": {'+
            '"number": 102009'+
            '},'+
            '"message": {'+
            '"message": "Match schemes not used"'+
            '},'+
            '"values": {'+
            '"value": ['+
            '{'+
            '"value": 28'+
            '},'+
            '{'+
            '"value": 234'+
            '},'+
            '{'+
            '"value": 351'+
            '},'+
            '{' +
            '"value": 352'+
            '},'+
            '{'+
            '"value": 353'+
            '},'+
            '{'+
            '"value": 354'+
            '},'+
            '{'+
            '"value": 355'+
            '},'+
            '{'+
            '"value": 356'+
            '},'+
            '{'+
            '"value": 357'+
            '},'+
            '{'+
            '"value": 358'+
            '},'+
            '{'+
            '"value": 361'+
            '},'+
            '{'+
            '"value": 363'+
            '}'+
            ']'+
            '}'+
            '}'+
            '}'+
            '}'+
            '}'+
            '},'+
            '"timestamp": "2020-12-17 22:16:55.351",'+
            '"acknowledgementId": "5fdb8b7b09236cbd688ad17f"'+
            '}'+
            '}';
        HunterCorporateResponseBody obj = HunterCorporateResponseBody.parse(json);
        System.assert(obj != null);
}
    
}