@isTest
public class TestLegalReportExtension{
    public static testMethod void LegalReportExtension(){
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Name='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567890',RecordTypeId=RecordTypeIdAccount);
        insert acc;
        
        Account custObj = new Account();
        custObj.Name = 'Test Customer';
        custObj.Phone = '9999999998';
        database.insert(custObj);
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        database.insert(schemeObj);
        
        Id RecordTypeIdLA = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Loan Disbursal').getRecordTypeId();
         
        Loan_Application__c loanAppObj = new Loan_Application__c();
        //loanAppObj.RecordTypeId = RecordTypeIdLA;
        loanAppObj.customer__c = custObj.id;
        //loanAppObj.Loan_Application_Number__c = 'LA00001';
        loanAppObj.Loan_Purpose__c = '11';
        loanAppObj.scheme__c = schemeObj.id;
        loanAppObj.Transaction_type__c = 'PB';
        loanAppObj.Requested_Amount__c = 50;
        loanAppObj.StageName__c = 'Customer Onboarding';
        loanAppObj.sub_stage__c = 'Application Initiation';
        Database.insert(loanAppObj);
        
        System.debug('LA Stage=='+[SELECT name,stagename__c,sub_stage__c from Loan_application__c WHERE Id=:loanAppObj.Id]);
        
        loanAppObj.RecordTypeId = RecordTypeIdLA;
        loanAppObj.StageName__c = 'Loan Disbursal';
        loanAppObj.Sub_Stage__c = 'Scan Maker';
        Update loanAppObj;
        
        Third_Party_Verification__c tpvObj = new Third_Party_Verification__c();
        tpvObj.Loan_Application__c = loanAppObj.id;
        tpvObj.Status__c = 'New';
        Database.insert(tpvObj);    
        
        //system.assertNotEquals(null,tpvObj.id);   
        
        ApexPages.StandardController sc = new ApexPages.StandardController(tpvObj);
        LegalReportExtension testAccPlan = new LegalReportExtension(sc);
        
        testAccPlan.getOL();
        
    }
}