public class GroupExposureResponseBody {

    public String status;   //S
    public String errorMsg; //
    public cls_response[] response;
    class cls_response {
        public String customerCIFNo;    //123
        public String loanAgreementNo;  //3167578
        
        public String product;  //Loan Against Property
        public String exposureSanctionedButUndisbursed; //999999
        public String balancePOS;   //2082173
        public String customerGroupID;  //1
        public String customerID;   //3284832
        public String applicantName;    //JAYDEEP BIJLANI
        public String applicantType;    //A
        public String bucket;   //12
        public String dpd;  //345
        public String npastage; //SUBSTDA
        public String primaryApplicantName; //
        public String applicationID;
        
        public cls_response() {
            /*customerCIFNo = '';   //123
            loanAgreementNo = '';   //3167578
            applicationID = '';
            product = '';   //Loan Against Property
            exposureSanctionedButUndisbursed = '';  //999999
            balancePOS = '';    //2082173
            customerGroupID = '';   //1
            customerID = '';    //3284832
            applicantName = ''; //JAYDEEP BIJLANI
            applicantType = ''; //A
            bucket = '';    //12
            dpd = '';   //345
            npastage = '';  //SUBSTDA
            primaryApplicantName =  ''; //
            applicationID = '';*/
            
            
        }
    }
    public GroupExposureResponseBody() {
        
    }
}