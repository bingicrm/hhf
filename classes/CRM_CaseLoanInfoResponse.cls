public class CRM_CaseLoanInfoResponse {

	public class LoanDetailBean {
		public String assetCost;
		public String agreementId;
		public String agreementNumber;
		public String product;
		public String schemeDescription;
		public String loanStatus;
		public String agreementType;
		public String branchId;
		public String branchName;
		public String zone;
		public String state;
		public String dsa;
		public String dme;
		public String subDealer;
		public String netTenure;
		public String disbursedAmount;
		public String emiEndDate;
		public String netMaturityDate;
		public String cancellationDate;
		public String closureDate;
		public String marginMoneyRate;
		public String marginMoney;
		public String loanAmount;
		public String tenure;
		public String repayFrequency;
		public String noOfInstallment;
		public String rest;
		public String dueDay;
		public String installmentPlan;
		public String installmentMode;
		public String noOfAdvanceEmi;
		public String disbursalType;
		public String noOfDisbursal;
		public String disbursalTo;
		public String disbursalAmount;
		public String inflowCustomer;
		public String npaStage;
		public String dpd;
		public String bucket;
		public String dpdString;
	}

	public class CustomerPersonalDetailBean {
		public String customerId;
		public String applicantType;
		public String relation;
		public String firstName;
		public String middleName;
		public String lastName;
		public String cifNo;
		public String title;
		public String salutation;
		public String dateOfBirth;
	}

	public class LoanDetailParameterBean {
		public String rateEmiFlag;
		public String loanEmi;
		public String interestType;
		public String interestRate;
		public String businessIrr;
		public Object addOnRate;
		public String fixedFor;
		public String rateTypeFloatingFlag;
		public String floatingFrequency;
		public String effectiveInterestStartDate;
		public String firstEmiDue;
		public String firstEmiDate;
		public String interestTax;
		public String interestTaxRate;
		public String loanPlrType;
		public String loanPlrRate;
		public String spreadId;
		public String tdsRate;
		public String capitalizeInterest;
		public String interestRoundOff;
		public String interestRoundPrecision;
		public String installmentRoundMethod;
		public String installmentRoundPrecision;
		public String fileNo;
		public String employee;
		public String employer;
		public String employeeBranch;
		public String employeeCode;
		public String flatSpread;
		public String appLoanLimit;
		public String advanceEmiAmount;
		public String totalDownPayment;
		public String prodCategory;
	}

	public LoanDetailBean loanDetailBean;
	public LoanDetailParameterBean loanDetailParameterBean;
	public List<MonotoriumGraceDetailBean> monotoriumGraceDetailBean;
	public List<ChargesBean> chargesBean;
	public Object collateralBean;
	public Object assetDetailsBean;
	public List<InstrumentDetailBean> instrumentDetailBean;
	public List<SplitDisbursalBean> splitDisbursalBean;
	public List<AdditionalInformationBean> additionalInformationBean;
	public List<PropertyDetailsBean> propertyDetailsBean;
	public CustomerPersonalDetailBean customerPersonalDetailBean;
	public List<AddressDetailsBean> addressDetailsBean;
	public List<GuarantorCoapplicantDetailsBean> guarantorCoapplicantDetailsBean;
	public Object crossCollateralDetailsBean;
	public Object errorMsg;

	public class AdditionalInformationBean {
		public String previousDayDpd;
		public String beginningMonthDpd;
		public String previousDayBucket;
		public String beginningMonthBucket;
		public String previousDayNpaStageid;
		public String beginningMonthNpaStageId;
		public String heroBucket;
		public String beginningMonthHeroBucket;
		public String legalFlag;
		public String legalInitiatedBy;
		public String legalReason;
		public String gstState;
		public String nocIssuedFlag;
		public String nocNo;
		public Object nocIssuanceDate;
		public String nocIssuanceMode;
		public String nocHoldFlag;
		public String nocHoldReason;
		public String nocLiftReason;
		public String soaStopFlag;
		public String soaStopReason;
		public String foreclosureStopFlag;
		public String foreclosureStopReason;
		public String repaymentStopFlag;
		public String repaymentStopReason;
		public Object itletterStopFlag;
		public Object itletterStopReason;
	}

	public class AddressDetailsBean {
		public String addressType;
		public String propertyStatus;
		public String addressLine1;
		public String addressLine2;
		public String addressLine3;
		public String village;
		public String country;
		public String state;
		public String district;
		public String city;
		public String pinCode;
		public String poBox;
		public String areaCode;
		public String landlinePhone;
		public String extension;
		public String directLine;
		public String mobile;
		public String emailId;
		public String fax;
		public String pager;
		public String landmark;
		public String mailingAddress;
		public String inactive;
	}

	public class MonotoriumGraceDetailBean {
		public String moratoriumOrGraceApplicable;
		public String moratoriumOrGracePeriod;
		public String impactOnTenure;
	}

	public class GuarantorCoapplicantDetailsBean {
		public String applicantType;
		public String relation;
		public String customerId;
		public String cifNo;
		public String firstName;
		public String middleName;
		public String lastName;
	}

	public class SplitDisbursalBean {
		public String disbursalTo;
		public String amount;
		public String disbursalDate;
		public String businessPartnerName;
		public String paymentMode;
		public String ftMode;
		public String chequeDate;
		public String chequeNumber;
		public String effectiveDate;
		public String cashOrBankAccount;
		public String payableAt;
		public String accountNumber;
		public String inFavourOf;
		public String remarks;
	}

	public class ChargesBean {
		public String chargeInterest;
		public String addInterestInSchedule;
		public String chargeDescription;
		public String bpType;
		public String chargeRate;
		public String chargeAmount;
		public String effectiveChargeAmount;
		public String creditPeriod;
		public String chargeCode;
		public String txnType;
		public String chargeId;
		public String bpid;
		public String receivableOrPayable;
		public String taxApplicable;
		public String chargeType;
	}

	public class InstrumentDetailBean {
		public String instrumentType;
		public String micr;
		public String city;
		public String bank;
		public String agreemntId;
		public String customerAccountNo;
		public String ifscCode;
		public String pdcByApplicantOrCoapplicant;
		public String pdcBy;
		public String destBankAccountType;
		public String destAccountHolder;
		public String accValidationFlag;
		public String accValidationReason;
		public String bankBranch;
		public String umrReferenceNumber;
		public Object activeOrInavtiveFlag;
	}

	public class PropertyDetailsBean {
		public String propertyTypeNew;
		public String propertyType;
		public String propertyDescription;
		public String builderName;
		public String propertyOwner1;
		public String propertyOwner2;
		public String builderClassificationType;
		public String projectName;
		public String typeOfPurchase;
		public String propertyInsurance;
		public String totalAmountFinanced;
		public Object personalInsurance;
		public String ruralOrUrbanClassification;
		public String propertyUsage;
		public String propertyAddressLine1;
		public String propertyAddressLine2;
		public String propertyArea;
		public String propertyAreaUnit;
		public String addressLine1;
		public String addressLine2;
		public String addressLine3;
		public String city;
		public String stateid;
		public String pincode;
		public String country;
		public String nearestLandmark;
	}

	
	public static CRM_CaseLoanInfoResponse parse(String json) {
		return (CRM_CaseLoanInfoResponse) System.JSON.deserialize(json, CRM_CaseLoanInfoResponse.class);
	}
}