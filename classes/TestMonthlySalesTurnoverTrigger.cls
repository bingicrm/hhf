@isTest
public class TestMonthlySalesTurnoverTrigger{
    public static testMethod void insertMST(){
        Monthly_Sales_Turnover__c mst = new Monthly_Sales_Turnover__c();
        mst.Month__c = 'December';
        mst.Turnover__c = 123.00;
        mst.Year__c = '2019';
        Database.insert(mst);
    }
}