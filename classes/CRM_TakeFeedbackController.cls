public without sharing class CRM_TakeFeedbackController {

    public String caseId  {get;set;}
    public list<Case> caseResponse {get;set;}
    public Id AsstRespId {get;set;}
    public boolean continueFeedBack {get;set;}
    public Boolean checkException{get; set;}

    public CRM_TakeFeedbackController(){
        
        caseId=Apexpages.currentPage().getParameters().get('id');
        System.debug('Case Id' +caseId);
 
        if(caseId!=null){ 
            System.debug('1');
            caseResponse = [select id, ClosedDate, Status,CaseNumber,CRM_Feedback_Summited__c from Case where id =: caseId and Status='Closed'];
            System.debug('2');
            
            if(caseResponse != null && caseResponse.size() >0 && caseResponse[0].ClosedDate >= System.now()-7  && caseResponse[0].CRM_Feedback_Summited__c == false){
                System.debug('3');
                continueFeedBack = true;
                checkException=false;
            }
            else if(caseResponse != null && caseResponse.size() >0 && caseResponse[0].CRM_Feedback_Summited__c == true){
                continueFeedBack = false;
                checkException=true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Feedback already summited'));
            }
            else{
                System.debug('4');
                continueFeedBack = false;
                checkException=true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Customer Feedback Link is Expired'));
            }
        }
        else{
            System.debug('5');
            continueFeedBack = false;
            checkException=true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Customer Feedback Link is Expired'));
        }
        //System.debug('Assment Responce Id' +AsstRespId);
    }
    

    public PageReference dosubmit(){
        
        PageReference pr = null;
        // create case, etc.
        //AsstRespId = CRM_Assesment.getCaseAssessmentRespId(null, caseId);
        //Resolution for PMD Issue : ApexXSSFromURLParam 
        //AsstRespId = CRM_Assesment.getCaseAssessmentRespId(null, String.escapeSingleQuotes(caseId.escapeHtml4()));
        //System.debug('Assment Responce Id' +AsstRespId); 
        //CRM_GenericConfiguration__c objGenericConfig = CRM_GenericConfiguration__c.getValues('Customer Preview Site URL');
        
        //if(objGenericConfig != null &&  objGenericConfig.Value__c != null){
          //  pr = new PageReference(objGenericConfig.Value__c+AsstRespId); 
           // pr.setRedirect(true);
            
        //}
        String sitedomain = Label.CRM_Survey_Feedback_URL;
        pr = new PageReference(sitedomain +caseId);
        return pr;  
    }

}