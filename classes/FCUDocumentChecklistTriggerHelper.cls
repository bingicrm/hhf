public class FCUDocumentChecklistTriggerHelper {
	
    public static void updateDocumentChecklistFields(List<FCU_Document_Checklist__c> lstFCUDC){
        List<Document_Checklist__c> lstDCToUpdate = new List<Document_Checklist__c>();
        
        for(FCU_Document_Checklist__c fdc: lstFCUDC){
            Document_Checklist__c dc = new Document_Checklist__c();
            dc.Id = fdc.Document_Checklist__c;
            if(fdc.FCU_Decision__c != null){
                dc.Decision__c = fdc.FCU_Decision__c;
            }
            
            if(fdc.Sampled__c != null){
                dc.Sampled_p__c = fdc.Sampled__c;
            }
            
            if(fdc.Screened__c != null){
                dc.Screened_p__c = fdc.Screened__c;
            }
            
            lstDCToUpdate.add(dc);
        }
        
        if(lstDCToUpdate.size() > 0){
            update lstDCToUpdate;
        }
    }
    
    public static void setThirdPartyFCUReportStatus(Set<Id> tpvIds){
        List<FCU_Document_Checklist__c> lstFCUDC = [SELECT Id,Name,FCU_Decision__c,Third_Party_Verification__c from FCU_Document_Checklist__c WHERE Third_Party_Verification__c =: tpvIds];
        List<Third_Party_Verification__c> lstTPVToUpdate = new List<Third_Party_Verification__c>();
        
        if(lstFCUDC.size() > 0){
            for(String tpvId: tpvIds){
                Integer positive = 0;
                Integer negative = 0;
                Integer CNV = 0;
                Integer RTC = 0;
                Integer decisionNull = 0;
                for(FCU_Document_Checklist__c fdc: lstFCUDC){
                    if(fdc.Third_Party_Verification__c == tpvId){
                        if(fdc.FCU_Decision__c == null){
                            decisionNull++;
                            //break;
                        }
                        else if(fdc.FCU_Decision__c == Constants.NEGATIVE){
                            negative++;
                        }
                        else if(fdc.FCU_Decision__c == Constants.CNV){
                            CNV++;
                        }
                        else if(fdc.FCU_Decision__c == Constants.RTC){
                            RTC++;
                        }
                        else{
                            positive++;
                        }
                    }
                }
                
                /*if(decisionNull > 0){
                    continue;
                }*/
                
                Third_Party_Verification__c objTPV = new Third_Party_Verification__c();
                objTPV.Id = tpvId;
                //objTPV.Status__c = Constants.COMPLETED;
                
                /*if(decisionNull > 0){
                    objTPV.Report_Status__c = '';
                }*/
                if(negative > 0){
                    objTPV.Report_Status__c = Constants.NEGATIVE;
                }
                else if(CNV > 0){
                    objTPV.Report_Status__c = Constants.CNV;
                }
                else if(RTC > 0){
                    objTPV.Report_Status__c = Constants.RTC;
                }
                else if(positive > 0){
                    objTPV.Report_Status__c = Constants.POSITIVE;
                }
                else{
                    objTPV.Report_Status__c = '';
                }
                lstTPVToUpdate.add(objTPV);
            }
        }
        
        if(lstTPVToUpdate.size() > 0){
            update lstTPVToUpdate;
        }
    }
}