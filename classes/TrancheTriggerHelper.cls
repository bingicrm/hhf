/*=====================================================================
* Deloitte India
* Name:TrancheTriggerHelper
* Description: This class is the helper class for the Tranche object Trigger.
* Created Date: [12/Oct/2018]
* Created By:Asit Porwal (Deloitte India) 
* Date Modified                Modified By                  Description of the update
* [24/12/2019]                 [Shobhit Saxena(Deloitte)]   [Update Made in createTrancheAchievement method]
=====================================================================*/

public class TrancheTriggerHelper {
    public static void insertUpdateDocs(List<Tranche__c> newList, Map<Id,Tranche__c> oldMap){
        system.debug('AAAA');
        if(Trigger.isUpdate && Trigger.isAfter){
            Map<Id, Tranche__c> mapTranche = new Map<Id, Tranche__c>();
            for(Tranche__c t : newList){                
                if(t.Request_Type__c != oldMap.get(t.id).Request_Type__c){
                    mapTranche.put(t.Id,t);                    
                }
            }
            if(!mapTranche.isEmpty()){
                deleteDocs(mapTranche.keySet());
                for(Tranche__c t : mapTranche.values()){
                    attachDocs(t);
                }
            }
        }
        
        else if(Trigger.isInsert  && Trigger.isAfter){
            for(Tranche__c t : newList){
                attachDocs(t);    
            }            
        }
        
        else if(Trigger.isBefore && Trigger.isDelete){
            Set<Id> setTranId = new Set<Id>();
            for(Tranche__c t : oldMap.values()){
                setTranId.add(t.id);
            }
            List<Document_Checklist__c> lstDoc = [SELECT Id,Status__c FROM Document_Checklist__c WHERE Tranche__c IN : setTranId];
            List<Third_Party_Verification__c> lstTPV = [SELECT Id FROM Third_Party_Verification__c WHERE Tranche__c IN : setTranId];
            List<Disbursement__c> lstDisb = [SELECT Id FROM Disbursement__c WHERE Tranche__c IN : setTranId];
            if(!(lstDoc.isEmpty())){
                delete lstDoc;                
            }
            if(!(lstTPV.isEmpty())){
                delete lstTPV;                
            }
            if(!(lstDisb.isEmpty())){
                delete lstDisb;
            }
        }    
    }
    
    private static void attachDocs(Tranche__c t){        
        List<Loan_Application_Document_Checklist__c> lstDocMaster = getDocument(t.Request_Type__c).values();
        List<Document_Checklist__c> insertDocChk = new List<Document_Checklist__c>();        
        if(t.Request_Type__c == Constants.TRANCHEDISBURSAL){ 
            if(!lstDocMaster.isEmpty()){
                for(Loan_Application_Document_Checklist__c dm : lstDocMaster) {
                    Document_Checklist__c doc = new Document_Checklist__c();
                    doc.Loan_Applications__c = t.Loan_Application__c;
                    doc.Tranche__c = t.Id;
                    doc.Document_Master__c = dm.Document_Master__c;
                    doc.Document_Type__c = dm.Document_Type__c;
                    //doc.OTC_PDD_check__c = dm.Document_Master__r.OTC__c;  
                    doc.Loan_Engine_Mandatory__c = dm.Loan_Engine_Mandatory__c;
                    insertDocChk.add(doc);                       
                }                        
            }
        }
        try{
            insert insertDocChk;
        }
        catch(DmlException e){
            System.debug('DML Exception : ' + e.getDmlMessage(0));
        }
        
    }
    
    private static void deleteDocs(Set<Id> setTranId){
        List<Document_Checklist__c> lstDoc = new List<Document_Checklist__c>();
        List<Document_Checklist__c> docToDel = new List<Document_Checklist__c>();
        
        if(!setTranId.isEmpty() && !setTranId.contains(null)){
            lstDoc = [SELECT Id,Status__c FROM Document_Checklist__c WHERE Tranche__c IN : setTranId];
        }
        if(!lstDoc.isEmpty()){
            for(Document_Checklist__c dc : lstDoc){
                if(dc.Status__c == Constants.DOC_STATUS_PENDING){
                    docToDel.add(dc);
                }
            }            
        }
        if(!docToDel.isEmpty()){
            delete docToDel;    
        }        
    }
    
    public static void deleteTranche(Map<Id, Tranche__c> oldMap){
        List<Profile> lstProfile = [SELECT Name FROM Profile WHERE Id =: UserInfo.getProfileId()];
		String TrancheRecordTypeId = Schema.SObjectType.Tranche__c.getRecordTypeInfosByName().get(constants.Tranche_Sales_Coordinator_Rec_Type).getRecordTypeId();  
        for(Tranche__c t : oldMap.values()){
            if(!lstProfile.isEmpty() && lstProfile.size() == 1 && lstProfile[0].Name == 'Sales Coordinator' && t.Status__c != Constants.STATUS_DRAFT){
                t.addError('Cannot delete Tranche as the Tranche Status is Not Draft');
            }    
			//Below If block added by Abhilekh on 6th August 2019 for TIL-1272
            if(!lstProfile.isEmpty() && lstProfile.size() == 1 && lstProfile[0].Name != 'System Administrator' && t.RecordTypeId != TrancheRecordTypeId){
                t.addError('Tranche deletetion is not allowed at this stage.');
            } 
        }
        
    }
    public static void updateStatusAndPendingAmt(List<Tranche__c> newList){
        if(Trigger.isInsert && Trigger.isBefore){
            Set<Id> setLoanId = new Set<Id>();            
            for(Tranche__c t : newList){
                setLoanId.add(t.Loan_Application__c);
            }
            Map<Id,Loan_Application__c> mapLoanApp = new Map<Id,Loan_Application__c>([SELECT Id, Assigned_Sales_User__c,Disbursed_Amount__c, Repayment_Start_Date__c FROM Loan_Application__c WHERE Id IN : setLoanId]);
            
            for(Tranche__c t : newList){               
                if(mapLoanApp != null && t.Loan_Application__c != null && mapLoanApp.containsKey(t.Loan_Application__c)){
                    Loan_Application__c la = mapLoanApp.get(t.Loan_Application__c);
                    if(la.Assigned_Sales_User__c != null){
                        t.Status__c = Constants.STATUS_DRAFT;                              
                    }
                    if(la.Disbursed_Amount__c != null){
                        t.Pending_Amount2__c = la.Disbursed_Amount__c;
                        t.Repayment_Start_Date__c = la.Repayment_Start_Date__c;
                    }
                }
            }            
        }        
    }    
    
    private static Map<Id,Loan_Application_Document_Checklist__c> getDocument(String reqType){                
        Map<Id,Loan_Application_Document_Checklist__c> mapLADC = new Map<Id,Loan_Application_Document_Checklist__c>([SELECT Id, Document_Master__c, Document_Master__r.OTC__c, Document_Type__c, Name, Loan_Engine_Mandatory__c, Stage__c, Sub_Stage__c
                                                                                                                     FROM Loan_Application_Document_Checklist__c
                                                                                                                     WHERE Document_Type__c IN
                                                                                                                     (SELECT Id FROM Document_Type__c WHERE Document_Type_Id__c =: reqType)]);
        
        return mapLADC;
    }
    
    public static void propertyVerifications(Id laId, Id tranId){
        
        List<Third_Party_Verification__c> lstTPV = [SELECT Id FROM Third_Party_Verification__c WHERE Loan_Application__c =:laId AND Tranche__c =:tranId];
        system.debug('List of TPV : ' + lstTPV);
        if(lstTPV.isEmpty()){
            Decimal value = 1;
            Loan_Application__c l = new Loan_Application__c();
            List<Loan_Application__c> lstL = [SELECT Id, Name, StageName__c, Sub_Stage__c, Final_Property_Valuation__c, Transaction_type__c, Branch_Lookup__r.Name, Branch_Lookup__c, Property_Identified__c, Credit_Manager_User__c, Assigned_Sales_User__c, Verification_Count_All__c, Scheme__c, Scheme__r.Product_Code__c, Requested_Amount__c, Existing_Loan_Application_Number__c FROM Loan_Application__c
                                     WHERE Id = :laId LIMIT 1];
            
            if(!lstL.isEmpty() && lstL.size() == 1 && lstL[0] != null){
                l = lstL[0];
                system.debug('Queried Loan Application : ' + l);
                
                
                List<Property__c> propertyLst = [SELECT Id, City__c, State__c, Country__c, Pincode_LP__c, Pincode_LP__r.Servicing_Branch__c, Pincode__c, Pincode_LP__r.Name, Loan_Application__c, Property_Type__c FROM Property__c WHERE Loan_Application__c =: l.Id];
                system.debug('Queried Property List : ' + propertyLst);
                
                Map<Id, List<Property__c>> loanProp = new Map<Id, List<Property__c>>();
                for(Property__c p: propertyLst){
                    if (!loanProp.containsKey(p.Loan_Application__c)) {
                        loanProp.put(p.Loan_Application__c, new List<Property__c>{p});                
                    }else{
                        loanProp.get(p.Loan_Application__c).add(p);
                    }
                }
                
                List<User_Branch_Mapping__c> ubCreds = [Select Id, User__r.Role_Assigned_Date__c,user__r.Profile.Name,User__r.UserRole.Name, User__c, Servicing_Branch__r.Pincode_Name__c from User_Branch_Mapping__c where User__r.isActive = TRUE and User__r.Profile.Name =: 'Credit Team' and User__r.UserRole.Name LIKE 'Credit Manager%' and Servicing_Branch__r.Name =: l.Branch_Lookup__r.Name ORDER BY User__r.Role_Assigned_Date__c ASC];
                
                List<User_Branch_Mapping__c> ubTechL = [Select Id, User__r.Role_Assigned_Date__c,user__r.Profile.Name,User__r.UserRole.Name, User__c, Servicing_Branch__c from User_Branch_Mapping__c where User__r.isActive = TRUE and User__r.Profile.Name =: Constants.Technical_Profile ORDER BY User__r.Role_Assigned_Date__c ASC];
                List<Technical_Evaluation__c> techL = [Select Id, Scheme_Lookup__c, Transaction_Type__c, Type_of_Property__c, Threshold_Amount__c, Evaluation_Count__c from Technical_Evaluation__c];
                Id techRec = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get(Constants.TECHNICAL).getRecordTypeId();
                List<Third_Party_Verification__c> tprList = new List<Third_Party_Verification__c>();
                if(loanProp.size()>0){
                    for(Property__c prop: loanProp.get(l.Id)){
                        if(ubTechL.size() >= value){
                            for(integer i=0; i<value; i++){
                                Third_Party_Verification__c tprTechnical = new Third_Party_Verification__c();
                                Id OwnerIdTechnical;
                                Datetime ladTechnical = System.now();
                                Datetime radTechnical;
                                
                                tprTechnical.recordtypeid = techRec;
                                tprTechnical.Loan_Application__c = l.Id;
                                tprTechnical.Tranche__c = tranId;
                                tprTechnical.Partner_Community_Login__c = Label.Third_Party_Community_Login;
                                //tprTechnical.Property_Address__c = prop.Street__c+', '+prop.City__c+', '+prop.State__c+', '+prop.Country__c+', '+prop.Pincode__r.Name;
                                tprTechnical.Property__c = prop.Id;
                                tprTechnical.Final_Property_Valuation__c = l.Final_Property_Valuation__c;
                                tprTechnical.Manually_Created_Record__c = FALSE;
                                tprTechnical.LA_Credit_Manager_User__c = l.Credit_Manager_User__c;
                                tprTechnical.LA_Sales_Manager_User__c = l.Assigned_Sales_User__c;
                                if(!ubTechL.isEmpty()){
                                    for(User_Branch_Mapping__c ubt : ubTechL){
                                        if(ubt.Servicing_Branch__c == prop.Pincode_LP__r.Servicing_Branch__c){
                                            tprTechnical.Owner__c = ubt.User__c;
                                            radTechnical = ubt.User__r.Role_Assigned_Date__c;
                                            if(radTechnical == null){
                                                OwnerIdTechnical = ubt.User__c;
                                            }
                                            else if(radTechnical < ladTechnical){
                                                ladTechnical = radTechnical;
                                                OwnerIdTechnical = ubt.User__c;
                                            }
                                        }
                                    }    
                                }
                                tprTechnical.Owner__c = OwnerIdTechnical;
                                if(tprTechnical.Owner__c != null){
                                    tprTechnical.Status__c = Constants.INITIATED;
                                }
                                else{
                                    tprTechnical.Status__c = Constants.NEW_VALUE;
                                }
                                System.debug('----tprTechnicalOwner----'+tprTechnical.Owner__c);
                                if(ubCreds.size()>0){
                                    tprTechnical.Default_Credit_Manager__c = ubCreds[0].User__c;
                                }
                                tprList.add(tprTechnical);
                            }
                            
                        }
                        else if(ubTechL.size() < value){
                            
                            for(integer i=0; i<value; i++){
                                Third_Party_Verification__c tprTechnical = new Third_Party_Verification__c();
                                Id OwnerIdTechnical;
                                Datetime ladTechnical = System.now();
                                Datetime radTechnical;
                                
                                tprTechnical.recordtypeid = techRec;
                                tprTechnical.Loan_Application__c = l.Id;
                                tprTechnical.Tranche__c = tranId;                        
                                tprTechnical.Partner_Community_Login__c = Label.Third_Party_Community_Login;                        
                                tprTechnical.Property__c = prop.Id;
                                tprTechnical.Manually_Created_Record__c = FALSE;
                                tprTechnical.LA_Credit_Manager_User__c = l.Credit_Manager_User__c;
                                tprTechnical.LA_Sales_Manager_User__c = l.Assigned_Sales_User__c;
                                if(ubCreds.size()>0){
                                    tprTechnical.Default_Credit_Manager__c = ubCreds[0].User__c;
                                }
                                if(!ubTechL.isEmpty()){
                                    for(User_Branch_Mapping__c ubt : ubTechL){
                                        if(ubt.Servicing_Branch__c == prop.Pincode_LP__r.Servicing_Branch__c){
                                            tprTechnical.Owner__c = ubt.User__c;
                                            radTechnical = ubt.User__r.Role_Assigned_Date__c;
                                            if(radTechnical == null){
                                                OwnerIdTechnical = ubt.User__c;
                                            }
                                            else if(radTechnical < ladTechnical){
                                                ladTechnical = radTechnical;
                                                OwnerIdTechnical = ubt.User__c;
                                            }
                                        }
                                    }    
                                }
                                tprTechnical.Owner__c = OwnerIdTechnical;
                                if(tprTechnical.Owner__c != null){
                                    tprTechnical.Status__c = Constants.INITIATED;
                                }
                                else{
                                    tprTechnical.Status__c = Constants.NEW_VALUE;
                                }
                                System.debug('----tprTechnicalOwner----'+tprTechnical.Owner__c);
                                tprList.add(tprTechnical);
                            }
                            //msg = 'Technical vendors were less than evaluation count. Please assign owners to remaining records manually.';
                            
                        }
                    }
                }
                system.debug('TPR LIST is found as : ' + tprList);
                insert tprList;
            }
        }
    }
    
    public static void checkDisbursalAmt(List<Tranche__c> newList){
        Set<Id> setTranId = new Set<Id>();
        for(Tranche__c t : newList){
            setTranId.add(t.Loan_Application__c);
        }
        Map<Id, Loan_Application__c> mapLA = new Map<Id,Loan_Application__c>([SELECT Id, Approved_Loan_Amount__c,Disbursed_Amount__c FROM Loan_Application__c WHERE ID IN : setTranId]);
        
        for(Tranche__c t : newList){
            Decimal pendingAmt = 0.0;
            Decimal disbursalAmt = 0.0;
            system.debug('Approved Loan Amount : ' + mapLA.get(t.Loan_Application__c).Approved_Loan_Amount__c+ ' , Disbursed Amount : ' + mapLA.get(t.Loan_Application__c).Disbursed_Amount__c );
            if(mapLA != null && mapLA.containsKey(t.Loan_Application__c)){
                if(mapLA.get(t.Loan_Application__c).Approved_Loan_Amount__c != null){                
                    if(mapLA.get(t.Loan_Application__c).Disbursed_Amount__c != null){
                        pendingAmt = mapLA.get(t.Loan_Application__c).Approved_Loan_Amount__c - mapLA.get(t.Loan_Application__c).Disbursed_Amount__c;
                        system.debug('pendingAmt--->>'+pendingAmt);
                        if(t.Disbursal_Amount__c != null){
                            disbursalAmt = t.Disbursal_Amount__c;
                            system.debug('disbursalAmt---->>'+disbursalAmt);
                            /*if(pendingAmt > 0 && pendingAmt < disbursalAmt ){
                                t.addError('Disbursal Amount cannot be greater than the Pending Amount');
                            }*/
                            if(pendingAmt < 0){
                                t.addError('Please enter the sufficient amount available');
                            }
                        }
                        else{
                            t.addError('Please provide the Disbursal Amount');
                        }
                    }
                }
            }
            
        }        
    }
    
    public static void checkTrancheCreation(List<Tranche__c> newList){
        Set<Id> setLoanId = new Set<Id>();               
        boolean flag = false;
        
        for(Tranche__c t : newList){
            if(t.Loan_Application__c != null){
                setLoanId.add(t.Loan_Application__c);    
            }            
        }
        if(!setLoanId.isEmpty()){            
            Map<Id,Tranche__c> mapTranche = new Map<Id, Tranche__c>([SELECT Id, Loan_Application__c, Tranche_Stage__c, Status__c FROM Tranche__c WHERE Loan_Application__c IN : setLoanId]);
            if(!mapTranche.isEmpty()){
                for(Tranche__c tr : newList){
                    for(Tranche__c t : mapTranche.values()){
                        if(t.Loan_Application__c == tr.Loan_Application__c){
                            if(t.Tranche_Stage__c != Constants.Tranche_Initiation && t.Status__c != Constants.STATUS_DISBURSED /*Cancelled check added by KK for Tranche Cancellation*/ && t.Status__c != Constants.STATUS_CANCELLED){
                                flag = true;
                                break;
                            }
                        }    
                    }
                    if(flag){
                        tr.addError('Please process the existing Tranche before creating a new one.');
                    }
                }    
            }
        }        
    }
    
    public static void updateTrancheStage(List<Tranche__c> newList){
        for(Tranche__c t : newList){
            if(t.Tranche_Stage__c == Constants.Tranche_Initiation || t.Tranche_Stage__c == null){
                t.RecordTypeId = Schema.SObjectType.Tranche__c.getRecordTypeInfosByName().get(constants.Tranche_Sales_Coordinator_Rec_Type).getRecordTypeId();
            }
            else if(t.Tranche_Stage__c == Constants.Tranche_File_Check || t.Tranche_Stage__c == Constants.Tranche_Scan_Checker || t.Tranche_Stage__c == Constants.Tranche_Scan_Maker){
                t.RecordTypeId = Schema.SObjectType.Tranche__c.getRecordTypeInfosByName().get(constants.Tranche_Read_Only_Rec_Type).getRecordTypeId();
            }
            else if(t.Tranche_Stage__c == Constants.Tranche_Credit_Approval){
                t.RecordTypeId = Schema.SObjectType.Tranche__c.getRecordTypeInfosByName().get(constants.Tranche_Credit_Rec_Type).getRecordTypeId();
            }
            else if(t.Tranche_Stage__c == Constants.Tranche_Disbursement_Maker){
                t.RecordTypeId = Schema.SObjectType.Tranche__c.getRecordTypeInfosByName().get(constants.Tranche_Disbursement_Rec_Type).getRecordTypeId();
            }
            else if(t.Tranche_Stage__c == Constants.Tranche_Disbursement_Checker){
                t.RecordTypeId = Schema.SObjectType.Tranche__c.getRecordTypeInfosByName().get(constants.Tranche_Disbursement_Check_Rec_Type).getRecordTypeId();
            }
            else if(t.Tranche_Stage__c == Constants.Tranche_Disbursed){
                t.RecordTypeId = Schema.SObjectType.Tranche__c.getRecordTypeInfosByName().get(constants.Tranche_Disbursed_Rec_Type).getRecordTypeId();
            }
            
        }
    }
    
    public static void changeOnRepayment(List<Tranche__c> newList,Map<Id, Tranche__c> oldMap){
        Date dt = LMSDateUtility.lmsDateValue;
		List<Id> LAId = new List<Id>();//Added By Abhishek for TIL 2417
        for(Tranche__c tr : newList){
            tr.Business_Date__c = (Date)dt;
			LAId.add(tr.Loan_Application__c);//Added By Abhishek for TIL 2417
        }
        for(Tranche__c tra : newList){
            if(tra.Repayment_Start_Date__c != oldMap.get(tra.id).Repayment_Start_Date__c){
                if(tra.Repayment_Start_Date__c < tra.Business_Date__c){
                    tra.addError('Repayment Date cannot be in the past when compared to Business Date.');            
                }
            }
        }
		//Updated By Abhishek for TIL 2417 - START
        List<Loan_Repayment__c> LRList= [SELECT Id, dueDay__c, Loan_Application__c FROM Loan_Repayment__c WHERE Loan_Application__c IN :LAId];
        for(Tranche__c tr: newList){
            for(Loan_Repayment__c LRNObj: LRList){
                if(LRNObj.Loan_Application__c == tr.Loan_Application__c && !String.isBlank(String.valueof(LRNObj)) && !String.isBlank(String.valueof(LRNObj.dueDay__c))){
                    if(tr.Repayment_Start_Date__c.Day() != LRNObj.dueDay__c){
                        tr.addError('Repayment Date can only be according to Loan Repayment Due Day.');
                    }
                }
            }
        }
		//Updated By Abhishek for TIL 2417 - END
    }
    public static void updateAmtChanged(List<Tranche__c> newList, Map<Id, Tranche__c> oldMap){
        for(Tranche__c tr : newList){
            if(tr.Approved_Disbursal_Amount__c != oldMap.get(tr.id).Approved_Disbursal_Amount__c ){
                tr.Amount_Changed__c = true;
            }
        }
    }
    public static void updateSequence(List<Tranche__c> newList){
        Id appId;
        List<Tranche__c> trLst = new List<Tranche__c>();
        for(Tranche__c tr : newList){
            appId = tr.Loan_Application__c;
        }
        trLst = [SELECT Id,Loan_Application__c,Disbursal_Sequence__c FROM Tranche__c WHERE Loan_Application__c =: appId];
        if(trLst.size()>0){
            for(Tranche__c t : newList){
                t.Disbursal_Sequence__c = trLst.size()+2;
            }
            system.debug('===+'+trlst);
            update trlst;
        }
    }

    public static void validationOnRemainingAmt(List<Tranche__c> newList){
        Id appId;
        List<Loan_Application__c> laLst = new List<Loan_Application__c>();
        Integer pendAmt;
        for(Tranche__c tr : newList){
            appId = tr.Loan_Application__c;
        }
        laLst = [SELECT Id, Disbursed_Amount__c, Approved_Loan_Amount__c 
                 FROM Loan_Application__c 
                 WHERE Id =: appId];
        if(newList.size()>0){
            for(Loan_Application__c la : laLst){
                for(Tranche__c t : newList){
                    if(t.Loan_Application__c == la.Id){
                        if(((la.Approved_Loan_Amount__c - la.Disbursed_Amount__c)-t.Disbursal_Amount__c)<=Decimal.valueOf(Label.Tranche_Limit) && ((la.Approved_Loan_Amount__c - la.Disbursed_Amount__c)-t.Disbursal_Amount__c) != 0){
                            t.addError(Label.Tranche_Error);
                        }
                    }
                }
            }
        }
    }
	// [03-06-19] Added by KK for Target Achievement: Code Begins
    public static void createTrancheAchievement(List<Tranche__c> newList){
		Set<Id> setTrancheId = new Set<Id>();
		for(Tranche__c objTranche : newList) {
			setTrancheId.add(objTranche.Id);
		}
		
		if(!setTrancheId.isEmpty()) {
			insertAchievements(setTrancheId);
		}
		
        
    }
	//Added by Shobhit Saxena on 24/12/2019 to lookout for Tranch Issue of Stage Movement post disbursal.
	@future
	public static void insertAchievements(Set<Id> setTrancheIds) {		
	//public static void insertAchievements(List<Achievement__c> lstAchievementtoInsert) {
		if(!setTrancheIds.isEmpty()) {
			List<Tranche__c> lstTrancheNewList = [Select Id, Name, Loan_Application__c, Business_Date__c, Approved_Disbursal_Amount__c From Tranche__c WHERE Id IN: setTrancheIds];
			if(!lstTrancheNewList.isEmpty()) {
				List<Achievement__c> newAchievement = new List<Achievement__c>();
				List<Targets__c> targetList = new List<Targets__c>();
				Set<Id> loanIds = new Set<Id>();
				for(Tranche__c t: lstTrancheNewList){
					loanIds.add(t.Loan_Application__c);         
				}
				List<Loan_Application__c> laList = [Select Id, CreatedById, Sales_Profile__c, Sales_User_Manager_ID__c, Assigned_Sales_User__c, Disbursement_DxROI_Tranche__c,SM_CBM_ID__c from Loan_Application__c where Id =: loanIds];
				targetList = [Select Id, Channel_Partner__c, Month__c, Sales_User__c, Sales_User__r.Name, Sales_User__r.Profile.Name, From__c, To__c from Targets__c];
				for(Tranche__c t: lstTrancheNewList){
					for(Loan_Application__c la: laList){
						if(t.Loan_Application__c == la.Id){
							for(Targets__c tar: targetList){
								if(la.Sales_Profile__c == 'Sales Team' && la.CreatedById == tar.Sales_User__c && String.valueOf(t.Business_Date__c.month()) == tar.Month__c){
									Achievement__c ach = new Achievement__c();
									ach.Disbursal_Amount__c = t.Approved_Disbursal_Amount__c;
									ach.Type__c = 'Tranche';
									ach.Active__c = TRUE;
									ach.Business_Date__c = t.Business_Date__c;
									ach.Loan_Application__c = la.Id;
									ach.Disbursed_Amount_X_ROI__c = la.Disbursement_DxROI_Tranche__c;
									ach.Tranche__c = t.Id;
									ach.Target__c = tar.Id;
									ach.Tranche_F__c = TRUE;
									newAchievement.add(ach);
								}
								else if((la.Sales_Profile__c != 'Sales Team' && ((la.SM_CBM_ID__c != null && la.SM_CBM_ID__c == tar.Sales_User__c) || (la.SM_CBM_ID__c == null && la.Sales_User_Manager_ID__c == tar.Sales_User__c))) && t.Business_Date__c >= tar.From__c &&  t.Business_Date__c <= tar.To__c){
									Achievement__c ach = new Achievement__c();
									ach.Disbursal_Amount__c = t.Approved_Disbursal_Amount__c;
									ach.Type__c = 'Tranche';
									ach.Active__c = TRUE;
									ach.Business_Date__c = t.Business_Date__c;
									ach.Loan_Application__c = la.Id;
									ach.Disbursed_Amount_X_ROI__c = la.Disbursement_DxROI_Tranche__c;
									ach.Tranche__c = t.Id;
									ach.Target__c = tar.Id;
									ach.Tranche_F__c = TRUE;
									newAchievement.add(ach);
								}
							}
						}
					}
				}
				system.debug('===Achievement Size==='+newAchievement.size());
				if(newAchievement.size()>0){
					List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
					if(!newAchievement.isEmpty()) {
						Database.SaveResult[] lstDSR = Database.insert(newAchievement, false);
						for(Database.SaveResult objDSR : lstDSR) {
							
								if (objDSR.isSuccess()) {
									// Operation was successful, so get the ID of the record that was processed
									System.debug('Successfully inserted record with Id ' + objDSR.getId());
								}
								else {
									// Operation failed, so get all errors                
									for(Database.Error err : objDSR.getErrors()) {
										System.debug('The following error has occurred.');                    
										System.debug(err.getStatusCode() + ': ' + err.getMessage());
										System.debug('Fields that affected this error: ' + err.getFields());
										Error_Log__c objErrorLog = new Error_Log__c(Error_Message__c = err.getStatusCode() + ': ' + err.getMessage(), Type__c = 'Tranche Processing Error', Error_Code__c = 'Tranche Processing Error during Achievement Creation');
										lstErrorLog.add(objErrorLog);
									}
								}
						}
						
						if(!lstErrorLog.isEmpty()) {
							Database.insert(lstErrorLog, false);
						}
					}
				}
			}
		}
	}
	
	
    public static void deactivateTrancheAchievement(List<Tranche__c> newList){
        List<Monthly_Target__c> monthlyTargetList = new List<Monthly_Target__c>();
        List<Monthly_Target__c> updateMonthlyTargets = new List<Monthly_Target__c>();       
        monthlyTargetList = [Select Id, Sales_User__c, Sales_User__r.Name, Sales_User__r.Profile.Name, To__c, From__c, Actual_Amount__c, Actual_Count__c from Monthly_Target__c];
        
        Set<Id> loanIds = new Set<Id>();
        for(Tranche__c t: newList){
            loanIds.add(t.Loan_Application__c);         
        }
        List<Loan_Application__c> laList = [Select Id, CreatedById, Sales_Profile__c, Sales_User_Manager_ID__c, Assigned_Sales_User__c, Disbursement_DxROI_Tranche__c,SM_CBM_ID__c from Loan_Application__c where Id =: loanIds];
        for(Tranche__c t: newList){
            for(Loan_Application__c la: laList){
                if(t.Loan_Application__c == la.Id){
                    for(Monthly_Target__c tar: monthlyTargetList){
                        if(la.Sales_Profile__c == 'Sales Team' && la.CreatedById == tar.Sales_User__c && t.Cancellation_Date__c >= tar.From__c && t.Cancellation_Date__c <= tar.To__c){
                            if(tar.Actual_Amount__c != null){
                                tar.Actual_Amount__c = tar.Actual_Amount__c - t.Approved_Disbursal_Amount__c;
                            }
                            else{
                                tar.Actual_Amount__c = 0 - t.Approved_Disbursal_Amount__c;
                            }
                            updateMonthlyTargets.add(tar);
                        }
                        else if((la.Sales_Profile__c != 'Sales Team' && ((la.SM_CBM_ID__c != null && la.SM_CBM_ID__c == tar.Sales_User__c) || (la.SM_CBM_ID__c == null && la.Sales_User_Manager_ID__c == tar.Sales_User__c))) && t.Business_Date__c >= tar.From__c &&  t.Business_Date__c <= tar.To__c){
                            if(tar.Actual_Amount__c != null){
                                tar.Actual_Amount__c = tar.Actual_Amount__c - t.Approved_Disbursal_Amount__c;
                            }
                            else{
                                tar.Actual_Amount__c = 0 - t.Approved_Disbursal_Amount__c;
                            }
                            updateMonthlyTargets.add(tar);
                        }
                    }
                }
            }
        }
        system.debug('===Update Size==='+updateMonthlyTargets.size());
        if(updateMonthlyTargets.size()>0){
            database.update(updateMonthlyTargets);
        }
    }
    // Code Ends
    // Added by KK for Tranche Cancellation: Code Begins
    public static void updateDisbursements(List<Tranche__c> cancelledList){
        List<Disbursement__c> disbList = new List<Disbursement__c>();
        List<Disbursement__c> disbToUpdate = new List<Disbursement__c>();
        List<Loan_Application__c> laList = new List<Loan_Application__c>();
        List<Loan_Application__c> laListToUpdate = new List<Loan_Application__c>();
        List<Id> idList = new List<Id>();
        LMS_Date_Sync__c dt = [Select Id, Current_Date__c from LMS_Date_Sync__c ORDER BY CreatedDate DESC limit 1];
        for(Tranche__c t: cancelledList){
            idList.add(t.Loan_Application__c);
        }
        laList = [Select Id, Cancelled_Amount__c, Disbursed_Amount__c, Business_Date_Modified__c, Pending_Amount_for_Disbursement__c from Loan_Application__c where Id =: idList];
        disbList = [Select Id, Tranche__c, Tranche_Cancelled__c, Cancellation_Date__c, Cancelled_Amount__c, Disbursal_Amount__c from Disbursement__c where Tranche__c =: cancelledList];
        for(Disbursement__c disb: disbList){
            for(Tranche__c t: cancelledList){
                if(disb.Tranche__c == t.Id){
                    disb.Tranche_Cancelled__c = TRUE;
                    disb.Cancellation_Date__c = dt.Current_Date__c;
                    disb.Cancelled_Amount__c = disb.Disbursal_Amount__c;
                    disbToUpdate.add(disb);
                }
            }
        }
        for(Loan_Application__c la: laList){
            for(Tranche__c t: cancelledList){
                if(t.Loan_Application__c == la.Id){
                    if(la.Cancelled_Amount__c != null){
                        la.Cancelled_Amount__c = la.Cancelled_Amount__c + t.Approved_Disbursal_Amount__c;
                    }
                    else{
                        la.Cancelled_Amount__c = t.Approved_Disbursal_Amount__c;
                    }
                    la.Tranche_Cancelled_Date__c = la.Business_Date_Modified__c;
                    la.Pending_Amount_for_Disbursement__c = la.Pending_Amount_for_Disbursement__c + t.Approved_Disbursal_Amount__c;
                    laListToUpdate.add(la);
                }
            }
        }
        system.debug('==DISBURSEMENT SIZE=='+disbToUpdate.size());
        if(disbToUpdate.size()>0){
            database.update(disbToUpdate);
        }
        system.debug('==LOAN APP SIZE=='+laListToUpdate.size());
        if(laListToUpdate.size()>0){
            database.update(laListToUpdate);
        }
    }
    // Code Ends
    // Added by KK for Loan Downsizing [16-09-2019]: Code Begins
    public static void checkDisbStatus(List<Tranche__c> newList){
        List<Loan_Application__c> laList = new List<Loan_Application__c>();
        List<Loan_Application__c> laListToUpdate = new List<Loan_Application__c>();
        List<Id> loanIds = new List<Id>();
        for(Tranche__c t: newList){
            loanIds.add(t.Loan_Application__c);
        }
        laList = [Select Id, Disbursement_Status__c, Pending_Amount_for_Disbursement__c, Approved_Loan_Amount__c, Disbursed_Amount__c from Loan_Application__c where Id =: loanIds];
        for(Loan_Application__c la: laList){
            if(la.Approved_Loan_Amount__c == la.Disbursed_Amount__c){
                la.Disbursement_Status__c = 'Fully Disbursed';
                la.Pending_Amount_for_Disbursement__c = 0;
            }
            else{
                la.Pending_Amount_for_Disbursement__c = la.Approved_Loan_Amount__c - la.Disbursed_Amount__c;
            }
            laListToUpdate.add(la);
        }
        System.debug('==SIZE of LA=='+laListToUpdate.size());
        //Added try catch by vaishali - for exception handling
        try {
            if(laListToUpdate.size()>0){
                List<Database.SaveResult> results1  = Database.update(laListToUpdate);
            }
        } catch (DMLException e) {
            system.debug('DML EXCEPTION DURING TRANCHE PROCESSING');
            Error_Log__c errorLog = new Error_Log__c(Error_Message__c = e.getDmlMessage(0), Type__c = 'Tranche Processing Error', Error_Code__c = 'Tranche Processing Error');
            Database.insert(errorLog,false);
            
        } catch (Exception e) {
            system.debug('DML EXCEPTION DURING TRANCHE');
            Error_Log__c errorLog = new Error_Log__c(Error_Message__c = e.getDmlMessage(0), Type__c = 'Tranche Processing Error', Error_Code__c = 'Tranche Processing Error');
            Database.insert(errorLog,false);
        }
    }
    // Code Ends
    //Method to check if there is any pre-existing Loan Downsizing in system. -- Code not bulkified
    public static void cancelTranche(List<Tranche__c> newList)
    {
        List<Loan_Downsizing__c> loanDownsizingList = new List<Loan_Downsizing__c>();
        List<Loan_Downsizing__c> cancelledLoanDownsizingList = new List<Loan_Downsizing__c>();
        
        if(newList!=null && newList.size()==1)
        {
            ID loanApplicationID = newList[0].Loan_Application__c;
            
            loanDownsizingList = [Select Status__c from Loan_Downsizing__c Where (Status__c='Initiated' or Status__c='Approved') and Loan_Application__c = :loanApplicationID];
            
            if(loanDownsizingList!=null && loanDownsizingList.size() > 0)
            {
                for(Loan_Downsizing__c ld : loanDownsizingList)
                {
                    ld.Status__c = 'Cancelled'; 
                    cancelledLoanDownsizingList.add(ld);
                }
            }
        } 
        if(cancelledLoanDownsizingList.size() > 0)
        {
            Database.SaveResult[] srList = Database.update(cancelledLoanDownsizingList, false);

            
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    System.debug('Successfully  ' + sr.getId());
                }
                else {
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Fields that affected this error: ' + err.getFields());
                    }
                }
            }    
        }
    }
}