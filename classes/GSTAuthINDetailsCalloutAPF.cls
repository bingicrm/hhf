public class GSTAuthINDetailsCalloutAPF {
    
    public static void makeGstinAuthDetailsCallout(String strGSTIN,String strRecordId,String strProjectId,String strPanNumber,String strMobile, String strEmail, String strConstitution) {
        System.debug('Debug Log for strGSTIN'+strGSTIN);
        System.debug('Debug Log for strRecordId'+strRecordId);
        if(String.isNotBlank(strProjectId)) {
            System.debug('Debug Log for strProjectId'+strProjectId);
        }
        System.debug('Debug Log for strPanNumber'+strPanNumber);
        System.debug('Debug Log for strMobile'+strMobile);
        System.debug('Debug Log for strEmail'+strEmail);
        System.debug('Debug Log for strConstitution'+strConstitution);
        String sObjName = '';
        if(String.isNotBlank(strRecordId)) {
            sObjName = Id.valueOf(strRecordId).getSObjectType().getDescribe().getName();
        }
        System.debug('Debug Log for sObjName'+sObjName);
        if(String.isNotBlank(strGSTIN)) {
            List<Rest_Service__mdt> lstRestService = [SELECT 
                                                      MasterLabel, 
                                                      QualifiedApiName, 
                                                      Service_EndPoint__c, 
                                                      Client_Password__c, 
                                                      Client_Username__c, 
                                                      Request_Method__c, 
                                                      Time_Out_Period__c,
                                                      org_id__c
                                                      FROM   
                                                      Rest_Service__mdt
                                                      WHERE  
                                                      DeveloperName = 'GST_2_GstinAuthDetails'
                                                      LIMIT 1
                                                     ]; 
            System.debug('Debug Log for lstRestService'+lstRestService);
            if(!lstRestService.isEmpty()) {
                GstinAuthDetailsResponseBody objResponseBody = new GstinAuthDetailsResponseBody();
                Blob headerValue = Blob.valueOf(lstRestService[0].Client_Username__c + ':' + lstRestService[0].Client_Password__c);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                Map<String,String> headerMap = new Map<String,String>();
                headerMap.put('Authorization',authorizationHeader);
                headerMap.put('Username',lstRestService[0].Client_Username__c);
                headerMap.put('Password',lstRestService[0].Client_Password__c);
                headerMap.put('Content-Type','application/json');
                headerMap.put('org-id','HHFL');//lstRestService[0].org_id__c
                system.debug('==ORG ID=='+lstRestService[0].org_id__c);
                RequestBody objRequestBody = new RequestBody();
                objRequestBody.consent = 'Y';
                //objRequestBody.pdfOutputType = '';
                objRequestBody.gstin = strGSTIN != null ? strGSTIN : '';
                
                System.debug('Debug Log for objRequestBody'+objRequestBody);
                String jsonRequest = Json.serializePretty(objRequestBody); 
                
                Http objHttp = new Http();
                HttpRequest objHttpRequest = new HttpRequest();
                
                if(String.isNotBlank(lstRestService[0].Service_EndPoint__c)) {
                    objHttpRequest.setEndPoint(lstRestService[0].Service_EndPoint__c);
                }
                else {
                    System.debug('Debug Log for error : Endpoint Empty. Please specify endpoint in metadata record.');
                }
                if(String.isNotBlank(lstRestService[0].Request_Method__c)) {
                    objHttpRequest.setMethod(lstRestService[0].Request_Method__c);
                }
                else {
                    System.debug('Debug Log for error : Requested Method Empty. Please specify Requested Method in metadata record.');
                }
                
                objHttpRequest.setBody(jsonRequest);
                if(String.isNotBlank(String.valueOf(lstRestService[0].Time_Out_Period__c))) {
                    objHttpRequest.setTimeOut(Integer.valueOf(lstRestService[0].Time_Out_Period__c));
                }
                //objHttpRequest.setHeader('content-Type','application/json');
                //objHttpRequest.setHeader('Authorization', authorizationHeader);
                //objHttpRequest.setHeader('org-id', 'HHFL'); // Added as a check 15-10-19
                System.debug('Request => ' + objHttpRequest);
                System.debug('Request Header=> ' + objHttpRequest.getHeader('Authorization'));
                System.debug('Request Body=> ' + objHttpRequest.getbody());
                for(String headerKey : headerMap.keySet()){
                    objHttpRequest.setHeader(headerKey, headerMap.get(headerKey));
                    System.debug('Debug Log for headerKey'+headerKey);
                    System.debug('Debug Log for headerMap.get(headerKey)'+headerMap.get(headerKey));
                }
                System.debug('Debug Log for request'+objHttpRequest);
                
                try {
                    HTTPResponse httpRes = objHttp.send(objHttpRequest);
                    //Create integration logs
                    LogUtility.createIntegrationLogs(jsonRequest,httpRes,lstRestService[0].Service_EndPoint__c);//Changed from Future
                    System.debug('Debug Log for response'+httpRes);
                    System.debug('Debug Log for response body'+httpRes.getBody());
                    System.debug('Debug Log for response status'+httpRes.getStatus());
                    System.debug('Debug Log for response status code'+httpRes.getStatusCode());
                    //List<Customer_Integration__c> gstList = new List<Customer_Integration__c >();
                    objResponseBody = (GstinAuthDetailsResponseBody)Json.deserialize(httpRes.getBody(), GstinAuthDetailsResponseBody.class);
                    
                    if(sObjName == Constants.strProjectBuilderAPI) {
                        Builder_Integrations__c objBI = new Builder_Integrations__c();
                        objBI.RecordTypeId = Schema.SObjectType.Builder_Integrations__c.getRecordTypeInfosByName().get('GSTIN').getRecordTypeId();
                        if(httpRes.getStatus() == 'OK' && httpRes.getStatusCode() == 200) {
                            if(objResponseBody.statusCode == 101){
                                system.debug('Status Code ' + objResponseBody.statusCode);
                                if(objResponseBody != null) {
                                    System.debug('Result Array'+objResponseBody.result);
                                    System.debug('Result Request Id'+objResponseBody.requestId);
                                    System.debug('Result Status Code'+objResponseBody.statusCode);
                                    
                                    Map<String,String> ConstitutionlabelToValueMap = new Map<String,String>();
                                    Schema.DescribeFieldResult fieldResult2 = Project_Builder__c.Constitution__c.getDescribe();  
                                    List<Schema.PicklistEntry> plentry = fieldResult2.getPicklistValues();  
                                    for (Schema.PicklistEntry f : plentry) {  
                                        ConstitutionlabelToValueMap.put(f.getValue(),f.getLabel());
                                    } 
                                    System.debug('ConstitutionlabelToValueMapinAPI ' + ConstitutionlabelToValueMap);
                                    
                                objBI.GSTIN__c = String.isNotBlank(strGSTIN) ? strGSTIN : '';
                                
                                if(objResponseBody.result.rgdt != null){
                                    objBI.GST_Date_Of_Registration__c = Date.parse(objResponseBody.result.rgdt);
                                }
                                objBI.Project_Builder__c = strRecordId;
                                objBI.Project__c = strProjectId;
                                objBI.Constitution__c = String.isNotBlank(strConstitution) ? ConstitutionlabelToValueMap.get(strConstitution) : '' ;
                                objBI.PAN_Number__c = String.isNotBlank(strPanNumber) ? strPanNumber : '' ;
                                objBI.Mobile__c = String.isNotBlank(strMobile) ? strMobile : '' ;
                                objBI.Email__c = String.isNotBlank(strEmail) ? strEmail : '' ;
                                objBI.Request_Id__c = String.isNotBlank(objResponseBody.requestId) ? objResponseBody.requestId : '' ; 
                                objBI.Status_Code__c = String.isNotBlank(String.valueOf(objResponseBody.statusCode)) ? String.valueOf(objResponseBody.statusCode) : '';
                                objBI.Legal_Name__c = String.isNotBlank(objResponseBody.result.lgnm) ? objResponseBody.result.lgnm : '' ;
                                objBI.Trade_Name__c = String.isNotBlank(objResponseBody.result.tradeNam) ? objResponseBody.result.tradeNam : '' ;
                                objBI.GST_Tax_payer_type__c =String.isNotBlank(objResponseBody.result.dty) ? objResponseBody.result.dty : 'none' ;
                                objBI.Current_Status_of_Registration_Under_GST__c = String.isNotBlank(objResponseBody.result.sts) ? objResponseBody.result.sts : '' ;
                                objBI.Manual_GSTIN__c = false;
                                objBI.System_Created__c = true;
                                
                                List<string> slist = new List<String>();
                                slist = objResponseBody.result.nba;
                                string businessActivity = string.join(sList,',');
                                system.debug('businessActivity' + businessActivity);
                                objBI.Business_Activity__c = businessActivity;
                                system.debug('objBI' + objBI);
                                }
                            }
                            else if(objResponseBody.statusCode == 102){
                                SYSTEM.debug('STATUS CODE  ' +objResponseBody.statusCode);
                                system.debug('recordType ' + objBI.RecordTypeId);
                                objBI.Manual_GSTIN__c = false;
                                objBI.System_Created__c = true;
                                objBI.Project_Builder__c = strRecordId;
    							objBI.Project__c = strProjectId;
                                objBI.GSTIN__c = String.isNotBlank(strGSTIN) ? strGSTIN : '';
                                system.debug('D');   
                                objBI.GSTIN_Error_Message__c = 'Invalid ID number or combination of inputs';
                            }
                        }
                        else{
                            objBI.Manual_GSTIN__c = false;
                            objBI.System_Created__c = true;
                            objBI.Project_Builder__c = strRecordId;
                            objBI.Project__c = strProjectId;
                            objBI.GSTIN__c = String.isNotBlank(strGSTIN) ? strGSTIN : '';
                            system.debug('D');   
                            GstinAuthDetailsResponseBody errorBody = (GstinAuthDetailsResponseBody)Json.deserialize(httpRes.getBody(), GstinAuthDetailsResponseBody.class);
                            if(errorBody != null){
                            	objBI.GSTIN_Error_Message__c = errorBody.error.errorMessage;
                            	system.debug('Debug Log for GSTIN_Error_Message__c  ' + errorBody.error.errorMessage);
                            }
                        }
                        
                        insert objBI;
                        system.debug('GSTRecord BA' + objBI.Business_Activity__c);
                        system.debug('GSTRecord' + objBI.Id);
                        if(objBI.Id != null){
                            Project_Builder__c objProjectBuilder = [Select id,GST_Initiated__c from Project_Builder__c where id =: strRecordId limit 1];
                            if(objProjectBuilder.Id != null){
                                objProjectBuilder.GST_Initiated__c = true;
                                update objProjectBuilder;
                            }
                            
                        }
                    }
                    
                    else if(sObjName == Constants.strPromoterAPI) {
                        Promoter_Integrations__c objPI = new Promoter_Integrations__c();
                        objPI.RecordTypeId = Schema.SObjectType.Promoter_Integrations__c.getRecordTypeInfosByName().get('GSTIN').getRecordTypeId();
                        if(httpRes.getStatus() == 'OK' && httpRes.getStatusCode() == 200) {
                            if(objResponseBody.statusCode == 101){
                                system.debug('Status Code ' + objResponseBody.statusCode);
                                if(objResponseBody != null) {
                                    System.debug('Result Array'+objResponseBody.result);
                                    System.debug('Result Request Id'+objResponseBody.requestId);
                                    System.debug('Result Status Code'+objResponseBody.statusCode);
                                    
                                    Map<String,String> ConstitutionlabelToValueMap = new Map<String,String>();
                                    Schema.DescribeFieldResult fieldResult2 = Promoter__c.Constitution__c.getDescribe();  
                                    List<Schema.PicklistEntry> plentry = fieldResult2.getPicklistValues();  
                                    for (Schema.PicklistEntry f : plentry) {  
                                        ConstitutionlabelToValueMap.put(f.getValue(),f.getLabel());
                                    } 
                                    System.debug('ConstitutionlabelToValueMapinAPI ' + ConstitutionlabelToValueMap);
                                    objPI.GSTIN__c = String.isNotBlank(strGSTIN) ? strGSTIN : '';
                                
                                    if(objResponseBody.result.rgdt != null){
                                        objPI.GST_Date_Of_Registration__c = Date.parse(objResponseBody.result.rgdt);
                                    }
                                    objPI.Promoter__c = strRecordId;
                                    objPI.Constitution__c = String.isNotBlank(strConstitution) ? ConstitutionlabelToValueMap.get(strConstitution) : '' ;
                                    objPI.PAN_Number__c = String.isNotBlank(strPanNumber) ? strPanNumber : '' ;
                                    objPI.Mobile__c = String.isNotBlank(strMobile) ? strMobile : '' ;
                                    objPI.Email__c = String.isNotBlank(strEmail) ? strEmail : '' ;
                                    objPI.Request_Id__c = String.isNotBlank(objResponseBody.requestId) ? objResponseBody.requestId : '' ; 
                                    objPI.Status_Code__c = String.isNotBlank(String.valueOf(objResponseBody.statusCode)) ? String.valueOf(objResponseBody.statusCode) : '';
                                    objPI.Legal_Name__c = String.isNotBlank(objResponseBody.result.lgnm) ? objResponseBody.result.lgnm : '' ;
                                    objPI.Trade_Name__c = String.isNotBlank(objResponseBody.result.tradeNam) ? objResponseBody.result.tradeNam : '' ;
                                    objPI.GST_Tax_payer_type__c =String.isNotBlank(objResponseBody.result.dty) ? objResponseBody.result.dty : 'none' ;
                                    objPI.Current_Status_of_Registration_Under_GST__c = String.isNotBlank(objResponseBody.result.sts) ? objResponseBody.result.sts : '' ;
                                    objPI.Manual_GSTIN__c = false;
                                    objPI.System_Created__c = true;
                                    
                                    List<string> slist = new List<String>();
                                    slist = objResponseBody.result.nba;
                                    string businessActivity = string.join(sList,',');
                                    system.debug('businessActivity' + businessActivity);
                                    objPI.Business_Activity__c = businessActivity;
                                    system.debug('objPI' + objPI);
                                    
                                }
                            }
                            else if(objResponseBody.statusCode == 102){
                                SYSTEM.debug('STATUS CODE  ' +objResponseBody.statusCode);
                                system.debug('recodType ' + objPI.RecordTypeId);
                                objPI.Manual_GSTIN__c = false;
                                objPI.System_Created__c = true;
                                objPI.Promoter__c = strRecordId;
                                objPI.GSTIN__c = String.isNotBlank(strGSTIN) ? strGSTIN : '';
                                system.debug('D');   
                                objPI.GSTIN_Error_Message__c = 'Invalid ID number or combination of inputs';
                                
                            }
                        }
                        else{
                            objPI.Manual_GSTIN__c = false;
                            objPI.System_Created__c = true;
                            objPI.GSTIN__c = String.isNotBlank(strGSTIN) ? strGSTIN : '';
                            objPI.Promoter__c = strRecordId;
                            system.debug('D');   
                            GstinAuthDetailsResponseBody errorBody = (GstinAuthDetailsResponseBody)Json.deserialize(httpRes.getBody(), GstinAuthDetailsResponseBody.class);
                            if(errorBody != null){
                            	objPI.GSTIN_Error_Message__c = errorBody.error.errorMessage;
                            	system.debug('Debug Log for GSTIN_Error_Message__c  ' + errorBody.error.errorMessage);
                            }
                        }
                        
                        insert objPI;
                        system.debug('GSTRecord BA' + objPI.Business_Activity__c);
                        system.debug('GSTRecord' + objPI.Id);
                        if(objPI.Id != null){
                            Promoter__c objPromoter = [Select id,GST_Initiated__c from Promoter__c where id =: strRecordId limit 1];
                            if(objPromoter.Id != null){
                                objPromoter.GST_Initiated__c = true;
                                update objPromoter;
                            }
                            
                        }
                    }
                }
                catch(Exception ex) {
                    System.debug('Debug Log for exception'+ex);
                }
            }
        }
    }
    
    public class RequestBody {
        public String consent;
        public String gstin;
        //public String pdfOutputType;
    }
    
}