@isTest
public class Test_cls_ProcessAppLevelDocumentsBatch {


    private static testMethod void testSample() {
        Profile p = [SELECT id, name from profile where name =: 'Sales Team'];
        
        User u = new User(
            ProfileId = p.Id,
            LastName = 'last',  
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST', 
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            Technical__c = true,
            FCU__c= true,Legal__c = true, FI__c= true,LIP__C = true,PD__c= true,Role_in_Sales_Hierarchy__c = 'SM');
        
        insert u;
        
        Loan_Contact__c lc;
        Loan_Application__c la;
        
        System.runAs(u) {
            Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
            
            Account acc = new Account();
            acc.CountryCode__c = '+91';
            acc.Email__c = 'puser001@amamama.com';
            acc.Gender__c = 'M';
            acc.Mobile__c = '9800765432';
            acc.Phone__c = '9800765434';
            acc.PersonMobilePhone = '9800765434';
            acc.FirstName = 'abc';
            acc.LastName = 'def';
            acc.Salutation = 'Mr';
            acc.RecordTypeId = RecordTypeIdCustomer;
            insert acc;
            
            Scheme__c sch = new Scheme__c(Name = 'Construction Finance', Scheme_Code__c = 'CF', Scheme_Group_ID__c = '5',
                                          Scheme_ID__c = 11, Product_Code__c = 'CF', Max_ROI__c = 10, Min_ROI__c = 5,
                                          Min_Loan_Amount__c = 10000, Min_FOIR__c = 1,
                                          Max_Loan_Amount__c = 3000001, Max_Tenure__c = 84, Min_Tenure__c = 6,
                                          Max_LTV__c = 5, Min_LTV__c = 1, Max_FOIR__c = 5);
            insert sch;
            
            Scheme__c sch1=new Scheme__c();
            sch1.Name='Personal Loan';
            sch1.Product_Code__c='IPL'; 
            sch1.Scheme_Code__c='IPL';
            sch1.Scheme_Group_ID__c='Test';
            insert sch1;
            Local_Policies__c lp = new Local_Policies__c(Name='Standard', Branch__c='All',Product_Scheme__c='All Schemes',IsActive__c=true);
            insert lp;
            
            Id RecordTypeIdLAAppIni = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Application Initiation').getRecordTypeId();
            
            la = new Loan_Application__c(Customer__c = acc.Id, Scheme__c = sch.Id, Loan_Number__c = '124',
                                         Transaction_type__c = 'PB', Requested_Amount__c = 1000000,
                                         Loan_Purpose__c = '11', StageName__c = 'Customer Onboarding', Sub_Stage__c = 'Application Initiation',
                                         RecordTypeId = RecordTypeIdLAAppIni,Branch__c='Delhi',
                                         Business_Date_Created__c = System.today(),
                                         Approved_Loan_Amount__c = 1000000,Requested_Loan_Tenure__c=84,
                                         Processing_Fee_Percentage__c = 1, Mode_of_Repayment__c = 'NACH',
                                         Repayment_Start_Date__c = Date.Today().addDays(100),Income_Program_Type__c='Normal Salaried',
                                         Sanctioned_Disbursement_Type__c = 'Multiple Tranche', Property_Identified__c = true,
                                         Local_Policies__c=lp.Id, Assigned_Sales_User__c = u.Id,Folders_Created_in_S3__c=true);
            insert la;
            Test.startTest();
            lc = [SELECT Id, Borrower__c from Loan_Contact__c where Loan_Applications__c =: la.Id];
            lc.Borrower__c = '1';
            //lc.Category__c='1';
            lc.Constitution__c = '20';
            lc.Customer_segment__c = '3';
            lc.BRE_Record__c = true;
            lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc.Marital_Status__c = 'M';
            lc.Gross_Salary__c = 1000;
            lc.Basic_Pay__c = 1000;
            lc.DA__c = 1000;
            lc.Qualification__c = 'UMS';
            //lc.Total_Fixed_Pay__c = 1000;
            //lc.Variable_Income_Monthly__c = 1000;
            lc.Net_Income__c = 1000;
            lc.PF__c = 1000;
            lc.TDS__c = 1000;
            lc.Current_Organization_Work_Ex_in_Months__c = 12;
            lc.Total_Work_Experience__c = 12;
            lc.Customer_Details_Verified__c = true;
            lc.Voter_ID_Number__c = '123456789';
            lc.Passport_Number__c = '123456789';
            lc.PAN_Number__c = 'BAFPV6543A';
            lc.Aadhaar_Number__c = '898518762736';
            lc.Income_Considered__c = true;
            lc.AIP_month__c = '4-2020';
            lc.Mobile__c = '7878565677';
            lc.Email__c = 'abc@g.com';
            lc.Father_s_Husband_s_Name__c ='sww';
			lc.Mother_s_Maiden_Name__c ='avdd';
            update lc;
            /*
            Cross_Sell_Partner_Name__c CSPN = new Cross_Sell_Partner_Name__c(Name='Test');
            insert CSPN;
            
            DST_Master__c DSTMaster = new DST_Master__c(Name='Test DST',Inspector_ID__c='1234'); 
            insert DSTMaster;
            
            Sourcing_Detail__c sd= new Sourcing_Detail__c(Loan_Application__c=la.id,Source_Code__c='16',DST_Name__c=DSTMaster.id,Cross_Sell_Partner_Name__c=CSPN.id);
            insert sd;
            
            la.StageName__c = 'Operation Control';
            la.Sub_Stage__c = 'COPS:Data Maker';
            la.IMGC_Premium_Fees_GST_Inclusive1__c = 20000;
            la.Approved_cross_sell_amount__c = 50000;
            update la;*/
		Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=la.Id, Loan_Contact__c= lc.Id);
        insert objDocCheck;
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
         
        //Get Content Documents
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
            
        Document_Migration_Log__c objDoc = new Document_Migration_Log__c ();
        objDoc.Description__c='Test';
        objDoc.Loan_Application__c=la.Id;
        objDoc.Document_Checklist__c = objDocCheck.Id;
        objDoc.Document_Id__c = conDocId;
        objDoc.Successfully_Migrated__c = true;
        objDoc.Old_Release_Data__c = false;
        insert objDoc;
        
        Attachment att=new Attachment();
        att.ParentId = objDoc.Loan_Application__c ;
        att.Name='test';
        att.Body=Blob.valueOf('test');
        insert att;
        
        //Create ContentDocumentLink 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = objDocCheck.Id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        Insert cdl;
		Date dt = System.Today();
		la.StageName__c='Loan Cancelled';
		la.Sub_Stage__c='Loan Cancel';
        la.Date_of_Cancellation__c = dt.addDays(-60);
		update la;
 		cls_ProcessAppLevelDocumentsBatch obj = new cls_ProcessAppLevelDocumentsBatch();
		Database.executeBatch(obj);
            Test.stopTest();
        }

    }
    
      private static testMethod void testReject() {
        Profile p = [SELECT id, name from profile where name =: 'Sales Team'];
        
        User u = new User(
            ProfileId = p.Id,
            LastName = 'last',  
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST', 
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            Technical__c = true,
            FCU__c= true,Legal__c = true, FI__c= true,LIP__C = true,PD__c= true,Role_in_Sales_Hierarchy__c = 'SM');
        
        insert u;
        
        Loan_Contact__c lc;
        Loan_Application__c la;
        
        System.runAs(u) {
            Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
            
            Account acc = new Account();
            acc.CountryCode__c = '+91';
            acc.Email__c = 'puser001@amamama.com';
            acc.Gender__c = 'M';
            acc.Mobile__c = '9800765432';
            acc.Phone__c = '9800765434';
            acc.PersonMobilePhone = '9800765434';
            acc.FirstName = 'abc';
            acc.LastName = 'def';
            acc.Salutation = 'Mr';
            acc.RecordTypeId = RecordTypeIdCustomer;
            insert acc;
            
            Scheme__c sch = new Scheme__c(Name = 'Construction Finance', Scheme_Code__c = 'CF', Scheme_Group_ID__c = '5',
                                          Scheme_ID__c = 11, Product_Code__c = 'CF', Max_ROI__c = 10, Min_ROI__c = 5,
                                          Min_Loan_Amount__c = 10000, Min_FOIR__c = 1,
                                          Max_Loan_Amount__c = 3000001, Max_Tenure__c = 84, Min_Tenure__c = 6,
                                          Max_LTV__c = 5, Min_LTV__c = 1, Max_FOIR__c = 5);
            insert sch;
            
            Scheme__c sch1=new Scheme__c();
            sch1.Name='Personal Loan';
            sch1.Product_Code__c='IPL'; 
            sch1.Scheme_Code__c='IPL';
            sch1.Scheme_Group_ID__c='Test';
            insert sch1;
            Local_Policies__c lp = new Local_Policies__c(Name='Standard', Branch__c='All',Product_Scheme__c='All Schemes',IsActive__c=true);
            insert lp;
            
            Id RecordTypeIdLAAppIni = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Application Initiation').getRecordTypeId();
            
            la = new Loan_Application__c(Customer__c = acc.Id, Scheme__c = sch.Id, Loan_Number__c = '124',
                                         Transaction_type__c = 'PB', Requested_Amount__c = 1000000,
                                         Loan_Purpose__c = '11', StageName__c = 'Customer Onboarding', Sub_Stage__c = 'Application Initiation',
                                         RecordTypeId = RecordTypeIdLAAppIni,Branch__c='Delhi',
                                         Business_Date_Created__c = System.today(),
                                         Approved_Loan_Amount__c = 1000000,Requested_Loan_Tenure__c=84,
                                         Processing_Fee_Percentage__c = 1, Mode_of_Repayment__c = 'NACH',
                                         Repayment_Start_Date__c = Date.Today().addDays(100),Income_Program_Type__c='Normal Salaried',
                                         Sanctioned_Disbursement_Type__c = 'Multiple Tranche', Property_Identified__c = true,
                                         Local_Policies__c=lp.Id, Assigned_Sales_User__c = u.Id,Folders_Created_in_S3__c=true);
            insert la;
            Test.startTest();
            lc = [SELECT Id, Borrower__c from Loan_Contact__c where Loan_Applications__c =: la.Id];
            lc.Borrower__c = '1';
            //lc.Category__c='1';
            lc.Constitution__c = '20';
            lc.Customer_segment__c = '3';
            lc.BRE_Record__c = true;
            lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc.Marital_Status__c = 'M';
            lc.Gross_Salary__c = 1000;
            lc.Basic_Pay__c = 1000;
            lc.DA__c = 1000;
            lc.Qualification__c = 'UMS';
            //lc.Total_Fixed_Pay__c = 1000;
            //lc.Variable_Income_Monthly__c = 1000;
            lc.Net_Income__c = 1000;
            lc.PF__c = 1000;
            lc.TDS__c = 1000;
            lc.Current_Organization_Work_Ex_in_Months__c = 12;
            lc.Total_Work_Experience__c = 12;
            lc.Customer_Details_Verified__c = true;
            lc.Voter_ID_Number__c = '123456789';
            lc.Passport_Number__c = '123456789';
            lc.PAN_Number__c = 'BAFPV6543A';
            lc.Aadhaar_Number__c = '898518762736';
            lc.Income_Considered__c = true;
            lc.AIP_month__c = '4-2020';
            lc.Mobile__c = '7878565677';
            lc.Email__c = 'abc@g.com';
            lc.Father_s_Husband_s_Name__c ='sww';
			lc.Mother_s_Maiden_Name__c ='avdd';
            update lc;
            /*
            Cross_Sell_Partner_Name__c CSPN = new Cross_Sell_Partner_Name__c(Name='Test');
            insert CSPN;
            
            DST_Master__c DSTMaster = new DST_Master__c(Name='Test DST',Inspector_ID__c='1234'); 
            insert DSTMaster;
            
            Sourcing_Detail__c sd= new Sourcing_Detail__c(Loan_Application__c=la.id,Source_Code__c='16',DST_Name__c=DSTMaster.id,Cross_Sell_Partner_Name__c=CSPN.id);
            insert sd;
            
            la.StageName__c = 'Operation Control';
            la.Sub_Stage__c = 'COPS:Data Maker';
            la.IMGC_Premium_Fees_GST_Inclusive1__c = 20000;
            la.Approved_cross_sell_amount__c = 50000;
            update la;*/
		Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=la.Id, Loan_Contact__c= lc.Id);
        insert objDocCheck;
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
         
        //Get Content Documents
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
            
        Document_Migration_Log__c objDoc = new Document_Migration_Log__c ();
        objDoc.Description__c='Test';
        objDoc.Loan_Application__c=la.Id;
        objDoc.Document_Checklist__c = objDocCheck.Id;
        objDoc.Document_Id__c = conDocId;
        objDoc.Successfully_Migrated__c = true;
        objDoc.Old_Release_Data__c = false;
        insert objDoc;
        
        Attachment att=new Attachment();
        att.ParentId = objDoc.Loan_Application__c ;
        att.Name='test';
        att.Body=Blob.valueOf('test');
        insert att;
        
        //Create ContentDocumentLink 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = objDocCheck.Id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        Insert cdl;
		Date dt = System.Today();
		la.StageName__c='Loan Reject';
		la.Sub_Stage__c='Loan Reject';
		update la;
        la.Date_of_Rejection__c = dt.addDays(-60);
		update la;
 		cls_ProcessAppLevelDocumentsBatch obj = new cls_ProcessAppLevelDocumentsBatch();
		Database.executeBatch(obj);
            Test.stopTest();
        }

    }
    
      private static testMethod void testRetry() {
        Profile p = [SELECT id, name from profile where name =: 'Sales Team'];
        
        User u = new User(
            ProfileId = p.Id,
            LastName = 'last',  
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST', 
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            Technical__c = true,
            FCU__c= true,Legal__c = true, FI__c= true,LIP__C = true,PD__c= true,Role_in_Sales_Hierarchy__c = 'SM');
        
        insert u;
        
        Loan_Contact__c lc;
        Loan_Application__c la;
        
        System.runAs(u) {
            Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
            
            Account acc = new Account();
            acc.CountryCode__c = '+91';
            acc.Email__c = 'puser001@amamama.com';
            acc.Gender__c = 'M';
            acc.Mobile__c = '9800765432';
            acc.Phone__c = '9800765434';
            acc.PersonMobilePhone = '9800765434';
            acc.FirstName = 'abc';
            acc.LastName = 'def';
            acc.Salutation = 'Mr';
            acc.RecordTypeId = RecordTypeIdCustomer;
            insert acc;
            
            Scheme__c sch = new Scheme__c(Name = 'Construction Finance', Scheme_Code__c = 'CF', Scheme_Group_ID__c = '5',
                                          Scheme_ID__c = 11, Product_Code__c = 'CF', Max_ROI__c = 10, Min_ROI__c = 5,
                                          Min_Loan_Amount__c = 10000, Min_FOIR__c = 1,
                                          Max_Loan_Amount__c = 3000001, Max_Tenure__c = 84, Min_Tenure__c = 6,
                                          Max_LTV__c = 5, Min_LTV__c = 1, Max_FOIR__c = 5);
            insert sch;
            
            Scheme__c sch1=new Scheme__c();
            sch1.Name='Personal Loan';
            sch1.Product_Code__c='IPL'; 
            sch1.Scheme_Code__c='IPL';
            sch1.Scheme_Group_ID__c='Test';
            insert sch1;
            Local_Policies__c lp = new Local_Policies__c(Name='Standard', Branch__c='All',Product_Scheme__c='All Schemes',IsActive__c=true);
            insert lp;
            
            Id RecordTypeIdLAAppIni = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Application Initiation').getRecordTypeId();
            
            la = new Loan_Application__c(Customer__c = acc.Id, Scheme__c = sch.Id, Loan_Number__c = '124',
                                         Transaction_type__c = 'PB', Requested_Amount__c = 1000000,
                                         Loan_Purpose__c = '11', StageName__c = 'Customer Onboarding', Sub_Stage__c = 'Application Initiation',
                                         RecordTypeId = RecordTypeIdLAAppIni,Branch__c='Delhi',
                                         Business_Date_Created__c = System.today(),
                                         Approved_Loan_Amount__c = 1000000,Requested_Loan_Tenure__c=84,
                                         Processing_Fee_Percentage__c = 1, Mode_of_Repayment__c = 'NACH',
                                         Repayment_Start_Date__c = Date.Today().addDays(100),Income_Program_Type__c='Normal Salaried',
                                         Sanctioned_Disbursement_Type__c = 'Multiple Tranche', Property_Identified__c = true,
                                         Local_Policies__c=lp.Id, Assigned_Sales_User__c = u.Id,Folders_Created_in_S3__c=true);
            insert la;
            Test.startTest();
            lc = [SELECT Id, Borrower__c from Loan_Contact__c where Loan_Applications__c =: la.Id];
            lc.Borrower__c = '1';
            //lc.Category__c='1';
            lc.Constitution__c = '20';
            lc.Customer_segment__c = '3';
            lc.BRE_Record__c = true;
            lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc.Marital_Status__c = 'M';
            lc.Gross_Salary__c = 1000;
            lc.Basic_Pay__c = 1000;
            lc.DA__c = 1000;
            lc.Qualification__c = 'UMS';
            //lc.Total_Fixed_Pay__c = 1000;
            //lc.Variable_Income_Monthly__c = 1000;
            lc.Net_Income__c = 1000;
            lc.PF__c = 1000;
            lc.TDS__c = 1000;
            lc.Current_Organization_Work_Ex_in_Months__c = 12;
            lc.Total_Work_Experience__c = 12;
            lc.Customer_Details_Verified__c = true;
            lc.Voter_ID_Number__c = '123456789';
            lc.Passport_Number__c = '123456789';
            lc.PAN_Number__c = 'BAFPV6543A';
            lc.Aadhaar_Number__c = '898518762736';
            lc.Income_Considered__c = true;
            lc.AIP_month__c = '4-2020';
            lc.Mobile__c = '7878565677';
            lc.Email__c = 'abc@g.com';
            lc.Father_s_Husband_s_Name__c ='sww';
			lc.Mother_s_Maiden_Name__c ='avdd';
            update lc;
            /*
            Cross_Sell_Partner_Name__c CSPN = new Cross_Sell_Partner_Name__c(Name='Test');
            insert CSPN;
            
            DST_Master__c DSTMaster = new DST_Master__c(Name='Test DST',Inspector_ID__c='1234'); 
            insert DSTMaster;
            
            Sourcing_Detail__c sd= new Sourcing_Detail__c(Loan_Application__c=la.id,Source_Code__c='16',DST_Name__c=DSTMaster.id,Cross_Sell_Partner_Name__c=CSPN.id);
            insert sd;
            
            la.StageName__c = 'Operation Control';
            la.Sub_Stage__c = 'COPS:Data Maker';
            la.IMGC_Premium_Fees_GST_Inclusive1__c = 20000;
            la.Approved_cross_sell_amount__c = 50000;
            update la;*/
		Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=la.Id, Loan_Contact__c= lc.Id);
        insert objDocCheck;
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
         
        //Get Content Documents
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
            
        Document_Migration_Log__c objDoc = new Document_Migration_Log__c ();
        objDoc.Description__c='Test';
        objDoc.Loan_Application__c=la.Id;
        objDoc.Document_Checklist__c = objDocCheck.Id;
        objDoc.Document_Id__c = conDocId;
        objDoc.Successfully_Migrated__c = false;
        objDoc.Old_Release_Data__c = false;
        insert objDoc;
        
        Attachment att=new Attachment();
        att.ParentId = objDoc.Loan_Application__c ;
        att.Name='test';
        att.Body=Blob.valueOf('test');
        insert att;
        
        //Create ContentDocumentLink 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = objDocCheck.Id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        Insert cdl;
		Date dt = System.Today();
		la.StageName__c='Loan Reject';
		la.Sub_Stage__c='Loan Reject';
        la.Date_of_Rejection__c = dt.addDays(-60);
		update la;
 		cls_ProcessAppLevelDocumentsBatch obj = new cls_ProcessAppLevelDocumentsBatch();
		Database.executeBatch(obj);
            Test.stopTest();
        }

    }
}