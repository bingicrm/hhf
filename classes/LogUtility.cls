/**
 *    @author        Deloitte Digital
 *    @date          4/11/2016
 *    @description   Main class for Log Framework
 *
 *    Modification Log:
 *    ------------------------------------------------------------------------------------     
 *    Developer                       Date                Description
 *    ------------------------------------------------------------------------------------        
 *    Nikhil More                   4/11/2016          Original Version
**/
public without sharing class LogUtility
{
    
    public LogUtility() 
    {
        /* Default Constructor */
    }
    
    
    public static void createIntegrationLogs(String strRequest, HttpResponse strResponse, String strURL) {
        try {
            Error_Log__c obj = new Error_Log__c();
            strRequest = strRequest != null ? Json.serializePretty(strRequest) : 'Request is null';
            String strResponseBody = strResponse != null ? (strResponse.getBody()) : 'Response is null';
            
            system.debug('@@@@@@strRequest' + strRequest);
            strRequest = strRequest .replaceAll('\"', '"');
            if (strRequest.contains('\n')) {
                strRequest = strRequest .replaceAll('\n', '<br/>');
                strRequest = strRequest .replaceAll('\t', '&nbsp;');
            } else {
                strRequest = strRequest .replaceAll(',', ',<br/>');
                strRequest = strRequest .replaceAll('}', '}<br/>');
            }
            strResponseBody= strResponseBody.mid(0,80070).replaceAll('\"', '"');
            if (strResponseBody.contains('\n')) {
                strResponseBody= strResponseBody.replaceAll('\n', '<br/>');
                strResponseBody= strResponseBody.replaceAll('\t', '&nbsp;');
            } else {
                strResponseBody= strResponseBody.replaceAll(',', ',<br/>');
                strResponseBody= strResponseBody.replaceAll('}', '}<br/>');
            }
            
            Integer maxSize = 80070;
            system.debug('strResponseBody size' + strResponseBody.length());
            if(strResponseBody.length() > maxSize ){
                strResponseBody= strResponseBody.substring(0, maxSize);
                 system.debug('strResponseBody size after' + strResponseBody.length());
            }
            
            obj.request__c = strRequest;
            obj.response__c= strResponseBody;
            obj.URL__c= strURL;
            insert obj;
            system.debug('create Integration Log Id--> '+obj.Id);
        } catch (Exception e) {
            system.debug('create Integration Logs error--> '+e.getmessage()+' at line no. '+e.getlinenumber());
        }
    }

    public static void createLogs(String logType, String logSeverity, String logSourceType, String logSource, String logDetails, String logMessage, String logPayloadFile, Boolean executeDML)
    {
        Boolean createLogs;
        LogWrapper logObj = new LogWrapper();

        logObj.logType = logType;
        logObj.logSource = logSource;
        logObj.logMessage = logMessage;
        logObj.logTimeStamp = System.now();
        logObj.logSourceType = logSourceType;
        logObj.logPayloadFile = logPayloadFile;
        logObj.logDetails = LogHelper.setLogDetails(logDetails);
        logObj.logSeverity = LogHelper.setLogSeverity(logSeverity,logType);

        if(executeDML)
        {
            if(String.isNotBlank(LogHelper.getLogObjectName()))
            {
                SObject dLog = LogHelper.getObjectInstance(logObj);

                if(dLog != null)
                {
                    if(logType == 'Integration')
                    {
                        createLogs = LogHelper.getIsIntegrationLoggingEnabled();
                    }
                    else
                    {
                        createLogs = LogHelper.getIsLoggingEnabled();
                    }
                    if(createLogs)
                    {
                        try
                        {
                            INSERT dLog;

                            if(String.isNotBlank(logPayloadFile))
                            {
                                LogHelper.createAttachmentFromPayload(dLog, logObj);
                            }
                            if(String.isNotBlank(logMessage) && logMessage.length() > 1000)
                            {
                                LogHelper.createAttachmentFromMessage(dLog, logMessage);
                            }
                        }
                        catch(Exception e)
                        {
                            // Capture logs if any errors here
                        }
                    }
                }
            }
        }
        else
        {
            LogHelper.lstLogWrapper.add(logObj); // This list is further used by commit method
        }
    }

    public static void commitLogs(Boolean executeNow)
    {
        if(executeNow)
        {
            LogHelper.commitLogsNow();
        }
        else
        {
            String lstLogWrapperJSON = LogWrapper.encodeJSON(LogHelper.lstLogWrapper); // Serialize lstLogWrapper to JSON

            LogHelper.commitLogsInFuture(lstLogWrapperJSON);
        }
    }

    public static LogWrapper returnLogs(String logType, String logSeverity, String logSourceType, String logSource, String logDetails, String logMessage, String logPayloadFile)
    {
        LogWrapper logObj = new LogWrapper();

        logObj.logType = logType;
        logObj.logSource = logSource;
        logObj.logDetails = LogHelper.setLogDetails(logDetails);
        logObj.logMessage = LogHelper.setLogMessage(logMessage);
        logObj.logSeverity = LogHelper.setLogSeverity(logSeverity,logType);
        
        logObj.logTimeStamp = System.now();
        logObj.logSourceType = logSourceType;
        logObj.logPayloadFile = logPayloadFile;

        return logObj;
    }

    public static void commitLogs(List<LogWrapper> logWrapperList)
    {
        SObject so;
        LogWrapper lw;
        Attachment attFile;
        Attachment attMess;
        Boolean allLogsCreated;
        Set<Id> successIDs = new Set<Id>();
        Set<Id> failureIDs = new Set<Id>();
        List<SObject> lstDLogs = new List<SObject>();
        List<Attachment> lstAttachments = new List<Attachment>();
        
        if(!logWrapperList.isEmpty())
        {
            if(String.isNotBlank(LogHelper.getLogObjectName()))
            {
                lstDLogs = LogHelper.getObjectInstances(logWrapperList);

                if(!lstDLogs.isEmpty())
                {
                    if(!LogHelper.getIsIntegrationLoggingEnabled())
                    {
                        for(Integer i = 0; i < lstDLogs.size(); i++)
                        {
                            if(lstDLogs[i].get('Type__c') == 'Integration')
                            {
                                lstDLogs.remove(i);
                            }
                        }
                    }
                    if(!LogHelper.getIsLoggingEnabled())
                    {
                        for(Integer i = 0; i < lstDLogs.size(); i++)
                        {
                            if(lstDLogs[i].get('Type__c') != 'Integration')
                            {
                                lstDLogs.remove(i);
                            }
                        }
                    }
                }

                if(!lstDLogs.isEmpty())
                {
                    try
                    {
                        Database.SaveResult[] srList = Database.insert(lstDLogs,false);
                        
                        System.Debug('lstDLogs'+lstDLogs);

                        for(Database.SaveResult sr : srList)
                        {
                            if (sr.isSuccess()) 
                            {
                                successIDs.add(sr.getId());
                            }
                            else 
                            {
                                for(Database.Error err : sr.getErrors()) 
                                {

                                }
                            }
                        }

                        if(!successIDs.isEmpty())
                        {
                            for(Integer i = 0; i < logWrapperList.size(); i++)
                            {
                                so = lstDLogs[i];
                                lw = logWrapperList[i];
                                attFile = new Attachment();
                                attMess = new Attachment();
                                if(successIDs.contains((Id)so.get('Id')))
                                {
                                    if((LogHelper.validateId((String)so.get('Id')) != null) && (so.get('Type__c') == 'Integration') && String.isNotBlank(lw.logPayloadFile))
                                    {
                                        attFile = LogHelper.returnAttachmentFromPayload(so, lw);
                                        lstAttachments.add(attFile);
                                    }
                                    if(String.isNotBlank(lw.logMessage) && lw.logMessage.length() > 1000)
                                    {
                                        attMess = LogHelper.returnAttachmentFromMessage(so, lw.logMessage);
                                        lstAttachments.add(attMess);
                                    }
                                }
                                
                                if(!lstAttachments.isEmpty())
                                {
                                    Database.SaveResult[] srAttList = Database.insert(lstAttachments,false);
                                }
                            }
                        }
                    }
                    catch(Exception e)
                    {
                        // Capture logs if any errors here
                    }
                }
            }
        }
    }
}