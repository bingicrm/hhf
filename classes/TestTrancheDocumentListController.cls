@isTest
public class TestTrancheDocumentListController{

    public static testMethod void method1(){
    
     id trancheRecTypeId = Schema.SObjectType.tranche__c.getRecordTypeInfosByName().get('Credit').getRecordTypeId();
     id loanAppRecType = Schema.SObjectType.loan_application__c.getRecordTypeInfosByName().get('Tranche').getRecordTypeId();
     id sourcerecordid = Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('DSA').getRecordTypeId();        
           
    Account acc=new Account();
    acc.name='sdfnjksndvksdn Accoount for trigger';
    acc.phone='9845340895';
    acc.PAN__c='UTHDY5769G';
    acc.Date_of_Incorporation__c=date.today();
    insert acc;
    
    System.debug(acc.id);
    System.debug('here 2');
    
    
    Scheme__c sc=new Scheme__c();
    sc.name='Construction Finance';
    sc.Scheme_Code__c='CF';
    sc.Scheme_Group_ID__c='6';
    sc.Product_Code__c='HL';
    insert sc;
    System.debug(sc.id);
    
    Loan_Application__c lap=new Loan_Application__c ();
    lap.Customer__c=acc.id;
    lap.Transaction_type__c='DAA';
    lap.Scheme__c=sc.id;
    lap.recordtypeid=loanAppRecType;
    //lap.Approved_Loan_Amount__c=5000000;
    lap.Requested_Amount__c = 200000;
    lap.Approved_Loan_Amount__c = 150000;
    lap.StageName__c='Loan Disbursal';
    lap.Sub_Stage__c='Tranche File Check';
    lap.transaction_type__c='PB';
    insert lap;
    System.debug('STAGE  '+lap.Sub_Stage__c);
    System.debug(lap.Sub_Stage__c);
    List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
    Document_Master__c dm=new Document_Master__c();
    dm.name='test';
    dm.Doc_Id__c='asdfsdf';
    dm.Expiration_Possible__c=true;
    dm.FCU_Required__c=true;
    dm.OTC__c=true;
    insert dm;
    
    Document_Type__c dt=new Document_Type__c();
    dt.name='Property Papers';
    dt.Document_Type_Id__c='asfmsdkvns';
    dt.Expiration_Possible__c=true;
    dt.OTC__c=true;
    insert dt;
    
    Tranche__c tr=new Tranche__c();
    tr.Loan_Application__c=lap.id;
    tr.Disbursal_Amount__c=60000;
    insert tr;
    
    Document_Checklist__c dc=new Document_Checklist__c();
    dc.Loan_Applications__c=lap.id;
    dc.Loan_Contact__c=lstofcon[0].id;
    dc.Document_Type__c=dt.id;
    dc.Document_Master__c=dm.id;
    dc.REquest_Date_for_OTC__c=date.today();
    dc.Document_Collection_Mode__c='Photocopy';
    dc.Loan_Engine_Mandatory__c=true;
    dc.Express_Queue_Mandatory__c=true;
    dc.status__c='Pending';
    dc.File_Check_Completed__c=true;
    dc.Scan_Check_Completed__c=true;
    dc.Tranche__c=tr.id;
    dc.Original_Seen_and_Verified__c=true;
    
    insert dc;
    
    List<Document_Checklist__c > ldc=new List<Document_Checklist__c >();
    ldc.add(dc);
    
    
    String st=TrancheDocumentListController.getLoanId(tr.id);
    
    System.assertEquals(st,lap.id);
    
    TrancheDocumentListController.fetchSubStage(tr.id);
    //System.assertEquals(st2,'Application Initiation');
    //TrancheDocumentListController.DocumentListWrapper dlw;
    
    TrancheDocumentListController.DocumentListWrapper dlw1=new TrancheDocumentListController.DocumentListWrapper('Applicant123',ldc);
    List<TrancheDocumentListController.DocumentListWrapper> dlw=new List<TrancheDocumentListController.DocumentListWrapper>();
    dlw.add(dlw1);
    
    TrancheDocumentListController.fetchDocuments(tr.id);
    
    
    }
    
    public static testMethod void method2(){
    
         id trancheRecTypeId = Schema.SObjectType.tranche__c.getRecordTypeInfosByName().get('Credit').getRecordTypeId();
     id loanAppRecType = Schema.SObjectType.loan_application__c.getRecordTypeInfosByName().get('Tranche').getRecordTypeId();
     id sourcerecordid = Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('DSA').getRecordTypeId();        
           
        
    Account acc=new Account();
    acc.name='tesst thisfor controller for trigger';
    acc.phone='9782491385';
    acc.PAN__c='JHSBU8757B';
    acc.Date_of_Incorporation__c=date.today();
    insert acc;
    
    System.debug(acc.id);
    
    Scheme__c sc=new Scheme__c();
    sc.name='Construction Finance';
    sc.Scheme_Code__c='CF';
    sc.Scheme_Group_ID__c='6';
    sc.Product_Code__c='HL';
    insert sc;
    System.debug(sc.id);
    
    Loan_Application__c lap=new Loan_Application__c ();
    lap.Customer__c=acc.id;
    lap.Transaction_type__c='DAA';
    lap.Scheme__c=sc.id;
    lap.recordtypeid=loanAppRecType;
    //lap.Approved_Loan_Amount__c=5000000;
    lap.Requested_Amount__c = 200000;
    lap.Approved_Loan_Amount__c = 150000;
    lap.StageName__c='Loan Disbursal';
    lap.Sub_Stage__c='Tranche File Check';
    lap.transaction_type__c='PB';
    insert lap;
    System.debug('STAGE  '+lap.Sub_Stage__c);
    System.debug(lap.Sub_Stage__c);
    List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
    Document_Master__c dm=new Document_Master__c();
    dm.name='test';
    dm.Doc_Id__c='asdfsdf';
    dm.Expiration_Possible__c=true;
    dm.FCU_Required__c=true;
    dm.OTC__c=true;
    insert dm;
    
    Document_Type__c dt=new Document_Type__c();
    dt.name='Property Papers';
    dt.Document_Type_Id__c='asfmsdkvns';
    dt.Expiration_Possible__c=true;
    dt.OTC__c=true;
    insert dt;
    
    Tranche__c tr=new Tranche__c();
    tr.Loan_Application__c=lap.id;
    tr.Disbursal_Amount__c=60000;
    insert tr;
    
    Document_Checklist__c dc=new Document_Checklist__c();
    dc.Loan_Applications__c=lap.id;
    //dc.Loan_Contact__c=lstofcon[0].id;
    dc.Document_Type__c=dt.id;
    dc.Document_Master__c=dm.id;
    dc.REquest_Date_for_OTC__c=date.today();
    dc.Document_Collection_Mode__c='Photocopy';
    dc.Loan_Engine_Mandatory__c=true;
    dc.Express_Queue_Mandatory__c=true;
    dc.status__c='Pending';
    dc.File_Check_Completed__c=true;
    dc.Scan_Check_Completed__c=true;
    dc.Tranche__c=tr.id;
    dc.Original_Seen_and_Verified__c=true;
    
    insert dc;
    
    List<Document_Checklist__c > ldc=new List<Document_Checklist__c >();
    ldc.add(dc);
    
    
    String st=TrancheDocumentListController.getLoanId(tr.id);
    
    System.assertEquals(st,lap.id);
    
    TrancheDocumentListController.fetchSubStage(tr.id);
    //System.assertEquals(st2,'Application Initiation');
    //TrancheDocumentListController.DocumentListWrapper dlw;
    
    TrancheDocumentListController.DocumentListWrapper dlw1=new TrancheDocumentListController.DocumentListWrapper('Applicant123',ldc);
    List<TrancheDocumentListController.DocumentListWrapper> dlw=new List<TrancheDocumentListController.DocumentListWrapper>();
    dlw.add(dlw1);
    
    TrancheDocumentListController.fetchDocuments(tr.id);
    
    
    }
    
    public static testMethod void method3(){
    
         id trancheRecTypeId = Schema.SObjectType.tranche__c.getRecordTypeInfosByName().get('Credit').getRecordTypeId();
     id loanAppRecType = Schema.SObjectType.loan_application__c.getRecordTypeInfosByName().get('Tranche').getRecordTypeId();
     id sourcerecordid = Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('DSA').getRecordTypeId();        
           
        
    Account acc=new Account();
    acc.name='tesst thisfor controller for trigger';
    acc.phone='9782491385';
    acc.PAN__c='JHSBU8757B';
    acc.Date_of_Incorporation__c=date.today();
    insert acc;
    
    System.debug(acc.id);
    
    Scheme__c sc=new Scheme__c();
    sc.name='Construction Finance';
    sc.Scheme_Code__c='CF';
    sc.Scheme_Group_ID__c='6';
    sc.Product_Code__c='HL';
    insert sc;
    System.debug(sc.id);
    
    Loan_Application__c lap=new Loan_Application__c ();
    lap.Customer__c=acc.id;
    lap.Transaction_type__c='DAA';
    lap.Scheme__c=sc.id;
    lap.recordtypeid=loanAppRecType;
    //lap.Approved_Loan_Amount__c=5000000;
    lap.Requested_Amount__c = 200000;
    lap.Approved_Loan_Amount__c = 150000;
    lap.StageName__c='Loan Disbursal';
    lap.Sub_Stage__c='Tranche File Check';
    lap.transaction_type__c='PB';
    insert lap;
    System.debug('STAGE  '+lap.Sub_Stage__c);
    System.debug(lap.Sub_Stage__c);
    List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
    Document_Master__c dm=new Document_Master__c();
    dm.name='test';
    dm.Doc_Id__c='asdfsdf';
    dm.Expiration_Possible__c=true;
    dm.FCU_Required__c=true;
    dm.OTC__c=true;
    insert dm;
    
    Document_Type__c dt=new Document_Type__c();
    dt.name='Property Papers';
    dt.Document_Type_Id__c='asfmsdkvns';
    dt.Expiration_Possible__c=true;
    dt.OTC__c=true;
    insert dt;
    
    Tranche__c tr=new Tranche__c();
    tr.Loan_Application__c=lap.id;
    tr.Disbursal_Amount__c=60000;
    insert tr;
    
    Document_Checklist__c dc=new Document_Checklist__c();
    dc.Loan_Applications__c=lap.id;
    //dc.Loan_Contact__c=lstofcon[0].id;
    dc.Document_Type__c=dt.id;
    dc.Document_Master__c=dm.id;
    dc.REquest_Date_for_OTC__c=date.today();
    dc.Document_Collection_Mode__c='Photocopy';
    dc.Loan_Engine_Mandatory__c=true;
    dc.Express_Queue_Mandatory__c=true;
    dc.status__c='Pending';
    dc.File_Check_Completed__c=true;
    dc.Scan_Check_Completed__c=true;
    dc.Tranche__c=tr.id;
    dc.Original_Seen_and_Verified__c=true;
    
    insert dc;
    
    List<Document_Checklist__c > ldc=new List<Document_Checklist__c >();
    ldc.add(dc);
    
    
    TrancheDocumentListController.getLoanId(tr.id);
    
    //System.assertEquals(st,lap.id);
    
    TrancheDocumentListController.fetchSubStage(tr.id);
    //System.assertEquals(st2,'Application Initiation');
    //TrancheDocumentListController.DocumentListWrapper dlw;
    
    TrancheDocumentListController.DocumentListWrapper dlw1=new TrancheDocumentListController.DocumentListWrapper('Applicant123',ldc);
    List<TrancheDocumentListController.DocumentListWrapper> dlw=new List<TrancheDocumentListController.DocumentListWrapper>();
    dlw.add(dlw1);
    
    //System.assertEquals(dlw,TrancheDocumentListController.fetchDocuments(tr.id));
    List < String > allOpts = new list < String > ();
    //allOpts.clear();
    allOpts.add('Pending');
    allOpts.add('Uploaded');
    allOpts.add('Received'); 
    sobject sh=new  Loan_Application__c(id=lap.id);
    TrancheDocumentListController.getselectOptions(dc,'Status__c','Tranche');
    }
    
    public static testmethod void method4(){
    
         id trancheRecTypeId = Schema.SObjectType.tranche__c.getRecordTypeInfosByName().get('Credit').getRecordTypeId();
     id loanAppRecType = Schema.SObjectType.loan_application__c.getRecordTypeInfosByName().get('Tranche').getRecordTypeId();
     id sourcerecordid = Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('DSA').getRecordTypeId();        
           
        
    Account acc=new Account();
    acc.name='tesst thisfor controller for trigger';
    acc.phone='9782491385';
    acc.PAN__c='JHSBU8757B';
    acc.Date_of_Incorporation__c=date.today();
    insert acc;
    
    System.debug(acc.id);
    
    Scheme__c sc=new Scheme__c();
    sc.name='Construction Finance';
    sc.Scheme_Code__c='CF';
    sc.Scheme_Group_ID__c='6';
    sc.Product_Code__c='HL';
    insert sc;
    System.debug(sc.id);
    
    Loan_Application__c lap=new Loan_Application__c ();
    lap.Customer__c=acc.id;
    lap.Transaction_type__c='DAA';
    lap.Scheme__c=sc.id;
    lap.recordtypeid=loanAppRecType;
    //lap.Approved_Loan_Amount__c=5000000;
    lap.Requested_Amount__c = 200000;
    lap.Approved_Loan_Amount__c = 150000;
    lap.StageName__c='Loan Disbursal';
    lap.Sub_Stage__c='Tranche File Check';
    lap.transaction_type__c='PB';
    insert lap;
    System.debug('STAGE  '+lap.Sub_Stage__c);
    System.debug(lap.Sub_Stage__c);
    List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
    Document_Master__c dm=new Document_Master__c();
    dm.name='test';
    dm.Doc_Id__c='asdfsdf';
    dm.Expiration_Possible__c=true;
    dm.FCU_Required__c=true;
    dm.OTC__c=true;
    insert dm;
    
    Document_Type__c dt=new Document_Type__c();
    dt.name='Property Papers';
    dt.Document_Type_Id__c='asfmsdkvns';
    dt.Expiration_Possible__c=true;
    dt.OTC__c=true;
    insert dt;
    
    Tranche__c tr=new Tranche__c();
    tr.Loan_Application__c=lap.id;
    tr.Disbursal_Amount__c=60000;
    insert tr;
    
    Document_Checklist__c dc=new Document_Checklist__c();
    dc.Loan_Applications__c=lap.id;
    //dc.Loan_Contact__c=lstofcon[0].id;
    dc.Document_Type__c=dt.id;
    dc.Document_Master__c=dm.id;
    dc.REquest_Date_for_OTC__c=date.today();
    dc.Document_Collection_Mode__c='Photocopy';
    dc.Loan_Engine_Mandatory__c=true;
    dc.Express_Queue_Mandatory__c=true;
    dc.status__c='Pending';
    dc.File_Check_Completed__c=true;
    dc.Scan_Check_Completed__c=true;
    dc.Tranche__c=tr.id;
    dc.Original_Seen_and_Verified__c=true;
    
    insert dc;
    
    Attachment at=new Attachment();
    String existi='asdfbsdvnjksnvjkosnvjionsdjiof';
    at.Body = EncodingUtil.base64Decode(existi);
    at.contentType='pdf';
    at.name='testfile';
    at.parentid=dc.id;
    insert at;
    String base64data='safsd';
    String contentType1='pdf';
    TrancheDocumentListController.saveChunk(dc.id,'testfile',base64data,contentType1,at.id);
    
    TrancheDocumentListController.saveChunk(dc.id,'testfile',base64data,contentType1,'');
    String dcid=String.valueof(dc.id);
    
    TrancheDocumentListController.queryAttachments(dc.id);
    TrancheDocumentListController.queryAttachments(lap.id);
    
    String paramjson='[{"strContactName":"tesst","lstDocumentCheckList":[{"id":"'+dc.id+'"}]}]';
    
    TrancheDocumentListController.saveLoan(paramjson);
    TrancheDocumentListController.validateChkList(paramjson);
    String paramjson1='[{"strContactName":"tesst","lstDocumentCheckList":[{"id":""}]}]';
    TrancheDocumentListController.saveLoan(paramjson1);
    TrancheDocumentListController.validateChkList(paramjson1);
    
    

}

}