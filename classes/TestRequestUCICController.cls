@isTest
public class TestRequestUCICController {
@testSetup static void testData() {
    		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        Profile p1 =[SELECT Id FROM Profile WHERE Name='Standard User'];
                
        User u1 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser001@amamama.com',
                     Username = 'puser001@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US'
                     );
                     
        insert u1;
        
        User u2 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser002@amamama.com',
                     Username = 'puser002@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US'
                     );
                     
        insert u2;
                
        User u3 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser003@amamama.com',
                     Username = 'puser003@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US'
                     );
                     
        insert u3;
                
        User u4 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser004@amamama.com',
                     Username = 'puser004@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US'
                     );
                     
        insert u4;
               
       
        User u5 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser005@amamama.com',
                     Username = 'puser005@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US'
                     );
                     
        insert u5;
            
            Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
                        
            Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                       Phone='9989786750',RecordTypeId=RecordTypeIdAccount, Customer_ID__c='1234589756754');
            insert acc1;
    		Account acc2 = new Account(PAN__c='ABCDE2234G',Name='Test Account2',Date_of_Incorporation__c=System.today(),
                                       Phone='9989782750',RecordTypeId=RecordTypeIdAccount, Customer_ID__c='1234589756754');
            insert acc2;
            
            acc1 =[Select Id, PAN__c, Name, Date_of_Incorporation__c, Phone, RecordTypeId, Customer_ID__c From Account where Id =: acc1.Id];
            Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
            insert con1;
            
            Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                                Scheme_Group_ID__c='12');
            insert objScheme;
            
            Loan_Application__c la1 = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                              Transaction_type__c='PB',Requested_Amount__c=100000,
                                                              Loan_Purpose__c='11', Loan_Number__c ='12345',branch__c='test',Line_Of_Business__c='Open Market');
            insert la1;

   /* Contact cnt2 = new Contact (LastName = 'LNameLoanContact', AccountId = acc2.Id, MobilePhone = '9999667739');
        insert cnt2;
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Guarantor').getRecordTypeId();
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=la1.Id,Customer__c=acc2.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='2',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='18',
                                                             Customer_segment__c='9',Applicant_Status__c='1',
                                                             GSTIN_Available__c=true, Contact__c = cnt2.Id,
                                                             GSTIN_Number__c = '27AAHFG0551H1ZL',
                                                             income_considered__c = false);
        insert objLoanContact;*/
            Branch_LOB_User_Mapping__c BLUMob= new Branch_LOB_User_Mapping__c(branch__c='test',Line_Of_Business__c='Open Market',BH__c=u1.id,NCH__c=u2.id,CCH__c=u3.id,ZCH__c=u4.id,CRO__c=u5.id,CEO__c=u5.id, Policy_Head__c=u5.id);
            insert BLUMob;

    }
    
    public static testMethod void makeRequestUCICTest(){
        Account accObj = [SELECT Id,Customer_ID__c FROM Account WHERE Name='Test Account1' LIMIT 1];
        Branch_LOB_User_Mapping__c branchLOBUserMappingObj = [SELECT Id,BH__c, NCH__c, CCH__c,ZCH__c,CRO__c, CEO__c, Policy_Head__c FROM Branch_LOB_User_Mapping__c WHERE branch__c='test' LIMIT 1];
        Loan_Application__c la = [SELECT Id FROM Loan_Application__c LIMIT 1];
        RequestUCICController.checkEligibility(la.Id);
    }
    
    
}