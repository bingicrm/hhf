public class UCICDecisionCallout {
    
    @future(callout = true) 
    public static void makeUCICDecisionCallout(Set<Id> setLAIds){
        if(setLAIds.size() > 0){
            List<Loan_Application__c> lstLA = [SELECT
                                               Id,
                                               Name,
                                               StageName__c,
                                               Sub_Stage__c,
                                               Loan_Number__c,
                                               Approved_Loan_Amount__c,
                                               Approved_Loan_Tenure__c,
                                               Product_Name__c,
                                               Rejection_Reason__c,
                                               Remarks_Rejection__c,
                                               Cancellation_Reason__c,
                                               Remarks__c
                                               FROM Loan_Application__c
                                               WHERE Id IN: setLAIds];
            
            List<Rest_Service__mdt> lstRestService = [SELECT
                                                      MasterLabel,
                                                      QualifiedApiName, 
                                                      Service_EndPoint__c, 
                                                      Client_Password__c, 
                                                      Client_Username__c, 
                                                      Request_Method__c, 
                                                      Time_Out_Period__c
                                                      FROM Rest_Service__mdt
                                                      WHERE DeveloperName = 'UCICDecisionCallout'];
            
            System.debug('lstRestService==>'+lstRestService);
            
            List<Rejection_Category_Mapping__mdt> lstRejectCategory = [SELECT
                                                                       Rejection_Reason__c,
                                                                       Rejection_Category__c,
                                                                       Active__c
                                                                       FROM Rejection_Category_Mapping__mdt
                                                                       WHERE Active__c = true];
            
            Map<String,Rejection_Category_Mapping__mdt> mapRejectCategory = new Map<String,Rejection_Category_Mapping__mdt>();
            
            for(Rejection_Category_Mapping__mdt rcm: lstRejectCategory){
                mapRejectCategory.put(rcm.Rejection_Reason__c,rcm);
            }
            
            List<UCICDecisionRequestBody> lstReqbody = new List<UCICDecisionRequestBody>();
            for(Loan_Application__c la: lstLA){
                UCICDecisionRequestBody reqBody = new UCICDecisionRequestBody();
                reqBody.APPLICATION_ID = la.Loan_Number__c != null ? la.Loan_Number__c : '';
                reqBody.SOURCE_CHANNEL = 'SFDC-HL';
                reqBody.LOAN_ACCOUNT_NUMBER = '';
                reqBody.LOAN_APPLICATION_ID = la.Loan_Number__c != null ? la.Loan_Number__c : '';
                reqBody.NUMBER_OF_BOUNCES = '';
                reqBody.SOURCE_APPLICATION_ID = la.Loan_Number__c != null ? la.Loan_Number__c : '';
                
                if(la.Sub_Stage__c == Constants.Customer_Negotiation){
                    reqBody.LOAN_STATUS = Constants.STATUS_APPROVAL;
                }
                else if(la.Sub_Stage__c == Constants.Loan_Cancel){
                    reqBody.LOAN_STATUS = Constants.STATUS_CANCELLED;
                }
                else if(la.Sub_Stage__c == Constants.Loan_reject){
                    reqBody.LOAN_STATUS = Constants.STATUS_REJECTED;
                }
                else{
                    reqBody.LOAN_STATUS = '';
                }
                
                reqBody.LOAN_BUCKET = '';
                reqBody.DPD = '';
                reqBody.POS = '';
                reqBody.MOB = '';
                reqBody.PRODUCT = la.Product_Name__c != null ? la.Product_Name__c : '';
                reqBody.INSTLNUM_DUE = '';
                reqBody.TENURE = la.Approved_Loan_Tenure__c != null ? String.valueOf(la.Approved_Loan_Tenure__c) : '';
                reqBody.DISBURSAL_DATE = '';
                reqBody.NPA_STAGE = '';
                reqBody.DEMOGRAPHIC_DETAIL = '';
                reqBody.LOAN_AMOUNT = la.Approved_Loan_Amount__c != null ? String.valueOf(la.Approved_Loan_Amount__c) : '';
                reqBody.DISBURSAL_AMOUNT = '';
                reqBody.PRINCIPAL_OS = '';
                reqBody.RC_DETAILS = '';
                reqBody.EMI_AMOUNT = '';
                reqBody.ASSET_MODEL = '';
                reqBody.REGISTRATION_NO = '';
                reqBody.COLLATERAL_ADDRESS = '';
                reqBody.PRIMARY_CUSTOMER_CIF_NO = '';
                
                Datetime dt = Datetime.now();
                reqBody.LASTUPDATEDDATE = dt.format('dd-MMM-yy hh:mm:ss a');
                
                if(la.Sub_Stage__c == Constants.Loan_reject){
                    if(!mapRejectCategory.isEmpty() && mapRejectCategory.containsKey(la.Rejection_Reason__c) && la.Rejection_Reason__c != null){
                        reqBody.PER_FILLER_1 = mapRejectCategory.get(la.Rejection_Reason__c).Rejection_Category__c;
                    }
                    else{
                        reqBody.PER_FILLER_1 = '';
                    }
                }
                else if(la.Sub_Stage__c == Constants.Loan_Cancel){
                    if(!mapRejectCategory.isEmpty() && mapRejectCategory.containsKey(la.Cancellation_Reason__c) && la.Cancellation_Reason__c != null){
                        reqBody.PER_FILLER_1 = mapRejectCategory.get(la.Cancellation_Reason__c).Rejection_Category__c;
                    }
                    else{
                        reqBody.PER_FILLER_1 = '';
                    }
                }
                else{
                    reqBody.PER_FILLER_1 = '';
                }
                
                if(la.Sub_Stage__c == Constants.Loan_Cancel){
                    reqBody.PER_FILLER_2 = la.Cancellation_Reason__c != null ? la.Cancellation_Reason__c : '';
                }
                else if(la.Sub_Stage__c == Constants.Loan_reject){
                    reqBody.PER_FILLER_2 = la.Rejection_Reason__c != null ? la.Rejection_Reason__c : '';
                }
                else{
                    reqBody.PER_FILLER_2 = '';
                }
                
                if(la.Sub_Stage__c == Constants.Loan_Cancel){
                    reqBody.PER_FILLER_3 = la.Remarks__c != null ? la.Remarks__c : '';
                }
                else if(la.Sub_Stage__c == Constants.Loan_reject){
                    reqBody.PER_FILLER_3 = la.Remarks_Rejection__c != null ? la.Remarks_Rejection__c : '';
                }
                else{
                    reqBody.PER_FILLER_3 = '';
                }
                
                reqBody.PER_FILLER_4 = '';
                reqBody.PER_FILLER_5 = '';
                reqBody.PER_FILLER_6 = '';
                reqBody.PER_FILLER_7 = '';
                reqBody.PER_FILLER_8 = '';
                reqBody.PER_FILLER_9 = '';
                reqBody.PER_FILLER_10 = '';
                reqBody.PER_FILLER_11 = '';
                reqBody.PER_FILLER_12 = '';
                reqBody.PER_FILLER_13 = '';
                reqBody.PER_FILLER_14 = '';
                reqBody.PER_FILLER_15 = '';
                lstReqbody.add(reqBody);
            }
            
            for(UCICDecisionRequestBody rb: lstReqbody){
                String jsonRequest = Json.serializePretty(rb);
                
                Blob headerValue = Blob.valueOf(lstRestService[0].Client_Username__c + ':' + lstRestService[0].Client_Password__c);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                Map<String,String> headerMap = new Map<String,String>();
                headerMap.put('Authorization',authorizationHeader);
                headerMap.put('Username',lstRestService[0].Client_Username__c);
                headerMap.put('Password',lstRestService[0].Client_Password__c);
                headerMap.put('Content-Type','application/json');
                
                HTTP http = new HTTP();
                HTTPRequest httpReq = new HTTPRequest();
                
                if(String.isNotBlank(lstRestService[0].Service_EndPoint__c)) {
                    httpReq.setEndPoint(lstRestService[0].Service_EndPoint__c);
                }
                else {
                    System.debug('Debug Log for error : Endpoint Empty. Please specify endpoint in metadata record.');
                }
                
                if(String.isNotBlank(lstRestService[0].Request_Method__c)) {
                    httpReq.setMethod(lstRestService[0].Request_Method__c);
                }
                else {
                    System.debug('Debug Log for error : Requested Method Empty. Please specify Requested Method in metadata record.');
                }
                
                for(String headerKey : headerMap.keySet()){
                    httpReq.setHeader(headerKey, headerMap.get(headerKey));
                }
                
                httpReq.setBody(jsonRequest);
                
                if(String.isNotBlank(String.valueOf(lstRestService[0].Time_Out_Period__c))) {
                    httpReq.setTimeOut(Integer.valueOf(lstRestService[0].Time_Out_Period__c));
                }
                
                UCICDecisionResponseBody resBody = new UCICDecisionResponseBody();
                
                try{
                	HTTPResponse httpRes = http.send(httpReq);
                    
                    //Create integration logs
                    LogUtility.createIntegrationLogs(jsonRequest,httpRes,lstRestService[0].Service_EndPoint__c);
                    System.debug('Debug Log for response'+httpRes);
                    System.debug('Debug Log for response body'+httpRes.getBody());
                    System.debug('Debug Log for response status'+httpRes.getStatus());
                    System.debug('Debug Log for response status code'+httpRes.getStatusCode());
                    
                    resBody = (UCICDecisionResponseBody)JSON.deserialize(httpRes.getBody(), UCICDecisionResponseBody.class);
                    
                    if(httpRes.getStatus() == 'OK' && httpRes.getStatusCode() == 200) {
                        if(resBody != null){
                            
                        }
                    }
                }
                catch(Exception ex) {
                    System.debug('Debug Log for exception'+ex);
                }
            }
        }
    }
}