@isTest
public class TestdocumentControllerFCU{
    
    public static testMethod void checkUserRoleTest(){
        Profile p1=[SELECT id,name from profile where name=:'Credit Team'];
        UserRole r = [Select Id From UserRole Where Name =: 'BCM - Delhi 1.1' limit 1];
        
        User u1 = new User(
            ProfileId = p1.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id, Role_in_Credit_Hierarchy__c = 'BCM'
        ); 
        insert u1;
        
        system.debug('u1' + u1);
        system.runAs(u1){
            UserRole ur = new UserRole(Name = 'BCM');
            insert ur;
            documentControllerFCU.checkUserRole();
        }
    }
    
    public static testMethod void documentControllerFCUTest(){
        Profile p=[SELECT id,name from profile where name=:'Sales Team'];
        UserRole r = [Select Id From UserRole Where Name =: 'BCM - Delhi 1.1' limit 1];
        Profile p1=[SELECT id,name from profile where name=:'Credit Team'];
		        Hierarchy__c objHierarchy = new Hierarchy__c();
        /*objHierarchy.BCM__c = u2.Id;
        objHierarchy.ZCM__c = u4.Id;
        objHierarchy.CCM__c = u2.Id;
        objHierarchy.CRO__c = u2.Id;*/
        insert objHierarchy;
        List<Hierarchy__c> lstHierarchy = [Select Id, Name, Active__c, BCM__c, BH__c, CCM__c, CRO__c, NCM__c, ZCM__c, ZCM_Salaried__c from Hierarchy__c where Active__c = true];
        String HierarchyName = lstHierarchy[0].Name; 
        User u1 = new User(
            ProfileId = p1.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,
            FCU__c =true,Hierarchy__c = HierarchyName,
            	     Non_Salaried_Authority__c = 'A2',
            		 Salaried_Authority__c = 'A2',Role_in_Credit_Hierarchy__c = 'BCM'
        ); 
        insert u1;
        
        Profile p2=[SELECT id,name from profile where name=:'Third Party Vendor'];
        User u2 = new User(
            ProfileId = p2.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,
            FCU__c= true,
            //Location__c = 'Mumbai', getting error as 
            Role_Assigned_Date__c = datetime.newInstance(2020, 9, 15, 12, 30, 0)
            
        ); 
        //database.insert(u2);
        
        User thisUser = [ select Id,Name from User where Id = :UserInfo.getUserId() ];
        system.runAs(thisUser){
            UserRole ru = [Select Id,Name From UserRole Where name=: 'BCM - Delhi 1.1'];
            Account custObj = new Account();
            custObj.Name = 'Test Customer';
            custObj.Phone = '9999999999';
            database.insert(custObj);
            
            scheme__c schemeObj = new scheme__c();
            schemeObj.Name = 'Scheme Name';
            schemeObj.Scheme_Group_ID__c = '12345';
            schemeObj.Scheme_Code__c = '12345';
            database.insert(schemeObj);
            
            
            Loan_Application__c loanAppObj = new Loan_Application__c();
            loanAppObj.customer__c = custObj.id;
            loanAppObj.Loan_Application_Number__c = 'LA00001';
            loanAppObj.Loan_Purpose__c = '11';
            loanAppObj.scheme__c = schemeObj.id;
            loanAppObj.Transaction_type__c = 'PB';
            loanAppObj.Requested_Amount__c = 50; 
			loanAppObj.Credit_Manager_User__c= u1.Id;
            //Database.insert(loanAppObj);
            insert loanAppObj;
            
            Cross_Sell_Partner_Name__c CSPN=new Cross_Sell_Partner_Name__c(Name='Test');
            Database.insert(CSPN);
            
            DST_Master__c DSTMaster= new DST_Master__c(Name='Test DST',Inspector_ID__c='1234'); 
            Database.insert(DSTMaster);
            
            Sourcing_Detail__c SDob= new Sourcing_Detail__c(Loan_Application__c=loanAppObj.id,Source_Code__c='16',
                                                            DST_Name__c=DSTMaster.id,
                                                            //Sales_User_Name__c=u.id,
                                                            Cross_Sell_Partner_Name__c=CSPN.id);
            
            Database.insert(SDob);    
            test.startTest();
            loanAppObj.StageName__c='Operation Control';
            loanAppObj.Sub_Stage__c='Scan: Data Maker';
            update loanAppObj;
            Id rt = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Loan Disbursal').getRecordTypeId();
            loanAppObj.StageName__c='Loan Disbursal';
            //loanAppObj.Sub_Stage__c='Tranche Processing';
            loanAppObj.RecordtypeId = rt; 
            update loanAppObj;
            
            Third_Party_Verification__c tpvObj = new Third_Party_Verification__c();
            tpvObj.Loan_Application__c = loanAppObj.id;
            tpvObj.Status__c = 'New';
            //tpvObj.Owner__c = /*UserInfo.getUserId();*/ u1.Id;    
            Database.insert(tpvObj); 
            
            
            
            Loan_Contact__c loanConObj = new Loan_Contact__c();
            loanConObj.Loan_Applications__c = loanAppObj.id;
            //loanConObj.Customer_segment__c = 'Salaried';
            //loanConObj.Customer__c = custObj.id;
            loanConObj.Applicant_Type__c = 'Co- Applicant';
            loanConObj.Applicant_Status__c = 'Resident';
            //Database.insert(loanConObj);
            
            Document_Master__c docMasObj = new Document_Master__c();
            docMasObj.Name = 'Test';
            docMasObj.Doc_Id__c = '123344';
            docMasObj.Approving_authority__c = 'Business Head';
            Database.insert(docMasObj);
            
            Document_type__c docTypeObj = new Document_type__c();
            docTypeObj.Name = 'Test Document Type';
            docTypeObj.Approving_authority__c    = 'Business Head';
            Database.insert(docTypeObj);
            
            Document_Type__c DType=new Document_Type__c(Name='Property Papers');
            insert DType;
            
            
            DocumentType_Document_Mapping__c mapObj = new DocumentType_Document_Mapping__c();
            mapObj.Document_Master__c = docMasObj.id;
            mapObj.Document_Type__c = docTypeObj.id;
            Database.insert(mapObj);
            Set<Id> ids = new Set<Id>();
            List<Document_Checklist__c> lstofDocs = new List<Document_Checklist__c>();
            Document_Checklist__c docObj = new Document_Checklist__c();
            docObj.Loan_Applications__c = loanAppObj.id;
            docObj.status__c = 'Pending';
            docObj.document_type__c = docTypeObj.id;
            docObj.Document_Master__c = docMasObj.id;
            docObj.Loan_Contact__c = loanConObj.id;
            docObj.Express_Queue_Mandatory__c = true;
            docObj.Document_Collection_Mode__c = 'Photocopy';
            docObj.Screened_p__c = 'Yes';
            docObj.Sampled_p__c = 'Yes';
            lstofDocs.add(docObj);
            ids.add(docObj.id);
            Database.insert(docObj);
            Document_Checklist__c docObj1 = new Document_Checklist__c();
            docObj1.Loan_Applications__c = loanAppObj.id;
            docObj1.status__c = 'Pending';
            docObj1.document_type__c = docTypeObj.id;
            docObj1.Document_Master__c = docMasObj.id;
            docObj1.Loan_Contact__c = loanConObj.id;
            docObj1.Express_Queue_Mandatory__c = true;
            docObj1.Document_Collection_Mode__c = 'Photocopy';
            docObj1.Screened_p__c = 'Yes';
            docObj1.Sampled_p__c = 'Yes';
            docObj1.FCU_Done__c = True;
            lstofDocs.add(docObj1);
            ids.add(docObj1.id);    
            Database.insert(docObj1);
            
            List<Id> idList = new List<Id>();    
            //List<Property_Document_Checklist__c> lstofPDC = new List<Property_Document_Checklist__c>(); 
            Property_Document_Checklist__c pd = new Property_Document_Checklist__c(Document_Checklist__c=docObj.id, Loan_Application__c=loanAppObj.id, Third_Party_Verification__c=tpvObj.id);
            //lstofPDC.add(pd);
            idList.add(docObj.id);
            Database.insert(pd);
            List<Document_Checklist__c> docList = [Select Id, Name, Document_Master__r.Name, Document_Collection_Mode__c, Screened_p__c, Sampled_p__c 
                                                   from Document_Checklist__c where Id =: idList[0]];
            
            Document_Checklist__c docObj2 = new Document_Checklist__c();
            docObj2.Loan_Applications__c = loanAppObj.id;
            docObj2.status__c = 'Pending';
            docObj2.document_type__c = docTypeObj.id;
            docObj2.Document_Master__c = docMasObj.id;
            docObj2.Loan_Contact__c = loanConObj.id;
            docObj2.Express_Queue_Mandatory__c = true;
            docObj2.Document_Collection_Mode__c = 'Photocopy';
            docObj2.Screened_p__c = 'Yes';
            docObj2.Sampled_p__c = 'Yes';
            docObj2.FCU_Done__c = False;
            
            Database.insert(docObj2);
            
            
            Property_Document_Checklist__c pd1 = new Property_Document_Checklist__c(Document_Checklist__c=docObj2.id, Loan_Application__c=loanAppObj.id, Third_Party_Verification__c=tpvObj.id);
            insert pd1;
            //test.stoptest();
            List<Document_checkList__c> docCheckList = new List<Document_checkList__c>();
            docCheckList.add(docObj);
            
            Property_Document_Checklist__c propDocObj = new Property_Document_Checklist__c();
            propDocObj.Document_Checklist__c = docObj.id;
            propDocObj.Loan_Application__c = loanAppObj.id;
            propDocObj.Third_Party_Verification__c = tpvObj.id;
            Database.insert(propDocObj);
            ContentVersion conVer = new ContentVersion();
            conVer.Title = 'CZDSTOU';
            conVer.PathOnClient = 'Test';
            conVer.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body');
            
            List<contentVersion> conVlist = new List<contentVersion>();
            conVlist.add(conVer);
            database.insert(conVList);
            
            List<contentDocument> documents = [select id, Title, LatestPublishedVersionId from contentDocument];
            contentDocumentLink cdlObj = new contentDocumentLink();
            cdlObj.linkedEntityId = docObj.id;
            cdlObj.shareType = 'I';
            cdlObj.contentDocumentId = documents[0].id;
            cdlObj.Visibility = 'AllUsers';
            Database.insert(cdlObj);
            String paramContactName = 'Test contact';
            String errorMsg = 'Error';
            //controller.DocumentListWrapper docWrap = new controller.DocumentListWrapper(lstofDocs,paramContactName,errorMsg);
            //documentControllerFCU controller = new documentControllerFCU();
            documentControllerFCU.fetchDocList(tpvObj.id);
            
            Lead objLead = new Lead();
            documentControllerFCU.getselectOptions(objLead,'Call_Status__c');
            documentControllerFCU.getMode();
            documentControllerFCU.getSampled();
            documentControllerFCU.getScreened();
            documentControllerFCU.fetchProfileName();
            documentControllerFCU.fetchAllDocs(docObj1.id);
            documentControllerFCU.fetchDocList1(pd.id);
            
            
            List<Document_checkList__c> docCheckList1 = new List<Document_checkList__c>();
            docCheckList1.add(docObj2);
            
            Property_Document_Checklist__c propDocObj1 = new Property_Document_Checklist__c();
            propDocObj1.Document_Checklist__c = docObj2.id;
            propDocObj1.Loan_Application__c = loanAppObj.id;
            propDocObj1.Third_Party_Verification__c = tpvObj.id;
            //Database.insert(propDocObj);
            ContentVersion conVer1 = new ContentVersion();
            conVer1.Title = 'CZDSTOU';
            conVer1.PathOnClient = 'Test';
            conVer1.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body');
            
            List<contentVersion> conVlist1 = new List<contentVersion>();
            conVlist1.add(conVer1);
            database.insert(conVlist1);
            
            List<contentDocument> documents1 = [select id, Title, LatestPublishedVersionId from contentDocument];
            contentDocumentLink cdlObj1 = new contentDocumentLink();
            cdlObj1.linkedEntityId = docObj2.id;
            cdlObj1.shareType = 'I';
            cdlObj1.contentDocumentId = documents1[0].id;
            cdlObj1.Visibility = 'AllUsers';
            Database.insert(cdlObj1);	
            
            List<User_Branch_Mapping__c> lstofUB = new List<User_Branch_Mapping__c>();    
            /*User_Branch_Mapping__c userBrnch = new User_Branch_Mapping__c(User__c=u1.id);
lstofUB.add(userBrnch);
insert lstofUB; */
            
            Branch_Master__c BMobject=new Branch_Master__c(Name='Test',Office_Code__c='123456');
            Database.insert(BMobject);
            loanAppObj.Branch_Lookup__c = BMobject.id;
            update  loanAppObj;  
            User_Branch_Mapping__c UBMob= new User_Branch_Mapping__c(Sourcing_Branch__c=BMobject.id,Servicing_Branch__c=BMobject.id,
                                                                     User__c=u1.id); 
            Database.insert(UBMob);  
            
            List<FCU_Document_Checklist__c> lstofFCU = new List<FCU_Document_Checklist__c>();
            FCU_Document_Checklist__c FCU = new FCU_Document_Checklist__c(Loan_Application__c=loanAppObj.id,Document_Checklist__c=docObj2.Id,
                                                                          Third_Party_Verification__c=tpvObj.id);
            lstofFCU.add(FCU);   
            insert lstofFCU;
            String paramJSONList = JSON.serialize(lstofFCU);  
            documentControllerFCU.saveLoan(paramJSONList,true,tpvObj.id); 
            documentControllerFCU.queryAttachments(docObj.id,true);
            documentControllerFCU.queryAttachments(tpvObj.id,true);
            //documentControllerFCU.checkUserRole();
            documentControllerFCU.checkLAFCUBRDExemption(tpvObj.id);
            documentControllerFCU.checkLAFCUBRDExemption1(loanAppObj.id);
            User_Branch_Mapping__c ubList = new User_Branch_Mapping__c();    
            String docIdsList = JSON.serialize(ids); 
            documentControllerFCU.createFreshFCUTPV(docIdsList,loanAppObj.id);    
            test.stoptest();
        }
    }
    
    public static testMethod void documentControllerFCUTest1(){
        Profile p=[SELECT id,name from profile where name=:'Sales Team'];
        
        User thisUser = [ select Id,Name from User where Id = :UserInfo.getUserId() ];
        system.runAs(thisUser){
            UserRole ru = [Select Id,Name From UserRole Where name=: 'BCM - Delhi 1.1'];
            Account custObj = new Account();
            custObj.Name = 'Test Customer';
            custObj.Phone = '9999999999';
            database.insert(custObj);
            
            scheme__c schemeObj = new scheme__c();
            schemeObj.Name = 'Scheme Name';
            schemeObj.Scheme_Group_ID__c = '12345';
            schemeObj.Scheme_Code__c = '12345';
            database.insert(schemeObj);
            
            
            Loan_Application__c loanAppObj = new Loan_Application__c();
            loanAppObj.customer__c = custObj.id;
            loanAppObj.Loan_Application_Number__c = 'LA00001';
            loanAppObj.Loan_Purpose__c = '11';
            loanAppObj.scheme__c = schemeObj.id;
            loanAppObj.Transaction_type__c = 'PB';
            loanAppObj.Requested_Amount__c = 50;
            //Database.insert(loanAppObj);
            insert loanAppObj;
            
            Cross_Sell_Partner_Name__c CSPN=new Cross_Sell_Partner_Name__c(Name='Test');
            Database.insert(CSPN);
            
            DST_Master__c DSTMaster= new DST_Master__c(Name='Test DST',Inspector_ID__c='1234'); 
            Database.insert(DSTMaster);
            
            Sourcing_Detail__c SDob= new Sourcing_Detail__c(Loan_Application__c=loanAppObj.id,Source_Code__c='16',
                                                            DST_Name__c=DSTMaster.id,
                                                            //Sales_User_Name__c=u.id,
                                                            Cross_Sell_Partner_Name__c=CSPN.id);
            
            Database.insert(SDob);    
            test.startTest();
            loanAppObj.StageName__c='Operation Control';
            loanAppObj.Sub_Stage__c='Scan: Data Maker';
            update loanAppObj;
            Id rt = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Loan Disbursal').getRecordTypeId();
            loanAppObj.StageName__c='Loan Disbursal';
            //loanAppObj.Sub_Stage__c='Tranche Processing';
            loanAppObj.RecordtypeId = rt; 
            update loanAppObj;
            
            Third_Party_Verification__c tpvObj = new Third_Party_Verification__c();
            tpvObj.Loan_Application__c = loanAppObj.id;
            tpvObj.Status__c = 'New';    
            insert tpvObj;    
            //Database.insert(tpvObj); 
            
            
            
            Loan_Contact__c loanConObj = new Loan_Contact__c();
            loanConObj.Loan_Applications__c = loanAppObj.id;
            //loanConObj.Customer_segment__c = 'Salaried';
            //loanConObj.Customer__c = custObj.id;
            loanConObj.Applicant_Type__c = 'Co- Applicant';
            loanConObj.Applicant_Status__c = 'Resident';
            //Database.insert(loanConObj);
            
            Document_Master__c docMasObj = new Document_Master__c();
            docMasObj.Name = 'Test';
            docMasObj.Doc_Id__c = '123344';
            docMasObj.Approving_authority__c = 'Business Head';
            Database.insert(docMasObj);
            
            Document_type__c docTypeObj = new Document_type__c();
            docTypeObj.Name = 'Test Document Type';
            docTypeObj.Approving_authority__c    = 'Business Head';
            Database.insert(docTypeObj);
            
            Document_Type__c DType=new Document_Type__c(Name='Property Papers');
            insert DType;
            
            
            DocumentType_Document_Mapping__c mapObj = new DocumentType_Document_Mapping__c();
            mapObj.Document_Master__c = docMasObj.id;
            mapObj.Document_Type__c = docTypeObj.id;
            Database.insert(mapObj);
            List<Document_Checklist__c> lstofDocs = new List<Document_Checklist__c>();
            Document_Checklist__c docObj = new Document_Checklist__c();
            docObj.Loan_Applications__c = loanAppObj.id;
            docObj.status__c = 'Pending';
            docObj.document_type__c = docTypeObj.id;
            docObj.Document_Master__c = docMasObj.id;
            docObj.Loan_Contact__c = loanConObj.id;
            docObj.Express_Queue_Mandatory__c = true;
            docObj.Document_Collection_Mode__c = 'Photocopy';
            docObj.Screened_p__c = 'Yes';
            docObj.Sampled_p__c = 'Yes';
            lstofDocs.add(docObj);
            Database.insert(docObj);
            Document_Checklist__c docObj1 = new Document_Checklist__c();
            docObj1.Loan_Applications__c = loanAppObj.id;
            docObj1.status__c = 'Pending';
            docObj1.document_type__c = docTypeObj.id;
            docObj1.Document_Master__c = docMasObj.id;
            docObj1.Loan_Contact__c = loanConObj.id;
            docObj1.Express_Queue_Mandatory__c = true;
            docObj1.Document_Collection_Mode__c = 'Photocopy';
            docObj1.Screened_p__c = 'Yes';
            docObj1.Sampled_p__c = 'Yes';
            docObj1.FCU_Done__c = True;
            lstofDocs.add(docObj1);
            Database.insert(docObj1);
            
            
            Property_Document_Checklist__c pd = new Property_Document_Checklist__c(Document_Checklist__c=docObj.id, Loan_Application__c=loanAppObj.id, Third_Party_Verification__c=tpvObj.id);
            insert pd;
            Document_Checklist__c docObj2 = new Document_Checklist__c();
            docObj2.Loan_Applications__c = loanAppObj.id;
            docObj2.status__c = 'Pending';
            docObj2.document_type__c = docTypeObj.id;
            docObj2.Document_Master__c = docMasObj.id;
            docObj2.Loan_Contact__c = loanConObj.id;
            docObj2.Express_Queue_Mandatory__c = true;
            docObj2.Document_Collection_Mode__c = 'Photocopy';
            docObj2.Screened_p__c = 'Yes';
            docObj2.Sampled_p__c = 'Yes';
            docObj2.FCU_Done__c = False;
            
            Database.insert(docObj2);
            
            
            Property_Document_Checklist__c pd1 = new Property_Document_Checklist__c(Document_Checklist__c=docObj2.id, Loan_Application__c=loanAppObj.id, Third_Party_Verification__c=tpvObj.id);
            insert pd1;
            //test.stoptest();
            List<Document_checkList__c> docCheckList = new List<Document_checkList__c>();
            docCheckList.add(docObj);
            
            Property_Document_Checklist__c propDocObj = new Property_Document_Checklist__c();
            propDocObj.Document_Checklist__c = docObj.id;
            propDocObj.Loan_Application__c = loanAppObj.id;
            propDocObj.Third_Party_Verification__c = tpvObj.id;
            Database.insert(propDocObj);
            ContentVersion conVer = new ContentVersion();
            conVer.Title = 'CZDSTOU';
            conVer.PathOnClient = 'Test';
            conVer.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body');
            
            List<contentVersion> conVlist = new List<contentVersion>();
            conVlist.add(conVer);
            database.insert(conVList);
            
            List<contentDocument> documents = [select id, Title, LatestPublishedVersionId from contentDocument];
            contentDocumentLink cdlObj = new contentDocumentLink();
            cdlObj.linkedEntityId = docObj.id;
            cdlObj.shareType = 'I';
            cdlObj.contentDocumentId = documents[0].id;
            cdlObj.Visibility = 'AllUsers';
            Database.insert(cdlObj);
            String paramContactName = 'Test contact';
            String errorMsg = 'Error';
            //controller.DocumentListWrapper docWrap = new controller.DocumentListWrapper(lstofDocs,paramContactName,errorMsg);
            
            //documentControllerFCU controller = new documentControllerFCU();
            documentControllerFCU.fetchDocList(tpvObj.id);
            
            Lead objLead = new Lead();
            documentControllerFCU.getselectOptions(objLead,'Call_Status__c');
            documentControllerFCU.getMode();
            documentControllerFCU.getSampled();
            documentControllerFCU.getScreened();
            documentControllerFCU.fetchProfileName();
            documentControllerFCU.fetchAllDocs(docObj1.id);
            documentControllerFCU.fetchDocList1(pd.id);
            
            
            List<Document_checkList__c> docCheckList1 = new List<Document_checkList__c>();
            docCheckList1.add(docObj2);
            
            Property_Document_Checklist__c propDocObj1 = new Property_Document_Checklist__c();
            propDocObj1.Document_Checklist__c = docObj2.id;
            propDocObj1.Loan_Application__c = loanAppObj.id;
            propDocObj1.Third_Party_Verification__c = tpvObj.id;
            insert propDocObj1;
            //Database.insert(propDocObj);
            ContentVersion conVer1 = new ContentVersion();
            conVer1.Title = 'CZDSTOU';
            conVer1.PathOnClient = 'Test';
            conVer1.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body');
            
            List<contentVersion> conVlist1 = new List<contentVersion>();
            conVlist1.add(conVer1);
            database.insert(conVlist1);
            
            List<contentDocument> documents1 = [select id, Title, LatestPublishedVersionId from contentDocument];
            contentDocumentLink cdlObj1 = new contentDocumentLink();
            cdlObj1.linkedEntityId = docObj2.id;
            cdlObj1.shareType = 'I';
            cdlObj1.contentDocumentId = documents1[0].id;
            cdlObj1.Visibility = 'AllUsers';
            Database.insert(cdlObj1);	
            
            List<FCU_Document_Checklist__c> lstofFCU = new List<FCU_Document_Checklist__c>();
            FCU_Document_Checklist__c FCU = new FCU_Document_Checklist__c(Loan_Application__c=loanAppObj.id,Document_Checklist__c=docObj2.Id,
                                                                          Third_Party_Verification__c=tpvObj.id);
            lstofFCU.add(FCU);   
            insert lstofFCU;
            String paramJSONList = JSON.serialize(lstofFCU);  
            documentControllerFCU.saveLoan(paramJSONList,false,tpvObj.id);    
            documentControllerFCU.queryAttachments(docObj.id,false);  
            documentControllerFCU.queryAttachments(tpvObj.id,false); 
            documentControllerFCU.checkLAFCUBRDExemption(tpvObj.id);
            documentControllerFCU.checkLAFCUBRDExemption1(loanAppObj.id);
            test.stoptest();    
        }
    }
}