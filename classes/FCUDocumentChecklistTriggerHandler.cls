public class FCUDocumentChecklistTriggerHandler {
	
    public static void afterInsert(List<FCU_Document_Checklist__c> fcuDCList){
        Set<Id> setDocChecklistIds = new Set<Id>();
        List<Document_Checklist__c> lstDocChecklist = new List<Document_Checklist__c>();
        List<Document_Checklist__c> lstDocChecklistToUpdate = new List<Document_Checklist__c>();
        
        for(FCU_Document_Checklist__c fcuDC: fcuDCList){
            setDocChecklistIds.add(fcuDC.Document_Checklist__c);
        }
        
        lstDocChecklist = [SELECT Id,Name,FCU_Done__c from Document_Checklist__c WHERE Id IN: setDocChecklistIds];
        
        for(Document_Checklist__c dc: lstDocChecklist){
            if(dc.FCU_Done__c == FALSE){
                dc.FCU_Done__c = TRUE;
            }
            lstDocChecklistToUpdate.add(dc);
        }
        
        System.debug('lstDocChecklistToUpdate==>'+lstDocChecklistToUpdate);
        System.debug('lstDocChecklistToUpdate.size()==>'+lstDocChecklistToUpdate.size());
        if(lstDocChecklistToUpdate.size() > 0){
            update lstDocChecklistToUpdate;
        }
    }
    
    public static void afterUpdate(List<FCU_Document_Checklist__c> newList,List<FCU_Document_Checklist__c> oldList,Map<Id,FCU_Document_Checklist__c> newMap,Map<Id,FCU_Document_Checklist__c> oldMap){
        List<FCU_Document_Checklist__c> lstFCUDCFields = new List<FCU_Document_Checklist__c>();
        Set<Id> tpvIds = new Set<Id>();
        
        for(FCU_Document_Checklist__c fcuDC: newList){
            if(fcuDC.FCU_Decision__c != oldMap.get(fcuDC.Id).FCU_Decision__c || fcuDC.Screened__c != oldMap.get(fcuDC.Id).Screened__c || fcuDC.Sampled__c != oldMap.get(fcuDC.Id).Sampled__c){
                lstFCUDCFields.add(fcuDC);
            }
            
            if(fcuDC.FCU_Decision__c != oldMap.get(fcuDC.Id).FCU_Decision__c){
                tpvIds.add(fcuDC.Third_Party_Verification__c);
            }
        }
        
        if(!lstFCUDCFields.isEmpty()){
            FCUDocumentChecklistTriggerHelper.updateDocumentChecklistFields(lstFCUDCFields);
        }
        
        if(!tpvIds.isEmpty()){
            FCUDocumentChecklistTriggerHelper.setThirdPartyFCUReportStatus(tpvIds);
        }
    }
}