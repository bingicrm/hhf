@isTest
public class TestPropertyTriggerHandler {

    public static testMethod void methodTest() {
    Loan_Application__c la;
    Loan_Contact__c lc;
    Applicable_Deviation__c objAD;
    Applicable_Deviation__c objAD1;
    List<Applicable_Deviation__c> objList = new List<Applicable_Deviation__c>();
    Profile p = [SELECT id, name from profile where name =: 'Sales Team'];
    Profile p1 = [SELECT id, name from profile where name =: 'Credit Team'];

        User u = new User(
            ProfileId = p.Id, 
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',Role_in_Sales_Hierarchy__c = 'SM');

        insert u;
        
        User u1 = new User(
            ProfileId = p1.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',Role_in_Credit_Hierarchy__c = 'BCM'
);

        insert u1;

        System.runAs(u) {
            Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();

            Account acc = new Account();
            acc.CountryCode__c = '+91';
            acc.Email__c = 'puser001@amamama.com';
            acc.Gender__c = 'M';
            acc.Mobile__c = '9800765432';
            acc.Phone__c = '9800765434';
            acc.PersonMobilePhone = '9800765434';
            acc.FirstName = 'abc';
            acc.LastName = 'def';
            acc.Salutation = 'Mr';
            acc.RecordTypeId = RecordTypeIdCustomer;
            insert acc;

            Scheme__c sch = new Scheme__c(Name = 'Construction Finance', Scheme_Code__c = 'CF', Scheme_Group_ID__c = '5',
                Scheme_ID__c = 11, Product_Code__c = 'CF', Max_ROI__c = 10, Min_ROI__c = 5,
                Min_Loan_Amount__c = 10000, Min_FOIR__c = 1,
                Max_Loan_Amount__c = 3000001, Max_Tenure__c = 5, Min_Tenure__c = 1,
                Max_LTV__c = 5, Min_LTV__c = 1, Max_FOIR__c = 5);
            insert sch;

            Id RecordTypeIdLAAppIni = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Application Initiation').getRecordTypeId();

            la = new Loan_Application__c(Customer__c = acc.Id, Scheme__c = sch.Id, Loan_Number__c = '124',
                Transaction_type__c = 'PB', Requested_Amount__c = 100000,
                Loan_Purpose__c = '11',StageName__c = 'Customer Onboarding', Sub_Stage__c = 'Application Initiation',
                RecordTypeId = RecordTypeIdLAAppIni,
                Business_Date_Created__c = System.today(),
                Approved_Loan_Amount__c = 100000,
                Processing_Fee_Percentage__c = 1, Mode_of_Repayment__c = 'NACH',
                Repayment_Start_Date__c = Date.Today().addDays(100),
                Sanctioned_Disbursement_Type__c = 'Multiple Tranche', Property_Identified__c = true);
            insert la;

            lc= [SELECT Id, Borrower__c from Loan_Contact__c where Loan_Applications__c =: la.Id];
            lc.Borrower__c = '1';
            //lc.Category__c='1';
            lc.Constitution__c = '20';
            lc.Customer_segment__c = '1';
            lc.BRE_Record__c = true;
            lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc.Marital_Status__c = 'M';
            lc.Gross_Salary__c = 1000;
            lc.Basic_Pay__c = 1000;
            lc.DA__c = 1000;
            //lc.Total_Fixed_Pay__c = 1000;
            //lc.Variable_Income_Monthly__c = 1000;
            lc.Net_Income__c = 1000;
            lc.PF__c = 1000;
            lc.TDS__c = 1000;
            lc.Current_Organization_Work_Ex_in_Months__c = 12;
            lc.Total_Work_Experience__c = 12;
            lc.Customer_Details_Verified__c = true;
            lc.Voter_ID_Number__c = '123456789';
            lc.Passport_Number__c = '123456789';
            lc.PAN_Number__c = 'BAFPV6543A';
            lc.Aadhaar_Number__c = '898518762736';
            lc.Income_Considered__c = true;
            lc.Customer_Verified__c= true;
            update lc;
            Test.startTest();
        
            Sourcing_Detail__c sc = new Sourcing_Detail__c(Loan_Application__c = la.Id, Source_Code__c = '19', Lead_Sourcing_Detail__c = 'HHFL Website', recordTypeId = Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('Digital Direct').getRecordTypeId());
            insert sc;

            Pincode__c pin = new Pincode__c(Name = '11044', ZIP_ID__c = '45', City__c = 'Ludiana', State_Text__c = 'Punjab', Country_Text__c = 'India');
            insert pin;
             
            Id recordTypeBR= Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('BRE2').getRecordTypeId();
            Customer_Integration__c objCustIntegration = new Customer_Integration__c(Loan_Application__c=la.Id,RecordTypeId=recordTypeBR,Loan_Contact__c=lc.Id);
                insert objCustIntegration;
                objCustIntegration.BRE2_Success__c=true;
                objCustIntegration.BRE2_Recent__c=true;
                update objCustIntegration;
                
            la.StageName__c = 'Operation Control';
            la.Sub_Stage__c = 'COPS:Data Maker';
            la.RecordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('COPS:Data Maker').getRecordTypeId();
            update la;
            
            Property__c prop = new Property__c(First_Property_Owner__c = lc.Id, Loan_Application__c = la.Id, Address_Line_1__c = '17827872', Address_line_2__c = 'Test', Area__c = 12, Floor__c = '1', Area_Unit__c = 'Sqft', Flat_No_House_No__c = '1', Type_Property__c = 'Residential', Pincode_LP__c = pin.Id);
            prop.reTriggerBRE2__c=false;
            insert prop;
            
            prop.Address_Line_1__c='768';
            try{
                update prop;
            }
            catch(Exception e){}
            Test.stopTest();
        }

           
        
    }
    public static testMethod void methodTest2() {
        Loan_Application__c la;
        Loan_Contact__c lc;
        Applicable_Deviation__c objAD;
        Applicable_Deviation__c objAD1;
        List<Applicable_Deviation__c> objList = new List<Applicable_Deviation__c>();
        Profile p = [SELECT id, name from profile where name =: 'Sales Team'];
        Profile p1 = [SELECT id, name from profile where name =: 'Credit Team'];
    
            User u = new User(
                ProfileId = p.Id, 
                LastName = 'last',
                Email = 'puser000@amamama.com',
                Username = 'puser000@amamama.com' + System.currentTimeMillis(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',Role_in_Sales_Hierarchy__c = 'SM');
    
            insert u;
            
            System.runAs(u) {
                Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
    
                Account acc = new Account();
                acc.CountryCode__c = '+91';
                acc.Email__c = 'puser001@amamama.com';
                acc.Gender__c = 'M';
                acc.Mobile__c = '9800765432';
                acc.Phone__c = '9800765434';
                acc.PersonMobilePhone = '9800765434';
                acc.FirstName = 'abc';
                acc.LastName = 'def';
                acc.Salutation = 'Mr';
                acc.RecordTypeId = RecordTypeIdCustomer;
                insert acc;
    
                Scheme__c sch = new Scheme__c(Name = 'Construction Finance', Scheme_Code__c = 'CF', Scheme_Group_ID__c = '5',
                    Scheme_ID__c = 11, Product_Code__c = 'CF', Max_ROI__c = 10, Min_ROI__c = 5,
                    Min_Loan_Amount__c = 10000, Min_FOIR__c = 1,
                    Max_Loan_Amount__c = 3000001, Max_Tenure__c = 5, Min_Tenure__c = 1,
                    Max_LTV__c = 5, Min_LTV__c = 1, Max_FOIR__c = 5);
                insert sch;
    
                Id RecordTypeIdLAAppIni = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Application Initiation').getRecordTypeId();
    
                la = new Loan_Application__c(Customer__c = acc.Id, Scheme__c = sch.Id, Loan_Number__c = '124',
                    Transaction_type__c = 'PB', Requested_Amount__c = 100000,
                    Loan_Purpose__c = '11', StageName__c = 'Customer Onboarding', Sub_Stage__c = 'Application Initiation',
                    RecordTypeId = RecordTypeIdLAAppIni,
                    Business_Date_Created__c = System.today(),
                    Approved_Loan_Amount__c = 100000,
                    Processing_Fee_Percentage__c = 1, Mode_of_Repayment__c = 'NACH',
                    Repayment_Start_Date__c = Date.Today().addDays(100),
                    Sanctioned_Disbursement_Type__c = 'Multiple Tranche', Property_Identified__c = true);
                insert la;
    
                lc= [SELECT Id, Borrower__c from Loan_Contact__c where Loan_Applications__c =: la.Id];
                lc.Borrower__c = '1';
                //lc.Category__c='1';
                lc.Constitution__c = '20';
                lc.Customer_segment__c = '1';
                lc.BRE_Record__c = true;
                lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
                lc.Marital_Status__c = 'M';
                lc.Gross_Salary__c = 1000;
                lc.Basic_Pay__c = 1000;
                lc.DA__c = 1000;
                //lc.Total_Fixed_Pay__c = 1000;
                //lc.Variable_Income_Monthly__c = 1000;
                lc.Net_Income__c = 1000;
                lc.PF__c = 1000;
                lc.TDS__c = 1000;
                lc.Current_Organization_Work_Ex_in_Months__c = 12;
                lc.Total_Work_Experience__c = 12;
                lc.Customer_Details_Verified__c = true;
                lc.Voter_ID_Number__c = '123456789';
                lc.Passport_Number__c = '123456789';
                lc.PAN_Number__c = 'BAFPV6543A';
                lc.Aadhaar_Number__c = '898518762736';
                lc.Income_Considered__c = true;
                lc.Customer_Verified__c= true;
                update lc;
    
                Sourcing_Detail__c sc = new Sourcing_Detail__c(Loan_Application__c = la.Id, Source_Code__c = '19', Lead_Sourcing_Detail__c = 'HHFL Website', recordTypeId = Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('Digital Direct').getRecordTypeId());
                insert sc;
    
                Pincode__c pin = new Pincode__c(Name = '11044', ZIP_ID__c = '45', City__c = 'Ludiana', State_Text__c = 'Punjab', Country_Text__c = 'India');
                insert pin;
				
				Test.startTest();
				
                la.StageName__c = 'Operation Control';
                la.Sub_Stage__c = 'COPS:Data Maker';
                la.RecordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('COPS:Data Maker').getRecordTypeId();
                update la;
    
                Property__c prop = new Property__c(First_Property_Owner__c = lc.Id, Loan_Application__c = la.Id, Address_Line_1__c = '17827872', Address_line_2__c = 'Test', Area__c = 12, Floor__c = '1', Area_Unit__c = 'Sqft', Flat_No_House_No__c = '1', Type_Property__c = 'Residential', Pincode_LP__c = pin.Id);
                insert prop;
                Property__c prop1 = new Property__c(First_Property_Owner__c = lc.Id, Loan_Application__c = la.Id, Address_Line_1__c = '17827872', Address_line_2__c = 'Test', Area__c = 12, Floor__c = '1', Area_Unit__c = 'Sqft', Flat_No_House_No__c = '1', Type_Property__c = 'Residential', Pincode_LP__c = pin.Id);
                try{
                    insert prop1;
                }
                catch(Exception e){}
				
				Test.stopTest();
            }
   }     
}