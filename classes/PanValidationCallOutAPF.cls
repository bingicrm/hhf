public class PanValidationCallOutAPF{

    /**************************************************************************************************************************************************
        Created By     : Shobhit Saxena(Deloitte)
        Purpose        : This class will perform PAN Integration for APF Module, on Project Builder and Promoter objects.
    
    ***************************************************************************************************************************************************/
    
    public sObject validatePan(Id strRecordId) {
        
        if(String.isNotBlank(strRecordId)) {
            List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
            String sObjName = strRecordId.getSObjectType().getDescribe().getName();
            System.debug('Debug Log for sObjName'+sObjName);
            List<Project_Builder__c> lstProjBuilder = new List<Project_Builder__c>();
            List<Promoter__c> lstPromoter = new List<Promoter__c>();
            Map<String,String> headerMap = new Map<String,String>();
            Rest_Service__mdt restService = [ SELECT MasterLabel, QualifiedApiName , Service_EndPoint__c , 
                                                     Client_Password__c, Client_Username__c, Request_Method__c, Time_Out_Period__c  
                                              FROM   Rest_Service__mdt
                                              WHERE  MasterLabel = 'Pan Card Validated'
                                            ]; 
            System.debug('Debug Log for restService'+restService);
            if(restService != null) {               
                Blob headerValue = Blob.valueOf(restService.Client_Username__c + ':' + restService.Client_Password__c);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                headerMap.put('Authorization',authorizationHeader);
                headerMap.put('Username',restService.Client_Username__c);
                headerMap.put('Password',restService.Client_Password__c);
                headerMap.put('Content-Type','application/json');
                
                System.debug('Debug Log for headerMap'+headerMap);
                for(String str : headerMap.keySet()) {
                    System.debug('HeaderMap Key: '+str+' Value: '+headerMap.get(str));
                }
                
                RequestPanWrapper requestWrap = new RequestPanWrapper();
                requestWrap.RequestType = 'REQUEST';
                requestWrap.RequestTime = String.valueOf(System.now());
                
                if(sObjName == Constants.strProjectBuilderAPI) {
                    lstProjBuilder = [Select Id, Name, Project__c, PAN_Number__c, Pan_Verification_status__c, PAN_First_Name__c, PAN_Middle_Name__c, PAN_Last_Name__c, PAN_Response__c From Project_Builder__c Where Id =: strRecordId LIMIT 1];
                    if(!lstProjBuilder.isEmpty()) {
                        requestWrap.ApplicationId = lstProjBuilder[0].Id;
                        requestWrap.PanNumber = lstProjBuilder[0].PAN_Number__c;
                    }
                }
                
                else if(sObjName == Constants.strPromoterAPI) {
                    lstPromoter = [Select Id, Name, Pan_Verification_status__c, PAN_Number__c, PAN_Response__c From Promoter__c Where Id =: strRecordId LIMIT 1];
                    if(!lstPromoter.isEmpty() && String.isNotBlank(lstPromoter[0].PAN_Number__c)) {
                        requestWrap.ApplicationId = lstPromoter[0].Id;
                        requestWrap.PanNumber = lstPromoter[0].PAN_Number__c;
                    }
                }
                System.debug('Debug Log for requestWrap.PanNumber' + requestWrap.PanNumber);
                String jsonRequest = Json.serialize(requestWrap);
                System.debug('Debug Log for jsonRequest'+jsonRequest);
                
                HttpResponse res;
                
                if(String.isNotBlank(requestWrap.PanNumber)) {
                    res   = Utility.sendRequest(restService.Service_EndPoint__c,restService.Request_Method__c, jsonRequest , headerMap, Integer.valueOf(restService.Time_Out_Period__c));
                    System.debug('@res'+res);
                    ResponsePanWrapper wrap = (ResponsePanWrapper) System.JSON.deserialize(res.getBody(), ResponsePanWrapper.class);
                    if(wrap !=null && wrap.TxnStatus  != null && wrap.TxnStatus == 'SUCCESS'){
                        if(sObjName == Constants.strProjectBuilderAPI) {
                            Builder_Integrations__c objBuilderIntegration = new Builder_Integrations__c();
                            objBuilderIntegration.RecordTypeId = Schema.SObjectType.Builder_Integrations__c.getRecordTypeInfosByName().get('PAN Integration').getRecordTypeId();
                            objBuilderIntegration.PAN_Last_Name__c = wrap.LastName;
                            objBuilderIntegration.PAN_First_Name__c = wrap.FirstName;
                            objBuilderIntegration.PAN_Middle_Name__c =   wrap.MiddleName;
                            objBuilderIntegration.PAN_Status__c = wrap.PanStatus;
                            objBuilderIntegration.PAN_Number__c = wrap.Pan;
                            objBuilderIntegration.Pan_Title__c = wrap.PanTitle;
                            objBuilderIntegration.Project_Builder__c = lstProjBuilder[0].Id;
                            objBuilderIntegration.Project__c = lstProjBuilder[0].Project__c;
                            System.debug('Debug Log for objBuilderIntegration'+objBuilderIntegration);
                            //insert objBuilderIntegration;
                            Database.SaveResult objDSR = Database.insert(objBuilderIntegration,false);
                            if(!objDSR.isSuccess()) {
                                for(Database.Error objError : objDSR.getErrors()) {
                                    System.debug('The following error has occurred.');                    
                                    System.debug(objError.getStatusCode() + ': ' + objError.getMessage());
                                    System.debug('Fields that affected this error: ' + objError.getFields());
                                    Error_Log__c objErrorLog = new Error_Log__c(Error_Code__c = String.valueOf(objError.getStatusCode()), Type__c = 'Builder Integration Creation'+lstProjBuilder[0].Project__c, Error_Message__c = objError.getMessage(), URL__c = 'APF');
                                    lstErrorLog.add(objErrorLog);
                                }
                                insert lstErrorLog;
                            }
                            else {
                                lstProjBuilder[0].PAN_First_Name__c = objBuilderIntegration.PAN_First_Name__c;
                                lstProjBuilder[0].Validated_PAN__c = objBuilderIntegration.PAN_Number__c;
                                lstProjBuilder[0].PAN_Middle_Name__c = objBuilderIntegration.PAN_Middle_Name__c;
                                lstProjBuilder[0].Pan_Last_Name__c = objBuilderIntegration.Pan_Last_Name__c;
                                lstProjBuilder[0].Pan_Status__c = objBuilderIntegration.PAN_Status__c;
                                lstProjBuilder[0].Pan_Title__c = objBuilderIntegration.Pan_Title__c;
                                lstProjBuilder[0].Pan_Verification_status__c  = true;
                                lstProjBuilder[0].PAN_Response__c = true;
                                update lstProjBuilder[0];
                                Database.SaveResult objDSR1 = Database.update(lstProjBuilder[0],false);
                                if(!objDSR1.isSuccess()) {
                                    for(Database.Error objError : objDSR1.getErrors()) {
                                        System.debug('The following error has occurred while updating Project Builder.');                    
                                        System.debug(objError.getStatusCode() + ': ' + objError.getMessage());
                                        System.debug('Project Builder Fields that affected this error: ' + objError.getFields());
                                        Error_Log__c objErrorLog = new Error_Log__c(Error_Code__c = String.valueOf(objError.getStatusCode()), Type__c = 'Project Builder Updation'+lstProjBuilder[0].Project__c,Error_Message__c = objError.getMessage(), URL__c = 'APF' );
                                        lstErrorLog.add(objErrorLog);
                                    }
                                    insert lstErrorLog;
                                }
                                else {
                                    return objBuilderIntegration;
                                }
                                return null;
                            }
                        }
                        else if(sObjName == Constants.strPromoterAPI) {
                            Promoter_Integrations__c objPromoterIntegration = new Promoter_Integrations__c();
                            objPromoterIntegration.RecordTypeId = Schema.SObjectType.Promoter_Integrations__c.getRecordTypeInfosByName().get('PAN Integration').getRecordTypeId();
                            objPromoterIntegration.Pan_Last_Name__c = wrap.LastName;
                            objPromoterIntegration.PAN_First_Name__c = wrap.FirstName;
                            objPromoterIntegration.PAN_Middle_Name__c =   wrap.MiddleName;
                            objPromoterIntegration.PAN_Status__c = wrap.PanStatus;
                            objPromoterIntegration.PAN_Number__c = wrap.Pan;
                            objPromoterIntegration.Pan_Title__c = wrap.PanTitle;
                            objPromoterIntegration.Promoter__c = strRecordId;
                            Database.SaveResult objDSR = Database.insert(objPromoterIntegration,false);
                            if(!objDSR.isSuccess()) {
                                for(Database.Error objError : objDSR.getErrors()) {
                                    System.debug('The following error has occurred.');                    
                                    System.debug(objError.getStatusCode() + ': ' + objError.getMessage());
                                    System.debug('Fields that affected this error: ' + objError.getFields());
                                    Error_Log__c objErrorLog = new Error_Log__c(Error_Code__c = String.valueOf(objError.getStatusCode()), Type__c = 'Promoter Integration Creation', Error_Message__c = objError.getMessage(), URL__c = 'APF');
                                    lstErrorLog.add(objErrorLog);
                                }
                                insert lstErrorLog;
                            }
                            else {
                                lstPromoter[0].PAN_First_Name__c = objPromoterIntegration.PAN_First_Name__c;
                                lstPromoter[0].Validated_PAN__c = objPromoterIntegration.PAN_Number__c;
                                lstPromoter[0].PAN_Middle_Name__c = objPromoterIntegration.PAN_Middle_Name__c;
                                lstPromoter[0].Pan_Last_Name__c = objPromoterIntegration.Pan_Last_Name__c;
                                lstPromoter[0].Pan_Status__c = objPromoterIntegration.PAN_Status__c;
                                lstPromoter[0].Pan_Title__c = objPromoterIntegration.Pan_Title__c;
                                lstPromoter[0].Pan_Verification_status__c  = true;
                                lstPromoter[0].PAN_Response__c = true;
                                update lstPromoter[0];
                                Database.SaveResult objDSR1 = Database.update(lstPromoter[0],false);
                                if(!objDSR1.isSuccess()) {
                                    for(Database.Error objError : objDSR1.getErrors()) {
                                        System.debug('The following error has occurred while updating Promoter.');                    
                                        System.debug(objError.getStatusCode() + ': ' + objError.getMessage());
                                        System.debug('Promoter Fields that affected this error: ' + objError.getFields());
                                        Error_Log__c objErrorLog = new Error_Log__c(Error_Code__c = String.valueOf(objError.getStatusCode()), Type__c = 'Promoter Updation via PAN', Error_Message__c = objError.getMessage(), URL__c = 'APF' );
                                        lstErrorLog.add(objErrorLog);
                                    }
                                    insert lstErrorLog;
                                }
                                else {
                                    return objPromoterIntegration;
                                }
                                return null;
                            }
                        }
                    }
                    else{
                        API_Log__c apiLog = new API_Log__c(Request_JSON__c =  jsonRequest,Response_JSON__c = res.getBody(), Type_of_Error__c = 'Outbound');
                        insert apiLog;
                        system.debug('inside else');
                    }
                
                }
            }
        }
        return null;
    }
    
    public class RequestPanWrapper{
        public String ApplicationId;    //10007
        public String RequestType;  //REQUEST
        public String RequestTime;  //23072015 14:22:24
        public String PanNumber;    
    }
    
   public class ResponsePanWrapper{   
        public String ApplicationId;    //
        public String ResponseType; //RESPONSE
        public String RequestReceivedTime;  //
        public String AcknowledgementId;    //
        public String TxnStatus;    //SUCCESS
        public String NsdlStatus;   //SUCCESS 
        public String Pan;  //AAAAA1111A
        public String PanStatus;    //E
        public String LastName; //LASTNM
        public String FirstName;    //FIRSTNM
        public String MiddleName;   //MIDDLENM
        public String PanTitle; //MR
        public String LastUpdateDate;   //03122010
        public String Filler1;  //
        public String Filler2;  //
        public String Filler3;  //               
    }

}