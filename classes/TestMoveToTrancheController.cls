@isTest
public class TestMoveToTrancheController {
static testmethod void test1(){
       /* Profile p=[SELECT id,name from profile where name=:'Sales Team'];
         UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        List<User> lstBranchManager= new List<User>();                          
          User u = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,
            Role_in_Sales_Hierarchy__c ='DSA'
        );
        lstBranchManager.add(u);
        
        User u1 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,
            Role_in_Sales_Hierarchy__c = 'DSA'
        );
         lstBranchManager.add(u1);
        insert lstBranchManager;                        
                           
                           
        User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId 
                       from user where ID=:UserInfo.getUserId()];
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=lstBranchManager[0].Id;
        loggedUser.ManagerID=lstBranchManager[0].Id;
        update loggedUser;
       
        
       
        u.ManagerId=UserInfo.getUserID();
        system.runAs(loggedUser){   */           
       
        
         Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='9999966667',
                                  Date_of_Incorporation__c=Date.newInstance(2018,11,11));
         
         Database.insert(Cob);                
                            
         Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                     Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                     Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                     Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                     Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
          
                           
         Database.insert(sc); 
        
         
         Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,
                                                           StageName__c='Customer Onboarding',
                                                           Sub_Stage__c='Credit Decisioning',
                                                           Transaction_type__c='SC',Requested_Amount__c=10000,
                                                           Applicant_Customer_segment__c='Salaried',
                                                           Branch__c='test',Line_Of_Business__c='Open Market',
                                                           //Assigned_Sales_User__c=u.id,
                                                           Loan_Purpose__c='20',
                                                           Requested_EMI_amount__c=1000,Approved_ROI__c=5,
                                                           Requested_Loan_Tenure__c=5,
                                                           Government_Programs__c='PMAY',
                                                           Loan_Engine_2_Output__c='STP',
                                                           Property_Identified__c=True,ownerId=UserInfo.getUserID(),
                                                           Approved_Loan_Amount__c = 500000
                                                           //Assigned_Scan_DataMaker__c=u.id,
                                                           /*Assigned_scan_DataChecker__c=u.id*/);
                                                           
                                                           
        
        
        // Database.insert(LAob);
        
        List<Loan_Application__c> listLAob= new List<Loan_Application__c>();
        listLAob.add(LAob); 
        insert listLAob;
      LMS_Date_Sync__c abc = new LMS_Date_Sync__c(EOD_BOD__c='N',Current_Date__c=Date.Today());
            insert abc;
             
        List<Communication_Trigger_Point__mdt> ctpObj = [SELECT Id, SMS_Template__c, Stage_Value__c, DeveloperName FROM Communication_Trigger_Point__mdt];
        Document_Type__c DType=new Document_Type__c(Name='Property Papers');
        insert DType;
        
              
        IMD__c IMDob=new IMD__c(Loan_Applications__c=LAob.id,Cheque_Number__c='123456',
                                 Cheque_Date__c=Date.today()-5,
                                 Amount__c=100,Receipt_Date__c=Date.today(),Payment_Mode__c='Q');
                              
        //Database.insert(IMDob);
            
        Document_Checklist__c DC=new Document_Checklist__c(Loan_Applications__c=LAob.id,Status__c='OTC Approved',
                                                           REquest_Date_for_OTC__c=Date.today());
        Database.insert(DC);
        
        
        
        Loan_Contact__c LCobject=[SELECT id from Loan_Contact__c where Loan_Applications__c=:LAob.id];
        LCobject.Customer_Verified__c=false;
        update LCobject;
       
             
        LCobject=[SELECT id,name,Customer_Verified__c from Loan_Contact__c where Loan_Applications__c=:LAob.id];
        
                        
        Third_Party_Verification__c TPVObject=new Third_Party_Verification__c(Loan_Application__c=LAob.id,
                                                                              Customer_Segment__c='Salaried',
                                                                              Status__c='NEW',
                                                                              Type_of_Verification__c='Full Profile Check',
                                                                              Date_Time_of_Visit__c=Date.today()-5
                                                                              );
      
        //Database.insert(TPVObject); 
        
        Sanction_Condition_Master__c SCM=new Sanction_Condition_Master__c(Name='TEST');
        insert SCM;
        
       /* Loan_Sanction_Condition__c LSC =new Loan_Sanction_Condition__c(Loan_Application__c=LAob.id,Sanction_Condition_Master__c=SCM.id,
                                                                       Customer_Details__c=LCobject.id,status__c='Verified');
        
        insert LSC;   
        */
        Document_Master__c DM=new Document_Master__c(Name='Financial Data entry- excel', Sanction_Condition_Master__c=SCM.id,
                                                     Doc_Id__c='test');  
        insert DM;  
        
        DST_Master__c DSTMaster=new DST_Master__c(Name='Test DST',Inspector_ID__c='1234');     
        insert DSTMaster;  
        
        Cross_Sell_Partner_Name__c CSPN=new Cross_Sell_Partner_Name__c(Name='Test');
        insert CSPN;                                               
        
       /* Sourcing_Detail__c SD=new Sourcing_Detail__c(Loan_Application__c=LAob.id,DST_Name__c=DSTMaster.id,
                                                      Source_Code__c='16',Sales_User_Name__c=u.id,
                                                      Cross_Sell_Partner_Name__c=CSPN.id);  
                                                      
        insert SD;*/
        
        
        List<Loan_Application__c> updatelist=new List<Loan_Application__c>();
        Id RecordTypeIdla = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Loan Disbursal').getRecordTypeId();
        for(Loan_Application__c ob:[SELECT id,Applicable_IMD__c,name,Customer__c,Verification_Count_All__c,Document_Waiver__c,recordtypeId,Scheme__c,StageName__c,Transaction_type__c,Requested_Amount__c,
                                    Customer_Detail_Check__c,All_Loan_Contact_Count__c,Assigned_FileCheck_User__c,IMGC_Premium_Fees__c,
                                    Assigned_cops_CreditOperation__c,assigned_handsight__c,Assigned_Disbursement_maker__c,
                                    Assigned_Scan_DataMaker__c,Assigned_scan_DataChecker__c,Assigned_Cops_dataMaker__c,Assigned_Scan_Checker__c,
                                    Assigned_credit_at_CN__c,Credit_Manager_User__c,Assigned_Sales_Review__c,sales_manager__c,
                                    Applicant_Customer_segment__c,Branch__c,Line_Of_Business__c,Assigned_Sales_User__c,Document_marked_OTC__c,OTC_on_third_party__c,
                                    Sub_Stage__c,
                                    Loan_Purpose__c,Requested_EMI_amount__c,Approved_ROI__c,Requested_Loan_Tenure__c,Verification_Count_LT__c,
                                    Government_Programs__c,Loan_Engine_2_Output__c,Property_Identified__c,ownerId, APF_Loan__c,BRE_2_Eligibility__c,CreatedDate from Loan_Application__c
                                    where id IN:listLAob])
       {
                 ob.Loan_Application_Number__c='HHFL12345';
                   ob.Sub_Stage__c='CPC Data Maker';
                   ob.StageName__c='Loan Disbursal';
                 ob.RecordTypeId=RecordTypeIdla;
                   //ob.Assigned_Sales_User__c=u.id;
                   //ob.Assigned_Scan_Checker__c=u.id;
                   ob.Disbursement_Details_verified__c=true;
                   ob.Disbursal_Details_entered__c=true;
                   ob.Customer_bank_details_entered__c=true;
                   ob.Repayment_schedule_at_Disbursement_Check__c=true;
                   ob.Original_Property_paper_verified__c =true;
                   ob.PDC_done__c=true;
                   ob.Property_Identified__c =true;
                   
                   updatelist.add(ob);
       }    
                   update updatelist;
    Tranche__c tr=new Tranche__c();
        tr.Loan_Application__c=updatelist[0].Id;
        tr.Disbursal_Amount__c=100000;
        tr.Approved_Disbursal_Amount__c=100000;
        tr.PEMI_Amount__c=10000;
        tr.Request_Type__c= Constants.TRANCHEDISBURSAL;
        insert tr;
        List<Disbursement__c> lstDisb = new List<Disbursement__c>();
        
        Disbursement__c objDisbursement = new Disbursement__c(Loan_Application__c=updatelist[0].Id,
                                                              Disbursal_Amount__c=500000,ROC_Charges__c=5000,
                                                              Stamp_Duty__c=2000,CERSAI_Charges__c=5000,
                                                              /*Legal_Vetting__c=7000,*/PEMI__c=10000,Cheque_Amount__c=10000,Tranche__c=tr.id);
         lstDisb.add(objDisbursement);                                                    
        insert lstDisb;        
        MoveToTrancheController.loadLoanApplicationStageInfo(updatelist[0].Id);
      MoveToTrancheController.loanApplicationStageChange(updatelist[0].Id);
       // }
      
    } 
}