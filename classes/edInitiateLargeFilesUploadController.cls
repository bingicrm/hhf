/* **************************************************************************
*
* Class: edInitiateLargeFilesUploadController
* Created by Suresh Meghnathi: 11/09/2019
*
* - This is controller to initiate the large file uploads.

* - Modifications:
* - Suresh Meghnathi, 11/09/2019 – Initial Development
************************************************************************** */
public class edInitiateLargeFilesUploadController {
    // Loan Application id
    public String loanApplicationId{get; set;}
    
    // Foler information
    public String foldersInformation{get; set;}
    
    // Flag for load successful
    public Boolean isLoadSuccess{get; set;}
    
    /*
    *   Contructor
    */
    public edInitiateLargeFilesUploadController(){
        // Get the loan application record details
        loanApplicationId = ApexPages.currentPage().getParameters().get('Id');
    }
    
    /*
    *   Purpose:    This method is used to get the folder name for document check list
    *   Parameters: 
    *   UnitTests:  
    */
    public void init(){
        // Set default value
        isLoadSuccess = false;
        
        try{
            // Check eligibility for the operation
            if(!cls_MoveDocumentstoS3.checkEligibility()){
                // Throw error
                throw new apApplicationException('This operation is not allowed, based on your profile.');
            }
            
            // List of folder information
            List<FolderInfo> folderInfos = new List<FolderInfo>();
            
            // Document check list by id
            Map<Id, Document_Checklist__c> documentChecklistById = new Map<Id, Document_Checklist__c>();
            
            // Content document links
            List<ContentDocumentLink> contentDocumentLinks = new List<ContentDocumentLink>();
            
            // Folder configs by name
            Map<String, S3_Folders_Configuration__mdt> folderConfigsByName = new Map<String, S3_Folders_Configuration__mdt>();
            for(S3_Folders_Configuration__mdt objS3ConfigData : [SELECT Id, Delete_files__c, Folder_Name__c, MasterLabel, DeveloperName, NamespacePrefix FROM S3_Folders_Configuration__mdt]) {
                folderConfigsByName.put(objS3ConfigData.DeveloperName, objS3ConfigData);
            }
            
            // Get loan application
            List<Loan_Application__c> loanApplications = [Select Id, Name, StageName__c, Sub_Stage__c,
                                Date_of_Cancellation__c, Days_Since_Loan_Cancelled__c, 
                                Date_of_Rejection__c, Days_Since_Loan_Rejected__c,
                                Sanctioned_Disbursement_Type__c, Days_Since_Loan_Disbursed__c
                                From Loan_Application__c Where Id =: loanApplicationId];
            
            // Check loan application
            if(loanApplications.isEmpty()){
                // Throw error
                throw new apApplicationException('Loan application not found.');
            }
            
            // Check loan application status
            if(!Test.isRunningTest() && !edInitiateLargeFilesUploadController.validateLoanApplication(loanApplications[0])){
                // Throw error
                throw new apApplicationException('This operation is not allowed, based on loan application status.');
            }
            
            // Get loan contact
            List<Loan_Contact__c> loanApplicants = [Select Id, Name, Applicant_Type__c from Loan_Contact__c Where Loan_Applications__c =: loanApplications[0].Id LIMIT 1];
            
            // Get all the document checklists for the selected loan application
            if(!loanApplications.isEmpty()){
                // Get document check lists
                documentChecklistById = new Map<Id, Document_Checklist__c>([SELECT Id, Loan_Applications__c, Loan_Contact__c, Loan_Contact__r.Applicant_Type__c From Document_Checklist__c Where Loan_Applications__c =: loanApplications[0].Id]);
            
                // Get content document links for the selected loan application
                contentDocumentLinks = [Select Id, LinkedEntityId, ContentDocumentId From ContentDocumentLink Where LinkedEntityId In : documentChecklistById.keySet() OR LinkedEntityId =: loanApplications[0].Id];
            }
            
            // Get attachment related to loan application
            List<Attachment> attachments = [Select Id From Attachment Where ParentId =: loanApplications[0].Id];
            
            // List of content document ids
            List<Id> contentDocumentIds = new List<Id>();
            
            // Diocument check list by Content document id
            Map<Id, Id> documentChecklistIdByContentDocumentId = new Map<Id, Id>();
            
            // Prepare maps
            for(ContentDocumentLink contentDocumentLink : contentDocumentLinks){
                contentDocumentIds.add(contentDocumentLink.ContentDocumentId);
                documentChecklistIdByContentDocumentId.put(contentDocumentLink.ContentDocumentId, contentDocumentLink.LinkedEntityId);
            }
            
            // Prepare maps
            for(Attachment attachment : attachments){
                contentDocumentIds.add(attachment.Id);
            }
            
            // Get existing S3-Files
            List<NEILON__File__c> s3Files = [Select Id, NEILON__Export_Attachment_Id__c From NEILON__File__c Where NEILON__Export_Attachment_Id__c In : contentDocumentIds];
            
            // List of existing content document ids
            List<Id> existingContentDocumentIds = new List<Id>();
            
            // Prepare list
            for(NEILON__File__c s3File : s3Files){
                existingContentDocumentIds.add(s3File.NEILON__Export_Attachment_Id__c);
            }
            
            // New content documents
            List<Id> newContentDocumentIds = new List<Id>();
            
            // Skipping the existing content documents and preparing the new content documents list
            for(Id contentDocumentId : contentDocumentIds){
                if(!existingContentDocumentIds.contains(contentDocumentId)){
                    newContentDocumentIds.add(contentDocumentId);
                }
            }
            
            // New content documents by folder name
            Map<String, Set<Id>> contentDocumentIdByFolderName = new Map<String, Set<Id>>();
            
            // Go through new content document and prepare map
            for(Id contentDocumentId : newContentDocumentIds){
                
                // Get document check list id
                Id documentChecklistId = documentChecklistIdByContentDocumentId.get(contentDocumentId);
                
                // Gocument check list
                Document_Checklist__c documentChecklist;
                
                // Get document check list
                if(documentChecklistId != null){
                    documentChecklist = documentChecklistById.get(documentChecklistId);
                }
                
                // Get target folder name
                String targetFolder = getFolderName(documentChecklist, loanApplications[0], folderConfigsByName);
                
                // Get content documents for target folder
                Set<Id> contentDocIdsForTargetFolder = contentDocumentIdByFolderName.get(targetFolder);
                
                // Initialize
                if(contentDocIdsForTargetFolder == null){
                    contentDocIdsForTargetFolder = new Set<Id>();
                }
                
                // Add into list
                contentDocIdsForTargetFolder.add(contentDocumentId);
                
                // Update map
                contentDocumentIdByFolderName.put(targetFolder, contentDocIdsForTargetFolder);
            }
            
            // Check size of folder names
            if(!contentDocumentIdByFolderName.isEmpty()){
                // Get buckets
                List<NEILON__Folder__c> buckets = [Select Id, NEILON__Bucket_Region__c, Name From NEILON__Folder__c Where NEILON__Bucket_Name__c = null];
                
                // Check buckets
                if(!buckets.isEmpty()) {
                    // Create folder for loan application
                    NEILON__Folder__c loanApplicationFolder = NEILON.apGlobalUtils.buildFolderArchitecture(loanApplicationId);
                    
                    // Check if loan application folder is created
                    if(String.isNotBlank(loanApplicationFolder.Id)) {
                        // Get full folder
                        List<NEILON__Folder__c> loanApplicationFolders = [Select Id, Name From NEILON__Folder__c Where Id =: loanApplicationFolder.Id];
                        
                        // Existing sub folders by name
                        Map<String, NEILON__Folder__c> existingSubFoldersByName = new Map<String, NEILON__Folder__c>();
                        
                        // Get existing sub folders for load application
                        for(NEILON__Folder__c subFolder: [Select Id, Name From NEILON__Folder__c Where NEILON__Parent__c =: loanApplicationFolder.Id]){
                            existingSubFoldersByName.put(subFolder.Name, subFolder);
                        }
                        
                        // New sub folders to be created
                        List<NEILON__Folder__c> newSubFolders = new List<NEILON__Folder__c>();
                        
                        // Go through folder names map
                        for(String folderName : contentDocumentIdByFolderName.keySet()){
                            // Check if sub folder already exist
                            if(!existingSubFoldersByName.containsKey(folderName)) {
                                NEILON__Folder__c newSubFolder = new NEILON__Folder__c();
                                newSubFolder.Name = folderName;
                                newSubFolder.Loan_Application__c = loanApplicationId;
                                newSubFolder.NEILON__Bucket_Name__c = buckets[0].Name;
                                newSubFolder.NEILON__Bucket_Region__c = buckets[0].NEILON__Bucket_Region__c;
                                newSubFolder.NEILON__Parent_Id__c = loanApplicationId;
                                newSubFolder.NEILON__Parent__c = loanApplicationFolders[0].Id; 
                                newSubFolder.NEILON__Parent_Object_API_Name__c = 'Loan_Application__c';
                                if(!loanApplicants.isEmpty()) {
                                    newSubFolder.Loan_Contact__c = loanApplicants[0].Id;
                                }
                                
                                // Add into list
                                newSubFolders.add(newSubFolder);
                            }
                        }
                        
                        // Create new sub folder
                        if(!newSubFolders.isEmpty()){
                            insert newSubFolders;
                        }
                        
                        // Prepare list of folder info
                        for(NEILON__Folder__c folder : [Select Id, Name From NEILON__Folder__c Where Name IN: contentDocumentIdByFolderName.keySet() AND NEILON__Parent__c =: loanApplicationFolders[0].Id]){
                            
                            // Prepare instance
                            FolderInfo folderInfo = new FolderInfo();
                            folderInfo.name = folder.Name;
                            folderInfo.id = folder.Id;
                            folderInfo.documents = contentDocumentIdByFolderName.get(folder.Name);
                            folderInfo.count = contentDocumentIdByFolderName.get(folder.Name).size();
                            folderInfos.add(folderInfo);
                        }
                    }
                }
            }
            
            // Convert into string
            foldersInformation = JSON.serialize(folderInfos);
            
            // Check if files available
            if(folderInfos.isEmpty()){
                // Throw error
                throw new apApplicationException('No documents found.');
            }
            
            // Set flag
            isLoadSuccess = true;
        } catch(apApplicationException ex){
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
            // Set success flag
            isLoadSuccess = false;
        } catch(Exception ex){
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(errorMsg);
            // Set success flag
            isLoadSuccess = false;
        }
    }
    
    /*
    *   Purpose:    This method is used to validate loan application
    *   Parameters: 
    *   UnitTests:  
    */
    public static Boolean validateLoanApplication(Loan_Application__c loanApplication){
        if(String.isNotBlank(Label.Delete_Documents_for_Previous_Records) && Label.Delete_Documents_for_Previous_Records == 'true') {
            if(loanApplication.StageName__c == Constants.Loan_Cancelled && loanApplication.Sub_Stage__c == Constants.Loan_Cancel && loanApplication.Date_of_Cancellation__c != null && String.isNotBlank(Label.Document_Movement_Cancelled_Loan) && loanApplication.Days_Since_Loan_Cancelled__c >= Decimal.valueOf(Label.Document_Movement_Cancelled_Loan).setScale(0)) {
                return true;
            } else if(loanApplication.StageName__c == Constants.Loan_Rejected && loanApplication.Sub_Stage__c == Constants.Loan_reject && loanApplication.Date_of_Rejection__c != null && String.isNotBlank(Label.Document_Movement_Rejected_Loan) && loanApplication.Days_Since_Loan_Rejected__c >= Decimal.valueOf(Label.Document_Movement_Rejected_Loan).setScale(0)) {
                return true;
            } else if(loanApplication.Sanctioned_Disbursement_Type__c == Constants.SingleTranche && loanApplication.Sub_Stage__c == Constants.Loan_Disbursed && Label.Document_Movement_Disbursed_Loan != null && loanApplication.Days_Since_Loan_Disbursed__c >= Decimal.valueOf(Label.Document_Movement_Disbursed_Loan).setScale(0)) {
                return true;
            } else if(loanApplication.Sanctioned_Disbursement_Type__c == Constants.MultiTranche && loanApplication.Sub_Stage__c == Constants.Loan_Disbursed) {
                return true;
            }
        } else {
            if(loanApplication.Sanctioned_Disbursement_Type__c == Constants.SingleTranche && loanApplication.Sub_Stage__c == Constants.Loan_Disbursed && Label.Document_Movement_Disbursed_Loan != null && loanApplication.Days_Since_Loan_Disbursed__c == Decimal.valueOf(Label.Document_Movement_Disbursed_Loan).setScale(0)) {
                return true;
            } else if(loanApplication.Sanctioned_Disbursement_Type__c == Constants.MultiTranche && loanApplication.Sub_Stage__c == Constants.Loan_Disbursed) {
                return true;
            } else if(loanApplication.StageName__c == Constants.Loan_Cancelled && loanApplication.Sub_Stage__c == Constants.Loan_Cancel && loanApplication.Date_of_Cancellation__c != null && String.isNotBlank(Label.Document_Movement_Cancelled_Loan) && loanApplication.Days_Since_Loan_Cancelled__c == Decimal.valueOf(Label.Document_Movement_Cancelled_Loan).setScale(0)) {
                return true;
            } else if(loanApplication.StageName__c == Constants.Loan_Rejected && loanApplication.Sub_Stage__c == Constants.Loan_reject && loanApplication.Date_of_Rejection__c != null && String.isNotBlank(Label.Document_Movement_Rejected_Loan) && loanApplication.Days_Since_Loan_Rejected__c == Decimal.valueOf(Label.Document_Movement_Rejected_Loan).setScale(0)) {
                return true;
            }
        }
        return false;
    }
    
    /*
    *   Purpose:    This method is used to get the folder name for document check list
    *   Parameters: 
    *   UnitTests:  
    */
    public static string getFolderName(Document_Checklist__c documentChecklist, Loan_Application__c loanApplication, Map<String, S3_Folders_Configuration__mdt> folderConfigsByName){
        
        // Folder config name
        String folderConfigName = '';
        
        // Get folder config name
        if(documentCheckList != null){
            if(String.isNotBlank(documentCheckList.Loan_Contact__c) && String.isNotBlank(documentCheckList.Loan_Contact__r.Applicant_Type__c)) {
                if(documentCheckList.Loan_Contact__r.Applicant_Type__c == 'Applicant') {
                    folderConfigName = 'Applicant'; 
                } else if(documentCheckList.Loan_Contact__r.Applicant_Type__c == Constants.COAPP) {
                    folderConfigName = 'Co_Applicant'; 
                } else if(documentCheckList.Customer_Type__c == Constants.Guarantor_RECORD) {
                    folderConfigName = 'Co_Applicant';
                }
            } else{
                folderConfigName = 'Application';
            }
        } else{
            folderConfigName = 'Notes_and_Attachments';
        }
        
        // Get folder name
        return folderConfigsByName.get(folderConfigName).Folder_Name__c + ' '+ loanApplication.Name;
    }
    
    /*
    Folder Info
    */
    public class FolderInfo{
        @TestVisible String name;
        @TestVisible String id;
        @TestVisible Set<Id> documents;
        @TestVisible Integer count;
    }
}