public class ThirdPartyOperationsAPF {
    @AuraEnabled
    public static Project__c getProjectInfo(Id recordId){
        system.debug('=='+recordId);
        return [SELECT Id, Name, Stage__c, Sub_Stage__c, Credit_Manager_User__c,  APF_Type__c, Verification_Count__c, Branch__r.Name, Branch__c, Assigned_Credit_Review__c, Assigned_Sales_User__c,  Account__c FROM Project__c WHERE Id = :recordId ]; 
    }
    
    @AuraEnabled
    public static String getMessage() {
        return 'Hello World!';
    }
    
    @AuraEnabled
    public static String thirdPartyCall(Project__c objProject){
        if(objProject != null) {
            List<User_Branch_Mapping__c> ubCredList = new List<User_Branch_Mapping__c>();
            List<User_Branch_Mapping__c> ubList = new List<User_Branch_Mapping__c>();
            List<User_Branch_Mapping__c> ubTechList = new List<User_Branch_Mapping__c>();
            List<Project_Builder__c> lstProjectBuilder = [Select Id, Name, Constitution__c, Pincode__r.Name, Address_Line_1__c, Address_Line_2__c, City__c, State__c From Project_Builder__c Where Project__c =: objProject.Id];
            Id LegalRecordTypeId = Schema.SObjectType.Third_Party_Verification_APF__c.getRecordTypeInfosByName().get(Constants.strRecordTypeTPVLegal).getRecordTypeId();
            Id TechnicalRecordTypeId = Schema.SObjectType.Third_Party_Verification_APF__c.getRecordTypeInfosByName().get(Constants.strRecordTypeTPVTechnical).getRecordTypeId();
            Id FCURecordTypeId = Schema.SObjectType.Third_Party_Verification_APF__c.getRecordTypeInfosByName().get(Constants.strRecordTypeTPVFCU).getRecordTypeId();
            List<Project_Document_Checklist__c> lstProjDocChkLst = new List<Project_Document_Checklist__c>();
            List<Third_Party_Verification_APF__c> lstThirdPartyAPF = new List<Third_Party_Verification_APF__c>();
            
            ubCredList = [Select Id, User__r.Role_Assigned_Date__c,user__r.Profile.Name,User__r.UserRole.Name, User__c, Servicing_Branch__r.Pincode_Name__c from User_Branch_Mapping__c where User__r.isActive = TRUE and User__r.Profile.Name =: 'Credit Team' and User__r.UserRole.Name LIKE 'Credit Manager%' and Servicing_Branch__r.Name =: objProject.Branch__r.Name ORDER BY User__r.Role_Assigned_Date__c ASC];
            system.debug('***ubCredList**'+ubCredList.size());
            
            ubList = [Select Id, User__r.FI__c, User__r.Legal__c, User__r.FCU__c, User__r.LIP__c, User__r.PD__c, User__r.Role_Assigned_Date__c,user__r.Profile.Name,User__r.UserRole.Name, User__c, Servicing_Branch__r.Pincode_Name__c from User_Branch_Mapping__c where User__r.isActive = TRUE and User__r.Profile.Name = 'Third Party Vendor' and (User__r.FI__c = TRUE or User__r.Legal__c = TRUE or User__r.FCU__c = TRUE or User__r.LIP__c = TRUE or User__r.PD__c = TRUE)];
        
            ubTechList = [Select Id, User__r.Technical__c, User__r.Role_Assigned_Date__c,user__r.Profile.Name,User__r.UserRole.Name, User__c, Servicing_Branch__r.Pincode_Name__c from User_Branch_Mapping__c where User__r.isActive = TRUE and User__r.Profile.Name = 'Third Party Vendor' and User__r.Technical__c = TRUE and Servicing_Branch__c =: objProject.Branch__c ORDER BY User__r.Role_Assigned_Date__c ASC];
        
            
            lstProjDocChkLst = [select id,Document_Uploaded__c,Edited__c,Project_Builder__c, Locked__c, File_Check_Completed__c, Scan_Check_Completed__c, Project_Builder__r.Builder_Name__r.Name, Project__c, Project__r.Stage__c, Document_Type__r.name,Document_Master__c,Document_Master__r.name,Document_Master__r.Document_Title__c,Status__c,Notes_Remarks__c,Received_Stage__c, Mandatory__c from Project_Document_Checklist__c where Project__c =:objProject.Id ORDER BY Document_Type__r.name ASC];
            System.debug('Debug Log for lstProjDocChkLst'+lstProjDocChkLst);
            
            if(!lstProjectBuilder.isEmpty() && objProject.APF_Type__c != Constants.strApfTypeDeemed) {
                for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                    //Creation Logic for all FCU Verifications
                    lstThirdPartyAPF.add(fcuVerification(objProject, objProjectBuilder, ubList, FCURecordTypeId, ubCredList));
                }
            }
            
            lstThirdPartyAPF.addAll(propertyVerifications(objProject, ubList, ubTechList, LegalRecordTypeId, TechnicalRecordTypeId, ubCredList));
            /*
            Id OwnerIdLegal;
            Datetime ladLegal = System.now();
            Datetime radLegal;
            
            Third_Party_Verification_APF__c objTPVLegal = new Third_Party_Verification_APF__c();
                objTPVLegal.RecordTypeId = LegalRecordTypeId;
                objTPVLegal.Project__c = objProject.Id;
                objTPVLegal.Partner_Community_Login__c = Label.Third_Party_Community_Login;
                objTPVLegal.Manually_Created_Record__c = false;
                objTPVLegal.LA_Credit_Manager_User__c = objProject.Assigned_Credit_Review__c;
                objTPVLegal.LA_Sales_Manager_User__c = objProject.Assigned_Sales_User__r.ManagerId;
                for(User_Branch_Mapping__c ub: ubList){
                    if(ub.User__r.Legal__c == TRUE && ub.Servicing_Branch__c == objProject.Branch__c){
                        radLegal = ub.User__r.Role_Assigned_Date__c;
                        if(radLegal == null){
                            OwnerIdLegal = ub.User__c;
                        }
                        else if(radLegal < ladLegal){
                            ladLegal = radLegal;
                            OwnerIdLegal = ub.User__c;
                        }
                    }      
                }
                objTPVLegal.Owner__c = OwnerIdLegal;
                if(objTPVLegal.Owner__c != null){
                    objTPVLegal.Status__c = Constants.ASSIGNED;
                }
                else{
                    objTPVLegal.Status__c = Constants.NEW_VALUE;
                }
            
            lstThirdPartyAPF.add(objTPVLegal);
            
            
            Third_Party_Verification_APF__c tprTechnical = new Third_Party_Verification_APF__c();
            Id OwnerIdTechnical;
            Datetime ladTechnical = System.now();
            Datetime radTechnical;
            
            tprTechnical.recordtypeid = TechnicalRecordTypeId;
            tprTechnical.Project__c = objProject.Id;
            tprTechnical.Partner_Community_Login__c = Label.Third_Party_Community_Login;
            //tprTechnical.Property_Address__c = prop.Street__c+', '+prop.City__c+', '+prop.State__c+', '+prop.Country__c+', '+prop.Pincode__r.Name;
            //tprTechnical.Property__c = prop.Id;
            tprTechnical.Manually_Created_Record__c = FALSE;
            tprTechnical.LA_Credit_Manager_User__c = objProject.Assigned_Credit_Review__c;
            tprTechnical.LA_Sales_Manager_User__c = objProject.Assigned_Sales_User__r.ManagerId;
            for(User_Branch_Mapping__c ubt : ubTechList){
                if(ubt.Servicing_Branch__c == objProject.Branch__c){
                    tprTechnical.Owner__c = ubt.User__c;
                    radTechnical = ubt.User__r.Role_Assigned_Date__c;
                    if(radTechnical == null){
                        OwnerIdTechnical = ubt.User__c;
                    }
                    else if(radTechnical < ladTechnical){
                        ladTechnical = radTechnical;
                        OwnerIdTechnical = ubt.User__c;
                    }
                }
            }
            tprTechnical.Owner__c = OwnerIdTechnical;
            
            if(tprTechnical.Owner__c != null){
                tprTechnical.Status__c = Constants.ASSIGNED;
            }
            else{
                tprTechnical.Status__c = Constants.NEW_VALUE;
            }
            System.debug('----tprTechnicalOwner----'+tprTechnical.Owner__c);
            lstThirdPartyAPF.add(tprTechnical);
            
            */
            //FCU
            /*
            Third_Party_Verification_APF__c tprFCU = new Third_Party_Verification_APF__c();
            Id OwnerIdFCU;
            Datetime ladFCU = System.now();
            Datetime radFCU;
            
            tprFCU.Project__c = objProject.Id;
            tprFCU.Builder__c = objProject.Account__c; //Added by Abhilekh on 5th September 2019 for FCU BRD
            tprFCU.FCU_Type__c = Constants.Pre_Sanction; //Added by Abhilekh on 5th September 2019 for FCU BRD
            //tprFCU.Customer_Details__c = c.Id; //Commented by Abhilekh on 5th September 2019 for FCU BRD
            //tprFCU.Customer_Segment__c = c.Customer_segment__c; //Commented by Abhilekh on 5th September 2019 for FCU BRD
            tprFCU.Partner_Community_Login__c = Label.Third_Party_Community_Login;
            tprFCU.RecordTypeId = FCURecordTypeId;
            tprFCU.Manually_Created_Record__c = FALSE;
            tprFCU.LA_Credit_Manager_User__c = objProject.Assigned_Credit_Review__c;
            tprFCU.LA_Sales_Manager_User__c = objProject.Assigned_Sales_User__r.ManagerId;
            
            for(User_Branch_Mapping__c ub: ubList){
              system.debug('PROFILE---SVB---BRNCH'+ub.User__r.Profile.Name+ub.Servicing_Branch__c+objProject.Branch__c);
                if(ub.User__r.FCU__c == TRUE && ub.Servicing_Branch__c == objProject.Branch__c){
                
                radFCU = ub.User__r.Role_Assigned_Date__c;
                if(radFCU == null){
                  OwnerIdFCU = ub.User__c;
                }
                else if(radFCU < ladFCU){
                  ladFCU = radFCU;
                  OwnerIdFCU = ub.User__c;
                }
                
              }
          
            }
            if(ubCredList.size()>0){
                tprFCU.Default_Credit_Manager__c = ubCredList[0].User__c;
            }
            tprFCU.Owner__c = OwnerIdFCU;
            if(tprFCU.Owner__c != null){
              tprFCU.Status__c = Constants.ASSIGNED;
            }
            else{
              tprFCU.Status__c = Constants.NEW_VALUE;
            }
            System.debug('----tprFCUOwner----'+tprFCU.Owner__c);
            tprFCU.FCU_Internal_Manager__c = objProject.Assigned_Credit_Review__c;
            
            lstThirdPartyAPF.add(tprFCU);
            */
            if(!lstThirdPartyAPF.isEmpty()) {
                insert lstThirdPartyAPF;
            }
        }
        
        
        return '';
    }
    
    public static List<Third_Party_Verification_APF__c> propertyVerifications(Project__c objProject, List<User_Branch_Mapping__c> ubList, List<User_Branch_Mapping__c> ubTechList, Id LegalRecordTypeId, Id TechnicalRecordTypeId, List<User_Branch_Mapping__c> ubCredList){
    	List<Third_Party_Verification_APF__c> tprList = new List<Third_Party_Verification_APF__c>();
    	
    	
    	Third_Party_Verification_APF__c tprLegal = new Third_Party_Verification_APF__c();
    		  
    	Id OwnerIdLegal;
    	Datetime ladLegal = System.now();
    	Datetime radLegal;
    		  
    	tprLegal.Project__c = objProject.Id;
    	tprLegal.Partner_Community_Login__c = Label.Third_Party_Community_Login;
    	//tprLegal.Property_Address__c = prop.Street__c+', '+prop.City__c+', '+prop.State__c+','+prop.Country__c+', '+prop.Pincode__r.Name;
    	//tprLegal.Property__c = prop.Id;
    	tprLegal.RecordTypeId = LegalRecordTypeId;
    	tprLegal.Manually_Created_Record__c = FALSE;
    	tprLegal.LA_Credit_Manager_User__c = objProject.Credit_Manager_User__c;
    	tprLegal.LA_Sales_Manager_User__c = objProject.Assigned_Sales_User__c;
    	if(ubCredList.size()>0){
    		tprLegal.Default_Credit_Manager__c = ubCredList[0].User__c;      
    	}
    	for(User_Branch_Mapping__c ub: ubList){
    		if(ub.User__r.Legal__c == TRUE && ub.Servicing_Branch__c == objProject.Branch__c){
    			radLegal = ub.User__r.Role_Assigned_Date__c;
    			if(radLegal == null){
    				OwnerIdLegal = ub.User__c;
    			}
    			else if(radLegal < ladLegal){
    				ladLegal = radLegal;
    				OwnerIdLegal = ub.User__c;
    			}
    		}      
    	}
    	tprLegal.Owner__c = OwnerIdLegal;
    	if(tprLegal.Owner__c != null){
    		tprLegal.Status__c = Constants.ASSIGNED;
    	}
    	else{
    		tprLegal.Status__c = Constants.NEW_VALUE;
    	}
    	System.debug('----tprLegalOwner----'+tprLegal.Owner__c);
    	if(objProject.APF_Type__c != Constants.strApfTypeDeemed) {//Initiate only for New APF
    	    tprList.add(tprLegal);
    	}
    	
    			
		Third_Party_Verification_APF__c tprTechnical = new Third_Party_Verification_APF__c();
		Id OwnerIdTechnical;
		Datetime ladTechnical = System.now();
		Datetime radTechnical;
	  
		tprTechnical.recordtypeid = TechnicalRecordTypeId;
		tprTechnical.Project__c = objProject.Id;
		
		tprTechnical.Partner_Community_Login__c = Label.Third_Party_Community_Login;
		//tprTechnical.Property_Address__c = prop.Street__c+', '+prop.City__c+', '+prop.State__c+', '+prop.Country__c+', '+prop.Pincode__r.Name;
		//tprTechnical.Property__c = prop.Id;
		tprTechnical.Manually_Created_Record__c = FALSE;
		tprTechnical.LA_Credit_Manager_User__c = objProject.Credit_Manager_User__c;
		tprTechnical.LA_Sales_Manager_User__c = objProject.Assigned_Sales_User__c;
		
		/*for(User_Branch_Mapping__c ubt : ubTechList){
			if(ubt.Servicing_Branch__c == objProject.Branch__c){
				//tprTechnical.Owner__c = ubt.User__c;
				radTechnical = ubt.User__r.Role_Assigned_Date__c;
				if(radTechnical == null){
					OwnerIdTechnical = ubt.User__c;
				}
				else if(radTechnical < ladTechnical){
					ladTechnical = radTechnical;
					OwnerIdTechnical = ubt.User__c;
				}
			}
		}*/
		if(!ubTechList.isEmpty()) {
		    OwnerIdTechnical =  ubTechList[0].User__c;
		}
		
		tprTechnical.Owner__c = OwnerIdTechnical;
		if(tprTechnical.Owner__c != null){
		  tprTechnical.Status__c = Constants.ASSIGNED;
		}
		else{
		  tprTechnical.Status__c = Constants.NEW_VALUE;
		}
		System.debug('----tprTechnicalOwner----'+tprTechnical.Owner__c);
		if(ubCredList.size()>0){
			tprTechnical.Default_Credit_Manager__c = ubCredList[0].User__c;
		}
		tprList.add(tprTechnical);
    	
    	return tprList;
    }

    public static Third_Party_Verification_APF__c fcuVerification(Project__c l, Project_Builder__c c, List<User_Branch_Mapping__c> ubList, Id FCURecordTypeId, List<User_Branch_Mapping__c> ubCredList){
        
        
        
        Third_Party_Verification_APF__c tprFCU = new Third_Party_Verification_APF__c();
        Id OwnerIdFCU;
        Datetime ladFCU = System.now();
        Datetime radFCU;
        
        tprFCU.Project__c = l.Id;
        tprFCU.Project_Builder__c = c.Id;
        //tprFCU.Customer_Segment__c = c.Customer_segment__c;
        tprFCU.Partner_Community_Login__c = Label.Third_Party_Community_Login;
        tprFCU.RecordTypeId = FCURecordTypeId;
        tprFCU.Manually_Created_Record__c = FALSE;
        tprFCU.LA_Credit_Manager_User__c = l.Credit_Manager_User__c;
        tprFCU.LA_Sales_Manager_User__c = l.Assigned_Sales_User__c;
        
        for(User_Branch_Mapping__c ub: ubList){
          system.debug('PROFILE---SVB---BRNCH'+ub.User__r.Profile.Name+ub.Servicing_Branch__c+l.Branch__c);
            if(ub.User__r.FCU__c == TRUE && ub.Servicing_Branch__c == l.Branch__c){
            
            radFCU = ub.User__r.Role_Assigned_Date__c;
            if(radFCU == null){
              OwnerIdFCU = ub.User__c;
            }
            else if(radFCU < ladFCU){
              ladFCU = radFCU;
              OwnerIdFCU = ub.User__c;
            }
            
          }
      
        }
        if(ubCredList.size()>0){
            tprFCU.Default_Credit_Manager__c = ubCredList[0].User__c;
        }
        tprFCU.Owner__c = OwnerIdFCU;
        if(tprFCU.Owner__c != null){
          tprFCU.Status__c = Constants.ASSIGNED;
        }
        else{
          tprFCU.Status__c = Constants.NEW_VALUE;
        }
        System.debug('----tprFCUOwner----'+tprFCU.Owner__c);
        tprFCU.FCU_Internal_Manager__c = l.Credit_Manager_User__c;
        
        return tprFCU;
    }
}