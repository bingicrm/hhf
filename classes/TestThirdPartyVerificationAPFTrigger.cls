@isTest
public class TestThirdPartyVerificationAPFTrigger {
    static testmethod void test1(){
        List<RecordType> lstRecordType = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Individual'];
        Id builderIndvRecTypeId;
        if(!lstRecordType.isEmpty()) {
            builderIndvRecTypeId = lstRecordType[0].Id;
        }
        
        Account acc = new Account();
        acc.RecordTypeId = builderIndvRecTypeId;
        acc.Name = 'Individual Test Acc';
        acc.PAN__c = 'AAAAA3333B';
        acc.GSTIN__c = '27AAHFG0551H1ZL';
        insert acc;
        
        List<RecordType> lstRecordType2 = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Corporate'];
        Id builderCorpRecTypeId;
        if(!lstRecordType2.isEmpty()) {
            builderCorpRecTypeId = lstRecordType2[0].Id;
        }
        
        List<RecordType> lstIndividualPBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Individual'];
        List<RecordType> lstCorporatePBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Corporate'];
        
        Account acc2 = new Account();
        acc2.RecordTypeId = builderCorpRecTypeId;
        acc2.Name = 'Test Corporate Account';
        acc2.PAN__c = 'AAAAB2222B';
        
        insert acc2;
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83');
        insert objPincode;
        
        Document_Master__c docMasObj = new Document_Master__c();
        docMasObj.Name = 'Sales Brochure and Carpet/Built up - Sale area statement for Units';
        docMasObj.Applicable_for_APF__c = true;
        docMasObj.Mandatory_for_APF__c = true;
        docMasObj.Document_Title__c = 'Sales Brochure and Carpet/Built up - Sale area statement for Units';
        Database.insert(docMasObj);
        
        Document_type__c docTypeObj = new Document_type__c();
        docTypeObj.Name = Constants.strAPFProjDocTypeLegal;
        docTypeObj.Applicable_for_APF__c = true;  
        Database.insert(docTypeObj);
        
        APF_Document_Checklist__c objAPF = new APF_Document_Checklist__c();
        objAPF.Document_Master__c = docMasObj.Id;
        objAPF.Document_Type__c = docTypeObj.id;
        objAPF.Mandatory__c = true;
        objAPF.Valid_for_Project_Type__c = 'New';
        insert objAPF;
        
        Project__c objProject = new Project__c();
        //objProject.Assigned_Sales_User__c = assignedSalesUser != null ? assignedSalesUser.Id : null;
        objProject.Account__c = acc.Id;
        objProject.Project_Name__c = 'Test Project';
        objProject.APF_Status__c = 'APF Initiated';
        objProject.Stage__c = 'APF Initiation';
        objProject.Sub_Stage__c = 'Project Data Capture';
        objProject.APF_Type__c = 'New';
        objProject.Address_Line_1__c = 'Test Address';
        objProject.Pincode__c = objPincode.Id;
        insert objProject;
        
        Project_Builder__c objProjectBuilderCorp = new Project_Builder__c();
        objProjectBuilderCorp.RecordTypeId = lstCorporatePBId[0].Id;
        objProjectBuilderCorp.Builder_Name__c = acc2.Id;
        objProjectBuilderCorp.Constitution__c = '24';
        objProjectBuilderCorp.Builder_Status__c = 'Resident';
        objProjectBuilderCorp.Contact_Number_Mobile__c = '9999444223';
        objProjectBuilderCorp.PAN_Number__c = 'EGNPS1111T';
        objProjectBuilderCorp.Project__c = objProject.Id; 
        objProjectBuilderCorp.Preferred_Communication_Address__c = 'CURRES'; 
        Database.insert(objProjectBuilderCorp,false);
        
        acc2.GSTIN__c = '27AAHFG0551H1ZL';
        update acc2;
        
        insert objProjectBuilderCorp;
        System.debug('Debug Log for GSTIN'+objProjectBuilderCorp);
        
        
        objProjectBuilderCorp.GSTIN_Number__c = '27AAHFG0551H1ZL';
        update objProjectBuilderCorp;
        
        Set<Id> setProjectId = new Set<Id>();
        setProjectId.add(objProject.Id);
        
        List<Project_Builder__c> lstProjectBuilder = [SELECT Id, RecordTypeId, Builder_Category__c, GSTIN_Available__c, Name, Date_Of_Birth__c, Builder_Name__r.RecordType.DeveloperName, All_Promoter_Count__c, Builder_Type__c, Class_Of_Activity__c, Builder_Name__c, Builder_Name__r.RecordTypeId, Builder_Status__c, Constitution__c, Builder_Role__c, Contact_Number_Mobile__c, Email__c, Preferred_Communication_Address__c, Address_Line_1__c, Address_Line_2__c, Pincode__c, Promoter_PAN_Validation_Count__c, Promoter_Cibil_Validation_Count__c, Project__c,(Select Id, Name, Name_of_Promoter__c, Email__c, Contact_No_Mobile__c, Preferred_Communication_Address__c, Promoter_Status__c, Address_Line_1__c, Address_Line_2__c, Pincode__c from Promoters__r) From Project_Builder__c Where Project__c =: objProject.Id];
        System.debug('Debug Log for lstProjectBuilder size'+lstProjectBuilder.size());
        
        List<Promoter__c> lstPromoter = new List<Promoter__c>();
        if(!lstProjectBuilder.isEmpty()) {
            for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                objProjectBuilder.GSTIN_Available__c = true;
                objProjectBuilder.GSTIN_Number__c = '27AAHFG0551H1ZL';
                if(objProjectBuilder.Promoters__r.isEmpty() && objProjectBuilder.Builder_Name__r.RecordType.DeveloperName != 'Builder_Individual'){
                    Promoter__c objPromoter = new Promoter__c(Project_Builder__c = objProjectBuilder.Id, Preferred_Communication_Address__c = 'CURRES');
                    lstPromoter.add(objPromoter);
                }
            }
            update lstProjectBuilder;
            System.debug('Debug Log for lstPromoter'+lstPromoter.size());
            if(!lstPromoter.isEmpty()) {
                insert lstPromoter;
            }
            System.debug('Debug Log for lstPromoter post insertion from Address Check Method'+lstPromoter);
            update lstPromoter;
            
            Pincode__c pin=new Pincode__c(Name='TEST',City__c='TEST',ZIP_ID__c='TEST');
            Database.insert(pin);
            
            Address_Detail_Promoter__c objAddressDetail = new Address_Detail_Promoter__c(Type_of_address__c = lstPromoter[0].Preferred_Communication_Address__c,
                                                                                         Residence_Type__c = 'C', Pincode_LP__c = pin.Id, Address_Line_1__c = 'Test Address Line 1',
                                                                                         Address_Line_2__c = 'Test Address Line 2', Promoter__c = lstPromoter[0].Id);
            
            insert objAddressDetail;
            
            objProject.RecordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeCreditApproval).getRecordTypeId();
            objProject.Stage__c = 'APF Inventory Maintenance';
            objProject.Sub_Stage__c = 'APF Inventory Update';
            objProject.APF_Status__c = 'APF Approved';
            update objProject;
            
            Tower__c objTower = new Tower__c(Project__c = objProject.Id, Name = 'Villa 1');
            insert objTower;
            
            Project_Inventory__c objProjectInventory = new Project_Inventory__c(Project__c = objProject.Id, Tower__c = objTower.Id, Flat_House_No__c = '123', 
                                                                                Floor__c = 'Second Floor', LCR_per_sq_ft__c = 100, Area__c = 500, 
                                                                                Construction_Status__c = 'Under Construction',
                                                                                Inventory_Description__c = '1',Inventory_Status__c = 'Available',
                                                                                Type_of_Inventory__c = 'Residential');
            insert objProjectInventory;
            
            objProject.APF_Status__c= 'APF Inventory Approved';
            update objProject;
            
            objProjectInventory.Inventory_Approval_Status__c = 'Approved';
            objProjectInventory.Flat_House_No__c = '123B';
            update objProjectInventory;
            /*
            Document_type__c docTypeObj2 = new Document_type__c();
            docTypeObj2.Name = Constants.strAPFProjDocTypeLegal;
            docTypeObj2.Approving_authority__c    = 'Business Head';
            docTypeObj2.Applicable_for_APF__c = true;  
            Database.insert(docTypeObj2);
            
            Document_type__c docTypeObj3 = new Document_type__c();
            docTypeObj3.Name = Constants.strAPFProjDocTypeTechnical;
            docTypeObj3.Approving_authority__c    = 'Business Head';
            docTypeObj3.Applicable_for_APF__c = true;  
            Database.insert(docTypeObj3);
            
            Project_Document_Checklist__c pdcObj = new Project_Document_Checklist__c(Project__c=objProject.Id,
                                                                                    Document_Type__c=docTypeObj2.Id);
            insert pdcObj;
            
            Project_Document_Checklist__c pdcObj2 = new Project_Document_Checklist__c(Project__c=objProject.Id,
                                                                                    Document_Type__c=docTypeObj3.Id);
            insert pdcObj2;*/
            
            Id LegalRecordTypeId = Schema.SObjectType.Third_Party_Verification_APF__c.getRecordTypeInfosByName().get(Constants.strRecordTypeTPVLegal).getRecordTypeId();
            Id TechnicalRecordTypeId = Schema.SObjectType.Third_Party_Verification_APF__c.getRecordTypeInfosByName().get(Constants.strRecordTypeTPVTechnical).getRecordTypeId();
            Id FCURecordTypeId = Schema.SObjectType.Third_Party_Verification_APF__c.getRecordTypeInfosByName().get(Constants.strRecordTypeTPVFCU).getRecordTypeId();
            
            Third_Party_Verification_APF__c tpvAPFObj = new Third_Party_Verification_APF__c(RecordTypeId=LegalRecordTypeId, Project__c=objProject.Id);
            insert tpvAPFObj;
            
            Third_Party_Verification_APF__c tpvAPFObj2 = new Third_Party_Verification_APF__c(RecordTypeId=TechnicalRecordTypeId, Project__c=objProject.Id);
            insert tpvAPFObj2;
            
            Third_Party_Verification_APF__c tpvAPFObj3 = new Third_Party_Verification_APF__c(RecordTypeId=FCURecordTypeId, Project__c=objProject.Id);
            insert tpvAPFObj3;
        }
    }
}