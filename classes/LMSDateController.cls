@RestResource(urlMapping='/v1/LMS Date Sync/*')
global class LMSDateController {

    // GET request
    @HttpGet
    global static List<LMS_Date_Sync__c> getRecords() {
        List<LMS_Date_Sync__c> records = [SELECT Id, Name, Current_Date__c, EOD_BOD__c from LMS_Date_Sync__c];
        return records;
    }
    
    // POST request to create the Student into System
    @HttpPost 
    global static LMS_Date_Sync__c createNewRecord(String Name, Date CurrentDate, String EOD) {
        LMS_Date_Sync__c rec = new LMS_Date_Sync__c();
        rec.Current_Date__c = CurrentDate;
        rec.EOD_BOD__c = EOD;
        insert rec;
        String returnMesasge = 'You have successfully created a LMS Date Sync record into Salesforce - '+'Record Id of the record is - '+rec.id;
        //return 'Created '+returnMesasge;
        return [Select Id, Name, Current_Date__c, EOD_BOD__c, Owner.Name From LMS_Date_Sync__c Where Id=:rec.id];
   }

    // Delete to delete the given Student
    /*@HttpDelete
    global static String deleteRecordById() {
        String Id = RestContext.request.params.get('Id');
        List<LMS_Date_Sync__c> r = [ Select ID from LMS_Date_Sync__c where Id= :Id];

        delete r;

        return 'Deleted Record';
    }

    // Update the Student Record
    @HttpPut
    global static String updateRecord(String Id, String newEOD) {
        LMS_Date_Sync__c stnd = [ Select ID, Name, EOD_BOD__c from LMS_Date_Sync__c where Id= :Id];

        stnd.EOD_BOD__c = newEOD;
        update stnd;

        return 'Updated record';
    }*/
}