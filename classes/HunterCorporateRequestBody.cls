public class HunterCorporateRequestBody{
    public String sClassification;	//ACCEPTED
    public String sIdentifier;	//abcd
    public String sLoanPurpose;	//BUY FIRST HOUSE
    public String sTerm;	//12
    public String sValue;	//11
    public String sAssetOrignalValue;	//1100
    public String sAssetValuation;	//11000
    public String sInternalFlag;	//START
    public String sPriorityScore;	//11
    public String sBranchRegion;	//ABCD
    public String sSubmissionDate;	//2019-09-02
    public String sProduct;	//SME_ACC
    public String sApplicationDate;	//2019-08-02
    public cls_oCompanyDetails[] oCompanyDetails;
    public cls_oMainPromoter oMainPromoter;
    public cls_oCoPromoter[] oCoPromoter;
    public cls_oReference[] oReference;
    //public cls_oBroker oBroker;
    //public cls_oVehicle oVehicle;
    public class cls_oCompanyDetails {
        public String sOrgName;	//Softcell Technologies
        public String sIndustry;	//INFORMATION TECHNOLOGY
        public String sConstitution;	//
        public String sTAN;	//PDES03028F
        public String sDateOfIncorporation;	//2014-11-01
        public String sCompanyRegNumber;	//CRN111fff22ff
        public String sJobTitle;	//SOFTWARE ENGINEER
        public String sStartDate;	//2017-02-02
        public String sSalesTaxRegNumber;	//CDJCHDC123
        public String sCompanyAnnualTurnover;	//10000000
        public String sEmployeeNumber;	//002010
        public String sTimeWithEmployment;	//11
        //public String sStatus;	//Clear
        public cls_oCompanyAddress oCompanyAddress;
        public cls_oBusinessTelephone[] oBusinessTelephone;
        public cls_oBankDetails oBankDetails;
        public cls_oBusinessEmail[] oBusinessEmail;
        public cls_oCompanyDetails(){
            this.sOrgName ='Not Available';	//Softcell Technologies
            this.sIndustry ='';	//INFORMATION TECHNOLOGY
            this.sConstitution ='';	//
            this.sTAN ='';	//PDES03028F
            this.sDateOfIncorporation ='';	//2014-11-01
            this.sCompanyRegNumber ='';	//CRN111fff22ff
            this.sJobTitle ='';	//SOFTWARE ENGINEER
            this.sStartDate ='';	//2017-02-02
            this.sSalesTaxRegNumber ='';	//CDJCHDC123
            this.sCompanyAnnualTurnover ='';	//10000000
            this.sEmployeeNumber ='';	//002010
            this.sTimeWithEmployment ='';	//11
            //this.sStatus ='Clear';	//Clear
            this.oCompanyAddress =new cls_oCompanyAddress();
            this.oBusinessTelephone =new List<cls_oBusinessTelephone>();
            this.oBankDetails =new cls_oBankDetails();
            this.oBusinessEmail =new List<cls_oBusinessEmail>();    
        }
    }
    public class cls_oCompanyAddress {
        public String sAddressLines;	//wakdewadi
        public String sCity;	//shivaji nagar
        public String sState;	//maharashtra
        public String sCountry;	//India
        public String sPincode;	//411018
        //public String sStatus;	//Clear
        public cls_oCompanyAddress(){
            this.sAddressLines ='';	//wakdewadi
            this.sCity='';	//shivaji nagar
            this.sState='';	//maharashtra
            this.sCountry='';	//India
            this.sPincode='';	//411018
            //this.sStatus='Clear';	//Clear    
        }
    }
    public class cls_oCompanyPropertyAddress {
        public String sAddressLines;
        public String sCity;
        public String sState;
        public String sCountry;
        public String sPincode;
        
        public cls_oCompanyPropertyAddress(){
            this.sAddressLines = '';
            this.sCity = '';
            this.sState = '';
            this.sCountry = '';
            this.sPincode = '';
        }
    }
    public class cls_oBusinessTelephone {
        public String sTelephoneNo;	//111111111
        //public String sStatus;	//Clear
        public cls_oBusinessTelephone(){
            this.sTelephoneNo ='';	//111111111
            //this.sStatus ='Clear';    
        }
    }
    public class cls_oBankDetails {
        public String sBankName;	//STATE BANK OF INDIA
        public String sAccountNumber;	//33221499499
        public String sBranchName;	//SHIVAJINAGR
        public String sMicrCode;	//
        public cls_oBankDetails(){
            this.sBankName ='';	//STATE BANK OF INDIA
            this.sAccountNumber ='';	//33221499499
            this.sBranchName ='';	//SHIVAJINAGR
            this.sMicrCode ='';	    
        }
    }
    public class cls_oBusinessEmail {
        public String sEmailAddress;	//vishwajeet@softcell.com
        public String sCompanyAddress;	//support@softcell.com
        public String sDomain;	//softcell.com
        //public String sStatus;	//Clear
        public cls_oBusinessEmail(){
            this.sEmailAddress ='';	//vishwajeet@softcell.com
            this.sCompanyAddress ='';	//support@softcell.com
            this.sDomain ='';	//softcell.com
            //this.sStatus ='Clear';    
        }
    }
    public class cls_oMainPromoter {
        public String sPanNumber;	//BUMPP7688G
        public String sFirstName;	//Vishwajeet
        public String sMiddleName;	//PradeepKumar
        public String sLastName;	//Pandey
        public String sDOB;	//1996-11-26
        public String sAge;	//23
        public String sGender;	//MALE
        public String sQualification;	//Post Graduate
        public String sMaritalStatus;	//MARRIED
        public String sIncome;	//250000
        public String sNationality;	//INDIAN
        //public String sStatus;	//Clear
        public cls_oMainPromoterResidentialAddress oMainPromoterResidentialAddress;
        public cls_oMainPromoterPermanentAddress oMainPromoterPermanentAddress;
        public cls_oMainPromoterPreviousAddress[] oMainPromoterPreviousAddress;
        public cls_sHomeTelephone sHomeTelephone;
        public cls_sMobileTelephone sMobileTelephone;
        public cls_oBusinessTelephone[] oBusinessTelephone;
        public cls_oEmail[] oEmail;
        public cls_oBank oBank;
        public cls_oIdMainPromoterDocument[] oIdMainPromoterDocument;
        public cls_oMainPromoter(){
            this.sPanNumber ='';	//BUMPP7688G
            this.sFirstName ='';	//Vishwajeet
            this.sMiddleName ='';	//PradeepKumar
            this.sLastName ='';	//Pandey
            this.sDOB ='';	//1996-11-26
            this.sGender ='';	//MALE
            this.sQualification ='';	//Post Graduate
            this.sMaritalStatus ='';	//MARRIED
            this.sIncome ='';	//250000
            this.sNationality ='';	//INDIAN
            //this.sStatus ='Clear';	//Clear
            this.oMainPromoterResidentialAddress = new cls_oMainPromoterResidentialAddress();
            this.oMainPromoterPermanentAddress =  new cls_oMainPromoterPermanentAddress();
            this.oMainPromoterPreviousAddress = new List<cls_oMainPromoterPreviousAddress>();
            this.sHomeTelephone = new cls_sHomeTelephone();
            this.sMobileTelephone = new cls_sMobileTelephone();
            this.oBusinessTelephone = new List<cls_oBusinessTelephone>();
            this.oEmail = new List<cls_oEmail>();
            this.oBank =new cls_oBank();
            this.oIdMainPromoterDocument = new List<cls_oIdMainPromoterDocument>();
        }
    }
    public class cls_oMainPromoterResidentialAddress {
        public String sAddressLines;	//Pawar building Survey no -101
        public String sCity;	//Pimpri
        public String sState;	//Maharashtra
        public String sCountry;	//India
        public String sPincode;	//411018
        public String sTimeAtAddress;	//5
        public String sPropertyType;	//COTTAGE
        public String sResidentialStatus;	//SHARED OWNER
        //public String sStatus;	//Clear
        public cls_oMainPromoterResidentialAddress(){
            this.sAddressLines ='';	//Pawar building Survey no -101
            this.sCity ='';	//Pimpri
            this.sState ='';	//Maharashtra
            this.sCountry ='';	//India
            this.sPincode ='';	//411018
            this.sPropertyType ='';	//COTTAGE
            this.sResidentialStatus ='';	//SHARED OWNER
            //this.sStatus ='Clear';
            this.sTimeAtAddress = '';
        }
    }
    public class cls_oMainPromoterPermanentAddress {
        public String sAddressLines;	//Pawar building Survey no -101
        public String sCity;	//Pimpri
        public String sState;	//Maharashtra
        public String sCountry;	//India
        public String sPincode;	//411018
        public String sTimeAtAddress;	//5
        public String sPropertyType;	//COTTAGE
        public String sResidentialStatus;	//SHARED OWNER
        //public String sStatus;	//Clear
        public cls_oMainPromoterPermanentAddress(){
            this.sAddressLines ='';	//Pawar building Survey no -101
            this.sCity ='';	//Pimpri
            this.sState ='';	//Maharashtra
            this.sCountry ='';	//India
            this.sPincode ='';	//411018
            this.sPropertyType ='';	//COTTAGE
            this.sResidentialStatus ='';	//SHARED OWNER
            //this.sStatus ='Clear';
            this.sTimeAtAddress = '';
        }
    }
    public class cls_oMainPromoterPreviousAddress {
        public String sAddressLines;	//Pawar building Survey no -101
        public String sCity;	//Pimpri
        public String sState;	//Maharashtra
        public String sCountry;	//India
        public String sPincode;	//411018
        public String sTimeAtAddress;	//5
        public String sPropertyType;	//COTTAGE
        public String sResidentialStatus;	//SHARED OWNER
        //public String sStatus;	//Clear
        public cls_oMainPromoterPreviousAddress(){
            this.sAddressLines ='';	//Pawar building Survey no -101
            this.sCity ='';	//Pimpri
            this.sState ='';	//Maharashtra
            this.sCountry ='';	//India
            this.sPincode ='';	//411018
            this.sPropertyType ='';	//COTTAGE
            this.sResidentialStatus ='';	//SHARED OWNER
            //this.sStatus ='Clear';
            this.sTimeAtAddress = '';
        }
    }
    public class cls_sHomeTelephone {
        public String sTelNo;	//111111111
        //public String sStatus;	//Clear
        public cls_sHomeTelephone(){
            this.sTelNo ='';	//111111111
            //this.sStatus ='Clear';
        }
    }
    public class cls_sMobileTelephone {
        public String sTelNo;	//111111111
        //public String sStatus;	//Clear
        public cls_sMobileTelephone(){
            this.sTelNo ='';	//111111111
            //this.sStatus ='Clear';
        }
    }
    public class cls_oEmail {
        public String sEmailAddress;	//vishwajeetpandey57@gmail.com
        public String sUserAddress;	//Pimpri
        public String sDomain;	//gmail.com
        //public String sStatus;	//Clear
        public cls_oEmail(){
            this.sEmailAddress ='';	//vishwajeetpandey57@gmail.com
            this.sUserAddress ='';	//Pimpri
            this.sDomain ='';	//gmail.com
            //this.sStatus ='Clear';
        }
    }
    public class cls_oBank {
        public String sBankName;	//AXIS BANK
        public String sAccountNumber;	//1234567890
        public String sBranchName;	//Pimpri
        public String sMicrCode;	//12345
        //public String sStatus;	//Clear
        public cls_oBank(){
            this.sBankName ='';	//AXIS BANK
            this.sAccountNumber ='';	//1234567890
            this.sBranchName ='';	//Pimpri
            this.sMicrCode ='';	//12345
            //this.sStatus ='Clear';    
        }
    }
    public class cls_oIdMainPromoterDocument {
        public String sDocumentType;	//PAN CARD
        public String sDocumentNumber;	//AFAPJ1001R
        public String sDocAccountNumber;	//123467
        public String sPlaceOfIssue;	//Pimri
        public String sCountryOfIssue;	//INDIA
        public String sIssueDate;	//2018-10-30
        public String sExpiryDate;	//2020-10-20
        public String sSubmissionDate;	//2019-11-21
        public String sEReceipt;	//12543
        public String sFatherHusbandName;	//Sunil
        public String sAckNumber;	//1234756890
        public String sCompanyTAN;	//SOFTCELL
        public String sIdDOB;	//1994-12-03
        //public String sStatus;	//Clear
        public cls_oIdMainPromoterDocument(){
            this.sDocumentType ='';	//PAN CARD
            this.sDocumentNumber ='';	//AFAPJ1001R
            this.sDocAccountNumber ='';	//123467
            this.sPlaceOfIssue ='';	//Pimri
            this.sCountryOfIssue ='';	//INDIA
            this.sIssueDate ='';	//2018-10-30
            this.sExpiryDate ='';	//2020-10-20
            this.sSubmissionDate ='';	//2019-11-21
            this.sEReceipt ='';	//12543
            this.sFatherHusbandName ='';	//Sunil
            this.sAckNumber ='';	//1234756890
            this.sCompanyTAN ='';	//SOFTCELL
            this.sIdDOB ='';	//1994-12-03
            //this.sStatus ='Clear';    
        }
    }
    public class cls_oCoPromoter {
        public String sPanNumber;	//BUMPP7688G
        public String sFirstName;	//Vishwajeet
        public String sMiddleName;	//PradeepKumar
        public String sLastName;	//Pandey
        public String sDOB;	//1996-11-26
        public String sAge;	//23
        public String sGender;	//MALE
        public String sQualification;	//post Graduate
        public String sMaritalStatus;	//MARRIED
        public String sIncome;	//320000
        public String sNationality;	//INDIAN
        public String sRelation;	//
        //public String sStatus;	//Clear
        public cls_oCoPromoterResidentialAddress oCoPromoterResidentialAddress;
        public cls_oIdCoPromoterDocument[] oIdCoPromoterDocument;
        public cls_oCoPromoter(){
            this.sPanNumber ='';	//BUMPP7688G
            this.sFirstName ='';	//Vishwajeet
            this.sMiddleName ='';	//PradeepKumar
            this.sLastName ='';	//Pandey
            this.sDOB ='';	//1996-11-26
            this.sGender ='';	//MALE
            this.sQualification ='';	//post Graduate
            this.sMaritalStatus ='';	//MARRIED
            this.sIncome ='';	//320000
            this.sNationality ='';	//INDIAN
            this.sRelation ='';	//
            //this.sStatus ='Clear';	//Clear
            this.oCoPromoterResidentialAddress = new cls_oCoPromoterResidentialAddress();
            this.oIdCoPromoterDocument = new List<cls_oIdCoPromoterDocument>();    
        }
    }
    public class cls_oCoPromoterResidentialAddress {
        public String sAddressLines;	//Pawar building Survey no -101
        public String sCity;	//Pimpri
        public String sState;	//MAHARASHTRA
        public String sCountry;	//INDIA
        public String sPincode;	//411018
        public String sTimeAtAddress;	//5
        public String sPropertyType;	//cottage
        public String sResidentialStatus;	//SHARED OWNER
        //public String sStatus;	//Clear
        public cls_oCoPromoterResidentialAddress(){
            this.sAddressLines ='';	//Pawar building Survey no -101
            this.sCity ='';	//Pimpri
            this.sState ='';	//MAHARASHTRA
            this.sCountry ='';	//INDIA
            this.sPincode ='';	//411018
            this.sPropertyType ='';	//cottage
            this.sResidentialStatus ='';	//SHARED OWNER
            //this.sStatus ='Clear'; 
            this.sTimeAtAddress = '';
        }
    }
    public class cls_oIdCoPromoterDocument {
        public String sDocumentType;	//PAN CARD
        public String sDocumentNumber;	//BUMPP7688G
        public String sDocAccountNumber;	//123455677
        public String sPlaceOfIssue;	//Pimpri
        public String sCountryOfIssue;	//Pune
        public String sIssueDate;	//2018-11-20
        public String sExpiryDate;	//2021-12-31
        public String sSubmissionDate;	//2019-11-02
        public String sEReceipt;	//123456
        public String sFatherHusbandName;	//Viajy
        public String sAckNumber;	//1234567
        public String sCompanyTAN;	//SOFTCELL
        public String sIdDOB;	//1989-02-11
        //public String sStatus;	//Clear
        public cls_oIdCoPromoterDocument(){
            this.sDocumentType='';	//Mayfair Towers Old Mumbai Pune Hwy
            this.sDocumentNumber='';	//Pune
            this.sDocAccountNumber='';	//Maharashtra
            this.sPlaceOfIssue='';
            this.sCountryOfIssue='';	//Pune
            this.sIssueDate='';	//2018-11-20
            this.sExpiryDate='';	//2021-12-31
            this.sSubmissionDate='';	//2019-11-02
            this.sEReceipt='';	//123456
            this.sFatherHusbandName='';	//Viajy
            this.sAckNumber='';	//1234567
            this.sCompanyTAN ='';	//SOFTCELL
            this.sIdDOB ='';	//1989-02-11
            //this.sStatus ='Clear';    
        }
    }
    public class cls_oReference {
        public String sFirstName;	//Mukesh
        public String sLastName;	//Mahto
        //public String sStatus;	//Clear
        public cls_oRefResidentialAddr oRefResidentialAddr;
        public cls_oHomeTelephone oHomeTelephone;
        public cls_oMobileTelephone oMobileTelephone;
        public cls_oReference(){
            this.sFirstName='';	//Mayfair Towers Old Mumbai Pune Hwy
            this.sLastName='';	//Pune
            //this.sStatus='Clear';	//Maharashtra
            this.oRefResidentialAddr=new cls_oRefResidentialAddr();	//INDIA
            this.oHomeTelephone =new cls_oHomeTelephone();	//411005
            this.oMobileTelephone =new cls_oMobileTelephone();	//3       
        }
    }
    public class cls_oRefResidentialAddr {
        public String sAddressLines;	//Mayfair Towers Old Mumbai Pune Hwy
        public String sCity;	//Pune
        public String sState;	//Maharashtra
        public String sCountry;	//INDIA
        public String sPincode;	//411005
        public String sTimeAtAddress;	//3
        public String sPropertyType;	//LAND
        public String sResidentialStatus;	//OTHER
        //public String sStatus;	//Clear
        public cls_oRefResidentialAddr(){
            this.sAddressLines='';	//Mayfair Towers Old Mumbai Pune Hwy
            this.sCity='';	//Pune
            this.sState='';	//Maharashtra
            this.sCountry='';	//INDIA
            this.sPincode ='';	//411005
            this.sPropertyType ='';	//LAND
            this.sResidentialStatus ='';	//OTHER
            //this.sStatus = 'Clear'; 
            this.sTimeAtAddress = '';
        }    
    }
    public class cls_oHomeTelephone {
        public String sTelNo;	//111111111
        //public String sStatus;	//Clear
        public cls_oHomeTelephone(){
            this.sTelNo = '';	//111111111
            //this.sStatus = 'Clear';
        }
    }
    public class cls_oMobileTelephone {
        public String sTelNo;	//111111111
        //public String sStatus;	//Clear
        public cls_oMobileTelephone(){
            this.sTelNo = '';	//111111111
            //this.sStatus = 'Clear';
        }
    }
    public class cls_oBroker {
        public String sOrgName;	//SOFTCELL TECHNOLOGY
        public String sOrgCode;	//12345
        public String sRepName;	//ABCDEF
        //public String sStatus;	//Clear
        public cls_oBrokerAddress oBrokerAddress;
        public cls_oBrokerNumber oBrokerNumber;
        public cls_oBroker(){
            this.sOrgName ='';	//SOFTCELL TECHNOLOGY
            this.sOrgCode = '';	//12345
            this.sRepName = '';	//ABCDEF
            //this.sStatus = 'Clear';	//Clear
            this.oBrokerAddress = new cls_oBrokerAddress();
            this.oBrokerNumber = new cls_oBrokerNumber();    
        }
    }
    public class cls_oBrokerAddress {
        public String sAddressLines;	//Mayfair Towers Old Mumbai Pune Hwy
        public String sCity;	//PUNE
        public String sState;	//null
        public String sCountry;	//maharashtra
        public String sPincode;	//411005
        //public String sStatus;	//Clear
        public cls_oBrokerAddress(){
            this.sAddressLines = '';	//Mayfair Towers Old Mumbai Pune Hwy
            this.sCity = '';	//PUNE
            this.sState = '';	//null
            this.sCountry = '';	//maharashtra
            this.sPincode = '';	//411005
            //this.sStatus = 'Clear';	//Clear    
        }
    }
    public class cls_oBrokerNumber {
        public String sTelNo;	//111111111
        //public String sStatus;	//Clear
        public cls_oBrokerNumber(){
            this.sTelNo = '';	//111111111
            //this.sStatus = 'Clear';
        }
    }
    public class cls_oVehicle {
        public String sEngineNo;	//12345
        public String sChassisNo;	//67890
        public String sRegistrationNo;	//12345678
        public String sModel;	//TATA
        //public String sStatus;	//Clear
        public cls_oVehicle(){
            this.sEngineNo ='';	//12345
            this.sChassisNo ='';	//67890
            this.sRegistrationNo ='';	//12345678
            this.sModel ='';	//TATA
            //this.sStatus ='Clear';	//Clear
            
        }
    }
}