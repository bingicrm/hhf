global class cls_ProcessS3FolderCreationBatch implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {  
          return Database.getQueryLocator('Select Id, Name, Loan_Number__c, Days_Since_Loan_Disbursed__c, StageName__c, Sub_Stage__c, Disbursement_Date__c,  Folders_Created_in_S3__c, Sanctioned_Disbursement_Type__c, Date_of_Cancellation__c, Days_Since_Loan_Cancelled__c, Date_of_Rejection__c, Days_Since_Loan_Rejected__c From Loan_Application__c Where (Sub_Stage__c = \'CPC Scan Checker\' OR Sub_Stage__c = \'Loan Disbursed\' OR Sub_Stage__c = \'Loan Disbursed\' OR Sub_Stage__c = \'Loan Cancel\' OR Sub_Stage__c = \'Loan reject\') AND Folders_Created_in_S3__c = false');
    
    }
    global void execute(Database.BatchableContext BC, List<Loan_Application__c > lstLoanApplications)
        {    
            list<Loan_Application__c> lstloan = new list<Loan_Application__c>();
            for (Loan_Application__c objLA : lstLoanApplications){
                
                if(String.isNotBlank(Label.Delete_Documents_for_Previous_Records) && Label.Delete_Documents_for_Previous_Records == 'true') {
                    if(objLA.StageName__c == 'Loan Cancelled' && objLA.Sub_Stage__c == 'Loan Cancel' && objLA.Date_of_Cancellation__c != null && String.isNotBlank(Label.Document_Movement_Cancelled_Loan) && objLA.Days_Since_Loan_Cancelled__c >= Decimal.valueOf(Label.Document_Movement_Cancelled_Loan).setScale(0)) {
                        lstloan.add(objLA);
                    }
                    else if(objLA.StageName__c == 'Loan Rejected' && objLA.Sub_Stage__c == 'Loan reject' && objLA.Date_of_Rejection__c != null && String.isNotBlank(Label.Document_Movement_Rejected_Loan) && objLA.Days_Since_Loan_Rejected__c >= Decimal.valueOf(Label.Document_Movement_Rejected_Loan).setScale(0)) {
                        lstloan.add(objLA);
                    }
                    else if(objLA.Sanctioned_Disbursement_Type__c == 'Single Tranche' && objLA.Sub_Stage__c == 'Loan Disbursed' && Label.Document_Movement_Disbursed_Loan != null && objLA.Days_Since_Loan_Disbursed__c >= Decimal.valueOf(Label.Document_Movement_Disbursed_Loan).setScale(0)) {
                        lstloan.add(objLA);
                    }
                    else if(objLA.Sanctioned_Disbursement_Type__c == 'Multiple Tranche' && objLA.Sub_Stage__c == 'Loan Disbursed') {
                        lstloan.add(objLA);
                    }
                }
                else {
                    if(objLA.Sanctioned_Disbursement_Type__c == 'Single Tranche' && objLA.Sub_Stage__c == 'Loan Disbursed' && Label.Document_Movement_Disbursed_Loan != null && objLA.Days_Since_Loan_Disbursed__c == Decimal.valueOf(Label.Document_Movement_Disbursed_Loan).setScale(0)) {
                        lstloan.add(objLA);
                    }
                    else if(objLA.Sanctioned_Disbursement_Type__c == 'Multiple Tranche') {
                        lstloan.add(objLA);
                    }
                    else if(objLA.StageName__c == 'Loan Cancelled' && objLA.Sub_Stage__c == 'Loan Cancel' && objLA.Date_of_Cancellation__c != null && String.isNotBlank(Label.Document_Movement_Cancelled_Loan) && objLA.Days_Since_Loan_Cancelled__c == Decimal.valueOf(Label.Document_Movement_Cancelled_Loan).setScale(0)) {
                        lstloan.add(objLA);
                    }
                    else if(objLA.StageName__c == 'Loan Rejected' && objLA.Sub_Stage__c == 'Loan reject' && objLA.Date_of_Rejection__c != null && String.isNotBlank(Label.Document_Movement_Rejected_Loan) && objLA.Days_Since_Loan_Rejected__c == Decimal.valueOf(Label.Document_Movement_Rejected_Loan).setScale(0)) {
                        lstloan.add(objLA);
                    }
                }
            }
            System.debug('Debug Log fo lstLoan'+lstLoan.size());
            System.debug('Debug Log fo lstLoan'+lstLoan);
            System.debug('Debug Log for Label.Delete_Documents_for_Previous_Records'+Label.Delete_Documents_for_Previous_Records);
            System.debug('Debug Log for Label.Document_Movement_Disbursed_Loan'+Decimal.valueOf(Label.Document_Movement_Disbursed_Loan).setScale(0));
            System.debug('Debug Log for Label.Document_Movement_Cancelled_Loan'+Decimal.valueOf(Label.Document_Movement_Cancelled_Loan).setScale(0));
            System.debug('Debug Log for Label.Document_Movement_Rejected_Loan'+Decimal.valueOf(Label.Document_Movement_Rejected_Loan).setScale(0));
            
            if(!lstLoan.isEmpty()) {
                for(Loan_Application__c objLA : lstLoan) {
                    cls_MoveDocumentstoS3Initiation.moveDocumentstoS3(objLA);
                }
            }
      } 
        
    global void finish(Database.BatchableContext BC)
    {
		AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors,JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
          
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Folder Creation Batch ' + a.Status);
            mail.setPlainTextBody('During Folder Creation, records processed ' + a.TotalJobItems +
           'with '+ a.NumberOfErrors + ' failures.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        Database.executeBatch(new cls_ProcessApplicantDocumentsBatch(),1);
    }
}