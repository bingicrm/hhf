@isTest
public class TestLMSRepaymentSchedule {
    @TestSetup
    static void setup()
    {
        
        Account acc=new Account();
        acc.name='TestAccount1';
        acc.phone='9234567667';
        acc.PAN__c='fjauy0916u';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        System.debug(acc.id);
        
        Scheme__c sc=new Scheme__c();
        sc.name='TestScheme';
        sc.Scheme_Code__c='testcode';
        sc.Scheme_Group_ID__c='23131';
        sc.Product_Code__c='HL';
        insert sc;
        System.debug(sc.id);
        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Transaction_type__c='PB';
        lap.Scheme__c=sc.id;
        insert lap;
        System.debug(lap.id);
        
        City__c city=new city__c();
        city.City_ID__c='100000';
        city.Name='TEZPOOR';
        city.Country__c='1';
        city.State__c='5';
        insert city;
        Pincode__c pin=new Pincode__c();
        pin.City_LP__c=city.id;
        pin.ZIP_ID__c='46172';
        insert pin;
        list<Loan_Contact__c> lc=[select id, name from Loan_Contact__c where Loan_Applications__c=:lap.id limit 1];
        system.debug('loan contact'+lc[0]);
        Address__c address=new Address__c();
        address.Address_Line_1__c='abc Street';
        address.Address_Line_2__c='abc Area';
        address.Pincode_LP__c=pin.id;
        address.Type_of_address__c='PERMNENT';
        address.Loan_Contact__c=lc[0].id;
        //insert address;
        Repayment_Schedule__c rpsc=new Repayment_Schedule__c();
        rpsc.Loan_Application__c=lap.id;
        rpsc.Name='12312';
        
        insert rpsc; 
    }
    @istest
    public static void TestpullRepaymentSchedule(){
         Rest_Service__mdt restService = [ SELECT MasterLabel, QualifiedApiName , Service_EndPoint__c , 
                                                         Client_Password__c, Client_Username__c, Request_Method__c, Time_Out_Period__c  
                                                  FROM   Rest_Service__mdt
                                                  WHERE  MasterLabel = 'Repayment Schedule'
                                                ];
         list<Loan_Application__c> la = [select  Id, Name, StageName__c, Sub_Stage__c, Assigned_Sales_User__c, RecordTypeID from Loan_Application__c limit 1];
     list<Repayment_Schedule__c> lp = [select id,name  From Repayment_Schedule__c];
        
        
        LMSRepaymentSchedule.RepaymentResponse lmsRepaymentResponse= new  LMSRepaymentSchedule.RepaymentResponse();
        list<LMSRepaymentSchedule.RepaymentResponse> lst=new list<LMSRepaymentSchedule.RepaymentResponse>();
        lst.add(lmsRepaymentResponse);
 
                                               
                TestMockRequest req1=new TestMockRequest(200,
                                                 'Complete',
                                                 JSON.serialize(lst),
                                                 null);
                Test.setMock(HttpCalloutMock.class,req1);
                
            
                Test.startTest();
                LMSRepaymentSchedule.pullRepaymentSchedule(String.valueOf(lp[0].id), String.valueOf(la[0].id));
                LMSRepaymentSchedule.getUpdatedDate(System.today());
               
                Test.stopTest();
        
        
       
    }
    
}