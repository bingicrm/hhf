public class CreateDisbursementController {
    @AuraEnabled
    public static Id getLoanId(Id recordId){
        Id loanId;
        if(recordId != null){
            if(Schema.Tranche__c.SObjectType == recordId.getSobjectType()){
                List<Tranche__c> lstTranche = [SELECT Id, Loan_Application__c FROM Tranche__c WHERE Id =: recordId LIMIT 1];
                if(!lstTranche.isEmpty() && lstTranche.size() > 0 && lstTranche[0].Loan_Application__c != null){
                    loanId = lstTranche[0].Loan_Application__c;
                }
            }
        }
        return loanId;
    }

    @AuraEnabled
    public static String getRecTypeId(){
        //GN 26/12] Commenting it and referring to Schema.getDescribe for reducing SOQL queries.
        //String recTypeId = [SELECT Id FROM RecordType WHERE SobjectType='Disbursement__c' AND Name ='Others'].Id;
        Id recTypeId = Schema.SObjectType.Disbursement__c.getRecordTypeInfosByName().get('Others').getRecordTypeId();
        return recTypeId;
    }
}