public class CRM_CaseLoanListResponse {

	public class LoanSearchDetailBeans {
		public String aggrementId;
		public String aggrementNumber;
		public String productDescription;
		public String amountFinance;
		public String tenure;
		public String currencyId;
		public String effectiveRate;
		public String floatingFlag;
		public String disbursalDate;
		public String lesseeId;
		public String frequency;
		public String dueDay;
		public String agreementDate;
		public String title;
		public String mobileNumber;
		public String dateOfBirth;
		public String loanType;
		public String emailId;
		public String corporateDateOfInception;
		public String indvCorpFlag;
		public String isBirthday;
		public Object searchParameter;
		public String emiAmount;
		public Object panNumber;
		public Object vehicleRegistrationNumber;
		public Object sessionId;
		public String property;
		public String currentDate;
		public Object mobileParameterDetailBean;
		public String disbursalStatus;
		public String maturityFlag;
		public String productFlag;
		public String soaFlag;
		public String loanLegalFlag;
		public String soaChargeFlag;
		public String soaChargeMsg;
		public String soaChargeNote;
		public String customerId;
		public String applicantType;
		public String entityId;
		public String emiUnderConstruction;
		public Object maturityDate;
		public Object token;
		public String fcrCustomerId;
		public String emiOverdue;
		public String otherOverdues;
		public String overdueCharges;
		public String bounceCharges;
		public String netReceivable;
		public String foreclosureAmount;
		public String dpd;
		public String installmentMode;
		public String installmentDueDate;
		public String lastPaymentDate;
		public String lastPaymentAmount;
		public String agreementLoanStatus;
		public String disbursedAmountAsOfToday;
		public String principalOutstandingAsOfToday;
		public String emiStatus;
		public Object locationName;
		public Object cancellationDate;
		public Object closureDate;
	}
    public class DateDetailBeans {
		public String systemDate;
		public String currentDate;
	}
    
	public List<LoanSearchDetailBeans> loanSearchDetailBeans;
	public Object customerId;
	public Object ucic;
	public DateDetailBeans dateDetailBeans;
	public String pm;
	public String ba;
	public Object sessionId;
	public Object statusEnquiryBeans;
	public Object registeredloans;
	public Object selfSourcedLoans;
	public Object customerDetails;
	public Object errorMsg;

	public static CRM_CaseLoanListResponse parse(String json) {
		return (CRM_CaseLoanListResponse) System.JSON.deserialize(json, CRM_CaseLoanListResponse.class);
	}
}