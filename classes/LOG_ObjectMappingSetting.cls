/**
 *    @author          : Deloitte Digital - SFDC
 *    @date            : 04/04/2016
 *    @description     : Class for custom setting 'DLOG Object Mapping' records.
 *    Modification Log : 
 *    ------------------------------------------------------------------------------------     
 *    Developer                       Date                Description      
 *    ------------------------------------------------------------------------------------      
 *    Shilpa Menghani                04/04/2016            Original Version
 */
public without sharing class LOG_ObjectMappingSetting {
   
    /**
    * Current Mapping information 
    */
    public  Object_Mapping__c objectMapping {get;set;}
    
    /**
    * Existing assignment mapping information 
    */
    public  List<Object_Mapping__c> ObjectMappingList  {get;set;}
    
    
    /**
     * Constructor 
     */
    public LOG_ObjectMappingSetting(){
        objectMapping = new Object_Mapping__c();
        ObjectMappingList = new List<Object_Mapping__c>();
           
        List<Object_Mapping__c> ExistList = getObjectMappingDetails();
        for(Object_Mapping__c existObj: ExistList ){
             if(existObj.Name == 'Object Name'){
                objectMapping.Name = existObj.Name;
                objectMapping.API_Name__c = existObj.API_Name__c;
             }
             ObjectMappingList.add(existObj);
           } 
           ObjectMappingList.sort();
        
     }
   /**
   * Get object select list options 
   */
    public List<SelectOption> getObjectList(){
        List<SelectOption> objectOption = new List<SelectOption>();
        objectOption.add(new SelectOption('','--NONE--'));
        objectOption.add(new SelectOption('Task','TASK'));
        objectOption.add(new SelectOption('DLOG_Object__c','DLOG Object')); 
        
        return objectOption;
    }
     /**
     * This method is called on onchange of object select options
     */
     public void onChangeObjectOption(){
        ObjectMappingList = new List<Object_Mapping__c>();
        
        if(objectMapping != null && String.isNotBlank(objectMapping.API_Name__c)){
            
             objectMapping.Name = 'Object Name';
             getFieldSelectOptions('DLOG_Object__c');
             ObjectMappingList.add(objectMapping);
             ObjectMappingList.sort();
        }
        
     }
    /** This method is used to populate user lookup fields of passed api name object
    *  @objectApiName: API name of object whose field select list options needed
    */
    public void getFieldSelectOptions(string objectApiName){
        
        List<SelectOption> selectOptionList = new List<SelectOption>();
        Sobject obj = getObjectInstance(objectApiName);
        
        if(obj!= null){
             /**
             * Describe Object 
             */
            Schema.DescribeSObjectResult describeResult = obj.getSobjectType().getDescribe();
            
            if(describeResult!= null){
                /**
                * Get all fields map from describe result
                */
                Map<String, Schema.SObjectField> fieldMap = describeResult.fields.getMap();
            
                 for(String apiName : fieldMap.keySet()){
                       /**
                        * Describe field
                        */
                        Schema.DescribeFieldResult fieldDescribeResult = fieldMap.get(apiName).getDescribe();
                        boolean isCustom = fieldDescribeResult.isCustom();
                        /**
                        * Get only those custom Fields that are editable
                        */
                        boolean isUpdateable = fieldDescribeResult.isUpdateable();
                        
                            /**
                            * Available User lookup Fields
                            */
                            if(isUpdateable && isCustom){
                                Object_Mapping__c tempObjectMapping = new Object_Mapping__c();
                                tempObjectMapping.Name = fieldDescribeResult.getLabel();
                                tempObjectMapping.API_Name__c= fieldDescribeResult.getName();
                                ObjectMappingList.add(tempObjectMapping);
                            }
                            
                 } 
            }
        }
        
    } 
    
    /* Create instance from object api name
     * @name: Object API nmae
     */
    public Sobject getObjectInstance(string name){
        if(name != null){ 
            if(name.equalsIgnoreCase('DLOG_Object__c')){
                return new DLOG_Object__c();
            }
        }
        return null;
    }
    /**
    * This method is used to get the existing mapping data 
    */
    public List<Object_Mapping__c> getObjectMappingDetails(){
          List<Object_Mapping__c> ExistingMappingList = [Select Id, 
                                                        Name,API_Name__c
                                                 From Object_Mapping__c
                                                 where API_Name__c!=null 
                                                ];
                                             
           return ExistingMappingList;                                 
    }
     
    /** This method is called on Save button click
     * Add mapping to the database, if their datatype match
     * And also get the latest data for page 
     */
    public Pagereference Save(){
        try{
            if(objectMapping != null){
                if(objectMapping.API_Name__c!= null){ 
                   //Delete existing records
                   List<Object_Mapping__c> ExistMappingList = getObjectMappingDetails();
                   if(ExistMappingList != null && !ExistMappingList.isEmpty())
                   delete ExistMappingList;                              
                
                   //insert new mapping records
                  insert ObjectMappingList;
                }
                                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,'Record Saved')); 
                objectMapping = new Object_Mapping__c();
            }
            
        }
       catch(Exception ex){
            apexpages.addMessages(ex);          
            return null;
        }
        return null;
     }
   /** This method is called on delete button click
    * Removes mapping from database and also get the latest data for page 
    */
    public pagereference deleteMapping(){
        try{
            //Delete existing records
            /**
           * Get already added field mapping
           */
           
           String assignmentQuery = 'Select Id,Name,API_Name__c'+ 
                                    ' From Object_Mapping__c'+
                                    ' where API_Name__c!= null';
                                    
                                    
           if(objectMapping != null && objectMapping.API_Name__c!= null ){
               string objectApiName = objectMapping.API_Name__c;
                assignmentQuery += ' and API_Name__c=\''+objectApiName+'\'';
           }                        
          
            List<Object_Mapping__c> ObjectList = DataBase.query(assignmentQuery);   
            if(ObjectList != null && !ObjectList.isEmpty()){       
                   List<Object_Mapping__c> DeleteMappingList = getObjectMappingDetails();
                   if(DeleteMappingList != null && !DeleteMappingList.isEmpty())
                   delete DeleteMappingList; 
                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,'All Records are deleted'));
                ObjectMappingList.addAll(getObjectMappingDetails());
             }
             else
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,'No Record For Selected Object'));
            
        }   
        catch(Exception ex){
            ApexPages.addMessages(ex);
        }
        return null;
    }
    
}