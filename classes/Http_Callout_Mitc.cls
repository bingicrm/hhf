public class Http_Callout_Mitc {
    
    @AuraEnabled
    public static String mitccallout(Id recordId){
               
        id loanApp = RecordID;
         String strStatus;
          List<Loan_Application__c> lstLoan = [Select id,Repayment_Generated_for_Documents__c,Approved_cross_sell_amount__c,Approved_Date__c ,If_Other_please_specify__c ,Processing_Fee_Percentage__c,Approved_Processing_Fees1__c ,Installment_Type__c, Frequency__c,Branch_Lookup__c, Branch_Lookup__r.Name, Transaction_type__c,Product_Name__c , Requested_Amount__c,Spread__c,Business_Date_Created__c, Name,Approved_Loan_Amount__c,Approved_ROI__c,Approved_Loan_Tenure__c,Loan_Purpose__c,GST_on_PF_amount__c,Funded_By_Customer__c,
                                  Mode_of_Repayment__c, Customer__r.Recordtype.Name, Approved_EMI__c,Interest_Type__c,Approved_Processing_Fee__c,Loan_Number__c,
                                  Applicable_IMD__c,Approved_Foreclosure_Charges__c,Reference_Rate__c,Other_Security_Details__c,Insurance_Loan_Application__c, Scheme__r.Reference_Rate__c, Fixed_For_Months__c, Insurance_Loan_Created__c,Physical_Cheque_Received__c,Source_Code__c, /** Added by saumya for Physical Documents TIL 1150**/
								  IMGC_Premium_fee__c,/** Added by Saumya For IMGC BRD **/
                                  (Select id, Name, Phone_Number__c,Email__c, Customer__r.name,Applicant_Type__c from Loan_Contacts__r where Applicant_Type__c = 'Guarantor') 
                                  from Loan_Application__c where id =:loanApp]; /** Added by saumya Insurance_Loan_Application__c, Insurance_Loan_Created__c for Insurance Loan **/
									//Scheme__r.Reference_Rate__c, Fixed_For_Months__c added by Saumya for TIL-00001688
        /** Added by saumya for Insurance Loan **/
        if(lstLoan.size() > 0 && lstLoan[0].Insurance_Loan_Application__c == True){
            strStatus ='Insurance Loan Application';
        }
        /** Added by saumya for Insurance Loan **/
		 /** Added by saumya for Physical Documents TIL 1150**/
        else if(lstLoan.size() > 0 && (lstLoan[0].Physical_Cheque_Received__c != 'Yes' && lstLoan[0].Source_Code__c == 'DSA')){
            strStatus ='Physical Documents not received';
        }
        /** Added by saumya for Physical Documents TIL 1150**/
        else{
		strStatus = 'false';	
        List<Loan_Repayment__c> lstLoanRepay = [Select id,Disbursal_Date__c,Frequency__c,Moratorium_period__c from Loan_Repayment__c where Loan_Application__c =:loanApp];
        
        List<LMS_Date_Sync__c> lstLMS = [SELECT Id, LastModifiedDate, Current_Date__c FROM LMS_Date_Sync__c order by LastModifiedDate desc LIMIT 1];
        
        /*if (!lstLoanRepay.isEmpty() && lstLoanRepay[0].Disbursal_Date__c != lstLMS[0].Current_Date__c ) {
           return 'repayment';
        }*/
        
        List_of_Charges__c ObjlstOfCharges = List_of_Charges__c.getInstance();
        Grievance_Contact__c objGriev = Grievance_Contact__c.getInstance();
        string formattedDate = system.now().format('dd-MM-yyyy');
        system.debug('@@@@@ RecordID in Http_Callout_Mitc' + loanApp);
        
        List<Repayment_Schedule__c> lstRepay = [Select Id, Installment_Amount__c from Repayment_Schedule__c where Loan_Application__c =:loanApp AND Installment_Amount__c != Null LIMIT 1];
            
        List<Loan_Sanction_Condition__c> lstLoanSac = [Select id, Status__c ,Remarks__c,Sanction_Condition__c, Sanction_Condition_Master__r.Sanction_Condition__c from Loan_Sanction_Condition__c where Loan_Application__c =: loanApp ];
       
        List<Property__c> lstProp = [Select Id, Flat_No_House_No__c ,Floor__c , Builder_Name_Society_Name__c , Address_Line_1__c , Address_Line_2__c , Address_Line_3__c , City_LP__c,State_LP__c, Pincode_LP__r.Name from Property__C where Loan_Application__c=:loanApp Limit 1];        
            system.debug('lstProp@@' + lstProp);List<Charge_Code__c> lstCharge = [Select Name, Net_Amount__c, Tax__c, Charge_Rate__c, ChargeCodeID__c, Total__c From Charge_Code__c ];
        Map<String, Charge_Code__c> mapNameToCharge = new Map<String, Charge_Code__c>();
        for ( Charge_Code__c objCharge : lstCharge) {
            mapNameToCharge.put(objCharge.Name, objCharge);
        }
        
        Loan_Application__c la;
        if (!lstLoan.isEmpty()) {
            la  = lstLoan[0];
        }
        
        //Mandatory generation of repayment
        if ( !la.Repayment_Generated_for_Documents__c ) {
            la.Repayment_Generated_for_Documents__c = true;
            update la;
            return 'repayment';
        }
        
        List<Document_Master__c> lstdocMaster = [Select Doc_Id__c from Document_Master__c where Name = 'MITC' LIMIT 1];
        // Updated approved amount as per TIL-1352
        //Decimal updatedApprovedAmt = la.Approved_Loan_Amount__c != null && la.Approved_cross_sell_amount__c != null ? la.Approved_Loan_Amount__c + la.Approved_cross_sell_amount__c : la.Approved_Loan_Amount__c;
        /*Added by Saumya For insurance Loan*/
            Decimal updatedApprovedAmt;
            if(la.Insurance_Loan_Created__c){
                updatedApprovedAmt= la.Approved_Loan_Amount__c;
            }   
            else{
                updatedApprovedAmt= la.Approved_Loan_Amount__c != null && la.Approved_cross_sell_amount__c != null ? la.Approved_Loan_Amount__c + la.Approved_cross_sell_amount__c : la.Approved_Loan_Amount__c;
            }
            /*Added by Saumya For insurance Loan*/
			/*** Addeed by Saumya For IMGC BRD ***/
			Decimal TotalPF = null;/*** Added by Saumya For IMGC BRD ***/
			Decimal TotalPFCharges = null;/*** Added by Saumya For IMGC BRD ***/
            if(la.Processing_Fee_Percentage__c != null && updatedApprovedAmt != null) {/** Replaced by updatedApprovedAmt Added by Saumya for IMGC BRD **/
                Decimal AppProcessingfees = ((updatedApprovedAmt * la.Processing_Fee_Percentage__c)/100);//.round(System.RoundingMode.HALF_UP);
				TotalPF = AppProcessingfees;/** Addeed by Saumya For IMGC BRD ***/
                
                //Added by Saumya on 8th September 2020 FOR IMGC BRD
                if(la.Insurance_Loan_Created__c || la.Funded_By_Customer__c){
                    TotalPFCharges = (TotalPF /updatedApprovedAmt )*100; 
                }
                else{
                    TotalPFCharges = (TotalPF + (la.IMGC_Premium_fee__c != null ? la.IMGC_Premium_fee__c: 0))/(updatedApprovedAmt+ (la.IMGC_Premium_fee__c != null ? la.IMGC_Premium_fee__c: 0))*100;
                }
            }
        /*** Addeed by Saumya For IMGC BRD ***/   
        //Updated as per PROD defect 
        List<IMD__c> lstIMD = [Select Id,Amount__c,Cheque_Status__c,Cheque_ID__c from IMD__c where Loan_Applications__c = :loanApp];
        Decimal decIMD = 0;
        for (IMD__C objIMD : lstIMD) {
            if (objIMD.Cheque_Status__c != 'X' && objIMD.Cheque_Status__c != 'B' && objIMD.Cheque_ID__c != null && !objIMD.Cheque_ID__c.containsIgnoreCase('blank') ) {
                decIMD += objIMD.Amount__c;
                system.debug('decimd in for @@@' + decimd);
            }
        }
        
        system.debug('decimd@@@' + decimd);
                            
        mitcWrapper Wrapper = new mitcWrapper();
        if(updatedApprovedAmt != null)
        {
        Wrapper.loan_Amt = string.valueOf(updatedApprovedAmt);
        }
        else
        {
            Wrapper.loan_Amt = '';
        }
        
        NumberToWord objN = new NumberToWord ();    
        Wrapper.loan_Amt_Words = objN.convert(Integer.valueOf(updatedApprovedAmt)).replaceAll('  ',' ');
        Wrapper.Doc_ID = !lstdocMaster.isEmpty() && lstdocMaster[0].Doc_Id__c!= null ? lstdocMaster[0].Doc_Id__c : '';
        Wrapper.App_ID = la.Loan_Number__c != null ? la.Loan_Number__c : '';
		/**** Added by Saumya For TIL-00001688 ****/
            String interesttype = '';
            if(la.Interest_Type__c != null){
                if(la.Interest_Type__c == 'Fixed'){
                    interesttype= la.Interest_Type__c;
                }
                else if(la.Interest_Type__c == 'Floating'){
                    if(la.Fixed_For_Months__c != null && la.Fixed_For_Months__c != 0){
                      interesttype= System.label.Floating + ' '+ la.Fixed_For_Months__c + ' '+ System.label.Floating_Interest_Rate_Type_2;  
                    }
                    else{
                      interesttype=  la.Interest_Type__c;
                    }
                }
            }
            //Wrapper.Interest_Type = la.Interest_Type__c != null ? la.Interest_Type__c != '' : '';
            Wrapper.Interest_Type = interesttype;
            /**** Added by Saumya For TIL-00001688 ****/ 				
        Wrapper.Applicable_ROI_Floating = la.Approved_ROI__c != null ? String.valueOf(la.Approved_ROI__c) : ''; 
        Wrapper.Applicable_ROI_Fixed = la.Approved_ROI__c != null ? String.valueOf(la.Approved_ROI__c) : ''; 
        Wrapper.Imd_Amount = decIMd != null ? String.valueOf(decIMD) : '';
        Wrapper.Stmt_accnt_soft = mapNameToCharge.containsKey('SOFT COPY OF STATEMENT OF ACCOUNT CHARGES') && mapNameToCharge.get('SOFT COPY OF STATEMENT OF ACCOUNT CHARGES').Net_Amount__c!= null ? String.valueOf(mapNameToCharge.get('SOFT COPY OF STATEMENT OF ACCOUNT CHARGES').Net_Amount__c): ''; 
        Wrapper.Stmt_accnt_hard = mapNameToCharge.containsKey('HARD COPY OF STATEMENT OF ACCOUNT CHARGES') && mapNameToCharge.get('HARD COPY OF STATEMENT OF ACCOUNT CHARGES').Net_Amount__c!= null ? String.valueOf(mapNameToCharge.get('HARD COPY OF STATEMENT OF ACCOUNT CHARGES').Net_Amount__c): ''; 
         //Wrapper.Approved_Foreclosure_Charges = la.Approved_Foreclosure_Charges__c != null ? String.valueOf(la.Approved_Foreclosure_Charges__c) : '';// Commented By Saumya For Foreclosure BRD
        Wrapper.Approved_Foreclosure_Charges = System.Label.Approved_Foreclousure_MITC;// Added By Saumya For Foreclosure BRD
        //Wrapper.Hero_Reference_Rate = la.Reference_Rate__c != null ? String.valueOf(la.Reference_Rate__c) : '';
		/**** Added by Saumya For TIL-00001688 ****/
        //wrapper.Hero_Reference_Rate = '8.3';
        wrapper.Hero_Reference_Rate = la.Scheme__r.Reference_Rate__c != null ? string.valueOf(la.Scheme__r.Reference_Rate__c) : '';      
 		/**** Added by Saumya For TIL-00001688 ****/				   
        if(la.Approved_ROI__c != null)
        {
        Wrapper.Applicable_ROI = string.valueOf(la.Approved_ROI__c);  
        }
        else
        {
        Wrapper.Applicable_ROI = '';
        }
        
        Wrapper.Moratorium_Subsidy = !lstLoanRepay.isEmpty() && lstLoanRepay[0].Moratorium_period__c != null ? String.valueOf(lstLoanRepay[0].Moratorium_period__c) : '';   //loan repay.Moratorium_period__c
        
        Wrapper.Loan_Tenure = la.Approved_Loan_Tenure__c != null ?string.valueOf(la.Approved_Loan_Tenure__c):'';  
        Schema.DescribeFieldResult fieldResult = Loan_Application__c.Loan_Purpose__c.getDescribe();
        List<Schema.PicklistEntry> lstPicklist = fieldResult.getPicklistValues();
        String strPLValue;     
        for( Schema.PicklistEntry f : lstPicklist) {
            if (f.getValue() == la.Loan_Purpose__c) {
                strPLValue = f.getLabel();
            }
        }
        
        Wrapper.Purpose_Loan = strPLValue != null ? strPLValue : '';
            
        
        
        //Wrapper.Processing_Fee_Home = la.Product_Name__c == 'Home Loan' ? '1' : 'N/A';
        
        //Wrapper.PF_NonHousing =  la.Product_Name__c != 'Home Loan' ? '1' : 'N/A'; 
		/*** Added by Saumya For IMGC BRD ***/
        
        //Wrapper.Processing_Fee_Home = la.Product_Name__c == 'Home Loan' && la.Processing_Fee_Percentage__c!= null ? string.valueOf(la.Processing_Fee_Percentage__c) : 'N/A';/*** Commented by Saumya For IMGC BRD ***/
        
        //Wrapper.PF_NonHousing =  la.Product_Name__c != 'Home Loan' && la.Processing_Fee_Percentage__c!= null? string.valueOf(la.Processing_Fee_Percentage__c) : 'N/A'; /*** Commented by Saumya For IMGC BRD ***/
        
        Wrapper.Processing_Fee_Home = la.Product_Name__c == 'Home Loan' && TotalPFCharges!= null ? string.valueOf(TotalPFCharges) : 'N/A';
        
        Wrapper.PF_NonHousing =  la.Product_Name__c != 'Home Loan' && TotalPFCharges!= null? string.valueOf(TotalPFCharges) : 'N/A'; 
    	/*** Added by Saumya For IMGC BRD ***/
        Wrapper.Chrgs_paid_CERSAI = mapNameToCharge.containsKey('CERSAI Charges DOC') && mapNameToCharge.get('CERSAI Charges DOC').Net_Amount__c != null ? String.valueOf(mapNameToCharge.get('CERSAI Charges DOC').Net_Amount__c): '';  
        Wrapper.NonRefundable_amt = la.Requested_Amount__c != null && la.Requested_Amount__c > 3000000 ? '5000' : '2500';
        if(la.GST_on_PF_amount__c != null)
        {
        Wrapper.GST = string.valueOf(la.GST_on_PF_amount__c);  
        }
        else
        {
            Wrapper.GST = '';
        }
        Wrapper.Duplicate_ITCert = ObjlstOfCharges.Duplicate_Income_Tax_Certificate__c != null ? ObjlstOfCharges.Duplicate_Income_Tax_Certificate__c : ''; 
        Wrapper.Stmt_accnt = mapNameToCharge.containsKey('HARD COPY OF STATEMENT OF ACCOUNT CHARGES') && mapNameToCharge.get('HARD COPY OF STATEMENT OF ACCOUNT CHARGES').Total__c != null ? String.valueOf(mapNameToCharge.get('HARD COPY OF STATEMENT OF ACCOUNT CHARGES').Total__c): ''; 
        //Wrapper.Cheque_dishonour_chrgs = mapNameToCharge.containsKey('Rejection of ECS (Incl. Tax)') && mapNameToCharge.get('Rejection of ECS (Incl. Tax)').Net_Amount__c!= null ? String.valueOf(mapNameToCharge.get('Rejection of ECS (Incl. Tax)').Net_Amount__c): '';  
        Wrapper.Cheque_dishonour_chrgs = '500';
        Wrapper.Change_Repayment_Mode = mapNameToCharge.containsKey('Change in Repayment Mode') && mapNameToCharge.get('Change in Repayment Mode').Net_Amount__c != null ? String.valueOf(mapNameToCharge.get('Change in Repayment Mode').Net_Amount__c): '';
        Wrapper.Change_loan_tenor = mapNameToCharge.containsKey('Change in Loan Tenor or EMI') && mapNameToCharge.get('Change in Loan Tenor or EMI').Charge_Rate__c!= null ? String.valueOf(mapNameToCharge.get('Change in Loan Tenor or EMI').Charge_Rate__c): '';
        Wrapper.Change_Property =  mapNameToCharge.containsKey('PROPERTY SWAP CHARGES/Change in property') && mapNameToCharge.get('PROPERTY SWAP CHARGES/Change in property').Charge_Rate__c!= null ? String.valueOf(mapNameToCharge.get('PROPERTY SWAP CHARGES/Change in property').Charge_Rate__c): '';
        Wrapper.Loan_cancl_chrgs = mapNameToCharge.containsKey('CANCELLATION CHARGES') && mapNameToCharge.get('CANCELLATION CHARGES').Net_Amount__c != null ? String.valueOf(mapNameToCharge.get('CANCELLATION CHARGES').Net_Amount__c): '';
        //Wrapper.Doc_Retrieval = mapNameToCharge.containsKey('DOCUMENT RETRIEVAL CHARGE') && mapNameToCharge.get('DOCUMENT RETRIEVAL CHARGE').Net_Amount__c!= null ? String.valueOf(mapNameToCharge.get('DOCUMENT RETRIEVAL CHARGE').Net_Amount__c): ''; 
        Wrapper.Loan_cancl_chrgs = '5000';
        Wrapper.Doc_Retrieval = '3000';
        Wrapper.Docs_handling_chrgs  = mapNameToCharge.containsKey('DOCUMENT RETRIEVAL CHARGE') && mapNameToCharge.get('DOCUMENT RETRIEVAL CHARGE').Net_Amount__c!= null ? String.valueOf(mapNameToCharge.get('DOCUMENT RETRIEVAL CHARGE').Net_Amount__c): ''; 

        Wrapper.Collection_chrgs = mapNameToCharge.containsKey('COLLECTION CHARGES') && mapNameToCharge.get('COLLECTION CHARGES').Net_Amount__c!= null ? String.valueOf(mapNameToCharge.get('COLLECTION CHARGES').Net_Amount__c): '';
        Wrapper.Recovery_Fee = mapNameToCharge.containsKey('Recovery Fees') && mapNameToCharge.get('Recovery Fees').Net_Amount__c!= null ? String.valueOf(mapNameToCharge.get('Recovery Fees').Net_Amount__c): ''; 
        Wrapper.Title_Searchfee = mapNameToCharge.containsKey('Title Search Fee') && mapNameToCharge.get('Title Search Fee').Net_Amount__c != null ? String.valueOf(mapNameToCharge.get('Title Search Fee').Net_Amount__c): '';
        Wrapper.Legal_Fee = mapNameToCharge.containsKey('LEGAL CHARGES') && mapNameToCharge.get('LEGAL CHARGES').Net_Amount__c != null ? String.valueOf(mapNameToCharge.get('LEGAL CHARGES').Net_Amount__c ): ''; 
        // Commented By Saumya For Foreclosure BRD   
        /*Wrapper.Approved_Loan_Float = la.Approved_Foreclosure_Charges__c != null ? String.valueOf(la.Approved_Foreclosure_Charges__c) : ''; 
        Wrapper.Approved_Loan_FixedOwn = la.Approved_Foreclosure_Charges__c != null ? String.valueOf(la.Approved_Foreclosure_Charges__c) : ''; 
        Wrapper.Approved_Loan_Fixed = la.Approved_Foreclosure_Charges__c != null ? String.valueOf(la.Approved_Foreclosure_Charges__c) : ''; 
        Wrapper.Approved_Loan_Float_NonInd = la.Approved_Foreclosure_Charges__c != null ? String.valueOf(la.Approved_Foreclosure_Charges__c) : ''; 
         
        Wrapper.Recover_Preclosure_Loan = la.Approved_Foreclosure_Charges__c != null ? String.valueOf(la.Approved_Foreclosure_Charges__c) : ''; */
         //Wrapper.Approved_Loan_Float = la.Approved_Foreclosure_Charges__c != null ? String.valueOf(la.Approved_Foreclosure_Charges__c) : ''; 
		Wrapper.Approved_Loan_Float ='Nil';// Added By Saumya For Foreclosure BRD
        //Wrapper.Approved_Loan_Float = System.Label.Approved_Foreclousure_MITC;// Added By Saumya For Foreclosure BRD
        Wrapper.Approved_Loan_FixedOwn = System.Label.Approved_Foreclousure_MITC; // Added By Saumya For Foreclosure BRD
        //Wrapper.Approved_Loan_Fixed = la.Approved_Foreclosure_Charges__c != null ? String.valueOf(la.Approved_Foreclosure_Charges__c) : ''; // Added By Saumya For Foreclosure BRD
        Wrapper.Approved_Loan_Fixed = System.Label.Approved_Foreclousure_MITC; // Added By Saumya For Foreclosure BRD
        Wrapper.Approved_Loan_Float_NonInd = System.Label.Approved_Foreclousure_MITC; // Added By Saumya For Foreclosure BRD
         
        Wrapper.Recover_Preclosure_Loan = la.Approved_Foreclosure_Charges__c != null ? String.valueOf(la.Approved_Foreclosure_Charges__c) : '';								  
        //Wrapper.Docs_handling_chrgs = ObjlstOfCharges.Documents_handling_charges_post_closure__c != null ? ObjlstOfCharges.Documents_handling_charges_post_closure__c : ''; 
        //Wrapper.Refund_Processing_Fee = mapNameToCharge.containsKey('PF REFUND') && mapNameToCharge.get('PF REFUND').Net_Amount__c!= null ? String.valueOf(mapNameToCharge.get('PF REFUND').Net_Amount__c): ''; 
        Wrapper.Refund_Processing_Fee = 'Non refundable' ;
        Wrapper.Conversion_Chrgs = mapNameToCharge.containsKey('Conversion Charges (On outstanding principle)') && mapNameToCharge.get('Conversion Charges (On outstanding principle)').Charge_Rate__c != null ? String.valueOf(mapNameToCharge.get('Conversion Charges (On outstanding principle)').Charge_Rate__c): ''; 
        //Wrapper.Penalty_Delayed_Payments = mapNameToCharge.containsKey('Panelty of Delayed Payment (2 per month)') && mapNameToCharge.get('Panelty of Delayed Payment (2 per month)').Charge_Rate__c!= null ? String.valueOf(mapNameToCharge.get('Panelty of Delayed Payment (2 per month)').Charge_Rate__c): ''; 
        Wrapper.Penalty_Delayed_Payments = '2';
        
        system.debug('@lstProp' + lstProp);
            if(!lstProp.isEmpty() && lstProp[0].Address_Line_1__c != null) {
                //Below if and else blocks added by Abhilekh on 16th May 2019 for TIL-781
                if(lstProp[0].Address_Line_2__c != null){
                    Wrapper.Mortgage_Address = lstProp[0].Flat_No_House_No__c +  ','  + lstProp[0].Floor__c + ','+ lstProp[0].Builder_Name_Society_Name__c + ',' + lstProp[0].Address_Line_1__c + ',' + lstProp[0].Address_Line_2__c + ',' + lstProp[0].City_LP__c+ ',' +lstProp[0].State_LP__c+ ',' +  lstProp[0].Pincode_LP__r.Name;
                }
                else{
                	Wrapper.Mortgage_Address = lstProp[0].Flat_No_House_No__c +  ','  + lstProp[0].Floor__c + ','+ lstProp[0].Builder_Name_Society_Name__c + ',' + lstProp[0].Address_Line_1__c + ',' + lstProp[0].City_LP__c+ ',' +lstProp[0].State_LP__c+ ',' +  lstProp[0].Pincode_LP__r.Name;    
                }
            } else {
                Wrapper.Mortgage_Address = '';
            }
        //wrapper.CoBorrower_Name = new List<CoBorrower_Name>();
        list<String> lstCoAppl = new List<String>();
        if(la.Loan_Contacts__r.size()>0){
        
            for (Loan_Contact__c objlc : la.Loan_Contacts__r){
                if ( objlc.Applicant_Type__c == 'Co- Applicant')
                lstCoAppl.add(objlc.Customer__r.name);
            }
        
        }
        List<CoBorrower_Name> lstCob = new List<CoBorrower_Name>();
        CoBorrower_Name objCob;
        for (String strCoAp : lstCoAppl) {
            objCob = new CoBorrower_Name();
            objCob.CBN = strCoAp;
            lstCob.add(objCob);
        }
        wrapper.CoBorrower_Name = lstCob;
        List<Sanc_Condts> lstSanc = new List<Sanc_Condts>();
        Sanc_Condts objSc;
        for (Loan_Sanction_Condition__c objLc : lstLoanSac) {
                if( objLc.Sanction_Condition_Master__c != null && objLc.Sanction_Condition_Master__r.Sanction_Condition__c != null) {
                 if ( objLc.Status__c == 'Requested' || objLc.Status__c == 'Completed' || objLc.Status__c == 'Verified' || objLc.Status__c == 'Waiver Requested'){//Edited by Chitransh for TIL-1157 on [01-01-2019]
                        objSc = new Sanc_Condts();
                        if (objLc.Sanction_Condition_Master__r.Sanction_Condition__c != 'Others') {
                            objSc.Cond = objLc.Sanction_Condition_Master__r.Sanction_Condition__c;
                        } else {
                            objSc.Cond = objLc.Sanction_Condition__c;
                        }
                        if ( objSc.Cond == null)
                            objSc.Cond = '';     
                        lstSanc.add(objSc);
                    }
                }
        }
        wrapper.Sanc_Condts = lstSanc;
        
        if(la.Loan_Contacts__r.size()>0)
        { 
          Wrapper.Guarantor_Name = la.Loan_Contacts__r[0].Customer__r.name;
        }
        else
        {
          Wrapper.Guarantor_Name ='';    
        }
        Wrapper.other_security_dtls =  la.Other_Security_Details__c != null ? la.Other_Security_Details__c +' ' + la.If_Other_please_specify__c : ''; 
        
        Wrapper.Total_EMI_Amt = la.approved_emi__c != null ? String.valueOf(la.approved_emi__c) : '';
        
        
        if(la.Approved_Loan_Tenure__c != null)
        {
        Wrapper.No_Months = string.valueOf(la.Approved_Loan_Tenure__c);  
        }
        else
        {
            Wrapper.No_Months = '';
        }
        Wrapper.Date_annual_bal = ObjlstOfCharges.Date_on_which_annual_outstanding_balance__c != null ? ObjlstOfCharges.Date_on_which_annual_outstanding_balance__c : ''; 
        Wrapper.CustomerCare_Email = objGriev.CustomerCare_Email__c != null ? objGriev.CustomerCare_Email__c : ''; 
        Wrapper.CustomerCare_PhNo = objGriev.CustomerCare_PhNo__c != null ? objGriev.CustomerCare_PhNo__c : ''; 
        Wrapper.Level1_BO = objGriev.Level1_BO__c != null ? objGriev.Level1_BO__c : ''; 
        Wrapper.Level2_Grievance_RO = objGriev.Level2_Grievance_RO__c != null ? objGriev.Level2_Grievance_RO__c : ''; 
        Wrapper.Level3_CEO = objGriev.Level3_CEO__c != null ? objGriev.Level3_CEO__c : ''; 
         Wrapper.Place = '';//la.Branch_Lookup__c != null && la.Branch_Lookup__r.Name != null ? la.Branch_Lookup__r.Name : '';  /**Edited by Chitransh for TIL-1475**/
        wrapper.website = objGriev.website__c != null ? objGriev.website__c : ''; 
        wrapper.Level2_Address_Grievance_RO1 = objGriev.Level2_Address_Grievance_RO1__c != null ? objGriev.Level2_Address_Grievance_RO1__c : ''; 
        wrapper.Level2_Address_Grievance_RO2 = objGriev.Level2_Address_Grievance_RO2__c != null ? objGriev.Level2_Address_Grievance_RO2__c : ''; 
        wrapper.Level2_Address_Grievance_RO3 = objGriev.Level2_Address_Grievance_RO3__c != null ? objGriev.Level2_Address_Grievance_RO3__c : ''; 
        wrapper.Level2_Grievance_officer_Email_Id = objGriev.Level2_Grievance_officer_Email_Id__c != null ? objGriev.Level2_Grievance_officer_Email_Id__c : ''; 
        wrapper.Level2_Phone_No = objGriev.Level2_Phone_No__c != null ? objGriev.Level2_Phone_No__c : ''; 
        wrapper.Authority_Designation = objGriev.Authority_Designation__c != null ? objGriev.Authority_Designation__c : ''; 
        
        Datetime dateApproved ;
        List<loan_application__history> lstHistory = [select field, newvalue, oldvalue, createddate from loan_application__history where parentid=:loanApp and field='sub_stage__c' order by createddate desc];
        for ( loan_application__history obj : lstHistory) {
            system.debug('@@obj');
            if (obj.newvalue=='Customer Negotiation' && string.valueof(obj.oldvalue).containsignorecase('credit')) {
                system.debug('in if date approved@@');
                dateApproved = obj.createddate; 
                break;   
            }
        }
        system.debug('@@dateApproved' + dateApproved);
        //Updating dateApproved as per updated logic
            
        List<LMS_Date_Sync__c> lstApprovedBusDate;
        if ( dateApproved != null) {
            lstApprovedBusDate = [SELECT Id, LastModifiedDate, createdDate, Current_Date__c FROM LMS_Date_Sync__c where createdDate < :dateApproved order by LastModifiedDate desc LIMIT 1];
        }
        DateTime dateToday = lstApprovedBusDate != null ? lstApprovedBusDate[0].Current_Date__c : lstLMS[0].Current_Date__c ;
       
        
        //DateTime dateToday = la.Approved_Date__c != null ? la.Approved_Date__c : Date.Today() ;
        string str = dateToday.format('yyyy-mm-dd');
        system.debug(dateToday);
        system.debug(String.valueOf(dateToday).split(' ')[0]);
        
        //Commented for Production LA-79 and 88
        Wrapper.Date1 = String.valueOf(dateToday).split(' ')[0];
        //Wrapper.Date1 = '';
        
        Wrapper.Spread = la.Spread__c != null ? string.valueOf(la.Spread__c) : '';
        Wrapper.customerType = la.Customer__r.Recordtype.Name != null ? la.Customer__r.Recordtype.Name : '' ;
        Wrapper.Product = la.Product_Name__c != null ? la.Product_Name__c : '';
        Wrapper.Transaction_Type = la.Transaction_type__c != null ? la.Transaction_type__c : '';
        
        Schema.DescribeFieldResult fieldResult1 = Loan_Application__c.Frequency__c.getDescribe();
        List<Schema.PicklistEntry> lstPicklist1 = fieldResult1.getPicklistValues();
        String strPLValue1;     
        for( Schema.PicklistEntry f : lstPicklist1) {
            if (f.getValue() == la.Frequency__c) {
                strPLValue1 = f.getLabel();
            }
        }
        Wrapper.Installment_type = strPLValue1!= null ? strPLValue1 : 'Monthly';    
        
        
            String jsonstring = JSON.serialize(Wrapper);
            jsonstring = jsonstring.replace('\"Date1\":\"','\"Date\":\"');  
            jsonstring = jsonstring.replace('&','&amp;');   
            system.debug('@@@ jsonstring '+jsonstring);         
     
        Http p=new Http();
        Rest_Service__mdt rs = [Select Client_Username__c, Client_Password__c, Service_EndPoint__c from Rest_Service__mdt where DeveloperName = 'SanctionLetter_MITC'];
        //String endpoint='https://mocksvc.mulesoft.com/mocks/1c472452-0fb4-498b-a66b-81d54327f7b7/sanctionLetter-Mitc';
        String endpoint= rs.Service_EndPoint__c;
        HttpRequest request =new HttpRequest();
        String username = rs.Client_Username__c;
          String password = rs.Client_Password__c; 
          Blob headerValue = Blob.valueOf(rs.Client_Username__c + ':' + rs.Client_Password__c);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        
        request.setHeader('Content-Type','application/json');
        request.setHeader('Username',rs.Client_Username__c);  
        request.setHeader('Password',rs.Client_Password__c);
        request.setHeader('accept','application/json');
        request.setHeader('Authorization', authorizationHeader);
        request.setHeader('Doc_Name','MITC');
        request.setEndPoint(endpoint);
        request.setMethod('POST');
        request.setBody(jsonstring); 
        request.setTimeOut(50000);
        HttpResponse response =p.send(request);
        LogUtility.createIntegrationLogs(jsonstring,response,endpoint);
        system.debug(jsonstring);
        system.debug('@@@ Response '+endpoint);
        system.debug('@@@ Response '+response.getBody());
        system.debug('@@@ response.getStatusCode()'+response.getStatusCode());
        if (response.getStatusCode() == 200) 
        {         
           

            deserializeResponse deresp=(deserializeResponse)System.JSON.deserialize(response.getBody(),deserializeResponse.class);
          system.debug(deresp.Result);
          Blob enco = EncodingUtil.base64Decode(deresp.Result);
                  
          Attachment attach = new Attachment();
                attach.contentType = 'application/pdf';
                attach.name = 'MITC-' + formattedDate + '.pdf';
                attach.parentId = loanApp;
                attach.body = enco; 
                insert attach;
                
          la.Repayment_Generated_for_Documents__c = false;
          update la;   
             
        }
        strStatus = 'true';
		}
        return strStatus;
    }
    
    @AuraEnabled
    public static void generateRepayment(String idLoanApp){
        system.debug('@@@in generate Repayment sanction');
        Utility.generateUpdatedRepayment(idLoanApp);
    }

    public class deserializeResponse 
    {
         public String Result;
    }
    
    public class mitcWrapper{
           public String Doc_ID;
           public String App_ID;
           public String loan_Amt;
           public String loan_Amt_Words;
           public String Interest_Type;
           public String Applicable_ROI_Floating;
           public String Applicable_ROI_Fixed;
           public String Applicable_ROI;
           public String Moratorium_Subsidy;
           public String Loan_Tenure;
           public String Purpose_Loan;
           public String Processing_Fee_Home;
           public String PF_NonHousing;
           public String Chrgs_paid_CERSAI;
           public String NonRefundable_amt;
           public String Imd_Amount;
           public String GST;
           public String Duplicate_ITCert;
           public String Stmt_accnt_soft;
           public String Stmt_accnt_hard;
           public String Stmt_accnt;
           public String Cheque_dishonour_chrgs;
           public String Change_Repayment_Mode;
           public String Change_loan_tenor;
           public String Change_Property;
           public String Loan_cancl_chrgs;
           public String Approved_Foreclosure_Charges;
           public String Hero_Reference_Rate;
           public String Doc_Retrieval;
           public String Collection_chrgs;
           public String Recovery_Fee;
           public String Title_Searchfee;
           public String Legal_Fee;
           public String Approved_Loan_Float;
           public String Approved_Loan_FixedOwn;
           public String Approved_Loan_Fixed;
           public String Approved_Loan_Float_NonInd;
           public String Recover_Preclosure_Loan;
           public String Docs_handling_chrgs;
           public String Refund_Processing_Fee;
           public String Conversion_Chrgs;
           public String Penalty_Delayed_Payments;
           public String Mortgage_Address;
           public String Guarantor_Name;
           public String other_security_dtls;
           public String Total_EMI_Amt;
           public String No_Months;
           public String Date_annual_bal;
           public String CustomerCare_Email;
           public String CustomerCare_PhNo;
           public String Level1_BO;
           public String Level2_Grievance_RO;
           public String Level3_CEO;
           public String Place;
           public List<CoBorrower_Name> CoBorrower_Name;
           public List<Sanc_Condts> Sanc_Condts;
           public String website ;
            public String Level2_Address_Grievance_RO1  ;
            public String Level2_Address_Grievance_RO2 ;
            public String Level2_Address_Grievance_RO3 ;
            public String Level2_Grievance_officer_Email_Id ;
            public String Level2_Phone_No  ;
            public String Authority_Designation ;
            public String Date1;
            public String Spread;
            public String customerType;
            public String Transaction_Type;
            public String Product;
            public String Installment_type;
    }


    public class Sanc_Condts {
      public String Cond;
    }

    public class CoBorrower_Name {
      public String CBN;
    }   
    
}