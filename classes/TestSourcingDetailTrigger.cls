@isTest
public class TestSourcingDetailTrigger {
     public static testMethod void method1()
    {

           Profile p=[SELECT id,name from profile where name=:'Sales Team'];
         UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        List<User> lstBranchManager= new List<User>();                          
          User u = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,Role_in_Sales_Hierarchy__c = 'SM'
        );
        lstBranchManager.add(u);
        
        User u1 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
         lstBranchManager.add(u1);
        insert lstBranchManager;                        
                           
                           
        User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId 
                       from user where ID=:UserInfo.getUserId()];
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=lstBranchManager[0].Id;
        loggedUser.ManagerID=lstBranchManager[0].Id;
        update loggedUser;
       
        
       
        u.ManagerId=UserInfo.getUserID();
       
        system.runAs(loggedUser){              
       
        
         Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='9923453331',
                                  Date_of_Incorporation__c=Date.newInstance(2018,11,11));
         
         Database.insert(Cob);                
                            
         Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                     Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                     Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                     Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                     Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
          
                           
         Database.insert(sc); 
        
         
         Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,
                                                           StageName__c='Customer Onboarding',
                                                           Sub_Stage__c='Application Initiation',
                                                           Transaction_type__c='SC',Requested_Amount__c=10000,
                                                           Applicant_Customer_segment__c='Salaried',
                                                           Branch__c='test',Line_Of_Business__c='Open Market',
                                                           //Assigned_Sales_User__c=u.id,
                                                           Loan_Purpose__c='20',
                                                           Requested_EMI_amount__c=1000,Approved_ROI__c=5,
                                                           Requested_Loan_Tenure__c=5,
                                                           Government_Programs__c='PMAY',
                                                           Loan_Engine_2_Output__c='STP',
                                                           Property_Identified__c=True,ownerId=UserInfo.getUserID(),
                                                           Assigned_Scan_DataMaker__c=u.id,
                                                           Assigned_scan_DataChecker__c=u.id);
                                                           
                                                           
        
        
        // Database.insert(LAob);
        
        List<Loan_Application__c> listLAob= new List<Loan_Application__c>();
        listLAob.add(LAob); 
        insert listLAob;
        
        
        DST_Master__c DSTMaster=new DST_Master__c(Name='Test DST',Inspector_ID__c='1234');     
        insert DSTMaster;  
        
        Cross_Sell_Partner_Name__c CSPN=new Cross_Sell_Partner_Name__c(Name='Test');
        insert CSPN;                                               
        Sourcing_Detail__c SD2=new Sourcing_Detail__c(Loan_Application__c=LAob.id,DST_Name__c=DSTMaster.id,
                                                      Source_Code__c='20',Lead_Sourcing_Detail__c='PaisaBazaar.com',//Sales_User_Name__c=u.id,
                                                      Cross_Sell_Partner_Name__c=CSPN.id);  
                                                      
        insert SD2; 
        }
    }
    
     public static testMethod void method2()
    {

           Profile p=[SELECT id,name from profile where name=:'Sales Team'];
         UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        List<User> lstBranchManager= new List<User>();                          
          User u = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,Role_in_Sales_Hierarchy__c = 'SM'
        );
        lstBranchManager.add(u);
        
        User u1 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
         lstBranchManager.add(u1);
        insert lstBranchManager;                        
                           
                           
        User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId 
                       from user where ID=:UserInfo.getUserId()];
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=lstBranchManager[0].Id;
        loggedUser.ManagerID=lstBranchManager[0].Id;
        update loggedUser;
       
        
       
        u.ManagerId=UserInfo.getUserID();
       
        system.runAs(loggedUser){              
       
        
         Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='9923453331',
                                  Date_of_Incorporation__c=Date.newInstance(2018,11,11));
         
         Database.insert(Cob);                
                            
         Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                     Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                     Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                     Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                     Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
          
                           
         Database.insert(sc); 
        
         
         Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,
                                                           StageName__c='Customer Onboarding',
                                                           Sub_Stage__c='Application Initiation',
                                                           Transaction_type__c='SC',Requested_Amount__c=10000,
                                                           Applicant_Customer_segment__c='Salaried',
                                                           Branch__c='test',Line_Of_Business__c='Open Market',
                                                           //Assigned_Sales_User__c=u.id,
                                                           Loan_Purpose__c='20',
                                                           Requested_EMI_amount__c=1000,Approved_ROI__c=5,
                                                           Requested_Loan_Tenure__c=5,
                                                           Government_Programs__c='PMAY',
                                                           Loan_Engine_2_Output__c='STP',
                                                           Property_Identified__c=True,ownerId=UserInfo.getUserID(),
                                                           Assigned_Scan_DataMaker__c=u.id,
                                                           Assigned_scan_DataChecker__c=u.id);
                                                           
                                                           
        
        
        // Database.insert(LAob);
        
        List<Loan_Application__c> listLAob= new List<Loan_Application__c>();
        listLAob.add(LAob); 
        insert listLAob;
        
        
        DST_Master__c DSTMaster=new DST_Master__c(Name='Test DST',Inspector_ID__c='1234');     
        insert DSTMaster;  
        
        Cross_Sell_Partner_Name__c CSPN=new Cross_Sell_Partner_Name__c(Name='Test');
        insert CSPN;                                               
       
        Id RectypeId = Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('Digital Alliance').getRecordTypeId(); 
        Sourcing_Detail__c SD4=new Sourcing_Detail__c(Loan_Application__c=LAob.id,DST_Name__c=DSTMaster.id,
                                                      Source_Code__c='20',Lead_Sourcing_Detail__c='My Money Mantra',//Sales_User_Name__c=u.id,
                                                      Cross_Sell_Partner_Name__c=CSPN.id, recordTypeId = RectypeId);  
                                                      
        insert SD4;  
          
        }
    }
    public static testMethod void method3()
    {

           Profile p=[SELECT id,name from profile where name=:'Sales Team'];
         UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        List<User> lstBranchManager= new List<User>();                          
          User u = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,Role_in_Sales_Hierarchy__c = 'SM'
        );
        lstBranchManager.add(u);
        
        User u1 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
         lstBranchManager.add(u1);
        insert lstBranchManager;                        
                           
                           
        User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId 
                       from user where ID=:UserInfo.getUserId()];
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=lstBranchManager[0].Id;
        loggedUser.ManagerID=lstBranchManager[0].Id;
        update loggedUser;
       
        
       
        u.ManagerId=UserInfo.getUserID();
       
        system.runAs(loggedUser){              
       
        
         Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='9923453331',
                                  Date_of_Incorporation__c=Date.newInstance(2018,11,11));
         
         Database.insert(Cob);                
                            
         Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                     Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                     Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                     Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                     Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
          
                           
         Database.insert(sc); 
        
         
         Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,
                                                           StageName__c='Customer Onboarding',
                                                           Sub_Stage__c='Application Initiation',
                                                           Transaction_type__c='SC',Requested_Amount__c=10000,
                                                           Applicant_Customer_segment__c='Salaried',
                                                           Branch__c='test',Line_Of_Business__c='Open Market',
                                                           //Assigned_Sales_User__c=u.id,
                                                           Loan_Purpose__c='20',
                                                           Requested_EMI_amount__c=1000,Approved_ROI__c=5,
                                                           Requested_Loan_Tenure__c=5,
                                                           Government_Programs__c='PMAY',
                                                           Loan_Engine_2_Output__c='STP',
                                                           Property_Identified__c=True,ownerId=UserInfo.getUserID(),
                                                           Assigned_Scan_DataMaker__c=u.id,
                                                           Assigned_scan_DataChecker__c=u.id);
                                                           
                                                           
        
        
        // Database.insert(LAob);
        
        List<Loan_Application__c> listLAob= new List<Loan_Application__c>();
        listLAob.add(LAob); 
        insert listLAob;
        
        
        DST_Master__c DSTMaster=new DST_Master__c(Name='Test DST',Inspector_ID__c='1234');     
        insert DSTMaster;  
        
        Cross_Sell_Partner_Name__c CSPN=new Cross_Sell_Partner_Name__c(Name='Test');
        insert CSPN;                                               
        Id RectypeId = Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('Digital Alliance').getRecordTypeId(); 
        List<Sourcing_Detail__c> lstSD = new List<Sourcing_Detail__c>();
            Sourcing_Detail__c SD5=new Sourcing_Detail__c(Loan_Application__c=LAob.id,DST_Name__c=DSTMaster.id,
                                                      Source_Code__c='20',Lead_Sourcing_Detail__c='My Money Karma',//Sales_User_Name__c=u.id,
                                                      Cross_Sell_Partner_Name__c=CSPN.id, recordTypeId= RectypeId);  
                                                      
        insert SD5; 
            lstSD.add(SD5);
        /*Sourcing_Detail__c SD6=new Sourcing_Detail__c(Loan_Application__c=LAob.id,DST_Name__c=DSTMaster.id,
                                                      Source_Code__c='20',Lead_Sourcing_Detail__c='My Money Karma',//Sales_User_Name__c=u.id,
                                                      Cross_Sell_Partner_Name__c=CSPN.id, recordTypeId= RectypeId);  
                                                      
        insert SD6;   
           lstSD.add(SD6); */
        SourcingDetailTriggerHelper.duplicationDetection(lstSD);
        SourcingDetailTriggerHelper.checkSourceDetail(SD5);
        }
    }
    
    /* public static testMethod void method6()
    {

           Profile p=[SELECT id,name from profile where name=:'Sales Team'];
         UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        List<User> lstBranchManager= new List<User>();                          
          User u = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,Role_in_Sales_Hierarchy__c = 'SM'
        );
        lstBranchManager.add(u);
        
        User u1 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
         lstBranchManager.add(u1);
        insert lstBranchManager;                        
                           
                           
        User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId 
                       from user where ID=:UserInfo.getUserId()];
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=lstBranchManager[0].Id;
        loggedUser.ManagerID=lstBranchManager[0].Id;
        update loggedUser;
       
        
       
        u.ManagerId=UserInfo.getUserID();
       
        system.runAs(loggedUser){              
       
        
         Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='9923453331',
                                  Date_of_Incorporation__c=Date.newInstance(2018,11,11));
         
         Database.insert(Cob);   
            
            Account Cob1= new Account(PAN__c='AXEGA3767B',Name='TestCustomer',Phone='9923553331',
                                  Date_of_Incorporation__c=Date.newInstance(2018,11,11));
         
         Database.insert(Cob1);   
                            
         Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                     Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                     Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                     Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                     Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
          
                           
         Database.insert(sc); 
        
         
         Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,
                                                           StageName__c='Customer Onboarding',
                                                           Sub_Stage__c='Application Initiation',
                                                           Transaction_type__c='SC',Requested_Amount__c=10000,
                                                           Applicant_Customer_segment__c='Salaried',
                                                           Branch__c='test',Line_Of_Business__c='Open Market',
                                                           //Assigned_Sales_User__c=u.id,
                                                           Loan_Purpose__c='20',
                                                           Requested_EMI_amount__c=1000,Approved_ROI__c=5,
                                                           Requested_Loan_Tenure__c=5,
                                                           Government_Programs__c='PMAY',
                                                           Loan_Engine_2_Output__c='STP',
                                                           Property_Identified__c=True,ownerId=UserInfo.getUserID(),
                                                           Assigned_Scan_DataMaker__c=u.id,
                                                           Assigned_scan_DataChecker__c=u.id);
       
            Loan_Application__c LAob1= new Loan_Application__c(Customer__c=Cob1.id,Scheme__c=sc.id,
                                                           StageName__c='Customer Onboarding',
                                                           Sub_Stage__c='Application Initiation',
                                                           Transaction_type__c='SC',Requested_Amount__c=10000,
                                                           Applicant_Customer_segment__c='Salaried',
                                                           Branch__c='test',Line_Of_Business__c='Open Market',
                                                           //Assigned_Sales_User__c=u.id,
                                                           Loan_Purpose__c='20',
                                                           Requested_EMI_amount__c=1000,Approved_ROI__c=5,
                                                           Requested_Loan_Tenure__c=5,
                                                           Government_Programs__c='PMAY',
                                                           Loan_Engine_2_Output__c='STP',
                                                           Property_Identified__c=True,ownerId=UserInfo.getUserID(),
                                                           Assigned_Scan_DataMaker__c=u.id,
                                                           Assigned_scan_DataChecker__c=u.id);
                                                           
        
        
        // Database.insert(LAob);
        
        List<Loan_Application__c> listLAob= new List<Loan_Application__c>();
        listLAob.add(LAob); 
        listLAob.add(LAob1);     
        insert listLAob;
        
        
        DST_Master__c DSTMaster=new DST_Master__c(Name='Test DST',Inspector_ID__c='1234');     
        insert DSTMaster;  
        
        Cross_Sell_Partner_Name__c CSPN=new Cross_Sell_Partner_Name__c(Name='Test');
        insert CSPN;                                               
        Id RectypeId = Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('Digital Alliance').getRecordTypeId(); 
        List<Sourcing_Detail__c> lstsource = new List<Sourcing_Detail__c>();
        Sourcing_Detail__c SD5=new Sourcing_Detail__c(Loan_Application__c=LAob.id,DST_Name__c=DSTMaster.id,
                                                      Source_Code__c='20',Lead_Sourcing_Detail__c='HHFL Website',//Sales_User_Name__c=u.id,
                                                      Cross_Sell_Partner_Name__c=CSPN.id, recordTypeId= RectypeId);  
        lstsource.add(SD5);                                            
        Sourcing_Detail__c SD=new Sourcing_Detail__c(Loan_Application__c=LAob1.id,DST_Name__c=DSTMaster.id,
                                                      Source_Code__c='20',Lead_Sourcing_Detail__c='HHFL Website',//Sales_User_Name__c=u.id,
                                                      Cross_Sell_Partner_Name__c=CSPN.id, recordTypeId= RectypeId);  
        lstsource.add(SD);                                           
        insert lstsource;   
        SourcingDetailTriggerHelper.checkSourceDetail(SD);
        }
    }*/

}