public class RequestUCICController {
    @AuraEnabled
    public static String checkEligibility(Id loanId){
        String responseString;
        if(String.isNotBlank(loanId)){
            Loan_Application__c objLoanApplication = [SELECT Id,Name,StageName__c,Sub_Stage__c,Branch__c,Line_Of_Business__c,OwnerId,Credit_Manager_User__c,
                                                      (SELECT Id,Applicant_Type__c FROM Loan_Contacts__r),
                                                      (SELECT Id,RecordTypeId,Loan_Application__c,CreatedDate,
                                                       UCIC_Successful_Initiation_DateTime__c 
                                                       FROM Customer_Integrations__r 
                                                       WHERE RecordType.Name =: Constants.UCIC ORDER BY CreatedDate DESC)
                                                      FROM Loan_Application__c WHERE Id=: loanId LIMIT 1];
            //Customer_Integrations__r child relationship query added by Abhilekh on 11th September 2020 for UCIC Phase II
            
            System.debug('Debug Log for objLoanApplication'+objLoanApplication);
            if(objLoanApplication != null) {
                //Commented by Saumya For UCIC II Enhancements
                /*if(objLoanApplication.Sub_Stage__c != Constants.Disbursement_Maker || ) {
responseString = 'Operation not allowed : You cannot hit UCIC at this stage.';
}
else {*/ //Commented by Saumya For UCIC II Enhancements
                // List<Loan_Contact__c> loanContactList = [Select id,Loan_Applications__r.Id,Customer__r.UCICId__c from Loan_Contact__c WHERE Customer__r.UCICId__c = NULL AND Loan_Applications__r.Id  =: loanId];
                /*if(loanContactList.size()>0){
responseString = 'Eligible for Callout';
UCICCallout.makeUCICCallout(loanId);
}
else{
responseString = 'Not Eligible for Callout';
}*/
                /******Below code added by Abhilekh on 11th September 2020 for UCIC Phase II*******************/
				User p = [SELECT Id, Profile.Name from User where Id =: UserInfo.getUserId()];
                if(objLoanApplication.Sub_Stage__c == Constants.Credit_Review || objLoanApplication.Sub_Stage__c == Constants.Re_Credit ||  objLoanApplication.Sub_Stage__c == Constants.Re_Look || objLoanApplication.Sub_Stage__c == Constants.Customer_Negotiation || objLoanApplication.Sub_Stage__c == 'L1 Credit Approval' || objLoanApplication.Sub_Stage__c == 'Credit Authority Approval') {                   
                if(UserInfo.getUserId() != objLoanApplication.Credit_Manager_User__c && p.Profile.Name != 'System Administrator'){
                    responseString = 'Operation not allowed : You are not allowed to generate Dedupe Results.';
                    }
                }
                if(objLoanApplication.Customer_Integrations__r.size() > 0){
                    for(Customer_Integration__c ci: objLoanApplication.Customer_Integrations__r){
                        if(ci.UCIC_Successful_Initiation_DateTime__c != null){
                            Long dt = ci.UCIC_Successful_Initiation_DateTime__c.getTime();
                            Long dt1 = DateTime.now().getTime();
                            Long milliseconds = dt1 - dt;
                            Long seconds = milliseconds / 1000;
                            Long minutes = seconds / 60;
                            
                            if(minutes <= Long.valueOf(Label.UCIC_Initiation_Time_Limit)){
                                responseString = 'Dedupe Results has already been initiated for response, please try after some time.';
                            }
                        }
                    }
                }
                
                if(responseString == null || responseString == ''){
                    List<Customer_Integration__c> lstCI = new List<Customer_Integration__c>();
                    Id UCICRecordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get(Constants.UCIC).getRecordTypeId();
                    List<Branch_LOB_User_Mapping__c> allBLUs = [Select id,Policy_Head__c,BH__c,NCH__c,CCH__c,ZCH__c,CRO__c,CEO__c,ZCH_Central_Salaried__c from Branch_LOB_User_Mapping__c where Branch__c = :objLoanApplication.Branch__c AND Line_Of_Business__c = :objLoanApplication.Line_Of_Business__c]; // Added by Saumya ZCH_Central_Salaried__c For Central Salaried Issue of Approval Process
                    for(Loan_Contact__c lc: objLoanApplication.Loan_Contacts__r){
                        if(lc.Applicant_Type__c == Constants.Applicant){
                            Customer_Integration__c ci = new Customer_Integration__c();
                            ci.Loan_Application__c = objLoanApplication.Id;
                            ci.Loan_Contact__c = lc.Id;
                            ci.Latest_Record__c = true;
                            ci.RecordTypeId = UCICRecordTypeId;
                            if( allBLUs.size() > 0){
                                if( allBLUs[0].CRO__c != null)    
                                    ci.CRO__c = allBLUs[0].CRO__c;
                                
                                if( allBLUs[0].NCH__c != null)
                                    ci.NCM__c = allBLUs[0].NCH__c;
                                
                                if( allBLUs[0].Policy_Head__c != null)    
                                    ci.Policy_Head__c = allBLUs[0].Policy_Head__c;
                            }
                            lstCI.add(ci);
                        }
                    }
                    
                    if(lstCI.size() > 0){
                        insert lstCI;
                    }
                    responseString = 'Eligible for Callout';
                    UCICCallout.makeUCICCallout(loanId);
                }
                
                /******Below code added by Abhilekh on 11th September 2020 for UCIC Phase II*******************/
                
                //} //Commented by Saumya For UCIC II Enhancements
            }
        }
        System.debug('Debug Log for responseString'+responseString);
        return responseString;
    }
}