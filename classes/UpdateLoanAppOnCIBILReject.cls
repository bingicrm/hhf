public class UpdateLoanAppOnCIBILReject {
    
    @AuraEnabled
    public static MoveNextWrapper updateLoanApplication(id laID) 
    {
        try{ 
           loan_application__c la = new loan_application__c();
            la = [select Id, StageName__c, Sub_Stage__c from loan_application__c where Id =: laID
                     ];
            
          
                    la.StageName__c='Loan Rejected';
                    la.Sub_Stage__c='CIBIL Reject';   
             
                            update la; 

            return new MoveNextWrapper(false,la.Sub_Stage__c);
        }
        
        catch (DMLException e) 
        {
            //return e.getDmlMessage(0);
            return new MoveNextWrapper(true,e.getDmlMessage(0));
        } 
        catch (Exception e) 
        {
            return new MoveNextWrapper(true,e.getMessage());
        }    
    }
    
    public class MoveNextWrapper
    {
        @AuraEnabled
        public Boolean error;
        @AuraEnabled
        public String msg;
        
        public MoveNextWrapper(Boolean error, String msg )
        {
            this.error = error;
            this.msg = msg;
        }
        
    }
    
}