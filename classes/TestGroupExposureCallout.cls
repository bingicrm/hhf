@istest
public class TestGroupExposureCallout {
    public static testmethod void method1(){
    	
    GroupExposureResponseBody obj1 = new GroupExposureResponseBody();
   	Account acc=new Account();
    acc.Salutation='Mr';
    acc.FirstName='Test';
    acc.LastName='TesstMWDFSADF';
    acc.phone='9688467902';
    acc.PAN__c='YCJDP7834G';
    acc.Date_of_Incorporation__c=date.today();
    insert acc;
     
     Document_Master__c objdoc = new Document_Master__c(Name = 'Sanction Letter', Doc_Id__c = '123');
     insert objdoc;
  
     
    System.debug(acc.id);
    
    Scheme__c sc=new Scheme__c();
    sc.name='Construction Finance';
    sc.Scheme_Code__c='CF';
    sc.Scheme_Group_ID__c='6';
    sc.Product_Code__c='HL';
    insert sc;
    System.debug(sc.id);
      
    Loan_Application__c lap=new Loan_Application__c ();
    lap.Customer__c=acc.id;
    lap.Transaction_type__c='DAA';
    lap.Scheme__c=sc.id;
    lap.Requested_Amount__c = 50000000;
    lap.Approved_Loan_Amount__c=5000000;
    lap.Transaction_Type__c='PB';
    insert lap;


  	Sanction_Condition_Master__c scm = new Sanction_Condition_Master__c();
   scm.Sanction_Condition__c = 'Sanction condition ABC';
   insert scm;
   
   List<Loan_Contact__c> lstofcon=[select id,Borrower__c from Loan_Contact__c where Loan_Applications__c=:lap.id];
        Group_Exposure__c obj =  new Group_Exposure__c(LoanApplication__c = lap.id, Customer_Detail__c = lstofcon[0].id);
        insert obj;
        list<group_exposure__c> lst = new list<group_exposure__c>();
        lst.add(obj);
            groupExposureTriggerHandler.afterInsert(lst);
        lstofcon[0].Borrower__c = '1';
        lstofcon[0].Category__c = '1';
        update lstofcon[0];
        GroupExposureCallout.checkGroupID(lap);
        GroupExposureCallout.makeAPICallout(lstofcon[0].id);
        GroupExposureCallout.deletRecs(lap);
        GroupExposureCallout.getLoanApp(lap.id);
        Group_Exposure__c obj2 =  new Group_Exposure__c(LoanApplication__c = lap.id, Customer_Detail__c = lstofcon[0].id);
        list<group_exposure__c> lst2 = new list<group_exposure__c>();
        lst2.add(obj2);
        GroupExposureCallout.saveGERecords(JSON.serialize(lst2));
        GroupExpViewController.getGroupExp(lap.id);
        GroupExpViewController.calculateSumPO(lap.id);
        GroupExpViewController.saveGERecords(JSON.serialize(lst2), JSON.serialize(lst2));
        GroupExpViewController.saveGEAdjustment(lap.id, 10, 100);
         
    }
    
        public static testmethod void method2(){
           Account acc=new Account();
    acc.Salutation='Mr';
    acc.FirstName='Test';
    acc.LastName='TesstMWDFSADF';
    acc.phone='9688467902';
    acc.PAN__c='YCJDP7834G';
    acc.Date_of_Incorporation__c=date.today();
    insert acc;
     
     Document_Master__c objdoc = new Document_Master__c(Name = 'Sanction Letter', Doc_Id__c = '123');
     insert objdoc;
  
     
    System.debug(acc.id);
    
    Scheme__c sc=new Scheme__c();
    sc.name='Construction Finance';
    sc.Scheme_Code__c='CF';
    sc.Scheme_Group_ID__c='6';
    sc.Product_Code__c='HL';
    insert sc;
    System.debug(sc.id);
      
    Loan_Application__c lap=new Loan_Application__c ();
    lap.Customer__c=acc.id;
    lap.Transaction_type__c='DAA';
    lap.Scheme__c=sc.id;
    lap.Requested_Amount__c = 50000000;
    lap.Approved_Loan_Amount__c=5000000;
    lap.Transaction_Type__c='PB';
    insert lap;


  	Sanction_Condition_Master__c scm = new Sanction_Condition_Master__c();
   scm.Sanction_Condition__c = 'Sanction condition ABC';
   insert scm;
   
   List<Loan_Contact__c> lstofcon=[select id,Borrower__c from Loan_Contact__c where Loan_Applications__c=:lap.id];
        Group_Exposure__c obj =  new Group_Exposure__c(LoanApplication__c = lap.id, Customer_Detail__c = lstofcon[0].id);
        insert obj;
        list<group_exposure__c> lst = new list<group_exposure__c>();
        lst.add(obj);
            groupExposureTriggerHandler.afterInsert(lst);
        lstofcon[0].Borrower__c = '1';
        lstofcon[0].Category__c = '1';
        update lstofcon[0];
        //GroupExposureCallout.checkGroupID(lap);
        GroupExposureCallout.makeAPICallout(lstofcon[0].id);
        //GroupExposureCallout.deletRecs(lap);
        //GroupExposureCallout.getLoanApp(lap.id);
        Group_Exposure__c obj2 =  new Group_Exposure__c(LoanApplication__c = lap.id, Customer_Detail__c = lstofcon[0].id);
        list<group_exposure__c> lst2 = new list<group_exposure__c>();
        lst2.add(obj2);
        GroupExposureCallout.saveGERecords(JSON.serialize(lst2));
    }
}