@isTest(SeeAllData = false)
private class ProjectBuilderTriggerTest {

	private static testMethod void createMultipleBuilders() {
        
	    List<RecordType> lstRecordType = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Individual'];
        Id builderIndvRecTypeId;
        if(!lstRecordType.isEmpty()) {
            builderIndvRecTypeId = lstRecordType[0].Id;
        }
        
        Account acc = new Account();
            acc.RecordTypeId = builderIndvRecTypeId;
            acc.Name = 'Individual Test Acc';
            acc.PAN__c = 'AAAAA3333B';
            acc.GSTIN__c = '27AAHFG0551H1ZL';
        insert acc;
        
        List<RecordType> lstRecordType2 = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Corporate'];
        Id builderCorpRecTypeId;
        if(!lstRecordType2.isEmpty()) {
            builderCorpRecTypeId = lstRecordType2[0].Id;
        }
        
        List<RecordType> lstIndividualPBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Individual'];
        List<RecordType> lstCorporatePBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Corporate'];
        
        Account acc2 = new Account();
            acc2.RecordTypeId = builderCorpRecTypeId;
            acc2.Name = 'Test Corporate Account';
            acc2.PAN__c = 'AAAAB2222B';
            
        insert acc2;
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83');
        insert objPincode;
        
        Document_Master__c docMasObj = new Document_Master__c();
        docMasObj.Name = 'Test GSTIN - Registration certificate';
        docMasObj.Doc_Id__c = '123344';
        docMasObj.Approving_authority__c = 'Business Head';
        docMasObj.Applicable_for_APF__c = true;
        Database.insert(docMasObj);
        
        Document_type__c docTypeObj = new Document_type__c();
        docTypeObj.Name = 'Builder - APF';
        docTypeObj.Approving_authority__c    = 'Business Head';
         docTypeObj.Applicable_for_APF__c = true;	
        Database.insert(docTypeObj);
        
        APF_Document_Checklist__c objAPF = new APF_Document_Checklist__c();
        objAPF.Active_del__c = true;
        objAPF.Document_Master__c = docMasObj.Id;
        objAPF.Document_Type__c = docTypeObj.id;
        objAPF.Mandatory__c = true;
        objAPF.Valid_for_Project_Type__c = 'New';
        insert objAPF;
        
        Project__c objProject = new Project__c();
        //objProject.Assigned_Sales_User__c = assignedSalesUser != null ? assignedSalesUser.Id : null;
        objProject.Account__c = acc.Id;
        objProject.Project_Name__c = 'Test Project';
        objProject.APF_Status__c = 'APF Initiated';
        objProject.Stage__c = 'APF Initiation';
        objProject.Sub_Stage__c = 'Project Data Capture';
        objProject.APF_Type__c = 'New';
        objProject.Address_Line_1__c = 'Test Address';
        objProject.Pincode__c = objPincode.Id;
        insert objProject;
        
        Project_Builder__c objProjectBuilderCorp = new Project_Builder__c();
        objProjectBuilderCorp.RecordTypeId = lstCorporatePBId[0].Id;
        objProjectBuilderCorp.Builder_Name__c = acc2.Id;
        objProjectBuilderCorp.Constitution__c = '24';
        objProjectBuilderCorp.Builder_Status__c = 'Resident';
        objProjectBuilderCorp.Contact_Number_Mobile__c = '9999444223';
        objProjectBuilderCorp.PAN_Number__c = 'EGNPS1111T';
        objProjectBuilderCorp.Project__c = objProject.Id; 
        objProjectBuilderCorp.Preferred_Communication_Address__c = 'CURRES'; 
        //objProjectBuilderCorp. =
        Database.insert(objProjectBuilderCorp,false);
        
        acc2.GSTIN__c = '27AAHFG0551H1ZL';
        update acc2;
        
        insert objProjectBuilderCorp;
        System.debug('Debug Log for GSTIN'+objProjectBuilderCorp);
        
        
        objProjectBuilderCorp.GSTIN_Number__c = '27AAHFG0551H1ZL';
        //objProjectBuilderCorp.Builder_Role__c = Constants.strPBConstructorMarketer;
        update objProjectBuilderCorp;
        
        Set<Id> setProjectId = new Set<Id>();
        setProjectId.add(objProject.Id);
        
        
        
        List<Project_Builder__c> lstProjectBuilder = [SELECT Id, RecordTypeId, Builder_Category__c, GSTIN_Available__c, Name, Date_Of_Birth__c, Builder_Name__r.RecordType.DeveloperName, All_Promoter_Count__c, Builder_Type__c, Class_Of_Activity__c, Builder_Name__c, Builder_Name__r.RecordTypeId, Builder_Status__c, Constitution__c, Builder_Role__c, Contact_Number_Mobile__c, Email__c, Preferred_Communication_Address__c, Address_Line_1__c, Address_Line_2__c, Pincode__c, Promoter_PAN_Validation_Count__c, Promoter_Cibil_Validation_Count__c, Project__c,(Select Id, Name, Name_of_Promoter__c, Email__c, Contact_No_Mobile__c, Preferred_Communication_Address__c, Promoter_Status__c, Address_Line_1__c, Address_Line_2__c, Pincode__c from Promoters__r) From Project_Builder__c Where Project__c =: objProject.Id];
        System.debug('Debug Log for lstProjectBuilder size'+lstProjectBuilder.size());
        
        List<Promoter__c> lstPromoter = new List<Promoter__c>();
        if(!lstProjectBuilder.isEmpty()) {
            for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                objProjectBuilder.GSTIN_Available__c = true;
                objProjectBuilder.GSTIN_Number__c = '27AAHFG0551H1ZL';
                if(objProjectBuilder.Promoters__r.isEmpty() && objProjectBuilder.Builder_Name__r.RecordType.DeveloperName != 'Builder_Individual'){
                    Promoter__c objPromoter = new Promoter__c(Project_Builder__c = objProjectBuilder.Id);
                    lstPromoter.add(objPromoter);
                }
            }
            update lstProjectBuilder;
            System.debug('Debug Log for lstPromoter'+lstPromoter.size());
            if(!lstPromoter.isEmpty()) {
                insert lstPromoter;
            }
            System.debug('Debug Log for lstPromoter post insertion'+lstPromoter);
            update lstPromoter;
        }
        
        
        Id ProjectId = objProject.Id;
        
        
        List<Project_Document_Checklist__c> lstProjDocChecklist = [Select Id, Document_Master__c, Document_Type__c, File_Check_Completed__c, Scan_Check_Completed__c, Mandatory__c, Valid_for_Project_Type__c, Project__c, Status__c From Project_Document_Checklist__c Where Mandatory__c = true AND Project__c = : ProjectId];
        if(!lstProjDocChecklist.isEmpty()) {
            for(Project_Document_Checklist__c objPDC : lstProjDocChecklist) {
                objPDC.Status__c = Constants.strDocStatusReceived;
            }
            
            update lstProjDocChecklist;
            
        }
        
        APF_MoveToNextStageController.showWarningMsgs(ProjectId);
            APF_MoveToNextStageController.initialSubStageNotification(ProjectId);
            APF_MoveToNextStageController.MoveNextResponse objMoveNextResponse = APF_MoveToNextStageController.saveProject(ProjectId);
        	APF_MoveToNextStageController.NextStageWrapper nextWrap = new APF_MoveToNextStageController.NextStageWrapper('No Movement', 'No Movement');
            System.debug('Debug Log for objMoveNextResponse From '+objMoveNextResponse);
            
            
            objProject.Stage__c = 'APF Operations Control';
            objProject.Sub_Stage__c = 'COPS : Data Maker';
            objProject.APF_Status__c = 'Operations Control';
            
            update objProject;
            
            APF_MoveToNextStageController.showWarningMsgs(ProjectId);
            APF_MoveToNextStageController.initialSubStageNotification(ProjectId);
            APF_MoveToNextStageController.MoveNextResponse objMoveNextResponseCOPS = APF_MoveToNextStageController.saveProject(ProjectId);
            System.debug('Debug Log for objMoveNextResponseCOPS From '+objMoveNextResponseCOPS);
            
            ProjectTriggerHelper.subStgChangetoCOPSDCOperations(setProjectId);
            ProjectTriggerHelper.subStgChangefromCOPSDCOperations(setProjectId);
            
            
            List<Project_Builder__c> lstProjBuilderTriggerNew = new List<Project_Builder__c>();
            
            Project_Builder__c objProjectBuilderIndNew = new Project_Builder__c(Builder_Name__c = acc.Id, Project__c = objProject.Id, Contact_Number_Mobile__c = '8887877333', PAN_Number__c = 'AAAEE9986O', Date_Of_Birth__c = Date.newInstance(1990, 9, 7));
            Project_Builder__c objProjectBuilderCorpNew = new Project_Builder__c(Builder_Name__c = acc2.Id, Project__c = objProject.Id,  Contact_Number_Mobile__c = '8777799999', PAN_Number__c = 'BBBRE1234I', Date_Of_Birth__c = Date.newInstance(1989, 9, 7));
            
            lstProjBuilderTriggerNew.add(objProjectBuilderIndNew);
            lstProjBuilderTriggerNew.add(objProjectBuilderCorpNew);
            Database.insert(lstProjBuilderTriggerNew,false);
            System.debug('>>>>>>>>>>>>>>>>>>'+lstProjBuilderTriggerNew);
            //ProjectBuilderTriggerHandler.onBeforeInsert(lstProjBuilderTriggerNew);
            
            Project_Builder__c objProjectBuilderIndNew2 = new Project_Builder__c(RecordTypeId = lstCorporatePBId[0].Id, Builder_Name__c = acc.Id, Project__c = objProject.Id, Contact_Number_Mobile__c = '8887877333', PAN_Number__c = 'AAAEE9986O', Date_Of_Birth__c = Date.newInstance(1990, 9, 7));
            Project_Builder__c objProjectBuilderCorpNew2 = new Project_Builder__c(RecordTypeId = lstIndividualPBId[0].Id,Builder_Name__c = acc2.Id, Project__c = objProject.Id,  Contact_Number_Mobile__c = '8777799999', PAN_Number__c = 'BBBRE1234I', Date_Of_Birth__c = Date.newInstance(1989, 9, 7));
            
            lstProjBuilderTriggerNew.add(objProjectBuilderIndNew2);
            lstProjBuilderTriggerNew.add(objProjectBuilderCorpNew2);
            Database.insert(lstProjBuilderTriggerNew,false);
            System.debug('>>>>>>>>>>>>>>>>>>'+lstProjBuilderTriggerNew);
            
            Set<Id> setPBIDs = new Set<Id>();
            for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                if(objProjectBuilder.RecordTypeId == lstCorporatePBId[0].Id) {
                    objProjectBuilder.Builder_Category__c = 'Cat B';
                }
                
                else {
                    objProjectBuilder.Builder_Category__c = 'Cat A';
                }
                objProjectBuilder.Builder_Role__c = Constants.strPBConstructorMarketer;
                setPBIDs.add(objProjectBuilder.Id);
            }
            
            update lstProjectBuilder;
            ProjectBuilderTriggerHelper.populateProjectCategoryonProject(setPBIDs);
            
            for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                if(objProjectBuilder.RecordTypeId == lstCorporatePBId[0].Id) {
                    objProjectBuilder.Builder_Category__c = 'Cat C';
                }
                
                else {
                    objProjectBuilder.Builder_Category__c = 'Cat A';
                }
                objProjectBuilder.Builder_Role__c = Constants.strPBConstructorMarketer;
                setPBIDs.add(objProjectBuilder.Id);
            }
            
            update lstProjectBuilder;
            test.startTest();
            ProjectBuilderTriggerHelper.populateProjectCategoryonProject(setPBIDs);
            test.stopTest();
            
	}
	
	private static testMethod void createMultipleBuilders2() {
        
	    List<RecordType> lstRecordType = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Individual'];
        Id builderIndvRecTypeId;
        if(!lstRecordType.isEmpty()) {
            builderIndvRecTypeId = lstRecordType[0].Id;
        }
        
        Account acc = new Account();
            acc.RecordTypeId = builderIndvRecTypeId;
            acc.Name = 'Individual Test Acc';
            acc.PAN__c = 'AAAAA3333B';
            acc.GSTIN__c = '27AAHFG0551H1ZL';
        insert acc;
        
        List<RecordType> lstRecordType2 = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Corporate'];
        Id builderCorpRecTypeId;
        if(!lstRecordType2.isEmpty()) {
            builderCorpRecTypeId = lstRecordType2[0].Id;
        }
        
        List<RecordType> lstIndividualPBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Individual'];
        List<RecordType> lstCorporatePBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Corporate'];
        
        Account acc2 = new Account();
            acc2.RecordTypeId = builderCorpRecTypeId;
            acc2.Name = 'Test Corporate Account';
            acc2.PAN__c = 'AAAAB2222B';
            
        insert acc2;
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83');
        insert objPincode;
        
        Document_Master__c docMasObj = new Document_Master__c();
        docMasObj.Name = 'Test GSTIN - Registration certificate';
        docMasObj.Doc_Id__c = '123344';
        docMasObj.Approving_authority__c = 'Business Head';
        docMasObj.Applicable_for_APF__c = true;
        Database.insert(docMasObj);
        
        Document_type__c docTypeObj = new Document_type__c();
        docTypeObj.Name = 'Builder - APF';
        docTypeObj.Approving_authority__c    = 'Business Head';
         docTypeObj.Applicable_for_APF__c = true;	
        Database.insert(docTypeObj);
        
        APF_Document_Checklist__c objAPF = new APF_Document_Checklist__c();
        objAPF.Active_del__c = true;
        objAPF.Document_Master__c = docMasObj.Id;
        objAPF.Document_Type__c = docTypeObj.id;
        objAPF.Mandatory__c = true;
        objAPF.Valid_for_Project_Type__c = 'New';
        insert objAPF;
        
        Project__c objProject = new Project__c();
        //objProject.Assigned_Sales_User__c = assignedSalesUser != null ? assignedSalesUser.Id : null;
        objProject.Account__c = acc.Id;
        objProject.Project_Name__c = 'Test Project';
        objProject.APF_Status__c = 'APF Initiated';
        objProject.Stage__c = 'APF Initiation';
        objProject.Sub_Stage__c = 'Project Data Capture';
        objProject.APF_Type__c = 'New';
        objProject.Address_Line_1__c = 'Test Address';
        objProject.Pincode__c = objPincode.Id;
        insert objProject;
        
        Project_Builder__c objProjectBuilderCorp = new Project_Builder__c();
        objProjectBuilderCorp.RecordTypeId = lstCorporatePBId[0].Id;
        objProjectBuilderCorp.Builder_Name__c = acc2.Id;
        objProjectBuilderCorp.Constitution__c = '24';
        objProjectBuilderCorp.Builder_Status__c = 'Resident';
        objProjectBuilderCorp.Contact_Number_Mobile__c = '9999444223';
        objProjectBuilderCorp.PAN_Number__c = 'EGNPS1111T';
        objProjectBuilderCorp.Project__c = objProject.Id; 
        objProjectBuilderCorp.Preferred_Communication_Address__c = 'CURRES'; 
        //objProjectBuilderCorp. =
        Database.insert(objProjectBuilderCorp,false);
        
        acc2.GSTIN__c = '27AAHFG0551H1ZL';
        update acc2;
        
        insert objProjectBuilderCorp;
        System.debug('Debug Log for GSTIN'+objProjectBuilderCorp);
        
        
        objProjectBuilderCorp.GSTIN_Number__c = '27AAHFG0551H1ZL';
        //objProjectBuilderCorp.Builder_Role__c = Constants.strPBConstructorMarketer;
        update objProjectBuilderCorp;
        
        Set<Id> setProjectId = new Set<Id>();
        setProjectId.add(objProject.Id);
        
        
        
        List<Project_Builder__c> lstProjectBuilder = [SELECT Id, RecordTypeId, Builder_Category__c, GSTIN_Available__c, Name, Date_Of_Birth__c, Builder_Name__r.RecordType.DeveloperName, All_Promoter_Count__c, Builder_Type__c, Class_Of_Activity__c, Builder_Name__c, Builder_Name__r.RecordTypeId, Builder_Status__c, Constitution__c, Builder_Role__c, Contact_Number_Mobile__c, Email__c, Preferred_Communication_Address__c, Address_Line_1__c, Address_Line_2__c, Pincode__c, Promoter_PAN_Validation_Count__c, Promoter_Cibil_Validation_Count__c, Project__c,(Select Id, Name, Name_of_Promoter__c, Email__c, Contact_No_Mobile__c, Preferred_Communication_Address__c, Promoter_Status__c, Address_Line_1__c, Address_Line_2__c, Pincode__c from Promoters__r) From Project_Builder__c Where Project__c =: objProject.Id];
        System.debug('Debug Log for lstProjectBuilder size'+lstProjectBuilder.size());
        
        List<Promoter__c> lstPromoter = new List<Promoter__c>();
        if(!lstProjectBuilder.isEmpty()) {
            for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                objProjectBuilder.GSTIN_Available__c = true;
                objProjectBuilder.GSTIN_Number__c = '27AAHFG0551H1ZL';
                if(objProjectBuilder.Promoters__r.isEmpty() && objProjectBuilder.Builder_Name__r.RecordType.DeveloperName != 'Builder_Individual'){
                    Promoter__c objPromoter = new Promoter__c(Project_Builder__c = objProjectBuilder.Id);
                    lstPromoter.add(objPromoter);
                }
            }
            update lstProjectBuilder;
            System.debug('Debug Log for lstPromoter'+lstPromoter.size());
            if(!lstPromoter.isEmpty()) {
                insert lstPromoter;
            }
            System.debug('Debug Log for lstPromoter post insertion'+lstPromoter);
        }
        
        
        Id ProjectId = objProject.Id;
        
        
        List<Project_Document_Checklist__c> lstProjDocChecklist = [Select Id, Document_Master__c, Document_Type__c, File_Check_Completed__c, Scan_Check_Completed__c, Mandatory__c, Valid_for_Project_Type__c, Project__c, Status__c From Project_Document_Checklist__c Where Mandatory__c = true AND Project__c = : ProjectId];
        if(!lstProjDocChecklist.isEmpty()) {
            for(Project_Document_Checklist__c objPDC : lstProjDocChecklist) {
                objPDC.Status__c = Constants.strDocStatusReceived;
            }
            
            update lstProjDocChecklist;
            
        }
        
        APF_MoveToNextStageController.showWarningMsgs(ProjectId);
            APF_MoveToNextStageController.initialSubStageNotification(ProjectId);
            APF_MoveToNextStageController.MoveNextResponse objMoveNextResponse = APF_MoveToNextStageController.saveProject(ProjectId);
            System.debug('Debug Log for objMoveNextResponse From '+objMoveNextResponse);
            
            
            objProject.Stage__c = 'APF Operations Control';
            objProject.Sub_Stage__c = 'COPS : Data Maker';
            objProject.APF_Status__c = 'Operations Control';
            
            update objProject;
            
            APF_MoveToNextStageController.showWarningMsgs(ProjectId);
            APF_MoveToNextStageController.initialSubStageNotification(ProjectId);
            APF_MoveToNextStageController.MoveNextResponse objMoveNextResponseCOPS = APF_MoveToNextStageController.saveProject(ProjectId);
            System.debug('Debug Log for objMoveNextResponseCOPS From '+objMoveNextResponseCOPS);
            
            ProjectTriggerHelper.subStgChangetoCOPSDCOperations(setProjectId);
            ProjectTriggerHelper.subStgChangefromCOPSDCOperations(setProjectId);
            
            
            List<Project_Builder__c> lstProjBuilderTriggerNew = new List<Project_Builder__c>();
            
            Project_Builder__c objProjectBuilderIndNew = new Project_Builder__c(Builder_Name__c = acc.Id, Project__c = objProject.Id, Contact_Number_Mobile__c = '8887877333', PAN_Number__c = 'AAAEE9986O', Date_Of_Birth__c = Date.newInstance(1990, 9, 7));
            Project_Builder__c objProjectBuilderCorpNew = new Project_Builder__c(Builder_Name__c = acc2.Id, Project__c = objProject.Id,  Contact_Number_Mobile__c = '8777799999', PAN_Number__c = 'BBBRE1234I', Date_Of_Birth__c = Date.newInstance(1989, 9, 7));
            
            lstProjBuilderTriggerNew.add(objProjectBuilderIndNew);
            lstProjBuilderTriggerNew.add(objProjectBuilderCorpNew);
            Database.insert(lstProjBuilderTriggerNew,false);
            System.debug('>>>>>>>>>>>>>>>>>>'+lstProjBuilderTriggerNew);
            //ProjectBuilderTriggerHandler.onBeforeInsert(lstProjBuilderTriggerNew);
            
            Project_Builder__c objProjectBuilderIndNew2 = new Project_Builder__c(RecordTypeId = lstCorporatePBId[0].Id, Builder_Name__c = acc.Id, Project__c = objProject.Id, Contact_Number_Mobile__c = '8887877333', PAN_Number__c = 'AAAEE9986O', Date_Of_Birth__c = Date.newInstance(1990, 9, 7));
            Project_Builder__c objProjectBuilderCorpNew2 = new Project_Builder__c(RecordTypeId = lstIndividualPBId[0].Id,Builder_Name__c = acc2.Id, Project__c = objProject.Id,  Contact_Number_Mobile__c = '8777799999', PAN_Number__c = 'BBBRE1234I', Date_Of_Birth__c = Date.newInstance(1989, 9, 7));
            
            lstProjBuilderTriggerNew.add(objProjectBuilderIndNew2);
            lstProjBuilderTriggerNew.add(objProjectBuilderCorpNew2);
            Database.insert(lstProjBuilderTriggerNew,false);
            System.debug('>>>>>>>>>>>>>>>>>>'+lstProjBuilderTriggerNew);
            
            Set<Id> setPBIDs = new Set<Id>();
            for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                if(objProjectBuilder.RecordTypeId == lstCorporatePBId[0].Id) {
                    objProjectBuilder.Builder_Category__c = 'Cat B';
                }
                
                else {
                    objProjectBuilder.Builder_Category__c = 'Cat A';
                }
                objProjectBuilder.Builder_Role__c = Constants.strPBConstructorMarketer;
                setPBIDs.add(objProjectBuilder.Id);
            }
            
            update lstProjectBuilder;
            ProjectBuilderTriggerHelper.populateProjectCategoryonProject(setPBIDs);
            
            
            
            update lstProjectBuilder;
            ProjectBuilderTriggerHelper.populateProjectCategoryonProject(setPBIDs);
            
            for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                objProjectBuilder.Builder_Category__c = 'Cat A';
                objProjectBuilder.Builder_Role__c = Constants.strPBConstructorMarketer;
                setPBIDs.add(objProjectBuilder.Id);
            }
            
            update lstProjectBuilder;
            test.startTest();
            ProjectBuilderTriggerHelper.populateProjectCategoryonProject(setPBIDs);
            test.stopTest();
            
	}
	
	public static testMethod void PromoterAddressTriggerCheck() {
	    
        
	    List<RecordType> lstRecordType = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Individual'];
        Id builderIndvRecTypeId;
        if(!lstRecordType.isEmpty()) {
            builderIndvRecTypeId = lstRecordType[0].Id;
        }
        
        Account acc = new Account();
            acc.RecordTypeId = builderIndvRecTypeId;
            acc.Name = 'Individual Test Acc';
            acc.PAN__c = 'AAAAA3333B';
            acc.GSTIN__c = '27AAHFG0551H1ZL';
        insert acc;
        
        List<RecordType> lstRecordType2 = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Corporate'];
        Id builderCorpRecTypeId;
        if(!lstRecordType2.isEmpty()) {
            builderCorpRecTypeId = lstRecordType2[0].Id;
        }
        
        List<RecordType> lstIndividualPBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Individual'];
        List<RecordType> lstCorporatePBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Corporate'];
        
        Account acc2 = new Account();
            acc2.RecordTypeId = builderCorpRecTypeId;
            acc2.Name = 'Test Corporate Account';
            acc2.PAN__c = 'AAAAB2222B';
            
        insert acc2;
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83');
        insert objPincode;
        
        Document_Master__c docMasObj = new Document_Master__c();
        docMasObj.Name = 'Test GSTIN - Registration certificate';
        docMasObj.Doc_Id__c = '123344';
        docMasObj.Approving_authority__c = 'Business Head';
        docMasObj.Applicable_for_APF__c = true;
        Database.insert(docMasObj);
        
        Document_type__c docTypeObj = new Document_type__c();
        docTypeObj.Name = 'Builder - APF';
        docTypeObj.Approving_authority__c    = 'Business Head';
         docTypeObj.Applicable_for_APF__c = true;	
        Database.insert(docTypeObj);
        
        APF_Document_Checklist__c objAPF = new APF_Document_Checklist__c();
        objAPF.Active_del__c = true;
        objAPF.Document_Master__c = docMasObj.Id;
        objAPF.Document_Type__c = docTypeObj.id;
        objAPF.Mandatory__c = true;
        objAPF.Valid_for_Project_Type__c = 'New';
        insert objAPF;
        
        Project__c objProject = new Project__c();
        //objProject.Assigned_Sales_User__c = assignedSalesUser != null ? assignedSalesUser.Id : null;
        objProject.Account__c = acc.Id;
        objProject.Project_Name__c = 'Test Project';
        objProject.APF_Status__c = 'APF Initiated';
        objProject.Stage__c = 'APF Initiation';
        objProject.Sub_Stage__c = 'Project Data Capture';
        objProject.APF_Type__c = 'New';
        objProject.Address_Line_1__c = 'Test Address';
        objProject.Pincode__c = objPincode.Id;
        insert objProject;
        
        Project_Builder__c objProjectBuilderCorp = new Project_Builder__c();
        objProjectBuilderCorp.RecordTypeId = lstCorporatePBId[0].Id;
        objProjectBuilderCorp.Builder_Name__c = acc2.Id;
        objProjectBuilderCorp.Constitution__c = '24';
        objProjectBuilderCorp.Builder_Status__c = 'Resident';
        objProjectBuilderCorp.Contact_Number_Mobile__c = '9999444223';
        objProjectBuilderCorp.PAN_Number__c = 'EGNPS1111T';
        objProjectBuilderCorp.Project__c = objProject.Id; 
        objProjectBuilderCorp.Preferred_Communication_Address__c = 'CURRES'; 
        //objProjectBuilderCorp. =
        Database.insert(objProjectBuilderCorp,false);
        
        acc2.GSTIN__c = '27AAHFG0551H1ZL';
        update acc2;
        
        insert objProjectBuilderCorp;
        System.debug('Debug Log for GSTIN'+objProjectBuilderCorp);
        
        
        objProjectBuilderCorp.GSTIN_Number__c = '27AAHFG0551H1ZL';
        //objProjectBuilderCorp.Builder_Role__c = Constants.strPBConstructorMarketer;
        update objProjectBuilderCorp;
        
        Set<Id> setProjectId = new Set<Id>();
        setProjectId.add(objProject.Id);
        
        
        
        List<Project_Builder__c> lstProjectBuilder = [SELECT Id, RecordTypeId, Builder_Category__c, GSTIN_Available__c, Name, Date_Of_Birth__c, Builder_Name__r.RecordType.DeveloperName, All_Promoter_Count__c, Builder_Type__c, Class_Of_Activity__c, Builder_Name__c, Builder_Name__r.RecordTypeId, Builder_Status__c, Constitution__c, Builder_Role__c, Contact_Number_Mobile__c, Email__c, Preferred_Communication_Address__c, Address_Line_1__c, Address_Line_2__c, Pincode__c, Promoter_PAN_Validation_Count__c, Promoter_Cibil_Validation_Count__c, Project__c,(Select Id, Name, Name_of_Promoter__c, Email__c, Contact_No_Mobile__c, Preferred_Communication_Address__c, Promoter_Status__c, Address_Line_1__c, Address_Line_2__c, Pincode__c from Promoters__r) From Project_Builder__c Where Project__c =: objProject.Id];
        System.debug('Debug Log for lstProjectBuilder size'+lstProjectBuilder.size());
        
        List<Promoter__c> lstPromoter = new List<Promoter__c>();
        if(!lstProjectBuilder.isEmpty()) {
            for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                objProjectBuilder.GSTIN_Available__c = true;
                objProjectBuilder.GSTIN_Number__c = '27AAHFG0551H1ZL';
                if(objProjectBuilder.Promoters__r.isEmpty() && objProjectBuilder.Builder_Name__r.RecordType.DeveloperName != 'Builder_Individual'){
                    Promoter__c objPromoter = new Promoter__c(Project_Builder__c = objProjectBuilder.Id, Preferred_Communication_Address__c = 'CURRES');
                    lstPromoter.add(objPromoter);
                }
            }
            update lstProjectBuilder;
            System.debug('Debug Log for lstPromoter'+lstPromoter.size());
            if(!lstPromoter.isEmpty()) {
                insert lstPromoter;
            }
            System.debug('Debug Log for lstPromoter post insertion from Address Check Method'+lstPromoter);
            update lstPromoter;
            
            Pincode__c pin=new Pincode__c(Name='TEST',City__c='TEST',ZIP_ID__c='TEST');
    	            Database.insert(pin);
            
            Address_Detail_Promoter__c objAddressDetail = new Address_Detail_Promoter__c(Type_of_address__c = lstPromoter[0].Preferred_Communication_Address__c,
                                                                        Residence_Type__c = 'C', Pincode_LP__c = pin.Id, Address_Line_1__c = 'Test Address Line 1',
                                                                        Address_Line_2__c = 'Test Address Line 2', Promoter__c = lstPromoter[0].Id);
                                                                        
            insert objAddressDetail;
            
            objProject.RecordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeCreditApproval).getRecordTypeId();
            objProject.Stage__c = 'APF Inventory Maintenance';
            objProject.Sub_Stage__c = 'APF Inventory Update';
            objProject.APF_Status__c = 'APF Approved';
            update objProject;
            
            Tower__c objTower = new Tower__c(Project__c = objProject.Id, Name = 'Villa 1');
            insert objTower;
            
            Project_Inventory__c objProjectInventory = new Project_Inventory__c(Project__c = objProject.Id, Tower__c = objTower.Id, Flat_House_No__c = '123', 
                                                                                Floor__c = 'Second Floor', LCR_per_sq_ft__c = 100, Area__c = 500, 
                                                                                Construction_Status__c = 'Under Construction',
                                                                                Inventory_Description__c = '1',Inventory_Status__c = 'Available',
                                                                                Type_of_Inventory__c = 'Residential');
            insert objProjectInventory;
            
            objProjectInventory.Flat_House_No__c = '123B';
            update objProjectInventory;
            
            delete objProjectInventory;
            
            Credit_Deviation_Master_APF__c objCDM = new Credit_Deviation_Master_APF__c(Name = 'APF Norms Not Met', Approver_Level__c = 'CRO', Description__c = 'Test', Is_Enabled__c = true);
            insert objCDM;
            
            Applicable_Deviation_APF__c objAPFDeviation = new Applicable_Deviation_APF__c(Credit_Deviation_Master_APF__c = objCDM.Id, Project__c = objProject.Id, Remarks_Limit_255_characters__c = 'Test');
            insert objAPFDeviation;
        }
	}

}