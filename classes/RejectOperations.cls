public class RejectOperations {

    @AuraEnabled
    public static Loan_Application__c getLoanApp(Id lappId){
        system.debug('=='+lappId);
        return [SELECT Id, Name, StageName__c, Sub_Stage__c,Rejected_Cancelled_Sub_Stage__c,Rejected_Cancelled_Stage__c, OwnerId/*** Added by Saumya For Reject Revive BRD ***/, Assigned_Sales_User__c, RecordTypeID, Date_of_Rejection__c, Date_of_Cancellation__c, Rejection_Reason__c, Severity_Level__c, Remarks_Rejection__c,Previous_Sub_Stage__c,Moved_To_FCU_Review_From_Credit__c,Exempted_from_FCU_BRD__c,
                Overall_FCU_Status__c,Overall_Post_Sanction_FCU_Status__c,Reviewed_by_FCU_Manager__c,Post_Sanction_FCU_Manager_Review__c
                FROM Loan_Application__c
                WHERE Id = :lappId ]; //Previous_Sub_Stage__c,Moved_To_FCU_Review_From_Credit__c,Overall_FCU_Status__c,Overall_Post_Sanction_FCU_Status__c,Reviewed_by_FCU_Manager__c,Post_Sanction_FCU_Manager_Review__c added by Abhilekh on 23rd October 2019 for Post Sanction FCU BRD
        //Exempted_from_FCU_BRD__c added by Abhilekh on 23rd December 2019 for FCU BRD Exemption
    }

    @AuraEnabled
    public static String rejectApplication(loan_application__c la){

        String msg;
        msg='';
		//Added by Chitransh for TIL-840 on [27-09-2018]//
        if(Approval.isLocked(la) == true){
            system.debug('Debug log for Approval Pending');
            msg = 'Approval is Pending!!';
        }
        //End of patch Added by Chitransh for TIL-840 on [27-09-2018]//
        else{ //Added by Chitransh for TIL-840 on [27-09-2018]//
        Savepoint sp = Database.setSavepoint();
       
        system.debug('UserInfo.getUserId()::'+UserInfo.getUserId()+'la.OwnerId::'+la.OwnerId);
        List<Reject_Revive_Master__mdt> lstofRejectRevivemdt =[Select Id,Re_Open_Stage__c,Cancel__c,Re_Open_Sub_Stage__c,Revive_Stage__c,Revive_Sub_Stage__c,Cancelled_Reject_Stage__c,Reject__c from Reject_Revive_Master__mdt where IsActive__c=true and Cancelled_Reject_Stage__c =: la.Sub_Stage__c];// Added by Saumya For Reject Revive BRD
        if(lstofRejectRevivemdt.size() > 0 && !lstofRejectRevivemdt[0].Reject__c){ // Added by Saumya For Reject Revive BRD
             msg= 'Rejection is not allowed at this stage.';
        }
        /*else if(UserInfo.getUserId() != la.OwnerId){
            msg= 'You are not authorized for Rejection.';
        }*/
        else{ // Added by Saumya For Reject Revive BRD
			//Below if block added by Abhilekh on 25th September 2019 for FCU BRD
			if(la.Sub_Stage__c == Constants.FCU_Review){
				//Below if block only and else block added by Abhilekh on 23rd October 2019 for Post Sanction FCU BRD
				if(la.Previous_Sub_Stage__c != Constants.OTC_Receiving){
					if(((la.Previous_Sub_Stage__c == Constants.Credit_Review || la.Previous_Sub_Stage__c == Constants.Re_Credit || la.Previous_Sub_Stage__c == Constants.Re_Look) && la.Overall_FCU_Status__c == Constants.NEGATIVE && la.Reviewed_by_FCU_Manager__c == true) || (la.Previous_Sub_Stage__c == Constants.Disbursement_Maker && la.Overall_Post_Sanction_FCU_Status__c == Constants.NEGATIVE && la.Post_Sanction_FCU_Manager_Review__c == true)){
						la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Cancelled/Rejected').getRecordTypeId();
						la.ownerId = la.Assigned_Sales_User__c;
						la.Date_of_Rejection__c = System.TODAY();
						la.Rejected_Cancelled_Stage__c = la.Sub_Stage__c;
						la.StageName__c = 'Loan Rejected';
						la.Sub_Stage__c = 'Loan reject';
						la.Rejection_Reason__c = 'FCU Decline';
						la.Severity_Level__c = '2';
						if(la.Previous_Sub_Stage__c == Constants.Credit_Review || la.Previous_Sub_Stage__c == Constants.Re_Credit || la.Previous_Sub_Stage__c == Constants.Re_Look){
							la.Moved_To_FCU_Review_From_Credit__c = true;
						}
						else{
							la.Moved_To_FCU_Review_From_Credit__c = false;
						}
						msg='';
					}
					else{
						msg = ' The Overall Pre/post Sanction FCU Status should be FCU Decline and Reviewed by FCU Manager when rejecting from FCU Review sub stage.';
					}
				}
				else{
					msg = 'The application cannot be rejected here.';
				}
			}
			//Below else if block added by Abhilekh on 29th January 2020 for Hunter BRD
			else if(la.Sub_Stage__c == Constants.Hunter_Review){
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Cancelled/Rejected').getRecordTypeId();
				la.ownerId = la.Assigned_Sales_User__c;
				la.Date_of_Rejection__c = System.TODAY();
				la.Rejected_Cancelled_Stage__c = la.Sub_Stage__c;
				la.StageName__c = 'Loan Rejected';
				la.Sub_Stage__c = 'Loan reject';
				la.Rejection_Reason__c = 'Hunter Decline';
				la.Severity_Level__c = '2';
				msg='';
			}
         else if(la.Sub_Stage__c == 'Express Queue: Data Checker' || la.StageName__c == 'Credit Decisioning'  || la.Sub_Stage__c == 'Disbursement Checker' || la.Sub_Stage__c == 'Application Initiation' || /***** Added by Saumya for CIBIL *****/ la.Sub_Stage__c == 'CIBIL Reject'/***** Added by Saumya for CIBIL *****/ || (la.Sub_Stage__c == Constants.Disbursement_Maker && la.Exempted_from_FCU_BRD__c == false)) { //la.Sub_Stage__c == Constants.Disbursement_Maker added by Abhilekh on 23rd October 2019 for Post Sanction FCU BRD
			//&& la.Exempted_from_FCU_BRD__c == false added by Abhilekh on 23rd December 2019 for FCU BRD Exemption
            if(la.Rejection_Reason__c != NULL && la.Severity_Level__c != NULL && la.Remarks_Rejection__c != NULL){
                la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Cancelled/Rejected').getRecordTypeId();
                la.ownerId = la.Assigned_Sales_User__c;
                la.Date_of_Rejection__c = System.TODAY();
                la.Rejected_Cancelled_Stage__c = la.StageName__c;
                la.Rejected_Cancelled_Sub_Stage__c = la.Sub_Stage__c;
                la.StageName__c = 'Loan Rejected';
                la.Sub_Stage__c = 'Loan reject';
                //la.Rejected_Cancelled_Status__c = 'Submitted for Approval';
                msg='';
            }
            else{
                msg = 'Please fill in Rejection Reason, Severity Level and Remarks before continuing.';
            }
        }
        else{
            msg = 'You are not authorized to reject the loan application.';
        }
        System.debug('Before Updating==');
        try {
            if(msg == null || msg == ''){
                update la;
                system.debug('=========>>>' + la.recordtypeId);    
            }
            
        } catch (DMLException e) {
           // Database.rollback(sp);
            msg = e.getDmlMessage(0);
            
        } catch (Exception e) {
            //Database.rollback(sp);
            msg = e.getMessage();
                
        }
        }
		}//Added by Chitransh for TIL-840 on [27-09-2018]//												   
        return msg;
        
    }

    @AuraEnabled
    public static List<String> getPickListValuesIntoList(){
       List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Loan_Application__c.Rejection_Reason__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }
        system.debug('Picklist returned : ' + pickListValuesList);
        return pickListValuesList;
    }
    
    @AuraEnabled
    public static List<String> getPickListValuesIntoList2(){
       List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Loan_Application__c.Severity_Level__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }
        system.debug('Picklist returned : ' + pickListValuesList);
        return pickListValuesList;
    }
    
    @AuraEnabled 
    public static Map<String, List<String>> getDependentMap(sObject objDetail, string contrfieldApiName,string depfieldApiName) {
        String controllingField = contrfieldApiName.toLowerCase();
        String dependentField = depfieldApiName.toLowerCase();
        
        Map<String,List<String>> objResults = new Map<String,List<String>>();
        
        Schema.sObjectType objType = objDetail.getSObjectType();
        if (objType==null){
            return objResults;
        }
        
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
        
        if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)){
            return objResults;     
        }
        
        Schema.SObjectField theField = objFieldMap.get(dependentField);
        Schema.SObjectField ctrlField = objFieldMap.get(controllingField);
        
        List<Schema.PicklistEntry> contrEntries = ctrlField.getDescribe().getPicklistValues();
        List<PicklistEntryWrapper> depEntries = wrapPicklistEntries(theField.getDescribe().getPicklistValues());
        List<String> controllingValues = new List<String>();
        
        for (Schema.PicklistEntry ple : contrEntries) {
            String label = ple.getLabel();
            objResults.put(label, new List<String>());
            controllingValues.add(label);
        }
        
        for (PicklistEntryWrapper plew : depEntries) {
            String label = plew.label;
            String validForBits = base64ToBits(plew.validFor);
            for (Integer i = 0; i < validForBits.length(); i++) {
                String bit = validForBits.mid(i, 1);
                if (bit == '1') {
                    objResults.get(controllingValues.get(i)).add(label);
                }
            }
        }
        return objResults;
    }
    
    public static String decimalToBinary(Integer val) {
        String bits = '';
        while (val > 0) {
            Integer remainder = Math.mod(val, 2);
            val = Integer.valueOf(Math.floor(val / 2));
            bits = String.valueOf(remainder) + bits;
        }
        return bits;
    }
    
    public static String base64ToBits(String validFor) {
        if (String.isEmpty(validFor)) return '';
        
        String validForBits = '';
        
        for (Integer i = 0; i < validFor.length(); i++) {
            String thisChar = validFor.mid(i, 1);
            Integer val = base64Chars.indexOf(thisChar);
            String bits = decimalToBinary(val).leftPad(6, '0');
            validForBits += bits;
        }
        
        return validForBits;
    }
    
    private static final String base64Chars = '' +
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
        'abcdefghijklmnopqrstuvwxyz' +
        '0123456789+/';
    
    
    private static List<PicklistEntryWrapper> wrapPicklistEntries(List<Schema.PicklistEntry> PLEs) {
        return (List<PicklistEntryWrapper>)
            JSON.deserialize(JSON.serialize(PLEs), List<PicklistEntryWrapper>.class);
    }
    
    public class PicklistEntryWrapper{
        public String active {get;set;}
        public String defaultValue {get;set;}
        public String label {get;set;}
        public String value {get;set;}
        public String validFor {get;set;}
        public PicklistEntryWrapper(){            
        }
        
    }

}