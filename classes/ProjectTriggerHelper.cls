public class ProjectTriggerHelper {
    public static void queueOrIndividualAssignment(List<Project__c> lstProjectTriggerNew) {
        List<Project__c> lstQueueAssignment = new List<Project__c> ();
        List<Project__c> lstIndividualAssignment = new List<Project__c> ();
        List<Project__c> lstShareRecords = new List<Project__c> ();
        
        if(!lstProjectTriggerNew.isEmpty()) {
            for(Project__c objProject : lstProjectTriggerNew) {
                if ((objProject.Sub_Stage__c == Constants.strApfSubStgPDC) && (objProject.Stage__c == Constants.strApfStgAppInit)) {
                    if (String.isNotBlank(objProject.Assigned_Sales_User__c)) {
                        objProject.OwnerId = objProject.Assigned_Sales_User__c;
                        objProject.RecordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strApfStgAppInit).getRecordTypeId();
                    } 
                    else lstQueueAssignment.add(objProject);
                } 
                else if ((objProject.Sub_Stage__c == Constants.strApfSubStgFileChckr) && (objProject.Stage__c == Constants.strApfStgAppInit)) {
                    if (String.isNotBlank(objProject.Assigned_FileCheck_User__c)) {
                        objProject.OwnerId = objProject.Assigned_FileCheck_User__c;
                        objProject.RecordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeFileChecker).getRecordTypeId();
                    } 
                    else lstQueueAssignment.add(objProject);
                }
                else if(objProject.Sub_Stage__c == Constants.strApfSubStgScnMkr && objProject.Stage__c == Constants.strApfStgAppInit) {
                    if(String.isNotBlank(objProject.Assigned_Scan_Maker__c)) {
                        objProject.OwnerId = objProject.Assigned_Scan_Maker__c;
                        objProject.RecordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeFileChecker).getRecordTypeId();
                    } 
                    else lstQueueAssignment.add(objProject);
                }
                else if(objProject.Sub_Stage__c == Constants.strApfSubStgScnChkr && objProject.Stage__c == Constants.strApfStgAppInit) {
                    if (String.isNotBlank(objProject.Assigned_Scan_Checker__c)) {
                        objProject.OwnerId = objProject.Assigned_Scan_Checker__c;
                        objProject.RecordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeFileChecker).getRecordTypeId();
                    }
                    else lstQueueAssignment.add(objProject);
                }
                else if ((objProject.Sub_Stage__c == Constants.strApfSubStgCOPSDataMaker) && (objProject.Stage__c == Constants.strApfStgOpsControl)) {
                    if (String.isNotBlank(objProject.Assigned_Cops_dataMaker__c)) {
                        objProject.OwnerId = objProject.Assigned_Cops_dataMaker__c;
                        objProject.RecordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeCOPSDM).getRecordTypeId();
                    } 
                    else {
                        lstQueueAssignment.add(objProject);
                    }
                } 
                else if ((objProject.Sub_Stage__c == Constants.strApfSubStgCOPSDataChecker) && (objProject.Stage__c == Constants.strApfStgOpsControl)) {
                    if (String.isNotBlank(objProject.Assigned_Cops_dataChecker__c)) {
                        objProject.OwnerId = objProject.Assigned_Cops_dataChecker__c;
                        objProject.RecordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeCOPSDC).getRecordTypeId();
                    } 
                    else {
                        lstQueueAssignment.add(objProject);
                    }
                }
                else if(objProject.Sub_Stage__c == Constants.strApfSubStgCredReview && objProject.Stage__c == Constants.strApfStgCredReview) {  
                    if (String.isNotBlank(objProject.Assigned_Credit_Review__c )) {
                        objProject.OwnerId = objProject.Assigned_Credit_Review__c ;
                        objProject.RecordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeCreditReview).getRecordTypeId();
                    } 
                    else {
                        lstQueueAssignment.add(objProject);
                    }
                } 
                else if(objProject.Sub_Stage__c == Constants.strApfSubStgApprovalComm && objProject.Stage__c == Constants.strApfStgApprovalComm) {  
                    if (String.isNotBlank(objProject.Assigned_Sales_User__c )) {
                        objProject.OwnerId = objProject.Assigned_Sales_User__c ;
                        objProject.RecordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeAppComm).getRecordTypeId();
                    } 
                    else {
                        lstQueueAssignment.add(objProject);
                    }
                } 
                else if(objProject.Stage__c == Constants.strApfStgInvMaintenance) { 
                    if (String.isNotBlank(objProject.Assigned_Credit_Approver__c )) {
                        objProject.OwnerId = objProject.Assigned_Credit_Approver__c ;
                        objProject.RecordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeCreditApproval).getRecordTypeId();
                    } 
                    else {
                        lstQueueAssignment.add(objProject);
                    }
                } 
                
            }
            if(!lstQueueAssignment.isEmpty()) {
                System.debug('Debug Log for lstQueueAssignment'+lstQueueAssignment.size());
                System.debug('Debug Log for lstQueueAssignment'+lstQueueAssignment);
                queueBaseAssignment(lstQueueAssignment);
            }
        }
    }
    
    /*
    * @Description :- This method assigns the owner to a queue based on the sub stages of the APF Application
    * @param :- trigger.new
    * @returns :- void
    */
    public static void queueBaseAssignment(List<Project__c> lstProjectTriggerNew){
        Set<string> assignedQueue = new Set<string>();
        Set<String> setOfBranches = new Set<String>();
        Set<string> setOfStages = new Set<string>();
        Set<string> setOfSubStage = new Set<string>();
        list<id> ProjectListCred = new list<id>();
        map<string,QueueSObject> nameToQueRecMap = new map<string,QueueSObject>();
        Id projectRecTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeReadOnly).getRecordTypeId();
        if(!lstProjectTriggerNew.isEmpty()) {
            for(Project__c objProject : lstProjectTriggerNew) {
                if(objProject.sub_stage__c == Constants.strApfSubStgCredReview || objProject.sub_stage__c == Constants.strApfSubStgCOPSDataChecker){
                    setOfBranches.add('All');
                    setOfBranches.add(objProject.Branch_Name__c);
                    ProjectListCred.add(objProject.id);
                }
                else {
                    setOfBranches.add(objProject.Branch_Name__c);
                }
                setOfStages.add(objProject.Stage__c);
                setOfSubStage.add(objProject.sub_stage__c);
            }
            if(!setOfStages.isEmpty() && !setOfSubStage.isEmpty()) {
                System.debug('Debug Log for setOfStages'+setOfStages);
                System.debug('Debug Log for setOfSubStage'+setOfSubStage);
                List<Branch_Based_Queue_Assignment_APF__mdt> metaData = [select id, Queue_Assigned__c,
                                                                         stage__c, sub_stage__c, branch__c,Is_Enabled__c 
                                                                         from Branch_Based_Queue_Assignment_APF__mdt WHERE Branch__c IN :setOfBranches and 
                                                                         stage__c in :setOfStages and sub_stage__c in :setOfSubStage AND Is_Enabled__c = true];
                if(!metaData.isEmpty()) {
                    System.debug('Debug Log for metaData'+metaData);
                    for(Branch_Based_Queue_Assignment_APF__mdt queueData :metaData){
                        assignedQueue.add(queueData.Queue_Assigned__c);
                    } 
                    if(!assignedQueue.isEmpty()) {
                        System.debug('Debug Log for assignedQueue'+assignedQueue);
                        List<QueueSObject> queueList = [Select Queue.Id, Queue.Name, Queue.Type
                                                        from QueueSObject WHERE Queue.Type ='Queue' 
                                                        and Queue.Name in :assignedQueue];
                        if(!queueList.isEmpty()) {
                            System.debug('Debug Log for queueList'+queueList);
                            for(QueueSObject que : queueList){
                                if(!nameToQueRecMap.containsKey(que.Queue.Name)){
                                    nameToQueRecMap.put(que.queue.Name,que);                    
                                }else{
                                    nameToQueRecMap.get(que.Queue.Name);
                                }
                            }
                            if(!nameToQueRecMap.isEmpty()) {
                                System.debug('Debug Log for nameToQueRecMap'+nameToQueRecMap);
                                for(Project__c objProject : lstProjectTriggerNew){
                                    system.debug('==meta=='+metaData);
                                    for(Branch_Based_Queue_Assignment_APF__mdt mda : metaData){
                                        if(mda.sub_stage__c == objProject.sub_stage__c && mda.stage__c == objProject.stage__c && 
                                           (mda.branch__c == objProject.Branch_Name__c|| mda.branch__c == 'All')&&  objProject.sub_stage__c !=Constants.strApfSubStgCredReview){
                                               system.debug('==mda.Queue_Assigned__c=='+mda.Queue_Assigned__c);
                                               
                                               system.debug('==nameToQueRecMap.containsKey(mda.Queue_Assigned__c)=='+nameToQueRecMap.containsKey(mda.Queue_Assigned__c));
                                               if(nameToQueRecMap.containsKey(mda.Queue_Assigned__c)){
                                                   objProject.ownerId = nameToQueRecMap.get(mda.Queue_Assigned__c).Queue.id;
                                                   objProject.recordTypeId = projectRecTypeId;
                                                   system.debug('=hre here='+mda.Queue_Assigned__c);
                                                   
                                               } 
                                           } 
                                        else if(mda.sub_stage__c == objProject.sub_stage__c && mda.stage__c == objProject.stage__c && 
                                                (mda.branch__c == objProject.Branch_Name__c|| mda.branch__c == 'All')&&  objProject.sub_stage__c ==Constants.strApfSubStgCredReview){
                                            
                                            system.debug('==new=='+nameToQueRecMap+ '===='+mda.Queue_Assigned__c);
                                            
                                            if(mda.branch__c != 'All'){
                                                objProject.ownerId = nameToQueRecMap.get(mda.Queue_Assigned__c).Queue.id;
                                            }
                                            objProject.recordTypeId = projectRecTypeId;
                                            system.debug('=hre here='+mda.Queue_Assigned__c);
                                        }
                                    }
                                    
                                }
                            }
                        }
                        
                    }
                }
                
            }
        }
    }
    
    /*
    * @Description :- This method maps the pincode to branch in opp
    * @param :- Trigger.New
    * @returns :- void
    */
    public static void updateBranch(List<Project__c> newList) {
        
        id currentUser = UserInfo.getUserid();
        List<User_Branch_Mapping__c> usrRec = [select id, name, Servicing_Branch__c,Servicing_Branch__r.name, User__c from user_branch_mapping__c 
                                               where user__c =: currentUser];
        for(Project__c loa : newList){
            if(!usrRec.isEmpty()){
                if(loa.ownerid == currentUser){
                    for(User_branch_mapping__c usr : usrRec) {
                        loa.Branch_Name__c = usr.Servicing_Branch__r.Name;
                        loa.Branch__c = usr.Servicing_Branch__c;
                    }
                }
            }
        }
    }
    
    public static void subStgChangetoCOPSDCOperations(Set<Id> setProjectsatCOPSDC) {
        List<Promoter__c> lstPromoter = new List<Promoter__c>();
        if(!setProjectsatCOPSDC.isEmpty()) {
            List<Project_Builder__c> lstProjectBuilderwithPromoters = [Select Id, RecordTypeId, RecordType.DeveloperName, Name, Project__c, (Select Id, RecordTypeId, RecordType.DeveloperName, Name From Promoters__r) From Project_Builder__c Where Project__c IN: setProjectsatCOPSDC];
            System.debug('Debug Log for lstProjectBuilderwithPromoters'+lstProjectBuilderwithPromoters.size());
            if(!lstProjectBuilderwithPromoters.isEmpty()) {
                for(Project_Builder__c objPB : lstProjectBuilderwithPromoters) {
                    if(objPB.RecordType.DeveloperName == 'Builder_Corporate') {
                        objPB.RecordTypeId = Schema.SObjectType.Project_Builder__c.getRecordTypeInfosByName().get(Constants.strPBRecTypeReadOnlyCorp).getRecordTypeId();
                    }
                    else if(objPB.RecordType.DeveloperName == 'Builder_Individual') {
                        objPB.RecordTypeId = Schema.SObjectType.Project_Builder__c.getRecordTypeInfosByName().get(Constants.strPBRecTypeReadOnlyIndv).getRecordTypeId();
                    }
                    if(!objPB.Promoters__r.isEmpty()) {
                        for(Promoter__c objPromoter : objPB.Promoters__r) {
                            objPromoter.RecordTypeId = Schema.SObjectType.Promoter__c.getRecordTypeInfosByName().get('Read Only').getRecordTypeId();
                            lstPromoter.add(objPromoter);
                        }
                    }
                }
                //Database.update(lstProjectBuilderwithPromoters, false);
                update lstProjectBuilderwithPromoters;
                if(!lstPromoter.isEmpty()) {
                   update lstPromoter;
                }
            }
        }
    }
    
    public static void subStgChangefromCOPSDCOperations(Set<Id> setProjectsMovedfromCOPSDC) {
        List<Promoter__c> lstPromoter = new List<Promoter__c>();
        if(!setProjectsMovedfromCOPSDC.isEmpty()) {
            List<Project_Builder__c> lstProjectBuilderwithPromoters = [Select Id, RecordTypeId, RecordType.DeveloperName, Name, Project__c, (Select Id, RecordTypeId, RecordType.DeveloperName, Name From Promoters__r) From Project_Builder__c Where Project__c IN: setProjectsMovedfromCOPSDC];
            System.debug('Debug Log for lstProjectBuilderwithPromoters'+lstProjectBuilderwithPromoters.size());
            if(!lstProjectBuilderwithPromoters.isEmpty()) {
                for(Project_Builder__c objPB : lstProjectBuilderwithPromoters) {
                    if(objPB.RecordType.DeveloperName == 'Read_Only_Corporate') {
                        objPB.RecordTypeId = Schema.SObjectType.Project_Builder__c.getRecordTypeInfosByName().get(Constants.strPBRecTypeCorp).getRecordTypeId();
                    }
                    else if(objPB.RecordType.DeveloperName == 'Read_Only_Individual') {
                        objPB.RecordTypeId = Schema.SObjectType.Project_Builder__c.getRecordTypeInfosByName().get(Constants.strPBRecTypeIndv).getRecordTypeId();
                    }
                    if(!objPB.Promoters__r.isEmpty()) {
                        for(Promoter__c objPromoter : objPB.Promoters__r) {
                            objPromoter.RecordTypeId = Schema.SObjectType.Promoter__c.getRecordTypeInfosByName().get('Promoter').getRecordTypeId();
                            lstPromoter.add(objPromoter);
                        }
                    }
                }
                //Database.update(lstProjectBuilderwithPromoters, false);
                update lstProjectBuilderwithPromoters;
                if(!lstPromoter.isEmpty()) {
                    update lstPromoter;
                }
            }
        }
    }
    
    public static void populateProjectCategoryonProject(Set<Id> setProjectIds) {
        List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
        if(!setProjectIds.isEmpty()) {
            System.debug('Debug Log for setProjectIds'+setProjectIds);
            List<Project__c> lstProject = [Select Id, Name, Project_Category__c,(Select Id, Name, Builder_Category__c, Builder_Role__c From Project_Builders__r Where Builder_Role__c =: Constants.strPBConstructorMarketer) From Project__c WHERE ID IN: setProjectIds];
            if(!lstProject.isEmpty()) {
                System.debug('Debug Log for lstProject'+lstProject);
                for(Project__c objProject : lstProject) {
                    if(!objProject.Project_Builders__r.isEmpty()) {
                        if(objProject.Project_Builders__r.size() == 1) {
                            objProject.Project_Category__c = objProject.Project_Builders__r[0].Builder_Category__c;
                        }
                        else {
                            Integer countCatABuilderCategory = 0;
                            Integer countCatBBuilderCategory = 0;
                            Integer countCatCBuilderCategory = 0;
                            for(Project_Builder__c objProjectBuilder : objProject.Project_Builders__r) {
                                if(objProjectBuilder.Builder_Category__c == Constants.strPBCategoryCatA) {
                                    countCatABuilderCategory++;
                                }
                                else if(objProjectBuilder.Builder_Category__c == Constants.strPBCategoryCatB) {
                                    countCatBBuilderCategory++;
                                }
                                else if(objProjectBuilder.Builder_Category__c == Constants.strPBCategoryCatC) {
                                    countCatCBuilderCategory++;
                                }
                            }
                            System.debug('Debug Log for countCatABuilderCategory'+countCatABuilderCategory);
                            System.debug('Debug Log for countCatBBuilderCategory'+countCatBBuilderCategory);
                            System.debug('Debug Log for countCatCBuilderCategory'+countCatCBuilderCategory);
                            
                            if(countCatCBuilderCategory > 0) {
                                objProject.Project_Category__c = Constants.strPBCategoryCatC;
                            }
                            else if(countCatCBuilderCategory == 0 && countCatBBuilderCategory > 0) {
                                objProject.Project_Category__c = Constants.strPBCategoryCatB;
                            }
                            else if(countCatCBuilderCategory == 0 && countCatBBuilderCategory == 0 && countCatABuilderCategory > 0){
                                objProject.Project_Category__c = Constants.strPBCategoryCatA;
                            }
                        }
                    }
                }
            }
        }
    }
     /*
    * @Description :- This method assigns the Third Party Verification records
    * @param :- trigger.new
    * @returns :- void
    */
    public static void modifyTPVStatus(Set<Id> setProjectIds) {
        System.debug('In here for Status updation of TPVs');
        List<Third_Party_Verification_APF__c> lstToUpdateTPV = new List<Third_Party_Verification_APF__c>();
        List<Third_Party_Verification_APF__c> lstAPFTPVs = [Select Id, Name from Third_Party_Verification_APF__c where Status__c='Assigned' and Project__c IN: setProjectIds/* and (RecordType.Name='Legal' or RecordType.Name='Technical')*/];
        if(lstAPFTPVs.size() >0){
            for(Third_Party_Verification_APF__c obj: lstAPFTPVs){
                obj.Status__c='Initiated';
                lstToUpdateTPV.add(obj);
            }
        }
        System.debug('I am in Re-Credit Movement'+lstToUpdateTPV.size());
        if(lstToUpdateTPV.size() >0){
            update lstToUpdateTPV;
        }
    }
}