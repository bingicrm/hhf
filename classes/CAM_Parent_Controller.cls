public class CAM_Parent_Controller {
    @AuraEnabled
    public static boolean getLoanEligibility(Id recId){
        Loan_Application__c la = [Select Id, Scheme__r.Eligible_For_BRE2__c,BRE_2_Eligibility__c, CreatedDate, Income_Program_Type__c from Loan_Application__c where Id =: recId];
        return Utility.checkEligibilityForBRE2(la);
    }
}