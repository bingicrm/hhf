@isTest
public class addressController_Test {
    public static testMethod void methodTest()
    {
        Scheme__c sch=new Scheme__c();
        sch.Product_Code__c='HLAP'; 
        sch.Scheme_Code__c='ELAP';
        sch.Scheme_Group_ID__c='Test';
        insert sch;
        
        Scheme__c sch1=new Scheme__c();
        sch1.Product_Code__c='HL'; 
        sch1.Scheme_Code__c='EL';
        sch1.Scheme_Group_ID__c='Test1';
        insert sch1;
        
        Insurance_Loan_Application__mdt mdt=new Insurance_Loan_Application__mdt();
        mdt.Language='en_US';
        mdt.Label='Insurance Loan Application';
        mdt.Insurance_Application_Check__c=false;
        
        Account acc=new Account();
        acc.CountryCode__c='+91';
        acc.Email__c='puser001@amamama.com';
        acc.Gender__c='M';
        acc.Mobile__c='9800765432';
        acc.Phone__c='9800765434';
        //acc.Rejection_Reason__c='Rejected';
        acc.Name='Test Account';
        // acc.Type=;
        insert acc;
        
        Loan_Application__c app=new Loan_Application__c();
        app.Branch__c='Test';
        app.Cancellation_Reason__c='Cancelled';
        app.Approved_ROI__c=2;
        app.Approval_Taken__c=true;
        app.Scheme__c=sch.id;
        app.STP__c='N';
        app.Insurance_Loan_Application__c=false;
        app.Processing_Fee_Percentage__c=0;
        //app.Region__c='UTTAR PRADESH';
        app.StageName__c='Credit Decisioning';
        insert app;
        
        Loan_Application__c app1=new Loan_Application__c();
        app1.Branch__c='Test';
        app1.Cancellation_Reason__c='Cancelled';
        app1.Approved_ROI__c=2;
        app1.Approval_Taken__c=true;
        app1.Scheme__c=sch1.id;
        app1.STP__c='N';
        app1.Insurance_Loan_Application__c=false;
        app1.Processing_Fee_Percentage__c=0;
        //app.Region__c='UTTAR PRADESH';
        app1.StageName__c='Credit Decisioning';
        insert app1;
        
        Loan_Contact__c loan=new Loan_Contact__c();
        //loan.Age__c= 20;
        loan.Date_Of_Birth__c= Date.newInstance(2012, 12, 9);
        loan.Email__c='puser001@amamama.com';
        loan.Fax_Number__c= '8970090';
        loan.Gender__c='M';
        loan.Customer__c=acc.id;
        loan.Loan_Applications__c= app.id;
        insert loan;
        
        Pincode__c pin=new Pincode__c();
        pin.Active__c=true;
        pin.City__c='JAIPUR';
        pin.ZIP_ID__c='909090';
        insert pin;
        
        Address__c obj=new Address__c();
        obj.Address_Line_1__c='TILAK NAGAR WEST';
        obj.Loan_Contact__c= loan.id;
        obj.Type_of_address__c='CUROFF';
        obj.Pincode_LP__c=pin.id;
        insert obj;
        
        Test.startTest();
        addressController testCon =new addressController();
        list<addressController.DataWrapper> wrapAdd = addressController.getAddresses(loan.id);
        //List<Address__c> lstOfWrapper = new List<Address__c>();
        String JSONString = JSON.serialize(wrapAdd);
        addressController.addRowToTable(JSONString);
        //addressController.deleteRowFromTable(JSONString,1);
        addressController.editAddress(JSONString,0);
        String JSONString1 = JSON.serialize(wrapAdd[0]);
        addressController.saveAddress(JSONString1,1, loan.id);
        Test.stopTest();
    }
    
    public static testMethod void methodTest1()
    {
        Scheme__c sch=new Scheme__c();
        sch.Product_Code__c='HLAP'; 
        sch.Scheme_Code__c='ELAP';
        sch.Scheme_Group_ID__c='Test';
        insert sch;
        
        Loan_Application__c app=new Loan_Application__c();
        app.Branch__c='Test';
        app.Cancellation_Reason__c='Cancelled';
        app.Approved_ROI__c=2;
        app.Approval_Taken__c=true;
        app.Scheme__c=sch.id;
        app.STP__c='N';
        app.Insurance_Loan_Application__c=false;
        app.Processing_Fee_Percentage__c=0;
        //app.Region__c='UTTAR PRADESH';
        app.StageName__c='Credit Decisioning';
        insert app;
        
        Account acc=new Account();
        acc.CountryCode__c='+91';
        acc.Email__c='puser001@amamama.com';
        acc.Gender__c='M';
        acc.Mobile__c='9800765432';
        acc.Phone__c='9800765434';
        //acc.Rejection_Reason__c='Rejected';
        acc.Name='Test Account';
        // acc.Type=;
        insert acc;
        
         Loan_Contact__c loan=new Loan_Contact__c();
        //loan.Age__c= 20;
        loan.Date_Of_Birth__c= Date.newInstance(2012, 12, 9);
        loan.Email__c='puser001@amamama.com';
        loan.Fax_Number__c= '8970090';
        loan.Gender__c='M';
        loan.Customer__c=acc.id;
        loan.Loan_Applications__c= app.id;
        insert loan;
        
        Test.startTest();
        addressController.saveAddress(null, 1, loan.id);
        Test.stopTest();
    }
}