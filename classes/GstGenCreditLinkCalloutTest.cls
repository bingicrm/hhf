@isTest
public class GstGenCreditLinkCalloutTest {
    @testSetup 
    static void setup() {
        /*List<RecordType> lstRecordType = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Individual'];
        Id individualRecTypeId;
        if(!lstRecordType.isEmpty()) {
            individualRecTypeId = lstRecordType[0].Id;
        }
        Account acc = new Account();
        acc.RecordTypeId = individualRecTypeId;
        acc.Name = 'Individual Test Acc';
        acc.PAN__c = 'AAAAA3333B';
        acc.GSTIN__c = '27AAHFG0551H1ZL';
        insert acc;  */
        
        
        
        //Added by Amit Agarwal
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',
                                  Name='Test Account',
                                  Date_of_Incorporation__c=System.today(),
                                  Phone='9934567890',
                                  RecordTypeId=RecordTypeIdAccount);
        insert acc;  
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',
                                   Salutation='Mr',
                                   FirstName='Test',
                                   LastName='Test Account1',
                                   Date_of_Incorporation__c=System.today(),
                                   Phone='9934567891',RecordTypeId=RecordTypeIdAccount1);
        insert acc1;
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83123');
        insert objPincode;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',
                                            Scheme_Code__c='9934567890',
                                            Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,
                                                                         Scheme__c=objScheme.Id,
                                                                         Transaction_type__c='PB',
                                                                         Requested_Amount__c=100000,
                                                                         Loan_Purpose__c='11');
        insert objLoanApplication;
        
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Guarantor').getRecordTypeId();
        
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,
                                                             Customer__c=acc1.Id,
                                                             Applicant_Type__c='Guarantor',
                                                             Borrower__c='1',
                                                             RecordTypeId=RecordTypeIdLoanContact,Mobile__c='9876543210',
                                                             Email__c='testemail@gmail.com',Relationship_with_Applicant__c='FTH',
                                                             Category__c='1');
        insert objLoanContact;
        Id recordTypePAN = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('GSTIN').getRecordTypeId();
        
        Customer_Integration__c objCustIntegration = new Customer_Integration__c(Loan_Application__c=objLoanApplication.Id,
                                                                                 Mobile__c='9876543210',
                                                                                 Constitution__c= 'ABCD',
                                                                                 GST_Tax_payer_type__c = 'TaxPayerType',
                                                                                 Current_Status_of_Registration_Under_GST__c = 'CurrentStatus',
                                                                                 VAT_Registration_Number__c = '27AAHFG0551H1234',
                                                                                 PAN_Number__c = 'BCEST2489G',
                                                                                 Trade_Name__c = 'Trade Name',
                                                                                 Legal_Name__c ='Legal Name',
                                                                                 Filling_Frequency__c = '23',
                                                                                 Business_Activity__c = 'Business Activity',
                                                                                 GST_Date_Of_Registration__c =Date.newInstance(2018, 12, 10),
                                                                                 Cumulative_Sales_Turnover__c =5000000,
                                                                                 Month_Count__c = 50,
                                                                                 GSTIN__c ='27AAHFG0551H1ZL', 
                                                                                 Email__c='testemail@gmail.com',
                                                                                 RecordTypeId=recordTypePAN,
                                                                                 Loan_Contact__c=objLoanContact.Id,                                                                          
                                                                                 PerfiosITRReport_Error_Response__c='test1',
                                                                                 PerfiosBankReport_Error_Response__c='test2',
                                                                                 PerfiosFinancialReport_Error_Response__c='test3');
        insert objCustIntegration;
        
        List<Customer_Integration__c> lstCustIntegration = new List<Customer_Integration__c>();
        lstCustIntegration.add(objCustIntegration);
    }
    
    private static testMethod void test1() {
        List<Customer_Integration__c> lstCustIntegration = [Select Id, Name, RecordTypeId, Mobile__c,Loan_Contact__c, Email__c, GSTIN__c From Customer_Integration__c Where RecordTypeId =: Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('GSTIN').getRecordTypeId()];
       
        GstGenCreditLinkCallout gclcobj = new GstGenCreditLinkCallout(lstCustIntegration);
        //GstReturnStatusCallout objGstReturnStatusCallout = new GstReturnStatusCallout(lstCustIntegration);
        
        GstGenCreditLinkResponseBody objGstGenCreditLinkResponseBody = new GstGenCreditLinkResponseBody();
        
        GstGenCreditLinkResponseBody.cls_result objResult = new GstGenCreditLinkResponseBody.cls_result();
        objResult.weblink = 'https://gst.karza.in/gstn-cred/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjozNTkwMTksImNsaWVudF9pZCI6Imhlcm9maW5fYXBpX3Byb2QiLCJjbGllbnRfbmFtZSI6IkhFUk8gRklOQ09SUCIsInJlZl9pZCI6IjU0MTM1NDEzIiwiZXhwIjoxNTUwMDUwMTI4LjQxNDcwNiwicmVxdWVzdF9pZCI6IjcyMzA0MTU0LTI0NzEtMTFlOS1hYzA2LTBiMDQ5YTEzNmUyOSIsImdzdGluIjoiMDJBQUFDUjUwNTVLMVpKIn0.kHqtGLgjkHT-v7968bD80R7octq0w-XAR3edZfPp9_c';
        objGstGenCreditLinkResponseBody.result = objResult;
        objGstGenCreditLinkResponseBody.requestId = '71c0b669-2471-11e9-b70c-4719335f1e77';
        objGstGenCreditLinkResponseBody.statusCode = 101;
        
        Test.startTest();
        TestMockRequest req1=new TestMockRequest(200,'OK',JSON.serialize(objGstGenCreditLinkResponseBody),null);
        Test.setMock(HttpCalloutMock.class, req1);
        System.enqueueJob(gclcobj);
        Test.stopTest();   
    }
}