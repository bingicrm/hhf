public class CurrentUserAssignment {
//id record1 = Schema.SObjectType.loan_application__c.getRecordTypeInfosByName().get(Constants.COPS_Data_Checker_rt).getRecordTypeId();
   // system.
    
     @AuraEnabled
    public static loan_application__c setOwner(id oppId) {
        id curUser = Userinfo.getUserid();
        String ownerType; // Added by Shashikant - As a part of Loan Downsizing.
		system.debug('==oppid=='+oppId);
        loan_application__c la = new loan_application__c();
        la = [select OwnerId,recordTypeId, StageName__c, Sub_Stage__c,Assigned_Sales_User__c,Assigned_FileCheck_User__c,Assigned_Scan_DataMaker__c,Assigned_scan_DataChecker__c,Assigned_cops_CreditOperation__c,Assigned_Cops_dataChecker__c,Assigned_Cops_dataMaker__c,Credit_Manager_User__c,Assigned_Docket_Checker__c,Assigned_Scan_Maker__c,Assigned_Scan_Checker__c,assigned_handsight__c,Assigned_Disbursement_maker__c,Assigned_Disbursement_Checker__c,Assigned_OTC_User__c,Assigned_CPC_Data_Maker__c,Assigned_CPC_Scan_Maker__c,Assigned_CPC_Scan_Checker__c,Assigned_Downsizing_Checker__c,Assigned_Downsizing_Scan_Checker__c,Assigned_Downsizing_Scan_Maker__c,Assigned_Downsizing_Review__c,Assigned_Downsizing_Authorization__c,Owner.Type from loan_application__c where id = :oppId];
        system.debug('==la=='+la.OwnerId);
        List<GroupMember> userID = [select UserOrGroupId from GroupMember where GroupId = :la.ownerId]; 
        system.debug('==userID==');
        system.debug('==userID=='+userID);
        list<id> userEx = new list<id>();
        for(GroupMember us:userID){
			userEx.add(us.UserOrGroupId);

        }
        system.debug('====userEx==='+userEx);
        system.debug('====curUser==='+curUser);
        if(userEx.contains(curUser)){
            la.OwnerId = curUser;
			ownerType = la.Owner.Type; // Added by Shashikant - Loan-Downsizing
			if((la.Sub_Stage__c == Constants.Application_Initiation)&&(la.Assigned_Sales_User__c == null)){
				la.Assigned_Sales_User__c = curUser;
			}else if ((la.Sub_Stage__c == Constants.Express_Queue_Data_Maker)){
			   // la.Assigned_EQ_Data_Maker__c = curUser;
			   // Modified this line by Vaishali for BRE
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.Express_Queue_Data_Maker_Record).getRecordTypeId();
				//Schema.SObjectType.loan_application__c.getRecordTypeInfosByName().get('Data Entry').getRecordTypeId();
				
			} else if ((la.Sub_Stage__c == Constants.Express_Queue_Data_Checker)){
				//la.Assigned_EQ_Data_Checker__c = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.COPS_DATA_CHECKER_RECORD).getRecordTypeId();
			} else if ((la.Sub_Stage__c == Constants.File_Check)&&(la.Assigned_FileCheck_User__c == null)){
				la.Assigned_FileCheck_User__c = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.FILE_CHECK_RECORD).getRecordTypeId();
			} else if ((la.Sub_Stage__c == Constants.Scan_Data_Maker)&&(la.Assigned_Scan_DataMaker__c == null)){
				la.Assigned_Scan_DataMaker__c = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.FILE_CHECK_RECORD).getRecordTypeId();
			} else if ((la.Sub_Stage__c == Constants.Scan_Data_Checker)&&(la.Assigned_scan_DataChecker__c == null)){
				la.Assigned_scan_DataChecker__c = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.FILE_CHECK_RECORD).getRecordTypeId();
			}else if ((la.Sub_Stage__c == Constants.Document_collection)&&(la.Assigned_Sales_User__c == null)){
			   // la.Assigned_scan_DataChecker__c = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.DOCUMENT_COLLECTION_RECORD).getRecordTypeId();
			
			 }else if ((la.Sub_Stage__c == Constants.COPS_Credit_Operations_Entry)&&(la.Assigned_cops_CreditOperation__c == null)){
				la.Assigned_cops_CreditOperation__c = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.COPS_CREDIT_OPERATION_RECORD).getRecordTypeId();
			 }else if ((la.Sub_Stage__c == Constants.COPS_Data_Checker)&&(la.Assigned_Cops_dataChecker__c == null)){
				la.Assigned_Cops_dataChecker__c = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.COPS_DATA_CHECKER_RECORD).getRecordTypeId();
			 }else if ((la.Sub_Stage__c == Constants.COPS_Data_Maker)&&(la.Assigned_Cops_dataMaker__c == null)){
				la.Assigned_Cops_dataMaker__c = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.COPS_DATA_MAKER_RECORD).getRecordTypeId();
		  }else if(la.Sub_Stage__c == Constants.Credit_Review )
			{    
				la.Credit_Manager_User__c  = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.Credit_Review_and_Decision).getRecordTypeId();
			}else if(la.Sub_Stage__c == Constants.Docket_Checker )
			{    
				la.Assigned_Docket_Checker__c  = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.Loan_Disbursal).getRecordTypeId();
			}else if(la.Sub_Stage__c == Constants.Scan_Maker )
			{    
				la.Assigned_Scan_Maker__c  = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.Loan_Disbursal).getRecordTypeId();
			}else if(la.Sub_Stage__c == Constants.Scan_Checker )
			{    
				la.Assigned_Scan_Checker__c  = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.Loan_Disbursal).getRecordTypeId();
			}
			 else if(la.Sub_Stage__c == Constants.Hand_Sight )
			{    
				la.assigned_handsight__c  = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.Loan_Disbursal).getRecordTypeId();
			}
			else if(la.Sub_Stage__c == Constants.Disbursement_Maker )
			{    
				la.Assigned_Disbursement_maker__c  = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.Loan_Disbursal).getRecordTypeId();
			}
			else if(la.Sub_Stage__c == Constants.Disbursement_Checker )
			{    
				la.Assigned_Disbursement_Checker__c  = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.Disbursement_Checker).getRecordTypeId();
			}
			/***** Added by Saumya For CIBIL on new Cutomer Create ****/
			else if(la.Sub_Stage__c == constants.CIBIL_Reject )
			{    
				la.New_CIBIL_Reject_User__c  = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.CIBIL_Reject_RECORD).getRecordTypeId();
			}
			/***** Added by Saumya For CIBIL on new Cutomer Create ****/
			  else if(la.Sub_Stage__c == Constants.OTC_Receiving )
			{    
				//la.Credit_Manager_User__c  = curUser;//Commented by Chitransh for TIL-1479
				la.Assigned_OTC_User__c = curUser;//Added by Chitransh for TIL-1479
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.OTC_Receiving).getRecordTypeId();
			}
			 else if(la.Sub_Stage__c == Constants.CPC_Data_Maker )
			{    
				la.Assigned_CPC_Data_Maker__c  = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.Disbursement_Checker).getRecordTypeId();
			}
			else if(la.Sub_Stage__c == Constants.CPC_Scan_Maker )
			{    
				la.Assigned_CPC_Scan_Maker__c  = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.Disbursement_Checker).getRecordTypeId();
			}else if(la.Sub_Stage__c == Constants.CPC_Scan_Checker )
			{    
				la.Assigned_CPC_Scan_Checker__c  = curUser;
				la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.Disbursement_Checker).getRecordTypeId();
			}
			if(la.StageName__c=='Loan Downsizing')
            {
                system.debug('==Calling Downsizing==');
				la = setRecordTypeAndStoreAssignedQueueUser(la, ownerType);    
            }
			
			system.debug('===la=='+la);
			update la;
			return la;
		} else return null;
	}
	//Added by Shashikant - as part of Loan-Downsizing...
    public static Loan_Application__c setRecordTypeAndStoreAssignedQueueUser(Loan_Application__c la, String ownerType)
    {
        if(la.StageName__c=='Loan Downsizing')
        {        
            Id recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Downsizing').getRecordTypeId();
            la.RecordTypeId=recordTypeId;
            //Assigning the ID of user who accepted the case in corresponding Assigned field for re-assignment in case of stage is changed again.
            system.debug('==Inside Downsizing=='+ownerType+'=='+(la.OwnerId).getSobjectType().getDescribe().getName());
			//if(ownerType == 'Queue' && (la.OwnerId).getSobjectType().getDescribe().getName()=='User') [06-01-2020] Commented by KK
			if((la.OwnerId).getSobjectType().getDescribe().getName()=='User')
            {
                if(la.Sub_Stage__c=='Downsizing Checker')
                {
                    la.Assigned_Downsizing_Checker__c = la.ownerID;  
                }
                else if(la.Sub_Stage__c=='Downsizing Scan Checker')
                {
                    la.Assigned_Downsizing_Scan_Checker__c = la.ownerID;
                }
                else if(la.Sub_Stage__c=='Downsizing Scan Maker')
                {
                    la.Assigned_Downsizing_Scan_Maker__c = la.ownerID;
                }
                else if(la.Sub_Stage__c=='Downsizing Review')
                {
                    la.Assigned_Downsizing_Review__c = la.ownerID;
                }
                else if(la.Sub_Stage__c=='Downsizing Authorization')
                {
                    la.Assigned_Downsizing_Authorization__c = la.ownerID;
                }
            }
        }
		system.debug('==Returning from Downsizing=='+la);
        return la;
    }
}