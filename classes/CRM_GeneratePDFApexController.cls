public class CRM_GeneratePDFApexController {
@auraEnabled
     public case opp{get;set;}
    public CRM_GeneratePDFApexController(){
        Id caseId = apexpages.currentpage().getparameters().get('id');
		opp = [select id,CaseNumber from case where id =: caseId];
    }
    public static void savePDFCase(){
       PageReference pdfPage = Page.GeneratePDF;
		//pdfPage.getParameters().put('Id', caseId.id);
        Blob pdfContent = pdfPage.getContent();
        
        Attachment attach1= new Attachment();
       //	attach1.ParentId = caseId;
        attach1.Name = 'Test Attachment for PDF';
        attach1.Body = pdfContent;
        attach1.contentType = 'application/pdf';
		insert attach1;
        
    }
}