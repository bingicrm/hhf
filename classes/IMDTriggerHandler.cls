public class IMDTriggerHandler {

public static void onBeforeInsert(List<IMD__c> lstTriggerNew) {
        System.debug('Debug Log for lstTriggerNew' +lstTriggerNew.size());
        Date dt = LMSDateUtility.lmsDateValue;
//tat.Business_Start_Date__c = (Date) dt; 
        for(IMD__c objIMD : lstTriggerNew) {
            //IMDCallout.makeIMDCallout(objIMD.ID);
            objIMD.Business_Date__c = (Date)dt;
        }
    }

    public static void onAfterInsert(List<IMD__c> lstTriggerNew) {
        System.debug('Debug Log for lstTriggerNew' +lstTriggerNew.size());
        //Calculate custom roll up
        List<Id> lstLoanAppIds= new List<Id>();
        for(IMD__c objIMD : lstTriggerNew){
           
                lstLoanAppIds.add(objIMD.Loan_Applications__c);
        }
        
        calculateAmount(lstLoanAppIds);
        
        for(IMD__c objIMD : lstTriggerNew) {
           if(objIMD.Bank_Master__c != null ){
               // if((objIMD.Loan_Applications__r.Loan_Number__c != null)){
                IMDCallout.makeIMDCallout(objIMD.ID);
         //   }else objIMD.addError('Please generate the LMS ID before updating the record.');
        } // IMDCallout.makeIMDCallout(objIMD.ID);
        }    
    }
	// Added by Vaishali for BREII
    public static void onBeforeUpdate(List<IMD__c> newList, Map<Id,IMD__c> oldMap) {
        Map<Id, BRE2_Retrigger_Fields__mdt	> mapBRE2Retrigger = new Map<Id,BRE2_Retrigger_Fields__mdt	>([Select Id, Field__c, Object__c, isActive__c from BRE2_Retrigger_Fields__mdt where Object__c = 'IMD__c' and isActive__c = true]);
        Set<Id> setOfAppIds = new Set<Id>();

        for(IMD__c imdDetail : newList){
            for ( BRE2_Retrigger_Fields__mdt objbreReset : mapBRE2Retrigger.values() ) {
                if(imdDetail.get(objbreReset.Field__c) != oldMap.get(imdDetail.id).get(objbreReset.Field__c)) {
                    setOfAppIds.add(imdDetail.Loan_Applications__c); 
                    system.debug('Setting Financial_Eligibility_Valid__c to false');
                    break;
                }
            }
        }
        
        system.debug('setOfAppIds '+setOfAppIds);
        Id recordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('BRE2').getRecordTypeId();
        List<Customer_Integration__c> lstOfBRE2SuccessfulCustInt= new List<Customer_Integration__c>();
        if(!setOfAppIds.isEmpty()) {
            lstOfBRE2SuccessfulCustInt = [SELECT Id, Loan_Application__c, BRE2_Success__c from Customer_Integration__c WHERE Loan_Application__c IN: setOfAppIds and BRE2_Recent__c = true and recordTypeId =: recordTypeId ]; 
        }
        Map<Id, Boolean> mapOfBRE2SuccessfulCustInt = new Map<Id, Boolean>();
        if(!lstOfBRE2SuccessfulCustInt.isEmpty()) {
            for(Customer_Integration__c cust : lstOfBRE2SuccessfulCustInt) {
                if(cust.BRE2_Success__c == true) {
                    mapOfBRE2SuccessfulCustInt.put(cust.Loan_Application__c, true);
                } else {
                    mapOfBRE2SuccessfulCustInt.put(cust.Loan_Application__c, false);    
                }
            }
        }
        system.debug('mapOfBRE2SuccessfulCustInt '+mapOfBRE2SuccessfulCustInt);

        for(IMD__c imdDetail : newList) {
            if (mapOfBRE2SuccessfulCustInt != null) {
                if(mapOfBRE2SuccessfulCustInt.containsKey(imdDetail.Loan_Applications__c)) {
                    if(mapOfBRE2SuccessfulCustInt.get(imdDetail.Loan_Applications__c)) {
                        imdDetail.reTriggerBRE2__c = true;       
                    }
                }
            }
        }
        
    }
    //End of patch- Added by Vaishali for BREII

    public static void onAfterUpdate(List<IMD__c> lstTriggerNew, Map<Id,IMD__c> oldMap) {
        System.debug('Debug Log for lstTriggerNew' +lstTriggerNew.size());

        string Recor = Schema.SObjectType.IMD__c.getRecordTypeInfosByName().get('IMD').getRecordTypeId();
            list<IMD__c> StatusList = new list<IMD__c>(); 
            list<string> reasonList = new list<string>();  

        //Added by Vaishali for BREII
        Map<Id,BRE2_Retrigger_Fields__mdt> mapBRE2Retrigger = new Map<Id,BRE2_Retrigger_Fields__mdt	>([Select Id, Field__c, Object__c, isActive__c from BRE2_Retrigger_Fields__mdt where Object__c = 'IMD__c' and isActive__c = true]);
        Set<Id> setOfAppIds = new Set<Id>();
        //End of patch- Added by Vaishali for BREII
		
        // Calculate custom roll up
        List<Id> lstLoanAppIds= new List<Id>();
        for(IMD__c objIMD : lstTriggerNew){
            if (objIMD.Amount__c != oldMap.get(objIMD.id).Amount__c)
                lstLoanAppIds.add(objIMD.Loan_Applications__c);
        }
        
        calculateAmount(lstLoanAppIds);
        
        for(IMD__c objIMD : lstTriggerNew) {
		
			//Added by Vaishali for BREII
            for ( BRE2_Retrigger_Fields__mdt objbreReset : mapBRE2Retrigger.values() ) {
                if( objIMD.get(objbreReset.Field__c) != oldMap.get(objIMD.id).get(objbreReset.Field__c)) {
                    setOfAppIds.add(objIMD.Loan_Applications__c); 
                    system.debug('Setting Financial_Eligibility_Valid__c to false');
                    break;
                }
            }
            //End of patch- Added by Vaishali for BREII
			
            if((objIMD.Cheque_Status__c == Constants.Bounced)&& (objIMD.RecordTypeId == Recor)){

                    StatusList.add(objIMD);
                    reasonList.add(objIMD.Bounce_Reason_ID__c);

            }


            if(objIMD.Bank_Master__c != null && oldMap.get(objIMD.id).Bank_Master__c != objIMD.Bank_Master__c ){
               // if((objIMD.Loan_Applications__r.Loan_Number__c != null)){
                IMDCallout.makeIMDCallout(objIMD.ID);
         //   }else objIMD.addError('Please generate the LMS ID before updating the record.');
        }

            //IMDCallout.makeIMDCallout(objIMD.ID);
        }
		//Added by Vaishali for BREII
        if(!setOfAppIds.isEmpty()) {
            LoanContactHelper.updateBRE2RetriggerOnLA(setOfAppIds);
        }
        //End of patch- Added by Vaishali for BREII
		
        loanApplicationStageUpdate(StatusList, reasonList);
    }
    
    public static void onAfterDelete(List<IMD__c> lstTriggerOld){
        List<Id> lstLoanAppIds= new List<Id>();
        for(IMD__c objIMD : lstTriggerOld){
           
                lstLoanAppIds.add(objIMD.Loan_Applications__c);
        }
        
        calculateAmount(lstLoanAppIds);
    }

    public static void loanApplicationStageUpdate(List<IMD__c> LAList, list<string> reasonList) {
        list<Bounce_Reason_Master_metadata__mdt> metaList= new list<Bounce_Reason_Master_metadata__mdt>();
        List<id> loanIdList = new list<id>();
        metaList = [select MasterLabel,Bounce_Type__c, BounceCharge__c from Bounce_Reason_Master_metadata__mdt where MasterLabel in:     reasonList];
        for(IMD__c La : LAList){
            for(Bounce_Reason_Master_metadata__mdt md : metaList){
                if(md.masterLabel == La.Bounce_Reason_ID__c && md.BounceCharge__c == 'Y') {
                   loanIdList.add(la.Loan_Applications__c);
                }
            }
        }
        list<Loan_Application__c> LoanList = new list<Loan_Application__c>();
        LoanList =[select id, IMD_approval_required__c from Loan_Application__c where id in :loanIdList];
        for(Loan_Application__c la: LoanList){
            la.IMD_approval_required__c = true;
        }
        update LoanList;
    }
    
    //Calculate Roll up summary field on Parent 
    public static void calculateAmount(List<Id> lstLoanIds){
        system.debug('@@cal' + lstLoanIds);
        List<loan_application__C> lstLoanApp = new List<loan_application__C>();
        for(loan_application__C objLoan : [SELECT Sum_of_Charge_Details__c  ,(SELECT id, Amount__c FROM IMD__r) FROM loan_application__C WHERE id =: lstLoanIds]){
            objLoan.Sum_of_Charge_Details__c  = 0;
            for (IMD__c obj : objLoan.IMD__r) {
                system.debug('@@obj ' + obj );
                objLoan.Sum_of_Charge_Details__c += obj.Amount__c;
            }
            lstLoanApp.add(objLoan);
        }
        
        database.update(lstLoanApp,false);
    }
    
}