public class DisbursementTriggerHelper {
      
    /* 
    Method Name 		: validateChargetoSchemeMapping
    Parameter passed : List of Disbursement record(s)
    Created By  		: Shobhit Saxena (Deloitte)
    Created Date 	: 16 December, 2018
    Description 		: This method checks whether the Charges applied are valid for the Loan Scheme applied for.
    */
    public static void validateChargetoSchemeMapping(List<Disbursement__c> newList) {
        List<Scheme_to_Charge_Code_Mapping__mdt> lstSchemetoChargeCodeMapping = new List<Scheme_to_Charge_Code_Mapping__mdt>();
        Map<String,List<String>> mapSchemeIdtolstChargeCodes = new Map<String,List<String>>();
        Map<String,String> mapLoanAppIdtoSchemeId = new Map<String,String>();
        Map<String,String> mapLoanAppIdtoSchemeName = new Map<String,String>();
        Set<String> setLoanApplicationIds = new Set<String>();
        if(!newList.isEmpty()) {
            lstSchemetoChargeCodeMapping = [SELECT
                                            Charge_Code_ID__c,
                                            Charge_ID__c,
                                            DeveloperName,
                                            Id,
                                            Label,
                                            Scheme_ID__c 
                                            FROM 
                                            Scheme_to_Charge_Code_Mapping__mdt
                                           ];
            System.debug('Debug Log for lstSchemetoChargeCodeMapping'+lstSchemetoChargeCodeMapping.size());
            if(!lstSchemetoChargeCodeMapping.isEmpty()) {
                for(Scheme_to_Charge_Code_Mapping__mdt objSchemetoChargeCodeMapping : lstSchemetoChargeCodeMapping) {
                    if(!mapSchemeIdtolstChargeCodes.containsKey(objSchemetoChargeCodeMapping.Scheme_ID__c)) {
                        mapSchemeIdtolstChargeCodes.put(objSchemetoChargeCodeMapping.Scheme_ID__c, new List<String> {objSchemetoChargeCodeMapping.Charge_Code_ID__c});
                    }
                    else if(mapSchemeIdtolstChargeCodes.containsKey(objSchemetoChargeCodeMapping.Scheme_ID__c)) {
                        mapSchemeIdtolstChargeCodes.get(objSchemetoChargeCodeMapping.Scheme_ID__c).add(objSchemetoChargeCodeMapping.Charge_Code_ID__c);
                    }
                }
                System.debug('Debug Log for mapSchemeIdtolstChargeCodes'+mapSchemeIdtolstChargeCodes.size());
                if(!mapSchemeIdtolstChargeCodes.isEmpty()) {
                    for(String str : mapSchemeIdtolstChargeCodes.keySet()) {
                        System.debug('Key: '+str);
                        System.debug('Values: '+mapSchemeIdtolstChargeCodes.get(str));
                    }
                }
            }
            
            for(Disbursement__c objDisbursement : newList) {
                if(String.isNotBlank(objDisbursement.Loan_Application__c)) {
                    setLoanApplicationIds.add(objDisbursement.Loan_Application__c);
                }
            }
            
            System.debug('Debug Log for setLoanApplicationIds'+setLoanApplicationIds.size());
            if(!setLoanApplicationIds.isEmpty()) {
                List<Loan_Application__c> lstLoanApplication = [SELECT
                                                                Id,
                                                                Name,
                                                                Scheme__c,
                                                                Scheme__r.Name,
                                                                Scheme__r.Scheme_ID__c
                                                                FROM
                                                                Loan_Application__c
                                                                WHERE
                                                                Id IN: setLoanApplicationIds
                                                               ];
                System.debug('Debug Log for lstLoanApplication' +lstLoanApplication.size());
                
                if(!lstLoanApplication.isEmpty()) {
                    for(Loan_Application__c objLoan_Application : lstLoanApplication) {
                        mapLoanAppIdtoSchemeId.put(objLoan_Application.Id, String.valueOf(objLoan_Application.Scheme__r.Scheme_ID__c));
                        mapLoanAppIdtoSchemeName.put(objLoan_Application.Id, String.valueOf(objLoan_Application.Scheme__r.Name));
                    }
                    System.debug('Debug Log for mapLoanAppIdtoSchemeId'+mapLoanAppIdtoSchemeId.size());
                    
                    if(!mapLoanAppIdtoSchemeId.isEmpty()) {
                        for(String str : mapLoanAppIdtoSchemeId.keySet()) {
                            System.debug('Loan Application Id: '+str);
                            System.debug('Scheme Id: '+mapLoanAppIdtoSchemeId.get(str));
                        }
                        
                        for(Disbursement__c objDisbursement : newList) {
                            if(String.isNotBlank(objDisbursement.Loan_Application__c) && mapLoanAppIdtoSchemeId.containsKey(objDisbursement.Loan_Application__c)) {
                                if(String.isNotBlank(mapLoanAppIdtoSchemeId.get(objDisbursement.Loan_Application__c))) {
                                    if(String.isNotBlank(String.valueOf(objDisbursement.ROC_Charges__c)) && !mapSchemeIdtolstChargeCodes.get(mapLoanAppIdtoSchemeId.get(objDisbursement.Loan_Application__c)).contains(Constants.strROCChargeCode)) {
                                        objDisbursement.addError('Error : ROC Charges are not applicable for the Loan Scheme - '+mapLoanAppIdtoSchemeName.get(objDisbursement.Loan_Application__c));
                                    }
                                    if(String.isNotBlank(String.valueOf(objDisbursement.CERSAI_Charges__c)) && !mapSchemeIdtolstChargeCodes.get(mapLoanAppIdtoSchemeId.get(objDisbursement.Loan_Application__c)).contains(Constants.strCERSAIChargeCode)) {
                                        objDisbursement.addError('Error : CERSAI Charges are not applicable for the Loan Scheme - '+mapLoanAppIdtoSchemeName.get(objDisbursement.Loan_Application__c));
                                    }
                                    if(String.isNotBlank(String.valueOf(objDisbursement.Legal_Vetting__c)) && !mapSchemeIdtolstChargeCodes.get(mapLoanAppIdtoSchemeId.get(objDisbursement.Loan_Application__c)).contains(Constants.strLegalVettingChargeCode)) {
                                        objDisbursement.addError('Error : Legal Vetting Charges are not applicable for the Loan Scheme - '+mapLoanAppIdtoSchemeName.get(objDisbursement.Loan_Application__c));
                                    }
                                    if(String.isNotBlank(String.valueOf(objDisbursement.Stamp_Duty__c)) && !mapSchemeIdtolstChargeCodes.get(mapLoanAppIdtoSchemeId.get(objDisbursement.Loan_Application__c)).contains(Constants.strStampDutyChargeCode)) {
                                        objDisbursement.addError('Error : Stamp Duty Charges are not applicable for the Loan Scheme - '+mapLoanAppIdtoSchemeName.get(objDisbursement.Loan_Application__c));
                                    }
                                    if(String.isNotBlank(String.valueOf(objDisbursement.PEMI__c)) && !mapSchemeIdtolstChargeCodes.get(mapLoanAppIdtoSchemeId.get(objDisbursement.Loan_Application__c)).contains(Constants.strPEMIChargeCode)) {
                                        objDisbursement.addError('Error : PEMI Charges are not applicable for the Loan Scheme - '+mapLoanAppIdtoSchemeName.get(objDisbursement.Loan_Application__c));
                                    }
                                }
                            }
                        }
                    }
                    
                }
            }
        }
        
    }
    
/*    public static void validateDisbursedAmount(List<Disbursement__c> newList){
        Tranche__c tranRec;
        List<Disbursement__c> newDisList;
        for(Disbursement__c disb : newList){
            Id tranId = disb.Tranche__c;
            tranRec = [Select Approved_Disbursal_Amount__c from Tranche__c where Id =: tranId ];  
            newDisList = [Select Cheque_Amount__c from Disbursement__c where Tranche__c =: tranId];
            if(newDisList.size() <= 0){
                Decimal sum = checkAmount(newDisList);
                if (sum >= tranRec.Approved_Disbursal_Amount__c){
                    disb.addError('You cannot create any more disbursement');
                }
            }            
        }
    }*/

    //GN 27/12] Making modifications to the code to remove for loop queries.
    public static void validateDisbursedAmount(List<Disbursement__c> newList) {
        Set<Id> setOfTrancheIds = new Set<Id> ();
        for(Disbursement__c disb: newList) {
            if(disb.Tranche__c != null) {
                setOfTrancheIds.add(disb.Tranche__c);
            }
        }
        if(setOfTrancheIds.size() > 0) {
            Map<Id, Tranche__c> mapOfTrancheIdAndTranche = new Map<Id, Tranche__c> ();
            for(Tranche__c t : [SELECT Id, Approved_Disbursal_Amount__c
                                FROM Tranche__c
                                WHERE Id IN :setOfTrancheIds]) {
                mapOfTrancheIdAndTranche.put(t.Id, t);
            }
            Map<Id, List<Disbursement__c>> mapOfTrancheIdAndDisb = new Map<Id, List<Disbursement__c>> ();
            for(Disbursement__c d : [SELECT Id, Cheque_Amount__c, Tranche__c
                                    FROM Disbursement__c
                                    WHERE Tranche__c IN :setOfTrancheIds]) {
                if(!mapOfTrancheIdAndDisb.containsKey(d.Tranche__c)) {
                    mapOfTrancheIdAndDisb.put(d.Tranche__c, new LisT<Disbursement__c> ());
                }
                mapOfTrancheIdAndDisb.get(d.Tranche__c).add(d);
            }
            for(Disbursement__c disb : newList) {
                if(mapOfTrancheIdAndTranche.containsKey(disb.Tranche__c)) {
                    Tranche__c t = mapOfTrancheIdAndTranche.get(disb.Tranche__c);
                    List<Disbursement__c> lstOfDisb = new List<Disbursement__c> ();
                    if(mapOfTrancheIdAndDisb.containsKey(disb.Tranche__c)) {
                        lstOfDisb = mapOfTrancheIdAndDisb.get(disb.Tranche__c);
                        Decimal sum = checkAmount(lstOfDisb);
                        if (sum >= t.Approved_Disbursal_Amount__c) {
                            disb.addError('You cannot create any more disbursement');
                        }
                    }
                }
            }
        }
    }
    
    private static Decimal checkAmount(List<Disbursement__c> newDisList){
        Decimal disAmount = 0;
        for(Disbursement__c db : newDisList){
            disAmount = disAmount + db.Cheque_Amount__c;
        }
        return disAmount;
    }    
	/*********************Added by Chitransh for TIL-1472*************************************/
    public static void checkDisbursalAmount(List<Disbursement__c> newDisbRecords){
        Set<Id> loanAppIds = new Set<Id>();
        for(Disbursement__c disb : newDisbRecords){
            loanAppIds.add(disb.Loan_Application__c);
        }
        Map<Id, Loan_Application__c> mapLA = new Map<Id,Loan_Application__c>([SELECT Id, Approved_Loan_Amount__c,Disbursed_Amount__c FROM Loan_Application__c WHERE ID IN : loanAppIds]);
		Id recordTypeId = [Select id,Name from RecordType where name ='First Disbursement'].Id;//Added by Chitransh for TIL-1472[reopened]
        for(Disbursement__c disb : newDisbRecords){
			if(disb.RecordTypeId == recordTypeId){//IF BLOCK Added by Chitransh for TIL-1472[ReOpened]//
            Decimal pendingAmt = 0.0;
           // Decimal disbursalAmt = 0.0;
            system.debug('Approved Loan Amount : ' + mapLA.get(disb.Loan_Application__c).Approved_Loan_Amount__c+ ' , Disbursed Amount : ' + disb.Disbursal_Amount__c );
            if(mapLA != null && mapLA.containsKey(disb.Loan_Application__c)){
                if(mapLA.get(disb.Loan_Application__c).Approved_Loan_Amount__c != null){                
                    if(disb.Disbursal_Amount__c != null){
                        pendingAmt = mapLA.get(disb.Loan_Application__c).Approved_Loan_Amount__c - disb.Disbursal_Amount__c ;
                        system.debug('pendingAmt--->>'+pendingAmt);
                        if(pendingAmt<=Decimal.valueOf(Label.Tranche_Limit) && pendingAmt != 0){
                            disb.addError(Label.Disbursement_Error);
                        }
                    }
                }
            }
        }
    }
	}
    /**********************End of Patch added by Chitransh for TIL-1472****************/	

/********************Added by Chitransh to update Disburse_Amount__c on LA post Tranche Cancelled [30-01-2020]**********************/
    public static void updateLADisburseAmount(List<Disbursement__c> newList){
        if(!newList.isEmpty()){
            set<id> loanAppIds = new set<id>();
            set<id> trancheIds = new set<id>();
            Map<id,Tranche__c> trancheMap;
            Map<id,Loan_Application__c> loanAppMap;
            List<Disbursement__c> disbList = new List<Disbursement__c>();
            List<Loan_Application__c> loanAppUpdatelist = new List<Loan_Application__c>();
            for(Disbursement__c disb : newList){
                loanAppIds.add(disb.Loan_Application__c);
                trancheIds.add(disb.Tranche__c);
            }
            if(!loanAppIds.isEmpty())
                loanAppMap = new Map<id,Loan_Application__c>([Select id,Loan_Application_Number__c,Disburse_Amount__c from Loan_Application__c where id = : loanAppIds]);
            if(!trancheIds.isEmpty()) 
                trancheMap = new Map<id,Tranche__c>([Select id,Status__c from Tranche__c where id =: trancheIds]);
            for(Disbursement__c disb : newList){
                if(disb.Tranche_Cancelled__c == true 
                   && ((disb.Tranche__c == null && loanAppMap.containsKey(disb.Loan_Application__c) && loanAppMap.get(disb.Loan_Application__c).Loan_Application_Number__c != null)
                       || (disb.Tranche__c != null && trancheMap.containsKey(disb.Tranche__c) && trancheMap.get(disb.Tranche__c).Status__c == 'Cancelled' ))
                  ){
                      Loan_Application__c loanApp = loanAppMap.get(disb.Loan_Application__c); 
                      system.debug('loan App Disburse Amount ' + loanApp.Disburse_Amount__c );
                      loanApp.Disburse_Amount__c = loanApp.Disburse_Amount__c - disb.Disbursal_Amount__c;
                      system.debug('Amount to subtract :' + disb.Disbursal_Amount__c + '====Amount '+ loanApp.Disburse_Amount__c);
                      loanAppUpdatelist.add(loanApp);
                  }
            }
            if(!loanAppUpdatelist.isEmpty())
                update loanAppUpdatelist;
        }
    }
    /********************End of patch added by Chitransh to update Disburse_Amount__c on LA post Tranche Cancelled [30-01-2020]**********************/	

	//Added By Abhishek for Sourcing BRD - START
    public static void copySourcingDataToDisbursement(List<Disbursement__c> disbursedDisbusementList, Set<Id> loanAppIdSet) {
        Map<Id, Sourcing_Detail__c> LAIdandSDobjMap = new Map<Id, Sourcing_Detail__c>();
        //ASM__r.Name, ASM_Employee_Code__c, Team_Lead__r.Name, Team_Lead_Employee_Code__c, RSM__r.Name, RSM_Employee_Code__c added by Abhishek for TIL-00002442
        for(Sourcing_Detail__c sdObj : [SELECT Id, Name, ToLabel(Source_Code__c), Sales_User_Name__r.Name, Sales_Manager_Employee_Code__c, DST__r.Name, DST_Employee_Code__c, CreatedBy.Name, CreatedBy.EmployeeNumber, Loan_Application__c, Loan_Application__r.Channel_Partner__c, ASM__r.Name, ASM_Employee_Code__c, Team_Lead__r.Name, Team_Lead_Employee_Code__c, RSM__r.Name, RSM_Employee_Code__c FROM Sourcing_Detail__c WHERE Loan_Application__c IN :loanAppIdSet ORDER BY CreatedDate DESC]) {
            if(!LAIdandSDobjMap.containsKey(sdObj.Loan_Application__c)) {
                LAIdandSDobjMap.put(sdObj.Loan_Application__c, sdObj);
            }
        }
        
        for(Disbursement__c disbObj: disbursedDisbusementList) {
            If(LAIdandSDobjMap.containsKey(disbObj.Loan_Application__c)) {
                Sourcing_Detail__c sdObj = LAIdandSDobjMap.get(disbObj.Loan_Application__c);
                
                disbObj.Source_Code__c = String.isNotBlank(sdObj.Source_Code__c) ? sdObj.Source_Code__c : '';
                disbObj.Sales_Manager_Name__c = String.isNotBlank(sdObj.Sales_User_Name__r.Name) ? sdObj.Sales_User_Name__r.Name : '';
                disbObj.Sales_Manager_Employee_Code__c = String.isNotBlank(sdObj.Sales_Manager_Employee_Code__c) ? sdObj.Sales_Manager_Employee_Code__c : '';
                disbObj.DST_Name__c = String.isNotBlank(sdObj.DST__r.Name) ? sdObj.DST__r.Name : sdObj.CreatedBy.Name;
                disbObj.DST_Employee_Code__c = String.isNotBlank(sdObj.DST_Employee_Code__c) ? sdObj.DST_Employee_Code__c : sdObj.CreatedBy.EmployeeNumber;
				disbObj.Team_Lead_Name__c = String.isNotBlank(sdObj.Team_Lead__r.Name) ? sdObj.Team_Lead__r.Name : ''; //Added by Abhishek for TIL-00002442
                disbObj.Team_Lead_Employee_Code__c = String.isNotBlank(sdObj.Team_Lead_Employee_Code__c) ? sdObj.Team_Lead_Employee_Code__c : ''; //Added by Abhishek for TIL-00002442
                disbObj.Channel_Partner_Name__c = String.isNotBlank(sdObj.Loan_Application__r.Channel_Partner__c) ? sdObj.Loan_Application__r.Channel_Partner__c : '';
				disbObj.Area_Sales_Manager_Name__c = String.isNotBlank(sdObj.ASM__r.Name) ? sdObj.ASM__r.Name : ''; //Added by Abhishek for TIL-00002442
                disbObj.Area_Sales_Manager_Employee_Code__c = String.isNotBlank(sdObj.ASM_Employee_Code__c) ? sdObj.ASM_Employee_Code__c : ''; //Added by Abhishek for TIL-00002442
            	disbObj.Regional_Sales_Manager_Name__c = String.isNotBlank(sdObj.RSM__r.Name) ? sdObj.RSM__r.Name : ''; //Added by Abhishek for TIL-00002442
                disbObj.Regional_Sales_Manager_Employee_Code__c = String.isNotBlank(sdObj.RSM_Employee_Code__c) ? sdObj.RSM_Employee_Code__c : ''; //Added by Abhishek for TIL-00002442
            }
        }
    }
    //Added By Abhishek for Sourcing BRD - END
}