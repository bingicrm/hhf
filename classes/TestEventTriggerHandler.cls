@isTest
public class TestEventTriggerHandler {
     @TestSetup
    static void setup()
    {
               UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        ID roleID= r.id;
        User u = new User(
            ProfileId ='00e7F000002yr4O',
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = roleID);
        Insert u;
    }
@isTest
    public static void t(){
        User u = [select Id from User where Email = 'puser000@amamama.com'];
        Account acc=new Account();
        acc.name='TestAccount1';
        acc.phone='9234567667';
        acc.PAN__c='fjauy0916u';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        System.debug(acc.id);
        
        Scheme__c sc=new Scheme__c();
        sc.name='TestScheme';
        sc.Scheme_Code__c='testcode';
        sc.Scheme_Group_ID__c='23131';
        sc.Product_Code__c='HL';
        insert sc;
        System.debug(sc.id);
        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Transaction_type__c='PB';
        lap.Scheme__c=sc.id;
        insert lap;
        System.debug(lap.id);
        
        City__c city=new city__c();
        city.City_ID__c='100000';
        city.Name='TEZPOOR';
        city.Country__c='1';
        city.State__c='5';
        insert city;
        Pincode__c pin=new Pincode__c();
        pin.City_LP__c=city.id;
        pin.ZIP_ID__c='46172';
        insert pin;
        list<Loan_Contact__c> lc=[select id, name from Loan_Contact__c where Loan_Applications__c=:lap.id limit 1];
        system.debug('loan contact'+lc[0]);
        Address__c address=new Address__c();
        address.Address_Line_1__c='abc Street';
        address.Address_Line_2__c='abc Area';
        address.Pincode_LP__c=pin.id;
        address.Type_of_address__c='PERMNENT';
        address.Loan_Contact__c=lc[0].id;
        //insert address;
        //list<customer_integration__c> ci=[select id, name from loan_A where loan_application__c=:lap.id limit 1 ];
        system.debug('loan contact'+lc[0]);
        customer_integration__c ci =new customer_integration__c();
        ci.Loan_Application__c  =lap.id;
        ci.Loan_Contact__c=lc[0].id;
        insert ci;
         
       // list<Loan_Contact__c> lc=[select id, name from Loan_Contact__c];
      // list<Event> e=[select id, whatId from event limit 1 ];
        Event e = new Event();
        e.WhatId=lc[0].id;
        e.StartDateTime=system.today();
        e.EndDateTime=system.today()+5;
            
        Test.startTest();  
            try
            {
                insert  e;       
            }
            catch(Exception ex)
            {
                Boolean expectedExceptionThrown =  ex.getMessage().contains('New Event Cannot be Created') ? true : false;
            //  System.AssertEquals(expectedExceptionThrown, true);                
            }
        Test.stopTest();  

    }
}