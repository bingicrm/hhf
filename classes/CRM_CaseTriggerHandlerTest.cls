@isTest
Public class CRM_CaseTriggerHandlerTest {
    
    static testMethod void testMethod1() {
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='test1@gmail.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@testorg.com');
        
        
        String recordTypeId = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acc= new Account(
            RecordTypeID=recordTypeId ,
            FirstName='TestFName',
            LastName='TestLName',
            Salutation='Test',
            PersonMailingStreet='test@gmail.com',
            PersonMailingPostalCode='212345',
            PersonMailingCity='SFO',
            PersonEmail='test@gmail.com',
            PersonHomePhone='9234567891',
            PersonMobilePhone='9234567891'
        );
        
        insert acc;
        
        Case_Round_Robin_Assignment__c crr = new Case_Round_Robin_Assignment__c();
        crr.Name='default';
        crr.User_Index__c=6;
        insert crr;
        
        Test.startTest(); 
        try{
            Case c = new Case(Origin='Web',Original_Case_Owner__c = u.id,TAT__c =4,Net_Days_Used1__c=3);
            insert c;
            Id userId = UserInfo.getUserId();
            user userForTesting = [SELECT Id FROM User WHERE Id =: userId LIMIT 1];
            system.runAs(userForTesting){
                c.Original_Case_Owner__c=userId;
                c.Status='Closed';
                update c;
            }
            Case c2 = new Case(Origin='Email',Original_Case_Owner__c = u.id,TAT__c =4,Net_Days_Used1__c=3); 
            Insert c2;
            Case c3 = new Case(Original_Case_Owner__c = u.id,TAT__c =4,Net_Days_Used1__c=3); 
            Insert c3;
            Case c4 = new Case(Department__c='Sales',Category__c='DISBURSEMENT RELATED',Sub_Category__c='TOP UP REQUEST',SuppliedEmail='test@gmail.com',Origin='Email',Original_Case_Owner__c = u.id,TAT__c =4,Net_Days_Used1__c=3,Status='Closed'); 
            
            Insert c4;
            c4.Department__c='Credit';
            Update c4;
            
            Case c5 = new Case(Department__c='Sales',SuppliedPhone='9234567891',SuppliedEmail='test@gmail.com',Origin='Email',Original_Case_Owner__c = u.id,TAT__c =4,Net_Days_Used1__c=3,Status='Closed'); 
            Insert c5;
        }
        catch(exception e){}
        Test.stopTest();
    }
    
    static testMethod void testGetCaseLoanInfo(){
        //Case and loan application
        Loan_Application__c loanApp = new Loan_Application__c();
        loanApp.Loan_Number__c = '1000487';
        loanApp.Loan_Application_Number__c = 'HHFDELHOU18000000205';
        insert loanApp;
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new CRM_LmsLoanInfoMock());
        
        case theCase = new case();
        theCase.LD_Branch_ID__c = '1';
        theCase.Email__c = 'test@example.com';
        theCase.Loan_Application__c = loanApp.Id;
        theCase.LD_Customer_ID__c = '101402';
        insert theCase;
        test.stopTest();
    }
    
    static testMethod void testGetCaseLoanInfo2(){
        //Case and loan application
        Loan_Application__c loanApp = new Loan_Application__c();
        loanApp.Loan_Number__c = '1000487';
        loanApp.Loan_Application_Number__c = 'HHFDELHOU18000000205';
        insert loanApp;
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new CRM_LmsLoanInfoMock2());
        
        case theCase = new case();
        theCase.LD_Branch_ID__c = '1';
        theCase.Email__c = 'test@example.com';
        theCase.Loan_Application__c = loanApp.Id;
        theCase.LD_Customer_ID__c = '101402';
        insert theCase;
        test.stopTest();
    }
    
    static testMethod void testGetCaseLoanList(){
        //Case and loan application
        Loan_Application__c loanApp = new Loan_Application__c();
        loanApp.Loan_Number__c = '1000487';
        loanApp.Loan_Application_Number__c = 'HHFDELHOU18000000205';
        insert loanApp;
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new CRM_LmsLoanListMock());
        
        case theCase = new case();
        theCase.LD_Branch_ID__c = '1';
        theCase.Email__c = 'test@example.com';
        theCase.Loan_Application__c = loanApp.Id;
        theCase.LD_Customer_ID__c = '101402';
        insert theCase;
        
        test.stopTest();
    }
}