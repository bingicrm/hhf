public class LMSRepaymentSchedule 
{

    public static RepaymentResponseWrapper pullRepaymentSchedule( String repaymentId , String laId )
    {
        HttpResponse res; 
       try
        {
            List<Loan_Application__c> lstLoan = [Select Id, Repayment_Generated_for_Documents__c,Repayment_schedule_at_Disbursement_Check__c,Repayment_Start_Date__c,StageName__c,Sanctioned_Disbursement_Type__c,Sub_Stage__c, Approved_EMI__c from Loan_Application__c where Id =:laId];
            List<Loan_Repayment__c> allRepayment = [Select Id, Loan_Application__c from Loan_Repayment__c where Loan_Application__c = :laId];
            List<Repayment_Schedule__c> allSchedule = [Select id from Repayment_Schedule__c where Loan_Application__c = :laId ];
            List<Disbursement__c> lstDisbursment = [Select Id, PEMI__c,Recordtype.Name FROM Disbursement__c Where Loan_Application__c =: laId AND 
                                                    (Recordtype.Name = 'First Disbursement' OR Recordtype.Name = 'Others') ORDER BY CreatedDate DESC
                                                   ];
            
            Loan_Repayment__c repaymentRqst = [Select l.dueDay__c, l.Tenure__c, l.Sanction_Amount__c, l.RescheduleFlag__c, 
                                                      l.RequestId__c, l.Repayment_effective_date__c, l.Repayment_Drawn_On__c,
                                                      l.RepayType__c, l.Rate_Type__c, l.Number_of_Advance_Installment__c, 
                                                      l.Negative_Capitalization_Flag__c, l.Name, l.Moratorium_period__c, 
                                                      l.Moratorium_impact_on_tenure__c, l.Moratorium_charge_interest_adjust_in_sch__c, 
                                                      l.Moratorium_Flag__c, l.Moratorium_Charge_interest__c, l.Loan_Application__c, 
                                                      l.Interest_StartDate__c, l.Interest_Round_off_parameter__c, l.Interest_Round_off_parameter_Flag__c, 
                                                      l.Interest_Rate__c, l.Instalment_Round_off_parameter__c, l.Instalment_Round_off_parameter_Flag__c, l.Installment_Plan__c, 
                                                      l.Installment_Mode__c, l.IRR_calculation_parameters__c, l.Frequency__c, l.EMI_Start_date__c, l.EMI_Amount__c, l.Disbursement_Amount__c, 
                                                      l.Disbursal_Date__c, l.Days_Per_Year__c, l.Bulk_refund__c, l.Broken_Period_Interest_Handing__c, 
                                                      l.Advanced_Installment_Flag__c, l.Additional_Disbursement__c , l.Baloon_Amount__c, l.Loan_Application__r.Insurance_Loan_Application__c
                                               From Loan_Repayment__c l 
                                               where l.Id = :repaymentId];/** Added by saumya for Insurance Loan l.Loan_Application__r.Insurance_Loan_Application__c **/
            Rest_Service__mdt restService = [ SELECT MasterLabel, QualifiedApiName , Service_EndPoint__c , 
                                                         Client_Password__c, Client_Username__c, Request_Method__c, Time_Out_Period__c  
                                                  FROM   Rest_Service__mdt
                                                  WHERE  MasterLabel = 'Repayment Schedule'
                                                ];
            if (!lstLoan.isEmpty() && lstLoan[0].Sub_Stage__c == 'Disbursement Checker') {
                return new RepaymentResponseWrapper(true, 'You cannot perform this action at Disbursement Checker!');
            }                                    
            
			//Added by Mehul 18-03
			if ( lstLoan[0].Sub_Stage__c == 'Disbursement Maker' && (lstDisbursment == null || lstDisbursment.size() == 0) ) {
                return new RepaymentResponseWrapper(true, 'Please create Disbursement record before generating repayment!');
            }
            
            //Removed validation as per A.C. [Mehul-27-03]
            /*
            if ( lstLoan[0].Sub_Stage__c == 'Disbursement Maker' && lstDisbursment != null && lstDisbursment.size() > 0 ) {
                List<Disbursement_payment_details__c> lstDisbPayment = [Select Id from Disbursement_payment_details__c where Disbursement__c =: lstDisbursment[0].id] ;
                if ( lstDisbPayment != null && lstDisbPayment.size() > 0) {
                    return new RepaymentResponseWrapper(true, 'Please delete the Disbursement Payment record(s) before generating repayment!');
                }
            }*/
			
            List<Repayment_Schedule__c> repaymentResponse =  new List<Repayment_Schedule__c>();
            

            Map<String,String> headerMap = new Map<String,String>();
            
            Blob headerValue = Blob.valueOf(restService.Client_Username__c + ':' + restService.Client_Password__c);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            headerMap.put('Authorization',authorizationHeader);
            headerMap.put('Username',restService.Client_Username__c);
            headerMap.put('Password',restService.Client_Password__c);
            headerMap.put('Content-Type','application/json');
            headerMap.put('Accept' , 'application/json');
            

            LMSRepaymentRequest rqst = new LMSRepaymentRequest();
            rqst.RequestId = Utility.GenerateRandomRequestId().substring(0,16);
            rqst.SourceId = 'SFDC';
            rqst.RepayType = repaymentRqst.RepayType__c;
            rqst.SanctionAmt = String.valueOf(repaymentRqst.Sanction_Amount__c);
            rqst.DisbAmt = String.valueOf(repaymentRqst.Disbursement_Amount__c);
            rqst.RepayDrawOn = repaymentRqst.Repayment_Drawn_On__c;
            rqst.Tenure = String.valueOf(repaymentRqst.Tenure__c);

            rqst.InstlPlan = repaymentRqst.Installment_Plan__c;

            // Not required until installment plan is "Balloon " or bullet
            rqst.BalloonAmt = repaymentRqst.Baloon_Amount__c != null ? String.valueOf(repaymentRqst.Baloon_Amount__c) : '0';
            List<GradedDetails> allG = new List<GradedDetails>();
            // Only required in graded installment plan
            if( rqst.InstlPlan == 'Graded Instl')
            {
                List<Graded_Details__c> allGraded = [Select Id,Slab_and_EMI_Seq_No__c,Slab_and_EMI_Recovery__c,
                Slab_and_EMI_Instl_To__c,Slab_and_EMI_Instl_From__c,Slab_and_EMI_EMI__c,Loan_Repayment__c from Graded_Details__c 
                where Loan_Repayment__c = : repaymentId ORDER by   Slab_and_EMI_Seq_No__c ASC];

                for( Graded_Details__c grd : allGraded )
                {
                    GradedDetails test = new GradedDetails();
                    test.SlabSeq = String.valueOf(grd.Slab_and_EMI_Seq_No__c);
                    test.SlabInstlFrm = String.valueOf(grd.Slab_and_EMI_Instl_From__c);
                    test.SlabInstlTo = String.valueOf(grd.Slab_and_EMI_Instl_To__c);
                    test.SlabEMI = grd.Slab_and_EMI_EMI__c != null ? String.valueOf(grd.Slab_and_EMI_EMI__c) : '';
                    test.SlabRecvPtg = grd.Slab_and_EMI_Recovery__c != null ? String.valueOf(grd.Slab_and_EMI_Recovery__c) : '';
                    allG.add(test);
                }


            }
            rqst.GradedDetails = allG;

            rqst.InstlMode = repaymentRqst.Installment_Mode__c;
            // in case of advanced installment
            rqst.AdvInstlFlg = repaymentRqst.Advanced_Installment_Flag__c;
            rqst.NoOfAdvInstl = String.valueOf(repaymentRqst.Number_of_Advance_Installment__c);
            //Monthly Mandatory
            rqst.Frequency = repaymentRqst.Frequency__c;
            //Mandatory
            rqst.IntRate = repaymentRqst.Interest_Rate__c != null ? String.valueOf(repaymentRqst.Interest_Rate__c) : '0' ;
            //Mandatory and REcalculate
            rqst.IntStartDate = getUpdatedDate(repaymentRqst.Interest_StartDate__c);

            //Moratorium fields currently False
            rqst.MoratFlg = repaymentRqst.Moratorium_Flag__c;
            rqst.MoratPrd = repaymentRqst.Moratorium_Flag__c == 'Yes' ? String.valueOf(repaymentRqst.Moratorium_period__c) : '0';
            rqst.MoratChargeIntAdjSch = repaymentRqst.Moratorium_charge_interest_adjust_in_sch__c != null && repaymentRqst.Moratorium_Flag__c == 'Yes'?  String.valueOf(repaymentRqst.Moratorium_charge_interest_adjust_in_sch__c) : '';
            rqst.MoratChargeInt = repaymentRqst.Moratorium_Charge_interest__c != null && repaymentRqst.Moratorium_Flag__c == 'Yes'? String.valueOf(repaymentRqst.Moratorium_Charge_interest__c) : '';
            rqst.MoratImpactTenure = repaymentRqst.Moratorium_impact_on_tenure__c != null && repaymentRqst.Moratorium_Flag__c == 'Yes' ? String.valueOf(repaymentRqst.Moratorium_impact_on_tenure__c) : '';

            //Rate type
            rqst.RateType = 'EMI';
            // if rate type is EMI  we use this one
            
            
            system.debug('@@@@Repayment_Generated_for_Documents__c' + lstLoan[0].Repayment_Generated_for_Documents__c);
            //Send EMI amount as 0 for each case - 13-03 discussed with G.K.
            if ( lstLoan[0].Repayment_Generated_for_Documents__c ) {
                rqst.EmiAmt = '0';
            } else {
                rqst.EmiAmt = repaymentRqst.EMI_Amount__c != null ? String.valueOf(repaymentRqst.EMI_Amount__c) : '0' ;
            }
                //if (lstLoan[0].Sanctioned_Disbursement_Type__c == 'Multiple Tranche') {
                //    rqst.EmiStartDt = getUpdatedDate(lstLoan[0].Repayment_Start_Date__c);
                //} else if (lstLoan[0].Sanctioned_Disbursement_Type__c == 'Single Tranche') {
                    rqst.EmiStartDt = getUpdatedDate(repaymentRqst.EMI_Start_date__c);
                //}
            //} else {
            //    rqst.EmiStartDt = getUpdatedDate(repaymentRqst.EMI_Start_date__c);
            //}

            // IRR value
            rqst.IrrCalParam = String.valueOf(repaymentRqst.IRR_calculation_parameters__c);

            //Mandatory
            rqst.DaysPerYr = String.valueOf(repaymentRqst.Days_Per_Year__c);

            //Fixed
            rqst.TaxRate = '0';
            rqst.SerTaxRate = '0';
            rqst.SerTaxRate= '0';


            rqst.IntRndOffParFlg= 'Off';
            rqst.InstlRndOffParFlg= 'Off';
            rqst.IntRndOffParam= String.valueOf('0');
            rqst.InstRndOffParam= String.valueOf('0');


            rqst.Disbursal_date= getUpdatedDate(repaymentRqst.Disbursal_Date__c);
            rqst.DueDay = String.valueOf(repaymentRqst.dueDay__c);

            rqst.I_Brkn_Prd_Int= String.valueOf(repaymentRqst.Broken_Period_Interest_Handing__c);

            // Default No
            rqst.I_Capatilize_Flag= repaymentRqst.Negative_Capitalization_Flag__c !=null ?String.valueOf(repaymentRqst.Negative_Capitalization_Flag__c) : '';


            rqst.I_Resch_Flag= '';
            rqst.I_Bulk_Refund= '';
            rqst.I_Add_Disbursment= '';

            // Let us see
            rqst.I_Repay_Effdate= getUpdatedDate(repaymentRqst.Repayment_effective_date__c);            

            rqst.MultiRateDetail= new List<MultiRateDetail>();
            rqst.UnStructRepay= new List<UnStructRepay>();

        

            String jsonRequest = Json.serialize(rqst);
            res   = Utility.sendRequest(restService.Service_EndPoint__c,restService.Request_Method__c, jsonRequest , headerMap, Integer.valueOf(restService.Time_Out_Period__c) );
            
            system.debug('res' + res);
            if( res != null && res.getStatusCode() == 200 )
            {
                system.debug(res.getStatusCode());
                system.debug(res.getBody());
                Object obj = res.getBody();
                List<RepaymentResponse> allResponse = (List<RepaymentResponse>)JSON.deserialize(res.getBody(), List<RepaymentResponse>.class);
                system.debug(allResponse);

                if( allResponse[allResponse.size()-1].ClosingPrinciple > 0 )
                {
                    return new RepaymentResponseWrapper(true, 'Closing Principle is not zero. Please change parameter and try again.');
                }
                for(RepaymentResponse rRes  : allResponse )
                {   
                    repaymentResponse.add(getRepayment(rRes,repaymentId,laId));
                }
                
                system.debug('@repaymentResponse.size()' + repaymentResponse.size());
                if(repaymentResponse.size() > 0 )
                {
                    system.debug('size schedule'+allSchedule.size());
                    if( allSchedule.size() > 0 )
                    {
                        delete allSchedule;
                    }
                    system.debug(repaymentResponse[0].Loan_Application__c);
                    insert repaymentResponse;
                    
                    system.debug('repaymentRqst.EMI_Amount__c' + repaymentRqst.EMI_Amount__c);
                     system.debug('before update' + lstLoan[0].Approved_EMI__c);
                     if ( repaymentResponse != null && repaymentResponse.size() > 0) {
                     //Modified as per logic updated for TIL-1272
                     system.debug('@@rqst.Disbursal_date' + rqst.Disbursal_date);
                     system.debug('@@rqst.IntStartDate' + rqst.IntStartDate);
                        if ( rqst.Disbursal_date == rqst.EmiStartDt ) { 
                            lstLoan[0].Approved_EMI__c  = repaymentResponse[0].Installment_Amount__c;
                        } else {
                            if(String.valueOf(repaymentRqst.Broken_Period_Interest_Handing__c) == 'Return as charge'){
                                lstLoan[0].Approved_EMI__c  = repaymentResponse[0].Installment_Amount__c;
                            } else {
                                lstLoan[0].Approved_EMI__c  = repaymentResponse[1].Installment_Amount__c;
                            }
                        }
                    }
                    if ( lstLoan[0].Sub_Stage__c == 'Disbursement Maker') {
                        lstLoan[0].Repayment_schedule_at_Disbursement_Check__c = true;
                    }
                    /*Added by Ankit for TIL-00002476 -- Start*/
                    Boolean updateDisbursment = false;
                    //lstLoan[0].Sub_Stage__c == 'Disbursement Maker' added by Abhilekh on 15th May 2019 for Sanction Letter unable to generate issue
                    if (!lstDisbursment.isEmpty() && lstLoan[0].Sub_Stage__c == 'Disbursement Maker') {
                        Loan_Repayment__c loanRepObj = [SELECT Id,PEMI__c FROM Loan_Repayment__c WHERE Id=:repaymentId];
                        if(String.valueOf(repaymentRqst.Broken_Period_Interest_Handing__c) == 'Return as charge' /*&& repaymentRqst.Loan_Application__r.Insurance_Loan_Application__c != true*/ /** Commented by Saumya for IMGC **/) {
                            lstDisbursment[0].PEMI__c = Decimal.valueOf(allResponse[0].BrokenPeriodInt);
                            loanRepObj.PEMI__c = Decimal.valueOf(allResponse[0].BrokenPeriodInt);
                        }
                        /*else if(repaymentRqst.Loan_Application__r.Insurance_Loan_Application__c == true){//Added by saumya for Insurance Loan 
                            lstDisbursment[0].PEMI__c = 0;
                        }*/ 
                        else {
                            lstDisbursment[0].PEMI__c = 0;
                            loanRepObj.PEMI__c = 0;
                        }
                        update loanRepObj;
                        updateDisbursment = true;
                        //update lstDisbursment;
                    }
                    
                    update lstLoan[0];                    
                    system.debug('after update' + lstLoan[0]);
                    if(!lstDisbursment.isEmpty() && updateDisbursment == true){
                        update lstDisbursment;
                    }
                    return new RepaymentResponseWrapper(false, '');
                    
                    /*Added by Ankit for TIL-00002476 -- End*/
                }
            }else 
            {   
                system.debug('inside invalid' );
                ErrorWrapper errorResp= (ErrorWrapper)JSON.deserialize(res.getBody(), ErrorWrapper.class);
                system.debug('errorResp' + errorResp);
                return new RepaymentResponseWrapper(true, errorResp.error.errorMessage);
                //return new RepaymentResponseWrapper(true, 'Invalid Request');
            }

        }
        Catch(Exception e)
        {
            //system.debug('res' + res);
            //system.debug('hello from Exception body' + res.getBody());
            system.debug(e.getStackTraceString());
            
            if ( e.getStackTraceString().contains('JSON.deserialize') ) {
                system.debug('inside deserialize' );
                ErrorWrapper errorResp= (ErrorWrapper)JSON.deserialize(res.getBody(), ErrorWrapper.class);
                system.debug('errorResp' + errorResp);
                return new RepaymentResponseWrapper(true, errorResp.error.errorMessage);
            } else if ( e.getStackTraceString().contains('INSUFFICIENT_ACCESS')) {
                return new RepaymentResponseWrapper(true, 'The schedule has been generated previously by a higher authority person, please contact System Admin.');
            } else {
                return new RepaymentResponseWrapper(true, 'Please contact System Admin');
            }    
        }
        
        return null;
    }

    
    public static Repayment_Schedule__c getRepayment( RepaymentResponse resp, String repaymentId,String laId  )
    {
        Repayment_Schedule__c rSch =  new Repayment_Schedule__c();
        rSch.Name =  'Installment - ' +resp.InstlNo;
        rSch.Loan_Repayment__c = repaymentId;
        rSch.Interest_Amount__c =  Decimal.valueOf(resp.Interest);
        rSch.Closing_Principal_Amount__c = resp.ClosingPrinciple;
        rSch.Days__c = resp.Days;
        rSch.Principal_Amount__c = Decimal.valueOf(resp.Principal);
        rSch.Business_IRR__c = resp.CustomerIRR;
        rSch.Installment_No__c = Decimal.valueOf(resp.InstlNo);
        rSch.Opening_Principal_Amount__c = resp.OpeningPrinciple;
        rSch.Due_Date__c = Date.parse(resp.DueDate);
        rSch.Customer_IRR__c = Decimal.valueOf(resp.CustomerIRR);
        rSch.Loan_Application__c = laId;
        rSch.Installment_Amount__c= Decimal.valueOf(resp.InstAmt);
        rSch.Days_Per_Year__c = resp.DaysPerYear;
        return rSch;

    }
    
    
    public class ErrorWrapper{
        public String status;   //failure
        public cls_error error;
    }
    public class cls_error {
        public String errorCode;    //400
        public String errorType;    //Application
        public String errorMessage; //Full Recovery not Possible with the specified EMIs.Kindly increse the EMIs.
    }
    public class LMSRepaymentRequest 
    {
        public String RequestId;
        public String SourceId;
        public String RepayType;
        public String SanctionAmt;
        public String DisbAmt;
        public String RepayDrawOn;
        public String Tenure;
        public String InstlPlan;
        public String BalloonAmt;
        public List<GradedDetails> GradedDetails;
        public String InstlMode;
        public String AdvInstlFlg;
        public String NoOfAdvInstl;
        public String Frequency;
        public String IntRate;
        public String IntStartDate;
        public String MoratFlg;
        public String MoratPrd;
        public String MoratChargeInt;
        public String MoratChargeIntAdjSch;
        public String MoratImpactTenure;
        public String RateType;
        public String EmiAmt;
        public String EmiStartDt;
        public String IrrCalParam;
        public String DaysPerYr;
        public String TaxRate;
        public String SerTaxRate;
        public String IntRndOffParFlg;
        public String InstlRndOffParFlg;
        public String IntRndOffParam;
        public String InstRndOffParam;
        public String Disbursal_date;
        public String DueDay;
        public String I_Brkn_Prd_Int;
        public String I_Capatilize_Flag;
        public String I_Resch_Flag;
        public String I_Bulk_Refund;
        public String I_Add_Disbursment;
        public String I_Repay_Effdate;
        public List<MultiRateDetail> MultiRateDetail;
        public List<UnStructRepay> UnStructRepay;
    }

    public class UnStructRepay 
    {
        public String InstlNo;
        public String DueDt;
        public String OpenBalPrin;
        public String InstlAmt;
        public String PrinComp;
        public String IntComp;
        public String ClosBalPrin;
        public String IntRate;
    }

    public class GradedDetails 
    {
        public String SlabSeq;
        public String SlabInstlFrm;
        public String SlabInstlTo;
        public String SlabEMI;
        public String SlabRecvPtg;
    }

    public class MultiRateDetail 
    {
        public String MultirateFromdate;
        public String MultirateTodate;
        public String MultirateFloatFlag;
        public String MultirateFixedIntRate;
        public String MultirateFloatRate;
    }


    public class RepaymentResponse 
    {

        public String CustomerIRR;
        public String BusinessIRR;
        public String InstlNo;
        public Decimal OpeningPrinciple;
        public String DueDate;
        public String InstAmt;
        public String AdvanceInstAmt;
        public String BrokenPeriodInt;
        public String Interest;
        public Decimal ClosingPrinciple;
        public String Days;
        public String ServiceTax;
        public String ServiceTaxRate;
        public String IntRate;
        public String AdvncFlag;
        public String DaysPerYear;
        public String Tds;
        public String Dsrate;
        public String Excess_interest;
        public String Principal;

    }


    public static String getUpdatedDate( Date toUpdate)
    {    
        if (toUpdate == null) {
            return '';
        }    
        DateTime dt =  Datetime.newInstance(toUpdate.year(), toUpdate.month(), toUpdate.day());
        return dt.format('dd/MM/yyyy');
    }

    public class RepaymentResponseWrapper
    {
        public boolean isError;
        public String errorMessage;

        public RepaymentResponseWrapper( boolean isError, String errorMessage)
        {
            this.isError = isError;
            this.errorMessage = errorMessage;
        }
    }
}