/*=====================================================================
 * Deloitte India
 * Name: addressController
 * Description: This class is used for invoking address component from Loan application QDE screen.
 * Created Date: [6/8/2019]	
 * Created By: Vaishali Mehta (Deloitte India)
 *
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/
 

public class addressController {
    
    @AuraEnabled  
    public static Map<String,String> getPicklistOptions(String fieldName,String objectName) {
        System.debug('@@ fieldName==>'+fieldName + ' objectName==>'+objectName);
        Map<String,String> picklistoptions = new Map<String,String>();
        
        //get the string to sobject global map
        Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
        
        //get the type being dealt with
        Schema.SObjectType pType = objGlobalMap.get(objectName);
        Map<String, Schema.SObjectField> objFieldMap = pType.getDescribe().fields.getMap();
        Schema.DescribeFieldResult fieldResult = objFieldMap.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            picklistoptions.put(f.getValue() ,f.getLabel());
        }
        System.debug('@@ picklistoptions==>'+picklistoptions);
        return picklistoptions;
    }
    
    @AuraEnabled
    public static List<DataWrapper> getAddresses(Id customerDetailId) {
        system.debug('Hi');
        Map<String, String> resdientTypeValues = getPicklistOptions('Residence_Type__c','Address__c');
        Map<String, String> typeOfAddressValues = getPicklistOptions('Type_of_address__c','Address__c');
            List<Address__c> lstOfAddress = new List<Address__c>();
            lstOfAddress = [SELECT Id, CityLP__c, Type_of_address__c, Residence_Type__c, Address_Line_1__c, Address_Line_2__c, Pincode_LP__c, Loan_Contact__c 
                            FROM Address__c
                            WHERE Loan_Contact__c =: customerDetailId];
            Set<Id> setOfPinCodes = new Set<Id>();
            for(Address__c add :lstOfAddress ) {
                setOfPinCodes.add(add.Pincode_LP__c);
            }                
            Map<Id, Pincode__c> mapOfPinCodes = new Map<Id, Pincode__c>([SELECT Id, Name from Pincode__c WHERE ID IN: setOfPinCodes]);
            
            List<DataWrapper> lstOfWrapper = new List<DataWrapper>();
            if(!lstOfAddress.isEmpty()) {
                for(Address__c address : lstOfAddress) {
                    DataWrapper wrapperObj = new DataWrapper(); 
                    wrapperObj.addressType = address.Type_of_address__c;
                    wrapperObj.resdientType = address.Residence_Type__c;
                    wrapperObj.addressLine1 = address.Address_Line_1__c;
                    wrapperObj.addressLine2 = address.Address_Line_2__c;
                    wrapperObj.pincode = address.Pincode_LP__c;
                    wrapperObj.loanContact = address.Loan_Contact__c;
                    wrapperObj.saveMode = true;
                    wrapperObj.notEditMode = true;
                    wrapperObj.pincodeVal = mapOfPinCodes.get(address.Pincode_LP__c).Name;
                    wrapperObj.resdientTypeName = resdientTypeValues.get(address.Residence_Type__c);
                    wrapperObj.addressTypeName = typeOfAddressValues.get(address.Type_of_address__c);
                    wrapperObj.addressId = address.Id;
                    wrapperObj.city = address.CityLP__c;
                    lstOfWrapper.add(wrapperObj);
                }    
            } else {
                DataWrapper wrapperObj = new DataWrapper();
                lstOfWrapper.add(wrapperObj);
            }
            return lstOfWrapper;
    }
    
    @AuraEnabled
    public Static pincodeObj getPincode() {
        pincodeObj pin = new pincodeObj();
        return pin;
    }
    @AuraEnabled
    public Static List<DataWrapper> addRowToTable(String listOfWrapper) {
        system.debug(listOfWrapper);
        List<DataWrapper> lstOfWrapper = (List<DataWrapper>) JSON.deserialize(listOfWrapper, List<DataWrapper>.class);
        system.debug('lstOfWrapper '+lstOfWrapper);
        DataWrapper wrapperObj = new DataWrapper();
        lstOfWrapper.add(wrapperObj);
        system.debug('lstOfWrapper1'  +lstOfWrapper);
        return lstOfWrapper;
    }
    
    /*@AuraEnabled
    public Static String deleteRowFromTable(String listOfWrapper, Integer index) {
        List<DataWrapper> lstOfWrapper = (List<DataWrapper>) JSON.deserialize(listOfWrapper, List<DataWrapper>.class);
        system.debug('lstOfWrapper '+lstOfWrapper);
        system.debug('index '+index);
        DataWrapper wrapperObjToRemove = lstOfWrapper.remove(index);
        system.debug('wrapperObjToRemove' +wrapperObjToRemove);
        List<Address__c> lstOfAddressToDelete = new List<Address__c>();
        
        if(wrapperObjToRemove.addressId != '') {
            lstOfAddressToDelete = [SELECT Id FROM Address__c WHERE Id =: wrapperObjToRemove.addressId];
        }
        system.debug('lstOfAddressToDelete '+lstOfAddressToDelete);
        if(!lstOfAddressToDelete.isEmpty()) {
            try {
                system.debug('Deleting from DB');
                List<Database.DeleteResult> results = Database.delete(lstOfAddressToDelete , false);
                String errorMessage = '';
    			for ( Integer i = 0; i < results.size(); i++ ) {
        			if ( !results.get(i).isSuccess() ) {
                        for ( Database.Error theError : results.get(i).getErrors() ) {
                             errorMessage += theError.getMessage();
                        }
                        return 'Error: '+ errorMessage;
                    }
    			} 
            } catch(Exception e) {
                system.debug('EXCEPTION OCCURRED'+e);
            }    
            system.debug('ADDED');
            return 'Address deleted successfully';
        }    
        return null;
    } */
 /*   
    @AuraEnabled
    public static MessageWrapper saveAllAddresses(String listOfWrapper, String customerDetailId) {
        
        system.debug('listOfWrapper '+listOfWrapper);
        List<DataWrapper> lstOfWrapper = (List<DataWrapper>) JSON.deserialize(listOfWrapper, List<DataWrapper>.class);
        system.debug('lstOfWrapper '+lstOfWrapper);
        List<Address__c> lstOfAddress = new List<Address__c>();
        if(!lstOfWrapper.isEmpty()) {
            for(DataWrapper wrapperObj : lstOfWrapper) {
                Address__c address = new Address__c();
                if(wrapperObj.addressId != '') {
                  List<Address__c> lstOfaddress1 = [SELECT Id from Address__c WHERE ID =: wrapperObj.addressId];
                  if(!lstOfaddress1.isEmpty()) {
                    address = lstOfaddress1[0];   
                  }
                }
                address.Type_of_address__c = wrapperObj.addressType;
                address.Residence_Type__c= wrapperObj.resdientType;
                address.Address_Line_1__c = wrapperObj.addressLine1;
                address.Address_Line_2__c = wrapperObj.addressLine2;
                address.Pincode_LP__c = wrapperObj.pincode;
                address.Loan_Contact__c = customerDetailId;
                address.BRE_Record__c = true;
                wrapperObj.saveMode = true;
                wrapperObj.notEditMode = true;
                lstOfAddress.add(address);    
            }
        } else {
            MessageWrapper mwrapper = new MessageWrapper('Error: Please enter address', lstOfWrapper);
            return mwrapper;    
        }    
        
        if(!lstOfAddress.isEmpty()) {
            try {
                List<Database.UpsertResult> results = Database.upsert(lstOfAddress , false);
                String errorMessage = '';
    			for ( Integer i = 0; i < results.size(); i++ ) {
        			if ( !results.get(i).isSuccess() ) {
                        for ( Database.Error theError : results.get(i).getErrors() ) {
                             errorMessage += theError.getMessage();
                        }
                        MessageWrapper mwrapper = new MessageWrapper('Error: '+ errorMessage, lstOfWrapper);
                        return mwrapper;
                    } else {
                        lstOfWrapper[i].addressId = lstOfAddress[i].Id; 
                    }
    			}    
            } catch(Exception e) {
                system.debug('EXCEPTION OCCURRED'+e);
            }    
            system.debug('ADDED');
            system.debug('lstOfWrapper '+lstOfWrapper);
            MessageWrapper mwrapper = new MessageWrapper('Address added successfully', lstOfWrapper);
            return mwrapper;
        } else {
            MessageWrapper mwrapper = new MessageWrapper('Error: Please enter address', lstOfWrapper);
            return mwrapper;
        }    
    }
    
    */
    
    
    @AuraEnabled
    public static List<DataWrapper> editAddress(String listOfWrapper, Integer index) {
        String idOfRecord = '';
        system.debug('listOfWrapper '+listOfWrapper);
        system.debug('index '+index);
        List<Address__c> lstOfAddress = new List<Address__c>();
        List<DataWrapper> lstOfWrapper = (List<DataWrapper>) JSON.deserialize(listOfWrapper, List<DataWrapper>.class);
        system.debug('lstOfWrapper '+lstOfWrapper);
        if(lstOfWrapper[index] != null) {
            lstOfWrapper[index].notEditMode = false;
        }
        return lstOfWrapper;
    }  
    
    @AuraEnabled
    public static MessageWrapper saveAddress(String listOfWrapper, Integer index, String customerDetailId) {
        String idOfRecord = '';
        system.debug('listOfWrapper '+listOfWrapper);
        system.debug('index '+index);
        List<Address__c> lstOfAddress = new List<Address__c>();
        DataWrapper lstOfWrapper  = new DataWrapper();
        Address__c address = new Address__c();
        if(listOfWrapper != null ) {
            lstOfWrapper = (DataWrapper) JSON.deserialize(listOfWrapper, DataWrapper.class);
        }
        system.debug('lstOfWrapper '+lstOfWrapper);
        
        if(lstOfWrapper.addressId == '' && lstOfWrapper.addressType == '' && lstOfWrapper.resdientType == '' && lstOfWrapper.addressLine1 == '' && lstOfWrapper.addressLine2 == '' && lstOfWrapper.pincode == '' && lstOfWrapper.pincodeVal == '' && lstOfWrapper.loanContact == '' && lstOfWrapper.notEditMode == true && lstOfWrapper.saveMode == false && lstOfWrapper.resdientTypeName == '' && lstOfWrapper.resdientTypeName == '') {
            MessageWrapper mwrapper = new MessageWrapper('Error: Please enter address', lstOfWrapper);
            return mwrapper;    
        } else {    
            if(lstOfWrapper.addressId != '') {
              List<Address__c> lstOfaddress1 = [SELECT Id,CityLP__c from Address__c WHERE ID =: lstOfWrapper.addressId];
              if(!lstOfaddress1.isEmpty()) {
                address = lstOfaddress1[0];   
              }
            }
            address.Type_of_address__c = lstOfWrapper.addressType;
            address.Residence_Type__c= lstOfWrapper.resdientType;
            address.Address_Line_1__c = lstOfWrapper.addressLine1;
            address.Address_Line_2__c = lstOfWrapper.addressLine2;
            address.Pincode_LP__c = lstOfWrapper.pincode;
             address.Loan_Contact__c = customerDetailId;
             address.BRE_Record__c = true;
            address.BRE__c = true;//Added by Saumya For BRE
            
            lstOfWrapper.saveMode = true;
            lstOfWrapper.notEditMode = true;
            system.debug('address' +address);
                try {
                    Database.UpsertResult results = Database.upsert(address , false);
                    String errorMessage = '';
    				if ( !results.isSuccess() ) {
                        for ( Database.Error theError : results.getErrors() ) {
                             errorMessage += theError.getMessage();
                        }
                        MessageWrapper mwrapper = new MessageWrapper('Error: '+ errorMessage, lstOfWrapper);
                        return mwrapper;
                    } else {
                        lstOfWrapper.addressId = address.Id;
                        lstOfWrapper.city = [SELECT Id, CityLP__c from Address__c WHERE ID=: address.Id limit 1].CityLP__c;
                    } 
                } catch(Exception e) {
                    system.debug('EXCEPTION OCCURRED'+e);
                }    
                system.debug('lstOfWrapper '+lstOfWrapper);
                system.debug('ADDED');
                MessageWrapper mwrapper = new MessageWrapper('Address added successfully', lstOfWrapper);
                system.debug('mwrapper '+mwrapper);
                return mwrapper;
        }    
    }
    
    public class MessageWrapper {
        @AuraEnabled 
        public DataWrapper lstOfWrapper;
        @AuraEnabled
        Public String message;
        
        public MessageWrapper( String message, DataWrapper lstOfWrapper) {
            this.lstOfWrapper = lstOfWrapper;
            this.message = message;
        }
    }
    
    public class DataWrapper {
        @AuraEnabled
        public String addressId;
        @AuraEnabled 
        public String addressType;
        @AuraEnabled 
        public String resdientType;
        @AuraEnabled 
        public String addressLine1;
        @AuraEnabled 
        public String addressLine2;
        @AuraEnabled 
        public String pincode;
        @AuraEnabled
        public String pincodeVal;
        @AuraEnabled 
        public String loanContact;
        @AuraEnabled
        public boolean notEditMode;
        @AuraEnabled
        public boolean saveMode;
        @AuraEnabled
        public String addressTypeName;
        @AuraEnabled
        public String resdientTypeName;
        @AuraEnabled 
        public String city;
        
        public DataWrapper(){
            this.addressId = '';
            this.addressType ='';
            this.resdientType ='';
            this.addressLine1 = '';
            this.addressLine2 ='';
            this.pincode= '';
            this.pincodeVal = '';
            this.loanContact = '';
            this.notEditMode = true;
            this.saveMode = false;
            this.resdientTypeName = '';
            this.addressTypeName ='';
            this.city = '';
           
        }
    }
    
    public class pincodeObj {
        @AuraEnabled
        public string val;
        @AuraEnabled 
        public String text;
        @AuraEnabled
        public object PAN;
        @AuraEnabled
        public String objName;
        
        public pincodeObj() {
            this.val = '';
            this.text = '';
            this.PAN = null;
            this.objName = 'Pincode__c';
        } 
    }
}