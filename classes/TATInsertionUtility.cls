public class TATInsertionUtility {
	public static Boolean isTATinsertion = true; //Added by Ashima for conditional calling of TATinsertion method
	public static Boolean StageTATinsertion = true; //Added by Ashima for conditional calling of StageTATinsertion method


    public static void TATinsertion(list < id > laList) {

        list < TAT_Management__c > tatInsert = new list < TAT_Management__c > ();
        list < Loan_Application__c > appList = new list < Loan_Application__c > ();
        Date dt = LMSDateUtility.lmsDateValue;
        system.debug('Date========>>>>>>>>' + dt);

        list < TAT_Management__c > tatavailable = new list < TAT_Management__c > ();     

        tatavailable = [select id, Loan_Application__c, TAT_Type__c, Stage__c , SubStage__c, end_time__c, start_time__c from TAT_Management__c
            where loan_application__c in: laList and end_time__c = null
            and TAT_Type__c = 'Sub Stage'
        ];


        if (tatavailable.size() > 0) {
            for (TAT_Management__c tat: tatavailable) {
                tat.End_Time__c = system.now();
                tat.Business_End_Date__c = (Date) dt;
                tatInsert.add(tat);
            }
        }

        appList = [select id, StageName__c, Sub_Stage__c,Line_Of_Business__c, scheme__c,Product__c,
        Applicant_Customer_segment__c, Assigned_Sales_User__r.Name, owner.Name from Loan_Application__c 
        where id in: laList];
        list<string> substagelist = new list<string>();
        list<string> loblist = new list<string>();
        list<string> schemelist = new list<string>();
        list<string> prodlist = new list<string>();
        list<string> cusSEgList = new list<string>();
        
        for(loan_application__c la: appList){
                substagelist.add(la.Sub_Stage__c);
                loblist.add(la.Line_Of_Business__c);
                schemelist.add(la.scheme__c);
                prodlist.add(la.Product__c);
                cusSEgList.add(la.Applicant_Customer_segment__c);

        }

        system.debug('===substagelist=='+substagelist);
        system.debug('===loblist=='+loblist);
        system.debug('===schemelist=='+schemelist);
        system.debug('===prodlist=='+prodlist);
        system.debug('===cusSEgList=='+cusSEgList);

        list<TAT_Matrix__c> tatValue = [select Customer_Segment__c,Green__c, amber__c, tat_type__c,LOB__c,
        Scheme__c,SubStage__c,Product__c,TAT_time__c  from TAT_Matrix__c
        where Customer_Segment__c in :cusSEgList and LOB__c in : loblist 
        //and scheme__c in :schemelist
         and Product__c in :prodlist
         and SubStage__c in :substagelist
         and tat_type__c = 'Sub Stage'];

system.debug('===tatvalue=='+tatValue);
        for (loan_application__c la: appList) {
            for(TAT_Matrix__c tatm : tatValue){
                if((tatm.Customer_Segment__c == la.Applicant_Customer_segment__c)&&
                    (tatm.LOB__c == la.Line_Of_Business__c)
                    &&(tatm.Product__c == la.product__c)){

            TAT_Management__c tat = new TAT_Management__c();
            tat.Loan_Application__c = la.id;
            tat.Stage__c = la.StageName__c;
            tat.SubStage__c = la.sub_stage__c;
            tat.Start_Time__c = system.now();
            
            tat.St__c= '1996-01-01'+' '+System.now().format('HH:mm:ss');
            tat.Max_TAT__c = tatm.amber__c ;
            tat.Green__c = tatm.Green__c ;
            tat.TAT_Type__c = 'Sub Stage';
            tat.Business_Start_Date__c = (Date) dt;
            if (la.sub_stage__c != constants.Docket_Checker) {
                tat.User__c = la.Owner.Name;
            } else tat.User__c = la.Assigned_Sales_User__r.Name;
            if(String.valueOf(la.OwnerId).startsWith('005')){
                tat.user_type__c = 'User';
            }else tat.user_Type__c = 'Queue';

            if(la.sub_stage__c == 'COPS:Data Maker')
                tat.Effort_Weight__c = 8.3;
            else if(la.sub_stage__c == 'COPS:Data Checker')
                tat.Effort_Weight__c = 4.2;
            else if(la.sub_stage__c == 'Disbursement Maker')
                tat.Effort_Weight__c = 9.4;
            else if(la.sub_stage__c == 'Disbursement Checker')
                tat.Effort_Weight__c = 6.3;
            else if(la.sub_stage__c == 'CPC Scan Maker' || la.sub_stage__c == 'CPC Scan Checker')
                tat.Effort_Weight__c = 3.1;
            else if(la.sub_stage__c == 'CPC Data Maker')
                tat.Effort_Weight__c = 9.4;
            else if(la.sub_stage__c == 'COPS: Credit Operations Entry' && (la.Applicant_Customer_segment__c == '1'||la.Applicant_Customer_segment__c == '14' )  )
                tat.Effort_Weight__c = 3.1;
            else if(la.sub_stage__c == 'COPS: Credit Operations Entry' && la.Applicant_Customer_segment__c != '1' )
                tat.Effort_Weight__c = 25.0;
			//**********************Added by Chitransh for TAT [27-01-2020]************************//
            else if(la.sub_stage__c == 'Tranche Disbursement Maker')
                tat.Effort_Weight__c = 9.4;
            else if(la.sub_stage__c == 'Tranche Disbursement Checker')
                tat.Effort_Weight__c = 6.3;
          //**********************End of patch added by Chitransh for TAT [27-01-2020]************************//

            tatInsert.add(tat);

}
}


        }
system.debug(tatInsert+'=====tatInsert==');
        upsert tatInsert;

    }


    public static void StageTATinsertion(list < id > laList) {

        list < TAT_Management__c > tatInsert = new list < TAT_Management__c > ();
        list < Loan_Application__c > appList = new list < Loan_Application__c > ();
        Date dt = LMSDateUtility.lmsDateValue;
        system.debug('Date========>>>>>>>>' + dt);
        list < TAT_Management__c > tatUp = new list < TAT_Management__c > ();
        tatUp = [select id, Loan_Application__c, TAT_Type__c,Stage__c ,  SubStage__c, end_time__c, start_time__c from TAT_Management__c
            where loan_application__c in: laList
            and TAT_Type__c = 'Stage'
        ];


 appList = [select id, StageName__c, Line_Of_Business__c, scheme__c,product__c,
        Applicant_Customer_segment__c, Assigned_Sales_User__r.Name, owner.Name from Loan_Application__c 
        where id in: laList];
        list<string> stagelist = new list<string>();
        list<string> loblist = new list<string>();
        list<string> schemelist = new list<string>();
        list<string> prodlist = new list<string>();
        list<string> cusSEgList = new list<string>();
        
        for(loan_application__c la: appList){
                stagelist.add(la.StageName__c);
                loblist.add(la.Line_Of_Business__c);
              //  schemelist.add(la.scheme__c);
                prodlist.add(la.product__c);
                cusSEgList.add(la.Applicant_Customer_segment__c);

        }

list<TAT_Matrix__c> tatValue = [select Customer_Segment__c, Green__c, amber__c,tat_type__c,LOB__c,Scheme__c,SubStage__c,Product__c,TAT_time__c  from TAT_Matrix__c
        where Customer_Segment__c in :cusSEgList and LOB__c in : loblist 
         and Product__c in :prodlist
         and Stage__c in :stagelist
         and tat_type__c = 'Stage'];
      
       for (loan_application__c la: appList) {
            for(TAT_Matrix__c tatm : tatValue){
                if((tatm.Customer_Segment__c == la.Applicant_Customer_segment__c)&&
                    (tatm.LOB__c == la.Line_Of_Business__c)
                    &&(tatm.Product__c == la.product__c)){



            TAT_Management__c tat = new TAT_Management__c();
            tat.Loan_Application__c = la.id;
            tat.Stage__c = la.StageName__c;
            //tat.SubStage__c= la.sub_stage__c;
            tat.Start_Time__c = system.now();
            tat.St__c= '1996-01-01'+' '+System.now().format('HH:mm:ss');
            tat.Max_TAT__c = tatm.amber__c ;
            tat.Green__c = tatm.Green__c ;
            tat.TAT_Type__c = 'Stage';
            tat.Business_Start_Date__c = (Date) dt;

            tatInsert.add(tat);
            }}

            for (TAT_Management__c tat1: tatUp) {
if ( (tat1.Loan_Application__c == la.id) && tat1.end_time__c == null) {
                    tat1.End_Time__c = system.now();
                    tat1.Business_End_Date__c = (Date) dt;
                    tatInsert.add(tat1);
                }
            }



        }


      
        upsert tatInsert;

    }




}