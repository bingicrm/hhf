public class EnqueueDMLGSTINOneAPF implements Queueable {
    public Builder_Integrations__c objBuilderIntegration;
    public Promoter_Integrations__c objPromoterIntegration;
    public String strRequest;
    public String strResponse;
    public String fillingFreq;
    public String strURL;
    public EnqueueDMLGSTINOneAPF(Builder_Integrations__c objBuilderIntegration, Promoter_Integrations__c objPromoterIntegration, String fillingFreq, String strRequest, String strResponse, String strURL){
        if(objBuilderIntegration != null) {
            system.debug('Debug for objBuilderIntegration' + objBuilderIntegration);
        }
        if(objPromoterIntegration != null) {
            system.debug('Debug for objPromoterIntegration' + objPromoterIntegration);
        }
        system.debug('Debug for fillingFreq' + fillingFreq);
        system.debug('Debug for strRequest1' + strRequest);
        system.debug('Debug for strResponse1' + strResponse);
        system.debug('Debug for strURL1' + strURL);
        this.objBuilderIntegration = objBuilderIntegration;
        this.objPromoterIntegration = objPromoterIntegration;
        this.strRequest = strRequest;
        this.strResponse = strResponse;
        this.strURL = strURL;
        this.fillingFreq = fillingFreq;
    }
    public void execute(QueueableContext context) {
      Customer_Integration_For_GSTIN__c passValidationCheck =  Customer_Integration_For_GSTIN__c.getInstance();
      
        if(objBuilderIntegration != null && objBuilderIntegration.Id != null){
            passValidationCheck.CI_GSTIN_Editable__c = true;
            upsert passValidationCheck; 
            objBuilderIntegration.Filling_Frequency__c = fillingFreq;
            update objBuilderIntegration;
            if(passValidationCheck.CI_GSTIN_Editable__c){
                passValidationCheck.CI_GSTIN_Editable__c = false;
            upsert passValidationCheck; 
            }
            system.debug('GSTRecord.FillingFreq  ' + objBuilderIntegration.Filling_Frequency__c  );
        }
        if(objBuilderIntegration != null && objBuilderIntegration.Project_Builder__c != null){
            Project_Builder__c objProjBuilder = [Select id,GST_ReturnStatus__c,GST_Initiated__c from Project_Builder__c where id =: objBuilderIntegration.Project_Builder__c limit 1];
            if(objProjBuilder != null && objProjBuilder.Id != null){ 
                objProjBuilder.GST_ReturnStatus__c = true;
                update objProjBuilder;
            }
        }
        
        if(objPromoterIntegration != null && objPromoterIntegration.Id != null) {
            passValidationCheck.CI_GSTIN_Editable__c = true;
            upsert passValidationCheck; 
            objPromoterIntegration.Filling_Frequency__c = fillingFreq;
            update objPromoterIntegration;
            if(passValidationCheck.CI_GSTIN_Editable__c){
                passValidationCheck.CI_GSTIN_Editable__c = false;
            upsert passValidationCheck; 
            }
            system.debug('GSTRecord.FillingFreq  ' + objPromoterIntegration.Filling_Frequency__c  );
        }
        
        if(objPromoterIntegration != null && objPromoterIntegration.Promoter__c != null){
            Promoter__c objPromoter = [Select id,GST_ReturnStatus__c,GST_Initiated__c from Promoter__c where id =: objPromoterIntegration.Promoter__c limit 1];
            if(objPromoter != null && objPromoter.Id != null){ 
                objPromoter.GST_ReturnStatus__c = true;
                update objPromoter;
            }
        }
       
        HttpResponse res = new HttpResponse();
        res.setBody(strResponse);  
        LogUtility.createIntegrationLogs(strRequest,res,strURL);
    }
}