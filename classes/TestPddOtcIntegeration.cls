@isTest(SeeAllData=true)
public class TestPddOtcIntegeration {

public static testmethod void method1(){

  Account acc=new Account();
    acc.name='Pink Foyd';
    acc.phone='9517204825';
    acc.PAN__c='tjaoe7492a';
    //acc.recordtypeid='0120l000000HtAN';
    //acc.PersonEmail='abcefh@gmail.com';
    acc.Date_of_Incorporation__c=date.today();
    insert acc;

//System.debug(acc.recordtype.name);

Scheme__c sc=new Scheme__c();
sc.name='TestScheme';
sc.Scheme_Code__c='testcode';
sc.Scheme_Group_ID__c='23131';
sc.Product_Code__c='HL';
insert sc;
System.debug(sc.id);

Loan_Application__c lap=new Loan_Application__c ();

lap.Customer__c=acc.id;
lap.Transaction_type__c='DAA';
lap.Scheme__c=sc.id;
lap.Approved_Loan_Amount__c=5000000;
lap.Transaction_type__c='PB';
insert lap;
System.debug(lap.id);

List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
lstofcon[0].VoterID_Address__c='ascsd ascfsd';
update lstofcon;
//List<Document_Checklist__c> ldc=[select id,Document_Type__c, Loan_Contact__c, Loan_Applications__r.Name, Loan_Contact__r.Name, Name, Document_Type__r.Name, Current_Stage__c, Loan_Contact__r.VoterID_Address__c from Document_Checklist__c where Loan_Applications__c =:lap.id limit 1];
//System.debug(ldc);
//String idofrec=ldc[0].id;

Document_Type__c dt=new Document_Type__c();
dt.name='Any type';
dt.Document_Type_Id__c='asfmsdkvns';
dt.Expiration_Possible__c=true;
dt.OTC__c=true;
insert dt;


Document_Checklist__c dc=new Document_Checklist__c();
dc.Loan_Applications__c=lap.id;
dc.Loan_Contact__c=lstofcon[0].id;
dc.Document_Type__c=dt.id;
insert dc;
Test.startTest();
 Map<String, object> ResponseMap = new Map<String, object>();
Object obj =date.today();
ResponseMap.put('asdasdada',obj);                        
TestMockRequest req1=new TestMockRequest(200,'Complete',JSON.serialize(ResponseMap),null);
Test.setMock(HttpCalloutMock.class,req1);
System.assertEquals(req1.code,200);

Test.stopTest();



PddOtcIntegeration.PlaceRequest(dc.id);
}

}