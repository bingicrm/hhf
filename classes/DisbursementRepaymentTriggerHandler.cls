public class DisbursementRepaymentTriggerHandler {
    
    public static void beforeInsert(List < Disbursement_payment_details__c> newList) {
        List<LMS_Date_Sync__c> lstLMSDateSync = [SELECT Id, Name, CreatedDate, Current_Date__c FROM LMS_Date_Sync__c ORDER BY CreatedDate DESC];
        System.debug('Debug Log for lstLMSDateSync'+lstLMSDateSync);
        for(Disbursement_payment_details__c obj: newList) {
            if(String.isNotBlank(String.valueOf(obj.Cheque_Date__c)) && obj.Cheque_Date__c < lstLMSDateSync[0].Current_Date__c) {
                obj.addError('Cheque Date Cannot be less than the Business Date. Please correct data.');
            }
        }
        
    }
    
    
    
    public static void afterInsert(List < Disbursement_payment_details__c> newList) {
        
        
        
        list<Disbursement_payment_details__c> disburseList = new list<Disbursement_payment_details__c>();
        disburseList= [select id, Disbursement__c, Disbursement__r.Loan_Application__c ,Disbursement__r.Loan_Application__r.Favouring_details_entered__c from Disbursement_payment_details__c
                       where id in : newList];
        
        list<loan_application__c> loanListUpdate = new list<loan_application__c>();
        for(Disbursement_payment_details__c dispay:disburseList ){
            loan_application__c la = new loan_application__c();
            la.id= dispay.Disbursement__r.Loan_Application__c;
            la.Favouring_details_entered__c = true;
            loanListUpdate.add(la);
            
            
        }
        
        
        if(loanListUpdate.size() > 0)
            update loanListUpdate;
        
        
    }
    public static void beforeUpdate(List < Disbursement_payment_details__c> newList, List < Disbursement_payment_details__c> oldList, Map<Id,Disbursement_payment_details__c> newMap,  Map<Id,Disbursement_payment_details__c> oldMap) {
        List<LMS_Date_Sync__c> lstLMSDateSync = [SELECT Id, Name, CreatedDate, Current_Date__c FROM LMS_Date_Sync__c ORDER BY CreatedDate DESC];
        System.debug('Debug Log for lstLMSDateSync'+lstLMSDateSync);
        for(Disbursement_payment_details__c obj: newList) {
            if(String.isNotBlank(String.valueOf(obj.Cheque_Date__c)) && obj.Cheque_Date__c < lstLMSDateSync[0].Current_Date__c) {
                obj.addError('Cheque Date Cannot be less than the Business Date. Please correct data.');
            }
        }
    }
    
}