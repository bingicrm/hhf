/*=====================================================================
 * Deloitte India
 * Name: TestclsCheckExistingCustomer
 * Description: This class is a test class for clsCheckExistingCustomer.
 * Created Date: [01/09/2019]
 * Created By: Gaurav Nawal (Deloitte India)
 *
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/
 @isTest
public class Test_TaskTriggerHandler {
	@testSetup
    public static void init() {
    	Scheme__c sc = new Scheme__c(Name = 'Construction Finance', Scheme_Code__c = 'CF', Scheme_Group_ID__c = '5',
									Scheme_ID__c = 10,	Product_Code__c='CF', Max_ROI__c = 10, Min_ROI__c = 5,
									Min_Loan_Amount__c = 10000, Min_FOIR__c = 1, Max_Loan_Amount__c = 100000,
									Max_Tenure__c = 5, Min_Tenure__c = 1, Max_LTV__c = 5, Min_LTV__c = 1, Max_FOIR__c = 5);
		insert sc;
		Account corpCust = new Account(PAN__c='AXEGA3767M', Name='AccountTest', Phone='9992345933', 
										Date_of_Incorporation__c = Date.newInstance(2018,11,11));            
        insert corpCust;
    	List<Loan_Application__c> lstLoanApps = new List<Loan_Application__c> ();
		Loan_Application__c la = new Loan_Application__c(Customer__c = corpCust.Id, Scheme__c = sc.Id, 
														StageName__c = 'Customer Onboarding', 
														Transaction_type__c = 'TOP', Requested_Amount__c = 10000,
														Applicant_Customer_Segment__c = '1', Branch__c = 'test', 
														Line_Of_Business__c = 'Open Market', Loan_Purpose__c = '20',
														Requested_EMI_amount__c = 1000, Approved_ROI__c = 5, 
														Requested_Loan_Tenure__c = 5, Government_Programs__c = 'PMAY',
														Loan_Engine_2_Output__c = 'STP', Property_Identified__c = true, 
														Existing_Loan_Application_Number__c = '12334', Outstanding_Loan_amount__c = 10000,
														Name_of_BT_Bank__c = 'TST', Loan_Number__c = '12121212121');

		lstLoanApps.add(la);
		insert lstLoanApps;
    }

    public static testMethod void testInsertMethod() {
    	Loan_Application__c la = [SELECT Id FROM Loan_Application__c LIMIT 1][0];
    	Task t = new Task();
    	t.OwnerId = UserInfo.getUserId();
    	t.WhatId = la.Id;
    	Test.startTest();
    	insert t;
    	t.Status = 'Completed';
    	update t;
    	Test.stopTest();
    }
}