public class IntegrationFrameWork {    
    
    public static String returnJSON(String strAPIName, String strObject, String strObjectId){
        
        /*strAPIName = 'PddOtc';
        strObject = 'loan_application__c';
        strObjectId = 'a00p0000006uRPQ';
        */
        
      Rest_Service__mdt restService = [SELECT 
                                      MasterLabel, 
                                      QualifiedApiName, 
                                      Service_EndPoint__c, 
                                      Client_Password__c, 
                                      Client_Username__c, 
                                      Request_Method__c, 
                                      Time_Out_Period__c  
                                FROM   
                                      Rest_Service__mdt
                                WHERE  
                                      MasterLabel= :strAPIName
                                limit 1      
                              ]; 
        
        map<string, object> mapToSerialize = new map<string, object>();
        String strQueryMetadata = 'select tag_name__c, Is_List__c,Field_API_Name__c,Object__c,Service_Name__c from  Integration_Framework__mdt';
        system.debug('@@query meta' + strQueryMetadata );
        list<Integration_Framework__mdt> lstInt= database.query(strQueryMetadata );
        map<string,list<string>> mapchildToField = new map<string,list<string>>();
        string strInnerQuery ='';
            
        
        string strquery = 'select ';
        for (Integration_Framework__mdt objInt : lstInt ){
            if (objInt.Service_Name__c == strAPIName && !objInt.Object__c.contains('FUNCTIONCALL') && !objInt.Object__c.contains('CONSTANT')) {
                system.debug('objInt@@@' + objInt);
                 system.debug('mapchildToField@@@' + mapchildToField);
                if (objInt.Object__c.contains('__r')){
                    if (mapchildToField.containsKey(objInt.Object__c)){
                        mapchildToField.get(objInt.Object__c).add(objInt.Field_API_Name__c);
                    } else {
                        mapchildToField.put(objInt.Object__c,new list<string>{objInt.Field_API_Name__c});
                    }
                } else {
                        if (objInt.Field_API_Name__c != null)
                        strQuery+=objInt.Field_API_Name__c +  ',';
                    
                }
            }
        }
        system.debug('@@mapchildToField' +mapchildToField);
        
        for(String strObjName : mapchildToField.keySet()){
            strInnerQuery = '(select id,';
            for (String strFieldName : mapchildToField.get(strObjName)) {
                strInnerQuery+= +strFieldName +',';
            }
            strinnerquery = strinnerQuery.removeEnd(',');
            strinnerquery+=' from '+ strObjName +')';
        }
        system.debug('strinnerquery@@@' + strinnerquery);
        if(strinnerquery == '')
        strquery = strquery.removeEnd(',');
        if (strAPIName == 'IMDCallout') {
            strquery+=',Receipt_Date__c,Cheque_Date__c';
        }
        if (strAPIName == 'LMS Loan Application Id') {
            strquery+=',Name';
        }
        strquery+=strinnerquery+' from '+strObject+' where id= \''+strObjectId+'\'';
       
        system.debug(strquery);
        list<Sobject> lstLoan ;
        if(strObjectId == null) {
            lstLoan = new List<Sobject>();
        } else {
            lstLoan = Database.query(strQuery);
        }
        String strCalculated ;
        for ( Integration_Framework__mdt objInt : lstInt){
            if(objInt.Service_Name__c != null && objInt.Service_Name__c == strAPIName) {
                if (!objInt.Object__c.contains('__r')){
                    if (objInt.Field_API_Name__c != null && !objInt.Field_API_Name__c.contains('__r')) {
                        system.debug('objInt in main @@'  +objInt);
                        if (objInt.Object__c != null && objInt.Object__c.equals('CONSTANT')) {
                            mapToSerialize.put(objInt.tag_name__c,objInt.Field_API_Name__c);
                        } else if (objInt.Object__c != null && objInt.Object__c.contains('FUNCTIONCALL')) {
                            string strTobeCalculated = objInt.Field_API_Name__c != 'none' ? String.valueOf(lstLoan[0].get(objInt.Field_API_Name__c)) : 'none';
                            system.debug('@@@@object__c in function' + objInt.Object__c);
                            system.debug('@@@@strTobeCalculated in function' + strTobeCalculated);
                            strCalculated = calculateString(strTobeCalculated,objInt.Object__c.replace('$',',').split(',')[1]);
                            if ( strCalculated != null )
                            mapToSerialize.put(objInt.tag_name__c,strCalculated);
                            else
                            mapToSerialize.put(objInt.tag_name__c,'');
                        } else {
                            system.debug('objInt in else @@'  + lstLoan[0].get(objInt.Field_API_Name__c));
                            if ( lstLoan[0].get(objInt.Field_API_Name__c) != null)
                            mapToSerialize.put(objInt.tag_name__c, String.valueOf(lstLoan[0].get(objInt.Field_API_Name__c)));
                            else
                            mapToSerialize.put(objInt.tag_name__c,'');
                        }
                        
                    } else if(objInt.Field_API_Name__c != null){
                        system.debug('objInt @@' + objInt);
                        string str = objInt.Field_API_Name__c.replace('.',',');
                        system.debug('lstLoan[0] @@@' + lstLoan[0]);
                        if ( lstLoan[0].getSobject(str.split(',')[0]) != null && lstLoan[0].getSobject(str.split(',')[0]).get(str.split(',')[1]) != null)
                       // system.debug('lstLoan[0].getSobject(str.split(',')[0])' + lstLoan[0].getSobject(str.split(',')[0]) );
                        mapToSerialize.put(objInt.tag_name__c, lstLoan[0].getSobject(str.split(',')[0]).get(str.split(',')[1]));
                        else
                        mapToSerialize.put(objInt.tag_name__c, '');
                    } else {
                        mapToSerialize.put(objInt.tag_name__c, '');
                    }
                    
                } else{
                    if (objInt.Field_API_Name__c != null && !objInt.Field_API_Name__c.contains('__r')) {
                        if (objInt.Is_List__c){
                            list<wrapper> lstStr = new List<wrapper>();
                            wrapper objwrap;
                            string strlabel = objInt.tag_name__c.substringAfterLast('_');
                           
                            system.debug('strlabel@@' + strlabel);
                            for(SObject obj : lstLoan[0].getSobjects(objInt.Object__c)){
                                objWrap = new Wrapper();
                                objwrap.strVar = String.valueOf(obj.get(objInt.Field_API_Name__c)); 
                                lstStr.add(objWrap);
                            }
                            string strjson = JSON.serialize(lstStr);
                                
                            strjson = strjson.replaceAll('strVar',strlabel);
                            system.debug('@@list json' + strjson);
                            list<object> lstobj= (List<object>)JSON.deserializeUntyped(strjson);       
                            mapToSerialize.put(objInt.tag_name__c.substringBeforeLast('_'),lstobj);
                        } else{
                            if ( lstLoan[0].getSobjects(objInt.Object__c).get(0).get(objInt.Field_API_Name__c) != null )
                            mapToSerialize.put(objInt.tag_name__c, String.valueOf(lstLoan[0].getSobjects(objInt.Object__c).get(0).get(objInt.Field_API_Name__c)));  
                            else
                            mapToSerialize.put(objInt.tag_name__c, '');
                        }
                    } else if (objInt.Field_API_Name__c != null) {
                       string str = objInt.Field_API_Name__c.replace('.',',');
                       if (objInt.Is_List__c){
                            list<wrapper> lstStr = new List<wrapper>();
                            wrapper objwrap;
                            string strlabel = objInt.tag_name__c.substringAfterLast('_');
                           
                            system.debug('strlabel@@' + strlabel);
                            for(SObject obj : lstLoan[0].getSobjects(objInt.Object__c)){
                                objWrap = new Wrapper();
                                objwrap.strVar = String.valueOf(obj.getSobject(str.split(',')[0]).get(str.split(',')[1])); 
                                lstStr.add(objWrap);
                            }
                            string strjson = JSON.serialize(lstStr);
                                
                            strjson = strjson.replaceAll('strVar',strlabel);
                            system.debug('@@list json' + strjson);
                            list<object> lstobj= (List<object>)JSON.deserializeUntyped(strjson);       
                            mapToSerialize.put(objInt.tag_name__c.substringBeforeLast('_'),lstobj);
                        } else{ 
                            if (lstLoan[0].getSobjects(objInt.Object__c).get(0).getSobject(str.split(',')[0]).get(str.split(',')[1]) != null)
                            mapToSerialize.put(objInt.tag_name__c, lstLoan[0].getSobjects(objInt.Object__c).get(0).getSobject(str.split(',')[0]).get(str.split(',')[1]));
                            else 
                            mapToSerialize.put(objInt.tag_name__c, '');
                        }
                    }
                }
            }
            if (objInt.Service_Name__c == 'OtcPdd') {
                mapToSerialize.put('userid','cointrive');
                mapToSerialize.put('password','zqbAx8rZ0LvWMftg38eTatwjEANYAo/6');
            }
            if (objInt.Service_Name__c == 'LMS Loan Application Id') {
                mapToSerialize.put('UserId','cointrive');
                mapToSerialize.put('Password','zqbAx8rZ0LvWMftg38eTatwjEANYAo/6');
            }
        }
        
        string jsonstring = JSON.serializePretty(mapToSerialize);
        
        system.debug('Request String'+jsonstring);
        return jsonstring;
    }
    
    class wrapper{
        string strVar;
    }
    
    public static String calculateString(String strVal, String strFuncName){
        system.debug('_____________strVal' + strVal + strFuncName);
        if ( strFuncName == 'Utility.generateRandomRequestID') {
            return Utility.generateRandomRequestID();
        }
        else if ( strFuncName == 'getUpdatedDate(system.today())') {
            return OtcPddIntegration.getUpdatedDate(system.today());
        } 
        else if ( strFuncName == 'UserInfo.getFirstName()') {
            return UserInfo.getFirstName();
        } 
        else if ( strFuncName == 'formatDate()') {
            if(strVal != null) {
                return IMDCallout.formatDate(Date.valueOf(strVal));
            } else  {
                return '';
            }
        }
        else if ( strFuncName.contains('Utility.generateRandomRequestID().substring') ) {
            return Utility.generateRandomRequestID().substring(13,17);
        }
        else if (strFuncName == 'system.now()') {
            return String.valueOf(System.now());
        } 
        else if (strFuncName == 'substring()') {
            return strVal.substring(3, strVal.length()-1);
        }
        else {
            return null;  
        }
    }
    
}