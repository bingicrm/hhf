@isTest
public class TestTaskController {
 @TestSetup
    static void setup()
    {
        Task t = new Task();
        t.OwnerId = UserInfo.getUserId();
        t.Subject='Donni';
        t.Status='Not Started';
        t.Priority='Normal';
        insert t;       
    }
    @isTest
    public static void TestLoadTasks(){
        List<task> task=[SELECT Subject, ActivityDate FROM Task limit 1];
        TaskController.loadTasks(task[0].id);
        TaskController.saveTask(task[0]);
    }
}