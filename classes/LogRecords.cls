/**
 *    @author          : Deloitte Digital - SFDC
 *    @date            : 05/02/2016
 *    @description     : Class for showing 'LOG' records.
 *    Modification Log : 
 *    ------------------------------------------------------------------------------------     
 *    Developer                       Date                Description      
 *    ------------------------------------------------------------------------------------      
 *    Shilpa Menghani                05/02/2016            Original Version
 */
public without sharing class LogRecords {
   
    /**
    * Current Mapping information 
    */
    public static string sobjectAPIName {get;set;}
    
        
    /**
     * Constructor 
     */
    public LogRecords(){
        sobjectAPIName = LogHelper.getLogObjectName();
        if(String.isNotBlank(sobjectAPIName) && sobjectAPIName.equalsIgnoreCase('Task'))
        sObjectAPIName = 'Activity';
    }
 }