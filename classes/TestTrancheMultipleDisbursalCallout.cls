@isTest
public class TestTrancheMultipleDisbursalCallout {
	@testSetup static void testData() {
    	Account acc=new Account();
        acc.name='TestDFGWSEFS AccoountASFSADFS for trigger';
        acc.phone='9666622222';
        acc.PAN__c='DBUIE8289A';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        Scheme__c sc=new Scheme__c();
        sc.name='Construction Finance';
        sc.Scheme_Code__c='CF';
        sc.Scheme_Group_ID__c='6';
        sc.Product_Code__c='HL';
        insert sc;
        
        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Transaction_type__c='DAA';
        lap.Scheme__c=sc.id;
        lap.recordtypeid= Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Readonly Record').getRecordTypeId();
        lap.Approved_Loan_Amount__c=5000000;
        lap.Requested_Amount__c=50000;
        lap.StageName__c='Customer Onboarding';
        lap.Property_Identified__c = True;
        lap.Property_Entered__c = FALSE;
        insert lap;
        List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
        
        Document_Master__c dm=new Document_Master__c();
        dm.name='test';
        dm.Doc_Id__c='asdfsdf';
        dm.Expiration_Possible__c=true;
        dm.FCU_Required__c=true;
        dm.OTC__c=true;
        insert dm;
        
        Document_Type__c dt=new Document_Type__c();
        dt.name='Any type';
        dt.Document_Type_Id__c='asfmsdkvns';
        dt.Expiration_Possible__c=true;
        dt.OTC__c=true;
        insert dt;
        
        Tranche__c tr=new Tranche__c();
        tr.Loan_Application__c=lap.id;
        tr.Disbursal_Amount__c=50100;
        tr.Request_Type__c= Constants.TRANCHEDISBURSAL;
        tr.Repayment_Start_Date__c = system.today();
        tr.Tranche_Stage__c = 'Tranche Disbursement Checker';
        insert tr;
        
        tr.Request_Type__c = 'Tranche Disbursal2';  
        update tr;
        
        Document_Checklist__c dc=new Document_Checklist__c();
        dc.Loan_Applications__c=lap.id;
        dc.Document_Type__c=dt.id;
        dc.Document_Master__c=dm.id;
        dc.REquest_Date_for_OTC__c=date.today();
        dc.Document_Collection_Mode__c='Photocopy';
        dc.Loan_Engine_Mandatory__c=true;
        dc.Express_Queue_Mandatory__c=true;
        dc.status__c='Pending';
        dc.File_Check_Completed__c=true;
        dc.Scan_Check_Completed__c=true;
        dc.Tranche__c=tr.id;
        dc.Original_Seen_and_Verified__c=true;
        insert dc;
        
        Loan_Repayment__c objLoanRp = new Loan_Repayment__c(Broken_Period_Interest_Handing__c = 'Return as charge',Loan_Application__c=lap.id,
                                                           dueDay__c=System.today().day());
        insert objLoanRp;    
    } 
    
    @isTest    
    public static void makeTrancheMultipleDisbursalCalloutTest(){
        id loanAppRecType = Schema.SObjectType.loan_application__c.getRecordTypeInfosByName().get('Tranche').getRecordTypeId();
    	Loan_Application__c lap2 = [SELECT Id FROM Loan_Application__c LIMIT 1];
        
         Account custObj = [SELECT Id FROM Account LIMIT 1];
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        database.insert(schemeObj);
        
         
        Loan_Application__c lap = new Loan_Application__c();
        lap.RecordTypeId = loanAppRecType;
        lap.customer__c = custObj.id;
        //loanAppObj.Loan_Application_Number__c = 'LA00001';
        lap.Loan_Purpose__c = '11';
        lap.scheme__c = schemeObj.id;
        lap.Transaction_type__c = 'PB';
        lap.Requested_Amount__c = 200000;
        lap.Approved_Loan_Amount__c = 150000;
        lap.StageName__c = 'Loan Disbursal';
        lap.sub_stage__c = 'Application Initiation';
        insert lap;
        
        Test.startTest();
        Tranche__c tr=new Tranche__c();
        tr.Loan_Application__c=lap.id;
        tr.Disbursal_Amount__c=50100;
        tr.Request_Type__c= Constants.TRANCHEDISBURSAL;
        tr.Repayment_Start_Date__c = system.today();
        tr.Tranche_Stage__c = 'Tranche Disbursement Checker';
        insert tr;
        
        tr.Request_Type__c = 'Tranche Disbursal2';  
        update tr;
        //Tranche__c tr = [SELECT Id FROM Tranche__c WHERE Loan_Application__c =:lap.Id LIMIT 1]; 
        Disbursement__c dObj = new Disbursement__c(Loan_Application__c=lap.Id, Tranche__c=tr.Id, Disbursal_Amount__c=150000);
        insert dObj;
        //Disbursement_payment_details__c dpObj = new Disbursement_payment_details__c();
        //insert dpObj;
        String result = TrancheMultipleDisbursalCallout.makeTrancheMultipleDisbursalCallout(tr.Id);
        Test.stopTest();
    }
}