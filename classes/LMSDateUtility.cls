public class LMSDateUtility {
	public static Date lmsDateValue {
          get {
               if (lmsDateValue == null) {
                    List<LMS_Date_Sync__c> lstBusinessDates = [SELECT Id, Name, Current_Date__c 
																FROM LMS_Date_Sync__c 
																ORDER BY Name 
																DESC LIMIT 1];
					if(!lstBusinessDates.isEmpty()) {
						lmsDateValue = lstBusinessDates[0].Current_Date__c;
					}
               }
               return lmsDateValue;
          }
          set;
    }
}