/*
@@author : Deloitte Team
@@Created Date : 17th June 2019
@@Description : TriggerHandler on Target Object
*/

public class TargetTriggerHandler {

    public static void afterInsert(List<Targets__c> newList, Map<Id, Targets__c> oldMap) {
        createMonthlyTargets(newList, oldMap);
    }
    
    // Creates 1 child record of Monthly Target object with mapped values
    public static void createMonthlyTargets( List<Targets__c> newList, Map<Id, Targets__c> oldMap ) {
        List<Monthly_Target__c> lstMonTarg = new List<Monthly_Target__c>();
        Monthly_Target__c objMonTarg;
        for( Targets__c objTarg : newList){
            objMonTarg = new Monthly_Target__c();
            objMonTarg.LOB__c = objTarg.LOB__c;
            objMonTarg.SM_Category__c = objTarg.SM_Category__c;
            objMonTarg.Sales_User__c = objTarg.Sales_User__c;
            objMonTarg.Target_Amount__c = objTarg.Target_Amount__c;
            objMonTarg.Target_Count__c = objTarg.Target_Count__c;
            objMonTarg.Target_Yield__c = objTarg.Target_Yield__c;
            objMonTarg.Month__c = objTarg.Month__c;
            objMonTarg.From__c = objTarg.From__c;
            objMonTarg.To__c = objTarg.To__c;
            objMonTarg.Branch__c = objTarg.Branch__c;
            objMonTarg.SM_CBM__c = objTarg.SM_CBM__c;
            objMonTarg.Target__c = objTarg.id;
            lstMonTarg.add(objMonTarg);
        }
        
        insert lstMonTarg;
    }
}