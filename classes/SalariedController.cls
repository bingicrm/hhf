public with sharing class SalariedController {
 @AuraEnabled//Annotation to use method in lightning component
    public static List<Loan_Contact__c> getSalariedIncome(Id cusDtId) {  //Fetch data
        List<Loan_Contact__c> custDetailLst = new List<Loan_Contact__c>();
        Loan_Contact__c custDetial =  new Loan_Contact__c();
                   
        List<Loan_Contact__c> cd = [SELECT Id,IsVerified__c,Others_Fixed_Pay__c, Net_Income__c, Variable_Income_Monthly__c, Basic_Pay__c,HRA__c,DA__c,GrossSalary__c,PensionIncome__c,
                                    Gross_Monthly_Salary_Appraised__c,Monthly_Expenses__c,Total_Core_Income_Disposable_Net__c,
                                    Gross_Incentive__c,Avg_Bonus_Semi_Annually__c,Avg_Flying_Bonus_Semi_Annually__c,
                                    Fixed_Reimbursement_Cheque__c,Fixed_Reimbursement_Cash__c,Variable_Income__c,Pension_Considered_Flag__c,
                                    Customer_segment__c,Income_Program_Type__c,Loan_Applications__r.Name,Customer__r.Name,Applicant_Type__c,
                                    Loan_Applications__r.Sub_Stage__c
                                    FROM Loan_Contact__c where id =: cusDtId ];
        if(cd!= null && cd.size()>0){
            custDetial = cd[0];
        } 
        custDetailLst.add(custDetial);
        system.debug('custDetailLst '+custDetailLst);
        return custDetailLst;  
    } 
     @Auraenabled
    public static boolean GetIseditable(Id conId){
        //List<Customer_Obligations__c> obligationlist = new List<Customer_Obligations__c>();
        //Customer_Obligations__c obligationRec =  new Customer_Obligations__c(Customer_Integration__c = conId,isVerified__c = false);
        
        //List<Financial_Statement__c> fs1 = [SELECT isVerified__c,Actual_Cash_Profit__c,Adjusted_Networth__c,Administrative_selling_distribution_exp__c,Average_Collection_Period__c,Average_Days_in_Inventory__c,Balance_Sheet_Total_bre__c,Balance_Sheet_Total__c,Business_Income__c,Cash_and_Bank__c,Cash_Profits__c,Cash_Profit_Ratio__c,CreatedById,CreatedDate,Current_Assets__c,Current_Ratio__c,Customer_Integration__c,Debtors_greater_than_6_months__c,Debtors_less_than_6_months__c,Debt_Equity_Ratio__c,Depreciation__c,DSCR_after_the_proposed_Loan__c,DSCR__c,EBIDTA_Total_Operating_Income__c,EBITDA__c,Fixed_Assets_less_depreciation__c,Gross_Profit_cs__c,Gross_Profit__c,Group_Co_Investments__c,Id,Interest_Coverage_Ratio__c,Interest_paid_cs__c,Interest_Paid_on_Term_Loans__c,Interest_Paid_on_Working_Capitals_OD_C__c,Interest_Paid_to_family_member_as_shared__c,Interest_paid__c,Intrst_Exp_rent_paid_to_partner_director__c,Inventories__c,Investments__c,IsDeleted,LastModifiedById,LastModifiedDate,Liquidity_Ratio__c,Liquid_Marketable_Investments__c,Loans_Advances__c,Loan_Advance_given_to_director_partner__c,Manufacturing_expenses__c,Misc_Expense_DRE_Preop_Prelim_Acc_PL__c,Name,Net_Profit_Margin_Ratio__c,Net_Sales__c,NonBusiness_income__c,Non_cash_expenses_written_off__c,Non_Current_Loans_and_Advances__c,Other_Current_Liabilities_Provisions__c,Other_Income__c,Other_Interest_to_Outside_member__c,OwnerId,PAT__c,Profit_After_Tax__c,Profit_Before_Tax_bre__c,Profit_Before_Tax__c,Raw_Material_Cost__c,Receivables_Debtors__c,Reserves_Surplus__c,Salary_to_Partner_Director__c,Share_Capital__c,Short_Term_Loan_Advance_given_to_other__c,SystemModstamp,Tax__c,Term_Loans_from_Banks_FI__c,Total_Borrowings_from_banks_FI_NBFC_s__c,Total_Current_Assets__c,Total_Current_Liabilities__c,Total_Debt_EBITA__c,Total_Debt_Net_Cash_Accruals__c,Total_Income__c,Total_Liabilities_to_outsiders__c,Total_Networth__c,Trade_Creditors__c,Unquoted_Dead_Investments__c,Unsecured_loans_partners_shareholder__c,Wages__c,Working_Capital_Gap__c,Working_Capital_Limits_from_Banks_FI_s__c,Year__c from Financial_Statement__c where Customer_Integration__c =:custInt.Id and Year__c LIKE :('%'+String.valueOf(datetime.now().year())+'%')order by createdDate DESC limit 1];CurrentYe
        Loan_Contact__c custobj = [SELECT Id,Loan_Applications__r.Sub_Stage__c from Loan_Contact__c where id =:conId order by createdDate DESC limit 1];
        system.debug('custobj.Loan_Applications__r.Sub_Stage__c::'+custobj.Loan_Applications__r.Sub_Stage__c);
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        system.debug('ProfileName'+profileName);
        if(Label.Sub_Stage.contains(custobj.Loan_Applications__r.Sub_Stage__c) && Label.User_Profiles.contains(profileName))
        { 
            return true;//edit mode
        } else {
            return false;//read only mode}   
        }
        
        //system.debug('obligationlist'+obligationlist);
        //return true;
    }
    @Auraenabled
    Public static List<Loan_Contact__c> SaveSalary(List<Loan_Contact__c>  SalariedDetails, Id conId){
        //List<Loan_Contact__c> Allaccwrapperlist = (List<Loan_Contact__c>)JSON.deserialize(SalariedDetails,List<Loan_Contact__c>.class);
        List<Loan_Contact__c> Allaccwrapperlist=SalariedDetails;
        system.debug(Allaccwrapperlist); 
        if(Allaccwrapperlist[0].isVerified__c == true){
            update new Loan_Contact__c(id= conId,Salaried_Details_Status__c = 'Completed');

        }
        else{
            update new Loan_Contact__c(id= conId,Salaried_Details_Status__c = 'In Progress');
        
        }
        update Allaccwrapperlist;
        return Allaccwrapperlist;
        
    }
}