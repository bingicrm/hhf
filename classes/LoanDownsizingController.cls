public without sharing class LoanDownsizingController {
    /*@AuraEnabled
    public static boolean checkSalesUser(Id lappId){
        system.debug('=='+lappId);
        system.debug('Soql query+ '+ [SELECT Id, Name,Pending_Amount_for_Disbursement__c, StageName__c, Sub_Stage__c,Approved_Loan_Amount__c /*, Downsize_Amount__c, Downsize_Type__c  FROM Loan_Application__c
                                      WHERE Id = :lappId ]);
        Loan_Application__c la = [SELECT Id, Name, Assigned_Sales_User__c FROM Loan_Application__c WHERE Id = :lappId ];
        if(UserInfo.getUserId() == la.Assigned_Sales_User__c){
            return true;
        }
        else{
            return false;
        }
    }*/
    @AuraEnabled
    public static Loan_Application__c getLoanApp(Id lappId){
        system.debug('=='+lappId);
        system.debug('Soql query+ '+ [SELECT Id, Name,Pending_Amount_for_Disbursement__c, StageName__c, Sub_Stage__c,Approved_Loan_Amount__c /*, Downsize_Amount__c, Downsize_Type__c */ FROM Loan_Application__c
                                      WHERE Id = :lappId ]);
        Loan_Application__c la = [SELECT Id, Name, Assigned_Sales_User__c,Pending_Amount_for_Disbursement__c, StageName__c, Sub_Stage__c,Approved_Loan_Amount__c,(SELECT ID,Status__c FROM Tranches__r ORDER BY CreatedDate DESC) /*, Downsize_Amount__c, Downsize_Type__c */,(SELECT ID FROM Loan_Downsizings__r WHERE Status__c='Initiated' or Status__c='Approved') FROM Loan_Application__c
                WHERE Id = :lappId ];
        if(UserInfo.getUserId() == la.Assigned_Sales_User__c){
            return la;
        }
        else{
            return null;
        }
    }
    
    @AuraEnabled
    public static String partialDownsize( String recordId,  String dType , decimal dAmt, String creationRemarks ){
        List<Loan_Downsizing__c> existingLoanDownsizingList = new List<Loan_Downsizing__c>();
        existingLoanDownsizingList = [Select ID From Loan_Downsizing__c Where Loan_Application__c =:recordId And (Status__c='Initiated' OR Status__c='Approved')];
        Loan_Downsizing__c loanDownSizeRequest;
        
        if(existingLoanDownsizingList.size()==0 && dAmt!=null && String.isNotBlank(recordId) && String.isNotBlank(dType))
        {
            Loan_Application__c la = [Select Pending_Amount_for_Disbursement__c from Loan_Application__c where Id =: recordId];
            system.debug('La+ '+la);
            
            Decimal downSizeAmount = la.Pending_Amount_for_Disbursement__c - dAmt;
            
            if(la.Pending_Amount_for_Disbursement__c - dAmt >= Decimal.valueOf(Label.Tranche_Limit))
            {
                loanDownSizeRequest = new Loan_Downsizing__c();
                loanDownSizeRequest.Downsizing_Amount__c = dAmt;
                loanDownSizeRequest.Downsize_Type__c = dType;
                loanDownSizeRequest.Loan_Application__c = la.ID;
                loanDownSizeRequest.Status__c = 'Initiated';
                loanDownSizeRequest.Creation_Remark__c = creationRemarks; 
                system.debug('loanDownSizeRequest + '+loanDownSizeRequest + ' ' +loanDownSizeRequest + la.ID);
                
                try
                {
                    Database.SaveResult sr = Database.insert(loanDownSizeRequest,false);
                    if (sr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully inserted loanDownSizeRequest. loanDownSizeRequest ID: ' + sr.getId());
                        
                        //Updating Loan Application
                        Id downsizeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Downsizing').getRecordTypeId();
                        la.StageName__c = 'Loan Downsizing';
                        la.Sub_Stage__c = 'Downsizing Initiated';
                        la.RecordTypeId = downsizeId;
                        
                        update la;
                        
                        Document_Master__c dm = [Select Id from Document_Master__c where Name = 'Downsizing Agreement'];
                        Document_Type__c dt = [Select Id from Document_Type__c where Name = 'Others'];
                        Document_Checklist__c dc = new Document_Checklist__c();
                        dc.Loan_Applications__c = la.Id;
                        dc.Document_Master__c = dm.Id;
                        dc.Document_Type__c = dt.Id;
                        dc.Status__c = 'Pending';
                        dc.Loan_Downsizing__c=sr.getId();
                        
                        insert dc;
                        return 'Downsizing request initiated successfully.';
                    }
                    else
                    {
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Account fields that affected this error: ' + err.getFields());
                        }
                        //return 'Issue with Inserting Loan Downsizing Request';
                        return String.isNotBlank(sr.getErrors()[0].getMessage()) ? sr.getErrors()[0].getMessage() : 'Issue with Inserting Loan Downsizing Request';
                    }
                    
                }
                catch(Exception ex)
                {
                    system.debug('DML Error '+ex.getCause()); 
                    system.debug('DML Error '+ex.getMessage());
                    system.debug('DML Error '+ex.getLineNumber());
                    system.debug('DML Error '+ex.getStackTraceString());
                    
                    return 'Issue with Updating Loan Application';   
                }
            }
        }
        return 'Invalid Values for type and Amount';
    }
    
    @AuraEnabled
    public static String fullDownsize( String recordId,  String dType, String creationRemarks){
        List<Loan_Downsizing__c> existingLoanDownsizingList = new List<Loan_Downsizing__c>();
        existingLoanDownsizingList = [Select ID From Loan_Downsizing__c Where Loan_Application__c =:recordId And (Status__c='Initiated' OR Status__c='Approved')];
        
        system.debug('record ID-Full Downsizing '+ recordId);
        if(existingLoanDownsizingList.size()==0 && String.isNotBlank(recordId) && String.isNotBlank(dType))
        {
            Loan_Application__c la = [Select Pending_Amount_for_Disbursement__c 
                                      from Loan_Application__c where Id =: recordId];
            system.debug('La+ '+la);
            Loan_Downsizing__c loanDownSizeRequest;
            loanDownSizeRequest = new Loan_Downsizing__c();
            loanDownSizeRequest.Downsizing_Amount__c = la.Pending_Amount_for_Disbursement__c;
            loanDownSizeRequest.Downsize_Type__c = dType;
            loanDownSizeRequest.Loan_Application__c = la.ID;
            loanDownSizeRequest.Status__c = 'Initiated';
            loanDownSizeRequest.Creation_Remark__c = creationRemarks;
            system.debug('loanDownSizeRequest + '+loanDownSizeRequest + ' ' +loanDownSizeRequest + la.ID);
            try
            {
                Database.SaveResult sr = Database.insert(loanDownSizeRequest,false);
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted loanDownSizeRequest. loanDownSizeRequest ID: ' + sr.getId());
                    
                    //Updating Loan Application
                    Id downsizeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Downsizing').getRecordTypeId();
                    la.StageName__c = 'Loan Downsizing';
                    la.Sub_Stage__c = 'Downsizing Initiated';
                    la.RecordTypeId = downsizeId;
                    
                    update la;
                    
                    Document_Master__c dm = [Select Id from Document_Master__c where Name = 'Downsizing Agreement'];
                    Document_Type__c dt = [Select Id from Document_Type__c where Name = 'Others'];
                    Document_Checklist__c dc = new Document_Checklist__c();
                    dc.Loan_Applications__c = la.Id;
                    dc.Document_Master__c = dm.Id;
                    dc.Document_Type__c = dt.Id;
                    dc.Status__c = 'Pending';
                    dc.Loan_Downsizing__c=sr.getId();
                    insert dc;
                    
                    //Queue Assignment
                    //la = new Loan_Application__c();
                    //List<Loan_Application__c> la_List = new List<Loan_Application__c>();
                    //la = [Select Branch__c,StageName__c,sub_stage__c From Loan_Application__c Where ID = :recordId];
                    //la_List.add(la);
                    //LoanApplicationTriggerHelper.queueBaseAssignment(la_List);
                    
                    return 'Downsizing request initiated successfully.';
                }
                else
                {
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Account fields that affected this error: ' + err.getFields());
                    }
                    return String.isNotBlank(sr.getErrors()[0].getMessage()) ? sr.getErrors()[0].getMessage() : 'Issue with Inserting Loan Downsizing Request';
                    //return 'Issue with Inserting Loan Downsizing Request';
                }
            }
            catch(Exception ex)
            {
                system.debug('DML Error '+ex.getCause()); 
                system.debug('DML Error '+ex.getMessage());
                system.debug('DML Error '+ex.getLineNumber());
                system.debug('DML Error '+ex.getStackTraceString());
                
                return 'Issue with Updating Loan Application';   
            }
        }
        return 'Invalid Values for type';
    }
    
    //Method to Approve Reject Loan Downsize..
    /*@AuraEnabled
public static Loan_Downsizing__c getLoanDownsizeInfo(String loanDownsizeID)
{
return [Select Status__c, Remarks__c From Loan_Downsizing__c where ID = :loanDownsizeID];
}*/
    
    @AuraEnabled
    public static Loan_Downsizing__c getLoanDownsizeInfo(String loanApplicationID)
    {
        system.debug('loanApplicationID++ '+loanApplicationID);
        List<Loan_Downsizing__c> loanDownsizingList = new List<Loan_Downsizing__c>();
        loanDownsizingList = [Select Status__c, Remarks__c From Loan_Downsizing__c where Loan_Application__c = :loanApplicationID And (Status__c='Initiated' OR Status__c='Approved') Order By SystemModStamp Desc];
        if(loanDownsizingList.size() == 1)
        {
            return loanDownsizingList[0];    
        }
        return null;
    }
    
    //approveRejectLoanDownsize
    @AuraEnabled
    public static String approveRejectLoanDownsizing(String loanDownsizingID, String statusValue, String creditRemarks )
    {
        system.debug('approveRejectLoanDownsizing---loanDownsizingID '+loanDownsizingID);
        List<Loan_Downsizing__c> loanDownsizingList =  [Select Status__c, Remarks__c,PDD_s_Reviewed__c,Loan_Application__c From Loan_Downsizing__c Where ID = :loanDownsizingID For Update];
        List<Document_Checklist__c> loanApplicationList = new List<Document_Checklist__c>();
        
        if(loanDownsizingList!=null && loanDownsizingList.size() > 0)
        {
            loanApplicationList = [Select Scan_Check_Completed__c From Document_Checklist__c Where Loan_Applications__c= :loanDownsizingList[0].Loan_Application__c AND Document_Master__r.Name='Downsizing Agreement' AND Status__c = 'Uploaded' Order BY CreatedDate DESC Limit 1];
            
            if(loanApplicationList.size() > 0 && loanApplicationList[0].Scan_Check_Completed__c)
            {
                if((loanDownsizingList[0].Status__c == 'Initiated' || loanDownsizingList[0].Status__c == 'Approved') && String.isNotBlank(creditRemarks))
                {
                    if(!loanDownsizingList[0].PDD_s_Reviewed__c)
                    {
                        return 'PDDs Review Should be Checked before Approving or Rejecting Loan Downsizing.';
                    }
                    
                    loanDownsizingList[0].Status__c = statusValue; 
                    loanDownsizingList[0].Remarks__c = creditRemarks;
                    try
                    {
                        update loanDownsizingList[0];
                    }
                    catch(Exception ex)
                    {
                        system.debug('exception++ '+ex.getMessage());
                        // if(ex.getMessage().contains('PDDs')){
                        //   return 'Error: Please review all PDDs before taking this action.';
                        //}
                        //else{
                        return 'Error: '+ex.getMessage();
                        //}                  
                    }
                    return 'Loan Downsizing updated successfully.';
                }
                else
                {
                    return 'Loan Downsizing status is not Initiated/ Approved or Credit Remarks is Blank';
                }
            }
            else
            {
                if(!loanApplicationList[0].Scan_Check_Completed__c)
                {
                    return 'Complete Scan Check';
                } else
                {
                    return 'Contact Administrator - Issue with Loan Application';
                }  
            }
        }
        return 'No Loan Downsizing Found'; 
    }//Method ends
}