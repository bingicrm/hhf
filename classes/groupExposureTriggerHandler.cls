public class groupExposureTriggerHandler{

    public static void beforeInsert(List<Group_Exposure__c> newList,  Map<Id,Group_Exposure__c> newMap){
        Date dt = LMSDateUtility.lmsDateValue;
		/** Added by Saumya for BRE 2 **/
        Boolean GEcontainsLACD = false;
        Set<Id> lstloanApp = new Set<Id>();
        Set<String> lstOfAppId = new Set<String>();
        Map<String,Boolean> mapOfGEwithCustomerAvaialable = new Map<String,Boolean> ();
        /** Added by Saumya for BRE 2 **/ 				 
        for(Group_Exposure__c gExp : newList) {
            gExp.Business_Date__c = (Date)dt;
		/** Added by Saumya for BRE 2 **/
            lstloanApp.add(gExp.LoanApplication__c);
            lstOfAppId.add(gExp.ApplicationID__c);
            system.debug('gExp.ApplicationID__c::'+gExp.ApplicationID__c);
            /** Added by Saumya for BRE 2 **/
        }
 system.debug('lstloanApp::'+lstloanApp);
        system.debug('lstOfAppId::'+lstOfAppId);
/** Added by Saumya for BRE 2 **/
        List<Loan_Contact__c> lstofLC = [Select Customer__c from Loan_Contact__c where  Loan_Applications__c IN : lstloanApp];
        List<Id> lstLC = new List<Id>();
        for(Loan_Contact__c objlc: lstofLC){
            lstLC.add(objlc.Customer__c);
        }
        List<Loan_Contact__c> lstLC2 = [Select Id,Customer__c,Loan_Applications__c,Loan_Applications__r.Loan_Number__c from Loan_Contact__c where  Loan_Applications__r.Loan_Number__c IN : lstOfAppId];
        system.debug('lstLC2::'+ lstLC2);   
        for(Loan_Contact__c objlc: lstLC2){
        //system.debug('mapOfGEwithCustomerAvaialable.get(objlc.Loan_Applications__c)'+ mapOfGEwithCustomerAvaialable.get(objlc.Loan_Applications__c));
        system.debug('lstLC::'+ objlc.Id +'contains '+lstLC.contains(objlc.Customer__c));    
            if(lstLC.contains(objlc.Customer__c) /*&& GEcontainsLACD == false*/ && (mapOfGEwithCustomerAvaialable.get(objlc.Id) == false || mapOfGEwithCustomerAvaialable.get(objlc.Id) == null)){
                GEcontainsLACD = true;   
                system.debug('I am inside');
                mapOfGEwithCustomerAvaialable.put(objlc.Id, GEcontainsLACD);
            }
        }
        system.debug('mapOfGEwithCustomerAvaialable::::::'+ mapOfGEwithCustomerAvaialable);
        
        for(Group_Exposure__c gExp : newList) {
            system.debug(gExp.Customer_Available_in_Current_Loan__c + 'gExp.Customer_Available_in_Current_Loan__c');
            if(gExp.Customer_Available_in_Current_Loan__c != true){
            system.debug('gExp.Customer_Detail__c::::::'+ gExp.Customer_Detail__c + 'Customer Exist' + mapOfGEwithCustomerAvaialable.get(gExp.Customer_Detail__c));
            gExp.Customer_Available_in_Current_Loan__c = mapOfGEwithCustomerAvaialable.get(gExp.Customer_Detail__c) != null ? mapOfGEwithCustomerAvaialable.get(gExp.Customer_Detail__c) : false;
            }
                
        }
        /** Added by Saumya for BRE 2 **/
       // List<Loan_Application__c> lstLoanAppfrGE = [Select Id,Loan_Number__c, (Select Id From Loan_Contacts__r) From Loan_Application__c where Loan_Number__c IN: lstOfAppId];							
    }

    public static void afterInsert(List<Group_Exposure__c> newList){
    	Id lappId;
    	Set<String> lapp = new Set<String>();
    	for(Group_Exposure__c gexp : newList){
    		lappId = gexp.LoanApplication__c;
    	}
    	system.debug('Loan Application ID'+lappId);
    	List<Group_Exposure__c> gexpLst = new List<Group_Exposure__c>();
    	gexpLst = [SELECT Id, LoanApplication__c,ApplicationID__c FROM Group_Exposure__c WHERE LoanApplication__c =: lappId];
    	for(Group_Exposure__c ge : gexpLst){
    		lapp.add(ge.ApplicationID__c);
    	}
    	system.debug('lapp'+lapp);
    	List<Group_Exposure__c> geLst = [SELECT ApplicationID__c, LoanApplication__c FROM Group_Exposure__c WHERE ApplicationID__c IN: lapp AND LoanApplication__c =: lappId];
    	List<Group_Exposure__c> duplicateLst = new List<Group_Exposure__c>();
    	for(Group_Exposure__c g : gexpLst){
    		for(Group_Exposure__c gEx : geLst){
    			if(g.ApplicationID__c == gEx.ApplicationID__c){
    				duplicateLst.add(gEx);
    			}
    			else{
    				lapp.add(g.ApplicationID__c);

    			}
    		}
    	}
    	delete duplicateLst;
    }

}