public class CRM_Utility {
    @future
    public static void insertStandardUser(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser1@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_IN', ProfileId = p.Id, 
                          TimeZoneSidKey='Asia/Kolkata', UserName='hhfStandarduser1@testorg.com');
        
        insert u;
    }
    
    public static Rest_Service__mdt insertRestService(string developerName){
        Rest_Service__mdt restService;
        if(developerName=='repayment'){
            restService = new Rest_Service__mdt();
            restService.DeveloperName = 'CRM_Repayment_Schedule';
            restService.Client_Username__c = 'admin';
            restService.Client_Password__c = 'admin';
            restService.Service_EndPoint__c = 'https://services-qa.herofincorp.com/api/housing/crm/repayment';
        }
        else if(developerName=='itc'){
            restService = new Rest_Service__mdt();
            restService.DeveloperName = 'CRM_Case_Itc';
            restService.Client_Username__c = 'admin';
            restService.Client_Password__c = 'admin';
            restService.Service_EndPoint__c = 'https://services-qa.herofincorp.com/api/housing/crm/itc';
        }
        else if(developerName=='loaninformation'){
            restService = new Rest_Service__mdt();
            restService.DeveloperName = 'CRM_CaseLoanInfo';
            restService.Client_Username__c = 'admin';
            restService.Client_Password__c = 'admin';
            restService.Service_EndPoint__c = 'https://services-qa.herofincorp.com/api/housing/crm/loaninformation';
        }
        else if(developerName=='loanlist'){
            restService = new Rest_Service__mdt();
            restService.DeveloperName = 'CRM_CaseLoanList';
            restService.Client_Username__c = 'admin';
            restService.Client_Password__c = 'admin';
            restService.Service_EndPoint__c = 'https://services-qa.herofincorp.com/api/housing/crm/loanlist';
        }
        return restService;
    }
}