//Created By Amit Agarwal
@isTest
public class TestReopenOperations { 
    @testSetup
    private static void testSetupMethod() {
        Profile p = [SELECT id, name from profile where name =: 'Sales Team'];
        
        User u = new User(
            ProfileId = p.Id,
            LastName = 'last',  
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST', 
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            Technical__c = true,
            FCU__c= true,Legal__c = true, FI__c= true,LIP__C = true,PD__c= true,
        	Role_in_Sales_Hierarchy__c='SM');
        
        insert u;
        
        Loan_Contact__c lc;
        Loan_Application__c la;
        
        System.runAs(u) {
            Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
            
            Account acc = new Account();
            acc.CountryCode__c = '+91';
            acc.Email__c = 'puser001@amamama.com';
            acc.Gender__c = 'M';
            acc.Mobile__c = '9800765432';
            acc.Phone__c = '9800765434';
            acc.PersonMobilePhone = '9800765434';
            acc.FirstName = 'abc';
            acc.LastName = 'def';
            acc.Salutation = 'Mr';
            acc.RecordTypeId = RecordTypeIdCustomer;
            insert acc;
            
            Scheme__c sch = new Scheme__c(Name = 'Construction Finance', Scheme_Code__c = 'CF', Scheme_Group_ID__c = '5',
                                          Scheme_ID__c = 11, Product_Code__c = 'CF', Max_ROI__c = 10, Min_ROI__c = 5,
                                          Min_Loan_Amount__c = 10000, Min_FOIR__c = 1,
                                          Max_Loan_Amount__c = 3000001, Max_Tenure__c = 84, Min_Tenure__c = 6,
                                          Max_LTV__c = 5, Min_LTV__c = 1, Max_FOIR__c = 5);
            insert sch;
            
            Scheme__c sch1=new Scheme__c();
            sch1.Name='Personal Loan';
            sch1.Product_Code__c='IPL'; 
            sch1.Scheme_Code__c='IPL';
            sch1.Scheme_Group_ID__c='Test';
            insert sch1;
            Local_Policies__c lp = new Local_Policies__c(Name='Standard', Branch__c='All',Product_Scheme__c='All Schemes',IsActive__c=true);
            insert lp;
            
            Id RecordTypeIdLAAppIni = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Application Initiation').getRecordTypeId();
            
            la = new Loan_Application__c(Customer__c = acc.Id, Scheme__c = sch.Id, Loan_Number__c = '124',
                                         Transaction_type__c = 'PB', Requested_Amount__c = 1000000,
                                         Loan_Purpose__c = '11', StageName__c = 'Customer Onboarding', Sub_Stage__c = 'Application Initiation',
                                         RecordTypeId = RecordTypeIdLAAppIni,Branch__c='Delhi',
                                         Business_Date_Created__c = System.today(),
                                         Approved_Loan_Amount__c = 1000000,Requested_Loan_Tenure__c=84,
                                         Processing_Fee_Percentage__c = 1, Mode_of_Repayment__c = 'NACH',
                                         Repayment_Start_Date__c = Date.Today().addDays(100),Income_Program_Type__c='Normal Salaried',
                                         Sanctioned_Disbursement_Type__c = 'Multiple Tranche', Property_Identified__c = true,
                                         Local_Policies__c=lp.Id);
            insert la;
            
            lc = [SELECT Id, Borrower__c from Loan_Contact__c where Loan_Applications__c =: la.Id];
            lc.Borrower__c = '1';
            //lc.Category__c='1';
            lc.Gender__c = 'Male';
            lc.Applicant_Type__c = 'Applicant';
            lc.Constitution__c = '20';
            lc.Customer_segment__c = '3';
            lc.BRE_Record__c = true;
            lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc.Marital_Status__c = 'M';
            lc.Gross_Salary__c = 1000;
            lc.Basic_Pay__c = 1000;
            lc.DA__c = 1000;
            lc.Qualification__c = 'UMS';
            //lc.Total_Fixed_Pay__c = 1000;
            //lc.Variable_Income_Monthly__c = 1000;
            lc.Net_Income__c = 1000;
            lc.PF__c = 1000;
            lc.TDS__c = 1000;
            lc.Current_Organization_Work_Ex_in_Months__c = 12;
            lc.Total_Work_Experience__c = 12;
            lc.Customer_Details_Verified__c = true;
            lc.Voter_ID_Number__c = '123456789';
            lc.Passport_Number__c = '123456789';
            lc.PAN_Number__c = 'BAFPV6543A';
            lc.Aadhaar_Number__c = '898518762736';
            lc.Income_Considered__c = true;
            lc.AIP_month__c = '4-2020';
            lc.Mobile__c = '7878565677';
            lc.Email__c = 'abc@g.com';
            lc.Applicant_Type__c='Applicant';
            update lc;
            Id parameter = la.id;
            UCIC_Global__c setting = new UCIC_Global__c();
            setting.UCIC_Active__c = true;
            insert setting; 
        }
    }
    
    public static testMethod void method1(){
        Test.startTest();
        Loan_Application__c la = [SELECT Id from Loan_Application__c];
        Loan_Contact__c lc = [SELECT Id from Loan_Contact__c];
        Id param = la.id;
        Id RecordTypeId1 = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('UCIC').getRecordTypeId();
        Customer_Integration__c cust = new Customer_Integration__c(RecordTypeId = RecordTypeId1,Negative_UCIC_DB_Found__c=true,UCIC_Response_Status__c=true,UCIC_Negative_API_Response__c=true, BRE2_Success__c= true, Loan_Application__c = la.Id, Loan_Contact__c = lc.Id,CAM_Screen_Entry__c = true,API_Type__c= 'ScannedPDFBanking',Manual__c = false, Recent__c = false, Bank_Account_Number__c = '1234',Average_ABB__c = 0,Analysis_Status__c = 'Completed Successfully',Banking_Month_Year__c = '4-2020');
        insert cust;
        Loan_Application__c retVal = ReopenOperations.getLoanApp(param);
        System.assertNotEquals(Null, retVal);  
        Loan_Application__c objla = [SELECT Id, Name, StageName__c, Sub_Stage__c, Rejected_Cancelled_Stage__c,Rejected_Cancelled_Sub_Stage__c, OwnerId,Insurance_Loan_Application__c, Assigned_Sales_User__c, RecordTypeID, Date_of_Rejection__c, Date_of_Cancellation__c, Remarks_Revival__c,Rejection_Reason__c,Severity_Level__c,Re_Open_Date__c,Reopen_Count__c,/*** Added by Saumya forReject Revive WIP modification ***/ 
                                     (Select Id, Recordtype.DeveloperName, Recordtypeid, Applicant_Type__c /*** Added by Saumya for New UCIC II Enhancements ***/ from Loan_Contacts__r)
                                     FROM Loan_Application__c
                                     WHERE Id = :la.Id ];
        String retmsg = ReopenOperations.reopenApplication(objla);
        //System.assertEquals('You are not authorized for Reopening.', retmsg); 
        //System.assertNotEquals(Null, retmsg);
        Test.stopTest();
    }
    
    public static testMethod void method2(){
        Profile p = [SELECT id, name from profile where name =: 'Sales Team'];        
        User u = new User();
        u = [Select id from User WHERE Email = 'puser000@amamama.com' AND Profileid =: p.id];
        System.runAs(u){
            Loan_Application__c la = [SELECT Id from Loan_Application__c];
            Loan_Contact__c lc = [SELECT Id from Loan_Contact__c];
            Id param = la.id;
            Id RecordTypeId1 = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('UCIC').getRecordTypeId();
            Customer_Integration__c cust = new Customer_Integration__c(RecordTypeId = RecordTypeId1,Negative_UCIC_DB_Found__c=false,UCIC_Response_Status__c=true,UCIC_Negative_API_Response__c=true, BRE2_Success__c= true, Loan_Application__c = la.Id, Loan_Contact__c = lc.Id,CAM_Screen_Entry__c = true,API_Type__c= 'ScannedPDFBanking',Manual__c = false, Recent__c = false, Bank_Account_Number__c = '1234',Average_ABB__c = 0,Analysis_Status__c = 'Completed Successfully',Banking_Month_Year__c = '4-2020');
            upsert cust;
            Test.startTest();
            Loan_Application__c retVal = ReopenOperations.getLoanApp(param);
            System.assertNotEquals(Null, retVal); 
            Loan_Application__c objla = [SELECT Id, Name, StageName__c, Sub_Stage__c, Rejected_Cancelled_Stage__c,Rejected_Cancelled_Sub_Stage__c, OwnerId,Insurance_Loan_Application__c, Assigned_Sales_User__c, RecordTypeID, Date_of_Rejection__c, Date_of_Cancellation__c, Remarks_Revival__c,Rejection_Reason__c,Severity_Level__c,Re_Open_Date__c,Reopen_Count__c,/*** Added by Saumya forReject Revive WIP modification ***/ 
                                         (Select Id, Recordtype.DeveloperName, Recordtypeid, Applicant_Type__c /*** Added by Saumya for New UCIC II Enhancements ***/ from Loan_Contacts__r)
                                         FROM Loan_Application__c
                                         WHERE Id = :la.Id ];
            objla.Rejected_Cancelled_Sub_Stage__c = 'Hunter Review';
            objla.Date_of_Cancellation__c = System.Today()-3;
            update objla;
            String retmsg = ReopenOperations.reopenApplication(objla);            
            //System.assertEquals('Application Successfully reopened', retmsg); 
            //System.assertNotEquals(Null, retmsg);
            Test.stopTest();
        }
    }
    
    public static testMethod void method3(){
        Profile p = [SELECT id, name from profile where name =: 'Sales Team'];        
        User u = new User();
        u = [Select id from User WHERE Email = 'puser000@amamama.com' AND Profileid =: p.id];
        System.runAs(u){
            Loan_Application__c la = [SELECT Id from Loan_Application__c];
            Loan_Contact__c lc = [SELECT Id from Loan_Contact__c];
            Id param = la.id;
            Id RecordTypeId1 = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('UCIC').getRecordTypeId();
            Customer_Integration__c cust = new Customer_Integration__c(RecordTypeId = RecordTypeId1,Negative_UCIC_DB_Found__c=false,UCIC_Response_Status__c=true,UCIC_Negative_API_Response__c=true, BRE2_Success__c= true, Loan_Application__c = la.Id, Loan_Contact__c = lc.Id,CAM_Screen_Entry__c = true,API_Type__c= 'ScannedPDFBanking',Manual__c = false, Recent__c = false, Bank_Account_Number__c = '1234',Average_ABB__c = 0,Analysis_Status__c = 'Completed Successfully',Banking_Month_Year__c = '4-2020');
            upsert cust;
            Test.startTest();
            Loan_Application__c retVal = ReopenOperations.getLoanApp(param);
            System.assertNotEquals(Null, retVal); 
            Loan_Application__c objla = [SELECT Id, Name, StageName__c, Sub_Stage__c, Rejected_Cancelled_Stage__c,Rejected_Cancelled_Sub_Stage__c, OwnerId,Insurance_Loan_Application__c, Assigned_Sales_User__c, RecordTypeID, Date_of_Rejection__c, Date_of_Cancellation__c, Remarks_Revival__c,Rejection_Reason__c,Severity_Level__c,Re_Open_Date__c,Reopen_Count__c,/*** Added by Saumya forReject Revive WIP modification ***/ 
                                         (Select Id, Recordtype.DeveloperName, Recordtypeid, Applicant_Type__c /*** Added by Saumya for New UCIC II Enhancements ***/ from Loan_Contacts__r)
                                         FROM Loan_Application__c
                                         WHERE Id = :la.Id ];
            objla.Date_of_Cancellation__c = System.Today()-3;
            update objla;
            String retmsg = ReopenOperations.reopenApplication(objla);
            System.assertEquals('', retmsg);
            Test.stopTest();
        }
    }
}