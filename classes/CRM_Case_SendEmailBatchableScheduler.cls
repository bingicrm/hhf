global class CRM_Case_SendEmailBatchableScheduler implements Schedulable 
{
    global void execute(SchedulableContext ctx) 
    {       
        Database.executeBatch(new CRM_Case_SendEmailBatchable());
        
    }
}