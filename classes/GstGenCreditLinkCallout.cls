public class GstGenCreditLinkCallout implements Queueable, Database.AllowsCallouts{
    
    public list<customer_integration__c> custIntegrationRecords;
    public GstGenCreditLinkCallout(list<customer_integration__c> custIntegrationRecords){
        this.custIntegrationRecords = custIntegrationRecords;
    }
    public void execute(QueueableContext context) {
        For(customer_integration__c CI : custIntegrationRecords){ 
            system.debug('custIntegrationRecords  ' + CI);
            if(CI.Id != null && CI.GSTIN__c != null && CI.Mobile__c != null && CI.Email__c != null){
                makeGstGenCreditLinkCallout(CI.GSTIN__c,CI.Email__c, CI.Mobile__c, CI.Id,CI.Loan_Contact__c); 
            }
          }
    }
         
     @future(callout = true)   
    public static void makeGstGenCreditLinkCallout(String strGSTIN, String strEmail, String strMobile, String strRefId, String loanContactId) {
        system.debug('makeGstGenCreditLinkCallout ' + strGSTIN +' '+ strEmail +' '+ strMobile +' '+ strRefId);
        if(String.isNotBlank(strGSTIN)) {
            List<Rest_Service__mdt> lstRestService = [SELECT 
                                                            MasterLabel, 
                                                            QualifiedApiName, 
                                                            Service_EndPoint__c, 
                                                            Client_Password__c, 
                                                            Client_Username__c, 
                                                            Request_Method__c, 
                                                            Time_Out_Period__c,
                                                      		org_id__c
                                                      FROM   
                                                            Rest_Service__mdt
                                                      WHERE  
                                                            DeveloperName = 'GST_4_GstGenCreditLink'
                                                            LIMIT 1
                                                     ]; 
            System.debug('Debug Log for lstRestService  GST_5_GstTransactionDetails ='+lstRestService);
            if(!lstRestService.isEmpty()) {
                GstGenCreditLinkResponseBody objResponseBody = new GstGenCreditLinkResponseBody();
                Blob headerValue = Blob.valueOf(lstRestService[0].Client_Username__c + ':' + lstRestService[0].Client_Password__c);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                Map<String,String> headerMap = new Map<String,String>();
                headerMap.put('Authorization',authorizationHeader);
                headerMap.put('Username',lstRestService[0].Client_Username__c);
                headerMap.put('Password',lstRestService[0].Client_Password__c);
                headerMap.put('Content-Type','application/json');
                headerMap.put('org-id',lstRestService[0].org_id__c);
                
                
                RequestBody objRequestBody = new RequestBody();
                    objRequestBody.gstin = strGSTIN != null ? strGSTIN : '';
                    objRequestBody.email = strEmail != null ? strEmail : '';
                    objRequestBody.mobile = strMobile != null ? strMobile : '';
                    objRequestBody.consent = 'Y';
                	String refid = System.Label.GST_Product_Id + '-' + strRefId;
                	system.debug('refid ' + refid);
                    objRequestBody.refId = refid;  
                    
                System.debug('Debug Log for objRequestBody'+objRequestBody);
                String jsonRequest = Json.serializePretty(objRequestBody); 
                
                Http objHttp = new Http();
                HttpRequest objHttpRequest = new HttpRequest();
                
                
                if(String.isNotBlank(lstRestService[0].Service_EndPoint__c)) {
                    objHttpRequest.setEndPoint(lstRestService[0].Service_EndPoint__c);
                }
                else {
                    System.debug('Debug Log for error : Endpoint Empty. Please specify endpoint in metadata record.');
                }
                if(String.isNotBlank(lstRestService[0].Request_Method__c)) {
                    objHttpRequest.setMethod(lstRestService[0].Request_Method__c);
                }
                else {
                    System.debug('Debug Log for error : Requested Method Empty. Please specify Requested Method in metadata record.');
                }
                
                for(String headerKey : headerMap.keySet()){
                    objHttpRequest.setHeader(headerKey, headerMap.get(headerKey));
                    System.debug('Debug Log for headerKey'+headerKey);
                    System.debug('Debug Log for headerMap.get(headerKey)'+headerMap.get(headerKey));
                }
                objHttpRequest.setBody(jsonRequest);
                if(String.isNotBlank(String.valueOf(lstRestService[0].Time_Out_Period__c))) {
                    objHttpRequest.setTimeOut(Integer.valueOf(lstRestService[0].Time_Out_Period__c));
                }
                System.debug('Debug Log for request'+objHttpRequest);
                try {
                    HTTPResponse httpRes = objHttp.send(objHttpRequest);
                    //Create integration logs
                    //LogUtility.createIntegrationLogs(jsonRequest,httpRes,lstRestService[0].Service_EndPoint__c);
                    System.debug('Debug Log for response'+httpRes);
                    System.debug('Debug Log for response body'+httpRes.getBody());
                    System.debug('Debug Log for response status'+httpRes.getStatus());
                    System.debug('Debug Log for response status code'+httpRes.getStatusCode());
                    //Deserialization of response to obtain the result parameters
                    //objResponseBody = (ResponseBody)JSON.deserialize(httpRes.getBody(), ResponseBody.class);
                    objResponseBody = (GstGenCreditLinkResponseBody)Json.deserialize(httpRes.getBody(), GstGenCreditLinkResponseBody.class);
                    if(httpRes.getStatus() == 'OK' && httpRes.getStatusCode() == 200) {
                        if(objResponseBody != null) {
                            if(objResponseBody.statusCode == 101){
                            system.debug('objResponseBody ===' + objResponseBody);
                            Customer_Integration__c GSTRecord = [Select id,Loan_Contact__c,System_Created__c from Customer_Integration__c where id =: strRefId LIMIT 1];
                            System.debug('GSTRecord Return Status ' + GSTRecord);
                           /* if(GSTRecord.Id != null){
                                GSTRecord.System_Created__c = true;
                                //update GSTRecord;
                                system.debug('GSTRecord.System_Created__c  ' + GSTRecord.System_Created__c  );
                            }*/
                           
                            /*if(loanContactId != null){
                                Loan_Contact__c loanContact = [Select id,Success_GST__c from Loan_Contact__c where id =: loanContactId limit 1];
                                if(loanContact.Id != null){
                                    loanContact.Success_GST__c = true;
                                    update loanContact;
                                     //alert('message: GST link successfully sent to the Customer.');
                                }
                                
                            }*/
                            
                             System.debug('Result Array'+objResponseBody.result);
                            System.debug('Result Request Id'+objResponseBody.requestId);
                            System.debug('Result Status Code'+objResponseBody.statusCode);
                            System.enqueueJob(new EnqueDMLGSTINTwo(GSTRecord,jsonRequest,String.valueOf(httpRes),lstRestService[0].Service_EndPoint__c));
                            
                        }
                        }  
                        
                    }
                    
                }
                catch(Exception ex) {
                    System.debug('Debug Log for exception'+ex);
                }
           }
       }
    }
    public class RequestBody {
        public String gstin;    //02AAACR5055K1ZJ
        public String email;    //joshi.hitesh@gmail.com
        public String mobile;   //7208748564
        public String consent;  //Y
        public String refId;    //54135413
    }
}