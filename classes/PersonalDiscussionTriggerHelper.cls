public with sharing class PersonalDiscussionTriggerHelper {
	public static void sendSMStoCustomer(List<Personal_Discussion__c> evtList)
	{
      
        List<Id> idList = new List<Id>();
        List<Loan_Contact__c> lcList = new List<Loan_Contact__c>();
        String template='';
        Communication_Trigger_Point__mdt ct = [Select Id, Stage_Value__c, SMS_Template__c from Communication_Trigger_Point__mdt where Stage_Value__c =: Constants.INITIATE];
        for(Personal_Discussion__c e: evtList)
        {
            idList.add(e.Customer_Detail__c);
        }
        lcList = [Select Id, Customer__r.PersonMobilePhone, Customer__r.Name, Loan_Applications__r.Name,Loan_Applications__r.Credit_Manager_User__r.MobilePhone , Loan_Applications__r.Credit_Manager_User__r.Name from Loan_Contact__c where Id =: idList];
        for(Personal_Discussion__c e: evtList){
            if(e.Send_SMS__c)
            {
                for(Loan_Contact__c lc: lcList){
                    if(e.Customer_Detail__c == lc.Id && lc.Customer__r.PersonMobilePhone != null ){
                        template = ct.SMS_Template__c;
                        template = template.replace('<app id>', lc.Loan_Applications__r.Name);
                        template = template.replace('<PD Date> <PD Time>', String.valueOfGmt(e.PD_Initiated_On__c));
                        template = template.replace('<name>', lc.Loan_Applications__r.Credit_Manager_User__r.Name);
                        template = template.replace('<num>', lc.Loan_Applications__r.Credit_Manager_User__c != null ? lc.Loan_Applications__r.Credit_Manager_User__r.MobilePhone :'');
                        SMSIntegration.SMSinit(lc.Customer__r.PersonMobilePhone, template);
                    }
                }
            }
        }
        
    }
}