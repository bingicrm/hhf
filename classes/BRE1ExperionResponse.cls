/*=====================================================================
 * Deloitte India
 * Name: BRE1ExperionResponse
 * Description: This class is used for parsing BRE 1.1 B Experion response.
 * Created Date: [23/10/2019]	
 * Created By: Vaishali Mehta (Deloitte India)
 *
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/


@RestResource(urlMapping='/BRE1ExperionResponse/*')
global with sharing class BRE1ExperionResponse  {
    
     /*
    * @Description     This method is used to parse the BRE 1.1 B Experion Response
    * @author          Vaishali Mehta(Deloitte)
    * @returns         void
    */
	
    class testwrap {
        public string sample;
    }
    
    @HttpPost
    global static ResultResponse parseBRE1ExperionResponse() 
    {    
        
        Boolean errorFlag = false;
        ResultResponse resRep = new ResultResponse();
        BRE1ExperionResp objResponseBody = new BRE1ExperionResp();
        Loan_Application__c la = new Loan_Application__c();
        List<Loan_Application__c> lstOfLoanApp = new List<Loan_Application__c>();
        Loan_Contact__c lc = new Loan_Contact__c();
        List<Loan_Contact__c> lcList = new List<Loan_Contact__c>();
        system.debug('Request '+RestContext.request.requestBody.tostring());
      
       /* //testing 
        testwrap objtest;
         objTest = (testwrap)System.JSON.deserialize(RestContext.request.requestBody.tostring(),testwrap.class);
         system.debug('@@@'+objtest);
        //testing end  */
        
        
        objResponseBody = (BRE1ExperionResp)System.JSON.deserialize(RestContext.request.requestBody.tostring(),BRE1ExperionResp.class);
        
        System.debug('Response Body'+objResponseBody);
        
        if(objResponseBody!= null) {
            
            //Update Loan Application Details
            if(objResponseBody.applicationout != null && !String.isBlank(objResponseBody.applicationout.applicationid)) {
                lstOfLoanApp = [SELECT Id, Loan_Number__c, Business_Date_Modified__c, Requested_Amount__c, 
                        Approved_EMI__c, Requested_Loan_Tenure__c, Scheme__c, Loan_Purpose__c,BRE_Approval_Status__c 
                        FROM Loan_Application__c 
                        WHERE Id =: objResponseBody.applicationout.applicationid];
                
                if(!lstOfLoanApp.isEmpty()) {
                    la = lstOfLoanApp[0];
                /*   if(!String.isBlank(objResponseBody.applicationout.businessdate)) {
                        la.Business_Date_Modified__c = Date.valueOf(objResponseBody.applicationout.businessdate);
                    }        
                    if(objResponseBody.applicationout.emi != null) {
                        la.Approved_EMI__c = String.isBlank(objResponseBody.applicationout.emi) ?0: Decimal.valueOf(objResponseBody.applicationout.emi);
                    }
                     if(objResponseBody.applicationout.appliedloanamount != null) {
                        la.Requested_Amount__c = String.isBlank(objResponseBody.applicationout.appliedloanamount) ?0: Decimal.valueOf(objResponseBody.applicationout.appliedloanamount); 
                    } 
                    if(objResponseBody.applicationout.tenureapplied != null) {
                        la.Requested_Loan_Tenure__c = String.isBlank(objResponseBody.applicationout.tenureapplied) ?0: Decimal.valueOf(objResponseBody.applicationout.tenureapplied);
                    }
                    if(!String.isBlank(objResponseBody.applicationout.productcode)) {
                        la.Scheme__c = objResponseBody.applicationout.productcode;
                    }
                    if(!String.isBlank(objResponseBody.applicationout.subproduct)) {
                        la.Loan_Purpose__c = objResponseBody.applicationout.subproduct;
                    }
                    if(!String.isBlank(objResponseBody.applicationout.decisioncategory)) {
                        la.Decision_Category__c = objResponseBody.applicationout.decisioncategory;
                    }
                    if(!String.isBlank(objResponseBody.applicationout.decisioncomponenttreename)) {
                        la.Decision_Component_Tree_Name__c = objResponseBody.applicationout.decisioncomponenttreename;
                    }
                    if(objResponseBody.applicationout.roi != null) {
                        la.Approved_ROI__c = String.isBlank(objResponseBody.applicationout.roi) ?0: Decimal.valueOf(objResponseBody.applicationout.roi);
                    } 
                    
                    List<Application_CIBIL_Decision_Detail__c> lstCIBILDecisionOfApplication = new List<Application_CIBIL_Decision_Detail__c>();
                    for(Integer i = 0; i < objResponseBody.applicationout.sortedreasoncodetable.size(); i++) {
                        //sortedreasoncodetable sortedReasonCode = objResponseBody.applicationout.sortedreasoncodetable[i]; 
                        //lstCIBILDecisionOfApplication[i].Reason_Code__c = sortedReasonCode.reasonCode;
                        lstCIBILDecisionOfApplication[i].Reason_Code__c = objResponseBody.applicationout.sortedreasoncodetable[i];
                        lstCIBILDecisionOfApplication[i].Loan_Application__c = la.Id; 
                    }
                    for(Integer i = 0; i < objResponseBody.applicationout.decisionstatus.size(); i++) {
                        //decisionstatus decStatus = objResponseBody.applicationout.decisionstatus[i]; 
                        //lstCIBILDecisionOfApplication[i].Decision__c = decStatus.status;
                        lstCIBILDecisionOfApplication[i].Decision__c = objResponseBody.applicationout.decisionstatus[i];
                    }
                    for(Integer i = 0; i < objResponseBody.applicationout.deviationcode.size(); i++) {
                        //deviationcode devCode = objResponseBody.applicationout.deviationcode[i]; 
                        //lstCIBILDecisionOfApplication[i].Deviation_Code__c = devCode.code;
                        lstCIBILDecisionOfApplication[i].Deviation_Code__c = objResponseBody.applicationout.deviationcode[i];
                    }
                    for(Integer i = 0; i < objResponseBody.applicationout.ltv.size(); i++) {
                        //deviationcode devCode = objResponseBody.applicationout.deviationcode[i]; 
                        //lstCIBILDecisionOfApplication[i].Deviation_Code__c = devCode.code;
                        lstCIBILDecisionOfApplication[i].LTV__c = String.isBlank(objResponseBody.applicationout.ltv[i]) ?0: Decimal.valueOf(objResponseBody.applicationout.ltv[i]);
                    }
                    System.debug('Debug Log for lstCIBILDecisionOfApplication'+lstCIBILDecisionOfApplication.size());
                    if(!lstCIBILDecisionOfApplication.isEmpty()) {
                        List<Database.SaveResult> lstDSR = Database.insert(lstCIBILDecisionOfApplication, false);
                        for(Database.SaveResult objDSR : lstDSR) {
                            if(!objDSR.getErrors().isEmpty()) {
                                System.debug('Following Error Occured while saving CIBIL Decision of Application '+objDSR.getErrors());
                                resRep.ErrorMsg += objDSR.getErrors();
                                errorFlag = true;
                            }
                            else {
                                System.debug('CIBIL Decision of Application Record Saved Successfully, with Id'+objDSR.getId());
                            }
                        } 
                    } */
                }  
                else {
                    resRep.Result = 'Failed!! No loan application found for this Id.';
                    HttpResponse res = new HttpResponse();
                    res.setBody(String.valueOf(resRep)); 
                    return resRep;
                }   
            } else {
                resRep.Result = 'Failed!! Application Id not Specified.';
                HttpResponse res = new HttpResponse();
                res.setBody(String.valueOf(resRep)); 
                //LogUtility.createIntegrationLogs(RestContext.request.requestBody.tostring(),res,'Perfios Bank Statement Report Upload');
                return resRep;
            } 
            
            if(objResponseBody.applicationout.error != null) {
                for(Integer i = 0; i < objResponseBody.applicationout.error.size(); i++) {
                    //decisionstatus decStatus = objResponseBody.applicationout.decisionstatus[i]; 
                    //lstCIBILDecisionOfApplication[i].Decision__c = decStatus.status;
                    if(!String.isBlank(objResponseBody.applicationout.error[i].errorcode) || !String.isBlank(objResponseBody.applicationout.error[i].errordescription)) {
                        errorFlag = true;    
                    }
                }
            }
            //Update Loan Contact and related object CIBIL Decision Decision
            if(objResponseBody.applicant != null && objResponseBody.applicant.applicantOut != null && String.isNotBlank(objResponseBody.applicant.applicantOut.ccdid)) {
                lcList = [SELECT Id, Name, Decision_Category__c, Highest_Level_Refer__c 
                                        FROM Loan_Contact__c
                                        WHERE Id=: objResponseBody.applicant.applicantOut.ccdid];
                if(!lcList.isEmpty()) {
                    lc = lcList[0];
                    if(!String.isBlank(objResponseBody.applicant.applicantOut.decisioncategory)) {
                        lc.Decision_Category__c = objResponseBody.applicant.applicantOut.decisioncategory;
                    }
                    if(!String.isBlank(objResponseBody.applicant.applicantOut.decisioncomponenttreename)) {
                        lc.Highest_Level_Refer__c = objResponseBody.applicant.applicantOut.decisioncomponenttreename;
                    }    
                    if(!String.isBlank(objResponseBody.applicant.applicantOut.finaldecision)) {
                        lc.Final_Decision__c = objResponseBody.applicant.applicantOut.finaldecision;
                    } /*else {
                        resRep.Result = 'Failed!! Final Decision can not be blank.';
                        HttpResponse res = new HttpResponse();
                        res.setBody(String.valueOf(resRep)); 
                        //LogUtility.createIntegrationLogs(RestContext.request.requestBody.tostring(),res,'Perfios Bank Statement Report Upload');
                        return resRep;
                    } */
                    
                    List<CIBIL_Decision_Detail__c> lstCIBILDecision = new List<CIBIL_Decision_Detail__c>();
                    if(objResponseBody.applicant.applicantOut.sortedreasoncodetable != null && objResponseBody.applicant.applicantOut.decisionstatus != null) {
                        if((objResponseBody.applicant.applicantOut.sortedreasoncodetable.size() == objResponseBody.applicant.applicantOut.decisionstatus.size())) {
                            for(Integer i = 0; i < objResponseBody.applicant.applicantOut.sortedreasoncodetable.size(); i++) {
                                //sortedreasoncodetable sortedReasonCode = objResponseBody.applicant.applicantOut.sortedreasoncodetable[i]; 
                                //lstCIBILDecision[i].Reason_Code__c = sortedReasonCode.reasonCode;
                                CIBIL_Decision_Detail__c cibDecision = new CIBIL_Decision_Detail__c();
                                if(!String.isBlank(objResponseBody.applicant.applicantOut.sortedreasoncodetable[i])) {
                                    cibDecision.Reason_Code__c = objResponseBody.applicant.applicantOut.sortedreasoncodetable[i];
                                }
                                if(!String.isBlank(objResponseBody.applicant.applicantOut.decisionstatus[i])) {
                                    cibDecision.Decision__c = objResponseBody.applicant.applicantOut.decisionstatus[i];
                                }
                                /*if(!String.isBlank(objResponseBody.applicant.applicantOut.deviationcode[i])) {
                                    cibDecision.Deviation_Code__c = objResponseBody.applicant.applicantOut.deviationcode[i]; 
                                }*/
                                
                                cibDecision.Customer_Detail__c = objResponseBody.applicant.applicantOut.ccdid; 
                                if(la != null) {
                                    cibDecision.Loan_Application__c = la.Id;
                                }
                                if(!String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.parentId)) {
                                    Customer_Integration__c cust = new Customer_Integration__c();
                                    List<Customer_Integration__c> custList = new List<Customer_Integration__c>();
                                    custList = [SELECT Id from Customer_Integration__c WHERE Id=: objResponseBody.applicant.applicantOut.bureauSummary.parentId];
                                    if(!custList.isEmpty()) {
                                        cust = custList[0];
                                       cibDecision.Customer_Integration__c = cust.Id; 
                                    }
                                }    
                                lstCIBILDecision.add(cibDecision);
                            }
                           /* for(Integer i = 0; i < objResponseBody.applicant.applicantOut.decisionstatus.size(); i++) {
                                //decisionstatus decStatus = objResponseBody.applicant.applicantOut.decisionstatus[i]; 
                                //lstCIBILDecision[i].Decision__c = decStatus.status;
                                if(!String.isBlank(objResponseBody.applicant.applicantOut.decisionstatus[i])) {
                                    lstCIBILDecision[i].Decision__c = objResponseBody.applicant.applicantOut.decisionstatus[i];
                                }
                            }
                            for(Integer i = 0; i < objResponseBody.applicant.applicantOut.deviationcode.size(); i++) {
                                //deviationcode devCode = objResponseBody.applicant.applicantOut.deviationcode[i]; 
                                //lstCIBILDecision[i].Deviation_Code__c = devCode.code;
                                if(!String.isBlank(objResponseBody.applicant.applicantOut.deviationcode[i])) {
                                    lstCIBILDecision[i].Deviation_Code__c = objResponseBody.applicant.applicantOut.deviationcode[i]; 
                                }
                            } */
                        } else {
                            resRep.Result = 'Failed!! Invalid JSON, sorted reason code and decision status should be of same size.';
                            HttpResponse res = new HttpResponse();
                            res.setBody(String.valueOf(resRep)); 
                            //LogUtility.createIntegrationLogs(RestContext.request.requestBody.tostring(),res,'Perfios Bank Statement Report Upload');
                            return resRep;
                        }
                    }
                    Database.SaveResult result= Database.update(lc, false);
                    if(!result.getErrors().isEmpty()) {
                        System.debug('Following Error Occured while saving Loan Contact'+result.getErrors());
                        resRep.ErrorMsg += result.getErrors();
                        resRep.Result = 'Failed!!';
                        errorFlag = true;
                    }
                    else {
                        System.debug('Loan Contact Saved Successfully, with Id'+result.getId());
                        resRep.Result = 'Sucess!!';
                    }
                    System.debug('Debug Log for lstCIBILDecision'+lstCIBILDecision.size());
                    if(!lstCIBILDecision.isEmpty()) {
                        List<Database.SaveResult> lstDSR = Database.insert(lstCIBILDecision, false);
                        for(Database.SaveResult objDSR : lstDSR) {
                            if(!objDSR.getErrors().isEmpty()) {
                                System.debug('Following Error Occured while saving CIBIL Decision of Customer detail '+objDSR.getErrors());
                                resRep.ErrorMsg += objDSR.getErrors();
                                errorFlag = true;
                            }
                            else {
                                System.debug('CIBIL Decision of Customer Detail Record Saved Successfully, with Id'+objDSR.getId());
                            }
                        }
                    }
                }  else {
                    resRep.Result = 'Failed!! No customer detail record found for this Parent ID.';
                    HttpResponse res = new HttpResponse();
                    res.setBody(String.valueOf(resRep)); 
                    //LogUtility.createIntegrationLogs(RestContext.request.requestBody.tostring(),res,'Perfios Bank Statement Report Upload');
                    return resRep;
                }                       
            } else {
                resRep.Result = 'Failed!! CCCID not specified.';
                HttpResponse res = new HttpResponse();
                res.setBody(String.valueOf(resRep)); 
                //LogUtility.createIntegrationLogs(RestContext.request.requestBody.tostring(),res,'Perfios Bank Statement Report Upload');
                return resRep;
            }
            
            
            //Update Customer Integration Details
            if(objResponseBody.applicant != null && objResponseBody.applicant.applicantOut != null && objResponseBody.applicant.applicantOut.bureauSummary != null && String.isNotBlank(objResponseBody.applicant.applicantOut.bureauSummary.parentId)) {
                Customer_Integration__c cust = new Customer_Integration__c();
                List<Customer_Integration__c> custList = new List<Customer_Integration__c>();
                custList = [SELECT Id, No_of_Credit_Cards_DPD_since_1_Year__c, No_of_Credit_Cards_DPD_since_3_months__c, No_of_Credit_Cards_DPD_since_6_months__c, 
                            No_of_Credit_Cards_with_DPD_Till_Date__c, No_of_Enquiries_in_3_months__c, No_of_Enquiries_in_6_months__c, No_of_Enquiries_till_Date__c, 
                            No_of_Home_Loan_Enquiries_in_3_months__c, No_of_Home_Loan_Enquiries_in_6_months__c, No_of_Home_Loan_Enquiries_till_date__c, No_of_Loans_DPD_since_1_Year__c, 
                            No_of_Loans_DPD_since_3_Months__c, No_of_Loans_DPD_since_6_Months__c, No_of_Loans_with_DPD_Till_Date__c, No_of_Loans_negative_Status_since_1_year__c,
                            No_of_Loans_negative_Status_Since_3_mons__c, No_of_Loans_negative_Status_Since_6_mons__c, No_of_Loans_negative_Status_Till_Date__c,
                            Overdues_in_Credit_Cards_since_1_year__c, Overdues_in_Credit_Cards_since_3_months__c, 
                            Overdues_in_Credit_Cards_since_3_years__c, Overdues_in_Credit_Cards_since_6_months__c, Overdues_in_Credit_Cards_Till_Date__c, 
                            Overdues_in_Loans_since_1_year__c, Overdues_in_Loans_since_3_months__c, Overdues_in_Loans_since_3_years__c, 
                            Overdues_in_Loans_since_6_months__c, Overdues_in_Loans_Till_Date__c, Enquiry_Flag_for_HHFL_HFC_till_date__c, I_Cibil_Highest_Sanctioned_Amount__c,
                            Loan_Application__c, Loan_Contact__c, RecordType.Name
                            FROM Customer_Integration__c
                            WHERE ID =: objResponseBody.applicant.applicantOut.bureauSummary.parentId];
                
                if(!custList.isEmpty()) {
                    cust = custList[0];
                    cust.No_of_Credit_Cards_with_DPD_Till_Date__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totalccwithdpdtilldate) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totalccwithdpdtilldate);
                    cust.No_of_Credit_Cards_DPD_since_1_Year__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totalccwithdpdinlast1yr) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totalccwithdpdinlast1yr);
                    cust.No_of_Credit_Cards_DPD_since_6_months__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totalccwithdpdinlast6months) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totalccwithdpdinlast6months);
                    cust.No_of_Credit_Cards_DPD_since_3_months__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totalccwithdpdinlast3months) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totalccwithdpdinlast3months);
                    cust.No_of_Loans_with_DPD_Till_Date__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totalloanswithdpdtilldate) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totalloanswithdpdtilldate);
                    cust.No_of_Loans_DPD_since_1_Year__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totalloanswithdpdinlast1yr) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totalloanswithdpdinlast1yr);
                    cust.No_of_Loans_DPD_since_6_Months__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totalloanswithdpdinlast6months) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totalloanswithdpdinlast6months);
                    cust.No_of_Loans_DPD_since_3_Months__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totalloanswithdpdinlast3months) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totalloanswithdpdinlast3months);
                    cust.No_of_Loans_negative_Status_Till_Date__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totalloanswithnegativestatustilldate) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totalloanswithnegativestatustilldate);
                    cust.No_of_Loans_negative_Status_Since_6_mons__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totalloanswithnegativestatusinlast6months) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totalloanswithnegativestatusinlast6months);
                    cust.No_of_Loans_negative_Status_since_1_year__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totalloanswithnegativestatusinlast1yr) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totalloanswithnegativestatusinlast1yr);
                    cust.No_of_Loans_negative_Status_Since_3_mons__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totalloanswithnegativestatusinlast3months) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totalloanswithnegativestatusinlast3months);
                    cust.Overdues_in_Credit_Cards_Till_Date__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduescctilldate) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduescctilldate);
                    cust.Overdues_in_Credit_Cards_since_3_years__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduesccinlast3yrs) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduesccinlast3yrs);
                    cust.Overdues_in_Credit_Cards_since_1_year__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduesccinlast1yrs) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduesccinlast1yrs);
                    cust.Overdues_in_Credit_Cards_since_6_months__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduesccinlast6months) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduesccinlast6months);
                    cust.Overdues_in_Credit_Cards_since_3_months__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduesccinlast3months) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduesccinlast3months);
                    cust.Overdues_in_Loans_Till_Date__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduesloanstilldate) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduesloanstilldate);
                    cust.Overdues_in_Loans_since_3_years__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduesloansinlast3yrs) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduesloansinlast3yrs);
                    cust.Overdues_in_Loans_since_1_year__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduesloansinlast1yrs) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduesloansinlast1yrs);
                    cust.Overdues_in_Loans_since_6_months__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduesloansinlast6months) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduesloansinlast6months);
                    cust.Overdues_in_Loans_since_3_months__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduesloansinlast3months) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.totaloverduesloansinlast3months);
                    cust.No_of_Enquiries_in_3_months__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.enquiresloansinlast3months) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.enquiresloansinlast3months);
                    cust.No_of_Enquiries_in_6_months__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.enquiresloansinlast6months) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.enquiresloansinlast6months);
                    cust.No_of_Enquiries_till_Date__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.enquiresloanstilldate) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.enquiresloanstilldate);
                    cust.No_of_Home_Loan_Enquiries_in_3_months__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.enquireshlloansinlast3months) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.enquireshlloansinlast3months);
                    cust.No_of_Home_Loan_Enquiries_in_6_months__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.enquireshlloansinlast6months) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.enquireshlloansinlast6months);
                    cust.No_of_Home_Loan_Enquiries_till_date__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.enquireshlloanstilldate) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.enquireshlloanstilldate);
                    cust.Enquiry_Flag_for_HHFL_HFC_till_date__c = objResponseBody.applicant.applicantOut.bureauSummary.enquireshhflhfclflagtilldate;
                    cust.I_Cibil_Highest_Sanctioned_Amount__c = String.isBlank(objResponseBody.applicant.applicantOut.bureauSummary.highestsanctioncreditamount) ?0: Decimal.valueOf(objResponseBody.applicant.applicantOut.bureauSummary.highestsanctioncreditamount);
                    if(!errorFlag) {
                        cust.BRE11_Success__c = true;  
                        Database.SaveResult result= Database.update(cust, false);
                        if(!result.getErrors().isEmpty()) {
                            System.debug('Following Error Occured while saving cust'+result.getErrors());
                            resRep.ErrorMsg += result.getErrors();
                            resRep.Result = 'Failed!!';
                            errorFlag = true;
                        }
                        else {
                            System.debug('Cust Saved Successfully, with Id'+result.getId());
                            resRep.Result = 'Sucess!!';
                        }
                    } else {
                        resRep.Result = 'Failed!!';
                    }
                } else {
                    resRep.Result = 'Failed!! No customer integration record found for this Parent ID.';
                    HttpResponse res = new HttpResponse();
                    res.setBody(String.valueOf(resRep)); 
                    //LogUtility.createIntegrationLogs(RestContext.request.requestBody.tostring(),res,'Perfios Bank Statement Report Upload');
                    return resRep;
                }                   
            }else {
                resRep.Result = 'Failed!! Parent ID not specified';
                HttpResponse res = new HttpResponse();
                res.setBody(String.valueOf(resRep)); 
                //LogUtility.createIntegrationLogs(RestContext.request.requestBody.tostring(),res,'Perfios Bank Statement Report Upload');
                return resRep;
            }
            if(!errorFlag) {
                if(lc != null) {
                    lc.BRE11_Success__c = true;
                    update lc;
                    /*if(lc.Final_Decision__c == 'Red' && la.BRE_Approval_Status__c != 'Unapproved') {
                        la.BRE_Approval_Status__c = 'Unapproved';
                    }
                    update la;*/
                    
                }
            }
        } 
        else {
            resRep.Result = 'Failed!! Response body received in SFDC is null.';
            HttpResponse res = new HttpResponse();
            res.setBody(String.valueOf(resRep)); 
            return resRep;
        }
        HttpResponse res = new HttpResponse();
        res.setBody(String.valueOf(resRep)); 
        //LogUtility.createIntegrationLogs(RestContext.request.requestBody.tostring(),res,'Perfios Bank Statement Report Upload');
        return resRep;
    }
    
	global class Applicationout {
		public String strategytag;
		public String clientname;
		public String originationsourceid;
		public String requestid;
		public String requesteddatetime;
		public String applicationid;
		public String businessdate;
		public String responsedatetime;
		public String appliedloanamount;
		public String emi;
		public String tenureapplied;
		public String productcode;
		public String subproduct;
		public String decisioncategory;
		public String decisioncomponenttreename;
		public String[] decisionstatus;
		public String[] sortedreasoncodetable;
		public String[] deviationcode;
		public String[] ltv;
		public String roi;
		public Error[] error;
		public Applicationout(String applicationid) {
		    this.strategytag = '';
		    this.clientname = '';
		    this.originationsourceid = '';
		    this.requestid = '';
		    this.requesteddatetime = '';
		    this.applicationid = applicationid;
		    this.businessdate = '';
		    this.responsedatetime = '';
		    this.appliedloanamount = '';
		    this.emi = '';
		    this.tenureapplied = '';
		    this.productcode = '';
		    this.subproduct ='';
		    this.decisioncategory ='';
		    this.decisioncomponenttreename = '';
		    this.roi = '';
		}
		public Applicationout(String applicationid, Error[] error) {
		    this.strategytag = '';
		    this.clientname = '';
		    this.originationsourceid = '';
		    this.requestid = '';
		    this.requesteddatetime = '';
		    this.applicationid = applicationid;
		    this.businessdate = '';
		    this.responsedatetime = '';
		    this.appliedloanamount = '';
		    this.emi = '';
		    this.tenureapplied = '';
		    this.productcode = '';
		    this.subproduct ='';
		    this.decisioncategory ='';
		    this.decisioncomponenttreename = '';
		    this.roi = '';
		    this.error = error;
		}
		public Applicationout(){
		    
		}
	}

    global class BRE1ExperionResp {
    	public Applicationout applicationout;
	    public Applicant applicant;
	    public BRE1ExperionResp(Applicationout applicationout, Applicant applicant) {
	        this.applicationout = applicationout;
	        this.applicant = applicant;
	    }
	    public BRE1ExperionResp() {
	        
	    }
    }

	global class Error {
		public String errorcode;
		public String errordescription;
		public Error() {
		    
		}
		public Error(String errorcode, String errordescription) {
		    this.errorcode = errorcode;
		    this.errordescription = errordescription;
		}
	}

	global class ApplicantOut {
	    public String ccdid;
		public String decisioncategory;
		public String decisioncomponenttreename;
		public String[] sortedreasoncodetable;
		public String[] decisionstatus;
		public String[] deviationcode;
		public String finaldecision;
		public BureauSummary bureauSummary;
		public ApplicantOut() { 
		    
		}
		public ApplicantOut(String ccdid, String finaldecision, BureauSummary bureauSummary, String[] sortedreasoncodetable, String[] decisionstatus, String[] deviationcode) {
		    this.ccdid = ccdid;
		    this.decisioncategory = 'ABG';
		    this.decisioncomponenttreename = 'ABC';
		    this.finaldecision = finaldecision;
		    this.bureauSummary = bureauSummary;
		    this.sortedreasoncodetable = sortedreasoncodetable;
		    this.decisionstatus = decisionstatus;
		    this.deviationcode = deviationcode;
		}
		
	}

	global class BureauSummary {
	    public String parentId;
		public String totalccwithdpdtilldate;
		public String totalccwithdpdinlast1yr;
		public String totalccwithdpdinlast6months;
		public String totalccwithdpdinlast3months;
		public String totalloanswithdpdtilldate;
		public String totalloanswithdpdinlast1yr;
		public String totalloanswithdpdinlast6months;
		public String totalloanswithdpdinlast3months;
		public String totalloanswithnegativestatustilldate;
		public String totalloanswithnegativestatusinlast6months;
		public String totalloanswithnegativestatusinlast1yr;
		public String totalloanswithnegativestatusinlast3months;
		public String totaloverduescctilldate;
		public String totaloverduesccinlast3yrs;
		public String totaloverduesccinlast1yrs;
		public String totaloverduesccinlast6months;
		public String totaloverduesccinlast3months;
		public String totaloverduesloanstilldate;
		public String totaloverduesloansinlast3yrs;
		public String totaloverduesloansinlast1yrs;
		public String totaloverduesloansinlast6months;
		public String totaloverduesloansinlast3months;
		public String enquiresloansinlast3months;
		public String enquiresloansinlast6months;
		public String enquiresloanstilldate;
		public String enquireshlloansinlast3months;
		public String enquireshlloansinlast6months;
		public String enquireshlloanstilldate;
		public String enquireshhflhfclflagtilldate;
		public String highestsanctioncreditamount;
		public BureauSummary() {
		    
		}
		
		public BureauSummary(String parentId) {
		    this.parentId = parentId;
            this.totalccwithdpdtilldate = '';
            this.totalccwithdpdinlast1yr = '';
            this.totalccwithdpdinlast6months = '';
            this.totalccwithdpdinlast3months = '';
            this.totalloanswithdpdtilldate = '';
            this.totalloanswithdpdinlast1yr = '';
            this.totalloanswithdpdinlast6months = '';
            this.totalloanswithdpdinlast3months = '';
            this.totalloanswithnegativestatustilldate = '';
            this.totalloanswithnegativestatusinlast6months = '';
            this.totalloanswithnegativestatusinlast1yr = '';
            this.totalloanswithnegativestatusinlast3months = '';
            this.totaloverduescctilldate = '';
            this.totaloverduesccinlast3yrs = '';
            this.totaloverduesccinlast1yrs = '';
            this.totaloverduesccinlast6months = '';
            this.totaloverduesccinlast3months = '';
            this.totaloverduesloanstilldate = '';
            this.totaloverduesloansinlast3yrs = '';
            this.totaloverduesloansinlast1yrs = '';
            this.totaloverduesloansinlast6months = '';
            this.totaloverduesloansinlast3months = '';
            this.enquiresloansinlast3months = '';
            this.enquiresloansinlast6months = '';
            this.enquiresloanstilldate = '';
            this.enquireshlloansinlast3months = '';
            this.enquireshlloansinlast6months = '';
            this.enquireshlloanstilldate = '';
            this.enquireshhflhfclflagtilldate = '';
            this.highestsanctioncreditamount = '';
		}
	}

	global class Applicant {
		public ApplicantOut applicantOut;
		public Applicant() {
		    
		}
		public Applicant(ApplicantOut applicantOut) {
		    this.applicantOut = applicantOut;
		}
	}
	global class sortedreasoncodetable {
        public String reasonCode;         
	}
    global class decisionstatus{
        public String status;    
    }
	global class deviationcode{
	    public String code;
	}
	global class ltv {
	    public String ltv;
	}
	public static BRE1ExperionResponse parse(String json) {
		return (BRE1ExperionResponse) System.JSON.deserialize(json, BRE1ExperionResponse.class);
	}
	
	global class ResultResponse{
        String ErrorMsg;
        String Result;
        public ResultResponse(){
            ErrorMsg = '{}';
            Result = '';
        }
    }
}