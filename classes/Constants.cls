public class Constants{
  
  
   /*****************************Begin : Added by Shobhit Saxena for APF**************************************/
   
    //APF Project Constants : Account Record Types DeveloperName
    public static final String strAccRecTypeBIndv = 'Builder - Individual';
    public static final String strAccRecTypeBCorp = 'Builder - Corporate';
    
    //APF Project Constants : Project Builder Record Types DeveloperName
    public static final String strPBRecTypeIndv = 'Builder Individual';
    public static final String strPBRecTypeCorp = 'Builder Corporate';
    public static final String strPBRecTypeReadOnlyIndv = 'Read Only - Individual';
    public static final String strPBRecTypeReadOnlyCorp = 'Read Only - Corporate';
    
    //APF Project Constants : Object API Names
    public static final String strProjectBuilderAPI = 'Project_Builder__c';
    public static final String strPromoterAPI = 'Promoter__c';
    public static final String strCustomerIntegrationAPI = 'Customer_Integration__c';
    public static final String strBuilderIntegrationAPI = 'Builder_Integrations__c';
    public static final String strPromoterIntegrationAPI = 'Promoter_Integrations__c';
    
    //APF Project Constants : Record Type Third Party Verifications
    public static final String strRecordTypeTPVLegal = 'Legal';
    public static final String strRecordTypeTPVTechnical = 'Technical';
    public static final String strRecordTypeTPVFCU = 'FCU';
    
    //APF Project Constants : Record Type Projects
    
    public static final String strRecordTypeFileChecker = 'File Check Scanner';
    public static final String strRecordTypeCreditReview = 'Credit:Review and Decision';
    public static final String strRecordTypeCOPSDM = 'COPS:Data Maker';
    public static final String strRecordTypeCOPSDC = 'COPS:Data Checker';
    public static final String strRecordTypeReadOnly = 'Read Only';
    public static final String strRecordTypeAppComm = 'Approval Communication';
    public static final String strRecordTypeCreditApproval = 'Inventory Approval Credit';
    public static final String strRecordTypeRejected = 'Rejected';
    
    //APF Project Constants : Project Builder - Builder Type
    public static final String strProjectBuilderBTIndv = 'Individual';
    public static final String strProjectBuilderBTCorp = 'Corporate';
    
    //APF Project Constants : Project Builder - Builder Role
    public static final String strPBConstructorMarketer = 'Constructor & Marketer';
    public static final String strPBLandOwner = 'Land Owner';
    
    //APF Project Constants : Project Builder - Builder Category
    public static final String strPBCategoryCatA = 'Cat A';
    public static final String strPBCategoryCatB = 'Cat B';
    public static final String strPBCategoryCatC = 'Cat C';
    
    //APF Project Constants : Project Document Checklist - Status
    public static final String strDocStatusPending = 'Pending';
    public static final String strDocStatusReceived = 'Received';
    public static final String strDocStatusUploaded = 'Uploaded';
    public static final String strDocStatusWaiverReqstd = 'Waiver Requested';
    public static final String strDocStatusDigVerified = 'Digitally Verified';
    public static final String strDocStatusPhyVerified = 'Physically Verified';
    public static final String strDocStatusWaivedOff= 'Waived Off';
    public static final String strDocStatusEmailed = 'Emailed';
    public static final String strDocStatusOTCApprvd = 'OTC Approved';
    public static final String strDocStatusPDD = 'PDD';
    public static final String strDocStatusOTCRqstd = 'OTC Requested';
    public static final String strDocStatusLawyerOTC = 'Lawyer OTC';
    public static final String strDocStatusPreDisbursal = 'Pre Disbursal';
    
    //IMGC Status
    public static final String NotAvailedByCustomer = 'Not Availed by Customer'; //Added by Saumya on 7th September 2020 FOR IMGC BRD
    
    public static final String IMGCUsers = 'IMGC Users'; //Added by Saumya on 8th September 2020 FOR IMGC BRD
    
    //APF Project Constants : APF Type
    public static final String strApfTypeDeemed = 'Deemed';
    public static final String strApfTypeNew = 'New';

    //APF Project Constants : Stage
    public static final String strApfStgAppInit = 'APF Initiation';
    public static final String strApfStgOpsControl = 'APF Operations Control';
    public static final String strApfStgCredReview = 'APF Credit Review';
    public static final String strApfStgApprovalComm = 'Approval Communication';
    public static final String strApfStgInvMaintenance = 'APF Inventory Maintenance';
    
    //APF Project Constants : Sub-Stage
    public static final String strApfSubStgPDC = 'Project Data Capture';
    public static final String strApfSubStgFileChckr = 'File Checker';
    public static final String strApfSubStgScnMkr = 'Scan Maker';
    public static final String strApfSubStgScnChkr = 'Scan Checker';
    public static final String strApfSubStgCOPSDataMaker = 'COPS : Data Maker';
    public static final String strApfSubStgCOPSDataChecker = 'COPS : Data Checker';
    public static final String strApfSubStgCredReview = 'APF Credit Review';
    public static final String strApfSubStgApprovalComm = 'Approval Communication';
    public static final String strApfSubStgInvUpdate = 'APF Inventory Update';
    public static final String strApfSubStgInvApproval = 'APF Inventory Approval';
    
    
    //APF Project Constants : Status
    public static final String strApfStatusInitiated = 'APF Initiated';
    public static final String strApfStatusOpsControl = 'Operations Control';
    public static final String strApfStatusCredReview = 'Credit Review';
    public static final String strApfStatusAPFApproved = 'APF Approved';
    public static final String strApfStatusInvApprvd = 'APF Inventory Approved';
    public static final String strApfStatusBlckListed = 'Blacklisted';
    
    //APF Project Constants : Project Status
    public static final String strApfProjStatusUC = 'Under Construction';
    public static final String strApfProjStatusCompleted = 'Completed';
    
    //APF Project Constants : Document Type
    public static final String strAPFProjDocTypeBuilder = 'Builder - APF';
    public static final String strAPFProjDocTypeLegal = 'Legal - APF';
    public static final String strAPFProjDocTypeTechnical = 'Technical - APF';
    public static final String strAPFProjDocTypeRERA = 'RERA - APF';
    
    
    
    /*****************************End : Added by Shobhit Saxena for APF**************************************/

																		  
																			  
																  
  
  //Added by Shobhit Saxena to fix S3 Functionality on 23 September 2019
  public static final String strParentObjectAPIName = 'Loan_Application__c';
  public static final String strApplicantConstant = 'Applicant';
  
  //End of Addition made by Shobhit Saxena to fix S3 Functionality on 23 September 2019
  
  
    // webservice constantsKYC Documents
    public static final String eKYC = 'KYC Documents';
    public static final string Sales = 'Sales';
    public static final string Permanent_Address = 'Permanent Address Proof';
    public static final string Current_Address = 'Current Address Proof';
    public static final string Emailed = 'Email';
    public static final string Bounced = 'B';
    public static final string waiver_requested = 'Waiver Requested';
    public static final string waived_off = 'Waived Off';
																														  
     
      //Address Type Constants 
    
    public static final String Current_Office = 'CUROFF';
    public static final String Permanent = 'PERMNENT';
    public static final String Current_Residence = 'CURRES';
    public static final String Both_Address = 'Both';
    public static final String addTypeOffice_Business = 'OFFICE';
    public static final String addTypeResidence_cum_Office_Address = 'OFFRES';
    public static final String OTHER_ADDRESS = 'OTHER';
    
    //Constitution Constants
    
    public static final String strPRIVATE_LIMITED = '18';
    public static final String strPUBLIC_LIMITED = '19';
    public static final String strINDIVIDUAL = '20';
    public static final String strHUF = '21';
    public static final String strPROPRIETORSHIP_FIRM = '22';
    public static final String strPARTNERSHIP_FIRM = '23';
    public static final String strTrust_Society = '24';
    public static final String strLLPF = '25';
    
    //Charge Code Name Constants
    
    public static final String strIMDCharges = 'SHORT RECEIVED(INITIAL MONEY) equal to balance PF';
    public static final String strROCCharges = 'ROC CHARGES';
    public static final String strCERSAICharges = 'CERSAI Charges';
    public static final String strStampDutyCharges = 'STAMP DUTY';
    public static final String strLegalVettingCharges = 'LEGAL CHARGES';
    public static final String strTotalPFAmount = 'APPLICATION PROCESSING FEES';
    public static final String strPEMI = 'PEMI';
    
    
    //Payment Mode Constants - Disbusrsement
    
    public static final String strCheque = 'Q';
    public static final String strDemand_Draft = 'D';
    public static final String strFund_Transfer = 'F';
    
    //Broken Period Interest Handling Constants
    
    public static final String strAdd_To_Schedule = 'Add to schedule';
    public static final String strReturn_As_Charge = 'Return as charge';
    
    //Charge Code Id Constants
    
    public static final String strROCChargeCode = '500552';
    public static final String strCERSAIChargeCode = '500561';
    public static final String strStampDutyChargeCode = '500547';
    public static final String strLegalVettingChargeCode = '500553';
    public static final String strPEMIChargeCode = '500571';
    
    
    //Rate Type Constants
    public static final String rateTypeEMI = 'EMI';
    public static final String rateTypeRate = 'Rate';
    
    
    // sub stages constants
    public static final string COPS_Credit_Operations_Entry_rt = 'COPS_Credit_Operations_Entry';
    public static final string COPS_Data_Checker_rt = 'COPS_Data_Checker';
    public static final string COPS_Data_Maker_rt = 'COPS_Data_Maker';
    public static final String Application_Initiation = 'Application Initiation';
    public static final String Express_Queue_Data_Maker = 'Express Queue: Data Maker';
    public static final String Express_Queue_Data_Checker = 'Express Queue: Data Checker';
    public static final String Document_collection = 'Document collection';
    public static final String File_Check = 'File Check';
    public static final String Scan_Data_Maker = 'Scan: Data Maker';
    public static final String Scan_Data_Checker = 'Scan: Data Checker';
    public static final String COPS_Data_Maker = 'COPS:Data Maker';
    public static final String COPS_Data_Checker = 'COPS:Data Checker';
    public static final String COPS_Credit_Operations_Entry = 'COPS: Credit Operations Entry';
    public static final String Credit_Review = 'Credit Review';
    public static final String Credit_Data_Maker = 'Credit: Data Maker';
    public static final String Credit_Data_Checker = 'Credit: Data Checker';
    public static final String Credit_Review_and_Decision = 'Credit:Review and Decision';
    public static final String Customer_Negotiation = 'Customer Negotiation';
    public static final String Document_approval ='Document Approval';
    public static final String Credit_Approval = 'Credit Approval';
    public static final String Sales_Approval = 'Sales Approval';
    public static final String Docket_Scan = 'Docket Scan';
    public static final String Docket_Checker = 'Docket Checker';
    public static final String Loan_Engine_2_Data_Maker = 'Loan Engine 2: Data Maker';
    public static final String Express_Queue_Loan_Engine_1_Completed = 'Express Queue : Loan Engine 1 Completed';
    public static final String Loan_Cancelled = 'Loan Cancelled';
    public static final string L1_Credit_Approval= 'L1 Credit Approval';
    public static final String Loan_Disbursal = 'Loan Disbursal';
    public static final String Scan_Maker = 'Scan Maker';
    public static final String Scan_Checker = 'Scan Checker';
    public static final String Hand_Sight = 'Hand Sight';
    public static final String Disbursement_Maker = 'Disbursement Maker';
    public static final String Disbursement_Checker = 'Disbursement Checker';
    public static final String OTC_Receiving = 'OTC Receiving';
    public static final String CPC_Scan_Maker = 'CPC Scan Maker';
    public static final String CPC_Scan_Checker = 'CPC Scan Checker';
    public static final String CPC_Data_Maker ='CPC Data Maker';
    public static final String Loan_Disbursed = 'Loan Disbursed';
    public static final string Financial_Data_entry_pdf = 'Financial Data entry- pdf';
    public static final string Financial_Data_entry_excel = 'Financial Data entry- excel';
    public static final string CIBIL_Reject = 'CIBIL Reject'; //Added by saumya For CIBIL 
    public static final string Loan_reject = 'Loan reject'; //Added by saumya For CIBIL 
    public static final string Loan_Cancel = 'Loan Cancel'; //Added by Abhilekh on 19th June 2019 for TIL-816
    public static final string FCU_Review = 'FCU Review'; //Added by Abhilekh on 10th September 2019 for FCU BRD
    public static final string Hunter_Review = 'Hunter Review'; //Added by Abhilekh on 24th January 2020 for Hunter BRD
    
    public static final String Credit_authority_Approval ='Credit Authority Approval';
    public static final String FTNR_Rejected ='FTNR Rejected';
    public static final String Re_Credit ='Re Credit';
    public static final String Re_Look ='Re-Look'; //Added by Abhilekh on 24th April 2019
    // stages constants
    
    public static final String Customer_Onboarding = 'Customer Onboarding';
    public static final String Operation_Control = 'Operation Control';
    public static final String Credit_Decisioning = 'Credit Decisioning';
    public static final String Customer_Acceptance = 'Customer Acceptance';
    public static final String Loan_Rejected = 'Loan Rejected';
    public static final String Loan_Dispersed = 'Loan Dispersed';
    public static final String Express_Queue = 'Express Queue';
    public static final String Physical_Queue = 'Physical Queue';
    public static final String Individual = 'Individual';
    public static final String Queue = 'Queue';
    public static final String Tranche = 'Tranche';
    
    // Transaction Type constants
    public static final String Topup = 'TOP';
    public static final String BT_Topup = 'BT';
    public static final String Rebook = 'Rebook';
	/** Added by Saumya for BT TOP additional picklist Values **/
    public static final String TopUpLAPResidential = 'TLR';
    public static final String TopUpLAPCommercial	 = 'TLC';
    public static final String TopUpLAPIndustrial	 = 'TLI';
    public static final String BTHL = 'BHL';
    public static final String BTLAPIndustrial = 'BLI';
    public static final String BTLAPCommercial	 = 'BLC';
    public static final String BTLAPResidential	 = 'BLR';
/** Added by Saumya for BT TOP additional picklist Values **/ public static final String MultiTranche = 'Multiple Tranche';
    public static final String SingleTranche = 'Single Tranche';
    
    
    // Documents constants
    
    public static final String Uploaded = 'Uploaded';
    public static final String Property_Proof ='Property Documents';
    public static final String KYC ='KYC';
    public static final String Pending = 'Pending';
    public static final String Digitally_Verified = 'Digitally Verified';
    public static final string Property_Document = 'Property Document';
    public static final String strPDD = 'PDD';
    public static final String strWaivedOff = 'Waived Off';
    
     
     //Profiles
    public static final String FI_Vendor_Profile = 'FI Vendor';
    public static final String Technical_Profile = 'Technical Vendor';
    public static final String Legal_Profile = 'Legal Vendor';
    public static final String FCU_Profile = 'FCU Vendor';
    public static final String FCU_Internal_Profile = 'FCU Internal Team';
    
    
    // Third Party Verification  
    public static final String FI = 'FI';
    public static final String TECHNICAL = 'Technical';
    public static final String LEGAL = 'Legal';
    public static final String FCU_INTERNAL = 'FCU Internal';
    public static final String FCU = 'FCU';
    public static final String LIP = 'LIP';
    public static final String VENDOR_PD = 'Vendor PD';    
    public static final String NEW_VALUE = 'New';
    public static final String ASSIGNED = 'Assigned';
    public static final String RESIDENCE_ADDRESS = 'Residence Address';
    public static final String RESIDENCE_CUM_OFFICE_ADDRESS = 'Residence cum Office Address';
    public static final String OFFICE_BUSINESS = 'Office/ Business';
    public static final String POSITIVE = 'Positive';
    public static final String EVALUATE = 'Evaluate';
    public static final String REJECT = 'Reject';
    public static final String MITIGATE = 'Mitigate';
    public static final String APPROVE = 'Approve';
    public static final String NEGATIVE = 'Negative';
    public static final String CNV = 'Could Not Verify'; //Modified by Abhilekh on 9th September 2019 for FCU BRD
    public static final String RTC = 'Refer to Credit';
    public static final String LT_MSG = 'Please add at least one Property for assigning Legal/ Technical verification.';
    public static final String NO_ADD = 'Verifications cannot be assigned as there are no address records.';
    public static final String ALREADY_ASSIGNED = 'Verifications have already been assigned for this Loan Application.';
    public static final String BAD_STAGE = 'Verifications cannot be assigned at this sub stage.';
    public static final Decimal LOAN_AMOUNT = 5000000;
    public static final Decimal COUNT = 2;
    public static final String PROPERTY_PAPERS = 'Property Papers';
    public static final String LIQUID = 'Liquid Income Program';
    public static final String SALARIED = '1';
    public static final String INITIATE = 'Initiate PD';
    public static final String LEGAL_VETTING = 'Legal Vetting';
    public static final String INITIATED = 'Initiated';
    public static final String COMPLETED = 'Completed';
    public static final String Read = 'Read';
    public static final String Manual = 'Manual';
    public static final Decimal MIN_COUNT = 3;
    public static final String Minimum_Image_Error = 'Please add atleast 3 images before marking this report as completed.';
    public static final String PNG = 'PNG';
    public static final String JPG = 'JPG';
    public static final String Pre_Sanction = 'Pre Sanction'; //Added by Abhilekh on 5th September 2019 for FCU BRD
    public static final String Post_Sanction = 'Post Sanction'; //Added by Abhilekh on 5th September 2019 for FCU BRD
    public static final String Cleared = 'Cleared'; //Added by Abhilekh on 21st January 2020 for Hunter BRD
    public static final String Not_Cleared = 'Not Cleared'; //Added by Abhilekh on 21st January 2020 for Hunter BRD
    public static final String Hunter_Managers = 'Hunter Managers'; //Added by Abhilekh on 24th January 2020 for Hunter BRD
	public static final String Banking_Surrogate = 'Banking Surrogate'; //Added by Abhilekh for BRE2 Enhancements									  
    
    //Sharing Row Cause value
    
    public static final String Row_Cause = 'Manual';
    
    // Loan Applcation Record Types
    public static final string Salaried_RECORD = 'Salaried';
    public static final string READONLY_RECORD = 'Readonly Record';
    public static final string DATA_ENTRY_RECORD = 'Data Entry';
    public static final string Corporate_RECORD ='Corporate';
   // public static final string SCAN_DATA_MAKER_RECORD = 'Scan: Data Maker';
  //  public static final string SCAN_DATA_CHECKER_RECORD = 'Scan: Data Checker';
    public static final string DOCUMENT_COLLECTION_RECORD = 'Document Collection';
    public static final string CIBIL_Reject_RECORD = 'CIBIL Reject';
    public static final string FILE_CHECK_RECORD = 'File check Scanner';
    public static final string APPLICATION_INITIATION_RECORD = 'Application Initiation';
    public static final string Individual_API = '1';
    public static final string Corporate_API = '2';    
    
    public static final string COPS_CREDIT_OPERATION_RECORD = 'COPS: Credit Operations Entry';
    public static final string COPS_DATA_CHECKER_RECORD = 'COPS:Data Checker';
    public static final string COPS_DATA_MAKER_RECORD = 'COPS:Data Maker';
    public static final string CoApplicant_RECORD ='Co-Applicant';
    public static final string Guarantor_RECORD ='Guarantor';
    public static final string Others_RECORD ='Others';
    public static final string COAPP ='Co- Applicant';
    public static final string SEP ='Self Employed Professional';
    public static final string SENP ='Self Employed Non Professional';
    public static final string Promoter ='Promoter'; //Added by Abhilekh on 11th December 2019 for Hunter BRD
    public static final string Co_Promoter = 'Co-Promoter'; //Added by Abhilekh on 11th December 2019 for Hunter BRD
    public static final string Express_Queue_Data_Maker_Record = 'Express Queue Data Maker';   // Added by Vaishali for BRE
    
    // Queues
    public static final string EXPRESS_DATA_MAKER_QUEUE = 'Express Data Maker Queue';
    public static final String EXPRESS_DATA_CHECKER_QUEUE = 'Express Data Checker Queue';
    public static final string COPS_DATA_MAKER_QUEUE = 'COPS Data Maker';
    public static final string COPS_DATA_CHECKER_QUEUE = 'COPS Data Checker';
    public static final string COPS_CREDIT_OPERATION_ENTRY_QUEUE = 'COPS Credit Operations Entry';
    public static final string CREDIT_MANAGER_QUEUE = 'Credit Manager Queue';



    //Record Type - OCR
    public static final string AADHAAR = 'Aadhaar';
    public static final string OCREKYC = 'eKYC';
    public static final string GSTIN = 'GSTIN';
    public static final string ITR = 'ITR';
    public static final string PAN = 'PAN';
    public static final string PASSPORT = 'Passport'; 
    public static final string VOTERID = 'Voter ID';
    public static final string Applicant = 'Applicant';
    
    //Record Type - Loan Application 
    public static final String TRANCERECTYPE = 'Tranche';
    
    //OCR
    public static final string TXTDETECT = 'TEXT_DETECTION';
    public static final string TXTANNOTATE = 'textAnnotations';
    public static final string OCR_DESC = 'description';
    public static final string POST = 'POST';
    
    //Financials
    public static final string BALANCE = 'Balance Sheet';
    public static final string PROLOSS = 'Profit & Loss';
    public static final string CRED_BS = 'Credit - Balance Sheet';
    public static final string DEB_BS = 'Debit - Balance Sheet';
    public static final string CRED_PL = 'Credit - Profit Loss';
    public static final string DEB_PL = 'Debit - Profit Loss';
    public static final string RATIO = 'Ratio';
    
    public static final string PROPERTYPAPERS = 'Property Papers';
    public static final string DIGIVERIFIED = 'Digitally Verified';
    public static final string DOC_STATUS_PENDING = 'Pending';
    public static final string DOC_STATUS_RECIEVED = 'Received';
    public static final string DOC_STATUS_UPLOADED = 'Uploaded';
    public static final string THIRD_PARTY_VERIFICATION_INITIATED = 'Initiated';
    public static final string ACCESS_LEVEL_EDIT = 'Edit';
    public static final string DOC_CHECK_STATUS = 'OTC Approved';
    public static final string RECORD_TYPE_FCU = 'FCU';
    
    //***Tranche****
    //Tranche Request Type
    public static final String TRANCHEDISBURSAL = 'Tranche Disbursal';
    //Tranche Status
    public static final String STATUS_INITIATED = 'Initiated';
    public static final String STATUS_DRAFT = 'Draft';
    public static final String STATUS_APPROVAL_INITIATED = 'Credit Manager Approval Initiated';
    public static final String STATUS_DISB_APPROVAL_INITIATED = 'Disbursment Maker Approval Initiated';
    public static final String STATUS_DISB_CHECKER_APPROVAL_INITIATED = 'Disbursment Checker Approval Initiated';
    public static final String STATUS_APPROVAL = 'Approved';
    public static final String STATUS_REJECTED = 'Rejected';
    public static final String STATUS_CANCELLED = 'Cancelled';
    public static final String STATUS_DISBURSED = 'Disbursed';
    public static final String STATUS_OTC_PENDING = 'OTC Pending';
    
    //Tranche Flow
    public static final String Tranche_Initiation = 'Tranche Processing';
    public static final String Tranche_File_Check = 'Tranche File Check';
    public static final String Tranche_Scan_Maker = 'Tranche Scan Maker';
    public static final String Tranche_Scan_Checker = 'Tranche Scan Checker';
    public static final String Tranche_Credit_Approval = 'Tranche Credit Approval';
    public static final String Tranche_Disbursement_Maker = 'Tranche Disbursement Maker';
    public static final String Tranche_Disbursement_Checker = 'Tranche Disbursement Checker';
    public static final String Tranche_Disbursed = 'Tranche Disbursed';
    //Tranche Record Type
    public static final String Tranche_Credit_Rec_Type = 'Credit';
    public static final String Tranche_Disbursement_Rec_Type = 'Disbursement';
    public static final String Tranche_Disbursement_Check_Rec_Type = 'Disbursement Checker';
    public static final String Tranche_Read_Only_Rec_Type = 'Read Only';
    public static final String Tranche_Sales_Coordinator_Rec_Type = 'Sales Coordinator';
    public static final String Tranche_Disbursed_Rec_Type = 'Tranche Disbursed';

    // CIBIL constants
    public static final String HOMELOANTYPE = '2';
    public static final String DIRECT = 'DIRECT';
    public static final String BureauRegion = 'QA/UAT';
    
																	 
	
																																								   
																																																																	
																																			 
	
	//UCIC Constants
    public static final String UCIC = 'UCIC'; //Added by Abhilekh on 11th September 2020 for UCIC Phase II 
	
	/*Added by Ankit for Static Condition*/
    public static final string SPECIALSANCTCON = 'Insurance amount mentioned in the sanction letter is towards property insurance and life insurance';
    
 /** Added by Saumya for IMGC BRD **/ 
    //Loan Sanction Conditions
    public static final string IMGCSC1 = 'Undertaking to be taken from customer confirming that he/she is aware that there is a guarantee cover on the loan by IMGC';
    public static final string IMGCSC2 = 'Insurance of  the  property is required to safeguard  the interests of both,  Lender and the Borrower'; //Property Insurance Mandatory as previous value
    
    public static final string IMGC = 'IMGC'; //Added by Saumya on 26th August 2020 FOR IMGC BRD
	public static final string IMGCMortgageDeed = 'IMGC Mortgage Deed'; //Added by Saumya on 25th August 2020 FOR IMGC BRD 
    /** Added by Saumya for IMGC BRD **/ 
    //************************************CIBIL Constants Added by Vaishali for BRE
    public static final String StrategyTag = 'BRE 1.1';
    public static final String ClientName = 'HHFL';
    public static final String OriginationSourceId = 'SFDC';
    public static final String isBRERun = 'T';
    public static final String rejectionReasonBRE = 'BRE 1 Eligibility not met';
    //************************************End of the patch- added by Vaishali for BRE
    
    public static final String INFORMATION_ONLY = 'Information Only';
    
    public static final String subjectForOTCEmail = 'Post Sanction FCU Verifications have been completed | '; //Added by Abhilekh on 6th February 2020 for TIL-1941
    public static final String emailBodyForOTCEmail1 = 'Dear Member,<br/><br/>All the Post Sanction FCU Verifications have been completed for Loan Application : '; //Added by Abhilekh on 6th February 2020 for TIL-1941
    public static final String emailBodyForOTCEmail2 = '<br/><br/>Thank You<br/>Admin'; //Added by Abhilekh on 6th February 2020 for TIL-1941
	/*** Added by Saumya For IMGC BRD ***/
   public static final String IMGCCharge1 = 'PROCESSING FEES RECEIPT MGC - PL'; 
   public static final String IMGCCharge2 = 'PROCESSING FEES RECEIPT MGC - PL'; 
   public static final String IMGCCharge3 = 'PROCESSING FEES RECEIPT MGC - PL'; 
   public static final String IMGCCharge4 = 'PROCESSING FEES RECEIPT MGC - PL'; 
    /*** Added by Saumya For IMGC BRD ***/
}