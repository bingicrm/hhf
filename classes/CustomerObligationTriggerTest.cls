/*=====================================================================
* Deloitte India
* Name:CustomerObligationTriggerTest
* Description: This class is a test class and supports CustomerObligationTrigger and CustomerObligationTriggerHandler.
* Created Date: [03/04/2020]
* Created By:Saumya Bhasin(Deloitte India)
*
=====================================================================*/
@isTest
private class CustomerObligationTriggerTest {
    public static testMethod void methodTest()
    {
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
         insert u;                        
                           
                           
        User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId 
                       from user where ID=:UserInfo.getUserId()];
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=u.Id;
        loggedUser.ManagerID=u.Id;
        update loggedUser;
        
     
        system.runAs(loggedUser){   
            Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
             Account acc=new Account();
            acc.CountryCode__c='+91';
            acc.Email__c='puser001@amamama.com';
            acc.Gender__c='M';
            acc.Mobile__c='9800765432';
            acc.Phone__c='9800765434';
            acc.PersonMobilePhone = '9800765434';
            //acc.Rejection_Reason__c='Rejected';
            //acc.Name='Test Account';
            acc.FirstName = 'abc';
            acc.LastName='def';
            acc.Salutation='Mr';
            acc.RecordTypeId = RecordTypeIdCustomer;
            // acc.Type=;
            insert acc;
         
                                
             Scheme__c sch= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                         Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                         Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                         Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                         Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
            insert sch;    
            LMS_Date_Sync__c abc = new LMS_Date_Sync__c(EOD_BOD__c='N',Current_Date__c=Date.Today());
            insert abc;
            Id RecordTypeIdLoanApplication = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('COPS: Credit Operations Entry').getRecordTypeId();
        
            Loan_Application__c app = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=sch.Id,Loan_Number__c='124',
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',StageName__c='Customer Onboarding',
                                                                        RecordTypeId=RecordTypeIdLoanApplication,
                                                                        Business_Date_Created__c=System.today(),
                                                                        Approved_Loan_Amount__c=100000,
                                                                        Processing_Fee_Percentage__c=1,Mode_of_Repayment__c='NACH',
                                                                        Repayment_Start_Date__c=Date.Today().addDays(100),
                                                                        Sanctioned_Disbursement_Type__c='Multiple Tranche',Property_Identified__c = true);
            insert app;
            
            Loan_Contact__c lc =[SELECT Id,Borrower__c from Loan_Contact__c where Loan_Applications__c=:app.Id];
            lc.Borrower__c='1';
            //lc.Category__c='1';
            lc.Constitution__c = '20';
            lc.Customer_segment__c = '1';
            lc.BRE_Record__c = true;
            lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc.Marital_Status__c = 'M';
            lc.Gross_Salary__c = 1000;
            lc.Basic_Pay__c = 1000;
            lc.DA__c = 1000;
            //lc.Total_Fixed_Pay__c = 1000;
            //lc.Variable_Income_Monthly__c = 1000;
            lc.Net_Income__c = 1000;
            lc.PF__c = 1000;
            lc.TDS__c = 1000;
            lc.Current_Organization_Work_Ex_in_Months__c=12;
            lc.Total_Work_Experience__c = 12;
            lc.Customer_Details_Verified__c= true;
            lc.Voter_ID_Number__c = '123456789';
            lc.Passport_Number__c = '123456789';
            lc.PAN_Number__c = 'BAFPV6543A';
            lc.Aadhaar_Number__c = '898518762736';
            lc.Income_Considered__c = true;
            
            update lc;
            Pincode__c pin = new Pincode__c(Name='11044',ZIP_ID__c='45',City__c='Ludiana',State_Text__c='Punjab',Country_Text__c='India');
            insert pin;   
            system.debug('pin '+pin);
            Address__c add = new Address__c(Residence_Type__c= 'G',Type_of_address__c='OFFRES',Address_Line_1__c='11111111',Address_Line_2__c='127737',Pincode_LP__c=pin.Id,Loan_Contact__c=lc.Id);
            insert add;
            Customer_Integration__c cust = new Customer_Integration__c(CIBIL_Score__c='5',recordTypeId=Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId(), Loan_Application__c = app.Id, Loan_Contact__c=lc.Id,BRE_Recent__c=true);
            insert cust;
            Sourcing_Detail__c sc = new Sourcing_Detail__c(Loan_Application__c= app.Id, Source_Code__c= '19',Lead_Sourcing_Detail__c='HHFL Website',recordTypeId=Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('Digital Direct').getRecordTypeId());
            insert sc;
            Test.startTest();
            app.StageName__c='Operation Control';
            app.Sub_Stage__c  ='Scan: Data Maker';
            //app.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('File check Scanner').getRecordTypeId();
            update app;
            Property__c prop = new Property__c(First_Property_Owner__c=lc.Id, Loan_Application__c=app.Id,Address_Line_1__c='17827872',Area__c=12,Floor__c='1',Area_Unit__c='Sqft',Flat_No_House_No__c='1',Type_Property__c='Residential',Pincode_LP__c=pin.Id);
            insert prop;
            Sanction_Condition_Master__c objSCM = new Sanction_Condition_Master__c();
            objSCM.Name = 'Others';
            insert objSCM;
            LMS_Bus_Date__c objL = [Select Id, Current_Date__c from LMS_Bus_Date__c];
            objL.Current_Date__c = Date.valueOf(System.now());
            update objL;
            
            Customer_Obligations__c objCust = new Customer_Obligations__c();
            //objCust.Customer_Integration__c=cust.Id;
            //objCust.CD_Income_Considered__c = TRUE;
            objCust.Customer_Detail__c = lc.Id;
            objCust.Final_Obligations_Considered__c = FALSE; 
            objCust.Payment_History_End_Date__c = Date.valueOf(System.now().addMonths(13));
            objCust.POS__c = 120000 ;
            objCust.EMI_Amount__c = 10000 ;
            //objCust.Balance_Tenure_MOB__c = 12;
            insert objCust;
            
            
            
            Loan_Sanction_Condition__c objSancCond = new Loan_Sanction_Condition__c();
            objSancCond.Customer_Obligation__c = objCust.Id;
            objSancCond.Customer_Details__c = lc.Id;
            objSancCond.Loan_Application__c  = app.Id;
            objSancCond.Sanction_Condition_Master__c = objSCM.Id;
            insert objSancCond;
            
            objCust.Payment_History_End_Date__c = Date.valueOf(System.now().addMonths(14));
            objCust.Final_Obligations_Considered__c = True; 
            update objCust;
            
            Test.stopTest();
        }    
    }
    
    public static testMethod void methodTest1()
    {
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
         insert u;                        
                           
                           
        User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId 
                       from user where ID=:UserInfo.getUserId()];
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=u.Id;
        loggedUser.ManagerID=u.Id;
        update loggedUser;
        
     
        system.runAs(loggedUser){   
            Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
             Account acc=new Account();
            acc.CountryCode__c='+91';
            acc.Email__c='puser001@amamama.com';
            acc.Gender__c='M';
            acc.Mobile__c='9800765432';
            acc.Phone__c='9800765434';
            acc.PersonMobilePhone = '9800765434';
            //acc.Rejection_Reason__c='Rejected';
            //acc.Name='Test Account';
            acc.FirstName = 'abc';
            acc.LastName='def';
            acc.Salutation='Mr';
            acc.RecordTypeId = RecordTypeIdCustomer;
            // acc.Type=;
            insert acc;
         
                                
             Scheme__c sch= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                         Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                         Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                         Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                         Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
            insert sch;    
            LMS_Date_Sync__c abc = new LMS_Date_Sync__c(EOD_BOD__c='N',Current_Date__c=Date.Today());
            insert abc;
            Id RecordTypeIdLoanApplication = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('COPS: Credit Operations Entry').getRecordTypeId();
        
            Loan_Application__c app = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=sch.Id,Loan_Number__c='124',
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',StageName__c='Customer Onboarding',
                                                                        RecordTypeId=RecordTypeIdLoanApplication,
                                                                        Business_Date_Created__c=System.today(),
                                                                        Approved_Loan_Amount__c=100000,
                                                                        Processing_Fee_Percentage__c=1,Mode_of_Repayment__c='NACH',
                                                                        Repayment_Start_Date__c=Date.Today().addDays(100),
                                                                        Sanctioned_Disbursement_Type__c='Multiple Tranche',Property_Identified__c = true);
            insert app;
            
            Loan_Contact__c lc =[SELECT Id,Borrower__c from Loan_Contact__c where Loan_Applications__c=:app.Id];
            lc.Borrower__c='1';
            //lc.Category__c='1';
            lc.Constitution__c = '20';
            lc.Customer_segment__c = '1';
            lc.BRE_Record__c = true;
            lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc.Marital_Status__c = 'M';
            lc.Gross_Salary__c = 1000;
            lc.Basic_Pay__c = 1000;
            lc.DA__c = 1000;
            //lc.Total_Fixed_Pay__c = 1000;
            //lc.Variable_Income_Monthly__c = 1000;
            lc.Net_Income__c = 1000;
            lc.PF__c = 1000;
            lc.TDS__c = 1000;
            lc.Current_Organization_Work_Ex_in_Months__c=12;
            lc.Total_Work_Experience__c = 12;
            lc.Customer_Details_Verified__c= true;
            lc.Voter_ID_Number__c = '123456789';
            lc.Passport_Number__c = '123456789';
            lc.PAN_Number__c = 'BAFPV6543A';
            lc.Aadhaar_Number__c = '898518762736';
            lc.Income_Considered__c = true;
            
            update lc;
            Pincode__c pin = new Pincode__c(Name='11044',ZIP_ID__c='45',City__c='Ludiana',State_Text__c='Punjab',Country_Text__c='India');
            insert pin;   
            system.debug('pin '+pin);
            Address__c add = new Address__c(Residence_Type__c= 'G',Type_of_address__c='OFFRES',Address_Line_1__c='11111111',Address_Line_2__c='127737',Pincode_LP__c=pin.Id,Loan_Contact__c=lc.Id);
            insert add;
            Customer_Integration__c cust = new Customer_Integration__c(CIBIL_Score__c='5',recordTypeId=Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId(), Loan_Application__c = app.Id, Loan_Contact__c=lc.Id,BRE_Recent__c=true);
            insert cust;
            Sourcing_Detail__c sc = new Sourcing_Detail__c(Loan_Application__c= app.Id, Source_Code__c= '19',Lead_Sourcing_Detail__c='HHFL Website',recordTypeId=Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('Digital Direct').getRecordTypeId());
            insert sc;
            Test.startTest();
            app.StageName__c='Operation Control';
            app.Sub_Stage__c  ='Scan: Data Maker';
            //app.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('File check Scanner').getRecordTypeId();
            update app;
            Property__c prop = new Property__c(First_Property_Owner__c=lc.Id, Loan_Application__c=app.Id,Address_Line_1__c='17827872',Area__c=12,Floor__c='1',Area_Unit__c='Sqft',Flat_No_House_No__c='1',Type_Property__c='Residential',Pincode_LP__c=pin.Id);
            insert prop;
            Sanction_Condition_Master__c objSCM = new Sanction_Condition_Master__c();
            objSCM.Name = 'Others';
            insert objSCM;
            LMS_Bus_Date__c objL = [Select Id, Current_Date__c from LMS_Bus_Date__c];
            objL.Current_Date__c = Date.valueOf(System.now());
            update objL;
            
            Customer_Obligations__c objCust = new Customer_Obligations__c();
            //objCust.Customer_Integration__c=cust.Id;
            //objCust.CD_Income_Considered__c = TRUE;
            objCust.Customer_Detail__c = lc.Id;
            objCust.Final_Obligations_Considered__c = FALSE; 
            objCust.Payment_History_End_Date__c = Date.valueOf(System.now().addMonths(15));
            objCust.POS__c = 100000 ;
            objCust.EMI_Amount__c = 10000 ;
            //objCust.Balance_Tenure_MOB__c = 12;
            insert objCust;
            
            objCust.Payment_History_End_Date__c = Date.valueOf(System.now().addMonths(17));
            objCust.Final_Obligations_Considered__c = False; 
            objCust.POS__c = 120000 ;
            update objCust;
            
            
            
            Test.stopTest();
        }    
    }
    
     public static testMethod void methodTest2()
    {
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
         insert u;                        
                           
                           
        User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId 
                       from user where ID=:UserInfo.getUserId()];
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=u.Id;
        loggedUser.ManagerID=u.Id;
        update loggedUser;
        
     
        system.runAs(loggedUser){   
            Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
             Account acc=new Account();
            acc.CountryCode__c='+91';
            acc.Email__c='puser001@amamama.com';
            acc.Gender__c='M';
            acc.Mobile__c='9800765432';
            acc.Phone__c='9800765434';
            acc.PersonMobilePhone = '9800765434';
            //acc.Rejection_Reason__c='Rejected';
            //acc.Name='Test Account';
            acc.FirstName = 'abc';
            acc.LastName='def';
            acc.Salutation='Mr';
            acc.RecordTypeId = RecordTypeIdCustomer;
            // acc.Type=;
            insert acc;
         
                                
             Scheme__c sch= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                         Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                         Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                         Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                         Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
            insert sch;    
            LMS_Date_Sync__c abc = new LMS_Date_Sync__c(EOD_BOD__c='N',Current_Date__c=Date.Today());
            insert abc;
            Id RecordTypeIdLoanApplication = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('COPS: Credit Operations Entry').getRecordTypeId();
        
            Loan_Application__c app = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=sch.Id,Loan_Number__c='124',
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',StageName__c='Customer Onboarding',
                                                                        RecordTypeId=RecordTypeIdLoanApplication,
                                                                        Business_Date_Created__c=System.today(),
                                                                        Approved_Loan_Amount__c=100000,
                                                                        Processing_Fee_Percentage__c=1,Mode_of_Repayment__c='NACH',
                                                                        Repayment_Start_Date__c=Date.Today().addDays(100),
                                                                        Sanctioned_Disbursement_Type__c='Multiple Tranche',Property_Identified__c = true);
            insert app;
            
            Loan_Contact__c lc =[SELECT Id,Borrower__c from Loan_Contact__c where Loan_Applications__c=:app.Id];
            lc.Borrower__c='1';
            //lc.Category__c='1';
            lc.Constitution__c = '20';
            lc.Customer_segment__c = '1';
            lc.BRE_Record__c = true;
            lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc.Marital_Status__c = 'M';
            lc.Gross_Salary__c = 1000;
            lc.Basic_Pay__c = 1000;
            lc.DA__c = 1000;
            //lc.Total_Fixed_Pay__c = 1000;
            //lc.Variable_Income_Monthly__c = 1000;
            lc.Net_Income__c = 1000;
            lc.PF__c = 1000;
            lc.TDS__c = 1000;
            lc.Current_Organization_Work_Ex_in_Months__c=12;
            lc.Total_Work_Experience__c = 12;
            lc.Customer_Details_Verified__c= true;
            lc.Voter_ID_Number__c = '123456789';
            lc.Passport_Number__c = '123456789';
            lc.PAN_Number__c = 'BAFPV6543A';
            lc.Aadhaar_Number__c = '898518762736';
            lc.Income_Considered__c = true;
            
            update lc;
            Pincode__c pin = new Pincode__c(Name='11044',ZIP_ID__c='45',City__c='Ludiana',State_Text__c='Punjab',Country_Text__c='India');
            insert pin;   
            system.debug('pin '+pin);
            Address__c add = new Address__c(Residence_Type__c= 'G',Type_of_address__c='OFFRES',Address_Line_1__c='11111111',Address_Line_2__c='127737',Pincode_LP__c=pin.Id,Loan_Contact__c=lc.Id);
            insert add;
            Customer_Integration__c cust = new Customer_Integration__c(CIBIL_Score__c='5',recordTypeId=Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId(), Loan_Application__c = app.Id, Loan_Contact__c=lc.Id,BRE_Recent__c=true);
            insert cust;
            Sourcing_Detail__c sc = new Sourcing_Detail__c(Loan_Application__c= app.Id, Source_Code__c= '19',Lead_Sourcing_Detail__c='HHFL Website',recordTypeId=Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('Digital Direct').getRecordTypeId());
            insert sc;
            Test.startTest();
            app.StageName__c='Operation Control';
            app.Sub_Stage__c  ='Scan: Data Maker';
            //app.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('File check Scanner').getRecordTypeId();
            update app;
            Property__c prop = new Property__c(First_Property_Owner__c=lc.Id, Loan_Application__c=app.Id,Address_Line_1__c='17827872',Area__c=12,Floor__c='1',Area_Unit__c='Sqft',Flat_No_House_No__c='1',Type_Property__c='Residential',Pincode_LP__c=pin.Id);
            insert prop;
            Sanction_Condition_Master__c objSCM = new Sanction_Condition_Master__c();
            objSCM.Name = 'Others';
            insert objSCM;
            LMS_Bus_Date__c objL = [Select Id, Current_Date__c from LMS_Bus_Date__c];
            objL.Current_Date__c = Date.valueOf(System.now());
            update objL;
            
            Customer_Obligations__c objCust1 = new Customer_Obligations__c();
            //objCust.Customer_Integration__c=cust.Id;
            //objCust.CD_Income_Considered__c = TRUE;
            objCust1.Customer_Detail__c = lc.Id;
            objCust1.Final_Obligations_Considered__c = FALSE; 
            objCust1.Payment_History_End_Date__c = Date.valueOf(System.now().addMonths(15));
            objCust1.POS__c = 120000 ;
            objCust1.EMI_Amount__c = 10000 ;
            //objCust.Balance_Tenure_MOB__c = 12;
            insert objCust1;
            
            
            
            
            Test.stopTest();
        }    
    }

}