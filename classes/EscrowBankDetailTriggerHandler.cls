public class EscrowBankDetailTriggerHandler {
    public static void onBeforeInsert(List<Escrow_Bank_Details__c> lstTriggerNew) {
        if(!lstTriggerNew.isEmpty()) {
            Id profileId = UserInfo.getProfileId();
            String strProfileName;
            Map<Id, Boolean> mapProjIdtoSalesTeamFlag = new Map<Id, Boolean>();
            List<Profile> lstProfile = [Select Id, Name from Profile Where Id=: profileId LIMIT 1];
            strProfileName = !lstProfile.isEmpty() && String.isNotBlank(lstProfile[0].Name) ? lstProfile[0].Name : '';
            List<Escrow_Bank_Details__c> lstEBDExisting = [Select Id, Name, Project__c, CreatedById, CreatedBy.Profile.Name from Escrow_Bank_Details__c];
            if(!lstEBDExisting.isEmpty()) {
                for(Escrow_Bank_Details__c objEBD : lstEBDExisting) {
                    if(String.isNotBlank(objEBD.Project__c) && (objEBD.CreatedBy.Profile.Name == 'DSA' || objEBD.CreatedBy.Profile.Name == 'DST' || objEBD.CreatedBy.Profile.Name == 'Sales Team' || objEBD.CreatedBy.Profile.Name == 'Sales Coordinator')
                        && strProfileName != null && strProfileName != '' && (strProfileName == 'DSA' || strProfileName == 'DST' || strProfileName == 'Sales Team' || strProfileName == 'Sales Coordinator')
                    ) {
                        mapProjIdtoSalesTeamFlag.put(objEBD.Project__c,true);
                    }
                }
            }
            System.debug('Debug Log for mapProjIdtoSalesTeamFlag'+mapProjIdtoSalesTeamFlag.size());
            for(Escrow_Bank_Details__c objEBD : lstTriggerNew) {
                if(!mapProjIdtoSalesTeamFlag.isEmpty() && mapProjIdtoSalesTeamFlag.containsKey(objEBD.Project__c) && mapProjIdtoSalesTeamFlag.get(objEBD.Project__c)==true) {
                    objEBD.addError('You are not authorized to create this record. A record already exists.');
                }
            }
            
        }
    }

    public static void onAfterInsert(List<Escrow_Bank_Details__c> lstTriggerNew) {
        if(!lstTriggerNew.isEmpty()) {
            Set<Id> setProjectIds = new Set<Id>();
            for(Escrow_Bank_Details__c objEBD : lstTriggerNew) {
                if(String.isNotBlank(objEBD.Project__c)) {
                    setProjectIds.add(objEBD.Project__c);
                }
            }
            if(!setProjectIds.isEmpty()) {
                List<Escrow_Bank_Details__c> lstExistingRecords = [Select Id, Name, Project__c, Is_Active__c from Escrow_Bank_Details__c WHERE Id NOT IN: lstTriggerNew AND Project__c IN: setProjectIds AND Is_Active__c = True];
                if(!lstExistingRecords.isEmpty()) {
                    for(Escrow_Bank_Details__c objEBD : lstExistingRecords) {
                        objEBD.Is_Active__c = false;
                    }
                    update lstExistingRecords;
                }
            }
        }
    }
}