@isTest

public class TestDisbursementTriggerHandler {
    
    public static testMethod void method1(){
        
        Account acc=new Account();
        acc.name='Applicant123';
        acc.phone='9234509786';
        acc.PAN__c='ADGPW4078E';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        System.debug(acc.id);
        
        Scheme__c sc=new Scheme__c();
        sc.name='Construction Finance';
        sc.Scheme_Code__c='CF';
        sc.Scheme_Group_ID__c='6';
        sc.Product_Code__c='HL';
        insert sc;
        System.debug(sc.id);
        
        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Scheme__c=sc.id;
        lap.Transaction_type__c='PB';
        lap.Approved_Loan_Amount__c=5000000;
        insert lap;
        System.debug(lap.id); 
        
        pincode__c pc=new pincode__c();
        pc.name='Bengaluru';
        pc.City__c='Bengaluru';
        pc.ZIP_ID__c='560001';
        
        insert pc;
        Bank_Master__c bmc=new Bank_Master__c();
        bmc.A_c_Holder_Name__c='testtt';
        bmc.A_c_no__c=1231231231;
        bmc.Country__c='1';
        bmc.pincode__c=pc.id;
        
        insert bmc; 
        
        
        Disbursement__c d=new Disbursement__c();
        d.Loan_Application__c = lap.id;
        //d.Mode_of_Payment__c = 'Q';
       // d.Bank_Master__c = bmc.id;
        d.Cheque_Amount__c = 2000000;
        d.Cheque_Favouring__c = 'JGHR';
        d.Disbursal_Amount__c = 5000000;
        
        //insert d ;
        
        //Disbursement__c d2 = [SELECT Cheque_Amount__c FROM Disbursement__c WHERE Id=:d.Id];
        //system.AssertEquals(2000000,d2.Cheque_Amount__c);
        //d.Cheque_Amount__c = 2500000;
        //update d;
        
        
        
    }
    
}