public class CustomerBREDecisionTriggerHandler{
    public static void afterInsert(List<CIBIL_Decision_Detail__c> newList){
        List<CIBIL_Decision_Detail__c> decisionList = new List<CIBIL_Decision_Detail__c>();
        for(CIBIL_Decision_Detail__c decision: newList){
            if(decision.Product_Code__c != null && decision.Reason_Code__c != null && decision.BRE_II__c == TRUE){
                decisionList.add(decision);
            }
        }
        system.debug('newList '+newList.size());
        system.debug('In after insert of CustomerBREDecisionTriggerHandler');
        if(decisionList.size()>0){
            updateCustomerDeviations(decisionList);
        }
    }
    public static void beforeInsert(List<CIBIL_Decision_Detail__c> newList){
        List<CIBIL_Decision_Detail__c> reasonList = new List<CIBIL_Decision_Detail__c>();
        for(CIBIL_Decision_Detail__c decision: newList){
            system.debug('KK in for:'+decision);
            if(decision.Reason_Code__c != null){
                reasonList.add(decision);
            }
        }
        system.debug('KK List size:'+reasonList.size());
        if(reasonList.size()>0){
            updateDescription(reasonList);
        }
    }
    public static void updateCustomerDeviations(List<CIBIL_Decision_Detail__c> deviationList){
        List<Id> idList = new List<Id>();
        List<String> schemeList = new List<String>();
        List<Applicable_Deviation__c> toDelete = new List<Applicable_Deviation__c>();
        List<String> toInsertDevCodes = new List<String>();
        List<Applicable_Deviation__c> toInsert = new List<Applicable_Deviation__c>();
        for(CIBIL_Decision_Detail__c dec: deviationList){
            idList.add(dec.Loan_Application__c);
            schemeList.add(dec.Product_Code__c);
        }
        List<Applicable_Deviation__c> appDevList = [Select Id, Auto_Deviation__c,Loan_Application__r.Scheme__r.Product_Code__c  , Deviation_Code__c, Credit_Deviation_Master__c, Loan_Application__c, Customer_Integration__c, Customer_Details__c from Applicable_Deviation__c where Loan_Application__c IN: idList and  Loan_Application__r.Scheme__r.Product_Code__c   IN: schemeList and Auto_Deviation__c = true ];
        system.debug('appDevList '+appDevList);
        system.debug('deviationList '+deviationList);
        if(appDevList.size()>0){
            for(Applicable_Deviation__c appDev: appDevList){
                Integer i = 0;
        
                system.debug('Loan_Application__r.Scheme__c '+appDev.Loan_Application__r.Scheme__r.Product_Code__c  );
                for(CIBIL_Decision_Detail__c dec: deviationList){
                    system.debug('Product Code '+dec.Product_Code__c);
                     system.debug('appDev.Customer_Details__c:: '+appDev.Customer_Details__c + 'dec.Customer_Detail__c::' + dec.Customer_Detail__c);
                system.debug('appDev.Deviation_Code__c:: '+appDev.Deviation_Code__c + 'dec.Reason_Code__c::' + dec.Reason_Code__c);
                system.debug('appDev.Customer_Integration__c:: '+appDev.Customer_Integration__c + 'dec.Customer_Integration__c::' + dec.Customer_Integration__c);
                system.debug('appDev.Loan_Application__r.Scheme__r.Product_Code__c  :: '+appDev.Loan_Application__r.Scheme__r.Product_Code__c  + 'dec.Product_Code__c::' + dec.Product_Code__c);
                    if(appDev.Customer_Details__c != dec.Customer_Detail__c || (appDev.Customer_Details__c == dec.Customer_Detail__c && appDev.Deviation_Code__c == dec.Reason_Code__c && appDev.Loan_Application__r.Scheme__r.Product_Code__c  == dec.Product_Code__c)){
                        i=1;
                        system.debug('Match found for appDev ');
                        break;
                    }
               
                    else if(appDev.Customer_Details__c == dec.Customer_Detail__c && appDev.Deviation_Code__c != dec.Reason_Code__c && appDev.Customer_Integration__c != dec.Customer_Integration__c && appDev.Loan_Application__r.Scheme__r.Product_Code__c  != dec.Product_Code__c){
                        i=0;
                    }
                }
                if(i==0){
                    toDelete.add(appDev);
                }
            }
        }
        if(appDevList.size()>0){
            for(CIBIL_Decision_Detail__c dec: deviationList){
                Integer k = 0;
        
                for(Applicable_Deviation__c appDev: appDevList){
                    if(appDev.Customer_Details__c == dec.Customer_Detail__c && appDev.Deviation_Code__c == dec.Reason_Code__c && appDev.Loan_Application__r.Scheme__r.Product_Code__c  == dec.Product_Code__c){
                        k=1;
                        break;
                    }
                    else if((appDev.Customer_Details__c != dec.Customer_Detail__c) || (appDev.Customer_Details__c == dec.Customer_Detail__c && appDev.Deviation_Code__c != dec.Reason_Code__c && appDev.Customer_Integration__c != dec.Customer_Integration__c && appDev.Loan_Application__r.Scheme__r.Product_Code__c   != dec.Product_Code__c)){
                        k=0;
                    }
                }
                if(k==0){
                    toInsertDevCodes.add(dec.Reason_Code__c);
                }
            }
        } else {
            for(CIBIL_Decision_Detail__c dec: deviationList){
                toInsertDevCodes.add(dec.Reason_Code__c);    
            }    
        }
        system.debug('toDelete '+toDelete);
        system.debug('toInsertDevCodes' +toInsertDevCodes);
        String codes = '';
        if(!toInsertDevCodes.isEmpty()) {
            /*for(String str : toInsertDevCodes) {
                codes = '\''+ String.escapeSingleQuotes(str) + '\'' + ',';
            }
            codes = codes.removeEnd(',');
            String query = 'Select Id, Product__c,Deviation_Code__c from Credit_Deviation_Master__c';
            query += ' where Status__c = TRUE and Deviation_Code__c != null  AND Deviation_Code__c INCLUDES (' + codes+ ') LIMIT 50000';
             List<Credit_Deviation_Master__c> devMasterList = Database.Query(query);*/
        List<Credit_Deviation_Master__c> devMasterList = [Select Id, Product__c,Deviation_Code__c from Credit_Deviation_Master__c where Status__c = TRUE and Deviation_Code__c != null and  Product__c IN: schemeList];
        List<Mitigant__c> lstOfMitigants = [SELECT Id, Active__c from Mitigant__c where Name ='Others'];
        
        system.debug('devMasterList '+devMasterList); 
        for(CIBIL_Decision_Detail__c dec: deviationList){
            system.debug('decision details  '+dec);
            for(Credit_Deviation_Master__c devMaster: devMasterList){
                String devCode = devMaster.Deviation_Code__c;
                List<String> lstOfdevCodes = new List<String>();
                
                if(devCode.contains(';')) {
                    List<String> codeList = devCode.split(';');
                    lstOfdevCodes.addAll(codeList);
                } else {
                   lstOfdevCodes.add(devCode); 
                }
                
                system.debug('lstOfdevCode::s'+lstOfdevCodes);
                for(String code : lstOfdevCodes) {

                    if(toInsertDevCodes.contains(code ) && code == dec.Reason_Code__c && dec.Product_Code__c == devMaster.Product__c){
                        system.debug('devMaster '+devMaster);
                        system.debug('Match found for code '+dec.Reason_Code__c);
                        Applicable_Deviation__c app = new Applicable_Deviation__c();
                        app.Loan_Application__c = dec.Loan_Application__c;
                        app.Credit_Deviation_Master__c = devMaster.Id;
                        app.Customer_Details__c = dec.Customer_Detail__c;
                        app.Customer_Integration__c = dec.Customer_Integration__c;
                        app.Deviation_Code__c = dec.Reason_Code__c;
                        app.Auto_Deviation__c =true;
                        if(!lstOfMitigants.isEmpty()) {
                            app.Mitigant__c = lstOfMitigants[0].Id;
                            app.If_Others_please_specify__c = 'Created via BREII integration';
                        }
                        system.debug('Deviation created '+ app);
                        toInsert.add(app);
                    }
                }    
            }
        }
        }    
        if(toDelete.size()>0){
            system.debug('DELETE HERE');
            RunDeleteTriggerOnApplicableDeviation__c   runValidationCheck = RunDeleteTriggerOnApplicableDeviation__c.getInstance('RunDeleteTriggerOnApplicableDeviation');
            runValidationCheck.byPassTrigger__c = true;
            update runValidationCheck;
            try{
                database.delete(toDelete);
            }
            catch(Exception e){
                System.debug('Error encountered while deleting:'+e);
                runValidationCheck.byPassTrigger__c = false;
                update runValidationCheck;
            }
            runValidationCheck.byPassTrigger__c = false;
            update runValidationCheck;
        }
        system.debug('toInsert '+toInsert);
        if(toInsert.size()>0){
            system.debug('INSERT HERE');
            try{
                insert toInsert;
            }
            catch(Exception e){
                System.debug('Error encountered while insertion:'+e);
            }
            system.debug('INSERTED '+toInsert);
        }
    }
    public static void updateDescription(List<CIBIL_Decision_Detail__c> reasonList){
        List<BRE_Decision_Code__c> codeList = new List<BRE_Decision_Code__c>();
        codeList = [Select Id, Reason_Code__c, Reason_Description__c, Active__c from BRE_Decision_Code__c];
        if(codeList.size()>0){
            for(BRE_Decision_Code__c code: codeList){
                for(CIBIL_Decision_Detail__c dec: reasonList){
                    if(code.Reason_Code__c == dec.Reason_Code__c && code.Active__c == TRUE){
                        dec.Reason_Description__c = code.Reason_Description__c != null ? code.Reason_Description__c : null;
                    }
                }
            }
        }
    }
    // Added by KK for TIL-2351: Code Begins
    public static void deleteCustDeviations(Loan_Contact__c lc){
        List<Applicable_Deviation__c> lcDevList = New List<Applicable_Deviation__c>();
        List<Applicable_Deviation__c> devToDel = new List<Applicable_Deviation__c>();
        lcDevList = [Select Id, Deviation_Code__c, Customer_Details__c from Applicable_Deviation__c where Customer_Details__c =: lc.Id];
        for(Applicable_Deviation__c dev: lcDevList){
            if(dev.Deviation_Code__c != null){
                devToDel.add(dev);
            }
        }
        system.debug('Deviations to delete:'+devToDel);
        if(devToDel.size()>0){
            RunDeleteTriggerOnApplicableDeviation__c   runValidationCheck = RunDeleteTriggerOnApplicableDeviation__c.getInstance('RunDeleteTriggerOnApplicableDeviation');
            runValidationCheck.byPassTrigger__c = true;
            update runValidationCheck;
           
            try{
                database.delete(devToDel);
            }
            catch(Exception e){
                System.debug('Error encountered while deleting:'+e);
                runValidationCheck.byPassTrigger__c = false;
                update runValidationCheck;
            }
            runValidationCheck.byPassTrigger__c = false;
            update runValidationCheck;
        }
    }
    // Code Ends
}