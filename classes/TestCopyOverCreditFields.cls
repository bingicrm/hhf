@isTest
public class TestCopyOverCreditFields {
static testmethod void test1(){
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Name='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567890',RecordTypeId=RecordTypeIdAccount);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567891',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact cnt2 = new Contact (LastName = 'LNameLoanContact', AccountId = acc1.Id, MobilePhone = '9999667739');
        insert cnt2;
        
        Account acc2 = new Account(PAN__c='ABCDE1234H',Salutation='Mr',FirstName='Suresh',LastName='Nath',
                                   Date_of_Birth__c =Date.newInstance(1990,11,02),
                                   PersonMobilePhone='9234567892',PersonEmail='sn@gmail.com',RecordTypeId=RecordTypeIdAccount1);
        insert acc2;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert objLoanApplication;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Guarantor').getRecordTypeId();
        Class_Of_Activity__c objcls = new Class_Of_Activity__c();
    	objcls.Name = 'abc';
    	objcls.Full_Name__c = 'abc';
    	objcls.Active__c = true;
    	insert objcls;
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc1.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='2',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='18',
                                                             Customer_segment__c='9',Applicant_Status__c='1',
                                                             Class_Of_Activity__c=objcls.Id,
                                                             GSTIN_Available__c=true, Contact__c = cnt2.Id,
                                                             GSTIN_Number__c = '27AAHFG0551H1ZL',
                                                             income_considered__c = false);
        insert objLoanContact;
        
    	objLoanContact.Customer_Verified__c =true;
    	update objLoanContact;
            Test.startTest();

       	Loan_Contact__c objLoanContact2 = [Select Id, Customer_Verified__c from Loan_Contact__c where Loan_Applications__c =: objLoanApplication.Id and Customer__c =: acc.Id ];
		objLoanContact2.Customer_Verified__c =true;
    	update objLoanContact2;      
        CopyOverCreditFields.copyApplicantsValues(objLoanContact.Id);
    	objLoanApplication.StageName__c ='Credit Decisioning';
    	objLoanApplication.Sub_Stage__c ='Credit Review';
    	update objLoanApplication;
        CopyOverCreditFields.copyApplicantsValues(objLoanContact.Id);
        Test.stopTest();
    }   

static testmethod void test2(){
	
	 Profile p1 = [SELECT id, name from profile where name =: 'Credit Team'];
			Hierarchy__c objHierarchy = new Hierarchy__c();
			/*objHierarchy.BCM__c = u2.Id;
			objHierarchy.ZCM__c = u4.Id;
			objHierarchy.CCM__c = u2.Id;
			objHierarchy.CRO__c = u2.Id;*/
			insert objHierarchy;
			List<Hierarchy__c> lstHierarchy = [Select Id, Name, Active__c, BCM__c, BH__c, CCM__c, CRO__c, NCM__c, ZCM__c, ZCM_Salaried__c from Hierarchy__c where Active__c = true];
			String HierarchyName = lstHierarchy[0].Name;
        User u1 = new User(
            ProfileId = p1.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',Hierarchy__c = HierarchyName,
            	     Non_Salaried_Authority__c = 'A2',
            		 Salaried_Authority__c = 'A2',
            		 Role_in_Credit_Hierarchy__c = 'BCM');

        insert u1;
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
		
        Account acc = new Account(PAN__c='ABCDE1234F',Name='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567890',RecordTypeId=RecordTypeIdAccount);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567891',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact cnt2 = new Contact (LastName = 'LNameLoanContact', AccountId = acc1.Id, MobilePhone = '9999667739');
        insert cnt2;

		System.runAs(u1) {
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',Credit_Manager_User__c= u1.Id);
        insert objLoanApplication;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Guarantor').getRecordTypeId();
        
		Class_Of_Activity__c objcls = new Class_Of_Activity__c();
    	objcls.Name = 'abc';
    	objcls.Full_Name__c = 'abc';
    	objcls.Active__c = true;
    	insert objcls;
		
		Test.startTest();

        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc1.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='2',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='18',
                                                             Customer_segment__c='9',Applicant_Status__c='1',
                                                             GSTIN_Available__c=true, Contact__c = cnt2.Id,
															 Class_Of_Activity__c=objcls.Id,
                                                             GSTIN_Number__c = '27AAHFG0551H1ZL',
                                                             income_considered__c = false);
        insert objLoanContact;
        
    	objLoanContact.Customer_Verified__c =true;
    	update objLoanContact;

       	Loan_Contact__c objLoanContact2 = [Select Id, Customer_Verified__c from Loan_Contact__c where Loan_Applications__c =: objLoanApplication.Id and Customer__c =: acc.Id ];
		objLoanContact2.Customer_Verified__c =true;
    	update objLoanContact2;      
        CopyOverCreditFields.copyApplicantsValues(objLoanContact.Id);
    	objLoanApplication.StageName__c ='Credit Decisioning';
    	objLoanApplication.Sub_Stage__c ='Credit Review';
    	update objLoanApplication;
        CopyOverCreditFields.copyApplicantsValues(objLoanContact.Id);
        CopyOverCreditFields.copyApplicantsValues(objLoanContact2.Id);
        objLoanApplication.Applicant_Customer_segment__c ='9';
        update objLoanApplication;
        CopyOverCreditFields.copyApplicantsValues(objLoanContact.Id);    
        Test.stopTest();
		}
    } 	
}