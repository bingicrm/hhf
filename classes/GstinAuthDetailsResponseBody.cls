public class GstinAuthDetailsResponseBody {
/*****************************************************
Wrapper Class : CAS_SME_GSTAuthResponse 
Author        : Chitransh Porwal
Description   : This class is used to Deserialize GST Auth. Web service response.
Created Date  : 26 July 2019
*********************************************************/
   public cls_result result;
	public String requestId;	//4b260a59-b81c-11e9-b154-c7e325b79721
	public Integer statusCode;	//101
	public class cls_result {
		public cls_mbr[] mbr;
		public String canFlag;	//NA
		public cls_pradr pradr;
		public String tradeNam;	//SOFTAGE INFORMATION TECHNOLOGY LIMITED
		public String lstupdt;	//01/11/2018
		public cls_contacted contacted;
		public String rgdt;	//01/07/2017
		public String stjCd;	//WB093
		public String stj;	//TALTALA
		public String ctjCd;	//WA0606
		public String ppr;	//NA
		public String dty;	//Regular
		public String cmpRt;	//NA
		public String cxdt;	//
		public String ctb;	//Public Sector Undertaking
		public String sts;	//Active
		public String gstin;	//19AAFCS9264A1Z8
		public String lgnm;	//SOFTAGE INFORMATION TECHNOLOGY LIMITED
		public String[] nba;
		public String ctj;	//RANGE-VI
		public cls_adadr[] adadr;
	}
	public class cls_mbr {
	}
	public class cls_pradr {
		public String em;	//
		public String adr;	//ELLIOT ROAD, 77, P. S. STREET PARK, kolkata, West Bengal, pin: 700016
		public String addr;	//NA
		public String mb;	//
		public String ntr;	//Service Provision
		public String lastUpdatedDate;	//NA
	}
	public class cls_contacted {
	}
	public class cls_nba {
	}
	public class cls_adadr {
	}
    
    public String status;
    public cls_error error;
    
    public class cls_error {
        public String errorCode;
        public String errorType;
        public String errorMessage;
    }
    
	//public static fromJSON parse(String json){
	//	return (fromJSON) System.JSON.deserialize(json, fromJSON.class);
	//}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /* public cls_result result;
	public String status_code;	//101
	public String request_id;	//4cd8638c-246f-11e9-b94b-bdbe110cae61
	public class cls_result {
		public String stjCd;	//
		public String dty;	//Regular
		public String rgdt;	//01/07/2017
		public String tradeNam;	//RELIANCE INDUSTRIES LIMITED
		public String sts;	//Provisional
		public String gstin;	//02AAACR5055K1ZJ
		public String lgnm;	//RELIANCE INDUSTRIES LIMITED
		public String ctjCd;	//
		public String cxdt;	//
		public String lstupdt;	//06/01/2017
	}
	
    
    
       /* public Result result;
        public String requestId;
        public String statusCode;
        
        public class Contacted {
        }
        
        public class Result {
            public String canFlag;
            public Contacted contacted;
            public String lstupdt;
            public String stjCd;
            public String stj;
            public String ppr;
            public String dty;
            public String cmpRt;
            public String rgdt;
            public String tradeNam;
            public String sts;
            public String gstin;
            public String lgnm;
            public String ctjCd;
            public List<Contacted> mbr;
            public String cxdt;
        }
   
    public String status;
    public Error error;
    
    public class Error {
        public String errorCode;
        public String errorType;
        public String errorMessage;
    }*/
   
    
}