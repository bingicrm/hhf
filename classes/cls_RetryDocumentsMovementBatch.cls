global class cls_RetryDocumentsMovementBatch implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {  
          return Database.getQueryLocator('Select Id, Description__c, Upload_Status__c, Name, Document_Checklist__c, Document_Checklist__r.Loan_Contact__c, Document_Id__c, Size_of_the_Document__c, Document_Checklist__r.Loan_Contact__r.Applicant_Type__c, File_Name_If_Attachment__c, Loan_Application__c, Successfully_Migrated__c, Old_Release_Data__c, Loan_Application__r.Folders_Created_in_S3__c, Loan_Application__r.Name From Document_Migration_Log__c Where Successfully_Migrated__c = false AND Loan_Application__r.Folders_Created_in_S3__c = true AND Old_Release_Data__c = false');
    
    }
    global void execute(Database.BatchableContext BC, List<Document_Migration_Log__c> lstFailedDocMigrationLog)
    {   
        if(!lstFailedDocMigrationLog.isEmpty()) {
            for(Document_Migration_Log__c objDML : lstFailedDocMigrationLog) {
                if(String.isNotBlank(objDML.Loan_Application__c) && String.isNotBlank(objDML.Document_Checklist__c) && String.isNotBlank(objDML.Document_Checklist__r.Loan_Contact__c)) {
                    System.debug('Debug Log for applicant Type'+objDML.Document_Checklist__r.Loan_Contact__r.Applicant_Type__c);
                    if(objDML.Document_Checklist__r.Loan_Contact__r.Applicant_Type__c == 'Applicant') {
                        cls_RetryMoveFiles.createS3FilesApplicant(objDML);
                    }
                    else if(objDML.Document_Checklist__r.Loan_Contact__r.Applicant_Type__c == 'Co- Applicant' || objDML.Document_Checklist__r.Loan_Contact__r.Applicant_Type__c == 'Guarantor') {
                        cls_RetryMoveFiles.createS3FilesCoApplicantGuarantor(objDML);
                    }
                }
                else if(String.isNotBlank(objDML.Loan_Application__c) && String.isNotBlank(objDML.Document_Checklist__c) && String.isBlank(objDML.Document_Checklist__r.Loan_Contact__c)) {
                    cls_RetryMoveFiles.createS3FilesApplication(objDML);
                }
                else if(String.isNotBlank(objDML.Loan_Application__c) && String.isBlank(objDML.Document_Checklist__c)) {
                    List<Attachment> lstAttachment = [Select Id, Name, ParentId from Attachment Where ParentId =: objDML.Loan_Application__c  AND Id=: objDML.Document_Id__c];
                    System.debug('Debug Log for lstAttachment'+lstAttachment.size());
                    if(!lstAttachment.isEmpty()) {
                        cls_RetryMoveFiles.createS3FilesLoanKit2(objDML);
                    }
                    else {
                        cls_RetryMoveFiles.createS3FilesLoanKit(objDML);
                    }
                }
            }
        }
    }
    global void finish(Database.BatchableContext BC)
    {
		 AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors,JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
          
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Retry Document Migration Batch' + a.Status);
            mail.setPlainTextBody('During Retry Mechanism, records processed ' + a.TotalJobItems +
           'with '+ a.NumberOfErrors + ' failures.');
        if(!test.isrunningTest()){
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        Database.executeBatch(new cls_DeleteDocumentsBatch(),1);
    }
}