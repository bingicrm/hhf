public class TrancheDocumentListController {
    // method for fetching the associated Loan Id with the record.
    @AuraEnabled
    public static String getLoanId(Id recordId){
        Id loanId;
        if(Schema.Tranche__c.SObjectType == recordId.getSobjectType()){
            Tranche__c t = [SELECT Id, Loan_Application__c FROM Tranche__c WHERE Id =: recordId];
            loanId = t.Loan_Application__c;
            system.debug('Loan Id returned : ' + loanId);
        }
        return loanId;
    }
    // method for fetching the Sub Stage of the Loan Application record.
    @AuraEnabled
    public static String fetchSubStage(Id recordId) {
        String subStage;
        Id loanId;
        List<Loan_Application__c> lstLoanApplication;
        if(String.isNotBlank(recordId)) {
            if(Schema.Tranche__c.SObjectType == recordId.getSobjectType()){
                Tranche__c t = [SELECT Id, Loan_Application__c FROM Tranche__c WHERE ID =: recordId LIMIT 1];
                loanId = t.Loan_Application__c;
            }
            lstLoanApplication = [SELECT  ID, Sub_Stage__c FROM Loan_Application__c WHERE ID = :loanId LIMIT 1];
            if(!lstLoanApplication.isEmpty()) {
                subStage = lstLoanApplication[0].Sub_Stage__c;
            }
        }
        System.debug('Debug Log for subStage'+subStage);
        return subStage;
    }
    
    // method for fetch account records list  
    @AuraEnabled
    public static List <DocumentListWrapper> fetchDocuments(Id tranId) {
        Id loanId;
        If(tranId !=  null){
            Tranche__c t = [SELECT Id, Loan_Application__c FROM Tranche__c WHERE Id =: tranId];
            loanId = t.Loan_Application__c;
        }
        system.debug('loanId : ' + loanId + ' tranId : ' + tranId);
       //Map of Contact ID to Contact Name
       Map<String, List<Document_Checklist__c>> mapContactNametoDocChecklist = new Map<String, List<Document_Checklist__c>>();
       List < Document_Checklist__c > returnList = new List < Document_Checklist__c > ();
       List < Document_Checklist__c > lstOfDoc;
       List<DocumentListWrapper> lstDocumentWrapper = new List<DocumentListWrapper>();
       List<Loan_Application__c> lstLoanApplication = [SELECT  ID, Sub_Stage__c FROM Loan_Application__c WHERE ID = :loanId LIMIT 1];
       String subStage;
       String fieldsToQuery;
       
       if(!lstLoanApplication.isEmpty()) {
            subStage = lstLoanApplication[0].Sub_Stage__c;
       }
       System.debug('Debug Log for subStage'+subStage);
       
        if(subStage == 'Tranche File Check'){
            fieldsToQuery = 'Id, Contact_Name__c, Document_Type__r.name, Document_Master__c, Document_Master__r.Name, Status__c, File_Check_Completed__c';            
        }
        else if(subStage == 'Tranche Scan Checker'){
            fieldsToQuery = 'Id, Contact_Name__c, Document_Type__r.name, Document_Master__c, Document_Master__r.Name, Status__c, Scan_Check_Completed__c, Document_Collection_Mode__c, Screened__c, Sampled__c';
        }        
       
        System.debug('Debug Log for fieldsToQuery'+fieldsToQuery);
        
        if(String.isNotBlank(fieldsToQuery)) {
            String query = 'SELECT '+fieldsToQuery+' FROM Document_Checklist__c where Loan_Applications__c =\''+loanId+'\' AND Tranche__c =\'' + tranId +'\' ORDER BY Contact_Name__c NULLS FIRST';
            
            System.debug('Debug Log for query'+query);
            lstOfDoc = Database.query(query);
        }
        else {
            lstOfDoc = [select id,Contact_Name__c,Loan_Contact__r.name, REquest_Date_for_OTC__c, Document_Collection_Mode__c, Document_Type__r.name,Document_Master__c,Document_Master__r.name,Loan_Engine_Mandatory__c,Express_Queue_Mandatory__c,Status__c,Original_Seen_and_Verified__c,File_Check_Completed__c,Scan_Check_Completed__c from Document_Checklist__c where Loan_Applications__c =:loanId AND Tranche__c =: tranId];
            
        }
        system.debug('---->>>'+lstOfDoc);
        if(!lstOfDoc.isEmpty()) {
            for (Document_Checklist__c doc: lstOfDoc) {
                returnList.add(doc);
                System.debug('Debug Log for doc.Document_Type__r.name'+doc.Document_Type__r.name);
                System.debug('Debug Log for Document_Master__c'+doc.Document_Master__r.Name);
                System.debug('Debug Log for Document_Master__c'+doc.Contact_Name__c);
                
                if(String.isNotBlank(doc.Contact_Name__c)) {
                    if(!mapContactNametoDocChecklist.containsKey(doc.Contact_Name__c)) {
                        mapContactNametoDocChecklist.put(doc.Contact_Name__c,new List<Document_Checklist__c>());
                    }
                    mapContactNametoDocChecklist.get(doc.Contact_Name__c).add(doc);
                }
                else {
                    if(!mapContactNametoDocChecklist.containsKey('Application Related Documents')) {
                        mapContactNametoDocChecklist.put('Application Related Documents',new List<Document_Checklist__c>());
                    }
                    mapContactNametoDocChecklist.get('Application Related Documents').add(doc);
                }
            }
            for(String strContactName : mapContactNametoDocChecklist.keySet()) {
                lstDocumentWrapper.add(new DocumentListWrapper(strContactName, mapContactNametoDocChecklist.get(strContactName)));
            }
        } 
        System.debug('Debug Log for lstDocumentWrapper'+lstDocumentWrapper);
        return lstDocumentWrapper;
    }
    
    // method for fetch picklist values dynamic  
    @AuraEnabled
    public static List < String > getselectOptions(sObject objObject, string fld, string subStage) {
        system.debug('objObject --->' + objObject);
        system.debug('fld --->' + fld);
        system.debug('subStage --->' + subStage);
        
        List < String > allOpts = new list < String > ();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objObject.getSObjectType();
        
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        
        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values =
            fieldMap.get(fld).getDescribe().getPickListValues();
        
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        if(fld == 'Status__c' && subStage.startsWith('Tranche')) {
            allOpts.clear();
        	allOpts.add('Pending');
        	allOpts.add('Uploaded');
        	allOpts.add('Received');        	        	
        }
        
        system.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        return allOpts;
    }
    
    @AuraEnabled
    public static Id saveChunk(Id parentId, String fileName, String base64Data, String contentType, String fileId) {
            if (fileId == '') {
                fileId = saveTheFile(parentId, fileName, base64Data, contentType);
            } else {
                appendToFile(fileId, base64Data);
            }    
            system.debug('**'+fileId);
            return Id.valueOf(fileId);
        }
    
    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) {
            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
            Boolean isSuccess = false;
            Id returnId;
            /*
            Attachment oAttachment = new Attachment();
            oAttachment.parentId = parentId;
     
            oAttachment.Body = EncodingUtil.base64Decode(base64Data);
            oAttachment.Name = fileName;
            oAttachment.ContentType = contentType;
            system.debug('**insert'+oAttachment);
            insert oAttachment;
            */
            //New Logic to store the document in files. Added on 10 September, 2018
            ContentVersion conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = fileName; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = fileName; // Display name of the files
            conVer.VersionData = (Blob)EncodingUtil.base64Decode(base64Data); // converting your binary string to Blog
            try {
                insert conVer;
                System.debug('Debug Log for inserted record ContentVersion'+conVer.Id);
            }
            catch(Exception exc) {
                System.debug('Debug Log for exception from ContentVersion'+exc);
            }
            if(String.isNotBlank(String.valueOf(conVer.Id))) {
                Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
                //Create ContentDocumentLink
                ContentDocumentLink cDe = new ContentDocumentLink();
                cDe.ContentDocumentId = conDoc;
                cDe.LinkedEntityId = parentId; // you can use objectId,GroupId etc
                cDe.ShareType = 'I'; // Inferred permission, checkout description of ContentDocumentLink object for more details
                cDe.Visibility = 'AllUsers';
                try {
                    insert cDe;
                    System.debug('Record Id:'+cDe.Id);
                }
                catch(Exception ex1) {
                    System.debug('Debug Log for exception from ContentDocumentLink'+ex1);
                }
                if(String.isNotBlank(String.valueOf(cDE.Id))) {
                    isSuccess = true;
                    returnId = cDE.Id;
                }
                
            }
            if(isSuccess) {
                System.debug('Updating document Checklist');
                Document_Checklist__c doc =[select Id,Status__c from Document_Checklist__c where Id=:parentId];
                doc.Status__c='Uploaded';
                //update doc;
                Database.SaveResult updateResult = Database.update(doc, false);
                System.debug('Debug Log for updateResult'+updateResult);
                if(!updateResult.isSuccess()) {
                    for(Database.Error err : updateResult.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Fields that affected this error: ' + err.getFields());
                    }
                }
            }
            return returnId;
        }
     
    private static void appendToFile(Id fileId, String base64Data) {
            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
     
            Attachment a = [
                SELECT Id, Body
                FROM Attachment
                WHERE Id =: fileId
            ];
     
            String existingBody = EncodingUtil.base64Encode(a.Body);
     
            a.Body = EncodingUtil.base64Decode(existingBody + base64Data);
     
            update a;
        }
    
    @AuraEnabled
    public static List <DocumentListWrapper> saveLoan(String paramJSONList) {
        System.debug('Debug Log for param list'+paramJSONList);
        List<Document_Checklist__c> lstDocchkLst = new List<Document_Checklist__c>();
        List<DocumentListWrapper> lstSerializedWrapper = (List<DocumentListWrapper>)JSON.deserialize(paramJSONList,List<DocumentListWrapper>.class);
        System.debug('Debug Log for deserialized List'+lstSerializedWrapper.size());
        System.debug('Debug Log for deserialized List'+lstSerializedWrapper);
        if(!lstSerializedWrapper.isEmpty()) {
            for(DocumentListWrapper objDocumentListWrapper : lstSerializedWrapper) {
                lstDocchkLst.addAll(objDocumentListWrapper.lstDocumentCheckList);
                system.debug(objDocumentListWrapper.lstDocumentCheckList);
            }
        }
        System.debug('Debug Log for lstDocchkLst'+lstDocchkLst);
        if(!lstDocchkLst.isEmpty()) {
            Database.Saveresult[] updateList = Database.update(lstDocchkLst,false);
            for (Database.SaveResult sr : updateList) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully updated Document Checklist. Record ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
        return lstSerializedWrapper;
        //return null;
    }
    
    @AuraEnabled
    public static String validateChkList(String paramJSONList) {
        System.debug('Debug Log for param list'+paramJSONList);
        String statusMsg;
        Set<Id> setDocumentCheckList = new Set<Id>();
        Map<Id,List<ContentDocumentLink>> mapDocChkLsttoDocAttached = new Map<Id,List<ContentDocumentLink>>();
        List<Document_Checklist__c> lstDocchkLst = new List<Document_Checklist__c>();
        List<DocumentListWrapper> lstSerializedWrapper = (List<DocumentListWrapper>)JSON.deserialize(paramJSONList,List<DocumentListWrapper>.class);
        System.debug('Debug Log for deserialized List'+lstSerializedWrapper.size());
        System.debug('Debug Log for deserialized List'+lstSerializedWrapper);
        if(!lstSerializedWrapper.isEmpty()) {
            for(DocumentListWrapper objDocumentListWrapper : lstSerializedWrapper) {
                lstDocchkLst.addAll(objDocumentListWrapper.lstDocumentCheckList);
                system.debug(objDocumentListWrapper.lstDocumentCheckList);
            }
        }
        System.debug('Debug Log for lstDocchkLst'+lstDocchkLst);
        if(!lstDocchkLst.isEmpty()) {
        	for(Document_Checklist__c objDC : lstDocchkLst) {
        		setDocumentCheckList.add(objDC.Id);
        	}
        	System.debug('Debug Log for setDocumentCheckList'+setDocumentCheckList);
        	List<ContentDocumentLink> links;
			links = [SELECT id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN: setDocumentCheckList];
			System.debug('Debug Log for links'+links.size());
			System.debug('Debug Log for links'+links);
			if(!links.isEmpty()) {
				for(ContentDocumentLink cdl : links) {
					if(!mapDocChkLsttoDocAttached.containsKey(cdl.LinkedEntityId)) {
						mapDocChkLsttoDocAttached.put(cdl.LinkedEntityId,new List<ContentDocumentLink>());
					}
					mapDocChkLsttoDocAttached.get(cdl.LinkedEntityId).add(cdl);
				}
			}
			else {
				for(Document_Checklist__c objDC : lstDocchkLst) {
					mapDocChkLsttoDocAttached.put(objDC.Id,new List<ContentDocumentLink>());
				}
			}
			System.debug('Debug Log for mapDocChkLsttoDocAttached'+mapDocChkLsttoDocAttached.size());
			System.debug('Debug Log for mapDocChkLsttoDocAttached'+mapDocChkLsttoDocAttached);
			if(!mapDocChkLsttoDocAttached.isEmpty()) {
				for(Document_Checklist__c objDocument_Checklist : lstDocchkLst) {
					System.debug('Inside for loop');
					if(!mapDocChkLsttoDocAttached.containsKey(objDocument_Checklist.Id) && objDocument_Checklist.Status__c == 'Uploaded') {
						System.debug('Error!!');
						statusMsg = 'Error!!';
						break;
					}
					else if(mapDocChkLsttoDocAttached.containsKey(objDocument_Checklist.Id) && objDocument_Checklist.Status__c == 'Uploaded' && mapDocChkLsttoDocAttached.get(objDocument_Checklist.Id).isEmpty()) {
						statusMsg = 'Error!!';
						System.debug('Error!!');
						break;
					}
				}
			}
        }
        System.debug('Debug Log for statusMsg'+statusMsg);
        if(String.isBlank(statusMsg)) {
        	statusMsg = 'Successful';
        }
        System.debug('Debug Log for statusMsg1'+statusMsg);
        return statusMsg;
    }
    
    @AuraEnabled
    public static List<ID> queryAttachments (Id propertyId) {        
        List<ContentDocumentLink> links;
        List<ContentVersion> lstContentVersion;
        Set<Id> setContentVersionId = new Set<Id>();
        String objectAPIName = '';
        String keyPrefix = '';
        System.debug('Debug Log for propertyId'+propertyId);
        //keyPrefix = String.valueOf(propertyId).substring(0,3);
        objectAPIName = propertyId.getSObjectType().getDescribe().getName();
        System.debug('Debug Log for objectAPIName:'+objectAPIName);
        if(String.isNotBlank(objectAPIName)) {
            if(objectAPIName == 'Document_Checklist__c') {
                links = [select id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where LinkedEntityId=:propertyId];
                System.debug('Debug Log for links'+links.size());
                System.debug('Debug Log for links'+links);
                
                if (links.isEmpty()) {
                    return null;
                }
                
                Set<Id> contentIds = new Set<Id>();
                
                for (ContentDocumentLink link :links) {
                    contentIds.add(link.ContentDocumentId);
                }
                System.debug('Debug Log for contentIds'+contentIds);
                /*
                lstContentVersion = [SELECT Id, Title FROM ContentVersion WHERE ContentDocumentId IN :contentIds AND IsLatest=true];
                System.debug('Debug Log for lstContentVersion'+lstContentVersion);
                
                if(!lstContentVersion.isEmpty()) {
                    for(ContentVersion objContentVersion : lstContentVersion) {
                        setContentVersionId.add(objContentVersion.Id);
                    }
                }
                System.debug('Debug Log for setContentVersionId'+setContentVersionId);
                */
                List<Id> lstsettoList = new List<Id>(contentIds);            
                return lstsettoList;
            }
            
            else if(objectAPIName == 'Loan_Application__c') {
                List<Document_Checklist__c> lstDocumentList = [SELECT Id FROM Document_Checklist__c WHERE Loan_Applications__c =: propertyId];
                Set<Id> setDocumentListIds = new Set<Id>();
                System.debug('Debug Log for lstDocumentList'+lstDocumentList.size());
                if(!lstDocumentList.isEmpty()) {
                    for(Document_Checklist__c objDC : lstDocumentList) {
                        setDocumentListIds.add(objDC.Id);
                    }
                }
                System.debug('Debug Log for setDocumentListIds'+setDocumentListIds.size());
                if(!setDocumentListIds.isEmpty()) {
                    links = [select id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where LinkedEntityId IN:setDocumentListIds];
                    
                    if (links.isEmpty()) {
                        return null;
                    }
                    
                    Set<Id> contentIds = new Set<Id>();
                    
                    for (ContentDocumentLink link :links) {
                        contentIds.add(link.ContentDocumentId);
                    }
                    System.debug('Debug Log for contentIds'+contentIds);
                    List<Id> lstsettoList1 = new List<Id>(contentIds);            
                    return lstsettoList1;
                }
            }
        }
        return new List<Id>();
    }
    
    public class DocumentListWrapper {
        @AuraEnabled public String strContactName;
        @AuraEnabled public List<Document_Checklist__c> lstDocumentCheckList;
        
        public DocumentListWrapper(String paramContactName, List<Document_Checklist__c> paramListDocChkLst) {
            this.strContactName = paramContactName;
            this.lstDocumentCheckList = paramListDocChkLst;
        }
    }
}