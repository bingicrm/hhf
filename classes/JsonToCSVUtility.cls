/**
 * JsonToCSVUtility Class
 * This class has helper functiones to convert json string to csv
 * @author - Mehul Jain
 * */
public class JsonToCSVUtility {
    
    public static Integer MAX_ROW = 0;
    public static String strJSON = '{"responseReport":{"enquiryInformationRec":{"crn":"test data","addressCount":" ","cin":"test data","tin":"test data","borrowerName":".","addressVec":{"address":[{"addressLine":".","pinCode":".","state":".","city":"."}]},"pan":" ","dateOfRegistration":"test data"},"reportHeaderRec":{"memberReferenceNumber":" ","applicationReferenceNumber":"test data","memberDetails":"test data","reportOrderedBy":"test data","inquiryPurpose":"test data","daysPasswordToExpire":"test data","reportOrderNumber":"test data","reportOrderDate":"test data"},"reportIssuesVec":[{"message":" Unable to Process Request ","description":"test data","code":"0000"}],"productSec":{"oustandingBalanceByCFAndAssetClasificationSec":{"message":"test data","yourInstitution":{"total":{"STDVec":{"DPD61To90":{"count":"test data","value":"test data"},"DPD0":{"count":"test data","value":"test data"},"DPD1To30":{"count":"test data","value":"test data"},"DPD31To60":{"count":"test data","value":"test data"}},"NONSTDVec":{"sub":{"count":"test data","value":"test data"},"dbt":{"count":"test data","value":"test data"},"loss":{"count":"test data","value":"test data"},"DPD91To180":{"count":"test data","value":"test data"},"greaterThan180DPD":{"count":"test data","value":"test data"}}},"forex":{"total":{"count":"test data","value":"test data"},"STDVec":{"DPD61To90":{"count":"test data","value":"test data"},"DPD0":{"count":"test data","value":"test data"},"DPD1To30":{"count":"test data","value":"test data"},"DPD31To60":{"count":"test data","value":"test data"}},"NONSTDVec":{"sub":{"count":"test data","value":"test data"},"dbt":{"count":"test data","value":"test data"},"loss":{"count":"test data","value":"test data"},"DPD91To180":{"count":"test data","value":"test data"},"greaterThan180DPD":{"count":"test data","value":"test data"}}},"nonFunded":{"total":{"count":"test data","value":"test data"},"STDVec":{"DPD61To90":{"count":"test data","value":"test data"},"DPD0":{"count":"test data","value":"test data"},"DPD1To30":{"count":"test data","value":"test data"},"DPD31To60":{"count":"test data","value":"test data"}},"NONSTDVec":{"sub":{"count":"test data","value":"test data"},"dbt":{"count":"test data","value":"test data"},"loss":{"count":"test data","value":"test data"},"DPD91To180":{"count":"test data","value":"test data"},"greaterThan180DPD":{"count":"test data","value":"test data"}}},"termLoan":{"total":{"count":"test data","value":"test data"},"STDVec":{"DPD61To90":{"count":"test data","value":"test data"},"DPD0":{"count":"test data","value":"test data"},"DPD1To30":{"count":"test data","value":"test data"},"DPD31To60":{"count":"test data","value":"test data"}},"NONSTDVec":{"sub":{"count":"test data","value":"test data"},"dbt":{"count":"test data","value":"test data"},"loss":{"count":"test data","value":"test data"},"DPD91To180":{"count":"test data","value":"test data"},"greaterThan180DPD":{"count":"test data","value":"test data"}}},"workingCapital":{"total":{"count":"test data","value":"test data"},"STDVec":{"DPD61To90":{"count":"test data","value":"test data"},"DPD0":{"count":"test data","value":"test data"},"DPD1To30":{"count":"test data","value":"test data"},"DPD31To60":{"count":"test data","value":"test data"}},"NONSTDVec":{"sub":{"count":"test data","value":"test data"},"dbt":{"count":"test data","value":"test data"},"loss":{"count":"test data","value":"test data"},"DPD91To180":{"count":"test data","value":"test data"},"greaterThan180DPD":{"count":"test data","value":"test data"}}}},"outsideInstitution":{"total":{"STDVec":{"DPD61To90":{"count":"test data","value":"test data"},"DPD0":{"count":"test data","value":"test data"},"DPD1To30":{"count":"test data","value":"test data"},"DPD31To60":{"count":"test data","value":"test data"}},"NONSTDVec":{"sub":{"count":"test data","value":"test data"},"dbt":{"count":"test data","value":"test data"},"loss":{"count":"test data","value":"test data"},"DPD91To180":{"count":"test data","value":"test data"},"greaterThan180DPD":{"count":"test data","value":"test data"}}},"forex":{"total":{"count":"test data","value":"test data"},"STDVec":{"DPD61To90":{"count":"test data","value":"test data"},"DPD0":{"count":"test data","value":"test data"},"DPD1To30":{"count":"test data","value":"test data"},"DPD31To60":{"count":"test data","value":"test data"}},"NONSTDVec":{"sub":{"count":"test data","value":"test data"},"dbt":{"count":"test data","value":"test data"},"loss":{"count":"test data","value":"test data"},"DPD91To180":{"count":"test data","value":"test data"},"greaterThan180DPD":{"count":"test data","value":"test data"}}},"nonFunded":{"total":{"count":"test data","value":"test data"},"STDVec":{"DPD61To90":{"count":"test data","value":"test data"},"DPD0":{"count":"test data","value":"test data"},"DPD1To30":{"count":"test data","value":"test data"},"DPD31To60":{"count":"test data","value":"test data"}},"NONSTDVec":{"sub":{"count":"test data","value":"test data"},"dbt":{"count":"test data","value":"test data"},"loss":{"count":"test data","value":"test data"},"DPD91To180":{"count":"test data","value":"test data"},"greaterThan180DPD":{"count":"test data","value":"test data"}}},"termLoan":{"total":{"count":"test data","value":"test data"},"STDVec":{"DPD61To90":{"count":"test data","value":"test data"},"DPD0":{"count":"test data","value":"test data"},"DPD1To30":{"count":"test data","value":"test data"},"DPD31To60":{"count":"test data","value":"test data"}},"NONSTDVec":{"sub":{"count":"test data","value":"test data"},"dbt":{"count":"test data","value":"test data"},"loss":{"count":"test data","value":"test data"},"DPD91To180":{"count":"test data","value":"test data"},"greaterThan180DPD":{"count":"test data","value":"test data"}}},"workingCapital":{"total":{"count":"test data","value":"test data"},"STDVec":{"DPD61To90":{"count":"test data","value":"test data"},"DPD0":{"count":"test data","value":"test data"},"DPD1To30":{"count":"test data","value":"test data"},"DPD31To60":{"count":"test data","value":"test data"}},"NONSTDVec":{"sub":{"count":"test data","value":"test data"},"dbt":{"count":"test data","value":"test data"},"loss":{"count":"test data","value":"test data"},"DPD91To180":{"count":"test data","value":"test data"},"greaterThan180DPD":{"count":"test data","value":"test data"}}}}},"creditFacilityDetailsasBorrowerSecVec":{"message":"test data","creditFacilityDetailsasBorrowerSec":[{"message":"test data","creditFacilitySecurityDetailsVec":{"message":"test data","creditFacilitySecurityDetails":[{"lastReportedDt":"test data","classification":"test data","value":"test data","validationDt":"test data","relatedType":"test data","currency":"test data"}]},"creditFacilityCurrentDetailsVec":{"creditFacilityCurrentDetails":{"amount":{"drawingPower":"test data","markToMarket":"test data","notionalAmountOfContracts":"test data","installmentAmt":"test data","naorc":"test data","currency":"test data","lastRepaid":"test data","writtenOFF":"test data","contractsClassifiedAsNPA":"test data","overdue":"test data","settled":"test data","suitFiledAmt":"test data","sanctionedAmt":"test data","outstandingBalance":"test data","highCredit":"test data"},"otherDetails":{"restructingReason":"test data","repaymentFrequency":"test data","tenure":"test data","weightedAverageMaturityPeriodOfContracts":"test data","guaranteeCoverage":"test data","assetBasedSecurityCoverage":"test data"},"cfSerialNumber":"test data","accountNumber":"test data","cfType":"test data","derivative":"test data","status":"test data","dates":{"wilfulDefault":"test data","loanRenewalDt":"test data","suitFiledDt":"test data","loanExpiryDt":"test data","sanctionedDt":"test data"},"cfMember":"test data","lastReportedDate":"test data","statusDate":"test data","assetClassificationDaysPastDueDpd":"test data"}},"creditFacilityGuarantorDetailsVec":{"message":"test data","creditFacilityGuarantorDetails":[{"guarantorDetailsBorrwerIDDetailsVec":{"guarantorIDDetails":[{"uid":"test data","registrationNumber":"test data","din":"test data","drivingLicenseNumber":"test data","rationCard":"test data","passportNumber":"test data","cin":"test data","tin":"test data","otherID":"test data","voterID":"test data","serviceTaxNumber":"test data","pan":"test data"}],"lastReportedDate":"test data"},"guarantorAddressContactDetails":{"faxNumber":"test data","address":"test data","telephoneNumber":"test data","mobileNumber":"test data"},"guarantorDetails":{"message":"test data","dateOfBirth":"test data","name":"test data","gender":"test data","dateOfIncorporation":"test data","relatedType":"test data","businessCategory":"test data","businessIndustryType":"test data"}}]},"creditFacilityOverdueDetailsVec":{"message":"test data","creditFacilityOverdueDetails":{"DPD31To60Amt":"test data","DPD1To30Amt":"test data","DPDabove180Amt":"test data","DPD91To180Amt":"test data","DPD61T090Amt":"test data"}},"chequeDishounouredDuetoInsufficientFunds":{"message":"test data","CD4To6Monthcount":"test data","CD7To9Monthcount":"test data","CD10To12Monthcount":"test data","CD3Monthcount":"test data"},"CFHistoryforACOrDPDupto24MonthsVec":{"CFHistoryforACOrDPDupto24Months":[{"ACorDPD":"test data","month":"test data","OSAmount":"test data"}]}}]},"rankSec":{"message":"test data","rankVec":[{"rankValue":"test data","exclusionReason":"test data","rankName":"test data"}]},"creditRatingSummaryVec":{"message":"test data","creditRatingSummary":[{"creditRatingSummaryDetailsVec":[{"creditRating":"test data","lastReportedDt":"test data","ratingExpiryDt":"test data","ratingAsOn":"test data"}],"creditRatingAgency":"test data"}]},"derogatoryInformationSec":{"message":"test data","messageOfBorrowerOutsideInstitution":"test data","messageOfGuarantedParties":"test data","messageOfBorrowerYourInstitution":"test data","derogatoryInformationReportedOnGuarantedPartiesVec":{"derogatoryInformationReportedOnGuarantedParties":["test data"]},"messageOfRelatedPartiesYourInstitution":"test data","messageOfRelatedPartiesOutsideInstitution":"test data","derogatoryInformationBorrower":{"total":{"suitFilled":{"amt":"test data","numberOfSuitFiled":"test data"},"wilfulDefault":"test data","overdueCF":{"amt":"test data","numberOfSuitFiled":"test data"},"settled":{"amt":"test data","numberOfSuitFiled":"test data"},"dishonoredCheque":"test data","invoked":{"amt":"test data","numberOfSuitFiled":"test data"},"writtenOff":{"amt":"test data","numberOfSuitFiled":"test data"}},"yourInstitution":{"suitFilled":{"amt":"test data","numberOfSuitFiled":"test data"},"wilfulDefault":"test data","overdueCF":{"amt":"test data","numberOfSuitFiled":"test data"},"settled":{"amt":"test data","numberOfSuitFiled":"test data"},"dishonoredCheque":"test data","invoked":{"amt":"test data","numberOfSuitFiled":"test data"},"writtenOff":{"amt":"test data","numberOfSuitFiled":"test data"}},"outsideInstitution":{"suitFilled":{"amt":"test data","numberOfSuitFiled":"test data"},"wilfulDefault":"test data","overdueCF":{"amt":"test data","numberOfSuitFiled":"test data"},"settled":{"amt":"test data","numberOfSuitFiled":"test data"},"dishonoredCheque":"test data","invoked":{"amt":"test data","numberOfSuitFiled":"test data"},"writtenOff":{"amt":"test data","numberOfSuitFiled":"test data"}}},"messageOfRelatedParties":"test data","messageOfBorrower":"test data","derogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec":{"total":{"suitFilled":{"amt":"test data","numberOfSuitFiled":"test data"},"wilfulDefault":"test data","overdueCF":{"amt":"test data","numberOfSuitFiled":"test data"},"settled":{"amt":"test data","numberOfSuitFiled":"test data"},"dishonoredCheque":"test data","invoked":{"amt":"test data","numberOfSuitFiled":"test data"},"writtenOff":{"amt":"test data","numberOfSuitFiled":"test data"}},"yourInstitution":{"suitFilled":{"amt":"test data","numberOfSuitFiled":"test data"},"wilfulDefault":"test data","overdueCF":{"amt":"test data","numberOfSuitFiled":"test data"},"settled":{"amt":"test data","numberOfSuitFiled":"test data"},"dishonoredCheque":"test data","invoked":{"amt":"test data","numberOfSuitFiled":"test data"},"writtenOff":{"amt":"test data","numberOfSuitFiled":"test data"}},"outsideInstitution":{"suitFilled":{"amt":"test data","numberOfSuitFiled":"test data"},"wilfulDefault":"test data","overdueCF":{"amt":"test data","numberOfSuitFiled":"test data"},"settled":{"amt":"test data","numberOfSuitFiled":"test data"},"dishonoredCheque":"test data","invoked":{"amt":"test data","numberOfSuitFiled":"test data"},"writtenOff":{"amt":"test data","numberOfSuitFiled":"test data"}}}},"creditFacilityDetailsasGuarantorVec":{"message":"test data","creditFacilityDetailsasGuarantor":[{"borrwerInfo":{"borrwerAddressContactDetailsVec":{"borrwerAddressContactDetails":[{"faxNumber":"test data","address":"test data","telephoneNumber":"test data","mobileNumber":"test data"}]},"borrwerIDDetailsVec":{"borrwerIDDetails":[{"registrationNumber":"test data","cin":"test data","tin":"test data","serviceTaxNumber":"test data","pan":"test data"}],"lastReportedDate":"test data"},"borrwerDetails":{"borrowersLegalConstitution":"test data","name":"test data","salesFigure":"test data","classOfActivityVec":{"classOfActivity":[]},"numberOfEmployees":"test data","year":"test data","dateOfIncorporation":"test data","businessCategory":"test data","businessIndustryType":"test data"}},"creditFacilityCurrentDetails":[{"amount":{"drawingPower":"test data","markToMarket":"test data","notionalAmountOfContracts":"test data","installmentAmt":"test data","naorc":"test data","currency":"test data","lastRepaid":"test data","writtenOFF":"test data","contractsClassifiedAsNPA":"test data","overdue":"test data","settled":"test data","suitFiledAmt":"test data","sanctionedAmt":"test data","outstandingBalance":"test data","highCredit":"test data"},"otherDetails":{"restructingReason":"test data","repaymentFrequency":"test data","tenure":"test data","weightedAverageMaturityPeriodOfContracts":"test data","guaranteeCoverage":"test data","assetBasedSecurityCoverage":"test data"},"cfSerialNumber":"test data","accountNumber":"test data","cfType":"test data","derivative":"test data","status":"test data","dates":{"wilfulDefault":"test data","loanRenewalDt":"test data","suitFiledDt":"test data","loanExpiryDt":"test data","sanctionedDt":"test data"},"cfMember":"test data","lastReportedDate":"test data","statusDate":"test data","assetClassificationDaysPastDueDpd":"test data"}]}]},"enquiryDetailsInLast24MonthVec":{"message":"test data","enquiryDetailsInLast24Month":[{"enquiryAmt":"test data","enquiryDt":"test data","creditLender":"test data","enquiryPurpose":"test data"}]},"creditProfileSummarySec":{"total":{"totalCF":"test data","delinquentOutstanding":"test data","openCF":"test data","totalLenders":"test data","latestCFOpenedDate":"test data","totalOutstanding":"test data","delinquentCF":"test data"},"yourInstitution":{"message":"test data","totalCF":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"},"delinquentOutstanding":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"},"openCF":"test data","totalLenders":"test data","latestCFOpenedDate":"test data","totalOutstanding":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"},"delinquentCF":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"}},"outsideInstitution":{"message":"test data","outsideTotal":{"message":"test data","totalCF":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"},"delinquentOutstanding":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"},"openCF":"test data","totalLenders":"test data","latestCFOpenedDate":"test data","totalOutstanding":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"},"delinquentCF":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"}},"NBFCOthers":{"message":"test data","totalCF":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"},"delinquentOutstanding":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"},"openCF":"test data","totalLenders":"test data","latestCFOpenedDate":"test data","totalOutstanding":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"},"delinquentCF":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"}},"otherPrivateForeignBanks":{"message":"test data","totalCF":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"},"delinquentOutstanding":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"},"openCF":"test data","totalLenders":"test data","latestCFOpenedDate":"test data","totalOutstanding":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"},"delinquentCF":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"}},"otherPublicSectorBanks":{"message":"test data","totalCF":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"},"delinquentOutstanding":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"},"openCF":"test data","totalLenders":"test data","latestCFOpenedDate":"test data","totalOutstanding":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"},"delinquentCF":{"borrower":"test data","borrowerPercentage":"test data","guarantor":"test data","guarantorPercentage":"test data"}}}},"creditFacilitiesSummary":{"summaryOfCreditFacilitiesVec":{"summaryOfCreditFacilitiesRec":[]},"countOfCreditFacilities":{"noOfCreditFacilities":{"total":"test data","others":"test data","yourBank":"test data"},"noOfCreditGrantors":{"total":"test data","others":"test data","yourBank":"test data"}}},"locationDetailsSec":{"message":"test data","contactNumber":"test data","faxNumber":"test data","locationInformationVec":{"locationInformation":[{"numberOfInstitutions":"test data","borrowerOfficeLocationType":"test data","address":"test data","firstReportedDate":"test data","lastReportedDate":"test data"}]}},"enquirySummarySec":{"message":"test data","enquiryYourInstitution":{"noOfEnquiries":{"total":"test data","month4To6":"test data","mostRecentDate":"test data","month1":"test data","month12To24":"test data","month7To12":"test data","greaterthan24Month":"test data","month2To3":"test data"}},"enquiryOutsideInstitution":{"noOfEnquiries":{"total":"test data","month4To6":"test data","mostRecentDate":"test data","month1":"test data","month12To24":"test data","month7To12":"test data","greaterthan24Month":"test data","month2To3":"test data"}},"enquiryTotal":{"noOfEnquiries":{"total":"test data","month4To6":"test data","mostRecentDate":"test data","month1":"test data","month12To24":"test data","month7To12":"test data","greaterthan24Month":"test data","month2To3":"test data"}}},"relationshipDetailsVec":{"message":"test data","relationshipDetails":[{"borrwerIDDetailsVec":{"borrwerIDDetails":[{"uid":"test data","registrationNumber":"test data","din":"test data","drivingLicenseNo":"test data","rationCard":"test data","passportNumber":"test data","cin":"test data","tin":"test data","voterID":"test data","serviceTaxNumber":"test data","pan":"test data"}],"lastReportedDate":"test data"},"borrwerAddressContactDetails":{"faxNumber":"test data","address":"test data","telephoneNumber":"test data","mobileNumber":"test data"},"relationshipInformation":{"dateOfBirth":"test data","relationship":"test data","percentageOfControl":"test data","classOfActivity1":"test data","name":"test data","gender":"test data","dateOfIncorporation":"test data","relatedType":"test data","businessCategory":"test data","businessIndustryType":"test data"},"relationshipHeader":"test data"}]},"suitFiledVec":{"message":"test data","suitFilled":[{"suitFilledBy":"test data","suitRefNumber":"test data","dateSuit":"test data","suitAmt":"test data","suitStatus":"test data"}]},"creditFacilitiesDetailsVec":{"total":"test data","message":"test data","creditFacilitiesDetails":[{"creditType":"test data","ownership":"test data","reportedBy":"test data","closedDate":"test data","currentBalance":"test data","accountNo":"test data","lastReported":"test data","group":"test data"}]},"borrowerProfileSec":{"borrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec":{"message":"test data","borrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24Months":[]},"borrowerDelinquencyReportedOnBorrower":{"yourInstitution":{"last24Months":"test data","current":"test data"},"outsideInstitution":{"last24Months":"test data","current":"test data"}},"borrwerIDDetailsVec":{"borrwerIDDetails":[{"registrationNumber":"test data","cin":"test data","tin":"test data","serviceTaxNumber":"test data","pan":"test data"}],"lastReportedDate":"test data"},"borrwerAddressContactDetails":{"faxNumber":"test data","address":"test data","telephoneNumber":"test data","mobileNumber":"test data"},"borrwerDetails":{"borrowersLegalConstitution":"test data","name":"test data","salesFigure":"test data","classOfActivityVec":{"classOfActivity":["test data"]},"numberOfEmployees":"test data","year":"test data","dateOfIncorporation":"test data","businessCategory":"test data","businessIndustryType":"test data"}}}}}';
       
    /**
     * Recursive function to process json kev value pair to create Map<String, List<Object>>
     * @param jsonObject - JSON object to process
     * @param columnRowMAp - This will be final outcome after process key value pairs of a map
     * @param prefix - Prefix will be used to append key in front of inner object values
     * @param typeColumn - This will be used to create verticle type columns
     * @author - Mehul Jain
     **/
     
    public static Map<String, List<Object>> processJsonObject(Object jsonObject, Map<String, List<Object>> columnRowMap, String prefix, Set<String> typeColumns){
        if(jsonObject != null){
            //Json processing starts from here
            if(isMap(jsonObject)){
                //Get object map from json
                Map<String, Object> jsonObjectMap = (Map<String, Object>) jsonObject;
                //Iterate over each object
                for(String k : jsonObjectMap.keySet()){
                    Object obj = jsonObjectMap.get(k);
                    //Check if this key has an a Map as value
                    if(isMap(obj)){
                        //Process the object by calling the funtion recusively
                        processJsonObject(obj, columnRowMap, (prefix+k+'__'), typeColumns);
                    }
                    // Check if this key has a List as value
                    else if(isList(obj)){
                        //Add a type column record
                        addNewValue(columnRowMap, prefix+'type', prefix+k, typeColumns);
                        //Add new type column in set
                        typeColumns.add(prefix+'type');
                        processJsonObject(obj, columnRowMap, (prefix), typeColumns);
                        //Remove the type column from set
                        typeColumns.remove(prefix+'type');
                    }
                    //Normal key value pair
                    else{
                        //Push value inside the columnRowMap
                        addNewValue(columnRowMap, prefix+k, obj, typeColumns);
                    }
                }
            } else if(isList(jsonObject)){
                //Get object list from json
                List<Object> jsonObjectList = (List<Object>) jsonObject;
                //Iterate over each object
                for (Object obj : jsonObjectList) {
                    //Check if this key has an a Map or List as value
                    if(isMap(obj) || isList(obj)){
                        //Process the object by calling the funtion recusively
                        processJsonObject(obj, columnRowMap, (prefix), typeColumns);
                    }
                    //Normal key value pair
                    else{
                        //Push value inside the columnRowMap
                        addNewValue(columnRowMap, prefix, obj, typeColumns);
                    }
                }
            }
        }
        return columnRowMap;
    }
    
    /**
     * This function accepts a json string and returns csv string
     * @author - Mehul Jain
     * */
    public static String getCSVString(String jsonString){
        //Get JSON object from json string
        jsonString = strJSON ;
        Object jsonObject = JSON.deserializeUntyped(jsonString);
        //Output map for column and rows
        Map<String, List<Object>> columnRowMap = new Map<String, List<Object>>();
        //Get column row map
        //Map<String, List<Object>> columnRowMap = getColumnAndRows(jsonString);
        columnRowMap = processJsonObject(jsonObject, columnRowMap, '', new Set<String>());
        
        //String for csv body
        String csvBody = '';
        //String for column header
        String columns = '';
        //List of string for each row
        List<String> rows = new List<String>();
        //filling with blank rows
        for(Integer i=0; i<MAX_ROW; i++){
            rows.add('');
        }
        //Iterating over column row map
        for(String column : columnRowMap.keySet()){
            //Adding column header
            columns += column+',';
            //Getting records from a column
            List<Object> objectList = columnRowMap.get(column); //Get record in the column
            //Iterating over records from a columns
            for(Integer i=0; i<MAX_ROW; i++){
                //Get row value from rows list, if does not exist, it will throw an exception
                String row = rows.get(i);
                try{
                    Object obj = objectList.get(i);
                    //Getting record String value from the object. 
                    String record = obj != null ? (obj.toString()+',') : 'null,';
                    //Append the value in the row
                    row += record;
                    //Replace the existing row value with new one
                    rows.set(i, row);
                } catch(System.ListException e){
                    //Exception ocurred, means no record exist, add a comma for blank value
                    row += ',';
                    rows.set(i, row);
                } 
            }
        }
        csvBody = columns.removeEnd(',') + '\n'; //Add column header in csv body
        //Adding csv rows
        for(String row : rows){
            csvBody += row.removeEnd(',') + '\n';
        }
        return csvBody;
    }
    
    /**
     * Check if an object is a MAP<String, Object>
     * @author Mehul Jain
     * */
    public static Boolean isMap(Object obj){
        return (obj instanceof Map<String, Object>);
    }
    
    /**
     * Check if an object is a List<Object>
     * @author Mehul Jain
     * */
    public static Boolean isList(Object obj){
        return (obj instanceof List<Object>);
    }
    
    /**
     * Add new value inside columnRowMap
     * @author - Mehul Jain
     * */
    public static void addNewValue(Map<String, List<Object>> columnRowMap, String key, Object obj, Set<String> typeColumns){
        //Push value inside the columnRowMap
        if(columnRowMap.keySet().contains(key)){
            columnRowMap.get(key).add(obj);
            //Find maximum number of rows within a column
            if(columnRowMap.get(key).size() > MAX_ROW){
                MAX_ROW = columnRowMap.get(key).size();
                for(String typeColumn : typeColumns){
                    //Add null record in place of type column
                    columnRowMap.get(typeColumn).add('');
                }
            }
        } else{
            columnRowMap.put(key, new List<Object>{obj});
        }
    }
    
}