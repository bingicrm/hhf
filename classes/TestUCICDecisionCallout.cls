@isTest
public class TestUCICDecisionCallout {
    @testSetup static void testData() {
        /*
        UserRole r = [SELECT Id,Name FROM UserRole WHERE Name LIKE '%SM%' LIMIT 1];
        ID roleID= r.id;
        User u = new User(
            ProfileId ='00e7F000002yr4O',
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = roleID);
        Insert u;
        */
        //System.runAs(u){ 
            Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
            Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
            
            Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                      Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
            insert acc;
            
            Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                       Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
            insert acc1;
            
            acc1 =[Select Id, PAN__c, Name, Date_of_Incorporation__c, Phone, RecordTypeId, Customer_ID__c From Account where Id =: acc1.Id];
            Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
            insert con1;
            
            Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                                Scheme_Group_ID__c='12');
            insert objScheme;
            
            Loan_Application__c la1 = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                              Transaction_type__c='PB',Requested_Amount__c=100000,
                                                              Loan_Purpose__c='11', Loan_Number__c ='12345');
            insert la1;
            /*Loan_Application__c la2 = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                              Transaction_type__c='PB',Requested_Amount__c=500000,
                                                              Loan_Purpose__c='11', Loan_Number__c ='12345489');
            insert la2;*/
        //}
    }
    
    public static testMethod void makeUCICDecisionCalloutTest(){
        //User u = [SELECT Id FROM User WHERE Email = 'puser000@amamama.com'];
        UCICDecisionRequestBody reqBody = new UCICDecisionRequestBody();
        reqBody.SOURCE_CHANNEL = 'SFDC-HL';
        reqBody.LOAN_ACCOUNT_NUMBER = '';
        Map<Id, Loan_Application__c> mapLoanApplication = new Map<Id, Loan_Application__c>([SELECT Id FROM Loan_Application__c LIMIT 10]);
        TestMockRequest req1=new TestMockRequest(200, 'OK', JSON.serialize(reqBody), null);
        Test.setMock(HttpCalloutMock.class, req1);
        Test.startTest();
        //system.runAs(u){
            UCICDecisionCallout.makeUCICDecisionCallout(mapLoanApplication.keySet());
        //}
        Test.stopTest();
    }
}