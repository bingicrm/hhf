/*====================================================================================
class Name: GstReturnStatusCalloutTest
Author Name: Amit Agarwal
Description: This class Covers GstReturnStatusCallout Apex Class

===========================================================================================*/
@isTest
public class GstReturnStatusCalloutTest {
    @testSetup 
    static void setup() {
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',
                                  Name='Test Account',
                                  Date_of_Incorporation__c=System.today(),
                                  Phone='9934567890',
                                  RecordTypeId=RecordTypeIdAccount);
        insert acc;  
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',
                                   Salutation='Mr',
                                   FirstName='Test',
                                   LastName='Test Account1',
                                   Date_of_Incorporation__c=System.today(),
                                   Phone='9934567891',RecordTypeId=RecordTypeIdAccount1);
        insert acc1;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',
                                            Scheme_Code__c='9934567890',
                                            Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,
                                                                         Scheme__c=objScheme.Id,
                                                                         Transaction_type__c='PB',
                                                                         Requested_Amount__c=100000,
                                                                         Loan_Purpose__c='11');
        insert objLoanApplication;
        
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Guarantor').getRecordTypeId();
        
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,
                                                             Customer__c=acc1.Id,
                                                             Applicant_Type__c='Guarantor',
                                                             Borrower__c='1',
                                                             RecordTypeId=RecordTypeIdLoanContact,Mobile__c='9876543210',
                                                             Email__c='testemail@gmail.com',Relationship_with_Applicant__c='FTH',
                                                             Category__c='1');
        insert objLoanContact;
        Id recordTypePAN = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('GSTIN').getRecordTypeId();
        
        Customer_Integration__c objCustIntegration = new Customer_Integration__c(Loan_Application__c=objLoanApplication.Id,
                                                                                 Mobile__c='9876543210',
                                                                                 Constitution__c= 'ABCD',
                                                                                 GST_Tax_payer_type__c = 'TaxPayerType',
                                                                                 Current_Status_of_Registration_Under_GST__c = 'CurrentStatus',
                                                                                 VAT_Registration_Number__c = '27AAHFG0551H1234',
                                                                                 PAN_Number__c = 'BCEST2489G',
                                                                                 Trade_Name__c = 'Trade Name',
                                                                                 Legal_Name__c ='Legal Name',
                                                                                 Filling_Frequency__c = '23',
                                                                                 Business_Activity__c = 'Business Activity',
                                                                                 GST_Date_Of_Registration__c =Date.newInstance(2018, 12, 10),
                                                                                 Cumulative_Sales_Turnover__c =5000000,
                                                                                 Month_Count__c = 50,
                                                                                 GSTIN__c ='27AAHFG0551H1ZL', 
                                                                                 Email__c='testemail@gmail.com',
                                                                                 RecordTypeId=recordTypePAN,
                                                                                 Loan_Contact__c=objLoanContact.Id,                                                                          
                                                                                 PerfiosITRReport_Error_Response__c='test1',
                                                                                 PerfiosBankReport_Error_Response__c='test2',
                                                                                 PerfiosFinancialReport_Error_Response__c='test3');
        insert objCustIntegration;
        
        List<Customer_Integration__c> lstCustIntegration = new List<Customer_Integration__c>();
        lstCustIntegration.add(objCustIntegration);
    }
    
    private static testMethod void test1() {
        List<Customer_Integration__c> lstCustIntegration = [Select Id, Name, RecordTypeId, Mobile__c,Loan_Contact__c, Email__c, GSTIN__c From Customer_Integration__c Where RecordTypeId =: Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('GSTIN').getRecordTypeId()];
       
      GstReturnStatusCallout objGstReturnStatusCallout = new GstReturnStatusCallout(lstCustIntegration);
               
	    GstReturnStatusResponseBody objGstReturnStatusResponseBody = new GstReturnStatusResponseBody();
        
            GstReturnStatusResponseBody.cls_result objResult = new GstReturnStatusResponseBody.cls_result();
                objResult.filing_frequency = 'Monthly';
                objResult.financial_year = '2018-19';
                
                    GstReturnStatusResponseBody.cls_EFiledlist objEFiledlist = new GstReturnStatusResponseBody.cls_EFiledlist();
                        objEFiledlist.status = 'Filed';
                        objEFiledlist.dof = '04-06-2018';
                        objEFiledlist.ret_prd = '042018';
                        objEFiledlist.is_delay = false;
                        objEFiledlist.valid = 'Y';
                        objEFiledlist.rtntype = 'GSTR1';
                        objEFiledlist.mof = 'ONLINE';
                        objEFiledlist.arn = 'AB270418487880K';
                        
                    List<GstReturnStatusResponseBody.cls_EFiledlist> lstEFiledlist = new List<GstReturnStatusResponseBody.cls_EFiledlist>();
                    lstEFiledlist.add(objEFiledlist);
                    
                objResult.EFiledlist = lstEFiledlist;
                
            List<GstReturnStatusResponseBody.cls_result> lstResult = new List<GstReturnStatusResponseBody.cls_result>();
            lstResult.add(objResult);
            
        objGstReturnStatusResponseBody.result = lstResult;
        objGstReturnStatusResponseBody.requestId = '3852e90e-b90a-11e9-9fe7-d9cbf53ef6bd';
        objGstReturnStatusResponseBody.error = 'None';
        objGstReturnStatusResponseBody.statusCode = 101;
        
        
        Test.startTest(); 
        TestMockRequest req1=new TestMockRequest(200,'OK',JSON.serialize(objGstReturnStatusResponseBody),null);
        Test.setMock(HttpCalloutMock.class, req1);
        System.enqueueJob(objGstReturnStatusCallout);   
        Test.stopTest();   
    }
    
     private static testMethod void test2() {
     List<Customer_Integration__c> lstCustIntegration = [Select Id, Name, RecordTypeId, Mobile__c,Loan_Contact__c, Email__c, GSTIN__c From Customer_Integration__c Where RecordTypeId =: Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('GSTIN').getRecordTypeId()];
       
      GstReturnStatusCallout objGstReturnStatusCallout = new GstReturnStatusCallout(lstCustIntegration);
       //  EnqueDMLGSTIN objResq = new EnqueDMLGSTIN();
    Id recordTypePAN = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('GSTIN').getRecordTypeId();

    Loan_Application__c objLoanApplication = [Select Id From Loan_Application__c limit 1];
	Loan_Contact__c objLoanContact = [Select Id From Loan_Contact__c limit 1];
	Customer_Integration__c objCustIntegration1 = new Customer_Integration__c(Loan_Application__c=objLoanApplication.Id,
                                                                                 Mobile__c='9876543210',
                                                                                 Constitution__c= 'ABCD',
                                                                                 GST_Tax_payer_type__c = 'TaxPayerType',
                                                                                 Current_Status_of_Registration_Under_GST__c = 'CurrentStatus',
                                                                                 VAT_Registration_Number__c = '27AAHFG0551H1234',
                                                                                 PAN_Number__c = 'BCEST2489G',
                                                                                 Trade_Name__c = 'Trade Name',
                                                                                 Legal_Name__c ='Legal Name',
                                                                                 Filling_Frequency__c = '23',
                                                                                 Business_Activity__c = 'Business Activity',
                                                                                 GST_Date_Of_Registration__c =Date.newInstance(2018, 12, 10),
                                                                                 Cumulative_Sales_Turnover__c =5000000,
                                                                                 Month_Count__c = 50,
                                                                                 GSTIN__c ='27AAHFG0551H1ZL', 
                                                                                 Email__c='testemail@gmail.com',
                                                                                 RecordTypeId=recordTypePAN,
                                                                                 Loan_Contact__c=objLoanContact.Id,                                                                          
                                                                                 PerfiosITRReport_Error_Response__c='test1',
                                                                                 PerfiosBankReport_Error_Response__c='test2',
                                                                                 PerfiosFinancialReport_Error_Response__c='test3');
        
        List<Customer_Integration__c> lstCustIntegration1 = new List<Customer_Integration__c>();
        lstCustIntegration1.add(objCustIntegration1);	
        
	    GstReturnStatusResponseBody objGstReturnStatusResponseBody = new GstReturnStatusResponseBody();
        
            GstReturnStatusResponseBody.cls_result objResult = new GstReturnStatusResponseBody.cls_result();
                objResult.filing_frequency = 'Monthly';
                objResult.financial_year = '2018-19';
                
                    GstReturnStatusResponseBody.cls_EFiledlist objEFiledlist = new GstReturnStatusResponseBody.cls_EFiledlist();
                        objEFiledlist.status = 'Filed';
                        objEFiledlist.dof = '04-06-2018';
                        objEFiledlist.ret_prd = '042018';
                        objEFiledlist.is_delay = false;
                        objEFiledlist.valid = 'Y';
                        objEFiledlist.rtntype = 'GSTR1';
                        objEFiledlist.mof = 'ONLINE';
                        objEFiledlist.arn = 'AB270418487880K';
                        
                    List<GstReturnStatusResponseBody.cls_EFiledlist> lstEFiledlist = new List<GstReturnStatusResponseBody.cls_EFiledlist>();
                    lstEFiledlist.add(objEFiledlist);
                    
                objResult.EFiledlist = lstEFiledlist;
                
            List<GstReturnStatusResponseBody.cls_result> lstResult = new List<GstReturnStatusResponseBody.cls_result>();
            lstResult.add(objResult);
            
        objGstReturnStatusResponseBody.result = lstResult;
        objGstReturnStatusResponseBody.requestId = '3852e90e-b90a-11e9-9fe7-d9cbf53ef6bd';
        objGstReturnStatusResponseBody.error = 'None';
        objGstReturnStatusResponseBody.statusCode = 101;
        
        
        Test.startTest(); 
        TestMockRequest req1=new TestMockRequest(200,'OK',JSON.serialize(objGstReturnStatusResponseBody),null);
        Test.setMock(HttpCalloutMock.class, req1);
        System.enqueueJob(objGstReturnStatusCallout); 
        EnqueDMLGSTIN objResq = new EnqueDMLGSTIN(lstCustIntegration1[0],'jsonRequest','String','lstRestService');
        System.enqueueJob(objResq);      
        Test.stopTest();   
    }
}