//Created By Amit Agarwal
@isTest
public class TestGroupExposureGroupID { 
    @testSetup
    private static void testSetupMethod() {
        Profile p = [SELECT id, name from profile where name =: 'Sales Team'];
        User u = new User(
            ProfileId = p.Id,LastName = 'last',Email = 'puser000@amamama.com',Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',Title = 'title',Alias = 'alias',TimeZoneSidKey = 'America/Los_Angeles',EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US',Technical__c = true,FCU__c= true,Legal__c = true, FI__c= true,LIP__C = true,PD__c= true,
        	Role_in_Sales_Hierarchy__c='SM');
        insert u;        
        Loan_Contact__c lc;
        //Loan_Contact__c lc1;
        Loan_Application__c la;
        
        System.runAs(u) {
            Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
            
            Account acc = new Account();
            acc.CountryCode__c = '+91';
            acc.Email__c = 'puser001@amamama.com';
            acc.Gender__c = 'M';
            acc.Mobile__c = '9800765432';
            acc.Phone__c = '9800765434';
            acc.PersonMobilePhone = '9800765434';
            acc.FirstName = 'abc';
            acc.LastName = 'def';
            acc.Salutation = 'Mr';
            acc.RecordTypeId = RecordTypeIdCustomer;
            insert acc;
            
            Account acc1 = new Account();
            acc1.CountryCode__c = '+91';
            acc1.Email__c = 'puser002@amamama.com';
            acc1.Gender__c = 'M';
            acc1.Mobile__c = '9800765433';
            acc1.Phone__c = '9800765435';
            acc1.PersonMobilePhone = '7800765434';
            acc1.FirstName = 'abcd';
            acc1.LastName = 'defg';
            acc1.Salutation = 'Mr';
            acc1.RecordTypeId = RecordTypeIdCustomer;
            insert acc1;
            Scheme__c sch = new Scheme__c(Name = 'Construction Finance', Scheme_Code__c = 'CF', Scheme_Group_ID__c = '5',
                                          Scheme_ID__c = 11, Product_Code__c = 'CF', Max_ROI__c = 10, Min_ROI__c = 5,
                                          Min_Loan_Amount__c = 10000, Min_FOIR__c = 1,
                                          Max_Loan_Amount__c = 3000001, Max_Tenure__c = 84, Min_Tenure__c = 6,
                                          Max_LTV__c = 5, Min_LTV__c = 1, Max_FOIR__c = 5);
            insert sch;
            
            Scheme__c sch1=new Scheme__c();
            sch1.Name='Personal Loan';
            sch1.Product_Code__c='IPL'; 
            sch1.Scheme_Code__c='IPL';
            sch1.Scheme_Group_ID__c='Test';
            insert sch1;
            Local_Policies__c lp = new Local_Policies__c(Name='Standard', Branch__c='All',Product_Scheme__c='All Schemes',IsActive__c=true);
            insert lp;
            
            Id RecordTypeIdLAAppIni = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Application Initiation').getRecordTypeId();
            
            la = new Loan_Application__c(Customer__c = acc.Id, Scheme__c = sch.Id, Loan_Number__c = '124',
                                         Transaction_type__c = 'PB', Requested_Amount__c = 1000000,
                                         Loan_Purpose__c = '11', StageName__c = 'Customer Onboarding', Sub_Stage__c = 'Disbursement Checker',
                                         RecordTypeId = RecordTypeIdLAAppIni,Branch__c='Delhi',
                                         Business_Date_Created__c = System.today(),
                                         Repayment_schedule_at_Disbursement_Check__c = true,
                                         Approved_Loan_Amount__c = 1000000,Requested_Loan_Tenure__c=84,
                                         Processing_Fee_Percentage__c = 1, Mode_of_Repayment__c = 'NACH',
                                         Repayment_Start_Date__c = Date.Today().addDays(100),Income_Program_Type__c='Normal Salaried',
                                         Sanctioned_Disbursement_Type__c = 'Multiple Tranche', Property_Identified__c = true,
                                         Local_Policies__c=lp.Id);
            insert la;
            
            lc = [SELECT Id, Borrower__c from Loan_Contact__c where Loan_Applications__c =: la.Id];
            lc.Borrower__c = '1';
            //lc.Category__c='1';
            lc.Gender__c = 'Male';
            lc.Customer__c = acc.Id;
            lc.Applicant_Type__c = 'Applicant';
            lc.Constitution__c = '20';
            lc.Customer_segment__c = '3';
            lc.BRE_Record__c = true;
            lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc.Marital_Status__c = 'M';
            lc.Gross_Salary__c = 1000;
            lc.Basic_Pay__c = 1000;
            lc.DA__c = 1000;
            lc.Qualification__c = 'UMS';
            lc.Net_Income__c = 1000;
            lc.PF__c = 1000;
            lc.TDS__c = 1000;
            lc.Current_Organization_Work_Ex_in_Months__c = 12;
            lc.Total_Work_Experience__c = 12;
            lc.Customer_Details_Verified__c = true;
            lc.Voter_ID_Number__c = '123458967';
            lc.Passport_Number__c = '123456789';
            lc.PAN_Number__c = 'BAFPV6543A';
            lc.Aadhaar_Number__c = '898518762736';
            lc.Income_Considered__c = true;
            lc.AIP_month__c = '4-2020';
            lc.Mobile__c = '7878565698';
            lc.Email__c = 'abc@g.com';
            lc.Applicant_Type__c='Applicant';
            update lc;
            
            test.startTest();
            
            Loan_Contact__c lc1 = new Loan_Contact__c();
            lc1.Loan_Applications__c =la.id;
            lc1.Borrower__c = '1';
            //lc.Category__c='1';
            lc1.Gender__c = 'Male';
            lc1.Customer__c = acc1.Id;
            lc1.Constitution__c = '20';
            lc1.Customer_segment__c = '3';
            lc1.BRE_Record__c = true;
            lc1.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc1.Marital_Status__c = 'M';
            lc1.Gross_Salary__c = 1000;
            lc1.Basic_Pay__c = 1000;
            lc1.DA__c = 1000;
            lc1.Qualification__c = 'UMS';
            //lc.Total_Fixed_Pay__c = 1000;
            //lc.Variable_Income_Monthly__c = 1000;
            lc1.Net_Income__c = 1000;
            lc1.PF__c = 1000;
            lc1.TDS__c = 1000;
            lc1.Current_Organization_Work_Ex_in_Months__c = 12;
            lc1.Total_Work_Experience__c = 12;
            lc1.Customer_Details_Verified__c = true;
            lc1.Voter_ID_Number__c = '123456789';
            lc1.Passport_Number__c = '123456789';
            lc1.PAN_Number__c = 'BAFPV6543A';
            lc1.Aadhaar_Number__c = '898518762736';
            lc1.Income_Considered__c = true;
            lc1.AIP_month__c = '4-2020';
            lc1.Mobile__c = '7878565677';
            lc1.Email__c = 'abc@g.com';
            lc1.Applicant_Type__c='Co- Applicant';
            insert lc1;
            
            Id parameter = la.id;
            UCIC_Global__c setting = new UCIC_Global__c();
            setting.UCIC_Active__c = true;
            insert setting;
            
            test.stopTest();
        }
    }
    
    public static testmethod void method1(){
        User u = [Select id from User Where Email = 'puser000@amamama.com'];
        System.runAs(u){
            
            Loan_Contact__c lc = [SELECT Id,Loan_Applications__r.id from Loan_Contact__c where Loan_Applications__r.Sub_Stage__c =: 'Disbursement Checker' AND Applicant_Type__c=: 'Applicant' LIMIT 1];
            Id conparam = lc.id;
            Loan_Application__c la = [Select id from Loan_Application__c WHERE Sub_Stage__c =: 'Disbursement Checker' LIMIT 1]; 
            Test.startTest();
            GroupExposureGroupID.checkGroupID(la.id);
            Loan_Contact__c retVal = GroupExposureGroupID.getLoanCon(conparam);
            System.assertNotEquals(Null, retVal);     
            Test.stopTest();
            
            
        }       
    }
    
    public static testmethod void method2(){
        User u = [Select id from User Where Email = 'puser000@amamama.com'];
        System.runAs(u){
            Test.startTest();
            Account acc = [Select id from Account limit 1];
            acc.Group_Id__c ='123456';
            update acc;
            Loan_Contact__c lc = [SELECT Id,Loan_Applications__r.id from Loan_Contact__c where Loan_Applications__r.Sub_Stage__c =: 'Disbursement Checker' AND Applicant_Type__c=: 'Applicant' LIMIT 1];
            lc.Customer__c = acc.Id;
            update lc;
            Id conparam = lc.id;            
            Loan_Application__c la = [Select id from Loan_Application__c WHERE Sub_Stage__c =: 'Disbursement Checker' LIMIT 1]; 
            GroupExposureGroupID.checkGroupID(la.id);
            Loan_Contact__c retVal = GroupExposureGroupID.getLoanCon(conparam);
            System.assertNotEquals(Null, retVal);            
            Test.stopTest();
            
        }       
    }
    
    public static testmethod void method3(){
        User u = [Select id from User Where Email = 'puser000@amamama.com'];
        System.runAs(u){
            Test.startTest();
            Account acc = [Select id from Account Where Email__c =: 'puser001@amamama.com' limit 1];
            acc.Group_Id__c ='123456';
            update acc;
            Account acc1 = [Select id from Account Where Email__c =: 'puser002@amamama.com' limit 1];
            acc1.Group_Id__c ='123459';
            update acc1;            
            Loan_Application__c la = [Select id from Loan_Application__c WHERE Sub_Stage__c =: 'Disbursement Checker' LIMIT 1]; 
            Loan_Contact__c lc1 = [SELECT Id, Borrower__c from Loan_Contact__c where Applicant_Type__c=: 'Co- Applicant' AND Loan_Applications__c =: la.Id LIMIT 1];
            lc1.Customer__c = acc1.Id;
            update lc1;
            Loan_Contact__c lc = [SELECT Id, Borrower__c from Loan_Contact__c where Applicant_Type__c=: 'Applicant' AND Loan_Applications__c =: la.Id LIMIT 1];
            lc.Customer__c = acc.Id;
            update lc;       
            String retVal = GroupExposureGroupID.checkGroupID(la.id);
            System.assertNotEquals(Null, retVal);   
            Test.stopTest();
            
        }       
    }
}