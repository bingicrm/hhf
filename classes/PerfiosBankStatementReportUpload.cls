@RestResource(urlMapping='/PerfiosBankStatementReport/*')
global with sharing class PerfiosBankStatementReportUpload {
    /*
    * @Description     This method is used to parse the Perfios Bank Statement Report
    * @author          Shobhit Saxena(Deloitte)
    * @returns         ResultResponse
    */
    @HttpPost
    global static ResultResponse parsePerfiosBankStatementReport(){
        PerfiosBankStmtRetrieveResponseBodyRest objResponseBody = new PerfiosBankStmtRetrieveResponseBodyRest();
        objResponseBody = (PerfiosBankStmtRetrieveResponseBodyRest)System.JSON.deserialize(RestContext.request.requestBody.tostring(),PerfiosBankStmtRetrieveResponseBodyRest.class);
        ResultResponse resRep = new ResultResponse();
        Boolean isSuccess = true;
        System.debug('Debug Log for objResponseBody'+objResponseBody);
        Customer_Integration__c cust = new Customer_Integration__c();
        
        List<Bank_Statement_Summary__c> lstBankStmttSummary = new List<Bank_Statement_Summary__c>();
        List<Monthly_Inflow_Outflow__c> lstMonthlyInflowOutflow = new List<Monthly_Inflow_Outflow__c>();
        List<Regular_Debit__c> lstRegularDebit = new List<Regular_Debit__c>();
        
        if(objResponseBody != null && String.isNotBlank(objResponseBody.customerInfo.customerTransactionId)) {
                
                cust = [SELECT ID, Analysis_Status__c, (SELECT Id FROM Bank_Statement_Summaries__r),(SELECT Id FROM Monthly_Inflow_Outflows__r), (SELECT Id FROM Regular_Debits__r) FROM Customer_Integration__c where Id=:objResponseBody.customerInfo.customerTransactionId];
                
                if(objResponseBody.accountXns != null) {
                    for(PerfiosBankStmtRetrieveResponseBodyRest.cls_accountXns accountXN: objResponseBody.accountXns) {
                        if(accountXN.accountNo != null) {
                            cust.Bank_Account_Number__c = accountXN.accountNo;    
                        }  
                        
                    }
                }
                
                //Added by Vaishali for BREII
                if(objResponseBody.customerInfo != null) {
                    if(objResponseBody.customerInfo.name != null) {
                        cust.Account_Holder_Name__c = objResponseBody.customerInfo.name;    
                    }  
                }
                //End of patch- Added by Vaishali for BREII
                
                Bank_Statement_Summary__c objBankStmttSummary = new Bank_Statement_Summary__c();
                
                if(objResponseBody.summaryInfo != null) {
                    if(objResponseBody.summaryInfo.total != null) {
                        
                        objBankStmttSummary.Customer_Integration__c = objResponseBody.customerInfo.customerTransactionId;
                        
                        if(objResponseBody.summaryInfo.total.chqIssues != null) {
                            objBankStmttSummary.No_of_Cheque_Presented_for_Clearing__c = objResponseBody.summaryInfo.total.chqIssues;
                        }
                        
                        if(objResponseBody.summaryInfo.total.chqDeposits != null) {
                            objBankStmttSummary.No_of_Cheque_Received_for_Deposits__c = objResponseBody.summaryInfo.total.chqDeposits;
                        }
                        
                        if(objResponseBody.summaryInfo.total.inwChqBounces != null) {
                            objBankStmttSummary.No_of_Inward_Check_Bounce__c = objResponseBody.summaryInfo.total.inwChqBounces;
                        }
                        
                        if(objResponseBody.summaryInfo.total.outwChqBounces != null) {
                            objBankStmttSummary.No_of_Outward_Cheque_Bounce__c = objResponseBody.summaryInfo.total.outwChqBounces;
                        }
                        if(objResponseBody.summaryInfo.total.totalInwChqBounce != null) {
                            objBankStmttSummary.Inward_Cheque_Bounces_Amount__c = objResponseBody.summaryInfo.total.totalInwChqBounce;
                        }
                        if(objResponseBody.summaryInfo.total.totalOutwChqBounce != null) {
                            objBankStmttSummary.Outward_Cheque_Bounces_Amount__c = objResponseBody.summaryInfo.total.totalOutwChqBounce;
                        }
                        if(objResponseBody.summaryInfo.total.totalChqIssue != null) {
                            objBankStmttSummary.Cheque_Presented_for_Clearing_Amount__c = objResponseBody.summaryInfo.total.totalChqIssue;
                        }
                        if(objResponseBody.summaryInfo.total.totalChqDeposit != null) {
                            objBankStmttSummary.Cheque_Received_for_Deposits_Amount__c = objResponseBody.summaryInfo.total.totalChqDeposit;
                        }
                        if(objResponseBody.summaryInfo.total.inwBounces != null) {
                            objBankStmttSummary.No_of_EMI_Cheque_NACH_Bounce__c = objResponseBody.summaryInfo.total.inwBounces;
                        }
                        if(objResponseBody.summaryInfo.total.emiOrLoans != null) {
                            objBankStmttSummary.No_of_EMI_Payments_FOIR_Calculations_C__c = objResponseBody.summaryInfo.total.emiOrLoans;
                        }
                    }
                }
                if(objResponseBody.monthlyDetails != null) {
                    System.debug('Debug Log for Monthly Details Record Count'+objResponseBody.monthlyDetails.size());
                    Decimal ABBMonthly = 0.00;
                    for(PerfiosBankStmtRetrieveResponseBodyRest.cls_monthlyDetails obj_monthlyDetails : objResponseBody.monthlyDetails) {
                        Monthly_Inflow_Outflow__c objMonthlyInflowOutflow = new Monthly_Inflow_Outflow__c();
                        Monthly_Inflow_Outflow__c objMonthlyInflowOutflow1 = new Monthly_Inflow_Outflow__c();   //Added by Vaishali for BREII
                        
                        if(obj_monthlyDetails.monthName != null) {
                            if(obj_monthlyDetails.monthName.containsIgnoreCase('Aug')) {
                                objMonthlyInflowOutflow.Month__c = 'August';
                            }
                            else if(obj_monthlyDetails.monthName.containsIgnoreCase('Jul')) {
                                objMonthlyInflowOutflow.Month__c = 'July';
                            }
                            else if(obj_monthlyDetails.monthName.containsIgnoreCase('Jun')) {
                                objMonthlyInflowOutflow.Month__c = 'June';
                            }
                            else if(obj_monthlyDetails.monthName.containsIgnoreCase('May')) {
                                objMonthlyInflowOutflow.Month__c = 'May';
                            }
                            else if(obj_monthlyDetails.monthName.containsIgnoreCase('Sep')) {
                                objMonthlyInflowOutflow.Month__c = 'September';
                            }
                            else if(obj_monthlyDetails.monthName.containsIgnoreCase('Oct')) {
                                objMonthlyInflowOutflow.Month__c = 'October';
                            }
                            else if(obj_monthlyDetails.monthName.containsIgnoreCase('Nov')) {
                                objMonthlyInflowOutflow.Month__c = 'November';
                            }
                            else if(obj_monthlyDetails.monthName.containsIgnoreCase('Dec')) {
                                objMonthlyInflowOutflow.Month__c = 'December';
                            }
                            else if(obj_monthlyDetails.monthName.containsIgnoreCase('Jan')) {
                                objMonthlyInflowOutflow.Month__c = 'January';
                            }
                            else if(obj_monthlyDetails.monthName.containsIgnoreCase('Feb')) {
                                objMonthlyInflowOutflow.Month__c = 'February';
                            }
                            else if(obj_monthlyDetails.monthName.containsIgnoreCase('Mar')) {
                                objMonthlyInflowOutflow.Month__c = 'March';
                            }
                            else if(obj_monthlyDetails.monthName.containsIgnoreCase('Apr')) {
                                objMonthlyInflowOutflow.Month__c = 'April';
                            }
                            objMonthlyInflowOutflow.Year__c = '20'+obj_monthlyDetails.monthName.subStringAfterLast('-');
                        }
                        
                        if(obj_monthlyDetails.credits != null) {
                            objMonthlyInflowOutflow.Total_Inflow_Count__c = obj_monthlyDetails.credits;
                        }
                        if(obj_monthlyDetails.debits != null) {
                            objMonthlyInflowOutflow.Total_Outflow_Count__c = obj_monthlyDetails.debits;
                        }
                        if(obj_monthlyDetails.totalCredit != null) {
                            objMonthlyInflowOutflow.Total_Inflow_Value__c = obj_monthlyDetails.totalCredit;
                        }
                        if(obj_monthlyDetails.totalDebit != null) {
                            objMonthlyInflowOutflow.Total_Outflow_Value__c = obj_monthlyDetails.totalDebit;
                        }
                        if(obj_monthlyDetails.cashDeposits != null) {
                            objMonthlyInflowOutflow.Total_Cash_Inflow_Count__c = obj_monthlyDetails.cashDeposits;
                        }
                        if(obj_monthlyDetails.totalCashDeposit != null) {
                            objMonthlyInflowOutflow.Total_Cash_Inflow_Value__c = obj_monthlyDetails.totalCashDeposit;
                        }
                        if(obj_monthlyDetails.cashWithdrawals != null) {
                            objMonthlyInflowOutflow.Total_Cash_Outflow_Count__c = obj_monthlyDetails.cashWithdrawals;
                        }
                        if(obj_monthlyDetails.totalCashWithdrawal != null) {
                            objMonthlyInflowOutflow.Total_Cash_Outflow_Value__c = obj_monthlyDetails.totalCashWithdrawal;
                        }
                        if(obj_monthlyDetails.balAvg != null) {
                            objMonthlyInflowOutflow.ABB_Monthly__c = obj_monthlyDetails.balAvg;
                        }else {
                            objMonthlyInflowOutflow.ABB_Monthly__c = 0.00; 
                        }
                        objMonthlyInflowOutflow.Customer_Integration__c = objResponseBody.customerInfo.customerTransactionId;   
                        
                        //Added by Vaishali for BREII
                        if(obj_monthlyDetails.balOpen != null) {
                            objMonthlyInflowOutflow.Opening_Balance_of_Month__c = obj_monthlyDetails.balOpen;
                        }
                        if(obj_monthlyDetails.balLast != null) {
                            objMonthlyInflowOutflow.Closing_Balance_of_Month__c = obj_monthlyDetails.balLast;
                        }
                        if(obj_monthlyDetails.inwChqBounces != null) {
                            objMonthlyInflowOutflow.inwChqBounces__c = obj_monthlyDetails.inwChqBounces;
                        }
                        if(obj_monthlyDetails.outwChqBounces != null) {
                            objMonthlyInflowOutflow.outwChqBounces__c = obj_monthlyDetails.outwChqBounces;
                        }
                        if(obj_monthlyDetails.inwEMIBounces != null) {
                            objMonthlyInflowOutflow.EMI_Bounce__c = obj_monthlyDetails.inwEMIBounces;
                        }
                        if(obj_monthlyDetails.totalInwEMIBounce != null) {
                            objMonthlyInflowOutflow.EMI_Bounce_Amount__c = obj_monthlyDetails.totalInwEMIBounce;
                        }
                        if(obj_monthlyDetails.loanDisbursals != null) {
                            objMonthlyInflowOutflow.Loan_Disbursed__c = obj_monthlyDetails.loanDisbursals;
                        }
                        if(obj_monthlyDetails.totalLoanDisbursal != null) {
                            objMonthlyInflowOutflow.totalLoanDisbursal__c = obj_monthlyDetails.totalLoanDisbursal;
                        }
                        if(obj_monthlyDetails.balMin != null) {
                            objMonthlyInflowOutflow.Minimum_EOD_Balance__c = obj_monthlyDetails.balMin;
                        }
                        if(obj_monthlyDetails.balMax != null) {
                            objMonthlyInflowOutflow.Maximum_EOD_Balance__c = obj_monthlyDetails.balMax;
                        }
                        if(obj_monthlyDetails.salaries != null) {
                            objMonthlyInflowOutflow.Salary_Credit__c = obj_monthlyDetails.salaries;
                        }
                        if(obj_monthlyDetails.totalSalary != null) {
                            objMonthlyInflowOutflow.Salary_Credit_Amount__c = obj_monthlyDetails.totalSalary;
                        }
                        Decimal balOn5 = 0;
                        Decimal balOn15 = 0;
                        Decimal balOn25 = 0;
                        
                        if(obj_monthlyDetails.bal5 != null) {
                            objMonthlyInflowOutflow.Balance_on_5__c = obj_monthlyDetails.bal5;
                            balOn5 = obj_monthlyDetails.bal5;
                        }
                        if(obj_monthlyDetails.bal15 != null) {
                            objMonthlyInflowOutflow.Balance_on_15__c = obj_monthlyDetails.bal15;
                            balOn15 = obj_monthlyDetails.bal15;
                        }
                        if(obj_monthlyDetails.bal25 != null) {
                            objMonthlyInflowOutflow.Balance_on_25__c = obj_monthlyDetails.bal25;
                            balOn25 = obj_monthlyDetails.bal25;
                        }
                        
                        objMonthlyInflowOutflow.ABB_BRE__c = (balOn5 + balOn15 + balOn25)/3;
                        objMonthlyInflowOutflow1 = objMonthlyInflowOutflow.clone(); 
                        objMonthlyInflowOutflow1.BRE_Record__c = true;   
                        lstMonthlyInflowOutflow.add(objMonthlyInflowOutflow1); 
                        //End of patch- Added by Vaishali for BREII
                        
                        lstMonthlyInflowOutflow.add(objMonthlyInflowOutflow);
                        
                    }
                    
                    
                    lstBankStmttSummary.add(objBankStmttSummary);
                    
                }
                if(objResponseBody.regularDebits != null) {
                    System.debug('Debug Log for eODBalances Record Count'+objResponseBody.regularDebits.size());
                    for(PerfiosBankStmtRetrieveResponseBodyRest.cls_regularDebits obj_regularDebits : objResponseBody.regularDebits) {
                        Regular_Debit__c objRegularDebit = new Regular_Debit__c();
                        if(obj_regularDebits.amount != null) {
                            objRegularDebit.Amount_of_regular_debit__c = obj_regularDebits.amount;
                        }
                        if(!String.isBlank(obj_regularDebits.date_regularDebits)) {
                            system.debug('Date is blank');
                            objRegularDebit.Date_of_Debit__c = Date.valueOf(obj_regularDebits.date_regularDebits);
                        }
                        if(!String.isBlank(obj_regularDebits.category)) {
                            objRegularDebit.Category__c = obj_regularDebits.category;
                        }
                        if(!String.isBlank(obj_regularDebits.chqNo)) {
                            objRegularDebit.Cheque_No__c = obj_regularDebits.chqNo;
                        }
                        if(!String.isBlank(obj_regularDebits.narration)) {
                            objRegularDebit.Description__c = obj_regularDebits.narration;
                        }
                        if(obj_regularDebits.balance != null) {
                            objRegularDebit.Balance__c = obj_regularDebits.balance;
                        }
                        objRegularDebit.Customer_Integration__c = objResponseBody.customerInfo.customerTransactionId;
                        lstRegularDebit.add(objRegularDebit);
                    }
                }
            }
            
            else {
                resRep.Result = 'Failed!! SFDC Record Id not Specified.';
                HttpResponse res = new HttpResponse();
                res.setBody(String.valueOf(resRep)); 
                //LogUtility.createIntegrationLogs(RestContext.request.requestBody.tostring(),res,'Perfios Bank Statement Report Upload');
                return resRep;
            }
            
            if(!cust.Bank_Statement_Summaries__r.isEmpty()) {
                List<Database.DeleteResult> lstDSR = Database.delete(cust.Bank_Statement_Summaries__r, false);
                for(Database.DeleteResult objDSR : lstDSR) {
                    if(!objDSR.getErrors().isEmpty()) {
                        System.debug('Following Error Occured while deleting Bank Statement Summary'+objDSR.getErrors());
                        resRep.ErrorMsg += objDSR.getErrors();
                        isSuccess = false;
                    }
                    else {
                        System.debug('Bank Statement Summary record deleted Successfully, with Id'+objDSR.getId());
                    }
                }                
            }
            
            if(!cust.Monthly_Inflow_Outflows__r.isEmpty()) {
                List<Database.DeleteResult> lstDSR = Database.delete(cust.Monthly_Inflow_Outflows__r, false);
                for(Database.DeleteResult objDSR : lstDSR) {
                    if(!objDSR.getErrors().isEmpty()) {
                        System.debug('Following Error Occured while deleting Monthly Inflow Outflow'+objDSR.getErrors());
                        resRep.ErrorMsg += objDSR.getErrors();
                        isSuccess = false;
                    }
                    else {
                        System.debug('Monthly Inflow Outflow record deleted Successfully, with Id'+objDSR.getId());
                    }
                }    
            }
            
            if(!cust.Regular_Debits__r.isEmpty()) {
               List<Database.DeleteResult> lstDSR = Database.delete(cust.Regular_Debits__r, false);
                for(Database.DeleteResult objDSR : lstDSR) {
                    if(!objDSR.getErrors().isEmpty()) {
                        System.debug('Following Error Occured while deleting Regular Debits'+objDSR.getErrors());
                        resRep.ErrorMsg += objDSR.getErrors();
                        isSuccess = false;
                    }
                    else {
                        System.debug('Regular Debits record deleted Successfully, with Id'+objDSR.getId());
                    }
                }        
            }
            
            System.debug('Debug Log for lstBankStmttSummary'+lstBankStmttSummary.size());
            if(!lstBankStmttSummary.isEmpty()) {
                List<Database.SaveResult> lstDSR = Database.insert(lstBankStmttSummary, false);
                for(Database.SaveResult objDSR : lstDSR) {
                    if(!objDSR.getErrors().isEmpty()) {
                        System.debug('Following Error Occured while saving Bank Statement Summary'+objDSR.getErrors());
                        resRep.ErrorMsg += objDSR.getErrors();
                        isSuccess = false;
                    }
                    else {
                        System.debug('Bank Statement Summary Record Saved Successfully, with Id'+objDSR.getId());
                    }
                }
            }
            
            System.debug('Debug Log for lstMonthlyInflowOutflow'+lstMonthlyInflowOutflow.size());
            if(!lstMonthlyInflowOutflow.isEmpty()) {
                List<Database.SaveResult> lstDSR = Database.insert(lstMonthlyInflowOutflow, false);
                for(Database.SaveResult objDSR : lstDSR) {
                    if(!objDSR.getErrors().isEmpty()) {
                        System.debug('Following Error Occured while saving the monthly inflow outflow'+objDSR.getErrors());
                        resRep.ErrorMsg += objDSR.getErrors();
                        isSuccess = false;
                    }
                    else {
                        System.debug('Monthly inflow outflow Record Saved with Id'+objDSR.getId());
                    }
                }
            }           
                            
            System.debug('Debug Log for lstRegularDebit'+lstRegularDebit.size());
            if(!lstRegularDebit.isEmpty()) {
                List<Database.SaveResult> lstDSR = Database.insert(lstRegularDebit, false);
                for(Database.SaveResult objDSR : lstDSR) {
                    if(!objDSR.getErrors().isEmpty()) {
                        System.debug('Following Error Occured while saving Regular Debit'+objDSR.getErrors());
                        resRep.ErrorMsg += objDSR.getErrors();
                        isSuccess = false;
                    }
                    else {
                        System.debug('Regular Debit Record Saved Successfully, with Id'+objDSR.getId());
                    }
                }
            }
            
            system.debug('Result' + isSuccess);
            if(isSuccess){
                system.debug('Setting cust values');
                cust.Analysis_Status__c = 'Completed Successfully';
                Database.SaveResult result= Database.update(cust, false);
                if(!result.getErrors().isEmpty()) {
                    System.debug('Following Error Occured while saving cust'+result.getErrors());
                    resRep.ErrorMsg += result.getErrors();
                    resRep.Result = 'Failed!!';
                }
                else {
                    System.debug('Cust Saved Successfully, with Id'+result.getId());
                    resRep.Result = 'Sucess!!';
                }
            }
            else{
                resRep.Result = 'Failed!!';
            }
            
            HttpResponse res = new HttpResponse();
            res.setBody(String.valueOf(resRep)); 
            //LogUtility.createIntegrationLogs(RestContext.request.requestBody.tostring(),res,'Perfios Bank Statement Report Upload');
            return resRep;
        
    }
    
    global class ResultResponse{
        String ErrorMsg;
        String Result;
        public ResultResponse(){
            ErrorMsg = '{}';
            Result = '';
        }
    }
}