/*****************************************************
Class Name    : PddOtcIntegeration
Author        : Abhishek Pratik
Description   : This class is used to integrate saleforce to PDD OTC.
Created Date  : 28/09/2018
Related Component : 
*********************************************************/
public without sharing class PddOtcIntegeration {
	
	public static void PlaceRequest( String DocChecklistId){
		Map<String, object> ResponseMap = new Map<String, object>();
		
		Rest_Service__mdt serviceDetails=[select Client_Password__c,Client_Username__c,
       Request_Method__c,Time_Out_Period__c, Service_EndPoint__c from Rest_Service__mdt where MasterLabel='PddOtc'];
	   
	   Document_Checklist__c DocCheaklist = [select id,Document_Type__c, Loan_Contact__c, Loan_Applications__r.Name, Loan_Contact__r.Name, Name, Document_Type__r.Name, Current_Stage__c, Loan_Contact__r.VoterID_Address__c from Document_Checklist__c where id =: DocChecklistId];
	   
		if(serviceDetails!=null && DocCheaklist != null){
			if(DocCheaklist.Document_Type__c != null  && DocCheaklist.Loan_Contact__c != null){
				JSONGenerator gen = JSON.createGenerator(true);
					gen.writeStartObject();
						gen.writeStringField('applicationId',DocCheaklist.Loan_Applications__r.Name); // Required field
						gen.writeStringField('customerId',DocCheaklist.Loan_Contact__r.Name); // Required field
						gen.writeStringField('documentId',DocCheaklist.Name); // Required field
						gen.writeStringField('documenttype',DocCheaklist.Document_Type__r.Name); // Required field
						gen.writeStringField('fileName',DocCheaklist.Document_Type__r.Name);
						gen.writeStringField('imageName',DocCheaklist.Document_Type__r.Name);
						gen.writeStringField('stage',DocCheaklist.Current_Stage__c); // Required field
						gen.writeStringField('image','CAS_IMG1533811568826.jpg","stage":"qde","subStage":"qde","image":"/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB\\'+'nAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEB\\'+'nAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCACiAHkDASIA\\'+'nAhEBAxEB');
						if(DocCheaklist.Loan_Contact__r.VoterID_Address__c != null)
							gen.writeStringField('address',DocCheaklist.Loan_Contact__r.VoterID_Address__c);
						gen.writeStringField('longitude','');
						gen.writeStringField('latitude','');
						gen.writeStringField('userId','cointrive'); // Required field
						gen.writeStringField('password','zqbAx8rZ0LvWMftg38eTatwjEANYAo/6'); // Required field
						gen.writeStringField('requestId',Utility.generateRandomRequestID()); // Required field
						gen.writeStringField('sourceId','SFDC_HFL'); // Required field
						gen.writeStringField('receivedDate',string.valueOf(system.today())); // Required field
					gen.writeEndObject();
				
				// Instantiate a new HTTP request, specify the method (post) as well as the endpoint		
				HttpRequest req = new HttpRequest();
					req.setEndpoint(serviceDetails.Service_EndPoint__c);
					req.setHeader('Content-Type', 'application/json');
					req.setHeader('accept', 'application/json');
					req.setHeader('operation-flag', 'C');
					req.setHeader('user-id', 'CAS user id');
					Blob headerValue = Blob.valueOf(serviceDetails.Client_Username__c +':'+ serviceDetails.Client_Password__c);
					String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
					req.setHeader('Authorization', authorizationHeader);
					req.setTimeout(120000);
					req.setMethod(serviceDetails.Request_Method__c);
					req.setBody(gen.getAsString());
					
				// Send the request, and return a response
				HttpResponse res = new HttpResponse();
				
				// Instantiate a new http object
				Http h = new Http();
				res = h.send(req);
				System.debug(res.getStatusCode());
				//System.debug(res.getBody());
                if(res.getStatusCode() == 200){
                    ResponseMap =  (Map<String, object>)JSON.deserializeUntyped(res.getBody());
                    system.debug('ResponseMap.... '+ResponseMap); 
                    system.debug('customerId.... '+ResponseMap.get('customerId'));
                    system.debug('dateCreated.... '+ResponseMap.get('dateCreated'));
                }
			}
		}
	}
}