/* **************************************************************************
*
* Class: S3LinkFileTriggerHandler
* Created by Suresh Meghnathi: 12/09/2019
*
* - This is S3-File trigger handler.

* - Modifications:
* - Suresh Meghnathi, 12/09/2019 – Initial Development
************************************************************************** */
public class S3LinkFileTriggerHandler {
    // Flag to skipp trigger
    public static Boolean isSkipTrigger = false;
    
    /*
    *   Executed:   On after insert of files
    *   Purpose:    Setting field values
    *   Parameters: 
    *   UnitTests:  
    */
    public void onAfterInsert(List<NEILON__File__c> newFiles){
        if(!isSkipTrigger){
            // List of file ids to process
            Set<Id> fileIdsToUpdateUsingContentVersion = new Set<Id>();
            Set<Id> fileIdsToUpdateUsingAttachment = new Set<Id>();
            
            // Check if filed values needs to be displayed for file
            for(NEILON__File__c file : newFiles){
                if(file.NEILON__Export_Attachment_Id__c != null && file.Loan_Application__c != null && file.Document_Checklist__c == null){
                    if(file.NEILON__Export_Attachment_Id__c.startsWith('068')){
                        fileIdsToUpdateUsingContentVersion.add(file.Id);
                    } else if(file.NEILON__Export_Attachment_Id__c.startsWith('00P')){
                        fileIdsToUpdateUsingAttachment.add(file.Id);
                    }
                }
            }
            
            // Process files to set field values
            if(!fileIdsToUpdateUsingAttachment.isEmpty()){
                S3LinkFileTriggerHandler.setFieldValuesUsingAttachments(fileIdsToUpdateUsingAttachment);
            }
            if(!fileIdsToUpdateUsingContentVersion.isEmpty()){
                S3LinkFileTriggerHandler.setFieldValuesUsingContentVersions(fileIdsToUpdateUsingContentVersion);
            }
        }
    }
    
    
    /*
    *   Purpose:    This method is used to set field values for S3-Files using attachment ids
    *   Parameters: 
    *   UnitTests:  
    */
    public static void setFieldValuesUsingAttachments(Set<Id> fileIds){
        // Get files
        List<NEILON__File__c> files = [Select Id, NEILON__Export_Attachment_Id__c, Loan_Application__c, Loan_Application__r.Name, NEILON__Folder__r.Name From NEILON__File__c Where Id IN: fileIds];
        
        // Loan application Ids
        Set<Id> loanApplicationIds = new Set<Id>();
        
        // Attachment Ids
        Set<Id> attachmentIds = new Set<Id>();
        
        // File Id by attachment ids
        Map<Id, NEILON__File__c> fileByAttachmentIds = new Map<Id, NEILON__File__c>();
        
        // Folder configuration by name
        List<S3_Folders_Configuration__mdt> folderConfigs = [SELECT Id, Delete_files__c, Folder_Name__c FROM S3_Folders_Configuration__mdt Where DeveloperName = 'Notes_and_Attachments'];
        
        // Go through files and prepare list of loan application ids
        for(NEILON__File__c file : files){
            // Prepare list of loan applications
            loanApplicationIds.add(file.Loan_Application__c);
            
            // Prepare list of attachments
            attachmentIds.add(file.NEILON__Export_Attachment_Id__c);
            
            // Prepare map
            fileByAttachmentIds.put(file.NEILON__Export_Attachment_Id__c, file);
        }
        
        // Get attachments
        Map<Id, Attachment> attachmentsById = new Map<Id, Attachment>([Select Id, Name, BodyLength, ParentId From Attachment Where Id IN: attachmentIds]);
        
        // Get document migration logs
        List<Document_Migration_Log__c> existingDocumentMigrationLogs = [SELECT Document_Checklist__c, Document_Id__c, File_Name_If_Attachment__c, Id, Loan_Application__c, Successfully_Migrated__c FROM Document_Migration_Log__c WHERE Successfully_Migrated__c = false AND Loan_Application__c IN: loanApplicationIds AND Document_Id__c != null];
        
        // Document mogration logs by loan application and document id
        Map<String, Document_Migration_Log__c> documentMigrationLogsByUniqueId = new Map<String, Document_Migration_Log__c>();
        
        // Prepare map
        for(Document_Migration_Log__c existingDocumentMigrationLog : existingDocumentMigrationLogs){
            String uniqueId = existingDocumentMigrationLog.Loan_Application__c + '-' + existingDocumentMigrationLog.Document_Id__c;
            documentMigrationLogsByUniqueId.put(uniqueId, existingDocumentMigrationLog);
        }
        
        // List of files to update
        List<NEILON__File__c> filesToUpdate = new List<NEILON__File__c>();
        
        // List of document migration logs
        List<Document_Migration_Log__c> documentMigrationLogsToUpsert = new List<Document_Migration_Log__c>();
        
        // List of attachments to delete
        List<Attachment> attachmentsToDelete = new List<Attachment>();
        
        // Go through attachments
        for(Id attachmentId : attachmentsById.keySet()){
            // Get attachment
            Attachment attachment = attachmentsById.get(attachmentId);
            
            // Get S3-File
            NEILON__File__c file = fileByAttachmentIds.get(attachmentId);
            
            // Get folder
            NEILON__Folder__c folder = file.NEILON__Folder__r;
            
            // Get loan application
            Loan_Application__c loanApplication = file.Loan_Application__r;
            
            // Check if content document needs to be deleted
            for(S3_Folders_Configuration__mdt folderConfig : folderConfigs){
                // Folder name
                String folderName = folderConfig.Folder_Name__c + ' ' + loanApplication.Name;
                if(folderName == folder.Name && folderConfig.Delete_files__c) {
                    attachmentsToDelete.add(attachment);
                }
            }
            
            // Prepare document migration log
            Document_Migration_Log__c documentMigrationLog = new Document_Migration_Log__c();
            documentMigrationLog.Loan_Application__c = file.Loan_Application__c;
            documentMigrationLog.S3_File__c = file.Id;
            documentMigrationLog.Successfully_Migrated__c = false;
            documentMigrationLog.Document_Id__c = attachmentId;
            documentMigrationLog.Size_of_the_Document__c = String.valueOf(attachment.BodyLength);
            documentMigrationLog.File_Name_If_Attachment__c = attachment.Name;
            documentMigrationLog.Is_Attachment__c = true;
            
            // Get unique id to get migration log
            String uniqueId = loanApplication.Id +'-' + attachment.Id;
            
            
            // Check if document migration log already exist
            if(documentMigrationLogsByUniqueId.containsKey(uniqueId)){
                documentMigrationLog.Id = documentMigrationLogsByUniqueId.get(uniqueId).Id;
            }
            
            // Add into list
            documentMigrationLogsToUpsert.add(documentMigrationLog);
            
            // Set file fields from attachment
            file.NEILON__Description__c = String.isNotBlank(attachment.Name) ? attachment.Name : '';
            
            // Set custom field
            file.Exported_Id_Attachement__c = attachment.Id;
            
            // Add into the list to update
            filesToUpdate.add(file);
        }
        
        // Update files
        if(!filesToUpdate.isEmpty()){
            update filesToUpdate;
        }
        
        // Set success flag in document migration log
        if(!documentMigrationLogsToUpsert.isEmpty()) {
            // Go through files and set success flag
            for(NEILON__File__c updatedFile : filesToUpdate) {
                for(Document_Migration_Log__c documentMigrationLog : documentMigrationLogsToUpsert) {
                    if(documentMigrationLog.S3_File__c == updatedFile.Id){
                        documentMigrationLog.Successfully_Migrated__c = true;
                        documentMigrationLog.Upload_Status__c = null;
                        documentMigrationLog.Description__c = documentMigrationLog.File_Name_If_Attachment__c;
                    }
                }
            }
            
            // Create logs
            Database.UpsertResult[] logInsertResults = Database.upsert(documentMigrationLogsToUpsert, false);
        }
        
        // Delete attachments
        if(!attachmentsToDelete.isEmpty()){
            delete attachmentsToDelete;
        }
    }
    
    /*
    *   Purpose:    This method is used to set field values for S3-Files using content version ids
    *   Parameters: 
    *   UnitTests:  
    */
    public static void setFieldValuesUsingContentVersions(Set<Id> fileIds){
        // Get files
        List<NEILON__File__c> files = [Select Id, Document_Checklist__c, NEILON__Export_Attachment_Id__c, Loan_Application__c, Loan_Application__r.Name, NEILON__Folder__r.Name From NEILON__File__c Where Id IN: fileIds];
        
        // Loan application Ids
        Set<Id> loanApplicationIds = new Set<Id>();
        
        // Content version Ids
        Set<Id> contentVersionIds = new Set<Id>();
        
        // Content document Ids
        Set<Id> contentDocumentIds = new Set<Id>();
        
        // Content version Id by Content document Ids
        Map<Id, Id> contentVersionIdByContentDocumentIds = new Map<Id, Id>();
        
        // Linked Entity Id by Content document Ids
        Map<Id, Id> entityIdByContentDocumentIds = new Map<Id, Id>();
        
        // File Id by Content document Ids
        Map<Id, NEILON__File__c> fileByContentVersionIds = new Map<Id, NEILON__File__c>();
        
        // Folder configuration by name
        List<S3_Folders_Configuration__mdt> folderConfigs = [SELECT Id, Delete_files__c, Folder_Name__c FROM S3_Folders_Configuration__mdt];
        
        // Go through files and prepare list of loan application ids
        for(NEILON__File__c file : files){
            // Prepare list of loan applications
            loanApplicationIds.add(file.Loan_Application__c);
            
            // Prepare list of content versions
            contentVersionIds.add(file.NEILON__Export_Attachment_Id__c);
            
            // Prepare map
            fileByContentVersionIds.put(file.NEILON__Export_Attachment_Id__c, file);
        }
        
        // Get Document checklist for loan applications
        Map<Id, Document_Checklist__c> documentCheckListByIds = new Map<Id, Document_Checklist__c>([SELECT Id, Loan_Applications__c, Loan_Contact__c, Loan_Contact__r.Customer__r.Customer_ID__c, Document_Master__c, Document_Type__c, Customer_Type__c, Loan_Contact__r.Applicant_Type__c From Document_Checklist__c Where Loan_Applications__c IN: loanApplicationIds]);
        
        // Get content versions
        Map<Id, ContentVersion> contentVersionsById = new Map<Id, ContentVersion>([Select Id, ContentSize, Title, ContentDocumentId, ContentDocument.Id From ContentVersion Where Id IN: contentVersionIds]);
        
        // Prepare list of content docuemnts
        for(ContentVersion contentVersion : contentVersionsById.values()){
            // Prepare list
            contentDocumentIds.add(contentVersion.ContentDocumentId);
            
            // Prepare map
            contentVersionIdByContentDocumentIds.put(contentVersion.ContentDocumentId, contentVersion.Id);
        }
        
        // Get document migration logs
        List<Document_Migration_Log__c> existingDocumentMigrationLogs = [SELECT Document_Checklist__c, Document_Id__c, File_Name_If_Attachment__c, Id, Loan_Application__c, Successfully_Migrated__c FROM Document_Migration_Log__c WHERE Successfully_Migrated__c = false AND Loan_Application__c IN: loanApplicationIds AND Document_Id__c != null];
        
        // Document mogration logs by loan application and document id
        Map<String, Document_Migration_Log__c> documentMigrationLogsByUniqueId = new Map<String, Document_Migration_Log__c>();
        
        // Prepare map
        for(Document_Migration_Log__c existingDocumentMigrationLog : existingDocumentMigrationLogs){
            String uniqueId = existingDocumentMigrationLog.Loan_Application__c + '-' + existingDocumentMigrationLog.Document_Checklist__c + '-' + existingDocumentMigrationLog.Document_Id__c;
            documentMigrationLogsByUniqueId.put(uniqueId, existingDocumentMigrationLog);
        }
        
        // Get content document links
        List<ContentDocumentLink> contentDocumentLinks = [Select Id, ContentDocumentId, LinkedEntityId From ContentDocumentLink Where ContentDocumentId IN: contentDocumentIds AND (LinkedEntityId IN: documentCheckListByIds.keySet() OR LinkedEntityId IN: loanApplicationIds)];
    
        // Prepare map
        for(ContentDocumentLink contentDocumentLink : contentDocumentLinks){
            entityIdByContentDocumentIds.put(contentDocumentLink.ContentDocumentId, contentDocumentLink.LinkedEntityId);
        }
        
        // List of files to update
        List<NEILON__File__c> filesToUpdate = new List<NEILON__File__c>();
        
        // List of document migration logs
        List<Document_Migration_Log__c> documentMigrationLogsToUpsert = new List<Document_Migration_Log__c>();
        
        // List of content documents to delete
        List<ContentDocument> contentDocumentsToDelete = new List<ContentDocument>();
        
        // Go through content version
        for(Id contentDocumentId : entityIdByContentDocumentIds.keySet()){
            // Get content version
            Id contentVersionId = contentVersionIdByContentDocumentIds.get(contentDocumentId);
            
            // Get content version
            ContentVersion  contentVersion = contentVersionsById.get(contentVersionId);
            
            // Get S3-File
            NEILON__File__c file = fileByContentVersionIds.get(contentVersionId);
            
            // Get folder
            NEILON__Folder__c folder = file.NEILON__Folder__r;
            
            // Get loan application
            Loan_Application__c loanApplication = file.Loan_Application__r;
            
            // Check if content document needs to be deleted
            for(S3_Folders_Configuration__mdt folderConfig : folderConfigs){
                // Folder name
                String folderName = folderConfig.Folder_Name__c + ' ' + loanApplication.Name;
                if(folderName == folder.Name && folderConfig.Delete_files__c) {
                    contentDocumentsToDelete.add(contentVersion.ContentDocument);
                }
            }
            
            // Get document check list id
            Id documentCheckListId = entityIdByContentDocumentIds.get(contentDocumentId);
            
            // Get document check list
            Document_Checklist__c documentCheckList;
            if(documentCheckListId != null){
                documentCheckList = documentCheckListByIds.get(documentCheckListId);
            }
            
            // Prepare document migration log
            Document_Migration_Log__c documentMigrationLog = new Document_Migration_Log__c();
            documentMigrationLog.Loan_Application__c = file.Loan_Application__c;
            documentMigrationLog.S3_File__c = file.Id;
            documentMigrationLog.Successfully_Migrated__c = false;
            documentMigrationLog.Document_Id__c = contentDocumentId;
            documentMigrationLog.File_Name_If_Attachment__c = contentVersion.Title;
            documentMigrationLog.Is_Attachment__c = false;
            documentMigrationLog.Size_of_the_Document__c = String.valueOf(contentVersion.ContentSize);
            if(documentCheckList != null){
                documentMigrationLog.Document_Checklist__c = documentCheckList.Id;
            }
            
            // Get unique id to get migration log
            String uniqueId = loanApplication.Id + '-' + documentMigrationLog.Document_Checklist__c + '-' + contentDocumentId;
            
            // Check if document migration log already exist
            if(documentMigrationLogsByUniqueId.containsKey(uniqueId)){
                documentMigrationLog.Id = documentMigrationLogsByUniqueId.get(uniqueId).Id;
            }
            
            // Add into list
            documentMigrationLogsToUpsert.add(documentMigrationLog);
            
            
            // Set file fields from document check list
            edS3FileUtils.setFileFieldsFromDocumentCheckList(file, documentCheckList, contentVersion);
            
            // Add into the list to update
            filesToUpdate.add(file);
        }
        
        // Update files
        if(!filesToUpdate.isEmpty()){
            update filesToUpdate;
        }
        
        // Set success flag in document migration log
        if(!documentMigrationLogsToUpsert.isEmpty()) {
            // Go through files and set success flag
            for(NEILON__File__c updatedFile : filesToUpdate) {
                for(Document_Migration_Log__c documentMigrationLog : documentMigrationLogsToUpsert) {
                    if(documentMigrationLog.S3_File__c == updatedFile.Id){
                        documentMigrationLog.Successfully_Migrated__c = true;
                        documentMigrationLog.Upload_Status__c = null;
                        documentMigrationLog.Description__c = documentMigrationLog.File_Name_If_Attachment__c;
                    }
                }
            }
            
            // Create logs
            Database.UpsertResult[] logInsertResults = Database.upsert(documentMigrationLogsToUpsert, false);
        }
        
        // Delete content documents
        if(!contentDocumentsToDelete.isEmpty()){
            delete contentDocumentsToDelete;
        }
    }
}