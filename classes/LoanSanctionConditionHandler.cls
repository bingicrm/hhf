public with sharing class LoanSanctionConditionHandler 
{
    
    public static void afterInsertHandler( List<Loan_Sanction_Condition__c> newList )
    {
        Set<Id> documentCheckList = new Set<Id>();
        for(Loan_Sanction_Condition__c sc :  newList )
        {
            if( sc.Sanction_Condition_Master__c != null )
            {
                documentCheckList.add(sc.Sanction_Condition_Master__c);
            }
        }
        if(documentCheckList.size() > 0 )
            insertDocuments(documentCheckList,newList);
    }
    public static void beforeUdpateHandler( List<Loan_Sanction_Condition__c> newList, Map<Id,Loan_Sanction_Condition__c> oldMap )
    {
																			  
																										 
        for( Loan_Sanction_Condition__c sc : newList )
        {   
            if(sc.Customer_Details__c != oldMap.get(sc.id).Customer_Details__c && oldMap.get(sc.id).Customer_Details__c != null /*AND condition added by KK for BREII*/)
            {
                sc.addError('You cannot change the customer');
            }
																					  
		  
																   
								  
																														  
																	 
			 
        }
									   
														 
																
															   
				 
			  
		 
		
																		
    }

public static void AfterUdpateHandler( List<Loan_Sanction_Condition__c> newList, 
    Map<Id,Loan_Sanction_Condition__c> oldMap )
    {
        list<id> loanList = new list<id>();
        list<id> uncheckList = new list<id>();
        for( Loan_Sanction_Condition__c sc : newList )
        {   
            if((sc.Status__c != oldMap.get(sc.id).Status__c)&&(sc.Status__c == 'Waiver Requested'))
            {
                loanList.add(sc.Loan_Application__c);
            }

            if((sc.Status__c != oldMap.get(sc.id).Status__c)&&(oldMap.get(sc.id).Status__c == 'Waiver Requested'))
            {
                uncheckList.add(sc.Loan_Application__c);
            }

        }
        if(loanList.size()>0){
            list<Loan_Application__c> loanApp = [select id, sanction_waiver_requested__c from Loan_Application__c where id in: loanList];
            for(Loan_Application__c la: loanApp){
                la.sanction_waiver_requested__c = true;
            }
            update loanApp;
        }
        if(uncheckList.size()>0){
        //  list<Loan_Sanction_Condition__c> sancList = [select id, status__c , Loan_Application__c from 
        //  Loan_Sanction_Condition__c where Loan_Application__c in :uncheckList ];
            list<Loan_Application__c> loanApp = [select id, 
            (select id, status__c , Loan_Application__c from 
            Loan_Sanction_Conditions__r where status__c = 'Waiver Requested'), sanction_waiver_requested__c
             from Loan_Application__c where id in: uncheckList];

            for(Loan_Application__c la: loanApp){
                  if(la.Loan_Sanction_Conditions__r.size() == 0)
                la.sanction_waiver_requested__c = false;
            }
            update loanApp;
        }
    }

    public static void insertDocuments( Set<Id> sanctionConditionsMaster, List<Loan_Sanction_Condition__c> newList)
    {

        List<Document_Checklist__c> toInsert = new List<Document_Checklist__c>();
        if( sanctionConditionsMaster.size() > 0 )
        {
            List<Document_Master__c> allDocuments = [select id,Sanction_Condition_Master__c from Document_Master__c where Sanction_Condition_Master__c IN :sanctionConditionsMaster];
            for( Loan_Sanction_Condition__c sc :  newList )
            {
                for(Document_Master__c docMaster : allDocuments)
                {
                    if( sc.Loan_Application__c != null && sc.Sanction_Condition_Master__c != null && sc.Sanction_Condition_Master__c == docMaster.Sanction_Condition_Master__c)
                    {
                        Document_Checklist__c temp = new Document_Checklist__c();
                        temp.Document_Master__c = docMaster.Id;
                        temp.Loan_Applications__c = sc.Loan_Application__c;
                        temp.Status__c = 'Pending';
                        toInsert.add(temp);
                    }
                }   
            }

            if( toInsert.size() >  0 )
            {
                insert toInsert;
            }
        }
        
    }
}