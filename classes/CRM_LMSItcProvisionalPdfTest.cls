@isTest
public class CRM_LMSItcProvisionalPdfTest {
    @testSetup static void setup(){
        CRM_Utility.insertRestService('itc');
        
        Loan_Application__c loanApp = new Loan_Application__c();
        loanApp.Loan_Number__c = '1012682';
        loanApp.Loan_Application_Number__c = 'HHFDELHOU20000003628';
        insert loanApp;
        
        case theCase = new case();
        theCase.Loan_Application__c = loanApp.Id;
        theCase.LD_Branch_ID__c = '1';
        insert theCase;
    }
    
    static testMethod void testItcProvisionalPdf(){
        case theCase = [Select Id from case order by createddate desc limit 1];
        
        date caseFromDate = date.newinstance(2015, 12, 8);
        date caseToDate = date.newinstance(2016, 5, 11);
        map<string,string> dateMap = new map<string,string>();
        dateMap.put('caseFromDate',CRM_CaseController.getDate(caseFromDate).toUpperCase());
        dateMap.put('caseToDate',CRM_CaseController.getDate(caseToDate).toUpperCase());
        dateMap.put('today',CRM_CaseController.getDate(date.today()).toUpperCase());
        
        PageReference pdf = Page.CRM_LMSItcProvisionalPdf;
        Test.setCurrentPage(pdf);
        pdf.getParameters().put('id', String.valueOf(theCase.Id));
        pdf.getParameters().put('pdfDetailsMap',JSON.serialize(dateMap));
        ApexPages.StandardController sc = new ApexPages.StandardController(theCase);
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new CRM_LmsItcProvisionalMock());
        CRM_CaseItcProvisionalPdfController pdfControllerOne = new CRM_CaseItcProvisionalPdfController(sc);
        pdfControllerOne.logUtility();
        test.stopTest();
    }
}