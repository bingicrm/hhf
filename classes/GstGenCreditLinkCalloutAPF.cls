public class GstGenCreditLinkCalloutAPF implements Queueable, Database.AllowsCallouts{
    
    public List<Builder_Integrations__c> lstBuilderIntegration;
    public List<Promoter_Integrations__c> lstPromoterIntegration;
    public GstGenCreditLinkCalloutAPF(List<Builder_Integrations__c> lstBuilderIntegration, List<Promoter_Integrations__c> lstPromoterIntegration){
        this.lstBuilderIntegration = lstBuilderIntegration;
        this.lstPromoterIntegration = lstPromoterIntegration;
    }
    public void execute(QueueableContext context) {
        if(lstBuilderIntegration != null && !lstBuilderIntegration.isEmpty()) {
            for(Builder_Integrations__c objBI : lstBuilderIntegration) {
                if(objBI.Id != null && objBI.GSTIN__c != null && objBI.Mobile__c != null && objBI.Email__c != null) {
                    makeGstGenCreditLinkCallout(objBI.GSTIN__c,objBI.Email__c, objBI.Mobile__c, objBI.Id, objBI.Project_Builder__c); 
                }
            }
        }
        
        if(lstPromoterIntegration != null && !lstPromoterIntegration.isEmpty()) {
            for(Promoter_Integrations__c objPI : lstPromoterIntegration) {
                if(objPI.Id != null && objPI.GSTIN__c != null && objPI.Mobile__c != null && objPI.Email__c != null) {
                    makeGstGenCreditLinkCallout(objPI.GSTIN__c,objPI.Email__c, objPI.Mobile__c, objPI.Id, objPI.Promoter__c); 
                }
            }
        }
    }
         
    @future(callout = true)   
    public static void makeGstGenCreditLinkCallout(String strGSTIN, String strEmail, String strMobile, String strRecordId, String ParentRecordId) {
        system.debug('makeGstGenCreditLinkCallout ' + strGSTIN +' '+ strEmail +' '+ strMobile +' '+ strRecordId);
        if(String.isNotBlank(strGSTIN)) {
            String sObjectName = Id.valueOf(ParentRecordId).getSObjectType().getDescribe().getName();
            System.debug('Debug Log for sObjectName from makeGstGenCreditLinkCallout'+sObjectName);
            List<Rest_Service__mdt> lstRestService = [SELECT 
                                                            MasterLabel, 
                                                            QualifiedApiName, 
                                                            Service_EndPoint__c, 
                                                            Client_Password__c, 
                                                            Client_Username__c, 
                                                            Request_Method__c, 
                                                            Time_Out_Period__c,
                                                          org_id__c
                                                      FROM   
                                                            Rest_Service__mdt
                                                      WHERE  
                                                            DeveloperName = 'GST_4_GstGenCreditLink'
                                                            LIMIT 1
                                                     ]; 
            System.debug('Debug Log for lstRestService  GST_5_GstTransactionDetails ='+lstRestService);
            if(!lstRestService.isEmpty()) {
                GstGenCreditLinkResponseBody objResponseBody = new GstGenCreditLinkResponseBody();
                Blob headerValue = Blob.valueOf(lstRestService[0].Client_Username__c + ':' + lstRestService[0].Client_Password__c);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                Map<String,String> headerMap = new Map<String,String>();
                headerMap.put('Authorization',authorizationHeader);
                headerMap.put('Username',lstRestService[0].Client_Username__c);
                headerMap.put('Password',lstRestService[0].Client_Password__c);
                headerMap.put('Content-Type','application/json');
                headerMap.put('org-id',lstRestService[0].org_id__c);
                
                
                RequestBody objRequestBody = new RequestBody();
                    objRequestBody.gstin = strGSTIN != null ? strGSTIN : '';
                    objRequestBody.email = strEmail != null ? strEmail : '';
                    objRequestBody.mobile = strMobile != null ? strMobile : '';
                    objRequestBody.consent = 'Y';
                  String refid = System.Label.GST_Product_Id + '-' + strRecordId;
                  system.debug('refid ' + refid);
                    objRequestBody.refId = refid;  
                    
                System.debug('Debug Log for objRequestBody'+objRequestBody);
                String jsonRequest = Json.serializePretty(objRequestBody); 
                
                Http objHttp = new Http();
                HttpRequest objHttpRequest = new HttpRequest();
                
                
                if(String.isNotBlank(lstRestService[0].Service_EndPoint__c)) {
                    objHttpRequest.setEndPoint(lstRestService[0].Service_EndPoint__c);
                }
                else {
                    System.debug('Debug Log for error : Endpoint Empty. Please specify endpoint in metadata record.');
                }
                if(String.isNotBlank(lstRestService[0].Request_Method__c)) {
                    objHttpRequest.setMethod(lstRestService[0].Request_Method__c);
                }
                else {
                    System.debug('Debug Log for error : Requested Method Empty. Please specify Requested Method in metadata record.');
                }
                
                for(String headerKey : headerMap.keySet()){
                    objHttpRequest.setHeader(headerKey, headerMap.get(headerKey));
                    System.debug('Debug Log for headerKey'+headerKey);
                    System.debug('Debug Log for headerMap.get(headerKey)'+headerMap.get(headerKey));
                }
                objHttpRequest.setBody(jsonRequest);
                if(String.isNotBlank(String.valueOf(lstRestService[0].Time_Out_Period__c))) {
                    objHttpRequest.setTimeOut(Integer.valueOf(lstRestService[0].Time_Out_Period__c));
                }
                System.debug('Debug Log for request'+objHttpRequest);
                try {
                    HTTPResponse httpRes = objHttp.send(objHttpRequest);
                    //Create integration logs
                    //LogUtility.createIntegrationLogs(jsonRequest,httpRes,lstRestService[0].Service_EndPoint__c);
                    System.debug('Debug Log for response'+httpRes);
                    System.debug('Debug Log for response body'+httpRes.getBody());
                    System.debug('Debug Log for response status'+httpRes.getStatus());
                    System.debug('Debug Log for response status code'+httpRes.getStatusCode());
                    //Deserialization of response to obtain the result parameters
                    //objResponseBody = (ResponseBody)JSON.deserialize(httpRes.getBody(), ResponseBody.class);
                    objResponseBody = (GstGenCreditLinkResponseBody)Json.deserialize(httpRes.getBody(), GstGenCreditLinkResponseBody.class);
                    if(httpRes.getStatus() == 'OK' && httpRes.getStatusCode() == 200) {
                        if(objResponseBody != null) {
                            if(objResponseBody.statusCode == 101){
                            system.debug('objResponseBody ===' + objResponseBody);
                            if(sObjectName == Constants.strProjectBuilderAPI) {
                                Builder_Integrations__c objBI = [Select id,Project_Builder__c,System_Created__c from Builder_Integrations__c where id =: strRecordId LIMIT 1];
                                System.debug('GSTRecord Return Status ' + objBI);
                                System.debug('Result Array'+objResponseBody.result);
                                System.debug('Result Request Id'+objResponseBody.requestId);
                                System.debug('Result Status Code'+objResponseBody.statusCode);
                                System.enqueueJob(new EnqueDMLGSTINTwoAPF(objBI, null, jsonRequest,String.valueOf(httpRes),lstRestService[0].Service_EndPoint__c));
                            }
                            
                            else if(sObjectName == Constants.strPromoterAPI) {
                                Promoter_Integrations__c objPI = [Select id,Promoter__c,System_Created__c from Promoter_Integrations__c where id =: strRecordId LIMIT 1];
                                System.debug('GSTRecord Return Status ' + objPI);
                                System.debug('Result Array'+objResponseBody.result);
                                System.debug('Result Request Id'+objResponseBody.requestId);
                                System.debug('Result Status Code'+objResponseBody.statusCode);
                                System.enqueueJob(new EnqueDMLGSTINTwoAPF(null, objPI, jsonRequest,String.valueOf(httpRes),lstRestService[0].Service_EndPoint__c));
                            }
                            
                        }
                        }  
                        
                    }
                    
                }
                catch(Exception ex) {
                    System.debug('Debug Log for exception'+ex);
                }
           }
       }
    }
    public class RequestBody {
        public String gstin;    //02AAACR5055K1ZJ
        public String email;    //joshi.hitesh@gmail.com
        public String mobile;   //7208748564
        public String consent;  //Y
        public String refId;    //54135413
    }
}