@isTest
public class CRM_UtilityTest {
    static testMethod void testInsertStandardUser(){
        CRM_Utility.insertStandardUser();
    }
    
    static testMethod void testInsertRepaymentRestService(){
        CRM_Utility.insertRestService('repayment');
    }
    
    static testMethod void testInsertItcRestService(){
        CRM_Utility.insertRestService('itc');
    }
    
    static testMethod void testInsertLoanInfoRestService(){
        CRM_Utility.insertRestService('loaninformation');
    }
    
    static testMethod void testInsertloanListRestService(){
        CRM_Utility.insertRestService('loanlist');
    }
}