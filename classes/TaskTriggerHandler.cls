public class TaskTriggerHandler{
    public static void beforeUpdate(List<Task> newList){
        for(Task t: newList){
            if(t.Status == 'Completed'){
                t.OwnerId = t.CreatedById;
            }
        }
    }
    public static void afterUpdate(List<Task> newList){
        Set<Id> setTaskId = new Set<Id>();
        if(!newList.isEmpty()) {
            for(Task objTask : newList) {
                setTaskId.add(objTask.Id);
            }
        }
        system.debug('Inside after update');
        List<FeedItem> toInsert = new List<FeedItem>();
		List<Task> CompletedTaskLst = [Select Id, Status, OwnerId from Task where Status='Completed' and Id IN: newList]; //Added By Saumya for COde Optimization
        //List<FeedItem> item = new List<FeedItem>(); //Added By Saumya for COde Optimization
        if(!setTaskId.isEmpty() && !Test.isRunningTest()) {
            Utility.postToChatter(setTaskId, 'Task'); 
        }
        //Added By Saumya for COde Optimization
        //Added By Saumya for COde Optimization
        /*for(Task t: newList){
            system.debug('TASK'+t.Subject);
            List<FeedItem> item = new List<FeedItem>();
            if(t.Status == 'Completed'){
                system.debug('CHATTER');
                item = Utility.postToChatter(null,t.OwnerId, t.id, 'Task '+t.Subject+' has been completed and assigned back to you.');
                system.debug('LOAN APP:'+t.OwnerId);
            }
            toInsert.addAll(item);
        }*/
    }

    public static void afterInsert(List<Task> newList){
        List<Communication_Trigger_Point__mdt> ctpList = new List<Communication_Trigger_Point__mdt>();
        List<Task> taskList = new List<Task>();
        String template='';
        ctpList = [Select Id, SMS_Template__c, Stage_Value__c from Communication_Trigger_Point__mdt];
        taskList = [Select Id, WhatId, OwnerId, Status from Task where Id =: newList];
        Map<Id, String> ownerMap = new Map<Id, String>();
        Map<Id, String> loanMap = new Map<Id, String>();
        //Loan_Application__c la = [Select Id, Name, Loan_Number__c from Loan_Application__c where Id =: newList];
        for (Task t : newList){
            ownerMap.put(t.OwnerId, null);
            loanMap.put(t.WhatId, null);
        }
        if (ownerMap.size() > 0 && loanMap.size()>0){
            for (User[] users : [SELECT Id, MobilePhone FROM User WHERE Id IN :ownerMap.keySet()]){
                for (Integer i=0; i<users.size(); i++){
                    ownerMap.put(users[i].Id, users[i].MobilePhone);
                }
            }
            for (Loan_Application__c[] loans : [SELECT Id, Loan_Number__c FROM Loan_Application__c WHERE Id IN :loanMap.keySet()]){
                for (Integer i=0; i<loans.size(); i++){
                    loanMap.put(loans[i].Id, loans[i].Loan_Number__c);
                }
            }
			for(Third_Party_Verification__c[] tpv: [SELECT Id,Loan_Application__c,Loan_Application__r.Loan_Number__c FROM Third_Party_Verification__c WHERE Id IN: loanMap.keySet()]){
                for (Integer i=0; i<tpv.size(); i++){
                    loanMap.put(tpv[i].Id, tpv[i].Loan_Application__r.Loan_Number__c);
                }
            }
            for (Task t : newList){
                //t.OwnerName__c = ownerMap.get(lead.OwnerId);
                for(Communication_Trigger_Point__mdt ctp: ctpList){
                    system.debug('<<>>'+t.WhatId);
                    if(t.Status == 'Open' && ctp.Stage_Value__c == 'Task'){
                        template = ctp.SMS_Template__c;
                        template = template.replace('<App>', loanMap.get(t.WhatId));
                        SMSIntegration.SMSinit(ownerMap.get(t.OwnerId), template);
                    }
                
                }
            }
        }
    }
}