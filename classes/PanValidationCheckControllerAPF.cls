public class PanValidationCheckControllerAPF {
    /***********************************************************************************************
        Created By : Shobhit Saxena(Deloitte)
        Description : This class acts as an initiator for performing PAN Callout. A few checks are
        enabled to check whether the Project Builder/Promoter is eligible for PAN Validation or not.
    ***********************************************************************************************/
    @AuraEnabled
    public static String checkPanDetails(Id strRecordId)
    {
        List<Project_Builder__c> lstProjBuilder = new List<Project_Builder__c>();
        List<Promoter__c> lstPromoter = new List<Promoter__c>();
        String responseMsg = '';
        if(String.isNotBlank(strRecordId)) {
            String sObjName = strRecordId.getSObjectType().getDescribe().getName();
            System.debug('Debug Log for sObjName'+sObjName);
            
            if(sObjName == Constants.strProjectBuilderAPI) {
                lstProjBuilder = [Select Id, Name, Pan_Verification_status__c, PAN_Response__c, PAN_Number__c From Project_Builder__c Where Id =: strRecordId LIMIT 1];
                 system.debug('lstProjBuilder '+lstProjBuilder);
                if(!lstProjBuilder.isEmpty() && String.isNotBlank(lstProjBuilder[0].PAN_Number__c)) {
                    if(!lstProjBuilder.isEmpty() && lstProjBuilder[0].PAN_Response__c) {
                        responseMsg = 'PAN has been already hit for verification. Please try after some time.';  
                    }
                    else if(!lstProjBuilder.isEmpty() && lstProjBuilder[0].Pan_Verification_status__c){
                        responseMsg = 'PAN has been verified for this Builder! Are you sure you want to verify again?';
                    }
                    else{
                        responseMsg = 'OK';
                    }
                }
                else {
                    responseMsg = 'Please specify PAN before initiating PAN Verification.';
                }
            }
            else if(sObjName == Constants.strPromoterAPI) {
                lstPromoter = [Select Id, Name, Pan_Verification_status__c, PAN_Response__c, PAN_Number__c From Promoter__c Where Id =: strRecordId LIMIT 1];
                if(!lstPromoter.isEmpty() && String.isNotBlank(lstPromoter[0].PAN_Number__c)) {
                    if(!lstPromoter.isEmpty() && lstPromoter[0].PAN_Response__c) {
                        responseMsg = 'PAN has been already hit for verification. Please try after some time.';  
                    }
                    else if(!lstPromoter.isEmpty() && lstPromoter[0].Pan_Verification_status__c){
                        responseMsg = 'PAN has been verified for this Promoter! Are you sure you want to verify again?';
                    } 
                    else{
                        responseMsg = 'OK';
                    }   
                }
                else {
                    responseMsg = 'Please specify PAN before initiating PAN Verification.';
                }
            }
        }
        return responseMsg;
    }
}