public class MonthlySalesTurnoverTriggerHandler {
    public static void updateCustomerIntegration(List<id> newList){
        system.debug('newList ' + newList.size());
        if(!newList.isEmpty()){
            List<Customer_Integration__c> updateCustIntgList = new List<Customer_Integration__c>();
            List<Customer_Integration__c> ciRecords = [Select id,Business_Date_GSTIN__c,Cumulative_Sales_Turnover__c,Month_Count__c,(Select id,Month__c,Year__c,Turnover__c from Monthly_Sales_Turnover__r) from customer_integration__c where id in : newList];
            system.debug('ciRecords ' + ciRecords);
            if(!ciRecords.isEmpty()){
                for(Customer_Integration__c ciRecord : ciRecords){
                    system.debug('ciRecord.Monthly_Sales_Turnover__r.size() ' + ciRecord.Monthly_Sales_Turnover__r.size());
                    ciRecord.Month_Count__c = ciRecord.Monthly_Sales_Turnover__r.size();
                    
                    Date datobj = ciRecord.Business_Date_GSTIN__c;
                    Integer BusinessMonth = datobj.month();
                    Integer BusinessYear = datobj.year();
                    Decimal cumulativeSalesTurnover = 0.0;
                    for(Monthly_Sales_Turnover__c mst : ciRecord.Monthly_Sales_Turnover__r){
                        /*if(mst.Turnover__c != null){
                            if( BusinessMonth != null && BusinessYear != null && String.isNotBlank(mst.Year__c) && String.isNotBlank(mst.Month__c)){
                                system.debug('Debug log for Business Date ' + BusinessMonth + ' ' + BusinessYear);
                                system.debug('Debug log for MST Year and Month' + mst.Year__c + ' ' + mst.Month__c);
                                if((Integer.valueOf(mst.Year__c) == BusinessYear && Integer.valueOf(mst.Month__c) <= (BusinessMonth-2)) ||
                                   (Integer.valueOf(mst.Year__c) == (BusinessYear - 1) && Integer.valueOf(mst.Month__c) >= Integer.valueOf(System.label.Start_of_Financial_Year_Period))
                                  ){
                                      cumulativeSalesTurnover += mst.Turnover__c;
                                  }
                            }
                        }*/
                        
                        if(mst.Turnover__c != null){
                             cumulativeSalesTurnover += mst.Turnover__c;
                        }
                    }
                    ciRecord.Cumulative_Sales_Turnover__c = cumulativeSalesTurnover;
                    system.debug('cumulativeSalesTurnover '+ cumulativeSalesTurnover);
                    system.debug('Cumulative_Sales_Turnover__c ' + ciRecord.Cumulative_Sales_Turnover__c );
                    updateCustIntgList.add(ciRecord);
                }
                system.debug('updateCustIntgList ' + updateCustIntgList);
                if(!updateCustIntgList.isEmpty()){
                    Database.SaveResult[] results=  Database.update(updateCustIntgList, false);
                    system.debug('results' + results.size());
                }
            }  
        }
        
    }
}