@isTest
public class TestLMSDateController{
    public static testMethod void getRecords(){
        LMS_Date_Sync__c lmsObj = new LMS_Date_Sync__c();
        lmsObj.Current_Date__c = system.today();
        lmsObj.EOD_BOD__c = 'Test';
        Database.insert(lmsObj);
        
        LMSDateController.getRecords();
        LMSDateController.createNewRecord('Test Name',system.today(),'Test EOD');
    }
}