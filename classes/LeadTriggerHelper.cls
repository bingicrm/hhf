/*=====================================================================
 * Deloitte India
 * Name:LeadTriggerHelper
 * Description: This class is a helper class and supports Lead trigger handler.
 * Created Date: [13/06/2018]
 * Created By:Sonali Kansal(Deloitte India)
 *
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/
public class LeadTriggerHelper {
    
    public static void applyDefaultEmail(List<Lead> newList) {
        if(!newList.isEmpty()) {
            for(Lead objLead : newList) {
                if(String.isBlank(objLead.Email)) {
                    String strUniqueId = String.valueOf(Datetime.now().formatGMT('mmssSSS'));
                    objLead.Email = 'test'+strUniqueId+'@email.com';
                }
            }
        }
    }
    
    public static void validateCredentialsDigitalVendors(List<Lead> lstCriteriaList) {
        if(!lstCriteriaList.isEmpty()) {
            List<Digital_Lead_API_Metadata__mdt> lstDigLeadMtd = [Select Id, DeveloperName, MasterLabel, Source_Detail__c, User_Name__c, Vendor_Password__c From Digital_Lead_API_Metadata__mdt];
            Map<String, Digital_Lead_API_Metadata__mdt> mapLeadSourcetoMetadataRecord = new Map<String, Digital_Lead_API_Metadata__mdt>();
            if(!lstDigLeadMtd.isEmpty()) {
                for(Digital_Lead_API_Metadata__mdt objMetadata : lstDigLeadMtd) {
                    mapLeadSourcetoMetadataRecord.put(objMetadata.Source_Detail__c, objMetadata);
                }
            }
            System.debug('Debug Log for mapLeadSourcetoMetadataRecord'+mapLeadSourcetoMetadataRecord.size());
            for(Lead objLead : lstCriteriaList) {
                if(!mapLeadSourcetoMetadataRecord.isEmpty() && mapLeadSourcetoMetadataRecord.containsKey(objLead.source_detail__c) && String.isNotBlank(mapLeadSourcetoMetadataRecord.get(objLead.source_detail__c).User_Name__c) && String.isNotBlank(mapLeadSourcetoMetadataRecord.get(objLead.source_detail__c).Vendor_Password__c)) {
                    if(objLead.Vendor_Name__c != mapLeadSourcetoMetadataRecord.get(objLead.source_detail__c).User_Name__c) {
                        objLead.addError('Invalid Credentials : Wrong Username provided. Please enter correct Username.');
                    }
                    if(objLead.Vendor_Password__c != mapLeadSourcetoMetadataRecord.get(objLead.source_detail__c).Vendor_Password__c) {
                        objLead.addError('Invalid Credentials : Wrong Password provided. Please enter correct Password.');
                    }
                }
            }
        }
    }
    
    /*public static void FiFoAssignmentRule(set < string > setOfPINcodes, map < string, List < Lead >> leadPin) {
        try{
         system.debug('setOfPINCodes##' + setOfPINCodes);
        list<User_Pincode__c> UserUpdateList = new list<User_Pincode__c>();
        List < User_Pincode__c > userPin = new List < User_Pincode__c > ([SELECT User__c, Pincode__r.Name, Last_Assigned_Lead__c FROM User_Pincode__c
            WHERE Pincode__c in : setOfPINCodes and
            User_Role__c = 'Sales']);
            system.debug('userPin@@' + userPin);
      map < string, list < User_Pincode__c >> userPinMap = new map < string, list < User_Pincode__c >> ();
      
        for (User_Pincode__c up: userPin) {
            if (userPinMap.containsKey(up.pincode__r.Name)) {
                //user nw = new user(u);
                userPinMap.get(up.pincode__c).add(up);
            } else {
                list < User_Pincode__c > uslist = new list < User_Pincode__c > ();
                uslist.add(up);
                userPinMap.put(up.pincode__c, uslist);
            }
        }
        system.debug('userPinMap' + userPinMap);
     map < string, User_Pincode__c > userMap = new map < string, User_Pincode__c > ();
        List < lead > UpdationLead = new list < lead > ();
        list < User_Pincode__c > userList = new list < User_Pincode__c > ();*/
       
        /*for (string pin: setOfPINCodes) {

            for (lead ld: leadPin.get(pin)) {

               for (User_Pincode__c us: userPinMap.get(pin)) {
                    system.debug('#############################################');
                    if(userMap.isEmpty()){
                        userMap.put(pin, us);

                    }
                     if(us.Last_Assigned_Lead__c == null){

                        userMap.put(pin, us);
                    }
                    else if( ( (us.Last_Assigned_Lead__c < userMap.get(pin).Last_Assigned_Lead__c))){ 
                     userMap.put(pin, us);
                    
                    }
                }
              
                ld.ownerId = userMap.get(pin).User__c;
                
                UpdationLead.add(ld);
               for (User_Pincode__c us: userPinMap.get(pin)) {
                    if (us == userMap.get(pin)) {
                        
                        us.last_assigned_lead__c = system.now();
                        
                    }
                    userList.add(us);
                }
                //userPinMap.put(pin, userList);
                
            }
           for(User_Pincode__c us: userPinMap.get(pin)){
                
                UserUpdateList.add(us);
    
            }
        }
        system.debug('@@@ UserUpdateList' + UserUpdateList);
        update UserUpdateList;
    } 
     catch(exception e){
        System.debug('The following exception has occurred: ' + e.getMessage());
    } 
    

 }

    public static void checkCustomConvert(List<Lead> newList, Map<Id, Lead> oldMap) {
		String profileId = UserInfo.getProfileId(); //Added by Abhilekh on 23rd August 2019
        String profileName = [SELECT Id,Name from Profile WHERE Id =: profileId].Name; //Added by Abhilekh on 23rd August 2019
        Id idRTId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Digital_Direct').getRecordTypeId();
        Id idRTId2 = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Digital_Alliance').getRecordTypeId();
        if (trigger.size == 1 && 
            newList[0].Source_Detail__c != 'Inbound Call' && 
            
            (oldMap == null || ( oldMap != null && oldMap.get(newlist[0].id).Source_Detail__c != newList[0].Source_Detail__c) ) &&
            
            (newList[0].recordtypeid==idRTId || newList[0].recordtypeid==idRTId2 )
            ) {
            //newList[0].addError('Website/Alliance leads can only be created by Bulk Upload!');        
        }
        for(Lead objLead : newList) {
            system.debug('@@@ in checkcustom' + objLead.Status + '##' + objLead.Lead_Converted_Custom__c );
           
            if (trigger.isInsert && objLead.Status != 'New' ) {
                objLead.addError('Please mark the status as New while creating new lead.');
            } else if (trigger.isUpdate && oldMap.get(objLead.Id).Status == 'Rejected' && profileName != 'System Administrator') { //&& profileName != 'System Administrator' added by Abhilekh on 23rd August 2019
                objLead.AddError('You cannot modify a Rejected lead!');
            }
            if ( objLead.Status == 'Converted' && !objLead.Lead_Converted_Custom__c ) {
                system.debug('@@@ in checkcustom if');
                objLead.addError('Please select the Convert Lead button on top right for Lead conversion.');
            }
            
        }
    }
    
    public static void setConverted(List<Lead> newList) {
        Set<Id> setId = new Set<Id>{
        Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Connector').getRecordTypeId(),
        Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Hero Dealer DSA').getRecordTypeId(),
        Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Hero Employees').getRecordTypeId(),
        Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Hero Dealer').getRecordTypeId(),
        Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Open Market').getRecordTypeId(),
        Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Stategic Partner').getRecordTypeId()
        };
        List<User> lstUser = [SELECT Id, Name FROM User WHERE Id = :UserInfo.getUserId()];
        List<DSA_Master__c > lstDSA = [Select Id,Name,Manager__r.Name FROM DSA_Master__c where Name =:lstUser[0].Name];
        List<DST_Master__c> lstDST = [Select Id, Name, Manager__r.name from DST_Master__c where Name =:lstUser[0].Name];
        List<DST_SM_Master__c> lstDSASM ;
        if (!lstDSA.isEmpty()) {
            lstDSASM = [Select Id, Name from DST_SM_Master__c where Name =:lstDSA[0].Manager__r.Name];
        } else if (!lstDST.isEmpty()) {
            lstDSASM = [Select Id, Name from DST_SM_Master__c where Name =:lstDST[0].Manager__r.Name];
        }   
        
        system.debug('@@lstDSASM' + lstDSASM);
        for (Lead objLead : newList) {
            if ( setId.contains(objLead.RecordTypeId)) {
                system.debug('in set id if');
                objLead.Status = 'Nurturing';
                
                if ( !lstDSA.isEmpty() ) 
                    objLead.DSA_Name__c =  lstDSA[0].Id;
                if ( !lstDST.isEmpty() ) 
                    objLead.DST_Name__c =  lstDST[0].Id;    
                if ( lstDSASM != null && lstDSASM.size() > 0)
                    objLead.DST_SM_Name__c = lstDSASM[0].Id;
            }
        }
    }

    public static void validateNameDOB(List<Lead> newList, Map<Id,Lead> oldMap) {
        Set<String> setName = new Set<String>();
        Set<Date> setDOB = new Set<Date>();
        Set<String> setAccNameDOB = new Set<String>();
        Set<String> setLeadNameDOB = new Set<String>();
        String strNameDOB;
        Boolean boolValidate = true;
        for(Lead objLead : newList) {
            if ( trigger.isInsert || (trigger.isUpdate && ( oldMap.get(objLead.Id).FirstName != objLead.FirstName || oldMap.get(objLead.Id).LastName != objLead.LastName || oldMap.get(objLead.Id).Date_of_Birth__c != null && oldMap.get(objLead.Id).Date_of_Birth__c!= objLead.Date_of_Birth__c) )) {
                setName.add(objLead.FirstName + ' ' + objLead.LastName);
                setDOB.add(objLead.Date_of_Birth__c);
            } else {
                boolValidate = false;
            }
        }
        system.debug('setName@@' + setName);
        system.debug('setDOB@@' + setDOB);
        if (boolValidate) {
            List<Account> lstAccount = [Select Id, Name, PersonBirthdate,Date_of_Birth__c from Account where  Date_of_Birth__c IN :setDOB];
            List<Lead> lstLead = [Select Id, Name, Date_Of_Birth__c from Lead where Date_of_Birth__c IN :setDOB];
            
            for(Lead objLead : lstLead) {
                setLeadNameDOB.add(objLead.Name + objLead.Date_of_Birth__c);
            }
            for(Account objAcc : lstAccount) {
                setAccNameDOB.add(objAcc.Name + objAcc.Date_of_Birth__c);
            }
            system.debug('setAccNameDOB@@@' + setAccNameDOB);
            system.debug('setLeadNameDOB@@@' + setLeadNameDOB);
            for(Lead objLead : newList) {
                
                strNameDOB = objLead.FirstName + ' ' + objLead.LastName + objLead.Date_of_Birth__c;
                system.debug('strNameDOB@@@' + strNameDOB);
                if (setLeadNameDOB.contains(strNameDOB)) {
                    system.debug('in error');
                    objLead.addError('Duplicate Lead detected with this Name and Birthdate combination.');
                }
                if (setAccNameDOB.contains(strNameDOB)) {
                    objLead.addError('Duplicate Customer detected with this Name and Birthdate combination.');
                }
                
            }
        }
    }  
   public static void shareLeadsRoundRobin (List<Lead> newList, Map < Id, Lead > oldMap) {
        Id OwnerId;                                                                              
        Datetime lad = System.now();
        Datetime rad;
        List<Lead> lstLead = new List<Lead>();
        List<Lead_Owner_Mapping__c> leadOwnerList=[Select Branch__c,Line_Of_Business__c,User__c, User__r.Role_Assigned_Date__c from Lead_Owner_Mapping__c];
        System.debug('leadOwnerList:::'+leadOwnerList);
        for(Lead objLead : newList)  {            
            for(Lead_Owner_Mapping__c ub: leadOwnerList){
                if(ub.Branch__c == objLead.Branch__c){
                    
                    rad = ub.User__r.Role_Assigned_Date__c;
        			System.debug('ub.User__c:::'+ub.User__c);
                    if(rad == null){
                        OwnerId = ub.User__c;
                    }
                    else if(rad < lad){
                        lad = rad;
                        OwnerId = ub.User__c;
                    }
                    
                }
            }
            if(OwnerId != null)
            objLead.OwnerId = OwnerId;
            //lstLead.add(objLead);
        }
       
        
    }
	
	public static void shareLeadsRoundRobin (List<Lead> newList, Map < Id, Lead > oldMap, String branch) {
        Id OwnerId;
        Id lastassignedUser;
        Datetime lad = System.now();
        Datetime rad;
        List<Lead> lstLead = new List<Lead>();
        List<Lead_Owner_Mapping__c> leadOwnerList=[Select Branch__c,Line_Of_Business__c,User__c,User__r.Name, User__r.Role_Assigned_Date__c from Lead_Owner_Mapping__c where Branch__c =: branch];
        System.debug('leadOwnerList:::'+leadOwnerList);
		if(leadOwnerList.size() >= newList.size()){
        for(Lead objLead : newList)  {            
            for(Lead_Owner_Mapping__c ub: leadOwnerList){
                if(ub.Branch__c == objLead.Branch__c){
                    System.debug('lastassignedUser:::'+lastassignedUser);
                    System.debug('rad:::'+rad);
                    System.debug('lad:::'+lad);
                    rad = ub.User__r.Role_Assigned_Date__c;
                    
        			System.debug('ub.User__c:::'+ub.User__c+'ub.Branch__c:::'+ub.Branch__c+'objLead.Branch__c::'+objLead.Branch__c+'ub.User__r.name::::::'+ub.User__r.name);
                    if(rad == null){
                        OwnerId = ub.User__c;
                        lastassignedUser = ub.User__c;
                    }
                    else if(rad < lad){
                        lad = rad;
                        OwnerId = ub.User__c;
                        lastassignedUser = ub.User__c;
                    }
                    else if(rad == lad){
                        for(integer i= 0; i< leadOwnerList.size(); i++){
                            if(lastassignedUser == leadOwnerList[i].User__c){
                              	lad = rad;
                                if(i+1 == leadOwnerList.size()){
                        		OwnerId = leadOwnerList[0].User__c; 
                                lastassignedUser = leadOwnerList[0].User__c;   
                                }
                                else{
                                OwnerId = leadOwnerList[i+1].User__c;
                                lastassignedUser = leadOwnerList[i+1].User__c;       
                                }
                            }
                            else{
                              OwnerId = leadOwnerList[i].User__c;  
                            }
                        }
                        
                    }
                    
                }
            }
            System.debug('OwnerId::'+OwnerId);
            if(OwnerId != null)
            objLead.OwnerId = OwnerId;
            System.debug('objLead.OwnerId::'+objLead.OwnerId);
            //lstLead.add(objLead);
        }
		}
		else if(leadOwnerList.size() < newList.size()){
         
		for(integer i=0; i<newList.size(); i++){
                   
					if(leadOwnerList.size() >= (i+1)){
						OwnerId = leadOwnerList[i].User__c;
					}          
            System.debug('newList[i]'+newList[i]+'OwnerId::'+OwnerId);
            if(OwnerId != null)
            newList[i].OwnerId = OwnerId;
            System.debug('objLead.OwnerId::'+newList[i].OwnerId);
            //lstLead.add(objLead);
        
        }
		}
      
        
    }
    public static void updateOwnerDate(List<Lead> leadList){
        Set<Id> setOfUserIds = new Set<Id>();
        List<User> userList = new List<User>();
        List<User> updUsers = new List<User>();
        
        for(Lead objLead: leadList){
            setOfUserIds.add(objLead.OwnerId);
        }
        userList = [Select Id, Role_Assigned_Date__c from User where Id =: setOfUserIds];
        for(User u: userList){
            u.Role_Assigned_Date__c = System.now();
            updUsers.add(u);
        }
        if(updUsers.size()>0){
            update updUsers;
        }
    }*/
    
    
    public static void shareLeads (List<Lead> newList, Map < Id, Lead > oldMap) {
        LeadShare objLead = new LeadShare();
        List<LeadShare> lstLeadShare = new List<LeadShare>();
        Map<Id,Id> mapLeadtoManager = new Map<Id,Id>();
        Map<Id,Id> mapOwnerToLead = new Map<Id,Id>();
        Map<Id,Id> mapLeadtoCreateManager = new Map<Id,Id>();
        Map<Id,Id> mapCreateToLead = new Map<Id,Id>();
        Set<Id> setOwnerUserId = new Set<Id>();  //Added by Abhilekh on 8th Apr 2019
        Set<Id> setCreatedUserId = new Set<Id>(); //Added by Abhilekh on 8th Apr 2019                        
        
        for(Lead obj: newList){
            if (trigger.isinsert || (trigger.isupdate && obj.ownerid != oldmap.get(obj.id).ownerid) ) {
                mapOwnerToLead.put(obj.id, obj.Ownerid);  //Edited by Abhilekh on 8th Apr 2019
                mapCreateToLead.put(obj.id, obj.CreatedByid); //Edited by Abhilekh on 8th Apr 2019
                setOwnerUserId.add(obj.Ownerid); //Added by Abhilekh on 8th Apr 2019
                setCreatedUserId.add(obj.CreatedByid); //Added by Abhilekh on 8th Apr 2019                    
            }
        }
        
        if (!mapOwnerToLead.isEmpty()) {
            List<User> leadOwners = [Select Id, Managerid from User where id IN : setOwnerUserId ];  //Edited by Abhilekh on 8th Apr 2019
            List<User> leadCreators = [Select Id, Managerid from User where id IN : setCreatedUserId ];  //Edited by Abhilekh on 8th Apr 2019
            
            for(User obj: leadOwners){
                 mapLeadtoManager.put(obj.id, obj.Managerid );  //Edited by Abhilekh on 8th Apr 2019
            }
            
            for(User obj: leadCreators){
                 mapLeadtoCreateManager.put(obj.id, obj.Managerid );  //Edited by Abhilekh on 8th Apr 2019
            }
            
            for(Lead obj: newList){
                
                objLead = new LeadShare();
                 objLead.UserOrGroupId = mapLeadtoManager.get(mapOwnerToLead.get(obj.id));  //Edited by Abhilekh on 8th Apr 2019
                objLead.LeadAccessLevel = 'EDIT';
                objLead.RowCause = 'Manual';
                objLead.Leadid = obj.id;
                
                lstLeadShare.add(objLead);
                
                objLead = new LeadShare();
                objLead.UserOrGroupId = obj.CreatedByid;
                objLead.LeadAccessLevel = 'EDIT';
                objLead.RowCause = 'Manual';
                objLead.Leadid = obj.id;
                
                lstLeadShare.add(objLead);
                
                objLead = new LeadShare();
                objLead.UserOrGroupId = mapLeadtoCreateManager.get(obj.id);
                objLead.LeadAccessLevel = 'EDIT';
                objLead.RowCause = 'Manual';
                objLead.Leadid = obj.id;
                
                lstLeadShare.add(objLead);
        
            } 
            
            system.debug('@@lstLeadShare' + lstLeadShare);
            database.insert(lstLeadShare,false);
        }
    } 
}