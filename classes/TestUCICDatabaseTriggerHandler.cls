@isTest
public class TestUCICDatabaseTriggerHandler {
	public static testMethod void method1()
    {
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
        insert acc;
        
        
        //Contact con = new Contact(LastName = 'Test Contact', AccountId = acc.Id);
        //insert con;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        acc1 =[Select Id, PAN__c, Name, Date_of_Incorporation__c, Phone, RecordTypeId, Customer_ID__c From Account where Id =: acc1.Id];
        Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
        insert con1;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
         
        Loan_Application__c la = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11', Loan_Number__c ='12345');
        insert la;
         Loan_Contact__c lc = [SELECT Id, Name,Applicant_Type__c, Applicant_Status__c, Constitution__c, Customer_segment__c, Loan_Applications__c FROM Loan_Contact__c WHERE Loan_Applications__c =: la.Id];
        //lc.Applicant_Type__c='Co- Applicant';
        lc.Borrower__c='2';
        lc.Contact__c = con1.Id;
        lc.Customer__c = acc1.Id;
        lc.Constitution__c='18';
        lc.Applicant_Status__c='1';
        lc.Customer_segment__c='9';
        //lc.Income_Program_Type__c = 'Normal Salaried';
        //lc.Gender__c = 'M';
        //lc.Category__c ='1';
        //lc.Religion__c = '1';
        lc.Mobile__c='9876543210';
        lc.Email__c='testemail@gmail.com';
        //lc.Customer__c = acc.id;    
        update lc;
        Id RecordTypeIdCustUCIC = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('UCIC').getRecordTypeId();
        Customer_Integration__c objcust = new Customer_Integration__c();
			objcust.UCIC_Negative_API_Response__c = true;
			objcust.UCIC_Negative_API_Response__c = false;
			objcust.Loan_Application__c = la.Id;
			objcust.Loan_Contact__c = lc.Id;
			objcust.RecordTypeId = RecordTypeIdCustUCIC;
			insert objcust;
     
        Credit_Deviation_Master__c dev = new Credit_Deviation_Master__c();
        dev.Name = 'Test deviation';
        dev.Approver_Level__c = 'BH';
        dev.Status__c = TRUE;
        dev.UCIC_DB_Name__c ='UCIC Dedupe Check';
        insert dev;
        Credit_Deviation_Master__c dev2 = new Credit_Deviation_Master__c();
        dev2.Name = 'Test deviation';
        dev2.Approver_Level__c = 'BH';
        dev2.Status__c = TRUE;
        dev2.UCIC_DB_Name__c ='UNSC Sanctions List';
        insert dev2;
        Credit_Deviation_Master__c dev3 = new Credit_Deviation_Master__c();
        dev3.Name = 'Test deviation';
        dev3.Approver_Level__c = 'BH';
        dev3.Status__c = TRUE;
        dev3.UCIC_DB_Name__c ='NHB Wilful Defaulter';
        insert dev3;
        
                
		UCIC_Database__c objUC = new UCIC_Database__c();
        objUC.Match_Count__c = 2;
        objUC.Matched_Max_Percentage__c = '60';
        objUC.Name = 'UCIC Dedupe Check';
        objUC.Loan_Application__c = la.Id;
        objUC.Customer_Detail__c = lc.Id;
        objUC.Recommender_s_Decision__c = 'Approve';

        insert objUC;   
        objUC.Recommender_s_Decision__c = '';
        update objUC;
        
       /* Applicable_Deviation__c appDev = new Applicable_Deviation__c();
        appDev.Credit_Deviation_Master__c = dev.Id;
        appDev.Loan_Application__c = la.Id;
        appDev.Approved__c = true;
        appDev.UCIC_DB_Name__c= 'UCIC Dedupe Check';
		appDev.Auto_Deviation_UCIC__c= true;
        insert appDev;*/
        Applicable_Deviation__c appDev1 = new Applicable_Deviation__c();
        appDev1.Credit_Deviation_Master__c = dev.Id;
        appDev1.Loan_Application__c = la.Id;
        appDev1.Approved__c = true;
        appDev1.UCIC_DB_Name__c= 'NHB Wilful Defaulter';
		appDev1.Auto_Deviation_UCIC__c= true;
        insert appDev1;
        
        RunDeleteTriggerOnApplicableDeviation__c cs = new RunDeleteTriggerOnApplicableDeviation__c();
             cs.Name='RunDeleteTriggerOnApplicableDeviation';
             cs.byPassTrigger__c=false;
             insert cs;
        la.Overall_UCIC_Decision__c = 'Approve';
        update la;

 
        
        UCIC_Database__c objUC1 = new UCIC_Database__c();
        objUC1.Match_Count__c = 2;
        objUC1.Matched_Max_Percentage__c = '60';
        objUC1.Name = 'UNSC Sanctions List';
        objUC1.Loan_Application__c = la.Id;
        objUC1.Customer_Detail__c = lc.Id;														
        insert objUC1;
        
        objUC1.Match_Count__c = 3;
        objUC1.Recommender_s_Decision__c = 'Approve';
        objUC1.Approver_s_Decision__c = 'Approve';
        update objUC1;
        
      
       
    } 
    
}