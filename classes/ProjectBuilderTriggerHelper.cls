public class ProjectBuilderTriggerHelper {
    public static void populateProjectCategoryonProject(Set<id> setProjectIds) {
        List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
        if(!setProjectIds.isEmpty()) {
            System.debug('Debug Log for setProjectIds'+setProjectIds);
            List<Project__c> lstProject = [Select Id, Name, Project_Category__c,(Select Id, Name, Builder_Category__c, Builder_Role__c From Project_Builders__r Where Builder_Role__c =: Constants.strPBConstructorMarketer) From Project__c WHERE ID IN: setProjectIds];
            if(!lstProject.isEmpty()) {
                System.debug('Debug Log for lstProject'+lstProject);
                for(Project__c objProject : lstProject) {
                    if(!objProject.Project_Builders__r.isEmpty()) {
                        if(objProject.Project_Builders__r.size() == 1) {
                            objProject.Project_Category__c = objProject.Project_Builders__r[0].Builder_Category__c;
                        }
                        else {
                            Integer countCatABuilderCategory = 0;
                            Integer countCatBBuilderCategory = 0;
                            Integer countCatCBuilderCategory = 0;
                            for(Project_Builder__c objProjectBuilder : objProject.Project_Builders__r) {
                                if(objProjectBuilder.Builder_Category__c == Constants.strPBCategoryCatA) {
                                    countCatABuilderCategory++;
                                }
                                else if(objProjectBuilder.Builder_Category__c == Constants.strPBCategoryCatB) {
                                    countCatBBuilderCategory++;
                                }
                                else if(objProjectBuilder.Builder_Category__c == Constants.strPBCategoryCatC) {
                                    countCatCBuilderCategory++;
                                }
                            }
                            System.debug('Debug Log for countCatABuilderCategory'+countCatABuilderCategory);
                            System.debug('Debug Log for countCatBBuilderCategory'+countCatBBuilderCategory);
                            System.debug('Debug Log for countCatCBuilderCategory'+countCatCBuilderCategory);
                            
                            if(countCatCBuilderCategory > 0) {
                                objProject.Project_Category__c = Constants.strPBCategoryCatC;
                            }
                            else if(countCatCBuilderCategory == 0 && countCatBBuilderCategory > 0) {
                                objProject.Project_Category__c = Constants.strPBCategoryCatB;
                            }
                            else if(countCatCBuilderCategory == 0 && countCatBBuilderCategory == 0 && countCatABuilderCategory > 0){
                                objProject.Project_Category__c = Constants.strPBCategoryCatA;
                            }
                        }
                    }
                }
                DataBase.SaveResult[] lstDSR = Database.update(lstProject, false);
                for(Database.SaveResult objDSR : lstDSR) {
                    if (objDSR.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully inserted record with Id ' + objDSR.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : objDSR.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Fields that affected this error: ' + err.getFields());
                            Error_Log__c objErrorLog = new Error_Log__c(Error_Code__c = String.valueOf(err.getStatusCode()),Type__c = 'Builder Role Update', Error_Message__c = err.getMessage(), URL__c = 'APF');
                                
                            lstErrorLog.add(objErrorLog);
                        }
                    }
                }
                
                if(!lstErrorLog.isEmpty()) {
                    try {
                        insert lstErrorLog;
                    }
                    catch(Exception ex) {
                        System.debug('Exception occured'+ex);
                    }
                }
            }
        }
    }
}