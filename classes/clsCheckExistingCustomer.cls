public class clsCheckExistingCustomer {
	public static String existingCustomerCheck(Loan_Contact__c objLoanContact) {
		Integer existingLoanCounter = 0;
		if(objLoanContact != null) {
			if(String.isNotBlank(objLoanContact.Customer__c)) {
				if(objLoanContact.Customer__r.LMS_Existing_Customer__c) {
					System.debug('REturning Y for'+objLoanContact.Name);
					return 'Y';
				}
				List<Account> lstCustomerInfo = [SELECT
														Id,
														Name,
														(SELECT
																Id,
																Name,
																Loan_Number__c,
																Loan_Application_Number__c
														 FROM
														 		Loan_Applications__r
														)
												 FROM
												 		Account
										 		 WHERE
										 		 		Id =: objLoanContact.Customer__c
								 		 				LIMIT 1
												];
				System.debug('Debug Log for lstCustomerInfo'+lstCustomerInfo);
				if(!lstCustomerInfo.isEmpty()) {
					System.debug('Debug Log for lstCustomerInfo[0].Loan_Applications__r.size()'+lstCustomerInfo[0].Loan_Applications__r.size());
					if(lstCustomerInfo[0].Loan_Applications__r.size() == 0) {
						return 'N';
					}
					else {
						for(Loan_Application__c objLoanApplication : lstCustomerInfo[0].Loan_Applications__r) {
							if(String.isNotBlank(objLoanApplication.Loan_Application_Number__c)) {
								existingLoanCounter++;
							}
						}
					}
				}
				System.debug('Debug Log for existingLoanCounter'+existingLoanCounter);
				if(existingLoanCounter >=1) {
					return 'Y';
				}
				else {
					return 'N';
				}
			}
		}
		
		return '';
	}
}