public class CreditAuthorityUtility 
{
     public static String findCreditAuthority( String loanApplicationId )
    {
        List<Loan_Application__c> allLoanApplication =  [Select Id,Owner_Manager__c,Sub_Stage__c,Assigned_Credit_Review__c,Credit_Manager_User__c,Central_Salaried__c,Requested_Amount__c,Approved_Loan_Amount__c,Approved_cross_sell_amount__c,Group_Exposure_Amount__c,Branch__c,Line_Of_Business__c,(Select Credit_Deviation_Master__c,Credit_Deviation_Master__r.Approver_Level__c, Credit_Deviation_Master__r.Deviation_Level__c/** Added by Saumya for CO **/ From Applicable_Deviations__r) From Loan_Application__c  where id = :loanApplicationId]; // Added Sub_Stage__c, Assigned_Credit_Review__c by Saumya For Abhilekh related Changes
        Map<String,Decimal> mapRankToAmount = new Map<String,Decimal>();
        Set<String> allApproversRoles = new Set<String>();
        String userId;
        String highestRole;
        Integer highestIndex = 0;
        Integer index = 0; 
        String selectedUser ;
        Boolean boolAmtLessThanBH = false;
        
        System.debug('Approved Loan Amount & Approved Cross Sell & Group Exp Amount'+allLoanApplication[0].Approved_Loan_Amount__c+' & '+allLoanApplication[0].Approved_cross_sell_amount__c+' & '+allLoanApplication[0].Group_Exposure_Amount__c);
        /** Added by Saumya for CO **/
        List<Hierarchy__c> lstHierarchy = [Select Id, Name, Active__c, BCM__c, BH__c, CCM__c, CRO__c, NCM__c, ZCM__c, ZCM_Salaried__c from Hierarchy__c where Active__c = true];
        Map<String, Hierarchy__c> mapOfHierarchy = new Map<String, Hierarchy__c>();
        for(Hierarchy__c obj:lstHierarchy){
            mapOfHierarchy.put(obj.Name, obj);
        }
            system.debug('mapOfHierarchy:::'+mapOfHierarchy);
        List<User> lstUser = [SELECT Id, Name, Hierarchy__c, Role_in_Credit_Hierarchy__c, Salaried_Authority__c, Non_Salaried_Authority__c,RoleOfUser__c from User];
        user CreditUser;
        Map<Id, User> MapOfUser = new Map<Id, User>();
        for(User objUser: lstUser){
            MapOfUser.put(objUser.Id, objUser);
            if(objUser.Id == allLoanApplication[0].Credit_Manager_User__c){
                CreditUser = objUser;
            }
        }
        Authority_Master__mdt Authority;
        Map<String,Authority_Master__mdt> mapOfAuthority= New Map<String,Authority_Master__mdt>();
        List<Authority_Master__mdt> lstAuthority = [SELECT Id,Active__c, Deviation_Level__c, Group_Exposure_Amount__c, Loan_Amount__c, DeveloperName from Authority_Master__mdt where Active__c = true];
        for(Authority_Master__mdt objAuth: lstAuthority)
        {
            mapOfAuthority.put(objAuth.DeveloperName, objAuth);
        }
        if(allLoanApplication[0].Central_Salaried__c){
            Authority =  mapOfAuthority.get(CreditUser.Salaried_Authority__c);
        }
        else{
            if(CreditUser != null)
            Authority =  mapOfAuthority.get(CreditUser.Non_Salaried_Authority__c);
        }
        System.debug('Authority::'+Authority);
        /** Added by Saumya for CO **/
        Set<Integer> allApproversLevel = new Set<Integer>();
        Decimal HighestDeviationLevel;
        Map<String,String> RoleVsSDFieldMap = new Map<String,String>{'CO'=>'BCM__c','BCM'=>'CCM__c','CCM'=>'ZCM__c','ZCM'=>'NCM__c','ZCM_Salaried'=>'NCM__c','NCM'=>'CRO__c','CRO'=>'BH__c'};
            User HighestUser = CreditUser;
            system.debug('HighestUser Val:::'+HighestUser);
        if( allLoanApplication.size() > 0 )
        {
            for( Applicable_Deviation__c deviation : allLoanApplication[0].Applicable_Deviations__r )
            {
                if(HighestDeviationLevel == 0 || HighestDeviationLevel == null){
                    HighestDeviationLevel= deviation.Credit_Deviation_Master__r.Deviation_Level__c;
                }
                else if(HighestDeviationLevel < deviation.Credit_Deviation_Master__r.Deviation_Level__c){
                    HighestDeviationLevel=deviation.Credit_Deviation_Master__r.Deviation_Level__c;
                }
            }
            system.debug('HighestDeviationLevel:::'+HighestDeviationLevel);
            system.debug('CreditUser:::'+CreditUser);
            system.debug('Branch__c:::'+allLoanApplication[0].Branch__c);
            List<Branch_Wise_User_Hierarchy_Mapping__c> objBranchHierarchy = new List<Branch_Wise_User_Hierarchy_Mapping__c> ();
            if(CreditUser != null && allLoanApplication[0].Branch__c != null)
            objBranchHierarchy= [Select Id,Branch__c, User__c, Hierachy__c from Branch_Wise_User_Hierarchy_Mapping__c where User__c =: CreditUser.Id and Branch__c =: allLoanApplication[0].Branch__c];
            Hierarchy__c objHierarchy;
            if(objBranchHierarchy.size() > 0){
            objHierarchy = mapOfHierarchy.get(objBranchHierarchy[0].Hierachy__c);
            }
            else{
            if(HighestUser != null)
            objHierarchy = mapOfHierarchy.get(HighestUser.Hierarchy__c);
            }
            system.debug('objHierarchy:::'+objHierarchy);
            //Id NextUserId;
            if(Authority == null || (Authority.Loan_Amount__c < ((allLoanApplication[0].Approved_Loan_Amount__c != null ?allLoanApplication[0].Approved_Loan_Amount__c : 0) + (allLoanApplication[0].Approved_cross_sell_amount__c != null ? allLoanApplication[0].Approved_cross_sell_amount__c : 0 )))){
                String previousRole;
                String RoleInHierarchy;
                while(Authority == null || (Authority.Loan_Amount__c < ((allLoanApplication[0].Approved_Loan_Amount__c != null ?allLoanApplication[0].Approved_Loan_Amount__c : 0) + (allLoanApplication[0].Approved_cross_sell_amount__c != null ? allLoanApplication[0].Approved_cross_sell_amount__c : 0 )))){
                    //if(HighestUser != null && RoleVsSDFieldMap.get(HighestUser.Role_in_Credit_Hierarchy__c) != null){
                    if(HighestUser != null){
                    //previousRole = RoleVsSDFieldMap.get(HighestUser.Role_in_Credit_Hierarchy__c).split('__c')[0];
                    //RoleInHierarchy = HighestUser.Role_in_Credit_Hierarchy__c;
                    previousRole = HighestUser.Role_in_Credit_Hierarchy__c;
                    system.debug('previousRole1::'+previousRole);
                    }
                    else{
                    if(previousRole != null)
                    previousRole = RoleVsSDFieldMap.get(previousRole).split('__c')[0];
                    RoleInHierarchy = previousRole;
                    system.debug('previousRole2::'+previousRole);
                    }
                    system.debug('previousRole::'+previousRole);
                    //system.debug('RoleVsSDFieldMap.get(HighestUser.Role_in_Credit_Hierarchy__c)::'+RoleVsSDFieldMap.get(HighestUser.Role_in_Credit_Hierarchy__c));
					if(previousRole !=null && RoleVsSDFieldMap.get(previousRole) == 'ZCM' && allLoanApplication[0].Central_Salaried__c){
                    HighestUser = MapOfUser.get((Id)objHierarchy.get('ZCM_Salaried__c'));
					}
					else{
                        if(previousRole == 'BH'){
                        HighestUser= null; 
                        break;
                        }
                        else{
                            if(previousRole != null)
                            HighestUser = MapOfUser.get((Id)objHierarchy.get(RoleVsSDFieldMap.get(previousRole)));
                            
                        }
					}
                    system.debug('HighestUser::'+HighestUser);
                    
                    
                    if( HighestUser == null){
                        if(previousRole == 'CCM' && allLoanApplication[0].Central_Salaried__c ){
                            HighestUser = MapOfUser.get((Id)objHierarchy.get('ZCM_Salaried__c'));
                        }
                        else{  
                        if(previousRole == 'BH'){
                        HighestUser= null; 
                        break;
                        }
                        else{ 
                        HighestUser = MapOfUser.get((Id)objHierarchy.get(RoleVsSDFieldMap.get(previousRole)));
                        }
                        }
                    }
                    
                    if(allLoanApplication[0].Central_Salaried__c){
                        if(HighestUser != null && HighestUser.Salaried_Authority__c != null)
                        Authority =  mapOfAuthority.get(HighestUser.Salaried_Authority__c);
                    }
                    else{
                        if(HighestUser != null && HighestUser.Non_Salaried_Authority__c != null)
                        Authority =  mapOfAuthority.get(HighestUser.Non_Salaried_Authority__c);
                    }
                    //HighestUser = objHierarchy.get(RoleVsSDFieldMap.get(HighestUser.Role_in_Credit_Hierarchy__c));
                	/*}
                    else{
                        HighestUser = null;
                        break;
                    }*/
                }
                
            }
            system.debug('Authority 1::'+ Authority);
            system.debug('HighestUser 1::'+ HighestUser);
            if(Authority == null || (Authority.Group_Exposure_Amount__c < allLoanApplication[0].Group_Exposure_Amount__c)){
               String previousRole;
                String RoleInHierarchy;
                while(Authority == null || (Authority.Group_Exposure_Amount__c < allLoanApplication[0].Group_Exposure_Amount__c)){
                    //if(HighestUser != null && RoleVsSDFieldMap.get(HighestUser.Role_in_Credit_Hierarchy__c) != null){
                    if(HighestUser != null){
                    //previousRole = RoleVsSDFieldMap.get(HighestUser.Role_in_Credit_Hierarchy__c).split('__c')[0];
                    //RoleInHierarchy = HighestUser.Role_in_Credit_Hierarchy__c;
                    previousRole = HighestUser.Role_in_Credit_Hierarchy__c;
                    }
                    else{
                    previousRole = RoleVsSDFieldMap.get(previousRole).split('__c')[0];
                    RoleInHierarchy = previousRole;
                    }
                    system.debug('previousRole::'+previousRole);
                    //system.debug('RoleVsSDFieldMap.get(HighestUser.Role_in_Credit_Hierarchy__c)::'+RoleVsSDFieldMap.get(HighestUser.Role_in_Credit_Hierarchy__c));
					if(RoleVsSDFieldMap.get(previousRole) == 'ZCM' && allLoanApplication[0].Central_Salaried__c){
                    HighestUser = MapOfUser.get((Id)objHierarchy.get('ZCM_Salaried__c'));
					}
					else{
                        if(previousRole == 'BH'){
                        HighestUser= null; 
                        break;
                        }
                        else{
                            HighestUser = MapOfUser.get((Id)objHierarchy.get(RoleVsSDFieldMap.get(previousRole)));
                        }
					}
                    system.debug('HighestUser::'+HighestUser);
                    
                    
                    if( HighestUser == null){
                        if(previousRole == 'CCM' && allLoanApplication[0].Central_Salaried__c ){
                            HighestUser = MapOfUser.get((Id)objHierarchy.get('ZCM_Salaried__c'));
                        }
                        else{  
                        if(previousRole == 'BH'){
                        HighestUser= null; 
                        break;
                        }
                        else{    
                        HighestUser = MapOfUser.get((Id)objHierarchy.get(RoleVsSDFieldMap.get(previousRole)));
                        }
                        }
                    }
                    
                    if(allLoanApplication[0].Central_Salaried__c){
                        if(HighestUser != null && HighestUser.Salaried_Authority__c != null)
                        Authority =  mapOfAuthority.get(HighestUser.Salaried_Authority__c);
                    }
                    else{
                        if(HighestUser != null && HighestUser.Non_Salaried_Authority__c != null)
                        Authority =  mapOfAuthority.get(HighestUser.Non_Salaried_Authority__c);
                    }
                    //HighestUser = objHierarchy.get(RoleVsSDFieldMap.get(HighestUser.Role_in_Credit_Hierarchy__c));
                	/*}
                    else{
                        HighestUser = null;
                        break;
                    }*/
                }
                
            }
            system.debug('Authority 2::'+ Authority);
            system.debug('HighestUser 2::'+ HighestUser);
            system.debug('Authority.Deviation_Level__c:::'+ Authority.Deviation_Level__c);
            if(Authority == null || (Authority.Deviation_Level__c < HighestDeviationLevel)){
               String previousRole;
                String RoleInHierarchy;
                while(Authority == null || (Authority.Deviation_Level__c < HighestDeviationLevel)){
                    //if(HighestUser != null && RoleVsSDFieldMap.get(HighestUser.Role_in_Credit_Hierarchy__c) != null){
                    if(HighestUser != null){
                    //previousRole = RoleVsSDFieldMap.get(HighestUser.Role_in_Credit_Hierarchy__c).split('__c')[0];
                    //RoleInHierarchy = HighestUser.Role_in_Credit_Hierarchy__c;
                    previousRole = HighestUser.Role_in_Credit_Hierarchy__c;
                    system.debug('previousRole1::'+previousRole);
                    }
                    else{
                    previousRole = RoleVsSDFieldMap.get(previousRole).split('__c')[0];
                    RoleInHierarchy = previousRole;
                    system.debug('previousRole2::'+previousRole);
                    }
                    system.debug('previousRole::'+previousRole);
                    system.debug('RoleVsSDFieldMap.get(previousRole)::'+RoleVsSDFieldMap.get(previousRole));
                    //system.debug('RoleVsSDFieldMap.get(HighestUser.Role_in_Credit_Hierarchy__c)::'+RoleVsSDFieldMap.get(HighestUser.Role_in_Credit_Hierarchy__c));
					if(RoleVsSDFieldMap.get(previousRole) == 'ZCM' && allLoanApplication[0].Central_Salaried__c){
                    HighestUser = MapOfUser.get((Id)objHierarchy.get('ZCM_Salaried__c'));
					}
					else{
                        if(previousRole == 'BH'){
                        HighestUser= null; 
                        break;
                        }
                        else{
                            HighestUser = MapOfUser.get((Id)objHierarchy.get(RoleVsSDFieldMap.get(previousRole)));
                        }
					}
                    system.debug('HighestUser::'+HighestUser);
                    
                    
                    if( HighestUser == null){
                        if(previousRole == 'CCM' && allLoanApplication[0].Central_Salaried__c ){
                            HighestUser = MapOfUser.get((Id)objHierarchy.get('ZCM_Salaried__c'));
                        }
                        else{  
                        if(previousRole == 'BH'){
                        HighestUser= null; 
                        break;
                        }
                        else{    
                        HighestUser = MapOfUser.get((Id)objHierarchy.get(RoleVsSDFieldMap.get(previousRole)));
                        }
                        }
                    }
                    
                    if(allLoanApplication[0].Central_Salaried__c){
                        if(HighestUser != null)
                        Authority =  mapOfAuthority.get(HighestUser.Salaried_Authority__c);
                    }
                    else{
                        if(HighestUser != null)
                        Authority =  mapOfAuthority.get(HighestUser.Non_Salaried_Authority__c);
                    }
                    //HighestUser = objHierarchy.get(RoleVsSDFieldMap.get(HighestUser.Role_in_Credit_Hierarchy__c));
                	/*}
                    else{
                        HighestUser = null;
                        break;
                    }*/
                }
                
            }
            
        }
        
        system.debug('Authority 3::'+ Authority);
        system.debug('HighestUser 3::'+ HighestUser);
        if(HighestUser != null)
        selectedUser = String.ValueOf(HighestUser.Id);
        
        system.debug('HighestUser:: In End'+ HighestUser);
        system.debug('selectedUser:: In End'+ selectedUser);
        return selectedUser;
    }
}