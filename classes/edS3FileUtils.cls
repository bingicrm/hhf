/* **************************************************************************
*
* Class: edS3FileUtils
* Created by Anil Meghnathi: 14/09/2019
*
* - This is utility class to set value for file fields.

* - Modifications:
* - Anil Meghnathi, 14/09/2019 – Initial Development
************************************************************************** */
public class edS3FileUtils {
    /*
    *   Purpose:    This method is used to set file field values from document check list
    *   Parameters: 
    *   UnitTests:  
    */
    public static void setFileFieldsFromDocumentCheckList(NEILON__File__c file, Document_Checklist__c documentCheckList, ContentVersion contentVersion){
        
        // Set description
        file.NEILON__Description__c = String.isNotBlank(contentVersion.Title) ? contentVersion.Title : '';
        
        // Set export attachment id
        file.NEILON__Export_Attachment_Id__c = contentVersion.ContentDocumentId;
        
        // Set other fields from document checklist
        if(documentCheckList != null){
            // Set document checklist
            file.Document_Checklist__c = documentCheckList.Id;
            
            // Set document master
            file.Document_Master__c = documentCheckList.Document_Master__c;
            
            // Set document type
            file.Document_Type__c = documentCheckList.Document_Type__c;
            
            // Set customer type and loan contact
            if(String.isNotBlank(documentCheckList.Loan_Contact__c) && String.isNotBlank(documentCheckList.Loan_Contact__r.Applicant_Type__c)) {
                file.Loan_Contact__c = documentCheckList.Loan_Contact__c;
                if(documentCheckList.Loan_Contact__r.Applicant_Type__c == 'Applicant') {
                    file.Customer_Type__c = 'Applicant'; 
                } else if(documentCheckList.Loan_Contact__r.Applicant_Type__c == Constants.COAPP) {
                    file.Customer_Type__c = 'Co-Applicant'; 
                } else if(documentCheckList.Customer_Type__c == Constants.Guarantor_RECORD) {
                    file.Customer_Type__c = 'Guarantor';
                }
            }
        }
        
        // Return        
        return;
    }
}