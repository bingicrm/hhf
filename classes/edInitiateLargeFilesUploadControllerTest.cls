/* **************************************************************************
*
* Test Class: edInitiateLargeFilesUploadControllerTest
* Created by Anil Meghnathi
*
* - Test class for edInitiateLargeFilesUploadController.

* - Modifications:
* - Anil Meghnathi – Initial Development
************************************************************************** */
@isTest
public class edInitiateLargeFilesUploadControllerTest {
    @testSetup static void insertTestData(){
        // Create buckets
        NEILON__Folder__c bucket = s3LinkTestUtils.createFoldersForBucket('testbucket');
        
        // Create other user
        Profile p = [Select Id From Profile Where Name = 'Standard User'];
        User testUser = new User(alias='test', email='test@test.com', 
                    emailencodingkey='UTF-8', firstname = 'other', lastname='testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid=p.id, 
                    timezonesidkey='America/Los_Angeles', username='134323543@6543.com');
        insert testUser;
    }
    
    static testMethod void testEligibility(){
        // Start test
        Test.startTest();
        
        // Get loan application 
        Loan_Application__c loanApplication = createLoanApplicationData();
        
        // Start test
        Test.stopTest();
        
        // Set URL parameter
        ApexPages.currentPage().getParameters().put('Id', loanApplication.Id);
        
        // Get other user
        User otherUser = [Select Id From USer Where UserName  = '134323543@6543.com'];
        
        // Login as user other then System admin
        System.runAs(otherUser){
        
            // Contructor
            edInitiateLargeFilesUploadController con = new edInitiateLargeFilesUploadController();
            
            // Call init
            con.init();
            
            // Check load success flag
            System.assertEquals(con.isLoadSuccess, false);
        }
        
        // Validate status
        loanApplication = [Select Id, Name, StageName__c, Sub_Stage__c,
                                Date_of_Cancellation__c, Days_Since_Loan_Cancelled__c, 
                                Date_of_Rejection__c, Days_Since_Loan_Rejected__c,
                                Sanctioned_Disbursement_Type__c, Days_Since_Loan_Disbursed__c
                                From Loan_Application__c Where Id =: loanApplication.Id];
        Boolean isValid = edInitiateLargeFilesUploadController.validateLoanApplication(loanApplication);
        System.assertEquals(isValid, false);
    }
    
    static testMethod void testInit(){
        // Start test
        Test.startTest();
        
        // Get loan application 
        Loan_Application__c loanApplication = createLoanApplicationData();
        
        // Start test
        Test.stopTest();
        
        // Set URL parameter
        ApexPages.currentPage().getParameters().put('Id', loanApplication.Id);
        
        // Contructor
        edInitiateLargeFilesUploadController con = new edInitiateLargeFilesUploadController();
        
        // Call init
        con.init();
        
        // Check load success flag
        System.assertEquals(con.isLoadSuccess, true);
        
        // Check folders are created
        List<NEILON__Folder__c> folders = [Select Id, Name From NEILON__Folder__c Where Loan_Application__c =: loanApplication.Id];
        
        // Check size
        System.assertEquals(folders.size(), 3);
        
        // Check count for files for each folder
        List<edInitiateLargeFilesUploadController.FolderInfo> folderInfos = (List<edInitiateLargeFilesUploadController.FolderInfo>)JSON.deserialize(con.foldersInformation, List<edInitiateLargeFilesUploadController.FolderInfo>.class); 
    
        // Check document counts
        for(edInitiateLargeFilesUploadController.FolderInfo folderInfo : folderInfos){
            if(folderInfo.name.indexOf('Notes') != -1){
                System.assertEquals(folderInfo.count, 2);
            } else if(folderInfo.name.indexOf('Applicant') != -1){
                System.assertEquals(folderInfo.count, 3);
            }
        }
    }
    
    public static Loan_Application__c createLoanApplicationData(){
        // Create person account
        List<Account> personAccs = new List<Account>();
        Account personAcc1 = new Account(FirstName = 'Test', LastName = 'ABC');
        personAcc1.Phone = '9999999999';
        personAccs.add(personAcc1);
        Account personAcc2 = new Account(FirstName = 'Test', LastName = 'ABC');
        personAcc2.Phone = '9999999999';
        personAccs.add(personAcc2);
        insert personAccs;
        
        // Create document master
        Document_Master__c documentMaster = new Document_Master__c(Name = 'Test Document Master');
        insert documentMaster;
        
        // Create document type
        Document_Type__c documentType = new Document_Type__c(Name = Constants.PROPERTYPAPERS);
        insert documentType;
        
        // Create scheme
        Scheme__c scheme = new Scheme__c(Name = 'Test Scheme');
        scheme.Scheme_Group_ID__c = 'Test';
        scheme.Product_Code__c = 'HL';
        scheme.Scheme_Code__c = 'Test';
        insert scheme;
        
        // Create loan application
        Loan_Application__c loanApplication = new Loan_Application__c();
        loanApplication.Scheme__c = scheme.id;
        insert loanApplication;
        
        // Create customer detail
        Loan_Contact__c loanContact1 = new Loan_Contact__c(Customer__c = personAcc1.Id);
        loanContact1.Loan_Applications__c = loanApplication.Id;
        loanContact1.Applicant_Type__c = 'Applicant';
        insert loanContact1;
        
        Loan_Contact__c loanContact2 = new Loan_Contact__c(Customer__c = personAcc2.Id);
        loanContact2.Loan_Applications__c = loanApplication.Id;
        loanContact2.Applicant_Type__c = Constants.COAPP;
        insert loanContact2;
        
        // Create document check lists
        List<Document_Checklist__c> documentCheckLists = new List<Document_Checklist__c>();
        Document_Checklist__c documentCheckList1 = new Document_Checklist__c(Loan_Applications__c = loanApplication.Id);
        documentCheckList1.Document_Master__c = documentMaster.Id;
        documentCheckList1.Document_Type__c = documentType.Id;
        documentCheckList1.Loan_Contact__c = loanContact1.Id;
        documentCheckLists.add(documentCheckList1);
        insert documentCheckLists;
        
        // Create content documents for document check list
        List<ContentVersion> contentVersions = new List<ContentVersion>();
        
        ContentVersion contentVersion1 = new ContentVersion();
        contentVersion1.Description = 'Test';
        contentVersion1.Title = 'Test Content Document1';
        contentVersion1.OwnerId = UserInfo.getUserId();
        contentVersion1.VersionData = Blob.valueOf('Test');
        contentVersion1.PathOnClient = 'Bucket 1/'+contentVersion.Title+'.txt';
        contentVersion1.ContentLocation = 'S';
        contentVersions.add(contentVersion1);   
        
        // Create content version
        ContentVersion contentVersion2 = new ContentVersion();
        contentVersion2.Description = 'Test';
        contentVersion2.Title = 'Test Content Document2';
        contentVersion2.OwnerId = UserInfo.getUserId();
        contentVersion2.VersionData = Blob.valueOf('Test');
        contentVersion2.PathOnClient = 'Bucket 1/'+contentVersion2.Title+'.txt';
        contentVersion2.ContentLocation = 'S';
        contentVersions.add(contentVersion2);   
        
        // Create content version
        ContentVersion contentVersion3 = new ContentVersion();
        contentVersion3.Description = 'Test';
        contentVersion3.Title = 'Test Content Document3';
        contentVersion3.OwnerId = UserInfo.getUserId();
        contentVersion3.VersionData = Blob.valueOf('Test');
        contentVersion3.PathOnClient = 'Bucket 1/'+contentVersion3.Title+'.txt';
        contentVersion3.ContentLocation = 'S';
        contentVersions.add(contentVersion3);   
        
        // Create content version
        ContentVersion contentVersion4 = new ContentVersion();
        contentVersion4.Description = 'Test';
        contentVersion4.Title = 'Test Content Document4';
        contentVersion4.OwnerId = UserInfo.getUserId();
        contentVersion4.VersionData = Blob.valueOf('Test');
        contentVersion4.PathOnClient = 'Bucket 1/'+contentVersion4.Title+'.txt';
        contentVersion4.ContentLocation = 'S';
        contentVersions.add(contentVersion4);   
        
        // Insert the new content versions
        insert contentVersions;
        
        // Create links for check list
        contentVersions = [Select Id, ContentDocumentId From ContentVersion Where Id IN: contentVersions];
        
        // Links
        List<ContentDocumentLink> links = new List<ContentDocumentLink>();
        links.add(new ContentDocumentLink(LinkedEntityId = documentCheckLists[0].Id, ContentDocumentId = contentVersions[0].ContentDocumentId));
        links.add(new ContentDocumentLink(LinkedEntityId = documentCheckLists[0].Id, ContentDocumentId = contentVersions[1].ContentDocumentId));
        links.add(new ContentDocumentLink(LinkedEntityId = documentCheckLists[0].Id, ContentDocumentId = contentVersions[2].ContentDocumentId));
        links.add(new ContentDocumentLink(LinkedEntityId = loanApplication.Id, ContentDocumentId = contentVersions[3].ContentDocumentId));
        insert links;
        
        // Create attachments for loan application
        Attachment attachment = new Attachment(Name = 'Test Attachment');
        attachment.Body = Blob.valueOf('Test');
        attachment.ParentId = loanApplication.Id;
        insert attachment;
        
        // Return
        return loanApplication;
    }
}