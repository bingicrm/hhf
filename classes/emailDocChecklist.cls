public class emailDocChecklist {
    
    @AuraEnabled
    public static Loan_Application__c getLoanApp(Id lappId){
        system.debug('=='+lappId);
        return [SELECT Id, Name, StageName__c, Sub_Stage__c, Assigned_Sales_User__c, RecordTypeID
                FROM Loan_Application__c
                WHERE Id = :lappId ];
    }
    
    @AuraEnabled
    public static String mailDocChecklist(loan_application__c la){
        String msg;
        msg='';
        String[] mailIds = new List<String>();
        String allstring; 
        String scheme;
        String suname;
        String appId;
        String conName;
        String mob;
        String sub = 'List of Documents for <scheme> from HHFL, Application No- ';
        String blank = '';
        List<Loan_Contact__c> loanConOnApplication = new List<Loan_Contact__c>();
        List<Sourcing_Detail__c> srcDetail = new List<Sourcing_Detail__c>();
        List<String> documentsName = new List<String>();
        loanConOnApplication = [SELECT Id, Name,Loan_Applications__r.Assigned_Sales_User__r.MobilePhone, Email__c, Loan_Applications__r.Assigned_Sales_User__r.Name, Loan_Applications__c,Loan_Applications__r.Loan_Number__c, Loan_Applications__r.Scheme__r.Name,  Customer__c, Customer__r.Name, Customer__r.PersonEmail 
                            FROM Loan_Contact__c
                            WHERE Loan_Applications__c=:la.Id ];
        //srcDetail = [SELECT Id, Name, Loan_Application__c, ];
        for(Loan_Contact__c lc : loanConOnApplication){
            if(lc.Email__c != NULL){
                mailIds.add(lc.Email__c);
                scheme = lc.Loan_Applications__r.Scheme__r.Name;
                suname = lc.Loan_Applications__r.Assigned_Sales_User__r.name;
                appID = lc.Loan_Applications__r.Loan_Number__c;
                conName = lc.Customer__r.Name;
                mob = lc.Loan_Applications__r.Assigned_Sales_User__r.MobilePhone;
            }
        }
        List<Document_Checklist__c> docCheckLstToMail = new List<Document_Checklist__c>();
        docCheckLstToMail = [SELECT Id, Name, Status__c, Document_Master__r.Name, Document_Type__r.Name, Loan_Applications__c, Loan_Contact__c
                            FROM Document_Checklist__c
                            WHERE Loan_Applications__c=:la.Id AND Status__c = 'Pending' AND Loan_Contact__c != NULL];
        for(Document_Checklist__c dc : docCheckLstToMail){
            if(dc.Document_Master__r.Name != null){
                documentsName.add(dc.Document_Master__r.Name);
            }
            else{
                documentsName.add(dc.Document_Type__r.Name);
            }
            
        }
        if(documentsName.size()>0){
            //documentsName.remove(null);
            allstring = string.join(documentsName,',<br/>');
            system.debug('documentsName------->'+documentsName);
            system.debug('allstring----->'+allstring);

            EmailTemplate et=[Select id,body,htmlValue from EmailTemplate where DeveloperName = 'Mail_Document_Checklist' LIMIT 1];
            String htmlBody = et.HtmlValue;
            if(conName != null){
                htmlBody = htmlBody.replace('{!Contact.Name}',conName);
            }
            else{
                htmlBody = htmlBody.replace('{!Contact.Name}',blank);
            }
            htmlBody = htmlBody.replace('{!Contact.Birthdate}', allstring);
            if(mob != null){
                htmlBody = htmlBody.replace('{!Contact.MobilePhone}',mob);
            }
            else{
                htmlBody = htmlBody.replace('{!Contact.MobilePhone}',blank);
            }
            if(suname != null){
                htmlBody = htmlBody.replace('{!Contact.Description}',suname);
            }
            system.debug('html----->'+htmlBody);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(mailIds);
            //mail.setTargetObjectId(targetId);
            mail.setSenderDisplayName('Hero FinCorp'); 
            mail.setUseSignature(false); 
            mail.setBccSender(false); 
            mail.setSaveAsActivity(false);
            mail.setHtmlBody(htmlBody);
            sub = sub.replace('<scheme>', scheme);
            mail.setSubject(sub + appId); 
			if(!Test.isRunningTest()) {
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
			}
            msg = '';
        }
        else{
            msg = 'There are no documents pending to be mailed.';
        }

        return msg;
    }
}