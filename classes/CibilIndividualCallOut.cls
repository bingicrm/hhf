public class CibilIndividualCallOut
{
    //public static Boolean hitCIBILAgain = false;
    
    @AuraEnabled
    public static ResponseCIBILWrapper createCustomerIntegration( Id loanContactId,Boolean hitCIBILAgain)
    {
         system.debug('@@hitCIBILAgain'+hitCIBILAgain);
        Loan_Contact__c lc = [SELECT id,CIBIL_CheckUp__c,pan_verification_status__c,ITR_Aadhaar_Number__c,Passport_Number__c,CIBIL_Verified__c, Pan_Number__c,Date_of_Birth__c,Gender__c,Borrower__c,Customer__r.Name,Customer__r.PAN__c,PAN_Cust__c, Loan_Applications__c,Voter_ID_Number__c,Loan_Applications__r.Requested_Amount__c, Phone_Number__c,Retrigger_Individual_CIBIL__c,Customer__c,
                              CIBIL_Response__c,Loan_Applications__r.StageName__c,Loan_Applications__r.Sub_Stage__c,Loan_Applications__r.PAN_Customer_Detail__c,Loan_Applications__r.All_Loan_Contact_Count__c,Loan_Applications__r.Application_Form_Number__c ,Loan_Applications__r.Count_of_IMD_Records__c ,(Select Address_Line_1__c, Address_Line_2__c, CityLP__c, Pincode_LP__r.Name, StateLP__c, Type_of_address__c, Address_Type__c,Retrigger_Individual_CIBIL__c From Addresses__r),
                              (SELECT Id,Name,PAN_Expired__c from Customer_integrations__r WHERE RecordType.Name='CIBIL' ORDER BY CreatedDate DESC LIMIT 1)
                                        FROM   Loan_Contact__c
                                        WHERE  ID =:loanContactId]; //Loan_Applications__r.Sub_Stage__c added by Abhilekh on 24th 2019 and Retrigger_Individual_CIBIL__c in Address and Loan Contact, Customer__c in Loan Contact on 9th May 2019 for TIL-736

		//Added by Abhilekh on 9th May 2019 fot TIL-736
        Account acc = [SELECT Id,Name,Retrigger_Individual_CIBIL__c from Account WHERE Id =: lc.Customer__c];		
		
        //Added by Ashima for CIBIL response time check
        System.debug('lc.CIBIL_Response__c!!'+lc.CIBIL_Response__c);
        if(lc.CIBIL_Response__c == true){
            System.debug('lc.CIBIL_Response__c@@@'+lc.CIBIL_Response__c);
            return new ResponseCIBILWrapper(false,'CIBIL has already been hit for response. Please try after some time.');
        }
        
		//Below If block added by Abhilekh on 24th April 2019
        if(lc.Loan_Applications__r.Sub_Stage__c != Constants.Application_Initiation &&
           lc.Loan_Applications__r.Sub_Stage__c != Constants.COPS_Data_Maker &&
           lc.Loan_Applications__r.Sub_Stage__c != Constants.Credit_Review &&
           lc.Loan_Applications__r.Sub_Stage__c != Constants.Re_Credit &&
           lc.Loan_Applications__r.Sub_Stage__c != Constants.Re_Look){
            System.debug('lc.Loan_Applications__r.Sub_Stage__c@@@'+lc.Loan_Applications__r.Sub_Stage__c);
            return new ResponseCIBILWrapper(false,'You can not hit CIBIL Validation at this stage.');
        }
        
        if( lc.Loan_Applications__r.StageName__c == 'Customer Acceptance') {
            return new ResponseCIBILWrapper(false,'You can not hit CIBIL Validation at this stage.');
        }
        //Below if block added by Abhilekh on 7th October 2019 for TIL-1297 (Not actual issue though)
        if (lc.Pan_Number__c == null && lc.Voter_ID_Number__c == null && lc.ITR_Aadhaar_Number__c== null && lc.Passport_Number__c == null) {
            return new ResponseCIBILWrapper(false,'You cannot proceed further, atleast one out of PAN, AADHAR, VoterId or Passport is mandatory');
        }
        
        //Added by Ashima for IMD check
        //Commented by Mehul as per request by Ishan Jindal - 12-02
        /*if ( lc.Loan_Applications__r.Count_of_IMD_Records__c == 0) {
            return new ResponseCIBILWrapper(false,'Please create charge details before proceeding.');
        }*/ 
        
        //Added by Ashima for PAN Card validation in CIBIL
        if(lc.pan_verification_status__c == false){
            return new ResponseCIBILWrapper(false,'Please validate PAN Card before validating CIBIL.');
        }
        
        //Added by Ashima for checking blank Application form Number
        if(String.isBlank(lc.Loan_Applications__r.Application_Form_Number__c)){
            return new ResponseCIBILWrapper(false,'Please fill Application Form Number before proceeding.');

        }
        // Added by Vaishali for BRE
        if(lc.Borrower__c == null )
        {   
            return new ResponseCIBILWrapper(false,'Please fill borrower type before proceeding');
        }
        //End of the patch- Added by Vaishali for BRE
        if(lc.Borrower__c == Constants.Corporate_API )
        {
            return new ResponseCIBILWrapper(false,'Individual CIBIL is not valid for corporate customer.');
        }
        if( lc.Addresses__r.size() < 1 )
        {
            return new ResponseCIBILWrapper(false,'Address not present');
        }
        else if( lc.Gender__c == null )
        {
            return new ResponseCIBILWrapper(false,'Gender not selected');
        }
        
		Boolean retriggerIndCIBIL = false; //Added by Abhilekh on 9th May 2019 for TIL-736
        
        //Below for block added by Abhilekh on 9th May 2019 for TIL-736
        for(Address__c add : lc.Addresses__r){
            if(add.Retrigger_Individual_CIBIL__c){
                retriggerIndCIBIL = true;
                break;
            }
        }
        
        Boolean CIBILExpired=false; //Added by Abhilekh on 9th May 2019 for TIL-736
        
        //Below if block added by Abhilekh on 9th May 2019 for TIL-736
        if(!lc.Customer_integrations__r.isEmpty()){
            for(Customer_Integration__c CI: lc.Customer_integrations__r){
                if(CI.PAN_Expired__c == 'Yes' && lc.CIBIL_Verified__c){
                    CIBILExpired = true;
                    break;
                }
            }    
        }
		
        // Added by Ashima for CIBIL record validation
        if(!hitCIBILAgain){
			//Below if block added by Abhilekh on 9th May 2019 for TIL-736
            if(lc.CIBIL_Verified__c && CIBILExpired == false && (lc.Retrigger_Individual_CIBIL__c == false && retriggerIndCIBIL == false && acc.Retrigger_Individual_CIBIL__c == false)){
                return new ResponseCIBILWrapper(false,'CIBIL record already exists! Are you sure you want to initiate again?');
            }
			
			//Below code commmented by Abhilekh on 9th May 2019 for TIL-736
           /*ID recordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId();
            List<Customer_Integration__c> lstCI = [Select id,Loan_Contact__c,(select id,name from attachments where name LIKE 'CIBIL%') from Customer_Integration__c WHERE  Loan_Contact__c =:loanContactId AND RecordTypeId =:recordTypeId];
            if (!lstCI.isEmpty()) {
                for (Customer_Integration__c ci : lstCI) {
                    for(attachment objatt : ci.attachments){
                        if(String.valueOf(objatt.name).containsIgnoreCase('CIBIL')){
                            return new ResponseCIBILWrapper(false,'CIBIL record already exists! Are you sure you want to initiate again?');
                        
                        }           
                    }
                }
            }*/
        }
        Id userId = UserInfo.getUserId();
        User u = new User();
        u = [SELECT Id, Profile.Name FROM User WHERE Id =: userId];


        if(u.Profile.Name != 'Document Scanner and Checker') {   //u.Profile.Name != 'Credit Team' && removed by Abhilekh on 24th April 2019
            Customer_Integration__c integrations = new Customer_Integration__c();
            integrations.Loan_Contact__c = loanContactId;
            integrations.Loan_Application__c = lc.Loan_Applications__c;
            integrations.recordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId();
            // **************************************Added by Vaishali for BRE
            integrations.BRE_Recent__c = true;
            List<Customer_Integration__c> lstOfCustIntegrations = new List<Customer_Integration__c>();
            lstOfCustIntegrations = [SELECT Id, BRE_Recent__c from Customer_Integration__c WHERE Loan_Application__c =: lc.Loan_Applications__c AND Loan_Contact__c =:loanContactId AND RecordType.Name='CIBIL' AND BRE_Recent__c = true];
            if(!lstOfCustIntegrations.isEmpty()) {
                for(Customer_Integration__c cust1: lstOfCustIntegrations) {
                    if(cust1.Id != integrations.Id)
                        cust1.BRE_Recent__c = false;
                }
                update lstOfCustIntegrations;
            }    
            // **************************************End of the patch --Added by Vaishali for BRE
			
            try{
                insert integrations;
            }catch(DMLException e) {
                return new ResponseCIBILWrapper(false,e.getMessage().substringafter(','));
            }

            lc.CIBIL_CheckUp__c = true;
            lc.CIBIL_Verified__c = false; // Added by Abhilekh on 24th April 2019
            update lc;
            return new ResponseCIBILWrapper(true,integrations.Id);
        }
        else
        {
            return new ResponseCIBILWrapper(false,'Operation Not allowed');
        }
    }    
    
    
    @AuraEnabled            
    public static ResponseCIBILWrapper integrateCibil(Id loanContactId , Id customerIntegrationId ) {
        Loan_Contact__c loanContact = [ SELECT id, Borrower__c, ITR_Aadhaar_Number__c,Passport_Number__c,Pan_Number__c,Customer__r.PAN__c,Date_of_Birth__c,Gender__c,Customer__r.Name,PAN_Cust__c, Loan_Applications__c,Voter_ID_Number__c,Loan_Applications__r.Requested_Amount__c, Phone_Number__c,
                                       CIBIL_Response__c,
                                       (Select Address_Line_1__c, Address_Line_2__c, CityLP__c, Pincode_LP__r.Name, StateLP__c, Type_of_address__c, Address_Type__c,Pincode_LP__r.City_LP__r.State__c From Addresses__r)
                                        FROM   Loan_Contact__c
                                        WHERE  ID =:loanContactId
                                      ];
        if ( loanContact.Borrower__c == Constants.Corporate_API ) {
            return new ResponseCIBILWrapper(false,'Individual CIBIL is not valid for corporate customer.');
        } 
       
        //Added for Bug-1222 by Mehul                             
        if (loanContact.Pan_Number__c == null && loanContact.Voter_ID_Number__c == null && loanContact.ITR_Aadhaar_Number__c== null && loanContact.Passport_Number__c == null) {  //Edited by Abhilekh on 9th April 2019
            return new ResponseCIBILWrapper(false,'You cannot proceed further, atleast one out of PAN, AADHAR, VoterId or Passport is mandatory');
        }                             
        Rest_Service__mdt restService = [ SELECT MasterLabel, QualifiedApiName , Service_EndPoint__c , 
                                                 Client_Password__c, Client_Username__c, Request_Method__c, Time_Out_Period__c  
                                          FROM   Rest_Service__mdt
                                          WHERE  MasterLabel = 'Cibil Individual'
                                        ];       
        

                                        
        Map<String,String> headerMap = new Map<String,String>();                                                      
        
        
        cls_Header headRec = new cls_Header();
        headRec.ApplicationId =  loanContact.Loan_Applications__c;  // Added by Vaishali for BRE
        headRec.CustId = loanContactId;
        headRec.RequestType = 'REQUEST';
        headRec.RequestTime = String.valueOf(System.now());
        headRec.SFDCRecordId = customerIntegrationId;
        System.debug(headRec.SFDCRecordId);
         
        cls_Name name = new cls_Name();
        name.Name1 = loanContact.Customer__r.Name;
        List<cls_Address> address = new List<cls_Address>();
        if(loanContact.Addresses__r.size() > 0 )
        {
            for( Address__c add : loanContact.Addresses__r )
            {
                cls_Address add1 = new cls_Address();
                add1.Addresstype = getAddressType(add.Type_of_address__c);
                add1.AddressResidenceCode = 'OWNED';
                add1.AddressPin = add.Pincode_LP__r.Name;
                add1.AddressCity = add.CityLP__c;
                add1.Address = add.Address_Line_1__c;
                add1.AddressState = getStateCode(add.Pincode_LP__r.City_LP__r.State__c);
                address.add(add1);
            }
            
        }
        
        //Added for Bug-1222 by Mehul  
        cls_Id id = new cls_Id();
        //id.PanId = loanContact.PAN_Cust__c ;
        id.VoterId = loanContact.Voter_ID_Number__c != null ? loanContact.Voter_ID_Number__c : '';
        id.PanId = loanContact.Pan_Number__c != null ? loanContact.Pan_Number__c : '';
        id.PassportId = loanContact.Passport_Number__c != null ? loanContact.Passport_Number__c : '' ;
        id.UidNo= loanContact.ITR_Aadhaar_Number__c != null ? loanContact.ITR_Aadhaar_Number__c : '';
            
            
        List<cls_Phone> phone = new List<cls_Phone>();
        cls_Phone phone1 = new cls_Phone();
        phone1.PhoneNumber = loanContact.Phone_Number__c;
        phone1.PhoneType = 'Mobile Phone';
        phone.add(phone1);

        cls_Relation rel = new cls_Relation();
        
        //***********************************************Added by Vaishali for BRE
        cls_BRE breDetails = new cls_BRE();
        breDetails.StrategyTag = constants.StrategyTag;
        breDetails.ClientName = constants.ClientName;
        breDetails.OriginationSourceId = constants.OriginationSourceId;
        breDetails.requestId = customerIntegrationId + '-' +String.valueOf(System.now());
        breDetails.RequestedDate = System.now();
        breDetails.isBRERun = constants.isBRERun; 
        //*************************************************End of the code - Added by Vaishali for BRE
            
            
        cls_Request req = new cls_Request(); 
        req.LoanType = constants.HOMELOANTYPE;
        req.LoanAmount = STring.valueOf(Integer.valueOf(loanContact.Loan_Applications__r.Requested_Amount__c));
        req.SourceSystemName = constants.DIRECT;
        req.BureauRegion = constants.BureauRegion;
        req.Gender = loanContact.Gender__c;
        req.BirthDT = getUpdatedDate(loanContact.Date_of_Birth__c);
        req.name = name;
        req.address = address;
        req.id = id;
        req.phone = phone; 
        req.Relation = rel;
		req.bre = breDetails;        //Added by Vaishali for BRE
        CibilRequest cbReq = new CibilRequest();    
        cbReq.Header = headRec;
        cbReq.Request = req;
         
        
    
    
    Blob headerValue = Blob.valueOf(restService.Client_Username__c + ':' + restService.Client_Password__c);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        headerMap.put('Authorization',authorizationHeader);
        headerMap.put('Username',restService.Client_Username__c);
        headerMap.put('Password',restService.Client_Password__c);
    headerMap.put('Content-Type','application/json');
    
        String jsonRequest = Json.serialize(cbReq,true);
        HttpResponse res   = Utility.sendRequest(restService.Service_EndPoint__c,restService.Request_Method__c, jsonRequest , headerMap, Integer.valueOf(restService.Time_Out_Period__c) );
        
        system.debug('@@res'+res);
        if (res != null) {
            if( res.getStatusCode() == 400 )
            {
                loanContact.CIBIL_Response__c = true;
                update loancontact;
                return new ResponseCIBILWrapper(false,'Invalid Request!! Contact system admin.');
            } 
            else if(res.getStatusCode() == 200)
            {
				loanContact.Final_Decision__c = '';     //Added by Vaishali for BRE   
                update loancontact;                     //Added by Vaishali for BRE  
                return new ResponseCIBILWrapper(true,'Request submitted!!');
            }
        }
        loanContact.CIBIL_Response__c = true;
        update loancontact;
        return new ResponseCIBILWrapper(false,'Some Error occured!! Contact system admin.');
    }

    public class CibilRequest{
        public cls_Header Header;
        public cls_Request Request;
    }    
    class cls_Header {
    
        public String ApplicationId;    //
        public String CustId;   //
        public String RequestType;  //REQUEST
        public String RequestTime;
        public String SFDCRecordId;  //2017-02-02 18:40:19
    }    
    class cls_Request {
        public String Priority; //
        public String ProductType;  //
        public String LoanType; //
        public String LoanAmount;   //1000
        public String JointInd; //
        public String InquirySubmittedBy;   //
        public String SourceSystemName; //
        public String SourceSystemVersion;  //
        public String SourceSystemVender;   //
        public String SourceSystemInstanceId;   //
        public String BureauRegion; //
        public String LoanPurposeDesc;  //
        public String BranchIfsccode;   //
        public String Kendra;   //
        public String InquiryStage; //
        public String AuthrizationFlag; //
        public String AuthrizationBy;   //
        public String IndividualCorporateFlag;  //
        public String Constitution; //
        public cls_Name Name;
        public String Gender;   //
        public String MaritalStatus;    //
        public cls_Relation Relation;
        public Integer Age; //32
        public String AgeASonDT;    //02-02-2017
        public String BirthDT;  //02-02-2017
        public String NoOfDependents;   //3
        public List<cls_Address> Address;
        public cls_Id Id;
        public List<cls_Phone> Phone;
        public String EmailId1; //
        public String EmailId2; //
        public String Alias;    //
        public String ActOpeningDT; //02-02-2017
        public String AccountNumber1;   //
        public String AccountNumber2;   //
        public String AccountNumber3;   //
        public String AccountNumber4;   //
        public String Tenure;   //
        public String GroupId;  //
        public String NumberCreditCards;    //3
        public String CreditCardNo; //343223244
        public String MonthlyIncome;    //233
        public String SoaEmployerNameC; //
        public String TimewithEmploy;   //
        public String CompanyCategory;  //
        public String NatureOfBusiness; //
        public Double AssetCost;    //1000
        public Double Collateral1;  //
        public Double Collateral1Valuation1;    //
        public Double Collateral1Valuation2;    //
        public Double Collateral2;  //
        public Double Collateral2Valuation1;    //
        public Double Collateral2Valuation2;    //
		public cls_BRE bre;                   // Added by Vaishali for BRE 
    }
    class cls_Name {
        public String Name1;    //
        public String Name2;    //
        public String Name3;    //
        public String Name4;    //
        public String Name5;    //
    }
    class cls_Relation {
        public String FatherName;   //
        public String SpouseName;   //
        public String MotherName;   //
        public String RelationType1;    //
        public String RelationType1Value;   //
        public String RelationType2;    //
        public String RelationType2Value;   //
        public String KeyPersonName;    //
        public String KeyPersonRelation;    //
        public String NomineeName;  //
        public String NomineeRelationType;  //
    }
    class cls_Address {
        public String AddressType;  //
        public String AddressResidenceCode; //98989
        public String Address;  //
        public String AddressCity;  //
        public String AddressPin;   //212111
        public String AddressState; //
    }
    class cls_Id {
        public String PanId;    //
        public String PanissueDate; //02-02-2017
        public String PanExpirationDate;    //02-02-2017
        public String PassportId;   //
        public String PassportIssueDate;    //02-02-2017
        public String PassportExpirationDate;   //02-02-2017
        public String VoterId;  //
        public String VoterIdIssueDate; //02-02-2017
        public String VoterIdExpirationDate;    //02-02-2017
        public String DrivingLicenseNo; //
        public String DriverLicenseIssueDate;   //2017-02-02 18:40:19
        public String DriverLicenseExpirationDate;  //2017-02-02 18:40:19
        public String UidNo;    //
        public String UniversalIdIssuedate; //02-02-2017
        public String UniversalIdExpirationDate;    //02-02-2017
        public String RationCard;   //
        public String RationCardIssueDate;  //02-02-2017
        public String RationCardExpirationDate; //02-02-2017
        public String IdType1;  //
        public String IdType1Value; //
        public String IdType2;  //
        public String IdType2Value; //
    }
    class cls_Phone {
        public String PhoneType;    //
        public String PhoneNumber;  //87878787878
        public String PhoneExtn;    //
        public String StdCode;  //
    }
    //***********************************************Added By Vaishali for BRE
    class cls_BRE {
        public String StrategyTag;
        Public String ClientName;
        public String OriginationSourceId;
        public String requestId;
        public DateTime RequestedDate;    
        public String isBRERun;
    } 
    //**********************************************End of the code- Added by Vaishali for BRE
    
    public class ResponseCibil{
        public String ApplicationId;    //345582839222
        public String CstId;    //0020203030
        public String RequestType;  //REQUEST
        public String RequestTime;  //29042013 11:30:00
        public Integer AcknowledgementId;   //2222222
        public String Status;   //success
    }

    public static String getUpdatedDate( Date toUpdate)
    {
        DateTime dt =  Datetime.newInstance(toUpdate.year(), toUpdate.month(), toUpdate.day());
        return dt.format('dd/MM/yyyy');
    }

    public static String getAddressType( String addressType )
    {
        if( addressType == 'Permanent' || addressType == 'Both')
        {
            return 'PERMANENT';
        }
        else if( addressType == 'Residence Address')
        {
            return 'RESIDENCE';
        }
        else if( addressType == 'Office/ Business' )
        {
            return 'OFFICE';
        }
        return 'PERMANENT';
    }

    public static String getStateCode( String stateName )
    {
        List<CIBIL_State_Mapping__mdt> allState = [Select State_Code__c,State__c,LMS_Code__c,Id from CIBIL_State_Mapping__mdt where  LMS_Code__c = :stateName];
        if(allState.size() > 0 )
        {
            return allState[0].State__c;
        }
        return stateName;
    }
    public Class ResponseCIBILWrapper
    {
        @AuraEnabled
        public boolean failure;
        @AuraEnabled
        public String respString;
        public ResponseCIBILWrapper(boolean failure,String respString)
        {
            this.failure = failure;
            this.respString = respString;
        }

    }


}