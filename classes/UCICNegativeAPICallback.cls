@RestResource(urlMapping='/ucic/NegativeAPI/*')
global with sharing class UCICNegativeAPICallback {
    
    @HttpPost
    global static ResultResponse createorUpdateUCICDatabases( UCICNegativeCallback ucic ) 
    {
       ResultResponse resRep = new ResultResponse();
       /*UCICNegativeCallback ucic = new UCICNegativeCallback();
       ucic = (UCICNegativeCallback)System.JSON.deserialize(RestContext.request.requestBody.tostring(),UCICNegativeCallback.class);
       ResultResponse resRep = new ResultResponse();*/
        String Response;
        String Rejection_Reason;
        String Severity_Level;
        Decimal Priority;
        String UCICURL;
        system.debug('ucic::'+ucic);
        Boolean EmailRequired = false;
        Boolean OverallDecisionChange = false;
        Id laId;
        if( ucic != null && String.isNotEmpty(ucic.Application_id)){
            try{
            UCICURL = ucic.URL;
            Boolean NegativeDataSource = false;
            String strJSONrequest = JSON.serializePretty(ucic);
            system.debug('strJSONRequest::' + strJSONRequest);
            List<Loan_Contact__c> lstCustomerDetail = [SELECT 
                                                       Id,
                                                       Loan_Applications__c,
                                                       Loan_Applications__r.Id,
                                                       Loan_Applications__r.Loan_Number__c,
                                                       Loan_Applications__r.UCICApp_Sequence__c,
                                                       Name,
                                                       Customer__r.Customer_ID__c,
                                                       GSTIN_Number__c,
                                                       Voter_ID_Number__c,
                                                       Spouse_Name__c,
                                                       Pan_Number__c,
                                                       Mobile__c,
                                                       Borrower__c,
                                                       Gender__c,
                                                       Email__c,
                                                       Date_Of_Birth__c,
                                                       DL_Number__c,
                                                       Constitution__c,
                                                       Applicant_Type__c,
                                                       Aadhaar_Number__c,
                                                       customer_name__c,
                                                       (SELECT Id, Name, Match_Count__c,
                                                        Match_Count_Increased__c,Recommender_s_Decision__c,
                                                        Approver_s_Decision__c /*,Match_Status*/ ,IsMatchedWithMultipleUCIC__c ,IsRejectedInLast90__c ,Matched_Max_Percentage__c ,Matched_Max_Perc_Rule__c ,Matched_Max_Perc_UCIC__c,No_of_Loans__c,Bad_Loan_At_NPA_Stage__c ,BAD_LOADN_BUCKET__c,Highest_DPD_Ever__c,No_of_Bounces_in_Lifetime__c ,Repo_Flag__c,Write_Off_Flag__c ,Filler01__c ,Filler02__c ,Filler03__c ,Filler04__c ,Filler05__c ,Filler06__c ,Filler07__c ,Filler08__c ,Filler09__c,Filler10__c ,Filler11__c ,Filler12__c ,Filler13__c
                                                        FROM UCIC_Databases__r)
                                                       FROM Loan_Contact__c 
                                                       where Loan_Applications__r.Loan_Number__c =: ucic.Application_id];
            
            //Loan_Application__c objLA = [Select Id, Sub_Stage__c, Loan_Number__c, Loan_Engine_2_Output__c from Loan_Application__c where Loan_Number__c =: ucic.Application_id];
            List<UCIC_Negative_Database__mdt> UCICdblst = [Select Id, Priority__c, Email__c, Approval_Required__c, Deviation__c, Reject_Data_Match_Count__c, Rejection_Reason__c, Severity_Level__c, UCIC_Database_Name__c,Maximum_Match_Percentage__c from UCIC_Negative_Database__mdt where Active__c = true];
            //List
            
            MAP<String,Boolean> MAPofUCICdbtoApprovalRequired = new MAP<String,Boolean>();
            MAP<String,Boolean> MAPofUCICdbtoEmailRequired = new MAP<String,Boolean>();
                for(UCIC_Negative_Database__mdt obj:UCICdblst){
                 MAPofUCICdbtoApprovalRequired.put(obj.UCIC_Database_Name__c, obj.Approval_Required__c);  
                 MAPofUCICdbtoEmailRequired.put(obj.UCIC_Database_Name__c, obj.Email__c);   
                }
            Id RecordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('UCIC').getRecordTypeId(); 
            Id primaryApplicant;
            for(Loan_Contact__c objCon: lstCustomerDetail){
                if(objCon.Applicant_Type__c=='Applicant'){
                  primaryApplicant =  objCon.Id; 
                  laId = objCon.Loan_Applications__r.Id;
                }
             
            }
            system.debug('primaryApplicant '+primaryApplicant);
            List<Customer_Integration__c> lstcust = [SELECT Id,UCIC_Negative_API_Response__c,Negative_UCIC_DB_Found__c FROM Customer_Integration__c WHERE RecordTypeId =: RecordTypeId AND Loan_Application__r.Loan_Number__c =: ucic.Application_id  AND Loan_Contact__c =: primaryApplicant order by createdDate DESC LIMIT 1];
            Customer_Integration__c cust  = new Customer_Integration__c();              
                if(lstcust.size() > 0){
                cust = lstcust[0];
                }
            MAP<String, List<UCIC_Database__c>> MAPofCuatomerIdToUCICDatabases =new MAP<String, List<UCIC_Database__c>>();
            MAP<String, List<String>> MAPofCuatomerIdToUCICDatabasesName =new MAP<String, List<String>>();
            /*if(objLA.Loan_Engine_2_Output__c != 'Non STP' && (objLA.Sub_Stage__c =='Credit Review' || objLA.Sub_Stage__c =='Re Credit' || objLA.Sub_Stage__c =='Re-Look')){
            objLA.Loan_Engine_2_Output__c = 'Non STP' ;
            update objLA;
            }*/
            MAP<String, Id> MAPofCuatomerIdToCDId =new MAP<String, Id>();
            MAP<String, Id> MAPofCuatomerIdToLAId =new MAP<String, Id>();
            List<UCIC_Database__c> lstofAllUCICDb =  [SELECT Id, Name, Match_Count__c,
                                                        Match_Count_Increased__c,Recommender_s_Decision__c,
                                                        Approver_s_Decision__c /*,Match_Status*/ ,IsMatchedWithMultipleUCIC__c ,IsRejectedInLast90__c ,Matched_Max_Percentage__c ,Matched_Max_Perc_Rule__c ,Matched_Max_Perc_UCIC__c,No_of_Loans__c,Bad_Loan_At_NPA_Stage__c ,BAD_LOADN_BUCKET__c,Highest_DPD_Ever__c,No_of_Bounces_in_Lifetime__c ,Repo_Flag__c,Write_Off_Flag__c ,Filler01__c ,Filler02__c ,Filler03__c ,Filler04__c ,Filler05__c ,Filler06__c ,Filler07__c ,Filler08__c ,Filler09__c,Filler10__c ,Filler11__c ,Filler12__c ,Filler13__c
                                                        FROM UCIC_Database__c where Loan_Application__r.Loan_Number__c =: ucic.Application_id];
            if( lstCustomerDetail.size() > 0){
                for(Loan_Contact__c objlc: lstCustomerDetail){
                    List<UCIC_Database__c> lstUCICDb = new List<UCIC_Database__c>();
                    List<String> lstUCICDbName = new List<String>();
                    MAPofCuatomerIdToCDId.put(objlc.Customer__r.Customer_ID__c, objlc.Id);
                    MAPofCuatomerIdToLAId.put(objlc.Customer__r.Customer_ID__c, objlc.Loan_Applications__c);
                    for(UCIC_Database__c objUCICDb: objlc.UCIC_Databases__r){
                    lstUCICDb.add(objUCICDb); 
                    lstUCICDbName.add(objUCICDb.Name);  
                    //lstofAllUCICDb.add(objUCICDb);    
                    }
                    MAPofCuatomerIdToUCICDatabases.put(objlc.Customer__r.Customer_ID__c,lstUCICDb);
                    MAPofCuatomerIdToUCICDatabasesName.put(objlc.Customer__r.Customer_ID__c,lstUCICDbName);
                    
                 
                }
                
            }
            System.debug('MAPofCuatomerIdToLAId==>'+MAPofCuatomerIdToLAId);
            List<MatchResponse> lstMatchResponse = ucic.MatchResponse;
            List<UCIC_Database__c> lstUCICData = new List<UCIC_Database__c>();
            List<UCIC_Database__c> lstUCICFinalData = new List<UCIC_Database__c>();
            List<String> lstDBNames = new List<String>();
            List<String> lstUCICFinalDBName = new List<String>();
                if(lstMatchResponse.size() > 0){
                       System.debug('lstMatchResponse::'+lstMatchResponse);
                    for(MatchResponse objmatchResponse: lstMatchResponse){
                       System.debug('MAPofCuatomerIdToUCICDatabases::'+MAPofCuatomerIdToUCICDatabases); 
                       System.debug('objmatchResponse.Customer_id::'+objmatchResponse.Customer_id);                        
                        if(MAPofCuatomerIdToUCICDatabases.get(objmatchResponse.Customer_id)!= null && MAPofCuatomerIdToUCICDatabases.get(objmatchResponse.Customer_id).size() > 0){
                        
                        lstUCICData =MAPofCuatomerIdToUCICDatabases.get(objmatchResponse.Customer_id);  
                        lstDBNames = MAPofCuatomerIdToUCICDatabasesName.get(objmatchResponse.Customer_id); 
                                    System.debug('lstUCICData::'+lstUCICData);
                            for(UCICDatabases objucicDB: objmatchResponse.UCICDatabases){
                                    System.debug('objucicDB::'+objucicDB);
                                if(Decimal.ValueOf(objucicDB.Match_Count) > 0){

                                for(UCIC_Database__c objucic: lstUCICData){
                                    System.debug('objucic.Name::'+objucic.Name + '::objucicDB.Name::' + objucicDB.Name);
                                    if(objucic.Name == objucicDB.Name){
                                    System.debug('I am in IF Match Count::'+Decimal.ValueOf(objucicDB.Match_Count));
                                    System.debug('I am in IF Match objucic.Match_Count__c::'+objucic.Match_Count__c);
                                     //objucic.URL__c = objucicDB.URL;  
                                        if(Decimal.ValueOf(objucicDB.Match_Count) > objucic.Match_Count__c && MAPofUCICdbtoApprovalRequired.get(objucicDB.Name)){
                                        System.debug('Entered after compairsion');
                                        objucic.Match_Count_Increased__c = true;
                                        objucic.Recommender_s_Decision__c = '';
                                        objucic.Approver_s_Decision__c = '';
                                        OverallDecisionChange = true;    
                                        }
                                        objucic.Match_Count__c = Decimal.ValueOf(objucicDB.Match_Count);
                                        //objucic.Match_Status =objucicDB.Match_Status;
                                        objucic.IsMatchedWithMultipleUCIC__c = objucicDB.IsMatchedWithMultipleUCIC;
                                        objucic.IsRejectedInLast90__c = objucicDB.IsRejectedInLast90;
                                        objucic.Matched_Max_Percentage__c = objucicDB.Matched_Max_Percentage;
                                        objucic.Matched_Max_Perc_Rule__c = objucicDB.Matched_Max_Perc_Rule;
                                        objucic.Matched_Max_Perc_UCIC__c = objucicDB.Matched_Max_Perc_UCIC;
                                        objucic.No_of_Loans__c = objucicDB.No_OF_LOANS;
                                        objucic.Bad_Loan_At_NPA_Stage__c = objucicDB.Bad_Loan_At_NPA_Stage;
                                        objucic.BAD_LOADN_BUCKET__c = objucicDB.BAD_LOADN_BUCKET;
                                        objucic.Highest_DPD_Ever__c = objucicDB.Highest_DPD_Ever;
                                        objucic.No_of_Bounces_in_Lifetime__c = objucicDB.No_of_bounces_in_lifetime;
                                        objucic.Repo_Flag__c = objucicDB.Repo_Flag;
                                        objucic.Write_Off_Flag__c = objucicDB.Write_Off_Flag;
                                        objucic.Filler01__c = objucicDB.Filler01;
                                        objucic.Filler02__c = objucicDB.Filler02;
                                        objucic.Filler03__c = objucicDB.Filler03;
                                        objucic.Filler04__c = objucicDB.Filler04;
                                        objucic.Filler05__c = objucicDB.Filler05;
                                        objucic.Filler06__c = objucicDB.Filler06;
                                        objucic.Filler07__c = objucicDB.Filler07;
                                        objucic.Filler08__c = objucicDB.Filler08;
                                        objucic.Filler09__c = objucicDB.Filler09;
                                        objucic.Filler10__c = objucicDB.Filler10;
                                        objucic.Filler11__c = objucicDB.Filler11;
                                        objucic.Filler12__c = objucicDB.Filler12;
                                        objucic.Filler13__c = objucicDB.Filler13; 
                                              
                                      lstUCICFinalData.add(objucic);   
                                      lstUCICFinalDBName.add(objucic.Name);
                                      
                                        if(EmailRequired == false)
                                        EmailRequired = MAPofUCICdbtoEmailRequired.get(objucicDB.Name) != null ? MAPofUCICdbtoEmailRequired.get(objucicDB.Name): FALSE;
                                    }
                                    else if(!lstDBNames.contains(objucicDB.Name)){ // Check if Current DB doesnt Exist in Previous DB
                                            System.debug('I am in Else Match Count::'+Decimal.ValueOf(objucicDB.Match_Count));
                                            UCIC_Database__c objUCICData = new UCIC_Database__c();
                                            objUCICData.Name = objucicDB.Name;
                                            objUCICData.Match_Count__c = Decimal.ValueOf(objucicDB.Match_Count);
                                            objUCICData.Email_Required__c = MAPofUCICdbtoEmailRequired.get(objucicDB.Name) != null ? MAPofUCICdbtoEmailRequired.get(objucicDB.Name): FALSE;  
                                            //objUCICData.URL__c = objucicDB.URL;
                                            objUCICData.Customer_Detail__c = MAPofCuatomerIdToCDId.get(objmatchResponse.Customer_id);
                                            objUCICData.Loan_Application__c = MAPofCuatomerIdToLAId.get(objmatchResponse.Customer_id);
                                            //objUCICData.Match_Status =objucicDB.Match_Status;
                                            objUCICData.IsMatchedWithMultipleUCIC__c = objucicDB.IsMatchedWithMultipleUCIC;
                                            objUCICData.IsRejectedInLast90__c = objucicDB.IsRejectedInLast90;
                                            objUCICData.Matched_Max_Percentage__c = objucicDB.Matched_Max_Percentage;
                                            objUCICData.Matched_Max_Perc_Rule__c = objucicDB.Matched_Max_Perc_Rule;
                                            objUCICData.Matched_Max_Perc_UCIC__c = objucicDB.Matched_Max_Perc_UCIC;
                                            objUCICData.No_of_Loans__c = objucicDB.No_OF_LOANS;
                                            objUCICData.Bad_Loan_At_NPA_Stage__c = objucicDB.Bad_Loan_At_NPA_Stage;
                                            objUCICData.BAD_LOADN_BUCKET__c = objucicDB.BAD_LOADN_BUCKET;
                                            objUCICData.Highest_DPD_Ever__c = objucicDB.Highest_DPD_Ever;
                                            objUCICData.No_of_Bounces_in_Lifetime__c = objucicDB.No_of_bounces_in_lifetime;
                                            objUCICData.Repo_Flag__c = objucicDB.Repo_Flag;
                                            objUCICData.Write_Off_Flag__c = objucicDB.Write_Off_Flag;
                                            objUCICData.Filler01__c = objucicDB.Filler01;
                                            objUCICData.Filler02__c = objucicDB.Filler02;
                                            objUCICData.Filler03__c = objucicDB.Filler03;
                                            objUCICData.Filler04__c = objucicDB.Filler04;
                                            objUCICData.Filler05__c = objucicDB.Filler05;
                                            objUCICData.Filler06__c = objucicDB.Filler06;
                                            objUCICData.Filler07__c = objucicDB.Filler07;
                                            objUCICData.Filler08__c = objucicDB.Filler08;
                                            objUCICData.Filler09__c = objucicDB.Filler09;
                                            objUCICData.Filler10__c = objucicDB.Filler10;
                                            objUCICData.Filler11__c = objucicDB.Filler11;
                                            objUCICData.Filler12__c = objucicDB.Filler12;
                                            objUCICData.Filler13__c = objucicDB.Filler13; 
                                            lstUCICFinalData.add(objUCICData);    
                                            lstDBNames.add(objucicDB.Name);
                                            lstUCICFinalDBName.add(objUCICData.Name);
                                            if(EmailRequired == false)
                                            EmailRequired = MAPofUCICdbtoEmailRequired.get(objucicDB.Name) != null ? MAPofUCICdbtoEmailRequired.get(objucicDB.Name): FALSE;
                                            OverallDecisionChange = true;
                                    }
                                }
                                }
                            }    
                        }
                        else{
                        System.debug('I am else::');
                        System.debug('MAPofCuatomerIdToLAId11==>'+MAPofCuatomerIdToLAId);
                        System.debug('objmatchResponse.UCICDatabases==>'+objmatchResponse.UCICDatabases);

                        for(UCICDatabases objucicDB: objmatchResponse.UCICDatabases){
                        System.debug('objucicDB.Name==>'+objucicDB.Name +'objucicDB.Match_Count==>'+objucicDB.Match_Count);
                            if(Decimal.ValueOf(objucicDB.Match_Count) > 0){

                            UCIC_Database__c objUCICData = new UCIC_Database__c();
                            objUCICData.Name = objucicDB.Name;
                            objUCICData.Match_Count__c = Decimal.ValueOf(objucicDB.Match_Count);
                            objUCICData.Email_Required__c = MAPofUCICdbtoEmailRequired.get(objucicDB.Name) != null ? MAPofUCICdbtoEmailRequired.get(objucicDB.Name): FALSE;  
                            //objUCICData.URL__c = objucicDB.URL;
                            objUCICData.Customer_Detail__c = MAPofCuatomerIdToCDId.get(objmatchResponse.Customer_id);
                            System.debug('MAPofCuatomerIdToLAId22==>'+MAPofCuatomerIdToLAId);
                            System.debug('objmatchResponse.Customer_id 1==>'+objmatchResponse.Customer_id);
                            System.debug('objUCICData.Loan_Application__c 1==>'+MAPofCuatomerIdToLAId.get(objmatchResponse.Customer_id));
                            objUCICData.Loan_Application__c = MAPofCuatomerIdToLAId.get(objmatchResponse.Customer_id);
                            //objUCICData.Match_Status =objucicDB.Match_Status;
                            objUCICData.IsMatchedWithMultipleUCIC__c = objucicDB.IsMatchedWithMultipleUCIC;
                            objUCICData.IsRejectedInLast90__c = objucicDB.IsRejectedInLast90;
                            objUCICData.Matched_Max_Percentage__c = objucicDB.Matched_Max_Percentage;
                            objUCICData.Matched_Max_Perc_Rule__c = objucicDB.Matched_Max_Perc_Rule;
                            objUCICData.Matched_Max_Perc_UCIC__c = objucicDB.Matched_Max_Perc_UCIC;
                            objUCICData.No_of_Loans__c = objucicDB.No_OF_LOANS;
                            objUCICData.Bad_Loan_At_NPA_Stage__c = objucicDB.Bad_Loan_At_NPA_Stage;
                            objUCICData.BAD_LOADN_BUCKET__c = objucicDB.BAD_LOADN_BUCKET;
                            objUCICData.Highest_DPD_Ever__c = objucicDB.Highest_DPD_Ever;
                            objUCICData.No_of_Bounces_in_Lifetime__c = objucicDB.No_of_bounces_in_lifetime;
                            objUCICData.Repo_Flag__c = objucicDB.Repo_Flag;
                            objUCICData.Write_Off_Flag__c = objucicDB.Write_Off_Flag;
                            objUCICData.Filler01__c = objucicDB.Filler01;
                            objUCICData.Filler02__c = objucicDB.Filler02;
                            objUCICData.Filler03__c = objucicDB.Filler03;
                            objUCICData.Filler04__c = objucicDB.Filler04;
                            objUCICData.Filler05__c = objucicDB.Filler05;
                            objUCICData.Filler06__c = objucicDB.Filler06;
                            objUCICData.Filler07__c = objucicDB.Filler07;
                            objUCICData.Filler08__c = objucicDB.Filler08;
                            objUCICData.Filler09__c = objucicDB.Filler09;
                            objUCICData.Filler10__c = objucicDB.Filler10;
                            objUCICData.Filler11__c = objucicDB.Filler11;
                            objUCICData.Filler12__c = objucicDB.Filler12;
                            objUCICData.Filler13__c = objucicDB.Filler13; 
                            
                            lstUCICFinalData.add(objUCICData);
                            lstUCICFinalDBName.add(objUCICData.Name);
                            if(EmailRequired == false)
                            EmailRequired = MAPofUCICdbtoEmailRequired.get(objucicDB.Name) != null ? MAPofUCICdbtoEmailRequired.get(objucicDB.Name): FALSE;
                            OverallDecisionChange = true;
                            }
                        }
                            
                        }
                        
                    }
                }
            
            for(UCIC_Database__c objucic:lstUCICFinalData){
                system.debug('UCICdblst::'+UCICdblst);
                for(UCIC_Negative_Database__mdt objUCICdb: UCICdblst){
                    if(objUCICdb.UCIC_Database_Name__c == objucic.Name){
                            system.debug('objUCICdb.Matched_Max_Percentage__c::'+objucic.Matched_Max_Percentage__c);
                            system.debug('objUCICdb.Maximum_Match_Percentage__c::'+objUCICdb.Maximum_Match_Percentage__c);
                            system.debug('objucic.Match_Count__c::'+objucic.Match_Count__c);
                            system.debug('objUCICdb.Reject_Data_Match_Count__c::'+objUCICdb.Reject_Data_Match_Count__c);
                        if( objUCICdb.Reject_Data_Match_Count__c != null && objucic.Match_Count__c > = objUCICdb.Reject_Data_Match_Count__c && Decimal.ValueOf(objucic.Matched_Max_Percentage__c) >= objUCICdb.Maximum_Match_Percentage__c){
                            if(NegativeDataSource == false){
                            NegativeDataSource = true;   
                            }
                            system.debug('objUCICdb.UCIC_Database_Name__c::'+objUCICdb.UCIC_Database_Name__c);
                            system.debug('objUCICdb.Priority__c::'+objUCICdb.Priority__c);
                            system.debug('Priority::'+Priority);
                            if( Priority == null || objUCICdb.Priority__c < Priority){
                             Priority =  objUCICdb.Priority__c; 
                             Rejection_Reason = objUCICdb.Rejection_Reason__c;
                             Severity_Level = objUCICdb.Severity_Level__c;   
                            }
                            
                        } 
                    }
                }
            }
            
            cust.UCIC_Negative_API_Response__c = true;
            cust.Negative_UCIC_DB_Found__c = NegativeDataSource;
            cust.Overall_UCIC_Decision_Change__c = OverallDecisionChange;
            if(lstUCICFinalDBName.size() > 0){    
                    //cust.Email_Required__c = EmailRequired;
                }    
            if(Severity_Level != null)    
            cust.Severity_Level__c = Severity_Level;
            if(Rejection_Reason != null)     
            cust.Rejection_Reason__c = Rejection_Reason; 
            cust.UCIC_URL__c = UCICURL;
            update cust;
             List<UCIC_Database__c> lstUCICDBNotExistCurrently = new List<UCIC_Database__c>();
                system.debug('lstofAllUCICDb::::'+lstofAllUCICDb);
                system.debug('lstUCICFinalDBName::::'+lstUCICFinalDBName);
                for(UCIC_Database__c objucic:lstofAllUCICDb){
                    if(!lstUCICFinalDBName.contains(objucic.Name)){
                    lstUCICDBNotExistCurrently.add(objucic);   
                    }   
                }   
                system.debug('lstUCICDBNotExistCurrently::'+lstUCICDBNotExistCurrently);
                if(lstUCICDBNotExistCurrently.size() > 0){
                 system.debug('lstUCICDBNotExistCurrently:: In If' + lstUCICDBNotExistCurrently);
                 //delete lstUCICDBNotExistCurrently;
                database.delete(lstUCICDBNotExistCurrently);

                }                       
            if(lstUCICFinalData.size() > 0){
                system.debug('lstUCICFinalData::' + lstUCICFinalData);
                //List<Database.SaveResult> results = Database.upsert(lstUCICFinalData, false);
                Database.UpsertResult[] results = Database.upsert(lstUCICFinalData,false);
                for (Database.UpsertResult sr : results) {
                    if (sr.isSuccess()) {
                        // Operation was successful
                        resRep.Result = 'Success';  
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) {
                            resRep.ErrorMsg += err;

                            System.debug('error has occurred.' + err.getStatusCode() + ': ' + err.getMessage());                    
                            System.debug('fields that affected this error: ' + err.getFields());
                            
                        }
                    }
                }
                
                if(resRep.Result == 'Success'){
                    if(lstUCICFinalDBName.size() > 0){    
                        cust.Email_Required__c = EmailRequired;
                        update cust;
                    } 
                }
                
                /*for(Database.SaveResult result : results){
                    if(!result.getErrors().isEmpty()){
                        resRep.ErrorMsg += result.getErrors();
                        system.debug('result.getErrors()  ' + result.getErrors());
                        //success = false;
                    }
                }*/
                //upsert lstUCICFinalData;
            }
             //updateDeviations(lstUCICFinalData); 
              updateDeviations(laId);
              resRep.Result = 'Success';  
              //LogUtility.createIntegrationLogs(UCICNegativeCallback.tostring(),'','');
            }
            catch(Exception e){
            resRep.ErrorMsg += e.getMessage() + e.getLineNumber() +e.getCause();
            resRep.Result = 'Error';
            Response = 'Error : ' + e.getMessage();  
            }
        }
        else{
            resRep.Result = 'Error : ' + 'Application Id is Blank in request.';  
        }
        
        HttpResponse res = new HttpResponse();
        res.setBody(String.valueOf(resRep)); 
        system.debug('RestContext.request.requestBody.tostring()::'+res);

        LogUtility.createIntegrationLogs(String.valueOf(ucic),res,'UCIC Negative Callback Response');
        return resRep;
    }
    public static void updateDeviations(Id laId){
        List<Applicable_Deviation__c> toInsert = new List<Applicable_Deviation__c>();
        List<Applicable_Deviation__c> toDelete = new List<Applicable_Deviation__c>();
        List<String> UCICdblst = new List<String> ();
        for(UCIC_Negative_Database__mdt objDB: [Select UCIC_Database_Name__c from UCIC_Negative_Database__mdt where Active__c = true and Deviation__c = true]){
            UCICdblst.add(objDB.UCIC_Database_Name__c);
        }
        List<UCIC_Database__c> lstUCICDBApplicableforDevaition = new List<UCIC_Database__c>();
        List<String> lstUCICDBApplicableforDevaitionName = new List<String>();
     
        List<UCIC_Database__c> lstUCICDB = [Select Id, Loan_Application__c, Name from UCIC_Database__c where Loan_Application__c =: laId];
        for(UCIC_Database__c objDB: lstUCICDB){
            if(UCICdblst.contains(objDB.Name)){
            lstUCICDBApplicableforDevaition.add(objDB);
            lstUCICDBApplicableforDevaitionName.add(objDB.Name);  
            }
        }
        List<Applicable_Deviation__c> appDevList = [Select Id, UCIC_DB_Name__c, Loan_Application__r.Scheme__r.Product_Code__c, Deviation_Code__c, Credit_Deviation_Master__c, Credit_Deviation_Master__r.Status__c, Loan_Application__c, Customer_Integration__c, Auto_Deviation_UCIC__c from Applicable_Deviation__c where Loan_Application__c =: laId and Customer_Details__c = null and Auto_Deviation_UCIC__c = true];
        System.debug('appDevList:::'+appDevList);
        Map<String, Applicable_Deviation__c> MapOfAPPDevtoDBName = new Map<String, Applicable_Deviation__c>();
        for(Applicable_Deviation__c appDev: appDevList){
            if(appDev.Credit_Deviation_Master__r.Status__c == False){
            MapOfAPPDevtoDBName.put(appDev.UCIC_DB_Name__c, null);
            }
            else{
            MapOfAPPDevtoDBName.put(appDev.UCIC_DB_Name__c, appDev);
            }
            system.debug('appDev.UCIC_DB_Name__c::'+ appDev.UCIC_DB_Name__c);
            system.debug('lstUCICDBApplicableforDevaitionName::'+ lstUCICDBApplicableforDevaitionName);
            if(!lstUCICDBApplicableforDevaitionName.contains(appDev.UCIC_DB_Name__c) || appDev.Credit_Deviation_Master__r.Status__c == False){
            toDelete.add(appDev);   
            }
        }
        system.debug('toDelete::'+ toDelete);
        system.debug('MapOfAPPDevtoDBName::'+ MapOfAPPDevtoDBName);
        Map<String, Id> MapOfCreditDeviationandDBName = new Map<String, Id>();
        List<Credit_Deviation_Master__c> devMasterList =[Select Id, UCIC_DB_Name__c from Credit_Deviation_Master__c where UCIC_DB_Name__c != null];
        for(Credit_Deviation_Master__c objCDM: devMasterList){
            MapOfCreditDeviationandDBName.put(objCDM.UCIC_DB_Name__c, objCDM.Id);
        }
        System.debug('MapOfCreditDeviationandDBName::'+MapOfCreditDeviationandDBName);
        System.debug('lstUCICDBApplicableforDevaition::'+lstUCICDBApplicableforDevaition);
        List<Mitigant__c> lstOfMitigants = [SELECT Id, Active__c from Mitigant__c where Name ='Others'];
        
        if(appDevList.size() == 0){
            for(UCIC_Database__c objDB: lstUCICDBApplicableforDevaition){
                if(MapOfCreditDeviationandDBName.get(objDB.Name) != null ){
                    if(MapOfAPPDevtoDBName.get(objDB.Name) == null){
                    Applicable_Deviation__c app = new Applicable_Deviation__c();
                    app.Loan_Application__c = objDB.Loan_Application__c;
                    app.Credit_Deviation_Master__c = MapOfCreditDeviationandDBName.get(objDB.Name);
                    app.UCIC_DB_Name__c = objDB.Name;
                    app.Auto_Deviation_UCIC__c = true;
                    //app.Auto_Deviation__c =true;
                    if(!lstOfMitigants.isEmpty()) {
                        app.Mitigant__c = lstOfMitigants[0].Id;
                        app.If_Others_please_specify__c = 'Created via UCIC integration';
                    }
                    MapOfAPPDevtoDBName.put(objDB.Name, app);
                    toInsert.add(app);
                    }
                }
            }
        }
        else if(appDevList.size()>0){
            for(UCIC_Database__c objDB: lstUCICDBApplicableforDevaition){
                if(MapOfCreditDeviationandDBName.get(objDB.Name) != null ){
                if(MapOfAPPDevtoDBName.get(objDB.Name) == null){
                    Applicable_Deviation__c app = new Applicable_Deviation__c();
                    app.Loan_Application__c = objDB.Loan_Application__c;
                    app.Credit_Deviation_Master__c = MapOfCreditDeviationandDBName.get(objDB.Name);
                    app.UCIC_DB_Name__c = objDB.Name;
                    app.Auto_Deviation_UCIC__c = true;
                    //app.Auto_Deviation__c =true;
                    if(!lstOfMitigants.isEmpty()) {
                        app.Mitigant__c = lstOfMitigants[0].Id;
                        app.If_Others_please_specify__c = 'Created via UCIC integration';
                    }
                    MapOfAPPDevtoDBName.put(objDB.Name, app);
                    system.debug('MapOfAPPDevtoDBName after app insert::'+MapOfAPPDevtoDBName);
                    toInsert.add(app);
                }
                }
            }
            
        }
        
        if(toDelete.size()>0){
            RunDeleteTriggerOnApplicableDeviation__c   runValidationCheck = RunDeleteTriggerOnApplicableDeviation__c.getInstance('RunDeleteTriggerOnApplicableDeviation');
            runValidationCheck.byPassTrigger__c = true;
            update runValidationCheck;
           
            try{
                database.delete(toDelete);
            }
            catch(Exception e){
                System.debug('Error encountered while deleting:'+e);
                runValidationCheck.byPassTrigger__c = false;
                update runValidationCheck;
            }
            runValidationCheck.byPassTrigger__c = false;
            update runValidationCheck;
        }
        system.debug('toInsert '+toInsert);
        if(toInsert.size()>0){
            try{
                database.insert(toInsert);
            }
            catch(Exception e){
                System.debug('Error encountered while insertion:'+e);
            }
        }
    }   
                       
    global class UCICNegativeCallback
    {
        public String Source_System;
        public String Application_id;
        public String URL;
        public MatchResponse[] MatchResponse;
        public UCICNegativeCallback(){
            
        }
    }

    global class MatchResponse
    {
        public String Customer_id;
        public String UCICID;
        public UCICDatabases[] UCICDatabases;
    }
    global class UCICDatabases
    {
        public String Name;
        public String Match_Count;
        public String Match_Status;
        public String NoOfMatches;
        public String IsMatchedWithMultipleUCIC;
        public String IsRejectedInLast90;
        public String Matched_Max_Percentage;
        public String Matched_Max_Perc_Rule;
        public String Matched_Max_Perc_UCIC;
        public String No_OF_LOANS;
        public String Bad_Loan_At_NPA_Stage;
        public String BAD_LOADN_BUCKET;
        public String Highest_DPD_Ever;
        public String No_of_bounces_in_lifetime;
        public String Repo_Flag;
        public String Write_Off_Flag;
        public String Filler01;
        public String Filler02;
        public String Filler03;
        public String Filler04;
        public String Filler05;
        public String Filler06;
        public String Filler07;
        public String Filler08;
        public String Filler09;
        public String Filler10;
        public String Filler11;
        public String Filler12;
        public String Filler13; 
    }
    global class ResultResponse{
        public String ErrorMsg;
        public String Result;
        public ResultResponse(){
            ErrorMsg = '{}';
            Result = '';
        }
    }
    global Class Error {
        public String errorCode;
        public String errorType;
        public String errorMessage;
    }
}