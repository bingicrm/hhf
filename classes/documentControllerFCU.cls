public class documentControllerFCU {
	
	//Below method added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
    @AuraEnabled
    public static Boolean checkLAFCUBRDExemption(Id tpvId){
        Third_Party_Verification__c tpv = [SELECT Id,Name,Exempted_from_FCU_BRD__c from Third_Party_Verification__c WHERE Id =: tpvId];
        return tpv.Exempted_from_FCU_BRD__c;
    }
    
    //Below method added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
    @AuraEnabled
    public static Boolean checkLAFCUBRDExemption1(Id laId){
        Loan_Application__c la = [SELECT Id,Name,Exempted_from_FCU_BRD__c from Loan_Application__c WHERE Id =: laId];
        return la.Exempted_from_FCU_BRD__c;
    }
    
    //Below method added by Abhilekh on 14th September 2019 for FCU BRD
    @AuraEnabled
    public static String checkUserRole(){
        return [SELECT Id,Name from UserRole WHERE Id =: UserInfo.getUserRoleId()].Name;
    }
    
	//Below method added by Abhilekh on 14th September 2019 for FCU BRD
    @AuraEnabled
    public static List<Document_Checklist__c> fetchAllDocs(Id laId){
        List<Document_Checklist__c> lstDocChecklist = new List<Document_Checklist__c>();
        
        lstDocChecklist = [SELECT Id,Name,Document_Master__c,Document_Master__r.Name,Decision__c from Document_Checklist__c WHERE Loan_Applications__c =: laId AND Status__c =: Constants.Uploaded];
        System.debug('lstDocChecklist==>'+lstDocChecklist);
        System.debug('lstDocChecklist.size()==>'+lstDocChecklist.size());
        return lstDocChecklist;
    }
    
    //Below method added by Abhilekh on 14th September 2019 for FCU BRD
    @AuraEnabled
    public static String createFreshFCUTPV(String docIds,Id laId){
        System.debug('docIds==>'+docIds);
        List<FCU_Document_Checklist__c> lstNewFCUDocChecklist = new List<FCU_Document_Checklist__c>();
        List<Loan_Application__c> lstLAToUpdate = new List<Loan_Application__c>();
        
        Set<Id> setDocIds = (Set<Id>) System.JSON.deserialize(docIds, Set<Id>.class);
        System.debug('setDocIds==>'+setDocIds);
        System.debug('setDocIds.size()==>'+setDocIds.size());
        
        Loan_Application__c la = [SELECT Id,Name,StageName__c,Sub_Stage__c,Branch_Lookup__r.Name,Branch_Lookup__c,Credit_Manager_User__c,Assigned_Sales_User__c,Customer__c,Assigned_FCU_Agency__c FROM Loan_Application__c WHERE Id =: laId ];
        List<User_Branch_Mapping__c> ubList = new List<User_Branch_Mapping__c>();
        ubList = [Select Id,User__r.FCU__c,User__r.Role_Assigned_Date__c,User__r.Profile.Name,User__r.UserRole.Name, User__c, Servicing_Branch__r.Pincode_Name__c from User_Branch_Mapping__c where User__r.isActive = TRUE AND User__r.Profile.Name = 'Third Party Vendor' AND User__r.FCU__c = TRUE];
        
        Id FCURecordTypeId = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get(Constants.FCU).getRecordTypeId();
        
        Third_Party_Verification__c fcuTPV = new Third_Party_Verification__c();
        Id OwnerIdFCU;
        Datetime ladFCU = System.now();
        Datetime radFCU;
        
        fcuTPV.Loan_Application__c = la.Id;
        fcuTPV.Customer__c = la.Customer__c; //Added by Abhilekh on 5th September 2019 for FCU BRD
        fcuTPV.FCU_Type__c = Constants.Pre_Sanction; //Added by Abhilekh on 5th September 2019 for FCU BRD
        //tprFCU.Customer_Details__c = c.Id;
        //tprFCU.Customer_Segment__c = c.Customer_segment__c;
        fcuTPV.Partner_Community_Login__c = Label.Third_Party_Community_Login;
        fcuTPV.RecordTypeId = FCURecordTypeId;
        fcuTPV.Manually_Created_Record__c = FALSE;
        fcuTPV.LA_Credit_Manager_User__c = la.Credit_Manager_User__c;
        fcuTPV.LA_Sales_Manager_User__c = la.Assigned_Sales_User__c;
        
        for(User_Branch_Mapping__c ub: ubList){
          system.debug('PROFILE---SVB---BRNCH'+ub.User__r.Profile.Name+ub.Servicing_Branch__c+la.Branch_Lookup__c);
            if(ub.User__r.FCU__c == TRUE && ub.Servicing_Branch__c == la.Branch_Lookup__c){
            
            radFCU = ub.User__r.Role_Assigned_Date__c;
            if(radFCU == null){
              OwnerIdFCU = ub.User__c;
            }
            else if(radFCU < ladFCU){
              ladFCU = radFCU;
              OwnerIdFCU = ub.User__c;
            }
          }
        }
        
        if(la.Assigned_FCU_Agency__c != null){
            fcuTPV.Owner__c = la.Assigned_FCU_Agency__c;
        }
        else{
            fcuTPV.Owner__c = OwnerIdFCU;
            
            Loan_Application__c la1 = new Loan_Application__c();
            la1.Id = la.Id;
            la1.Assigned_FCU_Agency__c = OwnerIdFCU;
            lstLAToUpdate.add(la1);
        }
        
        if(fcuTPV.Owner__c != null){
          fcuTPV.Status__c = Constants.INITIATED;
        }
        else{
          fcuTPV.Status__c = Constants.NEW_VALUE;
        }
        System.debug('----fcuTPVOwner----'+fcuTPV.Owner__c);
        fcuTPV.FCU_Internal_Manager__c = la.Credit_Manager_User__c;
        
        try{
            insert fcuTPV;
            
            for(String docId: setDocIds){
                FCU_Document_Checklist__c fcuDC = new FCU_Document_Checklist__c();
                fcuDC.Document_Checklist__c = docId;
                fcuDC.Loan_Application__c = laId;
                fcuDC.Third_Party_Verification__c = fcuTPV.Id;
                lstNewFCUDocChecklist.add(fcuDC);
            }
            
            if(lstNewFCUDocChecklist.size() > 0){
                try{
                	insert lstNewFCUDocChecklist;
                    
                    if(lstLAToUpdate.size() > 0){
                        update lstLAToUpdate;
                    }
                    return 'SUCCESS';
                }
                catch(Exception e){
                    System.debug('Exception==>'+e);
                    return 'ERROR';
                }
            }
        }
        catch(Exception e){
            System.debug('Exception==>'+e);
            return 'ERROR';
        }
        return '';
    }
	
	@AuraEnabled
    public static List<FCU_Document_Checklist__c> fetchDocList(Id tpvId) {
    	//List<Document_Checklist__c> docList = new List<Document_Checklist__c>();
    	List<FCU_Document_Checklist__c> fcuDocList = new List<FCU_Document_Checklist__c>(); //Property_Document_Checklist__c replaced by FCU_Document_Checklist__c and propDocList renamed as fcuDocList by Abhilekh on 6th September 2019 for FCU BRD
    	List<Id> idList = new List<Id>();
        if(String.isNotBlank(tpvId)) {
    		fcuDocList = [SELECT ID,Document_Checklist__c,Document_Checklist__r.Document_Master__r.Name,Document_Checklist__r.Document_Type__r.Name,Third_Party_Verification__c,FCU_Decision__c,Sampled__c,Screened__c FROM FCU_Document_Checklist__c WHERE Third_Party_Verification__c = :tpvId]; //Property_Document_Checklist__c replaced by FCU_Document_Checklist__c by Abhilekh on 6th September 2019 for FCU BRD
    		/*if(!propDocList.isEmpty()) {
                for(Property_Document_Checklist__c pdc: propDocList){
                    idList.add(pdc.Document_Checklist__c);
                }
	        }*/
    	}
        /*if(!idList.isEmpty()){
            docList = [Select Id, Name, Document_Master__r.Name, Document_Collection_Mode__c, Screened_p__c, Sampled_p__c 
                       from Document_Checklist__c where Id =: idList];
        }*/
    	//System.debug('docList:'+docList);
    	return fcuDocList;
    }
	
	//Below method added by Abhilekh on 18th October 2019 for FCU BRD
    @AuraEnabled
    public static String fetchProfileName(){
        return [SELECT Id,Name from Profile WHERE Id =: UserInfo.getProfileId()].Name;
    }
    
    //Below method added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
    @AuraEnabled
    public static List<Document_Checklist__c> fetchDocList1(Id tpvId) {
    	List<Document_Checklist__c> docList = new List<Document_Checklist__c>();
    	List<Property_Document_Checklist__c> propDocList = new List<Property_Document_Checklist__c>();
    	List<Id> idList = new List<Id>();
        if(String.isNotBlank(tpvId)) {
    		propDocList = [SELECT  ID, Document_Checklist__c, Third_Party_Verification__c FROM Property_Document_Checklist__c WHERE Third_Party_Verification__c = :tpvId];
    		if(!propDocList.isEmpty()) {
                for(Property_Document_Checklist__c pdc: propDocList){
                    idList.add(pdc.Document_Checklist__c);
                }
	        }
    	}
        if(!idList.isEmpty()){
            docList = [Select Id, Name, Document_Master__r.Name, Document_Collection_Mode__c, Screened_p__c, Sampled_p__c 
                       from Document_Checklist__c where Id =: idList];
        }
    	System.debug('docList:'+docList);
    	return docList;
    }

    @AuraEnabled
    public static List<ID> queryAttachments (Id docId,Boolean exemptedFromFCUBRD) { //Boolean exemptedFromFCUBRD added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
        List<ContentDocumentLink> links;
        List<ContentVersion> lstContentVersion;
        Set<Id> idSet = new Set<Id>();
        Set<Id> setContentVersionId = new Set<Id>();
        String objectAPIName = '';
        String keyPrefix = '';
        System.debug('Debug Log for docId'+docId);
        keyPrefix = String.valueOf(docId).substring(0,3);
        for( Schema.SObjectType obj : Schema.getGlobalDescribe().Values() ){
             String prefix = obj.getDescribe().getKeyPrefix();
              if(prefix != null && prefix.equals(keyPrefix)){
                        objectAPIName = obj.getDescribe().getName();
                        break;
               }
        }
        System.debug('Debug Log for objectAPIName:'+objectAPIName);
        if(String.isNotBlank(objectAPIName)) {
            if(objectAPIName == 'Document_Checklist__c') {
                links = [select id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where LinkedEntityId=:docId];
                System.debug('Debug Log for links'+links.size());
                System.debug('Debug Log for links'+links);
                
                if (links.isEmpty()) {
                    return null;
                }
                
                Set<Id> contentIds = new Set<Id>();
                
                for (ContentDocumentLink link :links) {
                    contentIds.add(link.ContentDocumentId);
                }
                System.debug('Debug Log for contentIds'+contentIds);
                List<Id> lstsettoList = new List<Id>(contentIds);            
                return lstsettoList;
            }
            
            else if(objectAPIName == 'Third_Party_Verification__c' && exemptedFromFCUBRD == false) { //&& exemptedFromFCUBRD == false added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
                for(FCU_Document_Checklist__c pd: [Select Id, Document_Checklist__c, Loan_Application__c from FCU_Document_Checklist__c where Third_Party_Verification__c=: docId]){ //Property_Document_Checklist__c replaced by FCU_Document_Checklist__c by Abhilekh on 15th September 2019 for FCU BRD
                    idSet.add(pd.Document_Checklist__c);
                }
                List<Document_Checklist__c> lstDocumentList = [SELECT Id FROM Document_Checklist__c WHERE Id IN: idSet];
                Set<Id> setDocumentListIds = new Set<Id>();
                System.debug('Debug Log for lstDocumentList'+lstDocumentList.size());
                if(!lstDocumentList.isEmpty()) {
                    for(Document_Checklist__c objDC : lstDocumentList) {
                        setDocumentListIds.add(objDC.Id);
                    }
                }
                System.debug('Debug Log for setDocumentListIds'+setDocumentListIds.size());
                if(!setDocumentListIds.isEmpty()) {
                    links = [select id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where LinkedEntityId IN:setDocumentListIds];
                    
                    if (links.isEmpty()) {
                        return null;
                    }
                    
                    Set<Id> contentIds = new Set<Id>();
                    
                    for (ContentDocumentLink link :links) {
                        contentIds.add(link.ContentDocumentId);
                    }
                    System.debug('Debug Log for contentIds'+contentIds);
                    List<Id> lstsettoList1 = new List<Id>(contentIds);            
                    return lstsettoList1;
                }
            }
			//Below else if block added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
            else if(objectAPIName == 'Third_Party_Verification__c' && exemptedFromFCUBRD == true){
                for(Property_Document_Checklist__c pd: [Select Id, Document_Checklist__c, Loan_Application__c from Property_Document_Checklist__c where Third_Party_Verification__c=: docId]){
                    idSet.add(pd.Document_Checklist__c);
                }
                List<Document_Checklist__c> lstDocumentList = [SELECT Id FROM Document_Checklist__c WHERE Id IN: idSet];
                Set<Id> setDocumentListIds = new Set<Id>();
                System.debug('Debug Log for lstDocumentList'+lstDocumentList.size());
                if(!lstDocumentList.isEmpty()) {
                    for(Document_Checklist__c objDC : lstDocumentList) {
                        setDocumentListIds.add(objDC.Id);
                    }
                }
                System.debug('Debug Log for setDocumentListIds'+setDocumentListIds.size());
                if(!setDocumentListIds.isEmpty()) {
                    links = [select id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where LinkedEntityId IN:setDocumentListIds];
                    
                    if (links.isEmpty()) {
                        return null;
                    }
                    
                    Set<Id> contentIds = new Set<Id>();
                    
                    for (ContentDocumentLink link :links) {
                        contentIds.add(link.ContentDocumentId);
                    }
                    System.debug('Debug Log for contentIds'+contentIds);
                    List<Id> lstsettoList1 = new List<Id>(contentIds);            
                    return lstsettoList1;
                }
            }
        }
        return new List<Id>();
    }
    @AuraEnabled
    public static List < String > getselectOptions(sObject objObject, string fld) {
        system.debug('objObject --->' + objObject);
        system.debug('fld --->' + fld);
        
        List < String > allOpts = new list < String > ();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objObject.getSObjectType();
        
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        
        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values =
            fieldMap.get(fld).getDescribe().getPickListValues();
        
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        return allOpts;
    }
    @AuraEnabled
    public static List<String> getMode(){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Document_Checklist__c.Document_Collection_Mode__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            options.add(p.getLabel());
        }
        return options;
    }
    @AuraEnabled
    public static List<String> getScreened(){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Document_Checklist__c.Screened__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            options.add(p.getLabel());
        }
        return options;
    }
    @AuraEnabled
    public static List<String> getSampled(){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Document_Checklist__c.Sampled__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            options.add(p.getLabel());
        }
        return options;
    }
    @AuraEnabled
    public static void saveLoan(String paramJSONList, Boolean exemptedFromFCUBRD, Id tpvId) { //Boolean exemptedFromFCUBRD added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
        System.debug('exemptedFromFCUBRD==>'+exemptedFromFCUBRD);
        System.debug('paramJSONList==>'+paramJSONList);
        //Below if block only and else block added by Abhilekh on 22nd December 2019 for FCU BRD Exemption
        if(exemptedFromFCUBRD == false){
            System.debug('Debug Log for param list'+paramJSONList);
            //List<Document_Checklist__c> lstDocchkLst = new List<Document_Checklist__c>();
            //List<Document_Checklist__c> lockedDocLst = new List<Document_Checklist__c>();
            List<DocumentListWrapper> lstWrapper = new List<DocumentListWrapper>();
            lstWrapper.add(new DocumentListWrapper('',null,''));
            List<FCU_Document_Checklist__c> lstSerializedWrapper = (List<FCU_Document_Checklist__c>)JSON.deserialize(paramJSONList,List<FCU_Document_Checklist__c>.class); //Document_Checklist__c replaced by FCU_Document_Checklist__c by Abhilekh on 11th October 2019 for FCU BRD
            System.debug('Debug Log for deserialized List'+lstSerializedWrapper.size());
            System.debug('Debug Log for deserialized List'+lstSerializedWrapper);
			Boolean FCUDecisionChange = false;
            
            for(FCU_Document_Checklist__c fcuDC: lstSerializedWrapper){
                if(fcuDC.FCU_Decision__c != '' && fcuDC.FCU_Decision__c != null && (fcuDC.Screened__c == '' || fcuDC.Screened__c == 'No' || fcuDC.Screened__c == null)){
                    fcuDC.FCU_Decision__c = '';
                }
				if(fcuDC.FCU_Decision__c != '' && fcuDC.FCU_Decision__c != null){
                    FCUDecisionChange = true;
                }
            }
            
            if(!lstSerializedWrapper.isEmpty()) {
                if(FCUDecisionChange == true){
                    Third_Party_Verification__c tpv = new Third_Party_Verification__c();
                    tpv.Id = tpvId;
                    tpv.Report_Status_Changed_by_Docs__c = true;
                    update tpv;
                }
                
                Database.Saveresult[] updateList = Database.update(lstSerializedWrapper,false);
                
                for (Database.SaveResult sr : updateList) {
                    if (sr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully updated Document Checklist. Record ID: ' + sr.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Fields that affected this error: ' + err.getFields());
                            lstWrapper[0].errorMsg = err.getMessage();
                        }
                        // return lstWrapper;
                    }
                }
            }
        }
        
        else{
            System.debug('Debug Log for param list else==>'+paramJSONList);
            List<Document_Checklist__c> lstDocchkLst = new List<Document_Checklist__c>();
            List<Document_Checklist__c> lockedDocLst = new List<Document_Checklist__c>();
            List<DocumentListWrapper> lstWrapper = new List<DocumentListWrapper>();
            lstWrapper.add(new DocumentListWrapper('',null,''));
            List<Document_Checklist__c> lstSerializedWrapper = (List<Document_Checklist__c>)JSON.deserialize(paramJSONList,List<Document_Checklist__c>.class);
            System.debug('Debug Log for deserialized List'+lstSerializedWrapper.size());
            System.debug('Debug Log for deserialized List'+lstSerializedWrapper);
            if(!lstSerializedWrapper.isEmpty()) {
                Database.Saveresult[] updateList = Database.update(lstSerializedWrapper,false);
                for (Database.SaveResult sr : updateList) {
                    if (sr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully updated Document Checklist. Record ID: ' + sr.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Fields that affected this error: ' + err.getFields());
                            lstWrapper[0].errorMsg = err.getMessage();
                        }
                        // return lstWrapper;
                    }
                }
            }
        }
        //return lstSerializedWrapper;
        //return null;
    }
    public class DocumentListWrapper {
        @AuraEnabled public String strContactName;
        @AuraEnabled public List<Document_Checklist__c> lstDocumentCheckList;
        @AuraEnabled public String errorMsg;
        
        public DocumentListWrapper(List<Document_Checklist__c> paramListDocChkLst) {
            this.lstDocumentCheckList = paramListDocChkLst;
        }
        
        public DocumentListWrapper(String paramContactName, List<Document_Checklist__c> paramListDocChkLst, String errorMsg) {
            this.strContactName = paramContactName;
            this.lstDocumentCheckList = paramListDocChkLst;
            this.errorMsg = errorMsg;
        }
    }
}