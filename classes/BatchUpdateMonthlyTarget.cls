/*
@@author : Deloitte Team
@@Created Date : 17th June 2019
@@Description : Daily batch to update child monthly target records with parent target record values
*/

global class BatchUpdateMonthlyTarget implements Database.Batchable<sObject>, Schedulable{
    global Database.QueryLocator start(Database.BatchableContext BC)    {  
       String strQuery = ' SELECT Id,  Actual_Amount__c, Actual_Disbursed_X_ROI__c, Actual_Insurance__c, Actual_PF__c, Branch__c, LOB__c,' ;
              strQuery += 'Month__c, Sales_Person__c, SM_CBM__c, SM_Category__c, Target_Amount__c, Target_Count__c, Target__c,' ;
              strQuery += 'Target_Yield__c, Amount_Achieved__c, Actual_Count__c, Case_Achieved__c, Total_Insurance_Count__c, Actual_Login__c, Actual_Login_Amount__c, '; 
              strQuery += 'Count_Penetration__c, Actual_Insurance_P__c, Actual_PF_of_disbursed_Amt__c, Sales_User__c, Disbursed_Yield__c,';
              strQuery += 'Actual_LMTD_Amount__c, Actual_LMTD_Count__c, Actual_LMTD_Disbursed_X_ROI__c,';
              strQuery += 'Actual_M_2_Amt__c, Actual_M_2__c, Actual_M_2_Disbursed_X_ROI__c, ';
              strQuery += 'FTD_Amt__c, FTD__c, FTD_Disbursed_X_ROI__c, ';
                           
              strQuery += 'Target__r.Sales_User__c, Target__r.Target_Count__c, Target__r.Channel_Partner__c, Target__r.Target_Amount__c,';
              strQuery += 'Target__r.Actual_Count__c, Target__r.Actual_Amount__c, Target__r.Sales_Person__c, ';
              strQuery += 'Target__r.Branch__c, Target__r.Target_Yield__c, Target__r.Month__c, Target__r.LOB__c, Target__r.SM_Category__c,'; 
              strQuery += 'Target__r.SM_CBM__c, Target__r.Case_Achieved__c, Target__r.Amount_Achieved__c, Target__r.Actual_PF__c, ';
              strQuery += 'Target__r.Actual_PF_of_disbursed_Amt__c, Target__r.Actual_Insurance__c, Target__r.Actual_Insurance_P__c, ';
              strQuery += 'Target__r.Actual_Disbursed_X_ROI__c, Target__r.Actual_Login_Amount__c, Target__r.Actual_Login__c, ';
              strQuery += 'Target__r.Disbursed_Yield__c, Target__r.Total_Insurance_Count__c, Target__r.Count_Penetration__c, ';
              strQuery += 'Target__r.Actual_LMTD_Amount__c, Target__r.Actual_LMTD_Count__c, Target__r.Actual_LMTD_Disbursed_X_ROI__c, ';
              strQuery += 'Target__r.Actual_M_2_Amt__c, Target__r.Actual_M_2__c, Target__r.Actual_M_2_Disbursed_X_ROI__c, ';
              strQuery += 'Target__r.FTD_Amt__c, Target__r.FTD__c, Target__r.FTD_Disbursed_X_ROI__c';             
              strQuery += ' FROM Monthly_Target__c where Target__c != NULL'; 
              
              system.debug( '@@strQuery'  + strQuery );             
       return Database.getQueryLocator(strQuery);
        
    }
    
    //Mapping all values of Parent target to child monthly target record
    global void execute(Database.BatchableContext BC, List<Monthly_Target__c> scope){
        List<Monthly_Target__c> lstMonTarg = new List<Monthly_Target__c>();
        for ( Monthly_Target__c objMonTarg : scope ) {
            if ( objMonTarg.Target__c != null ) {
                objMonTarg.Actual_Amount__c = objMonTarg.Target__r.Actual_Amount__c;
                objMonTarg.Actual_Count__c = objMonTarg.Target__r.Actual_Count__c;
                objMonTarg.Actual_Disbursed_X_ROI__c = objMonTarg.Target__r.Actual_Disbursed_X_ROI__c;
                objMonTarg.Actual_PF__c = objMonTarg.Target__r.Actual_PF__c;
                objMonTarg.Actual_Insurance__c = objMonTarg.Target__r.Actual_Insurance__c;
                objMonTarg.Branch__c = objMonTarg.Target__r.Branch__c;
                //objMonTarg.RSM__c = objMonTarg.Target__r.RSM__c;
                objMonTarg.Sales_Person__c   = objMonTarg.Target__r.Sales_Person__c ;
                objMonTarg.SM_CBM__c = objMonTarg.Target__r.SM_CBM__c;
                objMonTarg.Total_Insurance_Count__c  = objMonTarg.Target__r.Total_Insurance_Count__c;
                objMonTarg.Target_Amount__c = objMonTarg.Target__r.Target_Amount__c;
                objMonTarg.Target_Count__c = objMonTarg.Target__r.Target_Count__c;
                objMonTarg.Target_Yield__c = objMonTarg.Target__r.Target_Yield__c;
                objMonTarg.SM_Category__c = objMonTarg.Target__r.SM_Category__c;
                objMonTarg.LOB__c = objMonTarg.Target__r.LOB__c;
                objMonTarg.Month__c = objMonTarg.Target__r.Month__c;
                objMonTarg.Actual_Login__c = objMonTarg.Target__r.Actual_Login__c;
                objMonTarg.Actual_Login_Amount__c = objMonTarg.Target__r.Actual_Login_Amount__c;
                objMonTarg.Actual_LMTD_Amount__c = objMonTarg.Target__r.Actual_LMTD_Amount__c;
                objMonTarg.Actual_LMTD_Count__c = objMonTarg.Target__r.Actual_LMTD_Count__c;
                objMonTarg.Actual_LMTD_Disbursed_X_ROI__c = objMonTarg.Target__r.Actual_LMTD_Disbursed_X_ROI__c;
                objMonTarg.Actual_M_2_Amt__c = objMonTarg.Target__r.Actual_M_2_Amt__c;
                objMonTarg.Actual_M_2__c = objMonTarg.Target__r.Actual_M_2__c;
                objMonTarg.Actual_M_2_Disbursed_X_ROI__c = objMonTarg.Target__r.Actual_M_2_Disbursed_X_ROI__c;
                objMonTarg.FTD_Amt__c = objMonTarg.Target__r.FTD_Amt__c;
                objMonTarg.FTD__c = objMonTarg.Target__r.FTD__c;
                objMonTarg.FTD_Disbursed_X_ROI__c = objMonTarg.Target__r.FTD_Disbursed_X_ROI__c;
            }
            
            lstMonTarg.add(objMonTarg);
        }
        
        update lstMonTarg;
    }
      
        
    global void finish(Database.BatchableContext BC){
    
    }
    
    global void execute (SchedulableContext SC) {
        Database.executeBatch(new batchUpdateMonthlyTarget (), 200);
    }
}