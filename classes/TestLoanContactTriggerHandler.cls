@isTest
private class TestLoanContactTriggerHandler {
    
        static testmethod void test1(){
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
        insert acc;
        
        
        //Contact con = new Contact(LastName = 'Test Contact', AccountId = acc.Id);
        //insert con;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
        insert con1;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
         
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert objLoanApplication;
        Loan_Contact__c lc = [SELECT Id, Name,Applicant_Type__c, Applicant_Status__c, Constitution__c, Customer_segment__c, Loan_Applications__c FROM Loan_Contact__c WHERE Loan_Applications__c =: objLoanApplication.Id];
        //lc.Applicant_Type__c='Co- Applicant';
        lc.Borrower__c='2';
        lc.Contact__c = con1.Id;
        lc.Constitution__c='18';
        lc.Applicant_Status__c='1';
        lc.Customer_segment__c='9';
        //lc.Income_Program_Type__c = 'Normal Salaried';
        //lc.Gender__c = 'M';
        //lc.Category__c ='1';
        //lc.Religion__c = '1';
        lc.Mobile__c='9876543210';
        lc.Email__c='testemail@gmail.com';
        //lc.Customer__c = acc.id;    
        update lc;
        test.startTest();
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Co-Applicant').getRecordTypeId();      
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc.Id,
                                                             Applicant_Type__c='Guarantor',Borrower__c='1',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='Brother',
                                                             Pan_Verification_status__c=true,Constitution__c='20',
                                                             Customer_segment__c='1',Applicant_Status__c='1',
                               Category__c='1',
                                                             Father_s_Husband_s_Name__c='ABC',
                                                             GSTIN_Number__c='04AATPB2258C1Z3',
                               Mobile__c='9978786756',Email__c='avd@g.com');
        insert objLoanContact;
        
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=objLoanApplication.Id,
                                                                     Loan_Contact__c=objLoanContact.Id);
        insert objDocCheck;
        
        Borrower_Document_Checklist__c objBorrowDocCheck = new Borrower_Document_Checklist__c();
        objBorrowDocCheck.Applicant_Type__c = 'Co- Applicant';
        objBorrowDocCheck.Applicant_Status__c = '1';
        objBorrowDocCheck.Customer_Type__c = '1';
        //objBorrowDocCheck.Customer_Segment__c = '3';
        objBorrowDocCheck.Income_program_typeq__c= '';
        objBorrowDocCheck.Customer_Type__c = '';
        objBorrowDocCheck.Constitution__c = '20';
        insert objBorrowDocCheck;
        
       
        
        Loan_Contact__c objLoanContactUpdated = new Loan_Contact__c(Id=objLoanContact.Id,Applicant_Type__c='Guarantor');
        update objLoanContactUpdated;
        
        List<Loan_Contact__c> lstLoanCon = new List<Loan_Contact__c>();
        lstLoanCon.add(objLoanContact);
        
        Map<Id,Loan_Contact__c> mapLoanCon = new Map<Id,Loan_Contact__c>();
        mapLoanCon.put(objLoanContact.Id,objLoanContact);
        
        List<Loan_Contact__c> newlstLoanCon = new List<Loan_Contact__c>();
        newlstLoanCon.add(objLoanContactUpdated);
        
        Map<Id,Loan_Contact__c> newmapLoanCon = new Map<Id,Loan_Contact__c>();
        newmapLoanCon.put(objLoanContactUpdated.Id,objLoanContactUpdated);
           
        LoanContactTriggerHandler.AfterInsert(lstLoanCon,mapLoanCon);
        
        LoanContactTriggerHandler.afterUpdate(newlstLoanCon, lstLoanCon, newmapLoanCon, mapLoanCon);
            
        LoanContactTriggerHandler.beforeDelete(mapLoanCon);
        LoanContactTriggerHandler.afterDelete(mapLoanCon);
        LoanContactTriggerHandler.updateRecordTypeLoanContactDetails(lstLoanCon,mapLoanCon);
        test.stopTest();
              
    }
    
    static testmethod void test2(){
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Name='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786756',RecordTypeId=RecordTypeIdAccount);
        insert acc;
        
        Contact con = new Contact(LastName = 'Test Contact', MobilePhone='8234567890',AccountId = acc.Id);
        insert con;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account3',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786755',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact con1 = new Contact(LastName = 'Test Contact',MobilePhone='7234567890', AccountId = acc1.Id);
        insert con1;
        
         Account acc2 = new Account(PAN__c='ABCDE5234G',Name='Test Account4',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786754',RecordTypeId=RecordTypeIdAccount);
        insert acc2;
        
        Contact con2 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890',AccountId = acc2.Id);
        insert con2;
        
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',HasCorporateCustomer__c = True,Insurance_Loan_application__c=true);
        insert objLoanApplication;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Co-Applicant').getRecordTypeId();
        
        Loan_Contact__c objOldLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc1.Id,Contact__c = con1.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='2',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='19',Nature_of_Business__c='M',
                                                             Customer_segment__c='11',Income_Program_Type__c='Low LTV Gross Margin',Applicant_Status__c='1');
        insert objOldLoanContact;
        
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc2.Id,Contact__c = con2.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='2',
                                                             RecordTypeId=RecordTypeIdLoanContact,ShareHolding_Percentage__c = 10,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='21',Nature_of_Business__c='M',
                                                             Customer_segment__c='9',Income_Program_Type__c='Liquid Income Program',Applicant_Status__c='1');
        insert objLoanContact;
        
        List<Loan_Contact__c> lstLoanCon = new List<Loan_Contact__c>();
      
        
        objLoanContact.GSTIN_Number__c = '19AAACB5343E1Z3';
        objLoanContact.Copy_of_GSTIN__c = '19AAACB5343E1Z3';
        //objLoanContact.Customer_segment__c ='1';
        objOldLoanContact.GSTIN_Number__c = '19AAACB5343E1Z2';
        objOldLoanContact.Copy_of_GSTIN__c = '19AAACB5343E1Z2';
		//objOldLoanContact.Customer_segment__c ='3' ;
        objOldLoanContact.Final_Decision__c = 'Red';
        lstLoanCon.add(objLoanContact);
        lstLoanCon.add(objOldLoanContact);
        update lstLoanCon;
        
        
        Map<Id,Loan_Contact__c> mapLoanCon = new Map<Id,Loan_Contact__c>();
        mapLoanCon.put(objLoanContact.Id,objLoanContact);
        mapLoanCon.put(objOldLoanContact.Id,objOldLoanContact);
        Map<Id,Loan_Contact__c> oldmapLoanCon = new Map<Id,Loan_Contact__c>();
        objLoanContact.GSTIN_Number__c = '19AAACB5343E1Z9';
        objLoanContact.Copy_of_GSTIN__c = '19AAACB5343E1Z9';
        objLoanContact.Final_Decision__c  = 'Green';
        objLoanContact.Customer_segment__c  = '13';
        oldmapLoanCon.put(objLoanContact.Id,objLoanContact);
        oldmapLoanCon.put(objOldLoanContact.Id,objOldLoanContact);
        LoanContactTriggerHandler.beforeUpdate(lstLoanCon,oldmapLoanCon.values(),mapLoanCon,oldmapLoanCon);
        LoanContactTriggerHandler.updateRecordTypeLoanContactDetails(lstLoanCon,mapLoanCon);
        LoanContactTriggerHandler.beforeDelete(mapLoanCon);
        
        for (Id objid : oldmapLoanCon.keyset()) {
            if (oldmapLoanCon.get(objid) == null)
                oldmapLoanCon.remove(objid);
        }
        
        
        
    }
    
    static testmethod void test3(){
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Id RecordTypeIdCustIntegration = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('GSTIN').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',FirstName='Test',Lastname='Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786756',RecordTypeId=RecordTypeIdAccount1 );
                      
        insert acc;
        
        Account acc3 = new Account(PAN__c='ABCDE1334F',FirstName='Test',Lastname='Account3',Date_of_Incorporation__c=System.today(),
                                  Phone='9989736756',RecordTypeId=RecordTypeIdAccount1 );
        insert acc3;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account3',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786755',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact con1 = new Contact(LastName = 'Test Contact',MobilePhone='7234567890', AccountId = acc1.Id);
        insert con1;
        
         Account acc2 = new Account(PAN__c='ABCDE5234G',Name='Test Account4',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786754',RecordTypeId=RecordTypeIdAccount);
        insert acc2;
        
        Contact con2 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890',AccountId = acc2.Id);
        insert con2;
        
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',Sub_Stage__c = Constants.Credit_Review);
        insert objLoanApplication;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Co-Applicant').getRecordTypeId();
        
        List<Sector_Vise_Mandatory_Fields__mdt> sectorRequiredFieldsDataList = [SELECT Industry__c,Sub_Industry__c,Service__c,Service_Industry_Type__c,Trading_Type__c,Skill_Level__c,Activity_Type__c,Platform_Type__c,Sector__c, Active__c from Sector_Vise_Mandatory_Fields__mdt where Active__c = true ];
        
        List<Loan_Contact__c> lstLoanCon = new List<Loan_Contact__c>();
      	test.startTest();
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc3.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='1',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='20',Nature_of_Business__c='M',
                                                             Customer_segment__c='1',Income_Program_Type__c='FLIP',Applicant_Status__c='1');
        
        
        objLoanContact.GSTIN_Number__c = '19AAACB5343E1Z3';
        objLoanContact.Copy_of_GSTIN__c = '19AAACB5343E1Z3';
        objLoanContact.Final_Decision__c = 'Red';
        lstLoanCon.add(objLoanContact);
        insert lstLoanCon;
        Loan_Contact__c loanContact = [Select Id from Loan_Contact__c where GSTIN_Number__c ='19AAACB5343E1Z3' ];
        Customer_Integration__c cust = new Customer_Integration__c(Loan_Application__c = objLoanApplication.Id, Loan_Contact__c = loanContact.id,Status__c = 'GSTIN Changed',RecordTypeId=RecordTypeIdCustIntegration,Constitution__c ='Test', GST_Tax_payer_type__c ='Test',Current_Status_of_Registration_Under_GST__c='Test',VAT_Registration_Number__c='1111',Email__c='test@test.com',Mobile__c='9999999999',GSTIN__c='19AAACB5343E1Z9',PAN_Number__c='AHGJO4371L',Trade_Name__c='Test',Legal_Name__c='test',Filling_Frequency__c='test',Business_Activity__c='Test',GST_Date_Of_Registration__c=system.today(),Cumulative_Sales_Turnover__c=111,Month_Count__c=1 );
        //insert cust;
        
        Map<Id,Loan_Contact__c> mapLoanCon = new Map<Id,Loan_Contact__c>();
        mapLoanCon.put(objLoanContact.Id,objLoanContact);
        
        Map<Id,Loan_Contact__c> oldmapLoanCon = new Map<Id,Loan_Contact__c>();
        objLoanContact.GSTIN_Number__c = '19AAACB5343E1Z9';
        objLoanContact.Copy_of_GSTIN__c = '19AAACB5343E1Z9';
        objLoanContact.Final_Decision__c  = 'Green';
        objLoanContact.BRE11_Success__c = True;
        objLoanContact.Pan_Verification_status__c = TRUE;
        oldmapLoanCon.put(objLoanContact.Id,objLoanContact);
        
        LoanContactTriggerHandler.updateRecordTypeLoanContactDetails(lstLoanCon,mapLoanCon);
        update objLoanContact ;
        Delete lstLoanCon;
        test.stopTest();
        
    }
}