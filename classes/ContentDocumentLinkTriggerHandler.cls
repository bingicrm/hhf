public class ContentDocumentLinkTriggerHandler {
	
    public static void afterInsert(List<ContentDocumentLink> newList){
        Set<Id> docId = new Set<Id>();
        Set<Id> tpvId = new Set<Id>();
        
        for(ContentDocumentLink cdl: newList){
            String objectAPIName;
            String keyPrefix = String.valueOf(cdl.LinkedEntityId).substring(0,3);
            for( Schema.SObjectType obj : Schema.getGlobalDescribe().Values() ){
                String prefix = obj.getDescribe().getKeyPrefix();
                if(prefix != null && prefix.equals(keyPrefix)){
                    objectAPIName = obj.getDescribe().getName();
                    break;
                }
            }
            System.debug('Debug Log for objectAPIName:'+objectAPIName);
            
            if(String.isNotBlank(objectAPIName) && objectAPIName == 'Document_Checklist__c'){
                docId.add(cdl.LinkedEntityId);
            }
            
            if(String.isNotBlank(objectAPIName) && objectAPIName == 'Third_Party_Verification__c'){
                tpvId.add(cdl.LinkedEntityId);
            }
        }
        
        if(docId.size() > 0){
            ContentDocumentLinkTriggerHelper.updateFCUDoneInDocChecklist(docId);
        }
        
        if(tpvId.size() > 0){
            ContentDocumentLinkTriggerHelper.updateFCUReportUploadedInTPV(tpvId);
        }
    }
}