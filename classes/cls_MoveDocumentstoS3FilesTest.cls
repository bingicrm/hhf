@isTest
public class cls_MoveDocumentstoS3FilesTest {
    
    public static testMethod void test1(){
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
        insert con1;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;        

        Loan_Application__c la = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert la;
        Test.startTest();
        Loan_Contact__c lc = [SELECT Id, Name,Applicant_Type__c, Applicant_Status__c, Constitution__c, Customer_segment__c, Loan_Applications__c FROM Loan_Contact__c WHERE Loan_Applications__c =: la.Id];
        System.debug('lc-CV-'+lc);
        lc.Borrower__c='2';
        lc.Contact__c = con1.Id;
        lc.Constitution__c='18';
        lc.Applicant_Status__c='1';
        lc.Customer_segment__c='9';
        lc.Mobile__c='9876543210';
        lc.Email__c='testemail@gmail.com';
        update lc;
        
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=la.Id,
                                                                     Loan_Contact__c=lc.Id);
        insert objDocCheck;
        System.debug('objDocCheck-cv-'+objDocCheck);    
        
         Document_Migration_Log__c objDoc = new Document_Migration_Log__c ();
        objDoc.Description__c='Test';
        objDoc.Loan_Application__c=la.Id;
        objDoc.Document_Checklist__c = objDocCheck.Id;
        //objDoc.Document_Id__c = '43434';
        insert objDoc;
        
        //Create Document
        ContentVersion cv1 = new ContentVersion();
        cv1.Title = 'Test Document';
        cv1.PathOnClient = 'TestDocument.pdf';
        cv1.VersionData = Blob.valueOf('Test Content');
        cv1.IsMajorVersion = true;
        //cv1.ContentDocumentId = objDoc.Id;
        Insert cv1;
        
        cv1=[SELECT Id, Title, VersionData, CreatedDate, ContentDocumentId, FileExtension,IsLatest FROM ContentVersion];
       	System.debug('cv--SOQL--'+cv1);     
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv1.Id].ContentDocumentId;
      	 System.debug('cv--conDocId--'+conDocId);  
        
        //Create ContentDocumentLink 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = objDocCheck.Id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        Insert cdl;
        
         //Create ContentDocumentLink 
        ContentDocumentLink cdl1 = New ContentDocumentLink();
        cdl1.LinkedEntityId = la.Id;//objDocCheck.Id;
        cdl1.ContentDocumentId = conDocId;
        cdl1.shareType = 'V';
        Insert cdl1;
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        cv.ContentDocumentId = cdl.ContentDocumentId;// objDocCheck.Id;//.Document_Id__c;
        Insert cv;
            
       	System.debug('cv--ContentVersion--'+cv);     
         
        //Get Content Documents
       /* cv=[SELECT Id, Title, VersionData, CreatedDate, ContentDocumentId, FileExtension,IsLatest FROM ContentVersion];
       	System.debug('cv--SOQL--'+cv);     
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
      	 System.debug('cv--conDocId--'+conDocId);     
        
        */
         Attachment att=new Attachment();
        att.ParentId = la.Id ;
        att.Name='test';
        att.Body=Blob.valueOf('test');
        //att.Id =: objDoc.Document_Id__c;
        insert att;
        
        
        cls_MoveDocumentstoS3Files.createS3FilesApplicant(la);
       // cls_MoveDocumentstoS3Files.createS3FilesApplication(la);
       /* cls_MoveDocumentstoS3Files.createS3FilesCoApplicantGuarantor(la);
        cls_MoveDocumentstoS3Files.createS3FilesLoanKit(la);
        cls_MoveDocumentstoS3Files.createS3FilesLoanKit2(la);*/
        Test.stopTest();
        
        
    }
    
    public static testMethod void test2(){
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
        insert con1;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;        

        Loan_Application__c la = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert la;
        Test.startTest();
        Loan_Contact__c lc = [SELECT Id, Name,Applicant_Type__c, Applicant_Status__c, Constitution__c, Customer_segment__c, Loan_Applications__c FROM Loan_Contact__c WHERE Loan_Applications__c =: la.Id];
        System.debug('lc-CV-'+lc);
        lc.Borrower__c='2';
        lc.Contact__c = con1.Id;
        lc.Constitution__c='18';
        lc.Applicant_Status__c='1';
        lc.Customer_segment__c='9';
        lc.Mobile__c='9876543210';
        lc.Email__c='testemail@gmail.com';
        update lc;
        
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=la.Id/*,
                                                                     Loan_Contact__c=lc.Id*/);
        insert objDocCheck;
        System.debug('objDocCheck-cv-'+objDocCheck);    
        
         Document_Migration_Log__c objDoc = new Document_Migration_Log__c ();
        objDoc.Description__c='Test';
        objDoc.Loan_Application__c=la.Id;
        objDoc.Document_Checklist__c = objDocCheck.Id;
        //objDoc.Document_Id__c = '43434';
        insert objDoc;
        
        //Create Document
        ContentVersion cv1 = new ContentVersion();
        cv1.Title = 'Test Document';
        cv1.PathOnClient = 'TestDocument.pdf';
        cv1.VersionData = Blob.valueOf('Test Content');
        cv1.IsMajorVersion = true;
        //cv1.ContentDocumentId = objDoc.Id;
        Insert cv1;
        
        cv1=[SELECT Id, Title, VersionData, CreatedDate, ContentDocumentId, FileExtension,IsLatest FROM ContentVersion];
       	System.debug('cv--SOQL--'+cv1);     
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv1.Id].ContentDocumentId;
      	 System.debug('cv--conDocId--'+conDocId);  
        
        //Create ContentDocumentLink 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = objDocCheck.Id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        Insert cdl;
        
         //Create ContentDocumentLink 
        ContentDocumentLink cdl1 = New ContentDocumentLink();
        cdl1.LinkedEntityId = la.Id;//objDocCheck.Id;
        cdl1.ContentDocumentId = conDocId;
        cdl1.shareType = 'V';
        Insert cdl1;
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        cv.ContentDocumentId = cdl.ContentDocumentId;// objDocCheck.Id;//.Document_Id__c;
        Insert cv;
            
       	System.debug('cv--ContentVersion--'+cv);     
         
        //Get Content Documents
       /* cv=[SELECT Id, Title, VersionData, CreatedDate, ContentDocumentId, FileExtension,IsLatest FROM ContentVersion];
       	System.debug('cv--SOQL--'+cv);     
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
      	 System.debug('cv--conDocId--'+conDocId);     
        
        */
         Attachment att=new Attachment();
        att.ParentId = la.Id ;
        att.Name='test';
        att.Body=Blob.valueOf('test');
        //att.Id =: objDoc.Document_Id__c;
        insert att;
        
        
        cls_MoveDocumentstoS3Files.createS3FilesApplicant(la);
        cls_MoveDocumentstoS3Files.createS3FilesApplication(la);
       /* cls_MoveDocumentstoS3Files.createS3FilesCoApplicantGuarantor(la);
        cls_MoveDocumentstoS3Files.createS3FilesLoanKit(la);
        cls_MoveDocumentstoS3Files.createS3FilesLoanKit2(la);*/
        Test.stopTest();
        
        
    }
    
    public static testMethod void test3(){
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
        insert con1;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;        

        Loan_Application__c la = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert la;
        
        Loan_Contact__c lc = [SELECT Id, Name,Applicant_Type__c, Applicant_Status__c, Constitution__c, Customer_segment__c, Loan_Applications__c FROM Loan_Contact__c WHERE Loan_Applications__c =: la.Id];
        System.debug('lc-CV-'+lc);
        lc.Borrower__c='2';
        lc.Contact__c = con1.Id;
        lc.Constitution__c='18';
        lc.Applicant_Status__c='1';
        lc.Customer_segment__c='9';
        lc.Mobile__c='9876543210';
        lc.Email__c='testemail@gmail.com';
        update lc;
        
       /* Loan_Contact__c loanContact2 = new Loan_Contact__c(Customer__c = acc.Id);
        loanContact2.Loan_Applications__c = la.Id;
        loanContact2.Applicant_Type__c = Constants.COAPP;
        insert loanContact2;*/
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Co-Applicant').getRecordTypeId();
        Test.startTest();
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=la.Id,Customer__c=acc.Id,
                                                             Applicant_Type__c='Guarantor',Borrower__c='1',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='Brother',
                                                             Pan_Verification_status__c=true,Constitution__c='20',
                                                             Customer_segment__c='1',Applicant_Status__c='1',
                                                             Category__c='1',
                                                             Father_s_Husband_s_Name__c='ABC',
                                                             GSTIN_Number__c='04AATPB2258C1Z3',
                               								 Mobile__c='9978786756',Email__c='avd@g.com');
        insert objLoanContact;
        
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=la.Id,
                                                                     Loan_Contact__c=objLoanContact.Id);
        insert objDocCheck;
        System.debug('objDocCheck-cv-'+objDocCheck);    
        
        Document_Migration_Log__c objDoc = new Document_Migration_Log__c ();
        objDoc.Description__c='Test';
        objDoc.Loan_Application__c=la.Id;
        objDoc.Document_Checklist__c = objDocCheck.Id;
        //objDoc.Document_Id__c = '43434';
        insert objDoc;
        
        //Create Document
        ContentVersion cv1 = new ContentVersion();
        cv1.Title = 'Test Document';
        cv1.PathOnClient = 'TestDocument.pdf';
        cv1.VersionData = Blob.valueOf('Test Content');
        cv1.IsMajorVersion = true;
        //cv1.ContentDocumentId = objDoc.Id;
        Insert cv1;
        
        cv1=[SELECT Id, Title, VersionData, CreatedDate, ContentDocumentId, FileExtension,IsLatest FROM ContentVersion];
       	System.debug('cv--SOQL--'+cv1);     
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv1.Id].ContentDocumentId;
      	System.debug('cv--conDocId--'+conDocId);  
        
        //Create ContentDocumentLink 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = objDocCheck.Id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        Insert cdl;
        
         //Create ContentDocumentLink 
        ContentDocumentLink cdl1 = New ContentDocumentLink();
        cdl1.LinkedEntityId = la.Id;//objDocCheck.Id;
        cdl1.ContentDocumentId = conDocId;
        cdl1.shareType = 'V';
        Insert cdl1;
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        cv.ContentDocumentId = cdl.ContentDocumentId;// objDocCheck.Id;//.Document_Id__c;
        Insert cv;
            
       	System.debug('cv--ContentVersion--'+cv);     
         
        //Get Content Documents
       /* cv=[SELECT Id, Title, VersionData, CreatedDate, ContentDocumentId, FileExtension,IsLatest FROM ContentVersion];
       	System.debug('cv--SOQL--'+cv);     
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
      	 System.debug('cv--conDocId--'+conDocId);     
        
        */
         Attachment att=new Attachment();
        att.ParentId = la.Id ;
        att.Name='test';
        att.Body=Blob.valueOf('test');
        //att.Id =: objDoc.Document_Id__c;
        insert att;
        
        
        //cls_MoveDocumentstoS3Files.createS3FilesApplicant(la);
        //cls_MoveDocumentstoS3Files.createS3FilesApplication(la);
        cls_MoveDocumentstoS3Files.createS3FilesCoApplicantGuarantor(la);
        cls_MoveDocumentstoS3Files.createS3FilesLoanKit(la);
        cls_MoveDocumentstoS3Files.createS3FilesLoanKit2(la);
        Test.stopTest();
        
        
    }

}