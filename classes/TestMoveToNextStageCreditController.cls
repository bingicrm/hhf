@isTest
public class TestMoveToNextStageCreditController{
    public static testMethod void MoveToNextStageCreditController(){
        //Profile p=[SELECT id,name from profile where name=:'Sales Team'];
       //  UserRole r = [Select Id From UserRole Where PortalType = 'None' and Name = 'RSM - Delhi 1'Limit 1];
        Profile p=[SELECT id,name from profile where name=:'Sales Team'];
         List<User> lstBranchManager= new List<User>();                          
          User u1 = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',Role_in_Sales_Hierarchy__c = 'SM'
           // UserRoleId = r.Id
        );
        lstBranchManager.add(u1);
        system.debug('u1:::'+u1);
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
           // UserRoleId = r.Id
            ManagerID = u1.id
        );
         lstBranchManager.add(u);
		Profile fcuManagerProfile = [SELECT Id from Profile WHERE Name='FCU Manager' LIMIT 1];
        List<User> lstUser = new List<User>();
        Hierarchy__c objHierarchy = new Hierarchy__c();
        /*objHierarchy.BCM__c = u2.Id;
        objHierarchy.ZCM__c = u4.Id;
        objHierarchy.CCM__c = u2.Id;
        objHierarchy.CRO__c = u2.Id;*/
        insert objHierarchy;
        List<Hierarchy__c> lstHierarchy = [Select Id, Name, Active__c, BCM__c, BH__c, CCM__c, CRO__c, NCM__c, ZCM__c, ZCM_Salaried__c from Hierarchy__c where Active__c = true];
        String HierarchyName = lstHierarchy[0].Name; 
        User fcuManager = new User(Username ='fcumanager@test.com',ProfileId = fcuManagerProfile.Id,
                              Alias = 'fcum123',Email = 'fcumanager@test.com',EmailEncodingKey = 'UTF-8',
                              LastName = 'Manager',CommunityNickname = 'fcumanager12345',TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US',FCU__c = TRUE,Hierarchy__c = HierarchyName,
            	     Non_Salaried_Authority__c = 'A2',
            		 Salaried_Authority__c = 'A2',
            		 Role_in_Credit_Hierarchy__c = 'BCM');
        SYstem.debug('u.ManagerID!!'+u.ManagerID);
		         lstBranchManager.add(fcuManager);

        insert lstBranchManager;

        User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                         Title,Alias,TimeZoneSidKey,
                         EmailEncodingKey,LanguageLocaleKey,
                         LocaleSidKey,UserRoleId 
                         from user where ID=:UserInfo.getUserId()];
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=lstBranchManager[0].Id;
        loggedUser.ManagerID=lstBranchManager[0].Id;
        update loggedUser;
        system.debug('loggedUser@@@'+loggedUser);
         Account custObj = new Account();
        custObj.Salutation='Mr';
        custObj.FirstName='Test';
        custObj.LastName = 'Test Customer';
        custObj.Phone = '9999999998';
        database.insert(custObj);
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        schemeObj.Max_LTV__c = 200;
        database.insert(schemeObj);
        
        Test.startTest(); 
        
       // Id RecordTypeIdLA = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Legal').getRecordTypeId();

        Loan_Application__c loanAppObj = new Loan_Application__c();
        loanAppObj.customer__c = custObj.id;
        loanAppObj.Loan_Application_Number__c = 'LA00001';
        loanAppObj.Loan_Purpose__c = '11';
        loanAppObj.scheme__c = schemeObj.id;
        loanAppObj.Transaction_type__c = 'PB';
        loanAppObj.Requested_Amount__c = 50;
        loanAppObj.Approved_ROI__c = 0;
        loanAppObj.Requested_ROI__c = 10;
        loanAppObj.branch__c = 'Delhi';
        loanAppObj.Line_Of_Business__c = 'Digital';  
        loanAppObj.Sub_Stage__c = 'Customer Onboarding';
        loanAppObj.Amount_Changed__c = true;
       // loanAppObj.Expiry_Date__c = Date.Today().addDays(1);
        //loanAppObj.StageName__c = 'Application Initiation';
        loanAppObj.property_identified__c = true;
        loanAppObj.Approved_cross_sell_amount__c = 3423;
		loanAppObj.Credit_Manager_User__c= fcuManager.Id;
		loanAppObj.Approved_Loan_Amount__c= 60000;
        Database.insert(loanAppObj);
        
        
        system.debug('loanAppObj.LTV_Amount__c::::'+loanAppObj.LTV_Amount__c);
        
        Scheme__c sc=new Scheme__c();
        sc.name='TestScheme';
        sc.Scheme_Code__c='testcode';
        sc.Scheme_Group_ID__c='23131';
        sc.Product_Code__c='HL';
        insert sc;
        

        
       Id RecordTypeIdAccount = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Co-Applicant').getRecordTypeId();

        Loan_Contact__c loanConObj = [select id, name, loan_applications__c, Income_Program_Type__c from loan_contact__c where loan_applications__c =: loanAppObj.id];
        loanConObj.Borrower__c = '1';
        loanConObj.Constitution__c= '20';
        loanConObj.Customer_segment__c= '1';
        loanConobj.Applicant_Status__c = '1';
        //loanConObj.Nature_of_Business__c  = 'S';
        loanConObj.Income_Program_Type__c ='FLIP';
        loanConObj.RecordTypeId = RecordTypeIdAccount;
        loanConObj.Mobile__c='9876543210';
        loanConObj.Email__c='testemail@gmail.com';
        loanConObj.Category__c='1';
        loanConobj.Pan_Number__c = 'AXSPJ2345K';
        loanConobj.Father_s_Husband_s_Name__c = 'test';
        loanConobj.Mother_s_Maiden_Name__c = 'test';
        update loanConObj;
        Document_Master__c docMasObj = new Document_Master__c();
        docMasObj.Name = 'Test';
        docMasObj.Doc_Id__c = '123344';
        docMasObj.Approving_authority__c = 'Business Head';
        Database.insert(docMasObj);
        
        Document_type__c docTypeObj = new Document_type__c();
        docTypeObj.Name = 'Test Document Type';
        docTypeObj.Approving_authority__c    = 'Business Head';
        Database.insert(docTypeObj);
        
        DocumentType_Document_Mapping__c mapObj = new DocumentType_Document_Mapping__c();
        mapObj.Document_Master__c = docMasObj.id;
        mapObj.Document_Type__c = docTypeObj.id;
        Database.insert(mapObj);
        
           LMS_Date_Sync__c objLMSDateSync = new LMS_Date_Sync__c(Current_Date__c=Date.Today().addDays(1));
        insert objLMSDateSync;
        
        Group_Exposure__c obj =  new Group_Exposure__c(LoanApplication__c = loanAppObj.id, Customer_Detail__c = loanConObj.id,Business_Date__c=Date.Today());
        insert obj;
   
        
        Test.stopTest();
        
        Personal_Discussion__c pdObj = new Personal_Discussion__c();
        pdObj.customer_detail__c = loanConObj.id;
        pdObj.Loan_Application__c = loanAppObj.id;
        pdObj.PD_Completed_On__c = system.today();
        pdObj.PD_Status__c = 'In Progress';
        pdObj.Type_Of_PD__c = 'Telephonic';
        Database.insert(pdObj);
        
        Sourcing_Detail__c sdObj = new Sourcing_Detail__c();
        sdObj.Loan_Application__c = loanAppObj.id;
        //sdObj.Sales_User_Name__c = userInfo.getUserId();
        //sdObj.Lead_Sourcing_Detail__c = 'HHFL Website';
        sdObj.Source_Description__c = 'Description';
        sdObj.Source_ID__c = 'Test';
        Database.insert(sdObj);
        
       /* Sourcing_Detail__c sdObj2 = new Sourcing_Detail__c();
        sdObj2.Loan_Application__c = loanAppObj.id;
        //sdObj.Sales_User_Name__c = userInfo.getUserId();
        //sdObj.Lead_Sourcing_Detail__c = 'HHFL Website';
        sdObj2.Source_Description__c = 'Description';
        sdObj2.Source_ID__c = 'Tes2t';
        Database.insert(sdObj2);*/
        
        loanAppObj.Sub_Stage__c = constants.COPS_Data_Maker;
        loanAppObj.StageName__c = constants.Operation_Control;
       // loanAppObj.ownerId=loggedUser.id;
        //Income_Program_Type__c =
        Database.update(loanAppObj);
       
        MoveToNextStageCreditController.initialValidation(loanAppObj.id);
        MoveToNextStageCreditController.getThirdPartyVerificationStatus(loanAppObj.id);
        MoveToNextStageCreditController.getPersonalDiscussionDetails(loanAppObj.id);
        
        loanAppObj.ownerId=loggedUser.id;
        loanAppObj.Assigned_Credit_Review__c = loggedUser.id;
        loanAppObj.Loan_Number__c = '768920';
        Database.update(loanAppObj);
        System.debug('loanAppObj!!'+loanAppObj);
       MoveToNextStageCreditController.moveToNextStage(loanAppObj.id);
       MoveToNextStageCreditController.assignToSalesUser(loanAppObj.id,'TEST');
       
       
        
    }
    
      public static testMethod void MoveToNextStageCreditController2(){
        //Profile p=[SELECT id,name from profile where name=:'Sales Team'];
       //  UserRole r = [Select Id From UserRole Where PortalType = 'None' and Name = 'RSM - Delhi 1'Limit 1];
        Profile p=[SELECT id,name from profile where name=:'Sales Team'];
         List<User> lstBranchManager= new List<User>();                          
          User u1 = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',Role_in_Sales_Hierarchy__c = 'SM'
           // UserRoleId = r.Id
        );
        lstBranchManager.add(u1);
        system.debug('u1:::'+u1);
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
           // UserRoleId = r.Id
            ManagerID = u1.id
        );
        SYstem.debug('u.ManagerID!!'+u.ManagerID);
         lstBranchManager.add(u);
        insert lstBranchManager;

        User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                         Title,Alias,TimeZoneSidKey,
                         EmailEncodingKey,LanguageLocaleKey,
                         LocaleSidKey,UserRoleId 
                         from user where ID=:UserInfo.getUserId()];
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=lstBranchManager[0].Id;
        loggedUser.ManagerID=lstBranchManager[0].Id;
        update loggedUser;
        system.debug('loggedUser@@@'+loggedUser);
         Account custObj = new Account();
        custObj.Salutation='Mr';
        custObj.FirstName='Test';
        custObj.LastName = 'Test Customer';
        custObj.Phone = '9999999988';
        database.insert(custObj);
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        database.insert(schemeObj);
        
        Test.startTest(); 
        Loan_Application__c loanAppObj = new Loan_Application__c();
        loanAppObj.customer__c = custObj.id;
        loanAppObj.Loan_Application_Number__c = 'LA00001';
        loanAppObj.Loan_Purpose__c = '11';
        loanAppObj.scheme__c = schemeObj.id;
        loanAppObj.Transaction_type__c = 'PB';
        loanAppObj.Requested_Amount__c = 50;
        loanAppObj.Approved_ROI__c = 0;
        loanAppObj.Requested_ROI__c = 10;
        loanAppObj.branch__c = 'Delhi';
        loanAppObj.Line_Of_Business__c = 'Digital';  
        loanAppObj.Sub_Stage__c = 'Customer Onboarding';
        //loanAppObj.StageName__c = 'Application Initiation';
        loanAppObj.Amount_Changed__c = true;
        loanAppObj.property_identified__c = true;
        loanAppObj.Approved_cross_sell_amount__c = 3423;
        Database.insert(loanAppObj);
        
        
        
        
        Scheme__c sc=new Scheme__c();
        sc.name='TestScheme';
        sc.Scheme_Code__c='testcode';
        sc.Scheme_Group_ID__c='23131';
        sc.Product_Code__c='HL';
        insert sc;
        
      
       Id RecordTypeIdAccount = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Co-Applicant').getRecordTypeId();

        Loan_Contact__c loanConObj = [select id, name, loan_applications__c, Income_Program_Type__c from loan_contact__c where loan_applications__c =: loanAppObj.id];
        loanConObj.Borrower__c = '1';
        loanConObj.Constitution__c= '20';
        loanConObj.Customer_segment__c= '1';
        loanConobj.Applicant_Status__c = '1';
        //loanConObj.Nature_of_Business__c  = 'S';
        loanConObj.Income_Program_Type__c ='FLIP';
        loanConObj.RecordTypeId = RecordTypeIdAccount;
        loanConObj.Mobile__c='9876543210';
        loanConObj.Email__c='testemail@gmail.com';
        loanConObj.Category__c='1';
        loanConobj.Pan_Number__c = 'AXSPJ2345K';
        update loanConObj;
        Document_Master__c docMasObj = new Document_Master__c();
        docMasObj.Name = 'Test';
        docMasObj.Doc_Id__c = '123344';
        docMasObj.Approving_authority__c = 'Business Head';
        Database.insert(docMasObj);
        
        Document_type__c docTypeObj = new Document_type__c();
        docTypeObj.Name = 'Test Document Type';
        docTypeObj.Approving_authority__c    = 'Business Head';
        Database.insert(docTypeObj);
        
        DocumentType_Document_Mapping__c mapObj = new DocumentType_Document_Mapping__c();
        mapObj.Document_Master__c = docMasObj.id;
        mapObj.Document_Type__c = docTypeObj.id;
        Database.insert(mapObj);
        
        Group_Exposure__c obj =  new Group_Exposure__c(LoanApplication__c = loanAppObj.id, Customer_Detail__c = loanConObj.id,Business_Date__c=System.Today());
        insert obj;
        
        Document_Checklist__c docObj = new Document_Checklist__c();
        docObj.Loan_Applications__c = loanAppObj.id;
        docObj.status__c = 'Uploaded';
        docObj.document_type__c = docTypeObj.id;
        docObj.Document_Master__c = docMasObj.id;
        docObj.Loan_Contact__c = loanConObj.id;
        docObj.Express_Queue_Mandatory__c = true;
        docObj.Document_Collection_Mode__c = 'Photocopy';
        docObj.Screened_p__c = 'Yes';
        docObj.Sampled_p__c = 'Yes';
        docObj.REquest_Date_for_OTC__c = datetime.now();
        docObj.Data_Entry_Required__c =  true;
        docObj.Status__c ='Uploaded';
        insert(docObj);
        
         Document_Checklist__c docObj2 = new Document_Checklist__c();
        docObj2.Loan_Applications__c = loanAppObj.id;
        docObj2.status__c = 'Uploaded';
        docObj2.document_type__c = docTypeObj.id;
        docObj2.Document_Master__c = docMasObj.id;
        docObj2.Loan_Contact__c = loanConObj.id;
        docObj2.Express_Queue_Mandatory__c = true;
        docObj2.Document_Collection_Mode__c = 'Photocopy';
        docObj2.Screened_p__c = 'Yes';
        docObj2.Sampled_p__c = 'Yes';
        docObj2.REquest_Date_for_OTC__c = datetime.now();
        docObj2.Data_Entry_Required__c =  true;
        docObj2.Status__c ='Uploaded';
        Database.insert(docObj2);
        
         Document_Checklist__c docObj3 = new Document_Checklist__c();
        docObj3.Loan_Applications__c = loanAppObj.id;
        docObj3.status__c = 'Received';
        docObj3.document_type__c = docTypeObj.id;
        docObj3.Document_Master__c = docMasObj.id;
        docObj3.Loan_Contact__c = loanConObj.id;
        docObj3.Express_Queue_Mandatory__c = true;
        docObj3.Document_Collection_Mode__c = 'Photocopy';
        docObj3.Screened_p__c = 'Yes';
        docObj3.Sampled_p__c = 'Yes';
        docObj3.REquest_Date_for_OTC__c = datetime.now();
        docObj3.Data_Entry_Required__c =  false;
       
        //Database.insert(docObj3);
        
        //system.assertEquals(1,loanAppObj.Count_of_Sourcing_Records__c);
        
        Test.stopTest();
        
        Personal_Discussion__c pdObj = new Personal_Discussion__c();
        pdObj.customer_detail__c = loanConObj.id;
        pdObj.Loan_Application__c = loanAppObj.id;
        pdObj.PD_Completed_On__c = system.today();
        pdObj.PD_Status__c = 'In Progress';
        pdObj.Type_Of_PD__c = 'Telephonic';
        Database.insert(pdObj);
        
        Sourcing_Detail__c sdObj = new Sourcing_Detail__c();
        sdObj.Loan_Application__c = loanAppObj.id;
        //sdObj.Sales_User_Name__c = userInfo.getUserId();
        //sdObj.Lead_Sourcing_Detail__c = 'HHFL Website';
        sdObj.Source_Description__c = 'Description';
        sdObj.Source_ID__c = 'Test';
        Database.insert(sdObj);
        
       /* Sourcing_Detail__c sdObj2 = new Sourcing_Detail__c();
        sdObj2.Loan_Application__c = loanAppObj.id;
        //sdObj.Sales_User_Name__c = userInfo.getUserId();
        //sdObj.Lead_Sourcing_Detail__c = 'HHFL Website';
        sdObj2.Source_Description__c = 'Description';
        sdObj2.Source_ID__c = 'Tes2t';
        Database.insert(sdObj2);*/
        
        loanAppObj.Sub_Stage__c = constants.COPS_Data_Maker;
        loanAppObj.StageName__c = constants.Operation_Control;
        loanAppObj.Property_Identified__c =  false;
       // loanAppObj.ownerId=loggedUser.id;
        //Income_Program_Type__c =
        Database.update(loanAppObj);
        
        Third_Party_Verification__c tpvObj = new Third_Party_Verification__c();
        tpvObj.Loan_Application__c = loanAppObj.id;
        tpvObj.Status__c = 'New';
        Database.insert(tpvObj); 
        MoveToNextStageCreditController.getThirdPartyVerificationStatus(loanAppObj.id);
       
        MoveToNextStageCreditController.initialValidation(loanAppObj.id);
        MoveToNextStageCreditController.moveToReCredit(loanAppObj.id);
        MoveToNextStageCreditController.movePreviousToReCredit(loanAppObj.id,'test');
        MoveToNextStageCreditController.moveToCOPS(loanAppObj.id,'test');
        MoveToNextStageCreditController.initialSubStageNotification(loanAppObj.id);
        MoveToNextStageCreditController.getPMAYDocumentStatus(loanAppObj.id);
        MoveToNextStageCreditController.getAadhaarStatus(loanAppObj.id);
      }
	
    
	public static testMethod void initialValidationTest(){
        //Profile p=[SELECT id,name from profile where name=:'Sales Team'];
       //  UserRole r = [Select Id From UserRole Where PortalType = 'None' and Name = 'RSM - Delhi 1'Limit 1];
        Profile p=[SELECT id,name from profile where name=:'Sales Team'];
         List<User> lstBranchManager= new List<User>();                          
          User u1 = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',Role_in_Sales_Hierarchy__c = 'SM'
           // UserRoleId = r.Id
        );
        lstBranchManager.add(u1);
        system.debug('u1:::'+u1);
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
           // UserRoleId = r.Id
            ManagerID = u1.id
        );
        SYstem.debug('u.ManagerID!!'+u.ManagerID);
         lstBranchManager.add(u);
        insert lstBranchManager;

        User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                         Title,Alias,TimeZoneSidKey,
                         EmailEncodingKey,LanguageLocaleKey,
                         LocaleSidKey,UserRoleId 
                         from user where ID=:UserInfo.getUserId()];
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=lstBranchManager[0].Id;
        loggedUser.ManagerID=lstBranchManager[0].Id;
        update loggedUser;
        system.debug('loggedUser@@@'+loggedUser);
         Account custObj = new Account();
        custObj.Salutation='Mr';
        custObj.FirstName='Test';
        custObj.LastName = 'Test Customer';
        custObj.Phone = '9999999988';
        database.insert(custObj);
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        database.insert(schemeObj);
        
        Test.startTest(); 
        Loan_Application__c loanAppObj = new Loan_Application__c();
        loanAppObj.customer__c = custObj.id;
        loanAppObj.Loan_Application_Number__c = 'LA00001';
        loanAppObj.Loan_Purpose__c = '11';
        loanAppObj.scheme__c = schemeObj.id;
        loanAppObj.Transaction_type__c = 'PB';
        loanAppObj.Requested_Amount__c = 50;
        loanAppObj.Approved_ROI__c = 0;
        loanAppObj.Requested_ROI__c = 10;
        loanAppObj.branch__c = 'Delhi';
        loanAppObj.Line_Of_Business__c = 'Digital';  
        loanAppObj.Sub_Stage__c = Constants.Credit_Review ;
        loanAppObj.StageName__c = 'Credit Decisioning';
        loanAppObj.Amount_Changed__c = true;
        loanAppObj.property_identified__c = true;
        loanAppObj.Approved_cross_sell_amount__c = 3423;
        Database.insert(loanAppObj);
        
        
        
        
        Scheme__c sc=new Scheme__c();
        sc.name='TestScheme';
        sc.Scheme_Code__c='testcode';
        sc.Scheme_Group_ID__c='23131';
        sc.Product_Code__c='HL';
        insert sc;
        
      
       Id RecordTypeIdAccount = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Co-Applicant').getRecordTypeId();

        Loan_Contact__c loanConObj = [select id, name, loan_applications__c, Income_Program_Type__c from loan_contact__c where loan_applications__c =: loanAppObj.id];
        loanConObj.Borrower__c = '1';
        loanConObj.Constitution__c= '20';
        loanConObj.Customer_segment__c= '1';
        loanConobj.Applicant_Status__c = '1';
        //loanConObj.Nature_of_Business__c  = 'S';
        loanConObj.Income_Program_Type__c ='FLIP';
        loanConObj.RecordTypeId = RecordTypeIdAccount;
        loanConObj.Mobile__c='9876543210';
        loanConObj.Email__c='testemail@gmail.com';
        loanConObj.Category__c='1';
        loanConobj.Pan_Number__c = 'AXSPJ2345K';
        update loanConObj;
        Document_Master__c docMasObj = new Document_Master__c();
        docMasObj.Name = 'Test';
        docMasObj.Doc_Id__c = '123344';
        docMasObj.Approving_authority__c = 'Business Head';
        Database.insert(docMasObj);
        
        Document_type__c docTypeObj = new Document_type__c();
        docTypeObj.Name = 'Test Document Type';
        docTypeObj.Approving_authority__c    = 'Business Head';
        Database.insert(docTypeObj);
        
        DocumentType_Document_Mapping__c mapObj = new DocumentType_Document_Mapping__c();
        mapObj.Document_Master__c = docMasObj.id;
        mapObj.Document_Type__c = docTypeObj.id;
        Database.insert(mapObj);
        
        Group_Exposure__c obj =  new Group_Exposure__c(LoanApplication__c = loanAppObj.id, Customer_Detail__c = loanConObj.id,Business_Date__c=System.Today());
        //insert obj;
        
        Document_Checklist__c docObj = new Document_Checklist__c();
        docObj.Loan_Applications__c = loanAppObj.id;
        docObj.status__c = 'Uploaded';
        docObj.document_type__c = docTypeObj.id;
        docObj.Document_Master__c = docMasObj.id;
        docObj.Loan_Contact__c = loanConObj.id;
        docObj.Express_Queue_Mandatory__c = true;
        docObj.Document_Collection_Mode__c = 'Photocopy';
        docObj.Screened_p__c = 'Yes';
        docObj.Sampled_p__c = 'Yes';
        docObj.REquest_Date_for_OTC__c = datetime.now();
        docObj.Data_Entry_Required__c =  false;
        docObj.Status__c ='Uploaded';
        insert(docObj);
        
         Document_Checklist__c docObj2 = new Document_Checklist__c();
        docObj2.Loan_Applications__c = loanAppObj.id;
        docObj2.status__c = 'Uploaded';
        docObj2.document_type__c = docTypeObj.id;
        docObj2.Document_Master__c = docMasObj.id;
        docObj2.Loan_Contact__c = loanConObj.id;
        docObj2.Express_Queue_Mandatory__c = true;
        docObj2.Document_Collection_Mode__c = 'Photocopy';
        docObj2.Screened_p__c = 'Yes';
        docObj2.Sampled_p__c = 'Yes';
        docObj2.REquest_Date_for_OTC__c = datetime.now();
        docObj2.Data_Entry_Required__c =  false;
        docObj2.Status__c ='Uploaded';
        Database.insert(docObj2);
        
         Document_Checklist__c docObj3 = new Document_Checklist__c();
        docObj3.Loan_Applications__c = loanAppObj.id;
        docObj3.status__c = 'Pending';
        docObj3.document_type__c = docTypeObj.id;
        docObj3.Document_Master__c = docMasObj.id;
        docObj3.Loan_Contact__c = loanConObj.id;
        docObj3.Express_Queue_Mandatory__c = true;
        docObj3.Document_Collection_Mode__c = 'Photocopy';
        docObj3.Screened_p__c = 'Yes';
        docObj3.Sampled_p__c = 'Yes';
        docObj3.REquest_Date_for_OTC__c = datetime.now();
        docObj3.Data_Entry_Required__c =  false;
       
        Database.insert(docObj3);
        
        //system.assertEquals(1,loanAppObj.Count_of_Sourcing_Records__c);
        
        Test.stopTest();
        
        Personal_Discussion__c pdObj = new Personal_Discussion__c();
        pdObj.customer_detail__c = loanConObj.id;
        pdObj.Loan_Application__c = loanAppObj.id;
        pdObj.PD_Completed_On__c = system.today();
        pdObj.PD_Status__c = 'In Progress';
        pdObj.Type_Of_PD__c = 'Telephonic';
        Database.insert(pdObj);
        
        Sourcing_Detail__c sdObj = new Sourcing_Detail__c();
        sdObj.Loan_Application__c = loanAppObj.id;
        //sdObj.Sales_User_Name__c = userInfo.getUserId();
        //sdObj.Lead_Sourcing_Detail__c = 'HHFL Website';
        sdObj.Source_Description__c = 'Description';
        sdObj.Source_ID__c = 'Test';
        Database.insert(sdObj);
        
       /* Sourcing_Detail__c sdObj2 = new Sourcing_Detail__c();
        sdObj2.Loan_Application__c = loanAppObj.id;
        //sdObj.Sales_User_Name__c = userInfo.getUserId();
        //sdObj.Lead_Sourcing_Detail__c = 'HHFL Website';
        sdObj2.Source_Description__c = 'Description';
        sdObj2.Source_ID__c = 'Tes2t';
        Database.insert(sdObj2);*/
        
        loanAppObj.Sub_Stage__c = constants.COPS_Data_Maker;
        loanAppObj.StageName__c = constants.Operation_Control;
        loanAppObj.Property_Identified__c =  false;
       // loanAppObj.ownerId=loggedUser.id;
        //Income_Program_Type__c =
        //Database.update(loanAppObj);
        
        Third_Party_Verification__c tpvObj = new Third_Party_Verification__c();
        tpvObj.Loan_Application__c = loanAppObj.id;
        tpvObj.Status__c = 'New';
        Database.insert(tpvObj); 
        
        UCIC_Global__c ucg = new UCIC_Global__c();
        ucg.UCIC_Active__c = true;
        insert ucg;
        MoveToNextStageCreditController.getThirdPartyVerificationStatus(loanAppObj.id);
       
        MoveToNextStageCreditController.initialValidation(loanAppObj.id);
    }
    
     
}