public class CurrentUserAssigmentAPF {
    @AuraEnabled
    public static Project__c setOwner(id projectId) {
        id curUser = Userinfo.getUserid();
        String managerId = [Select ManagerId From User Where Id =: curUser LIMIT 1].ManagerId;
        User upperHierarchyDetail = [Select Id, ManagerId, Manager.ManagerId from User Where Id =: managerId];
        System.debug('Debug Log for upperHierarchyDetail'+upperHierarchyDetail);
        system.debug('==oppid=='+projectId);
        Project__c objProject = new Project__c();
        objProject = [Select OwnerId, RecordTypeId, Stage__c, Sub_Stage__c, Assigned_CRO_Approver__c, Assigned_NCM_Approver__c,  Assigned_Sales_User__c, Assigned_FileCheck_User__c, Assigned_Cops_dataMaker__c, Assigned_Cops_dataChecker__c, Assigned_Approval_Communicator__c, Assigned_Scan_Maker__c, Assigned_Scan_Checker__c, Assigned_Credit_Review__c,Assigned_Credit_Approver__c  from Project__c where id = :projectId];
        system.debug('==objProject=='+objProject.OwnerId);
        List<GroupMember> userID = [Select UserOrGroupId from GroupMember where GroupId = :objProject.ownerId]; 
        system.debug('==userID==');
        system.debug('==userID=='+userID);
        list<id> userEx = new list<id>();
        for(GroupMember us:userID){
            userEx.add(us.UserOrGroupId);
            
        }
        system.debug('====userEx==='+userEx);
        system.debug('====curUser==='+curUser);
        if(userEx.contains(curUser)){
            objProject.OwnerId = curUser;
            if((objProject.Sub_Stage__c == Constants.strApfSubStgPDC)&&(objProject.Assigned_Sales_User__c == null)){
                objProject.Assigned_Sales_User__c = curUser;
            }
            else if ((objProject.Sub_Stage__c == Constants.strApfSubStgFileChckr)&&(objProject.Assigned_FileCheck_User__c == null)){
                objProject.Assigned_FileCheck_User__c = curUser;
                objProject.recordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeFileChecker).getRecordTypeId();
            }
            else if ((objProject.Sub_Stage__c == Constants.strApfSubStgScnMkr)&&(objProject.Assigned_Scan_Maker__c == null)){
                objProject.Assigned_Scan_Maker__c = curUser;
                objProject.recordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeFileChecker).getRecordTypeId();
            }
            else if ((objProject.Sub_Stage__c == Constants.strApfSubStgScnChkr)&&(objProject.Assigned_Scan_Checker__c == null)){
                objProject.Assigned_Scan_Checker__c = curUser;
                objProject.recordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeFileChecker).getRecordTypeId();
            }
            else if ((objProject.Sub_Stage__c == Constants.strApfSubStgCOPSDataMaker)&&(objProject.Assigned_Cops_dataMaker__c == null)){
                objProject.Assigned_Cops_dataMaker__c = curUser;
                objProject.recordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeCOPSDM).getRecordTypeId();
            }
            else if ((objProject.Sub_Stage__c == Constants.strApfSubStgCOPSDataChecker)&&(objProject.Assigned_Cops_dataChecker__c == null)){
                objProject.Assigned_Cops_dataChecker__c = curUser;
                objProject.recordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeCOPSDC).getRecordTypeId();
            }
            else if ((objProject.Sub_Stage__c == Constants.strApfSubStgCredReview)&&(objProject.Assigned_Credit_Review__c == null)){
                objProject.Assigned_Credit_Review__c  = curUser;
                if(upperHierarchyDetail != null) {
                    objProject.Assigned_NCM_Approver__c = upperHierarchyDetail.ManagerId != null ? upperHierarchyDetail.ManagerId : null;
                    objProject.Assigned_CRO_Approver__c = upperHierarchyDetail.Manager.ManagerId != null ? upperHierarchyDetail.Manager.ManagerId : null;
                }
                objProject.recordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeCreditReview).getRecordTypeId();
            }
            else if ((objProject.Sub_Stage__c == Constants.strApfSubStgApprovalComm)&&(objProject.Assigned_Approval_Communicator__c == null)){
                objProject.Assigned_Approval_Communicator__c = curUser;
                objProject.recordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeAppComm).getRecordTypeId();
            }
            else if ((objProject.Sub_Stage__c == Constants.strApfSubStgInvUpdate)&&(objProject.Assigned_Credit_Approver__c == null)){
                objProject.Assigned_Credit_Approver__c = curUser;
                objProject.recordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeCreditApproval).getRecordTypeId();
            }
            
            system.debug('===objProject=='+objProject);
            update objProject;
            return objProject;
        } else return null;
    }
}