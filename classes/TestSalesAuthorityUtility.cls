@isTest
public class TestSalesAuthorityUtility{
    public static testMethod void SalesAuthorityUtility(){
        Account custObj = new Account();
        custObj.Name = 'Test Customer';
        custObj.Phone = '9999999998';
        database.insert(custObj);
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        database.insert(schemeObj);
        
         
        Loan_Application__c loanAppObj = new Loan_Application__c();
        loanAppObj.customer__c = custObj.id;
        loanAppObj.Loan_Application_Number__c = 'LA00001';
        loanAppObj.Loan_Purpose__c = '11';
        loanAppObj.scheme__c = schemeObj.id;
        loanAppObj.Transaction_type__c = 'PB';
        loanAppObj.Requested_Amount__c = 50;
        loanAppObj.Approved_ROI__c = 50;
        loanAppObj.Requested_ROI__c = 10;
        loanAppObj.branch__c = 'Delhi';
        loanAppObj.Line_Of_Business__c = 'Digital';        
        Database.insert(loanAppObj);
        
        Third_Party_Verification__c tpvObj = new Third_Party_Verification__c();
        tpvObj.Loan_Application__c = loanAppObj.id;
        tpvObj.Status__c = 'New';
        //Database.insert(tpvObj); 
        
        Test.startTest();
        Loan_contact__c loanCOnObj = [select id, name, loan_Applications__c from loan_contact__c where loan_applications__c =: loanAppObj.id];
        
        Document_Master__c docMasObj = new Document_Master__c();
        docMasObj.Name = 'Test';
        docMasObj.Doc_Id__c = '123344';
        docMasObj.Approving_authority__c = 'Business Head';
        Database.insert(docMasObj);
        
        Document_type__c docTypeObj = new Document_type__c();
        docTypeObj.Name = 'Test Document Type';
        docTypeObj.Approving_authority__c    = 'Business Head';
        Database.insert(docTypeObj);
        
        DocumentType_Document_Mapping__c mapObj = new DocumentType_Document_Mapping__c();
        mapObj.Document_Master__c = docMasObj.id;
        mapObj.Document_Type__c = docTypeObj.id;
        Database.insert(mapObj);
        
        Document_Checklist__c docObj = new Document_Checklist__c();
        docObj.Loan_Applications__c = loanAppObj.id;
        docObj.status__c = 'Pending';
        docObj.document_type__c = docTypeObj.id;
        docObj.Document_Master__c = docMasObj.id;
        docObj.Loan_Contact__c = loanConObj.id;
        docObj.Express_Queue_Mandatory__c = true;
        docObj.Document_Collection_Mode__c = 'Photocopy';
        docObj.Screened_p__c = 'Yes';
        docObj.Sampled_p__c = 'Yes';
        docObj.REquest_Date_for_OTC__c = datetime.now();
        Database.insert(docObj);      
        
        Branch_LOB_User_Mapping__c blumObj = new Branch_LOB_User_Mapping__c();
        blumObj.branch__c = 'Delhi';
        blumObj.BH__c = Userinfo.getUserid();
        blumObj.CSH__c = Userinfo.getUserid();
        blumObj.Business_Head__c = Userinfo.getUserid();
        blumObj.Sales_Manager__c = Userinfo.getUserid();
        blumObj.CEO__c = Userinfo.getUserid();
        blumObj.NSH__c = Userinfo.getUserid();
        blumObj.ZSH__c = Userinfo.getUserid();
        blumObj.Line_Of_Business__c = 'Digital';
        Database.insert(blumObj);
        
        system.assertNotEquals(null,loanAppObj.id);
        
        SalesAuthorityUtility.findSalesAuthority(loanAppObj.id);
        SalesAuthorityUtility.findCreditAuthority(loanAppObj);
        Test.stopTest();
    }
}