@isTest
public class TestStatusTrigger{
    public static testMethod void insertStatusTracking(){
        List<Status_Tracking__c> lstStatusTrack = new List<Status_Tracking__c>();
        Status_Tracking__c st = new Status_Tracking__c();
        st.Application_Id__c = '1001001';
        st.Request_Id__c = '123456789';
        st.Request_Type__c = 'Loan Status';
        st.Repayment_Date__c = system.today();
        st.Date_of_Transaction__c = system.today();
        st.Maker_Name__c = 'test';
        st.Author_Name__c = 'test';
        st.Status__c = 'Cancelled';
        st.Remarks__c = 'Remarks';
        st.Reason_for_Cancellation__c = 'Reason';
        lstStatusTrack.add(st);
        insert lstStatusTrack;
        LMS_Date_Sync__c objLMSSync = new LMS_Date_Sync__c(EOD_BOD__c='N',Current_Date__c=System.today().addDays(-5));
        insert objLMSSync;
        Id RecordTypeIdLoanApp = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('COPS:Data Checker').getRecordTypeId();
        Branch_Master__c objBM = new Branch_Master__c(Name='Delhi',Office_Code__c='1');
        insert objBM;
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9934567890');
        insert acc;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                         Transaction_type__c='PB',
                                                                         Requested_Amount__c=100000,
                                                                         Loan_Purpose__c='11',
                                                                         Sub_Stage__c='Application Initiation',
                                                                         StageName__c='Customer Onboarding',
                                                                         RecordTypeId=RecordTypeIdLoanApp,
                                                                         Branch__c='All',Branch_Lookup__c=objBM.Id,
                                                                         Loan_Number__c = st.Application_Id__c,
                                                                         Loan_Status__c = 'Active',
                                                                         Repayment_Start_Date__c=Date.Today().addDays(100),
                                                                        Sanctioned_Disbursement_Type__c='Multiple Tranche'
                                                                        );
        insert objLoanApplication;
        
                StatusTriggerHelper.pemiToEmi(lstStatusTrack);

        StatusTriggerHelper.loanCancellation(lstStatusTrack);
    }

}