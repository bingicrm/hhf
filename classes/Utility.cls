/*************************************************************************************
Created By:     Gaurav Kumar Chadha(Deloitte)    
Created Date:   12-06-2018
Description:    This class contains utility methods utilized for Housing Loan Product
*************************************************************************************/
Public class Utility{
/*
  * Date Modified                Modified By                  Description of the update

      14/05/2019                  Shobhit Saxena              Update made in postToChatter method to make it asynchronous.
      
  =====================================================================*/                         

    /**********************************************************************************
    Created By:     Gaurav Kumar Chadha(Deloitte)
    Created Date:   12-06-2018
    Parameters:     
                    endPointURL   : End Point URL of the API to be consumed
                    body          : Body of the callout
                    method        : HTTP method of the callout such as GET, POST, PUT, PATCH ,DELETE etc
                    header        : Header of the callout such as Authorization, Application Type etc 
                    timeOut       : Maximum time to be taken for the callout
    Return Type:    
                    HTTPResponse  : This is the standard object to receive response of a rest callout
                    
    Description:    This is a general method which uses HTTP, HTTPRequest and HTTPResponse standard objects  
                    and there standard methods to send a rest callout and returns the response    
    ***********************************************************************************/
    public static HTTPResponse sendRequest(String endPointURL, String method, String body , Map<String,String> header, Integer timeOut ){
        
        HTTP http = new HTTP();
        HTTPRequest httpReq = new HTTPRequest();
        HTTPResponse httpRes;
        httpReq.setEndPoint(endPointURL);
        httpReq.setMethod(method);
        system.debug('==header=='+header);
        for(String headerKey : header.keySet()){
            httpReq.setHeader(headerKey, header.get(headerKey));
        }
        httpReq.setBody(body);
        httpReq.setTimeOut(50000);
        system.debug(body);
        try
        {
            httpRes = http.send(httpReq);
            system.debug('@@@@in utility' + httpRes );
            //LogUtility.createLogs('Integration', 'Debug', 'Apex Class', 'URL : '+endPointURL,'', httpRes.getBody(), '', true);
        }
        catch(Exception ex)
        {
            LogUtility.createLogs('Error', 'Error', 'Apex Class', 'URL : '+endPointURL, ex.getStackTraceString(), ex.getMessage(), '', true);
        }
        
        LogUtility.createIntegrationLogs(body,httpRes,endPointURL);
        return httpRes;
    
    }
    /**********************************************************************************
    Created By:     Gaurav Kumar Chadha(Deloitte)
    Created Date:   18-06-2018
    Parameters:     
                    loan_application__cId   : Id of the loan Application in context
                    
    Return Type:    
                    Id  : Id of the user to provide access 
                    
    Description:    This method returns the user to whom loan_application__c is assigned    
    ***********************************************************************************/
   public static Id getCurrentOperatorId(Map<String,List<User>> mapRoleBranchWithListOfUsers, String branch, String subStage){
                        
        DateTime lastAssignedDate = System.now();
        Id currentOperatorId;
        List<User> users = new list<user>();
        Datetime roleAssignedDate;                                           
        if(mapRoleBranchWithListOfUsers.containsKey(branch + subStage)){
           users = mapRoleBranchWithListOfUsers.get(branch + subStage);
        }
        for(User userObj : users){
            roleAssignedDate = userObj.Role_Assigned_Date__c;
            if( roleAssignedDate == null){
                return userObj.Id;
            }else if(roleAssignedDate <  lastAssignedDate  ){
                    lastAssignedDate = roleAssignedDate;
                    currentOperatorId = userObj.Id;
            }
        }
        return currentOperatorId; 
    }
   
   public static void validateExpressQueueDocs(List<loan_application__c> newList, Map<Id, loan_application__c> oldMap){
       
        Map<Id,List<Document_Checklist__c>> mapOppIdWithDocChecklIst = new Map<Id,List<Document_Checklist__c>>();
         Map<Id,List<IMD__c>> mapLAIdwithIMDId = new Map<Id,List<IMD__c>>();
         
         for(Document_Checklist__c  docCheck :  [ SELECT Id, Express_Queue_Mandatory__c, Status__c 
                                                  FROM   Document_Checklist__c 
                                                  WHERE  loan_applications__c in :newList
                                                  AND    Express_Queue_Mandatory__c = true
                                                ]){
              if(!mapOppIdWithDocChecklIst.keySet().contains(docCheck.loan_applications__c)){
                  mapOppIdWithDocChecklIst.put(docCheck.loan_applications__c, new List<Document_Checklist__c>());
                  mapOppIdWithDocChecklIst.get(docCheck.loan_applications__c).add(docCheck);    
              }else{
                  mapOppIdWithDocChecklIst.get(docCheck.loan_applications__c).add(docCheck);
              }                      
          }  
          
         for(IMD__c imdObj : [ SELECT Id, Amount__c 
                              FROM   IMD__c
                              WHERE  loan_applications__c  IN : newList
                            ]){
             if(mapLAIdwithIMDId.keySet().contains(imdObj.loan_applications__c)){
                 mapLAIdwithIMDId.put(imdObj.loan_applications__c, new List<IMD__c>());
                 mapLAIdwithIMDId.get(imdObj.loan_applications__c).add(imdObj);
             }else{
                 mapLAIdwithIMDId.get(imdObj.loan_applications__c).add(imdObj);
             } 
         }    
                                                  
         for(loan_application__c opp : newList){
            if(opp.Sub_Stage__c != null && oldMap.get(opp.Id).Sub_Stage__c != opp.Sub_Stage__c ){
                if(opp.Sub_Stage__c == Constants.Express_Queue_Data_Checker ){
                    if(opp.PAN_Customer_Detail__c != opp.All_Loan_Contact_Count__c){
                        opp.addError('Please validate PAN Card.');
                    }  
                }
                if(opp.Sub_Stage__c == Constants.Loan_Engine_2_Data_Maker){
                                                           
                    Boolean requiredDocumentsUploaded = true;
                                          
                    for(Document_Checklist__c doc : mapOppIdWithDocChecklIst.get(opp.Id)){
                        if(doc.Status__c != Constants.Uploaded ){
                            requiredDocumentsUploaded = false;
                        }
                    }
                    
                    if(requiredDocumentsUploaded == false || mapLAIdwithIMDId.get(opp.Id).size() == 0){
                        opp.addError('Please Upload required documents and add IMD deteials.');
                    }else{
                        opp.Express_Queue_Docs_Completed__c = true;
                    }                                                             
                }
           }
       }            
    }


    /*public static string postEmail( String queueId , String userId, string emailTemplateName, string loanId )
    {
          List<string> emailTo = new List<string>();
          list<user> userList = new list<user>();

          if(queueId != null){

        userList =[SELECT Email FROM User WHERE Id IN (
    SELECT UserOrGroupId FROM GroupMember WHERE GroupId = : queueId) ];

    
    } else{
      userList =[SELECT Email FROM User WHERE Id = :userId];
    }
      for(user us: userList){
      emailTo.add(us.email);
    }
system.debug('==email=='+emailTo);

    EmailTemplate et = [SELECT Id , body,htmlValue , subject FROM EmailTemplate 
    WHERE DeveloperName = :emailTemplateName];
String htmlBody = et.HtmlValue;
          //htmlBody = htmlBody.replace('{!Contact.Birthdate}', allstring);
          system.debug('html----->'+htmlBody);
          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          mail.setToAddresses(emailTo);
          //mail.setTargetObjectId(targetId);
          mail.setSenderDisplayName(Label.Email_Sender_Name); 
          mail.setUseSignature(false); 
          mail.setBccSender(false); 
          mail.setSaveAsActivity(false);
          mail.setTemplateId(et.id);
          mail.setHtmlBody(htmlBody);
          mail.setPlainTextBody(et.body);
          mail.setTreatBodiesAsTemplate(true);
          mail.setWhatId(loanId);
          mail.setSubject(et.subject); 
          try{
          Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
          }catch(Exception ex){
            
          }

return 'Success';
    }*/
   
     public static string postEmail( List<Loan_application__c> lstLoanApp, string emailTemplateName)
    {
          if(System.Label.Post_Email != null && System.Label.Post_Email == 'true') {
			  List<string> emailTo = new List<string>();
			  list<user> userList = new list<user>();
			  list<Id> queueList = new list<Id>();
			  list<Id> userIdList = new list<Id>();
			  for( Loan_application__c objla: lstLoanApp){
				  if(objla.OwnerId != null)
				  {
				   if( String.valueOf(objla.OwnerId).contains('005') )
					{
					queueList.add(objla.OwnerId);
					}
					else{
					userIdList.add(objla.OwnerId);
					}
				  }
				 
			  }
			  
			  if(queueList.size() >0){
			  
					  userList =[SELECT Email FROM User WHERE Id IN (
				SELECT UserOrGroupId FROM GroupMember WHERE GroupId IN: queueList) ];
			  }
			  
			  for(User objUser: [SELECT Email FROM User WHERE Id IN: userIdList]){
				userList.add(objUser);
			  }
			

																				
											  
										   
			
								   
							
	 
   
													   
								 

		EmailTemplate et = [SELECT Id , body,htmlValue , subject FROM EmailTemplate 
		WHERE DeveloperName = :emailTemplateName];
				String htmlBody = et.HtmlValue;
				
				for(user us: userList){
		  emailTo.add(us.email);
		}
	   
			  for(Loan_application__c objloan: lstLoanApp){
			  for(user us: userList){
													  
											 
											   
					  
																				   
									   
					
				   
			 
		
		   
		 

						   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					  mail.setToAddresses(emailTo);
					  //mail.setTargetObjectId(targetId);
					  mail.setSenderDisplayName(Label.Email_Sender_Name); 
					  mail.setUseSignature(false); 
					  mail.setBccSender(false); 
					  mail.setSaveAsActivity(false);
					  mail.setTemplateId(et.id);
					  mail.setHtmlBody(htmlBody);
					  mail.setPlainTextBody(et.body);
					  mail.setTreatBodiesAsTemplate(true);
					  mail.setWhatId(objloan.Id);
					  mail.setSubject(et.subject); 
					  try{
					  Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
					  }catch(Exception ex){
						
					  }
				}
			
			  }
			 

	return 'Success';
		}
		return 'Success';
	}

    
    /*public static List<FeedItem> postToChatter( String queueId , String userId , String contentId, String Content )
    {
          System.debug('Debug Log for strURL'+URL.getOrgDomainUrl());
          List<FeedItem> toInsert = new List<FeedItem>();
          String strURL;
          String strCommunityURL;
          ID hhflNetWorkID = [SELECT Id,Name,UrlPathPrefix FROM Network WHERE UrlPathPrefix = 'hhfllos' LIMIT 1].ID;
          System.debug('Debug Log for hhflNetWorkID'+hhflNetWorkID);
          if(String.isNotBlank(hhflNetWorkID)) {
            strCommunityURL = Network.getLoginUrl(hhflNetWorkID);
            System.debug('Debug Log for strCommunityURL'+strCommunityURL);
            strCommunityURL = strCommunityURL.removeEnd('/login');
            System.debug('Debug Log for udpated strCommunityURL'+strCommunityURL);
          }
          if( queueId != null )
          {

              List<GroupMember> chatterMembers = [SELECT GroupId,Id,UserOrGroupId FROM GroupMember where GroupId = :queueId];
              Set<Id> userIds =  new Set<Id>();
              Map<Id,String> maptoURL = new Map<Id,String>();
              for(GroupMember mem : chatterMembers)
              {
                  if(String.valueOf(mem.UserOrGroupId).contains('005'))
                  {
                    userIds.add(mem.UserOrGroupId);
                  }
              }
              if( userIds.size() > 0 )
              {
                  for( User u : [select id, Profile.Name, Profile.UserType from User where Id IN : userIds ] )
                  {
                      if( u.Profile.UserType == 'standard' )
                      {
                          strURL = URL.getOrgDomainUrl().toExternalForm();
                          System.debug('Debug Log for strURL'+URL.getOrgDomainUrl());
                          maptoURL.put(u.Id, strURL);

                      }else{
                            if(String.isNotBlank(strCommunityURL)) {
                                maptoURL.put(u.Id, strCommunityURL);
                            }
                      }
                  }
              }
              for( GroupMember mem : chatterMembers)
              {
                  FeedItem post = new FeedItem();
                  post.ParentId = mem.UserOrGroupId;
                  post.Body = Content;
                  post.Visibility = 'AllUsers';
                  if(maptoURL.containsKey(mem.UserOrGroupId))
                  {
                      
                      post.LinkUrl =  maptoURL.get(mem.UserOrGroupId)+'/'+ contentId;
                      if(maptoURL.get(mem.UserOrGroupId) == strCommunityURL)
                      {
                          post.NetworkScope = String.valueOf(hhflNetWorkID);
                      }
                   }

                  toInsert.add(post);
              }    
          }
          else if( userId != null && UserId != UserInfo.getUserId() )
          {
                  User u = [select id, Profile.Name, Profile.UserType from User where id = :userId];
                  FeedItem post = new FeedItem();
                  post.ParentId = userId;
                  post.Body = Content;
                  
                  if( u.Profile.UserType == 'standard' )
                      {
                          post.LinkUrl =  URL.getOrgDomainUrl().toExternalForm()+'/'+ contentId;
                      }else{
                          post.LinkUrl = strCommunityURL+'/'+ contentId;
                          post.NetworkScope = String.valueOf(hhflNetWorkID);
                      }
                  toInsert.add(post);
          }
          system.debug('Hello Error');
          system.debug(toInsert);
          return toInsert;
			
							
		 
							 
													   
		 
          
    }*/
    @future
    public static void postToChatter( Set<Id> setIDs, String sObjectName )
    {
		if(System.Label.Post_to_Chatter != null && System.Label.Post_to_Chatter == 'true') {
          System.debug('Debug Log for strURL'+URL.getOrgDomainUrl());
          List<FeedItem> toInsert = new List<FeedItem>();
          String strURL;
          List<sObject> lstLoanApp = new List<sObject>();
          String strCommunityURL;
          ID hhflNetWorkID = [SELECT Id,Name,UrlPathPrefix FROM Network WHERE UrlPathPrefix = 'hhfllos' LIMIT 1].ID;
          System.debug('Debug Log for hhflNetWorkID'+hhflNetWorkID);
          if(String.isNotBlank(hhflNetWorkID)) {
            strCommunityURL = Network.getLoginUrl(hhflNetWorkID);
            System.debug('Debug Log for strCommunityURL'+strCommunityURL);
            strCommunityURL = strCommunityURL.removeEnd('/login');
            System.debug('Debug Log for udpated strCommunityURL'+strCommunityURL);
          }
          
          list<Id> queueList = new list<Id>();
          list<Id> userIdList = new list<Id>();
          if(sObjectName != null && sObjectName != '' && !setIDs.isEmpty()) {
            if(sObjectName =='Loan_Application__c') {
              lstLoanApp = [SELECT Id, Name, OwnerId from Loan_Application__c Where ID IN: setIDs];
            }
            else if(sObjectName =='Task') {
              lstLoanApp = [SELECT Id, OwnerId from Task Where ID IN: setIDs];
            }
          }
          for( sObject objla: lstLoanApp){
              //Schema.SObjectType sObjName = objla.getSObjectType().getDescribe().fields.getMap().get('OwnerId');

              if(objla.get(objla.getSObjectType().getDescribe().fields.getMap().get('OwnerId'))!= null)
              {
               if( String.valueOf(objla.get(objla.getSObjectType().getDescribe().fields.getMap().get('OwnerId'))).contains('005') )
                {
                queueList.add(Id.valueOf(String.valueOf(objla.get(objla.getSObjectType().getDescribe().fields.getMap().get('OwnerId')))));
                }
                else{
                userIdList.add(Id.valueOf(String.valueOf(objla.get(objla.getSObjectType().getDescribe().fields.getMap().get('OwnerId')))));
                //userIdList.add(objla.OwnerId);
                }
              }
             
          }
          list<User> userList= new list<User>();
              if(userIdList.size() >0){
                 
                    userList = [select id, Profile.Name, Profile.UserType from User where id IN: userIdList];

              }
         
          if(queueList.size() >0 )
          {

              List<GroupMember> chatterMembers = [SELECT GroupId,Id,UserOrGroupId FROM GroupMember where GroupId IN: queueList];
              Set<Id> userIds =  new Set<Id>();
              Map<Id,String> maptoURL = new Map<Id,String>();
              for(GroupMember mem : chatterMembers)
              {
                  if(String.valueOf(mem.UserOrGroupId).contains('005'))
                  {
                    userIds.add(mem.UserOrGroupId);
                  }
              }
              if( userIds.size() > 0 )
              {
                  for( User u : [select id, Profile.Name, Profile.UserType from User where Id IN : userIds ] )
                  {
                      if( u.Profile.UserType == 'standard' )
                      {
                          strURL = URL.getOrgDomainUrl().toExternalForm();
                          System.debug('Debug Log for strURL'+URL.getOrgDomainUrl());
                          maptoURL.put(u.Id, strURL);

                      }else{
                            if(String.isNotBlank(strCommunityURL)) {
                                maptoURL.put(u.Id, strCommunityURL);
                            }
                      }
                  }
              }
              for(sObject objloan: lstLoanApp){
                for( GroupMember mem : chatterMembers)
                {
                if(mem.GroupId == objloan.get(objloan.getSObjectType().getDescribe().fields.getMap().get('OwnerId'))){
                     FeedItem post = new FeedItem();
                      post.ParentId = mem.UserOrGroupId;
                        if(objloan.getSObjectType().getDescribe().getName() == 'Loan_Application__c'){
                          post.Body = 'Loan Application '+objloan.get(objloan.getSObjectType().getDescribe().fields.getMap().get('Name'))+' has been assigned to you.';
                        }
                    else{
                          post.Body = 'Task '+objloan.get(objloan.getSObjectType().getDescribe().fields.getMap().get('Subject'))+' has been completed and assigned back to you.';
                    }
                      post.Visibility = 'AllUsers';
                      if(maptoURL.containsKey(mem.UserOrGroupId))
                      {
                          
                          post.LinkUrl =  maptoURL.get(mem.UserOrGroupId)+'/'+ objloan.Id;
                          if(maptoURL.get(mem.UserOrGroupId) == strCommunityURL)
                          {
                              post.NetworkScope = String.valueOf(hhflNetWorkID);
                          }
                       }

                      toInsert.add(post);
                }
                     
                }
            }             
          }
          else if(userList.size() >0){
          for( sObject objloan: lstLoanApp)
            {
              for(user userIDS: userList){
                  if( userIDS != null && userIDS.Id != UserInfo.getUserId() && userIDS.Id == objloan.get(objloan.getSObjectType().getDescribe().fields.getMap().get('OwnerId')) )
                  {
                         // User u = [select id, Profile.Name, Profile.UserType from User where id = :userIDS];
                          FeedItem post = new FeedItem();
                          post.ParentId = userIDS.Id;
                          post.Body ='Loan Application '+objloan.get(objloan.getSObjectType().getDescribe().fields.getMap().get('Name'))+' has been assigned to you.';
                          
                          if( userIDS.Profile.UserType == 'standard' )
                              {
                                  post.LinkUrl =  URL.getOrgDomainUrl().toExternalForm()+'/'+ objloan.Id;
                              }else{
                                  post.LinkUrl = strCommunityURL+'/'+ objloan.Id;
                                  post.NetworkScope = String.valueOf(hhflNetWorkID);
                              }
                          toInsert.add(post);
                  }
              }
            }
          
          }
         
          system.debug('Hello Error');
          system.debug(toInsert);
          //return toInsert;
      
      try{
        insert toInsert;
      }
          catch(Exception ex) {
        System.debug('Following Error Occured'+ex);
      }
	}
    }
    //Used to generate a random string based on the current date time. Used for GroupExposure API Integration for the generation of unique request ID.
    public static String generateRandomRequestID() {
        String hashString =  String.valueOf(Datetime.now().formatGMT('yyyyMMddHHmmssSSS'));
        System.debug('HashString'+hashString+' Length'+hashString.length());
        return hashString;
    }

    public static Decimal generateRandomGroupID() {
        Latest_Number__c cSettings = new Latest_Number__c();
        cSettings = [SELECT Latest_Number_Running__c from Latest_Number__c LIMIT 1];
        Decimal latestNumber = cSettings.Latest_Number_Running__c;
        Decimal num;
        num = latestNumber + 1;
        cSettings.Latest_Number_Running__c = num;
        update cSettings;
        return num;
    }
    
    
    public static void generateUpdatedRepayment(Id loanAppId){
        List<Loan_Repayment__c> lstLoanRepay = [Select Id,Disbursement_Amount__c ,Sanction_Amount__c ,Interest_Rate__c ,Tenure__c,
                                                Repayment_effective_date__c,Disbursal_Date__c,Interest_StartDate__c,EMI_Start_date__c,
                                                dueDay__c,
                                                Loan_Application__c, Loan_Application__r.Approved_cross_sell_amount__c,
                                                Loan_Application__r.Approved_Loan_Amount__c,Loan_Application__r.Approved_ROI__c,Loan_Application__r.Insurance_Loan_Created__c,/***** Added by Saumya For Insurance Loan ***/
                                                Loan_Application__r.Approved_Loan_Tenure__c 
                                                
                                                from Loan_Repayment__c where loan_application__c = :loanAppId];
        List<LMS_Date_Sync__c> lstLMS = [SELECT Id, LastModifiedDate, Current_Date__c FROM LMS_Date_Sync__c order by LastModifiedDate desc LIMIT 1];
                                                        
        if ( !lstLoanRepay.isEmpty() ) {                                
                          if(lstLoanRepay[0].Loan_Application__r.Insurance_Loan_Created__c){/***** Added by Saumya For Insurance Loan ***/
            lstLoanRepay[0].Disbursement_Amount__c  = lstLoanRepay[0].Loan_Application__r.Approved_Loan_Amount__c;
            }
            else{
            lstLoanRepay[0].Disbursement_Amount__c  = lstLoanRepay[0].Loan_Application__r.Approved_Loan_Amount__c != null && lstLoanRepay[0].Loan_Application__r.Approved_cross_sell_amount__c != null ? lstLoanRepay[0].Loan_Application__r.Approved_Loan_Amount__c  + lstLoanRepay[0].Loan_Application__r.Approved_cross_sell_amount__c: lstLoanRepay[0].Loan_Application__r.Approved_Loan_Amount__c ;
      }
      if(lstLoanRepay[0].Loan_Application__r.Insurance_Loan_Created__c){/***** Added by Saumya For Insurance Loan ***/
            lstLoanRepay[0].Sanction_Amount__c = lstLoanRepay[0].Loan_Application__r.Approved_Loan_Amount__c;
            }
            else{
            lstLoanRepay[0].Sanction_Amount__c = lstLoanRepay[0].Loan_Application__r.Approved_Loan_Amount__c != null && lstLoanRepay[0].Loan_Application__r.Approved_cross_sell_amount__c != null ? lstLoanRepay[0].Loan_Application__r.Approved_Loan_Amount__c  + lstLoanRepay[0].Loan_Application__r.Approved_cross_sell_amount__c: lstLoanRepay[0].Loan_Application__r.Approved_Loan_Amount__c ;
      }
            lstLoanRepay[0].Interest_Rate__c = lstLoanRepay[0].Loan_Application__r.Approved_ROI__c != null ? lstLoanRepay[0].Loan_Application__r.Approved_ROI__c : lstLoanRepay[0].Interest_Rate__c;
            lstLoanRepay[0].Tenure__c = lstLoanRepay[0].Loan_Application__r.Approved_Loan_Tenure__c != null ? lstLoanRepay[0].Loan_Application__r.Approved_Loan_Tenure__c : lstLoanRepay[0].Tenure__c;
            lstLoanRepay[0].Disbursal_Date__c = !lstLMS.isEMpty() ? lstLMS[0].Current_Date__c : null;   
            lstLoanRepay[0].Repayment_effective_date__c = !lstLMS.isEMpty() ? lstLMS[0].Current_Date__c : null; 
            lstLoanRepay[0].Interest_StartDate__c  = !lstLMS.isEMpty() ? lstLMS[0].Current_Date__c : null;  
            
            Date dt = lstLoanRepay[0].Disbursal_Date__c; 
            system.debug('@@lstLoanRepay[0].dueDay__c' + lstLoanRepay[0].dueDay__c);
            lstLoanRepay[0].EMI_Start_date__c = Date.newInstance(dt.year(),dt.month(),Integer.valueOf(lstLoanRepay[0].dueDay__c));
            system.debug('@@lstLoanRepay[0].Disbursal_Date__c' + lstLoanRepay[0].Disbursal_Date__c);
            if(lstLoanRepay[0].EMI_Start_date__c < lstLoanRepay[0].Disbursal_Date__c)
            {
                system.debug('in if @@lstLoanRepay[0].EMI_Start_date__c' + lstLoanRepay[0].EMI_Start_date__c);
                lstLoanRepay[0].EMI_Start_date__c = lstLoanRepay[0].EMI_Start_date__c.addMonths(1);
            }
            lstLoanRepay[0].Disbursal_Date__c = !lstLMS.isEMpty() ? lstLMS[0].Current_Date__c : null;   
            update lstLoanRepay[0];
            calloutRepayment(lstLoanRepay[0].id , loanAppId);
        }
    }
    
    @future(callout=true)
    public static void calloutRepayment(String loanRepayId, String loanAppId){
        LMSRepaymentSchedule.RepaymentResponseWrapper objWrap = LMSRepaymentSchedule.pullRepaymentSchedule(loanRepayId,loanAppId);
        system.debug('@@@objWrap' + objWrap);
        if (objWrap.isError) {
            List<Loan_Application__c> lstLoanApp = [Select Id,Repayment_Generated_for_Documents__c from Loan_Application__c where Id =:loanAppId];
            system.debug('@@@insideError');
            lstLoanApp[0].Repayment_Generated_for_Documents__c = false;
            update lstLoanApp;
        }
    }
	
	//Added by Vaishali for BREII
    public static Boolean checkEligibilityForBRE2 (Loan_Application__c la) {
        //BREIIFIX - modified this query to include BRE_2_Eligibility__c field
        Boolean schemeEligibility=true;
        system.debug('la.Scheme and CreatedDate:'+la.BRE_2_Eligibility__c);
        LMS_Bus_Date__c lms = LMS_Bus_Date__c.getInstance();
        Date dt = lms.BRE_2_Cut_Off_Date__c;
        if(la.BRE_2_Eligibility__c == FALSE  || la.CreatedDate <= dt){
            schemeEligibility = FALSE;
        }
        system.debug('schemeEligibility'+schemeEligibility );
        Boolean bre2Eligibility = Boolean.valueOf(Label.bre2Eligibility);
        system.debug('bre2Eligibility '+bre2Eligibility);
        return  schemeEligibility && bre2Eligibility;  
    } 
    //End of patch- Added by Vaishali for BREII
							 
}