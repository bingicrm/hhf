/*=====================================================================
 * Deloitte India
 * Name:TrancheTriggerHandler
 * Description: This is the trigger Handler for Tranche Trigger.
 * Created Date: [12/Oct/2018]
 * Created By:Asit Porwal (Deloitte India) 
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/

public class TrancheTriggerHandler {
	/*
     * @Description :- This method invokes all the methods of the helper class which need to run in after insert state
     * @param :- trigger.new, trigger.old, trigger.newMap, trigger.oldMap
     * @returns :- void
     */
    public static void beforeInsert(List<Tranche__c> newList){        
        TrancheTriggerHelper.checkTrancheCreation(newList);
        TrancheTriggerHelper.checkDisbursalAmt(newList);
        TrancheTriggerHelper.updateStatusAndPendingAmt (newList);
        TrancheTriggerHelper.updateTrancheStage(newList);
        TrancheTriggerHelper.updateSequence(newList);
        TrancheTriggerHelper.validationOnRemainingAmt(newList);
    }
    public static void afterInsert(List<Tranche__c> newList){
        TrancheTriggerHelper.insertUpdateDocs(newList,null);
        TrancheTriggerHelper.insertUpdateDocs(newList,null);//Loan - Downsizing - Added by Shashikant        
    }
    public static void beforeUpdate(List<Tranche__c> newList, Map<id,Tranche__c> oldMap){
        TrancheTriggerHelper.updateTrancheStage(newList); 
        TrancheTriggerHelper.checkDisbursalAmt(newList);
        TrancheTriggerHelper.changeOnRepayment(newList,oldMap);
        TrancheTriggerHelper.updateAmtChanged(newList,oldMap);
        system.debug('started');
		list<Tranche__c> listTranche = new list<Tranche__c>();
        for(Tranche__c tc : newList){
            if(oldMap.get(tc.Id).Disbursal_Amount__c != tc.Disbursal_Amount__c ){
                listTranche.add(tc);
            }
        }
        if(!listTranche.isEmpty()){
            TrancheTriggerHelper.validationOnRemainingAmt(listTranche);//Added by Chitransh for TIL-1472[ReOpened]
        }
    }
    public static void afterUpdate(List<Tranche__c> newList,Map<Id,Tranche__c> oldMap){
        TrancheTriggerHelper.insertUpdateDocs(newList,oldMap);
        //TrancheTriggerHelper.updateAmtChanged(newList,oldMap);
		// [03-06-19] Added by KK for Target Achievement: Code Begins
		set<id> trancheIds = new set<id>();//Added by Chitransh for TIL-1464
        list<Disbursement__c> listDisb = new list<Disbursement__c>();//Added by Chitransh for TIL-1464
        List<Tranche__c> disbTrancheList = new List<Tranche__c>();
        List<Tranche__c> cancTrancheList = new List<Tranche__c>();
		List<Id> laIdLsit = new List<Id>();
		Map<Id, Decimal> MapofTrancheAmtToLA = new Map<Id, Decimal>();/**** Added by Chitransh For TIL-1469 ****/
        for(Tranche__c t: newList){
			/*****************Added by Chitransh for TIL-1464**************/
            if(t.PEMI_Amount__c !=  oldMap.get(t.Id).PEMI_Amount__c){
                system.debug('Pemi Amount Changed.');
                trancheIds.add(t.Id);
            }
            /************End of patch added by Chitransh for TIL-1464********/
            if(t.Tranche_Stage__c == 'Tranche Disbursed' && oldMap.get(t.Id).Tranche_Stage__c == 'Tranche Disbursement Checker'){
                disbTrancheList.add(t);
				laIdLsit.add(t.Loan_Application__c);/**** Added by Chitransh For TIL-1469 ****/
                MapofTrancheAmtToLA.put(t.Loan_Application__c, t.Approved_Disbursal_Amount__c);/**** Added by Chitransh For TIL-1469 ****/
            }
            if(t.Status__c == 'Cancelled' && oldMap.get(t.Id).Status__c == 'Disbursed'){
                cancTrancheList.add(t);
            }
        }
		/*****************Added by Chitransh for TIL-1464**************/
        if(!trancheIds.isEmpty()){
           List<Tranche__c> listTranche = [Select id,PEMI_Amount__c,(Select id,RecordType.Name,PEMI__c from Disbursement__r LIMIT 1) from Tranche__c where id = : trancheIds];
            system.debug('listTranche size ' + listTranche.size());
            for(Tranche__c t : listTranche){
                if(!t.Disbursement__r.isEmpty()){
                    t.Disbursement__r[0].PEMI__c = t.PEMI_Amount__c;
                    listDisb.add(t.Disbursement__r[0]);
                }
            }
            if(!listDisb.isEmpty()){
                system.debug('listDisb size ' + listDisb.size());
                update listDisb;
            }
        }
        /************End of patch added by Chitransh for TIL-1464********/
		
        system.debug('===Disbursed Size==='+disbTrancheList.size());
        if(disbTrancheList.size()>0){
			/**** Added by Chitransh For TIL-1469 ****/
            List<Loan_Application__c> lstloanAppToUpdate = new List<Loan_Application__c>();
            List<Loan_Application__c> lstloanApp = [Select Id, Disburse_Amount__c from Loan_Application__c where Id IN: laIdLsit];
            for(Loan_Application__c objLA: lstloanApp){
                 system.debug('Disburse Amount Pre // Added by Chitransh [10-12-2019]' + objLA.Disburse_Amount__c);
                 objLA.Disburse_Amount__c = (objLA.Disburse_Amount__c!= null ? objLA.Disburse_Amount__c : 0)+ MapofTrancheAmtToLA.get(objLA.Id);
                 system.debug('Tranche Amount // Added by Chitransh [10-12-2019]' + MapofTrancheAmtToLA.get(objLA.Id));
                 system.debug('Disburse Amount Post // Added by Chitransh [10-12-2019]' + objLA.Disburse_Amount__c);
                 lstloanAppToUpdate.add(objLA);
            }
            if(lstloanAppToUpdate.size() > 0){
            update lstloanAppToUpdate;
            }
            /****End of Patch added  by Chitransh For TIL-1469 ****/
            TrancheTriggerHelper.createTrancheAchievement(disbTrancheList);
			TrancheTriggerHelper.checkDisbStatus(disbTrancheList); // Added by KK for Loan Downsizing [16-09-2019]
        }
        system.debug('===Cancelled Size==='+cancTrancheList.size());
        if(cancTrancheList.size()>0){
            TrancheTriggerHelper.deactivateTrancheAchievement(cancTrancheList);
			TrancheTriggerHelper.updateDisbursements(cancTrancheList); // Added by KK for Tranche Cancellation
        }
        // Code Ends
    }
    public static void beforeDelete(Map<Id,Tranche__c> oldMap){
        TrancheTriggerHelper.deleteTranche(oldMap);
        TrancheTriggerHelper.insertUpdateDocs(null,oldMap);        
    }
}