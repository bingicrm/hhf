//@RestResource(urlMapping= '/aaa')
global class CRM_RestAPIGeneratePDF {
    public Boolean checkException{get; set;}
    
    // @HttpPost
    global static void processOnClickEventForCase1(){
        
        HttpResponse res =  callRestAPI();
        parseResponse(res);
    }
    
    public static HttpResponse callRestAPI(){
        
        Http ht = new Http();
        HttpResponse res;
        HttpRequest req = new HttpRequest();
        req.setEndpoint('');
        req.setHeader('content-Type', 'application/json');
        req.setHeader('client-id', '');
        req.setHeader('client-secret', '');
        req.setMethod('POST');
        req.setBody(JSON.serialize(generateJSONRequest()));
        
        if(req.getBody()!=null){
            res = ht.send(req);
        }
        if(res.getStatusCode() == 200){
            parseResponse(res);
            return res;
        }
        else{
            return null;
        }
    }
    
    public static String generateJSONRequest(){
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('branchId', '');
        gen.writeStringField('agreementId', '');
        gen.writeStringField('loanType', '');
        gen.writeStringField('currency', '');
        gen.writeStringField('viewFlag', '');
        gen.writeStringField('reportFlag', '');
        gen.writeStringField('customerName', '');
        gen.writeStringField('loanNumber', '');
        gen.writeEndObject();
        String JSONRequest = gen.getAsString();
        return JSONRequest;
    }
    
    public static void parseResponse(HttpResponse response){
        
        CRM_RePaymentScheduleBean.ReportParameterDetailBean mt = new CRM_RePaymentScheduleBean.ReportParameterDetailBean();
        String email = mt.Email;
        String loanNumber = mt.LoanNumber;
        createPDFWithResponseDetails();
    }
    public static PageReference createPDFWithResponseDetails(){
        Id caseId = Apexpages.currentPage().getParameters().get('id');
        PageReference pg = new PageReference ('/apex/CRM_RepaymentSchedule');
        savePDFasAttachment(caseId);
        return pg;
    }
    public static void savePDFasAttachment(ID caseId){
        PageReference pdf = new PageReference('/apex/CRM_RepaymentSchedule');
        Attachment attach = new Attachment();
        Case cs = [select CaseNumber from Case where Id = :caseId];
        Blob bodyData;
        String htmlBody = '<html><title>Loan application</title>'
            + '<head><h1>Loan application detail page</h1></head>';
        try {
            bodyData = Blob.valueOf(htmlBody);
        } catch (VisualforceException e) {
        }
        attach.Body = bodyData;
        attach.Name = 'LoanApplication1' + '.pdf';
        attach.IsPrivate = false;
        attach.contentType = 'pdf';
        attach.ParentId = caseId;
        insert attach;
    }    
}