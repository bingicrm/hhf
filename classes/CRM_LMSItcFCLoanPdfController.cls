public class CRM_LMSItcFCLoanPdfController {
    public case theCase{get;set;}
    public String fromDate{set;get;}
    public String toDate{set;get;}
    public String fromYear{set;get;}
    public String toYear{set;get;}
    public String loanForText{set;get;}
    public map<string,Object> pdfDetailsMap;
    public String jsonString;
    public HttpResponse response;
    public String endpoint;
    public CRM_CaseItcFALoanResponse caseItcFALoanResponse{get;set;}
    
    public CRM_LMSItcFCLoanPdfController(ApexPages.StandardController stdController) {
        try{this.theCase = (Case)stdController.getRecord();
            Id caseId = ApexPages.currentPage().getParameters().get('id');
            pdfDetailsMap = new map<string,Object>();
            pdfDetailsMap = (Map<String, Object>)JSON.deserializeUntyped(ApexPages.currentPage().getParameters().get('pdfDetailsMap'));
            fromDate = (string)pdfDetailsMap.get('caseFromDate');
            toDate = (string)pdfDetailsMap.get('caseToDate');
            fromYear = (string)pdfDetailsMap.get('fromYear');
            toYear = (string)pdfDetailsMap.get('toYear');
            loanForText = 'FINAL IT CERTIFICATE FOR CLAIMING DEDUCTIONS UNDER SECTION 24 (b) & 80C (2) (xviii) OF THE INCOME TAX ACT, 1961';
            
            theCase = [select LMS_Application_ID__c, Loan_Application_Number__c, LD_Branch_ID__c from case where Id=: caseId];
            Rest_Service__mdt rs = [Select Client_Username__c, Client_Password__c, Service_EndPoint__c from Rest_Service__mdt where DeveloperName = 'CRM_Case_Itc'];
            
            String username = rs.Client_Username__c;
            String password = rs.Client_Password__c;
            endpoint = rs.Service_EndPoint__c;
            Blob headerValue = Blob.valueOf(rs.Client_Username__c+ ':' +rs.Client_Password__c);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            
            Map<String, String> jsonMap = new Map<String, String>();
            jsonMap.put('agreementId', theCase.LMS_Application_ID__c);
            jsonMap.put('loanNumber', theCase.Loan_Application_Number__c);
            jsonMap.put('branchId', theCase.LD_Branch_ID__c);
            jsonMap.put('currency', 'INR');
            jsonMap.put('viewFlag', 'V');
            jsonMap.put('reportFlag', 'ITC');
            jsonMap.put('fromDate', fromDate);
            jsonMap.put('toDate', toDate);
            jsonMap.put('itCertType', 'F');
            
            jsonString = JSON.Serialize(jsonMap);
            
            Http p=new Http();
            HttpRequest request =new HttpRequest();
            
            request.setHeader('Content-Type','application/json');
            request.setHeader('accept','application/json');
            
            request.setHeader('Authorization', authorizationHeader);
            request.setHeader('operation-flag','R');
            
            request.setEndPoint(endpoint);
            request.setMethod('POST');
            request.setBody(jsonString);
            
            response =p.send(request);
            caseItcFALoanResponse = CRM_CaseItcFALoanResponse.parse(response.getBody());
            
            if(response.getStatusCode() != 200) {
                throw new CalloutException('');
            }
           }
        catch(exception e){
            throw new AuraHandledException('');
        }
    }
    
    //Create integration logs
    public void logUtility(){
        try{
            system.debug('Repayment controller LOG UTILITY CALLED');
            LogUtility.createIntegrationLogs(jsonstring,response,endpoint);
        }
        catch(exception e){
            throw new AuraHandledException('');
        }
    }
}