@isTest
public class TestStoreUCIC {
	@testSetup static void testData() {
            Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
            Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
            
            Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',
                                      Date_of_Incorporation__c=System.today(),
                                      Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
            insert acc;
            
            Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                       Phone='9989786750',RecordTypeId=RecordTypeIdAccount, Customer_ID__c='1234589756754');
            insert acc1;
            
            acc1 =[Select Id, PAN__c, Name, Date_of_Incorporation__c, Phone, RecordTypeId, Customer_ID__c From Account where Id =: acc1.Id];
            Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
            insert con1;
            
            Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                                Scheme_Group_ID__c='12');
            insert objScheme;
            
            Loan_Application__c la1 = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                              Transaction_type__c='PB',Requested_Amount__c=100000,
                                                              Loan_Purpose__c='11', Loan_Number__c ='12345');
            insert la1;
    }
    
    public static testMethod void makeUCICCalloutTest(){
        Account accObj = [SELECT Id,Customer_ID__c FROM Account WHERE Name='Test Account1' LIMIT 1];
        Loan_Application__c la = [SELECT Id FROM Loan_Application__c LIMIT 1];
        Loan_Contact__c lc = [SELECT Id, Name,Applicant_Type__c, Applicant_Status__c, Constitution__c, Customer_segment__c, 
                              Loan_Applications__c FROM Loan_Contact__c WHERE Loan_Applications__c =: la.Id];
        Id RecordTypeIdCustUCIC = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('UCIC').getRecordTypeId();
        Customer_Integration__c objcust = new Customer_Integration__c();
        objcust.UCIC_Negative_API_Response__c = true;
        objcust.UCIC_Negative_API_Response__c = false;
        objcust.Loan_Application__c = la.Id;
        objcust.Loan_Contact__c = lc.Id;
        objcust.RecordTypeId = RecordTypeIdCustUCIC;
        insert objcust;
        
        UCICResponseBody req = new UCICResponseBody();
        UCICResponseBody.cls_Customer req1= new UCICResponseBody.cls_Customer();
        req1.Customer_id = accObj.Customer_ID__c;
        req1.Applicant_Type = 'test';
        req1.HUID = '656577';
        req.Customer = new List<UCICResponseBody.cls_Customer>{req1};
        req.Application_id = '12345';
        req.Source_System = 'test';
		String myJSON = JSON.serialize(req);
        
        Test.startTest();
 		RestRequest request = new RestRequest();
        //request.requestUri ='https://cs13.salesforce.com/services/apexrest/upsertaccount';
        request.requestUri ='/services/apexrest/AccountService/storeUCICId';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
		
        RestContext.request = request;
		StoreUCIC.ResultResponse res = StoreUCIC.insertUCICId();
        Test.stopTest();
    }
}