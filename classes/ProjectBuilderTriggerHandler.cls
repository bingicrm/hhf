public class ProjectBuilderTriggerHandler {
    /********************************************************************************************************************************************************************
            Created By  : Shobhit Saxena(Deloitte)
            Description : This class acts as a handler for various Trigger Automations performed on Project Builder Records.
    
    /********************************************************************************************************************************************************************/
    
    public static void onBeforeInsert(List<Project_Builder__c> lstTriggerNew) {
        /**************************************************************************************************************************************************
            Purpose of Code : 
            
            (1) Validation on GSTIN of the Builder's associated Account
            (2) Assignment of Record Types on the basis of the Account Record Type, If Builder Specified is Individual, then Individual Record Type is
                assigned, otherwise Corporate Record Type is Asssigned.
            
        ***************************************************************************************************************************************************/
        Set<Id> setAccountIds = new Set<Id>();
        for(Project_Builder__c objProjectBuilder : lstTriggerNew) {
            if(String.isNotBlank(objProjectBuilder.Builder_Name__c)) {
                setAccountIds.add(objProjectBuilder.Builder_Name__c);
            }
        }
        //Check for Blank GST on associated Builder Record, if the corresponding custom label value is true.
        
        if(!setAccountIds.isEmpty()) {
            Map<Id,Account> mapAccounts = new Map<Id,Account>([Select Id, GSTIN__c, RecordTypeId, RecordType.Name, RecordType.DeveloperName From Account Where Id IN: setAccountIds]);
            System.debug('Debug Log for mapAccounts'+mapAccounts);
            if(!mapAccounts.isEmpty()) {
                for(Project_Builder__c objProjectBuilder : lstTriggerNew) {
                    if(System.Label.CheckforBlankGST == 'true' && String.isNotBlank(objProjectBuilder.Builder_Name__c) && mapAccounts.containsKey(objProjectBuilder.Builder_Name__c) && String.isBlank(String.valueOf(mapAccounts.get(objProjectBuilder.Builder_Name__c).GSTIN__c))) {
                        objProjectBuilder.addError('GSTIN is not specified for the selected Builder. Please specify GSTIN on the Builder first.');
                    }
                    
                    if(String.isNotBlank(objProjectBuilder.RecordTypeId)) {
                        if(String.isNotBlank(objProjectBuilder.Builder_Name__c) && mapAccounts.containsKey(objProjectBuilder.Builder_Name__c) && mapAccounts.get(objProjectBuilder.Builder_Name__c).RecordTypeId == Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.strAccRecTypeBIndv).getRecordTypeId()) {
                            if(objProjectBuilder.RecordTypeId != Schema.SObjectType.Project_Builder__c.getRecordTypeInfosByName().get(Constants.strPBRecTypeIndv).getRecordTypeId()) {
                                objProjectBuilder.addError('Individual Account cannot be tagged when the Record Type Specified is Builder - Corporate.');
                            }
                        }
                        if(String.isNotBlank(objProjectBuilder.Builder_Name__c) && mapAccounts.containsKey(objProjectBuilder.Builder_Name__c) && mapAccounts.get(objProjectBuilder.Builder_Name__c).RecordTypeId == Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.strAccRecTypeBCorp).getRecordTypeId()) {
                            if(objProjectBuilder.RecordTypeId != Schema.SObjectType.Project_Builder__c.getRecordTypeInfosByName().get(Constants.strPBRecTypeCorp).getRecordTypeId()) {
                                objProjectBuilder.addError('Corporate Account cannot be tagged when the Record Type Specified is Builder - Individual.');
                            }
                        }
                    }
                    
                    if(String.isNotBlank(objProjectBuilder.Builder_Name__c) && mapAccounts.containsKey(objProjectBuilder.Builder_Name__c) && mapAccounts.get(objProjectBuilder.Builder_Name__c).RecordTypeId == Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.strAccRecTypeBIndv).getRecordTypeId()) {
                        objProjectBuilder.RecordTypeId = Schema.SObjectType.Project_Builder__c.getRecordTypeInfosByName().get(Constants.strPBRecTypeIndv).getRecordTypeId();
                    }
                    if(String.isNotBlank(objProjectBuilder.Builder_Name__c) && mapAccounts.containsKey(objProjectBuilder.Builder_Name__c) && mapAccounts.get(objProjectBuilder.Builder_Name__c).RecordTypeId == Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.strAccRecTypeBCorp).getRecordTypeId()) {
                        objProjectBuilder.RecordTypeId = Schema.SObjectType.Project_Builder__c.getRecordTypeInfosByName().get(Constants.strPBRecTypeCorp).getRecordTypeId();
                        if(String.isNotBlank(objProjectBuilder.Constitution__c)) {
                            if(objProjectBuilder.Constitution__c != '18'  && objProjectBuilder.Constitution__c != '19' && objProjectBuilder.Constitution__c != '25') {
                                objProjectBuilder.CIBIL_Verified__c = true;
                            }
                        }
                    }
                    
                }
            }
        }
        
    }
    
    public static void onAfterInsert(Map<Id,Project_Builder__c> mapTriggerNew) {
        /**************************************************************************************************************************************************
            Purpose of Code : 
            
            (1) To create Document Checklist Records for the Builder Related Document Types
            
            
        ***************************************************************************************************************************************************/
        Set<Id> setAccountIds = new Set<Id>();
        Set<String> setDocMastersNotRequired = new Set<String>();
        setDocMastersNotRequired.add('GSTIN - Registration certificate');
        setDocMastersNotRequired.add('GST Return for last 1 year');
        setDocMastersNotRequired.add('Recommendation Note');
        setDocMastersNotRequired.add('Technical Vetting Report');
        System.debug('After Insert from Project Builder Trigger Handler');
        List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
        Map<String, String> mapProjectBuilderIdtoAPFType = new Map<String, String>();
        Map<Id,APF_Document_Checklist__c> mapIdtoAPFDocChkLst = new Map<Id,APF_Document_Checklist__c>([Select Id, Name, Active_del__c, Valid_for_Project_Type__c, Document_Master__c, Document_Master__r.Name, Document_Type__c, Document_Type__r.Name, Mandatory__c From APF_Document_Checklist__c WHERE Document_Type__r.Name = :Constants.strAPFProjDocTypeBuilder AND Document_Master__r.Name NOT IN: setDocMastersNotRequired]);
        List<Project_Document_Checklist__c> lstProjDocChkLst = new List<Project_Document_Checklist__c>();
        if(!mapTriggerNew.isEmpty() && !mapIdtoAPFDocChkLst.isEmpty()) {
            List<Project_Builder__c> lstProjectBuilder = [Select Id, Name, Project__r.APF_Type__c From Project_Builder__c Where Id IN: mapTriggerNew.keySet()];
            if(!lstProjectBuilder.isEmpty()) {
                for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                    mapProjectBuilderIdtoAPFType.put(objProjectBuilder.Name, objProjectBuilder.Project__r.APF_Type__c);
                }
            }
            for(APF_Document_Checklist__c objAPFDC : mapIdtoAPFDocChkLst.values()) {
                for(Project_Builder__c objProjectBuilder : mapTriggerNew.values()) {
                    if(!mapProjectBuilderIdtoAPFType.isEmpty() && mapProjectBuilderIdtoAPFType.containsKey(objProjectBuilder.Name) && String.isNotBlank(mapProjectBuilderIdtoAPFType.get(objProjectBuilder.Name)) && mapProjectBuilderIdtoAPFType.get(objProjectBuilder.Name) == Constants.strApfTypeNew) {
                        Project_Document_Checklist__c objProjDocChkLst = new Project_Document_Checklist__c();
                            objProjDocChkLst.Document_Master__c = objAPFDC.Document_Master__c;
                            objProjDocChkLst.Document_Type__c = objAPFDC.Document_Type__c;
                            objProjDocChkLst.Mandatory__c = objAPFDC.Mandatory__c;
                            objProjDocChkLst.Project__c = objProjectBuilder.Project__c;
                            objProjDocChkLst.Status__c = Constants.strDocStatusPending;
                            objProjDocChkLst.Project_Builder__c = objProjectBuilder.Id;
                            objProjDocChkLst.Valid_for_Project_Type__c = objAPFDC.Valid_for_Project_Type__c;
                        lstProjDocChkLst.add(objProjDocChkLst);
                    }
                }
            }
            
            if(!lstProjDocChkLst.isEmpty()) {
                System.debug('Debug Log for lstProjDocChkLst'+lstProjDocChkLst.size());
                DataBase.SaveResult[] lstDSR = Database.insert(lstProjDocChkLst, false);
                for(Database.SaveResult objDSR : lstDSR) {
                    if (objDSR.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully inserted record with Id ' + objDSR.getId());
                    }
                    else{
                        // Operation failed, so get all errors                
                        for(Database.Error err : objDSR.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Fields that affected this error: ' + err.getFields());
                            Error_Log__c objErrorLog = new Error_Log__c();
                                //objErrorLog.Loan_Applications__c = objLA.Id;
                                objErrorLog.Error_Code__c = String.valueOf(err.getStatusCode());
                                objErrorLog.Type__c = 'Document Checklist Creation';
                                objErrorLog.Error_Message__c = err.getMessage();
                                objErrorLog.URL__c = 'APF';
                            lstErrorLog.add(objErrorLog);
                        }
                    }
                }
            }
            
            if(!lstErrorLog.isEmpty()) {
                try {
                    insert lstErrorLog;
                }
                catch(Exception ex) {
                    System.debug('Exception occured'+ex);
                }
            }
        }
    }
    
    public static void onBeforeUpdate(Map<Id,Project_Builder__c> mapTriggerNew, Map<Id,Project_Builder__c> mapTriggerOld) {
        /**************************************************************************************************************************************************
            Purpose of Code : 
            (1) Validation on GSTIN of the Builder's associated Account
            (2) To set Cibil Verified to True for Corporates not belonging to the relevant Constitution
            
        ***************************************************************************************************************************************************/
        Set<Id> setAccountIds = new Set<Id>();
        List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
        for(Project_Builder__c objProjectBuilder : mapTriggerNew.values()) {
            if(objProjectBuilder.Builder_Name__c != mapTriggerOld.get(objProjectBuilder.Id).Builder_Name__c) {
                setAccountIds.add(objProjectBuilder.Builder_Name__c);
            }
            if(objProjectBuilder.RecordTypeId == Schema.SObjectType.Project_Builder__c.getRecordTypeInfosByName().get(Constants.strPBRecTypeCorp).getRecordTypeId()) {
                if(String.isNotBlank(objProjectBuilder.Constitution__c) && objProjectBuilder.Constitution__c != '18' && objProjectBuilder.Constitution__c != '19' && objProjectBuilder.Constitution__c != '25') {
                    objProjectBuilder.CIBIL_Verified__c = true;
                }
            }
        }
        //Check for Blank GST on associated Builder Record, if the corresponding custom label value is true.
        if(System.Label.CheckforBlankGST == 'true') {
            if(!setAccountIds.isEmpty()) {
                Map<Id,Account> mapAccounts = new Map<Id,Account>([Select Id, GSTIN__c From Account Where Id IN: setAccountIds]);
                System.debug('Debug Log for mapAccounts'+mapAccounts);
                if(!mapAccounts.isEmpty()) {
                    for(Project_Builder__c objProjectBuilder : mapTriggerNew.values()) {
                        if(String.isNotBlank(objProjectBuilder.Builder_Name__c) && mapAccounts.containsKey(objProjectBuilder.Builder_Name__c) && String.isBlank(String.valueOf(mapAccounts.get(objProjectBuilder.Builder_Name__c).GSTIN__c))) {
                            objProjectBuilder.addError('GSTIN is not specified for the selected Builder. Please specify GSTIN on the Builder first.');
                        }
                    }
                }
            }
        }
        
    }
    
    public static void onAfterUpdate(Map<Id,Project_Builder__c> mapTriggerNew, Map<Id,Project_Builder__c> mapTriggerOld) {
        /*********************************************************************************************************************************************************
            Purpose of Code : 
            (1) To populate Project Category on Project, once the Builder Category is specified on the Builder Record. (Pt. 13 of APF Credit Review Cycle in BRD)
            
        *********************************************************************************************************************************************************/
        Set<id> setProjectIds = new Set<Id>();
        List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
        List<Project__c> lstProject;
        for(Project_Builder__c objProjectBuilder : mapTriggerNew.values()) {
            if(objProjectBuilder.Builder_Category__c != mapTriggerOld.get(objProjectBuilder.Id).Builder_Category__c) {
                setProjectIds.add(objProjectBuilder.Project__c);
            }
        }
        if(!setProjectIds.isEmpty()) {
            ProjectBuilderTriggerHelper.populateProjectCategoryonProject(setProjectIds);
        }
    }
}