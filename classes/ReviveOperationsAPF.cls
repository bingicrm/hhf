public class ReviveOperationsAPF {
    @AuraEnabled
    public static Project__c getProjectDetails(Id projectId){
        system.debug('=='+projectId);
        return [SELECT  Id, Name, Stage__c, Sub_Stage__c, RecordTypeID, Comments__c, Date_of_Rejection__c, Remarks_Revival__c, Revive_Application__c, OwnerId
                FROM    Project__c
                WHERE   Id = :projectId]; 
    }
    
    @AuraEnabled
    public static String reviveApplication(Project__c la){
        
        Savepoint sp = Database.setSavepoint();
        String msg;
        msg='';
        
        system.debug('UserInfo.getUserId()::'+UserInfo.getUserId()+'la.OwnerId::'+la.OwnerId);
        try {
        if(Approval.isLocked(la) == true){
            msg = 'Application has already been submitted for Approval.';
        }
       
        //else if(UserInfo.getUserId() != la.OwnerId && !Test.isRunningTest()){
            //msg= 'You are not authorized for Revive.';
        //}
        
        else{
            system.debug('@@la.Date_of_Rejection__c' + la.Date_of_Rejection__c);
            system.debug('@@la.Remarks_Revival__c' + la.Remarks_Revival__c);
            system.debug('@@la.' + la);
            if(la.Date_of_Rejection__c != null && la.Date_of_Rejection__c.daysBetween(System.Today()) < 30){
                if(la.Remarks_Revival__c != NULL) {
                   // if((la.Rejected_Cancelled_Stage__c == 'Credit Decisioning' || la.Rejected_Cancelled_Stage__c == 'Operation Control' || la.Rejected_Cancelled_Stage__c == 'Customer Onboarding' || (la.Rejected_Cancelled_Stage__c == 'Loan Rejected' && la.Rejected_Cancelled_Sub_Stage__c =='CIBIL Reject') || la.Rejected_Cancelled_Sub_Stage__c == 'Hunter Review')/* && la.SUD_count__c == 0*/){// Added by Saumya For Reject Revive BRD
                        //la.Rejected_Cancelled_Sub_Stage__c == 'Hunter Review' added by Abhilekh on 29th January 2020 for Hunter BRD
                       
                        
                        //la.recordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('APF Initiation').getRecordTypeId();
                        //la.Stage__c = Constants.strApfStgAppInit;//objrejectRevive.Revive_Stage__c;
                    	//la.Sub_Stage__c = Constants.strApfSubStgPDC;//objrejectRevive.Revive_Sub_Stage__c;
                        la.Revive_Application__c = true;
                        update la;
                    
                    List<Promoter__c> lstPromoter = new List<Promoter__c>();
                    List<Project_Builder__c> lstProjectBuilder = [Select Id, Name, RecordTypeId, RecordType.DeveloperName, (Select Id, Name, RecordTypeId From Promoters__r) From Project_Builder__c Where Project__c =: la.Id];
                    for(Project_Builder__c objPB : lstProjectBuilder) {
                        if(objPB.RecordType.DeveloperName == 'Read_Only_Corporate') {
                            objPB.RecordTypeId = Schema.SObjectType.Project_Builder__c.getRecordTypeInfosByName().get(Constants.strPBRecTypeCorp).getRecordTypeId();
                        }
                        else if(objPB.RecordType.DeveloperName == 'Read_Only_Individual') {
                            objPB.RecordTypeId = Schema.SObjectType.Project_Builder__c.getRecordTypeInfosByName().get(Constants.strPBRecTypeIndv).getRecordTypeId();
                        }
                        if(!objPB.Promoters__r.isEmpty()) {
                            for(Promoter__c objPromoter : objPB.Promoters__r) {
                                objPromoter.RecordTypeId = Schema.SObjectType.Promoter__c.getRecordTypeInfosByName().get('Promoter').getRecordTypeId();
                                lstPromoter.add(objPromoter);
                            }
                        }
                    }
                    update lstProjectBuilder;
                    if(!lstPromoter.isEmpty()) {
                       update lstPromoter;
                    }
                    
                     Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                        req1.setComments('Submitting request for approval.');
                        req1.setObjectId(la.id);// Submit the record to specific process
                        req1.setProcessDefinitionNameOrId('Project_Revival_Approval_Process');
                        Approval.ProcessResult result = Approval.process(req1);
                       
                        // Verify the result
                   
                        System.assert(result.isSuccess());
                       
                        System.assertEquals(
                            'Pending', result.getInstanceStatus(),
                            'Instance Status'+result.getInstanceStatus());
                    msg='Application Submitted for Revival Approval';    
                    
                    
                }
                else{
                    msg = 'Please fill in remarks before reviving the Loan Application.';
                }
                
            }
            else{
                msg = 'This application cannot be Revived as it has passed the 30 days limit.';
            }
        }

        
           /* if(Approval.isLocked(la) == true){
                Approval.UnlockResult urList = Approval.unlock(la, false);
                if (urList.isSuccess()){
                    system.debug('@@@ inside if locked');
                    update la;
                }
                else{
                }
            }
            else if(!Approval.isLocked(la) == true){
                system.debug('@@@ inside if ');
                update la;       
                system.debug('@@@ after inside if ' + la);         
            }
            system.debug('=========>>>' + la.recordtypeId);*/
            
        } catch (DMLException e) {
            system.debug('@@exception1' + e);
            Database.rollback(sp);
            
        } catch (Exception e) {
            system.debug('@@exception2' + e);
            Database.rollback(sp);
            
                
        }
        return msg;
    }
}