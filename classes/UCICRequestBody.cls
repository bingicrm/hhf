public class UCICRequestBody {
    public String Application_Id;   //1234
    public String Application_Status;   //Active
    public String Application_Type; //R
    public cls_Individuals[] Individuals;
    public cls_Loanproductdetail Loanproductdetail;
    public String Source_Channel;   //SFDC-CPL
	public String UCICFlag_SFDC; //Added by Abhilekh on 9th September 2020 for UCIC Phase II
    public String DBTOMATCHED; //Added by Abhilekh on 22nd September 2020 for UCIC Phase II
    
    public class cls_Individuals {
        public String Aadhar;   //
        public cls_Addresses[] Addresses;
        public String Applicant_Type;   //Borrower
        public String Areacode; //
        public String Cif_No;   //
        public String Constitution; //
        public String Customer_Id;  //FWWEFEFEWFFEWWFFFW
        public String Directline_Mobile2;   //
        public String Dl_No;    //
        public String Dob;  //13/10/1982
        public String Emailid;  //abc@test.com
        public String Extension;    //
        public String Father_Fname; //
        public String Father_Lname; //
        public String Father_Mname; //
        public String Fname;    //LEAD
        public String Gender;   //Male
        public String Indv_Corp_Flag;   //I
        public String Laa_Rc_Flag;  //
        public String Lname;    //TEST
        public String Lpgno;    //
        public String Match_Status_Un;  //
        public String Mobile;   //9654755422
        public String Mother_Fname; //
        public String Mother_Lname; //
        public String Pan_Card; //BANKS1236B
        public String Passport_No;  //
        public String Phone2;   //
        public String Rationcardno; //
        public String Relation; //
        public String Spouse_Fname; //
        public String Spouse_Lname; //
        public String Spouse_Mname; //
        public String Tanno_Gstanno;    //
        public String Voter_Id; //
        public String Workprofile;  //
    }
    public class cls_Addresses {
        public String Address_Type; //Communicaton
        public String Address1; //b1 , 2nd floor SGN
        public String Address2; //
        public String Address3; //
        public String City_District;    //NEW DELHI
        public String Pin_Code; //110092
        public String State;    //DELHI
        public String Village;  //
    }
    public class cls_Loanproductdetail {
        public String Asset_Details;    //
        public String Lob;  //PL
        public String Lob_Product;  //PL
        public String Lob_Segment;  //PL
        public String Program;  //
        public String Source;   //SFDC-CPL
    }
}