public class cls_DeleteMovedFiles {

    public static void deleteS3FilesApplicant(Document_Migration_Log__c objDML) {
        Set<Id> setApplicantRelatedDocs = new Set<Id>();
        List<ContentDocument> listToDelete = new List<ContentDocument>();
        List<ContentDocument> listToDeleteFinal = new List<ContentDocument>();
        Set<Id> setAllDocuments = new Set<Id>();
        List<ContentDocumentLink> lstContentDocLinks;
        List<ContentVersion> lstContentVersion;
        Set<Id> setContentDocumentLinkIds = new Set<Id>();
        Set<Id> setContentVersionId = new Set<Id>();
        Map<Id,Id> mapConDocLnktoDocChkLst = new Map<Id,Id>();
        Map<Id,Id> mapConDocIdtoConDocLink = new Map<Id,Id>();
        Map<Id, Document_Checklist__c> mapDocIdtoDocRecord = new Map<Id, Document_Checklist__c>();
        List<S3_Folders_Configuration__mdt> lstS3ConfigData = [SELECT Id, Delete_files__c, Folder_Name__c, MasterLabel, DeveloperName, NamespacePrefix FROM S3_Folders_Configuration__mdt];
        Map<String, S3_Folders_Configuration__mdt> mapDeveloperNametoRecord = new Map<String, S3_Folders_Configuration__mdt>();
        try {
            System.debug('Debug Log for lstS3ConfigData'+lstS3ConfigData.size());
            if(!lstS3ConfigData.isEmpty()) {
                for(S3_Folders_Configuration__mdt objS3ConfigData : lstS3ConfigData) {
                    mapDeveloperNametoRecord.put(objS3ConfigData.DeveloperName, objS3ConfigData);
                }
            }
            System.debug('Debug Log for mapDeveloperNametoRecord'+mapDeveloperNametoRecord.size());
            String customerId;
            List<Document_Checklist__c> lstDocChkLst = [SELECT Id, Loan_Applications__c, Loan_Contact__c, Loan_Contact__r.Customer__r.Customer_ID__c, Document_Master__c, Document_Type__c, Customer_Type__c, Loan_Contact__r.Applicant_Type__c From Document_Checklist__c Where Id =: objDML.Document_Checklist__c];
            System.debug('Debug Log for lstDocChkLst'+lstDocChkLst.size());
            if(!lstDocChkLst.isEmpty()) {
                for(Document_Checklist__c objDC : lstDocChkLst) {
                    if(String.isNotBlank(objDC.Loan_Applications__c) && String.isNotBlank(objDC.Loan_Contact__c)) {
                        if(objDC.Loan_Contact__r.Applicant_Type__c == 'Applicant') {
                            setApplicantRelatedDocs.add(objDC.Id);
                            setAllDocuments.add(objDC.Id);
                            customerId = objDC.Loan_Contact__c;
                            mapDocIdtoDocRecord.put(objDC.Id, objDC);
                        }
                    }
                }
            }
            
            System.debug('Debug Log for setApplicantRelatedDocs'+setApplicantRelatedDocs.size());  
            System.debug('Debug Log for setAllDocuments'+setAllDocuments.size()); 
            System.debug('Debug Log for customerId'+customerId);  
            
            if(!setAllDocuments.isEmpty()) {
                lstContentDocLinks = [select id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where LinkedEntityId IN: setAllDocuments];
                System.debug('Debug Log for lstContentDocLinks'+lstContentDocLinks.size());
                
                for(ContentDocumentLink objCDL : lstContentDocLinks) { 
                    setContentDocumentLinkIds.add(objCDL.ContentDocumentId);
                    mapConDocLnktoDocChkLst.put(objCDL.ContentDocumentId, objCDL.LinkedEntityId);
                    mapConDocIdtoConDocLink.put(objCDL.ContentDocumentId, objCDL.Id);
                }
                System.debug('Debug Log for mapConDocIdtoConDocLink'+mapConDocIdtoConDocLink.size());
                System.debug('Debug Log for mapConDocIdtoConDocLink');
                for(Id keyId : mapConDocIdtoConDocLink.keySet()) {
                    System.debug('Key'+keyId+' Value'+mapConDocIdtoConDocLink.get(keyId));
                }
                System.debug('Debug Log for mapConDocLnktoDocChkLst');
                for(Id keyId : mapConDocLnktoDocChkLst.keySet()) {
                    System.debug('Key'+keyId+' Value'+mapConDocLnktoDocChkLst.get(keyId));
                }
                
                lstContentVersion = [SELECT Id, Title, VersionData, CreatedDate, ContentDocumentId, FileExtension FROM ContentVersion WHERE IsLatest=true AND ContentDocumentId =: objDML.Document_Id__c];
                System.debug('Debug Log for lstContentVersion'+lstContentVersion.size());
                List<ContentVersion> lstContentDocsApplicant = new List<ContentVersion>();
                
                if(!lstContentVersion.isEmpty()) {
                    for(ContentVersion objContentVersion : lstContentVersion) {
                        if(mapConDocLnktoDocChkLst.containsKey(objContentVersion.ContentDocumentId) && String.isNotBlank(mapConDocLnktoDocChkLst.get(objContentVersion.ContentDocumentId))) {
                            if(!setApplicantRelatedDocs.isEmpty() && setApplicantRelatedDocs.contains(mapConDocLnktoDocChkLst.get(objContentVersion.ContentDocumentId))) {
                                //Send Document for Addition in Applicant Related Document Folder
                                lstContentDocsApplicant.add(objContentVersion);
                            }
                        }
                    }
                    System.debug('Debug Log for lstContentDocsApplicant'+lstContentDocsApplicant.size());
                    
                    
                    if(!lstContentDocsApplicant.isEmpty()) {
                        for(ContentVersion objApplicantDoc : lstContentDocsApplicant) {
                            listToDelete.add(new ContentDocument(Id = objApplicantDoc.ContentDocumentId));
                        }
                        System.debug('Debug Log for listToDelete'+listToDelete);
                        System.debug('Debug Log for listToDelete'+listToDelete.size());
                        if(!listToDelete.isEmpty()) {
                            delete listToDelete;
                        }
                    }
                }
            }
        }
        catch(Exception ex) {
            System.debug('Debug Log for Exception'+ex);
        }
    
    }
    
    public static void deleteS3FilesCoApplicantGuarantor(Document_Migration_Log__c objDML) {
        List<ContentDocument> listToDelete = new List<ContentDocument>();
        List<ContentDocument> listToDeleteFinal = new List<ContentDocument>();
        Set<Id> setCoApplicantGuarantorDocs = new Set<Id>();
        Set<Id> setAllDocuments = new Set<Id>();
        List<ContentDocumentLink> lstContentDocLinks;
        List<ContentVersion> lstContentVersion;
        Set<Id> setContentDocumentLinkIds = new Set<Id>();
        Set<Id> setContentVersionId = new Set<Id>();
        Map<Id,Id> mapConDocLnktoDocChkLst = new Map<Id,Id>();
        Map<Id,Id> mapConDocIdtoConDocLink = new Map<Id,Id>();
        Map<Id, Document_Checklist__c> mapDocIdtoDocRecord = new Map<Id, Document_Checklist__c>();
        List<Document_Checklist__c> lstDocChkLst = [SELECT Id, Loan_Applications__c, Loan_Contact__c, Loan_Contact__r.Customer__r.Customer_ID__c, Document_Master__c, Document_Type__c, Customer_Type__c, Loan_Contact__r.Applicant_Type__c From Document_Checklist__c Where Id =: objDML.Document_Checklist__c];
        try {
            System.debug('Debug Log for lstDocChkLst'+lstDocChkLst.size());
            if(!lstDocChkLst.isEmpty()) {
                for(Document_Checklist__c objDC : lstDocChkLst) {
                    if(String.isNotBlank(objDC.Loan_Applications__c) && String.isNotBlank(objDC.Loan_Contact__c)) {
                        if(objDC.Loan_Contact__r.Applicant_Type__c == 'Co- Applicant' || objDC.Loan_Contact__r.Applicant_Type__c == 'Guarantor') {
                            setCoApplicantGuarantorDocs.add(objDC.Id);
                            setAllDocuments.add(objDC.Id);
                            mapDocIdtoDocRecord.put(objDC.Id, objDC);
                        }
                    }
                }
            }
            System.debug('Debug Log for setCoApplicantGuarantorDocs'+setCoApplicantGuarantorDocs.size());
            System.debug('Debug Log for setAllDocuments'+setAllDocuments.size());   
            List<S3_Folders_Configuration__mdt> lstS3ConfigData = [SELECT Id, Delete_files__c, Folder_Name__c, MasterLabel, DeveloperName, NamespacePrefix FROM S3_Folders_Configuration__mdt];
            Map<String, S3_Folders_Configuration__mdt> mapDeveloperNametoRecord = new Map<String, S3_Folders_Configuration__mdt>();
            System.debug('Debug Log for lstS3ConfigData'+lstS3ConfigData.size());
            if(!lstS3ConfigData.isEmpty()) {
                for(S3_Folders_Configuration__mdt objS3ConfigData : lstS3ConfigData) {
                    mapDeveloperNametoRecord.put(objS3ConfigData.DeveloperName, objS3ConfigData);
                }
            }
            System.debug('Debug Log for mapDeveloperNametoRecord'+mapDeveloperNametoRecord.size());
            if(!setAllDocuments.isEmpty()) {
                lstContentDocLinks = [select id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where LinkedEntityId IN: setAllDocuments];
                System.debug('Debug Log for lstContentDocLinks'+lstContentDocLinks.size());
                
                for(ContentDocumentLink objCDL : lstContentDocLinks) { 
                    setContentDocumentLinkIds.add(objCDL.ContentDocumentId);
                    mapConDocLnktoDocChkLst.put(objCDL.ContentDocumentId, objCDL.LinkedEntityId);
                    mapConDocIdtoConDocLink.put(objCDL.ContentDocumentId, objCDL.Id);
                }
                System.debug('Debug Log for mapConDocIdtoConDocLink'+mapConDocIdtoConDocLink.size());
                System.debug('Debug Log for mapConDocIdtoConDocLink');
                for(Id keyId : mapConDocIdtoConDocLink.keySet()) {
                    System.debug('Key'+keyId+' Value'+mapConDocIdtoConDocLink.get(keyId));
                }
                System.debug('Debug Log for mapConDocLnktoDocChkLst');
                for(Id keyId : mapConDocLnktoDocChkLst.keySet()) {
                    System.debug('Key'+keyId+' Value'+mapConDocLnktoDocChkLst.get(keyId));
                }
                
                lstContentVersion = [SELECT Id, Title, VersionData, CreatedDate, ContentDocumentId, FileExtension FROM ContentVersion WHERE IsLatest=true AND ContentDocumentId =: objDML.Document_Id__c];
                System.debug('Debug Log for lstContentVersion'+lstContentVersion.size());
                List<ContentVersion> lstContentDocsCoApplicants = new List<ContentVersion>();
                if(!lstContentVersion.isEmpty()) {
                    for(ContentVersion objContentVersion : lstContentVersion) {
                        if(mapConDocLnktoDocChkLst.containsKey(objContentVersion.ContentDocumentId) && String.isNotBlank(mapConDocLnktoDocChkLst.get(objContentVersion.ContentDocumentId))) {
                            
                            if(!setCoApplicantGuarantorDocs.isEmpty() && setCoApplicantGuarantorDocs.contains(mapConDocLnktoDocChkLst.get(objContentVersion.ContentDocumentId))) {
                                //Send Document for Addition in Co-Applicant/Gurantor Related Document Folder
                                lstContentDocsCoApplicants.add(objContentVersion);
                            }
                            
                        }
                    }
                    
                    System.debug('Debug Log for lstContentDocsCoApplicants'+lstContentDocsCoApplicants.size());
                    
                    
                    
                    if(!lstContentDocsCoApplicants.isEmpty()) {
                        for(ContentVersion objCoAppGuarantorDoc : lstContentDocsCoApplicants) {
                            listToDelete.add(new ContentDocument(Id = objCoAppGuarantorDoc.ContentDocumentId));
                        }
                        System.debug('Debug Log for listToDelete'+listToDelete.size());
                        if(!listToDelete.isEmpty()){
                            delete listToDelete;
                        }
                    }
                }
            }
        }
        catch(Exception ex) {
            System.debug('Debug Log for Exception'+ex);
        }
    }
    
    public static void deleteS3FilesApplication(Document_Migration_Log__c objDML) {
        List<ContentDocument> listToDelete = new List<ContentDocument>();
        List<ContentDocument> listToDeleteFinal = new List<ContentDocument>();
        Set<Id> setApplicationRelatedDocs = new Set<Id>();
        Set<Id> setAllDocuments = new Set<Id>();
        List<ContentDocumentLink> lstContentDocLinks;
        List<ContentVersion> lstContentVersion;
        Set<Id> setContentDocumentLinkIds = new Set<Id>();
        Set<Id> setContentVersionId = new Set<Id>();
        Map<Id,Id> mapConDocLnktoDocChkLst = new Map<Id,Id>();
        Map<Id,Id> mapConDocIdtoConDocLink = new Map<Id,Id>();
        Map<Id, Document_Checklist__c> mapDocIdtoDocRecord = new Map<Id, Document_Checklist__c>();
        List<Document_Checklist__c> lstDocChkLst = [SELECT Id, Loan_Applications__c, Loan_Contact__c, Document_Master__c, Document_Type__c, Customer_Type__c, Loan_Contact__r.Applicant_Type__c From Document_Checklist__c Where ID =: objDML.Document_Checklist__c];
        try {
            System.debug('Debug Log for lstDocChkLst'+lstDocChkLst.size());
            if(!lstDocChkLst.isEmpty()) {
                for(Document_Checklist__c objDC : lstDocChkLst) {
                    if(String.isNotBlank(objDC.Loan_Applications__c) && String.isBlank(objDC.Loan_Contact__c)) {
                        setApplicationRelatedDocs.add(objDC.Id);
                        setAllDocuments.add(objDC.Id);
                        mapDocIdtoDocRecord.put(objDC.Id, objDC);
                    }
                }
            }
            System.debug('Debug Log for setApplicationRelatedDocs'+setApplicationRelatedDocs.size());
            System.debug('Debug Log for setAllDocuments'+setAllDocuments.size());   

            List<S3_Folders_Configuration__mdt> lstS3ConfigData = [SELECT Id, Delete_files__c, Folder_Name__c, MasterLabel, DeveloperName, NamespacePrefix FROM S3_Folders_Configuration__mdt];
            Map<String, S3_Folders_Configuration__mdt> mapDeveloperNametoRecord = new Map<String, S3_Folders_Configuration__mdt>();
            System.debug('Debug Log for lstS3ConfigData'+lstS3ConfigData.size());
            if(!lstS3ConfigData.isEmpty()) {
                for(S3_Folders_Configuration__mdt objS3ConfigData : lstS3ConfigData) {
                    mapDeveloperNametoRecord.put(objS3ConfigData.DeveloperName, objS3ConfigData);
                }
            }
            System.debug('Debug Log for mapDeveloperNametoRecord'+mapDeveloperNametoRecord.size());
            
            if(!setAllDocuments.isEmpty()) {
                lstContentDocLinks = [select id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where LinkedEntityId IN: setAllDocuments];
                System.debug('Debug Log for lstContentDocLinks'+lstContentDocLinks.size());
                
                for(ContentDocumentLink objCDL : lstContentDocLinks) { 
                    setContentDocumentLinkIds.add(objCDL.ContentDocumentId);
                    mapConDocIdtoConDocLink.put(objCDL.ContentDocumentId, objCDL.Id);
                    mapConDocLnktoDocChkLst.put(objCDL.ContentDocumentId, objCDL.LinkedEntityId);
                }
                System.debug('Debug Log for mapConDocIdtoConDocLink'+mapConDocIdtoConDocLink.size());
                System.debug('Debug Log for mapConDocIdtoConDocLink');
                for(Id keyId : mapConDocIdtoConDocLink.keySet()) {
                    System.debug('Key'+keyId+' Value'+mapConDocIdtoConDocLink.get(keyId));
                }
                System.debug('Debug Log for mapConDocLnktoDocChkLst');
                for(Id keyId : mapConDocLnktoDocChkLst.keySet()) {
                    System.debug('Key'+keyId+' Value'+mapConDocLnktoDocChkLst.get(keyId));
                }
                
                lstContentVersion = [SELECT Id, Title, VersionData, CreatedDate, ContentDocumentId, FileExtension FROM ContentVersion WHERE IsLatest=true AND ContentDocumentId =: objDML.Document_Id__c];
                System.debug('Debug Log for lstContentVersion'+lstContentVersion.size());
                List<ContentVersion> lstContentDocsApplicant = new List<ContentVersion>();
                List<ContentVersion> lstContentDocsCoApplicants = new List<ContentVersion>();
                List<ContentVersion> lstContentDocsApplication = new List<ContentVersion>();
                if(!lstContentVersion.isEmpty()) {
                    for(ContentVersion objContentVersion : lstContentVersion) {
                        if(mapConDocLnktoDocChkLst.containsKey(objContentVersion.ContentDocumentId) && String.isNotBlank(mapConDocLnktoDocChkLst.get(objContentVersion.ContentDocumentId))) {
                            if(!setApplicationRelatedDocs.isEmpty() && setApplicationRelatedDocs.contains(mapConDocLnktoDocChkLst.get(objContentVersion.ContentDocumentId))) {
                                //Send Document for Addition in Application Related Document Folder
                                lstContentDocsApplication.add(objContentVersion);
                            }
                        }
                    }
                    System.debug('Debug Log for lstContentDocsApplication'+lstContentDocsApplication.size());
                    
                    
                    if(!lstContentDocsApplication.isEmpty()) {  
                            for(ContentVersion objApplicationDoc : lstContentDocsApplication) {
                                listToDelete.add(new ContentDocument(Id = objApplicationDoc.ContentDocumentId));
                            }
                            System.debug('Debug Log for listToDelete'+listToDelete);
                            System.debug('Debug Log for listToDelete'+listToDelete.size());
                            if(!listToDelete.isEmpty()) {
                              delete listToDelete;
                            }
                  
                    }
                }
            }
        }
        catch(Exception ex) {
            System.debug('Debug Log for Exception'+ex);
        }
    }
    
    
    public static void deleteS3FilesLoanKit(Document_Migration_Log__c objDML) {
        List<ContentDocument> listToDelete = new List<ContentDocument>();
        List<ContentDocument> listToDeleteFinal = new List<ContentDocument>();
        Set<Id> setApplicationRelatedDocs = new Set<Id>();
        Set<Id> setAllDocuments = new Set<Id>();
        List<ContentDocumentLink> lstContentDocLinks;
        List<ContentVersion> lstContentVersion;
        Set<Id> setContentDocumentLinkIds = new Set<Id>();
        Set<Id> setContentVersionId = new Set<Id>();
        Map<Id,Id> mapConDocLnktoDocChkLst = new Map<Id,Id>();
        Map<Id,Id> mapConDocIdtoConDocLink = new Map<Id,Id>();
        Map<Id, Document_Checklist__c> mapDocIdtoDocRecord = new Map<Id, Document_Checklist__c>();
        try {
            List<S3_Folders_Configuration__mdt> lstS3ConfigData = [SELECT Id, Delete_files__c, Folder_Name__c, MasterLabel, DeveloperName, NamespacePrefix FROM S3_Folders_Configuration__mdt];
            Map<String, S3_Folders_Configuration__mdt> mapDeveloperNametoRecord = new Map<String, S3_Folders_Configuration__mdt>();
            System.debug('Debug Log for lstS3ConfigData'+lstS3ConfigData.size());
            if(!lstS3ConfigData.isEmpty()) {
                for(S3_Folders_Configuration__mdt objS3ConfigData : lstS3ConfigData) {
                    mapDeveloperNametoRecord.put(objS3ConfigData.DeveloperName, objS3ConfigData);
                }
            }
            System.debug('Debug Log for mapDeveloperNametoRecord'+mapDeveloperNametoRecord.size());
            
            if(String.isNotBlank(objDML.Loan_Application__r.Id)) {
                lstContentDocLinks = [select id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where LinkedEntityId =: objDML.Loan_Application__r.Id];
                System.debug('Debug Log for lstContentDocLinks'+lstContentDocLinks.size());
                
                for(ContentDocumentLink objCDL : lstContentDocLinks) { 
                    setContentDocumentLinkIds.add(objCDL.ContentDocumentId);
                    mapConDocIdtoConDocLink.put(objCDL.ContentDocumentId, objCDL.Id);
                    mapConDocLnktoDocChkLst.put(objCDL.ContentDocumentId, objCDL.LinkedEntityId);
                }
                System.debug('Debug Log for mapConDocIdtoConDocLink'+mapConDocIdtoConDocLink.size());
                System.debug('Debug Log for mapConDocIdtoConDocLink');
                for(Id keyId : mapConDocIdtoConDocLink.keySet()) {
                    System.debug('Key'+keyId+' Value'+mapConDocIdtoConDocLink.get(keyId));
                }
                System.debug('Debug Log for mapConDocLnktoDocChkLst');
                for(Id keyId : mapConDocLnktoDocChkLst.keySet()) {
                    System.debug('Key'+keyId+' Value'+mapConDocLnktoDocChkLst.get(keyId));
                }
                
                lstContentVersion = [SELECT Id, Title, VersionData, CreatedDate, ContentDocumentId, FileExtension FROM ContentVersion WHERE IsLatest=true AND ContentDocumentId =: objDML.Document_Id__c];
                System.debug('Debug Log for lstContentVersion'+lstContentVersion.size());
                List<ContentVersion> lstContentDocsApplicant = new List<ContentVersion>();
                List<ContentVersion> lstContentDocsCoApplicants = new List<ContentVersion>();
                List<ContentVersion> lstContentDocsApplication = new List<ContentVersion>();
                if(!lstContentVersion.isEmpty()) {
                    for(ContentVersion objContentVersion : lstContentVersion) {
                        if(mapConDocLnktoDocChkLst.containsKey(objContentVersion.ContentDocumentId) && String.isNotBlank(mapConDocLnktoDocChkLst.get(objContentVersion.ContentDocumentId))) {
                            lstContentDocsApplication.add(objContentVersion);
                        }
                    }
                    System.debug('Debug Log for lstContentDocsApplication'+lstContentDocsApplication.size());
                    
                    
                    if(!lstContentDocsApplication.isEmpty()) {
                        for(ContentVersion objApplicationDoc : lstContentDocsApplication) {
                            listToDelete.add(new ContentDocument(Id = objApplicationDoc.ContentDocumentId));
                        }
                        System.debug('Debug Log for listToDelete'+listToDelete.size());
                        if(!listToDelete.isEmpty()) {
                            delete listToDelete;
                        }
                            
                    }
                }
            }
        }
        catch(Exception ex) {
            System.debug('Debug Log for exception'+ex);
        }
    }
    
    public static void deleteS3FilesLoanKit2(Document_Migration_Log__c objDML) { 
        List<Attachment> lstAttachment;
        List<Attachment> lstAttachmentFinal;
        try {
            if(String.isNotBlank(objDML.Loan_Application__c)) {
                for(Attachment attch : [SELECT Id, Name, CreatedDate, Description, ParentId FROM Attachment Where ParentId =: objDML.Loan_Application__c  AND Id =: objDML.Document_Id__c]) {
                    delete attch;
                }
                /*lstAttachment = [SELECT Id, Name, CreatedDate, Description, ParentId FROM Attachment Where ParentId =: objDML.Loan_Application__c];
                System.debug('Debug Log for lstAttachment'+lstAttachment);
                System.debug('Debug Log for lstAttachment'+lstAttachment.size());
                List<S3_Folders_Configuration__mdt> lstS3ConfigData = [SELECT Id, Delete_files__c, Folder_Name__c, MasterLabel, DeveloperName, NamespacePrefix FROM S3_Folders_Configuration__mdt];
                Map<String, S3_Folders_Configuration__mdt> mapDeveloperNametoRecord = new Map<String, S3_Folders_Configuration__mdt>();
                System.debug('Debug Log for lstS3ConfigData'+lstS3ConfigData.size());
                if(!lstS3ConfigData.isEmpty()) {
                    for(S3_Folders_Configuration__mdt objS3ConfigData : lstS3ConfigData) {
                        mapDeveloperNametoRecord.put(objS3ConfigData.DeveloperName, objS3ConfigData);
                    }
                }
            System.debug('Debug Log for mapDeveloperNametoRecord'+mapDeveloperNametoRecord.size());
                
          
              if(!lstAttachment.isEmpty()) {
                delete lstAttachment;
              }*/
           
                
            }
        }
        catch(Exception ex) {
            System.debug('Debug Log for exception'+ex);
        }
    }
}