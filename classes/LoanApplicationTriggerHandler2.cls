public class LoanApplicationTriggerHandler2 { 
    public static void beforeupdate(list < loan_application__c > newList, list < loan_application__c > oldList, map < id, loan_application__c > newMap, map < id, loan_application__c > oldMap) 
    {
        /***************************************************************************************************************************************************************************
         * To Imitate the Lookup Filter of Project Inventory for Loan Applications marked as APF  
         * Mark Inventory as Avaiable on moving to previous stage from Customer Negotiation
         * Prohibit unflagging of the APF Flag, when the Application is at Customer Negotiation.
         **************************************************************************************************************************************************************************/
        List<Loan_Application__c> lstCriteriaLoanApplications = new List<Loan_Application__c>(); // Loan Applications that have been recently marked as APF Loan Applications.
        List<Loan_Application__c> lstLoanApplicationsMoveBackfromCN = new List<Loan_Application__c>(); // Loan Applications that have been moved back from Customer Negotiation.
        List<Loan_Application__c> lstLoanApplicationsforMinOCRRecalculation = new List<Loan_Application__c>();
        Set<Id> setAPFLAsupdatedOCRAmount = new Set<Id>();
        
        
        Map<Id,Project_Inventory__c> mapInventoryIdtoInventory;
        Set<Id> setInventoryIds = new Set<Id>();
        Set<Id> setInventoryIdsforAvailableUpdate = new Set<Id>();
        List<Loan_Application__c> eligibilityList = new List<Loan_Application__c>(); // Added by KK for BREII
       // List<Loan_Application__c> setofLoanAppIdsToUpdateBRE2Comments = new List<Loan_Application__c>();  // Added by Vaishali for BREII
        Set<Id> setOfLaFromCreditToNegotiation = new Set<Id>(); //Added by Vaishali for BREII
        Set<Id> setOfLAIdsForApprovedROIChange = new Set<Id>(); //Added by Vaishali for BREII
        Set<Id> setOfLAIdsForApprovedROIChange1 = new Set<Id>(); //Added by Vaishali for BREII
		String currentProfile;
		if(!test.isrunningTest()){
        currentProfile = [SELECT Name from Profile WHERE Id =: userinfo.getProfileId()].Name; //Added by Ankit for 101 issue - datafix
		}
        //Profile currentProfile = [SELECT Name from Profile WHERE Id =: userinfo.getProfileId()]; //Added by Vaishali for Code Optimization Activity
        //User currentUser = [Select id, RoleOfUser__c from User Where Id= : userinfo.getUserId()]; //Added by Vaishali for BREII-II
        List<Loan_Application__c> approvedList = new List<Loan_Application__c>(); // Added by KK for IMGC
        List<Loan_Application__c> rejectedList = new List<Loan_Application__c>(); // Added by KK for IMGC
        List<Loan_Application__c> notInitiatedList = new List<Loan_Application__c>(); // Added by KK for IMGC
        List<Loan_Application__c> notAvailedList = new List<Loan_Application__c>(); // Added by KK for IMGC
        List<Loan_Application__c> pendingList = new List<Loan_Application__c>(); // Added by KK for IMGC
        List<Loan_Application__c> initiatedList = new List<Loan_Application__c>(); //Added by Saumya on 24th August 2020 FOR IMGC BRD
        List<Loan_Application__c> checkIMGCVerificationlst = new List<Loan_Application__c>(); //Added by Saumya FOR IMGC BRD
        List<Loan_Application__c> lstIMGCPremiumFees = new List<Loan_Application__c>(); //Added by Saumya on 1st September 2020 for IMGC BRD
        List<Loan_Application__c> lstInsuranceLAForApprCrossSell = new List<Loan_Application__c>(); //Added by Saumya on 1st September 2020 for IMGC BRD
        Map<Id,Loan_Application__c> mapDocAppToDocketCheck = new Map<Id,Loan_Application__c>(); //Added by Abhilekh on 29th September 2020 FOR IMGC BRD
        List<Loan_Application__c> lstCheckUCICDatabase = new List<Loan_Application__c>(); //Added by Abhilekh on 25th September 2020 for UCIC Phase II
        UCIC_Global__c ucg = UCIC_Global__c.getInstance();/** Added by Saumya for UCIC II Enhacements **/
        for(Loan_Application__c objLoanApplication : newList) {
            /** Added by Saumya for UCIC II Enhacements **/
            if(runOnce5() && (oldMap.get(objLoanApplication.Id).StageName__c == Constants.Credit_Decisioning || oldMap.get(objLoanApplication.Id).StageName__c == Constants.Customer_Acceptance || oldMap.get(objLoanApplication.Id).StageName__c == Constants.Loan_Disbursal || oldMap.get(objLoanApplication.Id).StageName__c == Constants.Loan_Rejected || oldMap.get(objLoanApplication.Id).StageName__c == Constants.Loan_Cancelled) && (objLoanApplication.StageName__c == Constants.Operation_Control || objLoanApplication.StageName__c == Constants.Customer_Onboarding) && objLoanApplication.Retrigger_UCIC__c == false && ucg.UCIC_Active__c == true){
            	objLoanApplication.Retrigger_UCIC__c = true;
            }
	    if(objLoanApplication.Sub_Stage__c == Constants.COPS_Data_Checker && oldMap.get(objLoanApplication.Id).Sub_Stage__c == Constants.COPS_Credit_Operations_Entry && objLoanApplication.Retrigger_UCIC__c == false && objLoanApplication.Assigned_Cops_dataChecker__c != null && ucg.UCIC_Active__c == true){
                objLoanApplication.Retrigger_UCIC__c = true;
            }
            /** Added by Saumya for UCIC II Enhacements **/
            /*** Added by Saumya For IMGC BRD ***/
            /*if(objLoanApplication.IMGC_Decision__c != oldMap.get(objLoanApplication.Id).IMGC_Decision__c  && objLoanApplication.IMGC_Decision__c == 'Approved'){
                
                if(objLoanApplication.IMGC_Premium_Fees__c != 0){
                objLoanApplication.IMGC_Premium_Fees__c = 0;
                }
                if(objLoanApplication.IMGC_Premium_Fee__c != 0){
                objLoanApplication.IMGC_Premium_Fee__c = 0;
                }
            }*/
            /*if(objLoanApplication.IMGC_Premium_Fees__c != oldMap.get(objLoanApplication.Id).IMGC_Premium_Fees__c  && (objLoanApplication.IMGC_Premium_Fees__c == 0 || objLoanApplication.IMGC_Premium_Fees__c == null)){
                if(objLoanApplication.IMGC_Premium_Fees__c != 0){
                objLoanApplication.IMGC_Premium_Fees__c = 0;
                }
                if(objLoanApplication.IMGC_Premium_Fee__c != 0){
                objLoanApplication.IMGC_Premium_Fee__c = 0;
                }
            }*/
            /*** Added by Saumya For IMGC BRD ***/
            //Added by Vaishali for Code Optimization Activity
            // Throw Validation Errors on stage change
            if(oldMap.get(objLoanApplication.Id).Sub_Stage__c != objLoanApplication.Sub_Stage__c) {
                LoanApplicationTriggerHelper2.throwValidationsOnSubStageChange(objLoanApplication.Sub_Stage__c, oldMap.get(objLoanApplication.Id).Sub_Stage__c, objLoanApplication);    
            }
            //Added by Vaishali for BREII-II
            if(oldMap.get(objLoanApplication.Id).Approved_ROI__c != objLoanApplication.Approved_ROI__c && currentUser().RoleOfUser__c) {
                objLoanApplication.BH_Approved_ROI__c = objLoanApplication.Approved_ROI__c;     
            }
            //End of patch- Added by Vaishali for BREII-II
            // Throw Validation Errors - Generic
            if(objLoanApplication.Sub_Stage__c != Constants.Credit_Review && objLoanApplication.Sub_Stage__c != Constants.Re_Credit && objLoanApplication.Sub_Stage__c != Constants.Re_Look && currentProfile != 'System Administrator') { //Added by Ankit for 101 issue - datafix
                if(oldMap.get(objLoanApplication.Id).Appraised_Income__c != objLoanApplication.Appraised_Income__c) {
                    objLoanApplication.addError(Label.AppraisedIncomeEditableAtCreditDecision);
                }
                if(oldMap.get(objLoanApplication.Id).Appraised_Obligation__c != objLoanApplication.Appraised_Obligation__c) {
                    objLoanApplication.addError(Label.AppraisedObligEditableAtCreditDecision);
                }
            }
            if(
                ((oldMap.get(objLoanApplication.Id).Requested_Amount__c != objLoanApplication.Requested_Amount__c) || (oldMap.get(objLoanApplication.Id).Approved_Loan_Amount__c != objLoanApplication.Approved_Loan_Amount__c)) 
                && objLoanApplication.Approved_Loan_Amount__c != null && objLoanApplication.Requested_Amount__c != null 
                && objLoanApplication.Approved_Loan_Amount__c > objLoanApplication.Requested_Amount__c
                && (objLoanApplication.StageName__c == Constants.Customer_Acceptance || objLoanApplication.StageName__c == Constants.Credit_Decisioning)
                && currentProfile == 'Credit Team' //Added by Ankit for 101 issue - datafix
             ){
                objLoanApplication.addError(Label.Approved_Loan_Amount_cannot_be_greater);
             }
             if(oldMap.get(objLoanApplication.Id).Approved_cross_sell_amount__c != objLoanApplication.Approved_cross_sell_amount__c && currentProfile == 'Credit Team' && objLoanApplication.Sub_Stage__c == Constants.Sales_Approval) { //Added by Ankit for 101 issue - datafix
                objLoanApplication.addError(Label.CrossSell_not_Editable_at_Sales_Approval);
            }
            if(objLoanApplication.Disbursed_Amount__c > objLoanApplication.Approved_Loan_Amount__c && objLoanApplication.Sub_Stage__c == Constants.Disbursement_Maker) {
                objLoanApplication.addError(Label.Disbursed_Amt_not_greater_than_ALAmt);
            }
            if(objLoanApplication.Sub_Stage__c == Constants.COPS_Data_Checker && objLoanApplication.Pending_Doc__c > 0 && currentProfile != 'System Administrator') { //Added by Ankit for 101 issue - datafix
                objLoanApplication.addError(Label.Document_at_financial_data_entryr);
            }
            if(objLoanApplication.Insurance_Loan_Application__c && oldMap.get(objLoanApplication.Id).Calculated_FOIR__c != objLoanApplication.Calculated_FOIR__c) {
                objLoanApplication.addError(Label.FOIR_NonEditable_for_Insurance_Loan);
            }
            if(objLoanApplication.All_Loan_Contact_Count__c != objLoanApplication.Loan_Contact_Count__c && objLoanApplication.Sub_Stage__c == Constants.File_Check) {
                objLoanApplication.addError(Label.Mandatory_Fields_on_Loan_Contacts);
            } 
            if(objLoanApplication.Requested_Loan_Tenure__c == null && objLoanApplication.Sub_Stage__c == Constants.COPS_Credit_Operations_Entry) {
                objLoanApplication.addError(Label.Requested_Loan_Tenure_required);
            } 
            if(objLoanApplication.Count_of_Sourcing_Records__c == 0 && objLoanApplication.StageName__c == Constants.Operation_Control ) {
                objLoanApplication.addError(Label.Sourcing_Detail_Check);
            } 
            if((objLoanApplication.Sub_Stage__c == Constants.CPC_Data_Maker  || objLoanApplication.Sub_Stage__c == Constants.OTC_Receiving) && objLoanApplication.Disbursement_Details_verified__c == false) {
                objLoanApplication.addError(Label.verifying_data_loan_disbursal);
            }
            //Added by Vaishali for TIL-00002270
            if(objLoanApplication.CRE__c != oldMap.get(objLoanApplication.Id).CRE__c && objLoanApplication.CRE__c  == null && objLoanApplication.Sub_Stage__c == Constants.L1_Credit_Approval && objLoanApplication.Product_Name__c == 'Home Loan') {
                objLoanApplication.addError('CRE cannot be blank');
            }
            //End of the patch- Added by Vaishali for TIL-00002270
            //End of the patch- Added by Vaishali for Code Optimization Activity
            if(objLoanApplication.APF_Loan__c == true && objLoanApplication.Prorata_Disbursal__c == true && objLoanApplication.OCR_Amount_Paid__c != oldMap.get(objLoanApplication.Id).OCR_Amount_Paid__c && objLoanApplication.OCR_Amount_Paid__c != null) {
                setAPFLAsupdatedOCRAmount.add(objLoanApplication.Id);
            }
            // Added by Vaishali for BREII
            system.debug('objLoanApplication.Sub_Stage__c '+objLoanApplication.Sub_Stage__c);
            system.debug('objLoanApplication.StageName__c '+objLoanApplication.StageName__c);
            
            if(((objLoanApplication.Approved_ROI__c != null && (oldMap.get(objLoanApplication.Id).Approved_ROI__c != objLoanApplication.Approved_ROI__c)) )) {
                setOfLAIdsForApprovedROIChange.add(objLoanApplication.Id);
            }
            if( (oldMap.get(objLoanApplication.Id).Sub_Stage__c == Constants.Customer_Negotiation && (objLoanApplication.Sub_Stage__c == Constants.Document_approval || (objLoanApplication.BRE2_Retrigger_at_Negotiation__c == true && oldMap.get(objLoanApplication.Id).BRE2_Retrigger_at_Negotiation__c == false && objLoanApplication.sub_stage__c == constants.Customer_Negotiation)))) {
                setOfLAIdsForApprovedROIChange1.add(objLoanApplication.Id);
            }
            if(oldMap.get(objLoanApplication.Id).Sub_Stage__c != objLoanApplication.Sub_Stage__c && objLoanApplication.Sub_Stage__c == Constants.Customer_Negotiation && oldMap.get(objLoanApplication.Id).StageName__c == Constants.Credit_Decisioning) {
                setOfLaFromCreditToNegotiation.add(objLoanApplication.Id);
            }
            /*if(objLoanApplication.BRE_2_Final_Decision__c == 'Red' && (objLoanApplication.BRE_Approved__c != false || objLoanApplication.BRE_Approval_Comments__c != '')) {
                setofLoanAppIdsToUpdateBRE2Comments.add(objLoanApplication);
            }*/
            // End of patch- Added by Vaishali for BREII
           
           
           
            //To execute only and only when the APF Flag has recently been marked, to imitate the initial lookup filter on Project Inventory Field.
            //if(checkRecursive.runOnce()) {
                if(objLoanApplication.APF_Loan__c != oldMap.get(objLoanApplication.Id).APF_Loan__c  && objLoanApplication.APF_Loan__c == true && String.isNotBlank(objLoanApplication.Project_Inventory__c) && String.isNotBlank(objLoanApplication.APF_Name__c)) {
                    /*******************************************************************************************************************************************************
                     The following conditions must be fulfilled while specifying the Inventory for the first time:
                     * Project Inventory: Inventory Approval Status must be Approved
                     * Project Inventory: Inventory Status must be Available
                     * Inventory must be belonging the the APF Project specified at the Loan Application.
                     * *****************************************************************************************************************************************************/
                     setInventoryIds.add(objLoanApplication.Project_Inventory__c);
                     lstCriteriaLoanApplications.add(objLoanApplication);
                }
                
                else if(objLoanApplication.Project_Inventory__c != oldMap.get(objLoanApplication.Id).Project_Inventory__c && objLoanApplication.Project_Inventory__c != null) {
                    setInventoryIds.add(objLoanApplication.Project_Inventory__c);
                    lstCriteriaLoanApplications.add(objLoanApplication);
                }
            //}
            if(objLoanApplication.APF_Loan__c == true && runOnce10() && objLoanApplication.Sub_Stage__c != oldMap.get(objLoanApplication.Id).Sub_Stage__c && oldMap.get(objLoanApplication.Id).Sub_Stage__c == Constants.Customer_Negotiation && objLoanApplication.Sub_Stage__c != Constants.Sales_Approval) {
                setInventoryIdsforAvailableUpdate.add(objLoanApplication.Project_Inventory__c);
            }
            if(objLoanApplication.APF_Loan__c != oldMap.get(objLoanApplication.Id).APF_Loan__c && oldMap.get(objLoanApplication.Id).APF_Loan__c == true && objLoanApplication.APF_Loan__c == false && objLoanApplication.Sub_Stage__c == Constants.Customer_Negotiation) {
                objLoanApplication.addError('APF Flag cannot be removed at this stage.');
            }
            if(objLoanApplication.APF_Loan__c == true && (objLoanApplication.Earnest_Money__c != oldMap.get(objLoanApplication.Id).Earnest_Money__c || objLoanApplication.Cost_of_Property__c != oldMap.get(objLoanApplication.Id).Cost_of_Property__c || (objLoanApplication.Sub_Stage__c != oldMap.get(objLoanApplication.Id).Sub_Stage__c && objLoanApplication.Sub_Stage__c == 'Disbursement Maker'))) {
                lstLoanApplicationsforMinOCRRecalculation.add(objLoanApplication);
            }
            // Added by KK for BREII : Code Begins
            if(objLoanApplication.Copy_Eligibility__c == true && oldMap.get(objLoanApplication.Id).Copy_Eligibility__c == false){
                eligibilityList.add(objLoanApplication);
            }
            // Code Ends
            // Added by KK for IMGC: Code Begins
            if(objLoanApplication.IMGC_Decision__c == 'Approved' && oldMap.get(objLoanApplication.Id).IMGC_Decision__c != 'Approved' && currentProfile() != 'System Administrator'){
                approvedList.add(objLoanApplication);
            }
            if(objLoanApplication.IMGC_Decision__c == 'Rejected' && oldMap.get(objLoanApplication.Id).IMGC_Decision__c != 'Rejected' && currentProfile() != 'System Administrator'){
                rejectedList.add(objLoanApplication);
            }
            if(objLoanApplication.IMGC_Decision__c == 'Not Initiated' && oldMap.get(objLoanApplication.Id).IMGC_Decision__c != 'Not Initiated' && currentProfile() != 'System Administrator'){
                notInitiatedList.add(objLoanApplication);
            }
            if(objLoanApplication.IMGC_Decision__c == 'Not Availed by Customer' && oldMap.get(objLoanApplication.Id).IMGC_Decision__c != 'Not Availed by Customer' && currentProfile() != 'System Administrator'){
                notAvailedList.add(objLoanApplication);
            }
            if(objLoanApplication.IMGC_Decision__c == 'Decision Pending' && oldMap.get(objLoanApplication.Id).IMGC_Decision__c != 'Decision Pending' && currentProfile() != 'System Administrator'){
                pendingList.add(objLoanApplication);
            }
            if(objLoanApplication.IMGC_Premium_Fees__c != oldMap.get(objLoanApplication.Id).IMGC_Premium_Fees__c && objLoanApplication.IMGC_Premium_Fees__c != null && objLoanApplication.IMGC_Decision__c != 'Approved' && currentProfile() != 'System Administrator'){
                objLoanApplication.addError('IMGC Premium Fees % is only applicable for IMGC approved cases.');
            }
            
            system.debug('objLoanApplication.IMGC_Premium_Fees__c::'+ objLoanApplication.IMGC_Premium_Fees__c);
            if(objLoanApplication.IMGC_Decision__c != oldMap.get(objLoanApplication.Id).IMGC_Decision__c && objLoanApplication.IMGC_Decision__c == 'Approved' && (objLoanApplication.IMGC_Premium_Fees__c == null || objLoanApplication.IMGC_Premium_Fees__c == 0)){
                objLoanApplication.addError('IMGC Premium Fees % is mandatory before approving IMGC.');
            }
            
            // Code Ends
            //Added by Saumya on 24th August 2020 FOR IMGC BRD
            if(objLoanApplication.IMGC_Decision__c != 'Not Initiated' && oldMap.get(objLoanApplication.Id).IMGC_Decision__c == 'Not Initiated' && currentProfile() != 'System Administrator'){
                initiatedList.add(objLoanApplication);
            }
            
            //Added by Abhilekh on 25th September 2020 for UCIC Phase II
            if(objLoanApplication.StageName__c == Constants.Customer_Acceptance && oldMap.get(objLoanApplication.Id).StageName__c == Constants.Credit_Decisioning){
                lstCheckUCICDatabase.add(objLoanApplication);
            }
            
            //Added by Saumya on 1st September 2020 FOR IMGC BRD
            if((objLoanApplication.IMGC_Premium_Fees__c != oldMap.get(objLoanApplication.Id).IMGC_Premium_Fees__c || objLoanApplication.Approved_Loan_Amount_Exclusive__c != oldMap.get(objLoanApplication.Id).Approved_Loan_Amount_Exclusive__c) && objLoanApplication.Insurance_Loan_Application__c == false){
                if(objLoanApplication.IMGC_Premium_Fees__c == null || objLoanApplication.Approved_Loan_Amount_Exclusive__c == null){
                    objLoanApplication.IMGC_Premium_Fee__c = 0;
                }
                else{
                    objLoanApplication.IMGC_Premium_Fee__c = ((objLoanApplication.IMGC_Premium_Fees__c * objLoanApplication.Approved_Loan_Amount_Exclusive__c)/100).setScale(0);    
                }
            }
            //Added by Saumya on 1st September 2020 FOR IMGC BRD
            if(objLoanApplication.IMGC_Premium_Fee__c != oldMap.get(objLoanApplication.Id).IMGC_Premium_Fee__c){
                if(objLoanApplication.IMGC_Premium_Fee__c != null){
                    if(objLoanApplication.Insurance_Loan_Application__c == false){
                        objLoanApplication.IMGC_Premium_Fees_GST_Inclusive1__c = (objLoanApplication.IMGC_Premium_Fee__c + (objLoanApplication.IMGC_Premium_Fee__c * 0.18)).setScale(0);
                    }
                    else{
                        lstInsuranceLAForApprCrossSell.add(objLoanApplication);
                    }
                }
                else{
                    objLoanApplication.IMGC_Premium_Fees_GST_Inclusive1__c = 0;
                }
            }
            //Added by Saumya on 1st September 2020 FOR IMGC BRD
            if(objLoanApplication.IMGC_Premium_Fees__c != oldMap.get(objLoanApplication.Id).IMGC_Premium_Fees__c && objLoanApplication.Insurance_Loan_Application__c == true){
                System.debug('Inside relevant if');
                lstIMGCPremiumFees.add(objLoanApplication);
            }
            
            //Added by Saumya on 7th September 2020 FOR IMGC BRD
            if(objLoanApplication.IMGC_Decision__c == Constants.NotAvailedByCustomer && oldMap.get(objLoanApplication.Id).IMGC_Decision__c == Constants.STATUS_APPROVAL){
                if(objLoanApplication.IMGC_Premium_Fees__c != 0 && objLoanApplication.IMGC_Premium_Fees__c != null){
                    objLoanApplication.IMGC_Premium_Fees__c = 0;
                    objLoanApplication.IMGC_Premium_Fee__c = 0;
                    objLoanApplication.IMGC_Premium_Fees_GST_Inclusive1__c = 0;
                }
            }
            
            //Added by Abhilekh on 29th September 2020 FOR IMGC BRD
            if(objLoanApplication.Sub_Stage__c == Constants.Docket_Checker && oldMap.get(objLoanApplication.Id).Sub_Stage__c == Constants.Document_approval){
                if(objLoanApplication.Insurance_Loan_Created__c == true && objLoanApplication.IMGC_Decision__c == Constants.STATUS_APPROVAL){
                    mapDocAppToDocketCheck.put(objLoanApplication.Id,objLoanApplication);
                }
            }
            
            //Added by Abhilekh on 25th September 2020 for UCIC Phase II
            if(objLoanApplication.StageName__c == Constants.Customer_Acceptance && oldMap.get(objLoanApplication.Id).StageName__c == Constants.Credit_Decisioning){
                lstCheckUCICDatabase.add(objLoanApplication);
            }
        }
        
        
        //Added by Vaishali for BREII
        system.debug('setOfLaFromCreditToNegotiation '+setOfLaFromCreditToNegotiation);
        if(!setOfLAIdsForApprovedROIChange.isEmpty()) { 
            LoanApplicationTriggerHelper2.validateApprovedROI(setOfLAIdsForApprovedROIChange, newMap,false);
        }
        if(!setOfLAIdsForApprovedROIChange1.isEmpty()) {
            LoanApplicationTriggerHelper2.validateApprovedROI(setOfLAIdsForApprovedROIChange1, newMap,true);
        }
        if(!setOfLaFromCreditToNegotiation.isEmpty()) {
            LoanApplicationTriggerHelper2.validateDeviationApproval(setOfLaFromCreditToNegotiation, newMap);
        }
        /*if(!setofLoanAppIdsToUpdateBRE2Comments.isEmpty()) {
            LoanApplicationTriggerHelper2.resetApprovalComments(setofLoanAppIdsToUpdateBRE2Comments);
        }*/
        //End of patch - Added by Vaishali for BREII
        /*if(!setOfLAforSchemeChange.isEmpty()) {
            List<Scheme__c> lstOfSchemes = [SELECT Id, Product_Code__c, Loan_Applications1__r FROM Scheme__c WHERE Loan_Applications1__r IN: setOfLAforSchemeChange];
            
        }*/
        
        // Added by KK for BREII : Code Begins
        system.debug('Eligibility List:'+eligibilityList.size());
        if(eligibilityList.size()>0){
            LoanApplicationTriggerHelper2.BRE2EligibilityCheck(eligibilityList);
        }
        // Code Ends
        
        
        System.debug('Debug Log for setAPFLAsupdatedOCRAmount'+setAPFLAsupdatedOCRAmount);
        if(!setAPFLAsupdatedOCRAmount.isEmpty()) {
            List<Tranche__c> lstTranches = [SELECT Id, Name, Status__c, Loan_Application__c from Tranche__c Where Loan_Application__c IN: setAPFLAsupdatedOCRAmount AND Status__c !=:Constants.STATUS_DISBURSED LIMIT 50000];
            if(!lstTranches.isEmpty()) {
                update lstTranches;
            }
            
        }
        
        System.debug('Debug Log for lstLoanApplicationsforMinOCRRecalculation'+lstLoanApplicationsforMinOCRRecalculation.size());
        if(!lstLoanApplicationsforMinOCRRecalculation.isEmpty()) {
            LoanApplicationTriggerHelper.calculateMinimumOCR(lstLoanApplicationsforMinOCRRecalculation);
        }
        System.debug('Debug Log for setInventoryIds'+setInventoryIds.size());
        System.debug('Debug Log for setInventoryIdsforAvailableUpdate'+setInventoryIdsforAvailableUpdate.size());
        System.debug('Debug Log for lstCriteriaLoanApplications'+lstCriteriaLoanApplications.size());
        if(!setInventoryIds.isEmpty() && !lstCriteriaLoanApplications.isEmpty()) {
            mapInventoryIdtoInventory = new Map<Id,Project_Inventory__c>([Select Id, Name, Inventory_Approval_Status__c, Inventory_Status__c, Project__c From Project_Inventory__c Where Id IN: setInventoryIds]);
            System.debug('Debug Log for mapInventoryIdtoInventory'+mapInventoryIdtoInventory);
        
            if(!mapInventoryIdtoInventory.isEmpty()) {
                for(Loan_Application__c objLoanApplicationLoop2 : lstCriteriaLoanApplications) {
                    if(mapInventoryIdtoInventory.containsKey(objLoanApplicationLoop2.Project_Inventory__c)) {
                        System.debug('Inventory found');
                        if(mapInventoryIdtoInventory.get(objLoanApplicationLoop2.Project_Inventory__c).Project__c == objLoanApplicationLoop2.APF_Name__c) {
                            System.debug('Belonging to the same project');
                            System.debug('Inventory Approval Status'+mapInventoryIdtoInventory.get(objLoanApplicationLoop2.Project_Inventory__c).Inventory_Approval_Status__c);
                            System.debug('Inventory Status'+mapInventoryIdtoInventory.get(objLoanApplicationLoop2.Project_Inventory__c).Inventory_Status__c);
                            if(!(mapInventoryIdtoInventory.get(objLoanApplicationLoop2.Project_Inventory__c).Inventory_Approval_Status__c == 'Approved' && mapInventoryIdtoInventory.get(objLoanApplicationLoop2.Project_Inventory__c).Inventory_Status__c == 'Available')) {
                                 trigger.newMap.get(objLoanApplicationLoop2.Id).addError('Project Inventory specified must be Approved and Available. Please specify a suitable Inventory.');
                            }
                        }
                        else {
                            trigger.newMap.get(objLoanApplicationLoop2.Id).addError('Project Inventory specified must belong to the Project selected.');
                        }
                    }
                }
            }
                
            
        }
        
        if(!setInventoryIdsforAvailableUpdate.isEmpty()) {
            List<Project_Inventory__c> lstProjectInventorytoUpdateAvailable = [Select Id, Name, Inventory_Status__c From Project_Inventory__c Where Id IN: setInventoryIdsforAvailableUpdate];
            if(!lstProjectInventorytoUpdateAvailable.isEmpty()) {
                for(Project_Inventory__c objProjectInventory : lstProjectInventorytoUpdateAvailable) {
                    if(objProjectInventory.Inventory_Status__c != 'Available') {
                        objProjectInventory.Inventory_Status__c = 'Available';
                    }
                }
            }
            update lstProjectInventorytoUpdateAvailable;
        }
         // Added by KK for IMGC: Code Begins
        if(approvedList.size()>0){
            LoanApplicationTriggerHelper2.approvedCheck(approvedList);
        }
        if(rejectedList.size()>0){
            LoanApplicationTriggerHelper2.rejectedCheck(rejectedList);
        }
        if(notInitiatedList.size()>0){
            LoanApplicationTriggerHelper2.notInitiatedCheck(notInitiatedList);
        }
        if(notAvailedList.size()>0){
            LoanApplicationTriggerHelper2.notAvailedCheck(notAvailedList);
        }
        if(pendingList.size()>0){
            LoanApplicationTriggerHelper2.pendingCheck(pendingList);
        }
        
        /*if(checkIMGCVerificationlst.size()>0){
            LoanApplicationTriggerHelper2.checkIMGCVerification(checkIMGCVerificationlst);
        }*/
        // Code Ends
        //Added by Saumya on 24th August 2020 FOR IMGC BRD
        if(initiatedList.size() > 0){
            LoanApplicationTriggerHelper2.initiatedCheck(initiatedList);
        }
        
        //Added by Abhilekh on 25th September 2020 for UCIC Phase II
        if(lstCheckUCICDatabase.size() > 0){
            LoanApplicationTriggerHelper2.checkUCICDatabaseDecision(lstCheckUCICDatabase);
        }
        
        //Added by Saumya on 1st September 2020 for IMGC BRD
        if(lstIMGCPremiumFees.size() > 0){
            LoanApplicationTriggerHelper2.calculateIMGCPremiumFeesForInsuranceLA(lstIMGCPremiumFees);
        }
        
        //Added by Saumya on 1st September 2020 for IMGC BRD
        if(lstInsuranceLAForApprCrossSell.size() > 0){
            LoanApplicationTriggerHelper2.calculateIMGCPremiumFeesGSTIncForInsuranceLA(lstInsuranceLAForApprCrossSell);
        }
        
        //Added by Abhilekh on 29th September 2020 for IMGC BRD
        if(!mapDocAppToDocketCheck.isEmpty()){
            LoanApplicationTriggerHelper2.checkInsuranceLoanForIMGCMismatch(mapDocAppToDocketCheck);
        }
        
        //Added by Abhilekh on 25th September 2020 for UCIC Phase II
        if(lstCheckUCICDatabase.size() > 0){
            LoanApplicationTriggerHelper2.checkUCICDatabaseDecision(lstCheckUCICDatabase);
        }
    }
    
    private static boolean run10 = true;
    public static boolean runOnce10(){
      if(run10){
        run10=false;
        return true;
      }else{
        return run10;
      }
    }
    
    public static void afterupdate(list < loan_application__c > newList, list < loan_application__c > oldList, map < id, loan_application__c > newMap, map < id, loan_application__c > oldMap) 
    {
        Set<Id> setOfLAForCAMEntryUpdate = new Set<Id>(); //Added by Vaishali for BREIIEnchancement
        Set<Id> setOfLAForCAMEntryUpdateForCredit = new Set<Id>(); //Added by Vaishali for BREIIEnchancement
        Set<Id> setLAToCustNego = new Set<Id>(); //Added by Abhilekh on 28th September 2020 for UCIC Phase II
        Set<Id> setLAToCancel = new Set<Id>(); //Added by Abhilekh on 28th September 2020 for UCIC Phase II
        Set<Id> setLAToReject = new Set<Id>(); //Added by Abhilekh on 28th September 2020 for UCIC Phase II
        List<Loan_Application__c> lstLACreditAuthorityChange = new List<Loan_Application__c>(); //Added by Abhilekh on 9th October 2020 for UCIC Phase II
		List<Loan_Application__c> lstLAWithBankingSurrogateIncomeProgram = new List<Loan_Application__c>(); //Added by Abhilekh for BRE2 Enhancements
        List<Loan_Application__c> lstLAWithoutBankingSurrogateIncomeProgram = new List<Loan_Application__c>(); //Added by Abhilekh for BRE2 Enhancements
        
        for(Loan_Application__c la : newList) {
            //Added by Vaishali for BREIIEnchancement
            if(oldMap.get(la.Id).Sub_Stage__c != la.Sub_Stage__c &&  la.Sub_Stage__c == Constants.COPS_Credit_Operations_Entry && Utility.checkEligibilityForBRE2(la)) {
                setOfLAForCAMEntryUpdate.add(la.Id);
            }
            if(oldMap.get(la.Id).Sub_Stage__c != la.Sub_Stage__c && la.StageName__c == Constants.Credit_Decisioning && Utility.checkEligibilityForBRE2(la)) {
                setOfLAForCAMEntryUpdateForCredit.add(la.Id);
            }
            //End of patch- Added by Vaishali for BREIIEnchancement
            
            //Added by Abhilekh on 28th September 2020 for UCIC Phase II
            if(la.StageName__c == Constants.Customer_Acceptance && oldMap.get(la.Id).StageName__c == Constants.Credit_Decisioning){
                if(runOnce4()){
                	setLAToCustNego.add(la.Id);    
                }
            }
            
            //Added by Abhilekh on 28th September 2020 for UCIC Phase II
            if(la.Sub_Stage__c == Constants.Loan_Cancel && oldMap.get(la.Id).Sub_Stage__c != la.Sub_Stage__c){
                if(runOnce()){
                    setLAToCancel.add(la.Id);    
                }
            }
            
            //Added by Abhilekh on 28th September 2020 for UCIC Phase II
            if(la.Sub_Stage__c == Constants.Loan_reject && oldMap.get(la.Id).Sub_Stage__c != la.Sub_Stage__c){
                if(runOnce2()){
                    setLAToReject.add(la.Id);    
                }
            }
            
            //Added by Abhilekh on 9th October 2020 for UCIC Phase II
            if(runOnce3()){
                if((la.Sub_Stage__c == Constants.Credit_Review || la.Sub_Stage__c == Constants.Re_Credit || la.Sub_Stage__c == Constants.Re_Look) && la.Assigned_Credit_Review__c != oldMap.get(la.Id).Assigned_Credit_Review__c && la.Assigned_Credit_Review__c != la.OwnerId && oldMap.get(la.Id).Assigned_Credit_Review__c == la.OwnerId){
                    lstLACreditAuthorityChange.add(la);
                }
            }
			
			//Added by Abhilekh for BRE2 Enhancements
            if(la.Income_Program_Type__c == Constants.Banking_Surrogate && la.Income_Program_Type__c != oldMap.get(la.Id).Income_Program_Type__c){
               lstLAWithBankingSurrogateIncomeProgram.add(la); 
            }
            
            //Added by Abhilekh for BRE2 Enhancements
            if(la.Income_Program_Type__c != Constants.Banking_Surrogate && la.Income_Program_Type__c != oldMap.get(la.Id).Income_Program_Type__c && oldMap.get(la.Id).Income_Program_Type__c == Constants.Banking_Surrogate){
               lstLAWithoutBankingSurrogateIncomeProgram.add(la); 
            }
        }  
        
        //Added by Vaishali for BREIIEnchancement
        if(!setOfLAForCAMEntryUpdate.isEmpty()){
            LoanApplicationTriggerHelper2.updateCAMEntryStatus(setOfLAForCAMEntryUpdate, false);
        }
        if(!setOfLAForCAMEntryUpdateForCredit.isEmpty()) {
            LoanApplicationTriggerHelper2.updateCAMEntryStatus(setOfLAForCAMEntryUpdateForCredit, true);
        }
        //End of patch- Added by Vaishali for BREIIEnchancement
        
        //Added by Abhilekh on 28th September 2020 for UCIC Phase II
        if(setLAToCustNego.size() > 0){
            System.debug('Inside setLAToCustNego==>'+setLAToCustNego);
            LoanApplicationTriggerHelper2.makeUCICDecisionCallout(setLAToCustNego);
        }
        
        //Added by Abhilekh on 28th September 2020 for UCIC Phase II
        if(setLAToCancel.size() > 0){
            LoanApplicationTriggerHelper2.makeUCICDecisionCallout(setLAToCancel);
        }
        
        //Added by Abhilekh on 28th September 2020 for UCIC Phase II
        if(setLAToReject.size() > 0){
            LoanApplicationTriggerHelper2.makeUCICDecisionCallout(setLAToReject);
        }
        
        //Added by Abhilekh on 9th October 2020 for UCIC Phase II
        if(lstLACreditAuthorityChange.size() > 0){
            LoanApplicationTriggerHelper2.checkUDApproversDecision(lstLACreditAuthorityChange);
        }
		
		//Added by Abhilekh for BRE2 Enhancements
        if(lstLAWithBankingSurrogateIncomeProgram.size() > 0){
            LoanApplicationTriggerHelper2.createDeviationForODCCAccountType(lstLAWithBankingSurrogateIncomeProgram);
        }
        
        //Added by Abhilekh for BRE2 Enhancements
        if(lstLAWithoutBankingSurrogateIncomeProgram.size() > 0){
            LoanApplicationTriggerHelper2.deleteDeviationForODCCAccountType(lstLAWithoutBankingSurrogateIncomeProgram);
        }
    }
    
    //Added by Vaishali for BREII-II
    public static User currentUser(){
        User currentUser = [Select id, RoleOfUser__c from User Where Id= : userinfo.getUserId()]; //Added by Vaishali for BREII-II
        return currentUser;
    }
    
    //Added by Vaishali for Code Optimization Activity
    public static String currentProfile(){
        String currentProfile = [SELECT Name from Profile WHERE Id =: userinfo.getProfileId()].Name; //Added by Vaishali for Code Optimization Activity
        return currentProfile;
    }
    
    //Added by Abhilekh on 23rd October 2020 for UCIC Phase II
    private static boolean run = true;
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }
    
    //Added by Abhilekh on 23rd October 2020 for UCIC Phase II
    private static boolean run2 = true;
    public static boolean runOnce2(){
        if(run2){
            run2=false;
            return true;
        }else{
            return run2;
        }
    }
    
    //Added by Abhilekh on 23rd October 2020 for UCIC Phase II
    private static boolean run3 = true;
    public static boolean runOnce3(){
        if(run3){
            run3=false;
            return true;
        }else{
            return run3;
        }
    }
    
    //Added by Abhilekh on 23rd October 2020 for UCIC Phase II
    private static boolean run4 = true;
    public static boolean runOnce4(){
        if(run4){
            run4=false;
            return true;
        }else{
            return run4;
        }
    }
    
    //Added by Abhilekh on 23rd October 2020 for UCIC Phase II
    private static boolean run5 = true;
    public static boolean runOnce5(){
        if(run5){
            run5=false;
            return true;
        }else{
            return run5;
        }
    }
}