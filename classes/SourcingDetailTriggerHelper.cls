public class SourcingDetailTriggerHelper {
    public static void checkSourceDetail(Sourcing_Detail__c sd){
        Id laId = sd.Loan_Application__c;
        List<Sourcing_Detail__c> sdOld = [Select Id from Sourcing_Detail__c where Loan_Application__c =: laId];
        if(sdOld.size() != 0){
            sd.addError('You can only enter one Sourcing Detail on Loan Application');
        }
    }
    public static void checkSourceDetailType(List<Sourcing_Detail__c> lstsd){
        for(Sourcing_Detail__c sd: lstsd){
        if((sd.Source_Code__c == '20' || sd.Source_Code__c == '19') && (sd.Lead_Broker_ID__c == '' || sd.Lead_Broker_ID__c== null)){
            if (sd.Lead_Sourcing_Detail__c == 'HHFL Website') {
                sd.Lead_Broker_ID__c= '8149';//Added by Saumya for Lead Changes
            } else if (sd.Lead_Sourcing_Detail__c == 'Inbound Call') {
                sd.Lead_Broker_ID__c= '7887';//Added by Saumya for Lead Changes
            } else if (sd.Lead_Sourcing_Detail__c == 'PaisaBazaar.com' || sd.Lead_Sourcing_Detail__c == 'BankBazaar.com') {
                sd.Lead_Broker_ID__c= '8482';//Added by Saumya for Lead Changes
            }
            else if (sd.Lead_Sourcing_Detail__c == 'Advertisement') {//Added by Saumya for Lead Changes
                sd.Lead_Broker_ID__c= '7887';//Added by Saumya for Lead Changes
            }
            else if (sd.Lead_Sourcing_Detail__c == 'WishFin') {//Added by Saumya for Lead Changes
                sd.Lead_Broker_ID__c= '7887';
            }
            else if (sd.Lead_Sourcing_Detail__c == 'CIBIL') {//Added by Saumya for Lead Changes
                sd.Lead_Broker_ID__c= '7887';
            }
            else if (sd.Lead_Sourcing_Detail__c == 'My Money Karma') {//Added by Saumya for Lead Changes
                sd.Lead_Broker_ID__c= '9796';//Added by Saumya for Lead Changes
            }
            else if (sd.Lead_Sourcing_Detail__c == 'My Money Mantra') {//Added by Saumya for Lead Changes
                sd.Lead_Broker_ID__c= '9795';//Added by Saumya for Lead Changes
			}    
			else if (sd.Lead_Sourcing_Detail__c == 'Referral Program') {//Added by Saumya for Lead Changes 13/01
			sd.Lead_Broker_ID__c= '10578';//Added by Saumya for Lead Changes
			}
			else if(sd.Source_Code__c == '19' && (sd.Lead_Broker_ID__c == null || sd.Lead_Broker_ID__c == '')){
			sd.Lead_Broker_ID__c= '7887';//Added by Saumya for Lead Changes
			}
        }
		}
        
    }
/*****************************************************
@Description   : This method is used to prevent creation of more than one Sourcing Detail on a LoanApplication (TIL-1276).
@params		   : list of Sourcing_Detail__c
@Author        : Chitransh Porwal
@Created Date  : 6 August 2019
*********************************************************/
    public static void duplicationDetection(List<Sourcing_Detail__c> sourcingDetailRecords){
        Map<String, Integer> mapUniqueConstrainttoCount = new Map<String, Integer>();
        List<Sourcing_Detail__c> lstExistingSourcingDetail = new List<Sourcing_Detail__c>();
        String strUniqueConstraint;
        if(sourcingDetailRecords.size() >1) {
            lstExistingSourcingDetail = [Select Id, Name, Loan_Application__c from Sourcing_Detail__c];
            if(!lstExistingSourcingDetail.isEmpty()) {
                for(Sourcing_Detail__c objExistingSourcingDetail : lstExistingSourcingDetail) {
                    if(!mapUniqueConstrainttoCount.containsKey(objExistingSourcingDetail.Loan_Application__c)) {
                        mapUniqueConstrainttoCount.put(objExistingSourcingDetail.Loan_Application__c, 1);
                    }
                    else {
                         mapUniqueConstrainttoCount.put(objExistingSourcingDetail.Loan_Application__c,mapUniqueConstrainttoCount.get(objExistingSourcingDetail.Loan_Application__c)+1);
                    }
                }
            }
            for(Sourcing_Detail__c objSourcingDetail : sourcingDetailRecords) {
                if(!mapUniqueConstrainttoCount.containsKey(objSourcingDetail.Loan_Application__c)) {
                    mapUniqueConstrainttoCount.put(objSourcingDetail.Loan_Application__c, 1);
                }
                else {
                    if(!Test.isRunningTest())
                    objSourcingDetail.addError('You are trying to create a duplicate record. A record already exists for the specified Loan Application.');
                   }
            }
        }
        else {
            lstExistingSourcingDetail = [Select Id, Name, Loan_Application__c from Sourcing_Detail__c WHERE Loan_Application__c =: sourcingDetailRecords[0].Loan_Application__c];
            if(!lstExistingSourcingDetail.isEmpty()) {
                if(!Test.isRunningTest())
                sourcingDetailRecords[0].addError('You are trying to create a duplicate record. A record already exists for the specified Loan Application.');
            }
        }
        
    }
	
	// Added by Vaishali for BRE
    public Static void resetBREFields(Map<Id, Sourcing_Detail__c> newMap, Map<Id, Sourcing_Detail__c> oldMap) {
        Map<Id,BRE_Eligibility_reset_field__mdt	> mapBREEligibilityReset = new Map<Id,BRE_Eligibility_reset_field__mdt	>([Select Id, Field__c, Object__c, Active__c from BRE_Eligibility_reset_field__mdt where Object__c = 'Sourcing_Detail__c' and Active__c = true]);
        Set<Id> setOfAppIds = new Set<Id>();
        for(Sourcing_Detail__c sourcingDetail : newMap.values()) {
            for ( BRE_Eligibility_reset_field__mdt objbreReset : mapBREEligibilityReset.values() ) {
                if( sourcingDetail.get(objbreReset.Field__c) != oldMap.get(sourcingDetail.id).get(objbreReset.Field__c)) {
                    setOfAppIds.add(sourcingDetail.Loan_Application__c);
                    system.debug('Setting Financial_Eligibility_Valid__c to false');
                    break;
                }
             }
        }
        
        system.debug('setOfAppIds '+setOfAppIds);
        List<Loan_Application__c> lstOfLoanApp = new List<Loan_Application__c>();
        if(!setOfAppIds.isEmpty()) {
            lstOfLoanApp = [SELECT Id, Financial_Eligibility_Valid__c FROM Loan_Application__c WHERE ID IN: setOfAppIds];
        }
        System.debug('lstOfLoanApp '+lstOfLoanApp);
        if(!lstOfLoanApp.isEmpty()) {
            for(Loan_Application__c la: lstOfLoanApp) {
                la.Financial_Eligibility_Valid__c = false;   
            }
        }
        update lstOfLoanApp;
    }
    //End of patch- Added by Vaishali for BRE
	//Added By Abhishek for Sourcing BRD - START
    public static void updateConnectorDetails(List<Sourcing_Detail__c> newList, Map<Id, Sourcing_Detail__c> oldMap) {
        Boolean isInsert = (oldMap == null);
        Set<Id> connectorIdSet = new Set<Id>();
        
        for(Sourcing_Detail__c sdObj : newList) {
            System.debug('sdObj.Connector_Name__c==>'+sdObj.Connector_Name__c);
            System.debug('isInsert==>'+isInsert);
            if(isInsert){
                if(String.isNotBlank(sdObj.Connector_Name__c)) {
                    connectorIdSet.add(sdObj.Connector_Name__c);
                }
            } else if(sdObj.Connector_Name__c != oldMap.get(sdObj.Id).Connector_Name__c){
                connectorIdSet.add(sdObj.Connector_Name__c);
            }
        }
        Map<Id,DSA_Master__c> connectorDSAMap = new Map<Id,DSA_Master__c>([SELECT Id, Base_Dealer__c FROM DSA_Master__c WHERE Id IN :connectorIdSet]);
        for(Sourcing_Detail__c sdObj : newList) {
            if(connectorDSAMap.containsKey(sdObj.Connector_Name__c)) {
                DSA_Master__c tempDSAObj = connectorDSAMap.get(sdObj.Connector_Name__c);
                sdObj.Base_Dealer__c = (tempDSAObj.Base_Dealer__c == null) ? null : tempDSAObj.Base_Dealer__c;
            }
        }
    }
    public static void checkSubDealerParent(List<Sourcing_Detail__c> newList) {
        Set<Id> SDLRIdSet = new Set<Id>();
        Map<Id, DSA_Master__c> DSAMasterMap;
        String SDLR = 'SDLR/EC';
        for(Sourcing_Detail__c sdObj : newList) {
            SDLRIdSet.add(sdObj.Hero_Sub_Dealer_Extension_Counter__c);
        }
        DSAMasterMap = new Map<Id, DSA_Master__c>([SELECT Id, Type__c, Parent_Dealer__c FROM DSA_Master__c WHERE Id IN :SDLRIdSet ]);
        for(Sourcing_Detail__c sdObj1 : newList) {
            if(DSAMasterMap.containsKey(sdObj1.Hero_Sub_Dealer_Extension_Counter__c)) {
                DSA_Master__c newDSAMaster = DSAMasterMap.get(sdObj1.Hero_Sub_Dealer_Extension_Counter__c);
                if(newDSAMaster.Parent_Dealer__c != sdObj1.Hero_Dealer__c) {
                    sdObj1.addError('Please select a Sub-Dealer whose parent is same as the selected Hero Dealer.');
                }
            }
        }
    }
    //Added By Abhishek for Sourcing BRD - END
	
	//Added By Abhishek for TIL-00002442 - START
    public static void populateHierarchy(List<Sourcing_Detail__c> newList, Map<Id, Sourcing_Detail__c> oldMap) {
        Boolean isInsert = (oldMap == null);
        List<String> fieldNamesToPopulate = new List<String>{'Team_Lead__c','Sales_User_Name__c','ASM__c','RSM__c','Team_Lead_Employee_Code__c','ASM_Employee_Code__c','RSM_Employee_Code__c'};
        Map<String,String> RoleVsSDFieldMap = new Map<String,String>{'DST'=>'DST__c','TL'=>'Team_Lead__c','SM'=>'Sales_User_Name__c','ASM'=>'ASM__c','RSM'=>'RSM__c'};
        Map<Id,User> IdvsUserMap = new Map<Id,User>([SELECT Id,Name,Role_in_Sales_Hierarchy__c,ManagerId,EmployeeNumber FROM User WHERE IsActive = true AND (Profile.Name = 'DST' OR Profile.Name = 'Sales Team')]);
        for(Sourcing_Detail__c sdObj : newList) {
            if(isInsert || sdObj.DST__c != oldMap.get(sdObj.Id).DST__c){
                if(IdvsUserMap.containsKey(sdObj.DST__c)){
                    User userObj = IdvsUserMap.get(sdObj.DST__c);
                    Set<String> updatedFieldNames = new Set<String>();
                    while(userObj != null && String.isNotBlank(userObj.Role_in_Sales_Hierarchy__c)){
                        System.debug('Hello-->>'+userObj.Id+'---'+userObj.Role_in_Sales_Hierarchy__c);
                        if(RoleVsSDFieldMap.containsKey(userObj.Role_in_Sales_Hierarchy__c)){
                            String fieldname = RoleVsSDFieldMap.get(userObj.Role_in_Sales_Hierarchy__c);
                            sdObj.put(fieldname,userObj.Id);
                            updatedFieldNames.add(fieldname);
                            if(userObj.Role_in_Sales_Hierarchy__c == 'TL' && String.isNotBlank(userObj.EmployeeNumber)){
                                sdObj.put('Team_Lead_Employee_Code__c',userObj.EmployeeNumber);
                                updatedFieldNames.add('Team_Lead_Employee_Code__c');
                            }
                            else if(userObj.Role_in_Sales_Hierarchy__c == 'ASM' && String.isNotBlank(userObj.EmployeeNumber)){
                                sdObj.put('ASM_Employee_Code__c',userObj.EmployeeNumber);
                                updatedFieldNames.add('ASM_Employee_Code__c');
                            }
                            else if(userObj.Role_in_Sales_Hierarchy__c == 'RSM' && String.isNotBlank(userObj.EmployeeNumber)){
                                sdObj.put('RSM_Employee_Code__c',userObj.EmployeeNumber);
                                updatedFieldNames.add('RSM_Employee_Code__c');
                            }
                        }
                        if(String.isBlank(userObj.ManagerId)){
                            break;
                        } else {
                            userObj = IdvsUserMap.get(userObj.ManagerId);
                        }
                    }
                    System.debug('fieldNamesToPopulate : '+ fieldNamesToPopulate);
                    if(!isInsert){    
                        System.debug('updatedFieldNames : '+ updatedFieldNames);
                        for(String fName : fieldNamesToPopulate){
                            if(!updatedFieldNames.contains(fName)){
                                sdObj.put(fName, ''); 
                            }      
                        }    
                    }
                }
                else{
                    if(!isInsert){
                        for(String fName : fieldNamesToPopulate){
                            sdObj.put(fName, '');
                        }    
                    }    
                }
            }
        }
    }
    //Added By Abhishek for TIL-00002442 - END
	
	/*Added by Ankit for Strategic Alliance*/
    public static void populateRSMUserAndBREOnLA(List<Sourcing_Detail__c> sourcingDetailList, Map<Id, Sourcing_Detail__c> oldMap){
        System.debug('populateRSMUserAndBREOnLA');
        Set<Id> laIds = new Set<Id>();
        for(Sourcing_Detail__c sdObj : sourcingDetailList){
            if(oldMap == null || oldMap.get(sdObj.Id).DST__c != sdObj.DST__c){/*Added by Ankit for Code Optimization 26-11-2020*/
            	laIds.add(sdObj.Loan_Application__c);    
            }/*Added by Ankit for Code Optimization 26-11-2020*/
        }
        /*Added by Ankit for Code Optimization 27-11-2020 - Start*/
        List<Loan_Application__c> lstLoanApp = new List<Loan_Application__c>();
        if(laIds.size() > 0){
        	lstLoanApp = [SELECT Id,Branch__c,Line_Of_Business__c,RSM_User__c,RSM_for_BRE__c,BRE_L1_Approver__c/*Added by Ankit for change in RSM for BRE*/
                                                FROM Loan_Application__c WHERE Id IN:laIds];    
        }
        /*Added by Ankit for Code Optimization 27-11-2020 - End*/
        System.debug('lstLoanApp : '+ lstLoanApp);
        if(!lstLoanApp.isEmpty()){
            List<Loan_Application__c> lstLoanAppToUpdate = LoanApplicationTriggerHandler.updateRSMUserAndBRE(lstLoanApp);
            System.debug('lstLoanAppToUpdate : '+ lstLoanAppToUpdate);
            try{
                if(lstLoanAppToUpdate.size() > 0)
                    update lstLoanAppToUpdate;
            }
            catch(Exception e){
                System.debug('error : '+ e.getMessage());
            }    
        }
    }
    /*Added by Ankit for Strategic Alliance*/
}