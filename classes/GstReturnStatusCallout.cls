public class GstReturnStatusCallout implements Queueable, Database.AllowsCallouts{
    
    public list<customer_integration__c> custIntegrationRecords;
    public GstReturnStatusCallout(list<customer_integration__c> customerIntegrationRecords){
        this.custIntegrationRecords = customerIntegrationRecords;
    }
    
     public void execute(QueueableContext context) {
       For(customer_integration__c CI : custIntegrationRecords){ 
           if(CI.GSTIN__c != null){
                makeGstReturnStatus(CI.GSTIN__c, CI.Loan_Contact__c, CI.Id); 
           }
          }
		if(custIntegrationRecords != null && !custIntegrationRecords.isEmpty() && !Test.isRunningTest()) { //!Test.isRunningTest() applied By Amit Agarwal to overcome System.AsyncException: Maximum stack depth has been reached.
             system.debug('Call to GstGenCreditLinkCallout');
             System.enqueueJob(new GstGenCreditLinkCallout(custIntegrationRecords));
         }
     }
         
    @future(callout=true)
    public static void makeGstReturnStatus(String strGST , String loanContactId , String custIntegrationId ) {
        if(String.isNotBlank(strGST)) {
            List<Rest_Service__mdt> lstRestService = [SELECT 
                                                            MasterLabel, 
                                                            QualifiedApiName, 
                                                            Service_EndPoint__c, 
                                                            Client_Password__c, 
                                                            Client_Username__c, 
                                                            Request_Method__c, 
                                                            Time_Out_Period__c,
                                                      		org_id__c
                                                      FROM   
                                                            Rest_Service__mdt
                                                      WHERE  
                                                            DeveloperName = 'GST_3_GstReturnStatus'
                                                            LIMIT 1
                                                     ]; 
            System.debug('Debug Log for lstRestService GST_3_GstReturnStatus'+lstRestService);
            if(!lstRestService.isEmpty()) {
                GstReturnStatusResponseBody objResponseBody = new GstReturnStatusResponseBody();
                Blob headerValue = Blob.valueOf(lstRestService[0].Client_Username__c + ':' + lstRestService[0].Client_Password__c);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                Map<String,String> headerMap = new Map<String,String>();
                headerMap.put('Authorization',authorizationHeader);
                headerMap.put('Username',lstRestService[0].Client_Username__c);
                headerMap.put('Password',lstRestService[0].Client_Password__c);
                headerMap.put('Content-Type','application/json');
                headerMap.put('org-id',lstRestService[0].org_id__c);
                
                RequestBody objRequestBody = new RequestBody();
                    objRequestBody.consent = 'Y';
                	objRequestBody.pdfOutputType = '';
                    objRequestBody.gstin = strGST != null ? strGST : '';
                    
                System.debug('Debug Log for objRequestBody'+objRequestBody);
                String jsonRequest = Json.serializePretty(objRequestBody); 
                
                Http objHttp = new Http();
                HttpRequest objHttpRequest = new HttpRequest();
                
                
                if(String.isNotBlank(lstRestService[0].Service_EndPoint__c)) {
                    objHttpRequest.setEndPoint(lstRestService[0].Service_EndPoint__c);
                }
                else {
                    System.debug('Debug Log for error : Endpoint Empty. Please specify endpoint in metadata record.');
                }
                if(String.isNotBlank(lstRestService[0].Request_Method__c)) {
                    objHttpRequest.setMethod(lstRestService[0].Request_Method__c);
                }
                else {
                    System.debug('Debug Log for error : Requested Method Empty. Please specify Requested Method in metadata record.');
                }
                
                for(String headerKey : headerMap.keySet()){
                    objHttpRequest.setHeader(headerKey, headerMap.get(headerKey));
                    System.debug('Debug Log for headerKey'+headerKey);
                    System.debug('Debug Log for headerMap.get(headerKey)'+headerMap.get(headerKey));
                }
                objHttpRequest.setBody(jsonRequest);
                if(String.isNotBlank(String.valueOf(lstRestService[0].Time_Out_Period__c))) {
                    objHttpRequest.setTimeOut(Integer.valueOf(lstRestService[0].Time_Out_Period__c));
                }
                System.debug('Debug Log for request'+objHttpRequest);
                try {
                    HTTPResponse httpRes = objHttp.send(objHttpRequest);
                    //Create integration logs
                    //LogUtility.createIntegrationLogs(jsonRequest,httpRes,lstRestService[0].Service_EndPoint__c);
                    System.debug('Debug Log for response'+httpRes);
                    System.debug('Debug Log for response body'+httpRes.getBody());
                    System.debug('Debug Log for response status'+httpRes.getStatus());
                    System.debug('Debug Log for response status code'+httpRes.getStatusCode());
                    //Deserialization of response to obtain the result parameters
                    //objResponseBody = (ResponseBody)JSON.deserialize(httpRes.getBody(), ResponseBody.class);
                    objResponseBody = (GstReturnStatusResponseBody)Json.deserialize(httpRes.getBody(), GstReturnStatusResponseBody.class);
                    if(httpRes.getStatus() == 'OK' && httpRes.getStatusCode() == 200) {
                        if(objResponseBody != null) {
                            system.debug('objResponseBody ===' + objResponseBody);
                            
                            Customer_Integration__c GSTRecord = [Select id,Loan_Contact__c,Filling_Frequency__c,System_Created__c from Customer_Integration__c where id =: custIntegrationId LIMIT 1];
                            System.debug('GSTRecord Return Status ' + GSTRecord);
                            if(objResponseBody.result != null){
                                integer size = objResponseBody.result.size();
                                system.debug('size '+ size);
                                system.debug('objResponseBody.result[size-1]' + objResponseBody.result[size-1].filing_frequency);
                                System.enqueueJob(new EnqueDMLGSTINOne(GSTRecord,objResponseBody.result[size-1].filing_frequency,jsonRequest,String.valueOf(httpRes),lstRestService[0].Service_EndPoint__c));
                             }
                            /*integer size = objResponseBody.result.size();
                            system.debug('size '+ size);
                            system.debug('objResponseBody.result[size-1]' + objResponseBody.result[size-1].filing_frequency);
                            System.enqueueJob(new EnqueDMLGSTINOne(GSTRecord,objResponseBody.result[size-1].filing_frequency,jsonRequest,String.valueOf(httpRes),lstRestService[0].Service_EndPoint__c));
                            /*if(GSTRecord.Id != null){
                                system.debug('filing_frequency  ' + objResponseBody.result.filing_frequency);
                                GSTRecord.Filling_Frequency__c = objResponseBody.result.filing_frequency;
                                GSTRecord.System_Created__c = true;
                            	update GSTRecord;
                                system.debug('GSTRecord.FillingFreq  ' + GSTRecord.Filling_Frequency__c  );
                            }
                            Loan_Contact__c loanContact = [Select id,GST_ReturnStatus__c,GST_Initiated__c from Loan_Contact__c where id =: loanContactId limit 1];
                            if(loanContact.Id != null){
                                loanContact.GST_ReturnStatus__c = true;
                                update loanContact;
                            }*/
                            System.debug('Result Array'+objResponseBody.result);
                            //System.debug('Result Request Id'+objResponseBody.statusCode);
                            //System.debug('Result Status Code'+objResponseBody.requestId);
                        }
                    }
                    
                }
                catch(Exception ex) {
                    System.debug('Debug Log for exception'+ex);
                }
           }
       }
           
    }
    
    public class RequestBody {
        public String pdfOutputType;
        public String consent;
        public String gstin;
    }
}