@isTest
public class CRM_CaseControllerTest 
{
    @testSetup static void setup(){
        //System admin user
        CRM_Utility.insertStandardUser();
        
        //Case and loan application
        Loan_Application__c loanApp = new Loan_Application__c();
        loanApp.Loan_Number__c = '1000487';
        loanApp.Loan_Application_Number__c = 'HHFDELHOU18000000205';
        insert loanApp;
        
        case theCase = new case();
        theCase.LD_Branch_ID__c = '1';
        theCase.Email__c = 'test@example.com';
        theCase.Loan_Application__c = loanApp.Id;
        theCase.LD_Customer_ID__c = '101402';
        insert theCase;
    }
    
    static testMethod void testReassignOriginalOwner(){
        user testUser = [Select Id from user where username='hhfStandarduser1@testorg.com'];
        case theCase = [Select Id, Original_Case_Owner__c from case order by createddate desc limit 1];
        theCase.Original_Case_Owner__c = testUser.Id;
        update theCase;
        
        CRM_CaseController.reassignOriginalOwner(theCase.Id);
    }
    
    static testMethod void testGenDocRpSave(){
        Id caseId;
        String email;
        String attachName;
        String optionSelected;
        String attachmentOptionSelected;
        map<string,string> pdfDetailsMap = new map<string,string>();
        case theCase = [Select Id, Email__c from case order by createddate desc limit 1];
        
        caseId = theCase.Id;
        email = theCase.Email__c;
        attachName = 'Repayment';
        optionSelected = 'Repayment';
        attachmentOptionSelected = 'save';
        
        pdfDetailsMap.put('caseId',caseId);
        pdfDetailsMap.put('email',email);
        pdfDetailsMap.put('attachName',attachName);
        pdfDetailsMap.put('attachmentOptionSelected',attachmentOptionSelected);
        CRM_CaseController.generateDocument(optionSelected,pdfDetailsMap);
    }
    
    static testMethod void testGenDocRpSaveAndEmail(){
        Id caseId;
        String email;
        String attachName;
        String optionSelected;
        String attachmentOptionSelected;
        map<string,string> pdfDetailsMap = new map<string,string>();
        case theCase = [Select Id, Email__c from case order by createddate desc limit 1];
        
        caseId = theCase.Id;
        attachName = 'Repayment';
        email = theCase.Email__c;
        optionSelected = 'Repayment';
        attachmentOptionSelected = 'saveAndEmail';
        
        pdfDetailsMap.put('caseId',caseId);
        pdfDetailsMap.put('email',email);
        pdfDetailsMap.put('attachName',attachName);
        pdfDetailsMap.put('attachmentOptionSelected',attachmentOptionSelected);
        
        CRM_CaseController.generateDocument(optionSelected,pdfDetailsMap);
    }
    
    static testMethod void testGenDocItcProvisionalSave(){
        Id caseId;
        String email;
        String attachName;
        String SubType;
        String caseFromDate;
        String caseToDate;
        String optionSelected;
        String attachmentOptionSelected;
        
        map<string,string> pdfDetailsMap = new map<string,string>();
        case theCase = [Select Id, Email__c from case order by createddate desc limit 1];
        
        caseId = theCase.Id;
        email = theCase.Email__c;
        attachName = 'ITC';
        optionSelected = 'ITC';
        SubType = 'Provisional';
        attachmentOptionSelected = 'save';
        
        pdfDetailsMap.put('caseId',caseId);
        pdfDetailsMap.put('email',email);
        pdfDetailsMap.put('attachName',attachName);
        pdfDetailsMap.put('attachmentOptionSelected',attachmentOptionSelected);
        pdfDetailsMap.put('SubType',SubType);
        
        CRM_CaseController.generateDocument(optionSelected,pdfDetailsMap);
    }
    
    static testMethod void testGenDocItcProvisionalSaveAndEmail(){
        Id caseId;
        String email;
        String attachName;
        String SubType;
        String caseFromDate;
        String caseToDate;
        String optionSelected;
        String attachmentOptionSelected;
        
        map<string,string> pdfDetailsMap = new map<string,string>();
        case theCase = [Select Id, Email__c from case order by createddate desc limit 1];
        
        caseId = theCase.Id;
        email = theCase.Email__c;
        attachName = 'ITC';
        optionSelected = 'ITC';
        SubType = 'Provisional';
        attachmentOptionSelected = 'saveAndEmail';
        
        pdfDetailsMap.put('caseId',caseId);
        pdfDetailsMap.put('email',email);
        pdfDetailsMap.put('attachName',attachName);
        pdfDetailsMap.put('attachmentOptionSelected',attachmentOptionSelected);
        pdfDetailsMap.put('SubType',SubType);
        
        CRM_CaseController.generateDocument(optionSelected,pdfDetailsMap);
    }
    
    static testMethod void testGetCaseLoanInfo(){
        Id caseId;
        String appId;
        String appNumber;
        String customerId;
        
        case theCase = [Select Id, Email__c, LD_Customer_ID__c, LMS_Application_ID__c, Loan_Application_Number__c from case order by createddate desc limit 1];
        caseId = theCase.Id;
        appId = theCase.LMS_Application_ID__c;
        appNumber = theCase.Loan_Application_Number__c;
        customerId = theCase.LD_Customer_ID__c;
        
        system.debug(caseId+'-'+appId+'-'+appNumber+'-'+customerId);
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new CRM_LmsLoanInfoMock());
        CRM_CaseController.getCaseLoanInfo(caseId,appId,appNumber,customerId);
        test.stopTest();
    }
    
    static testMethod void testGetCaseLoanInfo2(){
        Id caseId;
        String appId;
        String appNumber;
        String customerId;
        
        case theCase = [Select Id, Email__c, LD_Customer_ID__c, LMS_Application_ID__c, Loan_Application_Number__c from case order by createddate desc limit 1];
        caseId = theCase.Id;
        appId = theCase.LMS_Application_ID__c;
        appNumber = theCase.Loan_Application_Number__c;
        customerId = theCase.LD_Customer_ID__c;
        
        system.debug(caseId+'-'+appId+'-'+appNumber+'-'+customerId);
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new CRM_LmsLoanInfoMock2());
        CRM_CaseController.getCaseLoanInfo(caseId,appId,appNumber,customerId);
        test.stopTest();
    }
    
    static testMethod void testGetCaseLoanList(){
        Id caseId;
        String appId;
        String appNumber;
        String customerId;
        
        case theCase = [Select Id, Email__c, LD_Customer_ID__c, LMS_Application_ID__c, Loan_Application_Number__c from case order by createddate desc limit 1];
        caseId = theCase.Id;
        appId = theCase.LMS_Application_ID__c;
        appNumber = theCase.Loan_Application_Number__c;
        customerId = theCase.LD_Customer_ID__c;
        
        system.debug(caseId+'-'+appId+'-'+appNumber+'-'+customerId);
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new CRM_LmsLoanListMock());
        CRM_CaseController.getCaseLoanInfo(caseId,appId,appNumber,customerId);
        test.stopTest();
    }
    
    static testMethod void testsetFiscalYearDates(){
        CRM_CaseController.setFiscalYearDates();
    }
}