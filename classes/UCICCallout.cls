/***********************************************************************************************
Created By:     Chitransh Porwal(Deloitte)    
Created Date:   19-06-2019
Description:    This class contains callout methods for API integration with UCIC.
***********************************************************************************************/
public class UCICCallout {
    
    @future(callout = true) 
    public static void makeUCICCallout(Id LoanApplicationId){
        
        system.debug(' == In UCICCallout == LoanApplicationId' + LoanApplicationId);
        if(String.isNotBlank(LoanApplicationId)){
            List<Loan_Contact__c> lstCustomerDetail = [SELECT 
                                                       Id,
                                                       Loan_Applications__r.Loan_Number__c,
                                                       Loan_Applications__r.UCICApp_Sequence__c,
                                                       Name,
                                                       Customer__r.Customer_ID__c,
                                                       GSTIN_Number__c,
                                                       Voter_ID_Number__c,
                                                       Spouse_Name__c,
                                                       Pan_Number__c,
                                                       Mobile__c,
                                                       Borrower__c,
                                                       Gender__c,
                                                       Email__c,
                                                       Date_Of_Birth__c,
                                                       DL_Number__c,
                                                       Constitution__c,
                                                       Applicant_Type__c,
                                                       Aadhaar_Number__c,
                                                       customer_name__c,
                                                       Customer__r.FirstName,Customer__r.LastName,Customer__r.Name,//Added by Chitransh For TIL-1542//
													   Business_Date_of_UCIC_Initiation__c,/** Added by Saumya for UCIC II Enhancements **/																							  
                                                       (SELECT Id,
                                                        StateLP__c,
                                                        Pincode_LP__r.Name,
                                                        CityLP__c,
                                                        Type_of_address__c,
                                                        Address_Line_2__c,
                                                        Address_Line_1__c,
                                                        Address_Type__c,
                                                        Address_Landmark__c
                                                        FROM Addresses__r),
                                                        Customer__r.Date_Of_Incorporation__c    //Added by Vaishali for UCIC-FIX
                                                       FROM Loan_Contact__c 
                                                       WHERE Loan_Applications__c =: LoanApplicationId];
            
			Loan_Application__c objLA = [SELECT 
                                         Id, 
                                         Retrigger_UCIC__c 
                                         FROM Loan_Application__c 
                                         WHERE Id =: LoanApplicationId]; // Added by Saumya for UCIC II Enhancements
            
            if(!lstCustomerDetail.isEmpty()){
                
                List<Rest_Service__mdt> lstRestService = [SELECT 
                                                          MasterLabel, 
                                                          QualifiedApiName, 
                                                          Service_EndPoint__c, 
                                                          Client_Password__c, 
                                                          Client_Username__c, 
                                                          Request_Method__c, 
                                                          Time_Out_Period__c  
                                                          FROM   
                                                          Rest_Service__mdt
                                                          WHERE  
                                                          DeveloperName = 'UCICCallout'
                                                          LIMIT 1
                                                         ];
                
                system.debug('lstRestService' + lstRestService);
				                
                List<UCIC_Negative_Database__mdt> lstUCICDB = [SELECT
                                                               MasterLabel,
                                                               DeveloperName,
                                                               UCIC_Database_Name__c,
                                                               Active__c
                                                               FROM UCIC_Negative_Database__mdt
                                                               WHERE Active__c = true]; //Added by Abhilekh on 22nd September 2020 for UCIC Phase II
                
                /******Below code added by Abhilekh on 11th September 2020 for UCIC Phase II*******************/
                Id UCICRecordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get(Constants.UCIC).getRecordTypeId();
                Id primaryApplicant;
                for(Loan_Contact__c lc: lstCustomerDetail){
                    if(lc.Applicant_Type__c == Constants.Applicant){
                       primaryApplicant = lc.Id;
                    }
                }
                
                /******Above code added by Abhilekh on 11th September 2020 for UCIC Phase II*******************/
                
                if(!lstRestService.isEmpty()){
                    Blob headerValue = Blob.valueOf(lstRestService[0].Client_Username__c + ':' + lstRestService[0].Client_Password__c);
                    String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                    Map<String,String> headerMap = new Map<String,String>();
                    headerMap.put('Authorization',authorizationHeader);
                    headerMap.put('Username',lstRestService[0].Client_Username__c);
                    headerMap.put('Password',lstRestService[0].Client_Password__c);
                    headerMap.put('Content-Type','application/json');
                    
                    List<UCICRequestBody.cls_Individuals> listRequestBody = new List<UCICRequestBody.cls_Individuals>();
                    UCICRequestBody reqBody = new UCICRequestBody();
                    system.debug('Debug log for LMS Application ID  ' + lstCustomerDetail[0].Loan_Applications__r.Loan_Number__c);
                    system.debug('Debug log for UCICApp_Sequence__c ' + lstCustomerDetail[0].Loan_Applications__r.UCICApp_Sequence__c);
                    String appId = '';
                    /*if(lstCustomerDetail[0].Loan_Applications__r.UCICApp_Sequence__c == 0){
                        appId = lstCustomerDetail[0].Loan_Applications__r.Loan_Number__c;
                    }
                    else{
                     appId = lstCustomerDetail[0].Loan_Applications__r.Loan_Number__c +'_'+ lstCustomerDetail[0].Loan_Applications__r.UCICApp_Sequence__c;   
                    }*/
                    appId = lstCustomerDetail[0].Loan_Applications__r.Loan_Number__c;
                    system.debug('appId  ' + appId);
                    reqBody.Application_Id = appId;//String.isNotBlank(lstCustomerDetail[0].Loan_Applications__r.Loan_Number__c)? lstCustomerDetail[0].Loan_Applications__r.Loan_Number__c : '';
                    reqBody.Source_Channel = 'SFDC-HL'; //SFDC-HL
                    reqBody.Application_Status = 'Active';
                    reqBody.Application_Type = 'R';
                    
					Boolean firstUCIC = false; //Added by Abhilekh on 9th September 2020 for UCIC Phase II
                    
                    //Below for loop added by Abhilekh on 9th September 2020 for UCIC Phase II
                    for(Loan_Contact__c lc: lstCustomerDetail){
                        if(lc.Applicant_Type__c == Constants.Applicant && lc.Business_Date_of_UCIC_Initiation__c == null){
                            firstUCIC = true;
                        }
                    }
                    
                    //Below if block added by Abhilekh on 9th September 2020 for UCIC Phase II
                    if(firstUCIC == true){
                        reqBody.UCICFlag_SFDC = 'I';
                    }
                    else{
                        reqBody.UCICFlag_SFDC = 'U';
                    }
                    
                    String allDBName = ''; //Added by Abhilekh on 22nd September 2020 for UCIC Phase II
                    
                    //Below for loop added by Abhilekh on 22nd September 2020 for UCIC Phase II
                    for(UCIC_Negative_Database__mdt dbName: lstUCICDB){
                        if(allDBName == ''){
                            allDBName = dbName.UCIC_Database_Name__c;
                        }
                        else{
                            allDBName = allDBName + ',' + dbName.UCIC_Database_Name__c;
                        }
                    }
                    
                    reqBody.DBTOMATCHED = allDBName != null ? allDBName : ''; //Added by Abhilekh on 22nd September 2020 for UCIC Phase II
                    
                    UCICRequestBody.cls_Loanproductdetail lpd = new UCICRequestBody.cls_Loanproductdetail();
                    lpd.Asset_Details = '';
                    lpd.Lob = 'HL';
                    lpd.Lob_Product = 'HL';
                    lpd.Source = 'SFDC-HL';
                    lpd.Lob_Segment = 'HL';
                    lpd.Program = '';
                    
                    reqBody.Loanproductdetail = lpd;
                    
                    for(Loan_Contact__c CustomerDetail : lstCustomerDetail){
                        
                        UCICRequestBody.cls_Individuals objRequestBody = new UCICRequestBody.cls_Individuals();
                        
                        objRequestBody.Workprofile = '';
                        objRequestBody.Voter_Id = String.isNotBlank(CustomerDetail.Voter_ID_Number__c) ? CustomerDetail.Voter_ID_Number__c : ''; 
                        objRequestBody.Tanno_Gstanno =  String.isNotBlank(CustomerDetail.GSTIN_Number__c) ? CustomerDetail.GSTIN_Number__c : '';
                        objRequestBody.Spouse_Mname = String.isNotBlank(CustomerDetail.Spouse_Name__c) ? CustomerDetail.Spouse_Name__c : '';  
                        objRequestBody.Spouse_Fname = String.isNotBlank(CustomerDetail.Spouse_Name__c) ? CustomerDetail.Spouse_Name__c : '';    
                        objRequestBody.Spouse_Lname = String.isNotBlank(CustomerDetail.Spouse_Name__c) ? CustomerDetail.Spouse_Name__c : '';    
                        objRequestBody.Relation = '';
                        objRequestBody.Rationcardno = '';
                        objRequestBody.Phone2 = '';
                        objRequestBody.Passport_No = '';
                        objRequestBody.Pan_Card = String.isNotBlank(CustomerDetail.Pan_Number__c) ? CustomerDetail.Pan_Number__c : ''; 
                        objRequestBody.Mother_Fname = '';
                        objRequestBody.Mother_Lname = '';
                        objRequestBody.Mobile = String.isNotBlank(CustomerDetail.Mobile__c)?CustomerDetail.Mobile__c:'';
                        objRequestBody.Match_Status_Un ='';
                        objRequestBody.Lpgno = '';
                        /*********Added by Chitransh for TIL-1542***************/
                        if(CustomerDetail.Borrower__c == Constants.Individual_API) {
                            objRequestBody.Fname = String.isNotBlank(CustomerDetail.Customer__r.FirstName) ? CustomerDetail.Customer__r.FirstName : '';
                            objRequestBody.Lname = String.isNotBlank(CustomerDetail.Customer__r.LastName) ? CustomerDetail.Customer__r.LastName : ''; 
                            objRequestBody.Dob = formatDate(CustomerDetail.Date_Of_Birth__c);
                        }
                        else {
                           objRequestBody.Fname = String.isNotBlank(CustomerDetail.Customer__r.Name) ? CustomerDetail.Customer__r.Name:'';
                            objRequestBody.Lname ='';// String.isNotBlank(CustomerDetail.Customer__r.Name) ? CustomerDetail.Customer__r.Name:'';
                            objRequestBody.Dob = formatDate(CustomerDetail.Customer__r.Date_Of_Incorporation__c );
                        }
                        /***************End of Patch added by Chitansh for TIL-1542*************/
                       // objRequestBody.Lname = String.isNotBlank(CustomerDetail.Customer__r.LastName)?CustomerDetail.Customer__r.LastName:'';//Commented By Chitransh for TIL-1542//
                        objRequestBody.Laa_Rc_Flag = '';
                        objRequestBody.Indv_Corp_Flag = String.isNotBlank(CustomerDetail.Borrower__c)?(CustomerDetail.Borrower__c == '1')?'I':'C' :'';
                        objRequestBody.Gender = String.isNotBlank(CustomerDetail.Gender__c)?CustomerDetail.Gender__c:'';
                        //objRequestBody.Fname = String.isNotBlank(CustomerDetail.Customer__r.FirstName)?CustomerDetail.Customer__r.FirstName:'';//Commented By Chitransh for TIL-1542//
                        objRequestBody.Father_Fname = '';
                        objRequestBody.Father_Lname = '';
                        objRequestBody.Father_Mname = '';
                        objRequestBody.Extension = '';
                        objRequestBody.Emailid = String.isNotBlank(CustomerDetail.Email__c)?CustomerDetail.Email__c:'';
                        
                        objRequestBody.Dl_No = String.isNotBlank(CustomerDetail.DL_Number__c)?CustomerDetail.DL_Number__c:'';
                        objRequestBody.Directline_Mobile2 = '';
                        objRequestBody.Customer_Id = String.isNotBlank(CustomerDetail.Customer__r.Customer_ID__c)?CustomerDetail.Customer__r.Customer_ID__c:'';
                        objRequestBody.Constitution = String.isNotBlank(CustomerDetail.Constitution__c)?CustomerDetail.Constitution__c:'';
                        objRequestBody.Cif_No = '';
                        objRequestBody.Areacode = '';
                        if(CustomerDetail.Applicant_Type__c == 'Applicant'){
                            objRequestBody.Applicant_Type = String.isNotBlank(CustomerDetail.Applicant_Type__c)?'BORROWER':'';
                        }
                        else if(CustomerDetail.Applicant_Type__c == 'Co- Applicant'){
                            objRequestBody.Applicant_Type = String.isNotBlank(CustomerDetail.Applicant_Type__c)?'CO-BORROWER':'';
                        }
                        else{
                            objRequestBody.Applicant_Type = String.isNotBlank(CustomerDetail.Applicant_Type__c)?CustomerDetail.Applicant_Type__c:'';
                        }
                        objRequestBody.Aadhar = String.isNotBlank(CustomerDetail.Aadhaar_Number__c)?CustomerDetail.Aadhaar_Number__c:'';
                        
                        List<UCICRequestBody.cls_Addresses> listAddress = new List<UCICRequestBody.cls_Addresses>();
                        for(Address__c CustomerAddress : CustomerDetail.Addresses__r){
                            UCICRequestBody.cls_Addresses Address = new UCICRequestBody.cls_Addresses();
                            Address.Village = '';
                            Address.State = String.isNotBlank(CustomerAddress.StateLP__c)?CustomerAddress.StateLP__c:'';
                            Address.Pin_Code = String.isNotBlank(CustomerAddress.Pincode_LP__r.Name)?CustomerAddress.Pincode_LP__r.Name:'';
                            Address.City_District = String.isNotBlank(CustomerAddress.CityLP__c)?CustomerAddress.CityLP__c:''; 
                            Address.Address_Type = getAddressType(CustomerAddress.Address_Type__c);
                            Address.Address3 = String.isNotBlank(CustomerAddress.Address_Landmark__c)?CustomerAddress.Address_Landmark__c:'';
                            Address.Address2 = String.isNotBlank(CustomerAddress.Address_Line_2__c)?CustomerAddress.Address_Line_2__c:'';
                            Address.Address1 = String.isNotBlank(CustomerAddress.Address_Line_1__c)?CustomerAddress.Address_Line_1__c:'';
                            listAddress.add(Address);
                            
                        }
                        if(listAddress.size()> 0 ){
                            objRequestBody.Addresses = listAddress;
                        }
                        listRequestBody.add(objRequestBody);
                    }
                    reqBody.Individuals = listRequestBody;
                    String jsonRequest = Json.serializePretty(reqBody); 
                    //system.debug('jsonRequest' +jsonRequest);
                    HTTP http = new HTTP();
                    HTTPRequest httpReq = new HTTPRequest();
                    if(String.isNotBlank(lstRestService[0].Service_EndPoint__c)) {
                        httpReq.setEndPoint(lstRestService[0].Service_EndPoint__c);
                    }
                    else {
                        System.debug('Debug Log for error : Endpoint Empty. Please specify endpoint in metadata record.');
                    }
                    if(String.isNotBlank(lstRestService[0].Request_Method__c)) {
                        httpReq.setMethod(lstRestService[0].Request_Method__c);
                    }
                    else {
                        System.debug('Debug Log for error : Requested Method Empty. Please specify Requested Method in metadata record.');
                    }
                    for(String headerKey : headerMap.keySet()){
                        httpReq.setHeader(headerKey, headerMap.get(headerKey));
                    }
                    httpReq.setBody(jsonRequest);
                    if(String.isNotBlank(String.valueOf(lstRestService[0].Time_Out_Period__c))) {
                        httpReq.setTimeOut(Integer.valueOf(lstRestService[0].Time_Out_Period__c));
                    }
                    ResponseBody objResponseBody = new ResponseBody();
                    try {
                        HTTPResponse httpRes = http.send(httpReq);
                        
                        //Create integration logs
                        LogUtility.createIntegrationLogs(jsonRequest,httpRes,lstRestService[0].Service_EndPoint__c);
                        System.debug('Debug Log for response'+httpRes);
                        System.debug('Debug Log for response body'+httpRes.getBody());
                        System.debug('Debug Log for response status'+httpRes.getStatus());
                        System.debug('Debug Log for response status code'+httpRes.getStatusCode());
                        
                        //Deserialization of response to obtain the result parameters
                        objResponseBody = (ResponseBody)JSON.deserialize(httpRes.getBody(), ResponseBody.class);
                        
                        List<Customer_Integration__c> lstCI = [SELECT Id,UCIC_Negative_API_Response__c,
                                                               UCIC_Response_Status__c,Negative_UCIC_DB_Found__c,
                                                               UCIC_Successful_Initiation_DateTime__c,
                                                               UCIC_Callout_Failed__c 
                                                               FROM Customer_Integration__c 
                                                               WHERE RecordTypeId =: UCICRecordTypeId 
                                                               AND Loan_Application__c =: loanApplicationId 
                                                               AND Loan_Contact__c =: primaryApplicant 
                                                               ORDER BY CreatedDate DESC LIMIT 1];
                        //Above query Added by Abhilekh on 9th September 2020 for UCIC Phase II
                        
                        if(httpRes.getStatus() == 'OK' && httpRes.getStatusCode() == 200) {
                            if(objResponseBody != null) {
                                System.debug('Debug Log for ResponseCode'+objResponseBody.ResponseCode);
                                System.debug('Debug Log for ResponseMessage'+objResponseBody.ResponseMessage);
                                //lstCustomerDetail[0].Loan_Applications__r.UCICApp_Sequence__c = lstCustomerDetail[0].Loan_Applications__r.UCICApp_Sequence__c + 1;
                               //  update lstCustomerDetail[0];
                                system.debug('updated APPSEQUence' + lstCustomerDetail[0].Loan_Applications__r.UCICApp_Sequence__c);
								 
                                /*****Below code added by Abhilekh on 9th September 2020 for UCIC Phase II*******/
                                if(objResponseBody.ResponseMessage.containsIgnoreCase('success')){
                                    //lstCI[0].UCIC_Response_Status__c = true;
                                    lstCI[0].UCIC_Successful_Initiation_DateTime__c = System.now();
                                    update lstCI[0];
                                }
                                else{
                                    lstCI[0].UCIC_Callout_Failed__c = true;
                                    update lstCI[0];
                                    
                                    objLA.Retrigger_UCIC__c = false; //Added by Abhilekh on 9th September 2020 for UCIC Phase II
                        			update objLA; //Added by Abhilekh on 9th September 2020 for UCIC Phase II
                                }
                                /*****Above code added by Abhilekh on 9th September 2020 for UCIC Phase II*******/
                            }
                            else{
                                lstCI[0].UCIC_Callout_Failed__c = true; //Added by Abhilekh on 9th September 2020 for UCIC Phase II
                                update lstCI[0]; //Added by Abhilekh on 9th September 2020 for UCIC Phase II
                                
                                objLA.Retrigger_UCIC__c = false; //Added by Abhilekh on 9th September 2020 for UCIC Phase II
                        		update objLA; //Added by Abhilekh on 9th September 2020 for UCIC Phase II
                            }
                        }
                        else{
                            lstCI[0].UCIC_Callout_Failed__c = true; //Added by Abhilekh on 9th September 2020 for UCIC Phase II
                            update lstCI[0]; //Added by Abhilekh on 9th September 2020 for UCIC Phase II
                            
                            objLA.Retrigger_UCIC__c = false; //Added by Abhilekh on 9th September 2020 for UCIC Phase II
                        	update objLA; //Added by Abhilekh on 9th September 2020 for UCIC Phase II
                        }
                    }
                    catch(Exception ex) {
                        System.debug('Debug Log for exception'+ex);
                    }
                }
            }
        }
        
    }
	
    public static String formatDate(Date dt){
        if(dt != null) {
            Datetime receiptDateTime = DateTime.newInstance(dt.year(), dt.month(), dt.day());
            return receiptDateTime.format('dd/MM/yyyy'); 
        } else {
            return '';
        }    
        
    }
	
    public static String getAddressType(String addressType){
        if(addressType == 'Permanent'){
            return 'Permenant';
        }
        else if(addressType == 'Residence Address'){
            return 'Communicaton';
        }
        else if(addressType == 'Office/ Business'){
            return 'Office';
        }
        else{
            return 'Permenant';
        }
        
    }  
	
    public class ResponseBody{
        public Integer ResponseCode;   
        public String ResponseMessage;    
        
    }
    
}