@isTest
public class CRM_Case_SendEmailBatchableSchedulerTest {
    CRM_Case_SendEmailBatchableScheduler emailBatch=new CRM_Case_SendEmailBatchableScheduler();
  
    testmethod static void scheduleTest(){
        Test.startTest();
        CRM_Case_SendEmailBatchableScheduler emailBatch=new CRM_Case_SendEmailBatchableScheduler();
        String sch='0 0 * ? * MON-FRI *';
        String JobId=System.schedule('sendEmailBatchJob',sch, emailBatch);
         CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];
        Test.stopTest();
        
        
    }
}