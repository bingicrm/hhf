@isTest
public class TestMitcHandler
{
  public static testMethod void method()
  {
  LMS_Date_Sync__c objDate = new LMS_Date_Sync__c(Current_Date__c = system.today());
   insert objDate;
   Account acc=new Account();
    acc.name='Applicant123';
    acc.phone='9234509786';
    acc.PAN__c='ADGPW4078E';
    acc.Date_of_Incorporation__c=date.today();
    insert acc;

System.debug(acc.id);

Scheme__c sc=new Scheme__c();
sc.name='Construction Finance';
sc.Scheme_Code__c='CF';
sc.Scheme_Group_ID__c='6';
sc.Product_Code__c='HL';
insert sc;
System.debug(sc.id);
  
Loan_Application__c lap=new Loan_Application__c ();
lap.Customer__c=acc.id;
lap.Scheme__c=sc.id;
lap.Requested_Amount__c = 50000000;
lap.Approved_Loan_Amount__c=5000000;
lap.TRansaction_Type__C='PB';
insert lap;
ID parameter = lap.id;

Http_Callout_Mitc.deserializeResponse dR = new Http_Callout_Mitc.deserializeResponse();
dR.result = 'hello';
TestMockRequest req1=new TestMockRequest(200,'Complete',JSON.serialize(dR),null);
Test.setMock(HttpCalloutMock.class,req1);
Test.startTest();
MitcHandler.getMitcDoc(parameter);
Test.stopTest();

  }
}