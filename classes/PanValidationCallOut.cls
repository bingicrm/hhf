public class PanValidationCallOut{
    
    public Customer_Integration__c validatePan(Id loanContactId){
        

        Loan_Contact__c loanContact = [ SELECT id, Pan_Number__c ,Applicant_Type__c,Pan_Verification_status__c, Loan_Applications__c,Loan_Applications__r.Requested_Amount__c , PAN_First_Name__c, Pan_Last_Name__c, PAN_Middle_Name__c
                                        FROM   Loan_Contact__c
                                        WHERE  ID =:loanContactId
                                      ];
                                     
        Rest_Service__mdt restService = [ SELECT MasterLabel, QualifiedApiName , Service_EndPoint__c , 
                                                 Client_Password__c, Client_Username__c, Request_Method__c, Time_Out_Period__c  
                                          FROM   Rest_Service__mdt
                                          WHERE  MasterLabel = 'Pan Card Validated'
                                        ];                               
        
        RequestPanWrapper requestWrap = new RequestPanWrapper();
        requestWrap.ApplicationId = loanContact.Id;
        requestWrap.RequestType = 'REQUEST';
        requestWrap.RequestTime = String.valueOf(System.now());
        requestWrap.PanNumber = loanContact.Pan_Number__c;
        Map<String,String> headerMap = new Map<String,String>();
        Blob headerValue = Blob.valueOf(restService.Client_Username__c + ':' + restService.Client_Password__c);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        headerMap.put('Authorization',authorizationHeader);
        headerMap.put('Username',restService.Client_Username__c);
        headerMap.put('Password',restService.Client_Password__c);
        headerMap.put('Content-Type','application/json');
        
        //String jsonRequest = Json.serialize(requestWrap);
        String jsonRequest = IntegrationFrameWork.returnJSON('Pan Card Validated','Loan_Contact__c',loanContactId);
        HttpResponse res ;
        system.debug('loanContact.Pan_Number__c' + loanContact.Pan_Number__c);
        if( loanContact.Pan_Number__c != null && !string.isblank(loanContact.Pan_Number__c)){
            res   = Utility.sendRequest(restService.Service_EndPoint__c,restService.Request_Method__c, jsonRequest , headerMap, Integer.valueOf(restService.Time_Out_Period__c) );
        } else {
            system.debug('in else pan');
            if (loancontact.Loan_Applications__r.Requested_Amount__c < 1000000 ||
                (loancontact.Loan_Applications__r.Requested_Amount__c >= 1000000 && (loancontact.Applicant_Type__c == 'Co- Applicant' || loancontact.Applicant_Type__c == 'Guarantor') )
                ) {
                system.debug('in if of amount');
                Customer_Integration__c cusInt = new Customer_Integration__c();
                cusInt.Loan_Contact__c = loanContact.Id;
                cusInt.Loan_Application__c = loanContact.Loan_Applications__c;
                cusInt.Pan_Last_Name__c = '';
                cusInt.PAN_First_Name__c = '';
                cusInt.PAN_Middle_Name__c =   '';
                cusInt.PAN_Status__c = '';
                cusInt.PAN_Number__c = '';
                cusInt.Pan_Title__c = '';
                insert cusInt;
                loancontact.Pan_Verification_status__c = true;
				loancontact.PAN_Response__c = true;//Added by Abhilekh on 22nd April 2019
                update loancontact;
                
                system.debug('exit if');
                return cusInt;
            } else {
                res   = Utility.sendRequest(restService.Service_EndPoint__c,restService.Request_Method__c, jsonRequest , headerMap, Integer.valueOf(restService.Time_Out_Period__c) );
        
            }
        }
        system.debug('@res'+res);
        
        ResponsePanWrapper wrap = (ResponsePanWrapper) System.JSON.deserialize(res.getBody(), ResponsePanWrapper.class);
        //string temp = '{"status": "failure","error": {"errorCode": "400","errorType": "Application","errorMessage": "Pan number should not be empty."}}';
        //ResponsePanWrapper wrap = (ResponsePanWrapper) System.JSON.deserialize(temp, ResponsePanWrapper.class);
        
        if(wrap !=null && wrap.TxnStatus  != null && wrap.TxnStatus == 'SUCCESS'){
          
            Customer_Integration__c cusInt = new Customer_Integration__c();
            cusInt.Pan_Last_Name__c = wrap.LastName;
            cusInt.PAN_First_Name__c = wrap.FirstName;
            cusInt.PAN_Middle_Name__c =   wrap.MiddleName;
            cusInt.PAN_Status__c = wrap.PanStatus;
            cusInt.PAN_Number__c = wrap.Pan;
            cusInt.Pan_Title__c = wrap.PanTitle;
			cusInt.Pan_Aadhaar_Seeding_Status__c = wrap.PanAadhaarSeedingStatus;//Added by Saumya for TIL -
            cusInt.Loan_Contact__c = loanContact.Id;
            cusInt.Loan_Application__c = loanContact.Loan_Applications__c;
            insert cusInt;
            //Pan_Last_Name__c
            loanContact.PAN_First_Name__c = cusInt.PAN_First_Name__c;
            loanContact.Validate_PAN_Number__c = cusInt.PAN_Number__c;
            loanContact.PAN_Middle_Name__c = cusInt.PAN_Middle_Name__c;
            loanContact.Pan_Last_Name__c = cusInt.Pan_Last_Name__c;
            loanContact.Pan_Status__c = cusInt.PAN_Status__c;
            loanContact.Pan_Title__c = cusInt.Pan_Title__c;
            loanContact.Pan_Verification_status__c  = true;
			loanContact.PAN_Response__c = true;//Added by Abhilekh on 22nd April 2019
            update loanContact;

            return cusInt;

        }else{
             
                API_Log__c apiLog = new API_Log__c();
                apiLog.Loan_Contact__c  = loanContact.Id;
                apiLog.Request_JSON__c =  jsonRequest; 
                apiLog.Response_JSON__c = res.getBody();
                apiLog.Type_of_Error__c = 'Outbound';
                insert apiLog;
                system.debug('inside else');
            
        } 
        return null;
    }
    
    public class RequestPanWrapper{
        public String ApplicationId;    //10007
        public String RequestType;  //REQUEST
        public String RequestTime;  //23072015 14:22:24
        public String PanNumber;    
    }
    
   public class ResponsePanWrapper{   
        public String ApplicationId;    //
        public String ResponseType; //RESPONSE
        public String RequestReceivedTime;  //
        public String AcknowledgementId;    //
        public String TxnStatus;    //SUCCESS
        public String NsdlStatus;   //SUCCESS 
        public String Pan;  //AAAAA1111A
        public String PanStatus;    //E
		public String PanAadhaarSeedingStatus;	//Y //Added by Saumya for TIL -
        public String LastName; //LASTNM
        public String FirstName;    //FIRSTNM
        public String MiddleName;   //MIDDLENM
        public String PanTitle; //MR
        public String LastUpdateDate;   //03122010
        public String Filler1;  //
        public String Filler2;  //
        public String Filler3;  //               
    }

}