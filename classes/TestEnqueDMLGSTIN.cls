@isTest
private class TestEnqueDMLGSTIN {
    
    static testmethod void test1(){
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Name='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567890',RecordTypeId=RecordTypeIdAccount);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567891',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact cnt2 = new Contact (LastName = 'LNameLoanContact', AccountId = acc1.Id, MobilePhone = '9999667739');
        insert cnt2;
        
        Account acc2 = new Account(PAN__c='ABCDE1234H',Salutation='Mr',FirstName='Suresh',LastName='Nath',
                                   Date_of_Birth__c =Date.newInstance(1990,11,02),
                                   PersonMobilePhone='9234567892',PersonEmail='sn@gmail.com',RecordTypeId=RecordTypeIdAccount1);
        insert acc2;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Test.startTest();
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert objLoanApplication;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Guarantor').getRecordTypeId();
        
		Loan_Contact__c objLCApplicant = [Select id , GST_Initiated__c from Loan_Contact__c where Loan_Applications__c=: objLoanApplication.Id and Applicant_Type__c = 'Applicant'];
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc1.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='2',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='18',
                                                             Customer_segment__c='9',Applicant_Status__c='1',
                                                             GSTIN_Available__c=true, Contact__c = cnt2.Id,
                                                             GSTIN_Number__c = '27AAHFG0551H1ZL',
                                                             income_considered__c = false);
        insert objLoanContact;
        /*
        List<Borrower_Document_Checklist__c> listBCheck = new List<Borrower_Document_Checklist__c>();
        Borrower_Document_Checklist__c objBorrowDocCheck = new Borrower_Document_Checklist__c();
        objBorrowDocCheck.Applicant_Type__c = 'Co- Applicant';
        objBorrowDocCheck.Applicant_Status__c = '1';
        objBorrowDocCheck.Customer_Type__c = '2';
        objBorrowDocCheck.Customer_Segment__c = '3';
        objBorrowDocCheck.Income_program_typeq__c= '';
        objBorrowDocCheck.Constitution__c = '18';
        objBorrowDocCheck.income_considered__c = false;
        objBorrowDocCheck.GST_Status__c = true;
        objBorrowDocCheck.Stage__c = '';
        listBCheck.add(objBorrowDocCheck);
        
        Borrower_Document_Checklist__c objBorrowDocCheck2 = new Borrower_Document_Checklist__c();
        objBorrowDocCheck2.Applicant_Type__c = 'Co- Applicant';
        objBorrowDocCheck2.Applicant_Status__c = '1';
        objBorrowDocCheck2.Customer_Type__c = '2';
        objBorrowDocCheck2.Customer_Segment__c = '';
        objBorrowDocCheck2.Income_program_typeq__c= '';
        objBorrowDocCheck2.Constitution__c = '18';
        objBorrowDocCheck2.income_considered__c = false;
        objBorrowDocCheck2.GST_Status__c = true;
        objBorrowDocCheck2.Stage__c = '';
        listBCheck.add(objBorrowDocCheck2);
        
        insert listBCheck;
       
        List<Loan_Contact__c> lstLoanCon = new List<Loan_Contact__c>();
        lstLoanCon.add(objLoanContact);*/
         Customer_Integration__c cust = new Customer_Integration__c(recordTypeId=Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('GSTIN').getRecordTypeId(), Loan_Application__c = objLoanApplication.Id, Loan_Contact__c=objLCApplicant.Id,
                                                                    Constitution__c = '18',
                                                                    GST_Tax_payer_type__c ='Regular',
                                                                    Current_Status_of_Registration_Under_GST__c ='Cancelled',
                                                                    VAT_Registration_Number__c='123',
                                                                    Email__c='abc@g.com',
                                                                    Mobile__c='9897866756',
                                                                    GSTIN__c='07AEIPC7117E1ZV',
                                                                    PAN_Number__c='AEIPC7117E',
                                                                    Trade_Name__c='AVCF',
                                                                    Legal_Name__c='saert',
                                                                    Filling_Frequency__c='Monthly',
                                                                    Business_Activity__c='Works Contract,Office / Sale Office',
                                                                    GST_Date_Of_Registration__c=Date.valueOf(System.NOW()),
                                                                    Cumulative_Sales_Turnover__c=1500000,
                                                                    Month_Count__c=2);
         insert cust;
        //LoanContactHelper.DocumentChecklist(lstLoanCon);
		/*String strRequest;
		String strResponse;
		String strURL;
		EnqueDMLGSTIN apc = new EnqueDMLGSTIN(cust,strRequest, strResponse, strURL );
        System.enqueueJob(apc);*/
        InitiateGSTController.getCriteriaList(objLoanApplication.Id);
        Test.stopTest();
    } 
	}