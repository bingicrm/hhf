public class UCICDecisionWarningOnCreditController {
    
    @AuraEnabled
    public static UCICWrapper getLAInfo(String laId){
        Loan_Application__c la = [SELECT Id,Name,StageName__c,Sub_Stage__c,Assigned_Credit_Review__c,OwnerId
                                  FROM Loan_Application__c WHERE Id =: laId];
        List<UCIC_Database__c> lstUData = [SELECT Id,Name,Match_Count__c,Recommender_s_Decision__c,Recommender_s_Remarks__c,
                                           Approver_s_Decision__c,Approver_s_Remarks__c,Loan_Application__c
                                           FROM UCIC_Database__c WHERE Loan_Application__c =: laId];
        
        UCICWrapper ucicWrap = new UCICWrapper();
        ucicWrap.currentUserId = UserInfo.getUserId();
        ucicWrap.profileName = [SELECT Id,Name FROM Profile WHERE Id =: UserInfo.getProfileId()].Name;
        ucicWrap.la = la;
        
        for(UCIC_Database__c ud: lstUData){
            UCICDatabase udw = new UCICDatabase();
            udw.UCICDBName = ud.Name;

            if(ud.Recommender_s_Decision__c != ''){
                udw.recommendersDecision = ud.Recommender_s_Decision__c;
            }
            else{
                udw.recommendersDecision = null;
            }
            
            if(ud.Approver_s_Decision__c != ''){
                udw.approversDecision = ud.Approver_s_Decision__c;
            }
            else{
                udw.approversDecision = null;
            }
            ucicWrap.lstUD.add(udw);
        }
        //ucicWrap.lstUD.addAll(lstUD);
        system.debug('ucicWrap::'+ ucicWrap);
        return ucicWrap;
    }
    
    public class UCICWrapper{
        @AuraEnabled public String currentUserId;
        @AuraEnabled public String profileName;
        @AuraEnabled public Loan_Application__c la;
        @AuraEnabled public List<UCICDatabase> lstUD;
        
        public UCICWrapper(){
            currentUserId = '';
            profileName = '';
            la = new Loan_Application__c();
            lstUD = new List<UCICDatabase>();
        }
    }
    
    public class UCICDatabase{
        @AuraEnabled public String recommendersDecision;
        @AuraEnabled public String approversDecision;
        @AuraEnabled public String UCICDBName;
        
        public UCICDatabase(){
            UCICDBName ='';
            recommendersDecision = null;
            approversDecision = null;
        }
    }
}