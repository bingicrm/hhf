public class MoveToOtcClass {
    @AuraEnabled
    public static Loan_Application__c getLoanApp(Id lappId){
        system.debug('=='+lappId);
        return [SELECT Id, Name, StageName__c, Sub_Stage__c, RecordTypeID, Comments__c, Sequence_number__c, Security_interest_id__c
                FROM Loan_Application__c
                WHERE Id = :lappId ];
    }
    @AuraEnabled
    public static MoveNextWrapper movetoOTC(id oppId, string comments) 
    {
                   
        
        loan_application__c la = new loan_application__c();
        
        la = [select Id, Comments_Trail__c,Previous_Sub_Stage__c, StageName__c, Sub_Stage__c, Assigned_scan_DataChecker__c, Comments__c, Sequence_number__c, Security_interest_id__c from loan_application__c where Id =: oppId];
        
        Id otcId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.OTC_Receiving).getRecordTypeId();
        Id disbCheckerId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get(Constants.Disbursement_Checker).getRecordTypeId();
        
        Profile p = [Select Name from Profile where Id =: UserInfo.getProfileId() limit 1];
        String substage;
        
        List<String> status = new List<String>();
        status.add('Lawyer OTC');
        status.add('PDD');
        
        if(p.Name == 'Credit Team'){
            List<Document_Checklist__c> docList = [Select Id from Document_Checklist__c where Loan_Applications__c =: la.Id and Status__c =: status];
            System.debug('===BATMAN==='+docList.size()+' and '+la.Security_interest_id__c+' with '+la.Sequence_number__c);
            if (la.sub_stage__c == constants.Tranche_Initiation && docList.size()>0) {
                la.Sub_Stage__c = Constants.OTC_Receiving;
                la.StageName__c = Constants.Loan_Disbursal;
                la.RecordTypeId = otcId;
                substage = Constants.OTC_Receiving;
            }
            else if(la.sub_stage__c == constants.Tranche_Initiation && docList.size() == 0 && (la.Security_interest_id__c == null || la.Sequence_number__c == null)) {
                la.Sub_Stage__c = Constants.CPC_Data_Maker;
                la.StageName__c = Constants.Loan_Disbursal;
                la.RecordTypeId = disbCheckerId;
                substage = Constants.CPC_Data_Maker;
            }
            else{
                return new MoveNextWrapper(true,'Sorry! This application is not eligible for being moved to OTC Receiving.');
            }
        }
        else{
            return new MoveNextWrapper(true,'You do not have the level of access to perform this operation.');
        }
        
        try 
        {                
            System.debug('Debug Log for Sub Stage'+la.Sub_Stage__c);
            update la;
            
                      
            
            
            //return null;
            return new MoveNextWrapper(false,substage);
            
        } 
        catch (DMLException e) 
        {
            //return e.getDmlMessage(0);
            return new MoveNextWrapper(true,e.getDmlMessage(0));
        } 
        catch (Exception e) 
        {
           return new MoveNextWrapper(true,e.getMessage());
        }    
        
        
    }
    public class MoveNextWrapper
    {
        @AuraEnabled
        public Boolean error;
        @AuraEnabled
        public String msg;

        public MoveNextWrapper(Boolean error, String msg )
        {
            this.error = error;
            this.msg = msg;
        }

    }
    
}