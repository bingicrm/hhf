/*=====================================================================
 * Deloitte India
 * Name: Test_LogArchiver
 * Description: This class is a test class for LogArchiver.
 * Created Date: [01/09/2019]
 * Created By: Gaurav Nawal (Deloitte India)
 *
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/
@isTest
public  class Test_LogArchiver {
	
	@testSetup
    public static void init() {
    	List<Archival_Settings__c> lstArchSettings = new List<Archival_Settings__c> ();
    	lstArchSettings.add(new Archival_Settings__c(Name = 'Default', ALERT__c = true, DEBUG__c = true, ERROR__c = true, INFO__c = true, 
    												INTERVAL__c = -2, WARNING__c = true));
    	insert lstArchSettings;
    }

    public static testMethod void testLogArchiverBatch() {
    	List<DLOG_Object__c> lstDLogRecords = new List<DLOG_Object__c> ();
    	lstDLogRecords.add(new DLOG_Object__c(Severity__c = 'Alert'));
    	lstDLogRecords.add(new DLOG_Object__c(Severity__c = 'Warning'));
    	insert lstDLogRecords;
    	System.assertEquals(2, [SELECT Id FROM DLOG_Object__c].size());
    	Test.startTest();
			LogPurgeScheduler myClass = new LogPurgeScheduler ();   
			String chron = '0 0 23 * * ?';        
			System.Schedule('Test Sched', chron, myClass);
			Database.executeBatch(new LogArchiver());
    	Test.stopTest();
    	System.assertEquals(0, [SELECT Id FROM DLOG_Object__c].size());
    }
}