@isTest
public class TestDocumentViewerCntrl{
    public static testMethod void DocumentViewerCntrl(){
        Id loanConRecTypeId = Schema.SObjectType.Loan_contact__c.getRecordTypeInfosByName().get(Constants.CoApplicant_RECORD).getRecordTypeId();
        
        Account custObj = new Account();
        custObj.Name = 'Test Customer';
        custObj.Phone = '9919999999';
        database.insert(custObj);
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        database.insert(schemeObj);
        
         
        Loan_Application__c loanAppObj = new Loan_Application__c();
        loanAppObj.customer__c = custObj.id;
        loanAppObj.Loan_Application_Number__c = 'LA00001';
        loanAppObj.Loan_Purpose__c = '11';
        loanAppObj.scheme__c = schemeObj.id;
        loanAppObj.Transaction_type__c = 'PB';
        loanAppObj.Requested_Amount__c = 50;
        loanAppObj.StageName__c = 'Customer Onboarding';
        loanAppObj.sub_stage__c = 'Application Initiation';
        Database.insert(loanAppObj);
        
        Third_Party_Verification__c tpvObj = new Third_Party_Verification__c();
        tpvObj.Loan_Application__c = loanAppObj.id;
        tpvObj.Status__c = 'New';
        //Database.insert(tpvObj);     
        
        /*Test.startTest();
        system.debug('docObj : '+[select id,name, Loan_Applications__c, status__c, Loan_Contact__c from document_checklist__c where Loan_Applications__c =: loanAppObj.id limit 1]);
        Document_Checklist__c docObj = [select id,name, Loan_Applications__c, status__c, Loan_Contact__c from document_checklist__c where Loan_Applications__c =: loanAppObj.id limit 1];
        system.assertNotEquals(null,docObj.id);
        Test.stopTest(); */
        
        
        
        /*
        Loan_Contact__c loanConObj = new Loan_Contact__c();
        loanConObj.Loan_Applications__c = loanAppObj.id;
        loanConObj.recordTypeId = loanConRecTypeId;
        Database.insert(loanConObj);
        */
        
        test.startTest();
        loan_contact__c loanConObj = [select id, name, loan_applications__c, recordtypeid from loan_contact__c where loan_Applications__c =: loanAppObj.id];
        system.assertNotEquals(null,loanConObj.id);
        Test.stopTest();
        
        Document_Master__c docMasObj = new Document_Master__c();
        docMasObj.Name = 'Test';
        docMasObj.Doc_Id__c = '123344';
        docMasObj.Approving_authority__c = 'Business Head';
        Database.insert(docMasObj);
        
        Document_type__c docTypeObj = new Document_type__c();
        docTypeObj.Name = 'Test Document Type';
        docTypeObj.Approving_authority__c    = 'Business Head';
        Database.insert(docTypeObj);
        
        DocumentType_Document_Mapping__c mapObj = new DocumentType_Document_Mapping__c();
        mapObj.Document_Master__c = docMasObj.id;
        mapObj.Document_Type__c = docTypeObj.id;
        Database.insert(mapObj);
        
        Document_Checklist__c docObj = new Document_Checklist__c();
        docObj.Loan_Applications__c = loanAppObj.id;
        docObj.status__c = 'Pending';
        docObj.document_type__c = docTypeObj.id;
        docObj.Document_Master__c = docMasObj.id;
        docObj.Loan_Contact__c = loanConObj.id;
        docObj.Express_Queue_Mandatory__c = true;
        docObj.Document_Collection_Mode__c = 'Photocopy';
        docObj.Screened_p__c = 'Yes';
        docObj.Sampled_p__c = 'Yes';
        Database.insert(docObj); 
        
        ContentVersion conVer = new ContentVersion();
        conVer.Title = 'CZDSTOU';
        conVer.PathOnClient = 'Test';
        conVer.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body');
        
        List<contentVersion> conVlist = new List<contentVersion>();
        conVlist.add(conVer);
        database.insert(conVList);
        
        List<contentDocument> documents = [select id, Title, LatestPublishedVersionId from contentDocument];
        contentDocumentLink cdlObj = new contentDocumentLink();
        cdlObj.linkedEntityId = docObj.id;
        cdlObj.shareType = 'I';
        cdlObj.contentDocumentId = documents[0].id;
        cdlObj.Visibility = 'AllUsers';
        Database.insert(cdlObj);
        
        
        //Test.stopTest();
        
        DocumentViewerCntrl.getAttachments(docObj.id);
        //Test.startTest();
        DocumentViewerCntrl.getAttachments(loanAppObj.id);
        //Test.stopTest();
    }
}