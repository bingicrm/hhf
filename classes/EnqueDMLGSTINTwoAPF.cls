public class EnqueDMLGSTINTwoAPF implements Queueable {
    //public Customer_Integration__c objBI;
    public Builder_Integrations__c objBI;
    public Promoter_Integrations__c objPI;
    public String strRequest;
    public String strResponse;
    public String fillingFreq;
    public String strURL;
    public EnqueDMLGSTINTwoAPF(Builder_Integrations__c objBI,Promoter_Integrations__c objPI, String strRequest, String strResponse, String strURL){
        system.debug('Debug for objBI2' + objBI);
        system.debug('Debug for strRequest2' + strRequest);
        system.debug('Debug for strResponse2' + strResponse);
        system.debug('Debug for strURL2' + strURL);
        this.objBI = objBI;
        this.objPI = objPI;
        this.strRequest = strRequest;
        this.strResponse = strResponse;
        this.strURL = strURL;
        this.fillingFreq = fillingFreq;
    }
    public void execute(QueueableContext context) {
        Customer_Integration_For_GSTIN__c passValidationCheck =  Customer_Integration_For_GSTIN__c.getInstance();
      
        
        if(objBI != null && objBI.Project_Builder__c != null){
            system.debug('Debug log for Loan Contact - Success GST');
            Project_Builder__c objPB = [Select id,Success_GST__c from Project_Builder__c where id =: objBI.Project_Builder__c limit 1];
            
            if(objPB != null && objPB.Id != null){
                system.debug('Success GST PREValue' + objPB.Success_GST__c);
                objPB.Success_GST__c = true;
                update objPB; 
                system.debug('Success GST PostValue' + objPB.Success_GST__c);
            }
        }
        
        if(objPI != null && objPI.Promoter__c != null){
            system.debug('Debug log for Loan Contact - Success GST');
            Promoter__c objPromoter = [Select id,Success_GST__c from Promoter__c where id =: objPI.Promoter__c limit 1];
            
            if(objPromoter != null && objPromoter.Id != null){
                system.debug('Success GST PREValue' + objPromoter.Success_GST__c);
                objPromoter.Success_GST__c = true;
                update objPromoter; 
                system.debug('Success GST PostValue' + objPromoter.Success_GST__c);
            }
        }
        
        HttpResponse res = new HttpResponse();
        res.setBody(strResponse);  
        LogUtility.createIntegrationLogs(strRequest,res,strURL);
    }
}