Public Class crossSellTriggerHandler {
    
    public static void beforeUpdate(List<Cross_Sell__c> newList, Map<Id, Cross_Sell__c> oldMap){
        Map<Id, BRE2_Retrigger_Fields__mdt	> mapBRE2Retrigger = new Map<Id,BRE2_Retrigger_Fields__mdt	>([Select Id, Field__c, Object__c, isActive__c from BRE2_Retrigger_Fields__mdt where Object__c = 'Cross_Sell__c' and isActive__c = true]);
        Set<Id> setOfAppIds = new Set<Id>();
        Map<Id,BRE2_Retrigger_Customer_Negotiation__mdt > mapBRE2RetriggerCustNegotiation = new Map<Id, BRE2_Retrigger_Customer_Negotiation__mdt  >([Select Id, Field__c, Object__c, isActive__c from BRE2_Retrigger_Customer_Negotiation__mdt where Object__c = 'Cross_Sell__c' and isActive__c = true]);
        system.debug('mapBRE2Retrigger '+mapBRE2Retrigger);
        system.debug('mapBRE2RetriggerCustNegotiation '+mapBRE2RetriggerCustNegotiation);

        for(Cross_Sell__c crossSell : newList){
            for ( BRE2_Retrigger_Fields__mdt objbreReset : mapBRE2Retrigger.values() ) {
                if(crossSell.reTriggerBRE2__c == false && crossSell.get(objbreReset.Field__c) != oldMap.get(crossSell.id).get(objbreReset.Field__c)) {
                    setOfAppIds.add(crossSell.Loan_Application__c); 
                    system.debug('Setting Financial_Eligibility_Valid__c to false');
                    break;
                }
            }
            
            for ( BRE2_Retrigger_Customer_Negotiation__mdt objbreReset : mapBRE2RetriggerCustNegotiation.values() ) {
                if(crossSell.reTriggerBRE2__c == false &&  crossSell.get(objbreReset.Field__c) != oldMap.get(crossSell.id).get(objbreReset.Field__c)) {
                    setOfAppIds.add(crossSell.Loan_Application__c); 
                    system.debug('Setting Financial_Eligibility_Valid__c to false');
                    break;
                }
            }
        }
        
        system.debug('setOfAppIds '+setOfAppIds);
        Id recordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('BRE2').getRecordTypeId();
        List<Customer_Integration__c> lstOfBRE2SuccessfulCustInt= new List<Customer_Integration__c>();
        if(!setOfAppIds.isEmpty()) {
            lstOfBRE2SuccessfulCustInt = [SELECT Id, Loan_Application__c, BRE2_Success__c from Customer_Integration__c WHERE Loan_Application__c IN: setOfAppIds and BRE2_Recent__c = true and recordTypeId =: recordTypeId ]; 
        }
        Map<Id, Boolean> mapOfBRE2SuccessfulCustInt = new Map<Id, Boolean>();
        if(!lstOfBRE2SuccessfulCustInt.isEmpty()) {
            for(Customer_Integration__c cust : lstOfBRE2SuccessfulCustInt) {
                if(cust.BRE2_Success__c == true) {
                    mapOfBRE2SuccessfulCustInt.put(cust.Loan_Application__c, true);
                } else {
                    mapOfBRE2SuccessfulCustInt.put(cust.Loan_Application__c, false);    
                }
            }
        }
        system.debug('mapOfBRE2SuccessfulCustInt '+mapOfBRE2SuccessfulCustInt);

        for(Cross_Sell__c crossSell : newList) {
            if (mapOfBRE2SuccessfulCustInt != null) {
                if(mapOfBRE2SuccessfulCustInt.containsKey(crossSell.Loan_Application__c)) {
                    if(mapOfBRE2SuccessfulCustInt.get(crossSell.Loan_Application__c)) {
                        crossSell.reTriggerBRE2__c = true;       
                    }
                }
            }
        }    
    }
    public static void afterUpdate(List<Cross_Sell__c> crossSellList, Map<Id, Cross_Sell__c> oldMap){
        Map<Id,BRE2_Retrigger_Fields__mdt> mapBRE2Retrigger = new Map<Id,BRE2_Retrigger_Fields__mdt	>([Select Id, Field__c, Object__c, isActive__c from BRE2_Retrigger_Fields__mdt where Object__c = 'Cross_Sell__c' and isActive__c = true]);
        Set<Id> setOfAppIds = new Set<Id>();
        Map<Id,BRE2_Retrigger_Customer_Negotiation__mdt > mapBRE2RetriggerCustNegotiation = new Map<Id, BRE2_Retrigger_Customer_Negotiation__mdt  >([Select Id, Field__c, Object__c, isActive__c from BRE2_Retrigger_Customer_Negotiation__mdt where Object__c = 'Cross_Sell__c' and isActive__c = true]);
        system.debug('mapBRE2Retrigger '+mapBRE2Retrigger);
        system.debug('mapBRE2RetriggerCustNegotiation '+mapBRE2RetriggerCustNegotiation);
        
        for( Cross_Sell__c crossSell : crossSellList )
        {
            for ( BRE2_Retrigger_Fields__mdt objbreReset : mapBRE2Retrigger.values() ) {
                if(  crossSell.get(objbreReset.Field__c) != oldMap.get(crossSell.id).get(objbreReset.Field__c)) {
                    setOfAppIds.add(crossSell.Loan_Application__c); 
                    system.debug('Setting Financial_Eligibility_Valid__c to false');
                    break;
                }
            }
            
            for ( BRE2_Retrigger_Customer_Negotiation__mdt objbreReset : mapBRE2RetriggerCustNegotiation.values() ) {
                if( crossSell.get(objbreReset.Field__c) != oldMap.get(crossSell.id).get(objbreReset.Field__c)) {
                    setOfAppIds.add(crossSell.Loan_Application__c); 
                    system.debug('Setting Financial_Eligibility_Valid__c to false');
                    break;
                }
            }
        }
        if(!setOfAppIds.isEmpty()) {
            LoanContactHelper.updateBRE2RetriggerOnLA(setOfAppIds);
        }
    }
}