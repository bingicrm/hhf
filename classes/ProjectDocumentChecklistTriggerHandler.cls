public class ProjectDocumentChecklistTriggerHandler {
    public static void onAfterInsert(List<Project_Document_Checklist__c> lstTriggerNew) {
        
    }
    
    public static void onBeforeUpdate(Map<Id,Project_Document_Checklist__c> mapTriggerNew, Map<Id,Project_Document_Checklist__c> mapTriggerOld) {
         /**************************************************************************************************************************************************
            Purpose of Code : 
            (1) To populate Document Received Stage, and Received Date, on Document Checklist Record.
            (2) Validation to avoid marking the document status as Uploaded, if the document is not attached against the document checklist record.
            (3) Validation to avoid marking the document as Received/Pending, if the documents are already attached against the document checklist record.
        ***************************************************************************************************************************************************/
        List<Project_Document_Checklist__c> lstDocUploaded = new List<Project_Document_Checklist__c>(); 
        List<Project_Document_Checklist__c> lstDocPending = new List<Project_Document_Checklist__c>();
        Date dt = LMSDateUtility.lmsDateValue;
        for(Project_Document_Checklist__c objPDC : mapTriggerNew.values()) {
            if(mapTriggerOld.get(objPDC.Id).Status__c == Constants.strDocStatusPending  && mapTriggerOld.get(objPDC.Id).Status__c != objPDC.Status__c && (objPDC.Status__c == Constants.strDocStatusReceived || objPDC.Status__c == Constants.strDocStatusUploaded) && (objPDC.Received_stage__c != null || objPDC.Received_stage__c != '')){ 
                objPDC.Received_stage__c = objPDC.Current_Stage__c;
                objPDC.Business_Date_Received_Updated__c = (Date)dt;
            }
            if(objPDC.Status__c != mapTriggerOld.get(objPDC.Id).Status__c && objPDC.Status__c == Constants.strDocStatusUploaded) {
                lstDocUploaded.add(objPDC);
            }
            if(objPDC.Status__c != mapTriggerOld.get(objPDC.Id).Status__c && (objPDC.Status__c == Constants.strDocStatusPending || objPDC.Status__c == Constants.strDocStatusReceived)) {
                lstDocPending.add(objPDC);
            }
        }
        if(!lstDocUploaded.isEmpty()){
            ProjectDocumentChecklistTriggerHelper.checkAttachmentOnUpload(lstDocUploaded);
        }
        if(!lstDocPending.isEmpty()){
            ProjectDocumentChecklistTriggerHelper.checkAttachmentOnUpload(lstDocPending);
        }
    }
    
    public static void onAfterUpdate(Map<Id,Project_Document_Checklist__c> mapTriggerNew, Map<Id,Project_Document_Checklist__c> mapTriggerOld) {
        
    }
}