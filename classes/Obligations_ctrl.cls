public class Obligations_ctrl {
    @Auraenabled
    public static List<Customer_Obligations__c> GetFinancials(Id conId){
        //Added by Vaishali for BREII-Enhancement
        List<Customer_Integration__c> customerIntegrationList = [Select createdDate, Loan_Contact__c,Id From Customer_Integration__c Where Loan_Contact__c =:conId And CIBIL_Score__c!=null And CIBIL_Score__c!='' And Recordtype.name='CIBIL' order by CreatedDate desc];
        //End of patch- Added by Vaishali for BREII-Enhancement
        
        
        List<Customer_Obligations__c> obligationlist = new List<Customer_Obligations__c>();
        //Customer_Obligations__c obligationRec =  new Customer_Obligations__c(Customer_Integration__c = conId,isVerified__c = false);
        
        //List<Financial_Statement__c> fs1 = [SELECT isVerified__c,Actual_Cash_Profit__c,Adjusted_Networth__c,Administrative_selling_distribution_exp__c,Average_Collection_Period__c,Average_Days_in_Inventory__c,Balance_Sheet_Total_bre__c,Balance_Sheet_Total__c,Business_Income__c,Cash_and_Bank__c,Cash_Profits__c,Cash_Profit_Ratio__c,CreatedById,CreatedDate,Current_Assets__c,Current_Ratio__c,Customer_Integration__c,Debtors_greater_than_6_months__c,Debtors_less_than_6_months__c,Debt_Equity_Ratio__c,Depreciation__c,DSCR_after_the_proposed_Loan__c,DSCR__c,EBIDTA_Total_Operating_Income__c,EBITDA__c,Fixed_Assets_less_depreciation__c,Gross_Profit_cs__c,Gross_Profit__c,Group_Co_Investments__c,Id,Interest_Coverage_Ratio__c,Interest_paid_cs__c,Interest_Paid_on_Term_Loans__c,Interest_Paid_on_Working_Capitals_OD_C__c,Interest_Paid_to_family_member_as_shared__c,Interest_paid__c,Intrst_Exp_rent_paid_to_partner_director__c,Inventories__c,Investments__c,IsDeleted,LastModifiedById,LastModifiedDate,Liquidity_Ratio__c,Liquid_Marketable_Investments__c,Loans_Advances__c,Loan_Advance_given_to_director_partner__c,Manufacturing_expenses__c,Misc_Expense_DRE_Preop_Prelim_Acc_PL__c,Name,Net_Profit_Margin_Ratio__c,Net_Sales__c,NonBusiness_income__c,Non_cash_expenses_written_off__c,Non_Current_Loans_and_Advances__c,Other_Current_Liabilities_Provisions__c,Other_Income__c,Other_Interest_to_Outside_member__c,OwnerId,PAT__c,Profit_After_Tax__c,Profit_Before_Tax_bre__c,Profit_Before_Tax__c,Raw_Material_Cost__c,Receivables_Debtors__c,Reserves_Surplus__c,Salary_to_Partner_Director__c,Share_Capital__c,Short_Term_Loan_Advance_given_to_other__c,SystemModstamp,Tax__c,Term_Loans_from_Banks_FI__c,Total_Borrowings_from_banks_FI_NBFC_s__c,Total_Current_Assets__c,Total_Current_Liabilities__c,Total_Debt_EBITA__c,Total_Debt_Net_Cash_Accruals__c,Total_Income__c,Total_Liabilities_to_outsiders__c,Total_Networth__c,Trade_Creditors__c,Unquoted_Dead_Investments__c,Unsecured_loans_partners_shareholder__c,Wages__c,Working_Capital_Gap__c,Working_Capital_Limits_from_Banks_FI_s__c,Year__c from Financial_Statement__c where Customer_Integration__c =:custInt.Id and Year__c LIKE :('%'+String.valueOf(datetime.now().year())+'%')order by createdDate DESC limit 1];CurrentYe
        
        //Added where filter in this query for BREII-Enhancement and Date_of_Closure__c,Date_of_Loan_Taken__c
        if(!customerIntegrationList.isEmpty()) {
            obligationlist = [SELECT isVerified__c,Id,Remarks__c,Name,Balance_Tenure_MOB__c,Payment_History_End_Date1__c,Payment_History_Start_Date1__c, Months_Onboard__c,MOB__c,Manual__c,Final_Obligations_Considered__c,Loan_Type__c,EMI_Amount__c,Payment_History_Start_Date__c,Payment_History_End_Date__c,Date_of_Loan_Taken__c, Date_of_Closure__c,POS__c,Highest_Sanctioned_Amount__c from Customer_Obligations__c where Customer_Detail__c =:conId  and Customer_Integration__c =: customerIntegrationList[0].Id ];
        }
        List<Customer_Obligations__c> obligationlist1 = new List<Customer_Obligations__c>();
        obligationlist1 = [SELECT isVerified__c,Id,Remarks__c,Name,Balance_Tenure_MOB__c,Payment_History_End_Date1__c,Payment_History_Start_Date1__c, Months_Onboard__c,MOB__c,Manual__c,Final_Obligations_Considered__c,Loan_Type__c,EMI_Amount__c,Payment_History_Start_Date__c,Payment_History_End_Date__c,Date_of_Loan_Taken__c, Date_of_Closure__c,POS__c,Highest_Sanctioned_Amount__c from Customer_Obligations__c where Customer_Detail__c =:conId  and Manual__c= true];
        system.debug('obligationlist'+obligationlist);
        system.debug('obligationlist1'+obligationlist1);
        for(Customer_Obligations__c custObligation : obligationlist1) {
            obligationlist.add(custObligation);    
        }
        system.debug('obligationlist'+obligationlist);
        return obligationlist;
    }
    @Auraenabled
    public static Decimal GetFinancialsTotal(Id conId){
        //List<Customer_Obligations__c> obligationlist = new List<Customer_Obligations__c>();
        //Customer_Obligations__c obligationRec =  new Customer_Obligations__c(Customer_Integration__c = conId,isVerified__c = false);
        Decimal emitotal = 0;
        //List<Financial_Statement__c> fs1 = [SELECT isVerified__c,Actual_Cash_Profit__c,Adjusted_Networth__c,Administrative_selling_distribution_exp__c,Average_Collection_Period__c,Average_Days_in_Inventory__c,Balance_Sheet_Total_bre__c,Balance_Sheet_Total__c,Business_Income__c,Cash_and_Bank__c,Cash_Profits__c,Cash_Profit_Ratio__c,CreatedById,CreatedDate,Current_Assets__c,Current_Ratio__c,Customer_Integration__c,Debtors_greater_than_6_months__c,Debtors_less_than_6_months__c,Debt_Equity_Ratio__c,Depreciation__c,DSCR_after_the_proposed_Loan__c,DSCR__c,EBIDTA_Total_Operating_Income__c,EBITDA__c,Fixed_Assets_less_depreciation__c,Gross_Profit_cs__c,Gross_Profit__c,Group_Co_Investments__c,Id,Interest_Coverage_Ratio__c,Interest_paid_cs__c,Interest_Paid_on_Term_Loans__c,Interest_Paid_on_Working_Capitals_OD_C__c,Interest_Paid_to_family_member_as_shared__c,Interest_paid__c,Intrst_Exp_rent_paid_to_partner_director__c,Inventories__c,Investments__c,IsDeleted,LastModifiedById,LastModifiedDate,Liquidity_Ratio__c,Liquid_Marketable_Investments__c,Loans_Advances__c,Loan_Advance_given_to_director_partner__c,Manufacturing_expenses__c,Misc_Expense_DRE_Preop_Prelim_Acc_PL__c,Name,Net_Profit_Margin_Ratio__c,Net_Sales__c,NonBusiness_income__c,Non_cash_expenses_written_off__c,Non_Current_Loans_and_Advances__c,Other_Current_Liabilities_Provisions__c,Other_Income__c,Other_Interest_to_Outside_member__c,OwnerId,PAT__c,Profit_After_Tax__c,Profit_Before_Tax_bre__c,Profit_Before_Tax__c,Raw_Material_Cost__c,Receivables_Debtors__c,Reserves_Surplus__c,Salary_to_Partner_Director__c,Share_Capital__c,Short_Term_Loan_Advance_given_to_other__c,SystemModstamp,Tax__c,Term_Loans_from_Banks_FI__c,Total_Borrowings_from_banks_FI_NBFC_s__c,Total_Current_Assets__c,Total_Current_Liabilities__c,Total_Debt_EBITA__c,Total_Debt_Net_Cash_Accruals__c,Total_Income__c,Total_Liabilities_to_outsiders__c,Total_Networth__c,Trade_Creditors__c,Unquoted_Dead_Investments__c,Unsecured_loans_partners_shareholder__c,Wages__c,Working_Capital_Gap__c,Working_Capital_Limits_from_Banks_FI_s__c,Year__c from Financial_Statement__c where Customer_Integration__c =:custInt.Id and Year__c LIKE :('%'+String.valueOf(datetime.now().year())+'%')order by createdDate DESC limit 1];CurrentYe
        AggregateResult[] groupedResults  = [SELECT Sum(EMI_Amount__c)aver from Customer_Obligations__c where Customer_Detail__c =:conId];
        
        emitotal = (Decimal)groupedResults[0].get('aver');
        system.debug('emitotal'+emitotal);
        return emitotal;
    }
    
    @Auraenabled
    Public static void SaveFinancials(List<Customer_Obligations__c> Financials,Id conId){
        system.debug('::::::::in Save con::::::::');
        List<Customer_Obligations__c> Allaccwrapperlist  = Financials;
        //List<Customer_Obligations__c> Allaccwrapperlist = (List<Customer_Obligations__c>)JSON.deserialize(Financials,List<Customer_Obligations__c>.class);
        System.debug(Allaccwrapperlist);
        //Allaccwrapperlist[0].Year__c = String.valueOf(datetime.now().year());
        //Allaccwrapperlist[1].Year__c = String.valueOf(datetime.now().year()-1);
        //Allaccwrapperlist[2].Year__c = String.valueOf(datetime.now().year()-2);
        //Allaccwrapperlist[3].Year__c = String.valueOf(datetime.now().year());
        //Allaccwrapperlist[4].Year__c = String.valueOf(datetime.now().year()-1);
        
        if(Allaccwrapperlist.size() >0){
            if(Allaccwrapperlist[0].isVerified__c == true){
                for(Customer_Obligations__c obj:Allaccwrapperlist)
                {
                    obj.isVerified__c = true;
                    obj.Customer_Detail__c=conId;
                }
                update new Loan_Contact__c(id= conId,Obligation_Status__c = 'Completed');
            }
        else{
            update new Loan_Contact__c(id= conId,Obligation_Status__c = 'In Progress');
            
            }
        }
        
        
        system.debug(Allaccwrapperlist); 
        system.debug(Allaccwrapperlist.size()); 
        database.upsert(Allaccwrapperlist);
        
    }
    @Auraenabled
    Public static  List<Customer_Obligations__c>  NewObligation(String Financials,Id conId){
        system.debug('::::::::in Save con::::::::');
        List<Customer_Obligations__c> Allaccwrapperlist = (List<Customer_Obligations__c>)JSON.deserialize(Financials,List<Customer_Obligations__c>.class);
        System.debug(Allaccwrapperlist);
        
        Allaccwrapperlist.add(new Customer_Obligations__c(Customer_Detail__c = conId,isVerified__c = false));
        
        system.debug(Allaccwrapperlist); 
        return Allaccwrapperlist;
        //database.upsert(Allaccwrapperlist);
        
    }
    @Auraenabled
    public static boolean GetIseditable(Id conId){
        //List<Customer_Obligations__c> obligationlist = new List<Customer_Obligations__c>();
        //Customer_Obligations__c obligationRec =  new Customer_Obligations__c(Customer_Integration__c = conId,isVerified__c = false);
        
        //List<Financial_Statement__c> fs1 = [SELECT isVerified__c,Actual_Cash_Profit__c,Adjusted_Networth__c,Administrative_selling_distribution_exp__c,Average_Collection_Period__c,Average_Days_in_Inventory__c,Balance_Sheet_Total_bre__c,Balance_Sheet_Total__c,Business_Income__c,Cash_and_Bank__c,Cash_Profits__c,Cash_Profit_Ratio__c,CreatedById,CreatedDate,Current_Assets__c,Current_Ratio__c,Customer_Integration__c,Debtors_greater_than_6_months__c,Debtors_less_than_6_months__c,Debt_Equity_Ratio__c,Depreciation__c,DSCR_after_the_proposed_Loan__c,DSCR__c,EBIDTA_Total_Operating_Income__c,EBITDA__c,Fixed_Assets_less_depreciation__c,Gross_Profit_cs__c,Gross_Profit__c,Group_Co_Investments__c,Id,Interest_Coverage_Ratio__c,Interest_paid_cs__c,Interest_Paid_on_Term_Loans__c,Interest_Paid_on_Working_Capitals_OD_C__c,Interest_Paid_to_family_member_as_shared__c,Interest_paid__c,Intrst_Exp_rent_paid_to_partner_director__c,Inventories__c,Investments__c,IsDeleted,LastModifiedById,LastModifiedDate,Liquidity_Ratio__c,Liquid_Marketable_Investments__c,Loans_Advances__c,Loan_Advance_given_to_director_partner__c,Manufacturing_expenses__c,Misc_Expense_DRE_Preop_Prelim_Acc_PL__c,Name,Net_Profit_Margin_Ratio__c,Net_Sales__c,NonBusiness_income__c,Non_cash_expenses_written_off__c,Non_Current_Loans_and_Advances__c,Other_Current_Liabilities_Provisions__c,Other_Income__c,Other_Interest_to_Outside_member__c,OwnerId,PAT__c,Profit_After_Tax__c,Profit_Before_Tax_bre__c,Profit_Before_Tax__c,Raw_Material_Cost__c,Receivables_Debtors__c,Reserves_Surplus__c,Salary_to_Partner_Director__c,Share_Capital__c,Short_Term_Loan_Advance_given_to_other__c,SystemModstamp,Tax__c,Term_Loans_from_Banks_FI__c,Total_Borrowings_from_banks_FI_NBFC_s__c,Total_Current_Assets__c,Total_Current_Liabilities__c,Total_Debt_EBITA__c,Total_Debt_Net_Cash_Accruals__c,Total_Income__c,Total_Liabilities_to_outsiders__c,Total_Networth__c,Trade_Creditors__c,Unquoted_Dead_Investments__c,Unsecured_loans_partners_shareholder__c,Wages__c,Working_Capital_Gap__c,Working_Capital_Limits_from_Banks_FI_s__c,Year__c from Financial_Statement__c where Customer_Integration__c =:custInt.Id and Year__c LIKE :('%'+String.valueOf(datetime.now().year())+'%')order by createdDate DESC limit 1];CurrentYe
        Loan_Contact__c custobj = [SELECT Id,Loan_Applications__r.Sub_Stage__c from Loan_Contact__c where id =:conId order by createdDate DESC limit 1];
        system.debug('custobj.Loan_Applications__r.Sub_Stage__c::'+custobj.Loan_Applications__r.Sub_Stage__c);
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        system.debug('ProfileName'+profileName);
        if((Label.Sub_Stage.contains(custobj.Loan_Applications__r.Sub_Stage__c) && Label.User_Profiles.contains(profileName)) || (Label.Credit_Team_Profile.contains(profileName)) || profileName == 'System Administrator')
        
        { 
            return true;//edit mode
        } else {
            return false;//read only mode}   
        }
        
        //system.debug('obligationlist'+obligationlist);
        //return true;
    }
    
    @Auraenabled
    public static Loan_Contact__c GetCustdetails(Id conId){
        Loan_Contact__c custobj = [SELECT Id,Loan_Applications__r.Name,Customer__r.Name,Applicant_Type__c from Loan_Contact__c where id =:conId order by createdDate DESC limit 1];
        system.debug('in get custdetails'+custobj);
        return custobj;
    }
    
}