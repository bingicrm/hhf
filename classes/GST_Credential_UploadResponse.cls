@RestResource(urlMapping = '/GSTCredResponse/*')
global with sharing class GST_Credential_UploadResponse{
    @HttpPost
    global static ResultResponse gstResponseReport(){ 
        GST_Credential_UploadResponseBody container = new GST_Credential_UploadResponseBody();
        
        container = (GST_Credential_UploadResponseBody)System.JSON.deserialize(RestContext.request.requestBody.tostring(),GST_Credential_UploadResponseBody.class);
        
        ResultResponse resRep = new ResultResponse();
        if(container != null){
            String custIntgId;
            List<Monthly_Sales_Turnover__c> mstList = new List<Monthly_Sales_Turnover__c>();
            List<Monthly_Sales_Turnover_Builder__c> mstListBuilder = new List<Monthly_Sales_Turnover_Builder__c>();
            List<Monthly_Sales_Turnover_Promoter__c> mstListPromoter = new List<Monthly_Sales_Turnover_Promoter__c>();
            List<GST_Credential_UploadResponseBody.cls_month_wise_summary> allmstList = new List<GST_Credential_UploadResponseBody.cls_month_wise_summary>();
            
            system.debug('container  ' + container);
            String refId = container.refId;
            if(refId != null){
                system.debug('refId ' + refId);
                custIntgId = refId.substringAfter('-');
                system.debug('custIntgId ' + custIntgId);
            }
            
            if(container.result != null && container.result.current != null && container.result.current.month_wise_summary != null){
                allmstList.addAll(container.result.current.month_wise_summary);
            }
            system.debug('container.result.previous ' + container.result.previous);
            system.debug('container.result.previous.month_wise_summary' + container.result.previous.month_wise_summary);
            if(container.result != null && container.result.previous != null && container.result.previous.month_wise_summary != null){
                allmstList.addAll(container.result.previous.month_wise_summary);
            }
            if(String.isNotBlank(custIntgId)){
                
                String sObjName = Id.valueOf(custIntgId).getSObjectType().getDescribe().getName();
                
                if(sObjName == Constants.strCustomerIntegrationAPI) {
                    List<Customer_Integration__c> custIntegration = [Select id,Status__c,GST_Report_link__c,Business_Date_GSTIN__c,Loan_Contact__c,Filling_Frequency__c from Customer_Integration__c where id =: custIntgId LIMIT 1];
                    Integer BusinessMonth = 0;
                    Integer BusinessYear = 0;
                    Date datobj;
                    if(String.isNotBlank(String.valueOf(custIntegration[0].Business_Date_GSTIN__c))){
                        datobj = custIntegration[0].Business_Date_GSTIN__c;
                        BusinessMonth = datobj.month();
                        BusinessYear = datobj.year();
                    }
                    system.debug('Debug log for datobj ' + custIntegration[0].Business_Date_GSTIN__c);
                    system.debug('Debug log for BusinessMonth  ' + BusinessMonth);
                    system.debug('Debug log for BusinessYear ' + BusinessYear);
                    if(!allmstList.isEmpty()){
                        for(GST_Credential_UploadResponseBody.cls_month_wise_summary obj_monthWiseSummary : allmstList){
                            Monthly_Sales_Turnover__c mst = new Monthly_Sales_Turnover__c();
                            mst.Customer_Integration__c = custIntgId;
                            system.debug('obj_monthWiseSummary.ret_period '+ obj_monthWiseSummary.ret_period);
                            String returnPeriod = obj_monthWiseSummary.ret_period;
                            String month = returnPeriod.substring(0,2);
                            String Year = returnPeriod.substring(2,6);
                            if( BusinessMonth != 0 && BusinessYear != 0 && String.isNotBlank(Year) && String.isNotBlank(month)){
                                system.debug('Debug log for Business Date ' + BusinessMonth + ' ' + BusinessYear);
                                system.debug('Debug log for MST Year and Month' + month + ' ' + Year);
                                if((Integer.valueOf(Year) == BusinessYear && Integer.valueOf(month) <= (BusinessMonth-2)) ||
                                   (Integer.valueOf(Year) == (BusinessYear - 1) && Integer.valueOf(month) >= 4)
                                  ){
                                      mst.Month__c = month;
                                      mst.Year__c  = Year;
                                      mst.Turnover__c = obj_monthWiseSummary.gstr3b.ttl_val;
                                      system.debug('mst.Month__c  ' + mst.Month__c);
                                      system.debug('mst.Year__c  ' + mst.Year__c);
                                      mstList.add(mst);
                                  }
                                
                            }
                            //mst.Month__c = returnPeriod.substring(0,2);
                            // mst.Year__c = returnPeriod.substring(2,6);
                            // mst.Turnover__c = obj_monthWiseSummary.gstr3b.ttl_val;
                            
                        } 
                    }
                    
                    
                    if(container.result != null && !mstList.isEmpty()){
                        if(container.result.pdfDownloadLink != null){
                           custIntegration[0].GST_Report_link__c = container.result.pdfDownloadLink; 
                        }
                        if(custIntegration[0].Filling_Frequency__c != null){
                            custIntegration[0].Status__c = 'Completed';
                        }
                        if( container.result.profile != null && container.result.profile.vintage != null && container.result.profile.vintage.registration_no != null){
                            custIntegration[0].VAT_Registration_Number__c = container.result.profile.vintage.registration_no;
                        }
                        update custIntegration[0];
                        if(custIntegration[0].Status__c == 'Completed'){
                            Id loanContactId = custIntegration[0].Loan_Contact__c;
                            List<Document_Checklist__c> dcListToUpdate = new List<Document_Checklist__c>();
                            if(loanContactId != null){
                                List<Document_Checklist__c> gstDocs = [Select id,Status__c,Data_Entry_Required__c,Scan_Check_Completed__c,Document_Master__r.Name from Document_Checklist__c where Loan_Contact__c = : loanContactId and Document_Master__r.Name like 'GST%'];
                                system.debug('gstDocs Size '+ gstDocs.size());
                                if(!gstDocs.isEmpty()){
                                    for(Document_Checklist__c dc : gstDocs){
                                        if(dc.Status__c == 'Pending'){
                                            dc.Data_Entry_Required__c = false;
                                            dc.Status__c = 'Waived Off';
                                            dc.Scan_Check_Completed__c = true;
                                            dcListToUpdate.add(dc);
                                        }
                                        
                                    }
                                    system.debug('dcListToUpdate '+ dcListToUpdate);
                                    if(!dcListToUpdate.isEmpty()){
                                        update dcListToUpdate;
                                    }
                                }
                                
                            }
                        }
                    }
                    system.debug('Debug log for mstList size ' + mstList.size());
                    if(!mstList.isEmpty()){
                        Database.SaveResult[] results = Database.insert(mstList , false);
                        system.debug('result ' + results.size());
                        Boolean success = true;
                        for(Database.SaveResult result : results){
                            if(!result.getErrors().isEmpty()){
                                resRep.ErrorMsg += result.getErrors();
                                system.debug('result.getErrors()  ' + result.getErrors());
                                success = false;
                            }
                        }
                        system.debug('Result' + success);
                        if(success){
                            resRep.Result = 'Success!!';
                            
                        }
                        else{
                            resRep.Result = 'Failed!!';
                        }
                    }
                }
                
                else if(sObjName == Constants.strBuilderIntegrationAPI) {
                    
                    List<Builder_Integrations__c> custIntegration = [Select id,Status__c,GST_Report_link__c,Project_Builder__c,Filling_Frequency__c from Builder_Integrations__c where id =: custIntgId LIMIT 1];
                    Integer BusinessMonth = 0;
                    Integer BusinessYear = 0;
                    Date datobj;
                    
					if(test.isRunningTest()){
                    BusinessMonth =12;
                    BusinessYear = 2018  ;  
                    }
                    if(!allmstList.isEmpty()){
                        for(GST_Credential_UploadResponseBody.cls_month_wise_summary obj_monthWiseSummary : allmstList){
                            Monthly_Sales_Turnover_Builder__c mst = new Monthly_Sales_Turnover_Builder__c();
                            mst.Builder_Integration__c = custIntgId;
                            system.debug('obj_monthWiseSummary.ret_period '+ obj_monthWiseSummary.ret_period);
                            String returnPeriod = obj_monthWiseSummary.ret_period;
                            String month = returnPeriod.substring(0,2);
                            String Year = returnPeriod.substring(2,6);
                            if( BusinessMonth != 0 && BusinessYear != 0 && String.isNotBlank(Year) && String.isNotBlank(month)){
                                system.debug('Debug log for Business Date ' + BusinessMonth + ' ' + BusinessYear);
                                system.debug('Debug log for MST Year and Month' + month + ' ' + Year);
                                if((Integer.valueOf(Year) == BusinessYear && Integer.valueOf(month) <= (BusinessMonth-2)) ||
                                   (Integer.valueOf(Year) == (BusinessYear - 1) && Integer.valueOf(month) >= 4)
                                  ){
                                      mst.Month__c = month;
                                      mst.Year__c  = Year;
                                      mst.Turnover__c = obj_monthWiseSummary.gstr3b.ttl_val;
                                      system.debug('mst.Month__c  ' + mst.Month__c);
                                      system.debug('mst.Year__c  ' + mst.Year__c);
                                      mstListBuilder.add(mst);
                                  }
                                
                            }
                            //mst.Month__c = returnPeriod.substring(0,2);
                            // mst.Year__c = returnPeriod.substring(2,6);
                            // mst.Turnover__c = obj_monthWiseSummary.gstr3b.ttl_val;
                            
                        } 
                    }
                    
                    
                    if(container.result != null && !mstList.isEmpty()){
                        if(container.result.pdfDownloadLink != null){
                           custIntegration[0].GST_Report_link__c = container.result.pdfDownloadLink; 
                        }
                        if(custIntegration[0].Filling_Frequency__c != null){
                            custIntegration[0].Status__c = 'Completed';
                        }
                        if( container.result.profile != null && container.result.profile.vintage != null && container.result.profile.vintage.registration_no != null){
                            custIntegration[0].VAT_Registration_Number__c = container.result.profile.vintage.registration_no;
                        }
                        update custIntegration[0];
                        if(custIntegration[0].Status__c == 'Completed'){
                            Id loanContactId = custIntegration[0].Project_Builder__c;
                            List<Project_Document_Checklist__c> dcListToUpdate = new List<Project_Document_Checklist__c>();
                            if(loanContactId != null){
                                List<Project_Document_Checklist__c> gstDocs = [Select id,Status__c,Scan_Check_Completed__c,Document_Master__r.Name from Project_Document_Checklist__c where Project_Builder__c = : loanContactId and Document_Master__r.Name like 'GST%'];
                                system.debug('gstDocs Size '+ gstDocs.size());
                                if(!gstDocs.isEmpty()){
                                    for(Project_Document_Checklist__c dc : gstDocs){
                                        if(dc.Status__c == 'Pending'){
                                            
                                            dc.Status__c = 'Waived Off';
                                            dc.Scan_Check_Completed__c = true;
                                            dcListToUpdate.add(dc);
                                        }
                                        
                                    }
                                    system.debug('dcListToUpdate '+ dcListToUpdate);
                                    if(!dcListToUpdate.isEmpty()){
                                        update dcListToUpdate;
                                    }
                                }
                                
                            }
                        }
                    }
                    system.debug('Debug log for mstListBuilder size ' + mstListBuilder.size());
                    if(!mstListBuilder.isEmpty()){
                        Database.SaveResult[] results = Database.insert(mstListBuilder , false);
                        system.debug('result ' + results.size());
                        Boolean success = true;
                        for(Database.SaveResult result : results){
                            if(!result.getErrors().isEmpty()){
                                resRep.ErrorMsg += result.getErrors();
                                system.debug('result.getErrors()  ' + result.getErrors());
                                success = false;
                            }
                        }
                        system.debug('Result' + success);
                        if(success){
                            resRep.Result = 'Success!!';
                            
                        }
                        else{
                            resRep.Result = 'Failed!!';
                        }
                    }
                
                }
                
                else if(sObjName == Constants.strPromoterIntegrationAPI) {
                    
                    List<Promoter_Integrations__c> custIntegration = [Select id,Status__c,GST_Report_link__c,Promoter__c,Filling_Frequency__c from Promoter_Integrations__c where id =: custIntgId LIMIT 1];
                    Integer BusinessMonth = 0;
                    Integer BusinessYear = 0;
                    Date datobj;
                    
					if(test.isRunningTest()){
                    BusinessYear = 2018;
                    BusinessMonth = 06  ;  
                    }
                    if(!allmstList.isEmpty()){
                        for(GST_Credential_UploadResponseBody.cls_month_wise_summary obj_monthWiseSummary : allmstList){
                            Monthly_Sales_Turnover_Promoter__c mst = new Monthly_Sales_Turnover_Promoter__c();
                            mst.Promoter_Integration__c = custIntgId;
                            system.debug('obj_monthWiseSummary.ret_period '+ obj_monthWiseSummary.ret_period);
                            String returnPeriod = obj_monthWiseSummary.ret_period;
                            String month = returnPeriod.substring(0,2);
                            String Year = returnPeriod.substring(2,6);
                            if( BusinessMonth != 0 && BusinessYear != 0 && String.isNotBlank(Year) && String.isNotBlank(month)){
                                system.debug('Debug log for Business Date ' + BusinessMonth + ' ' + BusinessYear);
                                system.debug('Debug log for MST Year and Month' + month + ' ' + Year);
                                if((Integer.valueOf(Year) == BusinessYear && Integer.valueOf(month) <= (BusinessMonth-2)) ||
                                   (Integer.valueOf(Year) == (BusinessYear - 1) && Integer.valueOf(month) >= 4)
                                  ){
                                      mst.Month__c = month;
                                      mst.Year__c  = Year;
                                      mst.Turnover__c = obj_monthWiseSummary.gstr3b.ttl_val;
                                      system.debug('mst.Month__c  ' + mst.Month__c);
                                      system.debug('mst.Year__c  ' + mst.Year__c);
                                      mstListPromoter.add(mst);
                                  }
                                
                            }
                            //mst.Month__c = returnPeriod.substring(0,2);
                            // mst.Year__c = returnPeriod.substring(2,6);
                            // mst.Turnover__c = obj_monthWiseSummary.gstr3b.ttl_val;
                            
                        } 
                    }
                    
                    
                    if(container.result != null && !mstListPromoter.isEmpty()){
                        if(container.result.pdfDownloadLink != null){
                           custIntegration[0].GST_Report_link__c = container.result.pdfDownloadLink; 
                        }
                        if(custIntegration[0].Filling_Frequency__c != null){
                            custIntegration[0].Status__c = 'Completed';
                        }
                        if( container.result.profile != null && container.result.profile.vintage != null && container.result.profile.vintage.registration_no != null){
                            custIntegration[0].VAT_Registration_Number__c = container.result.profile.vintage.registration_no;
                        }
                        update custIntegration[0];
                        if(custIntegration[0].Status__c == 'Completed'){
                            Id loanContactId = custIntegration[0].Promoter__c;
                            
                        }
                    }
                    system.debug('Debug log for mstListPromoter size ' + mstListPromoter.size());
                    if(!mstListPromoter.isEmpty()){
                        Database.SaveResult[] results = Database.insert(mstListPromoter , false);
                        system.debug('result ' + results.size());
                        Boolean success = true;
                        for(Database.SaveResult result : results){
                            if(!result.getErrors().isEmpty()){
                                resRep.ErrorMsg += result.getErrors();
                                system.debug('result.getErrors()  ' + result.getErrors());
                                success = false;
                            }
                        }
                        system.debug('Result' + success);
                        if(success){
                            resRep.Result = 'Success!!';
                            
                        }
                        else{
                            resRep.Result = 'Failed!!';
                        }
                    }
                
                }
            }
            
        }
        HttpResponse res = new HttpResponse();
        res.setBody(String.valueOf(resRep)); 
        LogUtility.createIntegrationLogs(RestContext.request.requestBody.tostring(),res,'GST Callback Response');   
        return resRep;
        
    }
    
    
    global class ResultResponse{
        String ErrorMsg;
        String Result;
        public ResultResponse(){
            ErrorMsg = '{}';
            Result = '';
        }
    }
}