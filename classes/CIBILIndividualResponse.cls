@RestResource(urlMapping='/cibil/individual/*')
global with sharing class CIBILIndividualResponse  {

    @HttpPost
    global static void createCIBILReport( CibilRequest cibil ) 
    {    
        if( cibil != null && String.isNotEmpty(cibil.parentId))
        {
				String sObjName = Id.valueOf(cibil.parentId).getSObjectType().getDescribe().getName();
				String strJSONrequest = JSON.serializePretty(cibil);
                system.debug(strJSONRequest);
				
				if(sObjName == Constants.strCustomerIntegrationAPI) {
					// Modified this query by Vaishali for BRE - {to include BRE_1_Message__c fields}
					Customer_Integration__c ci = [Select Id,CIBIL_Score__c,I_Cibil_Highest_Sanctioned_Amount__c,I_Cibil_Current_Balance__c,I_Cibil_Total_Accounts__c,I_Cibil_No_of_Zero_Balance_Accounts__c,
														 I_Cibil_No_of_enquiries_in_past_30_days__c,I_Cibil_No_of_enquiries_in_past_12_mon__c,I_Cibil_No_of_enquiries_in_past_24_mon__c,I_Cibil_Recent_Enquiry_Date__c, Loan_Contact__c, BRE_1_Message__c
														from Customer_Integration__c where Id = : cibil.parentId];
					//Added Obligation_Status__c in this query for BREII-Enhancement
					Loan_Contact__c lc = [SELECT Id, Name, Obligation_Status__c,CIBIL_CheckUp__c,CIBIL_Verified__c,Customer__c FROM Loan_Contact__c WHERE Id =: ci.Loan_Contact__c]; //Customer__c added by Abhilekh on 10th May 2019 for TIL-736

					if( ci != null )
					{

						ci.CIBIL_Score__c = cibil.cibilScore;
						ci.I_Cibil_Highest_Sanctioned_Amount__c = cibil.creditSanctionAmount;
						ci.I_Cibil_Current_Balance__c = cibil.currentBalance;
						ci.I_Cibil_Total_Accounts__c = cibil.accountZeroBalance;
						ci.I_Cibil_No_of_Zero_Balance_Accounts__c = cibil.accountZeroBalance;
						ci.I_Cibil_Total_Enquiries__c =  cibil.totalEnquiryCount;
						ci.I_Cibil_No_of_enquiries_in_past_30_days__c = cibil.totalEnquiryCountX30D;
						ci.I_Cibil_No_of_enquiries_in_past_12_mon__c = cibil.totalEnquiryCountX1Y;
						ci.I_Cibil_No_of_enquiries_in_past_24_mon__c = cibil.totalEnquiryCount2Y;
						ci.I_Cibil_Recent_Enquiry_Date__c = Date.valueOf(cibil.recentEnquiryDate);
						ci.CIBIL_Message__c = cibil.errorMsg;
						ci.BRE_1_Message__c = cibil.bre1Message; //Added by Vaishali for BRE
						update ci;
					}
					if(lc != null){
						lc.CIBIL_CheckUp__c = true;
						lc.CIBIL_Verified__c = true;
						//Added by Vaishali for BREII-Enhancement
                        if(lc.Obligation_Status__c == 'Completed') {
                            lc.Obligation_Status__c = 'In Progress';
                        }
                        //End of patch- Added by Vaishali for BREII-Enhancement
						lc.Retrigger_Individual_CIBIL__c = false; //Added by Abhilekh on 9th May 2019 for TIL-736
						update lc;
					}
					
					//Below List and if block added by Abhilekh on 10th May 2019 for TIL-736
					List<Address__c> lstAddress = [Select Id,Retrigger_Individual_CIBIL__c from Address__c where Loan_Contact__c = :lc.id];
					if( !lstAddress.isEmpty() ) {
						for ( Address__c objAdd : lstAddress ) {
							objAdd.Retrigger_Individual_CIBIL__c = false;
						}
						update lstAddress;
					}
					
					//Below List and if block added by Abhilekh on 10th May 2019 for TIL-736
					List<Account> lstAcc = [Select Id,Retrigger_Individual_CIBIL__c from Account where id = :lc.Customer__c];
					if ( !lstAcc.isEmpty() ) {
						lstAcc[0].Retrigger_Individual_CIBIL__c = false;
						update lstAcc;
					}
					
					if( cibil.Obligations.size() > 0 )
					{
						List<Customer_Obligations__c> allObligations  = new List<Customer_Obligations__c>();
						
						//Added by Vaishali on 11-03-2020
						Set<Id> setOfCustIntegrationIds = new Set<Id>();
						Map<Id, Customer_Integration__c> mapOfCustIntegrations;
						for( Obligations obil : cibil.Obligations)
						{
							setOfCustIntegrationIds.add(cibil.parentId);
						}
						if(!setOfCustIntegrationIds.isEmpty()) {
							mapOfCustIntegrations = new Map<Id, Customer_Integration__c>([SELECT Id,Loan_Contact__c from Customer_Integration__c WHERE ID IN: setOfCustIntegrationIds]);
						}
						//End of patch- Added by Vaishali on 11-03-2020
						
						for( Obligations obil : cibil.Obligations)
						{
							Customer_Obligations__c temp = getObligations(obil,cibil.parentId, mapOfCustIntegrations);  //Parameter mapOfCustIntegrations added by Vaishali
							allObligations.add(temp);
						}

						if( allObligations.size() > 0 )
						{
							insert allObligations;
						}

					}
					//**************************************Added by Vaishali for BRE
					 //[08-11-2019] Null check added by Mehul for test class fix
					if( cibil.enquiryList != null && cibil.enquiryList.size() > 0 )
					{
						 List<Enquiry__c> allenquiryList  = new List<Enquiry__c>();
						for( enquiryList enq : cibil.enquiryList)
						{
							Enquiry__c temp = getenquiryList(enq, cibil.parentId);
							allenquiryList.add(temp);
						}

						if( allenquiryList.size() > 0 )
						{
							insert allenquiryList;
						}
					}
					
					//[08-11-2019] Null check added by Mehul for test class fix
					if( cibil.employmentList !=null && cibil.employmentList.size() > 0 )
					{
						List<Employment_Detail__c> allemploymentList  = new List<Employment_Detail__c>();
						for( employmentList emp : cibil.employmentList)
						{
							Employment_Detail__c temp = getemploymentList(emp, cibil.parentId);
							allemploymentList.add(temp);
						}

						if( allemploymentList.size() > 0 )
						{
							insert allemploymentList;
						}
					}
					
					if( cibil.addressList !=null && cibil.addressList.size() > 0 )
					{
						List<Bureau_address__c> alladdressList  = new List<Bureau_address__c>();
						
						//Added by Vaishali on 11-03-2020
						Map<String, addressList> mapOfAddressPincodesWithIds = new Map<String,addressList>();
						List<Pincode__c> lstofPinCodes;
						 Map<addressList, Id> mapOfAddressidWithPincodeIds = new Map<addressList,Id>();
					
						for( addressList add : cibil.addressList)
						{
							mapOfAddressPincodesWithIds.put(add.pinCode, add);   
						}
						if(!mapOfAddressPincodesWithIds.isEmpty()) {
							lstofPinCodes = [SELECT Id , Name FROM Pincode__c WHERE Name IN: mapOfAddressPincodesWithIds.keySet()];
							if(!lstofPinCodes.isEmpty()) {
								for(Pincode__c pc : lstofPinCodes) {
									if(mapOfAddressPincodesWithIds.containsKey(pc.Name)) {
										mapOfAddressidWithPincodeIds.put(mapOfAddressPincodesWithIds.get(pc.Name),pc.id);
									}
								}
							}
						}
						//End of patch- Added by Vaishali on 11-03-2020


						for( addressList add : cibil.addressList)
						{
							Bureau_address__c temp = getaddressList(add, cibil.parentId, mapOfAddressidWithPincodeIds); // Parameter added by Vaishali on 11-03-2020
							alladdressList.add(temp);
						}

						if( alladdressList.size() > 0 )
						{
							insert alladdressList;
						}
					}
					
					if( cibil.idList !=null && cibil.idList.size() > 0 )
					{
						List<ID_Detail__c> allidList  = new List<ID_Detail__c>();
						for( idList iddet : cibil.idList)
						{
							ID_Detail__c temp = getidList(iddet, cibil.parentId);
							allidList.add(temp);
						}

						if( allidList.size() > 0 )
						{
							insert allidList;
						}
					}
					//**************************************End of the block- added by vaishali for BRE
					

					CIBILResponse res = new CIBILResponse ();
					res.status = 'Success';
					res.errorMsg = '';
					try{
						RestContext.response.responseBody = Blob.valueOf(JSON.serialize(res));
					}catch(Exception e){
						system.debug('@@error' + e);
					}
				}
				
				else if(sObjName == Constants.strBuilderIntegrationAPI) {
                Builder_Integrations__c ci = [Select Id,CIBIL_Score__c,I_Cibil_Highest_Sanctioned_Amount__c, I_Cibil_Total_Enquiries__c, I_Cibil_Current_Balance__c,I_Cibil_Total_Accounts__c,I_Cibil_No_of_Zero_Balance_Accounts__c,
                                                         I_Cibil_No_of_enquiries_in_past_30_days__c,I_Cibil_No_of_enquiries_in_past_12_mon__c,I_Cibil_No_of_enquiries_in_past_24_mon__c,I_Cibil_Recent_Enquiry_Date__c, Project_Builder__c
                                                        from Builder_Integrations__c where Id = : cibil.parentId];
                Project_Builder__c lc = [SELECT Id, Name, Retrigger_Individual_CIBIL__c, CIBIL_CheckUp__c,CIBIL_Verified__c,Builder_Name__c FROM Project_Builder__c WHERE Id =: ci.Project_Builder__c];
                
                if( ci != null )
                {

                    ci.CIBIL_Score__c = cibil.cibilScore;
                    ci.I_Cibil_Highest_Sanctioned_Amount__c = cibil.creditSanctionAmount;
                    ci.I_Cibil_Current_Balance__c = cibil.currentBalance;
                    ci.I_Cibil_Total_Accounts__c = cibil.accountZeroBalance;
                    ci.I_Cibil_No_of_Zero_Balance_Accounts__c = cibil.accountZeroBalance;
                    ci.I_Cibil_Total_Enquiries__c =  cibil.totalEnquiryCount;
                    ci.I_Cibil_No_of_enquiries_in_past_30_days__c = cibil.totalEnquiryCountX30D;
                    ci.I_Cibil_No_of_enquiries_in_past_12_mon__c = cibil.totalEnquiryCountX1Y;
                    ci.I_Cibil_No_of_enquiries_in_past_24_mon__c = cibil.totalEnquiryCount2Y;
                    ci.I_Cibil_Recent_Enquiry_Date__c = Date.valueOf(cibil.recentEnquiryDate);
                    ci.CIBIL_Message__c = cibil.errorMsg;
                    update ci;
                }
                
                if(lc != null){
                    system.debug('CIBIL Checks');
                    lc.CIBIL_CheckUp__c = true;
                    lc.CIBIL_Verified__c = true;
                    lc.Retrigger_Individual_CIBIL__c = false; //Added by Abhilekh on 9th May 2019 for TIL-736
                    update lc;
                    system.debug('lc ' + lc);
                }
                
                CIBILResponse res = new CIBILResponse ();
                    res.status = 'Success';
                    res.errorMsg = '';
                try{
                    RestContext.response.responseBody = Blob.valueOf(JSON.serialize(res));
                }catch(Exception e){
                    system.debug('@@error' + e);
                }
            }
                
            else if(sObjName == Constants.strPromoterIntegrationAPI) {
                
                Promoter_Integrations__c ci = [Select Id,CIBIL_Score__c, CIBIL_Message__c, I_Cibil_Highest_Sanctioned_Amount__c, I_Cibil_Total_Enquiries__c, I_Cibil_Current_Balance__c,I_Cibil_Total_Accounts__c,I_Cibil_No_of_Zero_Balance_Accounts__c,
                                                         I_Cibil_No_of_enquiries_in_past_30_days__c,I_Cibil_No_of_enquiries_in_past_12_mon__c,I_Cibil_No_of_enquiries_in_past_24_mon__c,I_Cibil_Recent_Enquiry_Date__c, Promoter__c
                                                        from Promoter_Integrations__c where Id = : cibil.parentId];
                Promoter__c lc = [SELECT Id, Name, Retrigger_Individual_CIBIL__c, CIBIL_CheckUp__c,CIBIL_Verified__c FROM Promoter__c WHERE Id =: ci.Promoter__c];
                
                if( ci != null )
                {

                    ci.CIBIL_Score__c = cibil.cibilScore;
                    ci.I_Cibil_Highest_Sanctioned_Amount__c = cibil.creditSanctionAmount;
                    ci.I_Cibil_Current_Balance__c = cibil.currentBalance;
                    ci.I_Cibil_Total_Accounts__c = cibil.accountZeroBalance;
                    ci.I_Cibil_No_of_Zero_Balance_Accounts__c = cibil.accountZeroBalance;
                    ci.I_Cibil_Total_Enquiries__c =  cibil.totalEnquiryCount;
                    ci.I_Cibil_No_of_enquiries_in_past_30_days__c = cibil.totalEnquiryCountX30D;
                    ci.I_Cibil_No_of_enquiries_in_past_12_mon__c = cibil.totalEnquiryCountX1Y;
                    ci.I_Cibil_No_of_enquiries_in_past_24_mon__c = cibil.totalEnquiryCount2Y;
                    ci.I_Cibil_Recent_Enquiry_Date__c = Date.valueOf(cibil.recentEnquiryDate);
                    ci.CIBIL_Message__c = cibil.errorMsg;
                    update ci;
                }
                
                if(lc != null){
                    system.debug('CIBIL Checks');
                    lc.CIBIL_CheckUp__c = true;
                    lc.CIBIL_Verified__c = true;
                    lc.Retrigger_Individual_CIBIL__c = false; //Added by Abhilekh on 9th May 2019 for TIL-736
                    update lc;
                    system.debug('lc ' + lc);
                }
                
                CIBILResponse res = new CIBILResponse ();
                    res.status = 'Success';
                    res.errorMsg = '';
                try{
                    RestContext.response.responseBody = Blob.valueOf(JSON.serialize(res));
                }catch(Exception e){
                    system.debug('@@error' + e);
                }
            
            }
                
        }
        else
        {
            CIBILResponse res = new CIBILResponse ();
            res.status = 'Error';
            res.errorMsg = 'Invalid Request.';
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(res));
        }
        
    }


    global class CibilRequest
    {
        public String parentId;
        public String cibilScore;
        public Decimal creditSanctionAmount;
        public Date recentOpenedDate;
        public Date oldestOpenedDate;
        public Decimal currentBalance;
        public Integer totalAccount;
        public Integer accountZeroBalance;
        public Integer accountWithOverdue;
        public Integer totalEnquiryCount;
        public Integer totalEnquiryCountX30D;
        public Integer totalEnquiryCountX1Y;
        public Integer totalEnquiryCount2Y;
        public Date recentEnquiryDate;
        public Boolean panMatch;
        public Boolean mobileMatch;
        public Boolean addressMatch;
        public String bureauName;
        public String errorMsg;
		public String bre1Message;      // Added by Vaishali for BRE
        public Obligations[] Obligations;
		//*********************************Added by Vaishali for BRE
        public enquiryList[] enquiryList;
        public employmentList[] employmentList;
        public addressList[] addressList;
        public idList[] idList;
        //*********************************End of the block- Added by Vaishali for BRE
    }

    global class Obligations
    {
        public string loanType;
        public string currentStatus;
        public string writeOffFlag;
        public Decimal emiAmount;
        public Integer repaymentTenure;
        public Decimal balance;
        public Integer countDPD3Months;
        public Integer maxPeakDPD3Months;
        public Integer countDPD12Months;
        public Integer maxPeakDPD12Months;
		//**************************************Added by Vaishali for BRE
        public String accountType;
        public String accountNumber;
        public Double amountOverdue;
        public Double currentBalance;
        public String dateClosed;
        public String dateOfLastPayment;
        public String dateOpenedOrDisbursed;
        public String dateReportedandCertified;
        public String reportingMemberShortName;
        public Double highCreditOrSanctionedAmount;
        public String ownershipIndicator;
        public Double paymentFrequency;
        public String paymentHistory1;
        public String paymentHistory2;
        public String paymentHistoryEndDate;
        public String paymentHistoryStartDate;
        public Double rateOfInterest;
        public Double settlementAmount;
        public String suitFiledorWilfulDefault;
        public String typeOfCollateral;
        public Double valueOfCollateral;
        public Double writtenOffAmountPrincipal;
        public String writtenOffAndSettledStatus;
        public Double creditLimit;
        public Double cashLimit;
        public Double writtenOffAmountTotal;
        //****************************************End of the block- Added by Vaishali for BRE

    }
	
	//*********************************************Added by Vaishali for BRE
    global class enquiryList {
        public String dateReported;
        public String enquiryPurpose;
        public Double enquiryAmount;
        public String reportingMemberShortName;
    }
    
    global class employmentList{
        public String accountType;
        public Double income;
        public String occupationCode;
        public String netGrossIndicator;
        public String monthlyAnnuallyIndicator;
        public String dateReported;
    }
    
    global class addressList{
        public String addressCategory;
        public String residenceCode;
        public String addressLine1;
        public String addressLine2;
        public String addressLine3;
        public String addressLine4;
        public String addressLine5;
        public String stateCode;
        public String pinCode;
        public String dateReported;
        public String enrichedThroughtEnquiry;
    }
    
    global class idList{
        public String IdType;
        public String IdValue;
        public String issueDate;
        public String expirationDate;
        public String enrichedThroughtEnquiry;
    }
    //********************************************End of the block- Added by Vaishali for BRE

    global class CIBILResponse 
    {
        public String status ;
        public String errorMsg ;
        
    }

    public static Customer_Obligations__c getObligations( Obligations obil, String parentId, Map<Id, Customer_Integration__c> mapOfCustIntegrations) //Parameter added by Vaishali on 11-03-2020
    {
        Customer_Obligations__c obilgations = new Customer_Obligations__c();
        obilgations.Customer_Integration__c = parentId;
        obilgations.Current_Status__c = obil.currentStatus;
        obilgations.DPD_3_Month_Count__c = obil.countDPD3Months;
        obilgations.DPD_3_Month_Max_Peak__c = obil.maxPeakDPD3Months;
        obilgations.DPD_12_Month_Count__c = obil.countDPD12Months;
        obilgations.DPD_12_Month_Max_Peak__c = obil.maxPeakDPD12Months;
        obilgations.EMI_Amount__c = obil.emiAmount;
        obilgations.Loan_Type__c = obil.loanType;
        obilgations.Balance__c = obil.balance;
        obilgations.Repayment_Tenure__c = obil.repaymentTenure;
        obilgations.Write_Off_Flag__c = obil.writeOffFlag;
		//*****************************************************Added by Vaishali for BRE
		obilgations.Manual__c= false;
        Customer_Integration__c cust = new Customer_Integration__c();
        if(!String.isBlank(parentId)) {
			// Added by Vaishali on 11-03-2020
            if(mapOfCustIntegrations.containsKey(parentId)) {
                obilgations.Customer_Detail__c = mapOfCustIntegrations.get(parentId).Loan_Contact__c;
            }
            //End of patch- Added by Vaishali on 11-03-2020
            
			// Commented by Vaishali on 11-03-2020
            /*cust = [SELECT Id, Loan_Contact__c FROM Customer_Integration__c WHERE Id =: parentId];
            if(cust != null) {
                obilgations.Customer_Detail__c = cust.Loan_Contact__c;       
            }*/
        }
        if(!String.isBlank(obil.accountType)) {
            obilgations.Account_Type__c = obil.accountType;       
        }
        if(!String.isBlank(obil.accountNumber)) {
            obilgations.Account_Number__c = obil.accountNumber;  
        }
        if(obil.amountOverdue != null) {
            obilgations.Amount_Overdue__c = obil.amountOverdue;           
        }
        if(obil.currentBalance != null) {
            obilgations.Current_Balance__c = obil.currentBalance;            
        }
        if(!String.isBlank(obil.dateClosed)) {
            String dt = obil.dateClosed;
            obilgations.Date_of_Closure__c = Date.newInstance(Integer.ValueOf((dt).subString(4,8)), Integer.ValueOf((dt).subString(2,4)), Integer.ValueOf((dt).subString(0,2)));           
        }
        if(!String.isBlank(obil.dateOfLastPayment)) {
            String dt = obil.dateOfLastPayment;
            obilgations.Date_of_Last_Payment__c = Date.newInstance(Integer.ValueOf((dt).subString(4,8)), Integer.ValueOf((dt).subString(2,4)), Integer.ValueOf((dt).subString(0,2)));           
        }
        if(!String.isBlank(obil.dateOpenedOrDisbursed)) {
            String dt = obil.dateOpenedOrDisbursed;
            obilgations.Date_of_Loan_Taken__c = Date.newInstance(Integer.ValueOf((dt).subString(4,8)), Integer.ValueOf((dt).subString(2,4)), Integer.ValueOf((dt).subString(0,2)));          
        }
        if(!String.isBlank(obil.dateReportedandCertified)) {
            String dt = obil.dateReportedandCertified;
            obilgations.Reported_Date__c = Date.newInstance(Integer.ValueOf((dt).subString(4,8)), Integer.ValueOf((dt).subString(2,4)), Integer.ValueOf((dt).subString(0,2)));          
        }
        if(!String.isBlank(obil.reportingMemberShortName)) {
            obilgations.Organization_of_Account__c = obil.reportingMemberShortName;          
        }
        if(obil.highCreditOrSanctionedAmount != null) {
            obilgations.Highest_Sanctioned_Amount__c = obil.highCreditOrSanctionedAmount;           
        }
        if(!String.isBlank(obil.ownershipIndicator)) {
            obilgations.Ownership_Indicator__c = obil.ownershipIndicator;            
        }
        if(obil.paymentFrequency != null) {
            obilgations.Payment_Frequency__c = obil.paymentFrequency;           
        }
        if(!String.isBlank(obil.paymentHistory1)) {
            obilgations.Payment_History1__c = obil.paymentHistory1;           
        }
        if(!String.isBlank(obil.paymentHistory2)) {
            obilgations.Payment_History2__c = obil.paymentHistory2;           
        }
        if(!String.isBlank(obil.paymentHistoryEndDate)) {
            String dt = obil.paymentHistoryEndDate;
            obilgations.Payment_History_End_Date__c = Date.newInstance(Integer.ValueOf((dt).subString(4,8)), Integer.ValueOf((dt).subString(2,4)), Integer.ValueOf((dt).subString(0,2)));          
        }
        if(!String.isBlank(obil.paymentHistoryStartDate)) {
            String dt = obil.paymentHistoryStartDate;
            obilgations.Payment_History_Start_Date__c = Date.newInstance(Integer.ValueOf((dt).subString(4,8)), Integer.ValueOf((dt).subString(2,4)), Integer.ValueOf((dt).subString(0,2)));        
        }
        if(obil.rateOfInterest != null) {
            obilgations.ROI__c = obil.rateOfInterest;           
        }
        if(obil.settlementAmount != null) {
            obilgations.Settlement_Amount__c = obil.settlementAmount;           
        }
        if(!String.isBlank(obil.suitFiledorWilfulDefault)) {
            obilgations.Suit_Filed_and_Wilful_Default_Status__c = obil.suitFiledorWilfulDefault;           
        }
        if(!String.isBlank(obil.typeOfCollateral)) {
            obilgations.Type_of_Collateral__c = obil.typeOfCollateral;           
        }
        if(obil.valueOfCollateral != null) {
            obilgations.Value_of_Collateral__c = obil.valueOfCollateral;            
        }
        if(obil.writtenOffAmountPrincipal != null) {
            obilgations.Written_Off_Principal__c = obil.writtenOffAmountPrincipal;           
        }
        if(!String.isBlank(obil.writtenOffAndSettledStatus)) {
            obilgations.Written_Off_Settled_Status__c = obil.writtenOffAndSettledStatus;           
        }
        if(obil.creditLimit != null) {
            obilgations.Credit_Limit__c = obil.creditLimit;           
        }
        if(obil.cashLimit != null) {
            obilgations.Cash_Limit__c = obil.cashLimit;            
        }
        if(obil.writtenOffAmountTotal != null) {
            obilgations.Written_Off_Total_Amount__c = obil.writtenOffAmountTotal;           
        }
        //**************************************************End of the block- Added by Vaishali for BRE
        return obilgations;
    }
	
	//*********************************************************Added by Vaishali for BRE
    public static Enquiry__c getenquiryList( enquiryList enq, String parentId )
    {
        Enquiry__c enquiry = new Enquiry__c();
        enquiry.Customer_Integration__c = parentId;
        
        if(!String.isBlank(enq.dateReported)) {
            String dt = enq.dateReported;
            enquiry.Enquiry_Date__c = Date.newInstance(Integer.ValueOf((dt).subString(4,8)), Integer.ValueOf((dt).subString(2,4)), Integer.ValueOf((dt).subString(0,2))); 
        }
        if(!String.isBlank(enq.enquiryPurpose)) {
            enquiry.Purpose_of_Enquiry__c = enq.enquiryPurpose;
        }    
        if(enq.enquiryAmount != null) {
            enquiry.Amount_Enquired__c = enq.enquiryAmount;
        }
        if(!String.isBlank(enq.reportingMemberShortName)) {
            enquiry.Organization_of_Enquiry__c = enq.reportingMemberShortName;
        }    
        return enquiry;
    }
    public static Employment_Detail__c getemploymentList( employmentList emp, String parentId )
    {
        Employment_Detail__c employment = new Employment_Detail__c();
        employment.Customer_Integration__c = parentId;
        
        if(!String.isBlank(emp.accountType)) {
            employment.Account_Type__c = emp.accountType;
        }
        if(emp.income != null) {
            employment.Income__c = emp.income;
        }
        if(!String.isBlank(emp.occupationCode)) {
            employment.Occupation_Code__c = emp.occupationCode;
        }
        if(!String.isBlank(emp.netGrossIndicator)) {
            employment.Net_Gross_Income_Indicator__c = emp.netGrossIndicator;
        }
        if(!String.isBlank(emp.monthlyAnnuallyIndicator)) {
            employment.Monthly_Annual_Income_Indicator__c = emp.monthlyAnnuallyIndicator;
        }
        if(!String.isBlank(emp.dateReported)) {
            String dt = emp.dateReported;
            employment.Date_Reported__c = Date.newInstance(Integer.ValueOf((dt).subString(4,8)), Integer.ValueOf((dt).subString(2,4)), Integer.ValueOf((dt).subString(0,2))); 
        }
        return employment;
    }
    public static Bureau_address__c getaddressList( addressList add, String parentId,Map<addressList, Id> mapOfAddressidWithPincodeIds ) // Added this parameter by Vaishali on 11-03-2020
    {
        Bureau_address__c address = new Bureau_address__c();
        address.Customer_Integration__c = parentId;
        if(!String.isBlank(add.addressCategory)) {
            address.Address_Category__c = add.addressCategory;
        }
        if(!String.isBlank(add.residenceCode)){
            address.Residence_Code__c = add.residenceCode;
        }
        if(!String.isBlank(add.addressLine1)){
            address.Address_Line1__c = add.addressLine1;
        }
        if(!String.isBlank(add.addressLine2)){
            address.Address_Line2__c = add.addressLine2;
        }
        if(!String.isBlank(add.addressLine3)){
            address.Address_Line3__c = add.addressLine3;
        }
        if(!String.isBlank(add.addressLine4)){
            address.Address_Line4__c = add.addressLine4;
        }
        if(!String.isBlank(add.addressLine5)){
            address.Address_Line5__c = add.addressLine5;
        }
        if(!String.isBlank(add.stateCode)){
            address.State__c = add.stateCode;
        }
        if(!String.isBlank(add.pinCode)){
			// Added by Vaishali on 11-03-2020
            if(mapOfAddressidWithPincodeIds.containsKey(add)) {
                address.Pincode__c = mapOfAddressidWithPincodeIds.get(add);
            }
            //End of patch- Added by Vaishali on 11-03-2020

            /*List<Pincode__c> pcList = new List<Pincode__c>();
            pcList= [SELECT Id FROM Pincode__c WHERE Name =: add.pinCode];
            Pincode__c pc = new Pincode__c();
            if(!pcList.isEmpty()) {
                pc = pcList[0];
            }
            
            if(pc != null) {
                address.Pincode__c = pc.Id;
            }*/   
        }
        if(!String.isBlank(add.enrichedThroughtEnquiry)){
            address.Enriched_though_enquiry_flag__c = Boolean.valueOf(add.enrichedThroughtEnquiry);
        }
        if(!String.isBlank(add.dateReported)){
            String dt = add.dateReported;
            address.Date_Reported__c = Date.newInstance(Integer.ValueOf((dt).subString(4,8)), Integer.ValueOf((dt).subString(2,4)), Integer.ValueOf((dt).subString(0,2))); 
        }

        return address;
    }
    public static ID_Detail__c getidList( idList iddet, String parentId )
    {
        ID_Detail__c iddetail = new ID_Detail__c();
        iddetail.Customer_Integration__c = parentId;
        
        if(!String.isBlank(iddet.IdType)) {
            iddetail.ID_Document_Type__c = iddet.IdType;     
        }
        if(!String.isBlank(iddet.IdValue)) {
            iddetail.ID_Number__c = iddet.IdValue;
        }
        if(!String.isBlank(iddet.issueDate)) {
            String dt = iddet.issueDate;
            iddetail.Issue_Date__c = Date.newInstance(Integer.ValueOf((dt).subString(4,8)), Integer.ValueOf((dt).subString(2,4)), Integer.ValueOf((dt).subString(0,2)));  
        }
        if(!String.isBlank(iddet.expirationDate)) {
            String dt = iddet.expirationDate;
            iddetail.Expiry_Date__c = Date.newInstance(Integer.ValueOf((dt).subString(4,8)), Integer.ValueOf((dt).subString(2,4)), Integer.ValueOf((dt).subString(0,2)));  
        }
        if(!String.isBlank(iddet.enrichedThroughtEnquiry)) {
            iddetail.Enriched_though_enquiry_flag__c = iddet.enrichedThroughtEnquiry; 
        }
        return iddetail;
    }
    //*********************************************************End of the block- Added by Vaishali for BRE
}