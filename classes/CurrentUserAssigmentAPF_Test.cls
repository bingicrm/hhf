@isTest(SeeAllData = false)
private class CurrentUserAssigmentAPF_Test {

  private static testMethod void test1() {
        Project__c objProject = new Project__c();
      Group objGroup;
	User adminUser = new User(Id=UserInfo.getUserId());
      User assignedSalesUser = [Select Id, Name From User Where isActive=true AND Profile.Name = 'Sales Team' AND Location__c = 'Lucknow' LIMIT 1];
      System.runAs(assignedSalesUser) {
	    //Creates a new Project of Type - New
        List<RecordType> lstRecordType = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Individual'];
        Id builderIndvRecTypeId;
        if(!lstRecordType.isEmpty()) {
            builderIndvRecTypeId = lstRecordType[0].Id;
        }
        
        Account acc = new Account();
            acc.RecordTypeId = builderIndvRecTypeId;
            acc.Name = 'Test Individual Account';
            acc.PAN__c = 'AAAAA1111B';
            acc.GSTIN__c = '27AAHFG0551H1ZL';
        insert acc;
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83127');
        insert objPincode;
        
        Document_Master__c objDocMaster = new Document_Master__c(Name = 'Sales Brochure and Carpet/Built up - Sale area statement for Units',
                                                                Applicable_for_APF__c = true, Mandatory_for_APF__c = true,
                                                                Document_Title__c = 'Sales Brochure and Carpet/Built up - Sale area statement for Units');
                                                                
        insert objDocMaster;
        
        Document_Type__c objDocType = new Document_Type__c(Name = 'Legal - APF',
                                                            Applicable_for_APF__c = true);
        insert objDocType;
        
        
        APF_Document_Checklist__c objAPFDC = new APF_Document_Checklist__c(Document_Master__c = objDocMaster.Id, Document_Type__c = objDocType.Id, Mandatory__c = true, Valid_for_Project_Type__c = 'New');
        insert objAPFDC;
        
        //Project__c objProject = new Project__c();
        objProject.Assigned_Sales_User__c = assignedSalesUser != null ? assignedSalesUser.Id : null;
        objProject.Account__c = acc.Id;
        objProject.Project_Name__c = 'Test Project';
        objProject.APF_Status__c = 'APF Initiated';
        objProject.Stage__c = 'APF Initiation';
        objProject.Sub_Stage__c = 'Project Data Capture';
        objProject.APF_Type__c = 'New';
        objProject.Address_Line_1__c = 'Test Address';
        objProject.OwnerId = assignedSalesUser.Id;
        objProject.Pincode__c = objPincode.Id;
        insert objProject;
      System.runAs(adminUser) {
        objGroup = [SELECT Id from Group WHERE Type='Queue' AND DeveloperName='Express_Data_Checker'];

        GroupMember GM = new GroupMember(GroupId=objGroup.Id,UserOrGroupId=assignedSalesUser.Id);
        insert GM;
      }
 		objProject.OwnerId = objGroup.Id;
        update objProject;
          Test.startTest();
      	CurrentUserAssigmentAPF.setOwner(objProject.id);
        Test.stopTest();
      }
      
  	}
  }