@isTest

public class TestHttp_Callout_SanctionLetter
{
public static testMethod void method1()
 {
  LMS_Date_Sync__c objDate = new LMS_Date_Sync__c(Current_Date__c = system.today());
   insert objDate;
   Account acc=new Account();
    acc.name='TesstMWDFSADF';
    acc.phone='9688467902';
    acc.PAN__c='YCJDP7834G';
    acc.Date_of_Incorporation__c=date.today();
    insert acc;
     
     Document_Master__c objdoc = new Document_Master__c(Name = 'Sanction Letter', Doc_Id__c = '123');
     insert objdoc;
    
     
System.debug(acc.id);

Scheme__c sc=new Scheme__c();
sc.name='Construction Finance';
sc.Scheme_Code__c='CF';
sc.Scheme_Group_ID__c='6';
sc.Product_Code__c='HL';
insert sc;
System.debug(sc.id);
  
Loan_Application__c lap=new Loan_Application__c ();
lap.Customer__c=acc.id;
lap.Transaction_type__c='DAA';
lap.Scheme__c=sc.id;
lap.Requested_Amount__c = 50000000;
lap.Approved_Loan_Amount__c=5000000;
lap.Transaction_Type__c='PB';
insert lap;


  Sanction_Condition_Master__c scm = new Sanction_Condition_Master__c();
   scm.Sanction_Condition__c = 'Sanction condition ABC';
   insert scm;
   
   List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
   //lstofcon[0].Applicant_Type__c='Co- Applicant';
//update lstofcon;
Loan_Sanction_Condition__c lsc = new Loan_Sanction_Condition__c();
lsc.Customer_Details__c=lstofcon[0].id;
lsc.Loan_Application__c = lap.id;
lsc.Sanction_Condition_Master__c  = scm.id;
lsc.Sanction_Condition__c ='Sanction condition ABC';
lsc.Status__c = 'Requested';
lsc.Type_of_Query__c = 'Document Only';
insert lsc; 
ID parameter = lap.id;

Loan_Application__c laCo = [Select id, Name, loan_number__c,Sub_Stage__c,
                                      (Select id, Name, Phone_Number__c,Email__c, Customer__r.name from Loan_Contacts__r where Applicant_Type__c = 'Co- Applicant') 
                                      from Loan_Application__c where id =:parameter limit 1];
                                      
                                      System.debug(laCo);

Http_Callout_SanctionLetter.deserializeResponse dR = new Http_Callout_SanctionLetter.deserializeResponse();
dR.result = 'hello';
TestMockRequest req1=new TestMockRequest(200,'Complete',JSON.serialize(dR),null);
Test.setMock(HttpCalloutMock.class,req1);

Test.startTest();
Http_Callout_SanctionLetter.SanctionLetter(parameter);
Test.stopTest();
 }
 }