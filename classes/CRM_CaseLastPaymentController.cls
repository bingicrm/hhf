public with sharing class CRM_CaseLastPaymentController{
   
    public case theCase{get;set;}
    public map<string,Object> pdfDetailsMap;
    public date today {get{return date.today();}set;}
    public CRM_CaseLastpaymentResponse caseLastpaymentResponse{get;set;}
    public String jsonString;
    public HttpResponse response;
    public String endpoint;
   
    public CRM_CaseLastPaymentController(ApexPages.StandardController stdController) {
        try{this.theCase = (Case)stdController.getRecord();
            Id caseId = ApexPages.currentPage().getParameters().get('id');
        //    theCase = [select LMS_Application_ID__c, Loan_Application_Number__c, LD_Branch_ID__c, account.Name, LD_Address_Line1__c, LD_Address_Line2__c, LD_Address_Line3__c, LD_GST_State__c, LD_Pincode__c, CLD_Mobile_Number__c from case where Id=: caseId];
           
           
           }
        catch(Exception e){
            system.debug('@@@ Error--> '+e.getMessage()+''+e.getLineNumber());
        }
    }
    @AuraEnabled
    public static CRM_CaseLastpaymentResponse getResponse(Id caseId){
    //CRM_CaseLastpaymentResponse
            Case theCase = [select LMS_Application_ID__c,CLD_Disbursal_Status__c, Loan_Application_Number__c from case where Id=: caseId];
           
            Rest_Service__mdt rs = [Select Client_Username__c, Client_Password__c, Service_EndPoint__c from Rest_Service__mdt where DeveloperName = 'CRM_Case_Lastpayment'];
           
        if(theCase.Loan_Application_Number__c  != null && theCase.LMS_Application_ID__c != null && theCase.CLD_Disbursal_Status__c  != null){
               
                String username = rs.Client_Username__c;
                String password = rs.Client_Password__c;
                String endpoint = rs.Service_EndPoint__c;
                Blob headerValue = Blob.valueOf(rs.Client_Username__c+ ':' +rs.Client_Password__c);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
               
                Map<String, Object> jsonMap = new Map<String, Object>();
               
            
            	jsonMap.put('maturityFlag','N');
            	//jsonMap.put('disbursalStatus','F');
            	jsonMap.put('loanType','DEF');
       	     	//jsonMap.put('agreementId','1000021');
         	    //jsonMap.put('referenceNumber','HHFDELHOU18000000020');
        	
        		jsonMap.put('disbursalStatus',theCase.CLD_Disbursal_Status__c);
        		jsonMap.put('agreementId',theCase.LMS_Application_ID__c);
           		jsonMap.put('referenceNumber',theCase.Loan_Application_Number__c);
               
                String jsonString = JSON.Serialize(jsonMap);
               
                Http p=new Http();
                HttpRequest request =new HttpRequest();
                HttpResponse response =new HttpResponse();
                request.setHeader('Content-Type','application/json');
                request.setHeader('accept','application/json');
               
                request.setHeader('Authorization', authorizationHeader);
                request.setHeader('operation-flag','R');
               
                request.setEndPoint(endpoint);
                request.setMethod('POST');
                request.setBody(jsonString);
               
                response =p.send(request);
       
                CRM_CaseLastpaymentResponse caseLastpaymentResponse = CRM_CaseLastpaymentResponse.parse(response.getBody());
               
                system.debug('@@@ Response--> '+endpoint);
                system.debug('@@@ Response--> '+response.getBody().trim());
                system.debug('@@@ response.getStatusCode()--> '+response.getStatusCode());
                system.debug('*****InController**'+caseLastpaymentResponse);
                system.debug('*****Bounceddetails**'+caseLastpaymentResponse.bouncedetails);
                system.debug('*****bouncedetail**'+caseLastpaymentResponse.bouncedetail);
             
                return caseLastpaymentResponse;
            }
        else
            return null;
    }
   
    //Create integration logs
    public void logUtility(){
        try{
            //LogUtility.createIntegrationLogs(jsonstring,response,endpoint);
        }
        catch(Exception e){
            system.debug('@@@ Error--> '+e.getMessage()+''+e.getLineNumber());
        }
    }
   
   
}