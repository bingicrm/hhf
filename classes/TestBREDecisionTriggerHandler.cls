@isTest
private class TestBREDecisionTriggerHandler {
    
    private static testMethod void methodTest(){
        Profile p=[SELECT id,name from profile where name=:'Sales Team'];
        Profile p1=[SELECT id,name from profile where name=:'Credit Team'];
        Loan_Application__c la;
        Scheme__c sch;
		Hierarchy__c objHierarchy = new Hierarchy__c();
        /*objHierarchy.BCM__c = u2.Id;
        objHierarchy.ZCM__c = u4.Id;
        objHierarchy.CCM__c = u2.Id;
        objHierarchy.CRO__c = u2.Id;*/
        insert objHierarchy;
		List<Hierarchy__c> lstHierarchy = [Select Id, Name, Active__c, BCM__c, BH__c, CCM__c, CRO__c, NCM__c, ZCM__c, ZCM_Salaried__c from Hierarchy__c where Active__c = true];
        String HierarchyName = lstHierarchy[0].Name; 
        User u = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',			Role_in_Sales_Hierarchy__c = 'SM'
);
            
        insert u;
        
        User u1 = new User(
            ProfileId = p1.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',Hierarchy__c = HierarchyName,
            	     Non_Salaried_Authority__c = 'A2',
            		 Salaried_Authority__c = 'A2',
            		 Role_in_Credit_Hierarchy__c = 'BCM');
            
        insert u1;
        
        System.runAs(u){
            Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
            Account acc = new Account();
            acc.CountryCode__c='+91';
            acc.Email__c='puser001@amamama.com';
            acc.Gender__c='M';
            acc.Mobile__c='9800765432';
            acc.Phone__c='9800765434';
            acc.PersonMobilePhone = '9800765434';
            acc.FirstName = 'abc';
            acc.LastName='def';
            acc.Salutation='Mr';
            acc.RecordTypeId = RecordTypeIdCustomer;
            insert acc;
            
            sch= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                         Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                         Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                         Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                         Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
            insert sch;
            
            Id RecordTypeIdLoanApplication = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('COPS: Credit Operations Entry').getRecordTypeId();
        
            la = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=sch.Id,Loan_Number__c='124',
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',StageName__c='Customer Onboarding',
                                                                        RecordTypeId=RecordTypeIdLoanApplication,
                                                                        Business_Date_Created__c=System.today(),
                                                                        Approved_Loan_Amount__c=100000,
                                                                        Processing_Fee_Percentage__c=1,Mode_of_Repayment__c='NACH',
                                                                        Repayment_Start_Date__c=Date.Today().addDays(100),
                                                                        Sanctioned_Disbursement_Type__c='Multiple Tranche',Property_Identified__c = true,Credit_Manager_User__c= u1.Id);
            insert la;
            
            
            
            Loan_Contact__c lc =[SELECT Id,Borrower__c from Loan_Contact__c where Loan_Applications__c =: la.Id];
            lc.Borrower__c='1';
            //lc.Category__c='1';
            lc.Constitution__c = '20';
            lc.Customer_segment__c = '1';
            lc.BRE_Record__c = true;
            lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc.Marital_Status__c = 'M';
            lc.Gross_Salary__c = 1000;
            lc.Basic_Pay__c = 1000;
            lc.DA__c = 1000;
            //lc.Total_Fixed_Pay__c = 1000;
            //lc.Variable_Income_Monthly__c = 1000;
            lc.Net_Income__c = 1000;
            lc.PF__c = 1000;
            lc.TDS__c = 1000;
            lc.Current_Organization_Work_Ex_in_Months__c=12;
            lc.Total_Work_Experience__c = 12;
            lc.Customer_Details_Verified__c= true;
            lc.Voter_ID_Number__c = '123456789';
            lc.Passport_Number__c = '123456789';
            lc.PAN_Number__c = 'BAFPV6543A';
            lc.Aadhaar_Number__c = '898518762736';
            lc.Income_Considered__c = true;
            update lc;
            
            lc.Customer_Verified__c = true;
            update lc;
            
            la.StageName__c = 'Credit Decisioning';
            la.SUb_Stage__c = 'Credit Review';
            update la;
            
            
        }
        
        
            Credit_Deviation_Master__c dev = new Credit_Deviation_Master__c();
            dev.Name = 'Test deviation';
            dev.Approver_Level__c = 'BH';
            dev.Status__c = TRUE;
            dev.Deviation_Code__c = 'R162';
            dev.Product__c = 'CF';
            insert dev;
            
        
        
            
            Mitigant__c objM = new Mitigant__c();
            objM.Active__c = true;
            objM.Mitigant_ID__c = '123';
            objM.Name = 'Others';
            insert objM;
            
            Applicable_Deviation__c appDev = new Applicable_Deviation__c();
            appDev.Credit_Deviation_Master__c = dev.Id;
            appDev.Loan_Application__c = la.Id;
            appDev.Auto_Deviation__c = true;
            appDev.Mitigant__c = objM.Id;
            insert appDev;
            
            BRE_Decision_Code__c breDecisionCode = new BRE_Decision_Code__c(Reason_Code__c='R162',Active__c=true);
            insert breDecisionCode;
            
            RunDeleteTriggerOnApplicableDeviation__c RunDeleteTriggerOnApplicableDeviation = new RunDeleteTriggerOnApplicableDeviation__c();
            RunDeleteTriggerOnApplicableDeviation.Name = 'RunDeleteTriggerOnApplicableDeviation' ;
            RunDeleteTriggerOnApplicableDeviation.byPassTrigger__c = false;
            insert RunDeleteTriggerOnApplicableDeviation;
            
            Application_CIBIL_Decision_Detail__c appCIBILDecision = new Application_CIBIL_Decision_Detail__c();
            appCIBILDecision.Loan_Application__c = la.Id;
            appCIBILDecision.BRE_II__c = true;
            appCIBILDecision.Decision__c = 'AMBER';
            appCIBILDecision.Reason_Code__c = 'R162';
            //appCIBILDecision.Product__c = sch.Id;
            insert appCIBILDecision;
            
            //delete appCIBILDecision;
    
    }
}