public with sharing class SanctionConditionUtility {
    
    public static List<Loan_Sanction_Condition__c> getCreditDecisioningSanctionConditions( String loanAppId ) 
    {
        List<Loan_Sanction_Condition__c> conditions = new List<Loan_Sanction_Condition__c>();
        List<Loan_Application__c> la = [Select id,(Select Customer_segment__c From Loan_Contacts__r) From Loan_Application__c  where id =:loanAppId];
        //Select l.Sanction_Condition__c, l.Loan_Application__c From Loan_Sanction_Condition__c lCustomer_Details__c
        if( la.size() > 0 )
        {
           
                system.debug('Hello'+la[0].Loan_Contacts__r[0].Customer_segment__c);
                conditions = getLoanSanction(la[0].Id, la[0].Loan_Contacts__r[0].id, la[0].Loan_Contacts__r[0].Customer_segment__c);
                

              /*  if(lc.Customer_segment__c == '1')
                {
                    Loan_Sanction_Condition__c nondefaultCondition = getLoanSanction(la[0].Id, lc.id, '1');
                    conditions.add(nondefaultCondition);
                } 
                else if(lc.Customer_segment__c == '3' || lc.Customer_segment__c == '4')
                {
                    Loan_Sanction_Condition__c nondefaultCondition = getLoanSanction(la[0].Id, lc.id, 'default');
                    conditions.add(nondefaultCondition);
                }*/
            
        }

        return conditions;

    }

    private static List<Loan_Sanction_Condition__c> getLoanSanction( String laId, String lcId, String strCustSegment )
    {
        List<Sanction_Condition_Master__c> lstmaster = [Select id,Sanction_Condition__c from Sanction_Condition_Master__c where Customer_Segment__c =:strCustSegment AND Default__c = TRUE];
        List<Loan_Sanction_Condition__c> lstSancCond = new List<Loan_Sanction_Condition__c>();
        Loan_Sanction_Condition__c defaultCondition;
        for(Sanction_Condition_Master__c objSan : lstmaster) {
            defaultCondition = new Loan_Sanction_Condition__c();
            defaultCondition.Loan_Application__c = laId;
            defaultCondition.Customer_Details__c = lcId;
            defaultCondition.Type_of_Query__c= Constants.INFORMATION_ONLY ;
            defaultCondition.Sanction_Condition_Master__c = objSan.Id;
            defaultCondition.Sanction_Condition__c = objSan.Sanction_Condition__c;
            lstSancCond.add(defaultCondition);
        }    
        
        return lstSancCond;

    }
}