public class AddressDetailTriggerHandler {
    public static void onBeforeInsert(List<Address_Detail__c> lstTriggerNew) {
        List<Project_Builder__c> lstProjectBuilderwithAddresses = new List<Project_Builder__c>();
        Set<Id> setProjectBuilderIds = new Set<Id>();
        for(Address_Detail__c objAddressDetail : lstTriggerNew) {
            if(String.isNotBlank(objAddressDetail.Project_Builder__c)) {
                setProjectBuilderIds.add(objAddressDetail.Project_Builder__c);
            }
        }
        System.debug('Debug Log for setProjectBuilderIds'+setProjectBuilderIds);
        if(!setProjectBuilderIds.isEmpty()) {
            lstProjectBuilderwithAddresses = [Select Id, Name, (Select Id, Name from Address_Details__r Where Id NOT IN: lstTriggerNew) From Project_Builder__c Where ID IN: setProjectBuilderIds];
            System.debug('Debug Log for lstProjectBuilderwithAddresses'+lstProjectBuilderwithAddresses.size());
            if(!lstProjectBuilderwithAddresses.isEmpty()) {
                for(Project_Builder__c objProjectBuilder : lstProjectBuilderwithAddresses) {
                    for(Address_Detail__c objAddressDetail : lstTriggerNew) {
                        if(objAddressDetail.Project_Builder__c == objProjectBuilder.Id && !objProjectBuilder.Address_Details__r.isEmpty()) {
                            objAddressDetail.addError('Address already specified for the Project Builder.');
                        }
                    }
                }
            }
        }
    }
}