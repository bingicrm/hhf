@isTest
public class TestIMDCallout
{
 public static testMethod void method1()
 {
  Account acc=new Account();
    acc.name='Applicant123';
    acc.phone='9234509786';
    acc.PAN__c='ADGPW4078E';
    acc.Date_of_Incorporation__c=date.today();
    acc.Customer_ID__c = '1000002';
    insert acc;

System.debug(acc.id);

Scheme__c sc=new Scheme__c();
sc.name='Construction Finance';
sc.Scheme_Code__c='CF';
sc.Scheme_Group_ID__c='6';
sc.Product_Code__c='HL';
insert sc;
System.debug(sc.id);
  
Loan_Application__c lap=new Loan_Application__c ();
lap.Customer__c=acc.id;
lap.Transaction_type__c='PB';
lap.Scheme__c=sc.id;
lap.Requested_Amount__c = 50000000;
lap.Approved_Loan_Amount__c=4000000;
insert lap;

IMD__c imd = new IMD__c();
imd.Loan_Applications__c = lap.id;
imd.Cheque_Number__c = '930476';
imd.Amount__c = 5000;
imd.Payment_Mode__c = 'Q';
imd.Applicable_IMD__c = 8000;
imd.Cheque_Date__c = date.today();
imd.Receipt_Date__c = date.today();

insert imd;
ID recordId = imd.id;

IMDCallout.ResponseBody rb = new IMDCallout.ResponseBody();
/*rb.requestId = '9702';
rb.serviceStatus = 'SUCCESS';
rb.chequeId = '123525';
rb.errorMsg = 'Sorry , We could not process your request';*/
TestMockRequest req1=new TestMockRequest(null,null,JSON.serialize(rb),null);
Test.setMock(HttpCalloutMock.class,req1);
//Test.setMock(Response is null);
Test.startTest();
IMDCallout.makeIMDCallout(recordId);
Test.stopTest();
 }

}