public class CustomerObligationTriggerHandler{
    public static void afterInsert(List<Customer_Obligations__c> newList){
        List<Customer_Obligations__c> custObList = new List<Customer_Obligations__c>();
        for(Customer_Obligations__c custOb: newList){
            if(custOb.CD_Income_Considered__c == TRUE && custOb.Final_Obligations_Considered__c == FALSE && custOb.POS__c > 100000 && custOb.Balance_Tenure_MOB__c > 12){
                custObList.add(custOb);
            }
        }
        system.debug('SIZE of Inserted Obligations:'+custObList.size());
        if(custObList.size()>0){
            createSanctionCondition(custObList);
        }
    }
    public static void afterUpdate(List<Customer_Obligations__c> newList, List<Customer_Obligations__c> oldList, Map<id, Customer_Obligations__c> newMap, Map<id, Customer_Obligations__c> oldMap){
        List<Customer_Obligations__c> custObList = new List<Customer_Obligations__c>();
        List<Customer_Obligations__c> custObListOld = new List<Customer_Obligations__c>();
        for(Customer_Obligations__c custOb: newList){
            if((custOb.CD_Income_Considered__c == TRUE && custOb.Final_Obligations_Considered__c == FALSE && oldMap.get(custOb.Id).Final_Obligations_Considered__c == TRUE && custOb.POS__c > 100000 && custOb.Balance_Tenure_MOB__c > 12) || 
            (custOb.CD_Income_Considered__c == TRUE && custOb.Final_Obligations_Considered__c == FALSE && custOb.POS__c > 100000 && (oldMap.get(custOb.Id).POS__c <= 100000 || oldMap.get(custOb.Id).POS__c == null) && custOb.Balance_Tenure_MOB__c > 12) ||
            (custOb.CD_Income_Considered__c == TRUE && custOb.Final_Obligations_Considered__c == FALSE && custOb.POS__c > 100000 && custOb.Balance_Tenure_MOB__c > 12 && (oldMap.get(custOb.Id).Balance_Tenure_MOB__c <= 12 || oldMap.get(custOb.Id).Balance_Tenure_MOB__c == null))){
                custObList.add(custOb);
            }
            if((custOb.Final_Obligations_Considered__c == TRUE && oldMap.get(custOb.Id).Final_Obligations_Considered__c == FALSE) || (custOb.POS__c <= 100000 && oldMap.get(custOb.Id).POS__c > 100000) || (custOb.Balance_Tenure_MOB__c <= 12 && oldMap.get(custOb.Id).Balance_Tenure_MOB__c > 12)){
                custObListOld.add(custOb);
            }
        }
        system.debug('SIZE of Updated Obligations:'+custObList.size());
        if(custObList.size()>0){
            createSanctionCondition(custObList);
        }
        system.debug('SIZE of Old Obligations:'+custObListOld.size());
        if(custObListOld.size()>0){
            waiveSanctionCondition(custObListOld);
        }
    }
    public static void createSanctionCondition(List<Customer_Obligations__c> createList){
        List<Customer_Obligations__c> obList = new List<Customer_Obligations__c>();
        List<Loan_Sanction_Condition__c> toInsert = new List<Loan_Sanction_Condition__c>();
        List<Id> idList = new List<Id>();
        for(Customer_Obligations__c custOb : createList){
            idList.add(custOb.Id);
        }
        obList = [Select Id, Name, Remarks__c, Final_Obligations_Considered__c, Loan_Type__c, Highest_Sanctioned_Amount__c, Customer_Detail__c, Customer_Detail__r.Loan_Applications__c from Customer_Obligations__c where Id =: idList];
        Sanction_Condition_Master__c sancMaster = [Select Id, Name from Sanction_Condition_Master__c where Name = 'Others' limit 1];
        for(Customer_Obligations__c custOb : obList){
            Loan_Sanction_Condition__c lsc = new Loan_Sanction_Condition__c();
            lsc.Loan_Application__c = custOb.Customer_Detail__r.Loan_Applications__c;
            lsc.Customer_Details__c = custOb.Customer_Detail__c;
            lsc.Sanction_Condition_Master__c = sancMaster.Id;
            lsc.Sanction_Condition__c = 'For '+custOb.Loan_Type__c+' with a disbursed amount of Rs.'+custOb.Highest_Sanctioned_Amount__c;
            lsc.Status__c = 'Requested';
            lsc.Remarks__c = custOb.Remarks__c;
            lsc.Type_of_Query__c = 'Document Only';
            lsc.Customer_Obligation__c = custOb.Id;
            toInsert.add(lsc);
        }
        system.debug('SIZE of Sanction Conditions to Insert:'+toInsert.size());
        if(toInsert.size()>0){
            database.insert(toInsert);
        }
    }
    public static void waiveSanctionCondition(List<Customer_Obligations__c> waiveList){
        List<Id> idList = new List<Id>();
        for(Customer_Obligations__c custOb : waiveList){
            idList.add(custOb.Customer_Detail__c);
        }
        List<Loan_Sanction_Condition__c> sancList = new List<Loan_Sanction_Condition__c>();
        List<Loan_Sanction_Condition__c> updSancList = new List<Loan_Sanction_Condition__c>();
        sancList = [Select Id, Status__c, Customer_Obligation__c, Customer_Details__c from Loan_Sanction_Condition__c where Customer_Details__c =: idList];
        for(Customer_Obligations__c ob : waiveList){
            for(Loan_Sanction_Condition__c lsc : sancList){
                if(lsc.Customer_Obligation__c == ob.Id && lsc.Customer_Details__c == ob.Customer_Detail__c && (lsc.Status__c != 'Verified' && lsc.Status__c != 'Completed' && lsc.Status__c != 'Waived Off')){
                    lsc.Status__c = 'Waived Off';
                    updSancList.add(lsc);
                }
            }
        }
        system.debug('SIZE of Sanction Conditions to Waive:'+updSancList.size());
        if(updSancList.size()>0){
            database.update(updSancList);
        }
    }
}