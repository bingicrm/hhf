@isTest
public class TestApplicableDeviationTrigger {
    public static testMethod void method5()
    {
               
        /*User u=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId 
                       from user where profile.Name='DSA' and isActive=true limit 1];*/
         Profile p=[SELECT id,name from profile where name=:'Sales Team'];
           UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        List<User> lstBranchManager= new List<User>();                          
          User u1 = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,
			Role_in_Sales_Hierarchy__c = 'SM'
        );
        lstBranchManager.add(u1);
        
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,
						Role_in_Sales_Hierarchy__c = 'SM'

        );
         lstBranchManager.add(u);
        insert lstBranchManager;                        
                           
                           
        User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId,Profile.Name
                       from user where ID=:UserInfo.getUserId()];
        System.debug('Debug Log for loggedUser Profile' + loggedUser.Profile.Name);
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=lstBranchManager[0].Id;
        loggedUser.ManagerID=lstBranchManager[0].Id;
        update loggedUser;
        system.runAs(loggedUser){               
       
        
        
         Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='9872345333',
                                  Date_of_Incorporation__c=Date.newInstance(2018,11,11));
         
         Database.insert(Cob);                
                            
         Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                     Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                     Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                     Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                     Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
                            
         Database.insert(sc); 
        
         Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,
                                                           StageName__c='Customer Onboarding',
                                                           Sub_Stage__c='Credit Decisioning',
                                                           Transaction_type__c='SC',Requested_Amount__c=10000,
                                                           Applicant_Customer_segment__c='Salaried',
                                                           Branch__c='test',Line_Of_Business__c='Open Market',
                                                           Assigned_Sales_User__c=u.id,
                                                           Loan_Purpose__c='20',
                                                           Requested_EMI_amount__c=1000,Approved_ROI__c=5,
                                                           Requested_Loan_Tenure__c=5,
                                                           Government_Programs__c='PMAY',
                                                           Loan_Engine_2_Output__c='STP',
                                                           Property_Identified__c=True,ownerId=UserInfo.getUserID(),
                                                           Assigned_FileCheck_User__c=u.id);
                                                           
                                                           
        
        
        // Database.insert(LAob);
        
        List<Loan_Application__c> listLAob= new List<Loan_Application__c>();
        listLAob.add(LAob); 
        insert listLAob;
        List<Applicable_Deviation__c > appDevList = new List<Applicable_Deviation__c>();
        Credit_Deviation_Master__c dev = new Credit_Deviation_Master__c();
        dev.Name = 'Test deviation';
        dev.Approver_Level__c = 'BH';
        dev.Status__c = TRUE;
        insert dev;
        Applicable_Deviation__c appDev = new Applicable_Deviation__c();
        appDev.Credit_Deviation_Master__c = dev.Id;
        appDev.Loan_Application__c = LAob.Id;
        appDev.Approved__c = true;
        
        appDevList.add(appDev);
        System.debug('>>>>appDevList'+appDevList);
        insert appDevList;
        appDev.Approved__c = false;
        //appDevList.add(appDev);
        update appDev;
        
        Document_Type__c DType=new Document_Type__c(Name='Property Papers');
        insert DType;
        
              
        IMD__c IMDob=new IMD__c(Loan_Applications__c=LAob.id,Cheque_Number__c='123456',
                                 Cheque_Date__c=Date.today()-5,
                                 Amount__c=100,Receipt_Date__c=Date.today(),Payment_Mode__c='Q');
                              
        //Database.insert(IMDob);
            
        Document_Checklist__c DC=new Document_Checklist__c(Loan_Applications__c=LAob.id,Status__c='OTC Requested',
                                                           REquest_Date_for_OTC__c=Date.today());
        insert DC;
        
        
        
        Loan_Contact__c LCobject=[SELECT id from Loan_Contact__c where Loan_Applications__c=:LAob.id];
        LCobject.Customer_Verified__c=false;
        update LCobject;
        
             
        LCobject=[SELECT id,name,Customer_Verified__c from Loan_Contact__c where Loan_Applications__c=:LAob.id];
        
                        
        /*Third_Party_Verification__c TPVObject=new Third_Party_Verification__c(Loan_Application__c=LAob.id,
                                                                              Customer_Segment__c='Salaried',
                                                                              Status__c='NEW',
                                                                              Type_of_Verification__c='Full Profile Check',
                                                                              Date_Time_of_Visit__c=Date.today()-5);
      
        Database.insert(TPVObject); */
        
        Sanction_Condition_Master__c SCM=new Sanction_Condition_Master__c(Name='TEST');
        insert SCM;
        
        Loan_Sanction_Condition__c LSC =new Loan_Sanction_Condition__c(Loan_Application__c=LAob.id,Sanction_Condition_Master__c=SCM.id,
                                                                       Customer_Details__c=LCobject.id,status__c='Completed');
        
        insert LSC;   
        
        Document_Master__c DM=new Document_Master__c(Name='Financial Data entry- excel', Sanction_Condition_Master__c=SCM.id,
                                                     Doc_Id__c='test');  
        insert DM;  
        
        DST_Master__c DSTMaster=new DST_Master__c(Name='Test DST',Inspector_ID__c='1234');     
        insert DSTMaster;  
        
        Cross_Sell_Partner_Name__c CSPN=new Cross_Sell_Partner_Name__c(Name='Test');
        insert CSPN;                                               
        
        /*Sourcing_Detail__c SD=new Sourcing_Detail__c(Loan_Application__c=LAob.id,DST_Name__c=DSTMaster.id,
                                                      Source_Code__c='16',Sales_User_Name__c=u.id,
                                                      Cross_Sell_Partner_Name__c=CSPN.id);  
                                                      
        insert SD;*/
          
        Test.startTest();
        List<Loan_Application__c> updatelist=new List<Loan_Application__c>();
               
        for(Loan_Application__c ob:[SELECT id,Applicable_IMD__c,name,Customer__c,Verification_Count_All__c,Document_Waiver__c,recordtypeId,Scheme__c,StageName__c,Transaction_type__c,Requested_Amount__c,
                                    Customer_Detail_Check__c,All_Loan_Contact_Count__c,Assigned_FileCheck_User__c,
                                    Assigned_credit_at_CN__c,Credit_Manager_User__c,Assigned_Sales_Review__c,sales_manager__c,
                                    Applicant_Customer_segment__c,Branch__c,Line_Of_Business__c,Assigned_Sales_User__c,Document_marked_OTC__c,Sub_Stage__c,
                                    Loan_Purpose__c,Requested_EMI_amount__c,Approved_ROI__c,Requested_Loan_Tenure__c,Verification_Count_LT__c,
                                    Government_Programs__c,Loan_Engine_2_Output__c,Property_Identified__c,ownerId from Loan_Application__c
                                    where id IN:listLAob])
       {
           
                   ob.Sub_Stage__c='Tranche File Check';
                   ob.StageName__c='Operation Control';
                   ob.Property_Identified__c=True;
                   ob.Requested_Amount__c=3000001;
                   ob.Cus_neg_approval_required__c=false;
                   updatelist.add(ob);
            
       }
      
       //update updatelist;
       system.debug('HUNT update list'+updatelist[0].Sub_Stage__c);
       system.debug('HUNT update list'+updatelist[0].StageName__c);

       Test.stopTest();
        }
            
       }
    public static testMethod void method1()
    {

             Profile p=[SELECT id,name from profile where name=:'Sales Team'];
               UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
            insert r;
            List<User> lstBranchManager= new List<User>();                          
              User u1 = new User(
                ProfileId = p.Id,
                LastName = 'last',
                Email = 'puser000@amamama.com',
                Username = 'puser000@amamama.com' + System.currentTimeMillis(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                UserRoleId = r.Id,
							Role_in_Sales_Hierarchy__c = 'SM'

            );
            lstBranchManager.add(u1);
            
            User u = new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
                LastName = 'last',
                Email = 'puser000@amamama.com',
                Username = 'puser000@amamama.com' + System.currentTimeMillis(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                UserRoleId = r.Id,
							Role_in_Sales_Hierarchy__c = 'SM'

            );
             lstBranchManager.add(u);
            insert lstBranchManager;                        
                           
                           
        User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId,Profile.Name
                       from user where ID=:UserInfo.getUserId()];
        System.debug('Debug Log for loggedUser Profile' + loggedUser.Profile.Name);
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=lstBranchManager[0].Id;
        loggedUser.ManagerID=lstBranchManager[0].Id;
        update loggedUser;
        system.runAs(u){               
       
        
        
             Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='9872345333',
                                      Date_of_Incorporation__c=Date.newInstance(2018,11,11));
             
             Database.insert(Cob);                
                            
             Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                         Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                         Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                         Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                         Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
                                
             Database.insert(sc); 
             RunDeleteTriggerOnApplicableDeviation__c cs = new RunDeleteTriggerOnApplicableDeviation__c();
             cs.Name='RunDeleteTriggerOnApplicableDeviation';
             cs.byPassTrigger__c=false;
             insert cs;
             
             Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,
                                                               StageName__c='Customer Onboarding',
                                                               Sub_Stage__c='Credit Decisioning',
                                                               Transaction_type__c='SC',Requested_Amount__c=10000,
                                                               Applicant_Customer_segment__c='Salaried',
                                                               Branch__c='test',Line_Of_Business__c='Open Market',
                                                               Assigned_Sales_User__c=u.id,
                                                               Loan_Purpose__c='20',
                                                               Requested_EMI_amount__c=1000,Approved_ROI__c=5,
                                                               Requested_Loan_Tenure__c=5,
                                                               Government_Programs__c='PMAY',
                                                               Loan_Engine_2_Output__c='STP',
                                                               Property_Identified__c=True,ownerId=UserInfo.getUserID(),
                                                               Assigned_FileCheck_User__c=u.id);
                                                           
                                                           
        
        
        
            List<Loan_Application__c> listLAob= new List<Loan_Application__c>();
            listLAob.add(LAob); 
            insert listLAob;
            
            List<Applicable_Deviation__c > appDevList = new List<Applicable_Deviation__c>();
            Credit_Deviation_Master__c dev = new Credit_Deviation_Master__c();
            dev.Name = 'Test deviation';
            dev.Approver_Level__c = 'BH';
            dev.Status__c = TRUE;
            insert dev;
            
            Applicable_Deviation__c appDev = new Applicable_Deviation__c();
            appDev.Credit_Deviation_Master__c = dev.Id;
            appDev.Auto_Deviation__c = true;
            appDev.Approved__c = true;
            appDevList.add(appDev);
            insert appDevList;
        
            try{
    			delete appDevList;
    		}catch(Exception ee)
    		{}
        }
            
       }
      
}