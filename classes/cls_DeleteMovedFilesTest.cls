@isTest
public class cls_DeleteMovedFilesTest {
    
    public static testMethod void test1(){
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
        insert con1;
         
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;        

        Loan_Application__c la = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert la;
        
        Loan_Contact__c lc = [SELECT Id, Name,Applicant_Type__c, Applicant_Status__c, Constitution__c, Customer_segment__c, Loan_Applications__c FROM Loan_Contact__c WHERE Loan_Applications__c =: la.Id];
        lc.Borrower__c='2';
        lc.Contact__c = con1.Id;
        lc.Constitution__c='18';
        lc.Applicant_Status__c='1';
        lc.Customer_segment__c='9';
        lc.Mobile__c='9876543210';
        lc.Email__c='testemail@gmail.com';
        update lc;

        Test.startTest();        
       
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=la.Id,
                                                                     Loan_Contact__c=lc.Id);
        insert objDocCheck;
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
         
        //Get Content Documents
        cv=[SELECT Id, Title, VersionData, CreatedDate, ContentDocumentId, FileExtension,IsLatest FROM ContentVersion];
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
        
        Document_Migration_Log__c objDoc = new Document_Migration_Log__c ();
        objDoc.Description__c='Test';
        objDoc.Loan_Application__c=la.Id;
        objDoc.Document_Checklist__c = objDocCheck.Id;
        objDoc.Document_Id__c = conDocId;
        insert objDoc;
            
        Attachment att=new Attachment();
        att.ParentId = objDoc.Loan_Application__c ;
        att.Name='test';
        att.Body=Blob.valueOf('test');
        insert att;
       
        //Create ContentDocumentLink 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = objDocCheck.Id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        Insert cdl;
        
        cls_DeleteMovedFiles.deleteS3FilesApplicant(objDoc);
        Test.stopTest();        
    }
    
    public static testMethod void test2(){
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
        insert con1;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;        

        Loan_Application__c la = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert la;
        
        Loan_Contact__c lc = [SELECT Id, Name,Applicant_Type__c, Applicant_Status__c, Constitution__c, Customer_segment__c, Loan_Applications__c FROM Loan_Contact__c WHERE Loan_Applications__c =: la.Id];
        lc.Borrower__c='2';
        lc.Contact__c = con1.Id;
        lc.Constitution__c='18';
        lc.Applicant_Status__c='1';
        lc.Customer_segment__c='9';
        lc.Mobile__c='9876543210';
        lc.Email__c='testemail@gmail.com';
        update lc;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Co-Applicant').getRecordTypeId();
        Test.startTest();        
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=la.Id,Customer__c=acc.Id,
                                                             Applicant_Type__c='Guarantor',Borrower__c='1',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='Brother',
                                                             Pan_Verification_status__c=true,Constitution__c='20',
                                                             Customer_segment__c='1',Applicant_Status__c='1',
                                                             Category__c='1',
                                                             Father_s_Husband_s_Name__c='ABC',
                                                             GSTIN_Number__c='04AATPB2258C1Z3',
                               Mobile__c='9978786756',Email__c='avd@g.com');
        insert objLoanContact;
        
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=la.Id,
                                                                     Loan_Contact__c=objLoanContact.Id);
        insert objDocCheck;
        
        Document_Checklist__c objDocCheck2 = new Document_Checklist__c(Loan_Applications__c=la.Id,
                                                                     Loan_Contact__c=objLoanContact.Id);
        insert objDocCheck2;
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
         
        //Get Content Documents
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
        
        Document_Migration_Log__c objDoc = new Document_Migration_Log__c ();
        objDoc.Description__c = 'Test';
        objDoc.Loan_Application__c = la.Id;
        objDoc.Document_Checklist__c = objDocCheck.Id;
        objDoc.Document_Id__c = conDocId;
        insert objDoc;
        
        Attachment att=new Attachment();
        att.ParentId = objDoc.Loan_Application__c ;
        att.Name='test';
        att.Body=Blob.valueOf('test');
        insert att;
               
        //Create ContentDocumentLink 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = la.Id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        Insert cdl;
        
        //Create ContentDocumentLink 
        ContentDocumentLink cdl1 = New ContentDocumentLink();
        cdl1.LinkedEntityId = objDocCheck.Id;
        cdl1.ContentDocumentId = conDocId;
        cdl1.shareType = 'V';
        Insert cdl1;
        
        cls_DeleteMovedFiles.deleteS3FilesCoApplicantGuarantor(objDoc);
        cls_DeleteMovedFiles.deleteS3FilesLoanKit(objDoc);
        cls_DeleteMovedFiles.deleteS3FilesLoanKit2(objDoc);
     
        Test.stopTest();
    }
    
    public static testMethod void test3(){
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
         Profile p = [SELECT id, name from profile where name =: 'Sales Team'];
        
        User u = new User(
            ProfileId = p.Id,
            LastName = 'last',  
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST', 
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            Technical__c = true,
            FCU__c= true,Legal__c = true, FI__c= true,LIP__C = true,PD__c= true,Role_in_Sales_Hierarchy__c = 'SM');
        
        insert u;
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
        insert con1;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;        

        Loan_Application__c la = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11', Assigned_Sales_User__c = u.Id, Folders_Created_in_S3__c = true);
        insert la;

        
        Test.startTest();        
       
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=la.Id);
        insert objDocCheck;
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
         
        //Get Content Documents
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
            
        Document_Migration_Log__c objDoc = new Document_Migration_Log__c ();
        objDoc.Description__c='Test';
        objDoc.Loan_Application__c=la.Id;
        objDoc.Document_Checklist__c = objDocCheck.Id;
        objDoc.Document_Id__c = conDocId;
        objDoc.Successfully_Migrated__c = true;
        objDoc.Old_Release_Data__c = false;
        insert objDoc;
        
        Attachment att=new Attachment();
        att.ParentId = objDoc.Loan_Application__c ;
        att.Name='test';
        att.Body=Blob.valueOf('test');
        insert att;
        
        //Create ContentDocumentLink 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = objDocCheck.Id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        Insert cdl;
        
        la.StageName__c='Loan Cancelled';
        la.Sub_Stage__c='Loan Cancel';
        update la;
        cls_DeleteMovedFiles.deleteS3FilesApplication(objDoc);
       
        cls_DeleteDocumentsBatch obj = new cls_DeleteDocumentsBatch();
		Database.executeBatch(obj);
        Test.stopTest();        
    }

    private static testMethod void testSample() {
        Profile p = [SELECT id, name from profile where name =: 'Sales Team'];
        
        User u = new User(
            ProfileId = p.Id,
            LastName = 'last',  
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST', 
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            Technical__c = true,
            FCU__c= true,Legal__c = true, FI__c= true,LIP__C = true,PD__c= true,Role_in_Sales_Hierarchy__c = 'SM');
        
        insert u;
        
        Loan_Contact__c lc;
        Loan_Application__c la;
        
        System.runAs(u) {
            Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
            
            Account acc = new Account();
            acc.CountryCode__c = '+91';
            acc.Email__c = 'puser001@amamama.com';
            acc.Gender__c = 'M';
            acc.Mobile__c = '9800765432';
            acc.Phone__c = '9800765434';
            acc.PersonMobilePhone = '9800765434';
            acc.FirstName = 'abc';
            acc.LastName = 'def';
            acc.Salutation = 'Mr';
            acc.RecordTypeId = RecordTypeIdCustomer;
            insert acc;
            
            Scheme__c sch = new Scheme__c(Name = 'Construction Finance', Scheme_Code__c = 'CF', Scheme_Group_ID__c = '5',
                                          Scheme_ID__c = 11, Product_Code__c = 'CF', Max_ROI__c = 10, Min_ROI__c = 5,
                                          Min_Loan_Amount__c = 10000, Min_FOIR__c = 1,
                                          Max_Loan_Amount__c = 3000001, Max_Tenure__c = 84, Min_Tenure__c = 6,
                                          Max_LTV__c = 5, Min_LTV__c = 1, Max_FOIR__c = 5);
            insert sch;
            
            Scheme__c sch1=new Scheme__c();
            sch1.Name='Personal Loan';
            sch1.Product_Code__c='IPL'; 
            sch1.Scheme_Code__c='IPL';
            sch1.Scheme_Group_ID__c='Test';
            insert sch1;
            Local_Policies__c lp = new Local_Policies__c(Name='Standard', Branch__c='All',Product_Scheme__c='All Schemes',IsActive__c=true);
            insert lp;
            
            Id RecordTypeIdLAAppIni = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Application Initiation').getRecordTypeId();
            
            la = new Loan_Application__c(Customer__c = acc.Id, Scheme__c = sch.Id, Loan_Number__c = '124',
                                         Transaction_type__c = 'PB', Requested_Amount__c = 1000000,
                                         Loan_Purpose__c = '11', StageName__c = 'Customer Onboarding', Sub_Stage__c = 'Application Initiation',
                                         RecordTypeId = RecordTypeIdLAAppIni,Branch__c='Delhi',
                                         Business_Date_Created__c = System.today(),
                                         Approved_Loan_Amount__c = 1000000,Requested_Loan_Tenure__c=84,
                                         Processing_Fee_Percentage__c = 1, Mode_of_Repayment__c = 'NACH',
                                         Repayment_Start_Date__c = Date.Today().addDays(100),Income_Program_Type__c='Normal Salaried',
                                         Sanctioned_Disbursement_Type__c = 'Multiple Tranche', Property_Identified__c = true,
                                         Local_Policies__c=lp.Id, Assigned_Sales_User__c = u.Id,Folders_Created_in_S3__c=true);
            insert la;
            Test.startTest();
            lc = [SELECT Id, Borrower__c from Loan_Contact__c where Loan_Applications__c =: la.Id];
            lc.Borrower__c = '1';
            //lc.Category__c='1';
            lc.Constitution__c = '20';
            lc.Customer_segment__c = '3';
            lc.BRE_Record__c = true;
            lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc.Marital_Status__c = 'M';
            lc.Gross_Salary__c = 1000;
            lc.Basic_Pay__c = 1000;
            lc.DA__c = 1000;
            lc.Qualification__c = 'UMS';
            //lc.Total_Fixed_Pay__c = 1000;
            //lc.Variable_Income_Monthly__c = 1000;
            lc.Net_Income__c = 1000;
            lc.PF__c = 1000;
            lc.TDS__c = 1000;
            lc.Current_Organization_Work_Ex_in_Months__c = 12;
            lc.Total_Work_Experience__c = 12;
            lc.Customer_Details_Verified__c = true;
            lc.Voter_ID_Number__c = '123456789';
            lc.Passport_Number__c = '123456789';
            lc.PAN_Number__c = 'BAFPV6543A';
            lc.Aadhaar_Number__c = '898518762736';
            lc.Income_Considered__c = true;
            lc.AIP_month__c = '4-2020';
            lc.Mobile__c = '7878565677';
            lc.Email__c = 'abc@g.com';
            lc.Father_s_Husband_s_Name__c ='sww';
			lc.Mother_s_Maiden_Name__c ='avdd';
            update lc;
            /*
            Cross_Sell_Partner_Name__c CSPN = new Cross_Sell_Partner_Name__c(Name='Test');
            insert CSPN;
            
            DST_Master__c DSTMaster = new DST_Master__c(Name='Test DST',Inspector_ID__c='1234'); 
            insert DSTMaster;
            
            Sourcing_Detail__c sd= new Sourcing_Detail__c(Loan_Application__c=la.id,Source_Code__c='16',DST_Name__c=DSTMaster.id,Cross_Sell_Partner_Name__c=CSPN.id);
            insert sd;
            
            la.StageName__c = 'Operation Control';
            la.Sub_Stage__c = 'COPS:Data Maker';
            la.IMGC_Premium_Fees_GST_Inclusive1__c = 20000;
            la.Approved_cross_sell_amount__c = 50000;
            update la;*/
		Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=la.Id, Loan_Contact__c= lc.Id);
        insert objDocCheck;
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
         
        //Get Content Documents
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
            
        Document_Migration_Log__c objDoc = new Document_Migration_Log__c ();
        objDoc.Description__c='Test';
        objDoc.Loan_Application__c=la.Id;
        objDoc.Document_Checklist__c = objDocCheck.Id;
        objDoc.Document_Id__c = conDocId;
        objDoc.Successfully_Migrated__c = true;
        objDoc.Old_Release_Data__c = false;
        insert objDoc;
        
        Attachment att=new Attachment();
        att.ParentId = objDoc.Loan_Application__c ;
        att.Name='test';
        att.Body=Blob.valueOf('test');
        insert att;
        
        //Create ContentDocumentLink 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = objDocCheck.Id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        Insert cdl;
		
		la.StageName__c='Loan Cancelled';
		la.Sub_Stage__c='Loan Cancel';
		update la;
 		cls_DeleteDocumentsBatch obj = new cls_DeleteDocumentsBatch();
		Database.executeBatch(obj);
            Test.stopTest();
        }

    }
}