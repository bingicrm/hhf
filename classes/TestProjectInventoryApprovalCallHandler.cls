@isTest
public class TestProjectInventoryApprovalCallHandler {

    private static testMethod void ProjectInventoryApprovalCallHandler() {
	    User assignedSalesUser = [Select Id, Name From User Where isActive=true AND Profile.Name = 'Sales Team' AND Location__c = 'Lucknow' LIMIT 1];
	    
	    //Creates a new Project of Type - New
        List<RecordType> lstRecordType = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Individual'];
        Id builderIndvRecTypeId;
        if(!lstRecordType.isEmpty()) {
            builderIndvRecTypeId = lstRecordType[0].Id;
        }
        
        Account acc = new Account();
            acc.RecordTypeId = builderIndvRecTypeId;
            acc.Name = 'Test Individual Account';
            acc.PAN__c = 'AAAAA1111B';
            acc.GSTIN__c = '27AAHFG0551H1ZL';
        insert acc;
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83127');
        insert objPincode;
        
        Document_Master__c objDocMaster = new Document_Master__c(Name = 'Sales Brochure and Carpet/Built up - Sale area statement for Units',
                                                                Applicable_for_APF__c = true, Mandatory_for_APF__c = true,
                                                                Document_Title__c = 'Sales Brochure and Carpet/Built up - Sale area statement for Units');
                                                                
        insert objDocMaster;
        
        Document_Type__c objDocType = new Document_Type__c(Name = 'Legal - APF',
                                                            Applicable_for_APF__c = true);
        insert objDocType;
        
        
        APF_Document_Checklist__c objAPFDC = new APF_Document_Checklist__c(Document_Master__c = objDocMaster.Id, Document_Type__c = objDocType.Id, Mandatory__c = true, Valid_for_Project_Type__c = 'New');
        insert objAPFDC;
        
        Project__c objProject = new Project__c();
        objProject.Assigned_Sales_User__c = assignedSalesUser != null ? assignedSalesUser.Id : null;
        objProject.Account__c = acc.Id;
        objProject.Project_Name__c = 'Test Project';
        objProject.APF_Status__c = 'APF Initiated';
        objProject.Stage__c = 'APF Initiation';
        objProject.Sub_Stage__c = 'Project Data Capture';
        objProject.APF_Type__c = 'New';
        objProject.Address_Line_1__c = 'Test Address';
        objProject.Pincode__c = objPincode.Id;
        insert objProject;
		
        Tower__c objTower = new Tower__c(Project__c = objProject.Id, Name = 'Villa 1');
            insert objTower;
        
		ProjectInventoryApprovalCallHandler.countUnApprovedInventories(objProject.Id);
        Id obj;
		ProjectInventoryApprovalCallHandler.countUnApprovedInventories(obj);
        ProjectInventoryApprovalCallHandler.sendForApproval(objProject.Id);
        ProjectInventoryApprovalCallHandler.sendForApproval(obj);
		testLoggingCLass objt = new testLoggingCLass();
        objt.Method1();
	   
	}
}