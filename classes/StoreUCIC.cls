@RestResource(urlMapping='/storeUCICId/*')
global with sharing class StoreUCIC {
/*
* @Description  :- This method insert the UCIC ID on customer, which is coming through UCIC callback response after UCIC Callout.
* @author       :- Chitransh Porwal
* @returns      :- ResultResponse
*/
    @HttpPost
    global static ResultResponse insertUCICId(){
        UCICResponseBody container = new UCICResponseBody();
        container = (UCICResponseBody)System.JSON.deserialize(RestContext.request.requestBody.tostring(),UCICResponseBody.class);
        system.debug('container  ' + container.Customer.size());
        Map<String,String> CustomerToUCICId = new Map<String,String>();
        if(!container.Customer.isEmpty()){
            
            //*************Code to update the UCICsequence field for LMS_Application_ID*******************************//
            String appSequenceId = container.Application_id;
            String appId;
            Integer seq = 0;
            Integer updatedSeq;
            if(appSequenceId.containsAny('_')){
                system.debug('appSequenceId ' + appSequenceId);
                appId = appSequenceId.substringBefore('_');
                system.debug('appId ' + appId);
                seq = integer.valueof(appSequenceId.substringAfter('_'));
                updatedSeq = seq + 1;
                system.debug('seq ' + seq);
            }
            else{
                appId = appSequenceId;
                updatedSeq = seq + 1;
            }
           
            if(String.isNotBlank(appId)){
                Loan_Application__c loanApp = [SELECT Id,UCICApp_Sequence__c,Retrigger_UCIC__c,(SELECT Id,Applicant_Type__c,Business_Date_of_UCIC_Initiation__c FROM Loan_Contacts__r) FROM Loan_Application__c WHERE Loan_Number__c =: appId LIMIT 1];
                							   //Loan Contact child relationship query added by Abhilekh on 1st October 2020 for UCIC Phase II
                							   //Retrigger_UCIC__c added by Abhilekh on 1st October 2020 for UCIC Phase II
                							   
                Id UCICRecordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get(Constants.UCIC).getRecordTypeId(); //Added by Abhilekh on 1st October 2020 for UCIC Phase II
                
                List<Customer_Integration__c> lstCI = [SELECT Id,UCIC_Response_Status__c FROM Customer_Integration__c WHERE RecordTypeId =: UCICRecordTypeId AND Loan_Application__c =: loanApp.Id ORDER BY CreatedDate DESC LIMIT 1]; //Added by Abhilekh on 1st October 2020 for UCIC Phase II
                
                lstCI[0].UCIC_Response_Status__c = true; //Added by Abhilekh on 1st October 2020 for UCIC Phase II
                update lstCI[0]; //Added by Abhilekh on 1st October 2020 for UCIC Phase II
                
                system.debug('loanApp' + loanApp);
                loanApp.UCICApp_Sequence__c = updatedSeq;
                loanApp.Retrigger_UCIC__c = false; //Added by Abhilekh on 1st October 2020 for UCIC Phase II
                update loanApp;
                system.debug('loanApp' + loanApp);
                
                Date dt = LMSDateUtility.lmsDateValue;
                List<Loan_Contact__c> lstLCToUpdate = new List<Loan_Contact__c>();
                
                /******Below code added by Abhilekh on 1st October 2020 for UCIC Phase II*******************/
                for(Loan_Contact__c lc: loanApp.Loan_Contacts__r){
                    if(lc.Applicant_Type__c == Constants.Applicant){
                        lc.Business_Date_of_UCIC_Initiation__c = dt;
                    	lstLCToUpdate.add(lc);
                    }
                }
                
                if(lstLCToUpdate.size() > 0){
                    update lstLCToUpdate;
                }
                /******Above code added by Abhilekh on 1st October 2020 for UCIC Phase II*******************/
            }
            
            for(UCICResponseBody.cls_Customer Individual : container.Customer){
                CustomerToUCICId.put(Individual.Customer_id,Individual.HUID);
            } 
        }
        system.debug('CustomerToUCICId  ' + CustomerToUCICId);
        ResultResponse resRep = new ResultResponse();
                
        if(!CustomerToUCICId.isEmpty()){
            List<Account> customers = [SELECT Id, Customer_ID__c, UCICId__c FROM Account where Customer_ID__c in: CustomerToUCICId.keySet()];
            if(!customers.isEmpty()){
                for(Account customer : customers){
                    customer.UCICId__c = String.isNotBlank(CustomerToUCICId.get(customer.Customer_ID__c)) ? CustomerToUCICId.get(customer.Customer_ID__c) : '';  
                    system.debug('customer.UCICId__c   ' + customer.UCICId__c);
                }
                Boolean success = true;
                List<Database.SaveResult> results = Database.update(customers, false);
                for(Database.SaveResult result : results){
                    if(!result.getErrors().isEmpty()){
                        resRep.ErrorMsg += result.getErrors();
                        system.debug('result.getErrors()  ' + result.getErrors());
                        success = false;
                    }
                }
                system.debug('Result' + success);
                if(success){
                    resRep.Result = 'Sucess!!';
                    
                }
                else{
                    resRep.Result = 'Failed!!';
                }
            }
            
        }
        HttpResponse res = new HttpResponse();
        res.setBody(String.valueOf(resRep)); 
        LogUtility.createIntegrationLogs(RestContext.request.requestBody.tostring(),res,'UCIC Callback Response');
        return resRep;
        
    }
    global class ResultResponse{
        String ErrorMsg;
        String Result;
        public ResultResponse(){
            ErrorMsg = '{}';
            Result = '';
        }
    }
}