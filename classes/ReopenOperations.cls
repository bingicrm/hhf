public class ReopenOperations {
    
    @AuraEnabled
    public static Loan_Application__c getLoanApp(Id lappId){
        system.debug('=='+lappId);
        return [SELECT Id, Name, StageName__c, Sub_Stage__c, Rejected_Cancelled_Stage__c,Rejected_Cancelled_Sub_Stage__c,Property_Identified__c/*** Added by Abhishek for TIL-2322 ***/, Loan_Status__c/*** Added by Saumya for Loan Status ***/, OwnerId,Insurance_Loan_Application__c, Assigned_Sales_User__c, RecordTypeID, Date_of_Rejection__c, Date_of_Cancellation__c, Remarks_Revival__c,Rejection_Reason__c,Severity_Level__c,Re_Open_Date__c,Reopen_Count__c,/*** Added by Saumya forReject Revive WIP modification ***/ 
                (Select Id, Recordtype.DeveloperName, Recordtypeid, Applicant_Type__c /*** Added by Saumya for New UCIC II Enhancements ***/ from Loan_Contacts__r)
                FROM Loan_Application__c
                WHERE Id = :lappId ];
    }
    
    @AuraEnabled
    public static String reopenApplication(loan_application__c la){
        
        Savepoint sp = Database.setSavepoint();
        String msg;
        msg='';
        system.debug('UserInfo.getUserId()::'+UserInfo.getUserId()+'la.OwnerId::'+la.OwnerId);
        List<Reject_Revive_Master__mdt> lstofRejectRevivemdt =[Select Id,Re_Open_Stage__c,Reopen__c,Re_Open_Sub_Stage__c,Revive_Stage__c,Revive_Sub_Stage__c,Cancelled_Reject_Stage__c from Reject_Revive_Master__mdt where IsActive__c=true and Cancelled_Reject_Stage__c =: la.Rejected_Cancelled_Sub_Stage__c];
        system.debug('lstofRejectRevivemdt::'+lstofRejectRevivemdt);
        List<Property__c> propertyList = new List<Property__c>([SELECT Id, Loan_Application__c FROM Property__c WHERE Loan_Application__c = :la.Id]); //Added by Abhishek for TIL-2322
        List<Loan_Contact__c> contactList = [SELECT Id, Name FROM Loan_Contact__c WHERE Loan_Applications__c = :la.Id AND Created_at_Customer_Negotiation__c = True]; //Added by Abhishek for TIL-1991
        List<UCIC_Negative_Database__mdt> UCICdblst = [Select Id, Deviation__c, Reject_Data_Match_Count__c, Rejection_Reason__c, Severity_Level__c, UCIC_Database_Name__c from UCIC_Negative_Database__mdt where Active__c = true]; /*** Added by Saumya for New UCIC II Enhancements ***/
        LMS_Bus_Date__c BusinessDt = LMS_Bus_Date__c.getInstance();/*** Added by Saumya for New UCIC II Enhancements ***/
        UCIC_Global__c ucg = UCIC_Global__c.getInstance();/*** Added by Saumya for New UCIC II Enhancements ***/
        Customer_Integration__c cust = new Customer_Integration__c();/*** Added by Saumya for New UCIC II Enhancements ***/
        UCIC_Negative_API__c ucgNeg = UCIC_Negative_API__c.getInstance();/*** Added by Saumya for New UCIC II Enhancements ***/
        /*** Added by Saumya for New UCIC II Enhancements ***/
        //if(ucg.UCIC_Active__c == true){
            Id RecordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get(Constants.UCIC).getRecordTypeId(); 
            Id primaryApplicant;
            for(Loan_Contact__c objCon: la.Loan_Contacts__r){
                if(objCon.Applicant_Type__c=='Applicant'){
                    primaryApplicant =  objCon.Id; 
                }
                
            }
            system.debug('primaryApplicant '+primaryApplicant);
        List<Customer_Integration__c> lstcust = [SELECT Id, UCIC_Negative_API_Response__c, UCIC_Response_Status__c, Negative_UCIC_DB_Found__c FROM Customer_Integration__c WHERE RecordTypeId =: RecordTypeId AND Loan_Application__c =: la.Id  AND Loan_Contact__c =: primaryApplicant order by createdDate DESC LIMIT 1];
        if(lstcust.size() >0){
            cust = lstcust[0];   
        }        //}
        /*** Added by Saumya for New UCIC II Enhancements ***/
        try {
            if(Approval.isLocked(la) == true){
                msg = 'Application have already been submitted for approval.';
            }
            else if(la.Severity_Level__c == '1'){
                msg = 'Revival is not allowed for application whose Severity Level is 1.';
            }
            else if(la.Insurance_Loan_Application__c == true){ //Added By Saumya For Insurance Loan App
                msg = 'Reopen is not allowed for Insurance Loan application.';
            }
			/*** Added by Saumya for New UCIC II Enhancements ***/
            else if(cust != null && cust.Negative_UCIC_DB_Found__c == true){
                msg = 'You can not reopen this Application as Data Found in Negative UCIC Database.';   
            }
            /*** Added by Saumya for New UCIC II Enhancements ***/
            else if(UserInfo.getUserId() != la.OwnerId && !Test.isRunningTest()){
                msg= 'You are not authorized for Reopening.';
            } 
            else if(la.Loan_Status__c == 'Cancelled'){ //Added By Saumya For Loan Status
                msg = 'Application with Cancelled Loan Status cannot be reopened.';
            }
            else if(lstofRejectRevivemdt.size() >0 && !lstofRejectRevivemdt[0].Reopen__c){
                msg = 'Reopen is not allowed at this stage.';
            }
            //**************Added by Abhishek for TIL-1991****************************//
        	else if(Constants.Customer_Negotiation.EqualsIgnoreCase(la.Rejected_Cancelled_Sub_Stage__c) && !contactList.isEmpty()){
            	msg = 'This case cannot be Reopened. Please revive the case.';
            }
        	//**************End of code added bt Abhishek for TIL-1991****************//
            
        	//**************Added by Abhishek for TIL-2322****************************//
            else if(Constants.Customer_Negotiation.EqualsIgnoreCase(la.Rejected_Cancelled_Sub_Stage__c) && la.Property_Identified__c && propertyList.isEmpty()){
                msg = 'This case cannot be Reopened. Please revive the case.';
            }
        	//**************End of code added by Abhishek for TIL-2322****************//
            else{
                system.debug('@@la.Date_of_Cancellation__c' + la.Date_of_Cancellation__c);
                system.debug('@@la.Date_of_Rejection__c' + la.Date_of_Rejection__c);
                system.debug('@@la.Remarks_Revival__c' + la.Remarks_Revival__c);
                system.debug('@@la.' + la);
                if((la.Date_of_Cancellation__c != null && la.Date_of_Cancellation__c.daysBetween(System.Today()) < 30) || (la.Date_of_Rejection__c != null && la.Date_of_Rejection__c.daysBetween(System.Today()) < 30)){
                    System.debug('la.Rejected_Cancelled_Stage__c'+la.Rejected_Cancelled_Stage__c);
                    /*if((la.Rejected_Cancelled_Stage__c == 'Credit Decisioning' || la.Rejected_Cancelled_Stage__c == 'Operation Control' || la.Rejected_Cancelled_Stage__c == 'Customer Onboarding' || la.Rejected_Cancelled_Sub_Stage__c == 'Hunter Review')/* && la.SUD_count__c == 0){// Added by Saumya For Reject Revive BRD
                        //la.Rejected_Cancelled_Sub_Stage__c == 'Hunter Review' added by Abhilekh on 29th January 2020 for Hunter BRD
                        system.debug('I am in If'+ lstofRejectRevivemdt + 'la.Rejected_Cancelled_Sub_Stage__c'+ la.Rejected_Cancelled_Sub_Stage__c);
                        for(Reject_Revive_Master__mdt objrejectRevive: lstofRejectRevivemdt){
                            if(objrejectRevive.Cancelled_Reject_Stage__c == la.Rejected_Cancelled_Sub_Stage__c ){
                                if(la.Reopen_Count__c != null){
                                    la.Reopen_Count__c = la.Reopen_Count__c+1;
                                }
                                else{
                                    la.Reopen_Count__c = 1; 
                                }
                                la.Re_Open_Date__c = system.now();
                                la.StageName__c = objrejectRevive.Re_Open_Stage__c;
                                la.Sub_Stage__c = objrejectRevive.Re_Open_Sub_Stage__c;
                                update la;
                                
                                //Changes by Mehul to update CD record type
                                List<Loan_Contact__c> lstLoanCon = new List<Loan_Contact__c>(); 
                                for (Loan_Contact__c objLoanCon : [Select Id, Recordtype.DeveloperName, Recordtypeid from Loan_Contact__c where Loan_Applications__C = :la.id ]) {
                                    system.debug('@@before objLoanCon.Recordtype.DeveloperName' + objLoanCon.Recordtype.DeveloperName);
                                    if (objLoanCon.Recordtype.DeveloperName == 'Salaried_DC') {
                                        objLoanCon.Recordtypeid = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByDeveloperName().get('Salaried').getRecordTypeId();
                                        
                                    } else if (objLoanCon.Recordtype.DeveloperName == 'Others_DC') {
                                        objLoanCon.Recordtypeid = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByDeveloperName().get('Others').getRecordTypeId();
                                    }
                                    system.debug('@@after objLoanCon.Recordtype.DeveloperName' + objLoanCon.Recordtype.DeveloperName);
                                    lstLoanCon.add(objLoanCon); 
                                }
                                
                                update lstLoanCon;
                                system.debug('@@@lstLoanCon' + lstLoanCon);
                                msg='Application Successfully reopened';
                            }
                        }
                    }
                    else{*/
                        la.Reopen_Application__c = true;// Added by Saumya For Reject Revive BRD
                        la.Revive_Application__c = false;
                        la.Revive_Reopen_Status__c = 'Submitted for Approval';// Added by Saumya For Reject Revive BRD
                        update la;
                        msg='';// Added by Saumya For Reject Revive BRD
                    //}
                }
                
                else{
                    msg = 'This application cannot be Reopened as it has passed the 30 days limit.';
                }
            }
            
            
            
        } catch (DMLException e) {
            system.debug('@@exception1' + e);
            Database.rollback(sp);
            
        } catch (Exception e) {
            system.debug('@@exception2' + e);
            Database.rollback(sp);
            
            
        }
        return msg;
    }
    
}