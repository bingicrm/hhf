@isTest
public class TestLMS_Utility {
    @TestSetup
    static void setup()
    {
        Account acc=new Account();
        acc.name='TestAccount1';
        acc.phone='9234567667';
        acc.PAN__c='fjauy0916u';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        System.debug(acc.id);
        
        Scheme__c sc=new Scheme__c();
        sc.name='TestScheme';
        sc.Scheme_Code__c='testcode';
        sc.Scheme_Group_ID__c='23131';
        sc.Product_Code__c='HL';
        insert sc;
        System.debug(sc.id);
        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Transaction_type__c='PB';
        lap.Scheme__c=sc.id;
        insert lap;
        System.debug(lap.id);
        
        City__c city=new city__c();
        city.City_ID__c='100000';
        city.Name='TEZPOOR';
        city.Country__c='1';
        city.State__c='5';
        insert city;
        Pincode__c pin=new Pincode__c();
        pin.City_LP__c=city.id;
        pin.ZIP_ID__c='46172';
        insert pin;
        list<Loan_Contact__c> lc=[select id, name from Loan_Contact__c where Loan_Applications__c=:lap.id limit 1];
        system.debug('loan contact'+lc[0]);
        Address__c address=new Address__c();
        address.Address_Line_1__c='abc Street';
        address.Address_Line_2__c='abc Area';
        address.Pincode_LP__c=pin.id;
        address.Type_of_address__c='PERMNENT';
        address.Loan_Contact__c=lc[0].id;
        //insert address;
        //list<customer_integration__c> ci=[select id, name from loan_A where loan_application__c=:lap.id limit 1 ];
        system.debug('loan contact'+lc[0]);
        customer_integration__c ci =new customer_integration__c();
        ci.Loan_Application__c  =lap.id;
        ci.Loan_Contact__c=lc[0].id;
        insert ci;
    }
    @isTest
    public static void TestgenerateLMSID(){
        list<Loan_Application__c> la = [select id,Name,Loan_Application_Number__c from Loan_Application__c limit 1];
        LMS_Utility.LMSIdRequest r=new LMS_Utility.LMSIdRequest();
        r.applicantType='application type';
        r.applicationId='app id';
        r.customerId='cust id';
        r.ErrorMsg='error';
        r.Password='passwd';
        r.RequestID='reqId';
        r.responseDateTime='responseDT';
        r.UserId='userId';
        
                                                     
                TestMockRequest req1=new TestMockRequest(200,
                                                 'Complete',
                                                 JSON.serialize(r),
                                                 null);
                Test.setMock(HttpCalloutMock.class,req1);
                
            
                Test.startTest();
                LMS_Utility.generateLMSID(String.valueOf(la[0].id));
               
                Test.stopTest();
        
        
        
    }
}