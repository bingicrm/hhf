@isTest
private class DownloadAPFLetterControllerTest {
    @testSetup 
    static void setup() {
        List<RecordType> lstRecordType = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Individual'];
        Id builderIndvRecTypeId;
        if(!lstRecordType.isEmpty()) {
            builderIndvRecTypeId = lstRecordType[0].Id;
        }
        
        Account acc = new Account();
            acc.RecordTypeId = builderIndvRecTypeId;
            acc.Name = 'Individual Test Acc';
            acc.PAN__c = 'AAAAA3333B';
            acc.GSTIN__c = '27AAHFG0551H1ZL';
        insert acc;
        
        List<RecordType> lstRecordType2 = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Corporate'];
        Id builderCorpRecTypeId;
        if(!lstRecordType2.isEmpty()) {
            builderCorpRecTypeId = lstRecordType2[0].Id;
        }
        
        List<RecordType> lstIndividualPBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Individual'];
        List<RecordType> lstCorporatePBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Corporate'];
        
        Account acc2 = new Account();
            acc2.RecordTypeId = builderCorpRecTypeId;
            acc2.Name = 'Test Corporate Account';
            acc2.PAN__c = 'AAAAB2222B';
            
        insert acc2;
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83123');
        insert objPincode;
        
        Project__c objProject = new Project__c();
        //objProject.Assigned_Sales_User__c = assignedSalesUser != null ? assignedSalesUser.Id : null;
        objProject.Account__c = acc.Id;
        objProject.Project_Name__c = 'Test Project';
        objProject.APF_Status__c = 'APF Initiated';
        objProject.Stage__c = 'APF Initiation';
        objProject.Sub_Stage__c = 'Project Data Capture';
        objProject.APF_Type__c = 'New';
        objProject.Address_Line_1__c = 'Test Address';
        objProject.Pincode__c = objPincode.Id;
        insert objProject;
        
        Project_Builder__c objProjectBuilderCorp = new Project_Builder__c();
        objProjectBuilderCorp.RecordTypeId = lstCorporatePBId[0].Id;
        objProjectBuilderCorp.Builder_Name__c = acc2.Id;
        objProjectBuilderCorp.Constitution__c = '24';
        objProjectBuilderCorp.Builder_Status__c = 'Resident';
        objProjectBuilderCorp.Contact_Number_Mobile__c = '9999444223';
        objProjectBuilderCorp.PAN_Number__c = 'EGNPS1111T';
        objProjectBuilderCorp.Project__c = objProject.Id; 
        objProjectBuilderCorp.Preferred_Communication_Address__c = 'CURRES'; 
        //objProjectBuilderCorp. =
        Database.insert(objProjectBuilderCorp,false);
        
        acc2.GSTIN__c = '27AAHFG0551H1ZL';
        update acc2;
        
        insert objProjectBuilderCorp;
        
        objProjectBuilderCorp.GSTIN_Number__c = '27AAHFG0551H1ZL';
        //objProjectBuilderCorp.Builder_Role__c = Constants.strPBConstructorMarketer;
        update objProjectBuilderCorp;
        
        
        List<Project_Builder__c> lstProjectBuilder = [SELECT Id, Gender__c, RecordTypeId, Builder_Category__c, GSTIN_Available__c, Name, Date_Of_Birth__c, Builder_Name__r.RecordType.DeveloperName, All_Promoter_Count__c, Builder_Type__c, Class_Of_Activity__c, Builder_Name__c, Builder_Name__r.RecordTypeId, Builder_Status__c, Constitution__c, Builder_Role__c, Contact_Number_Mobile__c, Email__c, Preferred_Communication_Address__c, Address_Line_1__c, Address_Line_2__c, Pincode__c, Promoter_PAN_Validation_Count__c, Promoter_Cibil_Validation_Count__c, Project__c,(Select Id, Name, Name_of_Promoter__c, Email__c, Contact_No_Mobile__c, Preferred_Communication_Address__c, Promoter_Status__c, Address_Line_1__c, Address_Line_2__c, Pincode__c from Promoters__r) From Project_Builder__c Where Project__c =: objProject.Id];
        System.debug('Debug Log for lstProjectBuilder size'+lstProjectBuilder.size());
        String builderIndividualRecordId;
        List<Promoter__c> lstPromoter = new List<Promoter__c>();
        if(!lstProjectBuilder.isEmpty()) {
            for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                objProjectBuilder.GSTIN_Available__c = true;
                objProjectBuilder.GSTIN_Number__c = '27AAHFG0551H1ZL';
                if(objProjectBuilder.Promoters__r.isEmpty() && objProjectBuilder.Builder_Name__r.RecordType.DeveloperName != 'Builder_Individual'){
                    Promoter__c objPromoter = new Promoter__c(Project_Builder__c = objProjectBuilder.Id, Preferred_Communication_Address__c = 'CURRES', PAN_Number__c = 'AAARR1123L', GSTIN_Number__c = '27AAHFG0551H1ZL', GSTIN_Available__c = true);
                    lstPromoter.add(objPromoter);
                }
                else if(objProjectBuilder.Builder_Name__r.RecordType.DeveloperName == 'Builder_Individual') {
                    builderIndividualRecordId = objProjectBuilder.Id;
                    objProjectBuilder.Gender__c = 'M';
                }
            }
            update lstProjectBuilder;
            System.debug('Debug Log for lstPromoter'+lstPromoter.size());
            if(!lstPromoter.isEmpty()) {
                insert lstPromoter;
            }
            System.debug('Debug Log for lstPromoter post insertion from Address Check Method'+lstPromoter);
            update lstPromoter;
            
        }
        
        Builder_Integrations__c objBI = new Builder_Integrations__c();
        objBI.RecordTypeId = Schema.SObjectType.Builder_Integrations__c.getRecordTypeInfosByName().get('GSTIN').getRecordTypeId();
        objBI.Project_Builder__c = objProjectBuilderCorp.Id;
        objBI.Project__c = objProject.Id;
        objBI.GSTIN__c = objProjectBuilderCorp.GSTIN_Number__c;
        objBI.Constitution__c = objProjectBuilderCorp.Constitution__c;
        objBI.GST_Tax_payer_type__c =  'none';
        objBI.Current_Status_of_Registration_Under_GST__c = 'Done';
        objBI.VAT_Registration_Number__c = 'Test';
        objBI.Email__c = 'test@xyz.com';
        objBI.Mobile__c = '9999988898';
        objBI.PAN_Number__c = objProjectBuilderCorp.PAN_Number__c;
        objBI.Trade_Name__c = 'Test';
        objBI.Legal_Name__c = 'Test';
        objBI.Filling_Frequency__c = 'Test';
        objBI.Business_Activity__c = 'Test';
        objBI.GST_Date_Of_Registration__c = Date.today();
        objBI.Cumulative_Sales_Turnover__c = 1000000;
        objBI.Month_Count__c = 12;
        insert objBI;
        
        Promoter_Integrations__c objPI = new Promoter_Integrations__c();
        objPI.RecordTypeId = Schema.SObjectType.Promoter_Integrations__c.getRecordTypeInfosByName().get('GSTIN').getRecordTypeId();
        objPI.Promoter__c = lstPromoter[0].Id;
        objPI.GSTIN__c = lstPromoter[0].GSTIN_Number__c;
        objPI.Constitution__c = '20';
        objPI.GST_Tax_payer_type__c =  'none';
        objPI.Current_Status_of_Registration_Under_GST__c = 'Done';
        objPI.VAT_Registration_Number__c = 'Test';
        objPI.Email__c = 'test@xyz.com';
        objPI.Mobile__c = '9999988898';
        objPI.PAN_Number__c = objProjectBuilderCorp.PAN_Number__c;
        objPI.Trade_Name__c = 'Test';
        objPI.Legal_Name__c = 'Test';
        objPI.Filling_Frequency__c = 'Test';
        objPI.Business_Activity__c = 'Test';
        objPI.GST_Date_Of_Registration__c = Date.today();
        objPI.Cumulative_Sales_Turnover__c = 1000000;
        objPI.Month_Count__c = 12;
        insert objPI;
        
    }
    
    
	private static testMethod void test() {
        Project__c objProject = [Select Id, Name From Project__c Where Project_Name__c = 'Test Project'];
        DownloadAPFLetterController.downloadAPFLetter(objProject.Id);
        
        PageReference pageRef = Page.APF_Letter;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', String.valueOf(objProject.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(objProject);
		LMS_Date_Sync__c abc = new LMS_Date_Sync__c(EOD_BOD__c='N',Current_Date__c=Date.Today());
        insert abc;
        APFLetterExtension testAccPlan = new APFLetterExtension(sc);
	}

}