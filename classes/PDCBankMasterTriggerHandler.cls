public class PDCBankMasterTriggerHandler {
    public static void onBeforeInsert(List<PDC_Bank_Master__c> listPDCBankMaster){
        if(!listPDCBankMaster.isEmpty()){
         for(PDC_Bank_Master__c pdcRecord : listPDCBankMaster){
             String BankName = String.isNotBlank(pdcRecord.Name_Bank__c) ? pdcRecord.Name_Bank__c : ''; 
             String Micr = String.isNotBlank(pdcRecord.MICR__c) ? pdcRecord.MICR__c : '';
             pdcRecord.Name = BankName + '-' + Micr;
        }
       }
    }
    public static void onBeforeUpdate(List<PDC_Bank_Master__c> listPDCBankMaster){
        if(!listPDCBankMaster.isEmpty()){
         for(PDC_Bank_Master__c pdcRecord : listPDCBankMaster){
             String BankName = String.isNotBlank(pdcRecord.Name_Bank__c) ? pdcRecord.Name_Bank__c : ''; 
             String Micr = String.isNotBlank(pdcRecord.MICR__c) ? pdcRecord.MICR__c : '';
             pdcRecord.Name = BankName + '-' + Micr;
        }
       }
    }
}