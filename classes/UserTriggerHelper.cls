public class UserTriggerHelper{
    
 
    
/*@Description :- This method creates the record of DST_Master/DSA_Master and User_Branch_Mapping on AFTER INSERT of User.
* @param :- set of User Ids
* @returns :- void
* @author :- Chitransh Porwal
* @date :- 10-01-2019
*/ 
    @future
    public static void createMasterRecordandUBM(set<id> UsrIds){
        
        if(!UsrIds.isEmpty()){
            system.debug('Entry3');
            List<User> userList = [Select id,
                                   Name,
                                   Email,
                                   Phone,
                                   LastName,
                                   FirstName,	
                                   ManagerId,
                                   Location__c,
                                   MobilePhone,
                                   Profile.Name,
                                   EmployeeNumber,
                                   DSA_Broker_ID__c,
                                   Line_Of_Business__c,
                                   Role_Assigned_Date__c,
                                   Manager_Inspector_ID__c
                                   from User where id in : UsrIds];
            
            set<string> locationList = new set<string>();
            List<Branch_Master__c> bmList = new List<Branch_Master__c>();
            List<DST_Master__c> dstMasterlist = new List<DST_Master__c>();
            List<DST_SM_Master__c> dstSMMasterList = new List<DST_SM_Master__c>();
            List<DSA_Master__c> dsaMasterlist = new List<DSA_Master__c>();
            List<User_Branch_Mapping__c> UBMList = new List<User_Branch_Mapping__c>();
            Map<id,Branch_Master__c> userToBranchMasterIdMap = new Map<id,Branch_Master__c>();
            
            if(!userList.isEmpty()){
                for(User usr : userList){
                    if(String.isNotBlank(usr.Location__c)){
                        locationList.add(usr.Location__c);
                    }
                }
                
                if(!locationList.isEmpty()){
                    bmList = [Select id,Name from Branch_Master__c where Name in : locationList];
                }
                if(!bmList.isEmpty()){
                    for(User usr : userList){
                        for(Branch_Master__c bm : bmList){
                            if(usr.Location__c == bm.Name){
                                userToBranchMasterIdMap.put(usr.Id, bm);
                            }
                        }
                    }  
                }
                
                for(User userRecord : userList){
                    
                    //Create User Branch Mapping Records
                    User_Branch_Mapping__c UBM = new User_Branch_Mapping__c();
                    UBM.User__c = userRecord.Id;
					if(userToBranchMasterIdMap.get(userRecord.Id) != null){
                    UBM.Sourcing_Branch__c =  String.isNotBlank(userToBranchMasterIdMap.get(userRecord.Id).Id) ? userToBranchMasterIdMap.get(userRecord.Id).Id : NULL ;
					}
					else{
					UBM.Sourcing_Branch__c = NULL ;
					}
                    UBM.Servicing_Branch__c = UBM.Sourcing_Branch__c;
                    String userName = (String.isNotBlank(userRecord.FirstName) ? userRecord.FirstName : '')  + ' ' + userRecord.LastName;
					if(userToBranchMasterIdMap.get(userRecord.Id) != null){
                    UBM.Name = (String.isNotBlank(userToBranchMasterIdMap.get(userRecord.Id).Name) ? userToBranchMasterIdMap.get(userRecord.Id).Name + ' ' : '')  +  userName ;
					}
					else{
					UBM.Name = ''+ +  userName ;
					}
                    UBMList.add(UBM);
                    system.debug('UBMList'  +UBMList);
                    
                    //Create DST Master Records
                    if(userRecord.Profile.Name == 'DST' || userRecord.Profile.Name == 'Sales Team'){
                        DST_Master__c dstMasterRecord = new DST_Master__c();
                        dstMasterRecord.Status__c = 'Active';
                        dstMasterRecord.email_id__c = String.isNotBlank(userRecord.Email)  ? userRecord.Email : '';
                        dstMasterRecord.Manager__c = String.isNotBlank(userRecord.ManagerId) ? userRecord.ManagerId : NULL ;
                        dstMasterRecord.Contact_Number__c = String.isNotBlank(userRecord.Phone) ? userRecord.Phone : '' ;
                        dstMasterRecord.LOB__c = String.isNotBlank(userRecord.Line_Of_Business__c) ? userRecord.Line_Of_Business__c : '' ;
                        dstMasterRecord.Name = (String.isNotBlank(userRecord.FirstName) ? userRecord.FirstName : '')  + ' ' + userRecord.LastName;
                        dstMasterRecord.Inspector_ID__c = String.isNotBlank(userRecord.Manager_Inspector_ID__c) ? userRecord.Manager_Inspector_ID__c : '' ;
						if(userToBranchMasterIdMap.get(userRecord.Id) != null){
                        dstMasterRecord.Working_Location__c = String.isNotBlank(userToBranchMasterIdMap.get(userRecord.Id).Id) ? userToBranchMasterIdMap.get(userRecord.Id).Id : NULL ;
						}
						else{
						dstMasterRecord.Working_Location__c = NULL;
						}
                        dstMasterRecord.Employee_Id__c =  String.isNotBlank(userRecord.EmployeeNumber) ? Decimal.valueOf(userRecord.EmployeeNumber).setScale(0) : NULL;
                        dstMasterlist.add(dstMasterRecord);  
                    }
                    //Create Master Records for Sales team [DST-SM]
                     if(userRecord.Profile.Name == 'Sales Team'){
                        DST_SM_Master__c dstSMMasterRecord = new DST_SM_Master__c();
                        dstSMMasterRecord.Status__c = 'Active';
                        dstSMMasterRecord.Name = (String.isNotBlank(userRecord.FirstName) ? userRecord.FirstName : '')  + ' ' + userRecord.LastName;
                        dstSMMasterRecord.Inspector_ID__c = String.isNotBlank(userRecord.Manager_Inspector_ID__c) ? userRecord.Manager_Inspector_ID__c : '' ;
                        dstSMMasterList.add(dstSMMasterRecord);  
                    }
                    //Create DSA Master Records
                    if(userRecord.Profile.Name == 'DSA'){
                        DSA_Master__c dsaMasterRecord = new DSA_Master__c();
                        dsaMasterRecord.Type__c = 'DSA';
                        system.debug('Name' + userRecord.Name);
                        dsaMasterRecord.DSA_Name__c = String.isNotBlank(userRecord.Name) ? userRecord.Name : '';
                        dsaMasterRecord.Name = dsaMasterRecord.Type__c + ' ' + dsaMasterRecord.DSA_Name__c;
                        dsaMasterRecord.Manager__c  = String.isNotBlank(userRecord.ManagerId) ? userRecord.ManagerId : NULL ;
                        dsaMasterRecord.BrokerID__c = String.isNotBlank(userRecord.DSA_Broker_ID__c) ? userRecord.DSA_Broker_ID__c : '';
                        dsaMasterRecord.LOB__c = String.isNotBlank(userRecord.Line_Of_Business__c) ? userRecord.Line_Of_Business__c : '' ;
						if(userToBranchMasterIdMap.get(userRecord.Id) != null){
                        dsaMasterRecord.Location__c = String.isNotBlank(userToBranchMasterIdMap.get(userRecord.Id).Id) ? userToBranchMasterIdMap.get(userRecord.Id).Id : NULL ;
						}
						else{
						dsaMasterRecord.Location__c = NULL;
						}
                        dsaMasterlist.add(dsaMasterRecord);  
                    }
                }
            }
            if(!dsaMasterlist.isEmpty()){
                //insert dsaMasterlist;
                system.debug('dsaMasterlist' + dsaMasterlist);
            }
            if(!UBMList.isEmpty()){
                insert UBMList;
                system.debug('UBM   ' + UBMList);
            }
            if(!dstMasterlist.isEmpty()){
                insert dstMasterlist;
            }
            if(!dstSMMasterList.isEmpty()){
                insert dstSMMasterList;
            }
            
        }
    }
/*@Description :- This method used to insert the USER in a public group as per their Line_Of_Business.
* @param :- List of Users
* @returns :- void
* @author :- Chitransh Porwal
* @date :- 11-01-2019
*/     
    public static void addInPublicGroup(set<id> usrIds){
        
        if(!usrIds.isEmpty()){
            set<id> userRoleIdlist = new set<id>();
            List<User> usrList = [Select id,isActive,Line_Of_Business__c,Profile.Name,UserRoleId from User where id in : usrIds];
             for(User usr: usrList){
                //profileIdlist.add(usr.ProfileId);
                userRoleIdlist.add(usr.UserRoleId);
            }
           // Map<id,Profile> IdToProfileMap = new Map<id,Profile>([Select Id, Name from Profile where Id in : profileIdlist]);
            Map<id,UserRole> IdToRoleMap = new Map<id,UserRole>([select id, name from userrole where id in :userRoleIdList]);
            
            set<string> grpList = new set<string>();
            Map<id,id> UserToGroupIdMap = new Map<id,id>();
            List<Group> publicGroupList = new List<Group>();
            List<GroupMember> GMlist = new List<GroupMember>();
            
            
            for(User usr : usrList){
                if(String.isNotBlank(usr.Line_Of_Business__c)){
                    String lob = usr.Line_Of_Business__c + '%';
                    grpList.add(lob);
                }
                system.debug('PName ' + usr.Profile.Name);
                if(usr.Profile.Name == 'DST' || usr.Profile.Name == 'DSA'){
                   // String grpDST = 'DST';
                    grpList.add(usr.Profile.Name);
                }
                if(IdToRoleMap.get(usr.UserRoleId).Name.startswith('SM') || IdToRoleMap.get(usr.UserRoleId).Name.startswith('CBM')){
                    String grpName = 'SM';
                    grpList.add(grpName);
                }
               // system.debug('grpList' + grpList);
            }
            
            system.debug('grpList' + grpList); 
            
            if(!grpList.isEmpty()){
                publicGroupList = [SELECT Id, Name, DeveloperName FROM Group where Name like : grpList ];
                system.debug('publicGroupList' + publicGroupList);
            }
            if(!publicGroupList.isEmpty()){
                for(User usr : usrList){
                    for(Group g : publicGroupList){
                        String lob = String.isNotBlank(usr.Line_Of_Business__c) ? usr.Line_Of_Business__c : 'L';
                        String gName = String.isNotBlank(g.Name) ? g.Name : 'N';
                        String profileName = usr.Profile.Name;
                        String roleName = IdToRoleMap.get(usr.UserRoleId).Name;
                        if(gName.contains(lob)){
                            UserToGroupIdMap.put(g.Id, usr.Id) ;
                            system.debug('UserToGroupIdMap' + UserToGroupIdMap);
                        }
                        if(gName.contains(profileName)){
                             UserToGroupIdMap.put(g.Id, usr.Id) ;
                            system.debug('UserToGroupIdMap' + UserToGroupIdMap);
                        }
                        if(gName.contains('SM') && (roleName.startswith('SM') || roleName.startswith('CBM'))){
                            UserToGroupIdMap.put(g.Id, usr.Id) ;
                        }
                        
                    }
                }
                
                
                for(User U : usrList) {
                    if(U.isActive) {
                        for(string gId : UserToGroupIdMap.Keyset()){
                            if(U.Id == UserToGroupIdMap.get(gId) ){
                                 GroupMember GM = new GroupMember();
                        		 GM.UserOrGroupId = U.Id;
                                GM.GroupId = gId;
                                 GMList.add(GM); 
                            }
                        }
                       /* GroupMember GM = new GroupMember();
                        
                        GM.UserOrGroupId = U.Id;
                        if(UserToGroupIdMap.containsKey(U.Id) && String.isNotBlank(UserToGroupIdMap.get(U.Id))) {
                            GM.GroupId = UserToGroupIdMap.get(U.Id);
                        }*/
                        
                        
                        //GMList.add(GM);         
                    }
                }
                if(!GMList.isEmpty()) {
                    insert GMList;
                    System.debug('Group Member List is ' + GMList);
                }
            }
        }
    }
}
/*@Description :- This method validates the Location of the User and make a call to createMasterRecordandUBM Method.
* @param :- list of Users
* @returns :- void
* @author :- Chitransh Porwal
* @date :- 12-01-2019
*/     
  /*  public static void checkValidation(List<User> dstList){
        system.debug('CheckValidation');
        set<id> usrIds = new set<id>();
        set<string> branchMasters = new set<string>();
        if(!dstList.isEmpty()){
            List<Branch_Master__c> bmList  =[Select id,Name from Branch_Master__c];
            for(Branch_Master__c bm : bmList){
                branchMasters.add(bm.Name);
            }
            
            for(User usr : dstList){
                if(branchMasters.contains(usr.Location__c)){
                    usrIds.add(usr.Id);
                }
                else{
                    usr.Location__c.addError('Please Enter a valid Location.');
                }
                
            }
            if(!usrIds.isEmpty()){
                createMasterRecordandUBM(usrIds);
            }
           
        }
    }*/
    
    
   /* public static list<groupMember> AddUser(List<User> dstList, string groupName){
        system.debug(dstList);
        List<GroupMember> GMlist = new List<GroupMember>();
        List<Group> lstg = [Select Id, Name, DeveloperName from Group where DeveloperName =: groupName limit 1];
        Group g = new Group();
        if (!lstg.isempty()) {
            g = lstg[0];
        }
        
        for(User u : dstList) {
            if(U.isActive) {
                GroupMember GM = new GroupMember();
                GM.GroupId = g.Id;
                GM.UserOrGroupId = U.Id;
                GMList.add(GM);         
            }
        }
        if(!GMList.isEmpty()) {
            System.debug('Group Member List is ' + GMList);
            return GMList;
        }
        return null;
    }
    
   
  /*  public static void AddLegalUser(List<User> legalList){
        List<GroupMember> GMlist = new List<GroupMember>();
        Group g = [Select Id, Name, DeveloperName from Group where DeveloperName =: 'Legal_Vendors'];
        for(User u : legalList) {
            if(U.isActive) {
                GroupMember GM = new GroupMember();
                GM.GroupId = g.Id;
                GM.UserOrGroupId = U.Id;
                GMList.add(GM);         
            }
        }
        if(!GMList.isEmpty()) {
            System.debug('Group Member List is ' + GMList);
            insert GMList;
        }
    }
    public static void AddTechUser(List<User> techList){
        List<GroupMember> GMlist = new List<GroupMember>();
        Group g = [Select Id, Name, DeveloperName from Group where DeveloperName =: 'Technical_Vendors'];
        for(User u : techList) {
            if(U.isActive) {
                GroupMember GM = new GroupMember();
                GM.GroupId = g.Id;
                GM.UserOrGroupId = U.Id;
                GMList.add(GM);         
            }
        }
        if(!GMList.isEmpty()) {
            System.debug('Group Member List is ' + GMList);
            insert GMList;
        }
    }
    public static void AddFCUUser(List<User> fcuList){
        List<GroupMember> GMlist = new List<GroupMember>();
        Group g = [Select Id, Name, DeveloperName from Group where DeveloperName =: 'FCU_Vendors'];
        for(User u : fcuList) {
            if(U.isActive) {
                GroupMember GM = new GroupMember();
                GM.GroupId = g.Id;
                GM.UserOrGroupId = U.Id;
                GMList.add(GM);         
            }
        }
        if(!GMList.isEmpty()) {
            System.debug('Group Member List is ' + GMList);
            insert GMList;
        }
    }
    public static void AddLIPUser(List<User> lipList){
        List<GroupMember> GMlist = new List<GroupMember>();
        Group g = [Select Id, Name, DeveloperName from Group where DeveloperName =: 'LIP_Vendors'];
        for(User u : lipList) {
            if(U.isActive) {
                GroupMember GM = new GroupMember();
                GM.GroupId = g.Id;
                GM.UserOrGroupId = U.Id;
                GMList.add(GM);         
            }
        }
        if(!GMList.isEmpty()) {
            System.debug('Group Member List is ' + GMList);
            insert GMList;
        }
    }
    public static void AddVPDUser(List<User> VPDList){
        List<GroupMember> GMlist = new List<GroupMember>();
        Group g = [Select Id, Name, DeveloperName from Group where DeveloperName =: 'VPD_Vendors'];
        for(User u : VPDList) {
            if(U.isActive) {
                GroupMember GM = new GroupMember();
                GM.GroupId = g.Id;
                GM.UserOrGroupId = U.Id;
                GMList.add(GM);         
            }
        }
        if(!GMList.isEmpty()) {
            System.debug('Group Member List is ' + GMList);
            insert GMList;
        }
    }
*/