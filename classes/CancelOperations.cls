public class CancelOperations {
    
    @AuraEnabled
    public static Loan_Application__c getLoanApp(Id lappId) {
        
        List<Loan_Application__c> lstLA = [SELECT Id, Name, StageName__c,SUD_count__c,/* Added by Saumya for Reject Revive immediate changes*/ Rejected_Cancelled_Sub_Stage__c, Sub_Stage__c,Insurance_Loan_Application__c/* added by Saumya for insurance Loan **/, Loan_Application_Number__c/* added by Saumya for TIL-00000843**/, Cancellation_Reason__c, Remarks__c, Assigned_Sales_User__c, RecordTypeID, Date_of_Cancellation__c
                                           FROM Loan_Application__c
                                           WHERE Id = :lappId LIMIT 1];
        if(!lstLA.isEmpty()) {
            return lstLA[0] ;
        }
        else{
            return null;
        }
        
    }
    
    @AuraEnabled
    public static String cancelApplication(loan_application__c la){
        List<UCIC_Negative_Database__mdt> UCICdblst = [Select Id, Deviation__c, Reject_Data_Match_Count__c, Rejection_Reason__c, Severity_Level__c, UCIC_Database_Name__c from UCIC_Negative_Database__mdt where Active__c = true]; /*** Added by Saumya for New UCIC II Enhancements ***/
        LMS_Bus_Date__c BusinessDt = LMS_Bus_Date__c.getInstance();/*** Added by Saumya for New UCIC II Enhancements ***/
        UCIC_Negative_API__c ucg = UCIC_Negative_API__c.getInstance();/*** Added by Saumya for New UCIC II Enhancements ***/
        List<Customer_Integration__c> cust = new List<Customer_Integration__c>();/*** Added by Saumya for New UCIC II Enhancements ***/
        /*** Added by Saumya for New UCIC II Enhancements ***/
        // if(ucg.UCIC_Active__c == true){
        Id RecordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get(Constants.UCIC).getRecordTypeId(); 
        /*Id primaryApplicant;
        System.debug('la.Loan_Contacts__r==>'+la.Loan_Contacts__r);
        for(Loan_Contact__c objCon: la.Loan_Contacts__r){
            if(objCon.Applicant_Type__c=='Applicant'){
                primaryApplicant =  objCon.Id; 
            }
            system.debug('primaryApplicant '+primaryApplicant);
        }*/
        cust = [SELECT Id, UCIC_Negative_API_Response__c, UCIC_Response_Status__c, Negative_UCIC_DB_Found__c FROM Customer_Integration__c WHERE RecordTypeId =: RecordTypeId AND Loan_Application__c =: la.Id ORDER BY CreatedDate DESC LIMIT 1];
        /*** Added by Saumya for New UCIC II Enhancements ***/
        //Added by Chitransh for TIL-840 on [27-09-2018]//
        if(Approval.isLocked(la) == true){
            return 'Approval is Pending';
        }
        /*** Added by Saumya for New UCIC II Enhancements ***/
        else if(cust.size() > 0 && cust[0].Negative_UCIC_DB_Found__c == true && ucg.UCIC_Active__c == true && (la.Sub_Stage__c == Constants.Credit_Review || la.Sub_Stage__c == Constants.Re_Credit || la.Sub_Stage__c == Constants.Re_Look)){
            return 'You cannot cancel this Application as Data Found in Negative Dedupe Database.';  
        }
        /*** Added by Saumya for New UCIC II Enhancements ***/
        else if((la.Cancellation_Reason__c.contains('OTHER') || la.Cancellation_Reason__c.contains('other') || la.Cancellation_Reason__c.contains('Other')) && (la.Remarks__c == '' || la.Remarks__c == null)){ // Added by Saumya For Reject Revive BRD for TIL-00001517
            return 'Please fill in Cancellation Reason and Remarks before Cancelling.';
        }
        //End of patch Added by Chitransh for TIL-840 on [27-09-2018]//     
        else{ //Added by Chitransh for TIL-840 on [27-09-2018]//
            Savepoint sp = Database.setSavepoint();
            List<Reject_Revive_Master__mdt> lstofRejectRevivemdt =[Select Id,Re_Open_Stage__c,Cancel__c,Re_Open_Sub_Stage__c,Revive_Stage__c,Revive_Sub_Stage__c,Cancelled_Reject_Stage__c from Reject_Revive_Master__mdt where IsActive__c=true and Cancelled_Reject_Stage__c =: la.Sub_Stage__c];// Added by Saumya For Reject Revive BRD
            if(lstofRejectRevivemdt.size() > 0 && !lstofRejectRevivemdt[0].Cancel__c){ // Added by Saumya For Reject Revive BRD
                return 'Cancellation is not allowed at this stage.';
            }
            else{ // Added by Saumya For Reject Revive BRD
                if((la.StageName__c == 'Credit Decisioning' || la.StageName__c == 'Operation Control' || la.StageName__c == 'Customer Onboarding')/* && la.SUD_count__c == 0*/){// Added by Saumya For Reject Revive BRD
                    
                    List<Third_Party_Verification__c> tpvLst = new List<Third_Party_Verification__c>();
                    List<Third_Party_Verification__c> tpvLstToUpdate = new List<Third_Party_Verification__c>();
                    tpvLst = [SELECT Id, Name, Status__c, Loan_Application__c, Loan_Application__r.Insurance_Loan_Application__c
                              FROM Third_Party_Verification__c
                              WHERE Loan_Application__c=:la.Id AND RecordType.Name != 'Hunter'];
                    
                    for(Third_Party_Verification__c tpv : tpvLst){
                        if(tpv.Status__c == 'New' || tpv.Status__c == 'Assigned' || tpv.Status__c == 'Initiated'){
                            tpv.Status__c = 'Cancelled';
                            tpvLstToUpdate.add(tpv);
                        }
                    }
                    la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Cancelled/Rejected').getRecordTypeId();
                    la.ownerId = la.Assigned_Sales_User__c;
                    la.Date_of_Cancellation__c = System.TODAY();
					la.Rejected_Cancelled_Stage__c = la.StageName__c;
                    la.Rejected_Cancelled_Sub_Stage__c = la.Sub_Stage__c;  
                    la.StageName__c = 'Loan Cancelled';
                    la.Sub_Stage__c = 'Loan Cancel';
                      
                    try {
                        update tpvLstToUpdate;
                        update la;
                        return null;
                    } catch (DMLException e) {
                        Database.rollback(sp);
                        return e.getDmlMessage(0);
                    } catch (Exception e) {
                        Database.rollback(sp);
                        return '';
                    }
                }
                else /*if(la.SUD_count__c == 1)*/{// Added by Saumya For Reject Revive BRD
                    return 'Submit For Approval'; 
                }
                /* else{
return 'Some Error Occured';     
}*/
            }
        }//Added by Chitransh for TIL-840 on [27-09-2018]//
    }
    
    @AuraEnabled
    public static String callApprovalProcess(loan_application__c la){// Added by Saumya For Reject Revive BRD
        String msg;
        msg='';
        la.Rejected_Cancelled_Status__c = 'Submitted for Approval';
        try {
            if(msg == null || msg == ''){
                update la;
                system.debug('=========>>>' + la.recordtypeId);    
            }
            
        } catch (DMLException e) {
            // Database.rollback(sp);
            msg = e.getDmlMessage(0);
            
        } catch (Exception e) {
            //Database.rollback(sp);
            msg = e.getMessage();
            
        }
        return msg;
        
    }
    
    @AuraEnabled
    public static List<String> getPickListValuesIntoList(){
        List<String> pickListValuesList= new List<String>();
        List<Reason_for_Cancellation__c> lstToAdd = new List<Reason_for_Cancellation__c>();
        lstToAdd = [SELECT Id, Name, Status__c, Reason_Type__c
                    FROM Reason_for_Cancellation__c
                    WHERE Reason_Type__c = 'CAN' AND Status__c = 'Active'];
        if(!lstToAdd.isEmpty()){
            for(Reason_for_Cancellation__c rcan : lstToAdd){
                pickListValuesList.add(rcan.Name);
            }
        }
        return pickListValuesList;
    }
}