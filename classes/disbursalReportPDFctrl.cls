/**=====================================================================
 * Deloitte India
 * Name: disbursalReportPDFctrl
 * Description: Controller for disbursalReportPDF
 * Created Date: [10/10/2018]
 * Created By: Abhishek (Deloitte India)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/ 
 public class disbursalReportPDFctrl {
    public Loan_Application__c loanApp{get;set;}  
    public IMD__c IMDTemp{get;set;}  
    public IMD__c Fees{get;set;}
    public Disbursement__c disburseCharges{get;set;}
    public List<Loan_Contact__c> loanContactList{get;set;}  
    public List<Disbursement__c> DisbursementList{get;set;}
    public List<Disbursement_payment_details__c> DisbursementPaymentList{get;set;}
    public List<Third_Party_Verification__c> ThirdPartyVerifList;
    public String techVal1{get;set;}
    public String techVal2{get;set;}
    public List<Document_Checklist__c> DocChecList{get;set;}  
    public string loanAppId{get;set;}  
    public Loan_Contact__c LoanCon{get;set;}  
    public Address__c Address{get;set;}  
    public Sourcing_Detail__c source {get; set;}
    public Repayment_Schedule__c repay {get;set;}
    public list<bank_detail__c> bankDetails {get; set;} 
    public Loan_Repayment__c loanRepay {get; set;}
    public date dtlms {get; set;}
    public list<PDC__c> pdcList {get; set;}
    public list<PDC__c> pdcListInstrument {get; set;}
	public boolean isInsurance {get; set;} /* Added by Saumya For Insurance Loan */																			   

    public disbursalReportPDFctrl(ApexPages.StandardController stdController) {
        loanApp = new Loan_Application__c();
        IMDTemp = new IMD__c();
        Fees = new IMD__c();
        disburseCharges = new Disbursement__c();
        LoanCon = new Loan_Contact__c();
        Address = new Address__c();
        source = new Sourcing_Detail__c();
        loanContactList = new List<Loan_Contact__c>();
        DisbursementList = new List<Disbursement__c>();
        DisbursementPaymentList = new List<Disbursement_payment_details__c>();
        ThirdPartyVerifList = new List<Third_Party_Verification__c>();
        techVal1 = '0';
        techVal2 = '0';
		isInsurance = false; /* Added by Saumya For Insurance Loan*/													 
        DocChecList = new List<Document_Checklist__c>();
        bankDetails = new list<bank_detail__c>();
        repay = new Repayment_Schedule__c();
        loanRepay = new Loan_Repayment__c();
         dtlms = LMSDateUtility.lmsDateValue;
         pdcList = new list<PDC__c>();
         pdcListInstrument = new List<PDC__c>();
		 String addressType; // Added by KK
         list<string> substage = new list<string>();
         substage.add('Application Initiation');
         substage.add('File Check');
         substage.add('Scan: Data Maker');
         substage.add('Credit Review');
         substage.add('COPS: Credit Operations Entry');



        
        loanAppId = ApexPages.CurrentPage().getparameters().get('id');
        
        if(loanAppId != null && loanAppId != ''){
            loanApp = [Select id, Name,Approved_Processing_Fee__c,Approved_Processing_Fees1__c ,Disbursed_Amount__c , Disbursement_Date__c,Approved_Loan_Tenure__c,Insurance_Loan_Application__c/* Added by Saumya For Insurance Loan */, 
            Scheme__r.Product_Code__c, Scheme__r.Name,
			Scheme__r.Reference_Rate__c, 
			Final_Property_Valuation__c, Spread__c, Approved_EMI__c,Business_Date_Modified__c,
            Approved_Loan_Amount__c,Line_Of_Business__c,Sum_Of_IMD__c ,Short_recieve__c, tolabel(Mode_of_Repayment__c),  Branch__c, Customer__r.Name, LTV_amount__c,
            LTV_amount_with_Insurance__c, Bank_Master__r.Bank_Name__c, Bank_Master__r.Bank_address__c, Bank_Master__r.A_c_no__c, tolabel(Installment_Type__c),
            Assigned_Disbursement_maker__c,Assigned_Disbursement_maker__r.Name,Total_PF_Amount__c,Approved_ROI__c,Reference_Rate__c,
            (select id, Customer__r.Name, Applicant_Type__c, Age__c, tolabel(Constitution__c), Customer_segment__c,Phone_Number__c,Preferred_Communication_address__c from Loan_Contacts__r), 
            (select Id, Amount__c, CERSAI_Charges__c,Recordtype.Name, Credit_Days_Interest__c, ROC_Charges__c, Joining_Fees__c, PEMI__c, 
            Stamp_Duty__c,Insurance_funded_by_HHFL__c,Insurance_funded_by_customer__c, legal_vetting__c from IMD__r ), (select id, Loan_Application__r.Customer__r.Name,
             Bank_Master__r.A_c_no__c, Cheque_Amount__c, Bank_Master__r.Bank_Name__c,Bank_Master__r.Bank_address__c, Cheque_Number__c, 
             Cheque_Date__c,CERSAI_Charges__c,PEMI__c,ROC_Charges__c,Stamp_Duty__c,Insurance_funded_by_HHFL__c,
             Insurance_funded_by_customer__c,Legal_Vetting__c,CreatedDate from Disbursement__r where RecordType.DeveloperName != 'Insurance' ORDER BY CreatedDate DESC LIMIT 1), 
             (select Id,Final_Property_Valuation__c from Third_Party_Verifications1__r where RecordType.DeveloperName='Technical' and (Status__c = 'Completed' or Status__c = 'Expired') and (Report_Status__c ='Positive' or Report_Status__c='Refer to Credit') ORDER BY Final_Property_Valuation__c ASC),
            (select Id, Received_stage__c, Document_master__r.name,Document_Name__c, REquest_Date_for_OTC__c,Notes__c,Business_Date_Received_Updated__c,Status__c from 
             Document_Checklists__r WHERE Received_stage__c NOT in :substage OR Status__c='Pending' or status__c = 'PDD' or status__c= 'OTC Approved') ,
             (select DSA_Name__r.name,tolabel(Source_Code__c) from Sourcing_Details__r),
             (select Business_IRR__c,Customer_IRR__c from Repayment_Schedule__r),
             (select EMI_Start_date__c,Installment_Mode__c from Loan_Repayment__r),
             (select Account_Number__c,tolabel(Account_Type__c),Customer_Name__c, Name_of_the_Bank__c, Bank_Branch__c,
             UMRN__c,Debit_account_type__c,IFSC_Code__c,MICR__c,NACH_amount__c,frequency__c,Debit_Type__c,
             Period_From__c,Period_To__c,Until_Cancelled__c from 
             bank_details__r),
             (select Cheque_number__c, Cheque_amount__c, Cheque_Date__c, Bank_Detail__r.Account_Number__c , Nature_of_payment__c,
              UMRN__c,Debit_account_type__c,MICR_r__c,NACH_amount__c,Frequency__c,Debit_Type__c,Period_From__c,Period_To__c,Until_Cancelled__c,
              RecordType.Name,Number_of_NACH_collected__c
              from PDC__r)
             from Loan_Application__c where id =:loanAppId limit 1];                //  tolabel(Mode_of_Payment__c) removed from Disbursement__c
            
            loanContactList.addAll(loanApp.Loan_Contacts__r);
            DisbursementList.addAll(loanApp.Disbursement__r);
            ThirdPartyVerifList.addAll(loanApp.Third_Party_Verifications1__r);
			isInsurance=loanApp.Insurance_Loan_Application__c; // Added by Saumya For Insurance Loan																						

             dtlms = (Date) dtlms;

            if((loanApp.bank_details__r).size()>0)
            bankDetails.addAll(loanApp.bank_details__r);

            if((loanApp.PDC__r).size()>0){
                for(PDC__c pdcrec: loanApp.PDC__r){
                    if(pdcrec.RecordType.Name == 'NACH'){
                        pdcList.add(pdcrec);
                    }
                    else{
                        pdcListInstrument.add(pdcrec);
                    }
                }
            }
            
            if((loanApp.Document_Checklists__r).size()>0)
            DocChecList.addAll(loanApp.Document_Checklists__r);

            if((loanApp.Loan_Repayment__r).size() > 0){
                loanRepay = (loanApp.Loan_Repayment__r)[0];
            }

            if((loanApp.Repayment_Schedule__r).size() > 0){
                repay = (loanApp.Repayment_Schedule__r)[0];
            }

            for(Loan_Contact__c lcon: loanApp.Loan_Contacts__r){
                if(lcon.Applicant_Type__c == 'Applicant'){
                    LoanCon= lcon;
					addressType = lcon.Preferred_Communication_address__c; // Added by KK
                    break; 
                }
            }
            if(LoanCon != null){
            list<Address__c> Address1 = [select id, Address_Line_1__c, Address_Line_2__c, CityLP__c, StateLP__c, Pincode_LP__r.name,Type_of_address__c from Address__c WHERE
                 Loan_Contact__c = :loanCon.id AND Type_of_address__c =: addressType
                 limit 1];
                 
                 if(Address1.size() > 0){
                     Address = Address1[0];
                 }
            }
            
           
            
			/*Added by Saumya if condition For insurance Loan*/
                if(isInsurance){
                techVal1='N/A'; 
                techVal2='N/A';
                }
           /*Added by Saumya if condition For insurance Loan*/			  
            if((loanApp.IMD__r).size() > 0){

                for(IMD__c imd:loanApp.IMD__r){

                    if(imd.Recordtype.name == 'IMD'){
                        IMDTemp = imd;
                    } else if(imd.Recordtype.name == 'Others'){
                            Fees = imd;
                    }
                }
                //IMDTemp = (loanApp.IMD__r)[0];
            }
            if((loanApp.Sourcing_Details__r).size() > 0){
                source = (loanApp.Sourcing_Details__r)[0];
            }
            
            if(DisbursementList.size() > 0){
                disburseCharges = DisbursementList[0];
                
                DisbursementPaymentList = [SELECT Id,Name,Amount__c,Cheque_Date__c,Cheque_Number__c,Cheque_Favouring__c,
                                           Disbursement__c,Disbursement__r.CreatedDate,tolabel(Mode_of_Payment__c),
                                           Bank_Master__r.Bank_Name__c,
                                           Bank_Master__r.Bank_address__c from Disbursement_payment_details__c
                                           WHERE Disbursement__c =: DisbursementList[0].Id];
            }
            if(ThirdPartyVerifList.size() == 1){
				/*Added by Saumya if condition For insurance Loan*/
                if(isInsurance){
                techVal1='N/A'; 
                }
                else{
                techVal1 = String.valueOf(ThirdPartyVerifList[0].Final_Property_Valuation__c);
                }
            }
            else if(ThirdPartyVerifList.size() == 2){
				/*Added by Saumya if condition For insurance Loan*/
                if(isInsurance){
                techVal1='N/A'; 
                techVal2='N/A';
                }
                else{
                techVal1 = String.valueOf(ThirdPartyVerifList[0].Final_Property_Valuation__c);
                techVal2 = String.valueOf(ThirdPartyVerifList[1].Final_Property_Valuation__c);
                }
            }
            else if(ThirdPartyVerifList.size() > 2){
				 /*Added by Saumya if condition For insurance Loan*/
                if(isInsurance){
                techVal1='N/A'; 
                techVal2='N/A';
                }
                else{
                techVal1 = String.valueOf(ThirdPartyVerifList[0].Final_Property_Valuation__c);
                techVal2 = String.valueOf(ThirdPartyVerifList[1].Final_Property_Valuation__c);
                }
            }
        }
        
    }

}