public class UCICOutputScreenController {
	
    @AuraEnabled
    public static UCICOutputWrapper getUCICData(Id laId){
        Loan_Application__c la = [SELECT Id,Name,StageName__c,Sub_Stage__c,Assigned_Credit_Review__c,
                                  Overall_UCIC_Decision__c,Overall_UCIC_Decision_Remarks__c,OwnerId,
                                  Approved_Loan_Amount__c 
                                  FROM Loan_Application__c WHERE Id =: laId];
        List<Loan_Contact__c> lstCD = [SELECT Id,Name,Loan_Applications__c,Applicant_Type__c,Customer__r.Name
                                       FROM Loan_Contact__c WHERE Loan_Applications__c =: laId 
                                       ORDER BY CreatedDate ASC];
        List<Customer_Integration__c> lstCI = [SELECT Id,name,UCIC_URL__c,RecordType.Name,CreatedDate,LastModifiedDate,
                                               UCIC_Response_Status__c,UCIC_Negative_API_Response__c
                                               FROM Customer_Integration__c WHERE Loan_Application__c =: laId
                                               AND RecordType.Name =: Constants.UCIC 
                                               ORDER BY CreatedDate DESC LIMIT 1];
        List<UCIC_Database__c> lstUD = [SELECT Id,Name,Match_Count__c,Recommender_s_Decision__c,
                                        Recommender_s_Remarks__c,Approver_s_Decision__c,Approver_s_Remarks__c,
                                        Customer_Detail__c,Loan_Application__c,Matched_Max_Percentage__c,
                                        Matched_Max_Perc_Rule__c
                                        FROM UCIC_Database__c WHERE Customer_Detail__c IN: lstCD];
        
        if((la.Sub_Stage__c == Constants.Credit_Review || la.Sub_Stage__c == Constants.Re_Credit || la.Sub_Stage__c == Constants.Re_Look) && la.OwnerId == UserInfo.getUserId() && la.Approved_Loan_Amount__c != null){
            String creditAuthorityUser = CreditAuthorityUtility.findCreditAuthority(laId);
            
            if(creditAuthorityUser != null){
                la.Assigned_Credit_Review__c = creditAuthorityUser;
                update la;
            }
        }
        
        UCICOutputWrapper ucicOutput = new UCICOutputWrapper();
        ucicOutput.currentUserId = UserInfo.getUserId();
        ucicOutput.currentUserProfile = [SELECT Id,Name from Profile WHERE Id =: UserInfo.getProfileId()].Name;
        
        if(lstCI.size() > 0){
            if(lstCI[0].UCIC_URL__c != null){
                ucicOutput.UCICURL = lstCI[0].UCIC_URL__c;
            }
            if(lstCI[0].UCIC_Response_Status__c == true){
                ucicOutput.UCICResponseStatus = true;
            }
            if(lstCI[0].UCIC_Negative_API_Response__c == true){
                ucicOutput.UCICResponseDate = String.valueOf(lstCI[0].LastModifiedDate.format('dd-MMM-yyyy hh:mm:ss a'));
                ucicOutput.UCICNegativeAPIResponse = true;
            }
        }
        
        ucicOutput.la = la;
        ucicOutput.lstCD.addAll(lstCD);
        ucicOutput.lstUD.addAll(lstUD);
        
        return ucicOutput;
    }
    
    @AuraEnabled
    public static void saveUCICDatabaseDecision(String UCICData){
        UCICOutputScreenController.UCICOutputWrapper serializedWrapper = (UCICOutputScreenController.UCICOutputWrapper)System.JSON.deserialize(UCICData,UCICOutputScreenController.UCICOutputWrapper.class);
        
        if(serializedWrapper.lstUD.size() > 0){
            update serializedWrapper.lstUD;
        }
        
        system.debug('serializedWrapper.la::'+ serializedWrapper.la);
        if(serializedWrapper.la != null && (serializedWrapper.la.Overall_UCIC_Decision__c != null || serializedWrapper.la.Overall_UCIC_Decision_Remarks__c != null)){
            update serializedWrapper.la;
        }
    }
    
    @AuraEnabled
    public static List < String > getselectOptions(sObject objObject, string fld) {
        system.debug('objObject --->' + objObject);
        system.debug('fld --->' + fld);
        
        List < String > allOpts = new list < String > ();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objObject.getSObjectType();
        
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        
        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values =
            fieldMap.get(fld).getDescribe().getPickListValues();
        
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        return allOpts;
    }
    
    public class UCICOutputWrapper{
        @AuraEnabled public String currentUserId;
        @AuraEnabled public String currentUserProfile;
        @AuraEnabled public String UCICURL;
        @AuraEnabled public String UCICResponseDate;
        @AuraEnabled public Boolean UCICResponseStatus;
        @AuraEnabled public Boolean UCICNegativeAPIResponse;
        @AuraEnabled public Loan_Application__c la;
        @AuraEnabled public List<Loan_Contact__c> lstCD;
        @AuraEnabled public List<UCIC_Database__c> lstUD;
        
        public UCICOutputWrapper(){
            currentUserId = '';
            currentUserProfile = '';
            UCICURL = '';
            UCICResponseDate = '';
            UCICResponseStatus = false;
            UCICNegativeAPIResponse = false;
            la = new Loan_Application__c();
            lstCD = new List<Loan_Contact__c>();
            lstUD = new List<UCIC_Database__c>();
        }
    }
}