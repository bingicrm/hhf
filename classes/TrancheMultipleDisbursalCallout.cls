public class TrancheMultipleDisbursalCallout {
    public static String makeTrancheMultipleDisbursalCallout(Id objTrancheRecordId){
        String returnMsg;
        if(String.isNotBlank(objTrancheRecordId)){
            Id loanID;
            Id customerId;
            Id custDetailId;
            Id cityId;
            Id disbId;
            String disbursalTo = 'LS';
            String createdBy = 'PRABHAT';
            String creditPeriod = '0';
            String principleRecoveryFlag = 'D';
            
            //List to capture details of Rest Service Metadata
            List<Rest_Service__mdt> lstRestService = new List<Rest_Service__mdt>();
            //List to capture details of current Tranche (Tranche__c) and parent Loan Application fields.
            List<Tranche__c> lstTranche = new List<Tranche__c>();
            //List to capture fields of  Customer Details (Loan_Contact__c) and parent Customer (Account) fields. 
            List<Loan_Contact__c> lstCustDetail = new List<Loan_Contact__c>();
            //List to capture fields of Disbursement (Disbursement__c) and related Bank Master (Bank_Detail__c).
            List<Disbursement__c> lstDisb = new List<Disbursement__c>();
            //List to capture fields of Disbursement Payment Details (Disbursement_payment_details__c).
            List<Disbursement_payment_details__c> lstDisbPaymentDets = new List<Disbursement_payment_details__c>();
            //List to capture fields of Customer Bank Details ()
            List<Bank_Detail__c> lstCustBankDetail = new List<Bank_Detail__c>();
            //List to capture fields of City (City__c)
            List<City__c> lstCity = new List<City__c>();
            //List to record ReqPaymentDetails fields.
            List<ReqPaymentDetails> lstReqPaymentDetails = new List<ReqPaymentDetails>();
            //List to fetch the business Date from LMS_Date_Sync__c
            List<LMS_Date_Sync__c> lmsDate = new List<LMS_Date_Sync__c>();
			//Map to add Disbursement Id and Disbursement Record to update Disbursement from Disbursement Payment Detail
            Map<Id,Disbursement__c> mapDisb = new Map<Id,Disbursement__c>(); // Added by Abhilekh Anand on 1st March 2019
            
            Map<String,String> headerMap = new Map<String,String>();
            RequestBody reqBody = new RequestBody();
            ReqDisbursalDetail reqDisbDetail = new ReqDisbursalDetail();
            ResponseBody respBody = new ResponseBody();
            RespDisbursalDetail respDisbDetail = new RespDisbursalDetail();
            ErrResponseBody errRespBody = new ErrResponseBody();
            ErrResponseError errRespErr = new ErrResponseError();
            
            lstRestService = [SELECT 
                              MasterLabel, 
                              QualifiedApiName, 
                              Service_EndPoint__c, 
                              Client_Password__c, 
                              Client_Username__c, 
                              Request_Method__c, 
                              Time_Out_Period__c  
                              FROM   
                              Rest_Service__mdt
                              WHERE  
                              DeveloperName = 'TrancheMultipleDisbursal'
                              LIMIT 1
                             ]; 
            
            lstTranche = [Select 
                          Loan_Application__c,
                          Loan_Application__r.Loan_Number__c,
                          Loan_Application__r.Customer__c,                                           
                          Approved_Disbursal_Amount__c, 
                          Description__c,
                          Repayment_Start_Date__c
                          FROM 
                          Tranche__c 
                          WHERE 
                          Id =:objTrancheRecordId 
                          LIMIT 1];           
            
            if(!(lstTranche.isEmpty())){
                loanID = lstTranche[0].Loan_Application__c;
                customerId = lstTranche[0].Loan_Application__r.Customer__c;
            }
            
            lstCustDetail = [SELECT Id,
                             LMS_Customer_ID__c,
                             Customer__c, 
                             Customer__r.Customer_ID__c
                             FROM
                             Loan_Contact__c
                             WHERE Customer__c =: customerId
                             AND Loan_Applications__c =: loanID
                             AND Applicant_Type__c =: Constants.Applicant];
            
            if(!(lstCustDetail.isEmpty()) && lstCustDetail.size() == 1){
                custDetailId = lstCustDetail[0].Id;
                lstCustBankDetail = [SELECT 
                                     Account_Number__c
                                     FROM
                                     Bank_Detail__c
                                     WHERE 
                                     Loan_Application__c =: loanID];
            }
            
            lstDisb = [SELECT 
                       Id,
                       Bank_Master__c,
                       Bank_Master__r.IFSC_code__c,
                       Bank_Master__r.Bank_ID__c,
                       Bank_Master__r.City__c,
                       Cheque_Amount__c, 
                       Mode_of_Payment__c,
                       FT_Mode__c,
                       Cheque_Date__c,
                       Cheque_Number__c,
                       Cheque_Favouring__c,
                       Remarks__c,
					   Tranche_Disbursal_Date__c
                       FROM
                       Disbursement__c
                       WHERE
                       Tranche__c =: objTrancheRecordId
                       AND
                       Loan_Application__c =: loanID
                      ];    // Tranche_Disbursal_Date__c added by Abhilekh Anand on 1st March 2019

            for(Disbursement__c dis : lstDisb){
                disbId = dis.Id;
				mapDisb.put(dis.Id,dis);    //Added by Abhilekh Anand on 1st March 2019
            }
            system.debug('Disbursement Id====>>'+disbId);
            lstDisbPaymentDets = [SELECT
                                  Id,
                                  Amount__c,
								IFSC__c,//Added by Chitransh for TIL-940 FT IN Tranche//
								Customer_Account_Number__c,
                                  Bank_Master__c,
                                  Bank_Master__r.IFSC_code__c,
                                  Bank_Master__r.Bank_ID__c,
                                  Bank_Master__r.City__c,
                                  Cheque_Date__c,
                                  Cheque_Number__c,
                                  Cheque_Favouring__c,
                                  Mode_of_Payment__c,
								  Type_of_Fund_Transfer__c,
                                  Disbursement__c
                                  FROM
                                  Disbursement_payment_details__c
                                  WHERE
                                  Disbursement__c =: disbId
                                 ];
            system.debug('DisbursementPayment List====>>'+lstDisbPaymentDets);
            if(!lstDisb.isEmpty()){
                cityId = lstDisb[0].Bank_Master__r.City__c;
                if(String.isNotBlank(cityId)){
                    lstCity = [SELECT City_ID__c FROM City__c WHERE ID =:cityId LIMIT 1];    
                }
            }
            
            lmsDate = [SELECT Current_Date__c FROM LMS_Date_Sync__c ORDER BY Name DESC LIMIT 1];
            
            reqBody.unqRequestId = Utility.generateRandomRequestID().subString(7,15);
            reqBody.sourceSystem = 'SFDC-HFL';
            
            if(!(lstRestService.isEmpty())){
                //reqBody.userId = lstRestService[0].Client_Username__c;
                //reqBody.password = lstRestService[0].Client_Password__c;      
                reqBody.userId = 'cointrive';
                reqBody.password = 'zqbAx8rZ0LvWMftg38eTatwjEANYAo/6';                         
            }
            
            if(!(lstTranche.isEmpty())){
                reqDisbDetail.applicationID = lstTranche[0].Loan_Application__r.Loan_Number__c;
                //reqDisbDetail.applicationID = '';
                reqDisbDetail.agreementNo = '';
                reqDisbDetail.disbursalAmount = String.isNotBlank(String.valueOf(lstTranche[0].Approved_Disbursal_Amount__c))? String.valueOf(lstTranche[0].Approved_Disbursal_Amount__c) : '';
                reqDisbDetail.disbursalDate = ((!lmsDate.isEmpty() && lmsDate[0].Current_Date__c != null) ? formatDate(lmsDate[0].Current_Date__c) : '');
                reqDisbDetail.disbusalDesc = lstTranche[0].Description__c;
                reqDisbDetail.principalRecoveryFlag = principleRecoveryFlag;
                reqDisbDetail.repaymentDate = String.isNotBlank(String.valueOf(lstTranche[0].Repayment_Start_Date__c)) ? formatDate(lstTranche[0].Repayment_Start_Date__c) : '';                
                //reqDisbDetail.repaymentDate = (!lmsDate.isEmpty() && lmsDate[0].Current_Date__c != null ? formatDate(lmsDate[0].Current_Date__c) : '');
            }
            
            for(Disbursement_payment_details__c disb : lstDisbPaymentDets){
                ReqPaymentDetails reqPayDetails = new ReqPaymentDetails();
                reqPayDetails.disbursalTo = disbursalTo;                
                reqPayDetails.amount =  String.isNotBlank(String.valueOf(disb.Amount__c)) ? String.valueOf(disb.Amount__c) : '';
                reqPayDetails.creditPeriod = creditPeriod;
                reqPayDetails.paymentMode = (disb.Mode_of_Payment__c != null ? disb.Mode_of_Payment__c : '');
                reqPayDetails.ftMode = (disb.Type_of_Fund_Transfer__c != null ? disb.Type_of_Fund_Transfer__c : '');
				if ( reqPayDetails.ftMode == 'RTGS' ) {
                    reqPayDetails.ftMode = 'RT' ;
                } else if ( reqPayDetails.ftMode == 'NEFT' ) {
                    reqPayDetails.ftMode = 'NT' ;
                }
                //reqPayDetails.ftMode = '';				
                //reqPayDetails.ifscCode = (disb.Bank_Master__r.IFSC_code__c != null ? disb.Bank_Master__r.IFSC_code__c : '');
				reqPayDetails.ifscCode = String.isNotBlank(disb.IFSC__c) ? disb.IFSC__c : '';/****Edited by Chitransh for TIL-940 FT IN Tranche****/// Edited by Chitransh for POC on [22-10-2019] //(disb.Bank_Master__r.IFSC_code__c != null ? disb.Bank_Master__r.IFSC_code__c : '');
                reqPayDetails.chequeDate = String.isNotBlank(String.valueOf(disb.Cheque_Date__c)) ? formatDate(disb.Cheque_Date__c) : '';
                reqPayDetails.chequeNo = (disb.Cheque_Number__c != null ? disb.Cheque_Number__c : '');                
                reqPayDetails.effectiveDate = ((!lmsDate.isEmpty() && lmsDate[0].Current_Date__c != null) ? formatDate(lmsDate[0].Current_Date__c) : ''); 
                //reqPayDetails.accountNo = (!lstCustBankDetail.isEmpty()) ?  lstCustBankDetail[0].Account_Number__c : '';
				reqPayDetails.accountNo = String.isNotBlank(disb.Customer_Account_Number__c) ? disb.Customer_Account_Number__c : '' ;// Edited by Chitransh for POC on [22-10-2019] //(!lstCustBankDetail.isEmpty()) ?  lstCustBankDetail[0].Account_Number__c : '';
                reqPayDetails.disbrsmntBank = (disb.Bank_Master__r.Bank_ID__c != null ? disb.Bank_Master__r.Bank_ID__c : '');
                reqPayDetails.createdBy = createdBy;
                reqPayDetails.inFavourOf = (disb.Cheque_Favouring__c != null ? disb.Cheque_Favouring__c : '');
                //reqPayDetails.remarks = (disb.Remarks__c != null ? disb.Remarks__c : 'NA');
                reqPayDetails.remarks = 'NA';
                reqPayDetails.city = (!lstCity.isEmpty() && lstCity.size() == 1) ? lstCity[0].City_ID__c : '';
                reqPayDetails.bpID = (!lstCustDetail.isEmpty() && lstCustDetail.size() == 1 && lstCustDetail[0].Customer__c != null && lstCustDetail[0].Customer__r.Customer_ID__c != null) ? lstCustDetail[0].Customer__r.Customer_ID__c : '';
                lstReqPaymentDetails.add(reqPayDetails);
            }
            reqDisbDetail.paymentDetails = lstReqPaymentDetails;
            reqBody.disbursalDetail = reqDisbDetail;
            
            
            
            
            
            
            HTTP http = new HTTP();
            HTTPRequest httpReq = new HTTPRequest();
            String endPointURL = (String.isNotBlank(lstRestService[0].Service_EndPoint__c) ? lstRestService[0].Service_EndPoint__c : '');
            String method = (String.isNotBlank(lstRestService[0].Service_EndPoint__c) ? lstRestService[0].Request_Method__c : ''); 
            String jsonRequest = Json.serialize(reqBody);
            Integer timeOut = (Integer.valueOf(lstRestService[0].Time_Out_Period__c) != null ? Integer.valueOf(lstRestService[0].Time_Out_Period__c) : null);
            Blob headerValue = Blob.valueOf(lstRestService[0].Client_Username__c + ':' + lstRestService[0].Client_Password__c);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);              
            headerMap.put('Authorization',authorizationHeader);
            headerMap.put('Username',lstRestService[0].Client_Username__c);
            headerMap.put('Password',lstRestService[0].Client_Password__c);
            headerMap.put('operation-flag','C');
            headerMap.put('accept','application/json');
            headerMap.put('content-Type','application/json');
            headerMap.put('user-id','PRABHAT');                
            
            System.debug('Debug Log for headerMap'+headerMap);
            
            if(String.isBlank(endPointURL)){
                returnMsg = 'End Point URL is Blank in Metadata.';                
            }
            if(String.isBlank(method)){
                returnMsg = 'HTTP Method is Blank in Metadata.';                
            }
            if(timeOut == null){
                returnMsg = 'TimeOut is Blank in Metadata.';                
            }
            if(String.isBlank(jsonRequest)){
                returnMsg = 'jsonRequest is Blank in Metadata.';                
            }
            
            
            httpReq.setEndPoint(endPointURL);
            httpReq.setMethod(method);
            if(headerMap != null){
                for(String headerKey : headerMap.keySet()){
                    httpReq.setHeader(headerKey, headerMap.get(headerKey));
                }    
            }
            httpReq.setBody(jsonRequest);                
            httpReq.setTimeOut(timeOut);
            system.debug(jsonRequest);
            HTTPResponse httpRes = new HTTPResponse();
            
            try{
                httpRes = http.send(httpReq); 
                //Create integration logs
                LogUtility.createIntegrationLogs(jsonRequest,httpRes,endPointURL);
                system.debug('Response Came as : ' + httpRes.getBody());
                if(httpRes.getStatus() == 'OK' && httpRes.getStatusCode() == 200) {
                    respBody = parse(httpRes.getBody());      
                    System.debug('Response Body : >>> : ' + respBody);
                    if(respBody != null) {
                        system.debug('respBody :' + respBody);
                        respDisbDetail = respBody.disbursalDetail;
                        if(respDisbDetail != null && respDisbDetail.errorDescription == 'Successfull.'){
							//Added by Abhilekh Anand on 1st March 2019
							Disbursement__c objDisb = mapDisb.get(lstDisbPaymentDets[0].Disbursement__c);
                            objDisb.Tranche_Disbursal_Date__c = LMSDateUtility.lmsDateValue;
                            Database.update(objDisb);
						
                            returnMsg = 'SUCCESS';    
                        }
                        else{
                            if(respDisbDetail == null) {
                              errRespBody = errParse(httpRes.getBody());
                              if(errRespBody != null) {
                                  errRespErr = errRespBody.error;                        
                                  if(errRespBody.status == 'failure' && errRespErr.errorCode == String.valueOf(500)){
                                      returnMsg = 'Technical - Technical error with source system';
                                  }
                                  else if(errRespBody.status == 'failure' && errRespErr.errorCode == String.valueOf(401)){
                                      returnMsg = 'Security - Add Authorization key in header';
                                  }
                                  else if(errRespBody.status == 'failure' && errRespErr.errorCode == String.valueOf(400)){
                                      returnMsg = 'Bad Request - Bad Request received from source';
                                  }
                                  else{
                                      returnMsg = errRespBody.status + ' : ' + errRespErr.errorCode + ' : ' + errRespErr.errorMessage;
                                  }
                              }
                            } else {
                              system.debug('respDisbDetail.errorDescription : ' + respDisbDetail.errorDescription);
                              returnMsg = respDisbDetail.errorDescription;
                            }
                        }
                    }
                }
                else{
                    errRespBody = errParse(httpRes.getBody());
                    if(errRespBody != null) {
                        errRespErr = errRespBody.error;                        
                        if(errRespBody.status == 'failure' && errRespErr.errorCode == String.valueOf(500)){
                            returnMsg = 'Technical - Technical error with source system';
                        }
                        else if(errRespBody.status == 'failure' && errRespErr.errorCode == String.valueOf(401)){
                            returnMsg = 'Security - Add Authorization key in header';
                        }
                        else if(errRespBody.status == 'failure' && errRespErr.errorCode == String.valueOf(400)){
                            returnMsg = 'Bad Request - Bad Request received from source';
                        }
                        else{
                            returnMsg = errRespBody.status + ' : ' + errRespErr.errorCode + ' : ' + errRespErr.errorMessage;
                        }
                    }
                }
            }
            catch(DmlException ex){
                system.debug('Exception recorded as :'  + ex.getMessage() + ex.getLineNumber());                
                returnMsg = ex.getMessage();
                return returnMsg;
            }
            catch(Exception ex) {
                system.debug('Exception recorded as :'  + ex.getMessage() + ex.getLineNumber());                
                returnMsg = ex.getMessage();
                return returnMsg;
            }
            
        }
        return returnMsg;
    }
    
    //**************Field Mapping**********************************************************************
    //unqRequestId      :       Generate Randomly using utility class (Util.Class)
    //sourceSystem      :       SFDC
    //userId            :       From metadata
    //password          :       From metadata    
    public class RequestBody{
        public String unqRequestId;
        public String sourceSystem;
        public String userId;
        public String password;
        public ReqDisbursalDetail disbursalDetail;
    }       
    
    //**************Disbursal Details******************************************************************
    //applicationID     :       Field on Loan Application (Loan_Number__c) --
    //agreementNo       :       Blank --
    //disbursalAmount   :       Field on Tranche  - Approved_Disbursal_Amount__c --
    //disbursalDate     :       Current Date on Tranche when Stage is Disbursed/at the time of API call --
    //disbusalDesc      :       Field on Tranche - Description__c --
    //principalRecoveryFlag :   Default value as "D" otherwise "S"
    //repaymentDate     :       Business Date
    public class ReqDisbursalDetail {
        public String applicationID;
        public String agreementNo;
        public String disbursalAmount;
        public String disbursalDate;
        public String disbusalDesc;
        public String principalRecoveryFlag;
        public String repaymentDate;
        public List<ReqPaymentDetails> paymentDetails;
    }
    
    //  **************Payment Details******************************************************************
    //disbursalTo       :       Default 'LS' --
    //amount            :       Field on Disbursement - Cheque_Amount__c --
    //creditPeriod      :       Default "0" --
    //paymentMode       :       Field on Disbursement - Mode_of_Payment__c --
    //ftMode            :       Field on Disbursement - FT_Mode__c --
    //ifscCode          :       Field on Bank_Master - IFSC_code__c --
    //chequeDate        :       Field on Disbursement - Cheque_Date__c --
    //chequeNo          :       Field on Disbursement - Cheque_Number__c --
    //effectiveDate     :       Field on LMS_Date_Sync - Current_Date__c
    //accountNo         :       Beneficiary Account Number - Field on Customer Bank Detail - Account_Number__c --
    //disbrsmntBank     :       Field on Bank Master - Bank_ID__c --
    //createdBy         :       Default "PRABHAT" -- 
    //inFavourOf        :       Field on Disbursement  - Cheque_Favouring__c --
    //remarks           :       Field on Disbursement - Remarks__c --
    //city              :       Field on Bank Master - CityId__c <----TBD-----> --
    //bpID              :       Field on Account - Customer_ID__c -- 
    public class ReqPaymentDetails {
        public String disbursalTo;
        public String amount;
        public String creditPeriod;
        public String paymentMode;
        public String ftMode;
        public String ifscCode;
        public String chequeDate;
        public String chequeNo;
        public String effectiveDate;
        public String accountNo;
        public String disbrsmntBank;
        public String createdBy;
        public String inFavourOf;
        public String remarks;
        public String city;
        public String bpID;
    }    
    
    public class ResponseBody {
        public RespDisbursalDetail disbursalDetail;
        public String unqRequestId; //080022
    }
    
    class RespDisbursalDetail {
        public String applicationID;    //3168348
        public String errorcode;    //00000
        public String errorDescription; //Successful.
    }
    
    public class ErrResponseBody{
        public String status;
        public ErrResponseError error;
    }
    
    class ErrResponseError{
        public String errorCode;
        public String errorType;
        public String errorMessage;
    }
    
    public static ResponseBody parse(String json) {
        return (ResponseBody) System.JSON.deserialize(json, ResponseBody.class);
    }
    
    public static ErrResponseBody errParse(String json) {
        return (ErrResponseBody) System.JSON.deserialize(json, ErrResponseBody.class);
    }
    
    public static String formatDate(Date dt){
        if(dt != null) {
            DateTime currDate = dt;
            return currDate.format('dd/MM/yyyy'); 
        } else {
            return '';
        }    
        
    }
}