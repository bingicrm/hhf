public class ReferenceTriggerHandler{

    public static void beforeInsert(List <Reference__c> newList, List <Reference__c> oldList, Map < Id, Reference__c > newMap, Map < Id, Reference__c > oldMap){
        Set<string> num = new Set<String>();
        String lapp;
        List<Reference__c> refer = new List<Reference__c>();
        for(Reference__c ref : newList){
            lapp = ref.Loan_Application__c;
        }

        refer = [SELECT Id, Loan_Application__c, Mobile_No__c FROM Reference__c WHERE Loan_Application__c =: lapp ];
        for(Reference__c referen : refer){
            num.add(referen.Mobile_No__c);
        }
        system.debug('HEREE--->'+num);

        for(Reference__c re : newList){
            if(num.contains(re.Mobile_No__c)){
                re.addError('Duplicate Mobile Number detected.');
            }
            
        }
    }
	
	//Below method added by Abhilekh on 6th February 2020 for TIL-1942
    public static void beforeUpdate(List<Reference__c> newList,Map<Id,Reference__c> oldMap){
        Map<Id,Hunter_Required_Field__mdt> mapHunterFields= new Map<Id,Hunter_Required_Field__mdt>([Select Id, Field__c, Object__c, Critical_Field__c from Hunter_Required_Field__mdt where Object__c = 'Reference__c' and Critical_Field__c = true]);
        
        for(Reference__c ref: newList){
            for(Hunter_Required_Field__mdt objHunterFields: mapHunterFields.values()){
                if(ref.get(objHunterFields.Field__c) != oldMap.get(ref.Id).get(objHunterFields.Field__c)){
                    ref.Retrigger_Hunter__c = true;
                    break;
                }
            }
        }
    }
}