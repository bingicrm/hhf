@isTest
public class TestPanValidationCallOut{
    //public static ID parameter;
    
    public static testMethod void methodTest()
    {
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        Profile p2 = [SELECT Id FROM Profile WHERE Name='DSA'];    
        User u1 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser001@amamama.com',
                     Username = 'puser001@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
                     UserRoleId = r.id);
        insert u1;
           
        system.runAs(u1)
        {           
                Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='9992345333',Date_of_Incorporation__c=Date.newInstance(2018,11,11));
                insert Cob;
                
                
                Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',Scheme_ID__c=11,Product_Code__c='HL');
                insert sc;
                
                Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,Transaction_type__c='PB',Requested_Amount__c=1000,Branch__c='test',Line_Of_Business__c='Open Market',Loan_Purpose__c='20');
                insert LAob;   
                
                Loan_Contact__c LCobject=[SELECT id from Loan_Contact__c where Loan_Applications__c=:LAob.id];
                ID parameter=LCobject.id;
                system.debug('HUNT'+''+parameter);
                
                
                PanValidationCallOut.ResponsePanWrapper rp=new PanValidationCallOut.ResponsePanWrapper();
                rp.ApplicationId='345678';
                rp.ResponseType='RESPONSE';
                rp.RequestReceivedTime='4567';
                rp.AcknowledgementId='3456';
                rp.TxnStatus='SUCCESS';
                rp.NsdlStatus='SUCCESS';
                rp.Pan='AAAAA1111A';
                rp.PanStatus='E';
                rp.LastName='LASTNM';
                rp.FirstName='FIRSTNM';
                rp.MiddleName='MIDDLENM';
                rp.PanTitle='MR';
                rp.LastUpdateDate='03122010';
                rp.Filler1='45678';
                rp.Filler2='34567';
                rp.Filler3='4567';                                    
                                                            
                TestMockRequest req1=new TestMockRequest(100,
                                                 'Complete',
                                                 JSON.serialize(rp),
                                                 null);
                Test.setMock(HttpCalloutMock.class,req1);
                
            
                Test.startTest();
                Customer_Integration__c hold=PanValidationController.validationPanDetails(parameter);
                PanValidationController.getCustomerDetails(parameter);
            	PanValidationController.copyPanDetails(Cob.id, parameter);
                Test.stopTest();
        }
                     
     }
     
  /*  public static testMethod void methodTest2()
    {
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        Profile p2 = [SELECT Id FROM Profile WHERE Name='DSA'];  
        
     
                        User u=new User(Alias = 'standt', Email='standarduser181@testorg.com', 
                              EmailEncodingKey='UTF-8',FirstName='A', LastName='Testing', LanguageLocaleKey='en_US', 
                              LocaleSidKey='en_US', ProfileId = p.Id, 
                              TimeZoneSidKey='America/Los_Angeles', UserName='standarduser181@testorg.com');
                              insert u;
        
                
                system.runAs(u){        
                Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='2345333',Date_of_Incorporation__c=Date.newInstance(2018,11,11));
                insert Cob;
                
                
                Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',Scheme_ID__c=11,Product_Code__c='HL');
                insert sc;
                
                Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,Transaction_type__c='PB',Requested_Amount__c=1000,Branch__c='test',Line_Of_Business__c='Open Market',Loan_Purpose__c='20');
                insert LAob;   
                
                Loan_Contact__c LCobject=[SELECT id from Loan_Contact__c where Loan_Applications__c=:LAob.id];
                ID parameter=LCobject.id;
                system.debug('HUNT'+''+parameter);
                
                
                PanValidationCallOut.ResponsePanWrapper rp=new PanValidationCallOut.ResponsePanWrapper();
                rp.ApplicationId='345678';
                rp.ResponseType='RESPONSE';
                rp.RequestReceivedTime='4567';
                rp.AcknowledgementId='3456';
                rp.TxnStatus='SUCCESS';
                rp.NsdlStatus='SUCCESS';
                rp.Pan='AAAAA1111A';
                rp.PanStatus='E';
                rp.LastName='LASTNM';
                rp.FirstName='FIRSTNM';
                rp.MiddleName='MIDDLENM';
                rp.PanTitle='MR';
                rp.LastUpdateDate='03122010';
                rp.Filler1='45678';
                rp.Filler2='34567';
                rp.Filler3='4567';                                    
                                                            
                TestMockRequest req1=new TestMockRequest(100,
                                                 'Complete',
                                                 JSON.serialize(rp),
                                                 null);
                Test.setMock(HttpCalloutMock.class,req1);
                
            
                Test.startTest();
                Customer_Integration__c hold=PanValidationController.validationPanDetails(parameter);         
                Test.stopTest();
        }
                     
     }*/
}