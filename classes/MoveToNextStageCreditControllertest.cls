@isTest
public class MoveToNextStageCreditControllertest{
    public static testMethod void test1(){
        
        id trancheRecTypeId = Schema.SObjectType.tranche__c.getRecordTypeInfosByName().get('Credit').getRecordTypeId();
        id loanAppRecType = Schema.SObjectType.loan_application__c.getRecordTypeInfosByName().get('Tranche').getRecordTypeId();
        
        Account custObj = new Account();
        custObj.Name = 'Test Customer';
        custObj.Phone = '9999999998';
        database.insert(custObj);
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        database.insert(schemeObj);
        
         
        Loan_Application__c loanAppObj = new Loan_Application__c();
        loanAppObj.RecordTypeId = loanAppRecType;
        loanAppObj.customer__c = custObj.id;
        //loanAppObj.Loan_Application_Number__c = 'LA00001';
        loanAppObj.Loan_Purpose__c = '11';
        loanAppObj.scheme__c = schemeObj.id;
        loanAppObj.Transaction_type__c = 'PB';
        loanAppObj.Requested_Amount__c = 200000;
        loanAppObj.Approved_Loan_Amount__c = 150000;
        loanAppObj.StageName__c = 'Customer Onboarding';
        loanAppObj.sub_stage__c = 'Application Initiation';
        insert loanAppObj;
        
        Tranche__c trObj = new Tranche__c();
        trObj.Loan_Application__c = loanAppObj.id;
       // trObj.Loan_Application__r.Sub_Stage__c = Constants.Tranche_Initiation;
        //trObj.Loan_Application__r.StageName__c = Constants.Tranche;
        trObj.RecordTypeId = trancheRecTypeId;
        trObj.Status__c = 'Initiated';
        trObj.Request_Type__c = 'Test Purpose';
        trObj.Approval_Action__c = 'Approved';
        trObj.Tranche_Stage__c = 'Tranche Processing';
        trObj.Approved_Disbursal_Amount__c = 200000;
        trObj.Pending_Amount2__c = 10000;
        trObj.Disbursal_Amount__c = 60000;
        trObj.Reason__c = 'Test Purpose';
        insert trObj;
        
        loanAppObj.StageName__c = Constants.Tranche;
        loanAppObj.Sub_Stage__c = Constants.Tranche_Initiation;
        Update loanAppObj;
        
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=loanAppObj.Id,Tranche__c=trObj.Id,
                                                                      Status__c=Constants.DOC_STATUS_UPLOADED);
        insert objDocCheck;   
        
        //loanAppObj.Sub_Stage__c = constants.COPS_Data_Maker;
          //      loanAppObj.StageName__c = constants.Operation_Control;
         //update loanAppObj;
         
        test.starttest();
        string comments= 'good';
        MoveToNextStageCreditController.initialValidation(loanAppObj.id); 
        MoveToNextStageCreditController.assignToSalesUser(loanAppObj.id,comments);  
        system.assert(true); 
        test.stoptest();
       
    }
    
        public static testMethod void test3(){
        
        id trancheRecTypeId = Schema.SObjectType.tranche__c.getRecordTypeInfosByName().get('Credit').getRecordTypeId();
        id loanAppRecType = Schema.SObjectType.loan_application__c.getRecordTypeInfosByName().get('Tranche').getRecordTypeId();
        
        Account custObj = new Account();
        custObj.Name = 'Test Customer';
        custObj.Phone = '9999999995';
        database.insert(custObj);
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        database.insert(schemeObj);
        
         
        Loan_Application__c loanAppObj = new Loan_Application__c();
        loanAppObj.RecordTypeId = loanAppRecType;
        loanAppObj.customer__c = custObj.id;
        //loanAppObj.Loan_Application_Number__c = 'LA00001';
        loanAppObj.Loan_Purpose__c = '11';
        loanAppObj.scheme__c = schemeObj.id;
        loanAppObj.Transaction_type__c = 'PB';
        loanAppObj.Requested_Amount__c = 200000;
        loanAppObj.Approved_Loan_Amount__c = 150000;
        loanAppObj.StageName__c = 'Customer Onboarding';
        loanAppObj.sub_stage__c = 'Application Initiation';
        insert loanAppObj;
        
        Tranche__c trObj = new Tranche__c();
        trObj.Loan_Application__c = loanAppObj.id;
       // trObj.Loan_Application__r.Sub_Stage__c = Constants.Tranche_Initiation;
        //trObj.Loan_Application__r.StageName__c = Constants.Tranche;
        trObj.RecordTypeId = trancheRecTypeId;
        trObj.Status__c = 'Initiated';
        trObj.Request_Type__c = 'Test Purpose';
        trObj.Approval_Action__c = 'Approved';
        trObj.Tranche_Stage__c = 'Tranche Processing';
        trObj.Approved_Disbursal_Amount__c = 200000;
        trObj.Pending_Amount2__c = 10000;
        trObj.Disbursal_Amount__c = 60000;
        trObj.Reason__c = 'Test Purpose';
        insert trObj;
        
        loanAppObj.StageName__c = Constants.Tranche;
        loanAppObj.Sub_Stage__c = Constants.Tranche_Initiation;
        Update loanAppObj;
        
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=loanAppObj.Id,Tranche__c=trObj.Id,
                                                                      Status__c=Constants.DOC_STATUS_UPLOADED);
        insert objDocCheck;   
        
            
        test.starttest();
        string comments= 'good';
        MoveToNextStageCreditController.initialValidation(loanAppObj.id); 
        MoveToNextStageCreditController.assignToSalesUser(loanAppObj.id,comments);  
        system.assert(true); 
        //MoveToNextStageCreditController.moveToNextStage(loanAppObj.id);
        MoveToNextStageCreditController.getPersonalDiscussionDetails(loanAppObj.id);
        test.stoptest();
        
    }
    
    }