public class generateLMSIDCompController {
    
    @AuraEnabled
    public static String LMSCheck(String loanId){
        String msg = 'response';
        System.debug('loanId>>>'+loanId);
        if (loanId != null){
            Loan_Application__c la = [select Id,Name,LMSCheck__c from Loan_Application__c WHERE id =: loanId];
            System.debug('loanApp>>>'+la);
            if(la != null){
                if(la.LMSCheck__c == false){
                    la.LMSCheck__c = true;
                    update la;
                    msg = 'updated';
                }
                else{
                    msg = 'checked';
                }
            }
        }      
        System.debug('MSG>>>'+msg);
        return msg;
    }
    
    @AuraEnabled
    public static String checkId(String loanId){
        System.debug('loanId>>>'+loanId);
        String msg;
        if (loanId != null){
            Loan_Application__c la = [select Id,Name,Loan_Number__c from Loan_Application__c WHERE id =: loanId];            
            if(la != null){
                if(la.Loan_Number__c != null){
                    msg ='ID Found>> '+la.Loan_Number__c;
                    System.debug(msg);
                }
                else if(la.Loan_Number__c == null){
                    msg = 'ID not found';
                }
            }     
        }   
        return msg;
    }
    
    @AuraEnabled
    public static String generateId(String loanId){
        string status;
        if(loanId != null){
            LMS_Utility.generateLMSID(loanId);
            status = 'Success';
        }	
        else status = 'Error';
        return status;
    }
}