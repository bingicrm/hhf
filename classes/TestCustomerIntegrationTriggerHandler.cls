@isTest
private class TestCustomerIntegrationTriggerHandler {
    
    static testmethod void test1(){
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Name='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9934567890',RecordTypeId=RecordTypeIdAccount);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Salutation='Mr',FirstName='Test',LastName='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9934567891',RecordTypeId=RecordTypeIdAccount1);
        insert acc1;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='9934567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert objLoanApplication;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Guarantor').getRecordTypeId();
        
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc1.Id,
                                                             Applicant_Type__c='Guarantor',Borrower__c='1',
                                                             RecordTypeId=RecordTypeIdLoanContact,Mobile__c='9876543210',
                                                             Email__c='testemail@gmail.com',Relationship_with_Applicant__c='FTH',
                                                             Category__c='1');
        insert objLoanContact;
        
        Customer_Integration__c objCustIntegration = new Customer_Integration__c(Loan_Application__c=objLoanApplication.Id,
                                                                                Loan_Contact__c=objLoanContact.Id);
        insert objCustIntegration;
        
        List<Customer_Integration__c> lstCustIntegration = new List<Customer_Integration__c>();
        lstCustIntegration.add(objCustIntegration);
        
        Map<Id,Customer_Integration__c> mapCustIntegration = new Map<Id,Customer_Integration__c>();
        mapCustIntegration.put(objCustIntegration.Id,objCustIntegration);
        
        CustomerIntegrationTriggerHandler.AfterInsert(lstCustIntegration,mapCustIntegration);
    }
}