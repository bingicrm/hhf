public class CRM_CaseLastpaymentResponse {
     @AuraEnabled public String AgreementId{get;set;}
     @AuraEnabled public String AgreementNumber{get;set;}
    @AuraEnabled  public BounceDetails bouncedetail{get;set;}
    @AuraEnabled public PaymentDetails paymentdetail{get;set;}
   
     @AuraEnabled public List<PaymentDetails> paymentdetails {get;set;}
        /*public String Amount{get;set;}
        public String PaymentDate {get;set;}
        public String InstrumentType {get;set;}
        public String InstrumentNumber {get;set;}
        public String InstrumentDate {get;set;}
        public String BankName {get;set;}
    }*/
   
    @AuraEnabled public List<BounceDetails> bouncedetails{get;set;}
        /*public String Amount{get;set;}
        public String PaymentDate {get;set;}
        public String InstrumentType {get;set;}
        public String InstrumentNumber {get;set;}
        public String InstrumentDate {get;set;}
        public String BankName {get;set;}
    }*/ 
    
    public class PaymentDetails {
        @AuraEnabled public String Amount {get;set;} 
        @AuraEnabled public String PaymentDate {get;set;}
        @AuraEnabled public String InstrumentType {get;set;}
        @AuraEnabled public String InstrumentNumber {get;set;}
        @AuraEnabled public String InstrumentDate {get;set;}
        @AuraEnabled public String BankName {get;set;}
    } 
    public class BounceDetails{
        @AuraEnabled public String ChequeDate {get;set;}
        @AuraEnabled public String ChequeNumber {get;set;}
        @AuraEnabled public String BounceDate {get;set;}
        @AuraEnabled public String ChequeAmount {get;set;}
        @AuraEnabled public String BankName {get;set;}
        @AuraEnabled public String InstrumentType {get;set;}
    }
   
    public List<PaymentDetails> reportParameterDetailBean{get;set;}
 
    public Object errorMsg;
   
    @AuraEnabled
    public static CRM_CaseLastpaymentResponse parse(String json) {
        system.debug('*******FinalResponse***'+(CRM_CaseLastpaymentResponse) System.JSON.deserialize(json, CRM_CaseLastpaymentResponse.class));
        return (CRM_CaseLastpaymentResponse) System.JSON.deserialize(json, CRM_CaseLastpaymentResponse.class);
       
       
    }
}