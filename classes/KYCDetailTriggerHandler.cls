public class KYCDetailTriggerHandler{

	public static void beforeInsert(list < OCR_Information__c > newList, list < OCR_Information__c > oldList, map < id, OCR_Information__c > newMap, map < id, OCR_Information__c > oldMap) {
        
        Date dt = LMSDateUtility.lmsDateValue;

        for(OCR_Information__c ocr : newList){
            ocr.Business_Date__c = (Date) dt;
        }
    }

}