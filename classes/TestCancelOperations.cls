@isTest
/*
=======================================================================================================================================================
@Purpose :- This Test Class is for the apex class CancelOperations
@Creator :- Deloitte
@CreatedDate :- 20-11-2018
=======================================================================================================================================================
*/
public class TestCancelOperations{
    public static testMethod void CancelOperationsTest(){
        Account custObj = new Account();
        custObj.Name = 'Test Customer';
        custObj.Phone = '9999999990';
        database.insert(custObj);
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        database.insert(schemeObj);
        
         
        Loan_Application__c loanAppObj = new Loan_Application__c();
        loanAppObj.customer__c = custObj.id;
        loanAppObj.Loan_Application_Number__c = 'LA00001';
        loanAppObj.Loan_Purpose__c = '11';
        loanAppObj.scheme__c = schemeObj.id;
        loanAppObj.Transaction_type__c = 'PB';
        loanAppObj.Requested_Amount__c = 50;
        loanAppObj.Cancellation_Reason__c = 'Others';
        loanAppObj.Remarks__c = 'Tset';
        Database.insert(loanAppObj);
        
        Third_Party_Verification__c tpvObj = new Third_Party_Verification__c();
        tpvObj.Loan_Application__c = loanAppObj.id;
        tpvObj.Status__c = 'New';
        //Database.insert(tpvObj);       
        
        Reason_for_Cancellation__c rfcObj = new Reason_for_Cancellation__c();
        rfcObj.Name = 'Reason For Cancellation';
        rfcObj.status__c = 'Active';
        rfcObj.Reason_Type__c = 'CAN';
        Database.insert(rfcObj); 
        
        system.assertNotEquals(loanAppObj.id,null);
        
         CancelOperations.getLoanApp(loanAppObj.id);
         CancelOperations.cancelApplication(loanAppObj);
         CancelOperations.getPickListValuesIntoList();
         CancelOperations.callApprovalProcess(loanAppObj);
    }
    
    public static testMethod void CancelOperationsTest2(){
        Account custObj = new Account();
        custObj.Name = 'Test Customer';
        custObj.Phone = '9999999990';
        database.insert(custObj);
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        database.insert(schemeObj);
        
         
        Loan_Application__c loanAppObj = new Loan_Application__c();
        loanAppObj.customer__c = custObj.id;
        loanAppObj.Loan_Application_Number__c = 'LA00001';
        loanAppObj.Loan_Purpose__c = '11';
        loanAppObj.scheme__c = schemeObj.id;
        loanAppObj.Transaction_type__c = 'PB';
        loanAppObj.Requested_Amount__c = 50;
        loanAppObj.Cancellation_Reason__c = 'Others';
        loanAppObj.Remarks__c = 'Tset';
        loanAppObj.StageName__c = 'Customer Onboarding';
        Database.insert(loanAppObj);
        
        Loan_Contact__c lc = [SELECT Id, Borrower__c from Loan_Contact__c where Loan_Applications__c =: loanAppObj.Id];
        /*lc.Applicant_Type__c = 'Applicant';
        //lc.Borrower__c = '1';
        lc.Constitution__c = '20';
        lc.Customer_segment__c = '1';
        lc.BRE_Record__c = true;
        lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
        lc.Marital_Status__c = 'M';
        lc.Gross_Salary__c = 1000;
        lc.Basic_Pay__c = 1000;
        lc.DA__c = 1000;
        lc.Net_Income__c = 1000;
        lc.PF__c = 1000;
        lc.TDS__c = 1000;
        lc.Current_Organization_Work_Ex_in_Months__c = 12;
        lc.Total_Work_Experience__c = 12;
        lc.Customer_Details_Verified__c = true;
        lc.Voter_ID_Number__c = '123456789';
        lc.Passport_Number__c = '123456789';
        lc.PAN_Number__c = 'BAFPV6543A';
        lc.Aadhaar_Number__c = '898518762736';
        lc.Income_Considered__c = true;
        lc.Customer_Verified__c = true;
        lc.Cibil_Verified__c = true;
        lc.Pan_Verification_status__c = true;
        update lc;*/
        
        Id RecordTypeIdCustUCIC = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('UCIC').getRecordTypeId();
        Customer_Integration__c objcust = new Customer_Integration__c();
        objcust.UCIC_Negative_API_Response__c = true;
        objcust.UCIC_Negative_API_Response__c = false;
        objcust.Loan_Application__c = loanAppObj.Id;
        objcust.Loan_Contact__c = lc.Id;
        objcust.RecordTypeId = RecordTypeIdCustUCIC;
        insert objcust;
        
        DST_Master__c objDSTMaster = new DST_Master__c(Name='Test DST Master', Inspector_ID__c='1234');
        insert objDSTMaster;
        Sourcing_Detail__c objSourceDetail = new Sourcing_Detail__c(Loan_Application__c=loanAppObj.Id,
                                                                    //Sales_User_Name__c=thisUser.Id,
                                                                    DST_Name__c=objDSTMaster.Id);
        insert objSourceDetail;
        
        loanAppObj.StageName__c = 'Operation Control';
        update loanAppObj;
        
        Third_Party_Verification__c tpvObj = new Third_Party_Verification__c();
        tpvObj.Loan_Application__c = loanAppObj.id;
        tpvObj.Status__c = 'New';
        Database.insert(tpvObj);       
        
        Reason_for_Cancellation__c rfcObj = new Reason_for_Cancellation__c();
        rfcObj.Name = 'Reason For Cancellation';
        rfcObj.status__c = 'Active';
        rfcObj.Reason_Type__c = 'CAN';
        Database.insert(rfcObj); 
        
        system.assertNotEquals(loanAppObj.id,null);
        Loan_Application__c loanAppObj2 = [SELECT Id,customer__c,Loan_Application_Number__c,Loan_Purpose__c,scheme__c,Transaction_type__c,
                                           Requested_Amount__c,Cancellation_Reason__c,Remarks__c,StageName__c,Sub_Stage__c,Assigned_Sales_User__c,
                                           (SELECT Id,Applicant_Type__c FROM Loan_Contacts__r)
                                           FROM Loan_Application__c WHERE Id=:loanAppObj.Id];
         //CancelOperations.getLoanApp(loanAppObj.id);
         CancelOperations.cancelApplication(loanAppObj2);
         //CancelOperations.getPickListValuesIntoList();
         //CancelOperations.callApprovalProcess(loanAppObj);
    }
}