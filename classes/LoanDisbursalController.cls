public class LoanDisbursalController {
    public static Id loanId{get;set;}
    public static Loan_Application__c loanRec {get;set;}
    public static wrapLoan loanSection {get;set;}    
    public static List<Disbursement__c> chqList{get;set;}
    public static Integer chqRow{get;set;}
    public static Property__c prop{get;set;}
    public static List<Document_Checklist__c> doc {get;set;} 
    public static Integer count {get;set;}


   public Loan_Application__c loanApp{get;set;}  
    //public IMD__c IMDTemp{get;set;}  
//    public IMD__c Fees{get;set;}  
    public List<Loan_Contact__c> loanContactList{get;set;}  
    public List<Disbursement__c> DisbursementList{get;set;}  
    public List<Document_Checklist__c> DocChecList{get;set;}  
    public list<Property__c> propList {get;set;}
  //  public string loanAppId{get;set;}  
    public Loan_Contact__c LoanCon{get;set;}  
    public list<Address__c> Address{get;set;}  
    public Sourcing_Detail__c source {get; set;}
  //  public Repayment_Schedule__c repay {get;set;}
 //   public list<bank_detail__c> bankDetails {get; set;} 
   // public Loan_Repayment__c loanRepay {get; set;}
    public date dtlms {get; set;}
   // public list<PDC__c> pdcList {get; set;}
    
    
    public LoanDisbursalController(ApexPages.StandardController stdController){
        Id loanId = ApexPages.currentPage().getParameters().get('id');
      
       loanApp = new Loan_Application__c();
       propList = new list<Property__c>();
        
        LoanCon = new Loan_Contact__c();
        Address = new list<Address__c>();
        source = new Sourcing_Detail__c();
        loanContactList = new List<Loan_Contact__c>();
        DisbursementList = new List<Disbursement__c>();
        DocChecList = new List<Document_Checklist__c>();
        list<id> conId = new list<id>();


        loanApp = [Select Name,Loan_Number__c,Sale_Deed_Agreement_Value__c,Approved_Loan_Amount__c,Assigned_Sales_User__r.Name,
                   Credit_Manager_User__r.name,Customer__c,Name_of_BT_Bank__c,Transaction_type__c,Lawyer_Name__c ,Customer__r.Name,
            (select id, Customer__r.Name, Applicant_Type__c, Preferred_Communication_address__c, Age__c, Constitution__c, Customer_segment__c,Phone_Number__c 
            from Loan_Contacts__r), 
             (select id, Loan_Application__r.Customer__r.Name,
             Bank_Master__r.A_c_no__c, Cheque_Amount__c, Bank_Master__r.Bank_Name__c ,Cheque_Number__c  from Disbursement__r),          
             (select Id, Document__c, REquest_Date_for_OTC__c,Document_Collection_Mode__c,Notes__c,Business_Date_Received_Updated__c,Status__c from 
             Document_Checklists__r where Status__c = 'Lawyer OTC') ,
             (select DSA_Name__c, Channel_Partner_Name__c from Sourcing_Details__r),
             (select Property_Type__c,Address_Line_1__c,State__c,Pincode_LP__r.name,City_LP__c,Name_of_the_sellers_1__c 
             from Properties__r)
             from Loan_Application__c where id =:loanId limit 1];              //Mode_of_Payment__c,Cheque_Number__c,Cheque_Date__c removed from Disbursement__c
    
    loanContactList.addAll(loanApp.Loan_Contacts__r);

    if((loanApp.Disbursement__r).size()>0)
            DisbursementList.addAll(loanApp.Disbursement__r);


if((loanApp.Document_Checklists__r).size()>0)
            DocChecList.addAll(loanApp.Document_Checklists__r);

if((loanApp.Sourcing_Details__r).size() > 0){
                source = (loanApp.Sourcing_Details__r)[0];
            }

            if((loanApp.Properties__r).size() >0){
                    propList.addAll(loanApp.Properties__r);
            }

           

            if(loanContactList.size() > 0){
                for(Loan_Contact__c con:loanContactList){

                    conId.add(con.id);
                }
                list<Address__c> Address1  = [select id,Type_of_address__c, Address_Line_1__c, Address_Line_2__c, CityLP__c, StateLP__c, Pincode_LP__r.name, 
                Loan_Contact__c, Loan_Contact__r.customer_name__c,Loan_Contact__r.Preferred_Communication_address__c,Loan_Contact__r.Phone_Number__c from Address__c WHERE
                Loan_Contact__c in :conId ];


                 if(Address1.size() > 0){
                    for(Address__c add: Address1){
                    if(add.Type_of_address__c == add.Loan_Contact__r.Preferred_Communication_address__c)
                      Address.add(add);

                 }
                    }
                 }
               
            





    }
    
    public static void getDisbursement(Id loanId){        
        chqList = [SELECT Cheque_Favouring__c, Cheque_Number__c, Cheque_Amount__c
                   FROM Disbursement__c Where Loan_Application__c =: loanId];
        
        chqRow = chqList.size();        
    }
    
    /*public static void getProperty(Id loanId){
        List<Property__c> lstProp = [SELECT Property_Type__c,House_Flat_Number__c,Floor_Number__c,Building_Number__c,
                                     Building_Society_Name__c,Street__c,Locality__c,Sector_Number_Ward__c,Block_Number__c,Village__c,
                                     Town__c,District__c,City__c,State__c,Country__c, Landmark__c, Pincode__c,Build_Up_Area__c
                                     FROM Property__c where Loan_Applications__c =: loanId]; 
        if(lstProp.size() > 0 && lstProp != null){
            prop = lstProp[0];
        }
    }*/
    
    public static void getDocuments(Id loanId){
        List<Document_Checklist__c> lstDoc = [SELECT REquest_Date_for_OTC__c, Document__c FROM Document_Checklist__c
                                             WHERE Status__c = 'OTC'];
        if(lstDoc != null && lstDoc.size()>0){
            doc = lstDoc;                    
        }
        
    }
    
    public class wrapLoan{
        public String applicantName{get;set;}
        public String coApplicantName{get;set;}
        public String loanAppName{get;set;}
        public String channel{get;set;}
        public Double loanAmt{get;set;}
        public String smName{get;set;}
        public String btNonBt{get;set;}
        public String bcmName{get;set;}
        public String conAdd{get;set;}
        public String conPh{get;set;}
        public String conOwnerName {get;set;}
        public Double saleVal {get;set;}
        public String sellerName {get;set;}
        public String nameOfBT {get;set;}
        public String lawyerName {get;set;}
        
        public wrapLoan(Loan_Application__c loanRec){
            
            applicantName = '';
            coApplicantName='';
            loanAppName = '';
            channel ='';
            loanAmt = 0.0;
            smName = '';
            btNonBt = '';
            bcmName = '';
            conAdd='';
            conPh='';
            conOwnerName = '';
            saleVal = 0.0;
            sellerName = '';
            nameOfBT = '';
            lawyerName = '';
            
            if(loanRec != null){                
                loanAppName = loanRec.Name;
                loanAmt = loanRec.Approved_Loan_Amount__c;
                channel = 'Yet to be decided';
                btNonBt = loanRec.Transaction_type__c;                
                saleVal = loanRec.Sale_Deed_Agreement_Value__c;
                nameOfBT = loanRec.Name_of_BT_Bank__c;
                lawyerName = loanRec.Lawyer_Name__c;
                
                List<Third_Party_Verification__c> lstTPV = [SELECT Seller_Name__c FROM Third_Party_Verification__c 
                                                            WHERE Loan_Application__c =: loanId];
                if(lstTPV != null && lstTPV.size()>0){
                    sellerName = lstTPV[0].Seller_Name__c;
                }
                
                List<Loan_Contact__c> custDetails = [SELECT PAN_First_Name__c,PAN_Last_Name__c,Applicant_Type__c,Phone_Number__c,
                                                     VoterID_Address__c,PP_Address__c,Type_of_Occupant__c,PAN_Middle_Name__c 
                                                     FROM Loan_Contact__c 
                                                     WHERE Customer__c =: loanRec.Customer__c];
                if(custDetails != null && custDetails.size() >0){
                    for(Loan_Contact__c c : custDetails){
                        if(c.Applicant_Type__c == 'Applicant'){                            
                            applicantName = (c.PAN_First_Name__c != null ? c.PAN_First_Name__c : '') + ' ' +
                                            (c.PAN_Middle_Name__c != null ? c.PAN_Middle_Name__c : '' )+ ' ' + 
                                            (c.PAN_Last_Name__c != null ? c.PAN_Last_Name__c : '');    
                            conPh = c.Phone_Number__c;
                            if(c.VoterID_Address__c != null || String.isBlank(c.VoterID_Address__c)){
                                conAdd = c.VoterID_Address__c;
                            }
                            else if(c.PP_Address__c != null || String.isBlank(c.PP_Address__c)){
                                conAdd = c.PP_Address__c;
                            }
                        }
                        else if(c.Applicant_Type__c == 'Co- Applicant'){
                            coApplicantName = (c.PAN_First_Name__c != null ? c.PAN_First_Name__c : '') + ' ' +
                                            (c.PAN_Middle_Name__c != null ? c.PAN_Middle_Name__c : '' )+ ' ' + 
                                            (c.PAN_Last_Name__c != null ? c.PAN_Last_Name__c : '');    
                        }
                    }
                }               
                
                
                List<User> sUser = [SELECT ManagerId FROM User WHERE Id =:loanRec.Assigned_Sales_User__c];
                List<User> bcmUser = [SELECT Id, Name FROM User WHERE Id =: loanRec.Credit_Manager_User__c];
                if(bcmUser != null && bcmUser.size() > 0 ){
                    bcmName = bcmUser[0].Name;
                }
                if(sUser != null && sUser.size()>0){
                    List<User> smUser = [SELECT Id, Name FROM User WHERE Id =: sUser[0].ManagerId];
                    if(smUser != null && smUser.size() > 0){
                        smName = smUser[0].Name;
                    }
                }
                
                
                
            }
            
        }
    }  
    
}