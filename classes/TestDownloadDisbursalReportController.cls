@isTest
private class TestDownloadDisbursalReportController {
    
    private static testmethod void test1(){
        Profile p=[SELECT id,name from profile where name=:'Sales Team'];
        
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        
        List<User> lstBranchManager= new List<User>();                          
        User u1 = new User(
            ProfileId = p.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,Role_in_Sales_Hierarchy__c = 'SM'
        );
        lstBranchManager.add(u1);
        
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
        lstBranchManager.add(u);
        insert lstBranchManager;
        
        User loggedUser=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                         Title,Alias,TimeZoneSidKey,
                         EmailEncodingKey,LanguageLocaleKey,
                         LocaleSidKey,UserRoleId 
                         from user where ID=:UserInfo.getUserId()];
        loggedUser.Branch__c='Delhi';
        loggedUser.Role__c='Application Initiation';
        loggedUser.Line_Of_Business__c='Open Market';
        loggedUser.Branch_Manager__c=lstBranchManager[0].Id;
        loggedUser.ManagerID=lstBranchManager[0].Id;
        update loggedUser;
        
        System.runAs(loggedUser){
            Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='9999966667',
                                     Date_of_Incorporation__c=Date.newInstance(2018,11,11));
            
            Database.insert(Cob);                
            
            Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                        Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                        Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                        Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                        Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
            
            Database.insert(sc); 
            
            Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,
                                                              StageName__c='Loan Disbursal',
                                                              Sub_Stage__c='Disbursement Checker',
                                                              Transaction_type__c='SC',Requested_Amount__c=10000,
                                                              Applicant_Customer_segment__c='Salaried',
                                                              Branch__c='test',Line_Of_Business__c='Open Market',
                                                              Assigned_Sales_User__c=u.id,
                                                              Loan_Purpose__c='20',
                                                              Requested_EMI_amount__c=1000,Approved_ROI__c=5,
                                                              Requested_Loan_Tenure__c=5,
                                                              Government_Programs__c='PMAY',
                                                              Loan_Engine_2_Output__c='STP',
                                                              Property_Identified__c=True,ownerId=loggedUser.id);            
            Database.insert(LAob);
            /*****Added by Amit Agarwal for Test Coverage****/
            Attachment attach=new Attachment();       
            attach.Name='Unit Test Attachment';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attach.body=bodyBlob;
            attach.parentId=LAob.id;
            insert attach;
            
            String retmsg = DownloadDisbursalReportController.downloadDM(LAob.Id);
            System.assertEquals('Success',retmsg);
        }
    }
}