public class ProjectInventoryApprovalCallHandler {
    @AuraEnabled
    public static Integer countUnApprovedInventories(Id projectId) {
        if(projectId != null) {
            //List<Project_Inventory__c> lstPI = [Select Id, Name, Inventory_Approval_Status__c From Project_Inventory__c Where Inventory_Approval_Status__c != 'Approved' AND Project__c =: projectId];
            Integer countUnApprovedInventories = ([Select Id, Name, Inventory_Approval_Status__c From Project_Inventory__c Where Inventory_Approval_Status__c = 'Not Approved' AND Project__c =: projectId]).size();
            return countUnApprovedInventories;
        }
        return 0;
    }
    
    @AuraEnabled 
    public static String sendForApproval(Id projectId) {
        String returnMsg = '';
        system.debug('projectId '+projectId);
        if(projectId != null) {
            Project__c objProject = [Select Id,Name From Project__c Where Id=:projectId Limit 1];
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setComments('Submitting request for approval.');
                req1.setObjectId(objProject.id);// Submit the record to specific process
                req1.setProcessDefinitionNameOrId('Approve_Project_Inventories');
            if(!Test.isRunningTest()){
                Approval.ProcessResult result = Approval.process(req1);
                system.debug('Request submitted' +result); 
                // Verify the result
                system.debug('result.isSuccess() '+result.isSuccess());
                System.assert(result.isSuccess());
                System.assertEquals(
                    'Pending', result.getInstanceStatus(),
                    'Instance Status'+result.getInstanceStatus());
            }
                Set<Id> setUserIdstoShare = new Set<Id>();
                return 'Success';
    
            
        
        }
        system.debug('In else statement');
        return returnMsg;
    }
}