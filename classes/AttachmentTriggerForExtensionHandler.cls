public class AttachmentTriggerForExtensionHandler {
    public static void onBeforeDelete(Map<Id, Attachment> triggerOldMap) {
        
        if(!triggerOldMap.isEmpty()) {
            Map<Id,Document_Migration_Log__c> mapDMLIDtoDML = new Map<Id,Document_Migration_Log__c>([Select Id, Name, Document_Id__c, Successfully_Migrated__c, Loan_Application__c From Document_Migration_Log__c WHERE Document_Id__c IN: triggerOldMap.keySet()]);
            Map<Id, Id> mapAttachmentIdtoDMLId = new Map<Id, Id>();
            if(!mapDMLIDtoDML.isEmpty()) {
                for(Id DMLID : mapDMLIDtoDML.keySet()) {
                    mapAttachmentIdtoDMLId.put(mapDMLIDtoDML.get(DMLID).Document_Id__c,DMLID);
                }
            }
            
            List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();
            /*Map<String,String> objectMap = new Map<String,String>();
            for(Schema.SObjectType f : gd)
            {
                objectMap.put(f.getDescribe().getKeyPrefix(), f.getDescribe().getName());
            }*/
            List<Document_Deletion_Log__c> listDeletedAttachmentsofLoanApp = new List<Document_Deletion_Log__c>();
            for(Attachment objAttachment : triggerOldMap.values()) {
                if(String.isNotBlank(objAttachment.ParentId)){
                    Id parentId = objAttachment.ParentId;
                    //String prefix = //parentId.substring(0,3);
                    String objectName = parentId.getSObjectType().getDescribe().getName();
                    Boolean flag = objectName.equals('Loan_Application__c');
                    system.debug(objectName);
                    if(flag == true){
                        Document_Deletion_Log__c deletedAttachmentsofLoanApp = new Document_Deletion_Log__c(
                            Source_Loan_Application__c = objAttachment.ParentId,
                            Document_Name__c = String.isNotBlank(objAttachment.Name) ? objAttachment.Name : '',
                            Document_Id__c = objAttachment.Id,
                            Document_Size__c = String.valueOf(objAttachment.BodyLength),
                            Deletion_Initiated_as__c = (!mapAttachmentIdtoDMLId.isEmpty() && mapAttachmentIdtoDMLId.containsKey(objAttachment.Id) && String.isNotBlank(mapAttachmentIdtoDMLId.get(objAttachment.Id))) ? 'S3 Document Migration' : 'End User Operation',
                            Deletion_Status__c = 'This file has been deleted. ');
                            
                        listDeletedAttachmentsofLoanApp.add(deletedAttachmentsofLoanApp);
                    }
                    system.debug('listDeletedAttachmentsofLoanApp' + listDeletedAttachmentsofLoanApp);
                    if(!listDeletedAttachmentsofLoanApp.isEmpty()){
                        insert listDeletedAttachmentsofLoanApp;
                    }
                    system.debug(objectName);
                }
            }
        }
    }
}