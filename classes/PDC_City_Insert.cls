@RestResource(urlMapping='/upsertPDCCity/*')
global class PDC_City_Insert {
    
    @HttpPost
    global static ResultResponse createPDCCity(){
        PDCCityResponseBody container = new PDCCityResponseBody();
        container = (PDCCityResponseBody)System.JSON.deserialize(RestContext.request.requestBody.toString(), PDCCityResponseBody.class);
        system.debug('container size ' + container.records.size() );
        ResultResponse resRep = new ResultResponse();
        if(!container.records.isEmpty()){
            list<PDC_Bank_City__c> listcities = new list<PDC_Bank_City__c>();
            for(PDCCityResponseBody.cls_records cityRecord : container.records){
                PDC_Bank_City__c city = new PDC_Bank_City__c();
                city.Name = cityRecord.Name;
                city.City_Id__c = cityRecord.City_Id;
                listcities.add(city);
            }
            system.debug('listcities  '+ listcities);
            // upsert listcities;
            
            Boolean success = true;
            Schema.SObjectField f = PDC_Bank_City__c.Fields.City_Id__c;
            List<Database.UpsertResult> results = Database.upsert(listcities,f, false);
            for(Database.UpsertResult result : results){
                if(!result.getErrors().isEmpty()){
                    resRep.ErrorMsg += result.getErrors();
                    system.debug('result.getErrors()  ' + result.getErrors());
                    success = false;
                }
            }
            system.debug('Result' + success);
            if(success){
                resRep.Result = 'Sucess!!';
                
            }
            else{
                resRep.Result = 'Failed!!';
            }
        }
        
        
        HttpResponse res = new HttpResponse();
        res.setBody(String.valueOf(resRep)); 
        LogUtility.createIntegrationLogs(RestContext.request.requestBody.tostring(),res,'PDC City Callback Response');
        return resRep;
        
        
        
    }
    
    global class ResultResponse{
        String ErrorMsg;
        String Result;
        public ResultResponse(){
            ErrorMsg = '{}';
            Result = '';
        }
    }
    
    
}