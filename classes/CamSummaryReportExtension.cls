public with sharing class CamSummaryReportExtension {
    
    public Loan_Application__c loanApplication{get;set;}
    public List<Customer_Obligations__c> allObligations{get;set;}
    public List<Loan_Contact__c> allLoanContact{get;set;}
    
    public List<Loan_Contact__c> allLoanContactDetails{get;set;}
    public List<Loan_Contact__c> allLoanContactEmployment{get;set;}
    public List<Loan_Contact__c> allLoanContactBusiness{get;set;}
    public List<Loan_Contact__c> allLoanContactShare{get;set;}
    
    public List<Loan_Contact__c> allLoanContacts{get;set;}
    
    public List<Bank_Detail__c> Bank_Details{get;set;}
    public List<Third_Party_Verification__c> allVerificationlist{get;set;}
    public string custintegrationscore{get;set;}
    public Double propertyCost{get;set;}
    public Double marketValue{get;set;}
    public Property__c currProperty{get;set;}
    public String propAddress{get;set;}
    public String propSelected{get;set;}
    public List<String> lstComments{get;set;}
    public List<String> lstMitigants{get;set;}
    public List<ProcessInstance> lstProcessInst;
    public List<ProcessInstanceStep> lstProcessInstStep;
    public Decimal TechnicalValuation1{get;set;}
    public Decimal TechnicalValuation2{get;set;}
    public List<Applicable_Deviation__c> appDevList{get;set;}
    public List<Loan_Sanction_Condition__c> sanctionConditionsList{get;set;}
    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    
    /*
    Last Modified On :                  Last Modified By:                       Modification Description : 
    18 Feb, 2019                        Shobhit Saxena(Deloitte)                Modification was made to fix TIL - 361 raised in Dev 2 regarding the mapping issues.
    */
    public CamSummaryReportExtension(ApexPages.StandardController stdController) 
    {
        allObligations =  new List<Customer_Obligations__c>();
        Bank_Details = new List<Bank_Detail__c>();
        lstComments = new List<String>();
        this.loanApplication = (Loan_Application__c)stdController.getRecord();
        loanApplication = [Select l.Login_Date__c,l.Sum_Of_IMD__c,l.Approved_Processing_Fees1__c,l.Strengths__c,l.Weakness__c,l.Fixed_For_Months__c,l.Employement_Business_details_and_history__c,l.Family_Details_with_relations__c,l.Loan_Details_Eligibilty__c,l.Obligations__c,l.End_Use__c,l.Mitigants__c,l.Recommendation__c,l.Approver_Remarks__c,l.Approved_EMI__c,l.Type_of_Repayment__c,l.Requested_Loan_Tenure__c,l.Frequency__c,l.Appraised_Income__c,l.FOIR_Combined_FOIR__c,l.Comments__c,l.Recommendation_by_Credit__c,l.LTV_amount__c,l.LTV__c,l.Property_Identified__c, l.Remarks__c, l.Loan_Number__c , l.Sub_Stage__c, l.StageName__c,l.Processing_Fee_Percentage__c,l.Approved_Processing_Fee__c ,l.Requested_Processing_Fees1__c , l.Scheme__r.Name,l.Scheme__r.Product_Code__c, l.Scheme__c, l.Requested_Amount__c, l.Loan_Purpose__c, l.Loan_Application_Number__c, l.FOIR__c, l.CreatedDate, l.Branch__c, l.Branch_Lookup__r.Name, l.Branch_Lookup__c, l.Approved_ROI__c, l.Approved_Loan_Tenure__c, l.Approved_Loan_Amount__c, l.Interest_Type__c, l.Final_Property_Valuation__c,
	   (Select Employer_Type__c,Income_Considered__c,Class_Of_Activity__c,Department__c,Retirement_Age__c,Industry__r.Name,Nature_of_Business__c, ShareHolding_Percentage__c,Applicant_Status__c,Qualification__c,Class_Of_Activity__r.Name, Director_Name__c, Website__c,Period_of_Stay_at_Current_address__c,Relationship_with_Applicant__c,Name, Category__c, Customer__c,Customer__r.Name,Customer_segment__c, Date_Of_Birth__c, Declared_income__c, Designation__c, Father_s_Husband_s_Name__c, Gender__c, Gross_Salary__c, KYC__c, Marital_Status__c, Net_Income__c, Net_Salary__c, Appraised_Obligation__c, Pan_Number__c, PAN_First_Name__c, PAN_Last_Name__c,Age__c,Total_Work_Experience__c,Employment_Type__c, Organization_Name__c,  Year_in_Present_Occupation__c, Constitution__c, Total_income__c,Property_Owner__c, BRE_2_Final_Decision__c From Loan_Contacts__r), 
	   (Select Id, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, Cheque_Date__c, Bank_Name__c, Branch__c, Loan_Applications__c, Agreement_ID__c, Cheque_ID__c, Cheque_Status__c, Bounce_Reason_ID__c, Bounce_Reason_Description__c, Failure_Date__c,Amount__c , Realization_Date__c, Remarks__c From IMD__r),
	   (Select Name,Loan_Application__r.Customer_Name__c,Loan_Application__c, Type__c, Insurance_Company__r.Name,Premium__c,Sum_Assured__c From Cross_sells__r),
	   (select Name,First_Property_Owner__c,Address_Line_3__c,Complete_Address__c,Pincode_LP__r.ZIP_ID__c,Pincode_LP__r.Name,City_LP__c,State_LP__c,Country_LP__c,First_Property_Owner__r.Name,Flat_No_House_No__c,Floor__c,Property_Type__c,Type_Property__c,toLabel(Type_of_Property__c),toLabel(Area_Unit__c),Area__c,Address_Line_1__c,Address_line_2__c from Properties__r) ,
	   (SELECT Income_Method_Type__c,Eligible_Loan_Amount__c,Eligible_Tenure__c,Applicable_ROI__c,Applicable_LTV__c,Loan_Application__r.BRE_2_Final_Decision__c, Eligible_Monthly_Income__c, Sum_Obligations__c FROM Application_Eligibilities__r where BRE_II__c = TRUE and Recent__c = TRUE) 
	   From Loan_Application__c l 
	   where l.Id = :loanApplication.Id];//'a000w00000294cAAAQ'];//
	   sanctionConditionsList = [SELECT Comments__c, Status__c, Sanction_Condition__c, Sanction_Condition_Master__c, Sanction_Condition_Master__r.Name, Type_of_Query__c ,Approved_By__c, Approved_Date__c,Customer_Details__c, Customer_Details__r.Customer__r.Name,Createdby.Name,CreatedDate FROM Loan_Sanction_Condition__c WHERE Loan_Application__c = :loanApplication.Id];
        allObligations = [Select c.Write_Off_Flag__c,c.Loan_Amount__c,c.Name_of_the_Financer__c,c.To_be_Considered_as_Obligation__c,c.Repayment_Tenure__c , c.Name, c.Loan_Type__c, c.EMI_Amount__c, c.DPD_3_Month_Max_Peak__c, c.DPD_3_Month_Count__c, c.DPD_12_Month_Max_Peak__c, c.DPD_12_Month_Count__c, c.Customer_Integration__c, c.Customer_Detail__r.Id,c.Customer_Detail__r.Customer__r.Name, c.Customer_Detail__c, c.Current_Status__c, c.Balance__c,Payment_History_Start_Date__c, Payment_History_End_Date__c, Months_Onboard__c, POS__c, Highest_Sanctioned_Amount__c, Balance_Tenure_MOB__c, Final_Obligations_Considered__c, Manual__c, Remarks__c From Customer_Obligations__c c where c.Customer_Detail__r.Loan_Applications__c = :loanApplication.Id];
        //Bank_Details = [select Name,Bank_Name__c,Bank_City__c,Account_Type__c,Account_Number__c,IFSC_Code__c,Customer_Details__r.Customer__r.Name from Bank_Detail__c where Customer_Details__r.Loan_Applications__c = :loanApplication.id ];
        //BRE 2 changes
        List<Customer_Integration__c> custintegration = [select id, CIBIL_Score__c from  Customer_Integration__c where Loan_Contact__r.Loan_Applications__c = :loanApplication.id  order by createddate desc limit 1];
        if(custintegration!=null && custintegration.size()>0)
        custintegrationscore =custintegration[0].CIBIL_Score__c;
        //BRE 2 changes end
        /*lstProcessInst = [SELECT Id,LastActorId,LastActor.Name,Status FROM ProcessInstance WHERE TargetObjectId=:loanApplication.Id];
lstProcessInstStep = [SELECT Id,ActorId,Actor.Name,Comments,OriginalActorId,OriginalActor.Name,StepStatus FROM ProcessInstanceStep 
WHERE ProcessInstanceId IN: lstProcessInst];*/
        if(loanApplication!= null) {
            if(String.isNotBlank(String.valueOf(loanApplication.Final_Property_Valuation__c))) {
                marketValue = loanApplication.Final_Property_Valuation__c;
            }
            propSelected = loanApplication.Property_Identified__c == true ? 'Yes' : 'No';
            
        }
        if( loanApplication.Properties__r.size() > 0 )
        {
            currProperty = loanApplication.Properties__r[0];
            if(String.isNotBlank(currProperty.Address_Line_1__c)) {
                propAddress = currProperty.Address_Line_1__c;
            }
            else if(String.isNotBlank(currProperty.Address_line_2__c)) {
                propAddress = currProperty.Address_line_2__c;
            }
            else {
                propAddress = '';
            }
            
        }
        //BRE 2 changes
        List<Third_Party_Verification__c> allVerification = new List<Third_Party_Verification__c>();
        //List<Third_Party_Verification__c> allVerification = [Select Id, Market_Value__c,Property_Address__c from Third_Party_Verification__c where RecordType.Name  = 'Legal' AND Loan_Application__c = :loanApplication.Id  order by CreatedDate DESC];
        allVerification = [Select Id,Final_Property_Valuation__c,Reviewed_and_Approved__c from Third_Party_Verification__c where RecordType.Name  = 'Technical' AND Status__c= 'completed' AND Loan_Application__c = :loanApplication.Id  order by Final_Property_Valuation__c desc limit 2];
        if(allVerification !=null && allVerification.size()>0)
        TechnicalValuation1 = allVerification[0].Final_Property_Valuation__c;
        if(allVerification !=null && allVerification.size()>1)
        TechnicalValuation2 = allVerification[1].Final_Property_Valuation__c;
        allVerificationlist = [Select Id,Name,RecordType.Name,Status__c,Report_Status__c,Remarks__c,Final_Property_Valuation__c,Reviewed_and_Approved__c from Third_Party_Verification__c where  Loan_Application__c = :loanApplication.Id  ];
        
        //BRE 2 changes end
        if(loanApplication.Loan_Contacts__r.isEmpty())
        {
            allLoanContacts = [Select Id,ShareHolding_Percentage__c,tolabel(Applicant_Status__c) Applicant_Status,Applicant_Status__c,tolabel(Qualification__c) Qualification,Qualification__c,Period_of_Stay_at_Current_address__c,tolabel(Relationship_with_Applicant__c) Relationship_with_Applicant,toLabel(Relationship_with_Applicant__c),
               Name, Category__c, Customer__c,Customer__r.Name,Customer__r.isPersonAccount, tolabel(Customer_segment__c), Date_Of_Birth__c, Declared_income__c, Designation__c,
               Father_s_Husband_s_Name__c, tolabel(Gender__c) Gender,Gender__c, Gross_Salary__c, KYC__c, Marital_Status__c, Property_Owner__c, Net_Income__c, Net_Salary__c, Appraised_Obligation__c, Pan_Number__c, PAN_First_Name__c,
               PAN_Last_Name__c,Age__c,Total_Work_Experience__c, Organization_Name__c,Employment_Type__c,Employer_type__c,tolabel(Nature_of_Business__c) Nature_of_Business,Nature_of_Business__c,Income_Considered__c,Industry__r.Name,Year_in_Present_Occupation__c,
               tolabel(Constitution__c) Constitution,Constitution__c,Applicant_Type__c,Borrower__c,Class_Of_Activity__c,Department__c,
               (Select Customer_Detail__r.BRE_2_Final_Decision__c,Income_Method_Type__c,Id,Eligible_Net_Income__c,Obligations__c,Eligible_Loan_Amount__c from Customer_Detail_Eligibilities__r where BRE_II__c = TRUE and Recent__c = TRUE)
               From Loan_Contact__c where Loan_Applications__c = :loanApplication.Id];//loanApplication.Loan_Contacts__r;
               
               allLoanContact = loanApplication.Loan_Contacts__r;
               
               allLoanContactDetails = [Select Id,ShareHolding_Percentage__c,tolabel(Applicant_Status__c) Applicant_Status,toLabel(Applicant_Status__c),tolabel(Qualification__c) Qualification,toLabel(Qualification__c),Period_of_Stay_at_Current_address__c,tolabel(Relationship_with_Applicant__c) Relationship_with_Applicant,toLabel(Relationship_with_Applicant__c),
               Name, Category__c, Customer__c,Customer__r.Name,Customer__r.isPersonAccount, tolabel(Customer_segment__c), Date_Of_Birth__c, Declared_income__c, Designation__c,
               Father_s_Husband_s_Name__c, tolabel(Gender__c) Gender,toLabel(Gender__c), Gross_Salary__c, KYC__c, Marital_Status__c, Net_Income__c, Net_Salary__c, Appraised_Obligation__c, Pan_Number__c, PAN_First_Name__c,
               PAN_Last_Name__c,Age__c,Total_Work_Experience__c, Organization_Name__c,Employment_Type__c,Employer_type__c,tolabel(Nature_of_Business__c) Nature_of_Business,toLabel(Nature_of_Business__c),Income_Considered__c,Industry__r.Name,Year_in_Present_Occupation__c,
               tolabel(Constitution__c) Constitution,toLabel(Constitution__c),Applicant_Type__c,Borrower__c,Class_Of_Activity__c,Department__c,
               (Select Customer_Detail__r.BRE_2_Final_Decision__c,Income_Method_Type__c,Id,Eligible_Net_Income__c,Obligations__c,Eligible_Loan_Amount__c from Customer_Detail_Eligibilities__r where BRE_II__c = TRUE and Recent__c = TRUE)
               From Loan_Contact__c where tolabel(Constitution__c) =:'INDIVIDUAL' AND Loan_Applications__c = :loanApplication.Id];
               
               allLoanContactEmployment= [Select Retirement_Age__c,Id,ShareHolding_Percentage__c,tolabel(Applicant_Status__c) Applicant_Status,toLabel(Applicant_Status__c),tolabel(Qualification__c) Qualification,toLabel(Qualification__c),Period_of_Stay_at_Current_address__c,tolabel(Relationship_with_Applicant__c) Relationship_with_Applicant,toLabel(Relationship_with_Applicant__c),
               Name, Category__c, Customer__c,Customer__r.Name,Customer__r.isPersonAccount, tolabel(Customer_segment__c), Date_Of_Birth__c, Declared_income__c, Designation__c,
               Father_s_Husband_s_Name__c, tolabel(Gender__c) Gender,toLabel(Gender__c), Gross_Salary__c, KYC__c, Marital_Status__c, Net_Income__c, Net_Salary__c, Appraised_Obligation__c, Pan_Number__c, PAN_First_Name__c,
               PAN_Last_Name__c,Age__c,Total_Work_Experience__c, Organization_Name__c,Employment_Type__c,Employer_type__c,tolabel(Nature_of_Business__c) Nature_of_Business,toLabel(Nature_of_Business__c),Income_Considered__c,Industry__r.Name,Year_in_Present_Occupation__c,
               tolabel(Constitution__c) Constitution,toLabel(Constitution__c),Applicant_Type__c,Borrower__c,Class_Of_Activity__c,Department__c,
               (Select Customer_Detail__r.BRE_2_Final_Decision__c,Income_Method_Type__c,Id,Eligible_Net_Income__c,Obligations__c,Eligible_Loan_Amount__c from Customer_Detail_Eligibilities__r where BRE_II__c = TRUE and Recent__c = TRUE)
               From Loan_Contact__c where (tolabel(Customer_segment__c) =: 'Salaried' OR tolabel(Customer_segment__c) =: 'Cash Salaried' OR tolabel(Customer_segment__c) =: 'Others') AND Loan_Applications__c = :loanApplication.Id];
               
                allLoanContactBusiness= [Select Website__c,Director_Name__c,Class_Of_Activity__r.Name,Id,ShareHolding_Percentage__c,tolabel(Applicant_Status__c) Applicant_Status,toLabel(Applicant_Status__c),tolabel(Qualification__c) Qualification,toLabel(Qualification__c),Period_of_Stay_at_Current_address__c,tolabel(Relationship_with_Applicant__c) Relationship_with_Applicant,toLabel(Relationship_with_Applicant__c),
               Name, Category__c, Customer__c,Customer__r.Name,Customer__r.isPersonAccount, tolabel(Customer_segment__c), Date_Of_Birth__c, Declared_income__c, Designation__c,
               Father_s_Husband_s_Name__c, tolabel(Gender__c) Gender,toLabel(Gender__c), Gross_Salary__c, KYC__c, Marital_Status__c, Net_Income__c, Net_Salary__c, Appraised_Obligation__c, Pan_Number__c, PAN_First_Name__c,
               PAN_Last_Name__c,Age__c,Total_Work_Experience__c, Organization_Name__c,Employment_Type__c,Employer_type__c,tolabel(Nature_of_Business__c) Nature_of_Business,toLabel(Nature_of_Business__c),Income_Considered__c,Industry__r.Name,Year_in_Present_Occupation__c,
               tolabel(Constitution__c) Constitution,toLabel(Constitution__c),Applicant_Type__c,Borrower__c,Class_Of_Activity__c,Department__c,
               (Select Customer_Detail__r.BRE_2_Final_Decision__c,Income_Method_Type__c,Id,Eligible_Net_Income__c,Obligations__c,Eligible_Loan_Amount__c from Customer_Detail_Eligibilities__r where BRE_II__c = TRUE and Recent__c = TRUE)
               From Loan_Contact__c where (tolabel(Customer_segment__c) != 'Salaried' AND tolabel(Customer_segment__c) != 'Cash Salaried') AND Loan_Applications__c = :loanApplication.Id];
        
                allLoanContactShare = [Select Id,ShareHolding_Percentage__c,tolabel(Applicant_Status__c) Applicant_Status,toLabel(Applicant_Status__c),tolabel(Qualification__c) Qualification,toLabel(Qualification__c),Period_of_Stay_at_Current_address__c,tolabel(Relationship_with_Applicant__c) Relationship_with_Applicant,toLabel(Relationship_with_Applicant__c),
               Name, Category__c, Customer__c,Customer__r.Name,Customer__r.isPersonAccount, tolabel(Customer_segment__c), Date_Of_Birth__c, Declared_income__c, Designation__c,
               Father_s_Husband_s_Name__c, tolabel(Gender__c) Gender,toLabel(Gender__c), Gross_Salary__c, KYC__c, Marital_Status__c, Net_Income__c, Net_Salary__c, Appraised_Obligation__c, Pan_Number__c, PAN_First_Name__c,
               PAN_Last_Name__c,Age__c,Total_Work_Experience__c, Organization_Name__c,Employment_Type__c,Employer_type__c,tolabel(Nature_of_Business__c) Nature_of_Business,toLabel(Nature_of_Business__c),Income_Considered__c,Industry__r.Name,Year_in_Present_Occupation__c,
               tolabel(Constitution__c) Constitution,toLabel(Constitution__c),Applicant_Type__c,Borrower__c,Class_Of_Activity__c,Department__c,
               (Select Customer_Detail__r.BRE_2_Final_Decision__c,Income_Method_Type__c,Id,Eligible_Net_Income__c,Obligations__c,Eligible_Loan_Amount__c from Customer_Detail_Eligibilities__r where BRE_II__c = TRUE and Recent__c = TRUE)
               From Loan_Contact__c where Loan_Applications__r.HasCorporateCustomer__c = :true AND Loan_Applications__c = :loanApplication.Id];
        }
        
        
        
        
        /*List<Loan_Application__History> lstLAHistory = [SELECT Id,CreatedBy.Name,CreatedBy.Profile.Name,Field,NewValue,OldValue,IsDeleted from 
                                                          Loan_Application__History WHERE ParentId=:loanApplication.Id AND IsDeleted=False AND Field='Comments__c'];

        for(Loan_Application__History LAH: lstLAHistory){
            if(String.valueOf(LAH.NewValue) != null && String.valueOf(LAH.NewValue).length() > 0 && LAH.CreatedBy.Profile.Name=='Credit Team'){
                Integer startInd;
                Integer endInd;
                Integer lines = String.valueOf(LAH.NewValue).length() / 120;
                if(lines > 0){
                    String commenter='Added by '+LAH.CreatedBy.Name+':';
                    lstComments.add(commenter);
                    for(Integer i=0; i<lines ; i++){
                        startInd = (i==0) ? 120*i : (120*i)+1;
                        endInd = (i+1)*120;
                        String commentLine = String.valueOf(LAH.NewValue).substring(startInd,endInd);
                        lstComments.add(commentLine);
                    }
                    if(String.valueOf(LAH.NewValue).length() - endInd > 0){
                        lstComments.add(String.valueOf(LAH.NewValue).substring(endInd+1,endInd+(String.valueOf(LAH.NewValue).length() - endInd)));
                    }
                }
                else{
                    String comment='Added by '+LAH.CreatedBy.Name+':'+String.valueOf(LAH.NewValue);
                    lstComments.add(comment);
                }
            }
        }
        
        if(loanApplication.Recommendation_by_Credit__c != null && loanApplication.Recommendation_by_Credit__c.length() > 0){
            Integer startInd;
            Integer endInd;
            Integer lines = loanApplication.Recommendation_by_Credit__c.length() / 120;
            if(lines > 0){
                for(Integer i=0; i<lines ; i++){
                    startInd = (i==0) ? 120*i : (120*i)+1;
                    endInd = (i+1)*120;
                    if(i==0){
                        String commentLine = 'Comments by BCM:'+loanApplication.Recommendation_by_Credit__c.substring(startInd,endInd);
                        lstComments.add(commentLine);
                    }
                    else{
                        String commentLine = loanApplication.Recommendation_by_Credit__c.substring(startInd,endInd);
                        lstComments.add(commentLine);
                    }
                }
                if(loanApplication.Recommendation_by_Credit__c.length() - endInd > 0){
                    lstComments.add(loanApplication.Recommendation_by_Credit__c.substring(endInd+1,endInd+(loanApplication.Recommendation_by_Credit__c.length() - endInd)));
                }
            }
            else{
                String comment='Comments by BCM:'+loanApplication.Recommendation_by_Credit__c;
                lstComments.add(comment);
            }
        }*/
        
        for(ProcessInstance pi: [SELECT Id,LastActorId,LastActor.Name,Status FROM ProcessInstance WHERE TargetObjectId=:loanApplication.Id]){
            if(pi.Status=='Approved'){
                for(ProcessInstanceStep pis: [SELECT Id,ActorId,Actor.Name,Actor.Profile.Name,Comments,OriginalActorId,OriginalActor.Name,StepStatus FROM ProcessInstanceStep WHERE ProcessInstanceId =: pi.Id]){
                    String comm='';                             
                    if(pis.Comments != null && pis.Comments != '' && pis.Actor.Profile.Name == 'Credit Team' && !pis.Comments.containsignorecase('automatically using Trigger') ){
                        comm = 'Approved by '+pis.Actor.Name+': '+pis.Comments+';';
                    }
                    lstComments.add(comm);
                }
            }
        }
        
        appDevList = [SELECT Name,Customer_Details__r.Customer__r.Name, Credit_Deviation_Master__c,Credit_Deviation_Master__r.Description__c, Credit_Deviation_Master__r.Name,Credit_Deviation_Master__r.Approver_Level__c , Loan_Application__c, Remarks__c,Approved__c, Mitigant__r.Name,If_Others_please_specify__c FROM Applicable_Deviation__c WHERE Loan_Application__c  =:loanApplication.Id];
        for(Applicable_Deviation__c ad : appDevList){
            lstMitigants = new List<String>();
            if(ad.If_Others_please_specify__c != null && ad.If_Others_please_specify__c.length() > 0){
                Integer sIndex;
                Integer eIndex;
                Integer multipleLines = ad.If_Others_please_specify__c.length() / 90;
                if(multipleLines > 0){
                    for(Integer i=0; i<multipleLines ; i++){
                        sIndex = (i==0) ? 90*i : (90*i)+1;
                        eIndex = (i+1)*90;
                        String mitigantLine = ad.If_Others_please_specify__c.substring(sIndex,eIndex);
                        lstMitigants.add(mitigantLine);
                    }
                    if(ad.If_Others_please_specify__c.length() - eIndex > 0){
                        lstMitigants.add(ad.If_Others_please_specify__c.substring(eIndex+1,eIndex+(ad.If_Others_please_specify__c.length() - eIndex)));
                    }
                }
                else{
                    lstMitigants.add(ad.If_Others_please_specify__c);
                }
            }    
        }
        
    }
    
    public Map<String,String> getCustomerCategory()
    {
        Map<String,String> custCatergoryMap = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = Loan_Contact__c.Customer_segment__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        custCatergoryMap.put(null,'');
        for( Schema.PicklistEntry pickListVal : ple)
        {
            custCatergoryMap.put(pickListVal.getValue(),pickListVal.getLabel());
        } 
        
        return custCatergoryMap;
    }
     public Map<String,String> getAccountCategory()
    {
        Map<String,String> custCatergoryMap = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = Bank_Detail__c.Account_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        custCatergoryMap.put(null,'');
        for( Schema.PicklistEntry pickListVal : ple)
        {
            custCatergoryMap.put(pickListVal.getValue(),pickListVal.getLabel());
        } 
        
        return custCatergoryMap;
    }
    public Map<String,String> getConstitutionCategory()
    {
        Map<String,String> custCatergoryMap = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = Loan_Contact__c.Constitution__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        custCatergoryMap.put(null,'');
        for( Schema.PicklistEntry pickListVal : ple)
        {
            custCatergoryMap.put(pickListVal.getValue(),pickListVal.getLabel());
        } 
        
        return custCatergoryMap;
    }
    public Map<String,String> getApplicantCategory()
    {
        Map<String,String> custCatergoryMap = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = Loan_Contact__c.Applicant_Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        custCatergoryMap.put(null,'');
        for( Schema.PicklistEntry pickListVal : ple)
        {
            custCatergoryMap.put(pickListVal.getValue(),pickListVal.getLabel());
        } 
        
        return custCatergoryMap;
    }
    
    public Map<String,String> getPropertyType()
    {
        Map<String,String> custCatergoryMap = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = Property__c.Type_of_Property__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        custCatergoryMap.put(null,'');
        for( Schema.PicklistEntry pickListVal : ple)
        {
            custCatergoryMap.put(pickListVal.getValue(),pickListVal.getLabel());
        } 
        
        return custCatergoryMap;
    }
    
    public Map<String,String> getConstitution()
    {
        Map<String,String> custConstitutionMap = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = Loan_Contact__c.Constitution__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        custConstitutionMap.put(null,'');
        for( Schema.PicklistEntry pickListVal : ple)
        {
            custConstitutionMap.put(pickListVal.getValue(),pickListVal.getLabel());
        } 
        
        return custConstitutionMap;
    }
    
    public Map<String,String> getGender()
    {
        Map<String,String> custCatergoryMap = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = Loan_Contact__c.Gender__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        custCatergoryMap.put(null,'');
        for( Schema.PicklistEntry pickListVal : ple)
        {
            custCatergoryMap.put(pickListVal.getValue(),pickListVal.getLabel());
        } 
        
        return custCatergoryMap;
    }
    
    public Map<String,String> getLoanPurpose()
    {
        Map<String,String> custCatergoryMap = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = Loan_Application__c.Loan_Purpose__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        custCatergoryMap.put(null,'');
        for( Schema.PicklistEntry pickListVal : ple)
        {
            custCatergoryMap.put(pickListVal.getValue(),pickListVal.getLabel());
        } 
        
        return custCatergoryMap;
    }
    
    
    
}