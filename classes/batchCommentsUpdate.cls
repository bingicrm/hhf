global class batchCommentsUpdate implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {  
          return Database.getQueryLocator('select id, name,central_salaried__c, (select id from Loan_Contacts__r where Customer_Segment__c != \'1\' and Income_Considered__c = true) from loan_application__C');
    
    }
    global void execute(Database.BatchableContext BC, List<loan_application__C > lstloan2)
        {    
            list<loan_application__C> lstloan = new list<loan_application__C>();
            for (loan_application__C loan : lstloan2){
                if (loan.Loan_Contacts__r.size() == 0){
                   
                    loan.central_salaried__c = true ;
                    lstloan.add(loan);
                }
            }
            
            update lstLoan;
      } 
        
    global void finish(Database.BatchableContext BC)
    {
        
    }
}