@isTest
public class BRECalloutController_Test {

public static testMethod void methodTestSample()
    {
  
            Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
             Account acc=new Account();
            acc.CountryCode__c='+91';
            acc.Email__c='puser001@amamama.com';
            acc.Gender__c='M';
            acc.Mobile__c='9800765432';
            acc.Phone__c='9800765434';
            acc.PersonMobilePhone = '9800765434';
            //acc.Rejection_Reason__c='Rejected';
            //acc.Name='Test Account';
            acc.FirstName = 'abc';
            acc.LastName='def';
            acc.Salutation='Mr';
            acc.RecordTypeId = RecordTypeIdCustomer;
            // acc.Type=;
            insert acc;
         
                                
             Scheme__c sch= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                         Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                         Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                         Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                         Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
            insert sch;    
            

            Id RecordTypeIdLoanApplication = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('COPS: Credit Operations Entry').getRecordTypeId();
            Loan_Application__c app = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=sch.Id,Loan_Number__c='124',
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',StageName__c='Customer Onboarding',
                                                                        RecordTypeId=RecordTypeIdLoanApplication,
                                                                        Business_Date_Created__c=System.today(),
                                                                        Approved_Loan_Amount__c=100000,
                                                                        Processing_Fee_Percentage__c=1,Mode_of_Repayment__c='NACH',
                                                                        Repayment_Start_Date__c=Date.Today().addDays(100),
                                                                        Sanctioned_Disbursement_Type__c='Multiple Tranche',Property_Identified__c = true);
            insert app;
            
            Loan_Contact__c lc =[SELECT Id,Borrower__c from Loan_Contact__c where Loan_Applications__c=:app.Id];
            lc.Borrower__c='1';
            //lc.Category__c='1';
            lc.Constitution__c = '20';
            lc.Customer_segment__c = '1';
            lc.BRE_Record__c = true;
            lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc.Marital_Status__c = 'M';
            lc.Gross_Salary__c = 1000;
            lc.Basic_Pay__c = 1000;
            lc.DA__c = 1000;
            //lc.Total_Fixed_Pay__c = 1000;
            //lc.Variable_Income_Monthly__c = 1000;
            lc.Net_Income__c = 1000;
            lc.PF__c = 1000;
            lc.TDS__c = 1000;
            lc.Current_Organization_Work_Ex_in_Months__c=12;
            lc.Total_Work_Experience__c = 12;
            lc.Customer_Details_Verified__c= true;
            lc.Voter_ID_Number__c = '123456789';
            lc.Passport_Number__c = '123456789';
            lc.PAN_Number__c = 'BAFPV6543A';
            lc.Aadhaar_Number__c = '898518762736';
            
            update lc;
            LMS_Date_Sync__c abc = new LMS_Date_Sync__c(EOD_BOD__c='N',Current_Date__c=Date.Today());
            insert abc;
            Test.startTest();

            Pincode__c pin = new Pincode__c(Name='11044',ZIP_ID__c='45',City__c='Ludiana',State_Text__c='Punjab',Country_Text__c='India');
            insert pin;   
            system.debug('pin '+pin);
            Address__c add = new Address__c(Residence_Type__c= 'G',Type_of_address__c='OFFRES',Address_Line_1__c='11111111',Address_Line_2__c='127737',Pincode_LP__c=pin.Id,Loan_Contact__c=lc.Id);
            insert add;
            Customer_Integration__c cust = new Customer_Integration__c(CIBIL_Score__c='5',recordTypeId=Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId(), Loan_Application__c = app.Id, Loan_Contact__c=lc.Id,BRE_Recent__c=true);
            insert cust;
            Customer_Obligations__c custObligation = new Customer_Obligations__c(Customer_Integration__c=cust.Id,EMI_Amount__c=12000);
            insert custObligation;
            Sourcing_Detail__c sc = new Sourcing_Detail__c(Loan_Application__c= app.Id, Source_Code__c= '19',Lead_Sourcing_Detail__c='HHFL Website',recordTypeId=Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('Digital Direct').getRecordTypeId());
            insert sc;
            
            app.StageName__c='Operation Control';
            app.Sub_Stage__c  ='Scan: Data Maker';
            //app.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('File check Scanner').getRecordTypeId();
            update app;

            Property__c prop = new Property__c(First_Property_Owner__c=lc.Id, Loan_Application__c=app.Id,Address_Line_1__c='17827872',Address_line_2__c ='tEST',Area__c=12,Floor__c='1',Area_Unit__c='Sqft',Flat_No_House_No__c='1',Type_Property__c='Residential',Pincode_LP__c=pin.Id);
            insert prop;
            pin.State_Text__c='Punjab';
            pin.Country_Text__c='India';
            pin.City__c='Ludiana';
            update pin;
            BRECalloutController.getMessage(app.Id);
            BRECalloutController.createIntegration(app.Id);
            
            BRE2ExperionResponse.cls_ERROR err = new BRE2ExperionResponse.cls_ERROR();
            err.ERRORCODE = '';
            err.ERRORDESCRIPTION = '';
            List<BRE2ExperionResponse.cls_ERROR> errList = new List<BRE2ExperionResponse.cls_ERROR>();
            errList.add(err);
            
            BRE2ExperionResponse.cls_ELIGIBILITY eli = new BRE2ExperionResponse.cls_ELIGIBILITY();
            eli.INCOMEMETHOD = '20';
            eli.ELIGIBLEINCOME = '20';
            eli.ELIGIBILELOANAMOUNTLTV = '20';
            eli.ELIGIBILELOANAMOUNTFOIR = '20';
            eli.ELIGIBILELOANAMOUNTINSR = '20';
            eli.ELIGIBLELOANAMOUNT = '20';
            eli.FOIRCALCULATED = '20';
            eli.NPV = '20';
            eli.EMICALCULATED = '20';
            eli.ELIGIBLETENURE = '20';
            eli.ELIGIBILELOANAMOUNTPENSION = '20';
            eli.ELIGIBILTYDUMMY1 = '20';
            eli.ELIGIBILTYDUMMY2 = '20';
            eli.ELIGIBILTYDUMMY3 = '20';
            eli.ELIGIBILTYDUMMY4 = '20';
            
            BRE2ExperionResponse.cls_APPLICATIONOUT applicationOut = new BRE2ExperionResponse.cls_APPLICATIONOUT();
            applicationOut.STRATEGYTAG = '';
            applicationOut.CLIENTNAME = '';
            applicationOut.ORIGINATIONSOURCEID = '';
            applicationOut.REQUESTID = '';
            applicationOut.REQUESTEDDATETIME = '';
            applicationOut.APPLICATIONID = app.Id;
            applicationOut.CUSTINTEGRATIONID = '';
            applicationOut.BUSINESSDATE = '';
            applicationOut.RESPONSEDATETIME = '';
            applicationOut.DUMMYVARIABLE1 = '';
            applicationOut.DUMMYVARIABLE2 = '';
            applicationOut.DUMMYVARIABLE3 = '';
            applicationOut.DUMMYVARIABLE4 = '';
            applicationOut.DUMMYVARIABLE5 = '';
            applicationOut.DUMMYVARIABLE6 = '';
            applicationOut.APPLIEDLOANAMOUNT = '';
            applicationOut.DUMMYVARIABLE1 = '';
            applicationOut.EMI = '';
            applicationOut.TENUREAPPLIED = '';
            applicationOut.PRODUCTCODE = '';
            applicationOut.SUBPRODUCT = '';
            applicationOut.FINALDECISION = 'Green';
            applicationOut.DECISIONCATEGORY = 'ok';
            applicationOut.DECISIONCOMPONENTTREENAME = 'ok';
            applicationOut.ROI = '12';
            applicationOut.APPLICATIONOUTDUMMY1 = '';
            applicationOut.APPLICATIONOUTDUMMY2 = '';
            applicationOut.APPLICATIONOUTDUMMY3 = '';
            applicationOut.APPLICATIONOUTDUMMY4 = '';
            applicationOut.APPLICATIONOUTDUMMY5 = '';
            applicationOut.APPLICATIONOUTDUMMY6 = '';
            applicationOut.APPLICATIONOUTDUMMY7 = '';
            applicationOut.APPLICATIONOUTDUMMY8 = '';
            applicationOut.APPLICATIONOUTDUMMY9 = '';
            applicationOut.APPLICATIONOUTDUMMY10 = '';
            applicationOut.APPLICATIONOUTDUMMY11 = '';
            applicationOut.APPLICATIONOUTDUMMY12 = '';
            applicationOut.APPLICATIONOUTDUMMY13 = '';
            applicationOut.APPLICATIONOUTDUMMY14 = '';
            applicationOut.APPLICATIONOUTDUMMY15 = '';
            applicationOut.APPLICATIONOUTDUMMY16 = '';
            applicationOut.APPLICATIONOUTDUMMY17 = '';
            applicationOut.APPLICATIONOUTDUMMY18 = '';
            applicationOut.APPLICATIONOUTDUMMY19 = '';
            applicationOut.APPLICATIONOUTDUMMY20 = '';
            applicationOut.APPLICATIONOUTDUMMY21 = '';
            applicationOut.APPLICATIONOUTDUMMY22 = '';
            applicationOut.APPLICATIONOUTDUMMY23 = '';
            applicationOut.APPLICATIONOUTDUMMY24 = '';
            applicationOut.APPLICATIONOUTDUMMY25 = '';
            applicationOut.APPLICATIONOUTDUMMY26 = '';
            applicationOut.APPLICATIONOUTDUMMY27 = '';
            applicationOut.APPLICATIONOUTDUMMY28 = '';
            applicationOut.APPLICATIONOUTDUMMY29 = '';
            applicationOut.APPLICATIONOUTDUMMY30 = '';
            applicationOut.APPLICATIONOUTDUMMY31 = '';
            applicationOut.APPLICATIONOUTDUMMY32 = '';
            applicationOut.APPLICATIONOUTDUMMY33 = '';
            applicationOut.APPLICATIONOUTDUMMY34 = '';
            applicationOut.APPLICATIONOUTDUMMY35 = '';
            applicationOut.APPLICATIONOUTDUMMY36 = '';
            applicationOut.APPLICATIONOUTDUMMY37 = '';
            applicationOut.APPLICATIONOUTDUMMY38 = '';
            applicationOut.APPLICATIONOUTDUMMY39 = '';
            applicationOut.APPLICATIONOUTDUMMY40 = '';
            applicationOut.ERROR = errList; 
            applicationOut.ELIGIBILITY = eli;
            List<String> abc11 = new List<String>();
            abc11.add('12');
            abc11.add('13');
            applicationOut.DECISIONSTATUS = abc11;
            applicationOut.SORTEDREASONCODETABLE = abc11;
            applicationOut.DEVIATIONCODE = abc11;
            applicationOut.LTV = abc11;
            BRE2ExperionResponse.cls_INCOMEPROGRAM income= new BRE2ExperionResponse.cls_INCOMEPROGRAM();
            income.INCOMEMETHOD = '20';
            income.ELIGIBILEGROSSINCOME= '20';
            income.ELIGIBILENETINCOME= '20';
            income.OBLIGATIONSEMI= '20';
            income.ELIGIBILELOANAMOUNTFOIR= '20';
            income.ELIGIBILELOANAMOUNTINSR= '20';
            income.ELIGIBILELOANAMOUNTPENSION= '20';
            income.EMIPERLAC= '20';
            income.FOIRCALC= '20';
            income.INSRCALC= '20';
            income.FOIRPERCENTAGE= '20';
            income.INSRPERCENTAGE= '20';
            income.ELIGIBLENORMALINCOMEEMI= '20';
            income.ELIGIBLEPENSIONINCOMEEMI= '20';
            income.TENURESHORT= '20';
            income.TENURELONG= '20';
            income.APPLICANTOUTDUMMY1= '20';
            income.APPLICANTOUTDUMMY2= '20';
            income.APPLICANTOUTDUMMY3= '20';
            income.APPLICANTOUTDUMMY4= '20';
            income.APPLICANTOUTDUMMY5= '20';
            income.APPLICANTOUTDUMMY6= '20';
            
            
            BRE2ExperionResponse.cls_APPLICANTOUT  applicantOut = new BRE2ExperionResponse.cls_APPLICANTOUT();
            applicantOut.CCCID = lc.Id;
            applicantOut.FINALDECISION = 'Green';
            applicantOut.DECISIONCATEGORY = 'ok';
            applicantOut.DECISIONCOMPONENTTREENAME = 'ok';
            applicantOut.APPLICANTOUTDUMMY1 = '';
            applicantOut.APPLICANTOUTDUMMY2= '';
            applicantOut.APPLICANTOUTDUMMY3= '';
            applicantOut.APPLICANTOUTDUMMY4= '';
            applicantOut.APPLICANTOUTDUMMY5= '';
            applicantOut.APPLICANTOUTDUMMY6= '';
            applicantOut.APPLICANTOUTDUMMY7= '';
            applicantOut.APPLICANTOUTDUMMY8= '';
            applicantOut.APPLICANTOUTDUMMY9= '';
            applicantOut.APPLICANTOUTDUMMY10= '';
            applicantOut.APPLICANTOUTDUMMY11= '';
            applicantOut.APPLICANTOUTDUMMY12= '';
            applicantOut.APPLICANTOUTDUMMY13= '';
            applicantOut.APPLICANTOUTDUMMY14= '';
            applicantOut.APPLICANTOUTDUMMY15= '';
            applicantOut.APPLICANTOUTDUMMY16= '';
            applicantOut.APPLICANTOUTDUMMY17= '';
            applicantOut.APPLICANTOUTDUMMY18= '';
            applicantOut.APPLICANTOUTDUMMY19= '';
            applicantOut.APPLICANTOUTDUMMY20= '';
            applicantOut.APPLICANTOUTDUMMY21= '';
            applicantOut.APPLICANTOUTDUMMY22= '';
            applicantOut.APPLICANTOUTDUMMY23= '';
            applicantOut.APPLICANTOUTDUMMY24= '';
            applicantOut.APPLICANTOUTDUMMY25= '';
            applicantOut.APPLICANTOUTDUMMY26= '';
            applicantOut.APPLICANTOUTDUMMY27= '';
            applicantOut.APPLICANTOUTDUMMY28= '';
            applicantOut.APPLICANTOUTDUMMY29= '';
            applicantOut.APPLICANTOUTDUMMY30= '';
            applicantOut.APPLICANTOUTDUMMY31= '';
            applicantOut.APPLICANTOUTDUMMY32= '';
            applicantOut.APPLICANTOUTDUMMY33= '';
            applicantOut.APPLICANTOUTDUMMY34= '';
            applicantOut.APPLICANTOUTDUMMY35= '';
            applicantOut.APPLICANTOUTDUMMY36= '';
            applicantOut.APPLICANTOUTDUMMY37= '';
            applicantOut.APPLICANTOUTDUMMY38= '';
            applicantOut.APPLICANTOUTDUMMY39= '';
            applicantOut.APPLICANTOUTDUMMY40= '';
            applicantOut.SORTEDREASONCODETABLE = abc11;
            applicantOut.DECISIONSTATUS = abc11;
            applicantOut.DEVIATIONCODE = abc11;
            applicantOut.INCOMEPROGRAM = income;
            BRE2ExperionResponse.cls_APPLICANT appli = new BRE2ExperionResponse.cls_APPLICANT();
            appli.APPLICANTOUT = applicantOut; 
            List<BRE2ExperionResponse.cls_APPLICANT> appList = new List<BRE2ExperionResponse.cls_APPLICANT>();
            appList.add(appli);
            
            BRE2ExperionResponse.cls_APLCTION_RES appRes = new BRE2ExperionResponse.cls_APLCTION_RES();
            appRes.WARNINGCODE = '';
            appRes.WARNINGDESC = '';
            appRes.APPLICATIONOUT = applicationOut;
            appRes.APPLICANT = appList;
            
            BRE2ExperionResponse.BRE2ExperionResp bre2Res= new BRE2ExperionResponse.BRE2ExperionResp();
            bre2Res.APLCTION_RES = appRes;
            TestMockRequest req1=new TestMockRequest(200,'Complete',JSON.serialize(bre2Res),null);
            Test.setMock(HttpCalloutMock.class, req1);
            Test.stopTest();
            BRECalloutController.generateRequest(app.Id);
    }
	
}