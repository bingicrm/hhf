@isTest
public class TestReviveOperations
{
    public static testMethod void method1()
    {
        Account acc=new Account();
        acc.name='Applicant123';
        acc.phone='9234509786';
        acc.PAN__c='ADGPW4078E';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        Scheme__c sc=new Scheme__c();
        sc.name='Construction Finance';
        sc.Scheme_Code__c='CF';
        sc.Scheme_Group_ID__c='6';
        sc.Product_Code__c='HL';
        insert sc;  
        Loan_Application__c la = new  Loan_Application__c(); 
        la.Customer__c = acc.id;
        la.Scheme__c = sc.id;
        la.Requested_Amount__c = 2800000;      
        la.Requested_Loan_Tenure__c = 120;
        la.Date_of_Cancellation__c = (date.today()-10);
        la.Cancellation_Reason__c = 'Customer Cancelled';
        la.Rejected_Cancelled_Stage__c='Credit Decisioning';
        la.Rejected_Cancelled_Sub_Stage__c='Credit Review';
        la.Remarks_Revival__c = 'Kindly revive application';
        la.transaction_type__c = 'PB';
        insert la;
        Id parameter = la.id;
        UCIC_Global__c setting = new UCIC_Global__c();
        setting.UCIC_Active__c = true;
        insert setting; 
		Test.startTest();
        ReviveOperations.getLoanApp(parameter);
        ReviveOperations.reviveApplication(la);
        Test.stopTest();
    }
    
    public static testMethod void method2()
    {
        Account acc=new Account();
        acc.name='Applicant123';
        acc.phone='9234509786';
        acc.PAN__c='ADGPW4078E';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        Scheme__c sc=new Scheme__c();
        sc.name='Construction Finance';
        sc.Scheme_Code__c='CF';
        sc.Scheme_Group_ID__c='6';
        sc.Product_Code__c='HL';
        insert sc;  
        Loan_Application__c la = new  Loan_Application__c(); 
        la.Customer__c = acc.id;
        la.Scheme__c = sc.id;
        la.Requested_Amount__c = 2800000;      
        la.Requested_Loan_Tenure__c = 120;
        la.Date_of_Cancellation__c = (date.today()-10);
        la.Cancellation_Reason__c = 'Customer Cancelled';
        la.transaction_type__c = 'PB';
        insert la;
        ReviveOperations.reviveApplication(la);
    }  
    public static testMethod void method3()
    {
        Account acc=new Account();
        acc.name='Applicant123';
        acc.phone='9234509786';
        acc.PAN__c='ADGPW4078E';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        Scheme__c sc=new Scheme__c();
        sc.name='Construction Finance';
        sc.Scheme_Code__c='CF';
        sc.Scheme_Group_ID__c='6';
        sc.Product_Code__c='HL';
        insert sc;  
        Loan_Application__c la = new  Loan_Application__c(); 
        la.Customer__c = acc.id;
        la.Scheme__c = sc.id;
        la.Requested_Amount__c = 2800000;      
        la.Requested_Loan_Tenure__c = 120;
        la.Date_of_Cancellation__c = date.today()-50;
        la.Cancellation_Reason__c = 'Customer Cancelled';
        la.transaction_type__c = 'PB';
        insert la;
        try
        {
            ReviveOperations.reviveApplication(la);
        }
        catch(Exception e)
        {}
    }
    public static testMethod void method4()
    {
        Account acc=new Account();
        acc.name='Applicant123';
        acc.phone='9234509786';
        acc.PAN__c='ADGPW4078E';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        Scheme__c sc=new Scheme__c();
        sc.name='Construction Finance';
        sc.Scheme_Code__c='CF';
        sc.Scheme_Group_ID__c='6';
        sc.Product_Code__c='HL';
        insert sc;  
        Loan_Application__c la = new  Loan_Application__c(); 
        la.Customer__c = acc.id;
        la.Scheme__c = sc.id;
        la.Requested_Amount__c = 2800000;      
        la.Requested_Loan_Tenure__c = 120;
        la.Date_of_Cancellation__c = (date.today()-10);
        la.Cancellation_Reason__c = 'Customer Cancelled';
        
        la.Remarks_Revival__c = 'Kindly revive application';
        la.transaction_type__c = 'PB';
        insert la;
        Id parameter = la.id;
        
        ReviveOperations.getLoanApp(parameter);
        ReviveOperations.reviveApplication(la);
    }
    public static testMethod void method5()
    {
        Account acc=new Account();
        acc.name='Applicant122';
        acc.phone='9234509786';
        acc.PAN__c='ADGPW4078E';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        Scheme__c sc=new Scheme__c();
        sc.name='Construction Finance';
        sc.Scheme_Code__c='CF';
        sc.Scheme_Group_ID__c='6';
        sc.Product_Code__c='HL';
        insert sc;  
        Loan_Application__c la = new  Loan_Application__c(); 
        la.Customer__c = acc.id;
        la.Scheme__c = sc.id;
        la.Requested_Amount__c = 2800000;      
        la.Requested_Loan_Tenure__c = 120;
        la.Date_of_Cancellation__c = (date.today()-10);
        la.Cancellation_Reason__c = 'Customer Cancelled';
        la.Previous_Sub_Stage__c = Constants.FCU_Review;
        la.Remarks_Revival__c = 'Kindly revive application';
        la.transaction_type__c = 'PB';
        la.Rejection_Reason__c = 'FCU Decline';
        insert la;
        ReviveOperations.FCUReviveApplication(la);
        Loan_Application__c la1 = new  Loan_Application__c(); 
        la1.Customer__c = acc.id;
        la1.Scheme__c = sc.id;
        la1.Requested_Amount__c = 2800000;      
        la1.Requested_Loan_Tenure__c = 120;
        la1.Date_of_Cancellation__c = (date.today()-10);
        la1.Cancellation_Reason__c = 'Customer Cancelled';
        la1.Previous_Sub_Stage__c = Constants.FCU_Review;
        la1.Remarks_Revival__c = 'Kindly revive application';
        la1.transaction_type__c = 'PB';
        la1.Rejection_Reason__c = 'FCU Decline';
        la1.Moved_To_FCU_Review_From_Credit__c = true;
        insert la1;
        Test.startTest();
        ReviveOperations.FCUReviveApplication(la1);
        Test.stopTest();

    }
    public static testMethod void method6()
    {
        Account acc=new Account();
        acc.name='Applicant122';
        acc.phone='9234509786';
        acc.PAN__c='ADGPW4078E';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        Scheme__c sc=new Scheme__c();
        sc.name='Construction Finance';
        sc.Scheme_Code__c='CF';
        sc.Scheme_Group_ID__c='6';
        sc.Product_Code__c='HL';
        insert sc;  
        Loan_Application__c la = new  Loan_Application__c(); 
        la.Customer__c = acc.id;
        la.Scheme__c = sc.id;
        la.Requested_Amount__c = 2800000;      
        la.Requested_Loan_Tenure__c = 120;
        la.Date_of_Cancellation__c = (date.today()-10);
        la.Cancellation_Reason__c = 'Customer Cancelled';
        la.Previous_Sub_Stage__c = Constants.Hunter_Review;
        la.Remarks_Revival__c = 'Kindly revive application';
        la.Rejection_Reason__c = 'Hunter Decline';
        la.transaction_type__c = 'PB';
        insert la;
        ReviveOperations.FCUReviveApplication(la);
    }
}