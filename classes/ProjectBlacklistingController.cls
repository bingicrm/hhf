public class ProjectBlacklistingController {
    
    @AuraEnabled
    public static String showErrorMsg() {
        User userInfo = [Select Id, Name, Profile.Name from User Where Id=: UserInfo.getUserId()];
        if(userInfo.Profile.Name == 'DSA' || userInfo.Profile.Name == 'DST') {
            return 'You are not authorized to initiate Project Blacklisting';
        }
        return 'Success';
    }
    
    
    @AuraEnabled
    public static Project__c checkEligibility(Id lappId){
        system.debug('=='+lappId);
        return [SELECT Id, Name, Stage__c, Sub_Stage__c, APF_Status__c, Blacklisting_Status__c, RecordTypeID, Comments__c
                FROM Project__c
                WHERE Id = :lappId ];
    }
    
    @AuraEnabled
    public static MoveNextWrapper moveprevious(id oppId, string comments) 
    {
        Project__c la = new Project__c();
        //Added by Shobhit Saxena for Comments Trail
        la = [select Id, Comments_Trail__c, Previous_Sub_Stage__c, Stage__c, Sub_Stage__c, APF_Status__c, Blacklisting_Status__c,  Comments__c,Approval_Rejection_Comments__c from Project__c where Id =: oppId];
        
        if(la != null) {
            la.Blacklisting_Status__c = 'Blacklisting Initiated';
            
            
        }
        
        try 
        {                
            System.debug('Debug Log for Sub Stage'+la.Sub_Stage__c);
            update la;
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Reason: '+comments);
            req1.setObjectId(la.id);// Submit the record to specific process
            req1.setProcessDefinitionNameOrId('Project_Blacklisting_Approval_Process');
            Approval.ProcessResult result = Approval.process(req1);
           
            // Verify the result
       
            System.assert(result.isSuccess());
            
            if(!result.isSuccess()) {
                la.Blacklisting_Status__c = null;
                update la;
            }
           
            System.assertEquals(
                'Pending', result.getInstanceStatus(),
                'Instance Status'+result.getInstanceStatus());
            return new MoveNextWrapper(false,la.Sub_Stage__c);
        } 
        catch (DMLException e) 
        {
            return new MoveNextWrapper(true,e.getDmlMessage(0));
        } 
        catch (Exception e) 
        {
            return new MoveNextWrapper(true,e.getMessage());
        } 
    }
    
    public class MoveNextWrapper
    {
        @AuraEnabled
        public Boolean error;
        @AuraEnabled
        public String msg;
        
        public MoveNextWrapper(Boolean error, String msg )
        {
            this.error = error;
            this.msg = msg;
        }
        
    }
}