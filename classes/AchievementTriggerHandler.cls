public class AchievementTriggerHandler {

    public static void beforeInsert(list < Achievement__c > newList, list < Achievement__c > oldList, map < id, Achievement__c > newMap, map < id, Achievement__c > oldMap) {
    
        AchievementTriggerHelper.EvaluateConsiderforInsurance(newList);
    }
    
    public static void beforeUpdate(list < Achievement__c > newList, list < Achievement__c > oldList, map < id, Achievement__c > newMap, map < id, Achievement__c > oldMap) {
        
        AchievementTriggerHelper.EvaluateConsiderforInsurance(newList);
        
    }

}