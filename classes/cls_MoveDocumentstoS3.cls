public class cls_MoveDocumentstoS3 {

    @AuraEnabled
    public static Boolean checkEligibility() {
        String strProfileId;
        strProfileId = UserInfo.getProfileId();
        Profile objProfile = [Select Id, Name from Profile where Id=: strProfileId];
        if(objProfile != null) {
            if(objProfile.Name != 'System Administrator') {
                return false;
            }
            else {
                return true;
            }
        }
        return false;
    }
    
    @AuraEnabled
    public static void moveDocumentstoS3(Id loanApplicationId) {
        //The entire code block will be executed only when the loanApplicationId is not blank.
        if(!String.isBlank(loanApplicationId)) {
            Set<Id> setApplicantRelatedDocs = new Set<Id>();
            Set<Id> setCoApplicantGuarantorDocs = new Set<Id>();
            Set<Id> setApplicationRelatedDocs = new Set<Id>();
            Set<Id> setAllDocuments = new Set<Id>();
            List<ContentDocumentLink> lstContentDocLinks;
            List<ContentVersion> lstContentVersion;
            Set<Id> setContentDocumentLinkIds = new Set<Id>();
            Set<Id> setContentVersionId = new Set<Id>();
            Map<Id,Id> mapConDocLnktoDocChkLst = new Map<Id,Id>();
            
            /****************************************Beginning of Logic for Creation of S3 Folder for Loan Application************************************/
            
            List<NEILON__Folder__c> lstBucket = [Select Id, Name, NEILON__Bucket_Region__c From NEILON__Folder__c Where NEILON__Parent__c = NULL];
            String loanAppName = [Select Id, Name From Loan_Application__c Where Id=: loanApplicationId].Name;
            String strApplicantId;
            Loan_Contact__c objApplicant = [Select Id, Name, Applicant_Type__c from Loan_Contact__c Where Loan_Applications__r.Name =: loanAppName LIMIT 1];
            if(objApplicant != null) {
                strApplicantId = objApplicant.Id;
            }
            List<S3_Folders_Configuration__mdt> lstS3ConfigData = [SELECT Id, Delete_files__c, Folder_Name__c, MasterLabel, DeveloperName, NamespacePrefix FROM S3_Folders_Configuration__mdt];
            Map<String, S3_Folders_Configuration__mdt> mapDeveloperNametoRecord = new Map<String, S3_Folders_Configuration__mdt>();
            System.debug('Debug Log for lstS3ConfigData'+lstS3ConfigData.size());
            if(!lstS3ConfigData.isEmpty()) {
                for(S3_Folders_Configuration__mdt objS3ConfigData : lstS3ConfigData) {
                    mapDeveloperNametoRecord.put(objS3ConfigData.DeveloperName, objS3ConfigData);
                }
            }
            System.debug('Debug Log for mapDeveloperNametoRecord'+mapDeveloperNametoRecord.size());
            System.debug('Debug Log for lstBucket'+lstBucket.size());
            if(!lstBucket.isEmpty()) {
                // Create roo
                NEILON__Folder__c objS3Folder = NEILON.apGlobalUtils.buildFolderArchitecture(loanApplicationId);
                if(String.isNotBlank(objS3Folder.Id)) {
                    /****************************************Beginning of Logic for Creation of S3 Folder for Applicant Related Documents************************************/
                    List<NEILON__Folder__c> lstParentFolder = [Select Id, Name From NEILON__Folder__c Where Id =: objS3Folder.Id];
                    List<NEILON__Folder__c> lstFolderstoCreate = new List<NEILON__Folder__c>();
                    if(!lstParentFolder.isEmpty()) {
                        NEILON__Folder__c objS3Folder1 = new NEILON__Folder__c();
                        objS3Folder1.Loan_Application__c = loanApplicationId;
                        objS3Folder1.NEILON__Bucket_Name__c = lstBucket[0].Name;
                        objS3Folder1.NEILON__Bucket_Region__c = lstBucket[0].NEILON__Bucket_Region__c;
                        objS3Folder1.NEILON__Parent_Id__c = loanApplicationId;
                        objS3Folder1.NEILON__Parent__c = lstParentFolder[0].Id; 
                        objS3Folder1.NEILON__Parent_Object_API_Name__c = 'Loan_Application__c';
                        if(strApplicantId != null) {
                            objS3Folder1.Loan_Contact__c = strApplicantId;
                        }
                        if(!mapDeveloperNametoRecord.isEmpty() && mapDeveloperNametoRecord.containsKey('Applicant') && String.isNotBlank(mapDeveloperNametoRecord.get('Applicant').Folder_Name__c)) {
                            objS3Folder1.Name = mapDeveloperNametoRecord.get('Applicant').Folder_Name__c + ' '+loanAppName;
                        }
                        
                        lstFolderstoCreate.add(objS3Folder1);
                        
                        NEILON__Folder__c objS3Folder2 = new NEILON__Folder__c();
                        objS3Folder2.Loan_Application__c = loanApplicationId;
                        objS3Folder2.NEILON__Bucket_Name__c = lstBucket[0].Name;
                        objS3Folder2.NEILON__Bucket_Region__c = lstBucket[0].NEILON__Bucket_Region__c;
                        objS3Folder2.NEILON__Parent_Id__c = loanApplicationId;
                        objS3Folder2.NEILON__Parent__c = lstParentFolder[0].Id; 
                        objS3Folder2.NEILON__Parent_Object_API_Name__c = 'Loan_Application__c';
                        //objS3Folder2.Name = 'Co-Applicant Documents for '+loanAppName;
                        if(!mapDeveloperNametoRecord.isEmpty() && mapDeveloperNametoRecord.containsKey('Co_Applicant') && String.isNotBlank(mapDeveloperNametoRecord.get('Co_Applicant').Folder_Name__c)) {
                            objS3Folder2.Name = mapDeveloperNametoRecord.get('Co_Applicant').Folder_Name__c+ ' '+loanAppName;
                        }
                        lstFolderstoCreate.add(objS3Folder2);
                        
                        NEILON__Folder__c objS3Folder3 = new NEILON__Folder__c();
                        objS3Folder3.Loan_Application__c = loanApplicationId;
                        objS3Folder3.NEILON__Bucket_Name__c = lstBucket[0].Name;
                        objS3Folder3.NEILON__Bucket_Region__c = lstBucket[0].NEILON__Bucket_Region__c;
                        objS3Folder3.NEILON__Parent_Id__c = loanApplicationId;
                        objS3Folder3.NEILON__Parent__c = lstParentFolder[0].Id; 
                        objS3Folder3.NEILON__Parent_Object_API_Name__c = 'Loan_Application__c';
                        //objS3Folder3.Name = 'Application Documents for '+loanAppName;
                        if(!mapDeveloperNametoRecord.isEmpty() && mapDeveloperNametoRecord.containsKey('Application') && String.isNotBlank(mapDeveloperNametoRecord.get('Application').Folder_Name__c)) {
                            objS3Folder3.Name = mapDeveloperNametoRecord.get('Application').Folder_Name__c+ ' '+loanAppName;
                        }
                        lstFolderstoCreate.add(objS3Folder3);
                        
                        NEILON__Folder__c objS3Folder4 = new NEILON__Folder__c();
                        objS3Folder4.Loan_Application__c = loanApplicationId;
                        objS3Folder4.NEILON__Bucket_Name__c = lstBucket[0].Name;
                        objS3Folder4.NEILON__Bucket_Region__c = lstBucket[0].NEILON__Bucket_Region__c;
                        objS3Folder4.NEILON__Parent_Id__c = loanApplicationId;
                        objS3Folder4.NEILON__Parent__c = lstParentFolder[0].Id; 
                        objS3Folder4.NEILON__Parent_Object_API_Name__c = 'Loan_Application__c';
                        //objS3Folder4.Name = 'Notes & Attachments for '+loanAppName;
                        if(!mapDeveloperNametoRecord.isEmpty() && mapDeveloperNametoRecord.containsKey('Notes_and_Attachments') && String.isNotBlank(mapDeveloperNametoRecord.get('Notes_and_Attachments').Folder_Name__c)) {
                            objS3Folder4.Name = mapDeveloperNametoRecord.get('Notes_and_Attachments').Folder_Name__c+ ' '+loanAppName;
                        }
                        lstFolderstoCreate.add(objS3Folder4);
                        Database.SaveResult[] lstDSR= Database.insert(lstFolderstoCreate,false);
                        for(Database.SaveResult objDSR : lstDSR) {
                            if(objDSR.getID() != null) {
                                System.debug('Folder Creation Successful with Id'+objDSR.getId());
                            }
                            else {
                                System.debug('Debug Log for Errors(if any), during folder creation'+objDSR.getErrors());
                            }
                        }
                        
                    }
                    /*******************************************End of Logic for Creation of S3 Folder for Co-Applicant Related Documents***************************************/
                }
            }
            
            /*******************************************End of Logic for Creation of S3 Folder for Loan Application***************************************/
            cls_MoveDocumentstoS3Helper.createS3FilesApplicant(loanApplicationId);
            cls_MoveDocumentstoS3Helper.createS3FilesCoApplicantGuarantor(loanApplicationId);
            cls_MoveDocumentstoS3Helper.createS3FilesApplication(loanApplicationId);
            cls_MoveDocumentstoS3Helper.createS3FilesLoanKit(loanApplicationId);
            cls_MoveDocumentstoS3Helper.createS3FilesLoanKit2(loanApplicationId);
        }
    }
}