public class DowsizingErrorResponseWrapper {
	public String status;
	public Error error;

	public class Error {
		public String errorCode;
		public String errorType;
		public String errorMessage;
	}
}