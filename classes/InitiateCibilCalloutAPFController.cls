public class InitiateCibilCalloutAPFController {
    @AuraEnabled
    public static String checkCibilEligibility(Id strRecordId) {
        String returnMsg;
        if(String.isNotBlank(strRecordId)) {
            String sObjName = strRecordId.getSObjectType().getDescribe().getName();
            System.debug('Debug Log for sObjName'+sObjName);
            List<Project_Builder__c> lstProjBuilder = new List<Project_Builder__c>();
            List<Promoter__c> lstPromoter = new List<Promoter__c>();
            
            if(sObjName == Constants.strProjectBuilderAPI) {
                lstProjBuilder = [Select Id, Name, Address_Line_1__c, Address_Line_2__c, City__c, City_ID__c, Country__c, Pincode__c, State__c, Retrigger_Corp_CIBIL__c, Builder_Type__c, CIBIL_Response__c, Pan_Verification_status__c, Project__c, Project__r.Stage__c, Project__r.Sub_Stage__c, Constitution__c From Project_Builder__c Where Id =: strRecordId LIMIT 1];
                if(!lstProjBuilder.isEmpty()) {
                    if(String.isNotBlank(lstProjBuilder[0].Builder_Type__c) && lstProjBuilder[0].Builder_Type__c == Constants.strProjectBuilderBTIndv) {
                        returnMsg = 'INDIVIDUAL';
                        //if()
                    }
                    else if(String.isNotBlank(lstProjBuilder[0].Builder_Type__c) && lstProjBuilder[0].Builder_Type__c == Constants.strProjectBuilderBTCorp) {
                        if(lstProjBuilder[0].Constitution__c != Constants.strPRIVATE_LIMITED && lstProjBuilder[0].Constitution__c != Constants.strPUBLIC_LIMITED && lstProjBuilder[0].Constitution__c != Constants.strLLPF) {
                            returnMsg = 'Corporate CIBIL Not Applicable for this Customer, based on Constitution.';
                        }
                        else if(lstProjBuilder[0].CIBIL_Response__c) {
                            returnMsg = 'CIBIL has already been hit for response. Please try after some time.';
                        }
                        else if(!lstProjBuilder[0].Pan_Verification_status__c) {
                            returnMsg = 'Please validate PAN Card before validating CIBIL.';
                        }
                        else if(String.isBlank(lstProjBuilder[0].Address_Line_1__c) && String.isBlank(lstProjBuilder[0].Address_Line_2__c)) {
                            returnMsg = 'Address not present. Could Not Initiate Corporate Cibil.';
                        }
                        else if(String.isBlank(lstProjBuilder[0].City__c) || String.isBlank(lstProjBuilder[0].State__c) || String.isBlank(lstProjBuilder[0].Pincode__c)) {
                            returnMsg = 'Please fill City, Pin and State in Address before proceeding.';
                        }
                        else {
                            returnMsg = 'CORPORATE';
                        }
                    }
                }
            }
            
            else if(sObjName == Constants.strPromoterAPI) {
                lstPromoter = [Select Id, Name, Pan_Verification_status__c, PAN_Number__c, PAN_Response__c From Promoter__c Where Id =: strRecordId LIMIT 1];
                if(!lstPromoter.isEmpty() && String.isNotBlank(lstPromoter[0].PAN_Number__c)) {
                    returnMsg = 'INDIVIDUAL';
                }
            }
        }
        return returnMsg;
    }
    
    @AuraEnabled
    public static ResponseCIBILWrapper createCustomerIntegration( Id strRecordId,Boolean hitCIBILAgain)
    {
        system.debug('Debug Log for hitCIBILAgain param'+hitCIBILAgain);
        system.debug('Debug Log for strRecordId param'+strRecordId);
        if(String.isNotBlank(strRecordId)) {
            String sObjName = strRecordId.getSObjectType().getDescribe().getName();
            System.debug('Debug Log for sObjName'+sObjName);
            List<Project_Builder__c> lstProjBuilder = new List<Project_Builder__c>();
            List<Promoter__c> lstPromoter = new List<Promoter__c>();
            
            if(sObjName == Constants.strProjectBuilderAPI) {
                lstProjBuilder = [SELECT id,CIBIL_CheckUp__c,Constitution__c,Pan_Verification_status__c,CIBIL_Verified__c, PAN_Number__c,Builder_Type__c,Builder_Name__r.Name, Builder_Name__r.Retrigger_Corp_CIBIL__c, Retrigger_Corp_CIBIL__c,Contact_Number_Landline__c, Contact_Number_Mobile__c,
                                  CIBIL_Response__c,Address_Line_1__c, Address_Line_2__c, City__c, City_ID__c, Country__c, Pincode__c, State__c,Project__c,
                                  (SELECT Id,Name,Check_Expired__c from Builder_Integrations__r WHERE RecordType.Name='CIBIL' ORDER BY CreatedDate DESC LIMIT 1)          
                                  FROM    Project_Builder__c
                                  WHERE   Id =: strRecordId LIMIT 1];
                if(!lstProjBuilder.isEmpty()) {
                    System.debug('Debug Log for lstProjBuilder'+lstProjBuilder);
                    Boolean retriggerCorpCIBIL = false; 
                    Boolean CIBILExpired = false;
                    if(!lstProjBuilder[0].Builder_Integrations__r.isEmpty()){
                        for(Builder_Integrations__c CI: lstProjBuilder[0].Builder_Integrations__r){
                            if(CI.Check_Expired__c == 'Yes' && lstProjBuilder[0].CIBIL_Verified__c){
                                CIBILExpired = true;
                                break;
                            }
                        }    
                    }
                    if(!hitCIBILAgain){
                        if(lstProjBuilder[0].CIBIL_Verified__c && CIBILExpired == false && (lstProjBuilder[0].Retrigger_Corp_CIBIL__c == false && retriggerCorpCIBIL == false && lstProjBuilder[0].Builder_Name__r.Retrigger_Corp_CIBIL__c == false)){
                            return new ResponseCIBILWrapper(false,'CIBIL record already exists! Are you sure you want to initiate again?');
                        }
                    }
                    Id userId = UserInfo.getUserId();
                    User u = new User();
                    u = [SELECT Id, Profile.Name FROM User WHERE Id =: userId];
                    
                    
                    
                    Builder_Integrations__c integrations = new Builder_Integrations__c();
                    integrations.Project_Builder__c = strRecordId;
                    integrations.Project__c = lstProjBuilder[0].Project__c;
                    integrations.recordTypeId = Schema.SObjectType.Builder_Integrations__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId();
                    
                    try{
                        insert integrations;
                    }catch(DMLException e) {
                        return new ResponseCIBILWrapper(false,e.getMessage().substringafter(','));
                    }
                    
                    lstProjBuilder[0].CIBIL_CheckUp__c = true;
                    lstProjBuilder[0].CIBIL_Verified__c = true;
                    update lstProjBuilder[0];
                    return new ResponseCIBILWrapper(true,integrations.Id);
                    
                    
                }
            }
        }
        
        return new ResponseCIBILWrapper(false,'Operation Not allowed');
        
    }
    
    @AuraEnabled
    public static ResponseCIBILWrapper integrateCibil(Id strRecordId, Id customerIntegrationId) {
        if(String.isNotBlank(strRecordId)) {
            String sObjName = strRecordId.getSObjectType().getDescribe().getName();
            System.debug('Debug Log for sObjName'+sObjName);
            List<Project_Builder__c> lstProjBuilder = new List<Project_Builder__c>();
            List<Promoter__c> lstPromoter = new List<Promoter__c>();    
            Project__c objProject;
            
            Rest_Service__mdt restService = [SELECT  MasterLabel, QualifiedApiName , Service_EndPoint__c , 
                                             Client_Password__c, Client_Username__c, JSONBody__c, Request_Method__c, Time_Out_Period__c  
                                             FROM   
                                             Rest_Service__mdt
                                             WHERE  
                                             MasterLabel = 'Cibil Corporate'];       
            
            System.debug('@@strRecordId' + strRecordId);
            if(sObjName == Constants.strProjectBuilderAPI) {
                lstProjBuilder = [Select Id, Name, Residence_Type__c,Gender__c,  Address_Line_1__c,Project__c, Age__c, Class_Of_Activity__r.Full_Name__c, Class_Of_Activity__c, VoterID_Father_s_Name__c, Spouse_Name__c, Marital_Status__c, Voter_ID_Number__c, No_of_Dependents__c, Pincode__r.City_LP__r.State__c, PAN_Number__c, Address_Line_2__c, City__c, City_ID__c, Country__c, Pincode__c, Pincode__r.Name, Pincode__r.City_LP__c, State__c, Builder_Type__c,Builder_Name__c,Builder_Name__r.Name,Builder_Name__r.Date_of_Incorporation__c, Date_Of_Birth__c, Company_Registration_Number__c,Constitution__c, Contact_Number_Landline__c, Contact_Number_Mobile__c From Project_Builder__c where Id =: strRecordId];
                
                if(!lstProjBuilder.isEmpty()) {
                    objProject = [Select Id, Name,(Select Id, Name, Residence_Type__c,Address_Line_1__c, PAN_Number__c, Address_Line_2__c, City__c, City_ID__c, Country__c, Pincode__c, Pincode__r.Name, State__c, Builder_Type__c,Builder_Name__c,Builder_Name__r.Gender__c,Builder_Name__r.Name,Builder_Name__r.Date_of_Incorporation__c, Date_Of_Birth__c, Company_Registration_Number__c,Constitution__c, Contact_Number_Landline__c, Contact_Number_Mobile__c, Gender__c, Marital_Status__c, VoterID_Father_s_Name__c, Spouse_Name__c, Age__c, No_of_Dependents__c From Project_Builders__r) From Project__c Where Id=:lstProjBuilder[0].Project__c];
                    if( lstProjBuilder[0].Builder_Type__c == Constants.strProjectBuilderBTIndv ) {
                        return new ResponseCIBILWrapper(false,'Corporate CIBIL is not valid for individual customer.');
                    } 
                    if (lstProjBuilder[0].PAN_Number__c == null && lstProjBuilder[0].Contact_Number_Landline__c == null && lstProjBuilder[0].Contact_Number_Mobile__c == null) {
                        return new ResponseCIBILWrapper(false,'You cannot proceed further, atleast one out of PAN or Phone Number is mandatory');
                    }
                }
            }
            
            cls_Request objReq=new cls_Request();
            Header objHeader = new Header();
            
            objHeader.ApplicationId= lstProjBuilder[0].Id != null ? lstProjBuilder[0].Id : '';
            objHeader.CustId = strRecordId != null ? strRecordId : '';
            objHeader.RequestType = 'REQUEST';
            objHeader.RequestTime=String.valueOf(System.now());
            objHeader.SFDCRecordId=customerIntegrationId != null ? customerIntegrationId : '';
            objReq.Header=objHeader;
            Request req= new Request();
            req.Priority=''; 
            req.ProductType='CCIR';  
            req.LoanType='2';  
            req.LoanAmount= '0.00'; 
            req.JointInd='';  //missing
            req.InquirySubmittedBy='';  //missing
            req.SourceSystemName='COMM';  //missing
            req.SourceSystemVersion=''; //missing
            req.SourceSystemVender=''; //missing
            req.SourceSystemInstanceId=''; //missing
            req.BureauRegion='QA/UAT'; 
            req.LoanPurposeDesc='Property Loan';//getPicklistLabel(objProject.Loan_Purpose__c);  
            req.BranchIfsccode = '';//objProject.Bank_Master__r.IFSC_code__c != null ? objProject.Bank_Master__r.IFSC_code__c : ''; 
            req.Kendra=''; 
            req.InquiryStage=''; //missing
            req.AuthrizationFlag=''; //missing
            req.AuthrizationBy=''; //missing
            req.IndividualCorporateFlag=''; 
            req.Constitution=lstProjBuilder[0].Constitution__c!= null ? String.valueOf(lstProjBuilder[0].Constitution__c) : ''; 
            req.Name=lstProjBuilder[0].Builder_Name__r.Name != null ? lstProjBuilder[0].Builder_Name__r.Name : ''; 
            req.ShortName=lstProjBuilder[0].Builder_Name__r.Name != null ? lstProjBuilder[0].Builder_Name__r.Name : ''; 
            req.IncorpDt=lstProjBuilder[0].Builder_Name__r.Date_of_Incorporation__c != null ? String.valueOf(DateTime.newInstance(lstProjBuilder[0].Builder_Name__r.Date_of_Incorporation__c.year(), lstProjBuilder[0].Builder_Name__r.Date_of_Incorporation__c.month(), lstProjBuilder[0].Builder_Name__r.Date_of_Incorporation__c.day()).format('ddMMYYYY')) : ''; 
            
            Address objAddress = new Address();
            List<Address> lstAddress = new List<Address>();
            
            
            // if(getAddressType(lstProjBuilder[0].Type_of_address__c) == lstProjBuilder[0].Preferred_Communication_address__c){
            objAddress = new Address();
            //objAddress.AddressType=lstProjBuilder[0].Address_Type__c != null ? getAddressType(lstProjBuilder[0].Address_Type__c) : '';
            objAddress.AddressType = 'REGISTERED OFFICE';
            objAddress.AddressResidenceCode=lstProjBuilder[0].Residence_Type__c != null ? lstProjBuilder[0].Residence_Type__c: '';
            if (lstProjBuilder[0].Residence_Type__c == 'S' )
                objAddress.AddressResidenceCode = 'OWNED' ;  
            else 
                objAddress.AddressResidenceCode = 'RENTED' ;    
            objAddress.Address=lstProjBuilder[0].Address_Line_1__c + lstProjBuilder[0].Address_Line_2__c;
            objAddress.AddressCity= lstProjBuilder[0].City__c != null ? lstProjBuilder[0].City__c : '';
            objAddress.AddressPin = lstProjBuilder[0].Pincode__r.Name != null ? (lstProjBuilder[0].Pincode__r.Name) : '0';
            objAddress.AddressState= lstProjBuilder[0].Pincode__r.City_LP__r.State__c != null ? getStateCode(lstProjBuilder[0].Pincode__r.City_LP__r.State__c) : '';
            if(objAddress.AddressState=='DELHI') {
                objAddress.AddressState='New Delhi';
            }
            objAddress.AddressTan='';
            lstAddress.add(objAddress);
            // }
            
            
            req.Address = lstAddress;
            
            Identity id= new Identity();
            id.Pan = lstProjBuilder[0].PAN_Number__c != null ? lstProjBuilder[0].PAN_Number__c : '';
            id.Duns = '';//missing
            id.Cin= lstProjBuilder[0].Company_Registration_Number__c != null ?  String.valueOf(lstProjBuilder[0].Company_Registration_Number__c) : '';
            id.Tin= '';//missing
            id.IdType1= 'Voter Id';
            id.IdType1Value= lstProjBuilder[0].Voter_ID_Number__c != null ? lstProjBuilder[0].Voter_ID_Number__c : '';
            id.IdType2= 'PAN Id';
            id.IdType2Value= lstProjBuilder[0].PAN_Number__c != null ? lstProjBuilder[0].PAN_Number__c : '';
            req.Id = id;
            
            Phone phone1 = new Phone();
            phone1.PhoneType = 'Mobile Phone';
            phone1.PhoneNumber = lstProjBuilder[0].Contact_Number_Mobile__c != null ? String.valueOf(lstProjBuilder[0].Contact_Number_Mobile__c) : '0';
            phone1.PhoneExtn=lstProjBuilder[0].Contact_Number_Landline__c != null ? String.valueOf(lstProjBuilder[0].Contact_Number_Landline__c) : '0';
            phone1.StdCode='0';
            req.Phone = phone1;
            
            req.ActOpeningDt='';//missing
            req.AccountNumber1='';//missing
            req.AccountNumber2='';//missing
            req.AccountNumber3='';//missing
            req.AccountNumber4='';
            req.Tenure='';//objProject.Approved_Loan_Tenure__c != null ? String.valueOf(objProject.Approved_Loan_Tenure__c) : '0';
            req.GroupId='';
            req.CompanyCategory=lstProjBuilder[0].Constitution__c != null ? getEntityType(lstProjBuilder[0].Constitution__c) : 'Others';
            
            req.NatureOfBusiness = lstProjBuilder[0].Class_Of_Activity__c != null && lstProjBuilder[0].Class_Of_Activity__r.Full_Name__c != null ? lstProjBuilder[0].Class_Of_Activity__r.Full_Name__c : '';
            
            List<RelatedIndividuals> lstrelIndvdual = new List<RelatedIndividuals>();
            if(objProject.Project_Builders__r.size()>0){
                RelatedIndividuals objrI= new RelatedIndividuals();    
                for(Project_Builder__c objlc: objProject.Project_Builders__r){
                    if(objlc.Builder_Type__c == Constants.strProjectBuilderBTIndv && objlc.Id != strRecordId){
                        system.debug('@@@@objlc');
                        if( objlc.Gender__c == null ) {
                            return new ResponseCIBILWrapper(false,'Please fill Gender for ' + objlc.Name);
                        } 
                        
                        
                        IndividualEntity objIndvalEntity = new IndividualEntity();
                        objIndvalEntity.IndvEntityType='60';//objlc.Builder_Type__c;--It will be master driven acccording to Cibil Parameters, which are not captured in SFDC, 60 is a key for Others
                        
                        IndvEntityName objIndvEntityName = new IndvEntityName();
                        objIndvEntityName.Name1 = objlc.Builder_Name__r.Name;
                        objIndvalEntity.IndvEntityName=objIndvEntityName;
                        
                        objIndvalEntity.Din='';
                        objIndvalEntity.Gender=objlc.Builder_Name__r.Gender__c != null ? objlc.Builder_Name__r.Gender__c : '';
                        objIndvalEntity.MaritalStatus= objlc.Marital_Status__c != null ? objlc.Marital_Status__c : '';
                        
                        IndvEntityRelation objIndvEntityRel = new IndvEntityRelation();
                        objIndvEntityRel.FatherName = objlc.VoterID_Father_s_Name__c != null ? objlc.VoterID_Father_s_Name__c : '';
                        objIndvEntityRel.SpouseName= objlc.Spouse_Name__c != null ? objlc.Spouse_Name__c : '';
                        objIndvEntityRel.MotherName='';//objlc.Mother_s_Maiden_Name__c != null ? objlc.Mother_s_Maiden_Name__c : '';
                        objIndvEntityRel.RelationType1='';//'Relationship with Applicant';
                        objIndvEntityRel.RelationType1value='';//objlc.Relationship_with_Applicant__c != null ? objlc.Relationship_with_Applicant__c : '';
                        objIndvEntityRel.RelationType2='';
                        objIndvEntityRel.RelationType2Value='';
                        objIndvEntityRel.KeyPersonName='';
                        objIndvEntityRel.KeyPersonRelation='';
                        objIndvEntityRel.NomineeName='';
                        objIndvEntityRel.NomineeRelationType='';
                        objIndvalEntity.IndvEntityRelation =objIndvEntityRel;
                        
                        objIndvalEntity.Age= objlc.Age__c != null ? String.valueOf(objlc.Age__c) : '0';
                        objIndvalEntity.AgeASonDT='';
                        
                        objIndvalEntity.BirthDT =  objlc.Date_Of_Birth__c != null ? DateTime.newInstance(objlc.Date_Of_Birth__c.year(),objlc.Date_Of_Birth__c.month(),objlc.Date_Of_Birth__c.day()).format('ddMMYYYY') : '';
                        objIndvalEntity.NoOfDependents = objlc.No_of_Dependents__c != null ? String.valueOf(objlc.No_of_Dependents__c) : '0';
                        
                        Address objIndvEntityAddress= new Address();
                        
                        
                        //objIndvEntityAddress.AddressType=lstProjBuilder[0].Address_Type__c != null ? getAddressType(lstProjBuilder[0].Address_Type__c) : '';
                        objIndvEntityAddress.Addresstype = 'REGISTERED OFFICE';
                        objIndvEntityAddress.AddressResidenceCode=objlc.Residence_Type__c != null ? objlc.Residence_Type__c : '';
                        if ( lstProjBuilder[0].Residence_Type__c == 'S' )
                            objIndvEntityAddress.AddressResidenceCode = 'OWNED' ;  
                        else 
                            objIndvEntityAddress.AddressResidenceCode = 'RENTED' ;
                        objIndvEntityAddress.Address=objlc.Address_Line_1__c + objlc.Address_Line_2__c;
                        objIndvEntityAddress.AddressCity= objlc.City__c;
                        objIndvEntityAddress.AddressPin = objlc.Pincode__r.Name != null ? (objlc.Pincode__r.Name) : '0';
                        if(getStateCode(objlc.Pincode__r.City_LP__r.State__c) == 'DELHI') {
                            objIndvEntityAddress.AddressState= 'New Delhi';
                        }
                        else {
                            objIndvEntityAddress.AddressState= getStateCode(objlc.Pincode__r.City_LP__r.State__c);
                        }
                        
                        objIndvEntityAddress.AddressTan='';
                        
                        
                        
                        
                        objIndvalEntity.IndvEntityAddress =objIndvEntityAddress;
                        Identity id1= new Identity();
                        id1.Pan = objlc.PAN_Number__c != null ? objlc.PAN_Number__c : '';
                        id1.Duns = '';//missing
                        id1.Cin= objlc.Company_Registration_Number__c != null ?  String.valueOf(objlc.Company_Registration_Number__c) : '';
                        id1.Tin= '';//missing
                        id1.IdType1= 'Voter Id';
                        id1.IdType1Value= lstProjBuilder[0].Voter_ID_Number__c != null ? lstProjBuilder[0].Voter_ID_Number__c : '';
                        id1.IdType2= 'PAN Id';
                        id1.IdType2Value= objlc.PAN_Number__c != null ? objlc.PAN_Number__c : '';
                        objIndvalEntity.IndvEntityId = id1;
                        
                        Phone objphone = new Phone();
                        objphone.PhoneType = 'Mobile Phone';
                        objphone.PhoneNumber = objlc.Contact_Number_Mobile__c != null ? String.valueOf(objlc.Contact_Number_Mobile__c) : '0';
                        objphone.PhoneExtn=objlc.Contact_Number_Landline__c != null ? String.valueOf(objlc.Contact_Number_Landline__c) : '0';
                        objphone.StdCode='0';
                        objIndvalEntity.IndvEntityPhone = objphone;
                        
                        objIndvalEntity.EmailId1='';
                        objIndvalEntity.EmailId2='';
                        
                        objrI.IndividualEntity =objIndvalEntity;
                        lstrelIndvdual.add(objrI);
                    }
                }    
            }
            
            req.RelatedIndividuals = lstrelIndvdual;
            
            List<RelatedOrganizations> lstRelOrg = new List<RelatedOrganizations>();
            if(objProject.Project_Builders__r.size()>0){
                RelatedOrganizations objRelOrg = new RelatedOrganizations();
                for(Project_Builder__c objlc: objProject.Project_Builders__r){
                    if(objlc.Builder_Type__c =='Corporate' && objlc.Id == strRecordId){
                        OrganizationEntity objOrgEnt = new OrganizationEntity();
                        objOrgEnt.OrgnEntityType= '60';//getEntityType(objlc.Constitution__c);
                        objOrgEnt.OrgnEntityName= objlc.Builder_Name__r.Name != null ? objlc.Builder_Name__r.Name : '';
                        objOrgEnt.OrgnEntityShortName='';
                        objOrgEnt.IncorpDt=objlc.Builder_Name__r.Date_of_Incorporation__c != null ? String.valueOf(DateTime.newInstance(objlc.Builder_Name__r.Date_of_Incorporation__c.year(),objlc.Builder_Name__r.Date_of_Incorporation__c.month(), objlc.Builder_Name__r.Date_of_Incorporation__c.day()).format('ddMMYYYY')) : '';
                        objRelOrg.OrganizationEntity =objOrgEnt;
                        
                        Address objEntityAdd= new Address();
                        objEntityAdd.AddressType = 'REGISTERED OFFICE';
                        objEntityAdd.AddressResidenceCode=objlc.Residence_Type__c != null ? objlc.Residence_Type__c : '';
                        if ( objlc.Residence_Type__c == 'S' )
                            objEntityAdd.AddressResidenceCode = 'OWNED' ;  
                        else 
                            objEntityAdd.AddressResidenceCode = 'RENTED' ; 
                        objEntityAdd.Address=objlc.Address_Line_1__c + objlc.Address_Line_2__c;
                        objEntityAdd.AddressCity= objlc.City__c;
                        objEntityAdd.AddressPin = objlc.Pincode__r.Name != null ? (objlc.Pincode__r.Name) : '0';
                        if(getStateCode(objlc.Pincode__r.City_LP__r.State__c) == 'DELHI') {
                            objEntityAdd.AddressState='New Delhi';
                        }
                        else {
                            objEntityAdd.AddressState= getStateCode(objlc.Pincode__r.City_LP__r.State__c);
                        }
                        
                        objEntityAdd.AddressTan='';
                        //}
                        
                        
                        objRelOrg.OrgnEntityAddress =objEntityAdd;
                        
                        Identity id1= new Identity();
                        id1.Pan = objlc.PAN_Number__c != null ? objlc.PAN_Number__c : '';
                        id1.Duns = '';//missing
                        id1.Cin= '';
                        id1.Tin= '';//missing
                        id1.IdType1= 'Voter Id';
                        id1.IdType1Value= lstProjBuilder[0].Voter_ID_Number__c != null ? lstProjBuilder[0].Voter_ID_Number__c : '';
                        id1.IdType2= 'PAN Id';
                        id1.IdType2Value= objlc.PAN_Number__c != null ? objlc.PAN_Number__c : '';
                        objRelOrg.OrgnentityId = id1;
                        
                        Phone objphone = new Phone();
                        objphone.PhoneType = 'Mobile Phone';
                        objphone.PhoneNumber = objlc.Contact_Number_Mobile__c != null ? String.valueOf(objlc.Contact_Number_Mobile__c) : '0';
                        objphone.PhoneExtn=objlc.Contact_Number_Landline__c != null ? String.valueOf(objlc.Contact_Number_Landline__c) : '0';
                        objphone.StdCode='0';
                        objRelOrg.OrgnentityPhone = objphone;
                        
                        objRelOrg.ClassOfActivity= 'RE';//objlc.Class_Of_Activity__r.Full_Name__c != null ? objlc.Class_Of_Activity__r.Full_Name__c : '';
                        objRelOrg.DateOfRegistration=objlc.Builder_Name__r.Date_of_Incorporation__c != null ? String.valueOf(DateTime.newInstance(objlc.Builder_Name__r.Date_of_Incorporation__c.year(), objlc.Builder_Name__r.Date_of_Incorporation__c.month(), objlc.Builder_Name__r.Date_of_Incorporation__c.day()).format('ddMMYYYY')) : ''; 
                        objRelOrg.CmrFlag='';
                        objRelOrg.GstDestState='';
                        
                        lstRelOrg.add(objRelOrg);
                    }
                }
            }
            
            req.RelatedOrganizations = lstRelOrg;
            /*if ( objProject.Product_Name__c == 'Home Loan' ) 
req.BureauProductType = 'HOME_LOAN';
else if (objProject.Product_Name__c == 'Construction Finance' )
req.BureauProductType = 'CONSTRUCTION_FINANCE';
else if (objProject.Product_Name__c == 'Loan Against Property' )
req.BureauProductType = 'LOAN_AGAINST_PROPERTY';
else
req.BureauProductType= '';*/
            req.BureauProductType= 'CIR';
            
            
            System.debug('req::::'+req);
            objReq.Request=req;
            
            String jsonRequest = Json.serialize(objReq,true);
            System.debug('jsonRequest::::'+jsonRequest);
            HttpResponse res=(HttpResponse) returnResponse(restService.Client_Username__c,restService.Client_Password__c,restService.Service_EndPoint__c,restService.Request_Method__c, jsonRequest , Integer.valueOf(restService.Time_Out_Period__c),restService.MasterLabel );
            System.debug('res::::'+res);
            
            if (res != null) {
                
                if(res.getStatusCode() == 200)
                {
                    if(res.getBody().contains('errorMessage')){
                        Error objresult = (Error)JSON.deserialize(res.getBody(), Error.class);
                        system.debug('objresult::'+objresult);
                        system.debug('@@objResult.Error.errorMessage' + objResult.Error.errorMessage);
                        
                        if (objResult.Error.errorMessage.containsignoreCase('address') && objResult.Error.errorMessage.containsignoreCase('missing')) {
                            return new ResponseCIBILWrapper(false, 'Please fill in all address details for all applicants and co-applicants.');
                        } else if( objResult.Error.errorMessage.containsignorecase('REL_INDV_PAN is compulsory')) {
                            return new ResponseCIBILWrapper(false, 'Please fill in PAN for all the individuals before hitting Corporate CIBIL.');
                        } else {
                            return new ResponseCIBILWrapper(false, objResult.Error.errorMessage);
                        }
                        
                    }
                    else{
                        Resultresponse objresult = (Resultresponse)JSON.deserialize(res.getBody(), Resultresponse.class);
                        system.debug('objresult::'+objresult);
                        return new ResponseCIBILWrapper(true, 'Request submitted!!');
                    }
                }
                //system.debug('objresult::'+res.getBody().contains('Service Unavailable'));
                
                else if (res.getStatusCode() != 200){
                    if(res.getBody().contains('Service Unavailable')){
                        return new ResponseCIBILWrapper(false, 'Service Unavailable');
                    }
                    else{
                        Error objresult = (Error)JSON.deserialize(res.getBody(), Error.class);
                        system.debug('objresult::'+objresult);
                        if (objResult.Error.errorMessage.containsignoreCase('address') && objResult.Error.errorMessage.containsignoreCase('missing')) {
                            return new ResponseCIBILWrapper(false, 'Please fill in all address details for all applicants and co-applicants');
                        } else{
                            return new ResponseCIBILWrapper(false, objResult.Error.errorMessage);
                        }
                        
                    }
                    
                } 
            }
            
            
            return new ResponseCIBILWrapper(false, 'Invalid Request, please contact system Admin');
        }
        return new ResponseCIBILWrapper(false, 'Invalid Request, please contact system Admin');
    }
    
    @AuraEnabled
    public static object returnResponse(String clientUserName,String clientPassword,String endPointURL, String method, String body , Integer timeOut, String MasterLabel ) {
        Map<String,String> headerMap = new Map<String,String>();                                                      
        
        Blob headerValue = Blob.valueOf(clientUserName + ':' + clientPassword);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        headerMap.put('Authorization',authorizationHeader);
        //headerMap.put('Username',clientUserName);
        //headerMap.put('Password',clientPassword);
        headerMap.put('Content-Type','application/json');
        headerMap.put('Accept','application/json');
        //headerMap.put('vendorId','heroHousing');
        //endPointURL = 'https://mocksvc.mulesoft.com/mocks/be101d80-47ec-4fb2-a52f-471d4b04a872/api/multiBureauCommercial';
        HttpResponse res   = Utility.sendRequest(endPointURL, method, body , headerMap, timeOut );
        
        return res;
        
    }
    
    public static String getEntityType( String strConstitution) {
        if ( strConstitution == '18') 
            return 'PRIVATE LIMITED';
        if ( strConstitution == '19') 
            return 'PUBLIC LIMITED';    
        if ( strConstitution == '21') 
            return 'HINDU UNDIVIDED FAMILY';
        if ( strConstitution == '22') 
            return 'PROPRIETORSHIP';    
        if ( strConstitution == '23') 
            return 'PARTNERSHIP';
        if ( strConstitution == '24') 
            return 'TRUST'; 
        else 
            return 'NOT CLASSIFIED';
    }
    
    public static String getStateCode( String stateName )
    {
        List<CIBIL_State_Mapping__mdt> allState = [Select State_Code__c,State__c,LMS_Code__c,Id from CIBIL_State_Mapping__mdt where  LMS_Code__c = :stateName];
        if(allState.size() > 0 )
        {
            return allState[0].State__c;
        }
        return stateName;
    }
    
    
    public Class Error
    {
        @AuraEnabled
        public String status;
        @AuraEnabled
        public Error_Cls Error;
        public Error()
        {
            this.status = status;
            Error_Cls err= new Error_Cls();
            this.Error = err;
        }
    }
    
    public Class Error_Cls
    {
        @AuraEnabled
        public Integer errorCode;
        @AuraEnabled
        public String errorType;
        @AuraEnabled
        public String errorMessage;
        public Error_Cls()
        {
            this.errorCode = errorCode;
            this.errorType = errorType;
            this.errorMessage = errorMessage;
            
        }
        
    }
    
    public Class Resultresponse
    { 
        @AuraEnabled
        public Header Header;
        public String AcknowledgementId;
        public String Status;
        
        
    }
    
    class cls_Request {
        public Header Header;
        public Request Request;
    }
    
    public class Header{
        public String ApplicationId;
        public String CustId;  
        public String RequestType;
        public String RequestTime;
        public String SFDCRecordId;
    }
    
    class Request {
        
        public String Priority;
        public String ProductType;  
        public String LoanType;
        public String LoanAmount;
        public String JointInd;
        public String InquirySubmittedBy;
        public String SourceSystemName;
        public String SourceSystemVersion;
        public String SourceSystemVender;
        public String SourceSystemInstanceId;
        public String BureauRegion;
        public String LoanPurposeDesc;
        public String BranchIfsccode;
        public String Kendra;
        public String InquiryStage;
        public String AuthrizationFlag;
        public String AuthrizationBy;
        public String IndividualCorporateFlag;
        public String Constitution;
        public String Name;
        public String ShortName;
        public String IncorpDt;
        public List<Address> Address;
        public Identity Id;
        public Phone Phone;
        public String ActOpeningDt;
        public String AccountNumber1;
        public String AccountNumber2;
        public String AccountNumber3;
        public String AccountNumber4;
        public String Tenure;
        public String GroupId;
        public String CompanyCategory;
        public String NatureOfBusiness;
        public List<RelatedIndividuals> RelatedIndividuals;
        public List<RelatedOrganizations> RelatedOrganizations;
        public String BureauProductType;
    }
    class Address {
        public String AddressType;
        public String AddressResidenceCode;
        public String Address;
        public String AddressCity;
        public String AddressPin;
        public String AddressState;
        public String AddressTan;
    }
    class Identity{
        public String Pan;
        public String Duns;
        public String Cin;
        public String Tin;
        public String IdType1;
        public String IdType1Value;
        public String IdType2;
        public String IdType2Value;
    }
    class Phone{
        public String PhoneType;
        public String PhoneNumber;
        public String PhoneExtn;
        public String StdCode;
    }
    class RelatedIndividuals{
        public IndividualEntity IndividualEntity;
    }
    class IndividualEntity{
        public String IndvEntityType;
        public IndvEntityName IndvEntityName;
        public String Din;
        public String Gender;
        public String MaritalStatus;
        public IndvEntityRelation IndvEntityRelation;
        public String Age;
        public String AgeASonDT;
        public String BirthDT;
        public String NoOfDependents;
        public Address IndvEntityAddress;
        public Identity IndvEntityId;
        public Phone IndvEntityPhone;
        public String EmailId1;
        public String EmailId2;
    }
    class IndvEntityName{
        public String Name1;
        public String Name2;
        public String Name3;
        public String Name4;
        public String Name5;
    }
    class IndvEntityRelation{
        public String FatherName;
        public String SpouseName;
        public String MotherName;
        public String RelationType1;
        public String RelationType1value;
        public String RelationType2;
        public String RelationType2Value;
        public String KeyPersonName;
        public String KeyPersonRelation;
        public String NomineeName;
        public String NomineeRelationType;
    }
    class IndvEntityAddress{
        public String AddressType;
        public String AddressResidenceCode;
        public String Address;
        public String AddressCity;
        public String AddressPin;
        public String AddressState;
    }
    class IndvEntityId{
        public String Pan;
        public String Duns;
        public String Cin;
        public String Tin;
        public String Idtype1;
        public String IdType1Value;
        public String IdType2;
        public String IdType2Value;
        public String ServiceTaxNo;
        public String Crn;
        public String PssportId;
        public String VoterId;
        public String DrivingLicenseNo;
        public String UidNo;
        public String RationCard;
    }
    class IndvEntityPhone{
        public String PhoneType;
        public String PhoneNumber;
        public String PhoneExtn;
        public String StdCode;
    }
    class RelatedOrganizations{
        public OrganizationEntity OrganizationEntity;
        public Address OrgnEntityAddress;
        public Identity OrgnentityId;
        public Phone OrgnentityPhone;
        public String ClassOfActivity;
        public String DateOfRegistration;
        public String CmrFlag;
        public String GstDestState;
    }
    class OrganizationEntity{
        public String OrgnEntityType;
        public String OrgnEntityName;
        public String OrgnEntityShortName;
        public String IncorpDt;
    }
    
    
    public Class ResponseCIBILWrapper
    {
        @AuraEnabled
        public boolean failure;
        @AuraEnabled
        public String respString;
        public ResponseCIBILWrapper(boolean failure,String respString)
        {
            this.failure = failure;
            this.respString = respString;
        }
        
    }
    
    
}