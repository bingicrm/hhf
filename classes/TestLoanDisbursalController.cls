@isTest
private class TestLoanDisbursalController {
    
     @TestSetup
    static void setup()
    {
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        ID roleID= r.id;
        User u = new User(
            ProfileId ='00e7F000002yr4O',
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = roleID);
        Insert u;
    }
    static testmethod void test1(){
       Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Name='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567890',RecordTypeId=RecordTypeIdAccount);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567891',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact cnt2 = new Contact (LastName = 'LNameLoanContact', AccountId = acc1.Id, MobilePhone = '9999667739');
        insert cnt2;
        
        Account acc2 = new Account(PAN__c='ABCDE1234H',Salutation='Mr',FirstName='Suresh',LastName='Nath',
                                   Date_of_Birth__c =Date.newInstance(1990,11,02),
                                   PersonMobilePhone='9234567892',PersonEmail='sn@gmail.com',RecordTypeId=RecordTypeIdAccount1);
        insert acc2;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Test.startTest();
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert objLoanApplication;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Guarantor').getRecordTypeId();
        
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc1.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='2',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='18',
                                                             Customer_segment__c='9',Applicant_Status__c='1',
                                                             GSTIN_Available__c=true, Contact__c = cnt2.Id,
                                                             GSTIN_Number__c = '27AAHFG0551H1ZL',
                                                             income_considered__c = false);
        insert objLoanContact;
        
       /* Id RecordTypeIdDisbursement = Schema.SObjectType.Disbursement__c.getRecordTypeInfosByName().get('First Disbursement').getRecordTypeId();
        
        Disbursement__c objDisbursement = new Disbursement__c(Loan_Application__c=objLoanApplication.Id);
        insert objDisbursement;
        
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=objLoanApplication.Id,
                                                                     Loan_Contact__c=objLoanContact.Id,Status__c='Lawyer OTC');
        insert objDocCheck;*/
        
        Id RecordTypeIdSourcingDetail = Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('DSA').getRecordTypeId();
        
        DST_Master__c objDSTMaster = new DST_Master__c(Name='Test DST',Inspector_ID__c='1234');
        insert objDSTMaster;
        /*
        Sourcing_Detail__c objSourcingDetail = new Sourcing_Detail__c(Loan_Application__c=objLoanApplication.Id,
                                                                      DST_Name__c=objDSTMaster.Id,
                                                                      Sales_User_Name__c=u.Id);
        insert objSourcingDetail;
        */
        PageReference pageRef = Page.LoanDisbursal;
        Test.setCurrentPage(pageRef);
        
        pageRef.getParameters().put('id', objLoanApplication.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objLoanApplication);
        
        LoanDisbursalController objLoanDisbursalController = new LoanDisbursalController(sc);
    }

}