@isTest

public class TestHttp_Callout_Mitc {
    public static testMethod void insertLA(){
        Id RecordTypeIdLoanApp = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('COPS:Data Checker').getRecordTypeId();
        Branch_Master__c objBM = new Branch_Master__c(Name='Delhi',Office_Code__c='1');
        insert objBM;
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9934567890');
        insert acc;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        List<Document_Master__c> lstDocMaster = new List<Document_Master__c>();
        Document_Master__c docMasObj = new Document_Master__c();
        docMasObj.Name = 'MITC';
        docMasObj.Doc_Id__c = '123344';
        //docMasObj.Approving_authority__c = 'Business Head';
       // docMasObj.Applicable_for_APF__c = true;
        //lstDocMaster.add(docMasObj);
        insert docMasObj;
         LMS_Date_Sync__c objLMSDateSync = new LMS_Date_Sync__c(Current_Date__c=System.Today());
            insert objLMSDateSync;
            
            List<Charge_Code__c> lstOfChargeCodes = new List<Charge_Code__c> ();
            
            Charge_Code__c objChargeCode = new Charge_Code__c(Name='ROC CHARGES',ChargeCodeID__c='Test',Charge_Rate__c=5);
            lstOfChargeCodes.add(objChargeCode);
            
            Charge_Code__c objChargeCode1 = new Charge_Code__c(Name='STAMP DUTY',ChargeCodeID__c='Test1',Charge_Rate__c=10);
            lstOfChargeCodes.add(objChargeCode1);
            
            Charge_Code__c objChargeCode2 = new Charge_Code__c(Name='CERSAI Charges',ChargeCodeID__c='Test2',Charge_Rate__c=7);
            lstOfChargeCodes.add(objChargeCode2);
            
            Charge_Code__c objChargeCode3 = new Charge_Code__c(Name='LEGAL CHARGES',ChargeCodeID__c='Test3',Charge_Rate__c=6);
            lstOfChargeCodes.add(objChargeCode3);
            
            Charge_Code__c objChargeCode4 = new Charge_Code__c(Name='PEMI',ChargeCodeID__c='Test4',Charge_Rate__c=6);
            lstOfChargeCodes.add(objChargeCode4);
            
            Charge_Code__c objChargeCode5 = new Charge_Code__c(Name='APPLICATION PROCESSING FEES',ChargeCodeID__c='Test5',Charge_Rate__c=8);
            lstOfChargeCodes.add(objChargeCode5);
            
            insert lstOfChargeCodes;

        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                         Transaction_type__c='PB',
                                                                         Requested_Amount__c=100000,
                                                                         Loan_Purpose__c='11',
                                                                         Sub_Stage__c= Constants.FCU_Review,
                                                                         Previous_Sub_Stage__c = Constants.Credit_Review,
                                                                         Overall_FCU_Status__c = Constants.NEGATIVE,
                                                                         Reviewed_by_FCU_Manager__c = true,
                                                                         StageName__c='Customer Onboarding',
                                                                         RecordTypeId=RecordTypeIdLoanApp,
                                                                         Branch__c='All',Branch_Lookup__c=objBM.Id,
                                                                         Loan_Status__c = 'Active',
                                                                         Insurance_Loan_Created__c = true,
                                                                         Repayment_Start_Date__c=Date.Today().addDays(100),
                                                                         Sanctioned_Disbursement_Type__c='Multiple Tranche',
                                                                         Repayment_Generated_for_Documents__c=true
                                                                        );
        insert objLoanApplication;
        Http_Callout_Mitc.deserializeResponse dR = new Http_Callout_Mitc.deserializeResponse();
        dR.result = 'hello';
        TestMockRequest req1=new TestMockRequest(200,'Complete',JSON.serialize(dR),null);
        Test.setMock(HttpCalloutMock.class,req1);
        Sanction_Condition_Master__c objSCM = new Sanction_Condition_Master__c();
            objSCM.Name = 'Others';
            insert objSCM;
        Loan_Sanction_Condition__c objLSC = new Loan_Sanction_Condition__c();
            objLSC.Loan_Application__c = objLoanApplication.Id;
            objLSC.Sanction_Condition_Master__c = objSCM.Id;
            objLSC.Type_of_Query__c = 'Information Only';
            insert objLSC;
        Test.startTest();
        Http_Callout_Mitc.mitccallout(objLoanApplication.Id);
        Test.stopTest();
    }
}