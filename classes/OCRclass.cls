public class OCRclass {
    
    class GVision {
        @AuraEnabled
        public cls_requests[] requests;
    }
    
    class cls_requests {
        @AuraEnabled
        public cls_image image;
        @AuraEnabled
        public cls_features[] features;

    }
    
    class cls_image {
        //public cls_source source;
        @AuraEnabled
        public String content;  //9j/7QBEUGhvdG9zaG9...base64-encoded-image-content
    }
    
    class cls_source {
        @AuraEnabled
        public String imageUri;
    }
    
    class cls_features {
        @AuraEnabled
        public String type; 
        @AuraEnabled
        public Integer maxResults;
    }
    
    
    public static OCR_Information__c saverec(Id recId, String base64Data,Id loanContact,Id loanApplication, String doc, String subDoc){
        //base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        //blob file = EncodingUtil.base64Decode(base64Data);
        //System.debug('BLOB====>'+file);
        OCR_Information__c ocr = new OCR_Information__c();
        
        try {
            system.debug('Id---'+recId);
            cls_requests[] requests = new cls_requests[]{};
                cls_requests request = new cls_requests();
            cls_image image = new cls_image();
            cls_source source = new cls_source();
            cls_features[] features = new cls_features[]{};
                cls_features feature = new cls_features();
            GVision gVisionObj = new GVision();
            
            feature.type =Constants.TXTDETECT;
            feature.maxResults=1;
            features.add(feature);
            //image.content=EncodingUtil.base64Encode(file);
            image.content = base64Data;
            request.image=image;
            request.features=features;
            requests.add(request);
            gVisionObj.requests = requests;
            
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(Label.VISION);
            req.setMethod(Constants.POST);
            req.setHeader('Content-Type', 'application/json');
            req.setBody(JSON.serialize(gVisionObj));
            HttpResponse res = h.send(req);
            //Create integration logs
            //LogUtility.createIntegrationLogs(Json.serialize(gVisionObj),res,Label.VISION);
            
            list<string> dataJson = new list<string>();
            Util_JSONParser parser = Util_JSONParser.createParser(res.getbody());
            String arrayString = parser.get('responses').Value;
            system.debug('responses>>> ' + arrayString);
            arrayString=arrayString.replaceAll('\r',',').replaceAll('\n',',');
            system.debug('responses> ' + arrayString);
            string resp;
            List<Util_JSONParser> arrayParser = Util_JSONParser.parseArray(arrayString);
            system.debug('responses> ' + arrayString);
            for (Util_JSONParser p : arrayParser) {
                String value6 = p.get(Constants.TXTANNOTATE).Value;
                system.debug('value6> ' + value6);
                List<Util_JSONParser> arrayParser2 = Util_JSONParser.parseArray(value6);
                system.debug('DataJson>---' + arrayParser2[0].get(Constants.OCR_DESC).value);
                resp=arrayParser2[0].get(Constants.OCR_DESC).value;
            } 
            List<String> lstString = resp.split(',');
            for(integer i=0;i<lstString.size();i++){
                System.debug('-----lstString----'+i+lstString[i]);
            }
            
            //system.debug('Rec Id----'+docType);
            List<OCR_Information__c> lstOCRRecords = new List<OCR_Information__c> ();            
            if(recId != null) {
                lstOCRRecords = [SELECT Id, Aadhaar_Number__c,Acknowledgement_Number__c,Address__c,Constitution_of_Business__c,Date_of_Birth__c,Date_of_Filing__c,
                                 Date_of_Liability__c,Deduction_Under_Section_VI_A__c,Email_ID__c,Expiry_Date__c,Father_s_Name__c,Gender__c,
                                 Gross_Total_Income__c,GSTIN_Number__c,Issue_Date__c,Legal_Guardian__c,Loan_Application__c,Loan_Contact__c,
                                 Mobile_Numer__c,Mother_Name__c,Name_of_Spouse__c,Name_on_Document__c,Nationality__c,PAN_Number__c,Passport_Number__c,
                                 Period_of_validity__c,Place_of_Issue__c,Registration_Type__c,Taxable_income__c,Total_Tax_Interest_Payable__c,
                                 Total_Tax_Paid__c,Trade_Name__c,Voter_ID_Number__c,Ward_Circle_Number__c,Year_of_Assessment__c, RecordType.Name
                                 FROM OCR_Information__c 
                                 WHERE Loan_Contact__c =:recId];
                if(!lstOCRRecords.isEmpty()) {
                    for(OCR_Information__c oi : lstOCRRecords) {
                        if(doc == oi.RecordType.Name) {
                            ocr = oi;
                            break;
                        }    
                    }
                }
                else if(doc == 'Voter ID'){
                    if(subDoc == 'FRONT'){
                        List<string> name = lstString[4].split(':');
                        ocr.Name_on_Document__c = name[1];
                        List<string> fname = lstString[5].split(':');
                        ocr.Father_s_Name__c = fname[1];
                        /*List<string> gender = lstString[6].split('/');
                        List<string> gendersplit = gender[1].split(':');*/
                        ocr.Gender__c = lstString[6];
                        ocr.Voter_ID_Number__c = lstString[3];
                        ocr.RecordtypeID = Schema.SObjectType.OCR_Information__c.getRecordTypeInfosByName().get(Constants.VOTERID).getRecordTypeId();
                        ocr.Document_Checklist__c = recId;
                        ocr.Loan_Contact__c= loanContact;
                        ocr.Loan_Application__c= loanApplication;
                    }
                    else if(subDoc == 'BACK'){
                        ocr.Address__c = lstString[2];
                        ocr.Place_of_Issue__c = lstString[2];
                        ocr.Issue_Date__c = lstString[2];
                        ocr.Voter_ID_Number__c = lstString[2];
                        ocr.RecordtypeID = Schema.SObjectType.OCR_Information__c.getRecordTypeInfosByName().get(Constants.VOTERID).getRecordTypeId();
                        ocr.Document_Checklist__c = recId;
                        ocr.Loan_Contact__c= loanContact;
                        ocr.Loan_Application__c= loanApplication;
                    }
                }
                else if(doc == 'GSTIN'){
                    if(subDoc== 'Registration Certificate'){
                        List<string> gstin = lstString[4].split(':');
                        ocr.GSTIN_Number__c = gstin[1];
                        ocr.Name_on_Document__c = lstString[14];
                        ocr.Trade_Name__c = lstString[15];
                        //ocr.Constitution_of_Business__c = lstString[];
                        ocr.Address__c = lstString[16] + ', ' + lstString[17] + ', ' + lstString[18] + ', ' + lstString[20] + ', ' + lstString[21] + ', ' + lstString[22] + ', ' + lstString[23] + ', ' + lstString[24];
                        ocr.Date_of_Liability__c = lstString[25];
                        ocr.Period_of_validity__c = lstString[28];
                        ocr.Registration_Type__c = lstString[27];
                        ocr.Issue_Date__c = lstString[34];
                        ocr.RecordtypeID = Schema.SObjectType.OCR_Information__c.getRecordTypeInfosByName().get(Constants.GSTIN).getRecordTypeId();
                        ocr.Document_Checklist__c = recId;
                        ocr.Loan_Contact__c= loanContact;
                        ocr.Loan_Application__c= loanApplication;
                    }
                }
                else if(doc == 'Passport'){
                    if(subDoc == 'FRONT'){
                        ocr.Passport_Number__c = lstString[6];
                        ocr.Name_on_Document__c = lstString[10]+' '+lstString[8];
                        ocr.Nationality__c = lstString[12]; 
                        List<string> gendersplit = lstString[14].split(' ');
                        ocr.Gender__c = gendersplit[0];
                        List<string> dob = lstString[14].split(' ');
                        ocr.Date_of_Birth__c = dob[1];
                        ocr.Place_of_Issue__c = lstString[18];
                        ocr.Issue_Date__c = lstString[22];
                        ocr.Expiry_Date__c = lstString[20]; 
                        ocr.RecordtypeID = Schema.SObjectType.OCR_Information__c.getRecordTypeInfosByName().get(Constants.PASSPORT).getRecordTypeId();
                        ocr.Document_Checklist__c = recId;
                        ocr.Loan_Contact__c= loanContact;
                        ocr.Loan_Application__c= loanApplication;
                    }
                    else if(subDoc == 'BACK'){
                        ocr.Legal_Guardian__c = lstString[1];  
                        ocr.Mother_Name__c = lstString[3];
                        ocr.Name_of_Spouse__c = lstString[5]; 
                        ocr.Address__c = lstString[7]+', '+lstString[8]+', '+lstString[9];
                        ocr.RecordtypeID = Schema.SObjectType.OCR_Information__c.getRecordTypeInfosByName().get(Constants.PASSPORT).getRecordTypeId();
                        ocr.Document_Checklist__c = recId;
                        ocr.Loan_Contact__c= loanContact;
                        ocr.Loan_Application__c = loanApplication;
                    }                    
                }
                system.debug('===ocr=='+ocr);
                //upsert ocr;
            }
        }
        
        catch(exception ex) {
            Error_Log__c el = new Error_Log__c();
            el.Error_Message__c = ex.getMessage();
            system.debug('=+=+ ERROR??');
            el.Class_Name__c = 'OCR Class';
            el.Method_Name__c = 'saverec';
            el.Type__c = ex.getTypeName();
            el.Loan_Applications__c = loanApplication;
            el.Line_Number__c = ex.getLineNumber();
            el.Slack_Trace__c = ex.getStackTraceString();
            el.OCR_Information__c = ocr.Id;
            insert el;
        }
        return ocr;
    }
}