public class CAM_Input_Screen_Controller {
    
    @Auraenabled
    //public static Loan_Application__c getLoanAppDetails(Id recId){
    public static getLoansappDetails getLoanAppDetails(Id recId){
    
        getLoansappDetails getloansdetailslist;// = new getLoansappDetails();
        Loan_Application__c loanAppDetails = new Loan_Application__c();
        //toLabel(Loan_Purpose__c),
        loanAppDetails = [SELECT /*Added by Shobhit Saxena on 6 march, 2020*/ Review_by_Data_Checker__c,Name,Customer_Requested_ROI__c,Requested_Amount__c,CAM_Screen_Reviewed__c, Requested_ROI__c, Requested_Loan_Tenure__c, Scheme__c,Scheme__r.Name, Loan_Purpose__c,Transaction_type__c from Loan_Application__c where Id=:recId];
        system.debug('loanAppDetails'+loanAppDetails);
        //for(Loan_Application__c loanobj: loanAppDetails){
            getloansdetailslist = new getLoansappDetails(loanAppDetails);
        //}
        //return loanAppDetails;
        system.debug(getloansdetailslist.loanobject);
        return getloansdetailslist;
    }
    
    @Auraenabled
    public static List<Loan_Contact__c> getCustDetails(Id loanAppId){
        List<Loan_Contact__c> contactDetails = new List<Loan_Contact__c>();
        //Income_Program_Type__c,
        contactDetails = [select Id, Name,toLabel(Income_Program_Type__c),Customer__r.Name,Salaried_Details_Status__c,Financial_Details_Status__c,Banking_Status__c, AIP_Status__c, LIP_Status__c, Obligation_Status__c, Other_Income_Status__c from Loan_Contact__c where Loan_Applications__c =:loanAppId and Income_Considered__c =:true];
        return contactDetails;
    }
    public class getLoansappDetails{
      @AuraEnabled public Loan_Application__c loanobject = new Loan_Application__c();
      @AuraEnabled public String propertytype = '';
      @AuraEnabled public Decimal technicalinput1;
      @AuraEnabled public Decimal technicalinput2;
      @AuraEnabled public String Loan_Purpose='';
      @AuraEnabled public String Transaction_type='';

        getLoansappDetails(Loan_Application__c loanobj){
            loanobject = loanobj;
            Schema.DescribeFieldResult fieldResult = Loan_Application__c.Loan_Purpose__c.getDescribe();
            List<Schema.PicklistEntry> values = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry v : values) {
                //System.debug('Picklist Value Label:' + v.getLabel());
                //System.debug('Picklist API Name:' + v.getValue());
                if(v.getValue() == loanobj.Loan_Purpose__c){
                            Loan_Purpose = v.getLabel();
                  System.debug('Picklist Value Label:' + v.getLabel());
                System.debug('Picklist API Name:' + v.getValue());
                System.debug('Loan_Purpose:' + Loan_Purpose);
                }
            }
            Schema.DescribeFieldResult fieldResult1 = Loan_Application__c.Transaction_type__c.getDescribe();
            List<Schema.PicklistEntry> values1 = fieldResult1.getPicklistValues();
            for( Schema.PicklistEntry v : values1) {
                //System.debug('Picklist Value Label:' + v.getLabel());
                //System.debug('Picklist API Name:' + v.getValue());
                if(v.getValue() == loanobj.Transaction_type__c){
                    Transaction_type = v.getLabel();
                System.debug('Picklist Value Label:' + v.getLabel());
                System.debug('Picklist API Name:' + v.getValue());
                System.debug('Transaction_type:' + Transaction_type);
                
                }
                }
            
            //Loan_Purpose = loanobj.Loan_Purpose__c;
            //Transaction_type = loanobj.Transaction_type__c;
            
            list<Property__c> plist = new list<Property__c>([select id,toLabel(Type_of_Property__c) from Property__c where Loan_Application__c =:loanobj.Id limit 1]);
            system.debug('plist'+plist);
            if(plist!=null && plist.size()>0)
                propertytype = plist[0].Type_of_Property__c;
            id recordtypeids = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('Technical').getRecordTypeId();
            list<Third_Party_Verification__c> tlist = new list<Third_Party_Verification__c>([select id,Final_Property_Valuation__c  from Third_Party_Verification__c where Loan_Application__c =: loanobj.Id and RecordTypeid =: recordtypeids  order by Final_Property_Valuation__c desc limit 2]);
            system.debug('tlist'+tlist);
            if(tlist!=null && tlist.size()>0){
                technicalinput1 = tlist[0].Final_Property_Valuation__c;
                if(tlist.size()>1)
                    technicalinput2 = tlist[1].Final_Property_Valuation__c;
            }
        }
    }
        @Auraenabled
    public static boolean GetIseditable(Id recId){
        Loan_Application__c loanAppDetails = new Loan_Application__c();
        loanAppDetails = [SELECT Name,StageName__c from Loan_Application__c where Id=:recId];
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        system.debug('ProfileName'+profileName);
        if(Label.Reviewed_Stage.contains(loanAppDetails.StageName__c) && Label.Reviewed_Profile.contains(profileName))
        { 
            return true;//edit mode
        } else {
            return false;//read only mode}   
        }
    }
    @Auraenabled
    public static boolean GetIseditableNw(Id recId){
        Loan_Application__c loanAppDetails = new Loan_Application__c();
        loanAppDetails = [SELECT Name,StageName__c from Loan_Application__c where Id=:recId];
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        system.debug('ProfileName'+profileName);
        if(Label.Data_Entry.contains(profileName))
        { 
            return true;//edit mode
        } else {
            return false;//read only mode}   
        }
    }
        @Auraenabled
    public static void saveloanstatus(Loan_Application__c recId){
       String Name = UserInfo.getName();
        if(recId.CAM_Screen_Reviewed__c==True){
            recId.Reviewed_by_Credit_User__c=Name;
        }
        if(recId.Review_by_Data_Checker__c==True){
             recId.Reviewed_by_Data_Checker__c=Name;
        }
        system.debug('in saveloanstatus '+recId);
        update recId;
    }
    
}