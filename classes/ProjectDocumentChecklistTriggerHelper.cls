public class ProjectDocumentChecklistTriggerHelper {
    public static void checkAttachmentOnUpload(List<Project_Document_Checklist__c> lstDCUpload){
        Set<Id> setDCId = new Set<Id>();
        List<ContentDocumentLink> lstAttachments = new List<ContentDocumentLink>();
        Integer attachmentCount = 0;
        
        for(Project_Document_Checklist__c dc: lstDCUpload){
            setDCId.add(dc.Id);
        }
        
        if(setDCId.size() > 0){
            lstAttachments = [SELECT Id,LinkedEntityId from ContentDocumentLink WHERE LinkedEntityId IN: setDCId];
        }
        
        for(Project_Document_Checklist__c dc: lstDCUpload){
            for(ContentDocumentLink attach: lstAttachments){
                if(dc.Id == attach.LinkedEntityId){
                    attachmentCount++;
                }
            }
            
            if(dc.Status__c == Constants.strDocStatusUploaded && attachmentCount == 0){
                dc.addError('Please upload the document before marking the status to Uploaded.');
            }
            
            if((dc.Status__c == Constants.strDocStatusPending) && attachmentCount > 0){
                dc.addError('You cannot mark the status as Pending or Received if document has been uploaded.');
            }
        }
    }
}