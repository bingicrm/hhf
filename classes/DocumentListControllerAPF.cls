public class DocumentListControllerAPF {
    @AuraEnabled
    public static Project__c fetchSubStage(Id strRecordId) {
        String subStage;
        List<Project__c> lstProject;
        if(String.isNotBlank(strRecordId)) {
            lstProject = [SELECT  ID, Sub_Stage__c FROM Project__c WHERE ID = :strRecordId LIMIT 1];
            if(!lstProject.isEmpty()) {
                subStage = lstProject[0].Sub_Stage__c;
            }
        }
        System.debug('Debug Log for subStage'+lstProject[0]);
        return lstProject[0];
    }
    
    // method for fetch account records list  
    @AuraEnabled
    public static List <DocumentListWrapper> fetchDocuments(Id strRecordId) {
        //Map of Contact ID to Contact Name
        Map<String, List<Project_Document_Checklist__c>> mapContactNametoDocChecklist = new Map<String, List<Project_Document_Checklist__c>>();
        List <Project_Document_Checklist__c> returnList = new List <Project_Document_Checklist__c> ();
        List <Project_Document_Checklist__c> lstOfDoc;
        List<DocumentListWrapper> lstDocumentWrapper = new List<DocumentListWrapper>();
        List<Project__c> lstProject = [SELECT  ID, Sub_Stage__c FROM Project__c WHERE ID = :strRecordId LIMIT 1];
        String subStage;
        String fieldsToQuery;
        
        if(!lstProject.isEmpty()) {
            subStage = lstProject[0].Sub_Stage__c;
        }
        
        System.debug('Debug Log for subStage'+subStage);
        lstOfDoc = [select id,Document_Uploaded__c,Edited__c,Project_Builder__c, Locked__c, File_Check_Completed__c, Scan_Check_Completed__c, Project_Builder__r.Builder_Name__r.Name, Project__c, Project__r.Stage__c, Document_Type__r.name,Document_Master__c,Document_Master__r.name,Document_Master__r.Document_Title__c,Status__c,Notes_Remarks__c,Received_Stage__c, Mandatory__c from Project_Document_Checklist__c where Project__c =:strRecordId ORDER BY Document_Type__r.name ASC];
        
        if(!lstOfDoc.isEmpty()) {
            for (Project_Document_Checklist__c doc: lstOfDoc) {
                returnList.add(doc);
                System.debug('Debug Log for doc.Document_Type__r.name'+doc.Document_Type__r.name);
                System.debug('Debug Log for Document_Master__c'+doc.Document_Master__r.Name);
                System.debug('Debug Log for Document_Master__c'+doc.Project_Builder__r.Builder_Name__r.Name);
                
                if(String.isNotBlank(doc.Project_Builder__r.Builder_Name__r.Name) && !doc.Project_Builder__r.Builder_Name__r.Name.trim().contains('()')) {
                    if(!mapContactNametoDocChecklist.containsKey(doc.Project_Builder__r.Builder_Name__r.Name)) {
                        mapContactNametoDocChecklist.put(doc.Project_Builder__r.Builder_Name__r.Name,new List<Project_Document_Checklist__c>());
                    }
                    mapContactNametoDocChecklist.get(doc.Project_Builder__r.Builder_Name__r.Name).add(doc);
                }
                else {
                    if(!mapContactNametoDocChecklist.containsKey('Application Related Documents')) {
                        mapContactNametoDocChecklist.put('Application Related Documents',new List<Project_Document_Checklist__c>());
                    }
                    mapContactNametoDocChecklist.get('Application Related Documents').add(doc);
                }
            }
            for(String strBuilderName : mapContactNametoDocChecklist.keySet()) {
                lstDocumentWrapper.add(new DocumentListWrapper(strBuilderName, mapContactNametoDocChecklist.get(strBuilderName),''));
            }
        } 
        System.debug('Debug Log for lstDocumentWrapper'+lstDocumentWrapper);   
        //For Testing Purpose 
        for(DocumentListWrapper objDocumentListWrapper : lstDocumentWrapper) {
            System.debug('Key:'+objDocumentListWrapper.strBuilderName);
            System.debug('Value:'+objDocumentListWrapper.lstDocumentCheckList);
        }
        //return returnList;
        return lstDocumentWrapper;
    }
    
    // method for fetch picklist values dynamic  
    @AuraEnabled
    public static List < String > getselectOptions(sObject objObject, string fld, string subStage) {
        system.debug('objObject --->' + objObject);
        system.debug('fld --->' + fld);
        system.debug('subStage --->' + subStage);
        
        List < String > allOpts = new list < String > ();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objObject.getSObjectType();
        
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        
        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values =
            fieldMap.get(fld).getDescribe().getPickListValues();
        
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        if(fld == 'Status__c' && subStage == 'Project Data Capture') {
            allOpts.clear();
            allOpts.add('Pending');
            allOpts.add('Uploaded');
            allOpts.add('Received');
            
        }
        else if(fld == 'Status__c' && subStage == 'Scan Maker') {
            allOpts.clear();
            allOpts.add('Uploaded');
        }
        else if(fld == 'Status__c' && subStage == 'APF Credit Review') {
            allOpts.clear();
            allOpts.add('Pending');
            allOpts.add('Uploaded');
            allOpts.add('Received');
            allOpts.add('Pre Disbursal');
            allOpts.add('PDD');
            allOpts.add('Lawyer OTC');
            allOpts.add('OTC Approved');
            allOpts.add('Waived Off');
        }
        system.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        return allOpts;
    }
    
    @AuraEnabled
    public static String validateChkList(String paramJSONList) {
        System.debug('Debug Log for param list'+paramJSONList);
        String statusMsg;
        Set<Id> setDocumentCheckList = new Set<Id>();
        Map<Id,List<ContentDocumentLink>> mapDocChkLsttoDocAttached = new Map<Id,List<ContentDocumentLink>>();
        List<Project_Document_Checklist__c> lstDocchkLst = new List<Project_Document_Checklist__c>();
        List<DocumentListWrapper> lstSerializedWrapper = (List<DocumentListWrapper>)JSON.deserialize(paramJSONList,List<DocumentListWrapper>.class);
        System.debug('Debug Log for deserialized List'+lstSerializedWrapper.size());
        System.debug('Debug Log for deserialized List'+lstSerializedWrapper);
        if(!lstSerializedWrapper.isEmpty()) {
            for(DocumentListWrapper objDocumentListWrapper : lstSerializedWrapper) {
                lstDocchkLst.addAll(objDocumentListWrapper.lstDocumentCheckList);
                system.debug(objDocumentListWrapper.lstDocumentCheckList);
            }
        }
        System.debug('Debug Log for lstDocchkLst'+lstDocchkLst);
        if(!lstDocchkLst.isEmpty()) {
            for(Project_Document_Checklist__c objDC : lstDocchkLst) {
                setDocumentCheckList.add(objDC.Id);
            }
            System.debug('Debug Log for setDocumentCheckList'+setDocumentCheckList);
            List<ContentDocumentLink> links;
            links = [SELECT id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN: setDocumentCheckList];
            System.debug('Debug Log for links'+links.size());
            System.debug('Debug Log for links'+links);
            if(!links.isEmpty()) {
                for(ContentDocumentLink cdl : links) {
                    if(!mapDocChkLsttoDocAttached.containsKey(cdl.LinkedEntityId)) {
                        mapDocChkLsttoDocAttached.put(cdl.LinkedEntityId,new List<ContentDocumentLink>());
                    }
                    mapDocChkLsttoDocAttached.get(cdl.LinkedEntityId).add(cdl);
                }
            }
            else {
                for(Project_Document_Checklist__c objDC : lstDocchkLst) {
                    mapDocChkLsttoDocAttached.put(objDC.Id,new List<ContentDocumentLink>());
                }
            }
            System.debug('Debug Log for mapDocChkLsttoDocAttached'+mapDocChkLsttoDocAttached.size());
            System.debug('Debug Log for mapDocChkLsttoDocAttached'+mapDocChkLsttoDocAttached);
            if(!mapDocChkLsttoDocAttached.isEmpty()) {
                for(Project_Document_Checklist__c objDocument_Checklist : lstDocchkLst) {
                    System.debug('Inside for loop');
                    if(!mapDocChkLsttoDocAttached.containsKey(objDocument_Checklist.Id) && objDocument_Checklist.Status__c == 'Uploaded') {
                        System.debug('Not found in Map'+objDocument_Checklist.Document_Master__r.Name);
                        System.debug('Error!!');
                        statusMsg = 'Error!!';
                        break;
                    }
                    else if(mapDocChkLsttoDocAttached.containsKey(objDocument_Checklist.Id) && objDocument_Checklist.Status__c == 'Uploaded' && mapDocChkLsttoDocAttached.get(objDocument_Checklist.Id).isEmpty()) {
                        statusMsg = 'Error!!';
                        //System.debug('Found in Map but No document exists.'+objDocument_Checklist.Document_Master__r.Name);
                        //System.debug('Error!!');
                        break;
                    }
                }
            }
        }
        System.debug('Debug Log for statusMsg'+statusMsg);
        if(String.isBlank(statusMsg)) {
            statusMsg = 'Successful';
        }
        //System.debug('Debug Log for statusMsg1'+statusMsg);
        return statusMsg;
    }
    
    @AuraEnabled
    public static List<DocumentListWrapper> saveLoan(String paramJSONList) {
        System.debug('Debug Log for param list'+paramJSONList);
        List<Project_Document_Checklist__c> lstDocchkLst = new List<Project_Document_Checklist__c>();
        List<Project_Document_Checklist__c> lockedDocLst = new List<Project_Document_Checklist__c>();
        List<DocumentListWrapper> lstWrapper = new List<DocumentListWrapper>();
        lstWrapper.add(new DocumentListWrapper('',null,''));
        List<DocumentListWrapper> lstSerializedWrapper = (List<DocumentListWrapper>)JSON.deserialize(paramJSONList,List<DocumentListWrapper>.class);
        System.debug('Debug Log for deserialized List'+lstSerializedWrapper.size());
        System.debug('Debug Log for deserialized List'+lstSerializedWrapper);
        if(!lstSerializedWrapper.isEmpty()) {
            for(DocumentListWrapper objDocumentListWrapper : lstSerializedWrapper) {
                lstDocchkLst.addAll(objDocumentListWrapper.lstDocumentCheckList);
                system.debug(objDocumentListWrapper.lstDocumentCheckList);
            }
        }
        if(!lstDocchkLst.isEmpty()){
            for(Project_Document_Checklist__c docc : lstDocchkLst ){
                if( docc.Edited__c ){
                    lockedDocLst.add(docc);
                }
            }
        }
        System.debug('Debug Log for lstDocchkLst'+lstDocchkLst);
        System.debug('Debug Log for lockedDocLst'+lockedDocLst);
        if(!lockedDocLst.isEmpty()) {
            Database.Saveresult[] updateList = Database.update(lockedDocLst,false);
            for (Database.SaveResult sr : updateList) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully updated Document Checklist. Record ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Fields that affected this error: ' + err.getFields());
                        lstWrapper[0].errorMsg = err.getMessage();
                    }
                    System.debug('Debug Log for returning List'+lstWrapper);
                    System.debug('Debug Log for returning List 0th index'+lstWrapper[0]);
                    
                    return lstWrapper;
                }
            }
        }
        return lstSerializedWrapper;
        //return null;
    }
    
    @AuraEnabled
    public static List<ID> queryAttachments (Id propertyId) {
        List<ContentDocumentLink> links;
        List<ContentVersion> lstContentVersion;
        Set<Id> setContentVersionId = new Set<Id>();
        String objectAPIName = '';
        String keyPrefix = '';
        System.debug('Debug Log for propertyId'+propertyId);
        objectAPIName = propertyId.getSObjectType().getDescribe().getName();
        System.debug('Debug Log for objectAPIName:'+objectAPIName);
        if(String.isNotBlank(objectAPIName)) {
            if(objectAPIName == 'Project_Document_Checklist__c') {
                links = [select id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where LinkedEntityId=:propertyId];
                System.debug('Debug Log for links'+links.size());
                System.debug('Debug Log for links'+links);
                
                if (links.isEmpty()) {
                    return null;
                }
                
                Set<Id> contentIds = new Set<Id>();
                
                for (ContentDocumentLink link :links) {
                    contentIds.add(link.ContentDocumentId);
                }
                System.debug('Debug Log for contentIds'+contentIds);
                /*
                lstContentVersion = [SELECT Id, Title FROM ContentVersion WHERE ContentDocumentId IN :contentIds AND IsLatest=true];
                System.debug('Debug Log for lstContentVersion'+lstContentVersion);
                
                if(!lstContentVersion.isEmpty()) {
                for(ContentVersion objContentVersion : lstContentVersion) {
                setContentVersionId.add(objContentVersion.Id);
                }
                }
                System.debug('Debug Log for setContentVersionId'+setContentVersionId);
                */
                List<Id> lstsettoList = new List<Id>(contentIds);            
                return lstsettoList;
            }
            
            else if(objectAPIName == 'Project__c') {
                List<Project_Document_Checklist__c> lstDocumentList = [SELECT Id FROM Project_Document_Checklist__c WHERE Project__c =: propertyId];
                Set<Id> setDocumentListIds = new Set<Id>();
                System.debug('Debug Log for lstDocumentList'+lstDocumentList.size());
                if(!lstDocumentList.isEmpty()) {
                    for(Project_Document_Checklist__c objDC : lstDocumentList) {
                        setDocumentListIds.add(objDC.Id);
                    }
                }
                System.debug('Debug Log for setDocumentListIds'+setDocumentListIds.size());
                if(!setDocumentListIds.isEmpty()) {
                    links = [select id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where LinkedEntityId IN:setDocumentListIds];
                    
                    if (links.isEmpty()) {
                        return null;
                    }
                    
                    Set<Id> contentIds = new Set<Id>();
                    
                    for (ContentDocumentLink link :links) {
                        contentIds.add(link.ContentDocumentId);
                    }
                    System.debug('Debug Log for contentIds'+contentIds);
                    List<Id> lstsettoList1 = new List<Id>(contentIds);            
                    return lstsettoList1;
                    
                    //return [SELECT Id, Title FROM ContentVersion WHERE ContentDocumentId IN :contentIds AND IsLatest=true];
                }
            }
            
        }
        return new List<Id>();
    }
    
    public class DocumentListWrapper {
        @AuraEnabled public String strBuilderName;
        @AuraEnabled public List<Project_Document_Checklist__c> lstDocumentCheckList;
        @AuraEnabled public String errorMsg;
        
        public DocumentListWrapper(String paramBuilderName, List<Project_Document_Checklist__c> paramListDocChkLst, String errorMsg) {
            this.strBuilderName = paramBuilderName;
            this.lstDocumentCheckList = paramListDocChkLst;
            this.errorMsg = errorMsg;
        }
    }

}