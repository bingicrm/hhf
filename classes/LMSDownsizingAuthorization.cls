public class LMSDownsizingAuthorization {
    //
    @AuraEnabled
    public static Loan_Downsizing__c loadLoanDownsizingInfo(String loanApplicationID)
    {
        List<Loan_Downsizing__c> loanDownsizingList = [Select Name, Authorization_remark__c, Creation_Remark__c, Downsize_Type__c,Downsizing_Amount__c, Status__c, Case_is_not_delinquent__c,
                                                        NACH_is_registered__c, remarks__c, Loan_Application__r.Pending_Amount_for_Disbursement__c, Loan_Application__r.Approved_Loan_Amount__c
                                                       From Loan_Downsizing__c Where Loan_Application__c = :loanApplicationID AND Status__c='Approved' Order By SystemModStamp DESC Limit 1];
        if(loanDownsizingList.size() > 0 )
        {
            return loanDownsizingList[0];
        }
        return null;
        //return '';
    }
    
    
    @AuraEnabled
    public static String sendForLmsDownsizingAuthorization(String loanApplicationID, String authorizationRemarks)
    {
        ParsedResponseWrapper parsedResponseWrapper;
        Decimal pendingAmount;
        Decimal newSanctionedAmount;
        DowsizingErrorResponseWrapper downsizingErrorResponseWrapper;
        List<Loan_Downsizing__c> loanDownsizingList;
        String updateMessage;
        
        system.debug('loanApplicationID++ '+loanApplicationID);
        if(String.isNotBlank(loanApplicationID))
        {
            List<Loan_Downsizing__c> loanDownsizingListforID = [Select Downsizing_Amount__c, Remarks__c, Creation_Remark__c,createdby.name, 
                                                                Loan_Application__r.Loan_Application_Number__c, Loan_Application__r.Loan_Number__c,
                                                                Loan_Application__r.Business_Date_Modified__c, Case_is_not_delinquent__c,NACH_is_registered__c, Authorization_remark__c   
                                                                From Loan_Downsizing__c 
                                                                Where Loan_Application__c = :loanApplicationID AND Status__c='Approved' Order By SystemModStamp ASC Limit 1];
            
            system.debug('loanDownsizingListforID++ '+loanDownsizingListforID);
            if(loanDownsizingListforID.size() > 0)
            {
                loanDownsizingList = [Select Downsizing_Amount__c,Downsize_Type__c,Remarks__c, Loan_Application__r.Loan_Application_Number__c,Loan_Application__r.Loan_Number__c, 
                                      Loan_Application__r.Business_Date_Modified__c, Loan_Application__r.Approved_Loan_Amount__c,Loan_Application__r.Date_of_Downsizing__c, 
                                      Loan_Application__r.Pending_Amount_for_Disbursement__c,Creation_Remark__c,createdby.name,Case_is_not_delinquent__c,
                                      NACH_is_registered__c, Loan_Application__r.LTV_amount__c, Authorization_remark__c, Loan_Application__r.Final_Property_Valuation__c, 
                                      Loan_Application__r.Approved_cross_sell_amount__c, Loan_Application__r.StageName__c, Loan_Application__r.Insurance_Loan_Created__c, 
                                      Loan_Application__r.Sub_Stage__c From Loan_Downsizing__c Where ID = :loanDownsizingListforID[0].ID And Status__c='Approved' For Update];   
            }
            
            if( loanDownsizingList!=null && loanDownsizingList.size() > 0)
            {
                DownsizingRequest dRequest = generateHttpRquest(loanDownsizingList[0], authorizationRemarks);
                system.debug('dRequest++ '+dRequest);                
                if(dRequest.message=='Success')
                {
                    
                    Http http = new Http();
                    HttpRequest request =  dRequest.request;
                    system.debug('request+++ '+request);
                    system.debug('headers '+request.getBody());
                    system.debug('headers '+request.getHeader('Username'));
                    system.debug('headers '+request.getHeader('Password')); 
                    system.debug('headers '+request.getHeader('Content-Type'));                                                                                              
                    
                    
                    HttpResponse response = http.send(request);
                    system.debug('response++ '+response);
                    // logging request response
                    LogUtility.createIntegrationLogs(request.getBody(), response, request.getEndpoint());
                    //logging request response
                    //Process Response
                    SuccessResponseWrapper responseWrapper = new SuccessResponseWrapper();
                    
                    if(response.getStatusCode()==200 && String.isNotBlank(response.getBody()))// If http request success.
                    {
                        system.debug('responseWrapper+++ '+responseWrapper);
                        
                        try
                        {
                            responseWrapper =  (SuccessResponseWrapper)JSON.deserialize(response.getBody(), SuccessResponseWrapper.Class);
                            system.debug('responseWrapper++ after deserialize '+responseWrapper);
                        }
                        catch(Exception ex)
                        {
                            return 'Contact Administrator - Exception reading response from Server';  
                        }
                        
                        if(responseWrapper!=null && String.isNotBlank(responseWrapper.status) && (responseWrapper.status=='S' || responseWrapper.status=='Success') &&
                           String.isBlank(responseWrapper.errorMsg) && String.isNotBlank(responseWrapper.agreementId) &&
                           responseWrapper.agreementId==loanDownsizingList[0].Loan_Application__r.Loan_Number__c)
                        {
                            //parsedResponseWrapper =  new new ParsedResponseWrapper(Decimal.valueOf(responseWrapper.pendingAmt), Decimal.valueOf(responseWrapper.newSanctionedAmt),responseWrapper.errorMsg);
                            system.debug('Inside Success');
                            pendingAmount = Decimal.valueOf(responseWrapper.pendingAmt);
                            newSanctionedAmount = Decimal.valueOf(responseWrapper.newSanctionedAmt);
                            
                            //Update the Loan Downsizing and Loan Application..
                            updateMessage = updateLoandownsizingAndLoanApplication(loanDownsizingList[0], pendingAmount, newSanctionedAmount,authorizationRemarks);
                            if(updateMessage=='Success')
                            {   
                                return 'Loan Downsizing Successfull..';
                            }
                            else
                            {
                                return updateMessage;
                            }
                        }
                        else if(responseWrapper!=null && String.isNotBlank(responseWrapper.status) && responseWrapper.status!='S' && responseWrapper.status!='Success')
                        {
                            //parsedResponseWrapper =  new new ParsedResponseWrapper(Decimal.valueOf(responseWrapper.pendingAmt), Decimal.valueOf(responseWrapper.newSanctionedAmt),responseWrapper.errorMsg);
                            system.debug('Inside Failure');
                            return String.isNotBlank(responseWrapper.errorMsg) ? responseWrapper.errorMsg : 'Error - Contact Administrator';
                        }
                        return 'Unknown Error - Contact System Administrator';
                    }
                    else // Http resquest failure.
                    {
                        system.debug('response.getBody++ '+response.getBody());
                        downsizingErrorResponseWrapper = new DowsizingErrorResponseWrapper();
                        try
                        {
                            downsizingErrorResponseWrapper =  (DowsizingErrorResponseWrapper)JSON.deserializeStrict(response.getBody(), DowsizingErrorResponseWrapper.class);
                            if(downsizingErrorResponseWrapper.status=='failure' && (downsizingErrorResponseWrapper.Error.errorCode=='400' || downsizingErrorResponseWrapper.Error.errorCode=='401' ||
                                                                                    downsizingErrorResponseWrapper.Error.errorCode=='500'))
                            {
                                return (downsizingErrorResponseWrapper.Error.errorCode + ' ' +downsizingErrorResponseWrapper.Error.errorType + ' ' + downsizingErrorResponseWrapper.Error.errorMessage+'.');
                            }
                            return 'unknown Error';
                        }
                        catch(Exception ex)
                        {
                            //add Error Logging
                            return 'Error';
                        }
                    }
                    
                    //Process Response end
                }
                else
                {
                    system.debug('dRequest.message');
                }
            }
            else
            {
                system.debug('Invalid Loan Application ID');
            }
        }
        return 'Invalid Loan Application';
    }
    
    //Method to update Loan downsizing and Loan Application
	@TestVisible //Deloitte : Anushree Pawar 01/12 - added to access method in test class						 
    private static String updateLoandownsizingAndLoanApplication(Loan_Downsizing__c loanDownsizing, Decimal pendingAmount, Decimal newSanctionedAmount, String authorizationRemarks)
    {
        LMS_Date_Sync__c dt = [Select Id, Current_Date__c from LMS_Date_Sync__c ORDER BY CreatedDate DESC limit 1];
        if(loanDownsizing!=null && newSanctionedAmount!=null)
        {
            if(loanDownsizing.Downsize_Type__c=='Full Downsize')
            {
                loanDownsizing.Status__c='Authorized';
                loanDownsizing.Loan_Application__r.Pending_Amount_for_Disbursement__c=0;
                loanDownsizing.Loan_Application__r.Approved_Loan_Amount__c = newSanctionedAmount;
                loanDownsizing.Loan_Application__r.Date_of_Downsizing__c = dt.Current_Date__c;
                loanDownsizing.Authorization_remark__c=authorizationRemarks;
            }
            
            else if(loanDownsizing.Downsize_Type__c=='Partial Downsize')
            {
                loanDownsizing.Status__c='Authorized';
                loanDownsizing.Loan_Application__r.Pending_Amount_for_Disbursement__c= loanDownsizing.Loan_Application__r.Pending_Amount_for_Disbursement__c - loanDownsizing.Downsizing_Amount__c;
                loanDownsizing.Loan_Application__r.Approved_Loan_Amount__c = newSanctionedAmount;
                loanDownsizing.Loan_Application__r.Date_of_Downsizing__c = dt.Current_Date__c;
                loanDownsizing.Authorization_remark__c=authorizationRemarks;
                
            }  
            
            Database.SaveResult sr = Database.update(loanDownsizing);
            if(sr.isSuccess())
            {
                try
                {
                    update loanDownsizing.Loan_Application__r;
                    
                    return 'Success';
                }
                catch(Exception ex)
                {
                    //Log DMl Exception..
                    return 'Issue with Updating Loan Application';
                }
            }
            else 
            {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug(' fields that affected this error: ' + err.getFields());
                }
                return 'Issue updating Loan Downsizing !!';
            }
        }
        return 'Unexpected Issue Occurred';
    }
    
    //Method for Creating request header.
	@TestVisible //Deloitte : Anushree Pawar 01/12 - added to access method in test class					  
    private static DownsizingRequest generateHttpRquest(Loan_Downsizing__c loanApplication, String authorizationRemarks)
    {
        List<Rest_Service__mdt> restServiceList = [SELECT Client_Password__c, Client_Username__c, Request_Method__c, Time_Out_Period__c, Service_EndPoint__c, 
                                                   Content_Type__c, Request_Body__c FROM Rest_Service__mdt WHERE DeveloperName='DownsizingAuthorization'];
        
        if(loanApplication!=null && restServiceList.size() > 0)
        {
            String requestbody = createRequestBody(loanApplication, authorizationRemarks);
            system.debug('requestbody++ '+requestbody);
            if(String.IsNotBlank(requestbody))
            {
                
                HttpRequest request = new HttpRequest();
                
                //Setting Header values 
                Blob headerValue = Blob.valueOf(restServiceList[0].Client_Username__c + ':' + restServiceList[0].Client_Password__c);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                request.setHeader('Content-Type', restServiceList[0].Content_Type__c);
                request.setHeader('Username' , restServiceList[0].Client_Username__c);
                request.setHeader('Password', restServiceList[0].Client_Password__c);
                request.setHeader('accept','application/json');
                request.setHeader('Authorization', authorizationHeader);
                // Setting Header values
                
                request.setEndpoint(restServiceList[0].Service_EndPoint__c);
                request.setMethod(restServiceList[0].Request_Method__c);
                
                request.setBody(requestbody);
                
                
                return new DownsizingRequest(request,'Success');
            }
            else
            {
                system.debug('Issue with Request Body JSON');
                return new DownsizingRequest(NULL, 'Issue with Request Body');
            }
        }
        else
        {
            system.debug('Issue with Custom Metadata');
            return new DownsizingRequest(NULL, 'Issue with Custom Metadata');
        }
    }
    
    //Method for creating request body.
	@TestVisible //Deloitte : Anushree Pawar 01/12 - added to access method in test class					  
    private static String createRequestBody(Loan_Downsizing__c loanApplication, String authorizationRemarks)
    {
        RequestWrapper request = new RequestWrapper();
        // if(loanApplication!=null)
        //{
        // Move the Query to the 'sendForLmsDownsizingAuthorization' method and pass LoanDownsizing, and instead Quey..
        //List<Loan_Application__c> loanApplicationList = [Select Loan_Application_Number__c From Loan_Application__c Where ID = :loanApplicationID];
        // List<Loan_Downsizing__c> loanDownsizingList = [Select Downsizing_Amount__c,Remarks__c, Loan_Application__r.Loan_Application_Number__c, Loan_Application__r.Business_Date_Modified__c  
        //                                              From Loan_Downsizing__c 
        //                                            Where Loan_Application__c = :loanApplicationID Order By SystemModStamp]; 
        
        system.debug('request parameters');
        system.debug('loanApplication.Loan_Application__r.Loan_Application_Number__c++ '+loanApplication.Loan_Application__r.Loan_Number__c);
        system.debug('loanApplication.Downsizing_Amount__c++ '+loanApplication.Downsizing_Amount__c);
        //system.debug('loanApplication.Remarks__c++ '+loanApplication.Authorization_remark__c);
        
        if(loanApplication!=null && loanApplication.Downsizing_Amount__c!=null && loanApplication.Downsizing_Amount__c > 0 && 
           String.isNotBlank(loanApplication.Loan_Application__r.Loan_Number__c) && String.isNotBlank(authorizationRemarks) &&
           loanApplication.Loan_Application__r.Business_Date_Modified__c !=null && loanApplication.Case_is_not_delinquent__c && loanApplication.NACH_is_registered__c
          && loanApplication.Loan_Application__r.LTV_amount__c!=null)
        {
            Time myTime = Time.newInstance(0, 0, 0, 0);
            String dt = DateTime.newInstance(loanApplication.Loan_Application__r.Business_Date_Modified__c,myTime).format('dd-MMM-yyyy');
            Decimal ltv = 0.00;
            
            if(loanApplication.Loan_Application__r.Final_Property_Valuation__c == 0){
                ltv = 0.00;
            }
            else{
                if(loanApplication.Loan_Application__r.Approved_cross_sell_amount__c != null && (loanApplication.Loan_Application__r.StageName__c == 'Credit Decisioning' || loanApplication.Loan_Application__r.StageName__c == 'Customer Acceptance'
                || loanApplication.Loan_Application__r.Sub_Stage__c == 'Docket Checker' || loanApplication.Loan_Application__r.Sub_Stage__c == 'Hand Sight') && loanApplication.Loan_Application__r.Insurance_Loan_Created__c != TRUE){
                    ltv = (loanApplication.Loan_Application__r.Approved_Loan_Amount__c + loanApplication.Loan_Application__r.Approved_cross_sell_amount__c - loanApplication.Downsizing_Amount__c)/ loanApplication.Loan_Application__r.Final_Property_Valuation__c;
                }
                else{
                    ltv = (loanApplication.Loan_Application__r.Approved_Loan_Amount__c - loanApplication.Downsizing_Amount__c)/ loanApplication.Loan_Application__r.Final_Property_Valuation__c;
                }
            }
            
            request.requestid = System.now().format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
            request.source = Label.Source_Name; // Custom Label created by KK for source name
            request.agreementId =loanApplication.Loan_Application__r.Loan_Number__c;
            request.downSizingAmount = loanApplication.Downsizing_Amount__c;
            request.downSizingDate = dt;
            request.makerRemark =  loanApplication.Creation_Remark__c;
            request.authorRemark =  authorizationRemarks;
            request.maker = loanApplication.createdby.name; // Createdby
            request.author=UserInfo.getName(); // logged in user
            request.grossLtv = ltv.setScale(4)*100;
            request.netLtv = ltv.setScale(4)*100;
            
            system.debug('JSON.serializePretty(request)++'+JSON.serializePretty(request));
            return JSON.serializePretty(request);
        }
        //}
        return '';
    }
    
    // Downsizing Request Class.
    public class RequestWrapper
    {
        public String requestid;
        public String source;
        public String agreementId;
        public Decimal downSizingAmount;
        public String downSizingDate;
        public String makerRemark;
        public String authorRemark;
        public String maker;
        public String author;
        public Decimal grossLtv;
        public Decimal netLtv;
    }
    
    //Resquest and message
    public class DownsizingRequest
    {
        public HttpRequest request;
        public String message;
        
        public DownsizingRequest(HttpRequest req, String msg)
        {
            this.request = req;
            this.message = msg;
        }
    } 
    
    //Response Wrapper
    public class SuccessResponseWrapper
    {
        //public Object requestId;
        public String requestId;
        
        public String agreementId;
        public String status;
        public String errorMsg;
        //public Object newSanctionedAmt;
        public String newSanctionedAmt;
        //public Object pendingAmt;
        public String pendingAmt;
        
    }
    
    //ParsedResponseWrappe
    public class ParsedResponseWrapper
    {
        public Decimal pendingAmt;
        public Decimal newSanctionedAmt;
        
        //public String message;
        public ParsedResponseWrapper( Decimal pendingAmt, Decimal newSanctionedAmt)
        {
            this.pendingAmt = pendingAmt;
            this.newSanctionedAmt = newSanctionedAmt;
        }
    }
}