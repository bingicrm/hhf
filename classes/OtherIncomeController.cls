public with sharing class OtherIncomeController {

    @AuraEnabled//Annotation to use method in lightning component
    public static List<Loan_Contact__c> getOtherIncome1(Id cusDtId) {  //Fetch data
        List<Loan_Contact__c> custDetailLst = new List<Loan_Contact__c>();
        Loan_Contact__c custDetial =  new Loan_Contact__c();
                   
        List<Loan_Contact__c> cd = [SELECT Id,isOtherIncomeVerified__c,Rental_Income_Previous_Year__c,Rental_Income_Current_Year__c,Agricultural_Income_Previous_Year__c,Agricultural_Income_Current_Year__c,Interest_Divident_Income_Current_Year__c,
                                    Interest_Divident_Income_Previous_Year__c,Total_Other_Income_Current__c,Total_Other_Income_Previous__c  FROM Loan_Contact__c where id =: cusDtId ];
        if(cd!= null && cd.size()>0){
            custDetial = cd[0];
        } 
        custDetailLst.add(custDetial);
        system.debug('custDetailLst '+custDetailLst);
        return custDetailLst;  
    } 
    @Auraenabled
    Public static List<Loan_Contact__c> SaveOtherIncome(List<Loan_Contact__c>  OtherIncDetails,Id conId){
        Loan_Contact__c custDetial =  new Loan_Contact__c();
        List<Loan_Contact__c> custDetailLst = new List<Loan_Contact__c>(); 
        //List<Loan_Contact__c> Allaccwrapperlist = (List<Loan_Contact__c>)JSON.deserialize(OtherIncDetails,List<Loan_Contact__c>.class);
        List<Loan_Contact__c> Allaccwrapperlist = OtherIncDetails;
        system.debug('Allaccwrapperlist '+Allaccwrapperlist);
        if(Allaccwrapperlist[0].isOtherIncomeVerified__c == true){
            update new Loan_Contact__c(id= conId, Other_Income_Status__c = 'Completed', isOtherIncomeVerified__c = true);
        }else{
            update new Loan_Contact__c(id= conId, Other_Income_Status__c = 'In Progress', isOtherIncomeVerified__c = false);
        }
        Loan_Contact__c lc = [SELECT id, isOtherIncomeVerified__c from Loan_Contact__c WHERE ID =: conId];
        system.debug('lc '+lc);
        update Allaccwrapperlist; 
        return  Allaccwrapperlist;
        
    }
     @Auraenabled
    public static boolean GetIseditable(Id conId){
        Loan_Contact__c custobj = [SELECT Id,Loan_Applications__r.Sub_Stage__c from Loan_Contact__c where id =:conId order by createdDate DESC limit 1];
        system.debug('custobj.Loan_Applications__r.Sub_Stage__c::'+custobj.Loan_Applications__r.Sub_Stage__c);
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        system.debug('ProfileName'+profileName);
        if((Label.Sub_Stage.contains(custobj.Loan_Applications__r.Sub_Stage__c) && Label.User_Profiles.contains(profileName)) || (Label.Credit_Team_Profile.contains(profileName))){ 
            return true;//edit mode
        } else {
            return false;//read only mode}   
        }
    }
    @Auraenabled
    public static Loan_Contact__c GetCustdetails(Id conId){
        Loan_Contact__c custobj = [SELECT Id,Loan_Applications__r.Name,Customer__r.Name,Applicant_Type__c from Loan_Contact__c where id =:conId order by createdDate DESC limit 1];
        system.debug('in get custdetails'+custobj);
        return custobj;
    }
}