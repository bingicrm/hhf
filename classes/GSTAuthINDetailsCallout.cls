public class GSTAuthINDetailsCallout {//implements Queueable, Database.AllowsCallouts
    
    /*public String customerDetailId;
    public String constitution;
    public String loanappId;
    public String PanNumber;
    public  String Mobile;
    public String Email;
    public String GSTIN;
    public GSTAuthINDetailsCallout(String GSTIN,String customerDetailId,String loanappId,String PanNumber,String Mobile, String Email, String constitution){
        this.customerDetailId = customerDetailId;
        this.constitution =constitution;
        this.loanappId = loanappId;
        this.PanNumber = PanNumber;
        this.Mobile = Mobile;
        this.Email = Email;
        this.GSTIN = GSTIN;
    } 
    
    public void execute(QueueableContext context) {
        system.debug('execute GSTAuthinCallout');
        makeGstinAuthDetailsCallout( GSTIN, customerDetailId, loanappId, PanNumber, Mobile,  Email,  constitution);
    }*/
    
    public static void makeGstinAuthDetailsCallout(String GSTIN,String customerDetailId,String loanappId,String PanNumber,String Mobile, String Email, String constitution) {
        system.debug('==GSTAuthDetailsClone ===');
        System.debug('GSTIN ' + GSTIN);
        system.debug('customerDetailId '+ customerDetailId);
        system.debug('loanappId ' + loanappId);
        system.debug('PanNumber ' + PanNumber);
        system.debug('Mobile ' + Mobile);
        system.debug('Email ' + Email);
        system.debug('constitution ' + constitution);
        if(String.isNotBlank(GSTIN)) {
            List<Rest_Service__mdt> lstRestService = [SELECT 
                                                      MasterLabel, 
                                                      QualifiedApiName, 
                                                      Service_EndPoint__c, 
                                                      Client_Password__c, 
                                                      Client_Username__c, 
                                                      Request_Method__c, 
                                                      Time_Out_Period__c,
                                                      org_id__c
                                                      FROM   
                                                      Rest_Service__mdt
                                                      WHERE  
                                                      DeveloperName = 'GST_2_GstinAuthDetails'
                                                      LIMIT 1
                                                     ]; 
            System.debug('Debug Log for lstRestService'+lstRestService);
            if(!lstRestService.isEmpty()) {
                GstinAuthDetailsResponseBody objResponseBody = new GstinAuthDetailsResponseBody();
                Blob headerValue = Blob.valueOf(lstRestService[0].Client_Username__c + ':' + lstRestService[0].Client_Password__c);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                Map<String,String> headerMap = new Map<String,String>();
                headerMap.put('Authorization',authorizationHeader);
                headerMap.put('Username',lstRestService[0].Client_Username__c);
                headerMap.put('Password',lstRestService[0].Client_Password__c);
                headerMap.put('Content-Type','application/json');
                headerMap.put('org-id','HHFL');//lstRestService[0].org_id__c
                system.debug('==ORG ID=='+lstRestService[0].org_id__c);
                RequestBody objRequestBody = new RequestBody();
                objRequestBody.consent = 'Y';
                //objRequestBody.pdfOutputType = '';
                objRequestBody.gstin = GSTIN != null ? GSTIN : '';
                
                System.debug('Debug Log for objRequestBody'+objRequestBody);
                String jsonRequest = Json.serializePretty(objRequestBody); 
                
                Http objHttp = new Http();
                HttpRequest objHttpRequest = new HttpRequest();
                
                
                if(String.isNotBlank(lstRestService[0].Service_EndPoint__c)) {
                    objHttpRequest.setEndPoint(lstRestService[0].Service_EndPoint__c);
                }
                else {
                    System.debug('Debug Log for error : Endpoint Empty. Please specify endpoint in metadata record.');
                }
                if(String.isNotBlank(lstRestService[0].Request_Method__c)) {
                    objHttpRequest.setMethod(lstRestService[0].Request_Method__c);
                }
                else {
                    System.debug('Debug Log for error : Requested Method Empty. Please specify Requested Method in metadata record.');
                }
                
                objHttpRequest.setBody(jsonRequest);
                if(String.isNotBlank(String.valueOf(lstRestService[0].Time_Out_Period__c))) {
                    objHttpRequest.setTimeOut(Integer.valueOf(lstRestService[0].Time_Out_Period__c));
                }
                //objHttpRequest.setHeader('content-Type','application/json');
                //objHttpRequest.setHeader('Authorization', authorizationHeader);
                //objHttpRequest.setHeader('org-id', 'HHFL'); // Added as a check 15-10-19
                System.debug('Request => ' + objHttpRequest);
                System.debug('Request Header=> ' + objHttpRequest.getHeader('Authorization'));
                System.debug('Request Body=> ' + objHttpRequest.getbody());
                for(String headerKey : headerMap.keySet()){
                    objHttpRequest.setHeader(headerKey, headerMap.get(headerKey));
                    System.debug('Debug Log for headerKey'+headerKey);
                    System.debug('Debug Log for headerMap.get(headerKey)'+headerMap.get(headerKey));
                }
                //system.debug('CheckHEader' + objHttpRequest);
                
                System.debug('Debug Log for request'+objHttpRequest);
                try {
                    HTTPResponse httpRes = objHttp.send(objHttpRequest);
                    //Create integration logs
                    LogUtility.createIntegrationLogs(jsonRequest,httpRes,lstRestService[0].Service_EndPoint__c);//Changed from Future
                    System.debug('Debug Log for response'+httpRes);
                    System.debug('Debug Log for response body'+httpRes.getBody());
                    System.debug('Debug Log for response status'+httpRes.getStatus());
                    System.debug('Debug Log for response status code'+httpRes.getStatusCode());
                    //List<Customer_Integration__c> gstList = new List<Customer_Integration__c >();
                    objResponseBody = (GstinAuthDetailsResponseBody)Json.deserialize(httpRes.getBody(), GstinAuthDetailsResponseBody.class);
                    
                    Customer_Integration__c GSTRecord = new Customer_Integration__c();
                    Map<String, Schema.RecordTypeInfo> recordTypes = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName();
                    Id GSTINRecTypeId = recordTypes.get('GSTIN').getRecordTypeId();
                    
                    if(httpRes.getStatus() == 'OK' && httpRes.getStatusCode() == 200) {
                        if(objResponseBody.statusCode == 101){
                            system.debug('Status Code ' + objResponseBody.statusCode);
                            if(objResponseBody != null) {
                                System.debug('Result Array'+objResponseBody.result);
                                System.debug('Result Request Id'+objResponseBody.requestId);
                                System.debug('Result Status Code'+objResponseBody.statusCode);
                                
                                Map<String,String> ConstitutionlabelToValueMap = new Map<String,String>();
                                Schema.DescribeFieldResult fieldResult2 = Loan_Contact__c.Constitution__c.getDescribe();  
                                List<Schema.PicklistEntry> plentry = fieldResult2.getPicklistValues();  
                                for (Schema.PicklistEntry f : plentry) {  
                                    ConstitutionlabelToValueMap.put(f.getValue(),f.getLabel());
                                } 
                                System.debug('ConstitutionlabelToValueMapinAPI ' + ConstitutionlabelToValueMap);
                                
                                /*Customer_Integration__c GSTRecord = new Customer_Integration__c();
Map<String, Schema.RecordTypeInfo> recordTypes = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName();
Id GSTINRecTypeId = recordTypes.get('GSTIN').getRecordTypeId();*/
                                system.debug('recodType ' + GSTINRecTypeId);
                                GSTRecord.RecordTypeId = GSTINRecTypeId;
                                system.debug('1');
                                GSTRecord.GSTIN__c = String.isNotBlank(GSTIN) ? GSTIN : '';
                                system.debug('2');
                                if(objResponseBody.result.rgdt != null){
                                    GSTRecord.GST_Date_Of_Registration__c = Date.parse(objResponseBody.result.rgdt);
                                }
                                system.debug('3');
                                GSTRecord.Loan_Contact__c = String.isNotBlank(customerDetailId) ? customerDetailId : null ;
                                system.debug('4');
                                GSTRecord.Loan_Application__c =String.isNotBlank(loanappId) ? loanappId : null  ;
                                system.debug('5');
                                GSTRecord.Constitution__c = String.isNotBlank(constitution) ? ConstitutionlabelToValueMap.get(constitution) : '' ;
                                system.debug('6');
                                GSTRecord.PAN_Number__c = String.isNotBlank(PanNumber) ? PanNumber : '' ;
                                system.debug('7');
                                GSTRecord.Mobile__c = String.isNotBlank(Mobile) ? Mobile : '' ;
                                system.debug('8');
                                GSTRecord.Email__c = String.isNotBlank(Email) ? Email : '' ;
                                system.debug('9');
                                GSTRecord.Request_Id__c = String.isNotBlank(objResponseBody.requestId) ? objResponseBody.requestId : '' ;
                                system.debug('10');
                                GSTRecord.Status_Code__c = String.isNotBlank(String.valueOf(objResponseBody.statusCode)) ? String.valueOf(objResponseBody.statusCode) : '';
                                system.debug('11');
                                GSTRecord.Legal_Name__c = String.isNotBlank(objResponseBody.result.lgnm) ? objResponseBody.result.lgnm : '' ;
                                system.debug('12');
                                GSTRecord.Trade_Name__c = String.isNotBlank(objResponseBody.result.tradeNam) ? objResponseBody.result.tradeNam : '' ;
                                system.debug('13');
                                GSTRecord.GST_Tax_payer_type__c =String.isNotBlank(objResponseBody.result.dty) ? objResponseBody.result.dty : 'none' ;
                                system.debug('14');
                                GSTRecord.Current_Status_of_Registration_Under_GST__c = String.isNotBlank(objResponseBody.result.sts) ? objResponseBody.result.sts : '' ;
                                system.debug('15');
                                GSTRecord.Manual_GSTIN__c = false;
                                system.debug('16');
                                GSTRecord.System_Created__c = true;
                                //GSTRecord.Copy_of_GSTIN__c = String.isNotBlank(GSTIN) ? GSTIN : '';
                                system.debug('=== In CI ==');
                                List<string> slist = new List<String>();
                                slist = objResponseBody.result.nba;
                                string businessActivity = string.join(sList,',');
                                system.debug('businessActivity' + businessActivity);
                                GSTRecord.Business_Activity__c = businessActivity;
                                system.debug('GSTRecord' + GSTRecord);
                                // System.enqueueJob(new EnqueDMLGSTIN(GSTRecord,jsonRequest,String.valueOf(httpRes),lstRestService[0].Service_EndPoint__c));
                                /* insert GSTRecord;
system.debug('GSTRecord BA' + GSTRecord.Business_Activity__c);
system.debug('GSTRecord' + GSTRecord.Id);
if(GSTRecord.Id != null){
Loan_Contact__c loanContact = [Select id,GST_Initiated__c from Loan_Contact__c where id =: customerDetailId limit 1];
if(loanContact.Id != null){
loanContact.GST_Initiated__c = true;
update loanContact;
}

}*/
                            }
                        }
                        else if(objResponseBody.statusCode == 102){
                            SYSTEM.debug('STATUS CODE  ' +objResponseBody.statusCode);
                            /*Customer_Integration__c GSTRecord = new Customer_Integration__c();
Map<String, Schema.RecordTypeInfo> recordTypes = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName();
Id GSTINRecTypeId = recordTypes.get('GSTIN').getRecordTypeId();*/
                            system.debug('recodType ' + GSTINRecTypeId);
                            GSTRecord.RecordTypeId = GSTINRecTypeId;
                            system.debug('A');
                            GSTRecord.Manual_GSTIN__c = false;
                            GSTRecord.System_Created__c = true;
                            GSTRecord.Loan_Contact__c = String.isNotBlank(customerDetailId) ? customerDetailId : null ;
                            system.debug('B');
                            GSTRecord.Loan_Application__c =String.isNotBlank(loanappId) ? loanappId : null  ;
                            system.debug('C');
                            GSTRecord.GSTIN__c = String.isNotBlank(GSTIN) ? GSTIN : '';
                            system.debug('D');   
                            GSTRecord.GSTIN_Error_Message__c = 'Invalid ID number or combination of inputs';
                            
                        }
                    }
                    else{
                        /* Customer_Integration__c GSTRecord = new Customer_Integration__c();
Map<String, Schema.RecordTypeInfo> recordTypes = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName();
Id GSTINRecTypeId = recordTypes.get('GSTIN').getRecordTypeId();
system.debug('recodType ' + GSTINRecTypeId);*/
                        GSTRecord.RecordTypeId = GSTINRecTypeId;
                        system.debug('A');
                        GSTRecord.Manual_GSTIN__c = false;
                        GSTRecord.System_Created__c = true;
                        GSTRecord.Loan_Contact__c = String.isNotBlank(customerDetailId) ? customerDetailId : null ;
                        system.debug('B');
                        GSTRecord.Loan_Application__c =String.isNotBlank(loanappId) ? loanappId : null  ;
                        system.debug('C');
                        GSTRecord.GSTIN__c = String.isNotBlank(GSTIN) ? GSTIN : '';
                        system.debug('D');   
                        GstinAuthDetailsResponseBody errorBody = (GstinAuthDetailsResponseBody)Json.deserialize(httpRes.getBody(), GstinAuthDetailsResponseBody.class);
                        if(errorBody != null){
                            GSTRecord.GSTIN_Error_Message__c = errorBody.error.errorMessage;
                            system.debug('Debug Log for GSTIN_Error_Message__c  ' + errorBody.error.errorMessage);
                        }
                        
                       // System.enqueueJob(new EnqueDMLGSTIN(GSTRecord,jsonRequest,String.valueOf(httpRes),lstRestService[0].Service_EndPoint__c));
                    }
                    
                //==>>   //Changes from Future  // System.enqueueJob(new EnqueDMLGSTIN(GSTRecord,jsonRequest,String.valueOf(httpRes),lstRestService[0].Service_EndPoint__c));
                    insert GSTRecord;
                    system.debug('GSTRecord BA' + GSTRecord.Business_Activity__c);
                    system.debug('GSTRecord' + GSTRecord.Id);
                    if(GSTRecord.Id != null){
                        Loan_Contact__c loanContact = [Select id,GST_Initiated__c from Loan_Contact__c where id =: customerDetailId limit 1];
                        if(loanContact.Id != null){
                            loanContact.GST_Initiated__c = true;
                            update loanContact;
                        }
                        
                    }
                    
                    
                }
                catch(Exception ex) {
                    System.debug('Debug Log for exception'+ex);
                }
            }
        }
    }
    
    public class RequestBody {
        public String consent;
        public String gstin;
        //public String pdfOutputType;
    }
    
}