public class InitiateGSTControllerAPF {
    @AuraEnabled
    public static List<cls_RecordsWrapper> getCriteriaList(Id projectId) {
        if(String.isNotBlank(projectId)) {
            System.debug('Debug Log for projectId'+projectId);
            List<cls_RecordsWrapper> lstRecordsWrapper = new List<cls_RecordsWrapper>();
            List<Project_Builder__c> lstProjectBuilder = [Select Id, Name, Email__c, Contact_Number_Landline__c, Contact_Number_Mobile__c, GSTIN_Number__c, Builder_Name__r.Name, Constitution__c, Builder_Type__c, PAN_Number__c, Project__c, Project__r.Name From Project_Builder__c Where Project__c =: projectId];
            List<Promoter__c> lstPromoter = new List<Promoter__c>();
            if(!lstProjectBuilder.isEmpty()) {
                System.debug('Debug Log for lstProjectBuilder'+lstProjectBuilder);
                lstPromoter = [Select Id, Name, Email__c, Constitution__c, Contact_No_Landline__c, Contact_No_Mobile__c, GSTIN_Number__c, Name_of_Promoter__c, PAN_Number__c, Project_Builder__c From Promoter__c WHERE Project_Builder__c IN: lstProjectBuilder AND GSTIN_Number__c != null];
                
                Map<String,String> mapConstitutionlabelToValue = new Map<String,String>();
                Schema.DescribeFieldResult fieldResult2 = Project_Builder__c.Constitution__c.getDescribe();  
                List<Schema.PicklistEntry> plentry = fieldResult2.getPicklistValues();  
                for (Schema.PicklistEntry f : plentry) {  
                    mapConstitutionlabelToValue.put(f.getValue(),f.getLabel());
                } 
                System.debug('mapConstitutionlabelToValue ' + mapConstitutionlabelToValue);
                
                for(Project_Builder__c objPB : lstProjectBuilder) {
                    String strConstitutionLabel = '';
                    if(!mapConstitutionlabelToValue.isEmpty() && mapConstitutionlabelToValue.keyset().contains(objPB.Constitution__c)){
                        strConstitutionLabel = mapConstitutionlabelToValue.get(objPB.Constitution__c);
                    }
                    lstRecordsWrapper.add(new cls_RecordsWrapper('Project Builder',false,strConstitutionLabel,objPB.Id,new cls_RecordDetails(objPB.Builder_Name__r.Name, objPB.GSTIN_Number__c, objPB.Email__c, objPB.Contact_Number_Mobile__c, objPB.PAN_Number__c, objPB.Project__c)));
                }
                
                if(!lstPromoter.isEmpty()) {
                    System.debug('Debug Log for lstPromoter'+lstPromoter);
                    for(Promoter__c objPromoter : lstPromoter) {
                        String strConstitutionLabel = '';
                        if(!mapConstitutionlabelToValue.isEmpty() && mapConstitutionlabelToValue.keyset().contains(objPromoter.Constitution__c)){
                            strConstitutionLabel = mapConstitutionlabelToValue.get(objPromoter.Constitution__c);
                        }
                        lstRecordsWrapper.add(new cls_RecordsWrapper('Promoter',false,strConstitutionLabel,objPromoter.Id,new cls_RecordDetails(objPromoter.Name_of_Promoter__c, objPromoter.GSTIN_Number__c, objPromoter.Email__c, objPromoter.Contact_No_Mobile__c, objPromoter.PAN_Number__c, null)));
                    }
                }
                return lstRecordsWrapper;
            }
            else {
                return new List<cls_RecordsWrapper>();
            }
        }
        else {
            return new List<cls_RecordsWrapper>();
        }
    }
    
    @AuraEnabled
    public static void makeGSTINAuthCallout(String paramJSONList) {
        System.debug('Debug Log for param list authCallout'+paramJSONList);
        Set<Id> setProjectBuilderIds = new Set<Id>();
        Set<id> setPromoterIds = new Set<Id>();
        List<cls_RecordsWrapper> lstSerializedWrapper = (List<cls_RecordsWrapper>)JSON.deserialize(paramJSONList,List<cls_RecordsWrapper>.class);
        if(!lstSerializedWrapper.isEmpty()) {
            System.debug('Debug Log for deserialized List'+lstSerializedWrapper.size());
            System.debug('Debug Log for deserialized List'+lstSerializedWrapper);
            String sObjName = '';
            Integer counter = 0;
            for(cls_RecordsWrapper objcls_RecordsWrapper : lstSerializedWrapper) {
                if(String.isNotBlank(objcls_RecordsWrapper.strRecordId)) {
                    System.debug('Debug Log for recordId'+objcls_RecordsWrapper.strRecordId);
                    sObjName = objcls_RecordsWrapper.strRecordId.getSObjectType().getDescribe().getName();
                    
                    if(sObjName == Constants.strProjectBuilderAPI) {
                        setProjectBuilderIds.add(objcls_RecordsWrapper.strRecordId);
                    }
                    else if(sObjName == Constants.strPromoterAPI) {
                        setPromoterIds.add(objcls_RecordsWrapper.strRecordId);
                    }
                    
                    if(String.isNotBlank(objcls_RecordsWrapper.obj_RecordDetails.strGSTIN)) {
                        counter += 1;
                        System.debug('Debug Log for objcls_RecordsWrapper.obj_RecordDetails.strGSTIN'+objcls_RecordsWrapper.obj_RecordDetails.strGSTIN);
                    }
                }
                GSTAuthINDetailsCalloutAPF.makeGstinAuthDetailsCallout(objcls_RecordsWrapper.obj_RecordDetails.strGSTIN, objcls_RecordsWrapper.strRecordId, objcls_RecordsWrapper.obj_RecordDetails.strProjectId, objcls_RecordsWrapper.obj_RecordDetails.strPanNumber, objcls_RecordsWrapper.obj_RecordDetails.strMobileNumber, objcls_RecordsWrapper.obj_RecordDetails.strEmail,objcls_RecordsWrapper.strConstitution);
                System.debug('Counter ==>' +counter);
            }
        }
        
    }
    
    @AuraEnabled
    public static List<String> getErrorMessage(String paramJSONList) { 
        Set<Id> setProjectBuilderIds = new Set<Id>();
        Set<Id> setPromoterIds = new Set<Id>();
        
        List<cls_RecordsWrapper> lstSerializedWrapper = (List<cls_RecordsWrapper>)JSON.deserialize(paramJSONList,List<cls_RecordsWrapper>.class);
        System.debug('Debug Log for deserialized List'+lstSerializedWrapper.size());
        System.debug('Debug Log for deserialized List'+lstSerializedWrapper);
        
        if(!lstSerializedWrapper.isEmpty()) {
            for(cls_RecordsWrapper obj_RecordsWrapper : lstSerializedWrapper) {
                if(obj_RecordsWrapper.strRecordId.getSObjectType().getDescribe().getName() == Constants.strProjectBuilderAPI) {
                    setProjectBuilderIds.add(obj_RecordsWrapper.strRecordId);
                }
                else if(obj_RecordsWrapper.strRecordId.getSObjectType().getDescribe().getName() == Constants.strPromoterAPI) {
                    setPromoterIds.add(obj_RecordsWrapper.strRecordId);
                }
            }
        }
        
        List<String> cdWithErrorMessage = new List<String>();
        List<Customer_Integration__c> cilist = new List<Customer_Integration__c>();
        
        if(!setProjectBuilderIds.isEmpty()) {
            List<Project_Builder__c> lstProjectBuilder = [SELECT Id, Name, (SELECT Id, Name, Project_Builder__r.Name, GSTIN_Error_Message__c FROM Builder_Integrations__r WHERE RecordType.Name = 'GSTIN' ORDER By CreatedDate DESC LIMIT 1) FROM Project_Builder__c WHERE ID IN: setProjectBuilderIds];
            if(!lstProjectBuilder.isEmpty()) {
                for(Project_Builder__c objProjBuilder : lstProjectBuilder) {
                    if(!objProjBuilder.Builder_Integrations__r.isEmpty()) {
                        if(String.isNotBlank(objProjBuilder.Builder_Integrations__r[0].GSTIN_Error_Message__c)){
                            System.debug('Debug Log for Error Message Project Builder'+objProjBuilder.Builder_Integrations__r[0].GSTIN_Error_Message__c);
                            cdWithErrorMessage.add(objProjBuilder.Builder_Integrations__r[0].GSTIN_Error_Message__c);
                        }
                    }
                }
            }
        }
        
        if(!setPromoterIds.isEmpty()) {
            List<Promoter__c> lstPromoter = [SELECT Id, Name, (SELECT Id, Name, Promoter__r.Name, GSTIN_Error_Message__c FROM Promoter_Integrations__r WHERE RecordType.Name = 'GSTIN' ORDER By CreatedDate DESC LIMIT 1) FROM Promoter__c WHERE ID IN: setPromoterIds];
            if(!lstPromoter.isEmpty()) {
                for(Promoter__c objPromoter : lstPromoter) {
                   if(!objPromoter.Promoter_Integrations__r.isEmpty()) {
                        if(String.isNotBlank(objPromoter.Promoter_Integrations__r[0].GSTIN_Error_Message__c)) {
                            System.debug('Debug Log for Error Message Promoter'+objPromoter.Promoter_Integrations__r[0].GSTIN_Error_Message__c);
                            cdWithErrorMessage.add(objPromoter.Promoter_Integrations__r[0].GSTIN_Error_Message__c);
                        }
                    } 
                }
            }
            
        }
        //system.debug('cdToMessageMap ' + cdToMessageMap);
        system.debug('cdWithErrorMessage ' + cdWithErrorMessage);
        //return cdToMessageMap;
        return cdWithErrorMessage;
        
    }

    public class cls_RecordsWrapper {
        @AuraEnabled public String strObjectType; // To specify the type of record.
        @AuraEnabled public Boolean isChecked; // To specify the checked record for GSTIN Callout.
        @AuraEnabled public String strConstitution; // To specify the Constitution.
        @AuraEnabled public Id strRecordId; // To consume the record Id's.
        @AuraEnabled public cls_RecordDetails obj_RecordDetails; //To consume the relevant information from record.
        public cls_RecordsWrapper(String strObjectType, Boolean isChecked, String strConstitution, Id strRecordId, cls_RecordDetails obj_RecordDetails) {
            this.strObjectType = strObjectType;
            this.isChecked = isChecked;
            this.strConstitution = strConstitution;
            this.strRecordId = strRecordId;
            this.obj_RecordDetails = obj_RecordDetails;
        }
    }
    
    public class cls_RecordDetails {
        @AuraEnabled public String strBuilderName;
        @AuraEnabled public String strGSTIN;
        @AuraEnabled public String strEmail;
        @AuraEnabled public String strMobileNumber;
        @AuraEnabled public String strPanNumber;
        @AuraEnabled public String strProjectId;
        public cls_RecordDetails(String strBuilderName, String strGSTIN, String strEmail, String strMobileNumber, String strPanNumber, String strProjectId) {
            this.strBuilderName = strBuilderName;
            this.strGSTIN = strGSTIN;
            this.strEmail = strEmail;
            this.strMobileNumber = strMobileNumber;
            this.strPanNumber = strPanNumber;
            this.strProjectId = strProjectId;
        }
    }
}