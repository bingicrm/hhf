@isTest(SeeAllData=false)
/*
*  Name           :   LOG_ObjectMappingSetting
*  Created By     :   Shilpa Menghani
*  Created Date   :   May 9,2016
*  Description    :   Test class 
*/
private class LOG_ObjectMappingSettingTest{
    private static User user;
    private static void setUpData(){
        Profile profile = [select Id from Profile where Name =: 'System Administrator'];
        
        user = new User();
        user.Username = 'testUser88s@deloitte.com';
        user.Email = 'testUser123@deloitte.com';
        user.LastName = 'Test88';
        user.Alias = 'Test123';
        user.ProfileID = profile.Id;
        user.LocaleSidKey = 'en_US';
        user.LanguageLocaleKey = 'en_US';
        user.Country = 'India';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.EmployeeNumber = '22';
        user.EmailEncodingKey='UTF-8';
        insert user;
        
        
    } 
    static testMethod void myUnitTestOne() {
        // TO DO: implement unit test
        setUpData();
        
        Test.startTest();
        List<Object_Mapping__c> settingslist = new List<Object_Mapping__c>();
                
        
        
        System.runAs(user) { 
            Object_Mapping__c ObjectName = new Object_Mapping__c();
            ObjectName.Name = 'Object Name';
            ObjectName.API_Name__c = 'Task';
            settingslist.add(ObjectName);
            insert settingslist;
            LOG_ObjectMappingSetting logObjMapping = new LOG_ObjectMappingSetting();
            system.assertNotEquals(logObjMapping.ObjectMappingList.size(),null);
            system.assertEquals(logObjMapping.objectMapping.API_Name__c,'Task'); 
            logObjMapping.getObjectList();
            logObjMapping.onChangeObjectOption();
            system.assertNotEquals(logObjMapping.ObjectMappingList.size(),null);
            
            logObjMapping.save();
            logObjMapping.deleteMapping();
            
            Object_Mapping__c objMap = Object_Mapping__c.getInstance('Object Name');
           system.assertEquals(objMap,null);
        }
        Test.stopTest();
     }
}