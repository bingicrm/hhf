@isTest
public class TestTATInsertionUtility {
   
    public static testMethod void method1()
    {
               Profile p1 = [SELECT Id FROM Profile WHERE Name='DSA']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p1.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');
              
                Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='8890988909',
                                         Date_of_Incorporation__c=Date.newInstance(2018,11,11));
                insert Cob;                
                            
                Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                            Scheme_ID__c=11,Product_Code__c='CF');
                insert sc;
                            
                Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,StageName__c='Customer Onboarding',
                                                                  Transaction_type__c='PB',Requested_Amount__c=1000,Applicant_Customer_segment__c='Salaried',
                                                                  Branch__c='test',Line_Of_Business__c='Open Market',Assigned_Sales_User__c=u.id,
                                                                  Loan_Purpose__c='20',Sub_Stage__c='Application Initiation');
                insert LAob;  
                            
                List<ID> listId= new List<ID>();
                listId.add(LAob.id); 
                update LAob;
                            
                Loan_Contact__c LCobject=[SELECT id from Loan_Contact__c where Loan_Applications__c=:LAob.id];
                    
                TAT_Management__c TMob= new TAT_Management__c(Loan_Application__c=LAob.id,TAT_Type__c='Sub Stage',stage__c='test',
                                                              SubStage__c='Test',end_time__c=null,
                                                              start_time__c=DateTime.newInstance(2018,11,10,00,00,00));
                                                                          
                            Database.insert(TMob);
                            
                    
                            TAT_Matrix__c TMatrix=new TAT_Matrix__c(Customer_Segment__c='1',LOB__c='Open Market',Scheme__c=sc.id,Stage__c='Customer Onboarding',
                                                                    SubStage__c='Application Initiation',TAT_time__c=50);
                            Database.insert(TMatrix);
                            
                            
                            TMatrix.LOB__c=LAob.Line_Of_Business__c;
                            TMatrix.Scheme__c=LAob.Scheme__c;
                            update TMatrix; 
                            
                            //system.debug('HUNT'+TMatrix);
                            //system.debug('HUNT'+'TMatrixValues '+TMatrix);
                            system.debug('HUNT'+'Customer segement '+TMatrix.Customer_Segment__c +'LAvalues___  '+LAob.Applicant_Customer_segment__c);
                            system.debug('HUNT'+'LOB '+TMatrix.LOB__c+'LAValues___ '+ LAob.Line_Of_Business__c);
                            system.debug('HUNT'+'scheme '+TMatrix.Scheme__c+'LAvalues_____'+ LAob.Scheme__c);
                            system.debug('HUNT'+'product '+TMatrix.Product__c+'LAvalues_____ '+ LAob.product_Name__c);
                            
                            
                            Cross_Sell_Partner_Name__c CSPN=new Cross_Sell_Partner_Name__c(Name='Test');
                            Database.insert(CSPN);
                            
                            DST_Master__c DSTMaster= new DST_Master__c(Name='Test DST',Inspector_ID__c='1234');
                            Database.insert(DSTMaster);
                            
                            Sourcing_Detail__c SDob= new Sourcing_Detail__c(Loan_Application__c=LAob.id,Source_Code__c='16',DST_Name__c=DSTMaster.id,
                                                                            Sales_User_Name__c=u.id,Cross_Sell_Partner_Name__c=CSPN.id);
                            
                            Database.insert(SDob);
                                           
                            test.startTest();
                                TATInsertionUtility.TATinsertion(listId);
                               
                             test.stopTest();
                   
                
                
    }
    
    
    public static testMethod void method2()
    {
              Profile p1 = [SELECT Id FROM Profile WHERE Name='DSA']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p1.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');

                            Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='8890988909',
                                                     Date_of_Incorporation__c=Date.newInstance(2018,11,11));
                            insert Cob;                
                            
                            Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                                        Scheme_ID__c=11,Product_Code__c='HL');
                            insert sc;
                            
                            Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,StageName__c='Customer Onboarding',
                                                                              Transaction_type__c='PB',Requested_Amount__c=1000,Applicant_Customer_segment__c='Salaried',
                                                                              Branch__c='test',Line_Of_Business__c='Open Market',Assigned_Sales_User__c=u.id,
                                                                              Loan_Purpose__c='20',Sub_Stage__c='Application Initiation');
                            insert LAob;  
                            List<ID> listId= new List<ID>();
                            listId.add(LAob.id); 
                            
                            Loan_Contact__c LCobject=[SELECT id from Loan_Contact__c where Loan_Applications__c=:LAob.id];
                    
                            TAT_Management__c TMob= new TAT_Management__c(Loan_Application__c=LAob.id,TAT_Type__c='Stage',stage__c='Customer Onboarding',
                                                                          SubStage__c='Application Initiation',end_time__c=null,
                                                                          start_time__c=DateTime.newInstance(2018,11,10,00,00,00));
                                                                          
                            Database.insert(TMob);
                    
                            TAT_Matrix__c TMatrix=new TAT_Matrix__c(Customer_Segment__c='1',LOB__c='Open Market',Scheme__c=sc.id,Stage__c='Customer Onboarding',
                                                                    SubStage__c='Application Initiation',TAT_time__c=50);
                            Database.insert(TMatrix);
                            //system.debug('HUNT'+TMatrix);
                            
                            Cross_Sell_Partner_Name__c CSPN=new Cross_Sell_Partner_Name__c(Name='Test');
                            Database.insert(CSPN);
                            
                            DST_Master__c DSTMaster= new DST_Master__c(Name='Test DST',Inspector_ID__c='1234');
                            Database.insert(DSTMaster);
                            
                            Sourcing_Detail__c SDob= new Sourcing_Detail__c(Loan_Application__c=LAob.id,Source_Code__c='16',DST_Name__c=DSTMaster.id,
                                                                            Sales_User_Name__c=u.id,Cross_Sell_Partner_Name__c=CSPN.id);
                            
                            Database.insert(SDob);
                                           
                            test.startTest();
                            TATInsertionUtility.StageTATinsertion(listId);  
                            test.stopTest();
                                   
    }
   

}