public class StatusTriggerHelper{
    public static void loanCancellation(List<Status_Tracking__c> canList){
        String loanDisbursedRecType = [SELECT Id, Name From RecordType Where sObjectType = 'Loan_Application__c' AND DeveloperName = 'Loan_Disbursed' LIMIT 1].ID;
        String loanCancelledRecType = [SELECT Id, Name From RecordType Where sObjectType = 'Loan_Application__c' AND DeveloperName = 'Cancelled_Rejected' LIMIT 1].ID;
        Set<String> idList = new Set<String>();
        List<Loan_Application__c> listToUpdate = new List<Loan_Application__c>();
        List<Id> loanIdList = new List<Id>();
        LMS_Date_Sync__c dt = [Select Id, Current_Date__c from LMS_Date_Sync__c ORDER BY CreatedDate DESC limit 1];
        for(Status_Tracking__c st: canList){
            idList.add(st.Application_Id__c);
        }
        System.debug('Debug Log for idList'+idList.size());
        
        List<Loan_Application__c> laList = [Select Id, StageName__c, Sub_Stage__c, Loan_Number__c, Date_of_Transaction__c, Loan_Status_Remarks__c, Closed_Cancellation_Reason__c, Loan_Status__c, Remarks__c, Cancellation_Reason__c, Assigned_Sales_User__c, Loan_Status_Maker_Name__c, Loan_Status_Author_Name__c from Loan_Application__c where Loan_Number__c IN: idList];
        System.debug('Debug Log for laList:'+laList);
        for(Status_Tracking__c st: canList){
            for(Loan_Application__c la: laList){
                if(st.Application_Id__c == la.Loan_Number__c && la.Loan_Status__c == 'Active'){
                    la.Loan_Status__c = st.Status__c;
                    la.Closed_Cancellation_Reason__c = st.Reason_for_Cancellation__c;
                    la.Loan_Status_Remarks__c = st.Remarks__c;
                    la.Date_of_Transaction__c = st.Date_of_Transaction__c;
                    la.Loan_Status_Maker_Name__c = st.Maker_Name__c;
                    la.Loan_Status_Author_Name__c = st.Author_Name__c;
                    if(st.Status__c == 'Closed') {
                        la.StageName__c = 'Loan Disbursed';
                        la.Sub_Stage__c = 'Loan Disbursed';
                        if(String.isNotBlank(loanDisbursedRecType)) {
                            la.RecordTypeId = loanDisbursedRecType;
                        }
                    }
                    else if(st.Status__c == 'Cancelled') {
                        if(la.StageName__c != 'Loan Disbursed'){
                            la.StageName__c = 'Loan Cancelled';
                            la.Sub_Stage__c = 'Loan Cancel';
                            la.Tranche_Cancelled_Date__c = st.Date_of_Transaction__c;
                            la.OwnerId = la.Assigned_Sales_User__c;
                        }
                        la.Cancellation_Reason__c = st.Reason_for_Cancellation__c;
                        la.Remarks__c = st.Remarks__c;
                        if(String.isNotBlank(loanCancelledRecType)) {
                            la.RecordTypeId = loanCancelledRecType;
                        }
                        loanIdList.add(la.Id);
                    }
                    //loanIdList.add(la.Id);
                    listToUpdate.add(la);
                }
            }
        }
        List<Disbursement__c> disbList = [Select Id, Tranche_Cancelled__c, Loan_Application__c, Cancellation_Date__c, Cancelled_Amount__c, Disbursal_Amount__c from Disbursement__c where Loan_Application__c =: loanIdList];
        List<Disbursement__c> disbListToUpdate = new List<Disbursement__c>();
        for(Disbursement__c d: disbList){
            d.Tranche_Cancelled__c = TRUE;
            d.Cancellation_Date__c = dt.Current_Date__c;
            d.Cancelled_Amount__c = d.Disbursal_Amount__c;
            disbListToUpdate.add(d);
        }
        System.debug('Debug Log for listToUpdate + disbListToUpdate'+listToUpdate.size()+' + '+disbListToUpdate.size());
        if(listToUpdate.size()>0){
            Database.SaveResult[] lstDSR = database.update(listToUpdate,false);
			List<Error_Log__c> errLogList = new List<Error_Log__c>();	//Added by Ankit for LMS_Callback error log issue
            Integer index = 0;	//Added by Ankit for LMS_Callback error log issue
            for(Database.SaveResult objDSR : lstDSR) {
                if(!objDSR.isSuccess()) {
                    System.debug('Debug Log for Error During LA Update'+objDSR.getErrors());
					/*Added by Ankit for LMS_Callback error log issue - Start*/
                    String appId = (!listToUpdate.isEmpty() && listToUpdate[index] != null) ? listToUpdate[index].Loan_Number__c : '';
                    errLogList.add(new Error_Log__c(Error_Message__c = appId+'--'+objDSR.getErrors()[0].getMessage(), 
                                                    Class_Name__c = 'StatusTriggerHelper', Error_Code__c = 'LMS_Callback', 
                                                    Type__c = 'DML Exception'));
                    /*Added by Ankit for LMS_Callback error log issue - End*/
                }
                else {
                    System.debug('Record Updated Successfully'+objDSR.getId());
                    if(disbListToUpdate.size()>0){
                        database.update(disbListToUpdate);
                    }
                }
				index++;	//Added by Ankit for LMS_Callback error log issue
            }
			/*Added by Ankit for LMS_Callback error log issue - Start*/
            if(errLogList.size() > 0){
                insert errLogList;
            }
            /*Added by Ankit for LMS_Callback error log issue - End*/
        }
    }
    public static void trancheCancellation(List<Status_Tracking__c> trancheList){
        Set<String> idList = new Set<String>();
        if(!trancheList.isEmpty()) {
            for(Status_Tracking__c st: trancheList){
                idList.add(st.Application_Id__c);
            }
            System.debug('Debug Log for idList'+idList.size());
            if(!idList.isEmpty()) {
                List<Tranche__c> lstTranchetoUpdate = new List<Tranche__c>();
                List<String> lstTrancheStatus = new List<String>();
                lstTrancheStatus.add('Disbursed');
                lstTrancheStatus.add('Cancelled');
                lstTrancheStatus.add('Rejected');
                decimal amount = 0;
                List<Tranche__c> lstTranche = [Select Id, Name, Approved_Disbursal_Amount__c, Loan_Application__c, Reason__c, Loan_Application__r.Loan_Number__c, Maker_Name__c, Author_Name__c From Tranche__c WHERE Loan_Application__r.Loan_Number__c IN: idList AND Status__c = 'Disbursed' AND Pending_Amount__c != 0 ORDER BY CreatedDate ASC];
                System.debug('Debug Log for lstTranche'+lstTranche.size());
                if(!lstTranche.isEmpty()) {
                    for(Status_Tracking__c objST : trancheList) {
                        for(integer i = 0; i<lstTranche.size(); i++) {
                            if(objST.Application_Id__c == lstTranche[i].Loan_Application__r.Loan_Number__c && i == decimal.valueOf(objST.Tranch_Identifier__c)-2) {
                                lstTranche[i].Status__c = 'Cancelled';
                                lstTranche[i].Reason__c = String.isNotBlank(objST.Reason_for_Cancellation__c) ? objST.Reason_for_Cancellation__c : '';
                                lstTranche[i].Cancellation_Date__c = objST.Date_of_Transaction__c != null ? objST.Date_of_Transaction__c : null;
                                lstTranche[i].Cancellation_Remarks__c = objST.Remarks__c != null ? objST.Remarks__c : null;
                                lstTranche[i].Maker_Name__c = objST.Maker_Name__c != null ? objST.Maker_Name__c : null;
                                lstTranche[i].Author_Name__c = objST.Author_Name__c != null ? objST.Author_Name__c : null;
                                amount = amount + lstTranche[i].Approved_Disbursal_Amount__c;
                                //lstTranche[i].Approved_Disbursal_Amount__c = 0;
                                lstTranchetoUpdate.add(lstTranche[i]);
                            }                            
                        }
                    }
                    List<Tranche__c> latestTranche = [Select Id, Pending_Amount2__c from Tranche__c WHERE Loan_Application__r.Loan_Number__c IN: idList AND Status__c NOT IN: lstTrancheStatus ORDER BY CreatedDate DESC];
                    //Tranche__c latestTranche = [Select Id, Pending_Amount2__c from Tranche__c WHERE Loan_Application__r.Loan_Number__c IN: idList AND Status__c NOT IN: lstTrancheStatus ORDER BY CreatedDate DESC limit 1]; 
                    if(latestTranche.size() > 0){
                        latestTranche[0].Pending_Amount2__c = latestTranche[0].Pending_Amount2__c - amount;
                        lstTranchetoUpdate.add(latestTranche[0]);
                    }
                    System.debug('Debug Log for lstTranchetoUpdate'+lstTranchetoUpdate.size());
                    if(!lstTranchetoUpdate.isEmpty()) {
                        Database.SaveResult[] lstDSR = database.update(lstTranchetoUpdate,false);
						List<Error_Log__c> errLogList = new List<Error_Log__c>();	//Added by Ankit for LMS_Callback error log issue
                        Integer index = 0;	//Added by Ankit for LMS_Callback error log issue
                        for(Database.SaveResult objDSR : lstDSR) {
                            if(!objDSR.isSuccess()) {
                                System.debug('Debug Log for Error During Tranche Update'+objDSR.getErrors());
								/*Added by Ankit for LMS_Callback error log issue - Start*/
                                String appId = (!lstTranchetoUpdate.isEmpty() && lstTranchetoUpdate[index] != null) ? lstTranchetoUpdate[index].Loan_Application__r.Loan_Number__c : '';
                                errLogList.add(new Error_Log__c(Error_Message__c = appId+'--'+objDSR.getErrors()[0].getMessage(), 
                                                                Class_Name__c = 'StatusTriggerHelper', Error_Code__c = 'LMS_Callback', 
                                                                Type__c = 'DML Exception'));
                                /*Added by Ankit for LMS_Callback error log issue - End*/
                            }
                            else {
                                System.debug('Record Updated Successfully'+objDSR.getId());
                            }
							index++;	//Added by Ankit for LMS_Callback error log issue
                        }
						/*Added by Ankit for LMS_Callback error log issue - Start*/
                        if(errLogList.size() > 0){
                            insert errLogList;
                        }
                        /*Added by Ankit for LMS_Callback error log issue - End*/
                    }                  
                }
            }
        }
        
    }
    public static void pemiToEmi(List<Status_Tracking__c> pemiList){
        system.debug('==PEMI to EMI==');
        Set<String> idList = new Set<String>();
        List<Loan_Application__c> listToUpdate = new List<Loan_Application__c>();
        for(Status_Tracking__c st: pemiList){
            idList.add(st.Application_Id__c);
        }
        List<Loan_Application__c> laList = [Select Id, Loan_Number__c, Repayment_Start_Date__c, Loan_Status__c from Loan_Application__c where Loan_Number__c =: idList];
        for(Status_Tracking__c st: pemiList){
            for(Loan_Application__c la: laList){
                if(st.Application_Id__c == la.Loan_Number__c && la.Loan_Status__c == 'Active'){
                    la.Repayment_Start_Date__c = st.Repayment_Date__c;
                    listToUpdate.add(la);
                }
            }
        }
        if(listToUpdate.size()>0){
            system.debug('==PEMI to EMI Update Size=='+listToUpdate.size());
            database.update(listToUpdate);
        }
    }
}