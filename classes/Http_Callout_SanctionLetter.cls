public class Http_Callout_SanctionLetter {
    
    
    @AuraEnabled
    public static String SanctionLetter(Id recordId){
        
        String boolSuccess = 'false';  
		String profileName = [SELECT Id,Name from Profile WHERE ID =: UserInfo.getProfileId()].Name; //Added by Abhilekh on 11th October 2019 for FCU BRD
      //  try {
             
            //id loanApp = 'a00p0000006uRPQ';
            system.debug('@@id' + recordId);
            id loanApp = recordId;
         Loan_Application__c la = [Select id, Repayment_Generated_for_Documents__c,Sanction_Date__c,Approved_Date__c ,Approved_Processing_Fees1__c,Processing_Fee_Percentage__c,Scheme__c, Scheme__r.Reference_Rate__c,Fixed_For_Months__c,Total_PF_Amount__c,Name, Product_Name__c,Loan_Purpose__c, Requested_Amount__c, Requested_Loan_Tenure__c, Requested_EMI_amount__c,Requested_Processing_Fees1__c,
                                      Balance_PF_amount__c, Approved_Foreclosure_Charges__c,Approved_Loan_Amount__c, Approved_cross_sell_amount__c,Applicable_IMD__c,Interest_Type__c,
                                      Approved_Loan_Tenure__c,Approved_ROI__c,Loan_Number__c,Approved_EMI__c,Business_Date_Created__c,Spread__c,Reference_Rate__c,Insurance_Loan_Application__c,Funded_By_Customer__c,Insurance_Loan_Created__c,Physical_Cheque_Received__c,Source_Code__c, /** Added by saumya for Physical Documents TIL 1150**/
									  Sub_Stage__c,Overall_FCU_Status__c,FCU_Decline_Count__c,Exempted_from_FCU_BRD__c,Hunter_Decline_Count__c,IMGC_Decision__c,IMGC_Premium_Fees__c,IMGC_Premium_Fee__c,IMGC_Premium_Fees_GST_Inclusive1__c,/** Added by Saumya for IMGC BRD **/Transaction_type__c, /*Added by Ankit for Loan_Type change on Sanction Letter 30-11-2020 */
									  (Select id, Name, Borrower__c, Phone_Number__c,Email__c, Customer__r.name from Loan_Contacts__r where Applicant_Type__c = 'Applicant') 
                                      from Loan_Application__c where id =:loanApp];/** Added by saumya Insurance_Loan_Application__c, Insurance_Loan_Created__c for Insurance Loan **/
										//Scheme__r.Reference_Rate__c, Fixed_For_Months__c added by Saumya for TIL-00001688
										//Sub_Stage__c,Overall_FCU_Status__c,FCU_Decline_Count__c added by Abhilekh on 11th October 2019 for FCU BRD
                                      //Exempted_from_FCU_BRD__c added by Abhilekh on 23rd December 2019 for FCU BRD Exemption
									  //Hunter_Decline_Count__c added by Abhilekh on 24th January 2020 for Hunter BRD
									//IMGC_Decision__c,IMGC_Premium_Fees__c added by Saumya on 25th August 2020 FOR IMGC BRD
        /** Added by saumya for Insurance Loan **/
        if(la.Insurance_Loan_Application__c == True){
            boolSuccess ='Insurance Loan Application';
        }
        /** Added by saumya for Insurance Loan **/
		/** Added by saumya for Physical Documents TIL 1150**/
       else if(la.Physical_Cheque_Received__c != 'Yes' && la.Source_Code__c == 'DSA'){
           boolSuccess ='Physical Documents not received';
        }
        /** Added by saumya for Physical Documents TIL 1150**/
		
		//Below else if block added by Abhilekh on 11th October 2019 for FCU BRD,&& la.Exempted_from_FCU_BRD__c == false added by Abhilekh on 23rd December 2019 for FCU BRD Exemption
        else if(profileName == 'Credit Team' && la.Overall_FCU_Status__c == Constants.NEGATIVE && la.FCU_Decline_Count__c > 0 && (la.Sub_Stage__c == Constants.Credit_Review || la.Sub_Stage__c == Constants.Re_Credit || la.Sub_Stage__c == Constants.Re_Look) && la.Exempted_from_FCU_BRD__c == false){
            boolSuccess = 'Sancion letter cannot be generated because this application is FCU Decline.';
        }
		
		//Below else if block added by Abhilekh on 24th January 2020 for Hunter BRD
        else if(profileName == 'Credit Team' && la.Hunter_Decline_Count__c > 0 && (la.Sub_Stage__c == Constants.Credit_Review || la.Sub_Stage__c == Constants.Re_Credit || la.Sub_Stage__c == Constants.Re_Look)){
            boolSuccess = 'Sancion letter cannot be generated as Hunter analysis has not been recommended. Please reject or assign the application to Hunter Manager.';
        }
		
        else{
            List<Loan_Repayment__c> lstLoanRepay = [Select Id, Disbursal_Date__c ,EMI_Start_date__c, Installment_Plan__c, (Select Id, Slab_and_EMI_Instl_To__c, Slab_and_EMI_Instl_From__c, Slab_and_EMI_EMI__c from Graded_Details__r) from Loan_Repayment__c where Loan_Application__c =:loanApp LIMIT 1 ];
            List<LMS_Date_Sync__c> lstLMS = [SELECT Id, LastModifiedDate, Current_Date__c FROM LMS_Date_Sync__c order by LastModifiedDate desc LIMIT 1];
            
            //if ( !lstLMS.isEmpty() && !lstLoanRepay.isEmpty() && lstLoanRepay[0].Disbursal_Date__c != lstLMS[0].Current_Date__c ) {
            //    return 'repayment';
            //}
            
            List<Repayment_Schedule__c> lstRepay = [Select Id, LastmodifiedDate, Installment_Amount__c from Repayment_Schedule__c where Loan_Application__c =:loanApp AND Installment_Amount__c != Null ORDER BY LastmodifiedDate LIMIT 1];
          
            if ( lstRepay != null && lstRepay.size() > 0) { 
                Long int1 = system.now().getTime() - lstRepay[0].LastmodifiedDate.getTime(); 
                system.debug('@@@inst1' + int1);                       
                //Mandatory generation of repayment
                if ( !la.Repayment_Generated_for_Documents__c ) {
                    la.Repayment_Generated_for_Documents__c = true;
                    update la;
                    return 'repayment';
                } else if ( la.Repayment_Generated_for_Documents__c && int1 > 100000) {
                    return 'repayment';
                }
            }
            
            List<Address__c> lstAdd = [Select id, Type_of_address__c,Address_Line_1__c, Address_Line_2__c,CityLP__c, Loan_Contact__r.Preferred_Communication_address__c, Pincode_LP__r.name,  StateLP__c from Address__c where Loan_Contact__c =:la.Loan_Contacts__r[0].Id ]; 
            Address__c ad = new Address__c();
            for (Address__c  objAdd : lstAdd) {
                system.debug('@@@@objAdd' + objAdd);
                if ( objAdd.Loan_Contact__r.Preferred_Communication_address__c == objAdd.Type_of_address__c ) {
                    ad = objAdd;
                    break;
                }
            }
            
            system.debug('@@@@' + ad);
            List<Loan_Sanction_Condition__c> lstLoanSac = [Select id, Remarks__c, Status__c,Sanction_Condition__c,Sanction_Condition_Master__r.Sanction_Condition__c from Loan_Sanction_Condition__c where Loan_Application__c =: loanApp ];
           
            
            Loan_Application__c laCo = [Select id, Name, loan_number__c,Sub_Stage__c,
                                      (Select id, Name, Phone_Number__c,Email__c, Customer__r.name from Loan_Contacts__r where Applicant_Type__c = 'Co- Applicant') 
                                      from Loan_Application__c where id =:loanApp limit 1];
            
            // Query doc id from doc master :
            List<Document_Master__c> lstdocMaster = [Select Doc_Id__c from Document_Master__c where Name = 'Sanction Letter' LIMIT 1];
            
            
            
            List<Property__c> lstProp = [Select Id, Flat_No_House_No__c ,Floor__c , Builder_Name_Society_Name__c , Address_Line_1__c , Address_Line_2__c , Address_Line_3__c, City_LP__c,State_LP__c, Pincode_LP__r.Name from Property__C where Loan_Application__c=:loanApp Limit 1];        
            system.debug('lstProp@@' + lstProp);
            
            
            List<Loan_Repayment__c> LRNList= [SELECT Id, dueDay__c, Loan_Application__c FROM Loan_Repayment__c WHERE Loan_Application__c = :loanApp]; //Added By Abhishek for TIL 2417
            
            List<IMD__c> lstIMD = [Select Id,Amount__c,Cheque_Status__c,Cheque_ID__c from IMD__c where Loan_Applications__c = :loanApp];
            Decimal decIMD = 0;
            for (IMD__C objIMD : lstIMD) {
                if (objIMD.Cheque_Status__c != 'X' && objIMD.Cheque_Status__c != 'B' && objIMD.Cheque_ID__c != null && !objIMD.Cheque_ID__c.containsIgnoreCase('blank') ) {
                    decIMD += objIMD.Amount__c;
                    system.debug('decimd in for @@@' + decimd);
                }
            }
            
            system.debug('decimd@@@' + decimd);
            
            //List<Processing_Fees__mdt> lstPF = [Select Id, Processing_Fees__c, Product__c, Property_Type__c from Processing_Fees__mdt where Product__c =:la.Product_Name__c];
            // Updated approved amount as per TIL-1352
            //Decimal updatedApprovedAmt = la.Approved_Loan_Amount__c != null && la.Approved_cross_sell_amount__c != null ? la.Approved_Loan_Amount__c + la.Approved_cross_sell_amount__c : la.Approved_Loan_Amount__c;
			/*Added by Saumya For insurance Loan*/
            Decimal updatedApprovedAmt;
			Decimal updatedApprovedAmtwithIMGCFee; /** Added by Saumya for IMGC BRD **/	
            if(la.Insurance_Loan_Created__c){
                updatedApprovedAmt= la.Approved_Loan_Amount__c;
				updatedApprovedAmtwithIMGCFee = la.Approved_Loan_Amount__c; /** Added by Saumya for IMGC BRD **/   
            }   
            else{
                updatedApprovedAmt= la.Approved_Loan_Amount__c != null && la.Approved_cross_sell_amount__c != null ? la.Approved_Loan_Amount__c + la.Approved_cross_sell_amount__c : la.Approved_Loan_Amount__c;
				updatedApprovedAmtwithIMGCFee = updatedApprovedAmt+ (la.IMGC_Premium_fee__c != null ? la.IMGC_Premium_fee__c: 0); /** Added by Saumya for IMGC BRD **/
            }
            /*Added by Saumya For insurance Loan*/	  
            
            /******Added by Chitransh for TIL-1491*************/
            Decimal updatedPF = null;
            Decimal TotalPF = null;/*** Addeed by Saumya For IMGC BRD ***/
            Decimal TotalPFCharges = null;/*** Addeed by Saumya For IMGC BRD ***/
            if(la.Processing_Fee_Percentage__c != null && updatedApprovedAmt != null) {/** Replaced by updatedApprovedAmt Added by Saumya for IMGC BRD **/
                Decimal AppProcessingfees = ((updatedApprovedAmt * la.Processing_Fee_Percentage__c)/100);//.round(System.RoundingMode.HALF_UP);
                /** Replaced by updatedApprovedAmt Added by Saumya for IMGC BRD **/
                system.debug('App Processing fees ' + AppProcessingfees);
                TotalPF = AppProcessingfees;/** Addeed by Saumya For IMGC BRD ***/
                /** Addeed by Saumya For IMGC BRD ***/
                Decimal TotalPFA;
                if(la.Insurance_Loan_Created__c || la.Funded_By_Customer__c){
                TotalPFA = (AppProcessingfees * 1.18).round(System.RoundingMode.HALF_UP);
                }
                else{
                TotalPFA = (AppProcessingfees * 1.18).round(System.RoundingMode.HALF_UP) + (la.IMGC_Premium_Fees_GST_Inclusive1__c != null ? la.IMGC_Premium_Fees_GST_Inclusive1__c: 0);
                }
                system.debug('Total PF Amount ' + TotalPFA );
                updatedPF = TotalPFA;
                system.debug('updatedPF '+ updatedPF);
                
                if(la.Insurance_Loan_Created__c || la.Funded_By_Customer__c){
                TotalPFCharges = (TotalPF /updatedApprovedAmt )*100; 
                }
                else{
                TotalPFCharges = (TotalPF + (la.IMGC_Premium_fee__c != null ? la.IMGC_Premium_fee__c: 0))/(updatedApprovedAmt+ (la.IMGC_Premium_fee__c != null ? la.IMGC_Premium_fee__c: 0))*100; 
                }
                /** Addeed by Saumya For IMGC BRD ***/
            }
            
            /*********End of Patch added by Chitransh for TIL-1491*******/
            
           /**********************Commented by Chitransh for TIL-1491************/
            // Added by Vaishali for BRE
            /*if(la.Processing_Fee_Percentage__c != null && updatedApprovedAmt != null) {
            //End of the patch-- Added by Vaishali
                
                updatedPF = (updatedApprovedAmt * la.Processing_Fee_Percentage__c * 1.18)/100;
            // Added by Vaishali for BRE    
            }*/ 
            
            //End of the patch-- Added by Vaishali
            //End of the patch-- Added by Vaishali				  
            SanctionLetterWrapper Wrapper = new SanctionLetterWrapper();
            Wrapper.Doc_ID = lstdocMaster != null && lstdocMaster[0].Doc_Id__c != null ? lstdocMaster[0].Doc_Id__c : '';
			Wrapper.Offer_Letter = 'Sanction Letter'; // Added by KK for TIL-1421 [30-09-2019]
            //Wrapper.Offer_Letter = laCo.Sub_Stage__c != null && laCo.Sub_Stage__c == 'Document Approval' ? 'Sanction Letter' : 'Offer letter'; // Commented by KK for TIL-1421 [30-09-2019]
           // Wrapper.Ref_No = la.Name; 
            Wrapper.Ref_No = la.loan_number__c != null ? la.loan_number__c : ''; 
            //Wrapper.Ref_No = 'LA-00000270';
            //Wrapper.Ref_No = '1234';
            //Wrapper.Date1 = la.Business_Date_Created__c != null ? string.valueOf(la.Business_Date_Created__c) : '';
            Datetime dateApproved ;
            List<loan_application__history> lstHistory = [select field, newvalue, oldvalue, createddate from loan_application__history where parentid=:loanApp and field='sub_stage__c' order by createddate desc];
            for ( loan_application__history obj : lstHistory) {
                system.debug('@@obj');
                if (obj.newvalue=='Customer Negotiation' && string.valueof(obj.oldvalue).containsignorecase('credit')) {
                    system.debug('in if date approved@@');
                    dateApproved = obj.createddate;    
                    break;
                }
            }
            system.debug('@@dateApproved' + dateApproved);
            
            //Updating dateApproved as per updated logic
            
            List<LMS_Date_Sync__c> lstApprovedBusDate;
            if ( dateApproved != null) {
                lstApprovedBusDate = [SELECT Id, LastModifiedDate, createdDate, Current_Date__c FROM LMS_Date_Sync__c where createdDate < :dateApproved order by LastModifiedDate desc LIMIT 1];
            }
            DateTime dateToday = lstApprovedBusDate != null ? lstApprovedBusDate[0].Current_Date__c : lstLMS[0].Current_Date__c ;
           
            string str = dateToday.format('yyyy-mm-dd');
            system.debug(dateToday);
            system.debug(String.valueOf(dateToday).split(' ')[0]);
            //Commented for Production LA-79 and 88
            Wrapper.Date1 = String.valueOf(dateToday).split(' ')[0];
            //Wrapper.Date1 = '';
            
            if(la.Loan_Contacts__r.size()>0){
 
                Wrapper.Applicants_Name = la.Loan_Contacts__r[0].Customer__r.name != null ? la.Loan_Contacts__r[0].Customer__r.name : '';
                Wrapper.Mobile_number = la.Loan_Contacts__r[0].Phone_Number__c != null ? la.Loan_Contacts__r[0].Phone_Number__c : '';
                Wrapper.Email_ID = la.Loan_Contacts__r[0].Email__c != null ? la.Loan_Contacts__r[0].Email__c : '';
                Wrapper.Name_Appl = la.Loan_Contacts__r[0].Customer__r.name !=null ? la.Loan_Contacts__r[0].Customer__r.name : '';
                //wrapper.Foreclosure_Chgs = la.Loan_Contacts__r[0].Borrower__c == '1' ? '0' : '2';
            }
            else{
                Wrapper.Applicants_Name = '';
                Wrapper.Mobile_number = '';
                Wrapper.Email_ID = '';
                Wrapper.Name_Appl = '';
                //wrapper.Foreclosure_Chgs = ''  ;          
            }
            list<String> lstCoAppl = new List<String>();
            if(laCo.Loan_Contacts__r.size()>0){
                
                for (Loan_Contact__c objlc : laCo.Loan_Contacts__r){
                     lstCoAppl.add(objlc.Customer__r.name);
                }
                
            }
            
            wrapper.Name_CoAppl= !lstCoAppl.isEmpty() ? lstCoAppl[0]:'';
            if(ad !=null){ 
            Wrapper.Address1 = ad.Address_Line_1__c !=null ? ad.Address_Line_1__c : '';
            Wrapper.Address2 = ad.Address_Line_2__c != null ? ad.Address_Line_2__c : '';
            if(ad.CityLP__c != null)
            { 
            Wrapper.City = ad.CityLP__c;
            }
            else
            {
                Wrapper.City = '';
            }
            if(ad.StateLP__c != null)
            {
                
                Wrapper.State = ad.StateLP__c;
            }
            else
            {
                Wrapper.State = '';
            }
            Wrapper.Pincode = ad.Pincode_LP__r.name != null ?  ad.Pincode_LP__r.name : '';
            }
            else{
            Wrapper.Address1 = '';
            Wrapper.Address2 = ''; 
            Wrapper.State = '';
            Wrapper.Pincode = '';           
            }
            Wrapper.Product = la.Product_Name__c != null ? la.Product_Name__c : '';
            
            //Schema.DescribeFieldResult fieldResult = Loan_Application__c.Loan_Purpose__c.getDescribe();
            Schema.DescribeFieldResult fieldResult = Loan_Application__c.Transaction_type__c.getDescribe();/*Added by Ankit for Loan_Type change on Sanction Letter 30-11-2020 */
               List<Schema.PicklistEntry> lstPicklist = fieldResult.getPicklistValues();
               String strPLValue;     
               for( Schema.PicklistEntry f : lstPicklist) {
                  //if (f.getValue() == la.Loan_Purpose__c) { /*Added by Ankit for Loan_Type change on Sanction Letter 30-11-2020 */
                  if (f.getValue() == la.Transaction_type__c) { /*Added by Ankit for Loan_Type change on Sanction Letter 30-11-2020 */
                      strPLValue = f.getLabel();
                  }
                }
            Wrapper.Loan_Type = strPLValue != null ? strPLValue : '';
            
            NumberToWord objN = new NumberToWord ();
            // Updated total loan amount
            system.debug('@@Approved_Loan_Amount__c' + la.Approved_Loan_Amount__c);
            
            //Decimal decTotal = la.Approved_Loan_Amount__c != null && la.Approved_cross_sell_amount__c != null ? la.Approved_Loan_Amount__c + la.Approved_cross_sell_amount__c : la.Approved_Loan_Amount__c;
            //Decimal decTotal = updatedApprovedAmt != null ? updatedApprovedAmt : 0;
            Decimal decTotal = updatedApprovedAmtwithIMGCFee != null ? updatedApprovedAmtwithIMGCFee : 0;/** Replaced by updatedApprovedAmt Added by Saumya for IMGC BRD **/    
			
            system.debug('@@decTotal ' + decTotal );
            
            Wrapper.Total_LoanAmt = decTotal != null ? string.valueOf(decTotal) :'';
			if(la.Insurance_Loan_Created__c){ //Added by saumya for Insurance Loan
            Wrapper.Total_LoanAmt_words = decTotal != null ? objN.convert(Integer.valueOf(decTotal)).replaceAll('  ',' ') + ' only' +' *exclusive of insurance amount': '';
            }
            else{
            Wrapper.Total_LoanAmt_words = decTotal != null ? objN.convert(Integer.valueOf(decTotal)).replaceAll('  ',' ') + ' only' +' *inclusive of insurance amount': ''; //Added by saumya for Insurance Loan
            }
			/** Added and Replaced by Saumya for IMGC BRD **/   
            
            decimal insuraneAmt = la.Approved_cross_sell_amount__c != null ? la.Approved_cross_sell_amount__c + (la.IMGC_Premium_Fees_GST_Inclusive1__c != null ? la.IMGC_Premium_Fees_GST_Inclusive1__c: 0) : 0; 
            //Wrapper.InsuranceAmt = la.Approved_cross_sell_amount__c != null ? string.valueOf(la.Approved_cross_sell_amount__c) : ''; 
            Wrapper.InsuranceAmt = (insuraneAmt != null && insuraneAmt != 0)? string.valueOf(insuraneAmt) : ''; 
             /** Added and Replaced by Saumya for IMGC BRD **/  
            if(la.IMGC_Premium_Fees_GST_Inclusive1__c != null && la.IMGC_Premium_Fees_GST_Inclusive1__c != 0 ){
            Wrapper.InsuranceAmt_words = (insuraneAmt != null && insuraneAmt != 0)? objN.convert(Integer.valueOf(insuraneAmt)).replaceAll('  ',' ') + ' only)' + '(Including Additional Fee of ' + string.valueOf(la.IMGC_Premium_Fees_GST_Inclusive1__c) + ' /-': '';
            }
			else{
            Wrapper.InsuranceAmt_words = (insuraneAmt != null && insuraneAmt != 0)? objN.convert(Integer.valueOf(insuraneAmt)).replaceAll('  ',' ') + ' only' : '';
			}
             /** Added and Replaced by Saumya for IMGC BRD **/  
            //Wrapper.InsuranceAmt_words = la.Approved_cross_sell_amount__c != null ? objN.convert(Integer.valueOf(la.Approved_cross_sell_amount__c)).replaceAll('  ',' ') + ' only' : '';
            /** Added and Replaced by Saumya for IMGC BRD **/            
            //Wrapper.InsuranceAmt_words = (insuraneAmt != null && insuraneAmt != 0)? objN.convert(Integer.valueOf(insuraneAmt)).replaceAll('  ',' ') + ' only' : '';	  
            Wrapper.Tenure = la.Approved_Loan_Tenure__c !=null ? string.valueOf(la.Approved_Loan_Tenure__c) : '';
			/**** Added by Saumya For TIL-00001688 ****/
            String interesttype = '';
            if(la.Interest_Type__c != null){
                if(la.Interest_Type__c == 'Fixed'){
                    interesttype= la.Interest_Type__c;
                }
                else if(la.Interest_Type__c == 'Floating'){
                    if(la.Fixed_For_Months__c != null && la.Fixed_For_Months__c != 0){
                      interesttype= System.label.Floating + ' '+ la.Fixed_For_Months__c + ' '+ System.label.Floating_Interest_Rate_Type_2;  
                    }
                    else{
                      interesttype=  la.Interest_Type__c;
                    }
                }
            }
            //Wrapper.Interest_Type = la.Interest_Type__c != null ? la.Interest_Type__c != '' : '';
            Wrapper.Interest_Type = interesttype;
            /**** Added by Saumya For TIL-00001688 ****/
            Wrapper.Interest_Rate = la.Approved_ROI__c != null ? string.valueOf(la.Approved_ROI__c) : '';
            Wrapper.Spread = la.Spread__c != null ? string.valueOf(la.Spread__c) : '';
			/**** Added by Saumya For TIL-00001688 ****/
            //Wrapper.HHFL_RefRate = '8.3';
            Wrapper.HHFL_RefRate = la.Scheme__r.Reference_Rate__c != null ? string.valueOf(la.Scheme__r.Reference_Rate__c) : '';
            /**** Added by Saumya For TIL-00001688 ****/										
            Wrapper.EMI_Amount = la.approved_emi__c != null ?  String.valueOf(la.approved_emi__c ) : '';        
            Wrapper.EMI_Amount_words = la.approved_emi__c != null ? objN.convert(Integer.valueOf(la.approved_emi__c)).replaceAll('  ',' ') + ' only' : '';
			if(la.Insurance_Loan_Created__c){// Added by Saumya For Insurance Loan
            Wrapper.EMI_Amount_words += ' **Exclusive of Insurance EMI';
            }
            else{
            //Wrapper.EMI_Amount_words += laCo.Sub_Stage__c != null && laCo.Sub_Stage__c == 'Document Approval' ? '**Inclusive of Insurance EMI' : ' **Exclusive of Insurance EMI';
            Wrapper.EMI_Amount_words += '**Inclusive of Insurance EMI';// Added by Saumya for TIL-00000802
            }
            Wrapper.EMI_DueDt = !LRNList.isEmpty() && LRNList[0].DueDay__c != null ? String.valueOf(LRNList[0].DueDay__c) : ''; //Added By Abhishek for TIL 2417
            //Wrapper.Processing_Fees = la.Approved_Processing_Fees1__c != null ? string.valueOf(la.Approved_Processing_Fees1__c) : '';
            //Updated for production as per Priyanka TIL-
            Wrapper.Processing_Fees = updatedPF != null ? string.valueOf(updatedPF) : '';
            
            Wrapper.Initial_Money_Deposit = decIMD!= null ? string.valueOf(decIMD) : '';
            //Wrapper.Processing_Charges = la.Processing_Fee_Percentage__c != null ? string.valueof(la.Processing_Fee_Percentage__c) : '';
            /** Added and Replaced by Saumya for IMGC BRD **/
            Wrapper.Processing_Charges = TotalPFCharges != null ? string.valueof(TotalPFCharges) : '';
            /** Added and Replaced by Saumya for IMGC BRD **/
            //Wrapper.Processing_Fees = lstPF.isEmpty() && lstPF[0].Processing_Fees__c != null ? String.valueOf(lstPF[0].Processing_Fees__c) : '';
            //Wrapper.Processing_Charges = la.Product_Name__c == 'Home Loan' || la.Product_Name__c == 'Loan Against Property' ? '1' : '2';
            
            //Decimal objDecB = la.Approved_Processing_Fees1__c - decIMD;
            //Updated for production as per Priyanka TIL-
            Decimal objDecB = updatedPF  != null ? updatedPF  - decIMD : 0.00;
            Wrapper.Bal_ProcessingFee = objDecB != null ? string.valueOf(objDecB):'';
            system.debug('@lstProp' + lstProp);
            if(!lstProp.isEmpty() && lstProp[0].Address_Line_1__c != null) {
                //Below if and else blocks added by Abhilekh on 16th May 2019 for TIL-781
                if(lstProp[0].Address_Line_2__c == null){
                    Wrapper.Security_Address = lstProp[0].Flat_No_House_No__c +  ','  + lstProp[0].Floor__c + ','+ lstProp[0].Builder_Name_Society_Name__c + ',' + lstProp[0].Address_Line_1__c + ',' + lstProp[0].City_LP__c+ ',' +lstProp[0].State_LP__c+ ',' +  lstProp[0].Pincode_LP__r.Name;
                }
                else{
                    Wrapper.Security_Address = lstProp[0].Flat_No_House_No__c +  ','  + lstProp[0].Floor__c + ','+ lstProp[0].Builder_Name_Society_Name__c + ',' + lstProp[0].Address_Line_1__c + ',' + lstProp[0].Address_Line_2__c + ',' + lstProp[0].City_LP__c+ ',' +lstProp[0].State_LP__c+ ',' +  lstProp[0].Pincode_LP__r.Name;
                }
            } else {
                Wrapper.Security_Address = '';
            }
            Wrapper.Foreclosure_Chgs = la.Approved_Foreclosure_Charges__c != null ? string.valueOf(la.Approved_Foreclosure_Charges__c) : '';
            Wrapper.Validity_SancLeter = laCo.Sub_Stage__c != null && laCo.Sub_Stage__c == 'Document Approval' ? '30' : '30' ;//validity of sanction letter master 
            //wrapper.CoBorrower_Name = new List<CoBorrower_Name>();
            
            List<Sanc_Condts> lstSanc = new List<Sanc_Condts>();
            Sanc_Condts objSc;
            for (Loan_Sanction_Condition__c objLc : lstLoanSac) {
                if( objLc.Sanction_Condition_Master__c != null && objLc.Sanction_Condition_Master__r.Sanction_Condition__c != null) {
                    if ( (Wrapper.Offer_Letter == 'Offer letter' && objLc.Status__c == 'Requested') ||
                         (Wrapper.Offer_Letter == 'Sanction Letter' && (objLc.Status__c == 'Requested' || objLc.Status__c == 'Completed' || objLc.Status__c == 'Verified' || objLc.Status__c == 'Waiver Requested') ) //Edited by Chitransh for LA-1157 on [01-01-2019]    
                        ) {
                        objSc = new Sanc_Condts();
                        if (objLc.Sanction_Condition_Master__r.Sanction_Condition__c != 'Others') {
                            objSc.Cond = objLc.Sanction_Condition_Master__r.Sanction_Condition__c;
                        } else {
                            objSc.Cond = objLc.Sanction_Condition__c;
                        }
                        if ( objSc.Cond == null)
                            objSc.Cond = ''; 
                        lstSanc.add(objSc);
                    }
                }
            }
			//Added by Saumya on 25th August 2020 FOR IMGC BRD
            if((la.IMGC_Decision__c == 'Approved' || la.IMGC_Decision__c == 'Pending') && la.IMGC_Premium_Fees__c != 0 && la.IMGC_Premium_Fees__c != null){
                Sanc_Condts objSC1 = new Sanc_Condts();
                objSC1.Cond = Constants.IMGCSC1;
                
                Sanc_Condts objSC2 = new Sanc_Condts();
                objSC2.Cond = Constants.IMGCSC2;
                
                lstSanc.add(objSC1);
                lstSanc.add(objSC2);
            }
			
			/*Added by Ankit for Static Condition*/
            Sanc_Condts objSCSPL = new Sanc_Condts();
            objSCSPL.Cond = Constants.SPECIALSANCTCON;
            lstSanc.add(objSCSPL);
            /*Added by Ankit for Static Condition*/
			
            wrapper.Sanc_Condts = lstSanc;
            
            List<CoBorrower_Name> lstCob = new List<CoBorrower_Name>();
            CoBorrower_Name objCob;
            for (String strCoAp : lstCoAppl) {
                objCob = new CoBorrower_Name();
                objCob.CBN = strCoAp;
                lstCob.add(objCob);
            }
            wrapper.CoBorrower_Name = lstCob;
            
            wrapper.Placeholder_for_no_of_Signature_mutiple_applicants='';
            
            List_of_Charges__c objLc = List_of_Charges__c.getInstance();
            
            wrapper.Cheque_NACH_bounce = objLc.Cheque_dishonour_charges_rejection_of_EC__c != null ? objLc.Cheque_dishonour_charges_rejection_of_EC__c : '';
            wrapper.Default_Penal_interest = objLc.Default_Penal_Interest__c != null ? objLc.Default_Penal_Interest__c : '';
            
            wrapper.Authority_Designation ='';
            List<InstallDetails> lstInstDetails = new List<InstallDetails>();
            if ( !lstLoanRepay.isEmpty()) {
                wrapper.InstallmentPlan = lstLoanRepay[0].Installment_Plan__c;
                
                InstallDetails objInstDetails ;
                if ( lstLoanRepay[0].Installment_Plan__c == 'Graded Instl') {
                    for (  Graded_Details__c objGrade : lstLoanRepay[0].Graded_Details__r) {
                        objInstDetails = new InstallDetails();
                        objInstDetails.EMI_Amount = objGrade.Slab_and_EMI_EMI__c != null ? String.valueOf(objGrade.Slab_and_EMI_EMI__c) : '';
                        objInstDetails.Installment_From = objGrade.Slab_and_EMI_Instl_From__c != null ? String.valueOf(objGrade.Slab_and_EMI_Instl_From__c) : '';
                        objInstDetails.Installment_To = objGrade.Slab_and_EMI_Instl_To__c != null ? String.valueOf(objGrade.Slab_and_EMI_Instl_To__c) : '';
                        objInstDetails.EMI_Amount_Inwords = objGrade.Slab_and_EMI_EMI__c != null ? objN.convert(Integer.valueOf(objGrade.Slab_and_EMI_EMI__c)).replaceAll('  ',' ') + ' only' : '';
                        lstInstDetails.add(objInstDetails);
                    }
                }
                
            } else {
                wrapper.InstallmentPlan = '';
                
            }
            wrapper.Installment_details = lstInstDetails;
            Grievance_Contact__c objGriev = Grievance_Contact__c.getInstance();
            Wrapper.CustomerCare_Email = objGriev.CustomerCare_Email__c != null ? objGriev.CustomerCare_Email__c : ''; 
            Wrapper.CustomerCare_PhNo = objGriev.CustomerCare_PhNo__c != null ? objGriev.CustomerCare_PhNo__c : ''; 
            wrapper.website = objGriev.website__c != null ? objGriev.website__c : ''; 
            
            String jsonstring = JSON.serialize(Wrapper);
            system.debug('@@@ jsonstring '+jsonstring);     
            jsonstring = jsonstring.replace('\"Date1\":\"','\"Date\":\"');
            jsonstring = jsonstring.replace('&','&amp;');
			/*jsonstring = jsonstring.replace('/','&amp;'); // [27-5-2019] Added by KK for TIL-797
            jsonstring = jsonstring.replace('-','&amp;'); // [27-5-2019] Added by KK for TIL-797 			
			jsonstring = jsonstring.replace('*','&amp;');//[09-10-2019] Added by Chitransh for TIL-1446
            jsonstring = jsonstring.replace('(','&amp;');//[09-10-2019] Added by Chitransh for TIL-1446
            jsonstring = jsonstring.replace(')','&amp;');//[09-10-2019] Added by Chitransh for TIL-1446*/ /* Commented after approval from GK on 14-10-19 */					   
            system.debug('@@@ jsonstring after replace'+jsonstring); 
            
            Http p=new Http();
            
            Rest_Service__mdt rs = [Select Client_Username__c, Client_Password__c, Service_EndPoint__c from Rest_Service__mdt where DeveloperName = 'SanctionLetter_MITC'];
            
            //String endpoint='https://mocksvc.mulesoft.com/mocks/1c472452-0fb4-498b-a66b-81d54327f7b7/sanctionLetter-Mitc';
            String endpoint= rs.Service_EndPoint__c;
            HttpRequest request =new HttpRequest();
            String username = rs.Client_Username__c;
            String password = rs.Client_Password__c; 
            Blob headerValue = Blob.valueOf(rs.Client_Username__c + ':' + rs.Client_Password__c);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        
            request.setHeader('Content-Type','application/json');
            request.setHeader('Username',rs.Client_Username__c);  
            request.setHeader('Password',rs.Client_Password__c);
            request.setHeader('accept','application/json');
            request.setHeader('Authorization', authorizationHeader);
            request.setHeader('Doc_Name','SanctionLetter');
            request.setEndPoint(endpoint);
            request.setMethod('POST');
            request.setBody(jsonstring); //serialize the value
            request.setTimeOut(50000);
            HttpResponse response =p.send(request);
            //Create integration logs
            LogUtility.createIntegrationLogs(jsonstring,response,endpoint);
            system.debug('@@@ Response '+response);
            system.debug('@@@ Response '+response.getBody());
            
            if (response.getStatusCode() == 200) {
                
                system.debug('@@@ response '+response.getBody());
                deserializeResponse deresp=(deserializeResponse)System.JSON.deserialize(response.getBody(),deserializeResponse.class);
                    
                    Blob enco = EncodingUtil.base64Decode(deresp.Result);
                    //blob enco = EncodingUtil.base64Decode('JVBERi0xLjcKJeLjz9MKNyAwIG9iago8PCAvVHlwZSAvUGFnZSAvUGFyZW50IDEgMCBSIC9MYXN0TW9kaWZpZWQgKEQ6MjAxODA4MjAwMDM4MDkrMDUnMzAnKSAvUmVzb3VyY2VzIDIgMCBSIC9NZWRpYUJveCBbMC4wMDAwMDAgMC4wMDAwMDAgNTk1LjI3NjAwMCA4NDEuODkwMDAwXSAvQ3JvcEJveCBbMC4wMDAwMDAgMC4wMDAwMDAgNTk1LjI3NjAwMCA4NDEuODkwMDAwXSAvQmxlZWRCb3ggWzAuMDAwMDAwIDAuMDAwMDAwIDU5NS4yNzYwMDAgODQxLjg5MDAwMF0gL1RyaW1Cb3ggWzAuMDAwMDAwIDAuMDAwMDAwIDU5NS4yNzYwMDAgODQxLjg5MDAwMF0gL0FydEJveCBbMC4wMDAwMDAgMC4wMDAwMDAgNTk1LjI3NjAwMCA4NDEu');
                    Attachment attach = new Attachment();
                    attach.contentType = 'application/pdf';
                    attach.name = 'SanctionLetter_'+la.loan_number__c+'.pdf';
                    attach.parentId = loanApp;
                    attach.body = enco; 
                    insert attach;
                boolSuccess = 'true';       
                    
                    //la.Sanction_Date__c = date.newinstance(dateToday.year(), dateToday.month(), dateToday.day()); //Commented by Abhilekh on 25th April 2019
                    la.Repayment_Generated_for_Documents__c = false;
                    update la;
                    
            } else {
                la.Repayment_Generated_for_Documents__c = false;
                update la;
                boolSuccess = 'false'; 
            }
        }    
       // }catch(Exception e) {
       //     boolSuccess = 'false';
       // }  
        
        system.debug('@@bool' + boolSuccess);
        return boolSuccess;  
    }
    
    @AuraEnabled
    public static void generateRepayment(String idLoanApp){
        system.debug('@@@in generate Repayment sanction');
        Utility.generateUpdatedRepayment(idLoanApp);
    }
    
public class deserializeResponse {
    public String Result;
}
public class Sanc_Condts {
      public String Cond;
    }

    public class CoBorrower_Name {
      public String CBN;
    }  
    
    public class InstallDetails {
        public String Installment_From;
        public String Installment_To;
        public String EMI_Amount;
        public String EMI_Amount_Inwords;
    }
    
public class SanctionLetterWrapper {
    public String Doc_ID;
   public String Offer_Letter;
   public String Ref_No;
   public String Date1;            //Date is Reserved
   public String Applicants_Name;
   public String Mobile_number;
   public String Email_ID;
   public String Address1;
   public String Address2;
   public String City;
   public String State;
   public String Pincode;
   public String Product;
   public String Loan_Type;

   public String Total_LoanAmt;
   public String Total_LoanAmt_words;
   public String InsuranceAmt;
   public String InsuranceAmt_words;
   public String Tenure;
   public String Interest_Type;
   public String Interest_Rate;
   public String Spread;
   public String HHFL_RefRate;
   public String EMI_Amount;
   public String EMI_Amount_words;
   public String EMI_DueDt;
   public String Processing_Charges;
   public String Initial_Money_Deposit;
   public String Processing_Fees;
   public String Bal_ProcessingFee;
   public String Security_Address;
   public String Foreclosure_Chgs;
   public String Validity_SancLeter;
   public List<Sanc_Condts> Sanc_Condts;
   public List<CoBorrower_Name> CoBorrower_Name;
   //public String CoBorrower_Name;
   public String Name_Appl;
   public String Name_CoAppl;
   
    public String Placeholder_for_no_of_Signature_mutiple_applicants;
    public String Cheque_NACH_bounce;
    public String Default_Penal_interest ;
    public String Authority_Designation;
    public String InstallmentPlan;
    public List<InstallDetails> Installment_details;
    public String website;
    public String CustomerCare_PhNo;
    public String CustomerCare_Email;
}


  
     

    
}