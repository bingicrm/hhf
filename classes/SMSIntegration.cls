/*****************************************************
Class Name    : SMSIntegration
Author        : Sonali Kansal
Description   : This class is used to integrate saleforce to ACL SMS Mobile API.
Created Date  : 28/06/2018
Related Component : 
*********************************************************/
public with sharing class SMSIntegration {

@Future(callout = true)
   public static void SMSinit(String mobileNumber, string body){
      {
       String startUrl,midUrl,endUrl,finalURL, msg;
        
       Rest_Service__mdt serviceDetails=[select JSONBody__c,Client_ID__c,Client_Password__c,Client_Username__c,JSONBody2__c,
       Service_End_Point_SMS__c, Service_EndPoint__c from Rest_Service__mdt where MasterLabel='SendUserSMS'];
       system.debug('==serviceDetails=='+serviceDetails);
        if(serviceDetails!=null){
          startUrl=serviceDetails.JSONBody__c;
          String reg = serviceDetails.Client_ID__c;
          system.debug('==reg=='+reg);
          msg=String.valueOf(Math.abs(Crypto.getRandomInteger()));
          string user = serviceDetails.Client_Password__c;
          string sender = serviceDetails.Client_Username__c;
          startUrl=startUrl.replace('{appId}',serviceDetails.Client_ID__c);
          startUrl=startUrl.replace('{userPass}',serviceDetails.Client_Password__c);
          startUrl=startUrl.replace('{sender}',serviceDetails.Client_Username__c);
          startUrl=startUrl.replace('{msgId}',msg);
          //serviceDetails.Service_End_Point_SMS__c=serviceDetails.Service_End_Point_SMS__c.replace('<appId>',serviceDetails.Client_ID__c);
          //serviceDetails.Service_End_Point_SMS__c=serviceDetails.Service_End_Point_SMS__c.replace('<userId>',serviceDetails.Client_ID__c);
          //serviceDetails.Service_End_Point_SMS__c=serviceDetails.Service_End_Point_SMS__c.replace('<pass>',serviceDetails.Client_Password__c);
          //serviceDetails.Service_End_Point_SMS__c=serviceDetails.Service_End_Point_SMS__c.replace('<from>',serviceDetails.Client_Username__c);
          //serviceDetails.Service_End_Point_SMS__c=serviceDetails.Service_End_Point_SMS__c.replace('<<configuredtext>>','Request you to Check your Email for <var> confirmation and Signatures');
          //serviceDetails.Service_End_Point_SMS__c=serviceDetails.Service_End_Point_SMS__c.replace('<to>','91'+mobileNumber);
          system.debug('RANDOM:'+msg);
          midUrl = generateBody(mobileNumber, body);          
          endUrl =serviceDetails.JSONBody2__c;
          //System.debug('startUrl'+startUrl);
          //System.debug('midUrl'+midUrl);
          //System.debug('endUrl'+endUrl);
          finalURL = startUrl+midUrl+endUrl;
          System.debug('finalURL:'+finalURL);
          //system.debug('end_point:'+serviceDetails.Service_End_Point_SMS__c);
          //sendSMS(serviceDetails.Service_End_Point_SMS__c , finalURL);
          sendSMS(serviceDetails.Service_EndPoint__c , finalURL);
          
        } 
      
        
      }
       
    }
    
   public static void sendSMS(String endPointURL,String body){
      try{
        if(endPointURL!=null && body!=null){
            Http http = new HTTP();
            HttpRequest request = new HttpRequest();
            HttpResponse response;
            request.setEndpoint(endPointURL+EncodingUtil.urlEncode(body,'UTF-8'));
            //request.setEndpoint(endPointURL);
            request.setMethod('GET');
            request.setTimeout(10000);
            request.setHeader('Content-Type','text/html');
            
            //HttpResponse response = http.send(request);
            try{
                response = http.send(request);
                //Create integration logs
                //LogUtility.createIntegrationLogs(body,response,endPointURL+EncodingUtil.urlEncode(body,'UTF-8')); // Commented by KK on 30-07
                LogUtility.createIntegrationLogs(body,response,endPointURL); // Added by KK on 30-07 (removed encoding)
            }
            catch(Exception ex)
            {
                LogUtility.createLogs('Error', 'Error', 'Apex Class', 'Class Name : SMSIntegration', ex.getStackTraceString(), ex.getMessage(), '', true);
            }
            System.debug('Request => ' + request);
            System.debug('Response => ' + response);
            //System.debug('Response => ' + response.getBody());
        }
     }catch(Exception e){
       System.debug('Error'+e);
     }
  }
    
    public static String generateBody(String mobileNumber, String smsBody){
        try{
          Integer count=6;
         
          //String message ='We are happy to share with you the In-principal approval details subjected to further evalution of your application 123. Hero Housing Finance Ltd';
         // String message = 'Your OTP for Hero Fincorp loan application is' + OTP;
          String body ='';
          
          body+='<detail msisdn="91'+mobileNumber+'" id="'+ (count++) +'" msg="'+smsBody+'"/>';
          //body+='<detail msisdn='91''+mobileNumber+' id='+ (count++) +' msg='+message+'/>';
          System.debug('SMS Body Map=> '+body);
          return body;
        }catch(Exception e){
          System.debug('Error'+e);
        }  
        return null;
      }


}