@isTest
public class TestUCICNegativeAPICallback {
    public static testMethod void method1()
    {
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
        insert acc;
        
        
        //Contact con = new Contact(LastName = 'Test Contact', AccountId = acc.Id);
        //insert con;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        acc1 =[Select Id, PAN__c, Name, Date_of_Incorporation__c, Phone, RecordTypeId, Customer_ID__c From Account where Id =: acc1.Id];
        Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
        insert con1;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Credit_Deviation_Master__c credDevMasObj = new Credit_Deviation_Master__c(UCIC_DB_Name__c='UCIC Dedupe Check');
        insert credDevMasObj;
         
        Loan_Application__c la = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11', Loan_Number__c ='12345');
        insert la;
         Loan_Contact__c lc = [SELECT Id, Name,Applicant_Type__c, Applicant_Status__c, Constitution__c, Customer_segment__c, Loan_Applications__c FROM Loan_Contact__c WHERE Loan_Applications__c =: la.Id];
        //lc.Applicant_Type__c='Co- Applicant';
        lc.Borrower__c='2';
        lc.Contact__c = con1.Id;
        lc.Customer__c = acc1.Id;
        lc.Constitution__c='18';
        lc.Applicant_Status__c='1';
        lc.Customer_segment__c='9';
        //lc.Income_Program_Type__c = 'Normal Salaried';
        //lc.Gender__c = 'M';
        //lc.Category__c ='1';
        //lc.Religion__c = '1';
        lc.Mobile__c='9876543210';
        lc.Email__c='testemail@gmail.com';
        //lc.Customer__c = acc.id;    
        update lc;
        Id RecordTypeIdCustUCIC = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('UCIC').getRecordTypeId();
        Customer_Integration__c objcust = new Customer_Integration__c();
			objcust.UCIC_Negative_API_Response__c = true;
			objcust.UCIC_Negative_API_Response__c = false;
			objcust.Loan_Application__c = la.Id;
			objcust.Loan_Contact__c = lc.Id;
			objcust.RecordTypeId = RecordTypeIdCustUCIC;
			insert objcust;
                    List<UCIC_Negative_Database__mdt> UCICdblst = [Select Id, Priority__c, Email__c, Approval_Required__c, Deviation__c, Reject_Data_Match_Count__c, Rejection_Reason__c, Severity_Level__c, UCIC_Database_Name__c,Maximum_Match_Percentage__c from UCIC_Negative_Database__mdt where Active__c = true];
		/*UCIC_Negative_Database__mdt objUCICmdt = new UCIC_Negative_Database__mdt();
        objUCICmdt.Priority__c = 1;
        objUCICmdt.Email__c = true;
        objUCICmdt.Approval_Required__c = true;
        objUCICmdt.Deviation__c = true;
        objUCICmdt.UCIC_Database_Name__c = 'A';
        objUCICmdt.Maximum_Match_Percentage__c = 50;
        insert objUCICmdt;*/
        test.startTest();
			UCICNegativeAPICallback.UCICNegativeCallback ucic = new UCICNegativeAPICallback.UCICNegativeCallback();
        	ucic.Source_System = 'SFDC-HL';
            ucic.Application_id = la.Loan_Number__c;
            ucic.URL = 'Test URL';
            UCICNegativeAPICallback.MatchResponse objMatchResponse= new UCICNegativeAPICallback.MatchResponse();
            objMatchResponse.Customer_id=acc1.Customer_ID__c;
            objMatchResponse.UCICID='12345';
        	UCICNegativeAPICallback.UCICDatabases objUCICDB = new UCICNegativeAPICallback.UCICDatabases();
        	objUCICDB.Name ='UCIC Dedupe Check';
            objUCICDB.Match_Count ='1';
            objUCICDB.Match_Status ='Match';
            objUCICDB.NoOfMatches ='1';
            objUCICDB.IsMatchedWithMultipleUCIC ='Y';
            objUCICDB.IsRejectedInLast90 ='N';
            objUCICDB.Matched_Max_Percentage ='90';
            objUCICDB.Matched_Max_Perc_Rule ='abc';
            objUCICDB.Matched_Max_Perc_UCIC ='30';
            objUCICDB.No_OF_LOANS ='2';
            objUCICDB.Bad_Loan_At_NPA_Stage ='';
            objUCICDB.BAD_LOADN_BUCKET ='';
            objUCICDB.Highest_DPD_Ever ='';
            objUCICDB.No_of_bounces_in_lifetime ='';
            objUCICDB.Repo_Flag ='';
            objUCICDB.Write_Off_Flag ='';
            objUCICDB.Filler01 ='';
            objUCICDB.Filler02 ='';
            objUCICDB.Filler03 ='';
            objUCICDB.Filler04 ='';
            objUCICDB.Filler05 ='';
            objUCICDB.Filler06 ='';
            objUCICDB.Filler07 ='';
            objUCICDB.Filler08 ='';
            objUCICDB.Filler09 ='';
            objUCICDB.Filler10 ='';
            objUCICDB.Filler11 ='';
            objUCICDB.Filler12 ='';
            objUCICDB.Filler13 =''; 
        	List<UCICNegativeAPICallback.UCICDatabases> lstUCICDB = new List<UCICNegativeAPICallback.UCICDatabases>();
            lstUCICDB.add(objUCICDB);
        	objMatchResponse.UCICDatabases = lstUCICDB;
            List<UCICNegativeAPICallback.MatchResponse> lstMatchResponse= new List<UCICNegativeAPICallback.MatchResponse>();
            lstMatchResponse.add(objMatchResponse);
        	ucic.MatchResponse =lstMatchResponse;
        	UCICNegativeAPICallback.createorUpdateUCICDatabases(ucic);
        test.stopTest();
    } 
    
    public static testMethod void method2()
    {
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
        insert acc;
        
        //Contact con = new Contact(LastName = 'Test Contact', AccountId = acc.Id);
        //insert con;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        acc1 =[Select Id, PAN__c, Name, Date_of_Incorporation__c, Phone, RecordTypeId, Customer_ID__c From Account where Id =: acc1.Id];
        Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
        insert con1;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
         
        Loan_Application__c la = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11', Loan_Number__c ='12345');
        insert la;
         Loan_Contact__c lc = [SELECT Id, Name,Applicant_Type__c, Applicant_Status__c, Constitution__c, Customer_segment__c, Loan_Applications__c FROM Loan_Contact__c WHERE Loan_Applications__c =: la.Id];
        //lc.Applicant_Type__c='Co- Applicant';
        lc.Borrower__c='2';
        lc.Contact__c = con1.Id;
        lc.Customer__c = acc1.Id;
        lc.Constitution__c='18';
        lc.Applicant_Status__c='1';
        lc.Customer_segment__c='9';
        //lc.Income_Program_Type__c = 'Normal Salaried';
        //lc.Gender__c = 'M';
        //lc.Category__c ='1';
        //lc.Religion__c = '1';
        lc.Mobile__c='9876543210';
        lc.Email__c='testemail@gmail.com';
        //lc.Customer__c = acc.id;    
        update lc;
        
        Applicable_Deviation__c appDevObj = new Applicable_Deviation__c(Auto_Deviation_UCIC__c=true, Loan_Application__c=la.Id);
        insert appDevObj;
        Credit_Deviation_Master__c credDevMasObj = new Credit_Deviation_Master__c(UCIC_DB_Name__c='UCIC Dedupe Check');
        insert credDevMasObj;
        
        test.startTest();
        UCIC_Database__c objUC = new UCIC_Database__c();
        objUC.Match_Count__c = 2;
        objUC.Matched_Max_Percentage__c = '60';
        objUC.Name = 'UCIC Dedupe Check';
        objUC.Loan_Application__c = la.Id;
        objUC.Customer_Detail__c = lc.Id;														
        insert objUC;
        Id RecordTypeIdCustUCIC = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('UCIC').getRecordTypeId();
        Customer_Integration__c objcust = new Customer_Integration__c();
			objcust.UCIC_Negative_API_Response__c = true;
			objcust.UCIC_Negative_API_Response__c = false;
			objcust.Loan_Application__c = la.Id;
			objcust.Loan_Contact__c = lc.Id;
			objcust.RecordTypeId = RecordTypeIdCustUCIC;
			insert objcust;
        
                    List<UCIC_Negative_Database__mdt> UCICdblst = [Select Id, Priority__c, Email__c, Approval_Required__c, Deviation__c, Reject_Data_Match_Count__c, Rejection_Reason__c, Severity_Level__c, UCIC_Database_Name__c,Maximum_Match_Percentage__c from UCIC_Negative_Database__mdt where Active__c = true];
		/*UCIC_Negative_Database__mdt objUCICmdt = new UCIC_Negative_Database__mdt();
        objUCICmdt.Priority__c = 1;
        objUCICmdt.Email__c = true;
        objUCICmdt.Approval_Required__c = true;
        objUCICmdt.Deviation__c = true;
        objUCICmdt.UCIC_Database_Name__c = 'A';
        objUCICmdt.Maximum_Match_Percentage__c = 50;
        insert objUCICmdt;*/
			UCICNegativeAPICallback.UCICNegativeCallback ucic = new UCICNegativeAPICallback.UCICNegativeCallback();
        	ucic.Source_System = 'SFDC-HL';
            ucic.Application_id = la.Loan_Number__c;
            ucic.URL = 'Test URL';
            UCICNegativeAPICallback.MatchResponse objMatchResponse= new UCICNegativeAPICallback.MatchResponse();
            system.debug('acc1.Customer_ID__c::'+ acc1.Customer_ID__c);
        	objMatchResponse.Customer_id=acc1.Customer_ID__c;
            objMatchResponse.UCICID='12345';
        	UCICNegativeAPICallback.UCICDatabases objUCICDB = new UCICNegativeAPICallback.UCICDatabases();
        	objUCICDB.Name ='UCIC Dedupe Check';
            objUCICDB.Match_Count ='4';
            objUCICDB.Match_Status ='Match';
            objUCICDB.NoOfMatches ='1';
            objUCICDB.IsMatchedWithMultipleUCIC ='Y';
            objUCICDB.IsRejectedInLast90 ='N';
            objUCICDB.Matched_Max_Percentage ='90';
            objUCICDB.Matched_Max_Perc_Rule ='abc';
            objUCICDB.Matched_Max_Perc_UCIC ='30';
            objUCICDB.No_OF_LOANS ='2';
            objUCICDB.Bad_Loan_At_NPA_Stage ='';
            objUCICDB.BAD_LOADN_BUCKET ='';
            objUCICDB.Highest_DPD_Ever ='';
            objUCICDB.No_of_bounces_in_lifetime ='';
            objUCICDB.Repo_Flag ='';
            objUCICDB.Write_Off_Flag ='';
            objUCICDB.Filler01 ='';
            objUCICDB.Filler02 ='';
            objUCICDB.Filler03 ='';
            objUCICDB.Filler04 ='';
            objUCICDB.Filler05 ='';
            objUCICDB.Filler06 ='';
            objUCICDB.Filler07 ='';
            objUCICDB.Filler08 ='';
            objUCICDB.Filler09 ='';
            objUCICDB.Filler10 ='';
            objUCICDB.Filler11 ='';
            objUCICDB.Filler12 ='';
            objUCICDB.Filler13 =''; 
        	UCICNegativeAPICallback.UCICDatabases objUCICDB1 = new UCICNegativeAPICallback.UCICDatabases();
        	objUCICDB1.Name ='NHB Wilful Defaulter';
            objUCICDB1.Match_Count ='1';
            objUCICDB1.Match_Status ='Match';
            objUCICDB1.NoOfMatches ='1';
            objUCICDB1.IsMatchedWithMultipleUCIC ='Y';
            objUCICDB1.IsRejectedInLast90 ='N';
            objUCICDB1.Matched_Max_Percentage ='30';
            objUCICDB1.Matched_Max_Perc_Rule ='abc';
            objUCICDB1.Matched_Max_Perc_UCIC ='30';
            objUCICDB1.No_OF_LOANS ='2';
            objUCICDB1.Bad_Loan_At_NPA_Stage ='';
            objUCICDB1.BAD_LOADN_BUCKET ='';
            objUCICDB1.Highest_DPD_Ever ='';
            objUCICDB1.No_of_bounces_in_lifetime ='';
            objUCICDB1.Repo_Flag ='';
            objUCICDB1.Write_Off_Flag ='';
            objUCICDB1.Filler01 ='';
            objUCICDB1.Filler02 ='';
            objUCICDB1.Filler03 ='';
            objUCICDB1.Filler04 ='';
            objUCICDB1.Filler05 ='';
            objUCICDB1.Filler06 ='';
            objUCICDB1.Filler07 ='';
            objUCICDB1.Filler08 ='';
            objUCICDB1.Filler09 ='';
            objUCICDB1.Filler10 ='';
            objUCICDB1.Filler11 ='';
            objUCICDB1.Filler12 ='';
            objUCICDB1.Filler13 =''; 
        	List<UCICNegativeAPICallback.UCICDatabases> lstUCICDB = new List<UCICNegativeAPICallback.UCICDatabases>();
            lstUCICDB.add(objUCICDB);
            lstUCICDB.add(objUCICDB1);
        	objMatchResponse.UCICDatabases = lstUCICDB;
            List<UCICNegativeAPICallback.MatchResponse> lstMatchResponse= new List<UCICNegativeAPICallback.MatchResponse>();
            lstMatchResponse.add(objMatchResponse);
        	ucic.MatchResponse =lstMatchResponse;
        	UCICNegativeAPICallback.createorUpdateUCICDatabases(ucic);
        test.stopTest();
    } 
    
}