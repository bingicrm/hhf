public class TPViewController {
	
	//Below method added by Abhilekh on 19th September 2019 for FCU BRD
    @AuraEnabled
    public static User getUserType(){
        return [SELECT Id,Name,FI__c, Legal__c, Technical__c, FCU__c, LIP__c, PD__c from User WHERE Id =: UserInfo.getUserId()];
    }
    
    //Below method added by Abhilekh on 19th September 2019 for FCU BRD
    @AuraEnabled
    public static String getProfileName(){
        return [SELECT Id,Name from Profile WHERE Id =: UserInfo.getProfileId()].Name;
    }
	
    @AuraEnabled
      public static List <Third_Party_Verification__c> getTPV() {
        User u = [Select Id, Name, FI__c, Legal__c, Technical__c, FCU__c, LIP__c, PD__c,ProfileId from User where Id =: UserInfo.getUserId()]; //ProfileId added by Abhilekh on 19th September 2019 for FCU BRD
        List<Third_Party_Verification__c> tpList = new List<Third_Party_Verification__c>();
        List<Third_Party_Verification__c> returnList = new List<Third_Party_Verification__c>();
        Id FIRecordTypeId = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get(Constants.FI).getRecordTypeId();
        Id LegalRecordTypeId = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get(Constants.LEGAL).getRecordTypeId();
        Id TechnicalRecordTypeId = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get(Constants.TECHNICAL).getRecordTypeId();
        Id FCURecordTypeId = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get(Constants.FCU).getRecordTypeId();
        Id LIPRecordTypeId = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get(Constants.LIP).getRecordTypeId();
        Id VendorRecordTypeId = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get(Constants.VENDOR_PD).getRecordTypeId();
        Id LegalVettingRecordTypeId = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('Legal Vetting').getRecordTypeId();  
		Id OthersRecordTypeId = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('Others').getRecordTypeId(); /** Added by Saumya For IMGC **/
		String profileName = [SELECT Id,Name from Profile WHERE Id =: u.ProfileId].Name; //Added by Abhilekh on 19th September 2019 for FCU BRD
          if(u.FI__c == TRUE){
              tpList = [Select Id, Name, Name_of_Applicant__c, Loan_Application_Name__c, Initiation_Date__c, Requested_Loan_Amount__c, Product_Type__c, Branch__c, Owner_Name__c, Status__c, Owner__c, Record_Type_Name__c,Loan_Application__c,Loan_Application__r.Name,Loan_Application__r.Overall_FCU_Status__c,Owner__r.Profile.Name,Assigned_Vendor__c,Assigned_Vendor__r.Name,Loan_Application__r.Re_Appealed_Case__c from Third_Party_Verification__c where RecordTypeId =: FIRecordTypeId AND (Status__c = 'Initiated' or Status__c = 'Hold' ) AND Owner__c =: UserInfo.getUserId()];//Added by saumya Initiation_Date__c and Requested_Loan_Amount__c For Third Party view Change
              returnList.addAll(tpList);
          }
          if(u.FCU__c == TRUE){
            //Below if,else if and else block added by Abhilekh on 19th September 2019 for FCU BRD
            if(profileName == 'Third Party Vendor'){
                System.debug('Inside if');
                tpList = [Select Id, Name, Name_of_Applicant__c, Loan_Application_Name__c, Initiation_Date__c, Requested_Loan_Amount__c, Product_Type__c, Branch__c, Owner_Name__c, Status__c, Owner__c, Record_Type_Name__c,Loan_Application__c,Loan_Application__r.Name,Loan_Application__r.Overall_FCU_Status__c,Owner__r.Profile.Name,Assigned_Vendor__c,Assigned_Vendor__r.Name,FCU_Type__c from Third_Party_Verification__c where RecordTypeId =: FCURecordTypeId AND Status__c = 'Initiated' AND Owner__c =: UserInfo.getUserId() ORDER BY Initiation_Date__c ASC];//Added by saumya Initiation_Date__c and Requested_Loan_Amount__c For Third Party view Change
                //Loan_Application__c,Loan_Application__r.Name,Loan_Application__r.Overall_FCU_Status__c and Loan_Application__r.FCU_Status__c='Initiated',Status__c='Assigned',ORDER BY Initiation_Date__c ASC,Assigned_Vendor__r.Name,FCU_Type__c added by Abhilekh on 19th September 2019 for FCU BRD
                System.debug('tpList==>'+tpList+'==='+tpList.size());
                returnList.addAll(tpList);    
            }
            else if(profileName == 'FCU Manager'){
                tpList = [Select Id, Name, Name_of_Applicant__c, Loan_Application_Name__c, Initiation_Date__c, Requested_Loan_Amount__c, Product_Type__c, Branch__c, Owner_Name__c, Status__c, Owner__c, Record_Type_Name__c,Loan_Application__c,Loan_Application__r.Name,Loan_Application__r.Overall_FCU_Status__c,Loan_Application__r.Re_Appealed_Case__c,Owner__r.Profile.Name,Assigned_Vendor__c,Assigned_Vendor__r.Name,FCU_Type__c from Third_Party_Verification__c where RecordTypeId =: FCURecordTypeId AND Owner__c =: UserInfo.getUserId() ORDER BY Initiation_Date__c ASC];
                returnList.addAll(tpList);
            }
            else{
                tpList = [Select Id, Name, Name_of_Applicant__c, Loan_Application_Name__c, Initiation_Date__c, Requested_Loan_Amount__c, Product_Type__c, Branch__c, Owner_Name__c, Status__c, Owner__c, Record_Type_Name__c,Loan_Application__c,Loan_Application__r.Name,Loan_Application__r.Overall_FCU_Status__c,Loan_Application__r.Re_Appealed_Case__c,Owner__r.Profile.Name,Assigned_Vendor__r.Name,FCU_Type__c from Third_Party_Verification__c where RecordTypeId =: FCURecordTypeId AND Owner__c =: UserInfo.getUserId() ORDER BY Initiation_Date__c ASC];
                returnList.addAll(tpList);
            }
        }
          if(u.Legal__c == TRUE){
              tpList = [Select Id, Name, Name_of_Applicant__c, Loan_Application_Name__c, Initiation_Date__c, Requested_Loan_Amount__c, Product_Type__c, Branch__c, Owner_Name__c, Status__c, Owner__c, Record_Type_Name__c,Loan_Application__c,Loan_Application__r.Name,Loan_Application__r.Overall_FCU_Status__c,Owner__r.Profile.Name,Assigned_Vendor__c,Assigned_Vendor__r.Name,Loan_Application__r.Re_Appealed_Case__c from Third_Party_Verification__c where (RecordTypeId =: LegalRecordTypeId OR RecordTypeId =: LegalVettingRecordTypeId) AND (Status__c = 'Initiated' or Status__c = 'Hold') AND Owner__c =: UserInfo.getUserId()];//Added by saumya Initiation_Date__c and Requested_Loan_Amount__c For Third Party view Change
              returnList.addAll(tpList);
          }
          if(u.LIP__c == TRUE){
              tpList = [Select Id, Name, Name_of_Applicant__c, Loan_Application_Name__c, Initiation_Date__c, Requested_Loan_Amount__c, Product_Type__c, Branch__c, Owner_Name__c, Status__c, Owner__c, Record_Type_Name__c,Loan_Application__c,Loan_Application__r.Name,Loan_Application__r.Overall_FCU_Status__c,Owner__r.Profile.Name,Assigned_Vendor__c,Assigned_Vendor__r.Name,Loan_Application__r.Re_Appealed_Case__c from Third_Party_Verification__c where RecordTypeId =: LIPRecordTypeId AND (Status__c = 'Initiated'or Status__c = 'Hold') AND Owner__c =: UserInfo.getUserId()];//Added by saumya Initiation_Date__c and Requested_Loan_Amount__c For Third Party view Change
              returnList.addAll(tpList);
          }
          if(u.Technical__c == TRUE){
              tpList = [Select Id, Name, Name_of_Applicant__c, Loan_Application_Name__c, Initiation_Date__c, Requested_Loan_Amount__c, Product_Type__c, Branch__c, Owner_Name__c, Status__c, Owner__c, Record_Type_Name__c,Loan_Application__c,Loan_Application__r.Name,Loan_Application__r.Overall_FCU_Status__c,Owner__r.Profile.Name,Assigned_Vendor__c,Assigned_Vendor__r.Name,Loan_Application__r.Re_Appealed_Case__c from Third_Party_Verification__c where RecordTypeId =: TechnicalRecordTypeId AND (Status__c = 'Initiated' or Status__c = 'Hold') AND Owner__c =: UserInfo.getUserId()];//Added by saumya Initiation_Date__c and Requested_Loan_Amount__c For Third Party view Change
              returnList.addAll(tpList);
          }
          if(u.PD__c == TRUE){
              tpList = [Select Id, Name, Name_of_Applicant__c, Loan_Application_Name__c, Initiation_Date__c, Requested_Loan_Amount__c, Product_Type__c, Branch__c, Owner_Name__c, Status__c, Owner__c, Record_Type_Name__c,Loan_Application__c,Loan_Application__r.Name,Loan_Application__r.Overall_FCU_Status__c,Owner__r.Profile.Name,Assigned_Vendor__c,Assigned_Vendor__r.Name,Loan_Application__r.Re_Appealed_Case__c from Third_Party_Verification__c where RecordTypeId =: VendorRecordTypeId AND (Status__c = 'Initiated'or Status__c = 'Hold') AND Owner__c =: UserInfo.getUserId()];//Added by saumya Initiation_Date__c and Requested_Loan_Amount__c For Third Party view Change
              returnList.addAll(tpList);
          }
		  if(profileName == 'IMGC'){/** Added by Saumya For IMGC **/
             tpList = [Select Id, Name, Name_of_Applicant__c, Loan_Application_Name__c, Initiation_Date__c, Requested_Loan_Amount__c, Product_Type__c, Branch__c, Owner_Name__c, Status__c, Owner__c, Record_Type_Name__c,Loan_Application__c,Loan_Application__r.Name,Loan_Application__r.Overall_FCU_Status__c,Owner__r.Profile.Name from Third_Party_Verification__c where RecordTypeId =: OthersRecordTypeId AND (Status__c = 'Initiated' or Status__c = 'Hold') AND Owner__c =: UserInfo.getUserId()];//Added by saumya Initiation_Date__c and Requested_Loan_Amount__c For Third Party view Change
            returnList.addAll(tpList);
		  }
        return returnList;
      }
	  
	  //Below method added by Abhilekh on 22nd January 2020 for Hunter BRD
    @AuraEnabled
    public static List <Third_Party_Verification__c> getHunterTPV() {
        Id HunterRecordTypeId = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('Hunter').getRecordTypeId();
        
        User u = [Select Id,Name,Hunter__c from User where Id =: UserInfo.getUserId()];
        
        List<Third_Party_Verification__c> tpList = new List<Third_Party_Verification__c>();
        List<Third_Party_Verification__c> returnList = new List<Third_Party_Verification__c>();
        
        if(u.Hunter__c == true){
            tpList = [Select Id, Name, Name_of_Applicant__c, Loan_Application_Name__c, Initiation_Date__c, Requested_Loan_Amount__c, Product_Type__c, Branch__c, Owner_Name__c, Status__c, Owner__c, Record_Type_Name__c,Loan_Application__c,Loan_Application__r.Name from Third_Party_Verification__c WHERE RecordTypeId =: HunterRecordTypeId AND Status__c =: Constants.INITIATED ORDER BY Initiation_Date__c ASC];
            returnList.addAll(tpList);
        }
        
        return returnList;
    }
}