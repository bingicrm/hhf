public class AIPController {
    
    @AuraEnabled
    public static List<AIP__c> fetchAIP(String key){
        List <AIP__c> AIPList = new List <AIP__c> ();
        Boolean creditStage = GetIseditable(key);
        integer month;
        integer year;
        if(creditStage) {
            LMS_Bus_Date__c lms = LMS_Bus_Date__c.getInstance();
            Date datobj = lms.Current_Date__c;
            
            integer day= datobj.day();
            month = datobj.month();
            year =datobj.year();
            system.debug('month'+datobj.month());
            system.debug('year'+datobj.year()); 
            Integer cutOffDate;
            if(String.isNotBlank(Label.Cutoff_Date)){
                cutOffDate = Integer.valueof(Label.Cutoff_Date.trim());
            }
            if(cutOffDate!=Null && cutOffDate!=0 && day<=cutOffDate){
               if(month == 1) {
                    month=12;
                    year=year-1;
                } else {
                    month=month-1;
                }
            }
        } else {
            List<Loan_Contact__c> lstOfLC = [SELECT Id, AIP_month__c from Loan_Contact__c where Id= : key];
            List<String> month_Year = lstOfLC[0].AIP_month__c.split('-');
            month = Integer.valueOf(month_Year[0]);
            year =  Integer.valueOf(month_Year[1]);
            system.debug('month'+month);
            system.debug('year'+year);
        }
        
        List <AIP__c> AIPList1 = new List <AIP__c> ();
        AIPList1= [SELECT Name, IsVerified__c, Id, Sales__c, Receipts__c, Total_Income__c, Purchases__c, Rent_Utility_Expenses__c, Salary__c,
                    Other_expenses_Without_EMI__c, Total_Expenses__c, Net_Monthly_Income__c, Customer_Detail__c, month__c, year__c 
                    FROM AIP__c WHERE Customer_Detail__c=:key and month__c=:month and year__c =: year order by createdDate desc];
        if(!AIPList1.isEmpty()) {
            AIP__c aip1 =  AIPList1[0];  
            AIPList.add(aip1);
        } else {                                      
            AIP__c aip1= new AIP__c( month__c=month, year__c=year, Customer_Detail__c=key);
            AIPList.add(aip1);
        }
        
        
        if(month==1){
            month=12;
            year=year-1;
        } else month = month -1;
        
        List <AIP__c> AIPList2 = new List <AIP__c> ();
        AIPList2= [SELECT Name, IsVerified__c, Id, Sales__c, Receipts__c, Total_Income__c, Purchases__c, Rent_Utility_Expenses__c, Salary__c,
                    Other_expenses_Without_EMI__c, Total_Expenses__c, Net_Monthly_Income__c, Customer_Detail__c, month__c, year__c 
                    FROM AIP__c WHERE Customer_Detail__c=:key and month__c=:month and year__c =: year order by createdDate desc];
        if(!AIPList2.isEmpty()) {
            AIP__c aip2 =  AIPList2[0];  
            AIPList.add(aip2);
        } else {                                      
            AIP__c aip2= new AIP__c( month__c=month, year__c=year, Customer_Detail__c=key);
            AIPList.add(aip2);
        }
        
        
        if(month==1){
            month=12;
            year=year-1;
        }
        else month=month-1;
        List <AIP__c> AIPList3 = new List <AIP__c> ();
        AIPList3= [SELECT Name, IsVerified__c, Id, Sales__c, Receipts__c, Total_Income__c, Purchases__c, Rent_Utility_Expenses__c, Salary__c,
                    Other_expenses_Without_EMI__c, Total_Expenses__c, Net_Monthly_Income__c, Customer_Detail__c, month__c, year__c 
                    FROM AIP__c WHERE Customer_Detail__c=:key and month__c=:month and year__c =: year order by createdDate desc];
        if(!AIPList3.isEmpty()) {
            AIP__c aip3 =  AIPList3[0];  
            AIPList.add(aip3);
        } else {                                      
            AIP__c aip3= new AIP__c( month__c=month, year__c=year, Customer_Detail__c=key);
            AIPList.add(aip3);
        }
        
        
        if(month==1){
            month=12;
            year=year-1;
        }
        else month=month-1;
        List <AIP__c> AIPList4 = new List <AIP__c> ();
        AIPList4= [SELECT Name, IsVerified__c, Id, Sales__c, Receipts__c, Total_Income__c, Purchases__c, Rent_Utility_Expenses__c, Salary__c,
                    Other_expenses_Without_EMI__c, Total_Expenses__c, Net_Monthly_Income__c, Customer_Detail__c, month__c, year__c 
                    FROM AIP__c WHERE Customer_Detail__c=:key and month__c=:month and year__c =: year order by createdDate desc];
        if(!AIPList4.isEmpty()) {
            AIP__c aip4 =  AIPList4[0];  
            AIPList.add(aip4);
        } else {                                      
            AIP__c aip4= new AIP__c( month__c=month, year__c=year, Customer_Detail__c=key);
            AIPList.add(aip4);
        }
        if(month==1){
            month=12;
            year=year-1;
        }
        else month=month-1;
        List <AIP__c> AIPList5 = new List <AIP__c> ();
        AIPList5= [SELECT Name, IsVerified__c, Id, Sales__c, Receipts__c, Total_Income__c, Purchases__c, Rent_Utility_Expenses__c, Salary__c,
                    Other_expenses_Without_EMI__c, Total_Expenses__c, Net_Monthly_Income__c, Customer_Detail__c, month__c, year__c 
                    FROM AIP__c WHERE Customer_Detail__c=:key and month__c=:month and year__c =: year order by createdDate desc];
        if(!AIPList5.isEmpty()) {
            AIP__c aip5 =  AIPList5[0];  
            AIPList.add(aip5);
        } else {                                      
            AIP__c aip5= new AIP__c( month__c=month, year__c=year, Customer_Detail__c=key);
            AIPList.add(aip5);
        }
        if(month==1){
            month=12;
            year=year-1;
        }
        else month=month-1;
        List <AIP__c> AIPList6 = new List <AIP__c> ();
        AIPList6= [SELECT Name, IsVerified__c, Id, Sales__c, Receipts__c, Total_Income__c, Purchases__c, Rent_Utility_Expenses__c, Salary__c,
                    Other_expenses_Without_EMI__c, Total_Expenses__c, Net_Monthly_Income__c, Customer_Detail__c, month__c, year__c 
                    FROM AIP__c WHERE Customer_Detail__c=:key and month__c=:month and year__c =: year order by createdDate desc];
        if(!AIPList6.isEmpty()) {
            AIP__c aip6 =  AIPList6[0];  
            AIPList.add(aip6);
        } else {                                      
            AIP__c aip6= new AIP__c( month__c=month, year__c=year, Customer_Detail__c=key);
            AIPList.add(aip6);
        }
        
        return AIPList;
    }
    
    @AuraEnabled
    public static List<string> sendMonths(String key){
        List<String> monthList = new List<String>();
        Integer currentMonth;
        Integer currenYear;
        Boolean creditStage = GetIseditable(key);
        if(creditStage) {
            
            LMS_Bus_Date__c lms = LMS_Bus_Date__c.getInstance();
            Date datobj = lms.Current_Date__c;
            
            integer day= datobj.day();
            currentMonth = datobj.month();
            currenYear =datobj.year();
            system.debug('month'+datobj.month());
            system.debug('year'+datobj.year()); 
            Integer cutOffDate;
            if(String.isNotBlank(Label.Cutoff_Date)){
                cutOffDate = Integer.valueof(Label.Cutoff_Date.trim());
            }
            if(cutOffDate!=Null && cutOffDate!=0 && day<=cutOffDate){
                if(currentMonth == 1) {
                    currentMonth=12;
                    currenYear=currenYear-1;
                } else {
                    currentMonth=currentMonth-1;
                }
            }
            
        } else {
            List<Loan_Contact__c> lstOfLC = [SELECT Id, AIP_month__c from Loan_Contact__c where Id= : key];
            List<String> month_Year = lstOfLC[0].AIP_month__c.split('-');
            currentMonth = Integer.valueOf(month_Year[0]);
            currenYear =  Integer.valueOf(month_Year[1]);
            system.debug('month'+currentMonth);
            system.debug('year'+currenYear);
        }    
        
        final map<integer,string> MonthCodeMap = new map<integer,string>{ 1=>'January',2=>'February',3=>'March',
        4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December'};
        
        for(integer i=0 ;i<6;i++){
            
            if(currentMonth-i <= 0){
                integer Year = currenYear-1;
                integer Month = 12+(currentMonth-i);                
                monthList.add(MonthCodeMap.get(Month)+'-'+string.valueOf(Year));
                
            }else {
                monthList.add(MonthCodeMap.get(currentMonth-i)+'-'+string.valueOf(currenYear));
                
            }
        }
        system.debug(monthList);
        return  monthList ;    
    }
    
    
    @Auraenabled
    Public static List<AIP__c> saveAIP(List<AIP__c> AIPtoUpdateInsert, String custid){
        List<AIP__c> Allaccwrapperlist = AIPtoUpdateInsert;
        //List<AIP__c> Allaccwrapperlist = (List<AIP__c>)JSON.deserialize(AIPtoUpdateInsert,List<AIP__c>.class);
        system.debug(Allaccwrapperlist[0]);
        if(Allaccwrapperlist!=Null && Allaccwrapperlist[0].isVerified__c == true){
            Allaccwrapperlist[1].isVerified__c = true;
            Allaccwrapperlist[2].isVerified__c = true;
            Allaccwrapperlist[3].isVerified__c = true;
            Allaccwrapperlist[4].isVerified__c = true;
            Allaccwrapperlist[5].isVerified__c = true;
            update new Loan_Contact__c(id= custid, AIP_Status__c = 'Completed',AIP_month__c = Allaccwrapperlist[0].Month__c + '-' + Allaccwrapperlist[0].Year__c);
        }
        else if(Allaccwrapperlist==Null || Allaccwrapperlist[0].isVerified__c == false){
            Allaccwrapperlist[0].isVerified__c = false;
            Allaccwrapperlist[1].isVerified__c = false;
            Allaccwrapperlist[2].isVerified__c = false;
            Allaccwrapperlist[3].isVerified__c = false;
            Allaccwrapperlist[4].isVerified__c = false;
            Allaccwrapperlist[5].isVerified__c = false;
            update new Loan_Contact__c(id= custid, AIP_Status__c = 'In Progress',AIP_month__c = Allaccwrapperlist[0].Month__c + '-' + Allaccwrapperlist[0].Year__c);

        } 
        else if(Allaccwrapperlist != null && Allaccwrapperlist[0].Month__c != null && Allaccwrapperlist[0].Year__c != null ) {
            update new Loan_Contact__c(id= custid, AIP_Status__c = 'In Progress',AIP_month__c = Allaccwrapperlist[0].Month__c + '-' + Allaccwrapperlist[0].Year__c);
        }
        
        system.debug(Allaccwrapperlist); 
        upsert Allaccwrapperlist;
        return Allaccwrapperlist;
    }
    @auraenabled
    public static Loan_Contact__c sendCustomerDetails(String key){
        
        Loan_Contact__c cd = new Loan_Contact__c();
        cd=[Select IsVerified__c,Loan_Applications__r.Name, AIP_Status__c, Applicant_Type__c, Customer__r.Name from Loan_Contact__c where id=:key limit 1];
        return cd;
    }
    @Auraenabled
    public static boolean GetIseditable(Id conId){
        Loan_Contact__c custobj = [SELECT Id,Loan_Applications__r.Sub_Stage__c from Loan_Contact__c where id =:conId order by createdDate DESC limit 1];
        system.debug('custobj.Loan_Applications__r.Sub_Stage__c::'+custobj.Loan_Applications__r.Sub_Stage__c);
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        system.debug('ProfileName'+profileName);
        //if((Label.Sub_Stage.contains(custobj.Loan_Applications__r.Sub_Stage__c) && Label.User_Profiles.contains(profileName)) || (Label.Credit_Team_Profile.contains(profileName))){ 
        if((Label.Credit_Team_Profile.contains(profileName))) { 
            return true;//edit mode
        } else {
            return false;//read only mode}   
        }
    }
   
}