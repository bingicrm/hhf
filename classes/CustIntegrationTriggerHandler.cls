public class CustIntegrationTriggerHandler{
    
    public static void beforeInsert(list < Customer_Integration__c > newList, list < Customer_Integration__c > oldList, map < id, Customer_Integration__c > newMap, map < id, Customer_Integration__c > oldMap) {
        
        Date dt = LMSDateUtility.lmsDateValue;
        
        for(Customer_Integration__c ci : newList){
            ci.Business_Date__c = (Date) dt;
            // Below if block added by Vaishali for Perfios [17-09-2019]
            if(ci.Manual__c == true && ci.recordTypeId == Label.CI_Perfios_RT) {
                system.debug(ci.recordTypeId);
                ci.Recent__c = true;
                //This Line is added by Vaishali on 28-09-2019
                ci.Perfios_Inititated__c = true;
            }
        }
    }
    
    public static void AfterInsert(list < customer_integration__c > newList, Map < Id, customer_integration__c > newMap) {
        // Added by Vaishali for Perfios [17-09-2019]: Code Begins
        Id devRecordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('Perfios').getRecordTypeId();
        List<Customer_Integration__c> lstOfCustInt = new List<Customer_Integration__c>();
        Set<Id> setOfLoanAppIds = new Set<Id>();
        Set<Id> setOfCustomerDetailIds = new Set<Id>();
        
        for(Customer_Integration__c objCustomerIntegration : newList) {
            //Added if condition by Vaishali for BREII
            if((objCustomerIntegration.Manual__c == true && objCustomerIntegration.Recent__c == true) || objCustomerIntegration.CAM_Screen_Entry__c) {
                customer_integration__c cust = new customer_integration__c(API_Type__c = objCustomerIntegration.API_Type__c,Loan_Contact__c = objCustomerIntegration.Loan_Contact__c,recordTypeId = devRecordTypeId, Loan_Application__c=objCustomerIntegration.Loan_Application__c, Manual__c= false, Recent__c = false);
                lstOfCustInt.add(cust);
                //Added if condition by Vaishali for BREII
                if((objCustomerIntegration.Manual__c == true && objCustomerIntegration.Recent__c == true)) {
                    setOfLoanAppIds.add(objCustomerIntegration.Loan_Application__c);
                    setOfCustomerDetailIds.add(objCustomerIntegration.Loan_Contact__c);
                    List<Customer_Integration__c> lstOfCustIntegrations = new List<Customer_Integration__c>();
                    if(!setOfLoanAppIds.isEmpty()) {
                        lstOfCustIntegrations = [SELECT Id, Recent__c from Customer_Integration__c WHERE Loan_Application__c IN : setOfLoanAppIds AND Loan_Contact__c IN : setOfCustomerDetailIds AND RecordType.Name='PERFIOS' AND ID NOT IN: newMap.KeySet()];
                    } 
                    if(!lstOfCustIntegrations.isEmpty()) {
                        for(Customer_Integration__c cust1: lstOfCustIntegrations) {
                            if(!newList.contains(cust1)) {
                                cust1.Recent__c = false;
                            }
                        }
                        update lstOfCustIntegrations;
                    }
                }
                //insert lstOfCustInt; //Commented by Abhilekh for BRE2 Enhancements
            }
        }
		
		//Below if bloack added by Abhilekh for BRE2 Enhancements
        if(lstOfCustInt.size() > 0){
            insert lstOfCustInt;
        }
		
        // Code Ends
        //Edited By Chitransh for GST Integration//
        list<customer_integration__c> cusList = [select id,GSTIN_Error_Message__c,Mobile__c, Email__c, GSTIN__c,RecordType.Name, Loan_Contact__c, recordTypeId from customer_integration__c where id in:newList];
        
        list<customer_integration__c> gstRecords = new list<customer_integration__c>(); //Added By Chitransh for GST Integration//
        list<id> recordIdList = new list<id>();
        list<id> loanConList = new list<id>();
        
        if(!cusList.isEmpty()){
            for(customer_integration__c tp: cusList){
                
                recordIdList.add(tp.recordTypeId);
                loanConList.add(tp.Loan_Contact__c);
                //Added by Chitransh Porwal [20-08-2019] for GST implementation
                if(tp.RecordType.Name == 'GSTIN' && tp.GSTIN__c != null && tp.GSTIN_Error_Message__c == null){
                    gstRecords.add(tp);
                }
            }
            
        }
        //Added by Chitransh Porwal [20-08-2019] for GST implementation
        if(!gstRecords.isEmpty()){
            system.debug('NotEmpty List');
            System.enqueueJob(new GstReturnStatusCallout(gstRecords));
            
        }
        //End of Patch Added by Chitransh Porwal[20-08-2019]
        
        if(recordIdList.size() >0){
            system.debug('==herreeeeeee=');
            
            list<customer_integration__c> previousList = new list<customer_integration__c>();
            previousList = [ select id, Latest_Record__c, Loan_Contact__c,recordTypeId from customer_integration__c where
                            recordTypeId in :recordIdList and id not in: newList and Loan_Contact__c in :loanConList and RecordType.Name != 'Perfios' and RecordType.Name != 'GSTIN'];//Edited by Chitransh for GST Integration//
            
            for(customer_integration__c prev: previousList){
                
                prev.Latest_Record__c = false;
            }
            update previousList;
        }
        
    }
    // Added by Vaishali for Perfios [17-09-2019] : Code Begins
    public static void onAfterUpdate(list < Customer_Integration__c > newList, list < Customer_Integration__c > oldList, map < id, Customer_Integration__c > newMap, map < id, Customer_Integration__c > oldMap) {
        /**** Added by Saumya For Hunter *****/    
        List<customer_integration__c> hunterList = new List<customer_integration__c>();
        List<customer_integration__c> laUpdateList = new List<customer_integration__c>();
        
        Id HunterRecordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('Hunter Analysis').getRecordTypeId();
        /**** Added by Saumya For UCIC Enhacements *****/
        list<id> laIdList = new list<id>(); 
        
        List<Loan_Application__c> laListtoUpdate = new List<Loan_Application__c>();
        for(customer_integration__c tp: newList){
            laIdList.add(tp.Loan_Application__c);
        }
        list<Loan_Application__c> laList = [select id, recordTypeId, ownerId, Assigned_Sales_User__c, Date_of_Rejection__c, Rejected_Cancelled_Stage__c, StageName__c, Sub_Stage__c, Rejection_Reason__c,Severity_Level__c from Loan_Application__c where id in:laIdList];
        /**** Added by Saumya For For UCIC Enhacements *****/
        /**** Added by Saumya For Hunter *****/
		
		Id PerfiosRecordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('Perfios').getRecordTypeId(); //Added by Abhilekh for BRE2 Enhancements
		
        List<Customer_Integration__c> lstOfCust = new List<Customer_Integration__c>();
        List<Customer_Integration__c> lstOfCust1 = new List<Customer_Integration__c>();
		Set<Id> setBankConsideredCILAIds = new Set<Id>(); //Added by Abhilekh for BRE2 Enhancements
        Set<Id> setBankConsFalseLAIds = new Set<Id>(); //Added by Abhilekh for BRE2 Enhancements
        List<Customer_Integration__c> lstCIWithODCCAccountType = new List<Customer_Integration__c>(); //Added by Abhilekh for BRE2 Enhancements
        List<Customer_Integration__c> lstCIWithoutODCCAccountType = new List<Customer_Integration__c>(); //Added by Abhilekh for BRE2 Enhancements
		
        for(Customer_Integration__c objCustomerIntegration : newList) {
            /**** Added by Saumya For Hunter *****/
            if(objCustomerIntegration.Match_Count__c != null && Integer.valueOf(objCustomerIntegration.Match_Count__c) > 0 && objCustomerIntegration.Match_Count__c != oldMap.get(objCustomerIntegration.Id).Match_Count__c && objCustomerIntegration.RecordTypeId == HunterRecordTypeId){
                System.debug('objCustomerIntegration.Match_Count__c==>'+objCustomerIntegration.Match_Count__c);
                System.debug('objCustomerIntegration.Id==>'+objCustomerIntegration.Id);
                System.debug('objCustomerIntegration.RecordTypeId==>'+objCustomerIntegration.RecordTypeId);                
                hunterList.add(objCustomerIntegration);
            }
            
            //system.debug('RECORDTYPENAME..'+objCustomerIntegration.RecordType.DeveloperName);
            if(oldMap.get(objCustomerIntegration.Id).Match_Count__c == null && objCustomerIntegration.Match_Count__c != null && objCustomerIntegration.RecordType.DeveloperName=='Hunter_Analysis'){
                laUpdateList.add(objCustomerIntegration);
            }
            /**** Added by Saumya For Hunter *****/
            if(objCustomerIntegration.API_Type__c == 'ScannedPDFBanking' && objCustomerIntegration.Analysis_Status__c=='Completed Successfully' && objCustomerIntegration.Analysis_Status__c != oldMap.get(objCustomerIntegration.Id).Analysis_Status__c) {
                System.debug('Debug Log for objCustomerIntegration.Analysis_Status__c'+objCustomerIntegration.Analysis_Status__c);
                lstOfCust.add(objCustomerIntegration);            
            }
            if(objCustomerIntegration.API_Type__c == 'Financial' && String.isNotBlank(objCustomerIntegration.PerfiosFinancialReport_Response__c) && objCustomerIntegration.PerfiosFinancialReport_Response__c != oldMap.get(objCustomerIntegration.Id).PerfiosFinancialReport_Response__c) {
                System.debug('Debug Log for objCustomerIntegration.PerfiosITRReport_Response__c'+objCustomerIntegration.PerfiosFinancialReport_Response__c);
                lstOfCust.add(objCustomerIntegration);
            }
            
            //Added the filter manual so that documents don't become optional for manual records
            if((objCustomerIntegration.API_Type__c =='ITR' || objCustomerIntegration.API_Type__c == 'Net Banking') && objCustomerIntegration.Analysis_Status__c=='Completed Successfully' && objCustomerIntegration.Manual__c== false && objCustomerIntegration.Analysis_Status__c != oldMap.get(objCustomerIntegration.Id).Analysis_Status__c) {
                lstOfCust1.add(objCustomerIntegration);
            }
            /*** Added by Saumya for UCIC Database **/
            if(oldMap.get(objCustomerIntegration.Id).Negative_UCIC_DB_Found__c != objCustomerIntegration.Negative_UCIC_DB_Found__c && objCustomerIntegration.Negative_UCIC_DB_Found__c == true){
                for(Loan_Application__c la: laList){
                    if(la.Id == objCustomerIntegration.Loan_Application__c){
                        if(la.Sub_Stage__c == Constants.COPS_Data_Checker){
                            la.recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Cancelled/Rejected').getRecordTypeId();
                            la.ownerId = la.Assigned_Sales_User__c;
                            la.Date_of_Rejection__c = System.TODAY();
                            la.Rejected_Cancelled_Stage__c = la.Sub_Stage__c;
                            la.StageName__c = 'Loan Rejected';
                            la.Sub_Stage__c = 'Loan reject';
                            la.Rejection_Reason__c = objCustomerIntegration.Rejection_Reason__c;
                            la.Severity_Level__c = objCustomerIntegration.Severity_Level__c;
                            laListtoUpdate.add(la);
                        }
                        else{
                            la.Rejection_Reason__c = objCustomerIntegration.Rejection_Reason__c;
                            la.Severity_Level__c = objCustomerIntegration.Severity_Level__c;
                            laListtoUpdate.add(la);    
                        }
                    }
                }
            }
            else if(oldMap.get(objCustomerIntegration.Id).Negative_UCIC_DB_Found__c != objCustomerIntegration.Negative_UCIC_DB_Found__c && objCustomerIntegration.Negative_UCIC_DB_Found__c == false){
                for(Loan_Application__c la: laList){
                    if(la.Id == objCustomerIntegration.Loan_Application__c){
                        la.Rejection_Reason__c = objCustomerIntegration.Rejection_Reason__c;
                        la.Severity_Level__c = objCustomerIntegration.Severity_Level__c;
                        laListtoUpdate.add(la);    
                    }
                }
            }
            /*** Added by Saumya for UCIC Database **/
			
			//Below if block added by Abhilekh for BRE2 Enhancements
            if(objCustomerIntegration.RecordTypeId == PerfiosRecordTypeId && objCustomerIntegration.Bank_Account_Number__c != null && 
               objCustomerIntegration.Analysis_Status__c == 'Completed Successfully' && 
               (objCustomerIntegration.API_Type__c == 'ScannedPDFBanking' || objCustomerIntegration.API_Type__c == 'Net Banking') &&
               objCustomerIntegration.Bank_Considered__c == true && (objCustomerIntegration.Bank_Considered__c != oldMap.get(objCustomerIntegration.Id).Bank_Considered__c)){
                setBankConsideredCILAIds.add(objCustomerIntegration.Loan_Application__c);
            }
            
            //Below if block added by Abhilekh for BRE2 Enhancements
            if(objCustomerIntegration.RecordTypeId == PerfiosRecordTypeId && objCustomerIntegration.Analysis_Status__c == 'Completed Successfully' &&
               (objCustomerIntegration.API_Type__c == 'ScannedPDFBanking' || objCustomerIntegration.API_Type__c == 'Net Banking') &&
               objCustomerIntegration.Bank_Considered__c == false && (objCustomerIntegration.Bank_Considered__c != oldMap.get(objCustomerIntegration.Id).Bank_Considered__c)){
            	setBankConsFalseLAIds.add(objCustomerIntegration.Loan_Application__c);
            }
            
            //Below if block added by Abhilekh for BRE2 Enhancements
            if(objCustomerIntegration.RecordTypeId == PerfiosRecordTypeId && objCustomerIntegration.Analysis_Status__c == 'Completed Successfully' &&
               (objCustomerIntegration.API_Type__c == 'ScannedPDFBanking' || objCustomerIntegration.API_Type__c == 'Net Banking') &&
               (objCustomerIntegration.Banking_Account_Type__c == 'OD' || objCustomerIntegration.Banking_Account_Type__c == 'CC') &&
               objCustomerIntegration.Banking_Account_Type__c != oldMap.get(objCustomerIntegration.Id).Banking_Account_Type__c &&
               oldMap.get(objCustomerIntegration.Id).Banking_Account_Type__c != 'OD' && oldMap.get(objCustomerIntegration.Id).Banking_Account_Type__c != 'CC'){
                lstCIWithODCCAccountType.add(objCustomerIntegration);
            }
            
            //Below if block added by Abhilekh for BRE2 Enhancements
            if(objCustomerIntegration.RecordTypeId == PerfiosRecordTypeId && objCustomerIntegration.Analysis_Status__c == 'Completed Successfully' &&
               (objCustomerIntegration.API_Type__c == 'ScannedPDFBanking' || objCustomerIntegration.API_Type__c == 'Net Banking') &&
               objCustomerIntegration.Banking_Account_Type__c != 'OD' && objCustomerIntegration.Banking_Account_Type__c != 'CC' &&
               objCustomerIntegration.Banking_Account_Type__c != oldMap.get(objCustomerIntegration.Id).Banking_Account_Type__c &&
               (oldMap.get(objCustomerIntegration.Id).Banking_Account_Type__c == 'OD' || oldMap.get(objCustomerIntegration.Id).Banking_Account_Type__c == 'CC')){
                lstCIWithoutODCCAccountType.add(objCustomerIntegration);
            }
        }
        /**** Added by Saumya For Hunter *****/
        if(hunterList.size()>0){
            CustIntegrationTriggerHelper.createHunterVerification(hunterList);
        }
        system.debug('==Hunter Loans TPV List=='+laUpdateList.size());
        if(laUpdateList.size()>0){
            CustIntegrationTriggerHelper.updateLAonHunterUpdate(laUpdateList);
        }
        /**** Added by Saumya For Hunter *****/
        if(!lstOfCust.isEmpty()) {
            CustIntegrationTriggerHelper.updateAnalysisStatusOfDocuments(lstOfCust, oldMap);
        } 
        if(!lstOfCust1.isEmpty()) {
            CustIntegrationTriggerHelper.setDocumentAsOptional(lstOfCust1);
        }
        /**** Added by Saumya For Hunter *****/
        if(laListtoUpdate.size() > 0){
            update laListtoUpdate;
        }
        /**** Added by Saumya For Hunter *****/
		
		//Below if block added by Abhilekh for BRE2 Enhancements
        if(setBankConsideredCILAIds.size() > 0){
            CustIntegrationTriggerHelper.createDeviationForBankConsidered(setBankConsideredCILAIds);
        }
        
        //Below if block added by Abhilekh for BRE2 Enhancements
        if(setBankConsFalseLAIds.size() > 0){
            CustIntegrationTriggerHelper.deleteDeviationForBankConsidered(setBankConsFalseLAIds);
        }
        
        //Below if block added by Abhilekh for BRE2 Enhancements
        if(lstCIWithODCCAccountType.size() > 0){
            CustIntegrationTriggerHelper.createDeviationForODCCAccountType(lstCIWithODCCAccountType);
        }
        
        //Below if block added by Abhilekh for BRE2 Enhancements
        if(lstCIWithoutODCCAccountType.size() > 0){
            CustIntegrationTriggerHelper.deleteDeviationForODCCAccountType(lstCIWithoutODCCAccountType);
        }
    }
    
    public static void onBeforeUpdate(list < Customer_Integration__c > newList, list < Customer_Integration__c > oldList, map < id, Customer_Integration__c > newMap, map < id, Customer_Integration__c > oldMap) {
        
        //**********************************Added by Vaishali for BRE
        Id recordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('BRE').getRecordTypeId();
        Id recordTypeCIBIL = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId();
        Id recordTypePAN = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('PAN Integration').getRecordTypeId();
        //**********************************End of the patch- Added by Vaishali for BRE
        
        for(Customer_Integration__c objCustomerIntegration : newList) {
            /*** Added by saumya for UCIC Enhancements ***/
            if(oldMap.get(objCustomerIntegration.Id).Negative_UCIC_DB_Found__c != objCustomerIntegration.Negative_UCIC_DB_Found__c && objCustomerIntegration.Negative_UCIC_DB_Found__c == false){
                objCustomerIntegration.Rejection_Reason__c = '';
                objCustomerIntegration.Severity_Level__c = '';
            }
            /*** Added by saumya for UCIC Enhancements ***/
            //**********************************Added by Vaishali for BRE
            if((objCustomerIntegration.recordTypeId == recordTypeId || objCustomerIntegration.recordTypeId == recordTypeCIBIL || objCustomerIntegration.recordTypeId == recordTypePAN) && (objCustomerIntegration.Loan_Application__c != oldMap.get(objCustomerIntegration.Id).Loan_Application__c) || (objCustomerIntegration.Loan_Contact__c != oldMap.get(objCustomerIntegration.Id).Loan_Contact__c)) {
                objCustomerIntegration.addError('You are not allowed to modify this record');
            }
            //**********************************End of the patch -Added by Vaishali for BRE
            
            if(String.isNotBlank(objCustomerIntegration.PerfiosITRXML_Response__c) && objCustomerIntegration.PerfiosITRXML_Response__c != oldMap.get(objCustomerIntegration.Id).PerfiosITRXML_Response__c) {
                CustIntegrationTriggerHelper.parseITRStatementResponse(objCustomerIntegration.PerfiosITRXML_Response__c, objCustomerIntegration.Id);               
                objCustomerIntegration.Analysis_Status__c = 'Completed Successfully';
            }
            if(String.isNotBlank(objCustomerIntegration.PerfiosFinancialReport_Response__c) && objCustomerIntegration.PerfiosFinancialReport_Response__c != oldMap.get(objCustomerIntegration.Id).PerfiosFinancialReport_Response__c) {
                CustIntegrationTriggerHelper.parseFinancialStatementResponse(objCustomerIntegration.PerfiosFinancialReport_Response__c,objCustomerIntegration.Id);
                objCustomerIntegration.Analysis_Status__c = 'Completed Successfully';
            }
            
            if(objCustomerIntegration.PerfiosBankReport_Error_Response__c != oldMap.get(objCustomerIntegration.Id).PerfiosBankReport_Error_Response__c) {
                objCustomerIntegration.Analysis_Status__c = 'Failed';
            }
            if(objCustomerIntegration.PerfiosITRReport_Error_Response__c != oldMap.get(objCustomerIntegration.Id).PerfiosITRReport_Error_Response__c) {
                objCustomerIntegration.Analysis_Status__c = 'Failed';
            }
            if(objCustomerIntegration.PerfiosFinancialReport_Error_Response__c != oldMap.get(objCustomerIntegration.Id).PerfiosFinancialReport_Error_Response__c) {
                objCustomerIntegration.Analysis_Status__c = 'Failed';
            }
        }
        
    }
    // Code Ends
}