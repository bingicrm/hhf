public class EnqueDMLGSTINTwo implements Queueable{
    public Customer_Integration__c ciRecord;
    public String strRequest;
    public String strResponse;
    public String fillingFreq;
    public String strURL;
    public EnqueDMLGSTINTwo(Customer_Integration__c ciRecord,String strRequest, String strResponse, String strURL){
        system.debug('Debug for ciRecord2' + ciRecord);
        system.debug('Debug for strRequest2' + strRequest);
        system.debug('Debug for strResponse2' + strResponse);
        system.debug('Debug for strURL2' + strURL);
        this.ciRecord = ciRecord;
        this.strRequest = strRequest;
        this.strResponse = strResponse;
        this.strURL = strURL;
        this.fillingFreq = fillingFreq;
    }
    public void execute(QueueableContext context) {
        Customer_Integration_For_GSTIN__c passValidationCheck =  Customer_Integration_For_GSTIN__c.getInstance();
      
        /*if(ciRecord.Id != null){
            passValidationCheck.CI_GSTIN_Editable__c = true;
            upsert passValidationCheck;
            ciRecord.System_Created__c = true;
            update ciRecord;
             if(passValidationCheck.CI_GSTIN_Editable__c){
                passValidationCheck.CI_GSTIN_Editable__c = false;
            upsert passValidationCheck; 
            }
            system.debug('GSTRecord.System_Created__c  ' + ciRecord.System_Created__c  );
        }*/
        
        if(ciRecord.Loan_Contact__c != null){
            system.debug('Debug log for Loan Contact - Success GST');
            Loan_Contact__c loanContact = [Select id,Success_GST__c from Loan_Contact__c where id =: ciRecord.Loan_Contact__c limit 1];
            
            if(loanContact.Id != null){
                system.debug('Success GST PREValue' + loanContact.Success_GST__c);
                loanContact.Success_GST__c = true;
                update loanContact; 
                system.debug('Success GST PostValue' + loanContact.Success_GST__c);
            }
        }
        HttpResponse res = new HttpResponse();
        res.setBody(strResponse);  
        LogUtility.createIntegrationLogs(strRequest,res,strURL);
    }
}