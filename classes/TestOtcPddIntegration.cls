@isTest
public class TestOtcPddIntegration {
    @TestSetup
    static void setup()
    {
        
        Account acc=new Account();
        acc.name='TestAccount1';
        acc.phone='9234567667';
        acc.PAN__c='fjauy0916u';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        System.debug(acc.id);
        
        Scheme__c sc=new Scheme__c();
        sc.name='TestScheme';
        sc.Scheme_Code__c='testcode';
        sc.Scheme_Group_ID__c='23131';
        sc.Product_Code__c='HL';
        insert sc;
        System.debug(sc.id);
        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Transaction_type__c='PB';
        lap.Scheme__c=sc.id;
        insert lap;
        System.debug(lap.id);
        
        City__c city=new city__c();
        city.City_ID__c='100000';
        city.Name='TEZPOOR';
        city.Country__c='1';
        city.State__c='5';
        insert city;
        Pincode__c pin=new Pincode__c();
        pin.City_LP__c=city.id;
        pin.ZIP_ID__c='46172';
        insert pin;
        list<Loan_Contact__c> lc=[select id, name from Loan_Contact__c where Loan_Applications__c=:lap.id limit 1];
        system.debug('loan contact'+lc[0]);
       /* Address__c address=new Address__c();
        address.Address_Line_1__c='abc Street';
        address.Address_Line_2__c='abc Area';
        address.Pincode_LP__c=pin.id;
        address.Type_of_address__c='PERMNENT';
        address.Loan_Contact__c=lc[0].id;
        insert address;*/
        Document_Master__c dm=new Document_Master__c();
        dm.name='MasterDoc';
        dm.Doc_Id__c = '12';
        insert dm;
        Document_Checklist__c dc=new Document_Checklist__c();
        dc.Status__c='Pending';
        dc.Loan_Applications__c=lap.id;
        dc.Loan_Contact__c=lc[0].id;
        dc.Document_Master__c=dm.id;
        insert dc;
       ContentVersion contentVersion_1 = new ContentVersion(
      Title = 'Penguins',
      PathOnClient = 'Penguins.jpg',
      VersionData = Blob.valueOf('Test Content'),
      IsMajorVersion = true
    );
    insert contentVersion_1;
        
              
    }
    @isTest
    public static void TestIntegrateOTC(){
        list<Document_Checklist__c> dc = [select   Id,Loan_Applications__r.Loan_Number__c,OTC_PDD_Remarks__c,OTC_Received__c,Loan_Applications__r.Customer__r.Customer_ID__c ,Loan_Contact__c, Loan_Contact__r.Customer__r.Customer_ID__c,Document_master__r.Doc_Id__c,Document_master__r.Name,
                                          Document_Type__r.Document_Type_Id__c,Document_Type__c,REquest_Date_for_OTC__c from Document_Checklist__c limit 1];
        OtcPddIntegration.OTCResponse otc= new OtcPddIntegration.OTCResponse();
        Document_Type__c objDocType = new Document_Type__c(Name =Constants.PROPERTYPAPERS );
        insert objDocType;
        TestMockRequest req1=new TestMockRequest(200,
                                                 'Complete',
                                                 JSON.serialize(otc),
                                                 null);
                Test.setMock(HttpCalloutMock.class,req1);
                
            
                Test.startTest();
                OtcPddIntegration.IntegrateOTC(dc[0].id);
                Test.stopTest();
        
       
    }
}