public class monthlyInflowOutflowTriggerHandler {
    
    //Added by Abhilekh for BRE2 Enhancements
    public Static void beforeInsert(List<Monthly_Inflow_Outflow__c> newList) {
        
        for(Monthly_Inflow_Outflow__c io: newList){
            if(io.Balance_on_5__c != null || io.Balance_on_15__c != null || io.Balance_on_25__c != null){
                Decimal balOn5 = 0;
                Decimal balOn15 = 0;
                Decimal balOn25 = 0;
                Integer count = 0;
                
                if(io.Balance_on_5__c != null){
                    count++;
                    balOn5 = io.Balance_on_5__c;
                }
                if(io.Balance_on_15__c != null){
                    count++;
                    balOn15 = io.Balance_on_15__c;
                }
                if(io.Balance_on_25__c != null){
                    count++;
                    balOn25 = io.Balance_on_25__c;
                }
                
                if(count != 0){
                    io.ABB__c = (balOn5 + balOn15 + balOn25)/count;
                }
            }
        }
    }
    
    public Static void afterInsert(List<Monthly_Inflow_Outflow__c> newList) {
        /*Set<Id> setOfCustIntegrationsToUpdate = new Set<Id>();
for(Monthly_Inflow_Outflow__c mon: newList) {
if(String.isNotBlank(String.valueOf(mon.ABB_BRE__c  )) && mon.BRE_Record__c) {
setOfCustIntegrationsToUpdate.add(mon.Customer_Integration__c);
}
}
if(!setOfCustIntegrationsToUpdate.isEmpty()) {
monthlyInflowOutflowTriggerHandler.updateCustIntegrationsABB(setOfCustIntegrationsToUpdate);
}*/
    }
    
    //Added by Abhilekh for BRE2 Enhancements
    public Static void beforeUpdate(List<Monthly_Inflow_Outflow__c> newList,Map<Id, Monthly_Inflow_Outflow__c> oldMap) {
        
        for(Monthly_Inflow_Outflow__c io: newList){
            Decimal balOn5 = 0;
            Decimal balOn15 = 0;
            Decimal balOn25 = 0;
            Integer count = 0;
            
            if(io.Balance_on_5__c != null){
                count++;
                balOn5 = io.Balance_on_5__c;
            }
            if(io.Balance_on_15__c != null){
                count++;
                balOn15 = io.Balance_on_15__c;
            }
            if(io.Balance_on_25__c != null){
                count++;
                balOn25 = io.Balance_on_25__c;
            }
            
            if(count != 0){
                io.ABB__c = (balOn5 + balOn15 + balOn25)/count;
            }
            else{
                io.ABB__c = null;
            }
        }
    }
    
    public Static void afterUpdate(List<Monthly_Inflow_Outflow__c> newList,Map<Id, Monthly_Inflow_Outflow__c> oldMap) {
        /*Set<Id> setOfCustIntegrationsToUpdate = new Set<Id>();
for(Monthly_Inflow_Outflow__c mon: newList) {
if( mon.BRE_Record__c && mon.ABB_BRE__c  != oldMap.get(mon.Id).ABB_BRE__c   ) {
setOfCustIntegrationsToUpdate.add(mon.Customer_Integration__c);
}
}
if(!setOfCustIntegrationsToUpdate.isEmpty()) {
monthlyInflowOutflowTriggerHandler.updateCustIntegrationsABB(setOfCustIntegrationsToUpdate);
}
*/
    }
    
    /*public Static void updateCustIntegrationsABB(Set<id> setOfCustIntegrationsToUpdate) {
List<Customer_Integration__c> lstOfCustIntegrations = new List <Customer_Integration__c>();
lstOfCustIntegrations = [SELECT Id, Average_ABB__c from Customer_Integration__c WHERE Id IN: setOfCustIntegrationsToUpdate];

Map<Id, Decimal> mapOfCustIdWithMonthlyABB = new Map<Id, Decimal>();
List<AggregateResult> lstOfAggregates = [SELECT Customer_Integration__c cust, Avg(ABB_BRE__c    ) averageABB from Monthly_Inflow_Outflow__c group by Customer_Integration__c Having Customer_Integration__c IN: setOfCustIntegrationsToUpdate];
for(AggregateResult agg: lstOfAggregates) {
mapOfCustIdWithMonthlyABB.put((Id)agg.get('cust'), (Decimal)agg.get('averageABB'));    
}

for(Customer_Integration__c cust: lstOfCustIntegrations) {
cust.Average_ABB__c = mapOfCustIdWithMonthlyABB.get(cust.Id);    
}
update lstOfCustIntegrations;
}*/
}