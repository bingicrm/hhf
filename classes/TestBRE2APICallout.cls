@isTest(seeAllData=false)
private class TestBRE2APICallout {

    private static testMethod void test() {
        Profile p = [SELECT id, name from profile where name =: 'Sales Team'];

        User u = new User(
            ProfileId = p.Id,
            LastName = 'last',  
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST', 
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            Technical__c = true,
            FCU__c= true,Legal__c = true, FI__c= true,LIP__C = true,PD__c= true, Role_in_Sales_Hierarchy__c ='SM');

        insert u;
        
        Loan_Contact__c lc;
        Loan_Application__c la;

        System.runAs(u) {
            Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();

            Account acc = new Account();
            acc.CountryCode__c = '+91';
            acc.Email__c = 'puser001@amamama.com';
            acc.Gender__c = 'M';
            acc.Mobile__c = '9800765432';
            acc.Phone__c = '9800765434';
            acc.PersonMobilePhone = '9800765434';
            acc.FirstName = 'abc';
            acc.LastName = 'def';
            acc.Salutation = 'Mr';
            acc.RecordTypeId = RecordTypeIdCustomer;
            insert acc;

            Scheme__c sch = new Scheme__c(Name = 'Construction Finance', Scheme_Code__c = 'CF', Scheme_Group_ID__c = '5',
                Scheme_ID__c = 11, Product_Code__c = 'CF', Max_ROI__c = 10, Min_ROI__c = 5,
                Min_Loan_Amount__c = 10000, Min_FOIR__c = 1,
                Max_Loan_Amount__c = 3000001, Max_Tenure__c = 84, Min_Tenure__c = 6,
                Max_LTV__c = 5, Min_LTV__c = 1, Max_FOIR__c = 5);
            insert sch;
            
            Local_Policies__c lp = new Local_Policies__c(Name='Standard', Branch__c='All',Product_Scheme__c='All Schemes',IsActive__c=true);
            insert lp;

            Id RecordTypeIdLAAppIni = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Application Initiation').getRecordTypeId();

            la = new Loan_Application__c(Customer__c = acc.Id, Scheme__c = sch.Id, Loan_Number__c = '124',
                Transaction_type__c = 'PB', Requested_Amount__c = 100000,
                Loan_Purpose__c = '11', StageName__c = 'Customer Onboarding', Sub_Stage__c = 'Application Initiation',
                RecordTypeId = RecordTypeIdLAAppIni,Branch__c='Delhi',
                Business_Date_Created__c = System.today(),
                Approved_Loan_Amount__c = 100000,Requested_Loan_Tenure__c=84,
                Processing_Fee_Percentage__c = 1, Mode_of_Repayment__c = 'NACH',
                Repayment_Start_Date__c = Date.Today().addDays(100),Income_Program_Type__c='Normal Salaried',
                Sanctioned_Disbursement_Type__c = 'Multiple Tranche', Property_Identified__c = true,
                Local_Policies__c=lp.Id);
            insert la;
        Test.startTest();

            lc = [SELECT Id, Borrower__c from Loan_Contact__c where Loan_Applications__c =: la.Id];
            lc.Borrower__c = '1';
            //lc.Category__c='1';
            lc.Constitution__c = '20';
            lc.Customer_segment__c = '3';
            lc.BRE_Record__c = true;
            lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc.Marital_Status__c = 'M';
            lc.Gross_Salary__c = 1000;
            lc.Basic_Pay__c = 1000;
            lc.DA__c = 1000;
            lc.Qualification__c = 'UMS';
            //lc.Total_Fixed_Pay__c = 1000;
            //lc.Variable_Income_Monthly__c = 1000;
            lc.Net_Income__c = 1000;
            lc.PF__c = 1000;
            lc.TDS__c = 1000;
            lc.Current_Organization_Work_Ex_in_Months__c = 12;
            lc.Total_Work_Experience__c = 12;
            lc.Customer_Details_Verified__c = true;
            lc.Voter_ID_Number__c = '123456789';
            lc.Passport_Number__c = '123456789';
            lc.PAN_Number__c = 'BAFPV6543A';
            lc.Aadhaar_Number__c = '898518762736';
            lc.Income_Considered__c = true;
            lc.AIP_month__c = '4-2020';
            update lc;
            
            Cross_Sell_Partner_Name__c CSPN = new Cross_Sell_Partner_Name__c(Name='Test');
            insert CSPN;
            
            DST_Master__c DSTMaster = new DST_Master__c(Name='Test DST',Inspector_ID__c='1234'); 
            insert DSTMaster;
            
            Sourcing_Detail__c sd= new Sourcing_Detail__c(Loan_Application__c=la.id,Source_Code__c='16',DST_Name__c=DSTMaster.id,Cross_Sell_Partner_Name__c=CSPN.id);
            insert sd;
            
            la.StageName__c = 'Operation Control';
            la.Sub_Stage__c = 'COPS:Data Maker';
            update la;
        }
        
        Pincode__c pin = new Pincode__c(Name='121003',City__c='FARIDABAD',ZIP_ID__c='34565',State_Text__c='Haryana',Country_Text__c='India',Active__c=true,Serviceable__c=false,
                                        ZIP_Code_Description__c='Faridabad');
        insert pin;
         System.debug('Debug in test==>'+pin.City__c);
         System.debug('Debug in test==>'+pin.State_Text__c);
        Property__c prop = new Property__c(Loan_Application__c=la.id,Pincode_LP__c=pin.Id,First_Property_Owner__c=lc.id,Address_Line_1__c='Test',Address_Line_2__c='Test');
        insert prop;
        
        Property__c p11 = [Select Address_Line_1__c,Address_line_2__c  ,Geolimits__c, City__c,State__c,Country__c,Pincode_LP__c,Pincode_LP__r.name,Pincode_LP__r.City__c,Pincode_LP__r.State_Text__c,Pincode_LP__r.Country_Text__c,Pincode_LP__r.Serviceable__c,Pincode_LP__r.ZIP_Code_Description__c, City_LP__c, State_LP__c, Country_LP__c,Loan_Application__c From Property__c WHERE Id =: prop.Id];
        System.debug('Debug in test==>'+p11);
        System.debug('Debug in test==>'+p11.Pincode_LP__r.Name);
        System.debug('Debug in test==>'+p11.Pincode_LP__r.State_Text__c);
        System.debug('Debug in test==>'+p11.Pincode_LP__r.City__c);
        System.debug('Debug in test==>'+p11.Pincode_LP__r.Serviceable__c);
        System.debug('Debug in test==>'+p11.Pincode_LP__r.ZIP_Code_Description__c);
        System.debug('Debug in test==>'+p11.City_LP__c);
        System.debug('Debug in test==>'+p11.State_LP__c);
        System.debug('Debug in test==>'+p11.Country_LP__c);
        
        Address__c add = new Address__c(Loan_Contact__c = lc.Id,Type_of_address__c = 'PERMNENT',Residence_Type__c = 'P',Address_Line_1__c = 'Test', Pincode_LP__c=pin.Id);
        insert add;
        Id RecordTypeIdtechTPV = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('Technical').getRecordTypeId();
        Id RecordTypeIdFCUTPV = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('FCU').getRecordTypeId();
        Id RecordTypeIdLegalTPV = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('Legal').getRecordTypeId();
        Id RecordTypeIdFITPV = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('FI').getRecordTypeId();
        Id RecordTypeIdLIPTPV =Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('LIP').getRecordTypeId();
        Id RecordTypeIdVendorPdTPV = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('Vendor PD').getRecordTypeId();
        List<Third_Party_Verification__c> tpvs = new List<Third_Party_Verification__c>();
        Third_Party_Verification__c tpv = new Third_Party_Verification__c(Status__c ='Initiated', RecordTypeId=RecordTypeIdtechTPV,Owner__c= u.Id,Loan_Application__c = la.Id,Loan_Contact__c = lc.Id, Customer_Details__c = lc.Id);
        Third_Party_Verification__c tpv1 = new Third_Party_Verification__c(Status__c ='Initiated', RecordTypeId=RecordTypeIdFCUTPV,Owner__c= u.Id,Loan_Application__c = la.Id,Customer_Details__c = lc.Id,Loan_Contact__c = lc.Id);
        Third_Party_Verification__c tpv2 = new Third_Party_Verification__c(Status__c ='Initiated', RecordTypeId=RecordTypeIdLegalTPV,Owner__c= u.Id,Loan_Application__c = la.Id,Customer_Details__c = lc.Id,Loan_Contact__c = lc.Id);
        Third_Party_Verification__c tpv3 = new Third_Party_Verification__c(Status__c ='Initiated', RecordTypeId=RecordTypeIdFITPV,Owner__c= u.Id,Loan_Application__c = la.Id,Customer_Details__c = lc.Id,Address_Type__c = 'PERMNENT',Loan_Contact__c = lc.Id);
        Third_Party_Verification__c tpv4 = new Third_Party_Verification__c(Status__c ='Initiated', RecordTypeId=RecordTypeIdLIPTPV,Owner__c= u.Id,Loan_Application__c = la.Id,Customer_Details__c = lc.Id,Loan_Contact__c = lc.Id);
        Third_Party_Verification__c tpv5 = new Third_Party_Verification__c(Status__c ='Initiated', RecordTypeId=RecordTypeIdVendorPdTPV,Owner__c= u.Id,Loan_Application__c = la.Id,Customer_Details__c = lc.Id,Loan_Contact__c = lc.Id);
        
        tpvs.add(tpv);
        tpvs.add(tpv1);
        tpvs.add(tpv2);
        tpvs.add(tpv3);
        tpvs.add(tpv4);
        tpvs.add(tpv5);
        insert tpvs; 
        system.debug('tpv4 '+tpv4);
        List<Third_Party_Verification__c> tpvLI = new List<Third_Party_Verification__c>();
        tpvLI = [Select Id, RecordtypeId, RecordType.Name,Owner_Name__c,Type_of_Property__c,Final_Property_Valuation__c, Status__c, Report_Status__c, Loan_Application__c, Loan_Contact__c ,Property__r.Id,
                               Reviewed_and_Approved__c,toLabel(Address_Type__c), Agency_Name__c,Actual_Area_Valuation__c,Carpet_Area_in_Sq_Ft__c,Floor_Wise_Area__c,Class_of_Locality__c,Municipal_Limit__c,Property_within_municipal_limits__c,Year_of_Construction__c,Extension_Improvement_Area_sq_ft__c,Extension_Improvement_Area_Rate_sq_ft__c,Property_Construction_Stage__c,Total_Allotted__c,Total_Completed__c,Approx_rentals_in_case_of_100_completed__c,Property_Falling_in_Caution_Area__c,Name_of_Municipal_Corporation__c,Floor_Valued__c,No_of_Floors__c,Relationship_of_Occupant_with_Customer__c,Ownership_type__c,Current_Usage_of_Property__c,Property_Status__c
                                from Third_Party_Verification__c WHERE Loan_Contact__c = : lc.Id order by CreatedDate desc];
         system.debug('tpvLI '+tpvLI);                       
        List<Loan_Contact__c> loanContactList = [SELECT Id, (Select Id, RecordtypeId, RecordType.Name,Owner_Name__c,Type_of_Property__c,Final_Property_Valuation__c, Status__c, Report_Status__c, Loan_Application__c, Loan_Contact__c ,Property__r.Id,
                               Reviewed_and_Approved__c,toLabel(Address_Type__c), Agency_Name__c,Actual_Area_Valuation__c,Carpet_Area_in_Sq_Ft__c,Floor_Wise_Area__c,Class_of_Locality__c,Municipal_Limit__c,Property_within_municipal_limits__c,Year_of_Construction__c,Extension_Improvement_Area_sq_ft__c,Extension_Improvement_Area_Rate_sq_ft__c,Property_Construction_Stage__c,Total_Allotted__c,Total_Completed__c,Approx_rentals_in_case_of_100_completed__c,Property_Falling_in_Caution_Area__c,Name_of_Municipal_Corporation__c,Floor_Valued__c,No_of_Floors__c,Relationship_of_Occupant_with_Customer__c,Ownership_type__c,Current_Usage_of_Property__c,Property_Status__c
                                from Third_Party_Verifications1__r order by CreatedDate desc ) FROM Loan_Contact__c Where Loan_Applications__c =:la.Id and Borrower__c='1'  and Applicant_Type__c != 'Guarantor'];
        system.debug('loanContactList '+loanContactList);
        system.debug('loanContactList0'+loanContactList[0].Third_Party_Verifications1__r);
                Test.stopTest();
        Personal_Discussion__c pd = new Personal_Discussion__c(Customer_Detail__c = lc.Id, Loan_Application__c = la.Id, PD_Status__c='Positive',Type_Of_PD__c='Telephonic');
        insert pd;
        Date dt = System.today();
        Customer_Obligations__c  custObl = new Customer_Obligations__c(Customer_Detail__c = lc.Id,EMI_Amount__c = 1,Account_Type__c='Savings',Organization_of_Account__c = 'ABC',Payment_History1__c ='05/05/2019',Payment_History_End_Date__c= dt, Payment_History_Start_Date__c = dt);
        insert custObl; 
        Enquiry__c enq= new Enquiry__c(Enquiry_Date__c = dt, Organization_of_Enquiry__c = 'ABC');
        insert enq;
        Bureau_address__c bureauAddress = new Bureau_address__c(Address_Category__c = '01', Address_Line1__c = 'ABC',State__c = 'Haryana', Date_Reported__c = dt);
        insert bureauAddress;
        ID_Detail__c iddet = new ID_Detail__c(ID_Document_Type__c = 'abc', ID_Number__c='123');
        insert iddet;
        BRE2APICallout.getMessage(la.Id);
         
        Id RecordTypeIdBRE2 = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('BRE2').getRecordTypeId();
        Id RecordTypeIdPerfios = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('Perfios').getRecordTypeId();
        Id RecordTypeIdCIBIL = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId();
        Perfios_Bank_Information__c bank = new Perfios_Bank_Information__c(Name = 'ICICI', Bank_Name__c  = '2');
        insert bank;

        List<Customer_Integration__c> custList = new List<Customer_Integration__c>();
        Customer_Integration__c cust = new Customer_Integration__c(RecordTypeId = RecordTypeIdBRE2, BRE2_Success__c= true, Loan_Application__c = la.Id, Loan_Contact__c = lc.Id,Manual__c = false, Recent__c = false,Banking_Month_Year__c = '4-2020');
        Customer_Integration__c cust1 = new Customer_Integration__c(RecordTypeId = RecordTypeIdPerfios,Loan_Application__c = la.Id, Loan_Contact__c = lc.Id, CAM_Screen_Entry__c = true,API_Type__c= 'ScannedPDFBanking',Manual__c = false, Recent__c = false, Perfios_Bank_Information__c= bank.Id, Bank_Account_Number__c = '1234',Average_ABB__c = 0,Analysis_Status__c = 'Completed Successfully',Banking_Month_Year__c = '4-2020');
        Customer_Integration__c cust2 = new Customer_Integration__c(RecordTypeId = RecordTypeIdPerfios,Loan_Application__c = la.Id, Loan_Contact__c = lc.Id, CAM_Screen_Entry__c = true,API_Type__c = 'Financial', Financial_Year__c = '2020',Manual__c = false, Recent__c = false,Analysis_Status__c = 'Completed Successfully',Banking_Month_Year__c = '4-2020');
        Customer_Integration__c cust3 = new Customer_Integration__c(RecordTypeId = RecordTypeIdCIBIL, Loan_Application__c = la.Id, Loan_Contact__c = lc.Id, Manual__c = false, Recent__c = false, CIBIL_Score__c = '12',Banking_Month_Year__c = '4-2020');
        
        insert cust;
        insert cust1;
        insert cust2;
        insert cust3;
        Monthly_Inflow_Outflow__c mon = new Monthly_Inflow_Outflow__c (Customer_Integration__c = cust1.Id, BRE_Record__c = true,Is_Verified__c = true,Month__c = 'April', Year__c = '2020');
        insert mon;
        AIP__c aip = new AIP__c(Customer_Detail__c=lc.Id, Month__c = 4,Year__c = 2020);
        insert aip;
          
        List<Financial_Statement__c> fsList = new List<Financial_Statement__c>();
        Financial_Statement__c fs = new Financial_Statement__c(Customer_Integration__c = cust2.Id, BRE_Record__c = true, isGrowthRecord__c =false,Year__c = '2018',Is_Estimated__c = false,isVerified__c = true);
        Financial_Statement__c fs1 = new Financial_Statement__c(Customer_Integration__c = cust2.Id, BRE_Record__c = true, isGrowthRecord__c =false,Year__c = '2017',isVerified__c = true);
        Financial_Statement__c fs2 = new Financial_Statement__c(Customer_Integration__c = cust2.Id, BRE_Record__c = true, isGrowthRecord__c =false,Year__c = '2016',isVerified__c = true);
        Financial_Statement__c fs3 = new Financial_Statement__c(Customer_Integration__c = cust2.Id, BRE_Record__c = true, isGrowthRecord__c =false,Year__c = '2018',Is_Estimated__c = true,isVerified__c = true);
        Financial_Statement__c fs4 = new Financial_Statement__c(Customer_Integration__c = cust2.Id, BRE_Record__c = true, isGrowthRecord__c =false,Year__c = '2019',Is_Estimated__c = false,isVerified__c = true);
        Financial_Statement__c fs5 = new Financial_Statement__c(Customer_Integration__c = cust2.Id, BRE_Record__c = true, isGrowthRecord__c =false,Year__c = '2019',Is_Estimated__c = true,isVerified__c = true);
        Financial_Statement__c fs6 = new Financial_Statement__c(Customer_Integration__c = cust2.Id, BRE_Record__c = true, isGrowthRecord__c =false,Year__c = '2020',Is_Estimated__c = true,isVerified__c = true);
        Financial_Statement__c fs7 = new Financial_Statement__c(Customer_Integration__c = cust2.Id, BRE_Record__c = true, isGrowthRecord__c =false,Year__c = '2020',Is_Estimated__c = false,isVerified__c = true);
        
        
        fsList.add(fs);
        fsList.add(fs1);
        fsList.add(fs2);
         fsList.add(fs3);
         fsList.add(fs4);
         fsList.add(fs5);
         fsList.add(fs6);
         fsList.add(fs7);
        insert fsList;
        BRE2APICallout.getMessage(la.Id);
        BRE2APICallout.createRequestAsyn(la.Id);
        lc.Banking_Status__c = 'Required';
        lc.LIP_Status__c = 'Required';
        lc.AIP_Status__c ='Required';
        lc.Financial_Details_Status__c = 'Required';
        lc.Salaried_Details_Status__c ='Required';
        lc.Other_Income_Status__c ='Required';
        lc.Obligation_Status__c  = 'Required';
        update lc;
        BRE2APICallout.generateRequest(la.Id);
    }
}