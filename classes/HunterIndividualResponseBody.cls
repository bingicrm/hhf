public class HunterIndividualResponseBody {	
	public String path {get;set;} 
	public Integer statusCode {get;set;} 
	public Payload payload {get;set;} 
	public String timestamp {get;set;} 
	public String acknowledgementId {get;set;}
	
    public class Warning {
		public Number_Z number_Z {get;set;}
		public Message message {get;set;} 
		public Values values {get;set;}
	}
	
	public class Scheme {
		public SchemeID schemeID {get;set;} 
		public Score score {get;set;} 
	}
    
	public class SchemeID {
		public Integer schemeId {get;set;} 
	}
	
	public class Message {
		public String message {get;set;} 
	}
	
	public class MatchSchemes {
		public Integer schemeCount {get;set;} 
		public Scheme scheme {get;set;}
	}
	
	public class RuleID {
		public String ruleID {get;set;} 
	}
	
	public class Values {
		public List<Value> value {get;set;} 
	}
	
	public class Rules {
		public Integer totalRuleCount {get;set;} 
		public List<Rule> rule {get;set;} 
	}
	
	public class Number_Z {
		public Integer number_Z {get;set;} // in json: number
	}
    
	public class MatchSummary {
		public String matches {get;set;} 
		public TotalMatchScore totalMatchScore {get;set;} 
		public Rules rules {get;set;} 
		public MatchSchemes matchSchemes {get;set;} 
	}
	
	public class TotalMatchScore {
		public String totalScore {get;set;} 
	}
	
	public class Score {
		public Integer score {get;set;}
	}
	
	public class ResultBlockObject {
		public MatchSummary matchSummary {get;set;} 
		public ErrorWarnings errorWarnings {get;set;}
	}
	
	public class Value {
		public Integer value {get;set;}
	}
	
	public class Payload {
		public ResultBlockObject resultBlockObject {get;set;}
	}
	
	public class Rule {
		public Integer ruleCount {get;set;} 
		public String isGlobal {get;set;} 
		public RuleID ruleID {get;set;} 
		public Score score {get;set;}
	}
	
	public class Warnings {
		public Integer warningCount {get;set;} 
		public Warning warning {get;set;}
	}
	
	public class ErrorWarnings {
		public Warnings warnings {get;set;}
	}
}