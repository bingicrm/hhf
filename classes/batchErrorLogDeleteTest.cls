@isTest 
public class batchErrorLogDeleteTest 
{
    static testMethod void testMethod1() 
    {
        List<Error_Log__c> lstError= new List<Error_Log__c>();
        for(Integer i=0 ;i <200;i++)
        {
            Error_Log__c err = new Error_Log__c();
            err.Request__c='';
            err.Response__c='';
            err.Type__c='System.CalloutException';
            err.Slack_Trace__c='xyz';
            lstError.add(err);
        }
        
        insert lstError;
                 Account Cob= new Account(PAN__c='AXEGA3767A',Name='TestCustomer',Phone='9992345333',
                                  Date_of_Incorporation__c=Date.newInstance(2018,11,11));
         
         Database.insert(Cob);                
                            
         Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                     Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                     Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                     Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                     Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
                            
         Database.insert(sc); 
                 Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,
                                                           StageName__c='Customer Onboarding',
                                                           Sub_Stage__c='Tranche_File_Check',
                                                           Transaction_type__c='SC',Requested_Amount__c=10000,
                                                           Applicant_Customer_segment__c='Salaried',
                                                           Branch__c='test',Line_Of_Business__c='Open Market',
                                                           Assigned_Sales_User__c=UserInfo.getUserId(),
                                                           Loan_Purpose__c='20',
                                                           Requested_EMI_amount__c=1000,Approved_ROI__c=5,
                                                           Requested_Loan_Tenure__c=5,
                                                           Government_Programs__c='PMAY',
                                                           Loan_Engine_2_Output__c='STP',
                                                           Property_Identified__c=True,ownerId=UserInfo.getUserId(),
                                                           comments__c = 'test'
                                                           );
                                                           
                                                           
        
       
        Database.insert(LAob);
        
        Test.startTest();

            batchErrorLogDelete obj = new batchErrorLogDelete();
            DataBase.executeBatch(obj); 
            DeleteErrorLogRecords sh1 = new DeleteErrorLogRecords();      
            String sch = '0  00 1 3 * ?';
            system.schedule('Test', sch, sh1);
            
            batchCommentsUpdate obj1 = new batchCommentsUpdate();
            DataBase.executeBatch(obj1); 
            
        Test.stopTest();
    }
}