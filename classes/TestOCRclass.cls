@isTest
public class TestOCRclass {
@TestSetup
    static void setup()
    {
        Account acc=new Account();
        acc.name='TestAccount1';
        acc.phone='9234567667';
        acc.PAN__c='fjauy0916u';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        System.debug(acc.id);
        
        Scheme__c sc=new Scheme__c();
        sc.name='TestScheme';
        sc.Scheme_Code__c='testcode';
        sc.Scheme_Group_ID__c='23131';
        sc.Product_Code__c='HL';
        insert sc;
        System.debug(sc.id);
        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Transaction_type__c='PB';
        lap.Scheme__c=sc.id;
        insert lap;
        System.debug(lap.id);
        
        City__c city=new city__c();
        city.City_ID__c='100000';
        city.Name='TEZPOOR';
        city.Country__c='1';
        city.State__c='5';
        insert city;
        Pincode__c pin=new Pincode__c();
        pin.City_LP__c=city.id;
        pin.ZIP_ID__c='46172';
        insert pin;
        list<Loan_Contact__c> lc=[select id, name from Loan_Contact__c where Loan_Applications__c=:lap.id limit 1];
        system.debug('loan contact'+lc[0]);
        Address__c address=new Address__c();
        address.Address_Line_1__c='abc Street';
        address.Address_Line_2__c='abc Area';
        address.Pincode_LP__c=pin.id;
        address.Type_of_address__c='PERMNENT';
        address.Loan_Contact__c=lc[0].id;
        //insert address;
        //list<customer_integration__c> ci=[select id, name from loan_A where loan_application__c=:lap.id limit 1 ];
        system.debug('loan contact'+lc[0]);
        customer_integration__c ci =new customer_integration__c();
        ci.Loan_Application__c  =lap.id;
        ci.Loan_Contact__c=lc[0].id;
        insert ci;
        id recTypeId = Schema.SObjectType.OCR_Information__c.getRecordTypeInfosByName().get('Aadhaar').getRecordTypeId();
        OCR_Information__c ocr =new OCR_Information__c();
        ocr.Aadhaar_Number__c='123456787976';
        ocr.Loan_Application__c=lap.id;
        ocr.Loan_Contact__c=lc[0].id;
        ocr.RecordTypeId=recTypeId;
        insert ocr;
    }
    @isTest
    public static void Testsaverec(){
         list<Loan_Application__c> la = [select id,Name,Loan_Application_Number__c from Loan_Application__c limit 1];
        List<loan_contact__c> lc= [select id ,name from  loan_contact__c limit 1];
        list<OCR_Information__c> ocr=[select Id, Aadhaar_Number__c,Acknowledgement_Number__c,Address__c,Constitution_of_Business__c,Date_of_Birth__c,Date_of_Filing__c,
                                 Date_of_Liability__c,Deduction_Under_Section_VI_A__c,Email_ID__c,Expiry_Date__c,Father_s_Name__c,Gender__c,
                                 Gross_Total_Income__c,GSTIN_Number__c,Issue_Date__c,Legal_Guardian__c,Loan_Application__c,Loan_Contact__c,
                                 Mobile_Numer__c,Mother_Name__c,Name_of_Spouse__c,Name_on_Document__c,Nationality__c,PAN_Number__c,Passport_Number__c,
                                 Period_of_validity__c,Place_of_Issue__c,Registration_Type__c,Taxable_income__c,Total_Tax_Interest_Payable__c,
                                 Total_Tax_Paid__c,Trade_Name__c,Voter_ID_Number__c,Ward_Circle_Number__c,Year_of_Assessment__c, RecordType.Name
                                 FROM OCR_Information__c];
        string base64Data='base64Data';

         TestMockRequest req1=new TestMockRequest(100,
                                                 'Complete',
                                                 JSON.serialize(base64Data),
                                                 null);
                Test.setMock(HttpCalloutMock.class,req1);
                
            
                Test.startTest();
               OCRClass.saverec(lc[0].id, base64Data, lc[0].id, la[0].id, 'Voter ID', 'Front');       
                Test.stopTest();
        OCRClass.saverec(ocr[0].id, base64Data, lc[0].id, la[0].id, 'Voter ID', 'Front');
        
    }
}