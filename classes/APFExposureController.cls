public class APFExposureController {

    @AuraEnabled
    public static String checkEligibility(Id loanApplicationId) {
        if(loanApplicationId != null) {
            Loan_Application__c objLA = [Select Id, Name, APF_Exposure_Initiated__c, APF_Loan__c from Loan_Application__c Where Id=: loanApplicationId LIMIT 1];
            if(objLA.APF_Loan__c == false) {
                return 'APF Exposure cannot be calculated on a Non-APF Loan.';
            }
            
            else if(objLA.APF_Loan__c == true && objLA.APF_Exposure_Initiated__c == true) {
                 return 'APF Exposure already executed.';
            }
            
            else if(objLA.APF_Loan__c == true && objLA.APF_Exposure_Initiated__c == false) {
                APFExposureCallout.makeAPFExposureCallout(loanApplicationId);
                return 'APF Exposure request submitted. No records matched for the given APF Id';
            }
            
        }
        return 'Success';
    }
}