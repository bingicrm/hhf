global class cls_DeleteDocumentsBatch implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {  
          return Database.getQueryLocator('Select Id, Name, Document_Id__c, Document_Checklist__c, Document_Checklist__r.Loan_Contact__c, Document_Checklist__r.Loan_Contact__r.Applicant_Type__c, File_Name_If_Attachment__c, Loan_Application__c, Successfully_Migrated__c, Loan_Application__r.Folders_Created_in_S3__c, Loan_Application__r.Name, Old_Release_Data__c From Document_Migration_Log__c Where Old_Release_Data__c = false AND Successfully_Migrated__c = True AND Loan_Application__r.Folders_Created_in_S3__c = true AND (Loan_Application__r.Sub_Stage__c = \'Loan Cancel\' OR Loan_Application__r.Sub_Stage__c = \'Loan reject\' OR Loan_Application__r.Sub_Stage__c = \'Loan Disbursed\')');
    
    }
    global void execute(Database.BatchableContext BC, List<Document_Migration_Log__c> lstMigratedDocLogs)
    {   
        if(!lstMigratedDocLogs.isEmpty()) {
            System.debug('Debug Log for lstMigratedDocLogs' +lstMigratedDocLogs.size());
            for(Document_Migration_Log__c objDML : lstMigratedDocLogs) {
                if(String.isNotBlank(objDML.Loan_Application__c) && String.isNotBlank(objDML.Document_Checklist__c) && String.isNotBlank(objDML.Document_Checklist__r.Loan_Contact__c)) {
                    System.debug('Debug Log for applicant Type'+objDML.Document_Checklist__r.Loan_Contact__r.Applicant_Type__c);
                    if(objDML.Document_Checklist__r.Loan_Contact__r.Applicant_Type__c == 'Applicant') {
                        cls_DeleteMovedFiles.deleteS3FilesApplicant(objDML);
                    }
                    else if(objDML.Document_Checklist__r.Loan_Contact__r.Applicant_Type__c == 'Co- Applicant' || objDML.Document_Checklist__r.Loan_Contact__r.Applicant_Type__c == 'Guarantor') {
                        cls_DeleteMovedFiles.deleteS3FilesCoApplicantGuarantor(objDML);
                    }
                }
                else if(String.isNotBlank(objDML.Loan_Application__c) && String.isNotBlank(objDML.Document_Checklist__c) && String.isBlank(objDML.Document_Checklist__r.Loan_Contact__c)) {
                    cls_DeleteMovedFiles.deleteS3FilesApplication(objDML);
                }
                else if(String.isNotBlank(objDML.Loan_Application__c) && String.isBlank(objDML.Document_Checklist__c)) {
                    List<Attachment> lstAttachment = [Select Id, Name, ParentId from Attachment Where ParentId =: objDML.Loan_Application__c  AND Id =: objDML.Document_Id__c];
                    System.debug('Debug Log for lstAttachment'+lstAttachment.size());
                    if(!lstAttachment.isEmpty()) {
                        cls_DeleteMovedFiles.deleteS3FilesLoanKit2(objDML);
                    }
                    else {
                        cls_DeleteMovedFiles.deleteS3FilesLoanKit(objDML);
                    }
                }
            }
        }
    }
    global void finish(Database.BatchableContext BC)
    {
        AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors,JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
          
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Document Deletion Batch' + a.Status);
            mail.setPlainTextBody('During Deletion of Documents, records processed ' + a.TotalJobItems +
           'with '+ a.NumberOfErrors + ' failures.');
        if(!test.isrunningTest()){
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
}