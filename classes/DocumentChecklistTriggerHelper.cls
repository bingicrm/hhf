/*=====================================================================
 * Deloitte India
 * Name:DocumentChecklistTriggerHelper
 * Description: This class is a helper class and supports Document Checklist Trigger handler for insert and update methods.
 * Created Date: [20/06/2018]
 * Created By:Shwetadri Ghosh(Deloitte India)
 *
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/
public class DocumentChecklistTriggerHelper{

    @Future(callout = true)
    public static void completeOCR(Set<ID> docLSTid){
        blob blobfile;
        Id recId;
        Id loanContact;
        Id loanApplication;
        String doc;
        String subDoc;
        List<Document_Checklist__c> docChkLst = new List<Document_Checklist__c>();
        if(docLSTid != null){
            docChkLst = [SELECT Id,Loan_Contact__c, Loan_Applications__c, Document__c, Sub_Document__c FROM Document_Checklist__c
                          WHERE Id IN : docLSTid];
        }
        List<OCR_Information__c> ocrs = new List<OCR_Information__c>();
        for(Document_Checklist__c docChkLstItem : docChkLst){
            recId = docChkLstItem.Id;
            loanContact = docChkLstItem.Loan_Contact__c;
            loanApplication = docChkLstItem.Loan_Applications__c;
            doc = docChkLstItem.Document__c;
            subDoc  = docChkLstItem.Sub_Document__c;
            ContentDocumentLink dcl =[SELECT ContentDocumentId  
                                      FROM ContentDocumentLink 
                                      WHERE LinkedEntityId =:docChkLstItem.Id limit 1];
            ContentVersion lstDocumentVersion = [SELECT Id,ContentDocumentId,FileExtension,Title,VersionData 
                                                 FROM ContentVersion 
                                                 WHERE ContentDocumentId = :dcl.ContentDocumentId limit 1];
            
            blobfile = lstDocumentVersion.VersionData;            
            String blob64Data = EncodingUtil.base64Encode(blobfile);
            OCR_Information__c dataOCR = OCRclass.saverec(recId,blob64Data,loanContact,loanApplication,doc,subDoc);            
            if(dataOCR != NULL){
                ocrs.add(dataocr);
            }            
        }
        if(ocrs.size()>0){            
            upsert ocrs;
        }
    }


 @Future(callout = true)
public static void OTCIntegration(list<id> docList){

Rest_Service__mdt serviceDetails=[select Client_Password__c,Client_Username__c,
       Request_Method__c,Time_Out_Period__c, Service_EndPoint__c from Rest_Service__mdt where MasterLabel='PddOtc'];
       for(id docChec: docList){
            OtcPddIntegration.IntegrateOTC(docChec);
       }
      


}


    public static void DocumentMapping(list<Document_checklist__c> newList, map<id,Document_checklist__c> oldMap){
/*
     * @Description :- This method checks if the mapping matches with the mapping saved in the system for the document to document type
     * @param :- Trigger.New, trigger.oldMap
     * @returns :- void
     * Created By:Sonali Kansal(Deloitte India)
     */
         list<id> docList = new list<id>();
       list<id> doctypeList = new list<id>();
        for(Document_Checklist__c dc: newList){
              Document_Checklist__c old = oldMap.get(dc.Id);
            if ((dc.Document_Master__c != old.Document_Master__c) || (dc.Document_Type__c != old.Document_Type__c)) {
                //ids.add(lc);
                 doctypeList.add(dc.document_type__c);
                 docList.add(dc.Document_Master__c);
            }
        }
         list<DocumentType_Document_Mapping__c> docMap = new list<DocumentType_Document_Mapping__c>();
         docMap = [select Document_Master__c,Document_Type__c from DocumentType_Document_Mapping__c where Document_Type__c in :doctypeList or
         Document_Master__c in :docList ];
         system.debug('====doc==='+docMap);
            map<id,list<id>> mapDoc = new map<id, list<id>>();
              for (DocumentType_Document_Mapping__c up: docMap) {
            if (mapDoc.containsKey(up.Document_Type__c)) {
                //user nw = new user(u);
                mapDoc.get(up.Document_Type__c).add(up.Document_Master__c);
            } else {
                list < id > dtlist = new list < id > ();
                dtlist.add(up.Document_Master__c);
                mapDoc.put(up.Document_Type__c, dtlist);
            }
        }
        system.debug('==mapDoc=='+mapDoc);
         for(Document_Checklist__c dc :newList){
                if(mapDoc.containskey(dc.Document_Type__c)){
                   list<id> dtid= mapDoc.get(dc.Document_Type__c);
                   if(!dtid.contains(dc.Document_Master__c)){

                      dc.addError('Please select the Document specific to the Document Type.');
                   }

                }
          }
    }
    
    public static void createPropertyDocChecklist(List<Document_Checklist__c> newList){
        List<Property_Document_Checklist__c> newPropDocChecklist = new List<Property_Document_Checklist__c>();

        for(Document_Checklist__c docCheckLst : newList){
                Property_Document_Checklist__c propDocChkLst = new Property_Document_Checklist__c();
                
                propDocChkLst.Document_Checklist__c = docCheckLst.Id;
                propDocChkLst.Loan_Application__c = docCheckLst.Loan_Applications__c;
                System.debug('$$$$$$$$$$$' + propDocChkLst);
                newPropDocChecklist.add(propDocChkLst);
        }
        if(newPropDocChecklist.size()>0){
                insert newPropDocChecklist;
        }
    }
    public static void docUploaded(List<Document_Checklist__c> docChkList){
        
        List<ContentDocumentLink> conList = new List<ContentDocumentLink>();
        List<ContentDocumentLink> conCountList = new List<ContentDocumentLink>();
        List<Id> idList = new List<Id>();
        
        for(Document_Checklist__c d: docChkList){
            idList.add(d.Id);
        }
        if(idList.size()>0){
            conList = [Select Id, LinkedEntityId, ContentDocument.FileType from ContentDocumentLink where LinkedEntityId IN: idList];
        }
        Map<Id, List<ContentDocumentLink>> docToContentMap = new Map<Id, List<ContentDocumentLink>>();
        for(ContentDocumentLink con: conList){
            if (!docToContentMap.containsKey(con.LinkedEntityId)) {
               docToContentMap.put(con.LinkedEntityId, new List<ContentDocumentLink>{con});                
            }else{
                docToContentMap.get(con.LinkedEntityId).add(con);              
            }
        }
        
        for(Document_Checklist__c doc: docChkList){
            system.debug('~~INSIDE docChkLIST~~');
            if(doc.Status__c == 'Uploaded'){
                system.debug('~~INSIDE IF~~');
                if(docToContentMap.size()>0){
                    for(ContentDocumentLink c: docToContentMap.get(doc.Id)){
                        system.debug('~~INSIDE FOR~~');
                        conCountList.add(c);
                    }
                }
                system.debug(conCountList.size());
                if(conCountList.size()==0){
                    system.debug('SIZE is 0');
                    doc.addError(Constants.Minimum_Image_Error);
                }
            }
        }
    }
	 //Below method added by Abhilekh on 3rd June 2019 for TIL-794
    public static void checkAttachmentOnUpload(List<Document_Checklist__c> lstDCUpload){
        Set<Id> setDCId = new Set<Id>();
        List<ContentDocumentLink> lstAttachments = new List<ContentDocumentLink>();
        Integer attachmentCount = 0;
        
        for(Document_Checklist__c dc: lstDCUpload){
            setDCId.add(dc.Id);
        }
        
        if(setDCId.size() > 0){
            lstAttachments = [SELECT Id,LinkedEntityId from ContentDocumentLink WHERE LinkedEntityId IN: setDCId];
        }
        
        for(Document_Checklist__c dc: lstDCUpload){
            for(ContentDocumentLink attach: lstAttachments){
                if(dc.Id == attach.LinkedEntityId){
                    attachmentCount++;
                }
            }
            
            if(dc.Status__c == Constants.DOC_STATUS_UPLOADED && attachmentCount == 0){
                dc.addError('Please upload the document before marking the status to Uploaded.');
            }
            
            if((dc.Status__c == Constants.DOC_STATUS_PENDING /*|| dc.Status__c == Constants.DOC_STATUS_RECIEVED*/) && attachmentCount > 0){//Added by Saumya For OTC/PDD Changes for TIL 1246
                dc.addError('You cannot mark the status as Pending or Received if document has been uploaded.');
            }
        }
    }
}