public class CibilIndividualCallOutAPF {

    @AuraEnabled
    public static ResponseCIBILWrapper createCustomerIntegration(Id strRecordId, Boolean hitCIBILAgain)
    {
        System.debug('Debug Log for hitCIBILAgain'+hitCIBILAgain);
        if(String.isNotBlank(strRecordId)) {
            String sObjName = strRecordId.getSObjectType().getDescribe().getName();
            System.debug('Debug Log for sObjName'+sObjName);
            List<Project_Builder__c> lstProjBuilder = new List<Project_Builder__c>();
            List<Promoter__c> lstPromoter = new List<Promoter__c>();
            List<cls_Phone> phone = new List<cls_Phone>();
            
            if(sObjName == Constants.strProjectBuilderAPI) {
                //ITR_Aadhaar_Number__c,Passport_Number__c,Borrower__c
                lstProjBuilder = [Select Id, Name,Project__c,Address_Line_1__c, Address_Line_2__c, Gender__c, Pincode__c, CIBIL_CheckUp__c,CIBIL_Response__c, Retrigger_Individual_Cibil__c, pan_verification_status__c,CIBIL_Verified__c, PAN_Number__c,Date_of_Birth__c, Builder_Name__c, Builder_Name__r.Gender__c,Builder_Name__r.Name,Builder_Name__r.PAN__c, Voter_ID_Number__c,
                                         (Select Id, Name,Check_Expired__c From Builder_Integrations__r)
                                  From Project_Builder__c Where Id =: strRecordId LIMIT 1];
                System.debug('lstProjBuilder[0].CIBIL_Response__c!!'+lstProjBuilder[0].CIBIL_Response__c);
                if(lstProjBuilder[0].CIBIL_Response__c == true){
                    System.debug('lstProjBuilder[0].CIBIL_Response__c@@@'+lstProjBuilder[0].CIBIL_Response__c);
                    return new ResponseCIBILWrapper(false,'CIBIL has already been hit for response. Please try after some time.');
                }
                if(lstProjBuilder[0].pan_verification_status__c == false){
                    return new ResponseCIBILWrapper(false,'Please validate PAN Card before validating CIBIL.');
                }
                if(String.isBlank(lstProjBuilder[0].Address_Line_1__c) && String.isBlank(lstProjBuilder[0].Address_Line_2__c) && String.isBlank(lstProjBuilder[0].Pincode__c)) 
                {
                    return new ResponseCIBILWrapper(false,'Address not present. Please specify Address Line 1, Address Line 2, and Pincode, before proceeding.');
                }
                else if(lstProjBuilder[0].Gender__c == null) {
                    return new ResponseCIBILWrapper(false,'Gender not selected. Please specify Gender before proceeding.');
                }
                
                Boolean retriggerIndCIBIL = false; //Added by Abhilekh on 9th May 2019 for TIL-736
        
                
                Boolean CIBILExpired=false; //Added by Abhilekh on 9th May 2019 for TIL-736
                
                //Below if block added by Abhilekh on 9th May 2019 for TIL-736
                if(!lstProjBuilder[0].Builder_Integrations__r.isEmpty()){
                    for(Builder_Integrations__c CI: lstProjBuilder[0].Builder_Integrations__r){
                        if(CI.Check_Expired__c == 'Yes' && lstProjBuilder[0].CIBIL_Verified__c){
                            CIBILExpired = true;
                            break;
                        }
                    }    
                }
                
                if(!hitCIBILAgain){
    			//Below if block added by Abhilekh on 9th May 2019 for TIL-736
                    if(lstProjBuilder[0].CIBIL_Verified__c && CIBILExpired == false && (lstProjBuilder[0].Retrigger_Individual_CIBIL__c == false && retriggerIndCIBIL == false)){
                        return new ResponseCIBILWrapper(false,'CIBIL record already exists! Are you sure you want to initiate again?');
                    }
                }
                
                Builder_Integrations__c integrations = new Builder_Integrations__c();
                integrations.Project_Builder__c = lstProjBuilder[0].Id;
                integrations.Project__c = lstProjBuilder[0].Project__c;
                integrations.recordTypeId = Schema.SObjectType.Builder_Integrations__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId();
                
                try{
                    insert integrations;
                }catch(DMLException e) {
                    return new ResponseCIBILWrapper(false,e.getMessage().substringafter(','));
                }
    
                lstProjBuilder[0].CIBIL_CheckUp__c = true;
                lstProjBuilder[0].CIBIL_Verified__c = false; // Added by Abhilekh on 24th April 2019
                update lstProjBuilder[0];
                return new ResponseCIBILWrapper(true,integrations.Id);
                
            }
            
            else if(sObjName == Constants.strPromoterAPI) {
                lstPromoter = [Select Id, Name, Address_Line_1__c, Address_Line_2__c, Name_of_Promoter__c, Pincode__c, CIBIL_CheckUp__c,CIBIL_Response__c,Retrigger_Individual_Cibil__c, pan_verification_status__c, CIBIL_Verified__c, DOB__c, Gender__c, PAN_Number__c,
                                      (Select Id, Name,Check_Expired__c From Promoter_Integrations__r)
                                From Promoter__c Where Id =: strRecordId LIMIT 1];
                System.debug('lstPromoter[0].CIBIL_Response__c!!'+lstPromoter[0].CIBIL_Response__c);
                if(lstPromoter[0].CIBIL_Response__c == true){
                    System.debug('lstPromoter[0].CIBIL_Response__c@@@'+lstPromoter[0].CIBIL_Response__c);
                    return new ResponseCIBILWrapper(false,'CIBIL has already been hit for response. Please try after some time.');
                }
                if(lstPromoter[0].pan_verification_status__c == false){
                    return new ResponseCIBILWrapper(false,'Please validate PAN Card before validating CIBIL.');
                }
                if(String.isBlank(lstPromoter[0].Address_Line_1__c) && String.isBlank(lstPromoter[0].Address_Line_2__c) && String.isBlank(lstPromoter[0].Pincode__c))
                {
                    return new ResponseCIBILWrapper(false,'Address not present. Please specify Address Line 1, Address Line 2, and Pincode, before proceeding.');
                }
                else if(lstPromoter[0].Gender__c == null )
                {
                    return new ResponseCIBILWrapper(false,'Gender not selected. Please specify Gender before proceeding.');
                }
                
                Boolean retriggerIndCIBIL = false; //Added by Abhilekh on 9th May 2019 for TIL-736
        
                Boolean CIBILExpired=false; //Added by Abhilekh on 9th May 2019 for TIL-736
                
                //Below if block added by Abhilekh on 9th May 2019 for TIL-736
                if(!lstPromoter[0].Promoter_Integrations__r.isEmpty()){
                    for(Promoter_Integrations__c CI: lstPromoter[0].Promoter_Integrations__r){
                        if(CI.Check_Expired__c == 'Yes' && lstPromoter[0].CIBIL_Verified__c){
                            CIBILExpired = true;
                            break;
                        }
                    }    
                }
                
                if(!hitCIBILAgain){
    			//Below if block added by Abhilekh on 9th May 2019 for TIL-736
                    if(lstPromoter[0].CIBIL_Verified__c && CIBILExpired == false && (lstPromoter[0].Retrigger_Individual_CIBIL__c == false && retriggerIndCIBIL == false)){
                        return new ResponseCIBILWrapper(false,'CIBIL record already exists! Are you sure you want to initiate again?');
                    }
                }
                
                Promoter_Integrations__c integrations = new Promoter_Integrations__c();
                integrations.Promoter__c = lstPromoter[0].Id;
                integrations.recordTypeId = Schema.SObjectType.Promoter_Integrations__c.getRecordTypeInfosByName().get('CIBIL').getRecordTypeId();
                
                try{
                    insert integrations;
                }catch(DMLException e) {
                    return new ResponseCIBILWrapper(false,e.getMessage().substringafter(','));
                }
    
                lstPromoter[0].CIBIL_CheckUp__c = true;
                lstPromoter[0].CIBIL_Verified__c = false; // Added by Abhilekh on 24th April 2019
                update lstPromoter[0];
                return new ResponseCIBILWrapper(true,integrations.Id);
                
            }
            
        }
        return null;
    }
    
    @AuraEnabled            
    public static ResponseCIBILWrapper integrateCibil(Id strRecordId , Id customerIntegrationId ) {
        if(String.isNotBlank(strRecordId)) {
            Rest_Service__mdt restService = [ SELECT MasterLabel, QualifiedApiName , Service_EndPoint__c , 
                                                 Client_Password__c, Client_Username__c, Request_Method__c, Time_Out_Period__c  
                                          FROM   Rest_Service__mdt
                                          WHERE  MasterLabel = 'Cibil Individual'
                                        ];   
            Map<String,String> headerMap = new Map<String,String>();   
            
            cls_Header headRec = new cls_Header();
            cls_Name name = new cls_Name();
            List<cls_Address> address = new List<cls_Address>();
            cls_Id id = new cls_Id();
            List<cls_Phone> phone = new List<cls_Phone>();
            cls_Relation rel = new cls_Relation();
            cls_BRE breDetails = new cls_BRE();
                breDetails.StrategyTag = '';
                breDetails.ClientName = '';
                breDetails.OriginationSourceId = '';
                breDetails.requestId = '';
                breDetails.RequestedDate = System.now();
                breDetails.isBRERun = 'No';
                
            cls_Request req = new cls_Request(); 
            CibilRequest cbReq = new CibilRequest();   
        
            String sObjName = strRecordId.getSObjectType().getDescribe().getName();
            System.debug('Debug Log for sObjName'+sObjName);
            List<Project_Builder__c> lstProjBuilder = new List<Project_Builder__c>();
            List<Promoter__c> lstPromoter = new List<Promoter__c>();
            
            if(sObjName == Constants.strProjectBuilderAPI) {
                lstProjBuilder = [Select Id, Name,Project__c, Contact_Number_Mobile__c, Gender__c, Preferred_Communication_Address__c, City__c,Pincode__r.City_LP__r.State__c,ITR_Aadhaar_Number__c, Passport_Number__c, Address_Line_1__c, Address_Line_2__c, Pincode__c, Pincode__r.Name, CIBIL_CheckUp__c,CIBIL_Response__c, Retrigger_Individual_Cibil__c, pan_verification_status__c,CIBIL_Verified__c, PAN_Number__c,Date_of_Birth__c, Builder_Name__c, Builder_Name__r.Gender__c,Builder_Name__r.Name,Builder_Name__r.PAN__c, Voter_ID_Number__c,
                                         (Select Id, Name,Check_Expired__c From Builder_Integrations__r)
                                  From Project_Builder__c Where Id =: strRecordId LIMIT 1];
                
                if (lstProjBuilder[0].Pan_Number__c == null && lstProjBuilder[0].Voter_ID_Number__c == null && lstProjBuilder[0].ITR_Aadhaar_Number__c== null && lstProjBuilder[0].Passport_Number__c == null) {  //Edited by Abhilekh on 9th April 2019
                    return new ResponseCIBILWrapper(false,'You cannot proceed further, atleast one out of PAN, AADHAR, VoterId or Passport is mandatory');
                } 
                
                headRec.ApplicationId =  lstProjBuilder[0].Id;
                headRec.CustId = lstProjBuilder[0].Id;
                headRec.RequestType = 'REQUEST';
                headRec.RequestTime = String.valueOf(System.now());
                headRec.SFDCRecordId = customerIntegrationId;
                
                name.Name1 = String.isNotBlank(lstProjBuilder[0].Builder_Name__r.Name) ? lstProjBuilder[0].Builder_Name__r.Name : '';
                
                cls_Address add1 = new cls_Address();
                add1.Addresstype = getAddressType(lstProjBuilder[0].Preferred_Communication_Address__c);
                add1.AddressResidenceCode = 'OWNED';
                add1.AddressPin = lstProjBuilder[0].Pincode__r.Name;
                add1.AddressCity = lstProjBuilder[0].City__c;
                add1.Address = lstProjBuilder[0].Address_Line_1__c;
                add1.AddressState = getStateCode(lstProjBuilder[0].Pincode__r.City_LP__r.State__c);
                address.add(add1);
                
                id.VoterId = lstProjBuilder[0].Voter_ID_Number__c != null ? lstProjBuilder[0].Voter_ID_Number__c : '';
                id.PanId = lstProjBuilder[0].Pan_Number__c != null ? lstProjBuilder[0].Pan_Number__c : '';
                id.PassportId = lstProjBuilder[0].Passport_Number__c != null ? lstProjBuilder[0].Passport_Number__c : '' ;
                id.UidNo= lstProjBuilder[0].ITR_Aadhaar_Number__c != null ? lstProjBuilder[0].ITR_Aadhaar_Number__c : '';
                
                cls_Phone phone1 = new cls_Phone();
                phone1.PhoneNumber = lstProjBuilder[0].Contact_Number_Mobile__c;
                phone1.PhoneType = 'Mobile Phone';
                phone.add(phone1);
                
                req.LoanType = '2';
                req.LoanAmount = '0';
                req.SourceSystemName = constants.DIRECT;
                req.BureauRegion = constants.BureauRegion;
                req.Gender = lstProjBuilder[0].Gender__c;
                req.BirthDT = lstProjBuilder[0].Date_of_Birth__c != null ? getUpdatedDate(lstProjBuilder[0].Date_of_Birth__c) : '';
                req.name = name;
                req.address = address;
                req.id = id;
                req.phone = phone; 
                req.Relation = rel;
                req.bre = breDetails;  
                
                cbReq.Header = headRec;
                cbReq.Request = req;
                
            }
            
            else if(sObjName == Constants.strPromoterAPI) {
                lstPromoter = [Select Id, Name, Address_Line_1__c,City__c, Name_of_Promoter__c, Pincode__r.City_LP__c, Pincode__r.City_LP__r.State__c, Contact_No_Mobile__c, Preferred_Communication_Address__c, ITR_Aadhaar_Number__c, Passport_Number__c, Voter_ID_Number__c, Address_Line_2__c, Pincode__c, Pincode__r.Name, CIBIL_CheckUp__c,CIBIL_Response__c,Retrigger_Individual_Cibil__c, pan_verification_status__c, CIBIL_Verified__c, DOB__c, Gender__c, PAN_Number__c,
                                      (Select Id, Name,Check_Expired__c From Promoter_Integrations__r)
                                From Promoter__c Where Id =: strRecordId LIMIT 1];
                System.debug('lstPromoter[0].CIBIL_Response__c!!'+lstPromoter[0].CIBIL_Response__c);
                
                if (lstPromoter[0].Pan_Number__c == null && lstPromoter[0].Voter_ID_Number__c == null && lstPromoter[0].ITR_Aadhaar_Number__c== null && lstPromoter[0].Passport_Number__c == null) {  //Edited by Abhilekh on 9th April 2019
                    return new ResponseCIBILWrapper(false,'You cannot proceed further, atleast one out of PAN, AADHAR, VoterId or Passport is mandatory');
                }
                
                headRec.ApplicationId =  lstPromoter[0].Id;
                headRec.CustId = lstPromoter[0].Id;
                headRec.RequestType = 'REQUEST';
                headRec.RequestTime = String.valueOf(System.now());
                headRec.SFDCRecordId = customerIntegrationId;
                
                name.Name1 = String.isNotBlank(lstPromoter[0].Name_of_Promoter__c) ? lstPromoter[0].Name_of_Promoter__c : '';
                
                cls_Address add1 = new cls_Address();
                add1.Addresstype = getAddressType(lstPromoter[0].Preferred_Communication_Address__c);
                add1.AddressResidenceCode = 'OWNED';
                add1.AddressPin = lstPromoter[0].Pincode__r.Name;
                add1.AddressCity = lstPromoter[0].City__c;
                add1.Address = lstPromoter[0].Address_Line_1__c;
                add1.AddressState = getStateCode(lstPromoter[0].Pincode__r.City_LP__r.State__c);
                address.add(add1);
                
                id.VoterId = lstPromoter[0].Voter_ID_Number__c != null ? lstPromoter[0].Voter_ID_Number__c : '';
                id.PanId = lstPromoter[0].Pan_Number__c != null ? lstPromoter[0].Pan_Number__c : '';
                id.PassportId = lstPromoter[0].Passport_Number__c != null ? lstPromoter[0].Passport_Number__c : '' ;
                id.UidNo= lstPromoter[0].ITR_Aadhaar_Number__c != null ? lstPromoter[0].ITR_Aadhaar_Number__c : '';
                
                cls_Phone phone1 = new cls_Phone();
                phone1.PhoneNumber = lstPromoter[0].Contact_No_Mobile__c;
                phone1.PhoneType = 'Mobile Phone';
                phone.add(phone1);
                
                req.LoanType = '2';
                req.LoanAmount = '0';
                req.SourceSystemName = constants.DIRECT;
                req.BureauRegion = constants.BureauRegion;
                req.Gender = lstPromoter[0].Gender__c;
                req.BirthDT = lstPromoter[0].DOB__c != null ? getUpdatedDate(lstPromoter[0].DOB__c) : '';
                req.name = name;
                req.address = address;
                req.id = id;
                req.phone = phone; 
                req.Relation = rel;
                req.bre = breDetails;  
                
                cbReq.Header = headRec;
                cbReq.Request = req;
            }
            System.debug('Debug Log for cbReq'+cbReq);
            Blob headerValue = Blob.valueOf(restService.Client_Username__c + ':' + restService.Client_Password__c);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            headerMap.put('Authorization',authorizationHeader);
            headerMap.put('Username',restService.Client_Username__c);
            headerMap.put('Password',restService.Client_Password__c);
            headerMap.put('Content-Type','application/json');
            String jsonRequest = Json.serialize(cbReq,true);
            // Added by Vaishali for BRE
            system.debug('jsonRequest '+jsonRequest);
            // End of the code- Added by Vaishali
            HttpResponse res   = Utility.sendRequest(restService.Service_EndPoint__c,restService.Request_Method__c, jsonRequest , headerMap, Integer.valueOf(restService.Time_Out_Period__c) );
            system.debug('@@res'+res);
            if (res != null) {
                if( res.getStatusCode() == 400 )
                {
                    if(sObjName == Constants.strProjectBuilderAPI) {
                        lstProjBuilder[0].CIBIL_Response__c = true;
                        update lstProjBuilder[0];
                    }
                    
                    else if(sObjName == Constants.strPromoterAPI) {
                        lstPromoter[0].CIBIL_Response__c = true;
                        update lstPromoter[0];
                    }
                    
                    return new ResponseCIBILWrapper(false,'Invalid Request!! Contact system admin.');
                } 
                else if(res.getStatusCode() == 200)
                {
                    return new ResponseCIBILWrapper(true,'Request submitted!!');
                }
            }
            if(sObjName == Constants.strProjectBuilderAPI) {
                lstProjBuilder[0].CIBIL_Response__c = true;
                update lstProjBuilder[0];
            }
            
            else if(sObjName == Constants.strPromoterAPI) {
                lstPromoter[0].CIBIL_Response__c = true;
                update lstPromoter[0];
            }
            return new ResponseCIBILWrapper(false,'Some Error occured!! Contact system admin.');
            
        }
        return new ResponseCIBILWrapper(false,'Some Error occured!! Contact system admin.');
    }
    
    public class CibilRequest{
        public cls_Header Header;
        public cls_Request Request;
    }    
    class cls_Header {
    
        public String ApplicationId;    //
        public String CustId;   //
        public String RequestType;  //REQUEST
        public String RequestTime;
        public String SFDCRecordId;  //2017-02-02 18:40:19
    }    
    class cls_Request {
        public String Priority; //
        public String ProductType;  //
        public String LoanType; //
        public String LoanAmount;   //1000
        public String JointInd; //
        public String InquirySubmittedBy;   //
        public String SourceSystemName; //
        public String SourceSystemVersion;  //
        public String SourceSystemVender;   //
        public String SourceSystemInstanceId;   //
        public String BureauRegion; //
        public String LoanPurposeDesc;  //
        public String BranchIfsccode;   //
        public String Kendra;   //
        public String InquiryStage; //
        public String AuthrizationFlag; //
        public String AuthrizationBy;   //
        public String IndividualCorporateFlag;  //
        public String Constitution; //
        public cls_Name Name;
        public String Gender;   //
        public String MaritalStatus;    //
        public cls_Relation Relation;
        public Integer Age; //32
        public String AgeASonDT;    //02-02-2017
        public String BirthDT;  //02-02-2017
        public String NoOfDependents;   //3
        public List<cls_Address> Address;
        public cls_Id Id;
        public List<cls_Phone> Phone;
        public String EmailId1; //
        public String EmailId2; //
        public String Alias;    //
        public String ActOpeningDT; //02-02-2017
        public String AccountNumber1;   //
        public String AccountNumber2;   //
        public String AccountNumber3;   //
        public String AccountNumber4;   //
        public String Tenure;   //
        public String GroupId;  //
        public String NumberCreditCards;    //3
        public String CreditCardNo; //343223244
        public String MonthlyIncome;    //233
        public String SoaEmployerNameC; //
        public String TimewithEmploy;   //
        public String CompanyCategory;  //
        public String NatureOfBusiness; //
        public Double AssetCost;    //1000
        public Double Collateral1;  //
        public Double Collateral1Valuation1;    //
        public Double Collateral1Valuation2;    //
        public Double Collateral2;  //
        public Double Collateral2Valuation1;    //
        public Double Collateral2Valuation2;    //
        
        /* Added by Vaishali for BRE-1.1 */
        public cls_BRE bre;                        
       /*--------Added by Vaishali for BRE1.1 */
    }
    class cls_Name {
        public String Name1;    //
        public String Name2;    //
        public String Name3;    //
        public String Name4;    //
        public String Name5;    //
    }
    class cls_Relation {
        public String FatherName;   //
        public String SpouseName;   //
        public String MotherName;   //
        public String RelationType1;    //
        public String RelationType1Value;   //
        public String RelationType2;    //
        public String RelationType2Value;   //
        public String KeyPersonName;    //
        public String KeyPersonRelation;    //
        public String NomineeName;  //
        public String NomineeRelationType;  //
    }
    class cls_Address {
        public String AddressType;  //
        public String AddressResidenceCode; //98989
        public String Address;  //
        public String AddressCity;  //
        public String AddressPin;   //212111
        public String AddressState; //
    }
    class cls_Id {
        public String PanId;    //
        public String PanissueDate; //02-02-2017
        public String PanExpirationDate;    //02-02-2017
        public String PassportId;   //
        public String PassportIssueDate;    //02-02-2017
        public String PassportExpirationDate;   //02-02-2017
        public String VoterId;  //
        public String VoterIdIssueDate; //02-02-2017
        public String VoterIdExpirationDate;    //02-02-2017
        public String DrivingLicenseNo; //
        public String DriverLicenseIssueDate;   //2017-02-02 18:40:19
        public String DriverLicenseExpirationDate;  //2017-02-02 18:40:19
        public String UidNo;    //
        public String UniversalIdIssuedate; //02-02-2017
        public String UniversalIdExpirationDate;    //02-02-2017
        public String RationCard;   //
        public String RationCardIssueDate;  //02-02-2017
        public String RationCardExpirationDate; //02-02-2017
        public String IdType1;  //
        public String IdType1Value; //
        public String IdType2;  //
        public String IdType2Value; //
    }
    class cls_Phone {
        public String PhoneType;    //
        public String PhoneNumber;  //87878787878
        public String PhoneExtn;    //
        public String StdCode;  //
    }
    
    /*Added By Vaishali for BRE 1.1 */
     class cls_BRE {
        public String StrategyTag;
        Public String ClientName;
        public String OriginationSourceId;
        public String requestId;
        public DateTime RequestedDate;    
        public String isBRERun;
    } 
    /*-----------Added by Vaishali for BRE1.1 */
    
    public class ResponseCibil{
        public String ApplicationId;    //345582839222
        public String CstId;    //0020203030
        public String RequestType;  //REQUEST
        public String RequestTime;  //29042013 11:30:00
        public Integer AcknowledgementId;   //2222222
        public String Status;   //success
    }

    public static String getUpdatedDate( Date toUpdate)
    {
        DateTime dt =  Datetime.newInstance(toUpdate.year(), toUpdate.month(), toUpdate.day());
        return dt.format('dd/MM/YYYY');
    }

    public static String getAddressType( String addressType )
    {
        if( addressType == 'Permanent' || addressType == 'Both')
        {
            return 'PERMANENT';
        }
        else if( addressType == 'Residence Address')
        {
            return 'RESIDENCE';
        }
        else if( addressType == 'Office/ Business' )
        {
            return 'OFFICE';
        }
        return 'PERMANENT';
    }

    public static String getStateCode( String stateName )
    {
        List<CIBIL_State_Mapping__mdt> allState = [Select State_Code__c,State__c,LMS_Code__c,Id from CIBIL_State_Mapping__mdt where  LMS_Code__c = :stateName];
        if(allState.size() > 0 )
        {
            return allState[0].State__c;
        }
        return stateName;
    }
    public Class ResponseCIBILWrapper
    {
        @AuraEnabled
        public boolean failure;
        @AuraEnabled
        public String respString;
        public ResponseCIBILWrapper(boolean failure,String respString)
        {
            this.failure = failure;
            this.respString = respString;
        }

    }
}