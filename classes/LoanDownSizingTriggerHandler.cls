public class LoanDownSizingTriggerHandler 
{
    //method to move Loan Application to Tranche when Loandownsizing is rejected.
    public static void changeLoanApplicationStage(List<Loan_Downsizing__c> newList)
    {
        system.debug('LoanDownSizingTriggerHandler - changeLoanApplicationOwner '+newList);
        Set<ID> loanApplicationIDs = new Set<ID>();
        Set<ID> loanApplication_ApprovedLD_IDs = new Set<ID>();
        List<Loan_Application__c> loanApplicationsList = new List<Loan_Application__c>();
        List<Loan_Application__c> updatedLoanApplicationList = new List<Loan_Application__c>();
        
        if(newList != null && newList.size() > 0)
        {
            for(Loan_Downsizing__c ld : newList)
            {
                if(String.isNotBlank(ld.status__c))
                {
                    if(ld.status__c == 'Rejected')
                    {
                        loanApplicationIDs.add(ld.Loan_Application__c);     
                    }
                    else if(ld.status__c == 'Approved')
                    {
                        loanApplication_ApprovedLD_IDs.add(ld.Loan_Application__c);    
                    }
                }
            }
            system.debug('LoanDownSizingTriggerHandler -- changeLoanApplicationOwner-- loanApplicationIDs++ '+loanApplicationIDs);
            system.debug('LoanDownSizingTriggerHandler -- changeLoanApplicationOwner-- loanApplicationIDsloanApplication_ApprovedLD_IDs++ '+loanApplication_ApprovedLD_IDs);
            //Rejected Loan Dowsizing - Loan Application
            if(loanApplicationIDs.size() > 0)
            {
                loanApplicationsList = [Select Assigned_Sales_User__c From Loan_Application__c Where ID in :loanApplicationIDs];
                system.debug('loanApplicationsList '+loanApplicationsList);
                if(loanApplicationsList.size() > 0)
                {
                    system.debug('loanApplicationsList > 0');
                    Id recordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Tranche').getRecordTypeId();
                    
                    for(Loan_Application__c la : loanApplicationsList)
                    {
                        if(String.isNotBlank(la.Assigned_Sales_User__c))
                        {
                            system.debug('Inside Rejection');
                            updatedLoanApplicationList.add(new Loan_Application__c(ID=la.ID, OwnerID=la.Assigned_Sales_User__c, 
                                                                                   recordTypeID=recordTypeId, StageName__c='Tranche', Sub_Stage__c='Tranche Processing'));    
                        }
                        else
                        {
                            system.debug('No Sales Person available for this Loan Application');
                        }
                    }
                }
            }
            //Commented the below section - So that to remove the automatically movement of loan Application to Authorization stage if Loandownsizing is Approved.
            /*
            //Approved Loan Downsizing Application
            if(loanApplication_ApprovedLD_IDs.size() > 0)
            {
                loanApplicationsList = new List<Loan_Application__c>();
                
                loanApplicationsList = [Select StageName__c, Sub_Stage__c From Loan_Application__c Where ID in :loanApplication_ApprovedLD_IDs];
                for(Loan_Application__c la : loanApplicationsList)
                {
                    if(la.StageName__c=='Loan Downsizing' && la.Sub_Stage__c=='Downsizing Review') 
                    {
                        updatedLoanApplicationList.add(new Loan_Application__c(ID=la.ID, Sub_Stage__c='Downsizing Authorization'));    
                    }  
                }
            }
            */
            
            if(updatedLoanApplicationList.size() > 0)
            {
                system.debug('updatedLoanApplicationList++ '+updatedLoanApplicationList);
                updateRecords(updatedLoanApplicationList, false);    
            } 
        }// check on newList Size    
    } 
    
    
    private static void updateRecords(List<sObject> sObjectList, Boolean allOrNone)
    {
        if(sObjectList!=null && sObjectList.size() > 0 && allOrNone != null)
        {
            Database.SaveResult[] srList = Database.update(sObjectList,allOrNone);
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    System.debug('Successfully inserted Loan Application ' + sr.getId());
                }
                else {
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
    }
    
    /*List<Test__c> test_list = [SELECT Id, Name, Time_Stamp__c, Custom_Time__c FROM Test__c l WHERE CreatedDate = today];
    
    for(Test__c tests: test_list)
    {
        String my_time = tests.Time_Stamp__c.format('hh:mm a');
        tests.Custom_Time__c = my_time;
    }
    update test_list;*/
    
    /*public static void changeLoanTrancheStatus(List<Loan_Downsizing__c> newList)
    {
        List<Id> loanApplicationID = new List<Id>();
        List<Tranche__c> trancheList = new List<Tranche__c>();
        if(trancheList!=null && trancheList.size() > 0)
        {
            for(Tranche__c c:[Select Status__c From Tranche__c Where Loan_Application__c = :loanApplicationID])
            {
                c.Status__c='Cancelled';
                trancheList.add(c);
            }
            update trancheList;
        }   
    }*/
   
        
        
        
        
        /*if(newList!=null && newList.size()==1)
        {
            ID loanApplicationID = newList[0].Loan_Application__c;
            List<Tranche__c> updatedTrancheList = new List<Tranche__c>();
            List<Tranche__c> trancheList = new List<Tranche__c>();
            trancheList = [Select Status__c From Tranche__c Where Loan_Application__c = :loanApplicationID];
            
            if(trancheList!=null && trancheList.size() > 0)
            for(Tranche__c ta : trancheList)
                {
                 if(ta.Status__c!='Cancelled' && ta.Status__c!='Rejected' && ta.Status__c!='Disbursed'){
                  updatedTrancheList.add(new Tranche__c(ID=ta.ID, Status__c='Cancelled')); 
                }
            }
        }        
    }*/
    
    
    /*public static void changeLoanTrancheStatus(List<Tranche__c> newList)
    {
        if(newList!=null && newList.size()==1)
        {
            ID loanApplicationID = newList[0].Loan_Application__c;
            List<Tranche__c> updatedTrancheList = new List<Tranche__c>();
            List<Tranche__c> trancheList = new List<Tranche__c>();
            List<Disbursement__c> updatedDisburseList = new List<Disbursement__c>();
            List<Disbursement__c> disburseList = new List<Disbursement__c>();
            disburseList = [SELECT Tranche_Cancelled__c, Cheque_Number__c FROM Disbursement__c Where Loan_Application__c = :loanApplicationID];
            
            if(trancheList!=null && trancheList.size() > 0)
            for(Tranche__c ta : trancheList)
                {
                 if(ta.Status__c!='Cancelled' && ta.Status__c!='Rejected' && ta.Status__c!='Disbursed'){
                  updatedTrancheList.add(new Tranche__c(ID=ta.ID, Status__c='Cancelled')); 
                  updatedDisburseList.add(new Disbursement__c(ID=ta.ID, Tranche_Cancelled__c=true)); 
                 }
             }
        }        
    }*/
    // Added by Shashikant Method to stop Loan Downsizing creation if there is pre-existing Tranche in Progress.-- Code not bulkified.
    /*public static void checkValidLoanDownsizing(List<Loan_Downsizing__c> newList)
    {
        if(newList!=null && newList.size()==1)
        {
            ID loanApplicationID = newList[0].Loan_Application__c;
            List<Tranche__c> trancheList = new List<Tranche__c>();
            trancheList = [Select Status__c From Tranche__c Where Loan_Application__c = :loanApplicationID and Status__c !='Disbursed'];
            
            if(trancheList!=null && trancheList.size() > 0)
            {
                newList[0].addError('Existing Tranche in Progress. Please delete / disburse Tranche before initiating Loan Downsizing');    
            }
            
        }        
    }*/
    
    //Added by Shashikant - As part of LD
    public static void changeDocumentChecklistStatus(Loan_Downsizing__c loanDownsizing)
    {
        List<Document_Checklist__c> documentChecklistList = new List<Document_Checklist__c>();
        if(loanDownsizing!=null && (loanDownsizing.Status__c=='Cancelled' || loanDownsizing.Status__c=='Rejected'))
        {
            documentChecklistList = [Select Id, Status__c, Scan_Check_Completed__c From Document_Checklist__c Where Loan_Applications__c=:loanDownsizing.Loan_Application__c And Loan_Downsizing__c=:loanDownsizing.ID And (Status__c='Uploaded' OR Status__c='Received')  And Document_Master__r.Name='Downsizing Agreement' Order By SystemModStamp DESC Limit 1];
            system.debug('LoanDownSizingTriggerHandler -- changeDocumentChecklistStatus -- documentChecklistList-- '+documentChecklistList);
            if(documentChecklistList.size()==1)
            {
                documentChecklistList[0].Status__c='Waived Off';
                documentChecklistList[0].Scan_Check_Completed__c = TRUE;  
            }
            update documentChecklistList;
        }
    }
    
    // Added by KK for Cancellation of Tranche and Insertion of Downsizing Agreement on downsizing insertion: Code Begins
    public static void newDownsizingOperations(List<Loan_Downsizing__c> ldList){
        List<Id> idList = new List<Id>();
        List<Loan_Application__c> laList = new List<Loan_Application__c>();
        List<Tranche__c> tranchesToUpdate = new List<Tranche__c>();
        List<Tranche__c> trancheList = new List<Tranche__c>();
        List<Disbursement__c> disbList = new List<Disbursement__c>();
        List<Disbursement__c> disbToUpdate = new List<Disbursement__c>();
        List<Document_Checklist__c> dcUpdate = new List<Document_Checklist__c>();
        List<Document_Checklist__c> dcWaived = new List<Document_Checklist__c>();
        for(Loan_Downsizing__c ld: ldList){
            idList.add(ld.Loan_Application__c);
        }
        //laList = [Select Id, (Select Id, Name, Status__c from Tranches__r where Status__c != 'Cancelled' and Status__c != 'Rejected' and Status__c != 'Disbursed') from Loan_Application__c where Id =: idList];
        trancheList = [Select Id, Name, Loan_Application__c, Status__c from Tranche__c where Status__c != 'Cancelled' and Status__c != 'Rejected' and Status__c != 'Disbursed' and Loan_Application__c =: idList];
        
        if(trancheList.size()>0){
            for(Tranche__c t: trancheList){
                for(Loan_Downsizing__c ld: ldList){
                    if(ld.Loan_Application__c == t.Loan_Application__c){
                        system.debug('==TRANCHE NAME=='+t.Name);
                        t.Status__c = 'Cancelled';
                        tranchesToUpdate.add(t);
                    }
                }
            }
        }
        disbList = [Select Id,Name, Tranche_Cancelled__c, Tranche__c from Disbursement__c where Loan_Application__c =: idList];
        for(Disbursement__c d: disbList){
            for(Tranche__c t: tranchesToUpdate){
                if(d.Tranche__c == t.Id){
                    system.debug('==DISB NAME=='+d.Name);
                    d.Tranche_Cancelled__c = TRUE;
                    disbToUpdate.add(d);
                }
            }
        }
        dcUpdate = [Select Id, Status__c, Scan_Check_Completed__c, Tranche__c from Document_Checklist__c where Tranche__c != null and Loan_Applications__c =: idList];
        for(Tranche__c t: tranchesToUpdate){
            for(Document_Checklist__c dc: dcUpdate){
                if(dc.Tranche__c == t.Id){
                    dc.Status__c = 'Waived Off';
                    dc.Scan_Check_Completed__c = TRUE;
                    dcWaived.add(dc);
                }
            }
        }
        system.debug('==Updated Tranche Size=='+tranchesToUpdate.size());
        if(tranchesToUpdate.size()>0){
            system.debug('==INSIDE TRANCHE UPDATE==');
            database.update(tranchesToUpdate);
        }
        system.debug('==Updated Disbursement Size=='+disbToUpdate.size());
        if(disbToUpdate.size()>0){
            system.debug('==INSIDE DISB UPDATE==');
            database.update(disbToUpdate);
        }
        system.debug('==DC Size=='+dcWaived.size());
        if(dcWaived.size()>0){
            system.debug('==INSIDE DC UPDATE==');
            database.update(dcWaived);
        }
    }
    // Code Ends
}