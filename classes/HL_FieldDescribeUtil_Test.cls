@isTest
public class HL_FieldDescribeUtil_Test {
    public testmethod static void test1 () {
        
        Schema.DescribeFieldResult objDesc = Loan_Contact__c.Applicant_Status__c.getDescribe();
		Schema.sObjectField objField = objDesc.getSObjectField();

        Schema.DescribeFieldResult objDesc1 = Loan_Contact__c.Borrower__c.getDescribe();
		Schema.sObjectField objField1 = objDesc1.getSObjectField();

        HL_FieldDescribeUtil.getDependentOptionsImpl(objField, objField1);
    }
}