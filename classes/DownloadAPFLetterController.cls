public class DownloadAPFLetterController {
    @AuraEnabled
    public static String downloadAPFLetter(String projectId){
        Project__c objProject = [Select Id, Name from Project__c Where Id =: projectId];
        
        
        if(objProject != null) {
            PageReference pagePdf = new PageReference('/apex/APF_Letter');
            pagePdf.getParameters().put('id', projectId);
            
           /* List<Attachment> lstAPFLetterPrevious = [SELECT Id, ParentId, Name from Attachment where ParentId =: projectId AND Name = 'APFLetter.pdf'];
            if(!lstAPFLetterPrevious.isEmpty()) {
                delete lstAPFLetterPrevious;
            }*/
            
            Blob pdfPageBlob;
            if(Test.isRunningTest()){
                pdfPageBlob = Blob.valueOf('Unit.Test');

            }
            else{
                pdfPageBlob = pagePdf.getContent();
            }
            
            Attachment a = new Attachment();
            a.Body = pdfPageBlob;
            a.ParentID = projectId;
            a.Name = 'APFLetter.pdf';
            
            try {
                insert a;
                return 'Success';
            }
            
            catch(Exception e) {
                return 'Error';
            }
        }
        else {
            return 'Invalid Id';
        }
        
    }
}