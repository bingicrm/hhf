public without sharing class MoveToTrancheController 
{
    
    // Loads the Loan Application Information for display.
    @AuraEnabled
    public static StageInfoWrapper loadLoanApplicationStageInfo(ID loadApplicationID)
    {
        List<Loan_Application__c> loanApplicationList;
        if(loadApplicationID!=null)
        {
            loanApplicationList = [Select StageName__c,Sub_Stage__c From Loan_Application__c Where ID = :loadApplicationID]; 
            if(loanApplicationList.size()>0 && loanApplicationList[0].StageName__c=='Loan Downsizing' /*&& loanApplicationList[0].Sub_Stage__c=='Downsizing Checker' */)
            {
                return new StageInfoWrapper(loanApplicationList[0].StageName__c, loanApplicationList[0].Sub_Stage__c, 'Tranche', 'Tranche Processing','Loan Application Found');
                
            }
        }
        
        return new StageInfoWrapper('', '', '', '', 'No Such Loan Application record Found');
    }
    
    // updates the stage and Sub stage of loan application
    @AuraEnabled
    public static StageInfoWrapper loanApplicationStageChange(ID loadApplicationID)
    {
        List<Loan_Application__c> loanApplicationList;
        Loan_Application__c loanAppToBeUpdated;
        StageInfoWrapper stageInfo; 
        
        if(loadApplicationID!=null)
        {
            loanApplicationList = [Select StageName__c,Sub_Stage__c From Loan_Application__c Where ID = :loadApplicationID];  
            
            if(loanApplicationList.size()>0)
            {
                stageInfo = new StageInfoWrapper(loanApplicationList[0].StageName__c, loanApplicationList[0].Sub_Stage__c, 'Tranche', 'Tranche Processing','Loan Application Found');
                
                Id loanApplicationRecordTypeId = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Tranche').getRecordTypeId();

                loanAppToBeUpdated = new Loan_Application__c(ID=loanApplicationList[0].ID, StageName__c='Tranche', Sub_Stage__c='Tranche Processing', recordtypeId=loanApplicationRecordTypeId);
                Database.SaveResult sr =  Database.update(loanAppToBeUpdated);
                if(sr.isSuccess())
                {
                    system.debug('MoveToTrancheController - loanApplicationStageChange - Success');
                    stageInfo.message='Stage and Sub-Stage Successfully Changed to Tranche and Tranche Processing'; 
                    return stageInfo;
                }
                else
                {
                    system.debug('MoveToTrancheController - loanApplicationStageChange - false');
                    stageInfo.message='Error - Contact Administrator';  
                    return stageInfo;
                }
            }
        }
        system.debug('MoveToTrancheController - loanApplicationStageChange - Null loanApplicationID');
        return new StageInfoWrapper('', '', '', '', 'No Such Loan Application record Found');
    }// method ends  
    
    // Wrapper Declaration & Definition Start.
    public class StageInfoWrapper
    {
        @AuraEnabled
        public String currentStage;
        
        @AuraEnabled
        public String currentSubStage;
        
        @AuraEnabled
        public String nextStage;
        
        @AuraEnabled
        public String nextSubStage;
        
        @AuraEnabled
        public String message;
        
        //Constructor Starts
        
        public StageInfoWrapper(String currentStage, String currentSubStage, String nextStage, String nextSubStage, String message)
        {
            this.currentStage = currentStage;
            this.currentSubStage = currentSubStage;
            this.nextStage = nextStage;
            this.nextSubStage = nextSubStage;
            this.message = message;
        }
        // Constructor Ends.
    }
    // Wrapper Declaration End.
}