@isTest
Public class CRM_Case_SendEmailBatchableTest {
    	
    public static void testDataCreation(){
        DateTime currentDateTime=system.now();
        List<Case> casesList=new List<Case>();
        List<Case_TAT_Configuration__mdt> tatConfigListAll = [SELECT Category__c,Department__c,Dept_TAT__c,isActive__c,Request_Type__c,Sequence__c,Sub_Category__c,Total_TAT__c FROM Case_TAT_Configuration__mdt WHERE isActive__c = true AND Total_TAT__c<100 ORDER BY Dept_TAT__c DESC];
        BusinessHours caseBusinessHour = [SELECT Id FROM BusinessHours WHERE Name = 'HHF Business Hours' LIMIT 1];
        Map<String,Case_TAT_Configuration__mdt> tatConfigMap = new Map<String,Case_TAT_Configuration__mdt>();
        List<Integer> daysRemaining = new List<Integer>{2,1,-1};
        Map<String,Case_TAT_Configuration__mdt> caseTATConfig = new Map<String,Case_TAT_Configuration__mdt>();
        
         for(Case_TAT_Configuration__mdt tatConfig : tatConfigListAll){
          if(!tatConfigMap.containsKey(tatConfig.Department__c)){
            tatConfigMap.put(tatConfig.Department__c,tatConfig);
          }
        }
         
        List<Case_TAT_Configuration__mdt> tatConfigList = tatConfigMap.values();
        for(Case_TAT_Configuration__mdt tatConfig : tatConfigList ){
          for(Integer remDay : daysRemaining)
          {
              Case c=new Case();
              c.CreatedDate = currentDateTime;
              While(tatConfig.Total_TAT__c - ((System.BusinessHours.diff(caseBusinessHour.Id,c.CreatedDate,currentDateTime) / 3600000) / 24)>=remDay){
                c.CreatedDate = c.CreatedDate.addDays(-1);
              }
              c.Subject = 'Test Class Subject '+tatConfig.Department__c+remDay;
              c.Description = 'Test Description '+tatConfig.Department__c+remDay;
              c.Original_Case_Owner__c=UserInfo.getUserId();
              c.TAT__c=tatConfig.Total_TAT__c;
              c.Net_Days_Used1__c = tatConfig.Total_TAT__c - remDay;
              c.Department__c=tatConfig.Department__c;
              c.Origin='Walkin';
              c.Status='New';
              c.Category__c=tatConfig.Category__c;
              c.Sub_Category__c = tatConfig.Sub_Category__c; 
              c.BusinessHoursId = caseBusinessHour.Id;
              c.Assign_to_Self__c = false;
              casesList.add(c);
              caseTATConfig.put(c.Subject,tatConfig);
          }
        }
        insert casesList;

        for(Case caseRec : casesList)
        {
          if(caseRec.Department__c!=caseTATConfig.get(caseRec.Subject).Department__c){
            caseRec.Department__c=caseTATConfig.get(caseRec.Subject).Department__c;
          }
        }
        update casesList;
        //Making first CS department exit times same as entry times as the department change above taking today's date
       List<CRM_Department_Change__c> csDepList = [SELECT Id,Case__c,Case__r.Subject,CRM_Department__c,Entry_Time__c from CRM_Department_Change__c Where CRM_Department__c='CS' AND Exit_Time__c!=null AND Case__c IN :new Map<Id,Case>(casesList).keySet() ORDER BY CRM_Department__c];
        for(CRM_Department_Change__c depChange : csDepList){
            depChange.Exit_Time__c = depChange.Entry_Time__c;
            depChange.Days_Used__C  = 0;
        }
        update csDepList;
        
       List<CRM_Department_Change__c> updatedDepList = [SELECT Id,Case__c,Case__r.Subject,CRM_Department__c,Entry_Time__c from CRM_Department_Change__c Where Exit_Time__c=null AND Case__c IN :new Map<Id,Case>(casesList).keySet() ORDER BY CRM_Department__c];
       Integer tempInt = 0;
       for(CRM_Department_Change__c cdc : updatedDepList){
         cdc.Entry_Time__c = currentDateTime;
              While(caseTATConfig.get(cdc.Case__r.Subject).Dept_TAT__c - ((System.BusinessHours.diff(caseBusinessHour.Id,cdc.Entry_Time__c,System.today()) / 3600000) / 24)>daysRemaining[tempInt]){
                cdc.Entry_Time__c = cdc.Entry_Time__c.addDays(-1);
              }
             System.debug('61-->'+caseTATConfig.get(cdc.Case__r.Subject).Dept_TAT__c+'--'+daysRemaining[tempInt]);
             System.debug('65-->'+ cdc.Entry_Time__c);
              if(tempInt+1==3)
                tempInt=0;
              else
                tempInt = tempInt+1;
       }
       update updatedDepList;
    }
    @testSetup  static void setup(){
        testDataCreation();
    }

     @isTest
     public static void testTATandEmailsBatchLogic(){
        Test.startTest();
        Database.executeBatch(new CRM_Case_SendEmailBatchable(),200);
        /*for(CRM_Department_Change__c dep : [Select Id,CRM_Department__c,Case__r.Department_TAT__c,Days_Used__c from CRM_Department_Change__c]){
            System.debug('75--->'+dep.CRM_Department__c+'--'+dep.Case__r.Department_TAT__c+'--'+dep.Days_Used__c);
        }*/
        Test.stopTest();
     }
}