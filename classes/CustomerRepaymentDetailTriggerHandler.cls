public with sharing class CustomerRepaymentDetailTriggerHandler {
    public static void onBeforeInsert(List<PDC__c> lstPDCTriggerNew) {
    	Map<String,Integer> mapLoanAppIdtoPDCCount = new Map<String,Integer>();
    	Set<String> setLoanApplicationIds = new Set<String>();
    	for(PDC__c objPDC : lstPDCTriggerNew) {
    		if(String.isNotBlank(objPDC.Loan_Application__c)) {
    			setLoanApplicationIds.add(objPDC.Loan_Application__c);
    		}
    	}
    	System.debug('Debug Log for setLoanApplicationIds'+setLoanApplicationIds.size());
    	System.debug('Debug Log for setLoanApplicationIds'+setLoanApplicationIds);
    	
    	List<Loan_Application__c> lstLoanApplication = [SELECT 
    															Id,
    															Name,
    															(SELECT
    																	Id,
    																	Name,
    																	Loan_Application__c
																 FROM
																 		PDC__r
    															)
														 FROM
														 		Loan_Application__c
												 		 WHERE
												 		 		ID IN: setLoanApplicationIds
														];
		System.debug('Debug Log for lstLoanApplication'+lstLoanApplication.size());
		System.debug('Debug Log for lstLoanApplication'+lstLoanApplication);
		
		if(!lstLoanApplication.isEmpty()) {
			for(Loan_Application__c objLA : lstLoanApplication) {
				mapLoanAppIdtoPDCCount.put(objLA.ID,objLA.PDC__r.size());
			}
		}
		
		System.debug('Debug Log for mapLoanAppIdtoPDCCount'+mapLoanAppIdtoPDCCount.size());
		System.debug('Debug Log for mapLoanAppIdtoPDCCount'+mapLoanAppIdtoPDCCount);
		
		for(PDC__c objPDC : lstPDCTriggerNew) {
			if(String.isNotBlank(objPDC.Loan_Application__c) && mapLoanAppIdtoPDCCount.containsKey(objPDC.Loan_Application__c) && mapLoanAppIdtoPDCCount.get(objPDC.Loan_Application__c) >0) {
				objPDC.addError('More than 1 Record cannnot be created for a Single Loan Application.');
			}
		}
    }
}