@isTest
public class TestIMDTriggerHandler
{
 public static testMethod void method1()
 {
    UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        Profile p = [SELECT Id FROM Profile WHERE Name='Integration'];
        
         User u1 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser001@amamama.com',
                     Username = 'puser001@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
                     UserRoleId = r.id);
        insert u1;
        
       system.runAs(u1)
 {
  Account acc=new Account();
    acc.name='Applicant123';
    acc.phone='9234509786';
    acc.PAN__c='ADGPW4078E';
    acc.Date_of_Incorporation__c=date.today();
    insert acc;

System.debug(acc.id);

Scheme__c sc=new Scheme__c();
sc.name='Construction Finance';
sc.Scheme_Code__c='CF';
sc.Scheme_Group_ID__c='6';
sc.Product_Code__c='HL';
insert sc;
System.debug(sc.id);
  
Loan_Application__c lap=new Loan_Application__c ();
lap.Customer__c=acc.id;
lap.Scheme__c=sc.id;
lap.Requested_Amount__c = 50000000;
lap.Approved_Loan_Amount__c=4000000;
lap.IMD_approval_required__c = true;
lap.Transaction_Type__C = 'PB';
insert lap;

IMD__c imd = new IMD__c();
imd.Loan_Applications__c = lap.id;
imd.Cheque_Number__c = '930476';
imd.Amount__c = 5000;
imd.Payment_Mode__c = 'Q';
imd.Applicable_IMD__c = 8000;
imd.Cheque_Date__c = date.today();
imd.Receipt_Date__c = date.today();
imd.Cheque_Status__c = 'R';
insert imd;
 
imd.Cheque_Status__c = 'B';
imd.Bounce_Reason_ID__c = '11111';
imd.Bounce_Reason_Description__c = 'Non Technical';
update imd;
}

 }
}