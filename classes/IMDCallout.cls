public class IMDCallout {
    public IMDCallout(){
    }
    
@future(callout=true)
    public static void makeIMDCallout(ID objIMDRecordId) {
        
        System.debug('Debug Log for objIMDRecordId'+objIMDRecordId);
        
        if(String.isNotBlank(objIMDRecordId)) {
            List<IMD__c> lstIMD =   [SELECT 
                                            ID,
                                            Name,
                                            Amount__c,
                                            Bank_Master__r.Branch_Name__c,
                                            Bank_Master__r.Name,
                                            Bank_Master__r.Bank_Code__c,
                                            Bank_Master__r.MICR__c,
                                            Bank_Master__c,
                                            Bank_City__c,
                                            FT_Mode__c,
                                            API_Error__c,
                                            Cheque_ID__c,
                                            Cheque_Date__c,
                                            Cheque_Status__c,
                                            Cheque_Number__c,
                                            Receipt_Date__c,
                                            Agreement_ID__c,
                                            Payment_Mode__c,
                                            Loan_Applications__r.Loan_Number__c,
                                            Loan_Applications__r.Branch_Lookup__r.Office_Code__c,
                                            Loan_Applications__r.Scheme__r.Scheme_ID__c,
                                            Loan_Applications__r.Scheme__r.Product_Code__c
                                    FROM
                                            IMD__c
                                    WHERE
                                            ID =: objIMDRecordId
                                            LIMIT 1
                                    ];
            System.debug('Debug Log for lstIMD'+lstIMD);
            
            List<Loan_Contact__c> lstLoanCon = [Select id,LMS_Customer_ID__c, Customer__c, Customer__r.Customer_ID__c from Loan_Contact__c where loan_applications__c =:lstIMD[0].loan_applications__c AND Applicant_Type__c= 'Applicant'];
            
            if(!lstIMD.isEmpty()) {
                IMD__c objIMDforCallout = lstIMD[0];
                System.debug('Debug Log for objIMDforCallout'+objIMDforCallout);
                System.debug('Debug Log for objIMDforCallout.Cheque_Date__c'+objIMDforCallout.Cheque_Date__c);
                System.debug('Debug Log for objIMDforCallout.Receipt_Date__c'+objIMDforCallout.Receipt_Date__c);
                System.debug('Debug Log for objIMDforCallout.Payment_Mode__c'+objIMDforCallout.Payment_Mode__c);
                //Querying necessary fields from the IMD Record to be passed as parameters for the API Request Call.
                /*
                ***********************************Mapping Details***************************************
                ****************************Last Modified on 28 September 2k18***************************
                
                    API Request Param           :   Field in SFDC
      
                   "receiptAmount"              :   Amount__c field on Charge Detail object
                   "modeOfPayment"              :   Payment_Mode__c field on Charge Detail object
                   "chequeStatus"               :   Cheque_Status__c Field on Charge Detail object
                   "transactionCode"            :   Will always be "1"
                   "chequeDate"                 :   formatDate(objIMDforCallout.Cheque_Date__c)
                   "chequeNumber"               :   Cheque_Number__c field on Charge Detail object
                   "inFavourOf"                 :   "HHFL HL" Always // LIMIT 10 in live environment, so "HHFL HL"
                   "payableAt"                  :   First 3 characters of MICR code from PDC Bank Master
                   "dealingBank"                :   "113" - This is Temporary Hardcoded, will be considered for future release.
                   "reason"                     :   Always be "IMD Cheque" 
                   "homeBranch"                 :   Office code field on Associated Branch of the Loan Application - objIMDforCallout.Loan_Applications__r.Branch_Lookup__r.Office_Code__c
                   "receiptDate"                :   formatDate(objIMDforCallout.Receipt_Date__c) - "Receipt Date" field in SFDC
                   "requestedForFlag"           :   Always "N"
                   "requestId"                  :   LENGTH : 4, PREVIOSLY: "RecordId" field for the IMD Record in sfdc, NOW : Unique and random String for each request. Obtained by calling the generateRandomRequestID() method of Utility Class.
                   "agreementId"                :   Loan_Number__c field from parent Loan Application record in SFDC 
                   "loanNumber"                 :   <!--TBD--> - From SFDC - For now, pass a random string
                   "bpId"                       :   Loan_Number__c field from parent Loan Application record in SFDC
                   "bpType"                     :   Masters are provided, but for IMD, it will always be "LS"
                   "receiptChannel"             :   <!--TBD-->
                   "ftMode"                     :   Masters have been provided, FT Mode Field in SFDC
                   "agency"                     :   <!--TBD-->
                   "receiptNo"                  :   <!--TBD-->  Utility.generateRandomRequestID();
                   "drawnOn"                    :   <!--TBD-->
                   "cashAc"                     :   <!--TBD-->
                   "cardCategory"               :   <!--TBD-->
                   "cardNo"                     :   <!--TBD-->
                   "expiryDate"                 :   <!--TBD-->
                   "approvalCode"               :   <!--TBD-->
                   "cardHolderName"             :   <!--TBD-->
                   "allocationType"             :   Always "N"
                   "branch"                     :   <!--TBD-->
                   "chargeId"                   :   Always "56"
                   "imdFlag"                    :   Always "Y"
                   "sourceId"                   :   Always "SFDC-HFL"
                   "productFlag"                :   <!--TBD-->Required in Production org, so "Y"
                   "schemeId"                   :   <!--TBD-->Required in Production org, so "123"
                
                
                ***********************************Mapping Details***************************************
                */
                ResponseBody objResponseBody = new ResponseBody();
                //Querying the request parameters from the corresponding Metadata record.
                List<Rest_Service__mdt> lstRestService = [SELECT 
                                                        MasterLabel, 
                                                        QualifiedApiName, 
                                                        Service_EndPoint__c, 
                                                        Client_Password__c, 
                                                        Client_Username__c, 
                                                        Request_Method__c, 
                                                        Time_Out_Period__c  
                                                  FROM   
                                                        Rest_Service__mdt
                                                  WHERE  
                                                        DeveloperName = 'IMDCallout'
                                                        LIMIT 1
                                                ]; 
                System.debug('Debug Log for lstRestService'+lstRestService);
                System.debug('Debug Log for MICR Code'+objIMDforCallout.Bank_Master__r.MICR__c);
                
                if(!lstRestService.isEmpty()) {
                    //Setting up request headers.
                    /*Blob headerValue = Blob.valueOf('admin' + ':' + 'admin');
                    String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                    Map<String,String> headerMap = new Map<String,String>();
                    headerMap.put('Content-Type','application/json');
                    headerMap.put('Authorization',authorizationHeader);
                    System.debug('Debug Log for headerMap'+headerMap);*/
                    
                    Blob headerValue = Blob.valueOf(lstRestService[0].Client_Username__c + ':' + lstRestService[0].Client_Password__c);
                      String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                      Map<String,String> headerMap = new Map<String,String>();
                      headerMap.put('Authorization',authorizationHeader);
                      headerMap.put('Username',lstRestService[0].Client_Username__c);
                      headerMap.put('Password',lstRestService[0].Client_Password__c);
                      headerMap.put('Content-Type','application/json');
                         
                    //Create instance of RequestBody to initiate the Request Body components.
                     RequestBody objRequestBody = new RequestBody();
                        objRequestBody.receiptAmount = objIMDforCallout.Amount__c != null ? String.valueOf(objIMDforCallout.Amount__c) : '';//'2510'
                        objRequestBody.modeOfPayment = objIMDforCallout.Payment_Mode__c != null ? objIMDforCallout.Payment_Mode__c : 'Q'; //Will always be Q for cheque.
                        objRequestBody.chequeStatus =  objIMDforCallout.Cheque_Status__c != null ? objIMDforCallout.Cheque_Status__c : 'R';// 'R'; Will always be R for cheque.
                        objRequestBody.transactionCode = '1'; //Will always be "1"
                        
                        objRequestBody.chequeDate = formatDate(objIMDforCallout.Cheque_Date__c);
                        
                        
                        objRequestBody.chequeNumber = objIMDforCallout.Cheque_Number__c != null ? String.valueOf(objIMDforCallout.Cheque_Number__c) : '';//'454455'
                        objRequestBody.inFavourOf = 'HHFL HL'; // Will always be Hero Housing.
                        
                        objRequestBody.payableAt = objIMDforCallout.Bank_Master__c != null && objIMDforCallout.Bank_Master__r.MICR__c != null  && objIMDforCallout.Bank_Master__r.MICR__c.length() == 9 ? objIMDforCallout.Bank_Master__r.MICR__c.subString(0,3) : '';
                        objRequestBody.dealingBank = '113';//objIMDforCallout.Bank_Master__r.Bank_Code__c != null ? String.valueOf(objIMDforCallout.Bank_Master__r.Bank_Code__c) : '';  
                        objRequestBody.reason = 'IMD Cheque'; // Will always be IMD Cheque.
                        objRequestBody.homeBranch = objIMDforCallout.Loan_Applications__c != null && objIMDforCallout.Loan_Applications__r.Branch_Lookup__c != null && objIMDforCallout.Loan_Applications__r.Branch_Lookup__r.Office_Code__c != null ? objIMDforCallout.Loan_Applications__r.Branch_Lookup__r.Office_Code__c : '';   
                        
                        objRequestBody.receiptDate = formatDate(objIMDforCallout.Receipt_Date__c);
                        objRequestBody.requestedForFlag = 'N'; //Will always be "N".
                        objRequestBody.requestId = Utility.generateRandomRequestID().substring(11,17);//objIMDforCallout.Id;
                        objRequestBody.agreementId = objIMDforCallout.Loan_Applications__r.Loan_Number__c != null ? objIMDforCallout.Loan_Applications__r.Loan_Number__c:'';//'1364256';
                        objRequestBody.loanNumber = ''; //'DELTWL00100001049824'
                        objRequestBody.bpId = lstloancon != null && lstloancon.size() > 0 && lstloancon[0].Customer__c != null && lstloancon[0].Customer__r.Customer_ID__c != null ? lstloancon[0].Customer__r.Customer_ID__c : '' ; //'1427873';
                        objRequestBody.bpType = 'LS';
                        objRequestBody.receiptChannel = '';
                        objRequestBody.ftMode = objIMDforCallout.FT_Mode__c != null ? objIMDforCallout.FT_Mode__c : '';
                        objRequestBody.agency = '124';
                        objRequestBody.receiptNo = ''; //Utility.generateRandomRequestID();
                        objRequestBody.drawnOn = objIMDforCallout.Bank_Master__c != null && objIMDforCallout.Bank_Master__r.MICR__c != null  && objIMDforCallout.Bank_Master__r.MICR__c.length() == 9 ? objIMDforCallout.Bank_Master__r.MICR__c.subString(3,6) : '';
                        objRequestBody.cashAc = '';
                        objRequestBody.cardCategory = '';
                        objRequestBody.cardNo = '';
                        objRequestBody.expiryDate = '';
                        objRequestBody.approvalCode = '';
                        objRequestBody.cardHolderName = '';
                        objRequestBody.allocationType = 'N';
                        objRequestBody.branch = objIMDforCallout.Bank_Master__c != null && objIMDforCallout.Bank_Master__r.MICR__c != null  && objIMDforCallout.Bank_Master__r.MICR__c.length() == 9 ? objIMDforCallout.Bank_Master__r.MICR__c.subString(6,9) : '';
                        objRequestBody.chargeId = '35'; //'109'
                        objRequestBody.imdFlag = 'Y'; //'N'
                        objRequestBody.sourceId = 'SFDC-HFL'; // Will always be SFDC-HFL
                        objRequestBody.productFlag = objIMDforCallout.Loan_Applications__c != null && objIMDforCallout.Loan_Applications__r.Scheme__c != null && objIMDforCallout.Loan_Applications__r.Scheme__r.Product_Code__c != null ? String.valueOf(objIMDforCallout.Loan_Applications__r.Scheme__r.Product_Code__c) : '';//'HL';//As of now
                        objRequestBody.schemeId = objIMDforCallout.Loan_Applications__c != null && objIMDforCallout.Loan_Applications__r.Scheme__c != null && objIMDforCallout.Loan_Applications__r.Scheme__r.Scheme_ID__c != null ? String.valueOf(objIMDforCallout.Loan_Applications__r.Scheme__r.Scheme_ID__c) : '';
                        objRequestBody.UserId = lstRestService[0].Client_Username__c;
                        objRequestBody.Password = lstRestService[0].Client_Password__c;
                    System.debug('Debug Log for objRequestBody'+objRequestBody);
                       
                        
                        
                    //Create and Send Request
                    //system.debug('Debug Log for serialized JSON'+Json.serialize(objRequestBody));
                    String jsonRequest = Json.serializePretty(objRequestBody); 
                    //system.debug('jsonRequest2' + jsonRequest2);
                    
                    //String jsonRequest = IntegrationFrameWork.returnJSON('IMDCallout','IMD__c',objIMDRecordId);
                    system.debug('@@json string' +jsonRequest  );
                    system.debug('@@url' + lstRestService[0].Service_EndPoint__c);
                    HTTP http = new HTTP();
                    HTTPRequest httpReq = new HTTPRequest();
                    if(String.isNotBlank(lstRestService[0].Service_EndPoint__c)) {
                        httpReq.setEndPoint(lstRestService[0].Service_EndPoint__c);
                    }
                    else {
                        System.debug('Debug Log for error : Endpoint Empty. Please specify endpoint in metadata record.');
                    }
                    if(String.isNotBlank(lstRestService[0].Request_Method__c)) {
                        httpReq.setMethod(lstRestService[0].Request_Method__c);
                    }
                    else {
                        System.debug('Debug Log for error : Requested Method Empty. Please specify Requested Method in metadata record.');
                    }
                    //httpReq.setHeader('Content-Type', 'application/json');
                    for(String headerKey : headerMap.keySet()){
                        httpReq.setHeader(headerKey, headerMap.get(headerKey));
                        System.debug('Debug Log for headerKey'+headerKey);
                        System.debug('Debug Log for headerMap.get(headerKey)'+headerMap.get(headerKey));
                    }
                    httpReq.setBody(jsonRequest);
                    if(String.isNotBlank(String.valueOf(lstRestService[0].Time_Out_Period__c))) {
                        httpReq.setTimeOut(Integer.valueOf(lstRestService[0].Time_Out_Period__c));
                    }
                    System.debug('Debug Log for request'+httpReq);
                    try {
                        HTTPResponse httpRes = http.send(httpReq);
                        //Create integration logs
                        LogUtility.createIntegrationLogs(jsonRequest,httpRes,lstRestService[0].Service_EndPoint__c);
                        System.debug('Debug Log for response'+httpRes);
                        System.debug('Debug Log for response body'+httpRes.getBody());
                        System.debug('Debug Log for response status'+httpRes.getStatus());
                        System.debug('Debug Log for response status code'+httpRes.getStatusCode());
                        //Deserialization of response to obtain the result parameters
                        objResponseBody = (ResponseBody)JSON.deserialize(httpRes.getBody(), ResponseBody.class);
                        if(httpRes.getStatus() == 'OK' && httpRes.getStatusCode() == 200) {
                            if(objResponseBody != null) {
                                System.debug('Debug Log for requestId'+objResponseBody.requestId);
                                System.debug('Debug Log for serviceStatus'+objResponseBody.serviceStatus);
                                System.debug('Debug Log for chequeId'+objResponseBody.chequeId);
                                System.debug('Debug Log for errorMsg'+objResponseBody.errorMsg);
                                if(String.isBlank(objResponseBody.errorMsg)) {
                                    objIMDforCallout.Cheque_ID__c = objResponseBody.chequeId != null && String.isNotBlank(objResponseBody.chequeId) ? objResponseBody.chequeId :'Received Blank for: '+objIMDforCallout.Name;
                                    objIMDforCallout.API_Error__c =  objResponseBody.serviceStatus != null ? objResponseBody.serviceStatus : '';
                                    if ( objResponseBody.chequeId == null || String.isBlank(objResponseBody.chequeId) ) {
                                        objIMDforCallout.Cheque_Status__c = 'X';
                                    }
                                    
                                    /*Database.SaveResult updateResult = Database.update(objIMDforCallout, false);
                                    System.debug('Debug Log for updateResult'+updateResult);
                                    if(!updateResult.isSuccess()) {
                                        for(Database.Error err : updateResult.getErrors()) {
                                            System.debug('The following error has occurred.');                    
                                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                            System.debug('Fields that affected this error: ' + err.getFields());
                                        }
                                    }*/
                                    
                                } else {
                                    objIMDforCallout.Cheque_Status__c = 'X';
                                }
                            } else {
                                objIMDforCallout.Cheque_Status__c = 'X';
                            }
                        }
                        else {
                            if(String.isNotBlank(objResponseBody.errorMsg)) {
                                objIMDforCallout.API_Error__c = objResponseBody.errorMsg;
                            }
                            else {
                                objIMDforCallout.API_Error__c = httpRes.getStatus() + ' '+httpRes.getStatusCode();
                            }
                            objIMDforCallout.Cheque_Status__c = 'X';
                        }
                        /****Code uncommented by Abhilekh on 11th July 2019 for TIL-1194***/
                        Database.SaveResult updateAPIError = Database.update(objIMDforCallout, false);
                        System.debug('Debug Log for updateAPIError'+updateAPIError);
                        if(!updateAPIError.isSuccess()) {
                            for(Database.Error err : updateAPIError.getErrors()) {
                                System.debug('The following error has occurred.');                    
                                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                System.debug('Fields that affected this error: ' + err.getFields());
                            }
                        }
                        /****Code uncommented by Abhilekh on 11th July 2019 for TIL-1194***/
                        //update objIMDforCallout; //Commented by Abhilekh on 11th July 2019 for TIL-1194
                    }
                    catch(Exception ex) {
                        HttpResponse res = new HttpResponse();
                        res.setBody('Response is null'); 
                        LogUtility.createIntegrationLogs(jsonRequest,res,lstRestService[0].Service_EndPoint__c);
                        //objIMDforCallout.Cheque_Status__c = 'X';
                        //update objIMDforCallout;
                        System.debug('Debug Log for exception'+ex);
                    }
                }
            }
        }
    }
    
    
    public static String formatDate(Date dt){
        if(dt != null) {
            Datetime receiptDateTime = DateTime.newInstance(dt.year(), dt.month(), dt.day());
            return receiptDateTime.format('dd-MMM-yyyy'); 
        } else {
            return '';
        }    
        
    }
    
    //Contains the details about the request body
    class RequestBody {
        public String receiptAmount;
        public String modeOfPayment;
        public String chequeStatus;
        public String transactionCode;
        public String chequeDate;
        public String chequeNumber; 
        public String inFavourOf;
        public String payableAt;
        public String dealingBank;
        public String reason;
        public String homeBranch;
        public String receiptDate;
        public String requestedForFlag;
        public String requestId;
        public String agreementId;
        public String loanNumber;
        public String bpId; 
        public String bpType;
        public String receiptChannel;
        public String ftMode;   
        public String agency;   
        public String receiptNo;
        public String drawnOn;
        public String cashAc;
        public String cardCategory; 
        public String cardNo;   
        public String expiryDate;   
        public String approvalCode; 
        public String cardHolderName;
        public String allocationType;
        public String branch;   
        public String chargeId;
        public String imdFlag;  
        public String sourceId;
        public String productFlag;
        public String schemeId;
        public String UserId;
        public String Password;
    }
    
    public class ResponseBody{
        public String requestId;    //9702
        public String serviceStatus;    //Status : SUCCESS
        public String chequeId; //123525
        public String errorMsg;
        
        public ResponseBody() {
            requestId = '';
            serviceStatus = '';
            chequeId = '';
            errorMsg = '';
        }
    }
}