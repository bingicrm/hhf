public class PerfiosBankStmtRetrieveResponseBodyRest {
    public cls_customerInfo customerInfo;
    public cls_summaryInfo summaryInfo;
    public cls_monthlyDetails[] monthlyDetails;
    public cls_eODBalances[] eODBalances;
    public cls_top10PaymentsReceived[] top10PaymentsReceived;
    public cls_top10PaymentsMade[] top10PaymentsMade;
    public cls_regularDebits[] regularDebits;
    public cls_fCUAnalysis fCUAnalysis;
    public cls_accountXns[] accountXns;
    public cls_AdditionalMonthlyDetails[] AdditionalMonthlyDetails;
    public class cls_customerInfo {
        public String name; //BANDORUPALLE RAMESH
        public String address;  //19 352 TEKKAYA CHENU JAMMALAMADUGU KADAPA KADAPA
        public String landline; //
        public String mobile;   //
        public String email;    //
        public String pan;  //
        public String perfiosTransactionId; //FZ7T1545211804714
        public String customerTransactionId;    //testonlineclient123
        public String bank; //Canara Bank, India
        public Double instId;  //11
    }
    public class cls_summaryInfo {
        public String instName; //Canara Bank, India
        public String accNo;    //1222131000460
        public String accType;  //
        public Double fullMonthCount;  //6
        public cls_total total;
        public cls_average average;
    }
    public class cls_total {
        public Double bal15;    //74913.09
        public Double bal25;    //84003.77
        public Double bal5; //67450.39
        public Double balAvg;   //70516.58
        public Double balLast;  //91837.67
        public Double balMax;   //136520.24
        public Double balMin;   //25419.38
        public Double balOpen;  //84570.37
        public Double cashDeposits;    //0
        public Double cashWithdrawals; //14
        public Double chqDeposits; //0
        public Double chqIssues;   //0
        public Double credits; //37
        public Double debits;  //103
        public Double dpLimit; //1450
        public Double ecsIssues;   //0
        public Double emiOrLoans;  //0
        public Double intPayDelay; //0
        public Double inwBounceNonTechnical;   //0
        public Double inwBounces;  //0
        public Double inwChqBounces;   //0
        public Double inwECSBounces;   //0
        public Double inwEMIBounces;   //0
        public Double loanDisbursals;  //0
        public Double outwBounces; //0
        public Double outwChqBounces;  //0
        public Double overdrawnAmount; //0
        public Double overdrawnDays;   //0
        public Double overdrawnInstances;  //0
        public Double salaries;    //0
        public Double snLimit; //1450
        public Double totalCashDeposit;    //0
        public Double totalCashWithdrawal; //36500
        public Double totalChqDeposit; //0
        public Double totalChqIssue;   //0
        public Double totalCredit; //150909
        public Double totalDebit;   //143641.7
        public Double totalEmiOrLoan;  //0
        public Double totalInterestCharged;    //0
        public Double totalInterestIncome; //91
        public Double totalInterestPaid;   //0
        public Double totalInwBounceNonTechnical;  //0
        public Double totalInwChqBounce;   //0
        public Double totalInwEMIBounce;   //0
        public Double totalLoanDisbursal;  //0
        public Double totalOutwChqBounce;  //0
        public Double totalSalary; //0
    }
    public class cls_average {
        public Double bal15;    //12485.51
        public Double bal25;    //14000.63
        public Double bal5; //11241.73
        public Double balAvg;   //11752.76
        public Double balLast;  //15306.28
        public Double balMax;   //22753.37
        public Double balMin;   //4236.56
        public Double balOpen;  //14095.06
        public Double cashDeposits;    //0
        public Double cashWithdrawals; //2
        public Double chqDeposits; //0
        public Double chqIssues;   //0
        public Double credits; //6
        public Double debits;  //17
        public Double dpLimit;  //241.67
        public Double ecsIssues;   //0
        public Double emiOrLoans;  //0
        public Double intPayDelay; //0
        public Double inwBounceNonTechnical;   //0
        public Double inwBounces;  //0
        public Double inwChqBounces;   //0
        public Double inwECSBounces;   //0
        public Double inwEMIBounces;   //0
        public Double loanDisbursals;  //0
        public Double outwBounces; //0
        public Double outwChqBounces;  //0
        public Double overdrawnAmount; //0
        public Double overdrawnDays;   //0
        public Double overdrawnInstances;  //0
        public Double salaries;    //0
        public Double snLimit;  //241.67
        public Double totalCashDeposit;    //0
        public Double totalCashWithdrawal;  //6083.33
        public Double totalChqDeposit; //0
        public Double totalChqIssue;   //0
        public Double totalCredit;  //25151.5
        public Double totalDebit;   //23940.28
        public Double totalEmiOrLoan;  //0
        public Double totalInterestCharged;    //0
        public Double totalInterestIncome;  //15.17
        public Double totalInterestPaid;   //0
        public Double totalInwBounceNonTechnical;  //0
        public Double totalInwChqBounce;   //0
        public Double totalInwEMIBounce;   //0
        public Double totalLoanDisbursal;  //0
        public Double totalOutwChqBounce;  //0
        public Double totalSalary; //0
    }
    public class cls_monthlyDetails {
        public Double bal15;    //12576.76
        public Double bal25;    //12987.1
        public Double bal5; //7766.26
        public Double balAvg;   //10137.78
        public Double balLast;  //15574.1
        public Double balMax;   //16076.76
        public Double balMin;   //3766.26
        public Double balOpen;  //7129.86
        public Double cashDeposits;    //0
        public Double cashWithdrawals; //3
        public Double chqDeposits; //0
        public Double chqIssues;   //0
        public Double credits; //6
        public Double debits;  //11
        public Double dpLimit; //250
        public Double ecsIssues;   //0
        public Double emiOrLoans;  //0
        public Double intPayDelay; //0
        public Double inwBounceNonTechnical;   //0
        public Double inwBounces;  //0
        public Double inwChqBounces;   //0
        public Double inwECSBounces;   //0
        public Double inwEMIBounces;   //0
        public Double loanDisbursals;  //0
        public String monthName;    //May-18
        public Double outwBounces; //0
        public Double outwChqBounces;  //0
        public Double overdrawnAmount; //0
        public Double overdrawnDays;   //0
        public Double overdrawnInstances;  //0
        public Double salaries;    //0
        public Double snLimit; //250
        public String startDate;    //2018-05-01
        public Double totalCashDeposit;    //0
        public Double totalCashWithdrawal; //6500
        public Double totalChqDeposit; //0
        public Double totalChqIssue;   //0
        public Double totalCredit; //23000
        public Double totalDebit;   //14555.76
        public Double totalEmiOrLoan;  //0
        public Double totalInterestCharged;    //0
        public Double totalInterestIncome; //0
        public Double totalInterestPaid;   //0
        public Double totalInwBounceNonTechnical;  //0
        public Double totalInwChqBounce;   //0
        public Double totalInwEMIBounce;   //0
        public Double totalLoanDisbursal;  //0
        public Double totalOutwChqBounce;  //0
        public Double totalSalary; //0
    }
    public class cls_eODBalances {
        public String date_eODBalances; //2018-05-01
        public Double balance;  //4129.86
    }
    public class cls_top10PaymentsReceived {
        public String party;    //VIJAYABASK
        public Double amount;  //62500
        public Double count;   //19
    }
    public class cls_top10PaymentsMade {
        public String party;    //WALLET
        public Double amount;   //4647.41
        public Double count;   //11
    }
   public class cls_regularDebits {
        public Double group_regularDebits; //1
        public String date_regularDebits;   //2018-05-07
        public String chqNo;    //
        public String narration;    //MB-UPI DEBIT 700450- 07/05/18 10:31:52 (Ref# 812710169941)
        public Double amount;  //-4000
        public String category; //Transfer out
        public Double balance;  //3766.26
    }
    public class cls_fCUAnalysis {
        public cls_equalCreditDebitXns equalCreditDebitXns;
        public cls_suspiciousBankEStatements suspiciousBankEStatements;
    }
   public class cls_equalCreditDebitXns {
        public String status;   //false
    }
   public class cls_suspiciousBankEStatements {
        public String status;   //false
    }
   public class cls_accountXns {
        public String accountNo;    //1222131000460
        public String accountType;  //
        public String ifscCode; //
        public String micrCode; //
        public String location; //TRICHY TEPPAKULAM MAIN
        public cls_xns[] xns;
    }
    public class cls_xns {
        public String date_xns; //2018-05-01
        public String chqNo;    //
        public String narration;    //ATM Cash-00977009-JAMMALAMADUGUOPPBUSJAM MALAMADUGUAPIN-01/05/18 11:50:01/1177 (Ref# 812111026456)
        public Double amount;  //-3000
        public String category; //Cash Withdrawal
        public Double balance;  //4129.86
    }
    public class cls_AdditionalMonthlyDetails {
        public String monthName;    //May-18
        public String inwBouncePercentage;  //NA
        public String avgUtilization;   //4055.11
        public Double intPayInstance;  //0
    }
}