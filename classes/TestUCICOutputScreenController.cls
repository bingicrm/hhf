@isTest
public class TestUCICOutputScreenController {
	@testSetup static void testData() {
            Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
            Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
            
            Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',
                                      Date_of_Incorporation__c=System.today(),
                                      Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
            insert acc;
            
            Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                       Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
            insert acc1;
            
            acc1 =[Select Id, PAN__c, Name, Date_of_Incorporation__c, Phone, RecordTypeId, Customer_ID__c From Account where Id =: acc1.Id];
            Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
            insert con1;
            
            Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                                Scheme_Group_ID__c='12');
            insert objScheme;
            
            Loan_Application__c la1 = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                              Transaction_type__c='PB',Requested_Amount__c=100000,
                                                              Loan_Purpose__c='11', Loan_Number__c ='12345');
            insert la1;
    }
    
    public static testMethod void getUCICDataTest(){
        Loan_Application__c la = [SELECT Id FROM Loan_Application__c LIMIT 1];
        Loan_Contact__c lc = [SELECT Id, Name,Applicant_Type__c, Applicant_Status__c, Constitution__c, Customer_segment__c, 
                              Loan_Applications__c FROM Loan_Contact__c WHERE Loan_Applications__c =: la.Id];
        Id RecordTypeIdCustUCIC = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('UCIC').getRecordTypeId();
        Customer_Integration__c objcust = new Customer_Integration__c();
        objcust.UCIC_Negative_API_Response__c = true;
        objcust.UCIC_Negative_API_Response__c = false;
        objcust.Loan_Application__c = la.Id;
        objcust.Loan_Contact__c = lc.Id;
        objcust.RecordTypeId = RecordTypeIdCustUCIC;
        insert objcust;
        
        UCIC_Database__c objUC = new UCIC_Database__c();
        objUC.Match_Count__c = 2;
        objUC.Matched_Max_Percentage__c = '60';
        objUC.Name = 'UCIC Dedupe Check';
        objUC.Loan_Application__c = la.Id;
        objUC.Customer_Detail__c = lc.Id;
        objUC.Recommender_s_Decision__c = 'Approve';
        insert objUC; 
        
        Test.startTest();
        UCICOutputScreenController.UCICOutputWrapper result = UCICOutputScreenController.getUCICData(la.Id);
        UCICOutputScreenController.saveUCICDatabaseDecision(JSON.serialize(result));
        System.assertNotEquals(null, result);
        Test.stopTest();
    }
    
    public static testMethod void getselectOptionsTest(){
    	List<String> result = UCICOutputScreenController.getselectOptions(new Loan_Application__c(), 'Line_Of_Business__c');
        System.assertNotEquals(null, result);
    }
}