public class PromoterIntegrationTriggerHandler {
    /******************************************************************************************************************************************************************************
     * Created By   :   Shobhit Saxena(Deloitte)
     * 
     * 
     * 
     * 
    ******************************************************************************************************************************************************************************/
    public static void onBeforeInsert(List<Promoter_Integrations__c> lstTriggerNew) {
        Date dt = LMSDateUtility.lmsDateValue;
        for(Promoter_Integrations__c objPI : lstTriggerNew){
            objPI.Business_Date__c = (Date) dt;
        }
    }
    
    public static void onAfterInsert(List<Promoter_Integrations__c> lstTriggerNew) {
        List<Promoter_Integrations__c> lstPI = [select id, recordTypeId,GSTIN_Error_Message__c,Mobile__c, Email__c, GSTIN__c,RecordType.Name,Promoter__c from Promoter_Integrations__c where id in:lstTriggerNew];
        
        List<Promoter_Integrations__c> gstRecords = new List<Promoter_Integrations__c>();
        Set<id> setRecordTypeIds = new Set<id>();
        Set<id> setPromoter = new Set<id>();
        
        for(Promoter_Integrations__c objPI : lstPI){
            setRecordTypeIds.add(objPI.RecordTypeId);
            setPromoter.add(objPI.Promoter__c);
            if(objPI.RecordType.Name == 'GSTIN' && objPI.GSTIN__c != null && objPI.GSTIN_Error_Message__c == null) {
                gstRecords.add(objPI);
            }
        }

        if(!setRecordTypeIds.isEmpty()){
            system.debug('===');
            list<Promoter_Integrations__c> previousList = new list<Promoter_Integrations__c>();
            previousList = [ select id, Latest_Record__c, recordTypeId from Promoter_Integrations__c where recordTypeId in :setRecordTypeIds and id not in: lstPI ];
            if(!previousList.isEmpty()) {
                for(Promoter_Integrations__c prev: previousList){
    
                  prev.Latest_Record__c = false;
                }
                update previousList;
            }
        }
        
        if(setRecordTypeIds.size() >0){
            system.debug('==herreeeeeee=');

            list<Promoter_Integrations__c> previousList = new list<Promoter_Integrations__c>();
            previousList = [ select id, Latest_Record__c, Promoter__c,recordTypeId from Promoter_Integrations__c where
                                    recordTypeId in :setRecordTypeIds and id not in: lstTriggerNew and Promoter__c in :setPromoter and RecordType.Name != 'GSTIN'];//Edited by Chitransh for GST Integration//

                    for(Promoter_Integrations__c prev: previousList){

                        prev.Latest_Record__c = false;
                    }
                    update previousList;
        }
        
        if(!gstRecords.isEmpty()){
             system.debug('NotEmpty List');
             System.enqueueJob(new GstReturnStatusCalloutAPF(null,gstRecords));
             
         }
         
    }
    
    public static void onBeforeUpdate(Map<Id,Promoter_Integrations__c> mapTriggerNew, Map<Id,Promoter_Integrations__c> mapTriggerOld) {
        
    }
    
    public static void onAfterUpdate(Map<Id,Promoter_Integrations__c> mapTriggerNew, Map<Id,Promoter_Integrations__c> mapTriggerOld) {
        
    }
}