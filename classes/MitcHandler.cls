public class MitcHandler{
    @AuraEnabled
    public static void getMitcDoc(Id recordId) 
    {
        system.debug('@@@@@ RecordID in getMITCDoc' + recordId);
       Http_Callout_Mitc.mitccallout(recordId);
    }

}