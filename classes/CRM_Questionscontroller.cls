Global Class CRM_Questionscontroller{

    public List<CRM_Survey_Question__c> getQuestions { get; set; }
    public List<String> lstStr {get; set;}
    //public String ques1select {get; set;}
     String[] ques1select = new String[]{};
    public String[] getques1select () {
        return ques1select ;
    }
    public void setques1select (String[] ques1select ) {
        this.ques1select = ques1select ;
    }
    public List<String> lstStrradio1 {get; set;}
    public List<String> lstStrradio2 {get; set;}
    public List<String> lstStrradio3 {get; set;}
    public List<String> lstStrradio4 {get; set;}
    public List<String> lstStrradio5 {get; set;}
    public list<string>  lstStrradio6{get;set;}
    public List<String> lstStr2 {get; set;}
  //  public Boolean selectedcheckbox {get; set;}
    public String selectedRadio1 {get; set;}
    public String selectedRadio2 {get; set;}
    public String selectedRadio3 {get; set;}
    public String selectedRadio4 {get; set;}
   // public Boolean selectedRadio5 {get; set;}
   // public Boolean selectedRadio6 {get; set;}
    public String comments {get; set;}
    public String Name {get; set;}
    public String Email {get; set;}
    public String ContactNum {get; set;}
    public String currentRecordId {get;set;}
    
    public PageReference save() {
    List<CRM_Survey_Answer__c> lstAns = new List<CRM_Survey_Answer__c>();
        for(CRM_Survey_Question__c qc : getQuestions){
            //CRM_Survey_Answer__c ans = new CRM_Survey_Answer__c();
            //ans.Case__c=currentRecordId;
            //ans.CRM_Survey_Question__c=qc.id;
            if(qc.Order_Number__c ==1){
               // String selectedCheckBox;
               // ans.Answer_Text__c= ques1select;
               system.debug('***ques1select'+ques1select);
                for(String s: ques1select){
                    system.debug('***loop');
                   // if (selectedCheckBox == null)
                   //    { selectedCheckBox = s;}
                   // else{
                    //    selectedCheckBox =selectedCheckBox+'#'+s;
                    //}
                    CRM_Survey_Answer__c ans = new CRM_Survey_Answer__c();
                    ans.Case__c=currentRecordId;
                    ans.CRM_Survey_Question__c=qc.id;
                    ans.Answer_Text__c= s;
                    lstAns.add(ans);
                 }
                // system.debug('***selectedcheckbox'+selectedCheckBox);
                //ans.Answer_Text__c= selectedCheckBox; 
             }
             else if(qc.Order_Number__c ==2){
                CRM_Survey_Answer__c ans = new CRM_Survey_Answer__c();
                ans.Case__c=currentRecordId;
                ans.CRM_Survey_Question__c=qc.id;
                ans.Answer_Text__c= selectedRadio1;
                lstAns.add(ans);
             }
             else if(qc.Order_Number__c ==3){
                  CRM_Survey_Answer__c ans = new CRM_Survey_Answer__c();
                  ans.Case__c=currentRecordId;
                  ans.CRM_Survey_Question__c=qc.id;
                  ans.Answer_Text__c=selectedRadio2;
                  lstAns.add(ans);
             }
             else if(qc.Order_Number__c ==4){
                  CRM_Survey_Answer__c ans = new CRM_Survey_Answer__c();
                  ans.Case__c=currentRecordId;
                  ans.CRM_Survey_Question__c=qc.id;
                  ans.Answer_Text__c=selectedRadio3;
                  lstAns.add(ans);
             }
             else if(qc.Order_Number__c ==5){
                  CRM_Survey_Answer__c ans = new CRM_Survey_Answer__c();
                  ans.Case__c=currentRecordId;
                  ans.CRM_Survey_Question__c=qc.id;
                  ans.Answer_Text__c= selectedRadio4;
                  lstAns.add(ans);
             }
             else if(qc.Order_Number__c ==6){
                 CRM_Survey_Answer__c ans = new CRM_Survey_Answer__c();
                 ans.Case__c=currentRecordId;
                 ans.CRM_Survey_Question__c=qc.id;
                 ans.Answer_Text__c=comments;
                 lstAns.add(ans);
             }
             else if(qc.Order_Number__c ==7){
                 CRM_Survey_Answer__c ans = new CRM_Survey_Answer__c();
                 ans.Case__c=currentRecordId;
                 ans.CRM_Survey_Question__c=qc.id;
                 ans.Answer_Text__c=Name+'#'+ContactNum+'#'+Email;
                 lstAns.add(ans);
             }
         }
         insert lstAns;
         if(lstAns.size()>0 && currentRecordId != null){
             case c= [select id,CRM_Feedback_Summited__c from case where id=:currentRecordId][0];
             c.CRM_Feedback_Summited__c = true;
             update c;
         }
        
        string str=Label.CRM_Feedback_S;
        PageReference pg= new PageReference(str);
        return pg;
    }



    public CRM_Questionscontroller() {
    getQuestions = new  List<CRM_Survey_Question__c>();
    getQuestions =  [SELECT id,Question__c,Answer_List__c,Is_Active__c,Order_Number__c from CRM_Survey_Question__c where Is_Active__c= true Order By Order_Number__c ASC];
    for( CRM_Survey_Question__c qc : getQuestions){
        if(qc.Answer_List__c!=null && qc.Order_Number__c==1){
            lstStr = qc.Answer_List__c.split('#');
        }
        if(qc.Answer_List__c!=null && qc.Order_Number__c==2){
            lstStrradio1 = qc.Answer_List__c.split('#');
        }
        if(qc.Answer_List__c!=null && qc.Order_Number__c==3){
            lstStrradio2 = qc.Answer_List__c.split('#');
        }
        if(qc.Answer_List__c!=null && qc.Order_Number__c==4){
            lstStrradio3 = qc.Answer_List__c.split('#');
        }
        if(qc.Answer_List__c!=null && qc.Order_Number__c==5){
            lstStrradio4 = qc.Answer_List__c.split('#');
        }
        if(qc.Answer_List__c!=null && qc.Order_Number__c==6){
            lstStrradio5 = qc.Answer_List__c.split('#');
        }
        if(qc.Answer_List__c!=null && qc.Order_Number__c==7){
            lstStrradio6 = qc.Answer_List__c.split('#');
        }
      }
      currentRecordId = ApexPages.CurrentPage().getparameters().get('id');
    }
    public CRM_Questionscontroller(ApexPages.StandardController stdController) {
        //this.acct = (Account)stdController.getRecord();
    }
    public List<SelectOption> getQuestionOptions() {
        List<SelectOption> questionOptions = new List<SelectOption>();
        for(String str : lstStr){
            questionOptions.add(new SelectOption(str,str));
         }
        return questionOptions;
    }
    public List<SelectOption> getQuestion2Options() {
        List<SelectOption> questionOptions = new List<SelectOption>();
        for(String str : lstStrradio1 ){
            questionOptions.add(new SelectOption(str,str));
         }
        return questionOptions;
    }
    public List<SelectOption> getQuestion3Options() {
        List<SelectOption> questionOptions = new List<SelectOption>();
        for(String str : lstStrradio2 ){
            questionOptions.add(new SelectOption(str,str));
         }
        return questionOptions;
    }
    public List<SelectOption> getQuestion4Options() {
        List<SelectOption> questionOptions = new List<SelectOption>();
        for(String str : lstStrradio3 ){
            questionOptions.add(new SelectOption(str,str));
         }
        return questionOptions;
    }
    public List<SelectOption> getQuestion5Options() {
        List<SelectOption> questionOptions = new List<SelectOption>();
        for(String str : lstStrradio4 ){
            questionOptions.add(new SelectOption(str,str));
         }
        return questionOptions;
    }
    public List<SelectOption> getQuestion6Options() {
        List<SelectOption> questionOptions = new List<SelectOption>();
        for(String str : lstStrradio5 ){
            questionOptions.add(new SelectOption(str,str));
         }
        return questionOptions;
    }

}