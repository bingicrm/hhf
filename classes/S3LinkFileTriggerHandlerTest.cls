/* **************************************************************************
*
* Test Class: S3LinkFileTriggerHandlerTest
* Created by Anil Meghnathi
*
* - Test class for S3LinkFileTriggerHandler.

* - Modifications:
* - Anil Meghnathi – Initial Development
************************************************************************** */
@isTest
public class S3LinkFileTriggerHandlerTest {
    @testSetup static void insertTestData(){
        // Create buckets
        NEILON__Folder__c bucket = s3LinkTestUtils.createFoldersForBucket('testbucket');
    }
    
    static testMethod void testAfterInsert(){
        // Start test
        Test.startTest();
        
        // Folder configuration by name
        List<S3_Folders_Configuration__mdt> folderConfigs = [SELECT Id, Delete_files__c, Folder_Name__c FROM S3_Folders_Configuration__mdt];
        
        // Create loan application 
        Loan_Application__c loanApplication = edInitiateLargeFilesUploadControllerTest.createLoanApplicationData();
        
        // Set URL parameter
        ApexPages.currentPage().getParameters().put('Id', loanApplication.Id);
        
        // Contructor
        edInitiateLargeFilesUploadController con = new edInitiateLargeFilesUploadController();
        
        // Stop test
        Test.stopTest();
        
        // Call init
        con.init();
        
        // Check load success flag
        System.assertEquals(con.isLoadSuccess, true);
        
        // Check folders are created
        List<NEILON__Folder__c> folders = [Select Id, NEILON__Bucket_Name__c, NEILON__Bucket_Region__c, Name From NEILON__Folder__c Where Loan_Application__c =: loanApplication.Id];
        
        
        // Create S3-Files with Export Attachment Id, Loan Application and S3-Folder
        List<NEILON__File__c> files = new List<NEILON__File__c>();
        
        // Check count for files for each folder
        List<edInitiateLargeFilesUploadController.FolderInfo> folderInfos = (List<edInitiateLargeFilesUploadController.FolderInfo>)JSON.deserialize(con.foldersInformation, List<edInitiateLargeFilesUploadController.FolderInfo>.class); 
        
        // Document ids which needs to be deleted
        Set<String> documentsToDeleted = new Set<String>();
        
        // Map of content documents to content version
        Map<Id, Id> contentVersionIdByDocumentId = new Map<Id, Id>();
        
        // Get all the documents
        for(ContentVersion contentVersion : [Select Id, ContentDocumentId From ContentVersion]){
            contentVersionIdByDocumentId.put(contentVersion.ContentDocumentId, contentVersion.Id);
        }
        
        // Go through folders and create files for each documents
        for(edInitiateLargeFilesUploadController.FolderInfo folderInfo : folderInfos){
            // Check if content document needs to be deleted
            Boolean isDelete = false;
            for(S3_Folders_Configuration__mdt folderConfig : folderConfigs){
                // Folder name
                String folderName = folderConfig.Folder_Name__c + ' ' + loanApplication.Name;
                if(folderName == folderInfo.name && folderConfig.Delete_files__c) {
                    isDelete = true;
                }
            }
            
            for(Id documentId : folderInfo.documents){
                if(isDelete){
                    documentsToDeleted.add(documentId);
                }
                NEILON__File__c file = new NEILON__File__c();
                file.Name = documentId + '.txt';
                file.NEILON__Folder__c = folderInfo.id;
                file.NEILON__Export_Attachment_Id__c = contentVersionIdByDocumentId.containsKey(documentId) ? contentVersionIdByDocumentId.get(documentId) : documentId;
                file.NEILON__Size__c = 0;
                file.NEILON__Bucket_Name__c = folders[0].NEILON__Bucket_Name__c;
                file.NEILON__Bucket_Region__c = folders[0].NEILON__Bucket_Region__c;
                file.Loan_Application__c = loanApplication.Id;
                files.add(file);
            }
        }
        insert files;
        
        // Get document check list
        Document_Checklist__c documentCheckList = [Select Id, Loan_Contact__c, Document_Master__c, Document_Type__c From Document_Checklist__c Where Loan_Contact__r.Applicant_Type__c = 'Applicant'];
        
        // Check the values of other fields in S3-Files
        NEILON__File__c file = [Select Id, Document_Checklist__c, Document_Master__c, Document_Type__c,
                    Loan_Contact__c, Customer_Type__c, NEILON__Export_Attachment_Id__c 
                    From NEILON__File__c Where Document_Checklist__c =: documentCheckList.Id LIMIT 1];
        
        // Check other fields
        System.assertEquals(file.Document_Checklist__c, documentCheckList.Id);
        System.assertEquals(file.Document_Master__c, documentCheckList.Document_Master__c);
        System.assertEquals(file.Document_Type__c, documentCheckList.Document_Type__c);
        System.assertEquals(file.Loan_Contact__c, documentCheckList.Loan_Contact__c);
        System.assertEquals(file.Customer_Type__c, 'Applicant');
        
        // Check document migration logs
        List<Document_Migration_Log__c> migrationLogs = [Select Id From Document_Migration_Log__c Where Successfully_Migrated__c = true AND S3_File__c IN: files];
        
        // Check size of migration logs
        System.assertEquals(migrationLogs.size(), 5);
        
        // Check content documents and attachments are deleted
        List<Attachment> attachments = [Select Id From Attachment Where Id IN: documentsToDeleted];
        List<ContentDocument> contentDocuments = [Select Id From ContentDocument Where Id In: documentsToDeleted];
        
        // Check size of documents
        System.assertEquals(attachments.size(), 0);
        System.assertEquals(contentDocuments.size(), 0);
    }
}