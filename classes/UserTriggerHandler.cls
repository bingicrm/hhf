public class UserTriggerHandler{
    
    public static void beforeInsert(List<User> newList){
        if(!newList.isEmpty()){
            set<String> inspectorIdList = new set<String>();
            set<string> branchMasters = new set<string>();

            list<id> userRoleIdlist = new list<id>();
            List<Id> profileIdlist = new List<Id>();
            set<id> newUserIds = new set<id>();
            
            List<User> usrList = [Select Manager_Inspector_ID__c from User where Manager_Inspector_ID__c != null];
            if(!usrList.isEmpty()){
			
                for(User usr :usrList ){
                    inspectorIdList.add(usr.Manager_Inspector_ID__c);
									
                }
                
            }
            
            List<Branch_Master__c> bmList  =[Select id,Name from Branch_Master__c];
            for(Branch_Master__c bm : bmList){
                branchMasters.add(bm.Name);
            }
            for(User usr: newList){
                profileIdlist.add(usr.ProfileId);
                userRoleIdlist.add(usr.UserRoleId);
            }
            Map<id,Profile> IdToProfileMap = new Map<id,Profile>([Select Id, Name from Profile where Id in : profileIdlist]);
            Map<id,UserRole> IdToRoleMap = new Map<id,UserRole>([select id, name from userrole where id in :userRoleIdList]);
            
            for(User usr: newList){
                if((IdToProfileMap.Keyset().contains(usr.ProfileId)) && 
                   (
                    (IdToProfileMap.get(usr.ProfileId).Name == 'DST' ) ||
                    (IdToProfileMap.get(usr.ProfileId).Name == 'Sales Team' && 
                     IdToRoleMap.Keyset().contains(usr.UserRoleId) && 
                     (IdToRoleMap.get(usr.UserRoleId).Name.startswith('SM') || IdToRoleMap.get(usr.UserRoleId).Name.startswith('CBM'))
                    )
                   )
                  )
                {
                    if(String.isBlank(usr.Line_Of_Business__c)){
                        usr.Line_Of_Business__c.addError('Please Assign a LOB');
                    }
                    
                    if(String.isBlank(usr.Manager_Inspector_ID__c)){
                        usr.Manager_Inspector_ID__c.addError('Inspector Id can not be null');
                    }
                    else{
                        if(inspectorIdList.contains(usr.Manager_Inspector_ID__c)) {
                            usr.Manager_Inspector_ID__c.addError('Duplicate Inspector Id.');
                        }  
                    }
                    
                    if(!branchMasters.contains(usr.Location__c)){
                        usr.Location__c.addError('Please Enter a valid Location.');
                    }
                    
                }
                if(IdToProfileMap.Keyset().contains(usr.ProfileId) && 
                   (IdToProfileMap.get(usr.ProfileId).Name == 'DSA' )){
                       if(String.isBlank(usr.Line_Of_Business__c)){
                           usr.Line_Of_Business__c.addError('Please Assign a LOB');
                       }
                       if(String.isBlank(usr.DSA_Broker_Code__c)){
                           usr.DSA_Broker_Code__c.addError('Please Assign a Code');
                       }
                       if(String.isBlank(usr.DSA_Broker_ID__c)){
                           usr.DSA_Broker_ID__c.addError('Broker Id can\'t be Null');
                       }
                       if(!branchMasters.contains(usr.Location__c)){
                           usr.Location__c.addError('Please Enter a valid Location.');
                       }
                       
                   }
                
                if(IdToProfileMap.Keyset().contains(usr.ProfileId) && 
                   (IdToProfileMap.get(usr.ProfileId).Name == 'Third Party Vendor')){
                        usr.Role_Assigned_Date__c = System.now();
                       if(usr.Technical__c == false && usr.LIP__c == false && usr.FI__c == false && usr.Legal__c == false && usr.FCU__c == false && usr.PD__c == false){
                           usr.addError('Please assign at least one Verification type to Third Party Vendor');
                        }
                       if(String.isNotBlank(usr.Location__c)){
                           List<String> locations = usr.Location__c.split('/');
                           for(String loc : locations){
                               if(!branchMasters.contains(loc)){
                                   usr.Location__c.addError('Please Enter a valid Location.');
                               }
                               
                           }
                           
                       }
                       else{
                           usr.Location__c.addError('Please Enter a valid Location.');	
                       }
                   }
            }
	
        }
		 
    }
    
    
    public static void afterInsert(List<User> newList){
        if(!newList.isEmpty()){
            system.debug('Entry1');
																			
								  
			  
			 
		  
		 
            List<User> listUser = new List<User>();
            List<User> listThirdPartyVendorUser = new List<User>();
										  
            List<Id> profileIdlist = new List<Id>();
            list<id> userRoleIdlist = new list<id>();
            set<id> newUserIds = new set<id>();										   
		
            for(User usr: newList){
                profileIdlist.add(usr.ProfileId);
                userRoleIdlist.add(usr.UserRoleId);

            }
            Map<id,Profile> IdToProfileMap = new Map<id,Profile>([Select Id, Name from Profile where Id in : profileIdlist]);
            Map<id,UserRole> IdToRoleMap = new Map<id,UserRole>([select id, name from userrole where id in :userRoleIdList]);
            for(User usr: newList){
                if((IdToProfileMap.Keyset().contains(usr.ProfileId)) && 
                   ((IdToProfileMap.get(usr.ProfileId).Name == 'DST' ) ||
                    (IdToProfileMap.get(usr.ProfileId).Name == 'DSA' ) ||
                    (IdToProfileMap.get(usr.ProfileId).Name == 'Third Party Vendor') ||
                    ( IdToProfileMap.get(usr.ProfileId).Name == 'Sales Team' && 
                     IdToRoleMap.Keyset().contains(usr.UserRoleId) && 
																					 
										 
                     (IdToRoleMap.get(usr.UserRoleId).Name.startswith('SM') || IdToRoleMap.get(usr.UserRoleId).Name.startswith('CBM'))
                    )
                   )
                  )
                {
                    newUserIds.add(usr.Id);
                    listUser.add(usr);
					
                }
                
            }
            if(!listUser.isEmpty()){
                system.debug('Entry2');
                UserTriggerHelper.createMasterRecordandUBM(newUserIds);
                UserTriggerHelper.addInPublicGroup(newUserIds);
            }
   		
        }
  								  
    }
  
}