public class AccountTriggerHelper {

	public static void retriggerCorpCIBIL(List<account> newList, Map<Id,account> oldMap) {
	Map<Id,CIBIL_Required_Field__mdt> mapCorpCIBIL= new Map<Id,CIBIL_Required_Field__mdt>([Select Id, Field__c, Object__c, Critical_Field__c from CIBIL_Required_Field__mdt where Object__c = 'Account' and Critical_Field__c = true]);

	Set<Id> setLCAccId = new Set<Id>(); //Added by Abhilekh on 9th May 2019 for TIL-736

	//Below for block added by Abhilekh on 9th May 2019 for TIL-736
	for(Account acc: newList){
		setLCAccId.add(acc.Id);
	}

	List<Loan_Contact__c> lstLC = [SELECT Id,Name,Borrower__c,Customer__c from Loan_Contact__c WHERE Customer__c IN: setLCAccId]; //Added by Abhilekh on 9th May 2019 for TIL-736

	Map<Id,String> mapAccBorrowerTyp = new Map<Id,String>(); //Added by Abhilekh on 9th May 2019 for TIL-736

	//Below for block added by Abhilekh on 9th May 2019 for TIL-736
	for(Loan_Contact__c lc: lstLC){
		mapAccBorrowerTyp.put(lc.Customer__c,lc.Borrower__c);
	}

	//Added by Abhilekh on 9th May 2019 for TIL-736
	Map<Id,CIBIL_Individual_Required_Field__mdt> mapIndividualCIBIL = new Map<Id,CIBIL_Individual_Required_Field__mdt>([Select Id, Field__c, Object__c, Critical_Field__c from CIBIL_Individual_Required_Field__mdt where Object__c = 'Account' and Critical_Field__c = true]);

	for ( Account acc: newList) {
		for ( CIBIL_Required_Field__mdt objCorpCIBIL : mapCorpCIBIL.values() ) {
			if(acc.get(objCorpCIBIL.Field__c) != oldMap.get(acc.id).get(objCorpCIBIL.Field__c) && !mapAccBorrowerTyp.isEmpty() && mapAccBorrowerTyp.containsKey(acc.Id) && mapAccBorrowerTyp.get(acc.Id) == '2') {
				acc.Retrigger_Corp_CIBIL__c = true;
				system.debug('in retrigger corp cibil');
				break;
			}
		}
		
		//Below for block added by Abhilekh on 9th May 2019 for TIL-736
		for(CIBIL_Individual_Required_Field__mdt objIndiCIBIL : mapIndividualCIBIL.values() ){
			if(acc.get(objIndiCIBIL.Field__c) != oldMap.get(acc.id).get(objIndiCIBIL.Field__c)) {
				if(!mapAccBorrowerTyp.isEmpty() && mapAccBorrowerTyp.containsKey(acc.Id) && mapAccBorrowerTyp.get(acc.Id) == '1'){
					acc.Retrigger_Individual_CIBIL__c = true;
					system.debug('Inside for==>'+acc.Retrigger_Individual_CIBIL__c);
					break;    
				}
			}
		}
	}
	}

	public static void checkPanValidated(list<account> newList, Map<Id,account> oldMap) {
	system.debug('@@checkpanvalidated starts');
	String userFullName = UserInfo.getName(); //Added by Abhilekh on 12th June 2019 for Automated Process User Error
	System.debug('userFullName==>'+userFullName); //Added by Abhilekh on 12th June 2019 for Automated Process User Error
	Id profileId=userinfo.getProfileId();
	String profileName;  //Added by Abhilekh on 12th June 2019 for Automated Process User Error
	if(userFullName != 'Automated Process'){     //If condition (not whole If block) added by Abhilekh on 12th June 2019 for Automated Process User Error
	profileName=[Select Id,Name from Profile where Id=:profileId].Name;    
	}
	system.debug('ProfileName'+profileName);

	if(profileName !=  'System Administrator' && profileName !=  'Integration'){
	Set<String> setSubStage = new Set<String> ();
	Map<Id,List<String>> mapCustToSubStage = new Map<Id,List<String>>();
	//map<string,boolean> custToFlagMap = new map<string,boolean>();//Added by Chitransh for TIL-815
	List<Loan_Contact__c> lstLoanCon = [Select id,customer__c,loan_applications__r.sub_stage__c from Loan_Contact__c where customer__c IN :oldMap.KeySet()];//Edited By Chitransh for TIL-815 
	system.debug('@@lstLoanCon' + lstLoanCon);
	if ( !lstLoanCon.isEmpty() ) {
	for ( Loan_Contact__c obj : lstLoanCon ) {
	   setSubStage.add(obj.loan_applications__r.sub_stage__c); 
	   if( mapCustToSubStage.containsKey( obj.customer__c )){
		List<String> temp = mapCustToSubStage.get(obj.customer__c);
		temp.add(obj.loan_applications__r.sub_stage__c);
		mapCustToSubStage.put(obj.customer__c,temp);
	   } else {
		mapCustToSubStage.put(obj.customer__c,new List<String>{obj.loan_applications__r.sub_stage__c});
	   }
	}
	system.debug('@@mapCustToSubStage' + mapCustToSubStage);


	Set<String> pickListValuesList= new Set<String>();
	Schema.DescribeFieldResult fieldResult = Loan_Application__C.Sub_stage__c.getDescribe();
	List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	for( Schema.PicklistEntry pickListVal : ple){
	pickListValuesList.add(pickListVal.getValue());
	}     
	User u = [Select Id, Name from User where Id =: UserInfo.getUserId()]; // Added by KK for flow errors
	Set<String> setValid = new Set<String>{'Document collection','Express Queue: Data Checker','Express Queue: Data Maker','File Check','Scan: Data Maker','Scan: Data Checker','COPS:Data Maker','COPS:Data Checker','Application Initiation','Loan Cancel','Loan reject'}; //Added by Saumya Loan Cancel & Loan reject

	pickListValuesList.removeAll(setValid);
	system.debug('@@pickListValuesList' + pickListValuesList);


	for (Account objAcc : newList) {
	  System.debug('Debug Log for objAcc.Group_Id__c'+objAcc.Group_Id__c);
	  System.debug('Debug Log for oldMap.get(objAcc.id).Group_Id__c'+oldMap.get(objAcc.id).Group_Id__c);
	  if (
		(objAcc.Group_Id__c == oldMap.get(objAcc.id).Group_Id__c) &&
		(objAcc.Rejection_Reason__c == oldMap.get(objAcc.id).Rejection_Reason__c) &&
		(objAcc.Severity_Level__c == oldMap.get(objAcc.id).Severity_Level__c) &&
				(objAcc.Email__c == oldMap.get(objAcc.id).Email__c)//Added by Saumya For Defect TIL-00000420
	  ){
		if (mapCustToSubStage.containsKey(objAcc.id)) {
		  for ( String str : mapCustToSubStage.get(objAcc.id)) {
			system.debug('@@str'+str);
			system.debug('Debug Log for groupId'+objAcc.Group_Id__c);
			if ( pickListValuesList.contains(str) ) {
			  if (objAcc.Pan_Validated__c && (oldMap.get(objAcc.Id).PAN__c != objAcc.PAN__c || oldMap.get(objAcc.Id).Date_of_Birth__c != objAcc.Date_of_Birth__c || oldMap.get(objAcc.Id).Date_of_Incorporation__c != objAcc.Date_of_Incorporation__c || oldMap.get(objAcc.Id).Company_Registration_Number__c != objAcc.Company_Registration_Number__c || oldMap.get(objAcc.Id).Gender__c != objAcc.Gender__c || oldMap.get(objAcc.Id).Name != objAcc.Name)) {
				objAcc.addError('You cannot modify this record because this is PAN verified!');
				break;
				} else if (!objAcc.Pan_Validated__c && objAcc.PAN__c != null && objAcc.OwnerId == u.Id && objAcc.LMS_Existing_Customer__c == true && (oldMap.get(objAcc.Id).PAN__c != objAcc.PAN__c || oldMap.get(objAcc.Id).Date_of_Birth__c != objAcc.Date_of_Birth__c || oldMap.get(objAcc.Id).Date_of_Incorporation__c != objAcc.Date_of_Incorporation__c || oldMap.get(objAcc.Id).Company_Registration_Number__c != objAcc.Company_Registration_Number__c || oldMap.get(objAcc.Id).Gender__c != objAcc.Gender__c || oldMap.get(objAcc.Id).Name != objAcc.Name)) {//Edited by Chitransh for TIL-815//
				//  String checkId = objAcc.Id+''+lc.Id;//Added by Chitransh for TIL-815//
				//  if(custToFlagMap.get(checkId) == false){//Added by Chitransh for TIL-815//
				  
				  System.debug('objAcc.PAN__c==>'+objAcc.PAN__c+'objAcc.Id==>'+objAcc.Id);
				  if(u.Name != 'Automated Process'){
					  objAcc.addError('You cannot modify this record as PAN is available.');//because this is PAN verified!
				  }
				  break;
				}
								  //Added by Chitransh for TIL-815//
			  else if(!objAcc.Pan_Validated__c && objAcc.PAN__c != null && objAcc.OwnerId != u.Id && (profileName == 'DSA' || profileName == 'DST' || profileName == 'Sales Team' || profileName == 'Sales Coordinator' ) && (oldMap.get(objAcc.Id).PAN__c != objAcc.PAN__c || oldMap.get(objAcc.Id).Date_of_Birth__c != objAcc.Date_of_Birth__c || oldMap.get(objAcc.Id).Date_of_Incorporation__c != objAcc.Date_of_Incorporation__c || oldMap.get(objAcc.Id).Company_Registration_Number__c != objAcc.Company_Registration_Number__c || oldMap.get(objAcc.Id).Gender__c != objAcc.Gender__c || oldMap.get(objAcc.Id).Name != objAcc.Name)){
				  System.debug('objAcc.PAN__c==>'+objAcc.PAN__c+'objAcc.Id==>'+objAcc.Id);
				  if(u.Name != 'Automated Process'){
					  objAcc.addError('You cannot modify this record as PAN is available.');
				  }
				  break;
			  }
			  //End of patch Added by Chitransh for TIL-815//
			}
		  }
		}
	  }
	}

	}


	}
	}
	
	//Below method added by Abhilekh on 6th February 2020 for TIL-1942
    public static void retriggerHunter(List<Account> newList, Map<Id,Account> oldMap){
        Map<Id,Hunter_Required_Field__mdt> mapHunterFields= new Map<Id,Hunter_Required_Field__mdt>([Select Id, Field__c, Object__c, Critical_Field__c from Hunter_Required_Field__mdt where Object__c = 'Account' and Critical_Field__c = true]);
        
        for(Account acc: newList){
            for(Hunter_Required_Field__mdt objHunterFields: mapHunterFields.values()){
                if(acc.get(objHunterFields.Field__c) != oldMap.get(acc.Id).get(objHunterFields.Field__c)){
                    acc.Retrigger_Hunter__c = true;
                    break;
                }
            }
        }
    }
	}