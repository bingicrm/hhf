//
// Generated by JSON2Apex http://json2apex.herokuapp.com/
//

public class CRM_CaseItcFALoanResponse {

	public List<ReportParameterDetailBean> reportParameterDetailBean{get;set;}
	public AddressBean addressBean{get;set;}
	public Object errorMsg;

	public class ReportParameterDetailBean {
		public String agreementId;
		public Object closureDate;
		public Object loanNumber;
		public Object agreementDate;
		public Object viewFlag;
		public Object reportFlag;
		public Object url;
		public Object responseData;
		public Object fileStatusFlag;
		public Object mailId;
		public Object ccMailId;
		public String customerName{get;set;}
		public Object pdfData;
		public Object mailingStatus;
		public String fromDate;
		public String toDate;
		public String prepaymentAmount{get;set;}
		public Object changeType;
		public Object amountChangeValue;
		public Object emiChangeValue;
		public Object reportErrorCode;
		public Object loanType;
		public String customerId;
		public Object applicantType;
		public Object dbSchema;
		public Object entityId;
		public Object noteMessage;
		public Object alertMessage;
		public Object freeSOACount;
		public Object currSOACount;
		public Object applid;
		public Object customer;
		public Object disbursalDate;
		public String disbursalAmount{get;set;}
		public String currencyRepayment;
		public Object product;
		public Object emi;
		public Object firstEmiDueOn;
		public Object tenure;
		public Object userId;
		public String losLandmark;
		public Object lesseeId;
		public Object currencyId;
		public String country;
		public Object companyName;
		public Object adEmi;
		public Object dealerDesc;
		public Object assetDesc;
		public Object sessionId;
		public Object rateOfIntrest;
		public Object repaymentStartDate;
		public Object repaymentEndDate;
		public Object customerMobile;
		public Object customerCommunicationAddress;
		public Object phone1;
		public Object mobile;
		public Object productType;
		public Object email;
		public Object loanAmt;
		public Object vat;
		public Object propertyValue;
		public Object tenureInMonth;
		public Object installmentType;
		public Object depriciation;
		public Object vatInstl;
		public Object vatInt;
		public Object vatPrin;
		public Object ratePercent;
		public Object closingPrincipal;
		public Object serviceTax;
		public Object interest;
		public Object installmentAmount;
		public Object openingPrincipal;
		public Object dueDate;
		public Object netDisbursedAmount;
		public Object maturitydate;
		public Object frequency;
		public Object amountFinanced;
		public Object address1{get;set;}
		public Object address2{get;set;}
		public Object address3{get;set;}
		public Object address4{get;set;}
		public Object principle;
		public Object installments;
		public Object recAdvices;
		public Object penalty;
		public Object overDueCharges;
		public Object currentMonthInt;
		public Object advinstl;
		public Object currlPi;
		public Object currlpiWaveoffamt;
		public Object lpiTillDate;
		public Object woffPrepaypenalty;
		public String agreementNo;
		public Object principalOs;
		public Object lppCharges;
		public Object chequeBouncing;
		public Object intOnTermination;
		public Object foreclosure;
		public Object ppforeclosurechrgPercent;
		public Object ppforeclosurechrgPercentOn;
		public Object ppforeclosurechrgAmount;
		public Object pendingInstl;
		public Object refund;
		public Object nettot;
		public Object netpay;
		public Object dailyInterest;
		public Object tillDate;
		public Object foreclosurechrgPercent;
		public Object foreclosurechrgServicetax;
		public Object favouring;
		public Object processId;
		public Object dateRepayment;
		public Object interestPaid;
		public Object interestRatetype;
		public Object netReceivable;
		public Object branchState;
		public Object branchcity;
		public Object branchType;
		public Object applicationNo;
		public Object coapplicantName;
		public Object landStateCountry;
		public String propertyDesc;
		public Object propertyDddr1;
		public Object propertyDddr2;
		public Object propertyDddr3;
		public Object assetDescription;
		public Object regNo;
		public Object plr;
		public Object variance;
		public Object roi;
		public Object sanctionDate;
		public Object floatFlg;
		public Object customerIrr;
		public Object installmentPeriod;
		public Object rest;
		public Object amtFinanced;
		public Object amtDisbursed;
		public Object preemiOverdue;
		public Object unadjustedAmt;
		public Object futureInstallments;
		public Object futurePreemi;
		public Object instlPaid;
		public Object principalPaid;
		public Object preEmipaid;
		public Object status;
		public Object linked_agrno;
		public Object value_date;
		public Object perticular;
		public Object increaseBy;
		public Object decreaseBy;
		public Object odChargesDue;
		public Object odChargesPaid;
		public Object bonusChargesDue;
		public Object bonusChargesPaid;
		public Object sum;
		public Object userIdEnc;
		public Object passwordEnc;
		public String city{get;set;}
		public Object prePrincipleos;
		public Object branchDesc;
		public Object rescheduleNo;
		public Object productFlag;
		public Object advanceInstl;
		public Object reschedulingDate;
		public Object resEffectDate;
		public Object postEmi;
		public Object intGap;
		public String effRate;
		public Object addlDisb;
		public Object bulkRefund;
		public Object postEffRate;
		public Object postPrincipleOs;
		public Object postPendingLpp;
		public Object prepaymentCharges;
		public Object reschedulingCharges;
		public Object balGrRec;
		public Object balUnearnedInstl;
		public String stateDesc;
		public Object grossInt;
		public Object chequeBounce;
		public Object pendingLpp;
		public Object preTaxIrr;
		public Object tenureElapsed;
		public Object repaymentEffectDate;
		public Object reschedulingType;
		public Object balanceTenure;
		public String itCertificate;
		public String itCertType;
		public String salutation{get;set;}
		public String applnFormNo;
		public String loanFor{get;set;}
		public String typeValue;
		public String emiStartDate;
		public String pemiStartDate;
		public String schemeId;
		public String totalEmi{get;set;}
		public String prinEmi{get;set;}
		public String intEmi{get;set;}
		public String prinOsAmount;
		public String pemiAmount{get;set;}
		public Object address4CityZip;
		public Object cityZipCode{get;set;}
		public String currentDate;
		public String systemDate;
		public Object contactPerson;
		public String r1c1;
		public String r2c1;
		public String r3c1;
		public String r1c2;
		public String r2c2;
		public String r3c2;
		public String payablePreEmi;
		public String paidPreEmi;
		public Object letterId;
		public Object coApplNames;
		public Object pemiAmountPayable;
		public String emiAmount;
		public String interestComponent;
		public String principalComponent;
		public Object fcrCustomerId;
		public Object acknowledgementNo;
		public Object synchronousFlag;
		public String propertyDddr4;
		public String propertyDddr5;
		public Object instlFlag;
		public String disbStatus;
		public Object disbType;
		public Object assetBased;
		public Object branch;
		public Object interestRate;
		public Object flatIntType;
		public Object principal;
		public String seqNo;
		public String branchId;
		public Object zipCode;
		public Object statusFlag;
		public Object cnt;
		public Object lsoBranchDesc;
		public Object lsoSubbranchDesc;
		public Object advanceEmi;
		public Object agreementMode;
		public Object totalInstallment;
		public Object assetcost;
		public Object lsoCityDesc;
		public Object lsoStateDesc;
	}

	public class AddressBean {
		public Object tempCustId;
		public Object tempApplicationId;
		public Object tempAddrId;
		public Object addressType;
		public Object addressTypeDesc;
		public Object propertyStatus;
		public Object propertyStatusDesc;
		public String address1{get;set;}
		public String address2{get;set;}
		public String address3{get;set;}
		public String country;
		public Object countryDesc;
		public Object state;
		public String stateDesc{get;set;}
		public String city{get;set;}
		public Object cityDesc;
		public Object pinCode;
		public Object mobileNumber;
		public Object emailId;
		public Object createdDate;
		public Object isMailingFlag;
		public Object yearAtCurrentAddress;
		public Object yearAtCity;
		public Object existingCustomerId;
		public Object validatePinFlag;
		public Object monthsAtCurrentAddress;
		public Object monthsAtCity;
		public Object ucicNo;
		public String currentDate;
		public Object customerName;
		public Object address4{get;set;}
		public String losLandmark;
		public Object custName;
		public Object interestPaid;
		public Object interestRatetype;
		public Object netReceivable;
		public Object branchState;
		public Object branchcity;
		public Object branchType;
		public Object applicationNo;
		public Object coapplicantName;
		public Object landStateCountry;
		public String propertyDesc;
		public Object propertyDddr1;
		public Object propertyDddr2;
		public Object propertyDddr3;
		public Object assetDescription;
		public Object regNo;
		public Object plr;
		public Object variance;
		public Object roi;
		public Object sanctionDate;
		public Object floatFlg;
		public Object customerIrr;
		public Object installmentPeriod;
		public Object rest;
		public Object amtFinanced;
		public Object amtDisbursed;
		public Object instlOverdue;
		public Object preemiOverdue;
		public Object unadjustedAmt;
		public Object futureInstallments;
		public Object futurePreemi;
		public Object instlPaid;
		public Object principalPaid;
		public Object preEmipaid;
		public Object status;
		public Object linked_agrno;
		public Object branch;
		public Object product;
		public String currencyBean;
		public Object disbursalDate;
		public Object tenure;
		public Object frequency;
		public Object amountFinanced;
		public String cityZipCode{get;set;}
		public Object otherOverdues;
		public String agreementNo;
		public String proAddress1{get;set;}
		public String proAddress2{get;set;}
		public String proAddress3{get;set;}
		public String proCityPin{get;set;}
		public String zipCode{get;set;}
		public String address4CityZip;
		public Object lsoCityDesc;
		public Object lsoStateDesc;
		public Object subBranch;
		public Object loanType;
		public Object agreementDate;
		public Object propertyValue;
		public Object interestRate;
		public Object agreementMode;
		public Object totalInstl;
		public Object advanceEmi;
		public Object assetCost;
		public Object residualValue;
	}

	
	public static CRM_CaseItcFALoanResponse parse(String json) {
		return (CRM_CaseItcFALoanResponse) System.JSON.deserialize(json, CRM_CaseItcFALoanResponse.class);
	}
}