public  class SalesAuthorityUtility {
	


	public static String findSalesAuthority( id loanId )
	{
		Loan_Application__c allLoanApplication =  [Select Id,Difference_Cross_Sell__c,Difference_foreclosure_percent__c,Difference_processing_fee__c,Difference_ROI__c,Branch__c,Line_Of_Business__c From Loan_Application__c  where id = :loanId];
		
		String userId;
		String ReturnUser;
		Integer highestIndex = 0;
		String selectedUser  ;
		string ROIUser;
		string ForeclosureUser;
		List<Role_Rank_Mapping__mdt> allMappingBasedOnRoles = [ select Loan_Amount__c,Rank__c,Role__c from Role_Rank_Mapping__mdt  where process__c = 'Sales' ORDER BY Rank__c DESC ];
		selectedUser = allMappingBasedOnRoles[0].Role__c;
		map<string, decimal> rankMap = new map<string, decimal>();
		for(Role_Rank_Mapping__mdt rank: allMappingBasedOnRoles){

			 if(!rankMap.containsKey(rank.Role__c))
            {
                rankMap.put(rank.Role__c, rank.Rank__c);
                
            }
		}
		List<SalesApprovalMatrix__mdt> salesApproval = [select Approving_authority__c, fieldname__c, Max_Reduction__c, Min_Reduction__c, Vertical__c from SalesApprovalMatrix__mdt];
		
		list<document_checklist__c> docCheckList = [select id, Approving_authority__c, document_master__c from document_checklist__c 
		where REquest_Date_for_OTC__c != null and Loan_Applications__c = :loanId];

		system.debug('==loan app=='+allLoanApplication.Branch__c +'==='+allLoanApplication.Line_Of_Business__c);
		List<Branch_LOB_User_Mapping__c> allBLUs = [Select id,CSH__c,BH__c,Sales_Manager__c,Business_Head__c,CEO__c, NSH__c,ZSH__c from Branch_LOB_User_Mapping__c where Branch__c = :allLoanApplication.Branch__c AND Line_Of_Business__c = :allLoanApplication.Line_Of_Business__c];
		system.debug(allMappingBasedOnRoles);
		system.debug('==='+allBLUs);
		
		for(SalesApprovalMatrix__mdt sa:salesApproval){
		if((allLoanApplication.Difference_ROI__c <= sa.Max_Reduction__c)&&(allLoanApplication.Difference_ROI__c > sa.Min_Reduction__c)&&(sa.fieldname__c == 'ROI')&&(rankMap.get(sa.Approving_authority__c)< rankMap.get(selectedUser))){
			selectedUser = sa.Approving_authority__c;
			//}
		}
		else if((rankMap.get(sa.Approving_authority__c)< rankMap.get(selectedUser))&&(allLoanApplication.Difference_foreclosure_percent__c <= sa.Max_Reduction__c)&&(allLoanApplication.Difference_foreclosure_percent__c > sa.Min_Reduction__c)&&(sa.fieldname__c == 'Foreclosure')){
			selectedUser = sa.Approving_authority__c;
		}
		else if((rankMap.get(sa.Approving_authority__c)< rankMap.get(selectedUser))&&(allLoanApplication.Difference_Cross_Sell__c <= sa.Max_Reduction__c)&&(allLoanApplication.Difference_Cross_Sell__c > sa.Min_Reduction__c)&&(sa.fieldname__c == 'CrossSell')){
			selectedUser = sa.Approving_authority__c;
		}
		else if((rankMap.get(sa.Approving_authority__c)< rankMap.get(selectedUser))&&(allLoanApplication.Difference_processing_fee__c <= sa.Max_Reduction__c)&&(allLoanApplication.Difference_processing_fee__c > sa.Min_Reduction__c)&&(sa.fieldname__c == 'Processing fee')){
			selectedUser = sa.Approving_authority__c;
		}

	}

if(docCheckList.size() > 0){
	for(document_checklist__c doc: docCheckList){

		if(rankMap.get(doc.Approving_authority__c)< rankMap.get(selectedUser)){
			selectedUser = doc.Approving_authority__c;
		}


	}
}
	
	system.debug('==select=='+selectedUser);

  if( allBLUs.size() > 0 && String.isNotEmpty(selectedUser))
            {
             /*   if(selectedUser == 'CEO')
                {
                    ReturnUser = allBLUs[0].CEO__c;
                } 
                else if(selectedUser == 'National Sales Head')
                {
                    ReturnUser = allBLUs[0].NSH__c;
                }
                else if(selectedUser == 'Zonal Sales Head')
                {
                    ReturnUser = allBLUs[0].ZSH__c;
                }
                else if(selectedUser == 'Business Head')
                {
                    ReturnUser = allBLUs[0].Business_Head__c;
                }
                else if(selectedUser == 'Cluster Sales Head')
                {
                    ReturnUser = allBLUs[0].CSH__c;
                }
                else if(selectedUser == 'Branch Manager')
                {
                    ReturnUser = allBLUs[0].BH__c;
                }
                else if(selectedUser == 'Sales Manager')
                {
                    ReturnUser = allBLUs[0].Sales_Manager__c;
                }
                */

                ReturnUser = allBLUs[0].Business_Head__c;
            }
             

return ReturnUser;

	}


	public static String findCreditAuthority( Loan_Application__c allLoanApplication )
	{
		//Loan_Application__c allLoanApplication =  [Select Id,Difference_Cross_Sell__c,Difference_foreclosure_percent__c,Difference_processing_fee__c,Difference_ROI__c,Branch__c,Line_Of_Business__c From Loan_Application__c  where id = :loanApplicationId];
		
		String userId;
		String ReturnUser;
		Integer highestIndex = 0;
		String selectedUser  ;
		string ROIUser;
		string ForeclosureUser;
		List<Role_Rank_Mapping__mdt> allMappingBasedOnRoles = [ select Loan_Amount__c,Rank__c,Role__c from Role_Rank_Mapping__mdt  where process__c = 'Sales' ORDER BY Rank__c DESC ];
		selectedUser = allMappingBasedOnRoles[0].Role__c;
		map<string, decimal> rankMap = new map<string, decimal>();
		for(Role_Rank_Mapping__mdt rank: allMappingBasedOnRoles){

			 if(!rankMap.containsKey(rank.Role__c))
            {
                rankMap.put(rank.Role__c, rank.Rank__c);
                
            }
		}
		List<SalesApprovalMatrix__mdt> salesApproval = [select Approving_authority__c, fieldname__c, Max_Reduction__c, Min_Reduction__c, Vertical__c from SalesApprovalMatrix__mdt];
		system.debug('==loan app=='+allLoanApplication.Branch__c +'==='+allLoanApplication.Line_Of_Business__c);
		List<Branch_LOB_User_Mapping__c> allBLUs = [Select id,CSH__c,BH__c,Sales_Manager__c,Business_Head__c,CEO__c, NSH__c,ZSH__c from Branch_LOB_User_Mapping__c where Branch__c = :allLoanApplication.Branch__c AND Line_Of_Business__c = :allLoanApplication.Line_Of_Business__c];
		system.debug(allMappingBasedOnRoles);
		system.debug('==='+allBLUs);
		
		for(SalesApprovalMatrix__mdt sa:salesApproval){
		if((allLoanApplication.Difference_ROI__c <= sa.Max_Reduction__c)&&(allLoanApplication.Difference_ROI__c > sa.Min_Reduction__c)&&(sa.fieldname__c == 'ROI')&&(rankMap.get(sa.Approving_authority__c)< rankMap.get(selectedUser))){
			selectedUser = sa.Approving_authority__c;
			//}
		}
		else if((rankMap.get(sa.Approving_authority__c)< rankMap.get(selectedUser))&&(allLoanApplication.Difference_foreclosure_percent__c <= sa.Max_Reduction__c)&&(allLoanApplication.Difference_foreclosure_percent__c > sa.Min_Reduction__c)&&(sa.fieldname__c == 'Foreclosure')){
			selectedUser = sa.Approving_authority__c;
		}
		else if((rankMap.get(sa.Approving_authority__c)< rankMap.get(selectedUser))&&(allLoanApplication.Difference_Cross_Sell__c <= sa.Max_Reduction__c)&&(allLoanApplication.Difference_Cross_Sell__c > sa.Min_Reduction__c)&&(sa.fieldname__c == 'CrossSell')){
			selectedUser = sa.Approving_authority__c;
		}
		else if((rankMap.get(sa.Approving_authority__c)< rankMap.get(selectedUser))&&(allLoanApplication.Difference_processing_fee__c <= sa.Max_Reduction__c)&&(allLoanApplication.Difference_processing_fee__c > sa.Min_Reduction__c)&&(sa.fieldname__c == 'Processing fee')){
			selectedUser = sa.Approving_authority__c;
		}

	}
	
	system.debug('==select=='+selectedUser);

  if( allBLUs.size() > 0 && String.isNotEmpty(selectedUser))
            {
                if(selectedUser == 'CEO')
                {
                    ReturnUser = allBLUs[0].CEO__c;
                } 
                else if(selectedUser == 'National Sales Head')
                {
                    ReturnUser = allBLUs[0].NSH__c;
                }
                else if(selectedUser == 'Zonal Sales Head')
                {
                    ReturnUser = allBLUs[0].ZSH__c;
                }
                else if(selectedUser == 'Business Head')
                {
                    ReturnUser = allBLUs[0].Business_Head__c;
                }
                else if(selectedUser == 'Cluster Sales Head')
                {
                    ReturnUser = allBLUs[0].CSH__c;
                }
                else if(selectedUser == 'Branch Manager')
                {
                    ReturnUser = allBLUs[0].BH__c;
                }
                else if(selectedUser == 'Sales Manager')
                {
                    ReturnUser = allBLUs[0].Sales_Manager__c;
                }
            }

return ReturnUser;

	}
}