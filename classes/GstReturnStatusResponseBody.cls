public class GstReturnStatusResponseBody {
    
    public cls_result[] result;
    public String requestId;	//3852e90e-b90a-11e9-9fe7-d9cbf53ef6bd
	public Integer statusCode;	//101
	//public cls_error error;
    public String error;
    /*public class cls_error {
        public String errorCode;
        public String errorType;
        public String errorMessage;
    }*/
	public class cls_result {
		public String filing_frequency;	//Monthly
		public String financial_year;	//2018-19
		public cls_EFiledlist[] EFiledlist;
	}
	public class cls_EFiledlist {
		public String status;	//Filed
		public String dof;	//04-06-2018
		public String ret_prd;	//042018
		public boolean is_delay;
		public String valid;	//Y
		public String rtntype;	//GSTR1
		public String mof;	//ONLINE
		public String arn;	//AB270418487880K
	}
    
	/*public static fromJSON parse(String json){
		return (fromJSON) System.JSON.deserialize(json, fromJSON.class);
	}
    /*public cls_result result;
    public String requestId;	//3852e90e-b90a-11e9-9fe7-d9cbf53ef6bd
    public Integer statusCode;	//101
    public class cls_result {
        public cls_result[] result;
        public String filing_frequency;	//Monthly
        public String financial_year;	//2018-19
        public cls_EFiledlist[] EFiledlist;
    }
    public class cls_compliance_status {
        public boolean is_defaulter;
        public boolean is_any_delay;
    }
    public class cls_EFiledlist {
        public String status;	//Filed
        public String dof;	//31-05-2018
        public String ret_prd;	//042018
        public boolean is_delay;
        public String valid;	//Y
        public String rtntype;	//GSTR1
        public String mof;	//ONLINE
        public String arn;	//AA190418581477J
    }
    //public static fromJSON parse(String json){
    //	return (fromJSON) System.JSON.deserialize(json, fromJSON.class);
    //}
    
    /*public cls_result[] result;
public String status_code;	//101
public String request_id;	//62692af0-246f-11e9-b70c-4719335f1e77
class cls_result {
public String financial_year;	//2017-18
public cls_EFiledlist[] EFiledlist;
}
class cls_EFiledlist {
public String status;	//Filed
public String dof;	//20-03-2018
public String ret_prd;	//022018
public String valid;	//Y
public String rtntype;	//GSTR3B
public String mof;	//ONLINE
public String arn;	//AA2702187495403
}
//public static GstReturnStatusResponseBody parse(String json){
//	return (GstReturnStatusResponseBody) System.JSON.deserialize(json, GstReturnStatusResponseBody.class);
//}
*/
}