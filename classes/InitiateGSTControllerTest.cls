/************************************************************************************
Test Class Name     : InitiateGSTControllerTest
Version             : 1.0
Created Date        : 16th Dec 2020
Function            : Test class for InitiateGSTController
Author              : Amit Agarwal (Deloitte India)
Modification Log    :
* Developer                 Date                   Description
* ----------------------------------------------------------------------------                 
*************************************************************************************/
@isTest
public class InitiateGSTControllerTest {
 @testSetup
    private static void testSetupMethod() {
        Profile p = [SELECT id, name from profile where name =: 'Sales Team'];
        User u = new User(
            ProfileId = p.Id,LastName = 'last',Email = 'puser000@amamama.com',Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',Title = 'title',Alias = 'alias',TimeZoneSidKey = 'America/Los_Angeles',EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US',Technical__c = true,FCU__c= true,Legal__c = true, FI__c= true,LIP__C = true,PD__c= true, Role_in_Sales_Hierarchy__c ='SM');
        insert u;        
        Loan_Contact__c lc;
        Loan_Application__c la;
        
        System.runAs(u) {
            Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
            
            Account acc = new Account();
            acc.CountryCode__c = '+91';
            acc.Email__c = 'puser001@amamama.com';
            acc.Gender__c = 'M';
            acc.Mobile__c = '9800765432';
            acc.Phone__c = '9800765434';
            acc.PersonMobilePhone = '9800765434';
            acc.FirstName = 'abc';
            acc.LastName = 'def';
            acc.Salutation = 'Mr';
            acc.RecordTypeId = RecordTypeIdCustomer;
            insert acc;
          
            Scheme__c sch = new Scheme__c(Name = 'Construction Finance', Scheme_Code__c = 'CF', Scheme_Group_ID__c = '5',
                                          Scheme_ID__c = 11, Product_Code__c = 'CF', Max_ROI__c = 10, Min_ROI__c = 5,
                                          Min_Loan_Amount__c = 10000, Min_FOIR__c = 1,
                                          Max_Loan_Amount__c = 3000001, Max_Tenure__c = 84, Min_Tenure__c = 6,
                                          Max_LTV__c = 5, Min_LTV__c = 1, Max_FOIR__c = 5);
            insert sch;
            
            Scheme__c sch1=new Scheme__c();
            sch1.Name='Personal Loan';
            sch1.Product_Code__c='IPL'; 
            sch1.Scheme_Code__c='IPL';
            sch1.Scheme_Group_ID__c='Test';
            insert sch1;
            Local_Policies__c lp = new Local_Policies__c(Name='Standard', Branch__c='All',Product_Scheme__c='All Schemes',IsActive__c=true);
            insert lp;
            
            Id RecordTypeIdLAAppIni = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Application Initiation').getRecordTypeId();
            
            la = new Loan_Application__c(Customer__c = acc.Id, Scheme__c = sch.Id, Loan_Number__c = '124',
                                         Transaction_type__c = 'PB', Requested_Amount__c = 1000000,
                                         Loan_Purpose__c = '11', StageName__c = 'Customer Onboarding', Sub_Stage__c = 'Disbursement Checker',
                                         RecordTypeId = RecordTypeIdLAAppIni,Branch__c='Delhi',
                                         Business_Date_Created__c = System.today(),
                                         Repayment_schedule_at_Disbursement_Check__c = true,
                                         Approved_Loan_Amount__c = 1000000,Requested_Loan_Tenure__c=84,
                                         Processing_Fee_Percentage__c = 1, Mode_of_Repayment__c = 'NACH',
                                         Repayment_Start_Date__c = Date.Today().addDays(100),Income_Program_Type__c='Normal Salaried',
                                         Sanctioned_Disbursement_Type__c = 'Multiple Tranche', Property_Identified__c = true,
                                         Local_Policies__c=lp.Id);
            insert la;
            
            lc = [SELECT Id, Borrower__c from Loan_Contact__c where Loan_Applications__c =: la.Id];
            lc.Borrower__c = '1';           
            lc.Gender__c = 'Male';
            lc.Customer__c = acc.Id;
            lc.Applicant_Type__c = 'Applicant';
            lc.Constitution__c = '20';
            lc.Customer_segment__c = '3';
            lc.BRE_Record__c = true;
            lc.Date_Of_Birth__c = Date.newInstance(1990, 01, 01);
            lc.Marital_Status__c = 'M';
            lc.Gross_Salary__c = 1000;
            lc.Basic_Pay__c = 1000;
            lc.DA__c = 1000;
            lc.Qualification__c = 'UMS';
            lc.Net_Income__c = 1000;
            lc.PF__c = 1000;
            lc.TDS__c = 1000;
            lc.Current_Organization_Work_Ex_in_Months__c = 12;
            lc.Total_Work_Experience__c = 12;
            lc.Customer_Details_Verified__c = true;
            lc.Voter_ID_Number__c = '123458967';
            lc.Passport_Number__c = '123456789';
            lc.PAN_Number__c = 'BAFPV6543A';
            lc.Aadhaar_Number__c = '898518762736';
            lc.Income_Considered__c = true;
            lc.AIP_month__c = '4-2020';
            lc.Mobile__c = '7878565698';
            lc.Email__c = 'abc@g.com';
            lc.Applicant_Type__c='Applicant';
            update lc;
            
            Id parameter = la.id;
            UCIC_Global__c setting = new UCIC_Global__c();
            setting.UCIC_Active__c = true;
            insert setting; 
            
             Id recordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('GSTIN').getRecordTypeId();
            
            Customer_Integration__c cust = new Customer_Integration__c(Loan_Application__c = la.Id,
                                                                       Mobile__c ='9876543210',                                                                    
                                                                       Constitution__c = 'ABCD',
                                                                       GST_Tax_payer_type__c = 'TaxPayerType',
                                                                       Current_Status_of_Registration_Under_GST__c = 'CurrentStatus',
                                                                       VAT_Registration_Number__c = '27AAHFG0551H1234',
                                                                       PAN_Number__c = 'BCEST2489G',
                                                                       Trade_Name__c = 'Trade Name',
                                                                       Legal_Name__c ='Legal Name',
                                                                       Filling_Frequency__c = '23',
                                                                       Business_Activity__c = 'Business Activity',
                                                                       GST_Date_Of_Registration__c = Date.newInstance(2018, 12, 10),
                                                                       Cumulative_Sales_Turnover__c = 5000000,
                                                                       Month_Count__c = 50,
                                                                       GSTIN__c = '27AAHFG0551H1ZL', 
                                                                       Email__c = 'testemail@gmail.com',
                                                                       RecordTypeId = recordTypeId,
                                                                       Loan_Contact__c = lc.Id,  
                                                                       GSTIN_Error_Message__c = 'GSTIN Error',                                                                      
                                                                       PerfiosITRReport_Error_Response__c = 'test1',
                                                                       PerfiosBankReport_Error_Response__c = 'test2',
                                                                       PerfiosFinancialReport_Error_Response__c = 'test3');
        insert cust;
        
        }
    }
    
    
    public static testmethod void method1(){
        User u = [Select id from User Where Email = 'puser000@amamama.com'];
        System.runAs(u){
            List<InitiateGSTController.cls_LoanContactListWrapper> objWrapresonse = new List<InitiateGSTController.cls_LoanContactListWrapper>();
            Loan_Application__c la = [Select id from Loan_Application__c WHERE StageName__c =: 'Customer Onboarding' LIMIT 1];
            Test.startTest();
            objWrapresonse = InitiateGSTController.getCriteriaList(la.id);  
            System.assertEquals(new List<InitiateGSTController.cls_LoanContactListWrapper>(), objWrapresonse);
            Loan_Contact__c lc = [SELECT Id,Loan_Applications__r.id from Loan_Contact__c where Loan_Applications__r.id =: la.id AND Loan_Applications__r.StageName__c =: 'Customer Onboarding' LIMIT 1];
            lc.GSTIN_Number__c = '27AAHFG0551H1ZL';
            update lc;
            objWrapresonse =  InitiateGSTController.getCriteriaList(la.id); 
            System.assertNotEquals(Null, objWrapresonse);
            InitiateGSTController.makeGSTINAuthCallout(JSON.serialize(objWrapresonse));            
            
            Customer_Integration__c ci = [Select id,Loan_Contact__r.Name,GSTIN_Error_Message__c  FROM Customer_Integration__c WHERE Loan_Contact__r.id =: lc.id LIMIT 1];
            List<String> retStr =  InitiateGSTController.getErrorMessage(JSON.serialize(objWrapresonse));
            String str = ci.Loan_Contact__r.Name + ' - ' + ci.GSTIN_Error_Message__c;
            System.assertNotEquals(Null, retStr[0]);
            Test.stopTest();    
        }       
    }
}