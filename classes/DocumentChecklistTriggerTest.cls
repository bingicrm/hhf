@isTest
public class DocumentChecklistTriggerTest {
     @TestSetup
    static void setup()
    { 
        Document_Checklist__c docObj1 = new Document_Checklist__c();
        String strLoaId ;
        String strStatus ;
        String strCreditRemarks ;
        User u1 = [select Id,Name from User where Id = :UserInfo.getUserId()];
        Profile p=[SELECT id,name from profile where name=:'Sales Team'];
        UserRole r = [Select Id From UserRole Where Name =: 'BCM - Delhi 1.1' limit 1];
        User u = new User(ProfileId = p.Id,LastName = 'last',Email = 'puser000@amamama.com',Username = 'puser000@amamama.com' + System.currentTimeMillis(),
                          CompanyName = 'TEST',Title = 'title',Alias = 'alias',
                          TimeZoneSidKey = 'America/Los_Angeles',EmailEncodingKey = 'UTF-8',
                          LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US',
                          UserRoleId = r.Id,FCU__c =true, Role_in_Sales_Hierarchy__c = 'SM'); 
        insert u;
        
        system.runAs(u){
            //test.startTest();
            Account custObj = new Account();
            custObj.Name = 'Test Customer';
            custObj.Phone = '9999999999';
            database.insert(custObj);
            
            scheme__c schemeObj = new scheme__c();
            schemeObj.Name = 'Scheme Name';
            schemeObj.Scheme_Group_ID__c = '12345';
            schemeObj.Scheme_Code__c = '12345';
            database.insert(schemeObj);
            
            Loan_Application__c loanAppObj = new Loan_Application__c();
            loanAppObj.customer__c = custObj.id;
            loanAppObj.Loan_Application_Number__c = 'LA00001';
            loanAppObj.Loan_Purpose__c = '11';
            loanAppObj.scheme__c = schemeObj.id;
            loanAppObj.Transaction_type__c = 'PB';
            loanAppObj.Requested_Amount__c = 50; 
            loanAppObj.Assigned_Sales_User__c = u.id;
            loanAppObj.Pending_Amount_for_Disbursement__c = 56000;
            Database.insert(loanAppObj);
            test.startTest();
            Loan_Application__c loanAppObj1 = new Loan_Application__c();
            loanAppObj1.customer__c = custObj.id;
            loanAppObj1.Loan_Application_Number__c = 'LA00002';
            loanAppObj1.Loan_Purpose__c = '11';
            loanAppObj1.scheme__c = schemeObj.id;
            loanAppObj1.Transaction_type__c = 'PB';
            loanAppObj1.Requested_Amount__c = 90; 
            loanAppObj1.Pending_Amount_for_Disbursement__c = 52000;
            Database.insert(loanAppObj1);
            
            Document_Master__c docMasObj = new Document_Master__c();
            docMasObj.Name = 'Downsizing Agreement';
            docMasObj.Doc_Id__c = '123344';
            docMasObj.Approving_authority__c = 'Business Head';
            Database.insert(docMasObj);
            
            Document_Master__c docMasObj1 = new Document_Master__c();
            docMasObj1.Name = 'PTM';
            docMasObj1.Doc_Id__c = '123345';
            docMasObj1.Approving_authority__c = 'Business Head';
            Database.insert(docMasObj1);
            
            Document_type__c docTypeObj = new Document_type__c();
            docTypeObj.Name = 'Test Document Type';
            docTypeObj.Approving_authority__c = 'Business Head';
            Database.insert(docTypeObj);
            List<Document_Type__c> lstDocTyp = new List<Document_Type__c>();
            Document_Type__c DType=new Document_Type__c(Name='Others');
            Document_Type__c DType1=new Document_Type__c(Name=Constants.PROPERTYPAPERS);
            lstDocTyp.add(DType);
            lstDocTyp.add(DType1);
            insert lstDocTyp;
            
            List<Document_Checklist__c> lstofDocs = new List<Document_Checklist__c>();
            Document_Checklist__c docObj = new Document_Checklist__c();
            docObj.Loan_Applications__c = loanAppObj.id;
            docObj.status__c = 'Pending';
            docObj.document_type__c = DType1.id;
            docObj.Document_Master__c = docMasObj.id;
            docObj.Express_Queue_Mandatory__c = true;
            docObj.Document_Collection_Mode__c = 'Photocopy';
            docObj.Screened_p__c = 'Yes';
            docObj.Sampled_p__c = 'Yes';
            docObj.Scan_Check_Completed__c = false;
            lstofDocs.add(docObj);
            docObj1.Loan_Applications__c = loanAppObj1.id;
            docObj1.status__c = 'Pending';
            docObj1.document_type__c = DType1.id;
            docObj1.Document_Master__c = docMasObj.id;
            docObj1.Express_Queue_Mandatory__c = true;
            docObj1.Document_Collection_Mode__c = 'Photocopy';
            docObj1.Screened_p__c = 'Yes';
            docObj1.Sampled_p__c = 'Yes';
            lstofDocs.add(docObj1);
            insert lstofDocs;
            //Database.insert(docObj);
            
            //Create Document
            ContentVersion cv = new ContentVersion();
            cv.Title = 'Test Document';
            cv.PathOnClient = 'TestDocument.pdf';
            cv.VersionData = Blob.valueOf('Test Content');
            cv.IsMajorVersion = true;
            Insert cv;
            
            //Get Content Documents
            Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
            
            //Create ContentDocumentLink 
            ContentDocumentLink cdl = New ContentDocumentLink();
            cdl.LinkedEntityId = docObj.Id;
            cdl.ContentDocumentId = conDocId;
            cdl.shareType = 'V';
            Insert cdl;
            
            ContentDocumentLink cdll = New ContentDocumentLink();
            cdll.LinkedEntityId = docObj1.Id;
            cdll.ContentDocumentId = conDocId;
            cdll.shareType = 'V';
            Insert cdll;
            
            //Create ContentDocumentLink 
            ContentDocumentLink cdl1 = New ContentDocumentLink();
            cdl1.LinkedEntityId = loanAppObj.Id;//objDocCheck.Id;
            cdl1.ContentDocumentId = conDocId;
            cdl1.shareType = 'V';
            Insert cdl1;
            
            ContentDocumentLink cdl2 = New ContentDocumentLink();
            cdl2.LinkedEntityId = loanAppObj1.Id;//objDocCheck.Id;
            cdl2.ContentDocumentId = conDocId;
            cdl2.shareType = 'V';
            Insert cdl2;
            
            Attachment att=new Attachment();
            att.ParentId = loanAppObj.Id ;
            att.Name='Passport';
            att.Body=Blob.valueOf('Passport');
            insert att;
            
            Attachment att1=new Attachment();
            att1.ParentId = loanAppObj1.Id ;
            att1.Name='Passport';
            att1.Body=Blob.valueOf('Passport');
            insert att1;
           
            docObj.Status__c = 'Uploaded';
            update docObj;
            DocumentType_Document_Mapping__c doctypemap = new DocumentType_Document_Mapping__c();
            doctypemap.Document_Master__c = docMasObj1.id;
            doctypemap.Document_Type__c = DType1.id;
            insert doctypemap;
                
            docObj1.Status__c = 'Uploaded';
            update docObj1;
            
            Id RecordTypeIdLA = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Loan Disbursal').getRecordTypeId();
            loanAppObj1.StageName__c='Loan Disbursal';
            loanAppObj1.Sub_Stage__c='Scan Checker';
            loanAppObj1.RecordTypeId=RecordTypeIdLA;
            update loanAppObj1;
            docObj1.Scan_Check_Completed__c = true;
            update docObj1;
            try
            {
              LoanDownsizingController.approveRejectLoanDownsizing(strLoaId,strStatus,strCreditRemarks);   
            }
            catch(Exception ex)
            {
                system.assert(false, 'PDDs Review Should be Checked before Approving or Rejecting Loan Downsizing.');
            }   
        }        
    }
    
      public static testMethod void testmethod1(){
          Test.startTest();
          Map<id,Document_checklist__c> docListMap = new  Map<id,Document_checklist__c>( [Select id ,Status__c,Document_Type__c from document_checklist__c ]);
          List<Document_checklist__c> docList1 = [Select id ,Status__c,Document_Type__c from document_checklist__c ];
          List<Document_checklist__c> docList = new List<Document_checklist__c>();
          Set<id> docLstId = new Set<Id>();
          docLstId.add(docList1[0].id);
          DocumentChecklistTriggerHelper.completeOCR(docLstId);   
          Test.stopTest();
      }
                                                  
                                                  
    public static testMethod void testmethod2(){
        Test.startTest();     
        Map<id,Document_checklist__c> docListMap = new  Map<id,Document_checklist__c>( [Select id ,Status__c,Document_Type__c,Document_Master__c from document_checklist__c WHERE Document_Type__r.Name <> 'Others']);
        List<Document_checklist__c> docList1 = [Select id ,Status__c from document_checklist__c ];
        List<Document_checklist__c> docList = new List<Document_checklist__c>();
        List<Document_Type__c> DType= [Select id,Name From Document_Type__c WHERE Name =: 'Others' LIMIT 1];
        docList1[0].Status__c = 'Uploaded';
        docList1[0].Scan_Check_Completed__c = TRUE;
        docList1[0].OTC_PDD_check__c = TRUE;
        update docList1;
        
        List<Document_checklist__c> docList2 = [Select id ,Status__c,Document_Master__c,Document_Type__c from document_checklist__c  Where Status__c =:'Uploaded' AND Document_Type__r.Name =: 'Others'];
        DocumentChecklistTriggerHelper.DocumentMapping(docList2,docListMap);
        
         docList1[0].REquest_Date_for_OTC__c = System.today() + 1;
         docList1[0].Status__c = 'Lawyer OTC';
         update docList1;
        
        List<Document_checklist__c> docList3 = [Select id ,Status__c,Document_Master__c,Document_Type__c from document_checklist__c  Where Status__c =:'Lawyer OTC' AND Document_Type__r.Name =: 'Others'];
        DocumentChecklistTriggerHelper.DocumentMapping(docList3,docListMap);
        
         // docList1[0].REquest_Date_for_OTC__c = System.today() + 1;
         docList1[0].Scan_Check_Completed__c =false;
         docList1[0].Status__c = 'PDD';
         update docList1;
        
          Map<id,Document_checklist__c> docListMap2 = new  Map<id,Document_checklist__c>( [Select id ,Status__c,Document_Type__c,Document_Master__c from document_checklist__c WHERE Status__c =: 'Lawyer OTC']);
        List<Document_checklist__c> docList4 = [Select id ,Status__c,Document_Master__c,Document_Type__c from document_checklist__c  Where Status__c =:'PDD' AND Document_Type__r.Name =: 'Others'];
        DocumentChecklistTriggerHelper.DocumentMapping(docList4,docListMap2);
        DocumentChecklistTriggerHelper.createPropertyDocChecklist(docList4);
        Test.stopTest();
      }
}