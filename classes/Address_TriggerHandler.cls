/*=====================================================================
 * Deloitte India
 * Name: Address_TriggerHandler
 * Description: This class is a handler class and supports Address trigger.
 * Created Date: [10/15/2018]
 * Created By: Gaurav Nawal (Deloitte India)
 *
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/
public class Address_TriggerHandler {
    
    public static void beforeInsert(List<Address__c> lstAddress) {
/***********************************Added by Chitransh Porwal for duplicate detection (TIL-1276) on 6 August 2019*******************************88**/
        Map<String, Integer> mapUniqueConstrainttoCount = new Map<String, Integer>();
        List<Address__c> lstExistingAddressRecord = new List<Address__c>();
        String strUniqueConstraint;
        
        Set<String> setLCId = new Set<String>();
        for(Address__c add: lstAddress){
            setLCId.add(add.Loan_Contact__c);
        }
        
		validateAddress(lstAddress);
		
        if(lstAddress.size() >1) {
            lstExistingAddressRecord = [Select Id, Name, Loan_Contact__c,Type_of_address__c from Address__c WHERE Loan_Contact__c IN: setLCId LIMIT 49999];
            if(!lstExistingAddressRecord.isEmpty()) {
                for(Address__c objExistingAddressRecord : lstExistingAddressRecord) {
                    if(!mapUniqueConstrainttoCount.containsKey(objExistingAddressRecord.Loan_Contact__c + objExistingAddressRecord.Type_of_address__c)) {
                        mapUniqueConstrainttoCount.put(objExistingAddressRecord.Loan_Contact__c + objExistingAddressRecord.Type_of_address__c, 1);
                    }
                    else {
                         mapUniqueConstrainttoCount.put(objExistingAddressRecord.Loan_Contact__c + objExistingAddressRecord.Type_of_address__c,mapUniqueConstrainttoCount.get(objExistingAddressRecord.Loan_Contact__c + objExistingAddressRecord.Type_of_address__c)+1);
                    }
                }
            }
            for(Address__c objAddressRecord : lstAddress) {
                if(!mapUniqueConstrainttoCount.containsKey(objAddressRecord.Loan_Contact__c + objAddressRecord.Type_of_address__c)) {
                    mapUniqueConstrainttoCount.put(objAddressRecord.Loan_Contact__c + objAddressRecord.Type_of_address__c, 1);
                }
                else {
                    objAddressRecord.addError('You are trying to create a duplicate record. A record already exists for the specified Loan Application.');
                   }
            }
        }
        else {
            lstExistingAddressRecord = [Select Id, Name, Loan_Contact__c,Type_of_address__c from Address__c where Loan_Contact__c =: lstAddress[0].Loan_Contact__c and Type_of_address__c = : lstAddress[0].Type_of_address__c LIMIT 49999];
            if(!lstExistingAddressRecord.isEmpty()) {
                lstAddress[0].addError('You are trying to create a duplicate record. A record already exists for the specified Loan Application.');
            }
        
        
    }
        
/****************************************End of Patch added by Chitransh Porwal for duplicate detection (TIL-1276)[26 Aug 2019]******************/
        /*List<Address__c> addList = new List<Address__c>();
        list<Id> parentId = new list<Id>();
        
        for(Address__c ad : lstAddress){
            if(ad.Loan_Contact__c != null){
                parentId.add(ad.Loan_Contact__c);
            }
        }
        
        addList = [Select Id, Name, Type_of_address__c,Loan_Contact__c, CreatedDate from Address__c where Loan_Contact__c IN:parentId limit 50000];
        for(Address__c add: addList){
        system.debug('Address----'+add);
            for(Address__c a: lstAddress){
            system.debug('a----'+a);
                if(a.Loan_Contact__c == add.Loan_Contact__c && a.Type_of_address__c == add.Type_of_address__c){
                    a.addError('You cannot create this type of address as it already exists on this Customer Detail: '+add.Name);
                }
            }
        }
        
        addressToUpper(lstAddress);*/
    }

    public static void afterInsert(List<Address__c> lstAddress, Map<Id, Address__c> newMap) {
        
    }

    public static void beforeUpdate(List<Address__c> lstAddress,Map<Id, Address__c> newMap, Map<Id, Address__c> oldMap) {
        addressToUpper(lstAddress);
		validateAddress(lstAddress);
		retriggerHunter(lstAddress,oldMap); //Added by Abhilekh on 6th February 2020 for TIL-1942
		
        // Added  by Mehul for Corp CIBIL 03-04
        Map<Id,CIBIL_Required_Field__mdt> mapCorpCIBIL= new Map<Id,CIBIL_Required_Field__mdt>([Select Id, Field__c, Object__c, Critical_Field__c from CIBIL_Required_Field__mdt where Object__c = 'Address__c' and Critical_Field__c = true]);
		
		//Added by Abhilekh on 9th May 2019 for TIL-736
        Map<Id,CIBIL_Individual_Required_Field__mdt> mapIndividualCIBIL = new Map<Id,CIBIL_Individual_Required_Field__mdt>([Select Id, Field__c, Object__c, Critical_Field__c from CIBIL_Individual_Required_Field__mdt where Object__c = 'Address__c' and Critical_Field__c = true]);
        
        Set<Id> setAddId = new Set<Id>(); //Added by Abhilekh on 9th May 2019 for TIL-736
        
        //Below for block added by Abhilekh on 9th May 2019 for TIL-736
        for(Address__c add: newMap.values()){
            setAddId.add(add.Id);
        }
        
        List<Address__c> lstAdd = [SELECT Id,Name,Loan_Contact__c,Loan_Contact__r.Borrower__c from Address__c WHERE Id IN: setAddId]; //Added by Abhilekh on 9th May 2019 for TIL-736
        
        Map<Id,String> mapAddToBorrowerType = new Map<Id,String>(); //Added by Abhilekh on 9th May 2019 for TIL-736
        
        //Below for block added by Abhilekh on 9th May 2019 for TIL-736
        for(Address__c add: lstAdd){
            mapAddToBorrowerType.put(add.Id,add.Loan_Contact__r.Borrower__c);
        }
		
        for ( Address__c acc: newMap.values()) {
            for ( CIBIL_Required_Field__mdt objCorpCIBIL : mapCorpCIBIL.values() ) {
                if(acc.get(objCorpCIBIL.Field__c) != oldMap.get(acc.id).get(objCorpCIBIL.Field__c) && !mapAddToBorrowerType.isEmpty() && mapAddToBorrowerType.containsKey(acc.Id) && mapAddToBorrowerType.get(acc.Id) == '2') {
                    acc.Retrigger_Corp_CIBIL__c = true;
                    system.debug('in retrigger corp cibil');
                    break;
                }
            }
			
			//Below for block added by Abhilekh on 9th May 2019 for TIL-736
            for(CIBIL_Individual_Required_Field__mdt objIndiCIBIL : mapIndividualCIBIL.values() ){
                if(acc.get(objIndiCIBIL.Field__c) != oldMap.get(acc.id).get(objIndiCIBIL.Field__c) && !mapAddToBorrowerType.isEmpty() && mapAddToBorrowerType.containsKey(acc.Id)){
                    if(mapAddToBorrowerType.get(acc.Id) == '1'){
                        acc.Retrigger_Individual_CIBIL__c = true;
                        system.debug('Inside for==>'+acc.Retrigger_Individual_CIBIL__c);
                        break;    
                    }
                }
            }
        }
    }

    public static void afterUpdate(Map<Id, Address__c> newMap, Map<Id, Address__c> oldMap) {
		//*******************************************************************Added by Vaishali for BRE
		Map<Id,BRE_Eligibility_reset_field__mdt	> mapBREEligibilityReset = new Map<Id,BRE_Eligibility_reset_field__mdt	>([Select Id, Field__c, Object__c, Active__c from BRE_Eligibility_reset_field__mdt where Object__c = 'Address__c' and Active__c = true]);
		Set<Id> setOfCustomerIds = new Set<Id>();
		for(Address__c add : newMap.values()) {
			for ( BRE_Eligibility_reset_field__mdt objbreReset : mapBREEligibilityReset.values() ) {
				if( add.get(objbreReset.Field__c) != oldMap.get(add.id).get(objbreReset.Field__c)) {
					setOfCustomerIds.add(add.Loan_Contact__c);
					system.debug('Setting Financial_Eligibility_Valid__c to false');
					break;
				}
			}
			/*if((add.Type_of_address__c != oldMap.get(add.Id).Type_of_address__c) || (add.Residence_Type__c != oldMap.get(add.Id).Residence_Type__c) || (add.Address_Line_1__c != oldMap.get(add.Id).Address_Line_1__c) || (add.Address_Line_2__c != oldMap.get(add.Id).Address_Line_2__c) || (add.Pincode_LP__c != oldMap.get(add.Id).Pincode_LP__c)) {
				setOfCustomerIds.add(add.Loan_Contact__c);    
			}*/
		}
		System.debug('setOfCustomerIds '+setOfCustomerIds);
		List<Loan_Contact__c> lstOfCustomers = new List<Loan_Contact__c>();
		if(!setOfCustomerIds.isEmpty()) {
			lstOfCustomers = [SELECT Id, Loan_Applications__c FROM Loan_Contact__c WHERE Id IN: setOfCustomerIds];
		}
		System.debug('lstOfCustomers '+lstOfCustomers);
		Set<Id> setOfAppIds = new Set<Id>();
		if(!lstOfCustomers.isEmpty()){
			for(Loan_Contact__c lc : lstOfCustomers) {
				setOfAppIds.add(lc.Loan_Applications__c);
			}
		}
		system.debug('setOfAppIds '+setOfAppIds);
		List<Loan_Application__c> lstOfLoanApp = new List<Loan_Application__c>();
		if(!setOfAppIds.isEmpty()) {
			lstOfLoanApp = [SELECT Id, Financial_Eligibility_Valid__c FROM Loan_Application__c WHERE ID IN: setOfAppIds];
		}
		System.debug('lstOfLoanApp '+lstOfLoanApp);
		if(!lstOfLoanApp.isEmpty()) {
			for(Loan_Application__c la: lstOfLoanApp) {
				la.Financial_Eligibility_Valid__c = false;   
			}
		}
		update lstOfLoanApp;
		 //*******************************************************************End of the code- Added by Vaishali for BRE
    }
    
    private static void addressToUpper(List<Address__c> lstAddress){
        for(Address__c add : lstAddress) {
            if(add.Address_Line_1__c != null && add.Address_Line_2__c != null){
                String l1 = add.Address_Line_1__c;
                String l2 = add.Address_Line_2__c;
                add.Address_Line_1__c = l1.toUpperCase();
                add.Address_Line_2__c = l2.toUpperCase();
                }
        }
    }
	
	//Below method added by Abhilekh on 6th February 2020 for TIL-1942
    public static void retriggerHunter(List<Address__c> lstAddress, Map<Id, Address__c> oldMap){
        Map<Id,Hunter_Required_Field__mdt> mapHunterFields= new Map<Id,Hunter_Required_Field__mdt>([Select Id, Field__c, Object__c, Critical_Field__c from Hunter_Required_Field__mdt where Object__c = 'Address__c' and Critical_Field__c = true]);
        
        for(Address__c add: lstAddress){
            for(Hunter_Required_Field__mdt objHunterFields: mapHunterFields.values()){
                if(add.get(objHunterFields.Field__c) != oldMap.get(add.Id).get(objHunterFields.Field__c)){
                    add.Retrigger_Hunter__c = true;
                    break;
                }
            }
        }
    }
	
	//Below method added by Abhishek on 4th August 2020 for TIL-2328
    private static void validateAddress(List<Address__c> AddressList) {
        for(Address__c addressObj : AddressList) {
            if(String.isNotBlank(addressObj.Address_Line_1__c)){
                Integer countDoubleBackslashAdd1 = addressObj.Address_Line_1__c.countMatches('\\\\');
                Integer countSingleBackslashAdd1 = addressObj.Address_Line_1__c.countMatches('\\');
                if(countDoubleBackslashAdd1*2 != countSingleBackslashAdd1){
                    addressObj.addError('Address Line 1 can contain \'Backslash\' only in pairs.');
                }
            }
            if(String.isNotBlank(addressObj.Address_Line_2__c)){
                Integer countDoubleBackslashAdd2 = addressObj.Address_Line_2__c.countMatches('\\\\');
                Integer countSingleBackslashAdd2 = addressObj.Address_Line_2__c.countMatches('\\');
                if(countDoubleBackslashAdd2*2 != countSingleBackslashAdd2){
                    addressObj.addError('Address Line 2 can contain \'Backslash\' only in pairs.');
                }
            }
        }
    }
}