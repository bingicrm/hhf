public class Lookup {
    
    /**
* Returns JSON of list of ResultWrapper to Lex Components
* @objectName - Name of SObject
* @fld_API_Text - API name of field to display to user while searching
* @field_API_PAN - Api to add PAN field to get the PAN details of the Customer //Added by Aman for BRE
* @fld_API_Val - API name of field to be returned by Lookup COmponent
* @lim   - Total number of record to be returned
* @fld_API_Search - API name of field to be searched
* @searchText - text to be searched
* */

    //Modified this method by Aman- to include field_API_PAN
    //Modified by Vaishali to include loanApp and scheme
    @AuraEnabled 
    public static String searchDB(String objectName, String fld_API_Text, String fld_API_Val,string field_API_PAN, 
                                  Integer lim,String fld_API_Search,String searchText, String loanApp,  String scheme ){
                                      
                                      
                                      string query='';
                                      if(objectName =='Account')//  query added by Aman for Account only , including PAN field in Server call 
                                      {
                                          searchText='\'%' + String.escapeSingleQuotes(searchText.replaceAll(' ','')) + '%\'';
                                          query = 'SELECT '+fld_API_Text+' ,'+fld_API_Val+','+field_API_PAN+
                                              ' FROM '+objectName+
                                              ' WHERE '+fld_API_Search+' LIKE '+searchText+ 
                                              ' order by lastmodifieddate desc' +
                                              ' LIMIT '+lim ;    
                                              system.debug('query '+query); 
                                      }
                                      else if(objectName =='Local_Policies__c' && System.Label.BRE_Local_Policy_Filter != null && System.Label.BRE_Local_Policy_Filter == 'True' ) // Previously used Query
                                      {
                                          searchText='\'%' + String.escapeSingleQuotes(searchText.trim()) + '%\'';
                                          String location = [SELECT Id, Location__c FROM User where Id=: UserInfo.getUserId()].Location__c;
                                          location = '\'' + String.escapeSingleQuotes(location) + '\'';
                                          String all = '\'' + String.escapeSingleQuotes('All') + '\'';
                                          String allSchemes = '\'' + String.escapeSingleQuotes('All Schemes') + '\'';
                                          system.debug('location '+location);
                                          system.debug('loanApp '+loanApp);
                                          
                                          String branchId = '';
                                          String schemeId = '';
                                          if(loanApp != null && loanApp != '') {
                                              system.debug('Hi');
                                              Loan_Application__c la = [SELECT Id, Branch_Lookup__c,Scheme__c from Loan_Application__c WHERE Id =: loanApp];
                                              system.debug('loan app'+la);
                                              if(la.Branch_Lookup__c != null) {
                                                branchId = la.Branch_Lookup__c;
                                                system.debug('branchId '+branchId);
                                                branchId =  '\'' + String.escapeSingleQuotes(branchId) + '\'';
                                              }
                                              if(la.Scheme__c != null) {
                                                schemeId= la.Scheme__c;
                                                system.debug('SchemeId '+schemeId);
                                                schemeId = '\'' + String.escapeSingleQuotes(schemeId) + '\'';
                                              }
                                          }
                                          if(scheme != null) {
                                                schemeId= scheme;
                                                system.debug('SchemeId '+schemeId);
                                                schemeId = '\'' + String.escapeSingleQuotes(schemeId) + '\'';
                                          }
										  query = 'SELECT '+fld_API_Text+' ,'+fld_API_Val+
                                              ' FROM '+objectName+
                                              ' WHERE '+fld_API_Search+' LIKE '+searchText+ 'and ( (Branch__c =' +location + 'and ( Product_Scheme__c=' + allSchemes;
												/*query = 'SELECT '+fld_API_Text+' ,'+fld_API_Val+
                                              ' FROM '+objectName+
                                              ' WHERE '+fld_API_Search+' LIKE '+searchText+ 'and (Branch__c =' +location + ' or Branch__c = '+all + ' or Product_Scheme__c= '+allSchemes;  */
                                              
                                              /*if(branchId != '') {
                                                query +=  ' or Branch_Master__c=' +branchId;    
                                              } */
                                              if(schemeId != '') {
                                                  query += ' or Scheme__c=' +schemeId;
                                              } 
											  
											  query += '))';
											  if(branchId != '') {
                                                query +=  ' or Branch_Master__c=' +branchId;    
                                              }											  
											  query +=' or Branch__c =' + all +' or (Branch__c =' + all +' and Product_Scheme__c= ' + allSchemes;
                                              query += ' )) LIMIT '+lim; 
                                              system.debug('query '+query); 
                                      }
									  
                                      
                                      else// Previously used Query
                                      {
                                          searchText='\'%' + String.escapeSingleQuotes(searchText.trim()) + '%\'';
                                          query = 'SELECT '+fld_API_Text+' ,'+fld_API_Val+
                                              ' FROM '+objectName+
                                              ' WHERE '+fld_API_Search+' LIKE '+searchText+ 
                                              ' LIMIT '+lim; 
                                              system.debug('query '+query); 
                                      }
                                      
                                      List<sObject> sobjList = Database.query(query);
                                      List<ResultWrapper> lstRet = new List<ResultWrapper>();
                                      
                                      for(SObject s : sobjList){
                                          ResultWrapper obj = new ResultWrapper();
                                          obj.objName = objectName;
                                          //ternary operator, to add the PAN field to the Wrapper--- added by Aman
                                          obj.PAN= objectName=='Account'?String.valueOf(s.get(field_API_PAN)):null;
                                          // End of the patch- Added by aman
                                          obj.text = String.valueOf(s.get(fld_API_Text)) ;
                                          obj.val = String.valueOf(s.get(fld_API_Val))  ;
                                          lstRet.add(obj);
                                      } 
                                      return JSON.serialize(lstRet) ;
                                  }
    
    public class ResultWrapper{
        public String objName {get;set;}
        public String text{get;set;}
        public String val{get;set;}
        public String PAN{get;set;} // Added by Aman for BRE
    }
    
    //**********************************************************Added by Aman for BRE 
    
    //Added by Aman to send the Recordtype of the acc  in the lookup lightning cmp
    @auraEnabled
    Public static Map<string,String> getRecordType()
    {
        Map<String, Schema.RecordTypeInfo> recordTypes = Schema.SObjectType.Account.getRecordTypeInfosByName();
        Map<string,String> recordTypeMAp = new map<string,string>();
        recordTypeMAp.put(recordTypes.get('Individual').getName(),recordTypes.get('Individual').getRecordTypeId());
        recordTypeMAp.put(recordTypes.get('Corporate').getName(),recordTypes.get('Corporate').getRecordTypeId());
        return recordTypeMAp;
    }
    
    //added by Aman to send the duplicate Record of acc  in the lookup lightning cmp
    @auraEnabled 
    public static List<account> getDupRecord(List<Id> AccID)
    {
        List<account> AccData =  [select Id,Name,PAN__c ,Date_of_Birth__c,PersonMobilePhone,Phone from account where Id in:AccID];
        return AccData;
    }
    @auraEnabled 
    public static account insertDupRecord(Account AccID)
    {
        Account instOfAcc = AccId;
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.allowSave = true;
        dml.DuplicateRuleHeader.runAsCurrentUser = true; 
        Database.SaveResult sr = Database.insert(instOfAcc, dml); 
        if (sr.isSuccess()) {   
            System.debug('Duplicate account has been inserted in Salesforce!'); 
        }
        else
        {
            String message='';
            for(Database.Error err : sr.getErrors()) {
                message+=err.getMessage()+' ';
            }
            system.debug('message '+message);
            throw new AuraHandledException(message);
        }
        return instOfAcc;
    }
    // End of the patch - Added by aman
}