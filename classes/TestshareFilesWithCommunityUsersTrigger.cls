@isTest
private class TestshareFilesWithCommunityUsersTrigger {

	private static testMethod void test() {
	    
	     id loanAppRecType = Schema.SObjectType.loan_application__c.getRecordTypeInfosByName().get('Tranche').getRecordTypeId();
        
        Account custObj = new Account();
        custObj.Name = 'Test Customer';
        custObj.Phone = '9999999998';
        database.insert(custObj);
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        database.insert(schemeObj);
        
        test.startTest(); 
        Loan_Application__c loanAppObj = new Loan_Application__c();
        loanAppObj.RecordTypeId = loanAppRecType;
        loanAppObj.customer__c = custObj.id;
        loanAppObj.Loan_Purpose__c = '11';
        loanAppObj.scheme__c = schemeObj.id;
        loanAppObj.Transaction_type__c = 'PB';
        loanAppObj.Requested_Amount__c = 250000;
        
        loanAppObj.Approved_Loan_Amount__c = 200000;
        loanAppObj.StageName__c = 'Customer Onboarding';
        loanAppObj.sub_stage__c = 'Application Initiation';
        insert loanAppObj;

    Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=loanAppObj.Id,
                                                                      Status__c=Constants.DOC_STATUS_UPLOADED);
        insert objDocCheck;  
          string before = 'Testing base 64 encode';            
         Blob beforeblob = Blob.valueOf(before);
        
        ContentVersion cv = new ContentVersion();
             cv.title = 'test content trigger';      
             cv.PathOnClient ='test';           
             cv.VersionData =beforeblob;          
             insert cv;         
                                                    
             ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=objDocCheck.id;
        contentlink.ShareType= 'C';
        contentlink.ContentDocumentId=testContent.ContentDocumentId;
        contentlink.Visibility = 'AllUsers'; 
        insert contentlink;

	}

}