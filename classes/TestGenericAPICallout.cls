@isTest
private class TestGenericAPICallout {
    
        static testmethod void test1(){
         LMS_Date_Sync__c objDate = new LMS_Date_Sync__c(Current_Date__c = system.today());
   insert objDate;
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ATBQD1854N',Name='Account5789345',Date_of_Incorporation__c=System.today(),
                                  Phone='7835672213',RecordTypeId=RecordTypeIdAccount);
        
        
        Account acc1 = new Account(PAN__c='QWERT1567J',Name='Account1564',Date_of_Incorporation__c=System.Today(),
                                  Phone='9345678910',RecordTypeId=RecordTypeIdAccount);
        
        
         Account acc2 = new Account(PAN__c='QWERT1568J',Name='Account1594',Date_of_Incorporation__c=System.Today(),
                                  Phone='9345678980',RecordTypeId=RecordTypeIdAccount);
         
        List<Account> lstAcc = new list<account>();
            lstacc.add(acc);
            lstacc.add(acc1);
            lstacc.add(acc2);
            insert lstacc;
            
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Id RecordTypeIdLoanApplication = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('COPS: Credit Operations Entry').getRecordTypeId();
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',StageName__c='Customer Onboarding',
                                                                        RecordTypeId=RecordTypeIdLoanApplication,
                                                                         Repayment_Generated_for_Documents__c = true,
                                                                        Business_Date_Created__c=System.today(),Approved_Loan_Amount__c=100000);
        insert objLoanApplication;
        
        Loan_Application__c objLoanApplication1 = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',StageName__c='Customer Onboarding',
                                                                        RecordTypeId=RecordTypeIdLoanApplication,
                                                                          Repayment_Generated_for_Documents__c = true,
                                                                        Sanctioned_Disbursement_Type__c='Multiple Tranche',
                                                                         Business_Date_Created__c=System.today(),Approved_Loan_Amount__c=200000);
        insert objLoanApplication1;
        
        Id RecordTypeIdIMD = Schema.SObjectType.IMD__c.getRecordTypeInfosByName().get('IMD').getRecordTypeId();
        
        /*IMD__c objIMD = new IMD__c(Loan_Applications__c=objLoanApplication.Id,Cheque_Number__c='123456',
                                   Cheque_Date__c=System.Today(),Amount__c=1000,Receipt_Date__c=System.Today(),
                                  Payment_Mode__c='Q');
        insert objIMD;*/
        
        Loan_Repayment__c objLoanRepay = new Loan_Repayment__c(Loan_Application__c=objLoanApplication.Id,
                                                               Installment_Mode__c='Arrear',Installment_Plan__c='Equated Instl');
        insert objLoanRepay;
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83');
        insert objPincode;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Co-Applicant').getRecordTypeId();
        Contact con = new Contact(LAstNAme='test677', Accountid=acc1.Id);
        insert con;
             Contact con1 = new Contact(LAstNAme='test6177', Accountid=acc2.Id);
        insert con1;
            Test.startTest();
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc1.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='2',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='19',
                                                             Customer_segment__c='11',Applicant_Status__c='1',
                                                             Income_Program_Type__c='Liquid Income Program',
                                                             Contact__c=con.id,
                                                             Income_Considered__c=true);
        
            
        Loan_Contact__c objLoanContact2 = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc2.Id,
                                                             Applicant_Type__c='Guarantor',Borrower__c='2',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='OTH',
                                                              If_Others_please_specify__c = 'hkjkj',
                                                             Pan_Verification_status__c=true,Constitution__c='19',
                                                             Customer_segment__c='11',Applicant_Status__c='1',
                                                             Income_Program_Type__c='Liquid Income Program',
                                                             Contact__c=con1.id,
                                                             Income_Considered__c=true);
          
            list<loan_contact__C> lstloanCon = new list<loan_contact__c>();
            lstloancon.add(objloancontact);
            lstloancon.add(objloancontact2);
            
            insert lstloancon;
            
        Address__c objAddress = new Address__c(Loan_Contact__c=objLoanContact.Id);
            
        insert objAddress;
            Address__c objAddress2 = new Address__c(Loan_Contact__c=objLoanContact2.Id);
            
        insert objAddress2;
        /*Property__c objProperty = new Property__c(Loan_Application__c=objLoanApplication.Id,Pincode_LP__c=objPincode.Id,
                                                 First_Property_Owner__c=objLoanContact.Id);
        insert objProperty;*/ 
        
        /*Reference__c objRefer = new Reference__c(Loan_Application__c=objLoanApplication.Id,Mobile_No__c='9234567890',
                                                Pincode__c=objPincode.Id);
        Insert objRefer;*/
        
        Customer_Obligations__c objCustOblg = new Customer_Obligations__c(Customer_Detail__c=objLoanContact.Id, EMI_Amount__c = 1);
        insert objCustOblg;
        
        
        System.debug('objAddress==>'+objAddress);
        
          
        
        
        
        GenericAPICallout objGenericAPICallout = new GenericAPICallout();
        GenericAPICallout.makeCallout(objLoanApplication.Id);
        
        /*objLoanRepay = new Loan_Repayment__c(Id=objLoanRepay.Id,Frequency__c='Bi-monthly',Installment_Plan__c='Graded Instl',
                                            EMI_Amount__c=10);
        update objLoanRepay;
        objHomeLoanOnboardingCallout.HLOnboardingCallout(objLoanApplication.Id);
        
        objLoanRepay = new Loan_Repayment__c(Id=objLoanRepay.Id,Frequency__c='Quarterly',Installment_Mode__c='Advance');
        update objLoanRepay;
        objHomeLoanOnboardingCallout.HLOnboardingCallout(objLoanApplication.Id);
        
        objLoanRepay = new Loan_Repayment__c(Id=objLoanRepay.Id,Frequency__c='Half-yearly',Installment_Plan__c='Balloon');
        update objLoanRepay;
        objHomeLoanOnboardingCallout.HLOnboardingCallout(objLoanApplication.Id);
        
        objLoanRepay = new Loan_Repayment__c(Id=objLoanRepay.Id,Frequency__c='Annually',Installment_Plan__c='Bullet');
        update objLoanRepay;
        objScheme = new Scheme__c(Id=objScheme.Id,Product_Code__c='CF');
        Update objScheme;
        objHomeLoanOnboardingCallout.HLOnboardingCallout(objLoanApplication.Id);
        
        objHomeLoanOnboardingCallout.HLOnboardingCallout(objLoanApplication1.Id);
        
        objScheme = new Scheme__c(Id=objScheme.Id,Product_Code__c='HLAP');
        Update objScheme;
        objHomeLoanOnboardingCallout.HLOnboardingCallout(objLoanApplication.Id);
        
        LoanOnboardingResponseBody lrb = new LoanOnboardingResponseBody();
        lrb.requestId = '9702';
        lrb.status = 'SUCCESS';
        lrb.tempApplId = '123525';
        lrb.errorMsg = 'Sorry , We could not process your request';
        
        TestMockRequest req1=new TestMockRequest(200,'Complete',JSON.serialize(lrb),null);
        Test.setMock(HttpCalloutMock.class,req1);*/
                
        Test.stopTest();
    }

}