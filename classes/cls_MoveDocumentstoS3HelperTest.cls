@isTest
public class cls_MoveDocumentstoS3HelperTest {
    
    public static testMethod void test1(){

        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
        insert con1;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;        

        Loan_Application__c la = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert la;
        List<S3_Folders_Configuration__mdt> folderConfigs = [SELECT Id, Delete_files__c, Folder_Name__c FROM S3_Folders_Configuration__mdt];
        Loan_Application__c objla = [Select Id, Name from Loan_Application__c where Id =: la.Id];
        String targetFolder = 'Applicant Documents for'+ ' '+objla.Name;
        NEILON__Folder__c bucket1 = s3LinkTestUtils.createFoldersForBucket(targetFolder);
        String targetFolder1 = 'Application Documents for'+ ' '+objla.Name;
        NEILON__Folder__c bucket2 = s3LinkTestUtils.createFoldersForBucket(targetFolder1);
        system.debug('targetFolder1::'+targetFolder1);
        String targetFolder2 = 'Co-applicant and Guarantor Documents for'+ ' '+objla.Name;
        NEILON__Folder__c bucket3 = s3LinkTestUtils.createFoldersForBucket(targetFolder2);
        String targetFolder3 = 'Notes & Attachments for'+ ' '+objla.Name;
        NEILON__Folder__c bucket4 = s3LinkTestUtils.createFoldersForBucket(targetFolder3);

        Test.startTest();
        Loan_Contact__c lc = [SELECT Id, Name,Applicant_Type__c, Applicant_Status__c, Constitution__c, Customer_segment__c, Loan_Applications__c FROM Loan_Contact__c WHERE Loan_Applications__c =: la.Id];
        System.debug('lc-CV-'+lc);
        lc.Borrower__c='2';
        lc.Contact__c = con1.Id;
        lc.Constitution__c='18';
        lc.Applicant_Status__c='1';
        lc.Customer_segment__c='9';
        lc.Mobile__c='9876543210';
        lc.Email__c='testemail@gmail.com';
        update lc;
        
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=la.Id,
                                                                     Loan_Contact__c=lc.Id);
        insert objDocCheck;
        System.debug('objDocCheck-cv-'+objDocCheck);    
        
        Map<Id, Id> contentVersionIdByDocumentId = new Map<Id, Id>();
        
        // Get all the documents
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
         
        //Get Content Documents
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
        
        //Create ContentDocumentLink 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = objDocCheck.Id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        Insert cdl;
        
         //Create ContentDocumentLink 
        ContentDocumentLink cdl1 = New ContentDocumentLink();
        cdl1.LinkedEntityId = la.Id;//objDocCheck.Id;
        cdl1.ContentDocumentId = conDocId;
        cdl1.shareType = 'V';
        Insert cdl1;
        
        Attachment att=new Attachment();
        att.ParentId = la.Id ;
        att.Name='test';
        att.Body=Blob.valueOf('test');
        insert att;
        
        for(ContentVersion contentVersion : [Select Id, ContentDocumentId From ContentVersion]){
            contentVersionIdByDocumentId.put(contentVersion.ContentDocumentId, contentVersion.Id);
        }
        List<NEILON__File__c> lstfile = new List<NEILON__File__c>();

        NEILON__File__c file = new NEILON__File__c();
        file.Name = objDocCheck.Id + '.txt';
        //file.NEILON__Folder__c = folderInfo.id;
        file.NEILON__Export_Attachment_Id__c = contentVersionIdByDocumentId.containsKey(objDocCheck.Id) ? contentVersionIdByDocumentId.get(objDocCheck.Id) : objDocCheck.Id;
        file.NEILON__Size__c = 0;
        file.NEILON__Bucket_Name__c = bucket1.NEILON__Bucket_Name__c;
        file.NEILON__Bucket_Region__c = bucket1.NEILON__Bucket_Region__c;
        file.Loan_Application__c = la.Id;
        lstfile.add(file);
        NEILON__File__c file1 = new NEILON__File__c();
        file1.Name = objDocCheck.Id + '.txt';
        //file.NEILON__Folder__c = folderInfo.id;
        file1.NEILON__Export_Attachment_Id__c = contentVersionIdByDocumentId.containsKey(objDocCheck.Id) ? contentVersionIdByDocumentId.get(objDocCheck.Id) : objDocCheck.Id;
        file1.NEILON__Size__c = 0;
        file1.NEILON__Bucket_Name__c = bucket4.NEILON__Bucket_Name__c;
        file1.NEILON__Bucket_Region__c = bucket4.NEILON__Bucket_Region__c;
        file1.Loan_Application__c = la.Id;
        lstfile.add(file1);
        NEILON__File__c file2 = new NEILON__File__c();
        file2.Name = objDocCheck.Id + '.txt';
        //file.NEILON__Folder__c = folderInfo.id;
        file2.NEILON__Export_Attachment_Id__c = contentVersionIdByDocumentId.containsKey(objDocCheck.Id) ? contentVersionIdByDocumentId.get(objDocCheck.Id) : objDocCheck.Id;
        file2.NEILON__Size__c = 0;
        file2.NEILON__Bucket_Name__c = bucket2.NEILON__Bucket_Name__c;
        file2.NEILON__Bucket_Region__c = bucket2.NEILON__Bucket_Region__c;
        file2.Loan_Application__c = la.Id;
        lstfile.add(file2);
        NEILON__File__c file3 = new NEILON__File__c();
        file3.Name = objDocCheck.Id + '.txt';
        //file.NEILON__Folder__c = folderInfo.id;
        file3.NEILON__Export_Attachment_Id__c = contentVersionIdByDocumentId.containsKey(objDocCheck.Id) ? contentVersionIdByDocumentId.get(objDocCheck.Id) : objDocCheck.Id;
        file3.NEILON__Size__c = 0;
        file3.NEILON__Bucket_Name__c = bucket3.NEILON__Bucket_Name__c;
        file3.NEILON__Bucket_Region__c = bucket3.NEILON__Bucket_Region__c;
        file3.Loan_Application__c = la.Id;
        lstfile.add(file3);
        insert lstfile;
        cls_MoveDocumentstoS3Helper.createS3FilesApplicant(la.Id);
        cls_MoveDocumentstoS3Helper.createS3FilesLoanKit(la.Id);
        //cls_MoveDocumentstoS3Helper.createS3FilesLoanKit2(la.Id);
        Test.stopTest();
        
        
    }
    
    public static testMethod void test2(){
        NEILON__Folder__c bucket = s3LinkTestUtils.createFoldersForBucket('testbucket');
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
        insert con1;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;        

        Loan_Application__c la = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert la;
        Loan_Application__c objla = [Select Id, Name from Loan_Application__c where Id =: la.Id];
        String targetFolder = 'Applicant Documents for'+ ' '+objla.Name;
        NEILON__Folder__c bucket1 = s3LinkTestUtils.createFoldersForBucket(targetFolder);
        String targetFolder1 = 'Application Documents for'+ ' '+objla.Name;
        NEILON__Folder__c bucket2 = s3LinkTestUtils.createFoldersForBucket(targetFolder1);
        system.debug('targetFolder1::'+targetFolder1);
        String targetFolder2 = 'Co-applicant and Guarantor Documents for'+ ' '+objla.Name;
        NEILON__Folder__c bucket3 = s3LinkTestUtils.createFoldersForBucket(targetFolder2);
        String targetFolder3 = 'Notes & Attachments for'+ ' '+objla.Name;
        NEILON__Folder__c bucket4 = s3LinkTestUtils.createFoldersForBucket(targetFolder3);
        Test.startTest();
        Loan_Contact__c lc = [SELECT Id, Name,Applicant_Type__c, Applicant_Status__c, Constitution__c, Customer_segment__c, Loan_Applications__c FROM Loan_Contact__c WHERE Loan_Applications__c =: la.Id];
        System.debug('lc-CV-'+lc);
        lc.Borrower__c='2';
        lc.Contact__c = con1.Id;
        lc.Constitution__c='18';
        lc.Applicant_Status__c='1';
        lc.Customer_segment__c='9';
        lc.Mobile__c='9876543210';
        lc.Email__c='testemail@gmail.com';
        update lc;
        
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=la.Id/*,
                                                                     Loan_Contact__c=lc.Id*/);
        insert objDocCheck;
        System.debug('objDocCheck-cv-'+objDocCheck);    
        
        Document_Migration_Log__c objDoc = new Document_Migration_Log__c ();
        objDoc.Description__c='Test';
        objDoc.Loan_Application__c=la.Id;
        objDoc.Document_Checklist__c = objDocCheck.Id;
        insert objDoc;
        
        //Create Document
        ContentVersion cv1 = new ContentVersion();
        cv1.Title = 'Test Document';
        cv1.PathOnClient = 'TestDocument.pdf';
        cv1.VersionData = Blob.valueOf('Test Content');
        cv1.IsMajorVersion = true;
        Insert cv1;
        
        cv1=[SELECT Id, Title, VersionData, CreatedDate, ContentDocumentId, FileExtension,IsLatest FROM ContentVersion];
       	System.debug('cv--SOQL--'+cv1);     
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv1.Id].ContentDocumentId;
      	System.debug('cv--conDocId--'+conDocId);  
        
        //Create ContentDocumentLink 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = objDocCheck.Id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        Insert cdl;
        
        //Create ContentDocumentLink 
        ContentDocumentLink cdl1 = New ContentDocumentLink();
        cdl1.LinkedEntityId = la.Id;
        cdl1.ContentDocumentId = conDocId;
        cdl1.shareType = 'V';
        Insert cdl1;
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        cv.ContentDocumentId = cdl.ContentDocumentId;
        Insert cv;
            
       	System.debug('cv--ContentVersion--'+cv);     
        
         Attachment att=new Attachment();
        att.ParentId = la.Id ;
        att.Name='test';
        att.Body=Blob.valueOf('test');
        insert att;
        
        
        cls_MoveDocumentstoS3Helper.createS3FilesApplication(la.Id);
       /* cls_MoveDocumentstoS3Files.createS3FilesCoApplicantGuarantor(la);
        cls_MoveDocumentstoS3Files.createS3FilesLoanKit(la);
        cls_MoveDocumentstoS3Files.createS3FilesLoanKit2(la);*/
        Test.stopTest();
        
        
    }
    
    public static testMethod void test3(){
        NEILON__Folder__c bucket = s3LinkTestUtils.createFoldersForBucket('testbucket');
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
        insert con1;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;        

        Loan_Application__c la = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert la;
        Loan_Application__c objla = [Select Id, Name from Loan_Application__c where Id =: la.Id];
        String targetFolder = 'Applicant Documents for'+ ' '+objla.Name;
        NEILON__Folder__c bucket1 = s3LinkTestUtils.createFoldersForBucket(targetFolder);
        String targetFolder1 = 'Application Documents for'+ ' '+objla.Name;
        NEILON__Folder__c bucket2 = s3LinkTestUtils.createFoldersForBucket(targetFolder1);
        system.debug('targetFolder1::'+targetFolder1);
        String targetFolder2 = 'Co-applicant and Guarantor Documents for'+ ' '+objla.Name;
        NEILON__Folder__c bucket3 = s3LinkTestUtils.createFoldersForBucket(targetFolder2);
        String targetFolder3 = 'Notes & Attachments for'+ ' '+objla.Name;
        NEILON__Folder__c bucket4 = s3LinkTestUtils.createFoldersForBucket(targetFolder3);
        Loan_Contact__c lc = [SELECT Id, Name,Applicant_Type__c, Applicant_Status__c, Constitution__c, Customer_segment__c, Loan_Applications__c FROM Loan_Contact__c WHERE Loan_Applications__c =: la.Id];
        System.debug('lc-CV-'+lc);
        lc.Borrower__c='2';
        lc.Contact__c = con1.Id;
        lc.Constitution__c='18';
        lc.Applicant_Status__c='1';
        lc.Customer_segment__c='9';
        lc.Mobile__c='9876543210';
        lc.Email__c='testemail@gmail.com';
        update lc;
        
       /* Loan_Contact__c loanContact2 = new Loan_Contact__c(Customer__c = acc.Id);
        loanContact2.Loan_Applications__c = la.Id;
        loanContact2.Applicant_Type__c = Constants.COAPP;
        insert loanContact2;*/
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Co-Applicant').getRecordTypeId();
        Test.startTest();
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=la.Id,Customer__c=acc.Id,
                                                             Applicant_Type__c='Guarantor',Borrower__c='1',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='Brother',
                                                             Pan_Verification_status__c=true,Constitution__c='20',
                                                             Customer_segment__c='1',Applicant_Status__c='1',
                                                             Category__c='1',
                                                             Father_s_Husband_s_Name__c='ABC',
                                                             GSTIN_Number__c='04AATPB2258C1Z3',
                               								 Mobile__c='9978786756',Email__c='avd@g.com');
        insert objLoanContact;
        
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=la.Id,
                                                                     Loan_Contact__c=objLoanContact.Id);
        insert objDocCheck;
        System.debug('objDocCheck-cv-'+objDocCheck);    
        
        Document_Migration_Log__c objDoc = new Document_Migration_Log__c ();
        objDoc.Description__c='Test';
        objDoc.Loan_Application__c=la.Id;
        objDoc.Document_Checklist__c = objDocCheck.Id;
        //objDoc.Document_Id__c = '43434';
        insert objDoc;
        
        //Create Document
        ContentVersion cv1 = new ContentVersion();
        cv1.Title = 'Test Document';
        cv1.PathOnClient = 'TestDocument.pdf';
        cv1.VersionData = Blob.valueOf('Test Content');
        cv1.IsMajorVersion = true;
        //cv1.ContentDocumentId = objDoc.Id;
        Insert cv1;
        
        cv1=[SELECT Id, Title, VersionData, CreatedDate, ContentDocumentId, FileExtension,IsLatest FROM ContentVersion];
       	System.debug('cv--SOQL--'+cv1);     
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv1.Id].ContentDocumentId;
      	System.debug('cv--conDocId--'+conDocId);  
        
        //Create ContentDocumentLink 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = objDocCheck.Id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        Insert cdl;
        
         //Create ContentDocumentLink 
        ContentDocumentLink cdl1 = New ContentDocumentLink();
        cdl1.LinkedEntityId = la.Id;//objDocCheck.Id;
        cdl1.ContentDocumentId = conDocId;
        cdl1.shareType = 'V';
        Insert cdl1;
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        cv.ContentDocumentId = cdl.ContentDocumentId;// objDocCheck.Id;//.Document_Id__c;
        Insert cv;
            
       	System.debug('cv--ContentVersion--'+cv);     
         
        //Get Content Documents
       /* cv=[SELECT Id, Title, VersionData, CreatedDate, ContentDocumentId, FileExtension,IsLatest FROM ContentVersion];
       	System.debug('cv--SOQL--'+cv);     
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
      	 System.debug('cv--conDocId--'+conDocId);     
        
        */
         Attachment att=new Attachment();
        att.ParentId = la.Id ;
        att.Name='test';
        att.Body=Blob.valueOf('test');
        //att.Id =: objDoc.Document_Id__c;
        insert att;
        
        
        //cls_MoveDocumentstoS3Files.createS3FilesApplicant(la);
        //cls_MoveDocumentstoS3Files.createS3FilesApplication(la);
        cls_MoveDocumentstoS3Helper.createS3FilesCoApplicantGuarantor(la.Id);
      //  cls_MoveDocumentstoS3Files.createS3FilesLoanKit(la);
        //cls_MoveDocumentstoS3Files.createS3FilesLoanKit2(la);
        Test.stopTest();
        
        
    }

}