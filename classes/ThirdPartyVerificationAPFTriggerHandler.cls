public class ThirdPartyVerificationAPFTriggerHandler {
    public static void onBeforeInsert(List<Third_Party_Verification_APF__c> lstTriggerNew) {
        
    }
    
    public static void onAfterInsert(List<Third_Party_Verification_APF__c> lstTriggerNew) {
        Id LegalRecordTypeId = Schema.SObjectType.Third_Party_Verification_APF__c.getRecordTypeInfosByName().get(Constants.strRecordTypeTPVLegal).getRecordTypeId();
        Id TechnicalRecordTypeId = Schema.SObjectType.Third_Party_Verification_APF__c.getRecordTypeInfosByName().get(Constants.strRecordTypeTPVTechnical).getRecordTypeId();
        Id FCURecordTypeId = Schema.SObjectType.Third_Party_Verification_APF__c.getRecordTypeInfosByName().get(Constants.strRecordTypeTPVFCU).getRecordTypeId();
        Set<Id> setProjectIds = new Set<Id>();
        
        //Map<Id,Project__c> mapProjectIdtoProject = new Map<Id,Project__c>([Select Id, Name,(Select Id, Name, Document_Master__c, Document_Master__r.Name, Document_Type__c, Document_Type__r.Name, Status__c From Project_Document_Checklists__r) From Project__c WHERE ID IN: ]);
        
        List<Third_Party_Document_Checklist_APF__c> lstTPVDocChkList = new List<Third_Party_Document_Checklist_APF__c>();
        
        List<Third_Party_Verification_APF__c> lstLegalTPVs = new List<Third_Party_Verification_APF__c>();
        List<Third_Party_Verification_APF__c> lstTechnicalTPVs = new List<Third_Party_Verification_APF__c>();
        List<Third_Party_Verification_APF__c> lstFCUTPVs = new List<Third_Party_Verification_APF__c>();
        
        for(Third_Party_Verification_APF__c objTPV : lstTriggerNew) {
            if(objTPV.RecordTypeId == LegalRecordTypeId) {
                lstLegalTPVs.add(objTPV);
            }
            
            if(objTPV.RecordTypeId == TechnicalRecordTypeId) {
                lstTechnicalTPVs.add(objTPV);
            }
            
            if(objTPV.RecordTypeId == FCURecordTypeId) {
                lstFCUTPVs.add(objTPV);
            }
            setProjectIds.add(objTPV.Project__c);
        }
        
        if(!setProjectIds.isEmpty()) {
            Map<Id,Project__c> mapProjectIdtoProject = new Map<Id,Project__c>([Select Id, Name,(Select Id, Name, Document_Master__c, Document_Master__r.Name, Document_Type__c, Document_Type__r.Name, Status__c From Project_Document_Checklists__r Where Status__c = 'Uploaded') From Project__c WHERE ID IN: setProjectIds]);
        
            if(!lstLegalTPVs.isEmpty()) {
                for(Third_Party_Verification_APF__c objTPV : lstLegalTPVs) {
                    if(!mapProjectIdtoProject.isEmpty() && mapProjectIdtoProject.containsKey(objTPV.Project__c)) {
                        if(!mapProjectIdtoProject.get(objTPV.Project__c).Project_Document_Checklists__r.isEmpty()) {
                            for(Project_Document_Checklist__c objPDC : mapProjectIdtoProject.get(objTPV.Project__c).Project_Document_Checklists__r) {
                                if(objPDC.Document_Type__r.Name == Constants.strAPFProjDocTypeLegal) {
                                    Third_Party_Document_Checklist_APF__c objTPVDocChkList = new Third_Party_Document_Checklist_APF__c();
                                    objTPVDocChkList.Project_Document_Checklist__c = objPDC.Id;
                                    objTPVDocChkList.Project__c = objTPV.Project__c;
                                    objTPVDocChkList.Third_Party_Verification_APF__c = objTPV.Id;
                                    lstTPVDocChkList.add(objTPVDocChkList);
                                }
                            }
                        }
                    }
                }
            }
            
            if(!lstTechnicalTPVs.isEmpty()) {
                for(Third_Party_Verification_APF__c objTPV : lstTechnicalTPVs) {
                    if(!mapProjectIdtoProject.isEmpty() && mapProjectIdtoProject.containsKey(objTPV.Project__c)) {
                        if(!mapProjectIdtoProject.get(objTPV.Project__c).Project_Document_Checklists__r.isEmpty()) {
                            for(Project_Document_Checklist__c objPDC : mapProjectIdtoProject.get(objTPV.Project__c).Project_Document_Checklists__r) {
                                if(objPDC.Document_Type__r.Name == Constants.strAPFProjDocTypeTechnical) {
                                    Third_Party_Document_Checklist_APF__c objTPVDocChkList = new Third_Party_Document_Checklist_APF__c();
                                    objTPVDocChkList.Project_Document_Checklist__c = objPDC.Id;
                                    objTPVDocChkList.Project__c = objTPV.Project__c;
                                    objTPVDocChkList.Third_Party_Verification_APF__c = objTPV.Id;
                                    lstTPVDocChkList.add(objTPVDocChkList);
                                }
                            }
                        }
                    }
                }
            }
            
            if(!lstFCUTPVs.isEmpty()) {
                for(Third_Party_Verification_APF__c objTPV : lstFCUTPVs) {
                    if(!mapProjectIdtoProject.isEmpty() && mapProjectIdtoProject.containsKey(objTPV.Project__c)) {
                        if(!mapProjectIdtoProject.get(objTPV.Project__c).Project_Document_Checklists__r.isEmpty()) {
                            for(Project_Document_Checklist__c objPDC : mapProjectIdtoProject.get(objTPV.Project__c).Project_Document_Checklists__r) {
                                Third_Party_Document_Checklist_APF__c objTPVDocChkList = new Third_Party_Document_Checklist_APF__c();
                                objTPVDocChkList.Project_Document_Checklist__c = objPDC.Id;
                                objTPVDocChkList.Project__c = objTPV.Project__c;
                                objTPVDocChkList.Third_Party_Verification_APF__c = objTPV.Id;
                                lstTPVDocChkList.add(objTPVDocChkList);
                            }
                        }
                    }
                }
            }
            
            if(!lstTPVDocChkList.isEmpty()) {
                insert lstTPVDocChkList;
            }
        }
        
    }
    
    public static void onBeforeUpdate(Map<Id,Third_Party_Verification_APF__c> mapTriggerNew, Map<Id,Third_Party_Verification_APF__c> mapTriggerOld) {
        
    }
    
    public static void onAfterUpdate(Map<Id,Third_Party_Verification_APF__c> mapTriggerNew, Map<Id,Third_Party_Verification_APF__c> mapTriggerOld) {
        
    }
}