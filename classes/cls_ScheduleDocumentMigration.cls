global class cls_ScheduleDocumentMigration implements Schedulable {
   global void execute(SchedulableContext sc) {
      cls_ProcessS3FolderCreationBatch obj1 = new cls_ProcessS3FolderCreationBatch(); 
      //cls_ProcessApplicantDocumentsBatch obj1 = new cls_ProcessApplicantDocumentsBatch();
      database.executebatch(obj1,1);
   }
}