public class PerfiosDocumentScanningCallout {
    
    public static String boundary = '----------------------------741e90d99eff'; 
    public static Blob bodyBlob;
    public static String ab ='';
  //  @future(callout=true)
  // Added an argument password by vaishali- to support sending password protected efiles
    public static String makeApiCallout(String imageId, String transactionId, String bankvalue, String financialYear, String docType, String fileTitle, String pass) {
        //String image = lstEncodedFileCode[0];
        System.debug('Debug Log for transactionId'+transactionId);
        System.debug('imageId-->>'+imageId);
        //image = 'L3NlcnZpY2VzL2RhdGEvdjQ0LjAvc29iamVjdHMvQXR0YWNobWVudC8wMFBOMDAwMDAwOWtkQURNQVkvQm9keQ==';
        
            if(String.isNotBlank(transactionId) && String.isNotBlank(imageId)) {
                Rest_Service__mdt restService = new Rest_Service__mdt();
                // Call rest service metadata
                if(bankvalue == 'Financial') {
                    restService = [ SELECT MasterLabel, QualifiedApiName , Service_EndPoint__c , 
                                                 Client_Password__c, Client_Username__c, Request_Method__c, Time_Out_Period__c FROM   Rest_Service__mdt
                                                 WHERE  MasterLabel = 'PerfiosFinancialUpload'
                                                ]; 

                } else {
                     restService = [ SELECT MasterLabel, QualifiedApiName , Service_EndPoint__c , 
                                                 Client_Password__c, Client_Username__c, Request_Method__c, Time_Out_Period__c FROM   Rest_Service__mdt
                                                 WHERE  MasterLabel = 'PerfiosStmntUploadScan'
                                                ]; 
                }
                
                //Assemble pdf body
                String filename='3456065_1541349186570.pdf';
                //String filename = fileTitle+'.'+docType;
                system.debug('filename' +filename);
                String header = '\n--' + boundary + '\nContent-Disposition: form-data; name="file"; filename="' + fileName + '"\nContent-Type: application/octet-stream\n\n' ;
                system.debug('Header '+header);
                header = EncodingUtil.base64Encode(Blob.valueOf(header+'\n\n'));
                String footer = '--' + boundary + '--'; 
                footer = '\r\n' + footer;
                system.debug('footer '+footer);
                footer = EncodingUtil.base64Encode(Blob.valueOf(footer));
                //header = header+'\r\n\r\n';
                string body ='';
                try{
                    system.debug('In try');
                    //body = image;
                    system.debug('Body '+body);
                    system.debug('tyttytyegh'+body.length());
                    system.debug('uydifysjdh'+(header+body+footer).length());
                    body = header+body+footer;
                    system.debug('JHWDDTYW'+body.length());
                    bodyBlob = EncodingUtil.base64Decode(header+body+footer);
                    system.debug('Hi expection occurred??');  
                    System.debug('body!!!!!');
                } catch (Exception e) {  
                    system.debug('In catch scenario '+e);
                    return 'Error: Documents limits breached, please send documents in parts';
                }
				// Added an argument password by vaishali- to support sending password protected efiles
                // calling response method
                HttpResponse res=(HttpResponse) returnResponse(restService.Client_Username__c,restService.Client_Password__c,restService.Service_EndPoint__c,restService.Request_Method__c, imageId, Integer.valueOf(restService.Time_Out_Period__c), transactionId, bankvalue, financialYear, docType, pass);
                system.debug('res'+res);
                if (res != null) {
                    if(res.getStatusCode() == 200) // checking if status code is 200
                    {
                        if(res.getBody().contains('errorMessage')){
                            Error objresult = (Error)JSON.deserialize(res.getBody(), Error.class); // deserializing the response body into Error class type
                            system.debug('objresult::'+objresult);
                            //return objresult; 
							//Added by Vaishali -to support sending password protected efiles
							if(objresult.Error.errorMessage.contains('E_FILE_WRONG_PASSWORD') || objresult.Error.errorMessage.contains('E_FILE_NO_PASSWORD')) {
                                return 'Wrong Password- '+fileTitle;
                            }
                            if(objresult.Error.errorMessage.contains('E_STATEMENT_WRONG_FORMAT')) {
                                return 'Wrong Format- '+fileTitle;
							}
							// End of the code- Added by Vaishali

                            return 'Error- '+fileTitle;
                            
                        }
                        else{
                            
                            if(bankvalue == 'ScannedPDFBanking') {
                                response objresult = (response)JSON.deserialize(res.getBody(), response.class); // deserializing the response body into accClass class type
                                system.debug('objresult ' +objresult);
                            } else {
                                system.debug('In here');
                                accClass objresult = (accClass)JSON.deserialize(res.getBody(), accClass.class); // deserializing the response body into accClass class type 
                                system.debug('objresult '+objresult);
                                
                            }
                            return 'Success';
                            //return objresult;
                        }
                    } 
                    
                    else if (res.getStatusCode() != 200){
                        if(res.getBody().contains('Service Unavailable')){
                            return 'Error- '+fileTitle;
                            
                        }
                        else{
                            Error objresult = (Error)JSON.deserialize(res.getBody(), Error.class);
                            system.debug('objresult::'+objresult);
                            //return objresult; 
                            return 'Error- '+fileTitle;
                        }
                        
                    } 
                }
                    return 'Error- '+fileTitle;
                //return res;
            }
            return 'Error- '+fileTitle;
            
              
    }
    
	//Added an argument password by Vaishali to support sending password protected efiles
    public static object returnResponse(String clientUserName,String clientPassword,String endPointURL, String method, String imageId , Integer timeOut, String transactionId, String bankvalue, String financialYear, String docType, String pass) {
        system.debug('BANKVALUE2 '+bankValue);
        
        Map<String,String> headerMap = new Map<String,String>();                                                      
        Blob headerValue = Blob.valueOf(clientUserName + ':' + clientPassword);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        // Putting values in headerMap
        headerMap.put('Authorization',authorizationHeader);
		// Added by Vaishali to support sending password protected efiles
		headerMap.put('password',pass);
        //headerMap.put('password','');
		// End of the code - Added by Vaishali to support sending password protected efiles
        //headerMap.put('Content-Type', 'application/octet-stream');
        //headerMap.put('Content-Type','multipart/form-data; boundary='+boundary);
        headerMap.put('vendorId','HEROHOUSING');
        headerMap.put('accept','*/*');
        headerMap.put('accept-encoding','gzip, deflate');
        headerMap.put('Connection','keep-alive');
        headerMap.put('SFID',imageId);
        
        //headerMap.put('vendorId','heroHousing');
        headerMap.put('perfiosTransactionId',transactionId);
        //if(docType == 'TIFF' || docType == 'TIF') {
          //  headerMap.put('uploadingScannedStatements','true');    
        //}
        //System.debug('Request Body'+body);
        System.debug('headerMap@@'+headerMap);
        
        HttpResponse res=new HttpResponse();
        if(bankvalue =='Financial') {
             headerMap.put('financialYear',financialYear);
             //res   = sendRequest('https://services-qa.herofincorp.com/perfios/api/financialStatement/uploadStatement', method, body , headerMap, timeOut );
        } else {
            system.debug('In scanned banking');
            //res=sendRequest('https://services-qa.herofincorp.com/perfios/api/uploadStatement', method, body , headerMap, timeOut );
        }
        res=sendRequest(endPointURL, method, null, headerMap, timeOut );
        System.debug('res@@'+res);  
        return res;     
    }
    
    public static HTTPResponse sendRequest(String endPointURL, String method, String body , Map<String,String> header, Integer timeOut ){
        
        HTTP http = new HTTP();
        HTTPRequest httpReq = new HTTPRequest();
        HTTPResponse httpRes;
        httpReq.setEndPoint(endPointURL);
        httpReq.setMethod(method);
        system.debug('==header=='+header);
        for(String headerKey : header.keySet()){
            httpReq.setHeader(headerKey, header.get(headerKey));
        }
        //httpReq.setBody(body);

        //httpReq.setBodyAsBlob(bodyBlob);
        System.debug('Debug Log for request Body'+body);
        system.debug('bodyBlob '+bodyBlob);
        httpReq.setTimeOut(50000);
        system.debug(body);
        try
        {
            httpRes = http.send(httpReq);
            system.debug('@@@@in utility' + httpRes );
            system.debug('Response Body received' + httpRes.getBody() );
            //String strResponseBody = httpRes != null ? (httpRes.getBody()) : 'Response is null';
            //createIntegrationLogs(header, strResponseBody, endPointURL);
        }
        catch(Exception ex)
        {
            System.debug('Debug Log for Exception'+ex);
        }
        return httpRes;
        
    }
    
    // Parent wrapper class
    public Class ResponseWrapper {
        @AuraEnabled
        public String statementId;
        @AuraEnabled
        public List<accClass> accounts;
    }
    // Child wrapper of ResponseWrapper
    public Class accClass {
        @AuraEnabled
        public Integer accountPattern;
        @AuraEnabled
        public Date transactionStartDate;
        @AuraEnabled
        public Date transactionEndDate;
        public accClass(){
            this.accountPattern = accountPattern;
            this.transactionStartDate = transactionStartDate;
            this.transactionEndDate = transactionEndDate;
        }  
    }
    
    // Parent wrapper class for Error JSON
    public Class Error {
        @AuraEnabled
        public String status;
        @AuraEnabled
        public Error_Cls Error;
        public Error()
        {
            this.status = status;
            Error_Cls err= new Error_Cls();
            this.Error = err;
        }
    }
    
    // Child wrapper class for Error JSON
    public Class Error_Cls {
        @AuraEnabled
        public Integer errorCode;
        @AuraEnabled
        public String errorType;
        @AuraEnabled
        public String errorMessage;
        public Error_Cls()
        {
            this.errorCode = errorCode;
            this.errorType = errorType;
            this.errorMessage = errorMessage;
            
        }
    }
    
    public class response {
        @AuraEnabled
        Public String status;
        Public String statementId;
    }
    
    /*@future
    public static void createIntegrationLogs(Map<String,String> header, String strResponseBody, String endPointURL ) {
        LogUtility.createIntegrationLogs1('Transaction Id= '+header.get('perfiosTransactionId'),strResponseBody, endPointURL);    
    }*/
}