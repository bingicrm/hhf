@isTest
public class TestFCUDocumentChecklistTrigger {
    @testsetUp
    public static void checkUserRoleTest(){
        Profile p1=[SELECT id,name from profile where name=:'Sales Team'];
        UserRole r = [Select Id From UserRole Where Name =: 'BCM - Delhi 1.1' limit 1];
        Profile p2=[SELECT id,name from profile where name=:'Third Party Vendor'];
        
        Branch_Master__c bm = new Branch_Master__c(Name='Lucknow',Office_Code__c='10');
        insert bm;
        
        User u1;
        User u2;
        
        System.runAs(new User(Id=UserInfo.getUserId())){
            u1 = new User(
                ProfileId = p1.Id,LastName = 'last',Email = 'puser000@amamama.com',Username = 'puser000@amamama.com' + System.currentTimeMillis(),CompanyName = 'TEST',
                Title = 'title',Alias = 'alias',TimeZoneSidKey = 'America/Los_Angeles',EmailEncodingKey = 'UTF-8',LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US',
                FCU__c =true,
                UserRoleId = r.Id, Role_in_Sales_Hierarchy__c ='SM'); 
            insert u1;
            
            u2 = new User(
                ProfileId = p2.Id,LastName = 'last',Email = 'puser000@amamama.com',Username = 'puser000@amamama.com' + System.currentTimeMillis(),CompanyName = 'TEST FCU',
                Title = 'title',Alias = 'alias',TimeZoneSidKey = 'America/Los_Angeles',EmailEncodingKey = 'UTF-8',LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US',
                FCU__c =true, Location__c = 'Lucknow'); 
            //insert u2;
        }
        
        User thisUser = [ select Id,Name from User where Id = :UserInfo.getUserId() ];
        system.runAs(thisUser){
            Id RecordTypeIdCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();        
            Account custObj = new Account();
            custObj.Name = 'Test Customer';
            custObj.Phone = '9999999999';
            database.insert(custObj);
            
            scheme__c schemeObj = new scheme__c(Name = 'Scheme Name',Scheme_Group_ID__c = '12345',Scheme_Code__c = '12345');
            database.insert(schemeObj);        
            
            Loan_Application__c loanAppObj = new Loan_Application__c(customer__c = custObj.id,Loan_Application_Number__c = 'LA00001',Loan_Purpose__c = '11',
                                                                     scheme__c = schemeObj.id,Transaction_type__c = 'PB',Requested_Amount__c = 50/*,StageName__c='Tranche',Sub_Stage__c='Tranche Processing'*/);
            insert loanAppObj;
             Cross_Sell_Partner_Name__c CSPN=new Cross_Sell_Partner_Name__c(Name='Test');
            Database.insert(CSPN);
            test.startTest();
            DST_Master__c DSTMaster= new DST_Master__c(Name='Test DST',Inspector_ID__c='1234'); 
            Database.insert(DSTMaster);
            Sourcing_Detail__c SDob= new Sourcing_Detail__c(Loan_Application__c=loanAppObj.id,Source_Code__c='16',
                                                            DST_Name__c=DSTMaster.id,
                                                            //Sales_User_Name__c=u.id,
                                                            Cross_Sell_Partner_Name__c=CSPN.id);
            
            Database.insert(SDob);
            loanAppObj.StageName__c='Operation Control';
            loanAppObj.Sub_Stage__c='Scan: Data Maker';
            update loanAppObj;
            Id rt = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Loan Disbursal').getRecordTypeId();
            loanAppObj.StageName__c='Loan Disbursal';
            //loanAppObj.Sub_Stage__c='Tranche Processing';
            loanAppObj.RecordtypeId = rt; 
            update loanAppObj;
            Id RecordTypeIdFCU = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('FCU').getRecordTypeId();        
            Third_Party_Verification__c tpvObj = new Third_Party_Verification__c();
            tpvObj.Loan_Application__c = loanAppObj.id;
            tpvObj.Status__c = 'New';
            tpvObj.RecordTypeId = RecordTypeIdFCU;
            tpvObj.Owner__c = u1.Id;
            //tpvObj.Owner__c = UserInfo.getUserRoleId();     
            Database.insert(tpvObj);  
            tpvObj.Status__c = 'Initiated';
            tpvObj.Owner__c = u1.Id;
            update tpvObj;

            Test.stopTest();
            Loan_Contact__c loanConObj = new Loan_Contact__c(Loan_Applications__c = loanAppObj.id,Applicant_Type__c = 'Co- Applicant',Applicant_Status__c = 'Resident');
            //  Database.insert(loanConObj);
            
            Document_Master__c docMasObj = new Document_Master__c();
            docMasObj.Name = 'Test';
            docMasObj.Doc_Id__c = '123344';
            docMasObj.Approving_authority__c = 'Business Head';
            Database.insert(docMasObj);
            List<Document_type__c> lstDocType = new List<Document_type__c>();
            Document_type__c docTypeObj = new Document_type__c();
            docTypeObj.Name = 'Test Document Type';
            docTypeObj.Approving_authority__c = 'Business Head';
            lstDocType.add(docTypeObj);
            
            Document_Type__c DType=new Document_Type__c(Name='Property Papers');
            lstDocType.add(DType);
            insert lstDocType;        
            
            DocumentType_Document_Mapping__c mapObj = new DocumentType_Document_Mapping__c();
            mapObj.Document_Master__c = docMasObj.id;
            mapObj.Document_Type__c = docTypeObj.id;
            Database.insert(mapObj);
            
            List<Document_Checklist__c> lstofDocs = new List<Document_Checklist__c>();
            Document_Checklist__c docObj = new Document_Checklist__c();
            docObj.Loan_Applications__c = loanAppObj.id;
            docObj.status__c = 'Pending';
            docObj.document_type__c = docTypeObj.id;
            docObj.Document_Master__c = docMasObj.id;
            //docObj.Loan_Contact__c = loanConObj.id;
            docObj.Express_Queue_Mandatory__c = true;
            docObj.Document_Collection_Mode__c = 'Photocopy';
            docObj.Screened_p__c = 'Yes';
            docObj.Sampled_p__c = 'Yes';
            lstofDocs.add(docObj);
            //Database.insert(lstofDocs);
            
            List<Document_Checklist__c> docList = [Select Id, Name, Document_Master__r.Name, Document_Collection_Mode__c, Screened_p__c, Sampled_p__c 
                                                   from Document_Checklist__c where Id IN :lstofDocs];
            
            Document_Checklist__c docObj2 = new Document_Checklist__c();
            docObj2.Loan_Applications__c = loanAppObj.id;
            docObj2.status__c = 'Pending';
            docObj2.document_type__c = docTypeObj.id;
            docObj2.Document_Master__c = docMasObj.id;
            docObj2.Loan_Contact__c = loanConObj.id;
            docObj2.Express_Queue_Mandatory__c = true;
            docObj2.Document_Collection_Mode__c = 'Photocopy';
            docObj2.Screened_p__c = 'Yes';
            docObj2.Sampled_p__c = 'Yes';
            docObj2.FCU_Done__c = False; 
            lstofDocs.add(docObj2);
            //Database.insert(docObj2);
            insert lstofDocs;
            FCU_Document_Checklist__c fcudata = new FCU_Document_Checklist__c(Loan_Application__c=loanAppObj.id,Document_Checklist__c=docObj2.Id,
                                                                              Third_Party_Verification__c=tpvObj.id,FCU_Decision__c='Refer to Credit',Screened__c = 'No');
            insert fcudata;  
        }
    }
    public static testMethod void FCUDocChecklstAfterInsert(){ 
        
        Loan_Application__c loanAppObj = [Select id from Loan_Application__c LIMIT 1];
        Document_Checklist__c docObj2 = [Select id from Document_Checklist__c LIMIT 1 ];
        Third_Party_Verification__c tpvObj = [Select id from Third_Party_Verification__c LIMIT 1];
        FCU_Document_Checklist__c fcudata = new FCU_Document_Checklist__c(Loan_Application__c=loanAppObj.id,Document_Checklist__c=docObj2.Id,
                                                                          Third_Party_Verification__c=tpvObj.id,FCU_Decision__c=Constants.RTC,Screened__c = 'No');
        //Test.startTest();
        insert fcudata;                         
        //Test.stopTest();
    }
    
    public static testMethod void FCUDocCheckAfterUpdate(){
        FCUDocChecklstAfterInsert();
        List<FCU_Document_Checklist__c> fcudata1 = [Select id from FCU_Document_Checklist__c WHERE FCU_Decision__c =:Constants.RTC]; 
        fcudata1[0].FCU_Decision__c = Constants.CNV;
        fcudata1[0].Screened__c = 'Yes';
        
        //Test.startTest();
        update fcudata1[0]; 
        //Test.stopTest();
    }
}