public class BuilderIntegationTriggerHandler {
    /******************************************************************************************************************************************************************************
     * Created By   :   Shobhit Saxena(Deloitte)
     * 
     * 
     * 
     * 
    ******************************************************************************************************************************************************************************/
    public static void onBeforeInsert(List<Builder_Integrations__c> lstTriggerNew) {
        Date dt = LMSDateUtility.lmsDateValue;
        for(Builder_Integrations__c objBI : lstTriggerNew){
            objBI.Business_Date__c = (Date) dt;
        }
    }
    
    public static void onAfterInsert(List<Builder_Integrations__c> lstTriggerNew) {
        List<Builder_Integrations__c> lstBI = [select id, recordTypeId,GSTIN_Error_Message__c,Mobile__c, Email__c, GSTIN__c,RecordType.Name,Project_Builder__c from Builder_Integrations__c where id in:lstTriggerNew];
        
        List<Builder_Integrations__c> gstRecords = new List<Builder_Integrations__c>();
        Set<id> setRecordTypeIds = new Set<id>();
        Set<id> setProjectBuilder = new Set<id>();
        
        for(Builder_Integrations__c objBI : lstBI){
            setRecordTypeIds.add(objBI.RecordTypeId);
            setProjectBuilder.add(objBI.Project_Builder__c);
            if(objBI.RecordType.Name == 'GSTIN' && objBI.GSTIN__c != null && objBI.GSTIN_Error_Message__c == null) {
                gstRecords.add(objBI);
            }
        }

        if(!setRecordTypeIds.isEmpty()){
            system.debug('===');
            list<Builder_Integrations__c> previousList = new list<Builder_Integrations__c>();
            previousList = [ select id, Latest_Record__c, recordTypeId from Builder_Integrations__c where recordTypeId in :setRecordTypeIds and id not in: lstBI ];
            if(!previousList.isEmpty()) {
                for(Builder_Integrations__c prev: previousList){
    
                  prev.Latest_Record__c = false;
                }
                update previousList;
            }
        }
        
        if(setRecordTypeIds.size() >0){
            system.debug('==herreeeeeee=');

            list<Builder_Integrations__c> previousList = new list<Builder_Integrations__c>();
            previousList = [ select id, Latest_Record__c, Project_Builder__c,recordTypeId from Builder_Integrations__c where
                                    recordTypeId in :setRecordTypeIds and id not in: lstTriggerNew and Project_Builder__c in :setProjectBuilder and RecordType.Name != 'GSTIN'];//Edited by Chitransh for GST Integration//

                    for(Builder_Integrations__c prev: previousList){

                        prev.Latest_Record__c = false;
                    }
                    update previousList;
        }
        
        if(!gstRecords.isEmpty()){
             system.debug('NotEmpty List');
             System.enqueueJob(new GstReturnStatusCalloutAPF(gstRecords, null));
             
         }
         
    }
    
    public static void onBeforeUpdate(Map<Id,Builder_Integrations__c> mapTriggerNew, Map<Id,Builder_Integrations__c> mapTriggerOld) {
        
    }
    
    public static void onAfterUpdate(Map<Id,Builder_Integrations__c> mapTriggerNew, Map<Id,Builder_Integrations__c> mapTriggerOld) {
        System.debug('Inside handler');
        Set<id> setProjBuilderIds = new Set<Id>();
        String strGSTINRecordTypeID = Schema.SObjectType.Builder_Integrations__c.getRecordTypeInfosByName().get('GSTIN').getRecordTypeId();
        for(Builder_Integrations__c objBI : mapTriggerNew.values()) {
            System.debug('>>>>>Status'+objBI.Status__c);
            System.debug('>>>>>objBI.Latest_Record__c'+objBI.Latest_Record__c);
            System.debug('>>>>>objBI.RecordTypeId'+objBI.RecordType.Name+'>>>'+strGSTINRecordTypeID);
            
            if(objBI.Status__c != mapTriggerOld.get(objBI.Id).Status__c && objBI.Status__c == 'Completed' && objBI.Latest_Record__c && String.isNotBlank(objBI.GSTIN__c)) {
                setProjBuilderIds.add(objBI.Project_Builder__c);
            }
        }
        
        if(!setProjBuilderIds.isEmpty()) {
            System.debug('Debug Log for setProjBuilderIds'+setProjBuilderIds);
            List<Project_Document_Checklist__c> lstGSTDocs = [SELECT Id, Name, Document_Master__c, Project_Builder__c, Mandatory__c From Project_Document_Checklist__c Where Project_Builder__c IN: setProjBuilderIds AND (Document_Master__r.Name = 'GST Return for last 1 year' OR Document_Master__r.Name = 'GSTIN - Registration certificate')];
            if(!lstGSTDocs.isEmpty()) {
                System.debug('Debug Log for lstGSTDocs'+lstGSTDocs);
                for(Project_Document_Checklist__c objPDC : lstGSTDocs) {
                    objPDC.Mandatory__c = false;
                }
            }
            update lstGSTDocs;
            
        }
        
    }
}