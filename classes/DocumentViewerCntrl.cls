public class DocumentViewerCntrl 
{
    @AuraEnabled
    public static List<ContentVersion> getAttachments (Id propertyId) {
        List<ContentDocumentLink> links;
        String objectAPIName = '';
        String keyPrefix = '';
        keyPrefix = String.valueOf(propertyId).substring(0,3);
        for( Schema.SObjectType obj : Schema.getGlobalDescribe().Values() ){
             String prefix = obj.getDescribe().getKeyPrefix();
              if(prefix == keyPrefix){
                        objectAPIName = obj.getDescribe().getName();
                        break;
               }
        }
        System.debug('Debug Log for objectAPIName:'+objectAPIName);
        if(String.isNotBlank(objectAPIName)) {
            if(objectAPIName == 'Document_Checklist__c') {
                links = [select id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where LinkedEntityId=:propertyId];
                
                if (links.isEmpty()) {
                    return null;
                }
                
                Set<Id> contentIds = new Set<Id>();
                
                for (ContentDocumentLink link :links) {
                    contentIds.add(link.ContentDocumentId);
                }
                
                return [SELECT Id, Title FROM ContentVersion WHERE ContentDocumentId IN :contentIds AND IsLatest=true];
            }
            else if(objectAPIName == 'Loan_Application__c') {
                List<Document_Checklist__c> lstDocumentList = [SELECT Id FROM Document_Checklist__c WHERE Loan_Applications__c =: propertyId];
                Set<Id> setDocumentListIds = new Set<Id>();
                System.debug('Debug Log for lstDocumentList'+lstDocumentList.size());
                if(!lstDocumentList.isEmpty()) {
                    for(Document_Checklist__c objDC : lstDocumentList) {
                        setDocumentListIds.add(objDC.Id);
                    }
                }
                System.debug('Debug Log for setDocumentListIds'+setDocumentListIds.size());
                if(!setDocumentListIds.isEmpty()) {
                    links = [select id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where LinkedEntityId IN:setDocumentListIds];
                    
                    if (links.isEmpty()) {
                        return null;
                    }
                    
                    Set<Id> contentIds = new Set<Id>();
                    
                    for (ContentDocumentLink link :links) {
                        contentIds.add(link.ContentDocumentId);
                    }
                    
                    return [SELECT Id, Title FROM ContentVersion WHERE ContentDocumentId IN :contentIds AND IsLatest=true];
                }
            }
        }
        return new List<ContentVersion>();
    }
}