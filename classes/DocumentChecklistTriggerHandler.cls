/*=====================================================================
 * Deloitte India
 * Name:DocumentChecklistTriggerHandler
 * Description: This class is a handler class and supports Document Checklist trigger.
 * Created Date: [20/06/2018]
 * Created By:Shwetadri Ghosh(Deloitte India)
 *
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/
public class DocumentChecklistTriggerHandler {

    private static DocumentChecklistTriggerHandler instance = null;
    public static DocumentChecklistTriggerHandler getInstance(){

        if(instance == null) instance = new DocumentChecklistTriggerHandler();
        return instance;
    }

    public void afterUpdate(List<Document_Checklist__c> newList,List<Document_Checklist__c> oldList,Map<id,Document_Checklist__c> newMap,Map<id,Document_Checklist__c> oldMap){

        Set<ID> docLstIDs = new Set<ID>();
		List<Document_Checklist__c> docChkLsts = new List<Document_Checklist__c>();
        list<id> OTCList = new list<id>();
        Document_Type__c propertyPapers = [SELECT Id FROM Document_Type__c WHERE Name =: Constants.PROPERTYPAPERS];
		List<Document_Checklist__c> docList = new List<Document_Checklist__c>(); // [07-05-19] Added by KK for new OTC/ PDD changes
		Map<Document_Checklist__c,String> mapDocChkListtoLAID = new Map<Document_Checklist__c,String>();
		List<Document_Checklist__c> oldDocList = new List<Document_Checklist__c>(); // [07-05-19] Added by KK for new OTC/ PDD changes
		Document_Checklist__c doc = new Document_Checklist__c();

        for(Document_Checklist__c docLst : newList){            

            if(docLst.Status__c == 'Uploaded' && docLst.Scan_Check_Completed__c == TRUE && oldMap.get(docLst.id).Scan_Check_Completed__c != TRUE && (docLst.Document__c == 'Passport' || docLst.Document__c == 'Voter ID' || docLst.Document__c == 'GSTIN'))
            {
                docLstIDs.add(docLst.id);
            }

            if(propertyPapers != null && docLst.Document_Type__c == propertyPapers.Id)
            {
                docChkLsts.add(docLst);
            }

            if(docLst.OTC_PDD_check__c == true && (docLst.Scan_Check_Completed__c == TRUE && oldMap.get(docLst.Id).Scan_Check_Completed__c != TRUE) ) // [30-04] Logic modified by KK for new OTC/ PDD changes
            {
                OTCList.add(docLst.id);
            }
			// [07-05-19] Added by KK for new OTC/ PDD changes: Code Begins
            if((docLst.Status__c == 'Lawyer OTC' && oldMap.get(docLst.Id).Status__c != 'Lawyer OTC') || (docLst.Status__c == 'OTC Approved' && oldMap.get(docLst.Id).Status__c != 'OTC Approved') || (docLst.Status__c == 'PDD' && oldMap.get(docLst.Id).Status__c != 'PDD')){
                docList.add(docLst);
            }
            if((docLst.Status__c != 'Lawyer OTC' && oldMap.get(docLst.Id).Status__c == 'Lawyer OTC') || (docLst.Status__c != 'OTC Approved' && oldMap.get(docLst.Id).Status__c == 'OTC Approved') || (docLst.Status__c != 'PDD' && oldMap.get(docLst.Id).Status__c == 'PDD')){
                
                
                doc.Status__c = oldMap.get(docLst.Id).Status__c;
                doc.Id = docLst.Id;
                
                //oldDocList.add(doc);
                mapDocChkListtoLAID.put(doc, docLst.Loan_Applications__c);
            }
            // Code Ends
        }

        if(docLstIDs.size()>0){
             DocumentChecklistTriggerHelper.completeOCR(docLstIDs);
        }

        if(OTCList.size() > 0){
            DocumentChecklistTriggerHelper.OTCIntegration(OTCList);
        }
		// [07-05-19] Added by KK for new OTC/ PDD changes
        if(docList.size()>0){
            increaseCountonLA(docList);
        }
        if(mapDocChkListtoLAID.size()>0){
            decreaseCountonLA(mapDocChkListtoLAID);
        }
        // Code Ends
    } 


    
    public void beforeUpdate(List<Document_Checklist__c> newList,List<Document_Checklist__c> oldList,Map<id,Document_Checklist__c> newMap,Map<id,Document_Checklist__c> oldMap){
     DocumentChecklistTriggerHelper.DocumentMapping(newList, oldMap);
        List<Document_Checklist__c> lstDocUploaded = new List<Document_Checklist__c>(); //Added by Abhilekh on 3rd June 2019 for TIL-794
        List<Document_Checklist__c> lstDocPending = new List<Document_Checklist__c>(); //Added by Abhilekh on 3rd June 2019for TIL-794
     Date dt = LMSDateUtility.lmsDateValue;
     List<Id> lstLAIds = new List<Id>();  //Added by Saumya For OTC/PDD Changes for TIL 1246
     for(Document_Checklist__c doc : newList){

            lstLAIds.add(doc.Loan_Applications__c);  //Added by Saumya For OTC/PDD Changes for TIL 1246					 
                     /*if(doc.Status__c != constants.DOC_STATUS_PENDING  && oldMap.get(doc.Id).Status__c != doc.status__c && (doc.Status__c == constants.DOC_STATUS_RECIEVED || doc.Status__c == constants.DOC_STATUS_UPLOADED) && (doc.Received_stage__c != null || doc.Received_stage__c != '')){ //Added by Saumya For OTC/PDD Changes for TIL 1246
                
                doc.Received_stage__c = doc.Current_Stage__c;
															 
		  
																													 
                doc.Business_Date_Received_Updated__c = (Date)dt;
            }*/
             /**** //Added by Saumya For OTC/PDD Changes for TIL 1246 ****/

            if(oldMap.get(doc.Id).Status__c == constants.DOC_STATUS_PENDING  && oldMap.get(doc.Id).Status__c != doc.status__c && (doc.Status__c == constants.DOC_STATUS_RECIEVED || doc.Status__c == constants.DOC_STATUS_UPLOADED) && (doc.Received_stage__c != null || doc.Received_stage__c != '')){ //Added by Saumya For OTC/PDD Changes for TIL 1246
                doc.Received_stage__c = doc.Current_Stage__c;
                doc.Business_Date_Received_Updated__c = (Date)dt;
            }
            /*if(oldMap.get(doc.Id).Status__c != doc.status__c && (doc.Status__c == constants.DOC_STATUS_RECIEVED || doc.Status__c == constants.DOC_STATUS_UPLOADED) && (doc.Received_stage__c != null || doc.Received_stage__c != '')){ //Added by Saumya For OTC/PDD Changes for TIL 1246
                doc.Received_stage__c = doc.Current_Stage__c;
                doc.Business_Date_Received_Updated__c = (Date)dt;
            }*/
            /**** //Added by Saumya For OTC/PDD Changes for TIL 1246 ****/
            if(doc.Original_Status__c == null){
            doc.Original_Status__c = doc.Status__c; //Added by Saumya For OTC/PDD Changes for TIL 1246
            }
            if(doc.Last_Updated_Requested_Date__c == null){
                doc.Last_Updated_Requested_Date__c = doc.REquest_Date_for_OTC__c;
            }
            if(doc.Request_Date_for_OTC_PDD_at_OTC_Receive__c == null && doc.REquest_Date_for_OTC__c != null){
                 doc.Request_Date_for_OTC_PDD_at_OTC_Receive__c = doc.REquest_Date_for_OTC__c;
            }
            else if(doc.REquest_Date_for_OTC__c != null && doc.REquest_Date_for_OTC__c != oldMap.get(doc.Id).REquest_Date_for_OTC__c){
                    doc.Last_Updated_Requested_Date__c = doc.REquest_Date_for_OTC__c;
            }
            /**** //Added by Saumya For OTC/PDD Changes for TIL 1246 ****/
            //    else if(doc.Status__c == constants.DOC_STATUS_RECIEVED && doc.Status__c == constants.DOC_STATUS_UPLOADED){
            //     doc.Business_Date_Received_Updated__c = (Date)dt;
            // }  
             //Below if block added by Abhilekh on 3rd June 2019 for TIL-794
            if(doc.Status__c == Constants.DOC_STATUS_UPLOADED && oldMap.get(doc.Id).Status__c != Constants.DOC_STATUS_UPLOADED){ //Modified by Saumya For Workflow Issues
                lstDocUploaded.add(doc);
            }
            
            //Below if block added by Abhilekh on 3rd June 2019 for TIL-794
            if((doc.Status__c == Constants.DOC_STATUS_PENDING && oldMap.get(doc.Id).Status__c != Constants.DOC_STATUS_PENDING) || (doc.Status__c == Constants.DOC_STATUS_RECIEVED && oldMap.get(doc.Id).Status__c != Constants.DOC_STATUS_RECIEVED)){ //Modified by Saumya For Workflow Issues
                lstDocPending.add(doc);
            }							
     }
	if(lstLAIds.size() >0){ //Added by Saumya For OTC/PDD Changes for TIL 1246
			List<Loan_Application__c> lstLA = [Select Id, Sub_Stage__c from Loan_Application__c where Id IN: lstLAIds];
			for(Loan_Application__c la : lstLA){
				for(Document_Checklist__c doc : newList){
					if(la.Sub_Stage__c == 'CPC Scan Checker' && doc.Scan_Check_Completed__c == TRUE && oldMap.get(doc.Id).Scan_Check_Completed__c == FALSE/* && doc.Status__c != 'Uploaded' && doc.Status__c != 'Waived Off'*/){
					doc.Status_at_OTC_Receiving__c = doc.Status__c;
                        if(doc.Status__c != 'Uploaded' && doc.Status__c != 'Waived Off'){
                            doc.Scan_Check_Completed__c = false;
                        }
					}
                    if(doc.Scan_Check_Completed__c == TRUE && ((oldMap.get(doc.Id).Status__c == 'Uploaded' && doc.Status__c != 'Uploaded') || (oldMap.get(doc.Id).Status__c == 'Waived Off' && doc.Status__c != 'Waived Off')) && doc.Loan_Downsizing__c == null && la.Sub_Stage__c != 'Tranche Processing'/*Added by KK for downsizing*/){
						doc.Scan_Check_Completed__c = false;
					}
                        /*if((doc.Status__c != 'Uploaded' || doc.Status__c != 'Waived Off') && doc.Scan_Check_Completed__c && la.Sub_Stage__c == 'CPC Scan Checker'){
                            doc.Scan_Check_Completed__c = false;
                        }*/
                    if(la.Sub_Stage__c == 'OTC Receiving' && (doc.Status__c == 'Uploaded' || doc.Status__c == constants.DOC_STATUS_RECIEVED) && (doc.Received_stage__c == null || doc.Received_stage__c == '')){
					doc.Received_stage__c = doc.Current_Stage__c;
                    doc.Business_Date_Received_Updated__c = (Date)dt;
					}
				}
			}
		}
	 //Below if block added by Abhilekh on 3rd June 2019 for TIL-794
        if(lstDocUploaded.size() > 0){
            DocumentChecklistTriggerHelper.checkAttachmentOnUpload(lstDocUploaded);
        }
        
        //Below if block added by Abhilekh on 3rd June 2019 for TIL-794
        if(lstDocPending.size() > 0){
            DocumentChecklistTriggerHelper.checkAttachmentOnUpload(lstDocPending);
        }
    }
        
     public static void afterInsert(List<Document_Checklist__c> newList,List<Document_Checklist__c> oldList,Map<id,Document_Checklist__c> newMap,Map<id,Document_Checklist__c> oldMap){

    }

    public static void beforeInsert(List<Document_Checklist__c> newList)
    {   list<string> docType = new list<string>();
            list<string> docMaster = new list<string>();
list<Id> docLAId = new list<Id>();//Added by Saumya For OTC/PDD Changes for TIL 1246
        Set<Id> loanSanctionIds  = new Set<Id>();
        Date dt = LMSDateUtility.lmsDateValue;

       

        for( Document_Checklist__c doc : newList )
        {
             doctype.add(doc.Document_Type__c);
                docMaster.add(doc.document_master__c);

            docLAId.add(doc.Loan_Applications__c);//Added by Saumya For OTC/PDD Changes for TIL 1246
			doc.Last_Updated_Requested_Date__c = doc.REquest_Date_for_OTC__c; //Added by Saumya For OTC/PDD Changes for TIL 1246
			doc.Original_Status__c = doc.Status__c; //Added by Saumya For OTC/PDD Changes for TIL 1246
            if( doc.Loan_Sanction_Condition__c != null ){

                loanSanctionIds.add(doc.Loan_Sanction_Condition__c);
            }
            if(doc.Status__c == 'Received'){

                doc.Business_Date_Received_Updated__c = (Date)dt;
            }
        }

        expiryPOssible(newList,docMaster, doctype);
        if( loanSanctionIds.size() > 0 )
        {
            Map<Id,Loan_Sanction_Condition__c> allSanction = new Map<Id, Loan_Sanction_Condition__c>([select Id,Loan_Application__c from Loan_Sanction_Condition__c where Id IN :loanSanctionIds]);
            
            for(Document_Checklist__c doc : newList)
            {
               

                if(doc.Loan_Sanction_Condition__c != null)
                {
                    doc.Loan_Applications__c  = allSanction.get(doc.Loan_Sanction_Condition__c).Loan_Application__c;
                }   
            }
        }
	 //Added by Saumya For OTC/PDD Changes for TIL 1246
        if(docLAId.size()>0){
            List<Loan_Application__c> lstLA = [Select Id, Sub_Stage__c from Loan_Application__c where Id IN: docLAId];
            for(Loan_Application__c la : lstLA){
                for(Document_Checklist__c docLst : newList){
                    if(la.Sub_Stage__c == 'OTC Receiving'){
                        if(docLst.Status__c == 'Lawyer OTC' || docLst.Status__c == 'OTC Approved' || docLst.Status__c == 'PDD'){
                            docLst.Original_Status__c = docLst.Status__c;
                            docLst.Last_Updated_Requested_Date__c = docLst.REquest_Date_for_OTC__c;
                            docLst.Request_Date_for_OTC_PDD_at_OTC_Receive__c = docLst.REquest_Date_for_OTC__c;
                            docLst.Status_at_OTC_Receiving__c = docLst.Status__c;
                        }
                    }
                }
            }
        }
       //Added by Saumya For OTC/PDD Changes for TIL 1246
    }

    public static void expiryPOssible(List<Document_Checklist__c> newList, list<string> docMaster, list<string> docType){
        List<document_master__c> MasterList = new list<document_master__c>();
        list<Document_Type__c> TypeList = new list<Document_Type__c>();

        MasterList = [select id, Expiration_Possible__c , name from document_master__c where id in :docMaster];
        TypeList = [select id, Expiration_Possible__c , name from Document_Type__c where id in :docType];

        for(Document_Checklist__c dc: newList){

            for(document_master__c dm: MasterList){

                if(dc.document_master__c == dm.id){
                    dc.Expiration_Possible__c = dm.Expiration_Possible__c;
                }
            }

            for(Document_Type__c dm: TypeList){

                if(dc.Document_Type__c == dm.id){
                    dc.Expiration_Possible__c = dm.Expiration_Possible__c;

                }
                
            }
        }
    }
	 // [07-05-19] Added by KK for new OTC/ PDD changes: Code Begins
    public static void increaseCountonLA(List<Document_Checklist__c> docList){
        Set<Id> laId = new Set<Id>();
        List<Loan_Application__c> laList = new List<Loan_Application__c>();
        List<Loan_Application__c> laToUpdate = new List<Loan_Application__c>();
        for(Document_Checklist__c doc: docList){
            laId.add(doc.Loan_Applications__c);
        }
        laList = [Select Id, Lawyer_OTC__c, PDD__c, OTC_Approved__c from Loan_Application__c where Id =: laId];
        for(Loan_Application__c la: laList){
            for(Document_Checklist__c doc: docList){
                if(doc.Loan_Applications__c == la.Id){
                    if(doc.Status__c == 'Lawyer OTC'){
                        if(la.Lawyer_OTC__c != null){
                            la.Lawyer_OTC__c = la.Lawyer_OTC__c+1;
                        }
                        else{
                            la.Lawyer_OTC__c = 1;
                        }
                    }
                    if(doc.Status__c == 'OTC Approved'){
                        if(la.OTC_Approved__c != null){
                            la.OTC_Approved__c = la.OTC_Approved__c+1;
                        }
                        else{
                            la.OTC_Approved__c = 1;
                        }
                    }
                    if(doc.Status__c == 'PDD'){
                        if(la.PDD__c != null){
                            la.PDD__c = la.PDD__c +1;
                        }
                        else{
                            la.PDD__c = 1;
                        }
                    }
                }
            }
            laToUpdate.add(la);
        }
        System.debug('===SIZE==='+laToUpdate.size());
        if(laToUpdate.size()>0){
            database.update(laToUpdate);
        }
    }
    public static void decreaseCountonLA(Map<Document_Checklist__c,String> mapDocChkListtoLAID){
        Set<Id> laId = new Set<Id>();
        List<Loan_Application__c> laList = new List<Loan_Application__c>();
        List<Loan_Application__c> laToUpdate = new List<Loan_Application__c>();
        for(Document_Checklist__c doc: mapDocChkListtoLAID.keySet()){
            laId.add(mapDocChkListtoLAID.get(doc));
        }
        laList = [Select Id, Lawyer_OTC__c, PDD__c, OTC_Approved__c from Loan_Application__c where Id IN: laId];
        for(Loan_Application__c la: laList){
            //System.debug('===STATUS==='+doc.Status__c+'===LA==='+mapDocChkListtoLAID.get(doc));
            for(Document_Checklist__c doc: mapDocChkListtoLAID.keySet()){
                if(mapDocChkListtoLAID.get(doc) == la.Id){
                    if(doc.Status__c == 'Lawyer OTC'){
                        if(la.Lawyer_OTC__c != null){
                            la.Lawyer_OTC__c = la.Lawyer_OTC__c-1;
                        }
                        else{
                            la.Lawyer_OTC__c = 0;
                        }
                    }
                    if(doc.Status__c == 'OTC Approved'){
                        if(la.OTC_Approved__c != null){
                            la.OTC_Approved__c = la.OTC_Approved__c-1;
                        }
                        else{
                            la.OTC_Approved__c = 0;
                        }
                    }
                    if(doc.Status__c == 'PDD'){
                        if(la.PDD__c != null){
                            la.PDD__c = la.PDD__c -1;
                        }
                        else{
                            la.PDD__c = 0;
                        }
                    }
                }
            }
            laToUpdate.add(la);
        }
        System.debug('===SIZE==='+laToUpdate.size());
        if(laToUpdate.size()>0){
            database.update(laToUpdate);
        }
    }
    // Code Ends
}