public class PanValidationCheckController {
	
    @auraenabled
    public static String checkPanDetails(Id loanContactId)
    {
        String message;
        Loan_Contact__c lc = [Select Id, Pan_Verification_status__c,PAN_Response__c,Loan_Applications__r.Sub_Stage__c,Loan_Applications__r.Overall_FCU_Status__c,Loan_Applications__r.FCU_Decline_Count__c,Loan_Applications__r.Exempted_from_FCU_BRD__c,Loan_Applications__r.Hunter_Decline_Count__c from Loan_Contact__c where Id =: loanContactId];
		//Loan_Applications__r.Sub_Stage__c,Loan_Applications__r.Overall_FCU_Status__c,Loan_Applications__r.FCU_Decline_Count__c added by Abhilekh on 11th October 2019 for FCU BRD
		//Loan_Applications__r.Exempted_from_FCU_BRD__c added by Abhilekh on 23rd December 2019 for FCU BRD Exemption
		//Loan_Applications__r.Hunter_Decline_Count__c added by Abhilekh on 24th January 2020 for Hunter BRD
		
        if(lc.PAN_Response__c){
        	message = 'PAN has been already hit for verification. Please try after some time.';    
        }
		//Below else if block added by Abhilekh on 11th October 2019 for FCU BRD,&& lc.Loan_Applications__r.Exempted_from_FCU_BRD__c == false added by Abhilekh on 23rd October 2019 for FCU BRD Exemption
        else if((lc.Loan_Applications__r.Sub_Stage__c == Constants.Credit_Review || lc.Loan_Applications__r.Sub_Stage__c == Constants.Re_Credit || lc.Loan_Applications__r.Sub_Stage__c == Constants.Re_Look) && lc.Loan_Applications__r.Overall_FCU_Status__c == Constants.NEGATIVE && lc.Loan_Applications__r.FCU_Decline_Count__c > 0 && lc.Loan_Applications__r.Exempted_from_FCU_BRD__c == false){
            message = 'You cannot perform this action as the Loan Application is FCU Decline.';
        }
		//Below else if block added by Abhilekh on 24th January 2020 for Hunter BRD
        else if((lc.Loan_Applications__r.Sub_Stage__c == Constants.Credit_Review || lc.Loan_Applications__r.Sub_Stage__c == Constants.Re_Credit || lc.Loan_Applications__r.Sub_Stage__c == Constants.Re_Look) && lc.Loan_Applications__r.Hunter_Decline_Count__c > 0){
            message = 'You cannot perform this action as Hunter analysis has not been recommended. Please reject or assign the application to Hunter Manager.';
        }
        else if(lc.Pan_Verification_status__c){
            message = 'PAN has been verified for this customer! Are you sure you want to verify again?';
        }
        else{
            message = 'OK';
        }
        return message;
    }
}