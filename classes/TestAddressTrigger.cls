@istest
public class TestAddressTrigger {
    public static testmethod void method2(){
		Profile p1 = [SELECT Id FROM Profile WHERE Name='Sales Team'];
           
         User u = new User(
                     ProfileId =p1.id,
                     LastName = 'last',
                     Email = 'puser001@amamama.com',
                     Username = 'puser001@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias', 
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US', 
                     LocaleSidKey = 'en_US',
					 Role_in_Sales_Hierarchy__c = 'SM'

                     //UserRoleid = r.id
                     );
        insert u;
                Account Cob= new Account(PAN__c='BXEGQ3767A',Salutation='Mr',FirstName='Test',LastName='TestAccount',Phone='8878799098',
                                         Date_of_Incorporation__c=Date.newInstance(2018,11,11));
                
                Database.insert(Cob);                
                
                Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',
                                            Scheme_ID__c=11,Product_Code__c='CF',Max_ROI__c=10,Min_ROI__c=5,
                                            Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                            Max_Loan_Amount__c=6000000, Max_Tenure__c=5, Min_Tenure__c=1,
                                            Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
                
                Database.insert(sc); 
            Loan_Application__c LAob= new Loan_Application__c(Customer__c=cob.id,Scheme__c=sc.id,
                                                                  StageName__c='Customer Onboarding',
                                                                  Transaction_type__c='TOP',Requested_Amount__c=5000001,
                                                                  Applicant_Customer_segment__c='Salaried',
                                                                  Branch__c='test',Line_Of_Business__c='Open Market',
                                                                  Loan_Purpose__c='20',
                                                                  Requested_EMI_amount__c=1000,Approved_ROI__c=5,
                                                                  Requested_Loan_Tenure__c=5,
                                                                  Government_Programs__c='PMAY',
                                                                  Loan_Engine_2_Output__c='STP',
                                                                  Property_Identified__c=true,
                                                                  Existing_Loan_Application_Number__c='4567',
                                                                  Outstanding_Loan_amount__c=10000,Name_of_BT_Bank__c='TST',Running_since__c=Date.today().addDays(-100)
                                                                 );
                
            Database.insert(LAob);   
            String strName = [select id,name from loan_application__C where id=:laob.id].name;
                Loan_Contact__c LCobject=[SELECT id from Loan_Contact__c where Loan_Applications__c=:LAob.id];
                lcobject.Income_Considered__c=true;
                lcobject.Borrower__c='1';
                lcobject.Constitution__c='20';
                lcobject.Applicant_Status__c='1';
                lcobject.Customer_segment__c='1';
                lcobject.Income_Program_Type__c = 'Normal Salaried';
                lcobject.Gender__c = 'M';
                lcobject.Category__c ='1';
                lcobject.Religion__c = '1';
                lcobject.cibil_verified__c = true;
        		lcobject.Mobile__c='9876543210';
        		lcobject.Email__c='testemail@gmail.com';
                update lcobject;
         
            test.startTest();
            sourcing_detail__c objsource = new sourcing_detail__c();
            objsource.Loan_Application__c = LAob.id;
            insert objsource;
            laob.stagename__c = 'Operation Control';
            system.debug('@@@laob1' + laob.Existing_Loan_Application_Number__c);
            system.debug('@@@strName' + strName);
            laob.Existing_Loan_Application_Number__c = strName;
            update laob;
               Address__c objAdd = new Address__c(Loan_Contact__c = lcobject.id);
            insert objAdd;
        Pincode__c pin=new Pincode__c(Name='TEST',City__c='TEST',ZIP_ID__c='TEST');
                Database.insert(pin);
        objAdd.Pincode_LP__c = pin.id;
        objAdd.Address_Line_1__c = 'test';
        objAdd.Address_Line_2__c = 'test';
        update objAdd;
        map<id, address__c> mapLoan;
        list<address__C> lstLoan = new list<address__c>();
        lstloan.add(objAdd);
        //Address_TriggerHandler.updateStateAndCountry(lstloan,maploan);
    }
}