@isTest

public class TestDisbursementTriggerHelper {
     static testmethod void test1(){
        User dstu=[SELECT id,Name,ProfileId,LastName,Email,Username, CompanyName,
                       Title,Alias,TimeZoneSidKey,
                       EmailEncodingKey,LanguageLocaleKey,
                       LocaleSidKey,UserRoleId 
                       from user where profile.Name='DST' and isActive=true limit 1];
        
        Latest_Number__c cs = new Latest_Number__c(Latest_Number_Running__c=100);
        insert cs;
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ATBQD1854N',Name='Account5789345',Date_of_Incorporation__c=System.today(),
                                  Phone='7835672213',RecordTypeId=RecordTypeIdAccount);
        
        Account acc1 = new Account(PAN__c='QWERT1567J',Name='Account1564',Date_of_Incorporation__c=System.Today(),
                                  Phone='9345678910',RecordTypeId=RecordTypeIdAccount);
        
        List<Account> lstAcc = new List<Account>();
        lstAcc.add(acc);
        lstAcc.add(acc1);
        insert lstAcc;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='CF',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Branch_Master__c objBranchMaster = new Branch_Master__c(Name='Test Branch',Office_Code__c='TestOC');
        insert objBranchMaster;
        
        Id RecordTypeIdLoanApplication = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('COPS: Credit Operations Entry').getRecordTypeId();
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11',StageName__c='Customer Onboarding',
                                                                        RecordTypeId=RecordTypeIdLoanApplication,
                                                                        Business_Date_Created__c=System.today(),
                                                                        Approved_Loan_Amount__c=100000,Branch_Lookup__c=objBranchMaster.Id,
                                                                        Processing_Fee_Percentage__c=1,Mode_of_Repayment__c='NACH',
                                                                        Repayment_Start_Date__c=Date.Today().addDays(100),
                                                                        Sanctioned_Disbursement_Type__c='Multiple Tranche');
        insert objLoanApplication;
        
        Note_Details__c objNoteDetails = new Note_Details__c(Branch__c=objBranchMaster.Id);
        insert objNoteDetails;
        
        LMS_Date_Sync__c objLMSDateSync = new LMS_Date_Sync__c(Current_Date__c=System.Today());
        insert objLMSDateSync;
        
        Charge_Code__c objChargeCode = new Charge_Code__c(Name='ROC CHARGES',ChargeCodeID__c='Test',Charge_Rate__c=5);
            
        Charge_Code__c objChargeCode1 = new Charge_Code__c(Name='STAMP DUTY',ChargeCodeID__c='Test1',Charge_Rate__c=10);
        
        Charge_Code__c objChargeCode2 = new Charge_Code__c(Name='CERSAI Charges',ChargeCodeID__c='Test2',Charge_Rate__c=7);
        
        Charge_Code__c objChargeCode3 = new Charge_Code__c(Name='LEGAL CHARGES',ChargeCodeID__c='Test3',Charge_Rate__c=6);
        
        Charge_Code__c objChargeCode4 = new Charge_Code__c(Name='PEMI',ChargeCodeID__c='Test4',Charge_Rate__c=6);
        
        Charge_Code__c objChargeCode5 = new Charge_Code__c(Name='APPLICATION PROCESSING FEES',ChargeCodeID__c='Test5',Charge_Rate__c=8);
        
        List<Charge_Code__c> lstChargeCode = new List<Charge_Code__c>();
        lstChargeCode.add(objChargeCode);
        lstChargeCode.add(objChargeCode1);
        lstChargeCode.add(objChargeCode2);
        lstChargeCode.add(objChargeCode3);
        lstChargeCode.add(objChargeCode4);
        lstChargeCode.add(objChargeCode5);
        insert lstChargeCode;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Co-Applicant').getRecordTypeId();
        
        Contact con = new Contact(LAstNAme='test677', Accountid=acc1.Id);
        insert con;
        
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc1.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='2',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='19',
                                                             Customer_segment__c='11',Applicant_Status__c='1',
                                                             Income_Program_Type__c='Liquid Income Program',
                                                             Contact__c=con.id,
                                                             Income_Considered__c=true);
        insert objLoanContact;
        
        PDC_Bank_City__c objPDCBankCity = new PDC_Bank_City__c(Active_Status__c=true,City_Id__c='Udaipur');
        insert objPDCBankCity;
        
        PDC_Bank_Master__c objPDCBankMaster = new PDC_Bank_Master__c(PDC_Bank_City__c=objPDCBankCity.Id,Bank_Code__c='101',
                                                                     Branch_ID__c='102',MICR__c='110229003',IFSC__c='BANK000005');
        insert objPDCBankMaster;
        
        Loan_Contact__c objLC = [SELECT Id from Loan_Contact__c WHERE Applicant_Type__c='Applicant' AND Loan_Applications__c=:objLoanApplication.Id LIMIT 1];
        
        Customer_Obligations__c objCustOblg = new Customer_Obligations__c(Customer_Detail__c=objLC.Id, EMI_Amount__c = 1);
        
        Customer_Obligations__c objCustOblg1 = new Customer_Obligations__c(Customer_Detail__c=objLoanContact.Id, EMI_Amount__c = 1);
        
        List<Customer_Obligations__c> lstCustOblg = new List<Customer_Obligations__c>();
        lstCustOblg.add(objCustOblg);
        lstCustOblg.add(objCustOblg1);
        insert lstCustOblg;
        
        List<User> lstUser = [SELECT Id from User WHERE Profile.Name='Sales Team' AND UserRole.Name LIKE '%SM%' AND Manager_Inspector_ID__c != null];
        
        DSA_Master__c objDSAMaster = new DSA_Master__c(Name='A JAGAN MOHAN',Type__c='DSA', BrokerID__c = '1234');
        insert objDSAMaster;
        
        DST_Master__c objDSTMaster = new DST_Master__c(Name=dstu.name,Inspector_ID__c='1234');
        insert objDSTMaster;
        
        Id RecordTypeIdSourceDetail = Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('DSA').getRecordTypeId();
        
        Sourcing_Detail__c objSourceDetail = new Sourcing_Detail__c(Loan_Application__c=objLoanApplication.Id,Sales_User_Name__c=lstUser[0].Id,
                                                                    RecordTypeId=RecordTypeIdSourceDetail,DSA_Name__c=objDSAMaster.Id,
                                                                    DST_Name__c=objDSTMaster.Id,DST__c=dstu.Id);
        insert objSourceDetail;
        
        
        
        //objLoanApplication.StageName__c = 'Customer Onboarding';
        //update objLoanApplication;
        
        
        
        Test.startTest();
        
        objLoanApplication.StageName__c = 'Loan Disbursal';
        update objLoanApplication;
        Tranche__c tr=new Tranche__c();
        tr.Loan_Application__c=objLoanApplication.id;
        tr.Disbursal_Amount__c=100000;
        tr.Approved_Disbursal_Amount__c=100000;
        tr.PEMI_Amount__c=10000;
        tr.Request_Type__c= Constants.TRANCHEDISBURSAL;
        insert tr;
        List<Disbursement__c> lstDisb = new List<Disbursement__c>();
        
        Disbursement__c objDisbursement = new Disbursement__c(Loan_Application__c=objLoanApplication.Id,
                                                              Disbursal_Amount__c=50000,ROC_Charges__c=5000,
                                                              Stamp_Duty__c=2000,CERSAI_Charges__c=5000,
                                                              Legal_Vetting__c=7000,PEMI__c=10000,Cheque_Amount__c=10000,Tranche__c=tr.id);
         lstDisb.add(objDisbursement);                                                    
        insert lstDisb;
        DisbursementTriggerHelper.validateDisbursedAmount(lstDisb);
     }
    
    public static testMethod void method1(){
        
        id trancheRecTypeId = Schema.SObjectType.tranche__c.getRecordTypeInfosByName().get('Credit').getRecordTypeId();
        id loanAppRecType = Schema.SObjectType.loan_application__c.getRecordTypeInfosByName().get('Tranche').getRecordTypeId();
        
        
        Account custObj = new Account();
        custObj.Name = 'Test Customer';
        custObj.Phone = '9999999994';
        database.insert(custObj);
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        database.insert(schemeObj);
        
         
        Loan_Application__c lap = new Loan_Application__c();
        lap.RecordTypeId = loanAppRecType;
        lap.customer__c = custObj.id;
        //loanAppObj.Loan_Application_Number__c = 'LA00001';
        lap.Loan_Purpose__c = '11';
        lap.scheme__c = schemeObj.id;
        lap.Transaction_type__c = 'PB';
        lap.Requested_Amount__c = 200000;
        lap.Approved_Loan_Amount__c = 150000;
        lap.StageName__c = 'Loan Disbursal';
        lap.sub_stage__c = 'Application Initiation';
        insert lap;
        //String LoanApplicationID=String.valueOf(LAob.id);
        System.debug(lap.id); 
        
        pincode__c pc=new pincode__c();
        pc.name='Bengaluru';
        pc.City__c='Bengaluru';
        pc.ZIP_ID__c='560001';
        
        insert pc;
        Bank_Master__c bmc=new Bank_Master__c();
        bmc.A_c_Holder_Name__c='testtt';
        bmc.A_c_no__c=1231231231;
        bmc.Country__c='1';
        bmc.pincode__c=pc.id;
        
        insert bmc; 
        
        
        Disbursement__c d=new Disbursement__c();
        d.Loan_Application__c = lap.id;
        //d.Mode_of_Payment__c = 'Q';
        d.Bank_Master__c = bmc.id;
        d.Cheque_Amount__c = 2000000;
        d.Cheque_Favouring__c = 'Applicant123';
        d.Disbursal_Amount__c = 150000;
        //insert d ;
        
        //Disbursement__c d2 = [SELECT Cheque_Amount__c FROM Disbursement__c WHERE Id=:d.Id];
        //system.AssertEquals(2000000,d2.Cheque_Amount__c);
        //d.Cheque_Amount__c = 2500000;
        
        //update d;
        try 
        {
            //Disbursement__c d3 = [SELECT Cheque_Amount__c FROM Disbursement__c WHERE Id=:d.Id];
            //system.AssertEquals(2500000,d3.Cheque_Amount__c);
            //d.Cheque_Amount__c = 6000000;
            //update d;
        }
        catch(Exception e)
        {
            // Boolean expectedExceptionThrown =  e.getMessage().contains('Maximum allowed Amount for this disbursement is Rs.' + d.Cheque_Amount__c) ? true : false;
            // System.AssertEquals(expectedExceptionThrown, true);
        } 
        
        
        
    }
    public static testMethod void method2()
    {
       id trancheRecTypeId = Schema.SObjectType.tranche__c.getRecordTypeInfosByName().get('Credit').getRecordTypeId();
        id loanAppRecType = Schema.SObjectType.loan_application__c.getRecordTypeInfosByName().get('Tranche').getRecordTypeId();
        
        
        Account custObj = new Account();
        custObj.Name = 'Test Customer';
        custObj.Phone = '9999959999';
        database.insert(custObj);
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        database.insert(schemeObj);
        
         
        Loan_Application__c lap = new Loan_Application__c();
        lap.RecordTypeId = loanAppRecType;
        lap.customer__c = custObj.id;
        //loanAppObj.Loan_Application_Number__c = 'LA00001';
        lap.Loan_Purpose__c = '11';
        lap.scheme__c = schemeObj.id;
        lap.Transaction_type__c = 'PB';
        lap.Requested_Amount__c = 200000;
        lap.Approved_Loan_Amount__c = 150000;
        lap.StageName__c = 'Loan Disbursal';
        lap.sub_stage__c = 'Application Initiation';
        insert lap;
        
        pincode__c pc=new pincode__c();
        pc.name='Bengaluru';
        pc.City__c='Bengaluru';
        pc.ZIP_ID__c='560001';
        
        insert pc;
        Bank_Master__c bmc=new Bank_Master__c();
        bmc.A_c_Holder_Name__c='testtt';
        bmc.A_c_no__c=1231231231;
        bmc.Country__c='1';
        bmc.pincode__c=pc.id;
        
        insert bmc; 
        
        
        Disbursement__c d=new Disbursement__c();
        d.Loan_Application__c = lap.id;
        //d.Mode_of_Payment__c = 'Q';
        d.Bank_Master__c = bmc.id;
        d.Cheque_Amount__c = 2000000;
        d.Cheque_Favouring__c = 'Applicant123';
        d.Disbursal_Amount__c = 150000;
        
        try
        {
            List<Loan_Contact__c> LC = [SELECT Id FROM Loan_Contact__c WHERE Loan_Applications__c=:lap.id];
            d.Customer_Detail__c = LC[0].id;
            insert d ;
        }
        catch(Exception e)
        {}   
    }
    
    public static testMethod void method3()
    {
        id trancheRecTypeId = Schema.SObjectType.tranche__c.getRecordTypeInfosByName().get('Credit').getRecordTypeId();
        id loanAppRecType = Schema.SObjectType.loan_application__c.getRecordTypeInfosByName().get('Tranche').getRecordTypeId();
        
        
        Account custObj = new Account();
        custObj.Name = 'Test Customer';
        custObj.Phone = '9919999999';
        database.insert(custObj);
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        database.insert(schemeObj);
        
         
        Loan_Application__c lap = new Loan_Application__c();
        lap.RecordTypeId = loanAppRecType;
        lap.customer__c = custObj.id;
        //loanAppObj.Loan_Application_Number__c = 'LA00001';
        lap.Loan_Purpose__c = '11';
        lap.scheme__c = schemeObj.id;
        lap.Transaction_type__c = 'PB';
        lap.Requested_Amount__c = 200000;
        lap.Approved_Loan_Amount__c = 150000;
        lap.StageName__c = 'Loan Disbursal';
        lap.sub_stage__c = 'Application Initiation';
        insert lap;
        
        pincode__c pc=new pincode__c();
        pc.name='Bengaluru';
        pc.City__c='Bengaluru';
        pc.ZIP_ID__c='560001';
        
        insert pc;
        Bank_Master__c bmc=new Bank_Master__c();
        bmc.A_c_Holder_Name__c='testtt';
        bmc.A_c_no__c=1231231231;
        bmc.Country__c='1';
        bmc.pincode__c=pc.id;
        
        insert bmc; 
        
       /* Disbursement__c objDisbursement = new Disbursement__c(Loan_Application__c=lap.Id,
                                                              Disbursal_Amount__c=150000,ROC_Charges__c=5000,
                                                              Stamp_Duty__c=2000,CERSAI_Charges__c=5000,
                                                              Legal_Vetting__c=7000,PEMI__c=10000);
        insert objDisbursement;
        */
        Disbursement__c d=new Disbursement__c();
        d.Loan_Application__c = lap.id;
        //d.Mode_of_Payment__c = 'Q';
        d.Bank_Master__c = bmc.id;
        d.Cheque_Amount__c = 2000000;
        d.Disbursal_Amount__c = 150000;
        //insert d;
        d.Cheque_Amount__c = 2000000;
        //update d;
    }
    
    public static testMethod void method4()
    {
        id trancheRecTypeId = Schema.SObjectType.tranche__c.getRecordTypeInfosByName().get('Credit').getRecordTypeId();
        id loanAppRecType = Schema.SObjectType.loan_application__c.getRecordTypeInfosByName().get('Tranche').getRecordTypeId();
        
        
        Account custObj = new Account();
        custObj.Name = 'Test Customer';
        custObj.Phone = '9949999999';
        database.insert(custObj);
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        database.insert(schemeObj);
        
         
        Loan_Application__c lap = new Loan_Application__c();
        lap.RecordTypeId = loanAppRecType;
        lap.customer__c = custObj.id;
        //loanAppObj.Loan_Application_Number__c = 'LA00001';
        lap.Loan_Purpose__c = '11';
        lap.scheme__c = schemeObj.id;
        lap.Transaction_type__c = 'PB';
        lap.Requested_Amount__c = 200000;
        lap.Approved_Loan_Amount__c = 150000;
        lap.StageName__c = 'Loan Disbursal';
        lap.sub_stage__c = 'Application Initiation';
        insert lap;
        System.debug(lap.id); 
        
        pincode__c pc=new pincode__c();
        pc.name='Bengaluru';
        pc.City__c='Bengaluru';
        pc.ZIP_ID__c='560001';
        
        insert pc;
        Bank_Master__c bmc=new Bank_Master__c();
        bmc.A_c_Holder_Name__c='testtt';
        bmc.A_c_no__c=1231231231;
        bmc.Country__c='1';
        bmc.pincode__c=pc.id;
        
        insert bmc; 
        
        
        Disbursement__c d=new Disbursement__c();
        d.Loan_Application__c = lap.id;
        //d.Mode_of_Payment__c = 'Q';
      //  d.Bank_Master__c = bmc.id;
        d.Cheque_Amount__c = 2000000;
        d.Disbursal_Amount__c = 150000;
        List<Loan_Contact__c> LC1 = [SELECT Id FROM Loan_Contact__c WHERE Loan_Applications__c=:lap.id];
        d.Customer_Detail__c = LC1[0].id;
        try 
        {
           // insert d;
        }
        catch (Exception e)
        {}
    }
}