@IsTest(isParallel=false)
public class TestCreditAuthorityApprovalController 
{
    public static User userDetail;
    public static UserRole r;
    public static  User u1;
    public static User ManagerUser;
    public static User u2;
    
    public static  void createData()
    {
        r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        ID roleID= r.id;
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
         
         ManagerUser= new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puserManager@amamama.com',
                     Username = 'puserManager@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
                     UserRoleId = roleID);
                     
       Insert ManagerUser;
        
         u1 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser001@amamama.com',
                     Username = 'puser001@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
                     Branch_Manager__c=ManagerUser.Id,
                     UserRoleId = roleID,
                    isactive = true);
                     
        insert u1;
         u2 = new User(
                     ProfileId =p.id,
                     LastName = 'last',
                     Email = 'puser002@amamama.com',
                     Username = 'puser002@amamama.com' + System.currentTimeMillis(),
                     CompanyName = 'TEST',
                     Title = 'title',
                     Alias = 'alias',
                     TimeZoneSidKey = 'America/Los_Angeles',
                     EmailEncodingKey = 'UTF-8',
                     LanguageLocaleKey = 'en_US',
                     LocaleSidKey = 'en_US',
                     Branch_Manager__c=ManagerUser.Id,
                     UserRoleId = roleID,
                        isactive = true);
                     
        insert u2;
        
        //ID userId=UserInfo.getUserId();
         userDetail=[SELECT id,LastName,Email,Username,CompanyName,Title,
                        TimeZoneSidKey,EmailEncodingKey,LanguageLocaleKey,UserRoleId,
                        LocaleSidKey from user where ID=:UserInfo.getUserId()];
                        
        userDetail.Branch_Manager__c=u2.id;
        update userDetail;
        
        userDetail=[SELECT id,Branch_Manager__c,LastName,Email,Username,CompanyName,Title,
                        TimeZoneSidKey,EmailEncodingKey,LanguageLocaleKey,UserRoleId,
                        LocaleSidKey from user where profile.name='System Administrator' and email <> 'gnawal@deloitte.com' and email ='saubhasin@deloitte.com' and isactive = true limit 1];
         
        system.debug('***HUNT'+userDetail.Branch_Manager__c+''+'systemrunID'+userDetail.id);
    }
    
    public static  testmethod void method1(){
        createData();
           
        system.runAs(userDetail){
        Account CustomerOb= new Account(PAN__c='AXEGA3767A',Name='TestClassData',Phone='9723453331',Date_of_Incorporation__c=Date.newInstance(2018,11,11));
        insert CustomerOb;
        
        Scheme__c sc= new Scheme__c(Name='Construction Finance',Scheme_Code__c='CF',Scheme_Group_ID__c='5',Scheme_ID__c=11,Product_Code__c='HL');
        insert sc;
        
        Loan_Application__c LAob= new Loan_Application__c(Scheme__c=sc.id,Sub_stage__c='Disbursement Maker',Approval_Required_for_OTC__c=true,
                                                          StageName__c='Customer Onboarding',
                                                          Assigned_Credit_Review__c=u1.id,
                                                          Assigned_Sales_User__c=u2.id,IMD_approval_required__c=true,
                                                          Property_Identified__c=true,
                                                          ZCM__c=UserInfo.getUserId());
        insert LAob;
        LAob=[SELECT Scheme__c,Sub_stage__c,Approval_Required_for_OTC__c,StageName__c,
              Assigned_Credit_Review__c,Assigned_Sales_User__c,IMD_approval_required__c,
              Property_Identified__c,ZCM__c,id,Owner_Manager__c
              from Loan_Application__c where id=:LAob.id];
        
        
        LAob.Sub_stage__c='Disbursement Maker';
        LAob.Approval_Required_for_OTC__c=true;
        LAob.IMD_approval_required__c=true;
        LAob.Original_Property_paper_verified__c=true;
        
        update LAob;
        
        Loan_Application__c query=[select Sub_stage__c,IMD_approval_required__c,Approval_Required_for_OTC__c from Loan_Application__c where id=:LAob.id];
        system.debug('****'+query.Sub_stage__c+ ' ' +query.Approval_Required_for_OTC__c +' '+ query.IMD_approval_required__c);
        
        String str1=String.valueof(LAob.id);
        
        Loan_Application__c LAob2= new Loan_Application__c(Scheme__c=sc.id,Sub_stage__c='L1 Credit Approval',Approval_Required_for_OTC__c=true,
                                                          StageName__c='Customer Onboarding',
                                                          Assigned_Credit_Review__c=u1.id,
                                                          Assigned_Sales_User__c=u2.id,IMD_approval_required__c=true,
                                                          Property_Identified__c=true,
                                                          ZCM__c=UserInfo.getUserId());
        insert LAob2;
         Test.StartTest();
        LAob2=[SELECT Scheme__c,Sub_stage__c,Approval_Required_for_OTC__c,StageName__c,
              Assigned_Credit_Review__c,Assigned_Sales_User__c,IMD_approval_required__c,
              Property_Identified__c,ZCM__c,id,Owner_Manager__c
              from Loan_Application__c where id=:LAob2.id];

        String str2=String.valueof(LAob2.id);
        
        LAob2.Sub_stage__c='Disbursement Maker';
        LAob2.Approval_Required_for_OTC__c=true;
        LAob2.IMD_approval_required__c=true;
        LAob2.Original_Property_paper_verified__c=true;
        update LAob2;
        
       
        // Approval Proceess ...

        
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
        app.setObjectId(LAob.id);
        app.setNextApproverIds(new Id[] {ManagerUser.id});
        Approval.ProcessResult result = Approval.process(app);       
      
        Approval.ProcessSubmitRequest app2 = new Approval.ProcessSubmitRequest();
        app2.setObjectId(LAob2.id);
        app2.setNextApproverIds(new Id[] {ManagerUser.id});
        Approval.ProcessResult result2 = Approval.process(app2); 
        
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setComments('Approving');
        req.setAction('reject');
        req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
              
       
        //Call your class method
        CreditAuthorityApprovalController.setApprovalStatus(str1,'Approve','test');
        
        CreditAuthorityApprovalController.setApprovalStatus(str1,'Reject','test');
        Approval.ProcessWorkitemRequest req1 = new Approval.ProcessWorkitemRequest();
        req1.setComments('Approving');
        req1.setAction('Refer back to credit');
        req1.setNextApproverIds(new Id[] {UserInfo.getUserId()});    
        //CreditAuthorityApprovalController.setApprovalStatus(str1,'Refer back to credit','test');
        CreditAuthorityApprovalController.getPickListValuesIntoList();
        CreditAuthorityApprovalController.getPickListValuesIntoList2();
        //CreditAuthorityApprovalController.rejectApplication(LAob2.id,'Intentional manipulations','Test','1');
        Test.StopTest();
        
        
        }                                        
    
    
    }

    

}