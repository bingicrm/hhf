/*=====================================================================
 * Deloitte India
 * Name:LeadTriggerHandler
 * Description: This class is a handler class and supports Lead trigger.
 * Created Date: [12/06/2018]
 * Created By:Sonali Kansal(Deloitte India)
 *
 * Date Modified                Modified By                  Description of the update
 * []                              []                             []
 =====================================================================*/
public class LeadTriggerHandler {
    
																			   
    
    public static void beforeInsert(List < Lead > newList, Map < Id, Lead > newMap) {
    List<Lead> lstLead = new List<Lead>();// Added by Saumya For Partnership Lead
    List<Lead> lstAhmedabadLead = new List<Lead>();
    List<Lead> lstDelhiLead = new List<Lead>();
    List<Lead> lstHOLead = new List<Lead>();
    List<Lead> lstJaipurLead = new List<Lead>();
    List<Lead> lstMumLead = new List<Lead>();
    List<Lead> lstLkoLead = new List<Lead>();
    List<Lead> lstNashikLead = new List<Lead>();
    List<Lead> lstNSPLead = new List<Lead>();
    List<Lead> lstPuneLead = new List<Lead>();
    List<Lead> lstRajkotLead = new List<Lead>();
    List<Lead> lstSuratLead = new List<Lead>();
    List<Lead> lstUdaipurLead = new List<Lead>();
    List<Lead> lstVadodaraLead = new List<Lead>();
        List<Lead> lstCriteriaLeadsforUserNameValidation = new List<Lead>();
        //Commented to control Name and DOB combination check on insertion
        //LeadTriggerHelper.validateNameDOB(newList, null); 
        //LeadTriggerHelper.checkCustomConvert(newList,null);
        //Added By Shobhit Saxena on 3 May, 2019
        LeadTriggerHelper.applyDefaultEmail(newList);
        //salesMapping(newList,null); // Lead assignment will happen to queue now 
        system.debug('==newList=='+newList);
        //LeadTriggerHelper.setConverted(newList);
        String profileName = [Select Id, Name from Profile where Id=: UserInfo.getProfileId() LIMIT 1].Name;
        System.debug('Debug log for profileName'+profileName);
        /*for(Lead objLead : newList) {
            
        }*/
        if(profileName == 'Integration') {
            for(Lead objLead : newList) {
                
               if(String.isNotBlank(objLead.source_detail__c)) {
                  if(String.isNotBlank(objLead.Vendor_Name__c) && String.isNotBlank(objLead.Vendor_Password__c)) {
                      lstCriteriaLeadsforUserNameValidation.add(objLead);
                      
                      
                  }
               else {
                    if(!Test.isRunningTest()){
                        objLead.addError('Record cannot be saved in system. Kindly provide Vendor Name and Password');
                    }
               }
               }
        if(String.isNotBlank(objLead.Record_Type_Name__c)) {
                   objLead.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(objLead.Record_Type_Name__c).getRecordTypeId();
               }
					 
																		  
																																 
					   
           }
           
                
       }
       System.debug('Debug Log for lstCriteriaLeadsforUserNameValidation'+lstCriteriaLeadsforUserNameValidation.size());
       if(!lstCriteriaLeadsforUserNameValidation.isEmpty()) {
           LeadTriggerHelper.validateCredentialsDigitalVendors(lstCriteriaLeadsforUserNameValidation);
       }
    /** // Added by Saumya For Partnership Lead **/
     lstLead =roundRobin(newList);
	/*if(lstLead.size() > 0){ 
            LeadTriggerHelper.shareLeadsRoundRobin(lstLead, null);
          
        }*/
    for(Lead objLead: lstLead){
       if(objLead.Branch__c=='Mumbai'){
      lstMumLead.add(objLead);     
       }
       else if(objLead.Branch__c=='Nashik'){
      lstNashikLead.add(objLead);   
       }
       else if(objLead.Branch__c=='Vadodara'){
      lstVadodaraLead.add(objLead);
         
       }
       else if(objLead.Branch__c=='Surat'){
      lstSuratLead.add(objLead);   
       }
       else if(objLead.Branch__c=='Rajkot'){
      lstRajkotLead.add(objLead);   
       }
       else if(objLead.Branch__c=='Ahmedabad'){
      lstAhmedabadLead.add(objLead);    
       }
       else if(objLead.Branch__c=='Delhi'){
      lstDelhiLead.add(objLead);    
       }
       else if(objLead.Branch__c=='Head Office'){
      lstHOLead.add(objLead);   
       }
       else if(objLead.Branch__c=='Jaipur'){
      lstJaipurLead.add(objLead);   
       }
       else if(objLead.Branch__c=='Lucknow'){
      lstLkoLead.add(objLead);   
       }
       else if(objLead.Branch__c=='NSP'){
      lstNSPLead.add(objLead);    
       }
       else if(objLead.Branch__c=='Pune'){
      lstPuneLead.add(objLead);    
       }
       else if(objLead.Branch__c=='Udaipur'){
      lstUdaipurLead.add(objLead);     
       }
       
     }
       /*if(lstLead.size() > 0){ 
            LeadTriggerHelper.shareLeadsRoundRobin(lstLead, null);
          
        }*/
    if(lstUdaipurLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstUdaipurLead, null, 'Udaipur');
          
        }
    if(lstPuneLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstPuneLead, null, 'Pune');
          
        }
    if(lstNSPLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstNSPLead, null, 'NSP');
          
        }
    if(lstLkoLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstLkoLead, null, 'Lucknow');
          
        }
    if(lstJaipurLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstJaipurLead, null, 'Jaipur');
          
        }
    if(lstHOLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstHOLead, null, 'Head Office');
          
        }
    if(lstDelhiLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstDelhiLead, null, 'Delhi');
          
        }
    if(lstAhmedabadLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstAhmedabadLead, null, 'Ahmedabad');
          
        }
    if(lstRajkotLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstRajkotLead, null, 'Rajkot');
          
        }
    if(lstSuratLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstSuratLead, null, 'Surat');
          
        }
    if(lstVadodaraLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstVadodaraLead, null, 'Vadodara');
          
        }
    if(lstNashikLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstNashikLead, null, 'Nashik');
          
        }
    if(lstMumLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstMumLead, null, 'Mumbai');
          
        }
    
    /** // Added by Saumya For Partnership Lead **/
    }
    public static void beforeUpdate(List<Lead> newList,  List<Lead> oldList ,Map < Id, Lead > newMap , Map < Id, Lead > oldMap) {
    List<Lead> lstLead = new List<Lead>();
      List<Lead> lstAhmedabadLead = new List<Lead>();
    List<Lead> lstDelhiLead = new List<Lead>();
    List<Lead> lstHOLead = new List<Lead>();
    List<Lead> lstJaipurLead = new List<Lead>();
    List<Lead> lstMumLead = new List<Lead>();
    List<Lead> lstLkoLead = new List<Lead>();
    List<Lead> lstNashikLead = new List<Lead>();
    List<Lead> lstNSPLead = new List<Lead>();
    List<Lead> lstPuneLead = new List<Lead>();
    List<Lead> lstRajkotLead = new List<Lead>();
    List<Lead> lstSuratLead = new List<Lead>();
    List<Lead> lstUdaipurLead = new List<Lead>();
    List<Lead> lstVadodaraLead = new List<Lead>();
    List<Lead> lstLeadtoUpdate = new List<Lead>();
       //LeadTriggerHelper.validateNameDOB(newList, oldMap);
       //LeadTriggerHelper.checkCustomConvert(newList, oldMap);
       salesMapping(newList,oldMap); // Lead assignment will happen to queue now 
     /** // Added by Saumya For Partnership Lead **/
     for(Lead objLead: newList){
            if(objLead.Branch__c != oldMap.get(objLead.Id).Branch__c){
                lstLeadtoUpdate.add(objLead);
            }
        }
        if(lstLeadtoUpdate.size() >0){
     lstLead = roundRobin(lstLeadtoUpdate);
        }
         for(Lead objLead: lstLead){
       if(objLead.Branch__c=='Mumbai'){
      lstMumLead.add(objLead);     
       }
       else if(objLead.Branch__c=='Nashik'){
      lstNashikLead.add(objLead);   
       }
       else if(objLead.Branch__c=='Vadodara'){
      lstVadodaraLead.add(objLead);
         
       }
       else if(objLead.Branch__c=='Surat'){
      lstSuratLead.add(objLead);   
       }
       else if(objLead.Branch__c=='Rajkot'){
      lstRajkotLead.add(objLead);   
       }
       else if(objLead.Branch__c=='Ahmedabad'){
      lstAhmedabadLead.add(objLead);    
       }
       else if(objLead.Branch__c=='Delhi'){
      lstDelhiLead.add(objLead);    
       }
       else if(objLead.Branch__c=='Head Office'){
      lstHOLead.add(objLead);   
       }
       else if(objLead.Branch__c=='Jaipur'){
      lstJaipurLead.add(objLead);   
       }
       else if(objLead.Branch__c=='Lucknow'){
      lstLkoLead.add(objLead);   
       }
       else if(objLead.Branch__c=='NSP'){
      lstNSPLead.add(objLead);    
       }
       else if(objLead.Branch__c=='Pune'){
      lstPuneLead.add(objLead);    
       }
       else if(objLead.Branch__c=='Udaipur'){
      lstUdaipurLead.add(objLead);     
       }
       
     }
       /*if(lstLead.size() > 0){ 
            LeadTriggerHelper.shareLeadsRoundRobin(lstLead, null);
          
        }*/
    if(lstUdaipurLead.size() > 0){ 
      //LeadTriggerHelper.shareLeadsRoundRobin(lstUdaipurLead, null, 'Udaipur');
    }
    if(lstPuneLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstPuneLead, null, 'Pune');
        }
    if(lstNSPLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstNSPLead, null, 'NSP');
        }
    if(lstLkoLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstLkoLead, null, 'Lucknow');
        }
    if(lstJaipurLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstJaipurLead, null, 'Jaipur');
        }
    if(lstHOLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstHOLead, null, 'Head Office');
        }
    if(lstDelhiLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstDelhiLead, null, 'Delhi');
        }
    if(lstAhmedabadLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstAhmedabadLead, null, 'Ahmedabad');
        }
    if(lstRajkotLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstRajkotLead, null, 'Rajkot');
        }
    if(lstSuratLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstSuratLead, null, 'Surat');
          
        }
    if(lstVadodaraLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstVadodaraLead, null, 'Vadodara');
        }
    if(lstNashikLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstNashikLead, null, 'Nashik');
        }
    if(lstMumLead.size() > 0){ 
            //LeadTriggerHelper.shareLeadsRoundRobin(lstMumLead, null, 'Mumbai');
        }
       /*if(lstLead.size() > 0){ 
       LeadTriggerHelper.shareLeadsRoundRobin(lstLead, null);
       }*/
     /** // Added by Saumya For Partnership Lead **/
    }
    
   public static void afterInsert(List<Lead> newList) {
    /** // Added by Saumya For Partnership Lead **/
    List<Lead> ownerList = new List<Lead>();
    List<Lead> lstLead = new List<Lead>();
    Set<Id> LeadId = new Set<Id>();                 
		Map<String, Boolean> MapOfLeadATS = new Map<String, Boolean>();// Added by Saumya For Lead ATS send Filter						
        lstLead =roundRobin(newList);
 /**** Added by Saumya For Lead ATS send Filter ****/
		List<ATS_Lead_Source__c> lstATSLeadSource = [Select Name, IsActive__c from ATS_Lead_Source__c];
        for(ATS_Lead_Source__c objATS : lstATSLeadSource) {
            MapOfLeadATS.put(objATS.Name, objATS.IsActive__c);
            //LeadATSCallout.sendLeadDatatoATS(lstLead);
        }
        System.debug('MapOfLeadATS::'+MapOfLeadATS);				
    for(Lead objLead : lstLead) {
                System.debug('MapOfLeadATS.get(objLead.Source_Detail__c)::'+MapOfLeadATS.get(objLead.Source_Detail__c));
                if(MapOfLeadATS.get(objLead.Source_Detail__c) == true){
      LeadId.add(objLead.Id);
		}		 
      //LeadATSCallout.sendLeadDatatoATS(lstLead);
    }
	  /**** Added by Saumya For Lead ATS send Filter ****/														
    if(LeadId.size() > 0){
       for(Id objLeadid : LeadId) {
       LeadATSCallout.sendLeadDatatoATS(objLeadid);
       }		
    }
		  
        List<lead> lstLeadtoUpdate = new List<Lead>();
        
        String profileId = [Select Id, Name from Profile where Name=: 'Integration' LIMIT 1].Id;
        String UsertoAssignId = [Select Id, Name From User Where Name = 'Nikita Darshan' AND Profile.Name = 'Marketing Team' LIMIT 1].Id;
		String User2toAssignId = [Select Id, Name From User Where Name = 'Abhishek Sablok' AND Profile.Name = 'Marketing Team' LIMIT 1].Id;
		//String User3toAssignId = [Select Id, Name From User Where Name = 'Avni Chopra' AND Profile.Name = 'Sales Team' LIMIT 1].Id;//Added by Saumya for Lead Changes 27/11 
		String User3toAssignId = [Select Id, Name From User Where Name = 'Nikita Darshan' AND Profile.Name = 'Marketing Team' LIMIT 1].Id;// Modified by saumya from Avni Chopra to Nikita Darshan on 12 June 2020
        List<User> lstUsers = [SELECT Id, Name, Profile.Name from User WHERE isActive = true];
        System.debug('Debug Log for lstUsers'+lstUsers.size());
        Map<String, String> mapUserIdtoProfileId = new Map<String,String>();
        String digitalAllianceRecordTypeId = [Select Id, Name, DeveloperName from RecordType Where SObjectType = 'Lead' And DeveloperName = 'Digital_Alliance' LIMIT 1].ID;
		String digitalDirectRecordTypeId = [Select Id, Name, DeveloperName from RecordType Where SObjectType = 'Lead' And DeveloperName = 'Digital_Direct' LIMIT 1].ID;//Added by Saumya for Lead Changes 27/11
        for(User objUser : lstUsers) {
            mapUserIdtoProfileId.put(objUser.Id, objUser.Profile.Name);
        }
        
        for(Lead objLead : newList) {
      system.debug('obj.RecordType.Name::'+objLead.RecordType.Name);
           System.debug('Debug Log for Profile Name'+objLead.Owner);
            System.debug('Debug Log for Profile Id'+objLead.Owner);
            System.debug('>>mapUserIdtoProfileId.containsKey(objLead.OwnerId)'+mapUserIdtoProfileId.containsKey(objLead.OwnerId));
            System.debug('>>mapUserIdtoProfileId.get(objLead.OwnerId)'+mapUserIdtoProfileId.get(objLead.OwnerId));
            if(!mapUserIdtoProfileId.isEmpty() && mapUserIdtoProfileId.containsKey(objLead.OwnerId) && string.isNotBlank(mapUserIdtoProfileId.get(objLead.OwnerId))  && mapUserIdtoProfileId.get(objLead.OwnerId) == 'Integration' && UsertoAssignId != null && objLead.RecordTypeId == digitalAllianceRecordTypeId && String.isNotBlank(objLead.Source_Detail__c) && (objLead.Source_Detail__c == 'PaisaBazaar.com'|| objLead.Source_Detail__c == 'Lending adda')){//Added by Saumya for Lead Changes 27/11
                Lead newLead = new Lead();
                
                newLead.OwnerId = UsertoAssignId;
                newLead.Id = objLead.Id;
                lstLeadtoUpdate.add(newLead);
            }
 else if(!mapUserIdtoProfileId.isEmpty() && mapUserIdtoProfileId.containsKey(objLead.OwnerId) && string.isNotBlank(mapUserIdtoProfileId.get(objLead.OwnerId))  && mapUserIdtoProfileId.get(objLead.OwnerId) == 'Integration' && UsertoAssignId != null && objLead.RecordTypeId == digitalAllianceRecordTypeId && String.isNotBlank(objLead.Source_Detail__c) && (objLead.Source_Detail__c == 'My Money Karma' || objLead.Source_Detail__c == 'My Money Mantra')) {
                system.debug('I ma in Else');
                Lead newLead = new Lead();
                
                newLead.OwnerId = User2toAssignId;
                newLead.Id = objLead.Id;
                lstLeadtoUpdate.add(newLead);
            }
			 else if(!mapUserIdtoProfileId.isEmpty() && mapUserIdtoProfileId.containsKey(objLead.OwnerId) && string.isNotBlank(mapUserIdtoProfileId.get(objLead.OwnerId))  && mapUserIdtoProfileId.get(objLead.OwnerId) == 'Integration' && UsertoAssignId != null && objLead.RecordTypeId == digitalDirectRecordTypeId && String.isNotBlank(objLead.Source_Detail__c) && objLead.Source_Detail__c == 'Website') {//Added by Saumya for Lead Changes 27/11

                system.debug('I ma in Else 2');
                Lead newLead = new Lead();
                
                newLead.OwnerId = User3toAssignId;
                newLead.Id = objLead.Id;
                lstLeadtoUpdate.add(newLead);
            }
      /** // Added by Saumya For Partnership Lead **/

      if(objLead.OwnerId != NULL){            
                ownerList.add(objLead);
            } 
      /** // Added by Saumya For Partnership Lead **/

            
        }
        if(!lstLeadtoUpdate.isEmpty()) {
            update lstLeadtoUpdate;
        }
    /** // Added by Saumya For Partnership Lead **/

    if(ownerList.size() > 0){
            //LeadTriggerHelper.updateOwnerDate(ownerList);
        }
        LeadTriggerHelper.shareLeads(newList, null);
    /** // Added by Saumya For Partnership Lead **/

    }
    
    public static void afterUpdate(List<Lead> newList, Map<Id,Lead> oldMap) {
    List<Lead> ownerList = new List<Lead>();                    
        LeadTriggerHelper.shareLeads(newList, oldMap);
        List<Lead> lstLead = new List<Lead>();
        for(Lead obj:newList){
        system.debug('obj.RecordType.Name::'+obj.RecordType.Name);
            if(obj.OwnerId != NULL){
                ownerList.add(obj);
            }    
        }
            system.debug('ownerList::'+ownerList);
            if(ownerList.size() > 0){
                //LeadTriggerHelper.updateOwnerDate(ownerList);
            }
    }
    
    public static void salesMapping(List < Lead > newList, Map<Id, Lead> oldMap) {
         system.debug('@@newList' + newList);
         system.debug('@@oldMap' + oldMap);
        if ((newList[0].pincode__c != null && oldMap != null && newList[0].pincode__c != oldMap.get(newList[0].id).pincode__c)
            ||(newList[0].Property_pincode__c != null && oldMap != null && newList[0].Property_pincode__c != oldMap.get(newList[0].id).Property_pincode__c)
            ||(newlist[0].status == 'Nurturing' && oldMap.get(newList[0].id).status == 'New')
            ) {
           
            List < Lead > lstForFIFOAssignment = new List < Lead > ();
            Set < String > setOfPINCodes = new Set < String > ();
            map < string, list < lead >> leadPin = new map < string, list < lead >> ();
            Id idRTId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Digital_Direct').getRecordTypeId();
            Id idRTId2 = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Digital_Alliance').getRecordTypeId();
            
            
            
            Map<String,String> mapCityMetadata = new Map<String,String>();
            List<Lead_City_To_Sales_User__mdt> lstSalesMeta= [Select DeveloperName,MasterLabel, ProfileId__c from Lead_City_To_Sales_User__mdt];
            for ( Lead_City_To_Sales_User__mdt objMeta : lstSalesMeta) {
                mapCityMetadata.put(objMeta.MasterLabel,objMeta.ProfileId__c);
            }
            
            //salesuser   
            for (lead ld: newList) {
                if (ld.pincode__c != null && oldMap != null && ld.pincode__c != oldMap.get(ld.id).pincode__c) {
                
                    if (ld.RecordTypeId != idRTId && ld.RecordTypeId != idRTId2) {
                        if (leadPin.containsKey(ld.pincode__c)) {
                            leadPin.get(ld.pincode__c).add(ld);
                        } else {
                            list < lead > ldlist = new list < lead > ();
                            ldlist.add(ld);
                            leadPin.put(ld.pincode__c, ldlist);
                            if (String.isNotBlank(ld.PINCode__c)) {
                                setOfPINCodes.add(ld.PINCode__c);
                            }
                        }
                    } 
                } 
                
                if( (ld.Property_pincode__c != null && ld.status == 'Nurturing' && oldMap.get(ld.id).status == 'New') || 
                 (ld.Property_pincode__c != null && oldMap != null && ld.Property_pincode__c != oldMap.get(ld.id).Property_pincode__c) ) {
                   if ( ld.RecordTypeId == idRTId || ld.RecordTypeId == idRTId2 ) {
                        system.debug('ld.Property_City__c@@' + ld.Property_City__c);
                        if (mapCityMetadata.containsKey(ld.Property_City__c)) {
                            system.debug('@@'+ mapCityMetadata.get(ld.Property_City__c));
                            //ld.OwnerId = mapCityToUser.get(ld.Property_City__c);
                            ld.OwnerId = mapCityMetadata.get(ld.Property_City__c);
                        }
                    }
                }
            }
            if (!leadpin.isEmpty()) {
                system.debug('leadPin@@' + leadPin);
                //LeadTriggerHelper.FiFoAssignmentRule(setOfPINCodes, leadPin);
            }
        }
    }
  public static List<Lead> roundRobin(List < Lead > newList) {
       List<Lead> lstLead = new List<Lead>();

     Id ddId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Digital_Direct').getRecordTypeId();
       Id daId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Digital_Alliance').getRecordTypeId();
       Id hdCNTId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Hero_Dealer_CNT').getRecordTypeId();
       Id csId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Cross_Sell').getRecordTypeId();
       Id hd_DSAId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Hero_Dealer_DSA').getRecordTypeId();
       Id hdId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Hero_Fincorp_Dealer').getRecordTypeId();
       Id heId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Hero_Employees').getRecordTypeId();
                
       for(Lead obj:newList){

            if(obj.RecordTypeId == ddId || obj.RecordTypeId == daId || obj.RecordTypeId == hdCNTId || obj.RecordTypeId == csId || obj.RecordTypeId == hd_DSAId || obj.RecordTypeId == hdId || obj.RecordTypeId == heId){
            lstLead.add(obj);
            }
           
        }
        system.debug('lstLead::'+lstLead);
        return lstLead;
        
    }
    
}