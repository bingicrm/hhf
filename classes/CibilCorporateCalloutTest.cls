@isTest
private class CibilCorporateCalloutTest {
	
    static testmethod void checkCorporateCibilEligibilityTest(){
    	Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(Name='Test Account1',PAN__c='ABCDE1234F',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567890',RecordTypeId=RecordTypeIdAccount);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9234567891',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact cnt2 = new Contact (LastName = 'LNameLoanContact', AccountId = acc1.Id, MobilePhone = '9999667739');
        insert cnt2;
        
        Account acc2 = new Account(PAN__c='ABCDE1234H',Salutation='Mr',FirstName='Suresh',LastName='Nath',
                                   Date_of_Birth__c =Date.newInstance(1990,11,02),
                                   PersonMobilePhone='9234567892',PersonEmail='sn@gmail.com',RecordTypeId=RecordTypeIdAccount1);
        insert acc2;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert objLoanApplication;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Guarantor').getRecordTypeId();
        
        /*Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc1.Id,
                                                             Applicant_Type__c='Applicant',Borrower__c='1',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='18',
                                                             Customer_segment__c='9',Applicant_Status__c='1',
                                                             GSTIN_Available__c=true, Contact__c = cnt2.Id,
                                                             GSTIN_Number__c = '27AAHFG0551H1ZL',
                                                             Pan_Number__c='ASDFG1234L',
                                                             income_considered__c = false);
        insert objLoanContact; */
        Id RecordTypeIdLoanCon = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Salaried').getRecordTypeId();
        Loan_Contact__c objLoanCon = [SELECT ID FROM Loan_Contact__c LIMIT 1];
        objLoanCon.Borrower__c='2';
        objLoanCon.Pan_Number__c='ASDFG1234L';
        objLoanCon.Constitution__c='18';
        objLoanCon.pan_verification_status__c=true;
        update objLoanCon;
        
        Cross_Sell_Partner_Name__c CSPN = new Cross_Sell_Partner_Name__c(Name='Test');
        insert CSPN;
        
        DST_Master__c DSTMaster = new DST_Master__c(Name='Test DST',Inspector_ID__c='1234'); 
        insert DSTMaster;
        test.startTest();
        Sourcing_Detail__c sd= new Sourcing_Detail__c(Loan_Application__c=objLoanApplication.id,Source_Code__c='16',DST_Name__c=DSTMaster.id,Cross_Sell_Partner_Name__c=CSPN.id);
        insert sd;
        Loan_Application__c objLoanapp = [SELECT ID,StageName__c FROM Loan_Application__c LIMIT 1];
        objLoanapp.StageName__c='Operation Control';
        objLoanapp.Application_Form_Number__c = '123';
        update objLoanapp;
        
        City__c city = new City__c();
		city.Name = 'MUMBAI';
       	city.City_ID__c = '100';
        city.State__c='1';
        insert city;
        
        Pincode__c pin=new Pincode__c();
        pin.Name = 'Test';
        pin.Active__c=true;
        pin.City_Lp__c=city.Id;
        pin.ZIP_ID__c='909090';
        insert pin;
        
        Address__c obj=new Address__c();
        obj.Address_Line_1__c='TILAK NAGAR WEST';
        obj.Loan_Contact__c= objLoanCon.id;
        obj.Type_of_address__c='CUROFF';
        obj.Pincode_LP__c=pin.id;
        insert obj;
        
        CibilCorporateCallout.checkCorporateCibilEligibility(objLoanCon.id);
        CibilCorporateCallout.createCustomerIntegration(objLoanCon.id,true);
        CibilCorporateCallout.integrateCibil(objLoanCon.id,objLoanCon.id);
        CibilCorporateCallout.getAddressType('Both');
        CibilCorporateCallout.getAddressType('Residence Address');
        CibilCorporateCallout.getAddressType('Office/ Business');
        CibilCorporateCallout.getAddressType('Both1');
        CibilCorporateCallout.getStateCode('504');
        CibilCorporateCallout.getPicklistLabel('11');
        CibilCorporateCallout.getEntityType('18');
        CibilCorporateCallout.getEntityType('19');
        CibilCorporateCallout.getEntityType('21');
        CibilCorporateCallout.getEntityType('22');
        CibilCorporateCallout.getEntityType('23');
        test.stopTest();
        
        
    }
    
     static testmethod void checkCorporateCibilEligibilityTest1(){
    	//Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        Account acc1 = new Account(PAN__c='ABCDE1234H',Salutation='Mr',FirstName='Suresh',LastName='Nath',
                                   Date_of_Birth__c =Date.newInstance(1990,11,02),
                                   PersonMobilePhone='9234567892',PersonEmail='sn@gmail.com',RecordTypeId=RecordTypeIdAccount1);
        insert acc1; 
        Account acc2 = new Account(PAN__c='ABCDE1234I',Salutation='Mr',FirstName='Suresh1',LastName='Nath1',
                                   Date_of_Birth__c =Date.newInstance(1990,11,01),
                                   PersonMobilePhone='9234567812',PersonEmail='sn1@gmail.com',RecordTypeId=RecordTypeIdAccount1);
        insert acc2;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        //Contact cnt2 = new Contact (LastName = 'LNameLoanContact', AccountId = acc2.Id, MobilePhone = '9999667739');
        //insert cnt2;
         
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc2.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert objLoanApplication;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Co-Applicant').getRecordTypeId();
        
       
         
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=objLoanApplication.Id,Customer__c=acc1.Id,
                                                             Applicant_Type__c='Co- Applicant',Borrower__c='1',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='Son',
                                                             Pan_Verification_status__c=true,Constitution__c='20',
                                                             Customer_segment__c='1',Applicant_Status__c='1',
                                                             GSTIN_Available__c=true, //Contact__c = cnt2.Id,
                                                             GSTIN_Number__c = '27AAHFG0551H1ZL',
                                                             Pan_Number__c='ASDFG1234L',
                                                             income_considered__c = false,Gender__c='Male',Date_Of_Birth__c=system.today());
        insert objLoanContact;   
        /*Id RecordTypeIdLoanCon = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Salaried').getRecordTypeId();
        Loan_Contact__c objLoanCon = [SELECT ID FROM Loan_Contact__c LIMIT 1];
        objLoanCon.Borrower__c='2';
        objLoanCon.Pan_Number__c='ASDFG1234L';
        objLoanCon.Constitution__c='18';
        objLoanCon.pan_verification_status__c=true;
        update objLoanCon;*/
         
         Cross_Sell_Partner_Name__c CSPN = new Cross_Sell_Partner_Name__c(Name='Test');
         insert CSPN;
         
         DST_Master__c DSTMaster = new DST_Master__c(Name='Test DST',Inspector_ID__c='1234'); 
         insert DSTMaster;
         test.startTest();
         Sourcing_Detail__c sd= new Sourcing_Detail__c(Loan_Application__c=objLoanApplication.id,Source_Code__c='16',DST_Name__c=DSTMaster.id,Cross_Sell_Partner_Name__c=CSPN.id);
         insert sd;
        Loan_Application__c objLoanapp = [SELECT ID,StageName__c FROM Loan_Application__c LIMIT 1];
        objLoanapp.StageName__c='Operation Control';
        objLoanapp.Application_Form_Number__c = '123';
        update objLoanapp;
        
		Pincode__c pin=new Pincode__c();
        pin.Active__c=true;
        pin.City__c='JAIPUR';
        pin.ZIP_ID__c='909090';
        insert pin;
        
        Address__c obj=new Address__c();
        obj.Address_Line_1__c='TILAK NAGAR WEST';
        obj.Loan_Contact__c= objLoanContact.id;
        obj.Type_of_address__c='CUROFF';
        obj.Pincode_LP__c=pin.id;
        insert obj;
         
        Loan_Contact__c newId = [SELECT ID,Applicant_Type__c,Borrower__c FROM Loan_Contact__c where Applicant_Type__c!='Co- Applicant' LIMIT 1];  
        CibilCorporateCallout.integrateCibil(newId.id,objLoanContact.id);
        test.stopTest();
        
    }
}