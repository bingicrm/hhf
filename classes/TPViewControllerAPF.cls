public class TPViewControllerAPF {
    
    @AuraEnabled
    public static User getUserType(){
        System.debug('Test Debug Log');
        return [SELECT Id,Name,FI__c, Legal__c, Technical__c, FCU__c, LIP__c, PD__c from User WHERE Id =: UserInfo.getUserId()];
    }
    
    @AuraEnabled
    public static String getProfileName(){
        System.debug('Debug Log Test');
        return [SELECT Id,Name from Profile WHERE Id =: UserInfo.getProfileId()].Name;
    }
    
    @AuraEnabled
      public static List <Third_Party_Verification_APF__c> getTPV() {
        User u = [Select Id, Name, FI__c, Legal__c, Technical__c, FCU__c, LIP__c, PD__c,ProfileId from User where Id =: UserInfo.getUserId()]; //ProfileId added by Abhilekh on 19th September 2019 for FCU BRD
        System.debug('Debug Log for User'+u);
        List<Third_Party_Verification_APF__c> tpList = new List<Third_Party_Verification_APF__c>();
        List<Third_Party_Verification_APF__c> returnList = new List<Third_Party_Verification_APF__c>();
        
        System.debug('Debug Log for u'+u);
        
        Id LegalRecordTypeId = Schema.SObjectType.Third_Party_Verification_APF__c.getRecordTypeInfosByName().get(Constants.strRecordTypeTPVLegal).getRecordTypeId();
        Id TechnicalRecordTypeId = Schema.SObjectType.Third_Party_Verification_APF__c.getRecordTypeInfosByName().get(Constants.strRecordTypeTPVTechnical).getRecordTypeId();
        Id FCURecordTypeId = Schema.SObjectType.Third_Party_Verification_APF__c.getRecordTypeInfosByName().get(Constants.strRecordTypeTPVFCU).getRecordTypeId();
        
        String profileName = [SELECT Id,Name from Profile WHERE Id =: u.ProfileId].Name; //Added by Abhilekh on 19th September 2019 for FCU BRD
        System.debug('Debug Log for profileName'+profileName);
          if(u.FCU__c == TRUE){
              //Below if,else if and else block added by Abhilekh on 19th September 2019 for FCU BRD
              if(profileName == 'Third Party Vendor'){
                  tpList = [Select Id, Name,  Project__r.Name, Initiation_Date__c, Project__r.Branch__r.Name, Owner_Name__c, Status__c, Owner__c, RecordType.Name ,Project__c,Owner__r.Profile.Name from Third_Party_Verification_APF__c where RecordTypeId =: FCURecordTypeId AND Owner__c =: UserInfo.getUserId() ORDER BY Initiation_Date__c ASC];//Added by saumya Initiation_Date__c and Requested_Loan_Amount__c For Third Party view Change
                  //Loan_Application__c,Loan_Application__r.Name,Loan_Application__r.Overall_FCU_Status__c and Loan_Application__r.FCU_Status__c='Initiated',Status__c='Assigned',ORDER BY Initiation_Date__c ASC added by Abhilekh on 19th September 2019 for FCU BRD
                  returnList.addAll(tpList);    
              }
              else if(profileName == 'FCU Manager'){
                  tpList = [Select Id, Name,  Project__r.Name, Initiation_Date__c, Project__r.Branch__r.Name, Owner_Name__c, Status__c, Owner__c, RecordType.Name, Project__c,Owner__r.Profile.Name from Third_Party_Verification_APF__c where RecordTypeId =: FCURecordTypeId AND Owner__c =: UserInfo.getUserId() ORDER BY Initiation_Date__c ASC];
                  returnList.addAll(tpList);
              }
              else{
                  tpList = [Select Id, Name, Project__r.Name, Initiation_Date__c,  Project__r.Branch__r.Name, Owner_Name__c, Status__c, Owner__c, RecordType.Name, Project__c, Owner__r.Profile.Name from Third_Party_Verification_APF__c where RecordTypeId =: FCURecordTypeId AND Owner__c =: UserInfo.getUserId() ORDER BY Initiation_Date__c ASC];
                  returnList.addAll(tpList);
              }
          }
          if(u.Legal__c == TRUE){
              tpList = [Select Id, Name, Project__r.Name, Initiation_Date__c, Project__r.Branch__r.Name, Owner_Name__c, Status__c, Owner__c, RecordType.Name, Project__c, Owner__r.Profile.Name from Third_Party_Verification_APF__c where (RecordTypeId =: LegalRecordTypeId) AND Status__c = 'Initiated' AND Owner__c =: UserInfo.getUserId()];//Added by saumya Initiation_Date__c and Requested_Loan_Amount__c For Third Party view Change
              returnList.addAll(tpList);
          }
          
          if(u.Technical__c == TRUE){
              tpList = [Select Id, Name, Project__r.Name, Initiation_Date__c, Project__r.Branch__r.Name, Owner_Name__c, Status__c, Owner__c, RecordType.Name, Project__c, Owner__r.Profile.Name from Third_Party_Verification_APF__c where RecordTypeId =: TechnicalRecordTypeId AND Status__c = 'Initiated' AND Owner__c =: UserInfo.getUserId()];//Added by saumya Initiation_Date__c and Requested_Loan_Amount__c For Third Party view Change
              returnList.addAll(tpList);
          }
        System.debug('Debug Log for returnList'+returnList);
        return returnList;
      }
}