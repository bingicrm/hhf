public class PerfiosFinancialRetrieveResponseBody {

	public class FY {
		public String year;
		public ProfitAndLoss ProfitAndLoss;
		public BalanceSheet BalanceSheet;
	}

	public FinancialStatement FinancialStatement;

	public class FinancialStatement {
		public List<FY> FY;
		public String Organisation;
	}

	public class ProfitAndLoss {
		public Double Depreciation;
		public Double InterestPaid;
		public Double OtherIncomeNonbusinessIncome;
		public Double NonCashExpensesWrittenOff;
		public Double SalaryToPartnerDirector;
		public Double InterestExpensesAndRentPaidToPartnersDirector;
		public Double NetSalesNetOfExcise;
		public Double OtherIncomeIncidentalToBusinessBusinessIncome;
		public Double Tax;
		public Double Wages;
		public Double ManufacturingExpenses;
		public Double RawMaterialCost;
		public Double AdministrativeAndSellingAndDistributionExpenses;
	}

	public class Liabilities {
		public Double ShareCapital;
		public Double ReservesSurplusExcludingRevaluationReserve;
		public Double RevaluationReserve;
		public Double TermLoansFromBanksFi;
		public Double UnsecuredLoansFromPartnersShareholders;
		public Double TradeCreditors;
		public Double UnsecuredLoansOthers;
		public Double WorkingCapitalLimitsFromBanksFiS;
		public Double OtherCurrentLiabilitiesProvisions;
	}

	public class Assets {
		public Double CashAndBank;
		public Double Inventories;
		public Double LoansAdvancesGivenToDirectorsPartnersEtc;
		public Double DebtorsLt6Months;
		public Double MiscExpensesDrePreopPreliminaryAccPL;
		public Double GroupCoInvestments;
		public Double DebtorsGt6Months;
		public Double LiquidMarketableInvestments;
		public Double FixedAssetsLessDepreciation;
		public Double UnquotedDeadInvestments;
		public Double ShortTermLoansAndAdvancesGivenToOthers;
		public Double NonCurrentLoansAndAdvances;
	}

	public class BalanceSheet {
		public Liabilities Liabilities;
		public Assets Assets;
	}

	
	public static PerfiosFinancialRetrieveResponseBody parse(String json) {
		return (PerfiosFinancialRetrieveResponseBody) System.JSON.deserialize(json, PerfiosFinancialRetrieveResponseBody.class);
	}
}