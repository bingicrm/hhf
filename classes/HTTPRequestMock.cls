@isTest
global class HTTPRequestMock implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest req) {
     
 
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(JSON.serialize('{"userId": "admin","unqRequestId": "40717254313","sourceSystem": "SFDC-HFL","password": "admin",    "disbursalDetail": {"repaymentDate": "08/05/2019","principalRecoveryFlag": "D","paymentDetails": [{ "remarks": "NA","paymentMode": "Q","inFavourOf": "lokesh","ifscCode": "","ftMode": "","effectiveDate": "11/05/2019","disbursalTo": "LS","disbrsmntBank": "","creditPeriod": "0","createdBy":"PRABHAT","city": "1","chequeNo": "546543","chequeDate": "11/05/2019","bpID": "1001215","amount": "420000","accountNo": "987987987970"}],"disbusalDesc":  "3rd tranche","disbursalDate":"11/05/2019","disbursalAmount":"420000","applicationID": "1003949","agreementNo": ""}}'));
        res.setStatusCode(200);
        return res;
    }
 
}