public class BusinessDateTriggerHandler{
   @future
   public static void processAccountDatachievement(){
       List<LMS_Date_Sync__c> dateList = new List<LMS_Date_Sync__c>();
       dateList = [Select Id, Current_Date__c from LMS_Date_Sync__c ORDER BY CreatedDate DESC limit 1];
       List<Achievement__c> lstofAchievements = [Select Id, M_2__c, FTD__c, Business_Date__c, Month_Value__c, Month_Difference__c from Achievement__c];
       List<Achievement__c> lstofAchievementstoUpdate = new List<Achievement__c>();
       for(Achievement__c ach : lstofAchievements){
           if(ach.Month_Difference__c == 2 || ach.Month_Difference__c == -2){
              ach.M_2__c = TRUE; 
              if(!lstofAchievementstoUpdate.contains(ach)){
              lstofAchievementstoUpdate.add(ach);
              }
           }
           if(ach.Month_Difference__c == 0){
            if(dateList[0].Current_Date__c.addDays(1) == ach.Business_Date__c){
                ach.FTD__c = TRUE;
                ach.M_2__c = FALSE;
              if(!lstofAchievementstoUpdate.contains(ach)){
              lstofAchievementstoUpdate.add(ach);
              }
            }
            else if(dateList[0].Current_Date__c.addDays(1) != ach.Business_Date__c && ach.FTD__c == TRUE){
                ach.FTD__c = FALSE;
              if(!lstofAchievementstoUpdate.contains(ach)){
              lstofAchievementstoUpdate.add(ach);
              }
            }
           }
           if(ach.Month_Difference__c == 1 || ach.Month_Difference__c == -1){
              if(!lstofAchievementstoUpdate.contains(ach)){
              lstofAchievementstoUpdate.add(ach);
              }
           }
           
           if(ach.Month_Difference__c != 2 || ach.Month_Difference__c != -2){
                ach.M_2__c = FALSE;  
                if(!lstofAchievementstoUpdate.contains(ach)){
                 lstofAchievementstoUpdate.add(ach);
                }
           }
           
       }
       
       if(lstofAchievementstoUpdate.size()>0){
            database.update(lstofAchievementstoUpdate);
        }
   }
    public static void checkM2(){
        List<Achievement__c> achList = new List<Achievement__c>();
        List<Achievement__c> achToUpdate = new List<Achievement__c>();
        achList = [Select Id, M_2__c, Business_Date__c, Month_Value__c, Month_Difference__c from Achievement__c where Month_Difference__c = 2 OR Month_Difference__c = -2];
        for(Achievement__c ach : achList){
            ach.M_2__c = TRUE;
            achToUpdate.add(ach);
        }
        system.debug('--M-2 SIZE--:'+achToUpdate.size());
        if(achToUpdate.size()>0){
            database.update(achToUpdate);
        }
    }
    public static void checkFTD(List<LMS_Date_Sync__c> busList){
        LMS_Date_Sync__c bDate = busList[0];
        List<Achievement__c> achFTDList = new List<Achievement__c>();
        List<Achievement__c> achFTDUpdate = new List<Achievement__c>();
        achFTDList = [Select Id, M_2__c, FTD__c, Business_Date__c, Month_Value__c, Month_Difference__c from Achievement__c where Month_Difference__c = 0];
        for(Achievement__c ach : achFTDList){
            if(bDate.Current_Date__c.addDays(1) == ach.Business_Date__c){
                ach.FTD__c = TRUE;
                ach.M_2__c = FALSE;
                achFTDUpdate.add(ach);
            }
            else if(bDate.Current_Date__c.addDays(1) != ach.Business_Date__c && ach.FTD__c == TRUE){
                ach.FTD__c = FALSE;
                achFTDUpdate.add(ach);
            }
        }
        system.debug('--FTD SIZE--:'+achFTDUpdate.size());
        if(achFTDUpdate.size()>0){
            database.update(achFTDUpdate);
        }
    }
    public static void dummyUpdate(){
        List<Achievement__c> achLMTDList = new List<Achievement__c>();
        achLMTDList = [Select Id, Month_Difference__c from Achievement__c where Month_Difference__c = 1 OR Month_Difference__c = -1];
        system.debug('--M-1 SIZE to Trigger Workflow for LMTD Days Gap--:'+achLMTDList.size());
        if(achLMTDList.size()>0){
            database.update(achLMTDList);
        }
    }
    public static void uncheckM2(){
        List<Achievement__c> achNewList = new List<Achievement__c>();
        List<Achievement__c> achUncheck = new List<Achievement__c>();
        achNewList = [Select Id, M_2__c, Business_Date__c, Month_Value__c, Month_Difference__c from Achievement__c where Month_Difference__c != 2 AND Month_Difference__c != -2];
        for(Achievement__c ach : achNewList){
            ach.M_2__c = FALSE;
            achUncheck.add(ach);
        }
        system.debug('--Uncheck FTD, M-2 SIZE--:'+achUncheck.size());
        if(achUncheck.size()>0){
            database.update(achUncheck);
        }
    }
}