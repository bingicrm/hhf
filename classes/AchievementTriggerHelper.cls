public class AchievementTriggerHelper {

    public static void EvaluateConsiderforInsurance(list < Achievement__c > newList) {
    
        for(Achievement__c objAch: newList){
            if(objAch.Active__c == true && (objAch.Insurance__c == true || (objAch.Approved_Cross_Sell__c > 0 && objAch.Insurance_Loan_Created__c == false))){
                objAch.Consider_for_Insurance__c = true;
                system.debug('objAch.Consider_for_Insurance__c:::'+objAch.Consider_for_Insurance__c);
            }
        }
        
    }
}