@isTest(SeeAllData = false)

public class TestBlacklistingRemovalController {
public static testMethod void APFExposureControllerChek() {
      
        
      List<RecordType> lstRecordType = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Individual'];
        Id builderIndvRecTypeId;
        if(!lstRecordType.isEmpty()) {
            builderIndvRecTypeId = lstRecordType[0].Id;
        }
        
        Account acc = new Account();
            acc.RecordTypeId = builderIndvRecTypeId;
            acc.Name = 'Individual Test Acc';
            acc.PAN__c = 'AAAAA3333B';
            acc.GSTIN__c = '27AAHFG0551H1ZL';
        insert acc;
        
        List<RecordType> lstRecordType2 = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Corporate'];
        Id builderCorpRecTypeId;
        if(!lstRecordType2.isEmpty()) {
            builderCorpRecTypeId = lstRecordType2[0].Id;
        }
        
        List<RecordType> lstIndividualPBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Individual'];
        List<RecordType> lstCorporatePBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Corporate'];
        
        Account acc2 = new Account();
            acc2.RecordTypeId = builderCorpRecTypeId;
            acc2.Name = 'Test Corporate Account';
            acc2.PAN__c = 'AAAAB2222B';
            
        insert acc2;
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83');
        insert objPincode;
        
        Document_Master__c docMasObj = new Document_Master__c();
        docMasObj.Name = 'Test GSTIN - Registration certificate';
        docMasObj.Doc_Id__c = '123344';
        docMasObj.Approving_authority__c = 'Business Head';
        docMasObj.Applicable_for_APF__c = true;
        Database.insert(docMasObj);
        
        Document_type__c docTypeObj = new Document_type__c();
        docTypeObj.Name = 'Builder - APF';
        docTypeObj.Approving_authority__c    = 'Business Head';
         docTypeObj.Applicable_for_APF__c = true;  
        Database.insert(docTypeObj);
        
        APF_Document_Checklist__c objAPF = new APF_Document_Checklist__c();
        objAPF.Active_del__c = true;
        objAPF.Document_Master__c = docMasObj.Id;
        objAPF.Document_Type__c = docTypeObj.id;
        objAPF.Mandatory__c = true;
        objAPF.Valid_for_Project_Type__c = 'New';
        insert objAPF;
        
        Project__c objProject = new Project__c();
        //objProject.Assigned_Sales_User__c = assignedSalesUser != null ? assignedSalesUser.Id : null;
        objProject.Account__c = acc.Id;
        objProject.Project_Name__c = 'Test Project';
        objProject.APF_Status__c = 'APF Initiated';
        objProject.Stage__c = 'APF Initiation';
        objProject.Sub_Stage__c = 'Project Data Capture';
        objProject.APF_Type__c = 'New';
        objProject.Address_Line_1__c = 'Test Address';
        objProject.Pincode__c = objPincode.Id;
        insert objProject;
        
        Project_Builder__c objProjectBuilderCorp = new Project_Builder__c();
        objProjectBuilderCorp.RecordTypeId = lstCorporatePBId[0].Id;
        objProjectBuilderCorp.Builder_Name__c = acc2.Id;
        objProjectBuilderCorp.Constitution__c = '24';
        objProjectBuilderCorp.Builder_Status__c = 'Resident';
        objProjectBuilderCorp.Contact_Number_Mobile__c = '9999444223';
        objProjectBuilderCorp.PAN_Number__c = 'EGNPS1111T';
        objProjectBuilderCorp.Project__c = objProject.Id; 
        objProjectBuilderCorp.Preferred_Communication_Address__c = 'CURRES'; 
        //objProjectBuilderCorp. =
        Database.insert(objProjectBuilderCorp,false);
        
        acc2.GSTIN__c = '27AAHFG0551H1ZL';
        update acc2;
        
        insert objProjectBuilderCorp;
        System.debug('Debug Log for GSTIN'+objProjectBuilderCorp);
        
        
        objProjectBuilderCorp.GSTIN_Number__c = '27AAHFG0551H1ZL';
        //objProjectBuilderCorp.Builder_Role__c = Constants.strPBConstructorMarketer;
        update objProjectBuilderCorp;
        
        Set<Id> setProjectId = new Set<Id>();
        setProjectId.add(objProject.Id);
        
        
        
        List<Project_Builder__c> lstProjectBuilder = [SELECT Id, RecordTypeId, Builder_Category__c, GSTIN_Available__c, Name, Date_Of_Birth__c, Builder_Name__r.RecordType.DeveloperName, All_Promoter_Count__c, Builder_Type__c, Class_Of_Activity__c, Builder_Name__c, Builder_Name__r.RecordTypeId, Builder_Status__c, Constitution__c, Builder_Role__c, Contact_Number_Mobile__c, Email__c, Preferred_Communication_Address__c, Address_Line_1__c, Address_Line_2__c, Pincode__c, Promoter_PAN_Validation_Count__c, Promoter_Cibil_Validation_Count__c, Project__c,(Select Id, Name, Name_of_Promoter__c, Email__c, Contact_No_Mobile__c, Preferred_Communication_Address__c, Promoter_Status__c, Address_Line_1__c, Address_Line_2__c, Pincode__c from Promoters__r) From Project_Builder__c Where Project__c =: objProject.Id];
        System.debug('Debug Log for lstProjectBuilder size'+lstProjectBuilder.size());
        
        List<Promoter__c> lstPromoter = new List<Promoter__c>();
        if(!lstProjectBuilder.isEmpty()) {
            for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                objProjectBuilder.GSTIN_Available__c = true;
                objProjectBuilder.GSTIN_Number__c = '27AAHFG0551H1ZL';
                if(objProjectBuilder.Promoters__r.isEmpty() && objProjectBuilder.Builder_Name__r.RecordType.DeveloperName != 'Builder_Individual'){
                    Promoter__c objPromoter = new Promoter__c(Project_Builder__c = objProjectBuilder.Id, Preferred_Communication_Address__c = 'CURRES');
                    lstPromoter.add(objPromoter);
                }
            }
            update lstProjectBuilder;
            System.debug('Debug Log for lstPromoter'+lstPromoter.size());
            if(!lstPromoter.isEmpty()) {
                insert lstPromoter;
            }
            System.debug('Debug Log for lstPromoter post insertion from Address Check Method'+lstPromoter);
            update lstPromoter;
            
            Pincode__c pin=new Pincode__c(Name='TEST',City__c='TEST',ZIP_ID__c='TEST');
                  Database.insert(pin);
            
            Address_Detail_Promoter__c objAddressDetail = new Address_Detail_Promoter__c(Type_of_address__c = lstPromoter[0].Preferred_Communication_Address__c,
                                                                        Residence_Type__c = 'C', Pincode_LP__c = pin.Id, Address_Line_1__c = 'Test Address Line 1',
                                                                        Address_Line_2__c = 'Test Address Line 2', Promoter__c = lstPromoter[0].Id);
                                                                        
            insert objAddressDetail;
            
            objProject.RecordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get(Constants.strRecordTypeCreditApproval).getRecordTypeId();
            objProject.Stage__c = 'APF Inventory Maintenance';
            objProject.Sub_Stage__c = 'APF Inventory Update';
            objProject.APF_Status__c = 'APF Approved';
            update objProject;
            
            Tower__c objTower = new Tower__c(Project__c = objProject.Id, Name = 'Villa 1');
            insert objTower;
            
            Project_Inventory__c objProjectInventory = new Project_Inventory__c(Project__c = objProject.Id, Tower__c = objTower.Id, Flat_House_No__c = '123', 
                                                                                Floor__c = 'Second Floor', LCR_per_sq_ft__c = 100, Area__c = 500, 
                                                                                Construction_Status__c = 'Under Construction',
                                                                                Inventory_Description__c = '1',Inventory_Status__c = 'Available',
                                                                                Type_of_Inventory__c = 'Residential');
            insert objProjectInventory;
            
            objProject.APF_Status__c= 'APF Inventory Approved';
            update objProject;
            
            objProjectInventory.Inventory_Approval_Status__c = 'Approved';
            objProjectInventory.Flat_House_No__c = '123B';
            update objProjectInventory;
            
			
            /*Scheme__c sch1=new Scheme__c();
            sch1.Product_Code__c='HL'; 
            sch1.Scheme_Code__c='EL';
            sch1.Scheme_Group_ID__c='Test1';
            insert sch1;*/
            
            Scheme__c sc= new Scheme__c(Name='Home Loan',Scheme_Code__c='HL',Scheme_Group_ID__c='5',
                                     Scheme_ID__c=11,Product_Code__c='HL',Max_ROI__c=10,Min_ROI__c=5,
                                     Min_Loan_Amount__c=10000,Min_FOIR__c=1,
                                     Max_Loan_Amount__c=3000001, Max_Tenure__c=5, Min_Tenure__c=1,
                                     Max_LTV__c=5,Min_LTV__c=1,Max_FOIR__c=5);
                            
         Database.insert(sc);
            
			Loan_Application__c objLA = new Loan_Application__c();
            objLA.Scheme__c = sc.Id;
			objLA.StageName__c= 'Customer Onboarding';
			objLA.Sub_Stage__c= 'Application Initiation';
			
			insert objLA;
            Test.startTest();
			objLA.APF_Loan__c= true;
            objLA.APF_Name__c = objProject.Id;
			objLA.Project_Inventory__c = objProjectInventory.Id;
            objLA.APF_Exposure_Initiated__c= true;
            update objLA;
            BlacklistingRemovalController.checkEligibility(objProject.Id);
            BlacklistingRemovalController.moveprevious(objProject.Id, 'Test');
            Test.stopTest();
           
        }
  }
}