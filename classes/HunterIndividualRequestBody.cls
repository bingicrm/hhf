public class HunterIndividualRequestBody{
	public String sClassification;
	public String sSubmissionDate;
	public String sApplicationDate;
	public String sIdentifier;
	public String sLoanPurpose;
	public String sTerm;
	public String sValue;
	public String sAssetOrignalValue;
	public String sAssetValuation;
	public String sInternalFlag;
	public String sPriorityScore;
	public String sBranchRegion;
	public String sProduct;
	//public String sFanNo;
	public OMainApplicant oMainApplicant;
	public List<OJointApplicant> oJointApplicant;
	public List<OReference> oReference;
	//public OBroker oBroker;
	//public OVehicle oVehicle;

	/*public class OVehicle {
		public String sEngineNo;
		public String sChassisNo;
		public String sRegistrationNo;
		public String sModel;
	}*/

	public class OEmployerContact {
		public String sTelephoneNo;
		public String sExtensionNo;
	}
	
    public class cls_oMainApplicationResidentialAddress {
		public String sAddressLines;	//HSBC BANK INDIA PENNINSULA TIDLE PARK OPP.HANGLA LOWER PAREL TAMBARAM  TAMBARAM
		public String sCity;	//TAMBARAM
		public String sState;	//Tamil Nadu
		public String sCountry;	//India
		public String sPincode;	//600045
		public String sTimeAtAddress;
		public String sPropertyType;
		public String sResidentialStatus;
	}
    
	public class OMainApplicationPermanentAddress {
		public String sAddressLines;
		public String sCity;
		public String sState;
		public String sCountry;
		public String sPincode;
		public String sTimeAtAddress;
		public String sPropertyType;
		public String sResidentialStatus;
        
        public OMainApplicationPermanentAddress(){
            this.sAddressLines = '';
            this.sCity = '';
            this.sState = '';
            this.sCountry = '';
            this.sPincode = '';
            this.sTimeAtAddress = '';
            this.sPropertyType = '';
            this.sResidentialStatus = '';
        }
	}
    
    public class OMainApplicationPropertyAddress {
        public String sAddressLines;
        public String sCity;
        public String sState;
        public String sCountry;
        public String sPincode;
        
        public OMainApplicationPropertyAddress(){
            this.sAddressLines = '';
            this.sCity = '';
            this.sState = '';
            this.sCountry = '';
            this.sPincode = '';
        }
    }

	public class OBrokerAddress {
		public String sAddressLines;
		public String sCity;
		public String sState;
		public String sCountry;
		public String sPincode;
		public String sTimeAtAddress;
	}

	public class OReference {
		public String sFirstName;
		public String sLastName;
		public OMainApplicationPreviousAddress oRefResidentialAddr;
		public OHomeTelephone oHomeTelephone;
		public OHomeTelephone oMobileTelephone;
	}

	public class OJointApplicant {
		public String sPanNumber;
		public String sFirstName;
		public String sMiddleName;
		public String sLastName;
		public String sDOB;
		public String sAge;
		public String sGender;
		public String sQualification;
		public String sMaritalStatus;
		public String sIncome;
		public String sNationality;
		public cls_oJointApplicationResidentialAddress oJointApplicationResidentialAddress;
		//public List<OIdJointApplicantDocument> oIdJointApplicantDocument;
	}

	/*public class OBroker {
		public String sOrgName;
		public String sOrgCode;
		public String sRepName;
		public OBrokerAddress oBrokerAddress;
		public String sBrokerNumber;
	}*/
	
    public class cls_oJointApplicationResidentialAddress {
        public String sAddressLines;	//Mayfair Towers Old Mumbai Pune Hwy
		public String sCity;	//PUNE
		public String sState;	//MAHARASHTRA
		public String sCountry;	//INDIA
		public String sPincode;	//411005
		public String sTimeAtAddress;	//2
		public String sPropertyType;	//LAND
		public String sResidentialStatus;	//OTHER
    }
    
	public class OMainApplicationPreviousAddress {
		public String sAddressLines;
		public String sCity;
		public String sState;
		public String sCountry;
		public String sPincode;
		public String sTimeAtAddress;
		public String sPropertyType;
		public String sResidentialStatus;
	}

	public class OEmployer {
		public String sOrgName;
		public String sIndustry;
		public String sJobTitle;
		public String sEmpNo;
		public String sEmpStartDate;
		public String sTimeWithEmployment;
		public String sEmployeeStatus;
		public OMainApplicationPreviousAddress oEmployerAddr;
		public List<OEmployerContact> oEmployerContact;
	}

	public class OBank {
		public String sBankName;
		public String sAccountNumber;
		public String sBranchName;
		public String sMicrCode;
        public OBank(){
            this.sBankName ='';
		    this.sBranchName='';
			this.sMicrCode='';
            this.sAccountNumber='';
        }
	}
    
    public class OEmail {
		public String sEmailAddress;
		public String sUserAddress;
		public String sDomain;
	}

	public class OHomeTelephone {
		public String sTelNo;
	}

	/*public class OIdJointApplicantDocument {
		public String sDocumentType;
		public String sDocumentNumber;
		public String sDocAccountNumber;
		public String sPlaceOfIssue;
		public String sCountryOfIssue;
		public String sIssueDate;
		public String sExpiryDate;
		public String sSubmissionDate;
		public String sEReceipt;
		public String sFatherHusbandName;
		public String sAckNumber;
		public String sCompanyTAN;
		public String sIdDOB;
	}*/

	/*public class OIdMainApplicantDocument {
		public String sDocumentType;
		public String sDocumentNumber;
		public String sDocAccountNumber;
		public String sPlaceOfIssue;
		public String sCountryOfIssue;
		public String sIssueDate;
		public String sExpiryDate;
		public String sSubmissionDate;
		public String sEReceipt;
		public String sFatherHusbandName;
		public String sAckNumber;
		public String sCompanyTAN;
		public String sIdDOB;
	}*/

	public class OMainApplicant {
		public String sPanNumber;
		public String sFirstName;
		public String sMiddleName;
		public String sLastName;
		public String sDOB;
		public String sAge;
		public String sGender;
		public String sQualification;
		public String sMaritalStatus;
		public String sIncome;
		public String sNationality;
		public cls_oMainApplicationResidentialAddress oMainApplicationResidentialAddress;
		public OMainApplicationPermanentAddress oMainApplicationPermanentAddress;
		//public List<OMainApplicationPreviousAddress> oMainApplicationPreviousAddress;
		public OMainApplicationPropertyAddress oMainApplicationPropertyAddress;
		public OHomeTelephone oHomeTelephone;
		public OHomeTelephone oMobileTelephone;
		public OHomeTelephone oBusinessTelephone;
		public List<OEmail> oEmail;
		public OBank oBank;
		//public List<OIdMainApplicantDocument> oIdMainApplicantDocument;
		public List<OEmployer> oEmployer;
        
        public OMainApplicant(){
            this.sPanNumber = '';
            this.sFirstName = '';
            this.sMiddleName = '';
            this.sLastName = '';
            this.sDOB = '';
            this.sAge = '';
            this.sGender = '';
            this.sQualification = '';
            this.sMaritalStatus = '';
            this.sIncome = '';
            this.sNationality = '';
            this.oMainApplicationResidentialAddress = new cls_oMainApplicationResidentialAddress();
            this.oMainApplicationPermanentAddress = new OMainApplicationPermanentAddress();
            //public List<OMainApplicationPreviousAddress> oMainApplicationPreviousAddress;
            this.oMainApplicationPropertyAddress = new OMainApplicationPropertyAddress();
            this.oHomeTelephone = new OHomeTelephone();
            this.oMobileTelephone = new OHomeTelephone();
            this.oBusinessTelephone = new OHomeTelephone();
            this.oEmail = new List<OEmail>();
            this.oBank = new OBank();
            this.oEmployer = new List<OEmployer>();
        }
	}
}