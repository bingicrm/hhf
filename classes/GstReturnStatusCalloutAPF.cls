public class GstReturnStatusCalloutAPF implements Queueable, Database.AllowsCallouts { 
    public List<Builder_Integrations__c> lstBuilderIntegrations;
    public List<Promoter_Integrations__c> lstPromoterIntegrations;
    public GstReturnStatusCalloutAPF(List<Builder_Integrations__c> lstBuilderIntegrations, List<Promoter_Integrations__c> lstPromoterIntegrations){
        this.lstBuilderIntegrations = lstBuilderIntegrations;
        this.lstPromoterIntegrations = lstPromoterIntegrations;
    }
    
     public void execute(QueueableContext context) {
         if(lstBuilderIntegrations != null && !lstBuilderIntegrations.isEmpty()) {
            for(Builder_Integrations__c objBI : lstBuilderIntegrations){ 
                 if(String.isNotBlank(objBI.GSTIN__c)){
                    makeGstReturnStatus(objBI.GSTIN__c, objBI.Project_Builder__c, objBI.Id); 
                 }
             }
         }
         else if(lstPromoterIntegrations != null && !lstPromoterIntegrations.isEmpty()) {
            for(Promoter_Integrations__c objPI : lstPromoterIntegrations){ 
                 if(String.isNotBlank(objPI.GSTIN__c)){
                    makeGstReturnStatus(objPI.GSTIN__c, objPI.Promoter__c, objPI.Id); 
                 }
             }
         }
       
         System.debug('Call to GstGenCreditLinkCallout');
         if(lstBuilderIntegrations != null && !lstBuilderIntegrations.isEmpty() && !Test.isRunningTest()) { //!Test.isRunningTest() applied to overcome System.AsyncException: Maximum stack depth has been reached.
             System.debug('Debug Log for lstBuilderIntegrations Call to GstGenCreditLinkCallout'+lstBuilderIntegrations);
             System.enqueueJob(new GstGenCreditLinkCalloutAPF(lstBuilderIntegrations,null));
         }
         if(lstPromoterIntegrations != null && !lstPromoterIntegrations.isEmpty()  && !Test.isRunningTest()) {
             System.debug('Debug Log for lstPromoterIntegrations Call to GstGenCreditLinkCallout'+lstPromoterIntegrations);
             System.enqueueJob(new GstGenCreditLinkCalloutAPF(null,lstPromoterIntegrations));
         }
         //System.enqueueJob(new GstGenCreditLinkCalloutAPF(custIntegrationRecords));
     }
         
    @future(callout=true)
    public static void makeGstReturnStatus(String strGST , String strRecordId , String strRecordIntegrationLogId ) {
        if(String.isNotBlank(strGST)) {
            String sObjectName = Id.valueOf(strRecordId).getSObjectType().getDescribe().getName();
            System.debug('Debug Log for sObjectName'+sObjectName);
            List<Rest_Service__mdt> lstRestService = [SELECT 
                                                            MasterLabel, 
                                                            QualifiedApiName, 
                                                            Service_EndPoint__c, 
                                                            Client_Password__c, 
                                                            Client_Username__c, 
                                                            Request_Method__c, 
                                                            Time_Out_Period__c,
                                                          org_id__c
                                                      FROM   
                                                            Rest_Service__mdt
                                                      WHERE  
                                                            DeveloperName = 'GST_3_GstReturnStatus'
                                                            LIMIT 1
                                                     ]; 
            System.debug('Debug Log for lstRestService GST_3_GstReturnStatus'+lstRestService);
            if(!lstRestService.isEmpty()) {
                GstReturnStatusResponseBody objResponseBody = new GstReturnStatusResponseBody();
                Blob headerValue = Blob.valueOf(lstRestService[0].Client_Username__c + ':' + lstRestService[0].Client_Password__c);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                Map<String,String> headerMap = new Map<String,String>();
                headerMap.put('Authorization',authorizationHeader);
                headerMap.put('Username',lstRestService[0].Client_Username__c);
                headerMap.put('Password',lstRestService[0].Client_Password__c);
                headerMap.put('Content-Type','application/json');
                headerMap.put('org-id',lstRestService[0].org_id__c);
                
                RequestBody objRequestBody = new RequestBody();
                    objRequestBody.consent = 'Y';
                  objRequestBody.pdfOutputType = '';
                    objRequestBody.gstin = strGST != null ? strGST : '';
                    
                System.debug('Debug Log for objRequestBody'+objRequestBody);
                String jsonRequest = Json.serializePretty(objRequestBody); 
                
                Http objHttp = new Http();
                HttpRequest objHttpRequest = new HttpRequest();
                
                
                if(String.isNotBlank(lstRestService[0].Service_EndPoint__c)) {
                    objHttpRequest.setEndPoint(lstRestService[0].Service_EndPoint__c);
                }
                else {
                    System.debug('Debug Log for error : Endpoint Empty. Please specify endpoint in metadata record.');
                }
                if(String.isNotBlank(lstRestService[0].Request_Method__c)) {
                    objHttpRequest.setMethod(lstRestService[0].Request_Method__c);
                }
                else {
                    System.debug('Debug Log for error : Requested Method Empty. Please specify Requested Method in metadata record.');
                }
                
                for(String headerKey : headerMap.keySet()){
                    objHttpRequest.setHeader(headerKey, headerMap.get(headerKey));
                    System.debug('Debug Log for headerKey'+headerKey);
                    System.debug('Debug Log for headerMap.get(headerKey)'+headerMap.get(headerKey));
                }
                objHttpRequest.setBody(jsonRequest);
                if(String.isNotBlank(String.valueOf(lstRestService[0].Time_Out_Period__c))) {
                    objHttpRequest.setTimeOut(Integer.valueOf(lstRestService[0].Time_Out_Period__c));
                }
                System.debug('Debug Log for request'+objHttpRequest);
                try {
                    HTTPResponse httpRes = objHttp.send(objHttpRequest);
                    //Create integration logs
                    //LogUtility.createIntegrationLogs(jsonRequest,httpRes,lstRestService[0].Service_EndPoint__c);
                    System.debug('Debug Log for response'+httpRes);
                    System.debug('Debug Log for response body'+httpRes.getBody());
                    System.debug('Debug Log for response status'+httpRes.getStatus());
                    System.debug('Debug Log for response status code'+httpRes.getStatusCode());
                    //Deserialization of response to obtain the result parameters
                    //objResponseBody = (ResponseBody)JSON.deserialize(httpRes.getBody(), ResponseBody.class);
                    objResponseBody = (GstReturnStatusResponseBody)Json.deserialize(httpRes.getBody(), GstReturnStatusResponseBody.class);
                    if(httpRes.getStatus() == 'OK' && httpRes.getStatusCode() == 200) {
                        if(objResponseBody != null) {
                            system.debug('objResponseBody ===' + objResponseBody);
                            if(sObjectName == Constants.strProjectBuilderAPI) {
                                Builder_Integrations__c objBIGST = [Select Id, Project_Builder__c, Filling_Frequency__c, System_Created__c from Builder_Integrations__c Where Id =: strRecordIntegrationLogId Limit 1];
                                System.debug('GSTRecord Return Status ' + objBIGST);
                                if(objResponseBody.result != null){
                                    integer size = objResponseBody.result.size();
                                    system.debug('size '+ size);
                                    system.debug('objResponseBody.result[size-1]' + objResponseBody.result[size-1].filing_frequency);
                                    System.enqueueJob(new EnqueueDMLGSTINOneAPF(objBIGST, null, objResponseBody.result[size-1].filing_frequency, jsonRequest, String.valueOf(httpRes), lstRestService[0].Service_EndPoint__c));
                                 }
                                System.debug('Result Array'+objResponseBody.result);
                                
                            }
                            
                            else if(sObjectName == Constants.strPromoterAPI) {
                                Promoter_Integrations__c objPIGST = [Select Id, Promoter__c, Filling_Frequency__c, System_Created__c from Promoter_Integrations__c Where Id =: strRecordIntegrationLogId Limit 1];
                                System.debug('GSTRecord Return Status ' + objPIGST);
                                if(objResponseBody.result != null){
                                    integer size = objResponseBody.result.size();
                                    system.debug('size '+ size);
                                    system.debug('objResponseBody.result[size-1]' + objResponseBody.result[size-1].filing_frequency);
                                    System.enqueueJob(new EnqueueDMLGSTINOneAPF(null, objPIGST, objResponseBody.result[size-1].filing_frequency, jsonRequest, String.valueOf(httpRes), lstRestService[0].Service_EndPoint__c));
                                 }
                                System.debug('Result Array'+objResponseBody.result);
                            }
                        }
                    }
                    
                }
                catch(Exception ex) {
                    System.debug('Debug Log for exception'+ex);
                }
           }
       }
           
    }
    
    public class RequestBody {
        public String pdfOutputType;
        public String consent;
        public String gstin;
    }
}