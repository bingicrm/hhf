public class APFExposureProjectBuilderController {
    @AuraEnabled
    public static String checkEligibility(Id projectBuilderId) {
        if(projectBuilderId != null) {
                APFExposureCallout.makeAPFExposureCalloutProjectBuilder(projectBuilderId);
                return 'APF Exposure request submitted.';
            
        }
        return 'Success';
    }
}