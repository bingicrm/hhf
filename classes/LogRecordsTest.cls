@isTest(SeeAllData=false)
/*
*  Name           :   LogRecordsTest 
*  Created By     :   Shilpa Menghani
*  Created Date   :   May 9,2016
*  Description    :   Test class 
*/
private class LogRecordsTest{
    private static User user;
    private static void setUpData(){
        Profile profile = [select Id from Profile where Name =: 'System Administrator'];
        
        user = new User();
        user.Username = 'testUser12345@deloitte.com';
        user.Email = 'testUser12345@deloitte.com';
        user.LastName = 'Test123';
        user.Alias = 'Test123';
        user.ProfileID = profile.Id;
        user.LocaleSidKey = 'en_US';
        user.LanguageLocaleKey = 'en_US';
        user.Country = 'India';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.EmployeeNumber = '22';
        user.EmailEncodingKey='UTF-8';
        insert user;
        
        List<Object_Mapping__c> settingslist = new List<Object_Mapping__c>();
                
        Object_Mapping__c ObjectName = new Object_Mapping__c();
        ObjectName.Name = 'Object Name';
        ObjectName.API_Name__c = 'Task';
        settingslist.add(ObjectName);
        insert settingslist;
    } 
    static testMethod void myUnitTestOne() {
        // TO DO: implement unit test
        setUpData();
        
        Test.startTest();
        
        System.runAs(user) { 
            LogRecords logRec = new LogRecords();
            system.assertNotEquals(LogRecords.sobjectAPIName,Null);
            
            }
        Test.stopTest();
     }
}