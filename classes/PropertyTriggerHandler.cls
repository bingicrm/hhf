public class PropertyTriggerHandler {
    public static void onBeforeInsert(List<Property__c> lstTriggerNew) {
        duplicateDetection(lstTriggerNew); //Added by Chitransh on 6th August 2019 for TIL-1276
        
        Set<Id> setLoanApplicationIds = new Set<Id>();
        Map<Id, Integer> mapLoanAppIdtoPropertyCount = new Map<Id, Integer>();
        for(Property__c objProperty : lstTriggerNew) {
            if(String.isNotBlank(objProperty.Loan_Application__c)) {
                setLoanApplicationIds.add(objProperty.Loan_Application__c);
            }
			/*Added by Ankit for TIL-00002464 -- Start*/
            if(objProperty.Area__c != null){
                objProperty.Area__c = objProperty.Area__c.round(System.RoundingMode.HALF_UP);
            }
            /*Added by Ankit for TIL-00002464 -- End*/
        }
        System.debug('Debug Log for setLoanApplicationIds'+setLoanApplicationIds.size());
        
        if(!setLoanApplicationIds.isEmpty()) {
            List<Loan_Application__c> lstLoanApplication = [SELECT Id, Name,(Select Id, Name, Loan_Application__c From Properties__r) FROM Loan_Application__c WHERE ID IN: setLoanApplicationIds];
            System.debug('Debug Log for lstLoanApplication'+lstLoanApplication.size());
            if(!lstLoanApplication.isEmpty()) {
                for(Loan_Application__c objLA : lstLoanApplication) {
                    mapLoanAppIdtoPropertyCount.put(objLA.Id, objLA.Properties__r.size());
                }
            }
            System.debug('Debug Log for mapLoanAppIdtoPropertyCount'+mapLoanAppIdtoPropertyCount.size());
        }
        
        for(Property__c objProperty : lstTriggerNew) {
            if(mapLoanAppIdtoPropertyCount.containsKey(objProperty.Loan_Application__c) && mapLoanAppIdtoPropertyCount.get(objProperty.Loan_Application__c) >= 1) {
                objProperty.addError('More than 1 Property is not allowed on a single Loan Application.');
            }
        }
    }
    
    //Below block added by Abhilekh on 20th August 2019 for TIL-855
    private static boolean run = true;
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }
    
    //Below method added by Abhilekh on 20th August 2019 for TIL-855
    public static void onBeforeUpdate(List<Property__c> lstTriggerNew,List<Property__c> lstTriggerOld,Map<Id,Property__c> newMap,Map<Id,Property__c> oldMap){
        if(runOnce()){
            String profileId = UserInfo.getProfileId();
            String profileName = [SELECT Id,Name from Profile WHERE Id =: profileId].Name;
            
            Set<String> setLAIds = new Set<String>();
            List<Loan_Application__c> lstLA = new List<Loan_Application__c>();
            
            for(Property__c prop: lstTriggerNew){
                setLAIds.add(prop.Loan_Application__c);
            }
            
            if(setLAIds.size() > 0){
                lstLA = [SELECT Id,Name,StageName__c,Sub_Stage__c from Loan_Application__c WHERE Id IN: setLAIds];
                System.debug('lstLA==>'+lstLA);
                System.debug('lstLA.size()==>'+lstLA.size());   
            }
            
            //To gather the field API names through SObject describe information
            List<Schema.SObjectField> fieldAPINames = new List<Schema.SObjectField>();
            
            Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.Property__c.fields.getMap();
            System.debug('schemaFieldMap==>'+schemaFieldMap);
            
            for(String propField: schemaFieldMap.keySet()){
                fieldAPINames.add(schemaFieldMap.get(propField));
            }
            
            for(Integer i=0;i<fieldAPINames.size();i++){
                for(String propField: schemaFieldMap.keySet()){
                    if(fieldAPINames[i] == schemaFieldMap.get(propField) && (propField == 'id' || propField == 'isdeleted' || 
                                                                             propField == 'createdbyid' || propField == 'createddate' || propField == 'lastmodifiedbyid' || 
                                                                             propField == 'lastmodifieddate' || propField == 'lastreferenceddate' || propField == 'lastvieweddate' || 
                                                                             propField == 'name' || propField == 'systemmodstamp' || propField == 'first_property_owner__c' || 
                                                                             propField == 'second_property_owner__c')){
                                                                                 System.debug('Inside remove if=='+propField);
                                                                                 System.debug('Inside remove if2=='+fieldAPINames[i]);
                                                                                 fieldAPINames.remove(i);
                                                                             }
                }
            }
            
            if(profileName != 'System Administrator'){
                for(Property__c prop: lstTriggerNew){
                    for(Loan_Application__c la: lstLA){
                        if(prop.Loan_Application__c == la.Id && la.Sub_Stage__c == Constants.Disbursement_Maker){
                            for(Schema.SObjectField proField: fieldAPINames){
                                if(prop.get(proField) != oldMap.get(prop.Id).get(proField)){
                                    prop.addError('You can only modify First Property Owner and Second Property Owner at this sub stage.');
                                }
                            }
                        }
                    }
                }    
            }
        }
        
        /*******Below code added by Abhilekh on 6th February 2020 for TIL-1942******************/
        Map<Id,Hunter_Required_Field__mdt> mapHunterFields= new Map<Id,Hunter_Required_Field__mdt>([Select Id, Field__c, Object__c, Critical_Field__c from Hunter_Required_Field__mdt where Object__c = 'Property__c' and Critical_Field__c = true]);
        
		//Added by Vaishali for BREII
        Map<Id,BRE2_Retrigger_Fields__mdt	> mapBRE2Retrigger = new Map<Id,BRE2_Retrigger_Fields__mdt	>([Select Id, Field__c, Object__c, isActive__c from BRE2_Retrigger_Fields__mdt where Object__c = 'Property__c' and isActive__c = true]);
        Map<Id,BRE2_Retrigger_Customer_Negotiation__mdt > mapBRE2RetriggerCustNegotiation = new Map<Id, BRE2_Retrigger_Customer_Negotiation__mdt  >([Select Id, Field__c, Object__c, isActive__c from BRE2_Retrigger_Customer_Negotiation__mdt where Object__c = 'Property__c' and isActive__c = true]);
        system.debug('mapBRE2Retrigger '+mapBRE2Retrigger);
        system.debug('mapBRE2RetriggerCustNegotiation '+mapBRE2RetriggerCustNegotiation);
        Set<Id> setOfAppIds = new Set<Id>();
        //End of the patch- Added by Vaishali for BREII
		
        for(Property__c prop: lstTriggerNew){
		
			/*Added by Ankit for TIL-00002464 -- Start*/
            if(prop.Area__c != null && prop.Area__c != oldMap.get(prop.Id).Area__c){
                prop.Area__c = prop.Area__c.round(System.RoundingMode.HALF_UP);
            }
            /*Added by Ankit for TIL-00002464 -- End*/
			
			//Added by Vaishali for BREII
            for ( BRE2_Retrigger_Fields__mdt objbreReset : mapBRE2Retrigger.values() ) {
                if( prop.reTriggerBRE2__c == false && prop.get(objbreReset.Field__c) != oldMap.get(prop.id).get(objbreReset.Field__c)) {
                    setOfAppIds.add(prop.Loan_Application__c); 
                    system.debug('Setting Financial_Eligibility_Valid__c to false');
                    break;
                }
            }
            
            for ( BRE2_Retrigger_Customer_Negotiation__mdt objbreReset : mapBRE2RetriggerCustNegotiation.values() ) {
                if( prop.reTriggerBRE2__c == false && prop.get(objbreReset.Field__c) != oldMap.get(prop.id).get(objbreReset.Field__c)) {
                    setOfAppIds.add(prop.Loan_Application__c); 
                    system.debug('Setting Financial_Eligibility_Valid__c to false');
                    break;
                }
            }
            //******************************************End of Code-- Added by Vaishali for BREII
			
            for(Hunter_Required_Field__mdt objHunterFields: mapHunterFields.values()){
                if(prop.get(objHunterFields.Field__c) != oldMap.get(prop.Id).get(objHunterFields.Field__c)){
                    prop.Retrigger_Hunter__c = true;
                    break;
                }
            }
        }
		
		//Added by Vaishali for BREII
        system.debug('setOfAppIds '+setOfAppIds);
        Id recordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByName().get('BRE2').getRecordTypeId();
        List<Customer_Integration__c> lstOfBRE2SuccessfulCustInt= new List<Customer_Integration__c>();
        if(!setOfAppIds.isEmpty()) {
            lstOfBRE2SuccessfulCustInt = [SELECT Id, Loan_Application__c, BRE2_Success__c from Customer_Integration__c WHERE Loan_Application__c IN: setOfAppIds and BRE2_Recent__c = true and recordTypeId =: recordTypeId ]; 
        }
        Map<Id, Boolean> mapOfBRE2SuccessfulCustInt = new Map<Id, Boolean>();
        if(!lstOfBRE2SuccessfulCustInt.isEmpty()) {
            for(Customer_Integration__c cust : lstOfBRE2SuccessfulCustInt) {
                if(cust.BRE2_Success__c == true) {
                    mapOfBRE2SuccessfulCustInt.put(cust.Loan_Application__c, true);
                } else {
                    mapOfBRE2SuccessfulCustInt.put(cust.Loan_Application__c, false);    
                }
            }
        }
        system.debug('mapOfBRE2SuccessfulCustInt '+mapOfBRE2SuccessfulCustInt);

        for(Property__c prop: lstTriggerNew) {
            if (mapOfBRE2SuccessfulCustInt != null) {
                if(mapOfBRE2SuccessfulCustInt.containsKey(prop.Loan_Application__c)) {
                    if(mapOfBRE2SuccessfulCustInt.get(prop.Loan_Application__c)) {
                        prop.reTriggerBRE2__c = true;       
                    }
                }
            }
        }    
        //End of patch-Added by Vaishali for BREII
		
        /************Above code added by Abhilekh on 6th February 2020 for TIL-1942***************/
    }
    
	//Added by Vaishali for BREII
    public static void afterUpdate(List<Property__c> newList,Map<Id,Property__c> oldMap){
    
        Map<Id,BRE2_Retrigger_Fields__mdt> mapBRE2Retrigger = new Map<Id,BRE2_Retrigger_Fields__mdt	>([Select Id, Field__c, Object__c, isActive__c from BRE2_Retrigger_Fields__mdt where Object__c = 'Property__c' and isActive__c = true]);
        Set<Id> setOfAppIds = new Set<Id>();
        Map<Id,BRE2_Retrigger_Customer_Negotiation__mdt > mapBRE2RetriggerCustNegotiation = new Map<Id, BRE2_Retrigger_Customer_Negotiation__mdt  >([Select Id, Field__c, Object__c, isActive__c from BRE2_Retrigger_Customer_Negotiation__mdt where Object__c = 'Property__c' and isActive__c = true]);
        system.debug('mapBRE2Retrigger '+mapBRE2Retrigger);
        system.debug('mapBRE2RetriggerCustNegotiation '+mapBRE2RetriggerCustNegotiation);
        
        for( Property__c prop : newList )
        {
            for ( BRE2_Retrigger_Fields__mdt objbreReset : mapBRE2Retrigger.values() ) {
                if(  prop.get(objbreReset.Field__c) != oldMap.get(prop.id).get(objbreReset.Field__c)) {
                    setOfAppIds.add(prop.Loan_Application__c); 
                    system.debug('Setting Financial_Eligibility_Valid__c to false');
                    break;
                }
            }
            
            for ( BRE2_Retrigger_Customer_Negotiation__mdt objbreReset : mapBRE2RetriggerCustNegotiation.values() ) {
                if(  prop.get(objbreReset.Field__c) != oldMap.get(prop.id).get(objbreReset.Field__c)) {
                    setOfAppIds.add(prop.Loan_Application__c); 
                    system.debug('Setting Financial_Eligibility_Valid__c to false');
                    break;
                }
            }
        }
        if(!setOfAppIds.isEmpty()) {
            LoanContactHelper.updateBRE2RetriggerOnLA(setOfAppIds);
        }
    }
    //ENd of patch - Added by Vaishali for BREII
	
/*****************************************************
@Description   : This method is used to prevent creation of more than one Property on a Loan Application (TIL-1276).
@params		   : list of Property__c
@Author        : Chitransh Porwal
@Created Date  : 6 August 2019
*********************************************************/
    public static void duplicateDetection(List<Property__c> lstProperty){
        Map<String, Integer> mapUniqueConstrainttoCount = new Map<String, Integer>();
        List<Property__c > lstExistingProperty = new List<Property__c >();
        String strUniqueConstraint;
        /**********Added by Chitransh for TIL-1258*********/
        set<id> loanAppIDs = new set<id>();
        if(lstProperty.size() >1) {
            for(Property__c p : lstProperty){
                loanAppIDs.add(p.Loan_Application__c);
            }
         /***End of Patch added by Chitransh for TIL-1258***/
            lstExistingProperty = [Select Id, Name, Loan_Application__c from Property__c where Loan_Application__c in : loanAppIDs];//Edited by Chitransh for TIL-1258
            if(!lstExistingProperty.isEmpty()) {
                for(Property__c objExistingProperty : lstExistingProperty) {
                    if(!mapUniqueConstrainttoCount.containsKey(objExistingProperty.Loan_Application__c)) {
                        mapUniqueConstrainttoCount.put(objExistingProperty.Loan_Application__c, 1);
                    }
                    else {
                        mapUniqueConstrainttoCount.put(objExistingProperty.Loan_Application__c,mapUniqueConstrainttoCount.get(objExistingProperty.Loan_Application__c)+1);
                    }
                }
            }
            for(Property__c objProperty : lstProperty) {
                if(!mapUniqueConstrainttoCount.containsKey(objProperty.Loan_Application__c)) {
                    mapUniqueConstrainttoCount.put(objProperty.Loan_Application__c, 1);
                }
                else {
                    objProperty.addError('You are trying to create a duplicate record. A record already exists for the specified Loan Application.');
                }
            }
        }
        else {
            lstExistingProperty = [Select Id, Name, Loan_Application__c from Property__c WHERE Loan_Application__c =: lstProperty[0].Loan_Application__c];
            if(!lstExistingProperty.isEmpty()) {
                lstProperty[0].addError('You are trying to create a duplicate record. A record already exists for the specified Loan Application.');
            }
        }
    }
}