@isTest

public class TestRejectOperations {
    public static testMethod void insertLA(){
        Id RecordTypeIdLoanApp = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('COPS:Data Checker').getRecordTypeId();
		Branch_Master__c objBM = new Branch_Master__c(Name='Delhi',Office_Code__c='1');
        insert objBM;
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9934567890');
        insert acc;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                         Transaction_type__c='PB',
                                                                         Requested_Amount__c=100000,
                                                                         Loan_Purpose__c='11',
                                                                         Sub_Stage__c= Constants.FCU_Review,
                                                                         Previous_Sub_Stage__c = Constants.Credit_Review,
                                                                         Overall_FCU_Status__c = Constants.NEGATIVE,
                                                                         Reviewed_by_FCU_Manager__c = true,
                                                                         StageName__c='Customer Onboarding',
                                                                         RecordTypeId=RecordTypeIdLoanApp,
                                                                         Branch__c='All',Branch_Lookup__c=objBM.Id,
                                                                         Loan_Status__c = 'Active',
                                                                         Repayment_Start_Date__c=Date.Today().addDays(100),
                                                                        Sanctioned_Disbursement_Type__c='Multiple Tranche'
                                                                        );
        insert objLoanApplication;
        Test.startTest();
        RejectOperations.getLoanApp(objLoanApplication.Id);
        RejectOperations.rejectApplication(objLoanApplication);
        RejectOperations.getPickListValuesIntoList();
        RejectOperations.getPickListValuesIntoList2();
        RejectOperations.base64ToBits('validFor');
        Loan_Application__c la = new Loan_Application__c();
        RejectOperations.getDependentMap(la, 'StageName__c', 'Sub_Stage__c');
        Test.stopTest();
        
        Loan_Application__c objLoanApplication1 = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                         Transaction_type__c='PB',
                                                                         Requested_Amount__c=100000,
                                                                         Loan_Purpose__c='11',
                                                                         Sub_Stage__c= Constants.Hunter_Review,
                                                                         Previous_Sub_Stage__c = Constants.Credit_Review,
                                                                         Overall_FCU_Status__c = Constants.NEGATIVE,
                                                                         Reviewed_by_FCU_Manager__c = true,
                                                                         StageName__c='Customer Onboarding',
                                                                         RecordTypeId=RecordTypeIdLoanApp,
                                                                         Branch__c='All',Branch_Lookup__c=objBM.Id,
                                                                         Loan_Status__c = 'Active',
                                                                         Repayment_Start_Date__c=Date.Today().addDays(100),
                                                                        Sanctioned_Disbursement_Type__c='Multiple Tranche'
                                                                        );
        insert objLoanApplication1;
        RejectOperations.rejectApplication(objLoanApplication1);
    }
    public static testMethod void insertLANegative(){
        Id RecordTypeIdLoanApp = Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('COPS:Data Checker').getRecordTypeId();
		Branch_Master__c objBM = new Branch_Master__c(Name='Delhi',Office_Code__c='1');
        insert objBM;
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9934567890');
        insert acc;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;
        
        Loan_Application__c objLoanApplication = new Loan_Application__c(Customer__c=acc.Id,Scheme__c=objScheme.Id,
                                                                         Transaction_type__c='PB',
                                                                         Requested_Amount__c=100000,
                                                                         Loan_Purpose__c='11',
                                                                         Sub_Stage__c= 'Express Queue: Data Checker',
                                                                         Rejection_Reason__c = 'Fraudulent Documents',
                                                                         Severity_Level__c = '1',
                                                                         Remarks_Rejection__c = 'Test',
                                                                         Previous_Sub_Stage__c = Constants.Credit_Review,
                                                                         Overall_FCU_Status__c = Constants.NEGATIVE,
                                                                         Reviewed_by_FCU_Manager__c = true,
                                                                         StageName__c='Customer Onboarding',
                                                                         RecordTypeId=RecordTypeIdLoanApp,
                                                                         Branch__c='All',Branch_Lookup__c=objBM.Id,
                                                                         Loan_Status__c = 'Active',
                                                                         Repayment_Start_Date__c=Date.Today().addDays(100),
                                                                        Sanctioned_Disbursement_Type__c='Multiple Tranche'
                                                                        );
        insert objLoanApplication;
        Test.startTest();
        RejectOperations.rejectApplication(objLoanApplication);
        Test.stopTest();
    }
}