/*=====================================================================
* Persistent Systems Ltd
* Name: CRM_Case_SendEmailBatchable
* Description: This batch class is used to calculate the Net Used days in Case and Dept Change.
* And to send reminder email and escalation emails based on escalation setup.
* Created Date: [01/02/2021]
* Created By: Pavani Duggirala
*
* Date Modified                Modified By                  Description of the update

    
=====================================================================*/
global Class CRM_Case_SendEmailBatchable implements Database.Batchable<sobject>{
    
    global BusinessHours caseBusinessHour;
    global CRM_Case_SendEmailBatchable(){
        caseBusinessHour = [SELECT Id FROM BusinessHours WHERE Name = 'HHF Business Hours'];
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        // returns all open cases
        
        String query = 'Select id,CaseNumber,OwnerId,Original_Case_Owner__c,Remaining_Days__c,Due_Date__c,TAT__c,CreatedDateTime__c,Net_Days_Used1__c,IsClosed,ClosedDate,Department__c,Credit_Dept_Used_Days__c,Department_TAT__c,FCU_Dept_Used_Days__c,Original_Case_Owner__r.Email,Owner.Email,PMAY_Dept_Used_Days__c, Finance_Dept_Used_Days__c, CentralOPS_Dept_Used_Days__c, Sales_Dept_Used_Days__c, Banking_Dept_Used_Days__c, Product_Dept_Used_Days__c from Case where TAT__c !=NULL AND IsClosed = false AND CreatedBy.Name=\'Prasanth Kalluri\'';
        /* @prasanth changes done by me in the above query. 
         * AND CreatedBy.Name=\'Prasanth Kalluri\ for testing
        1.ClosedDate, =>  IF(caseRecord.ClosedDate == NULL)
        2. Department__c, => if(cs.Department__c == 'Credit')
        3. Credit_Dept_Used_Days__c, => Decimal usedDays = cs.Credit_Dept_Used_Days__c;
        4. Department_TAT__c, => Decimal DeptTAT = cs.Department_TAT__c;
        5. FCU_Dept_Used_Days__C,=> Decimal usedDays = cs.FCU_Dept_Used_Days__c
        6. Original_case_Owner__r.Email, => if(cs.Department__c  == escData.Department__c 
                   && cs.Original_Case_Owner__r.Email == escData.L1_Email_Id__c)
        7. Owner.Email => if(cs.Department__c  == escData.Department__c && cs.Owner.Email == escData.L1_Email_Id__c)
        */
        system.debug('Query --'+ query);
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Case> scope){
        system.debug('****in execute'+scope.size());
        
        List<Messaging.SingleEmailMessage> lstEmails = new List<Messaging.SingleEmailMessage>();
        String noreply = Label.CRM_No_Reply;
        List<OrgwideEmailAddress> orgWide= [select id, address, DisplayName from OrgwideEmailAddress where displayName = : noReply];
        Id orgWideId = orgWide.size()>0?orgWide[0].id:null;
        EmailTemplate reminderEmailTemplate = [Select id, Body,Subject from EmailTemplate where DeveloperName = 'HHF_CRM_TAT_Reminder_Mail'];
        EmailTemplate escalationEmailTemplate = [Select id, Body,Subject from EmailTemplate where DeveloperName = 'HHF_CRM_TAT_Escalation_Mail'];
        
        EmailTemplate csreminderEmailTemplate = [Select id, Body,Subject from EmailTemplate where DeveloperName = 'HHF_CRM_CS_TAT_Reminder_Mail'];
        EmailTemplate csescalationEmailTemplate = [Select id, Body,Subject from EmailTemplate where DeveloperName = 'HHF_CRM_CS_TAT_Escalation_Mail'];
        
        List<Case> updateCasesWithNetDays = new List<Case>();
        List<CRM_Department_Change__c> updateDeptChangeRecs = new List<CRM_Department_Change__c>();
        Map<Id,CRM_Email_Tracker__c> updateEmailTrackerMap=new Map<Id,CRM_Email_Tracker__c>();
        Map<String,Map<String,CRM_Email_Tracker__c>> caseDepEmailtrackerMap = new Map<String,Map<String,CRM_Email_Tracker__c>>();

        //cs Overall TAT
        List<Case> casesWithRemainingDays1 = new List<Case>();
        List<Case> casesWithRemainingDays2 = new List<Case>();
        List<Case> casesWithRemainingDayNeg1 = new List<Case>();
        
        //for case's department value 
        List<Case> deptCasesWithRemainingDays1 = new List<Case>();
        List<Case> deptCasesWithRemainingDays2 = new List<Case>();
        List<Case> deptCasesWithRemainingDayNeg1 = new List<Case>();

        System.debug('Current Day : '+System.today());

        // update the case net days used for all cases 
        for(Case caseRecord : scope) {
            if(caseRecord.TAT__c != NULL) {
                String idval = String.valueOf(caseBusinessHour.id);
                //long dc = caseRecord.TAT__c.longValue();
                //long ms = dc*86400000;               
                //calculate net used days 
                Decimal netdays = 0;
                IF(caseRecord.ClosedDate == NULL){    
                    netdays = (((System.BusinessHours.diff(idval,caseRecord.CreatedDateTime__c,System.today()) / 3600000) / 24));
                    System.debug('--> 76' + netdays);
                    caseRecord.Net_Days_Used1__c = netdays;
                    updateCasesWithNetDays.add(caseRecord);  
                    
                    Decimal remainingDays = caseRecord.TAT__c - netdays;
                    System.debug('48-->'+remainingDays);//@prasanth added for debuging
                    if(remainingDays == 1) {
                        casesWithRemainingDays1.add(caseRecord);
                    } else if(remainingDays == 2) {
                        casesWithRemainingDays2.add(caseRecord);
                    } else if (remainingDays == -1) {
                        casesWithRemainingDayNeg1.add(caseRecord);
                    }
                } 
            }
        }
        
        // update case's department changes used days for all cases
        Set<String> caseIds = new Set<String>();
        for(Case cs : scope) {
            caseIds.add(cs.Id);
        }
        List<CRM_Department_Change__c> listofDeptChanges = [Select id,CRM_Department__c,Entry_Time__c,Exit_Time__c,Days_Used__c 
                                                            from CRM_Department_Change__c 
                                                            where Case__c IN : caseIds AND Exit_Time__c = NULL];
        
        // getting email traker records for all cases
        for(CRM_Email_Tracker__c emailTracker : [Select ID,Case__c,Department__c,L1_Email_Sent__c,L2_Email_Sent__c,
                                                   L3_Email_Sent__c From CRM_Email_Tracker__c Where Case__c IN: caseIds]){
            Map<String,CRM_Email_Tracker__c> tmpMap = new Map<String,CRM_Email_Tracker__c>();
            if(caseDepEmailtrackerMap.containsKey(emailTracker.Case__c)){
                tmpMap = caseDepEmailtrackerMap.get(emailTracker.Case__c);
            }
            tmpMap.put(emailTracker.Department__c,emailTracker);
            caseDepEmailtrackerMap.put(emailTracker.Case__c,tmpMap);
        }
        system.debug('caseDepEmailtrackerMap' + caseDepEmailtrackerMap);
        
        for(CRM_Department_Change__c dcRec : listofDeptChanges) {
            String idval = String.valueOf(caseBusinessHour.id);
            Decimal netdays = (((System.BusinessHours.diff(idval,dcRec.Entry_Time__c,System.today()) / 3600000) / 24));
            dcRec.Days_Used__c = netdays;
            updateDeptChangeRecs.add(dcRec);
        }
        
        system.debug('updateCasesWithNetDays...'+ updateCasesWithNetDays.size());
        system.debug('updateDeptChangeRecs...'+ updateDeptChangeRecs.size());
        
        //Update days in Dep
        if(!updateCasesWithNetDays.isEmpty()) {
            update updateCasesWithNetDays;
        }
        if(!updateDeptChangeRecs.isEmpty()) {
            update updateDeptChangeRecs;
            system.debug('Completed dept updates');
        }
        
        // get the remaining days for depts logic
        for(Case cs : [Select id,CaseNumber,OwnerId,Original_Case_Owner__c,Remaining_Days__c,Due_Date__c,TAT__c,CreatedDateTime__c,Net_Days_Used1__c,IsClosed,ClosedDate,Department__c,Credit_Dept_Used_Days__c,Department_TAT__c,FCU_Dept_Used_Days__c,Original_Case_Owner__r.Email,Owner.Email,PMAY_Dept_Used_Days__c, Finance_Dept_Used_Days__c, CentralOPS_Dept_Used_Days__c, Sales_Dept_Used_Days__c, Banking_Dept_Used_Days__c, Product_Dept_Used_Days__c from Case where ID IN :caseIds]) 
             {
            //added query in the for-each loop to get the records with updated records with dept used days
            if(cs.Department__c == 'Credit') {
                Decimal usedDays = cs.Credit_Dept_Used_Days__c;
                Decimal DeptTAT = cs.Department_TAT__c;
                Decimal remainingDays = DeptTAT - usedDays;
                System.debug('Credit --->');
                System.debug('Case : '+cs.CaseNumber);
                System.debug('remainingDays : '+remainingDays);
                if(remainingDays == 1) {
                    deptCasesWithRemainingDays1.add(cs);
                } else if(remainingDays == 2) {
                    deptCasesWithRemainingDays2.add(cs);
                } else if (remainingDays == -1) {
                    deptCasesWithRemainingDayNeg1.add(cs);
                }
            } else if(cs.Department__c == 'Central OPS') {
                Decimal usedDays = cs.CentralOPS_Dept_Used_Days__c;
                Decimal DeptTAT = cs.Department_TAT__c;
                Decimal remainingDays = DeptTAT - usedDays;
                System.debug('Central OPS --->');
                System.debug('Case : '+cs.CaseNumber);
                System.debug('remainingDays : '+remainingDays);
                if(remainingDays == 1) {
                    deptCasesWithRemainingDays1.add(cs);
                } else if(remainingDays == 2) {
                    deptCasesWithRemainingDays2.add(cs);
                } else if (remainingDays == -1) {
                    deptCasesWithRemainingDayNeg1.add(cs);
                }   
            } else if (cs.Department__c == 'Product') {
                Decimal usedDays = cs.Product_Dept_Used_Days__c;
                Decimal DeptTAT = cs.Department_TAT__c;
                Decimal remainingDays = DeptTAT - usedDays;
                System.debug('Product --->');
                System.debug('Case : '+cs.CaseNumber);
                System.debug('remainingDays : '+remainingDays);
                if(remainingDays == 1) {
                    deptCasesWithRemainingDays1.add(cs);
                } else if(remainingDays == 2) {
                    deptCasesWithRemainingDays2.add(cs);
                } else if (remainingDays == -1) {
                    deptCasesWithRemainingDayNeg1.add(cs);
                }
            } else if (cs.Department__c == 'Banking') {
                Decimal usedDays = cs.Banking_Dept_Used_Days__c;
                Decimal DeptTAT = cs.Department_TAT__c;
                Decimal remainingDays = DeptTAT - usedDays;
                System.debug('Banking --->');
                System.debug('Case : '+cs.CaseNumber);
                System.debug('remainingDays : '+remainingDays);
                if(remainingDays == 1) {
                    deptCasesWithRemainingDays1.add(cs);
                } else if(remainingDays == 2) {
                    deptCasesWithRemainingDays2.add(cs);
                } else if (remainingDays == -1) {
                    deptCasesWithRemainingDayNeg1.add(cs);
                }
            } else if (cs.Department__c == 'Sales') {
                Decimal usedDays = cs.Sales_Dept_Used_Days__c;
                Decimal DeptTAT = cs.Department_TAT__c;
                Decimal remainingDays = DeptTAT - usedDays;
                System.debug('Sales --->');
                System.debug('Case : '+cs.CaseNumber);
                System.debug('remainingDays : '+remainingDays);
                if(remainingDays == 1) {
                    deptCasesWithRemainingDays1.add(cs);
                } else if(remainingDays == 2) {
                    deptCasesWithRemainingDays2.add(cs);
                } else if (remainingDays == -1) {
                    deptCasesWithRemainingDayNeg1.add(cs);
                }
            } else if (cs.Department__c == 'PMAY') {
                Decimal usedDays = cs.PMAY_Dept_Used_Days__c;
                Decimal DeptTAT = cs.Department_TAT__c;
                Decimal remainingDays = DeptTAT - usedDays;
                System.debug('PMAY --->');
                System.debug('Case : '+cs.CaseNumber);
                System.debug('remainingDays : '+remainingDays);
                if(remainingDays == 1) {
                    deptCasesWithRemainingDays1.add(cs);
                } else if(remainingDays == 2) {
                    deptCasesWithRemainingDays2.add(cs);
                } else if (remainingDays == -1) {
                    deptCasesWithRemainingDayNeg1.add(cs);
                }
            } else if (cs.Department__c == 'Finance') {
                Decimal usedDays = cs.Finance_Dept_Used_Days__c;
                Decimal DeptTAT = cs.Department_TAT__c;
                Decimal remainingDays = DeptTAT - usedDays;
                System.debug('Finance --->');
                System.debug('Case : '+cs.CaseNumber);
                System.debug('remainingDays : '+remainingDays);
                if(remainingDays == 1) {
                    deptCasesWithRemainingDays1.add(cs);
                } else if(remainingDays == 2) {
                    deptCasesWithRemainingDays2.add(cs);
                } else if (remainingDays == -1) {
                    deptCasesWithRemainingDayNeg1.add(cs);
                }
            } else if (cs.Department__c == 'FCU') {
                Decimal usedDays = cs.FCU_Dept_Used_Days__c;
                Decimal DeptTAT = cs.Department_TAT__c;
                Decimal remainingDays = DeptTAT - usedDays;
                System.debug('FCU --->');
                System.debug('Case : '+cs.CaseNumber);
                System.debug('remainingDays : '+remainingDays);
                if(remainingDays == 1) {
                    deptCasesWithRemainingDays1.add(cs);
                } else if(remainingDays == 2) {
                    deptCasesWithRemainingDays2.add(cs);
                } else if (remainingDays == -1) {
                    deptCasesWithRemainingDayNeg1.add(cs);
                }
            } 
        }

        // send emails
        List<CRM_Case_Escalation_Matrix_Setting__mdt> escalationData = [Select id, Department__c, isActive__c, L1_Email_Id__c, L1_Name__c, 
                                                                        L2_Email_Id__c, L2_Name__c, L3_Email_Id__c, L3_Name__c 
                                                                       From CRM_Case_Escalation_Matrix_Setting__mdt where isActive__c = true ];
        
        // sending email reminders for CS department which is using overall case TAT
     
        // Send email to Case Owner before 2 days of TAT
        for(Case cs : casesWithRemainingDays2) {
            if(!caseDepEmailtrackerMap.get(cs.Id).get(cs.Department__c).L1_Email_Sent__c && csreminderEmailTemplate != NULL && cs.Original_Case_Owner__c != NULL) {
                Messaging.SingleEmailMessage objEmail = Messaging.renderStoredEmailTemplate(csreminderEmailTemplate.id, cs.Original_Case_Owner__c, cs.id);
                if(orgWideId!=null){
                    objEmail.setOrgWideEmailAddressId(orgWideId);
                }
                objEmail.setSaveAsActivity(false);
                lstEmails.add(objEmail);
                CRM_Email_Tracker__c tmpTracker = caseDepEmailtrackerMap.get(cs.Id).get(cs.Department__c);
                if(updateEmailTrackerMap.containsKey(tmpTracker.Id)){
                    tmpTracker = updateEmailTrackerMap.get(tmpTracker.Id);
                }
                tmpTracker.L1_Email_Sent__c = true;
                updateEmailTrackerMap.put(tmpTracker.Id,tmpTracker);
            }
        }
        system.debug('CS before 2 days ..' + lstEmails);
        
        // send email to Case owner and L2 manager before 1 day of TAT
        for(Case cs : casesWithRemainingDays1) {
            List<String> L2MgrEmailId = new List<String>();
            List<String> escalationIds = new List<String>();
            for (CRM_Case_Escalation_Matrix_Setting__mdt escData : escalationData) {
                if(cs.Department__c  == escData.Department__c 
                   && cs.Original_Case_Owner__r.Email == escData.L1_Email_Id__c)  {
                       escalationIds = escData.L2_Email_Id__c.split('/');
                       L2MgrEmailId.addAll(escalationIds);
                 }               
            }
            system.debug('L2MgrEmailId .. '+ L2MgrEmailId);
            if(!caseDepEmailtrackerMap.get(cs.Id).get(cs.Department__c).L2_Email_Sent__c && csreminderEmailTemplate != NULL && cs.OwnerId != NULL && !L2MgrEmailId.isEmpty()) {
                Messaging.SingleEmailMessage objEmail = Messaging.renderStoredEmailTemplate(csreminderEmailTemplate.id, null, cs.id);
                if(orgWideId!=null){
                objEmail.setOrgWideEmailAddressId(orgWideId);
                }
                objEmail.setSaveAsActivity(false);
                objEmail.setToAddresses(L2MgrEmailId);
                lstEmails.add(objEmail);
                CRM_Email_Tracker__c tmpTracker = caseDepEmailtrackerMap.get(cs.Id).get(cs.Department__c);
                if(updateEmailTrackerMap.containsKey(tmpTracker.Id)){
                    tmpTracker = updateEmailTrackerMap.get(tmpTracker.Id);
                }
                tmpTracker.L2_Email_Sent__c = true;
                updateEmailTrackerMap.put(tmpTracker.Id,tmpTracker);
            }
        }
        system.debug('CS before 1 days ..' + lstEmails);
        
        // send email to Case owner and L3 manager if -1 day of TAT
        for(Case cs : casesWithRemainingDayNeg1) {
            List<String> L3MgrEmailId = new List<String>();
            List<String> escalationIds = new List<String>();
            for (CRM_Case_Escalation_Matrix_Setting__mdt escData : escalationData) {
                if(cs.Department__c  == escData.Department__c 
                   && cs.Original_Case_Owner__r.Email == escData.L1_Email_Id__c)  {
                       escalationIds = escData.L3_Email_Id__c.split('/');
                       L3MgrEmailId.addAll(escalationIds);
                 }
            }
            system.debug('L3MgrEmailId .. '+ L3MgrEmailId);
            if(!caseDepEmailtrackerMap.get(cs.Id).get(cs.Department__c).L3_Email_Sent__c && csescalationEmailTemplate != NULL && cs.OwnerId != NULL && !L3MgrEmailId.isEmpty()) {
                Messaging.SingleEmailMessage objEmail = Messaging.renderStoredEmailTemplate(csescalationEmailTemplate.id, null, cs.id);
                if(orgWideId!=null){
                objEmail.setOrgWideEmailAddressId(orgWideId);
                }
                objEmail.setSaveAsActivity(false);
                objEmail.setToAddresses(L3MgrEmailId);
                lstEmails.add(objEmail);
                CRM_Email_Tracker__c tmpTracker = caseDepEmailtrackerMap.get(cs.Id).get(cs.Department__c);
                if(updateEmailTrackerMap.containsKey(tmpTracker.Id)){
                    tmpTracker = updateEmailTrackerMap.get(tmpTracker.Id);
                }
                tmpTracker.L3_Email_Sent__c = true;
                updateEmailTrackerMap.put(tmpTracker.Id,tmpTracker);
            }
        }
        system.debug('CS -1 days ..' + lstEmails);

        // send emails for Department 
        
        // sending email reminders for Other department which is using Dept TAT
        // Send email to Case Owner before 2 days of Dept TAT
        for(Case cs : deptCasesWithRemainingDays2) {
            if(!caseDepEmailtrackerMap.get(cs.Id).get(cs.Department__c).L1_Email_Sent__c && reminderEmailTemplate != NULL && cs.OwnerId != NULL) {
                Messaging.SingleEmailMessage objEmail = Messaging.renderStoredEmailTemplate(reminderEmailTemplate.id, cs.OwnerId, cs.id);
                if(orgWideId!=null){
                objEmail.setOrgWideEmailAddressId(orgWideId);
                }
                objEmail.setSaveAsActivity(false);
                lstEmails.add(objEmail);
                CRM_Email_Tracker__c tmpTracker = caseDepEmailtrackerMap.get(cs.Id).get(cs.Department__c);
                if(updateEmailTrackerMap.containsKey(tmpTracker.Id)){
                    tmpTracker = updateEmailTrackerMap.get(tmpTracker.Id);
                }
                tmpTracker.L1_Email_Sent__c = true;
                updateEmailTrackerMap.put(tmpTracker.Id,tmpTracker);
            }
        }
        system.debug('Dept before 2 days ..' + lstEmails);
        
        // send email to Case owner and L2 manager before 1 day of Dept TAT
        for(Case cs : deptCasesWithRemainingDays1) {
            List<String> L2MgrEmailId = new List<String>();
            List<String> escalationIds = new List<String>();
            for (CRM_Case_Escalation_Matrix_Setting__mdt escData : escalationData) {
                if(cs.Department__c  == escData.Department__c 
                   && cs.Owner.Email == escData.L1_Email_Id__c)  {
                       escalationIds = escData.L2_Email_Id__c.split('/');
                       L2MgrEmailId.addAll(escalationIds);
                 }               
            }
            system.debug('Dept L2MgrEmailId .. '+ L2MgrEmailId);
            if(!caseDepEmailtrackerMap.get(cs.Id).get(cs.Department__c).L2_Email_Sent__c && reminderEmailTemplate != NULL && cs.OwnerId != NULL && !L2MgrEmailId.isEmpty()) {
                Messaging.SingleEmailMessage objEmail = Messaging.renderStoredEmailTemplate(reminderEmailTemplate.id, null, cs.id);
                if(orgWideId!=null){
                objEmail.setOrgWideEmailAddressId(orgWideId);
                }
                objEmail.setSaveAsActivity(false);
                objEmail.setToAddresses(L2MgrEmailId);
                lstEmails.add(objEmail);
                CRM_Email_Tracker__c tmpTracker = caseDepEmailtrackerMap.get(cs.Id).get(cs.Department__c);
                if(updateEmailTrackerMap.containsKey(tmpTracker.Id)){
                    tmpTracker = updateEmailTrackerMap.get(tmpTracker.Id);
                }
                tmpTracker.L2_Email_Sent__c = true;
                updateEmailTrackerMap.put(tmpTracker.Id,tmpTracker);
            }
        }
        system.debug('Dept before 1 days ..' + lstEmails);
        
        // send email to Case owner and L3 manager if -1 day of DeptTAT
        for(Case cs : deptCasesWithRemainingDayNeg1) { 
            List<String> L3MgrEmailId = new List<String>();
            List<String> escalationIds = new List<String>();
            for (CRM_Case_Escalation_Matrix_Setting__mdt escData : escalationData) {
                if(cs.Department__c  == escData.Department__c 
                   && cs.Owner.Email == escData.L1_Email_Id__c)  {
                       escalationIds = escData.L3_Email_Id__c.split('/');
                       L3MgrEmailId.addAll(escalationIds);
                 }
            }
            system.debug('Dept L3MgrEmailId .. '+ L3MgrEmailId);
            if(!caseDepEmailtrackerMap.get(cs.Id).get(cs.Department__c).L3_Email_Sent__c && reminderEmailTemplate != NULL && cs.OwnerId != NULL && !L3MgrEmailId.isEmpty()) {
                Messaging.SingleEmailMessage objEmail = Messaging.renderStoredEmailTemplate(escalationEmailTemplate.id, null, cs.id);
                if(orgWideId!=null){
                objEmail.setOrgWideEmailAddressId(orgWideId);
                }
                objEmail.setSaveAsActivity(false);
                objEmail.setToAddresses(L3MgrEmailId);
                lstEmails.add(objEmail);
                CRM_Email_Tracker__c tmpTracker = caseDepEmailtrackerMap.get(cs.Id).get(cs.Department__c);
                if(updateEmailTrackerMap.containsKey(tmpTracker.Id)){
                    tmpTracker = updateEmailTrackerMap.get(tmpTracker.Id);
                }
                tmpTracker.L3_Email_Sent__c = true;
                updateEmailTrackerMap.put(tmpTracker.Id,tmpTracker);
            }
        }
        system.debug('Dept -1 days ..' + lstEmails);
        
        system.debug('lstEmails size ...' + lstEmails.size());
        if(!Test.isRunningTest()) {
            if(!lstEmails.isEmpty()) {
                //Messaging.SendEmailResult[] results = Messaging.sendEmail(lstEmails);
            }
        }

        if(!updateEmailTrackerMap.isEmpty()){
            update updateEmailTrackerMap.values();
        }
    }
    global void finish(Database.BatchableContext BC) {
        //@Prasanth below logic for scheduling the next batch run in next 1 business hour
        DateTime currentTime = System.now();
        DateTime nextBatchRunTime = currentTime.addHours(1);
        While((System.BusinessHours.diff(caseBusinessHour.Id,currentTime,nextBatchRunTime) / 3600000)<1){
            nextBatchRunTime = nextBatchRunTime.addHours(1);
        }
        String nextFireTime = '0 0 ' + nextBatchRunTime.hour() + ' ' + nextBatchRunTime.day() + ' ' + nextBatchRunTime.month() + ' ?' + ' ' + nextBatchRunTime.year();
        CRM_Case_SendEmailBatchableScheduler schedulableSC = new CRM_Case_SendEmailBatchableScheduler(); 
        if(!Test.isRunningTest()) {
            String jobID = system.schedule('CRM_Case_SendEmailBatchable Job '+nextBatchRunTime.addMinutes(-nextBatchRunTime.minute()).addSeconds(-nextBatchRunTime.second()).format(), nextFireTime, schedulableSC);
        }
    }
        
}