@isTest
public class CRM_LMSItcFALoanPdfTest {
    @testSetup static void setup(){
        CRM_Utility.insertRestService('itc');
        
        Loan_Application__c loanApp = new Loan_Application__c();
        loanApp.Loan_Number__c = '1000273';
        loanApp.Loan_Application_Number__c = 'HHFDELHOU18000000146';
        insert loanApp;
        
        case theCase = new case();
        theCase.Loan_Application__c = loanApp.Id;
        theCase.LD_Branch_ID__c = '1';
        insert theCase;
    }
    
    static testMethod void testItcProvisionalPdf(){
        date caseFromDate = date.newinstance(2015, 12, 8);
        date caseToDate = date.newinstance(2016, 5, 11);
        map<string,string> dateMap = new map<string,string>();
        dateMap.put('caseFromDate',CRM_CaseController.getDate(caseFromDate).toUpperCase());
        dateMap.put('caseToDate',CRM_CaseController.getDate(caseToDate).toUpperCase());
        dateMap.put('today',CRM_CaseController.getDate(date.today()).toUpperCase());
        
        case theCase = [Select Id from case order by createddate desc limit 1];
        
        PageReference pdf = Page.CRM_LMSItcFALoanPdf;
        Test.setCurrentPage(pdf);
        pdf.getParameters().put('id', String.valueOf(theCase.Id));
        pdf.getParameters().put('pdfDetailsMap',JSON.serialize(dateMap));
        ApexPages.StandardController sc = new ApexPages.StandardController(theCase);
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new CRM_LmsItcFALoanMock());
        CRM_CaseItcFALoanPdfController pdfControllerOne = new CRM_CaseItcFALoanPdfController(sc);
        pdfControllerOne.logUtility();
        test.stopTest();
    }
}