@isTest

public class TestdisbursalReportPDFctrl {
    
    
    Public static testMethod void method1(){
        Account acc=new Account();
        acc.name='TestAccoun123t12';
        acc.phone='9232567891';
        acc.PAN__c='fjauy1936e';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        System.debug(acc.id);
        
        Scheme__c sc=new Scheme__c();
        sc.name='TestScheme';
        sc.Scheme_Code__c='testcode';
        sc.Scheme_Group_ID__c='23131';
        sc.Product_Code__c='HL';
        insert sc;
        System.debug(sc.id);
        
        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Scheme__c=sc.id;
        lap.Approved_Loan_Amount__c=5000000;
        lap.transaction_type__c = 'PB';
        insert lap;
        System.debug(lap.id);
        
        
        
        /*
Loan_Contact__c lc=
Loan_Contact__c();
lc.Customer__c=acc.Id;
lc.Loan_Applications__c=lap.id;
lc.Applicant_Type__c='Guarantor';
lc.recordtypeid='0120l000000IAiC';
insert lc; 
System.debug(lc.id);*/
        
        /*IMD__c immd= new IMD__c();
immd.Amount__c=100;
immd.Business_Date__c=Date.newInstance(2018, 11, 20);
immd.Cheque_Number__c='141234';
immd.recordtypeid='0120l000000IAhl';
immd.Cheque_Status__c='R';
immd.Payment_Mode__c='Q';
immd.Cheque_Date__c=Date.newInstance(2018, 10, 24);
immd.Loan_Applications__c=lap.id;
insert immd;
*/
        pincode__c pc=new pincode__c();
        pc.name='Bengaluru';
        pc.City__c='Bengaluru';
        pc.ZIP_ID__c='fnOt105';
        
        insert pc;
        
        //System.debug(immd.id);
        Bank_Master__c bmc=new Bank_Master__c();
        bmc.A_c_Holder_Name__c='testtt';
        bmc.A_c_no__c=1231231231;
        bmc.Country__c='1';
        bmc.pincode__c=pc.id;
        
        insert bmc;
        Disbursement__c db=new Disbursement__c();
        db.Loan_Application__c=lap.id;
        //db.Mode_of_Payment__c='Q';
        db.Disbursal_Amount__c = 5000000;
        db.Bank_Master__c=bmc.id;
        db.Cheque_Amount__c=2000000;
        db.Cheque_Favouring__c='tessst';
        //insert db;
        
        Sourcing_Detail__c src=new Sourcing_Detail__c();
        
        src.Source_Code__c='13';
        src.Loan_Application__c=lap.id;
        
        
        insert src;
        
        Repayment_Schedule__c rpsc=new Repayment_Schedule__c();
        rpsc.Loan_Application__c=lap.id;
        rpsc.Name='12312';
        
        insert rpsc;
        
        Loan_Repayment__c lrc=new Loan_Repayment__c();
        lrc.Loan_Application__c=lap.id;
        
        insert lrc;
        
        
        Test.startTest();
        
        Bank_Detail__c bdc=new Bank_Detail__c();
        List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
        bdc.Customer_Details__c=lstofcon[0].id;
        bdc.Name_of_the_Bank__c='tessst';
        bdc.Bank_Branch__c='Palace ground';
        bdc.Loan_Application__c=lap.id;
        
        insert bdc;
        
        
        Document_Checklist__c dc=new Document_Checklist__c();
        
        dc.Loan_Applications__c=lap.id;
       // dc.Loan_Contact__c=lstofcon[0].id;
        dc.REquest_Date_for_OTC__c=Date.newInstance(2018, 11, 29);
        //dc.Disbursement__c=db.id;
        //System.debug(db.id);
        insert dc;
        pdc__c ppd=new pdc__c();
        
        ppd.Loan_Application__c=lap.id;
        //ppd.name='tessssst';
        ppd.Bank_Detail__c=bdc.id;
        
        //insert ppd;
        
        Address__c address=new Address__c();
        address.Address_Line_1__c='abc Street';
        address.Address_Line_2__c='abc Area';
        address.Pincode_LP__c=pc.id;
        address.Type_of_address__c='PERMNENT';
        address.Loan_Contact__c=lstofcon[0].id;
        
        //insert address;
        Test.stopTest();
        
        
        /*
database.delete(lrc.id);

Database.delete(src);
//Database.delete(ppd.id);
Database.delete(bdc.id);
//database.delete(pc.id);
database.delete(bmc.id);
System.debug(lap.id);
Database.delete(acc.id);
Database.delete(sc.id);
Database.delete(lap.id);
*/
        
        
        
        PageReference myVfPage = Page.disbursalReportPDF;
        
        myVfPage.getParameters().put('id', lap.id);
        Test.setCurrentPage(myVfPage);
        
        
        ApexPages.StandardController sco = new ApexPages.StandardController(lap);
        
        
        disbursalReportPDFctrl drpc=new disbursalReportPDFctrl(sco);
        
        
    }
    
    public static testMethod  void method2(){
        
        Loan_Application__c lap=new Loan_Application__c ();
        ApexPages.StandardController sco = new ApexPages.StandardController(lap);
        disbursalReportPDFctrl drpc=new disbursalReportPDFctrl(sco);
        
    }
    
    
}