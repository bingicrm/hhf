@isTest
public class cls_RetryMoveFilesTest {
    
    public static testMethod void test1(){
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
        insert con1;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;        

        Loan_Application__c la = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert la;
        System.debug('la--cv-'+la+'-name-'+la.Name);
        Loan_Application__c la1= [Select Id, name,Scheme__c from Loan_Application__c where Id=:la.Id ];
        System.debug('la--cv-'+la1+'-name-'+la.Name);
        
        
        Loan_Contact__c lc = [SELECT Id, Name,Applicant_Type__c, Applicant_Status__c, Constitution__c, Customer_segment__c, Loan_Applications__c FROM Loan_Contact__c WHERE Loan_Applications__c =: la.Id];
        System.debug('lc-CV-'+lc);
        lc.Borrower__c='2';
        lc.Contact__c = con1.Id;
        lc.Constitution__c='18';
        lc.Applicant_Status__c='1';
        lc.Customer_segment__c='9';
        lc.Mobile__c='9876543210';
        lc.Email__c='testemail@gmail.com';
        update lc;
        
        Test.startTest();        
       
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=la.Id,
                                                                     Loan_Contact__c=lc.Id);
        insert objDocCheck;
        System.debug('objDocCheck-cv-'+objDocCheck);    
        
        Borrower_Document_Checklist__c objBorrowDocCheck = new Borrower_Document_Checklist__c();
        objBorrowDocCheck.Applicant_Type__c = 'Co- Applicant';
        objBorrowDocCheck.Applicant_Status__c = '1';
        objBorrowDocCheck.Customer_Type__c = '1';
        //objBorrowDocCheck.Customer_Segment__c = '3';
        objBorrowDocCheck.Income_program_typeq__c= '';
        objBorrowDocCheck.Customer_Type__c = '';
        objBorrowDocCheck.Constitution__c = '20';
        insert objBorrowDocCheck;
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
            
       	System.debug('cv--ContentVersion--'+cv);     
         
        //Get Content Documents
        cv=[SELECT Id, Title, VersionData, CreatedDate, ContentDocumentId, FileExtension,IsLatest FROM ContentVersion];
       	System.debug('cv--SOQL--'+cv);     
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
       	System.debug('cv--conDocId--'+conDocId); 
        
        Document_Migration_Log__c objDoc = new Document_Migration_Log__c ();
        objDoc.Description__c='Test';
        objDoc.Loan_Application__c=la.Id;
        objDoc.Document_Checklist__c = objDocCheck.Id;
        objDoc.Document_Id__c = conDocId;
        insert objDoc;
        Document_Migration_Log__c objDoc1 = [Select Id,Loan_Application__c,Document_Checklist__c,Document_Id__c from Document_Migration_Log__c];
        System.debug('objDoc-cv-'+objDoc1+'-Document_Id__c-'+objDoc1.Document_Id__c);    

        Attachment att=new Attachment();
        att.ParentId = objDoc.Loan_Application__c ;
        att.Name='test';
        att.Body=Blob.valueOf('test');
        //att.Id =: objDoc.Document_Id__c;
        insert att;
                
        //Create ContentDocumentLink 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = objDocCheck.Id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        Insert cdl;
        
       
        
        // Create person account
       /* List<Account> personAccs = new List<Account>();
        Account personAcc1 = new Account(FirstName = 'Test', LastName = 'ABC');
        personAcc1.Phone = '9999999999';
        personAccs.add(personAcc1);
        Account personAcc2 = new Account(FirstName = 'Test', LastName = 'ABC');
        personAcc2.Phone = '9999999999';
        personAccs.add(personAcc2);
        insert personAccs;*/
        
        // Create document master
        Document_Master__c documentMaster = new Document_Master__c(Name = 'Test Document Master');
        insert documentMaster;
        
        // Create document type
        Document_Type__c documentType = new Document_Type__c(Name = Constants.PROPERTYPAPERS);
        insert documentType;
        
        // Create scheme
        Scheme__c scheme = new Scheme__c(Name = 'Test Scheme');
        scheme.Scheme_Group_ID__c = 'Test';
        scheme.Product_Code__c = 'HL';
        scheme.Scheme_Code__c = 'Test';
        insert scheme;
        
        // Create loan application
        Loan_Application__c loanApplication = new Loan_Application__c();
        loanApplication.Scheme__c = scheme.id;
        insert loanApplication;
        Loan_Application__c la2= [Select Id, name,Scheme__c from Loan_Application__c where Id=:loanApplication.Id ];
        System.debug('loanApplication--cv-'+la2+'-name-'+loanApplication.Name);
        
        String targetFolder = 'Applicant Documents for '+la1.Name;
        System.debug('targetFolder-cv-'+targetFolder);
        // Create customer detail
        Loan_Contact__c loanContact1 = new Loan_Contact__c(Customer__c = acc.Id);
        loanContact1.Loan_Applications__c = loanApplication.Id;
        loanContact1.Applicant_Type__c = 'Applicant';
        insert loanContact1;
        
        Loan_Contact__c loanContact2 = new Loan_Contact__c(Customer__c = acc1.Id);
        loanContact2.Loan_Applications__c = loanApplication.Id;
        loanContact2.Applicant_Type__c = Constants.COAPP;
        insert loanContact2;
        
        // Create document check lists
        List<Document_Checklist__c> documentCheckLists = new List<Document_Checklist__c>();
        Document_Checklist__c documentCheckList1 = new Document_Checklist__c(Loan_Applications__c = loanApplication.Id);
        documentCheckList1.Document_Master__c = documentMaster.Id;
        documentCheckList1.Document_Type__c = documentType.Id;
        documentCheckList1.Loan_Contact__c = loanContact1.Id;
        documentCheckLists.add(documentCheckList1);
        insert documentCheckLists;
        
        // Create content documents for document check list
        List<ContentVersion> contentVersions = new List<ContentVersion>();
        
        ContentVersion contentVersion1 = new ContentVersion();
        contentVersion1.Description = 'Test';
        contentVersion1.Title = 'Test Content Document1';
        contentVersion1.OwnerId = UserInfo.getUserId();
        contentVersion1.VersionData = Blob.valueOf('Test');
        contentVersion1.PathOnClient = 'Bucket 1/'+contentVersion.Title+'.txt';
        contentVersion1.ContentLocation = 'S';
        contentVersions.add(contentVersion1);   
        
        // Create content version
        ContentVersion contentVersion2 = new ContentVersion();
        contentVersion2.Description = 'Test';
        contentVersion2.Title = 'Test Content Document2';
        contentVersion2.OwnerId = UserInfo.getUserId();
        contentVersion2.VersionData = Blob.valueOf('Test');
        contentVersion2.PathOnClient = 'Bucket 1/'+contentVersion2.Title+'.txt';
        contentVersion2.ContentLocation = 'S';
        contentVersions.add(contentVersion2);   
        
        // Create content version
        ContentVersion contentVersion3 = new ContentVersion();
        contentVersion3.Description = 'Test';
        contentVersion3.Title = 'Test Content Document3';
        contentVersion3.OwnerId = UserInfo.getUserId();
        contentVersion3.VersionData = Blob.valueOf('Test');
        contentVersion3.PathOnClient = 'Bucket 1/'+contentVersion3.Title+'.txt';
        contentVersion3.ContentLocation = 'S';
        contentVersions.add(contentVersion3);   
        
        // Create content version
        ContentVersion contentVersion4 = new ContentVersion();
        contentVersion4.Description = 'Test';
        contentVersion4.Title = 'Test Content Document4';
        contentVersion4.OwnerId = UserInfo.getUserId();
        contentVersion4.VersionData = Blob.valueOf('Test');
        contentVersion4.PathOnClient = 'Bucket 1/'+contentVersion4.Title+'.txt';
        contentVersion4.ContentLocation = 'S';
        contentVersions.add(contentVersion4);   
        
        // Insert the new content versions
        insert contentVersions;
        
        // Create links for check list
        contentVersions = [Select Id, ContentDocumentId From ContentVersion Where Id IN: contentVersions];
        
        // Links
        List<ContentDocumentLink> links = new List<ContentDocumentLink>();
        links.add(new ContentDocumentLink(LinkedEntityId = documentCheckLists[0].Id, ContentDocumentId = contentVersions[0].ContentDocumentId));
        links.add(new ContentDocumentLink(LinkedEntityId = documentCheckLists[0].Id, ContentDocumentId = contentVersions[1].ContentDocumentId));
        links.add(new ContentDocumentLink(LinkedEntityId = documentCheckLists[0].Id, ContentDocumentId = contentVersions[2].ContentDocumentId));
        links.add(new ContentDocumentLink(LinkedEntityId = loanApplication.Id, ContentDocumentId = contentVersions[3].ContentDocumentId));
        insert links;
        
        // Create attachments for loan application
        Attachment attachment = new Attachment(Name = 'Test Attachment');
        attachment.Body = Blob.valueOf('Test');
        attachment.ParentId = loanApplication.Id;
        insert attachment;
        
        NEILON__Folder__c nel = new NEILON__Folder__c();
        nel.Name = targetFolder;
        nel.NEILON__Amazon_File_Key__c = '323232';
        insert nel;
        
        
        cls_RetryMoveFiles.createS3FilesApplicant(objDoc);
        Test.stopTest();        
    }
    
    public static testMethod void test2(){
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
        insert con1;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;        

        Loan_Application__c la = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert la;
        
        Loan_Contact__c lc = [SELECT Id, Name,Applicant_Type__c, Applicant_Status__c, Constitution__c, Customer_segment__c, Loan_Applications__c FROM Loan_Contact__c WHERE Loan_Applications__c =: la.Id];
        lc.Borrower__c='2';
        lc.Contact__c = con1.Id;
        lc.Constitution__c='18';
        lc.Applicant_Status__c='1';
        lc.Customer_segment__c='9';
        lc.Mobile__c='9876543210';
        lc.Email__c='testemail@gmail.com';
        update lc;
        
        Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Co-Applicant').getRecordTypeId();
        Test.startTest();        
        Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=la.Id,Customer__c=acc.Id,
                                                             Applicant_Type__c='Guarantor',Borrower__c='1',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             Relationship_with_Applicant__c='Brother',
                                                             Pan_Verification_status__c=true,Constitution__c='20',
                                                             Customer_segment__c='1',Applicant_Status__c='1',
                                                             Category__c='1',
                                                             Father_s_Husband_s_Name__c='ABC',
                                                             GSTIN_Number__c='04AATPB2258C1Z3',
                               Mobile__c='9978786756',Email__c='avd@g.com');
        insert objLoanContact;
        
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=la.Id,
                                                                     Loan_Contact__c=objLoanContact.Id);
        insert objDocCheck;
        
        Document_Checklist__c objDocCheck2 = new Document_Checklist__c(Loan_Applications__c=la.Id,
                                                                     Loan_Contact__c=objLoanContact.Id);
        insert objDocCheck2;
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
         
        //Get Content Documents
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
        System.debug('conDocId-cv-'+conDocId);
        
        Document_Migration_Log__c objDoc = new Document_Migration_Log__c ();
        objDoc.Description__c = 'Test';
        objDoc.Loan_Application__c = la.Id;
        objDoc.Document_Checklist__c = objDocCheck.Id;
        objDoc.Document_Id__c = conDocId;
        insert objDoc;
        System.debug('objDoc--cv--'+objDoc.Document_Id__c);
        
        Attachment att=new Attachment();
        att.ParentId = objDoc.Loan_Application__c ;
        att.Name='test';
        att.Body=Blob.valueOf('test');
        insert att;
        System.debug('att--cv--'+att.Id);
               
        //Create ContentDocumentLink 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = objDoc.Loan_Application__c;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        Insert cdl;
        
        //Create ContentDocumentLink 
        ContentDocumentLink cdl1 = New ContentDocumentLink();
        cdl1.LinkedEntityId = objDocCheck.Id;
        cdl1.ContentDocumentId = conDocId;
        cdl1.shareType = 'V';
        Insert cdl1;
        
        cls_RetryMoveFiles.createS3FilesCoApplicantGuarantor(objDoc);
        cls_RetryMoveFiles.createS3FilesLoanKit(objDoc);
        cls_RetryMoveFiles.createS3FilesLoanKit2(objDoc);
     
        Test.stopTest();
    }
    
    public static testMethod void test3(){
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        
        Account acc = new Account(PAN__c='ABCDE1234F',Salutation='Mr',FirstName='Test',LastName='Test Account',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786758',RecordTypeId=RecordTypeIdAccount1);
        insert acc;
        
        Account acc1 = new Account(PAN__c='ABCDE1234G',Name='Test Account1',Date_of_Incorporation__c=System.today(),
                                  Phone='9989786750',RecordTypeId=RecordTypeIdAccount);
        insert acc1;
        
        Contact con1 = new Contact(LastName = 'Test Contact', MobilePhone='9234567890', AccountId = acc1.Id);
        insert con1;
        
        Scheme__c objScheme = new Scheme__c(Name='Test Scheme',Scheme_Code__c='1234567890',Product_Code__c='HL',
                                            Scheme_Group_ID__c='12');
        insert objScheme;        

        Loan_Application__c la = new Loan_Application__c(Customer__c=acc1.Id,Scheme__c=objScheme.Id,
                                                                        Transaction_type__c='PB',Requested_Amount__c=100000,
                                                                        Loan_Purpose__c='11');
        insert la;
        
        Test.startTest();        
       
        Document_Checklist__c objDocCheck = new Document_Checklist__c(Loan_Applications__c=la.Id);
        insert objDocCheck;
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
         
        //Get Content Documents
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
            
        Document_Migration_Log__c objDoc = new Document_Migration_Log__c ();
        objDoc.Description__c='Test';
        objDoc.Loan_Application__c=la.Id;
        objDoc.Document_Checklist__c = objDocCheck.Id;
        objDoc.Document_Id__c = conDocId;
        insert objDoc;
        
        Attachment att=new Attachment();
        att.ParentId = objDoc.Loan_Application__c ;
        att.Name='test';
        att.Body=Blob.valueOf('test');
        insert att;
        
        //Create ContentDocumentLink 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = objDocCheck.Id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        Insert cdl;
        
        /*NEILON__File__c objFile = new NEILON__File__c();
        objFile.NEILON__Description__c = cv.Title;
        /*objFile.Loan_Application__c = objDoc.Loan_Application__r.Id;
        objFile.NEILON__Export_Attachment_Id__c = cv.ContentDocumentId;
        objFile.Document_Checklist__c = cv.ContentDocumentId;
        objFile.Document_Master__c = objDocCheck.Document_Master__c;
        objFile.Document_Type__c = objDocCheck.Document_Type__c;
       /* objFile.Loan_Contact__c = lc.Id;
        objFile.Customer_Type__c = 'Co-Applicant'
        insert objFile;*/
            
            
        cls_RetryMoveFiles.createS3FilesApplication(objDoc);
       
        Test.stopTest();        
    }

}