@istest
public class TestCustIntegrationTriggerHelper{

 static testMethod void  testMethod1(){

    Scheme__c sch=new Scheme__c();
    sch.Product_Code__c='HLAP'; 
    sch.Scheme_Code__c='ELAP';
    sch.Scheme_Group_ID__c='Test';
    insert sch; 
 
    Loan_Application__c app=new Loan_Application__c();
    app.Branch__c='Test';
    app.Cancellation_Reason__c='Cancelled';
    app.Approved_ROI__c=2;
    app.Approval_Taken__c=true;
    app.Scheme__c=sch.id;
    app.STP__c='N';
    app.Loan_Number__c='1011379';
    app.Business_Date_Modified__c= Date.newInstance(2019, 08, 02);
    app.Requested_Amount__c= 450000;
    app.Approved_EMI__c= 47398;
    app.Requested_Loan_Tenure__c= 180;
    app.Loan_Purpose__c='25';
    app.Insurance_Loan_Application__c=false;
    app.Processing_Fee_Percentage__c=0;
    app.StageName__c='Credit Decisioning';
    app.Hunter_Expired__c=true;
    insert app;
    
    Account acc1=new Account();
    acc1.CountryCode__c='+91';
    acc1.Email__c='puser001@amamama.com';
    acc1.Gender__c='M';
    acc1.Mobile__c='9800765432';
    acc1.Phone__c='9800765434';
    acc1.Name='Test Account';
    insert acc1;
        
    Loan_Contact__c loan=new Loan_Contact__c();
    loan.Date_Of_Birth__c= Date.newInstance(2012, 12, 9);
    loan.Email__c='puser001@amamama.com';
    loan.Fax_Number__c= '8970090';
    loan.Gender__c='M';
    loan.Customer__c=acc1.id;
    loan.Loan_Applications__c= app.id;
    
    insert loan;
      Test.startTest();  
    //Id RecordTypeIdCust = [SELECT Id FROM RecordType    WHERE DeveloperName = 'Hunter_Analysis' AND sObjectType = 'Customer_Integration__c'].Id;
    Id recordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByDeveloperName()
                      .get('Hunter_Analysis').getRecordTypeId();//0120w000000JDlYAAW
    Customer_Integration__c cust = new Customer_Integration__c(Loan_Application__c = app.Id, Loan_Contact__c = loan.id,RecordtypeId = recordTypeId);
    insert cust;
    
    String perfiosFinancialResponse   =  '{"FinancialStatement": { "FY": [ { "year": 2017, "ProfitAndLoss": { "InterestPaid": 84.06881, "NonCashExpensesWrittenOff": 0, "InterestExpensesAndRentPaidToPartnersDirector": 0, "NetSalesNetOfExcise": 2114.58842, "OtherIncomeIncidentalToBusinessBusinessIncome": 0, "Tax": 0, "Depreciation": 2.49205, "OtherIncomeNonbusinessIncome": 33.03382, "AdministrativeAndSellingAndDistributionExpenses": 125.18806, "SalaryToPartnerDirector": 0, "Wages": 0, "ManufacturingExpenses": 53.55709, "RawMaterialCost": 1838.2405 }, "BalanceSheet": { "Liabilities": { "ShareCapital": 38.6958, "ReservesSurplusExcludingRevaluationReserve": 0, "RevaluationReserve": 0, "TermLoansFromBanksFi": 89.71718, "UnsecuredLoansFromPartnersShareholders": 0, "TradeCreditors": 67.36292, "UnsecuredLoansOthers": 0, "WorkingCapitalLimitsFromBanksFiS": 0, "OtherCurrentLiabilitiesProvisions": 342.53298 }, "Assets": { "CashAndBank": 25.56071, "Inventories": 57.07911, "LoansAdvancesGivenToDirectorsPartnersEtc": 0, "DebtorsLt6Months": 43.44221, "MiscExpensesDrePreopPreliminaryAccPL": 0, "GroupCoInvestments": 0, "DebtorsGt6Months": 0, "LiquidMarketableInvestments": 0, "FixedAssetsLessDepreciation": 13.67239, "UnquotedDeadInvestments": 0, "ShortTermLoansAndAdvancesGivenToOthers": 398.55446, "NonCurrentLoansAndAdvances": 0 } } }, { "year": 2016, "ProfitAndLoss": { "InterestPaid": 62.9494, "NonCashExpensesWrittenOff": 0, "InterestExpensesAndRentPaidToPartnersDirector": 0, "NetSalesNetOfExcise": 3168.56232, "OtherIncomeIncidentalToBusinessBusinessIncome": 0, "Tax": 0, "Depreciation": 6.63092, "OtherIncomeNonbusinessIncome": 2.45411, "AdministrativeAndSellingAndDistributionExpenses": 72.56841, "SalaryToPartnerDirector": 0, "Wages": 0, "ManufacturingExpenses": 0, "RawMaterialCost": 3007.39519 }, "BalanceSheet": { "Liabilities": { "ShareCapital": 35.49804, "ReservesSurplusExcludingRevaluationReserve": 0, "RevaluationReserve": 0, "TermLoansFromBanksFi": 648.67181, "UnsecuredLoansFromPartnersShareholders": 0, "TradeCreditors": 36.98478, "UnsecuredLoansOthers": 0, "WorkingCapitalLimitsFromBanksFiS": 0, "OtherCurrentLiabilitiesProvisions": 5.06756 }, "Assets": { "CashAndBank": 35.92789, "Inventories": 93.93522, "LoansAdvancesGivenToDirectorsPartnersEtc": 0, "DebtorsLt6Months": 447.93587, "MiscExpensesDrePreopPreliminaryAccPL": 0, "GroupCoInvestments": 0, "DebtorsGt6Months": 0, "LiquidMarketableInvestments": 0, "FixedAssetsLessDepreciation": 38.90692, "UnquotedDeadInvestments": 0, "ShortTermLoansAndAdvancesGivenToOthers": 109.51628, "NonCurrentLoansAndAdvances": 0 } } } ], "Organisation": "LUXMI AGENCY" }}';
    String perfiosITRResponse =  '{ "itrvDetails": [ { "acknowledgementNumber": "510782570170619", "assessmentYear": "2019-20", "dateOfSubmission": "2019-06-17", "designationOfAO": "WARD 2(2)(5) GHAZIABAD", "eFilingStatus": "ITR Processed", "financialYear": "2018-19", "formNumber": "ITR-1", "incomeDetail": { "currentYearLoss": "0", "deductionsUnderChapter6A": "161543", "exemptIncome": { "agriculture": "0", "others": "153278" }, "grossTotalIncome": "799888", "interestPayable": "0", "netTaxPayable": "41777", "refund": "12650", "taxPayable": "0", "taxesPaid": { "advanceTax": "0", "selfAssessmentTax": "0", "tcs": "0", "tds": "54425", "totalTaxPaid": "54425" }, "totalIncome": "638350", "totalTaxAndInterestPayable": "41777" }, "originalOrRevised": "", "personalInfo": { "address": "65 GOMTI NAGARNUMBER", "adhaarNumber": "NA", "name": "VINEET KUMAR TRIPATHI", "pan": "ALEPT8667P", "status": "Individual" } }, { "acknowledgementNumber": "949313700280718", "assessmentYear": "2018-19", "dateOfSubmission": "2018-07-28", "designationOfAO": "WARD 2(2)(5) GHAZIABAD", "eFilingStatus": "ITR Processed", "financialYear": "2017-18", "formNumber": "ITR-1", "incomeDetail": { "currentYearLoss": "0", "deductionsUnderChapter6A": "159064", "exemptIncome": { "agriculture": "0", "others": "118320" }, "grossTotalIncome": "908410", "interestPayable": "0", "netTaxPayable": "64241", "refund": "27770", "taxPayable": "0", "taxesPaid": { "advanceTax": "0", "selfAssessmentTax": "0", "tcs": "0", "tds": "92008", "totalTaxPaid": "92008" }, "totalIncome": "749350", "totalTaxAndInterestPayable": "64241" }, "originalOrRevised": "ORIGINAL", "personalInfo": { "address": "65 Virat Khand 4 Gomti Nagar Lucknow UTTAR PRADESH 226010", "adhaarNumber": "XXXX XXXX 6447", "name": "Vineet Kumar Tripathi", "pan": "ALEPT8667P", "status": "Individual" } }, { "acknowledgementNumber": "413303380270218", "assessmentYear": "2017-18", "dateOfSubmission": "2018-02-27", "designationOfAO": "WARD 2(2)(5) GHAZIABAD", "eFilingStatus": "ITR Processed", "financialYear": "2016-17", "formNumber": "ITR-1", "incomeDetail": { "currentYearLoss": "0", "deductionsUnderChapter6A": "158786", "exemptIncome": { "agriculture": "0", "others": "143091" }, "grossTotalIncome": "755282", "interestPayable": "0", "netTaxPayable": "45629", "refund": "0", "taxPayable": "0", "taxesPaid": { "advanceTax": "0", "selfAssessmentTax": "0", "tcs": "0", "tds": "45628", "totalTaxPaid": "45628" }, "totalIncome": "596500", "totalTaxAndInterestPayable": "45629" }, "originalOrRevised": "ORIGINAL", "personalInfo": { "address": "B 003 KAUSHAMBI SECTOR 18 GHAZIABAD UTTAR PRADESH 201001", "adhaarNumber": "XXXX XXXX 6447", "name": "VINEET KUMAR TRIPATHI", "pan": "ALEPT8667P", "status": "Individual" } } ] }';
    CustIntegrationTriggerHelper.parseFinancialStatementResponse(perfiosFinancialResponse,cust.id);
    CustIntegrationTriggerHelper.parseITRStatementResponse(perfiosITRResponse,cust.id);
    
    cust.Match_Count__c='1';
    update cust;
    Test.stopTest();
     

   }
   static testMethod void  testMethod2(){

    Scheme__c sch=new Scheme__c();
    sch.Product_Code__c='HLAP'; 
    sch.Scheme_Code__c='ELAP';
    sch.Scheme_Group_ID__c='Test';
    insert sch; 
 
    Loan_Application__c app=new Loan_Application__c();
    app.Branch__c='Test';
    app.Cancellation_Reason__c='Cancelled';
    app.Approved_ROI__c=2;
    app.Approval_Taken__c=true;
    app.Scheme__c=sch.id;
    app.STP__c='N';
    app.Loan_Number__c='1011379';
    app.Business_Date_Modified__c= Date.newInstance(2019, 08, 02);
    app.Requested_Amount__c= 450000;
    app.Approved_EMI__c= 47398;
    app.Requested_Loan_Tenure__c= 180;
    app.Loan_Purpose__c='25';
    app.Insurance_Loan_Application__c=false;
    app.Processing_Fee_Percentage__c=0;
    app.StageName__c='Credit Decisioning';
    app.Hunter_Expired__c=true;
    insert app;
    
    Account acc1=new Account();
    acc1.CountryCode__c='+91';
    acc1.Email__c='puser001@amamama.com';
    acc1.Gender__c='M';
    acc1.Mobile__c='9800765432';
    acc1.Phone__c='9800765434';
    acc1.Name='Test Account';
    insert acc1;
        
    Loan_Contact__c loan=new Loan_Contact__c();
    loan.Date_Of_Birth__c= Date.newInstance(2012, 12, 9);
    loan.Email__c='puser001@amamama.com';
    loan.Fax_Number__c= '8970090';
    loan.Gender__c='M';
    loan.Customer__c=acc1.id;
    loan.Loan_Applications__c= app.id;
    
    insert loan;
      Test.startTest();  
    //Id RecordTypeIdCust = [SELECT Id FROM RecordType    WHERE DeveloperName = 'Hunter_Analysis' AND sObjectType = 'Customer_Integration__c'].Id;
    Id recordTypeId = Schema.SObjectType.Customer_Integration__c.getRecordTypeInfosByDeveloperName()
                      .get('Hunter_Analysis').getRecordTypeId();//0120w000000JDlYAAW
    Customer_Integration__c cust = new Customer_Integration__c(Loan_Application__c = app.Id, Loan_Contact__c = loan.id,Manual__c=FALSE,RecordtypeId = recordTypeId,API_Type__c='ITR');
    insert cust;
    
    
    cust.Analysis_Status__c='Completed Successfully';
    update cust;
    Customer_Integration__c cust1 = new Customer_Integration__c(Loan_Application__c = app.Id, Loan_Contact__c = loan.id,Manual__c=FALSE,RecordtypeId = recordTypeId,API_Type__c='ScannedPDFBanking');
    insert cust1;
    
    
    cust1.Analysis_Status__c='Completed Successfully';
    update cust1;
    Test.stopTest();
     

   }
}