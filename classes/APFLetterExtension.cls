public with sharing class APFLetterExtension {
    public Project__c objProject{get;set;}
    public Date businessDate{get;set;}
    public String formattedDate{get;set;}
    public String APFApplicationNumber{get;set;}
    public List<String> lstAPFSpecialConditions{get;set;}
    public List<APF_Special_Condition__c> lstAPFSC{get;set;}
    public List<String> selectedPropertyPapers{get;set;}
    public APFLetterExtension(ApexPages.StandardController stdController) 
    {
        lstAPFSpecialConditions = new List<String>();
        selectedPropertyPapers = new List<String>();
        this.objProject = (Project__c)stdController.getRecord();
        businessDate = LMSDateUtility.lmsDateValue;
        formattedDate = DateTime.newInstance(businessDate.year(), businessDate.month(), businessDate.day()).format('dd-MMM-YYYY');
        objProject = [Select Id, Name, Property_Papers_Required__c, (select Id,Builder_Name__r.Name from Project_Builders__r), Project_Name__c,City__c, Account__c, Account__r.Name, Address_Line_1__c, Address_Line_2__c, Assigned_Sales_User__c, Assigned_Sales_User__r.Manager.Name, Assigned_Sales_User__r.Manager.MobilePhone From Project__c Where Id=: objProject.Id];
        if(objProject != null) {
            if(String.isNotBlank(objProject.City__c)) {
                APFApplicationNumber = 'HHFL'+objProject.City__c.subString(0,3)+objProject.Name.subStringAfterLast('-');
                
            }
            lstAPFSC = [Select Id, Name, Condition__c, Project__c From APF_Special_Condition__c WHERE Project__c =: objProject.Id];
            if(!lstAPFSC.isEmpty()) {
                for(APF_Special_Condition__c objAPFSpecialCondition : lstAPFSC) {
                   if(String.isNotBlank(objAPFSpecialCondition.Condition__c)) {
                       lstAPFSpecialConditions.add(objAPFSpecialCondition.Condition__c);
                   }
                }
            }
            System.debug('Debug log for lstAPFSpecialConditions'+lstAPFSpecialConditions);
            if(lstAPFSpecialConditions.isEmpty()) {
                lstAPFSpecialConditions.add('Not Applicable');
            }
            System.debug('Debug log for lstAPFSpecialConditions2'+lstAPFSpecialConditions);
            
            if(String.isNotBlank(objProject.Property_Papers_Required__c)) {
                selectedPropertyPapers.addAll(objProject.Property_Papers_Required__c.split(';'));
            }
            
            System.debug('Debug Log for selectedPropertyPapers'+selectedPropertyPapers);
            if(selectedPropertyPapers.isEmpty()) {
                selectedPropertyPapers.add('Not Specified');
            }
            System.debug('Debug Log for selectedPropertyPapers2'+selectedPropertyPapers);
        }
        
    }
}