@isTest
public class TestPDCBankMasterTrigger {
    public static Testmethod void insertPDC(){
        
    PDC_Bank_City__c pdccity = new PDC_Bank_City__c();
        pdccity.Name = 'Test City';
        pdccity.City_Id__c = '901';
        insert pdccity;
        
	PDC_Bank_Master__c pdc = new PDC_Bank_Master__c();
        pdc.Bank_Code__c = '002';
        pdc.Branch_ID__c = '016';
        pdc.Branch_Name__c = 'TestPDC';
        pdc.MICR__c = '333002016';
        pdc.Name_Bank__c = 'TestPDC';
        pdc.IFSC__c = 'UCBA0002044';
        pdc.PDC_Bank_City__c = pdccity.Id;
        insert pdc;
      
        List<PDC_Bank_Master__c> listToUpdatePDC = [Select id,Branch_ID__c from PDC_Bank_Master__c where Branch_ID__c = '016' LIMIT 1 ];
        for(PDC_Bank_Master__c pdcMaster : listToUpdatePDC){
           pdcMaster.Branch_Name__c = 'TestPDCCopy';
        }
        update listToUpdatePDC;
        
    
}
}