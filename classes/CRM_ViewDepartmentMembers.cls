public class CRM_ViewDepartmentMembers{
 
    @AuraEnabled
    public static List < CRM_Case_Escalation_Matrix_Setting__mdt > fetchDepartmentMembersmet() {
        
        //Case c=[SELECT id, Department__c FROM Case where id =:recordId];
        return [ SELECT Id, L1_Name__c, Department__c FROM CRM_Case_Escalation_Matrix_Setting__mdt order by Department__c];
    }
}