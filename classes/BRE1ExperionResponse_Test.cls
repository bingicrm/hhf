@istest
public class BRE1ExperionResponse_Test {

 static testMethod void  testPostRestService(){

    Scheme__c sch=new Scheme__c();
    sch.Product_Code__c='HLAP'; 
    sch.Scheme_Code__c='ELAP';
    sch.Scheme_Group_ID__c='Test';
    insert sch; 
 
    Loan_Application__c app=new Loan_Application__c();
    app.Branch__c='Test';
    app.Cancellation_Reason__c='Cancelled';
    app.Approved_ROI__c=2;
    app.Approval_Taken__c=true;
    app.Scheme__c=sch.id;
    app.STP__c='N';
    app.Loan_Number__c='1011379';
    app.Business_Date_Modified__c= Date.newInstance(2019, 08, 02);
    app.Requested_Amount__c= 450000;
    app.Approved_EMI__c= 47398;
    app.Requested_Loan_Tenure__c= 180;
    app.Loan_Purpose__c='25';
    app.Insurance_Loan_Application__c=false;
    app.Processing_Fee_Percentage__c=0;
    //app.Region__c='UTTAR PRADESH';
    app.StageName__c='Credit Decisioning';
    insert app;
    
    Account acc1=new Account();
    acc1.CountryCode__c='+91';
    acc1.Email__c='puser001@amamama.com';
    acc1.Gender__c='M';
    acc1.Mobile__c='9800765432';
    acc1.Phone__c='9800765434';
    //acc.Rejection_Reason__c='Rejected';
    acc1.Name='Test Account';
    // acc.Type=;
    insert acc1;
        
    Loan_Contact__c loan=new Loan_Contact__c();
    //loan.Age__c= 20;
    loan.Date_Of_Birth__c= Date.newInstance(2012, 12, 9);
    loan.Email__c='puser001@amamama.com';
    loan.Fax_Number__c= '8970090';
    loan.Gender__c='M';
    loan.Customer__c=acc1.id;
    loan.Loan_Applications__c= app.id;
    insert loan;
        
    Customer_Integration__c cust = new Customer_Integration__c(Loan_Application__c = app.Id, Loan_Contact__c = loan.id);
    insert cust;
    
    List<String> sortedreasoncodetable = new List<String>();
    sortedreasoncodetable.add('ABC');
    
    List<String> decisionstatus = new List<String>();
    decisionstatus.add('DEF');
    
    List<String> deviationcode = new List<String>();
    deviationcode.add('GHI');
    
    BRE1ExperionResponse.BureauSummary brSummary= new BRE1ExperionResponse.BureauSummary(cust.Id);
    BRE1ExperionResponse.ApplicantOut appOut = new BRE1ExperionResponse.ApplicantOut(loan.Id, 'Green', brSummary, sortedreasoncodetable, decisionstatus, deviationcode);
    BRE1ExperionResponse.Applicant applicant = new BRE1ExperionResponse.Applicant(appOut);
    BRE1ExperionResponse.Applicationout applicationOut = new BRE1ExperionResponse.Applicationout(app.Id);
    BRE1ExperionResponse.BRE1ExperionResp reqst=new BRE1ExperionResponse.BRE1ExperionResp(applicationOut, applicant);
   //reqst.acct=app;
   //reqst.cons=lstcon;

   String JsonMsg=JSON.serialize(reqst);

   Test.startTest();

  //As Per Best Practice it is important to instantiate the Rest Context 

  RestRequest req = new RestRequest(); 
   RestResponse res = new RestResponse();

    req.requestURI = '/services/apexrest/DemoUrl';  //Request URL
    req.httpMethod = 'POST';//HTTP Request Type
    req.requestBody = Blob.valueof(JsonMsg);
    RestContext.request = req;
    RestContext.response= res;


   BRE1ExperionResponse objTest= new BRE1ExperionResponse();
   BRE1ExperionResponse.parseBRE1ExperionResponse();
   BRE1ExperionResponse.parse(JsonMsg);
   //BRE1ExperionResponse.parseBRE1ExperionResponse resp =new BRE1ExperionResponse.parseBRE1ExperionResponse(); 
   //resp=BRE1ExperionResponse.doPost(reqst);  
   Test.stopTest();

   }
   
   static testMethod void  testPostRestService1(){

    Scheme__c sch=new Scheme__c();
    sch.Product_Code__c='HLAP'; 
    sch.Scheme_Code__c='ELAP';
    sch.Scheme_Group_ID__c='Test';
    insert sch; 
 
    Loan_Application__c app=new Loan_Application__c();
    app.Branch__c='Test';
    app.Cancellation_Reason__c='Cancelled';
    app.Approved_ROI__c=2;
    app.Approval_Taken__c=true;
    app.Scheme__c=sch.id;
    app.STP__c='N';
    app.Loan_Number__c='1011379';
    app.Business_Date_Modified__c= Date.newInstance(2019, 08, 02);
    app.Requested_Amount__c= 450000;
    app.Approved_EMI__c= 47398;
    app.Requested_Loan_Tenure__c= 180;
    app.Loan_Purpose__c='25';
    app.Insurance_Loan_Application__c=false;
    app.Processing_Fee_Percentage__c=0;
    //app.Region__c='UTTAR PRADESH';
    app.StageName__c='Credit Decisioning';
    insert app;
    
    Account acc1=new Account();
    acc1.CountryCode__c='+91';
    acc1.Email__c='puser001@amamama.com';
    acc1.Gender__c='M';
    acc1.Mobile__c='9800765432';
    acc1.Phone__c='9800765434';
    //acc.Rejection_Reason__c='Rejected';
    acc1.Name='Test Account';
    // acc.Type=;
    insert acc1;
        
    Loan_Contact__c loan=new Loan_Contact__c();
    //loan.Age__c= 20;
    loan.Date_Of_Birth__c= Date.newInstance(2012, 12, 9);
    loan.Email__c='puser001@amamama.com';
    loan.Fax_Number__c= '8970090';
    loan.Gender__c='M';
    loan.Customer__c=acc1.id;
    loan.Loan_Applications__c= app.id;
    insert loan;
        
    Customer_Integration__c cust = new Customer_Integration__c(Loan_Application__c = app.Id, Loan_Contact__c = loan.id);
    insert cust;
    
    List<String> sortedreasoncodetable = new List<String>();
    sortedreasoncodetable.add('ABC');
    
    List<String> decisionstatus = new List<String>();
    decisionstatus.add('DEF');
    
    List<String> deviationcode = new List<String>();
    deviationcode.add('GHI');
    
    BRE1ExperionResponse.BureauSummary brSummary= new BRE1ExperionResponse.BureauSummary(cust.Id);
    BRE1ExperionResponse.ApplicantOut appOut = new BRE1ExperionResponse.ApplicantOut(loan.Id, 'Green', brSummary, sortedreasoncodetable, decisionstatus, deviationcode);
    BRE1ExperionResponse.Applicant applicant = new BRE1ExperionResponse.Applicant(appOut);
    BRE1ExperionResponse.Applicationout applicationOut = new BRE1ExperionResponse.Applicationout('');
    BRE1ExperionResponse.BRE1ExperionResp reqst=new BRE1ExperionResponse.BRE1ExperionResp(applicationOut, applicant);
   //reqst.acct=app;
   //reqst.cons=lstcon;

   String JsonMsg=JSON.serialize(reqst);

   Test.startTest();

  //As Per Best Practice it is important to instantiate the Rest Context 

  RestRequest req = new RestRequest(); 
   RestResponse res = new RestResponse();

    req.requestURI = '/services/apexrest/DemoUrl';  //Request URL
    req.httpMethod = 'POST';//HTTP Request Type
    req.requestBody = Blob.valueof(JsonMsg);
    RestContext.request = req;
    RestContext.response= res;


   BRE1ExperionResponse objTest= new BRE1ExperionResponse();
   BRE1ExperionResponse.parseBRE1ExperionResponse();
   BRE1ExperionResponse.parse(JsonMsg);
   //BRE1ExperionResponse.parseBRE1ExperionResponse resp =new BRE1ExperionResponse.parseBRE1ExperionResponse(); 
   //resp=BRE1ExperionResponse.doPost(reqst);  
   Test.stopTest();

   }
   
   static testMethod void  testPostRestService2(){

    Scheme__c sch=new Scheme__c();
    sch.Product_Code__c='HLAP'; 
    sch.Scheme_Code__c='ELAP';
    sch.Scheme_Group_ID__c='Test';
    insert sch; 
 
    Loan_Application__c app=new Loan_Application__c();
    app.Branch__c='Test';
    app.Cancellation_Reason__c='Cancelled';
    app.Approved_ROI__c=2;
    app.Approval_Taken__c=true;
    app.Scheme__c=sch.id;
    app.STP__c='N';
    app.Loan_Number__c='1011379';
    app.Business_Date_Modified__c= Date.newInstance(2019, 08, 02);
    app.Requested_Amount__c= 450000;
    app.Approved_EMI__c= 47398;
    app.Requested_Loan_Tenure__c= 180;
    app.Loan_Purpose__c='25';
    app.Insurance_Loan_Application__c=false;
    app.Processing_Fee_Percentage__c=0;
    //app.Region__c='UTTAR PRADESH';
    app.StageName__c='Credit Decisioning';
    insert app;
    
    Account acc1=new Account();
    acc1.CountryCode__c='+91';
    acc1.Email__c='puser001@amamama.com';
    acc1.Gender__c='M';
    acc1.Mobile__c='9800765432';
    acc1.Phone__c='9800765434';
    //acc.Rejection_Reason__c='Rejected';
    acc1.Name='Test Account';
    // acc.Type=;
    insert acc1;
        
    Loan_Contact__c loan=new Loan_Contact__c();
    //loan.Age__c= 20;
    loan.Date_Of_Birth__c= Date.newInstance(2012, 12, 9);
    loan.Email__c='puser001@amamama.com';
    loan.Fax_Number__c= '8970090';
    loan.Gender__c='M';
    loan.Customer__c=acc1.id;
    loan.Loan_Applications__c= app.id;
    insert loan;
        
    Customer_Integration__c cust = new Customer_Integration__c(Loan_Application__c = app.Id, Loan_Contact__c = loan.id);
    insert cust;
    
    List<String> sortedreasoncodetable = new List<String>();
    sortedreasoncodetable.add('ABC');
    
    List<String> decisionstatus = new List<String>();
    decisionstatus.add('DEF');
    
    List<String> deviationcode = new List<String>();
    deviationcode.add('GHI');
    
    BRE1ExperionResponse.BureauSummary brSummary= new BRE1ExperionResponse.BureauSummary(cust.Id);
    BRE1ExperionResponse.ApplicantOut appOut = new BRE1ExperionResponse.ApplicantOut('', 'Green', brSummary, sortedreasoncodetable, decisionstatus, deviationcode);
    BRE1ExperionResponse.Applicant applicant = new BRE1ExperionResponse.Applicant(appOut);
    BRE1ExperionResponse.Applicationout applicationOut = new BRE1ExperionResponse.Applicationout(app.Id);
    BRE1ExperionResponse.BRE1ExperionResp reqst=new BRE1ExperionResponse.BRE1ExperionResp(applicationOut, applicant);
   //reqst.acct=app;
   //reqst.cons=lstcon;

   String JsonMsg=JSON.serialize(reqst);

   Test.startTest();

  //As Per Best Practice it is important to instantiate the Rest Context 

  RestRequest req = new RestRequest(); 
   RestResponse res = new RestResponse();

    req.requestURI = '/services/apexrest/DemoUrl';  //Request URL
    req.httpMethod = 'POST';//HTTP Request Type
    req.requestBody = Blob.valueof(JsonMsg);
    RestContext.request = req;
    RestContext.response= res;


   BRE1ExperionResponse objTest= new BRE1ExperionResponse();
   BRE1ExperionResponse.parseBRE1ExperionResponse();
   BRE1ExperionResponse.parse(JsonMsg);
   //BRE1ExperionResponse.parseBRE1ExperionResponse resp =new BRE1ExperionResponse.parseBRE1ExperionResponse(); 
   //resp=BRE1ExperionResponse.doPost(reqst);  
   Test.stopTest();

   }
   static testMethod void  testPostRestService3(){

    Scheme__c sch=new Scheme__c();
    sch.Product_Code__c='HLAP'; 
    sch.Scheme_Code__c='ELAP';
    sch.Scheme_Group_ID__c='Test';
    insert sch; 
 
    Loan_Application__c app=new Loan_Application__c();
    app.Branch__c='Test';
    app.Cancellation_Reason__c='Cancelled';
    app.Approved_ROI__c=2;
    app.Approval_Taken__c=true;
    app.Scheme__c=sch.id;
    app.STP__c='N';
    app.Loan_Number__c='1011379';
    app.Business_Date_Modified__c= Date.newInstance(2019, 08, 02);
    app.Requested_Amount__c= 450000;
    app.Approved_EMI__c= 47398;
    app.Requested_Loan_Tenure__c= 180;
    app.Loan_Purpose__c='25';
    app.Insurance_Loan_Application__c=false;
    app.Processing_Fee_Percentage__c=0;
    //app.Region__c='UTTAR PRADESH';
    app.StageName__c='Credit Decisioning';
    insert app;
    
    Account acc1=new Account();
    acc1.CountryCode__c='+91';
    acc1.Email__c='puser001@amamama.com';
    acc1.Gender__c='M';
    acc1.Mobile__c='9800765432';
    acc1.Phone__c='9800765434';
    //acc.Rejection_Reason__c='Rejected';
    acc1.Name='Test Account';
    // acc.Type=;
    insert acc1;
        
    Loan_Contact__c loan=new Loan_Contact__c();
    //loan.Age__c= 20;
    loan.Date_Of_Birth__c= Date.newInstance(2012, 12, 9);
    loan.Email__c='puser001@amamama.com';
    loan.Fax_Number__c= '8970090';
    loan.Gender__c='M';
    loan.Customer__c=acc1.id;
    loan.Loan_Applications__c= app.id;
    insert loan;
        
    Customer_Integration__c cust = new Customer_Integration__c(Loan_Application__c = app.Id, Loan_Contact__c = loan.id);
    insert cust;
    
    List<String> sortedreasoncodetable = new List<String>();
    sortedreasoncodetable.add('ABC');
    
    List<String> decisionstatus = new List<String>();
    decisionstatus.add('DEF');
    
    List<String> deviationcode = new List<String>();
    deviationcode.add('GHI');
    
    BRE1ExperionResponse.BureauSummary brSummary= new BRE1ExperionResponse.BureauSummary('');
    BRE1ExperionResponse.ApplicantOut appOut = new BRE1ExperionResponse.ApplicantOut(loan.Id, 'Green', brSummary, sortedreasoncodetable, decisionstatus, deviationcode);
    BRE1ExperionResponse.Applicant applicant = new BRE1ExperionResponse.Applicant(appOut);
    BRE1ExperionResponse.Applicationout applicationOut = new BRE1ExperionResponse.Applicationout(app.Id);
    BRE1ExperionResponse.BRE1ExperionResp reqst=new BRE1ExperionResponse.BRE1ExperionResp(applicationOut, applicant);
   //reqst.acct=app;
   //reqst.cons=lstcon;

   String JsonMsg=JSON.serialize(reqst);

   Test.startTest();

  //As Per Best Practice it is important to instantiate the Rest Context 

  RestRequest req = new RestRequest(); 
   RestResponse res = new RestResponse();

    req.requestURI = '/services/apexrest/DemoUrl';  //Request URL
    req.httpMethod = 'POST';//HTTP Request Type
    req.requestBody = Blob.valueof(JsonMsg);
    RestContext.request = req;
    RestContext.response= res;


   BRE1ExperionResponse objTest= new BRE1ExperionResponse();
   BRE1ExperionResponse.parseBRE1ExperionResponse();
   BRE1ExperionResponse.parse(JsonMsg);
   //BRE1ExperionResponse.parseBRE1ExperionResponse resp =new BRE1ExperionResponse.parseBRE1ExperionResponse(); 
   //resp=BRE1ExperionResponse.doPost(reqst);  
   Test.stopTest();

   }
   
   static testMethod void  testPostRestService4(){

    Scheme__c sch=new Scheme__c();
    sch.Product_Code__c='HLAP'; 
    sch.Scheme_Code__c='ELAP';
    sch.Scheme_Group_ID__c='Test';
    insert sch; 
 
    Loan_Application__c app=new Loan_Application__c();
    app.Branch__c='Test';
    app.Cancellation_Reason__c='Cancelled';
    app.Approved_ROI__c=2;
    app.Approval_Taken__c=true;
    app.Scheme__c=sch.id;
    app.STP__c='N';
    app.Loan_Number__c='1011379';
    app.Business_Date_Modified__c= Date.newInstance(2019, 08, 02);
    app.Requested_Amount__c= 450000;
    app.Approved_EMI__c= 47398;
    app.Requested_Loan_Tenure__c= 180;
    app.Loan_Purpose__c='25';
    app.Insurance_Loan_Application__c=false;
    app.Processing_Fee_Percentage__c=0;
    //app.Region__c='UTTAR PRADESH';
    app.StageName__c='Credit Decisioning';
    insert app;
    
    Account acc1=new Account();
    acc1.CountryCode__c='+91';
    acc1.Email__c='puser001@amamama.com';
    acc1.Gender__c='M';
    acc1.Mobile__c='9800765432';
    acc1.Phone__c='9800765434';
    //acc.Rejection_Reason__c='Rejected';
    acc1.Name='Test Account';
    // acc.Type=;
    insert acc1;
        
    Loan_Contact__c loan=new Loan_Contact__c();
    //loan.Age__c= 20;
    loan.Date_Of_Birth__c= Date.newInstance(2012, 12, 9);
    loan.Email__c='puser001@amamama.com';
    loan.Fax_Number__c= '8970090';
    loan.Gender__c='M';
    loan.Customer__c=acc1.id;
    loan.Loan_Applications__c= app.id;
    insert loan;
        
    Customer_Integration__c cust = new Customer_Integration__c(Loan_Application__c = app.Id, Loan_Contact__c = loan.id);
    insert cust;
    
    List<String> sortedreasoncodetable = new List<String>();
    sortedreasoncodetable.add('ABC');
    
    List<String> decisionstatus = new List<String>();
    decisionstatus.add('DEF');
    
    List<String> deviationcode = new List<String>();
    deviationcode.add('GHI');
    
    BRE1ExperionResponse.BureauSummary brSummary= new BRE1ExperionResponse.BureauSummary(cust.Id);
    BRE1ExperionResponse.ApplicantOut appOut = new BRE1ExperionResponse.ApplicantOut(loan.Id, 'Green', brSummary, sortedreasoncodetable, decisionstatus, deviationcode);
    BRE1ExperionResponse.Applicant applicant = new BRE1ExperionResponse.Applicant(appOut);
    BRE1ExperionResponse.Applicationout applicationOut = new BRE1ExperionResponse.Applicationout('3562356');
    BRE1ExperionResponse.BRE1ExperionResp reqst=new BRE1ExperionResponse.BRE1ExperionResp(applicationOut, applicant);
   //reqst.acct=app;
   //reqst.cons=lstcon;

   String JsonMsg=JSON.serialize(reqst);

   Test.startTest();

  //As Per Best Practice it is important to instantiate the Rest Context 

  RestRequest req = new RestRequest(); 
   RestResponse res = new RestResponse();

    req.requestURI = '/services/apexrest/DemoUrl';  //Request URL
    req.httpMethod = 'POST';//HTTP Request Type
    req.requestBody = Blob.valueof(JsonMsg);
    RestContext.request = req;
    RestContext.response= res;


   BRE1ExperionResponse objTest= new BRE1ExperionResponse();
   BRE1ExperionResponse.parseBRE1ExperionResponse();
   BRE1ExperionResponse.parse(JsonMsg);
   //BRE1ExperionResponse.parseBRE1ExperionResponse resp =new BRE1ExperionResponse.parseBRE1ExperionResponse(); 
   //resp=BRE1ExperionResponse.doPost(reqst);  
   Test.stopTest();

   }
   
   static testMethod void  testPostRestService5(){

    Scheme__c sch=new Scheme__c();
    sch.Product_Code__c='HLAP'; 
    sch.Scheme_Code__c='ELAP';
    sch.Scheme_Group_ID__c='Test';
    insert sch; 
 
    Loan_Application__c app=new Loan_Application__c();
    app.Branch__c='Test';
    app.Cancellation_Reason__c='Cancelled';
    app.Approved_ROI__c=2;
    app.Approval_Taken__c=true;
    app.Scheme__c=sch.id;
    app.STP__c='N';
    app.Loan_Number__c='1011379';
    app.Business_Date_Modified__c= Date.newInstance(2019, 08, 02);
    app.Requested_Amount__c= 450000;
    app.Approved_EMI__c= 47398;
    app.Requested_Loan_Tenure__c= 180;
    app.Loan_Purpose__c='25';
    app.Insurance_Loan_Application__c=false;
    app.Processing_Fee_Percentage__c=0;
    //app.Region__c='UTTAR PRADESH';
    app.StageName__c='Credit Decisioning';
    insert app;
    
    Account acc1=new Account();
    acc1.CountryCode__c='+91';
    acc1.Email__c='puser001@amamama.com';
    acc1.Gender__c='M';
    acc1.Mobile__c='9800765432';
    acc1.Phone__c='9800765434';
    //acc.Rejection_Reason__c='Rejected';
    acc1.Name='Test Account';
    // acc.Type=;
    insert acc1;
        
    Loan_Contact__c loan=new Loan_Contact__c();
    //loan.Age__c= 20;
    loan.Date_Of_Birth__c= Date.newInstance(2012, 12, 9);
    loan.Email__c='puser001@amamama.com';
    loan.Fax_Number__c= '8970090';
    loan.Gender__c='M';
    loan.Customer__c=acc1.id;
    loan.Loan_Applications__c= app.id;
    insert loan;
        
    Customer_Integration__c cust = new Customer_Integration__c(Loan_Application__c = app.Id, Loan_Contact__c = loan.id);
    insert cust;
    
    List<String> sortedreasoncodetable = new List<String>();
    sortedreasoncodetable.add('ABC');
    
    List<String> decisionstatus = new List<String>();
    decisionstatus.add('DEF');
    
    List<String> deviationcode = new List<String>();
    deviationcode.add('GHI');
    
    BRE1ExperionResponse.BureauSummary brSummary= new BRE1ExperionResponse.BureauSummary(cust.Id);
    BRE1ExperionResponse.ApplicantOut appOut = new BRE1ExperionResponse.ApplicantOut('5256256', 'Green', brSummary, sortedreasoncodetable, decisionstatus, deviationcode);
    BRE1ExperionResponse.Applicant applicant = new BRE1ExperionResponse.Applicant(appOut);
    BRE1ExperionResponse.Applicationout applicationOut = new BRE1ExperionResponse.Applicationout(app.Id);
    BRE1ExperionResponse.BRE1ExperionResp reqst=new BRE1ExperionResponse.BRE1ExperionResp(applicationOut, applicant);
   //reqst.acct=app;
   //reqst.cons=lstcon;

   String JsonMsg=JSON.serialize(reqst);

   Test.startTest();

  //As Per Best Practice it is important to instantiate the Rest Context 

  RestRequest req = new RestRequest(); 
   RestResponse res = new RestResponse();

    req.requestURI = '/services/apexrest/DemoUrl';  //Request URL
    req.httpMethod = 'POST';//HTTP Request Type
    req.requestBody = Blob.valueof(JsonMsg);
    RestContext.request = req;
    RestContext.response= res;


   BRE1ExperionResponse objTest= new BRE1ExperionResponse();
   BRE1ExperionResponse.parseBRE1ExperionResponse();
   BRE1ExperionResponse.parse(JsonMsg);
   //BRE1ExperionResponse.parseBRE1ExperionResponse resp =new BRE1ExperionResponse.parseBRE1ExperionResponse(); 
   //resp=BRE1ExperionResponse.doPost(reqst);  
   Test.stopTest();

   }
   static testMethod void  testPostRestService6(){

    Scheme__c sch=new Scheme__c();
    sch.Product_Code__c='HLAP'; 
    sch.Scheme_Code__c='ELAP';
    sch.Scheme_Group_ID__c='Test';
    insert sch; 
 
    Loan_Application__c app=new Loan_Application__c();
    app.Branch__c='Test';
    app.Cancellation_Reason__c='Cancelled';
    app.Approved_ROI__c=2;
    app.Approval_Taken__c=true;
    app.Scheme__c=sch.id;
    app.STP__c='N';
    app.Loan_Number__c='1011379';
    app.Business_Date_Modified__c= Date.newInstance(2019, 08, 02);
    app.Requested_Amount__c= 450000;
    app.Approved_EMI__c= 47398;
    app.Requested_Loan_Tenure__c= 180;
    app.Loan_Purpose__c='25';
    app.Insurance_Loan_Application__c=false;
    app.Processing_Fee_Percentage__c=0;
    //app.Region__c='UTTAR PRADESH';
    app.StageName__c='Credit Decisioning';
    insert app;
    
    Account acc1=new Account();
    acc1.CountryCode__c='+91';
    acc1.Email__c='puser001@amamama.com';
    acc1.Gender__c='M';
    acc1.Mobile__c='9800765432';
    acc1.Phone__c='9800765434';
    //acc.Rejection_Reason__c='Rejected';
    acc1.Name='Test Account';
    // acc.Type=;
    insert acc1;
        
    Loan_Contact__c loan=new Loan_Contact__c();
    //loan.Age__c= 20;
    loan.Date_Of_Birth__c= Date.newInstance(2012, 12, 9);
    loan.Email__c='puser001@amamama.com';
    loan.Fax_Number__c= '8970090';
    loan.Gender__c='M';
    loan.Customer__c=acc1.id;
    loan.Loan_Applications__c= app.id;
    insert loan;
        
    Customer_Integration__c cust = new Customer_Integration__c(Loan_Application__c = app.Id, Loan_Contact__c = loan.id);
    insert cust;
    
    List<String> sortedreasoncodetable = new List<String>();
    sortedreasoncodetable.add('ABC');
    
    List<String> decisionstatus = new List<String>();
    decisionstatus.add('DEF');
    
    List<String> deviationcode = new List<String>();
    deviationcode.add('GHI');
    
    BRE1ExperionResponse.BureauSummary brSummary= new BRE1ExperionResponse.BureauSummary('3625656');
    BRE1ExperionResponse.ApplicantOut appOut = new BRE1ExperionResponse.ApplicantOut(loan.Id, 'Green', brSummary, sortedreasoncodetable, decisionstatus, deviationcode);
    BRE1ExperionResponse.Applicant applicant = new BRE1ExperionResponse.Applicant(appOut);
    BRE1ExperionResponse.Applicationout applicationOut = new BRE1ExperionResponse.Applicationout(app.Id);
    BRE1ExperionResponse.BRE1ExperionResp reqst=new BRE1ExperionResponse.BRE1ExperionResp(applicationOut, applicant);
   //reqst.acct=app;
   //reqst.cons=lstcon;

   String JsonMsg=JSON.serialize(reqst);

   Test.startTest();

  //As Per Best Practice it is important to instantiate the Rest Context 

  RestRequest req = new RestRequest(); 
   RestResponse res = new RestResponse();

    req.requestURI = '/services/apexrest/DemoUrl';  //Request URL
    req.httpMethod = 'POST';//HTTP Request Type
    req.requestBody = Blob.valueof(JsonMsg);
    RestContext.request = req;
    RestContext.response= res;


   BRE1ExperionResponse objTest= new BRE1ExperionResponse();
   BRE1ExperionResponse.parseBRE1ExperionResponse();
   BRE1ExperionResponse.parse(JsonMsg);
   //BRE1ExperionResponse.parseBRE1ExperionResponse resp =new BRE1ExperionResponse.parseBRE1ExperionResponse(); 
   //resp=BRE1ExperionResponse.doPost(reqst);  
   Test.stopTest();

   }
   
   static testMethod void  testPostRestService7(){

    Scheme__c sch=new Scheme__c();
    sch.Product_Code__c='HLAP'; 
    sch.Scheme_Code__c='ELAP';
    sch.Scheme_Group_ID__c='Test';
    insert sch; 
 
    Loan_Application__c app=new Loan_Application__c();
    app.Branch__c='Test';
    app.Cancellation_Reason__c='Cancelled';
    app.Approved_ROI__c=2;
    app.Approval_Taken__c=true;
    app.Scheme__c=sch.id;
    app.STP__c='N';
    app.Loan_Number__c='1011379';
    app.Business_Date_Modified__c= Date.newInstance(2019, 08, 02);
    app.Requested_Amount__c= 450000;
    app.Approved_EMI__c= 47398;
    app.Requested_Loan_Tenure__c= 180;
    app.Loan_Purpose__c='25';
    app.Insurance_Loan_Application__c=false;
    app.Processing_Fee_Percentage__c=0;
    //app.Region__c='UTTAR PRADESH';
    app.StageName__c='Credit Decisioning';
    insert app;
    
    Account acc1=new Account();
    acc1.CountryCode__c='+91';
    acc1.Email__c='puser001@amamama.com';
    acc1.Gender__c='M';
    acc1.Mobile__c='9800765432';
    acc1.Phone__c='9800765434';
    //acc.Rejection_Reason__c='Rejected';
    acc1.Name='Test Account';
    // acc.Type=;
    insert acc1;
        
    Loan_Contact__c loan=new Loan_Contact__c();
    //loan.Age__c= 20;
    loan.Date_Of_Birth__c= Date.newInstance(2012, 12, 9);
    loan.Email__c='puser001@amamama.com';
    loan.Fax_Number__c= '8970090';
    loan.Gender__c='M';
    loan.Customer__c=acc1.id;
    loan.Loan_Applications__c= app.id;
    insert loan;
        
    Customer_Integration__c cust = new Customer_Integration__c(Loan_Application__c = app.Id, Loan_Contact__c = loan.id);
    insert cust;
    
    List<String> sortedreasoncodetable = new List<String>();
    sortedreasoncodetable.add('ABC');
    
    List<String> decisionstatus = new List<String>();
    decisionstatus.add('DEF');
    
    List<String> deviationcode = new List<String>();
    deviationcode.add('GHI');
    
    List<BRE1ExperionResponse.Error> error =  new List<BRE1ExperionResponse.Error>();
    BRE1ExperionResponse.Error err = new BRE1ExperionResponse.Error('error','error');
    error.add(err);
    
    BRE1ExperionResponse.BureauSummary brSummary= new BRE1ExperionResponse.BureauSummary(cust.Id);
    BRE1ExperionResponse.ApplicantOut appOut = new BRE1ExperionResponse.ApplicantOut(loan.Id, 'Green', brSummary, sortedreasoncodetable, decisionstatus, deviationcode);
    BRE1ExperionResponse.Applicant applicant = new BRE1ExperionResponse.Applicant(appOut);
    BRE1ExperionResponse.Applicationout applicationOut = new BRE1ExperionResponse.Applicationout(app.Id, error);
    BRE1ExperionResponse.BRE1ExperionResp reqst=new BRE1ExperionResponse.BRE1ExperionResp(applicationOut, applicant);
   //reqst.acct=app;
   //reqst.cons=lstcon;

   String JsonMsg=JSON.serialize(reqst);

   Test.startTest();

  //As Per Best Practice it is important to instantiate the Rest Context 

  RestRequest req = new RestRequest(); 
   RestResponse res = new RestResponse();

    req.requestURI = '/services/apexrest/DemoUrl';  //Request URL
    req.httpMethod = 'POST';//HTTP Request Type
    req.requestBody = Blob.valueof(JsonMsg);
    RestContext.request = req;
    RestContext.response= res;


   BRE1ExperionResponse objTest= new BRE1ExperionResponse();
   BRE1ExperionResponse.parseBRE1ExperionResponse();
   BRE1ExperionResponse.parse(JsonMsg);
   //BRE1ExperionResponse.parseBRE1ExperionResponse resp =new BRE1ExperionResponse.parseBRE1ExperionResponse(); 
   //resp=BRE1ExperionResponse.doPost(reqst);  
   Test.stopTest();

   }
   
   static testMethod void  testPostRestService8(){

    Scheme__c sch=new Scheme__c();
    sch.Product_Code__c='HLAP'; 
    sch.Scheme_Code__c='ELAP';
    sch.Scheme_Group_ID__c='Test';
    insert sch; 
 
    Loan_Application__c app=new Loan_Application__c();
    app.Branch__c='Test';
    app.Cancellation_Reason__c='Cancelled';
    app.Approved_ROI__c=2;
    app.Approval_Taken__c=true;
    app.Scheme__c=sch.id;
    app.STP__c='N';
    app.Loan_Number__c='1011379';
    app.Business_Date_Modified__c= Date.newInstance(2019, 08, 02);
    app.Requested_Amount__c= 450000;
    app.Approved_EMI__c= 47398;
    app.Requested_Loan_Tenure__c= 180;
    app.Loan_Purpose__c='25';
    app.Insurance_Loan_Application__c=false;
    app.Processing_Fee_Percentage__c=0;
    //app.Region__c='UTTAR PRADESH';
    app.StageName__c='Credit Decisioning';
    insert app;
    
    Account acc1=new Account();
    acc1.CountryCode__c='+91';
    acc1.Email__c='puser001@amamama.com';
    acc1.Gender__c='M';
    acc1.Mobile__c='9800765432';
    acc1.Phone__c='9800765434';
    //acc.Rejection_Reason__c='Rejected';
    acc1.Name='Test Account';
    // acc.Type=;
    insert acc1;
        
    Loan_Contact__c loan=new Loan_Contact__c();
    //loan.Age__c= 20;
    loan.Date_Of_Birth__c= Date.newInstance(2012, 12, 9);
    loan.Email__c='puser001@amamama.com';
    loan.Fax_Number__c= '8970090';
    loan.Gender__c='M';
    loan.Customer__c=acc1.id;
    loan.Loan_Applications__c= app.id;
    insert loan;
        
    Customer_Integration__c cust = new Customer_Integration__c(Loan_Application__c = app.Id, Loan_Contact__c = loan.id);
    insert cust;
    
    List<String> sortedreasoncodetable = new List<String>();
    sortedreasoncodetable.add('ABC');
    sortedreasoncodetable.add('AHDG');
    
    
    List<String> decisionstatus = new List<String>();
    decisionstatus.add('DEF');
    
    List<String> deviationcode = new List<String>();
    deviationcode.add('GHI');
    
    BRE1ExperionResponse.BureauSummary brSummary= new BRE1ExperionResponse.BureauSummary(cust.Id);
    BRE1ExperionResponse.ApplicantOut appOut = new BRE1ExperionResponse.ApplicantOut(loan.Id, 'Green', brSummary, sortedreasoncodetable, decisionstatus, deviationcode);
    BRE1ExperionResponse.Applicant applicant = new BRE1ExperionResponse.Applicant(appOut);
    BRE1ExperionResponse.Applicationout applicationOut = new BRE1ExperionResponse.Applicationout(app.Id);
    BRE1ExperionResponse.BRE1ExperionResp reqst=new BRE1ExperionResponse.BRE1ExperionResp(applicationOut, applicant);
   //reqst.acct=app;
   //reqst.cons=lstcon;

   String JsonMsg=JSON.serialize(reqst);

   Test.startTest();

  //As Per Best Practice it is important to instantiate the Rest Context 

  RestRequest req = new RestRequest(); 
   RestResponse res = new RestResponse();

    req.requestURI = '/services/apexrest/DemoUrl';  //Request URL
    req.httpMethod = 'POST';//HTTP Request Type
    req.requestBody = Blob.valueof(JsonMsg);
    RestContext.request = req;
    RestContext.response= res;


   BRE1ExperionResponse objTest= new BRE1ExperionResponse();
   BRE1ExperionResponse.parseBRE1ExperionResponse();
   BRE1ExperionResponse.parse(JsonMsg);
   //BRE1ExperionResponse.parseBRE1ExperionResponse resp =new BRE1ExperionResponse.parseBRE1ExperionResponse(); 
   //resp=BRE1ExperionResponse.doPost(reqst);  
   Test.stopTest();

   }
}