public with sharing class listviewController {
@AuraEnabled
public static List<ListView> getListViews() {
    List<ListView> listviews = 
        [SELECT Id, Name FROM ListView WHERE SobjectType = 'Third_Party_Verification__c' limit 1];
    return listviews;
}
}