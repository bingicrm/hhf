public class DownloadDisbursalReportController {

    @AuraEnabled
    public static String downloadDM(String loanApplicationId){
        Loan_Application__c la = [SELECT Id,Name,Sub_Stage__c,DMDownloaded__c from Loan_Application__c WHERE Id =: loanApplicationId LIMIT 1];
        
        if(la.Sub_Stage__c == Constants.Disbursement_Maker || la.Sub_Stage__c == Constants.Disbursement_Checker || la.Sub_Stage__c == Constants.Tranche_Disbursement_Maker){
            PageReference pagePdf = new PageReference('/apex/disbursalReportPDF');
            pagePdf.getParameters().put('id', loanApplicationId);
            
            /*****Added by Amit Agarwal for Test Coverage****/
            Blob pdfPageBlob;
                if(Test.isRunningTest()) { 
                    pdfPageBlob = blob.valueOf('Unit.Test');
                } else {
                    pdfPageBlob = pagePdf.getContent();
                }
           // pdfPageBlob = pagePdf.getContent();
            
            Attachment a = new Attachment();
            a.Body = pdfPageBlob;
            a.ParentID = loanApplicationId;
            a.Name = 'DisbursalMemo.pdf';
            
            try{
                insert a;
                /***** Added by saumya For TIL-00001316 *****/
                if(la.Sub_Stage__c == Constants.Disbursement_Checker || la.Sub_Stage__c == Constants.Tranche_Disbursement_Checker){ 
                    la.DMDownloaded__c = true;
                    update la;
                }
                /***** Added by saumya For TIL-00001316 *****/
                return 'Success';
            }
            catch(Exception e){
                System.debug('e==>'+e);
                return 'Error';
            }
        }
        else{
        	return 'Invalid Substage';    
        }
    }
}