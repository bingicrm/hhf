@isTest
private class AchievementTriggerTest {

	private static testMethod void AchievementTriggertest() {
	    
	    Targets__c tar = new Targets__c(LOB__c = 'Open Market',
                                SM_Category__c = 'DST',
                                Target_Count__c = 5);
        insert tar;
	    
	    Achievement__c objAch = new Achievement__c();
	    objAch.Target__c = tar.Id;
	    objAch.Active__c = true;
	    objAch.Insurance__c = true;
	    insert objAch;
	    
	    objAch.Insurance_Loan_Created__c = false;
	    objAch.Approved_Cross_Sell__c = 10000;
	    update objAch;

	}

}