@RestResource(urlMapping='/cibil/corporate/*')
global with sharing class CIBILCorporateResponse  {

    @HttpPost
    global static void createCIBILReport( CibilRequest cibil ) {
        system.debug('cibil@@@' + cibil);
		if(cibil != null && String.isNotBlank(cibil.parentId)) {
			String sObjName = Id.valueOf(cibil.parentId).getSObjectType().getDescribe().getName();
			CIBILResponse res = new CIBILResponse(); 
			try {
				
				String strJSONrequest = JSON.serializePretty(cibil);
				System.debug('Debug Log for strJSONRequest'+strJSONRequest);
				
				if(sObjName == Constants.strCustomerIntegrationAPI) {
					Customer_Integration__c ci = [Select Id,CIBIL_Score__c, Corporate_CIBIL_JSON__c, Loan_Contact__c from Customer_Integration__c where Id = : cibil.parentId];
				
					//Added Obligation_Status__c in this query for BREII-Enhancement
					Loan_Contact__c lc = [SELECT Id, Name, Obligation_Status__c,Customer__c, Retrigger_Corp_CIBIL__c,Loan_Applications__c, CIBIL_CheckUp__c,CIBIL_Verified__c FROM Loan_Contact__c WHERE Id =: ci.Loan_Contact__c];
					String strJSONrequestModified;
					strJSONrequestModified = strJSONrequest.replaceAll('&quot;','');
					strJSONrequestModified = strJSONrequest.replaceAll('                    ','');
					if(strJSONrequestModified != null) {
						ci.Corporate_CIBIL_JSON__c = (strJSONrequestModified.length() <= 131072 ? strJSONrequestModified : strJSONrequestModified.subString(0,131072));
						System.debug('Debug Log for ci.Corporate_CIBIL_JSON__c'+ci.Corporate_CIBIL_JSON__c);
						System.debug('Debug Log for ci.Corporate_CIBIL_JSON__c'+ci.Corporate_CIBIL_JSON__c.length());
						update ci;
					}
					
					if(lc != null){
							lc.CIBIL_CheckUp__c = true;
							lc.CIBIL_Verified__c = true;
							//Added by Vaishali for BREII-Enhancement
                            if(lc.Obligation_Status__c == 'Completed') {
                                lc.Obligation_Status__c = 'In Progress';
                            }
                            //End of patch- Added by Vaishali for BREII-Enhancement
							lc.Retrigger_Corp_CIBIL__c = false;
							
							update lc;
					}
					
					//Updating retrigger flag to false for Loan App, Address and Customer
					
					List<Loan_Application__C> lstLoanApp = [Select Id, Retrigger_Corp_CIBIL__c from Loan_Application__c where id =: lc.Loan_Applications__c];
					if ( !lstLoanApp.isEmpty() ) {
						lstLoanApp[0].Retrigger_Corp_CIBIL__c = false;
						update lstLoanApp;
					}    
					
					List<Address__c> lstAddress = [Select Id,Retrigger_Corp_CIBIL__c from Address__c where Loan_Contact__c = :lc.id];
					if( !lstAddress.isEmpty() ) {
						for ( Address__c objAdd : lstAddress ) {
							objAdd.Retrigger_Corp_CIBIL__c = false;
						}
						update lstAddress;
					}
					
					List<Account> lstAcc = [Select Id,Retrigger_Corp_CIBIL__c from Account where id = :lc.Customer__c];
					if ( !lstAcc.isEmpty() ) {
						lstAcc[0].Retrigger_Corp_CIBIL__c = false;
						update lstAcc;
					}
					
					res.status = 'Success';
					res.errorMsg = '';
					
					//Attaching CSV file of CIBIL response
					String strcsv = JsonToCSVUtility.getCSVString(strJSONrequest);
					Blob objBlob = Blob.valueOf(strcsv);
					Attachment attach = new Attachment();
					attach.contentType = 'text/csv';
					attach.name = 'CIBIL_Response.csv';
					attach.parentId = ci.id;
					attach.body = objBlob; 
					insert attach;
				}
				
				else if(sObjName == Constants.strBuilderIntegrationAPI) {
                    
                    Builder_Integrations__c ci = [Select Id,CIBIL_Score__c, Corporate_CIBIL_JSON__c, Project_Builder__c from Builder_Integrations__c where Id = : cibil.parentId];
            
                    Project_Builder__c lc = [SELECT Id, Name, Builder_Name__c, Retrigger_Corp_CIBIL__c, Project__c, CIBIL_CheckUp__c,CIBIL_Verified__c FROM Project_Builder__c WHERE Id =: ci.Project_Builder__c];
                    String strJSONrequestModified;
                    strJSONrequestModified = strJSONrequest.replaceAll('&quot;','');
                    strJSONrequestModified = strJSONrequest.replaceAll('                    ','');
                    if(strJSONrequestModified != null) {
                        ci.Corporate_CIBIL_JSON__c = (strJSONrequestModified.length() <= 131072 ? strJSONrequestModified : strJSONrequestModified.subString(0,131072));
                        System.debug('Debug Log for ci.Corporate_CIBIL_JSON__c'+ci.Corporate_CIBIL_JSON__c);
                        System.debug('Debug Log for ci.Corporate_CIBIL_JSON__c'+ci.Corporate_CIBIL_JSON__c.length());
                        update ci;
                    }
                    
                    if(lc != null){
                            lc.CIBIL_CheckUp__c = true;
                            lc.CIBIL_Verified__c = true;
                            lc.Retrigger_Corp_CIBIL__c = false;
                            
                            update lc;
                    }
                    
                    //Updating retrigger flag to false for Loan App, Address and Customer
                    
                    List<Project__c> lstLoanApp = [Select Id from Project__c where id =: lc.Project__c];
                     
                    
                    List<Address_Detail__c> lstAddress = [Select Id,Retrigger_Corp_CIBIL__c from Address_Detail__c where Project_Builder__c = :lc.id];
                    if( !lstAddress.isEmpty() ) {
                        for ( Address_Detail__c objAdd : lstAddress ) {
                            objAdd.Retrigger_Corp_CIBIL__c = false;
                        }
                        update lstAddress;
                    }
                    
                    List<Account> lstAcc = [Select Id,Retrigger_Corp_CIBIL__c from Account where id = :lc.Builder_Name__c];
                    if ( !lstAcc.isEmpty() ) {
                        lstAcc[0].Retrigger_Corp_CIBIL__c = false;
                        update lstAcc;
                    }
                    
                    res.status = 'Success';
                    res.errorMsg = '';
                    
                    //Attaching CSV file of CIBIL response
                    String strcsv = JsonToCSVUtility.getCSVString(strJSONrequest);
                    Blob objBlob = Blob.valueOf(strcsv);
                    Attachment attach = new Attachment();
                    attach.contentType = 'text/csv';
                    attach.name = 'CIBIL_Response.csv';
                    attach.parentId = ci.id;
                    attach.body = objBlob; 
                    insert attach;
                
                }
                
                else if(sObjName == Constants.strPromoterIntegrationAPI) {
                    //Corporate Cibil not Applicable for Promoters.
                }
				
			} 
			
			catch(Exception e) {
				res.status = 'Failure';
				res.errorMsg = e.getMessage();
			}
            try{
             	RestContext.response.responseBody = Blob.valueOf(JSON.serialize(res));   
            }
            catch(Exception ex){
            	    
            }
		}
        
    }
    
    
    //Wrapper classes
    global class CibilRequest {
        public responseReport responseReport;
        public String parentId;
        public List<errorReport> errorReport;
    }
        
    global class yourInstitution{
        public settled settled{get;set;}
        public String wilfulDefault{get;set;}
        public String dishonoredCheque{get;set;}
        public suitFilled suitFilled{get;set;}
        public invoked invoked{get;set;}
        public writtenOff writtenOff{get;set;}
        public String message{get;set;}
        public totalCF totalCF{get;set;}
        public delinquentOutstanding delinquentOutstanding{get;set;}
        public overdueCF overdueCF{get;set;}
        public String openCF{get;set;}
        public String totalLenders{get;set;}
        public String latestCFOpenedDate{get;set;}
        public workingCapital workingCapital{get;set;}
        public totalOutstanding totalOutstanding{get;set;}
        public termLoan termLoan{get;set;}
        public delinquentCF delinquentCF{get;set;}
        public nonFunded nonFunded{get;set;}
        public String last24Months{get;set;}
        public forex forex{get;set;}
        public String current{get;set;}
        public total total{get;set;}
    }
    global class writtenOff{
        public String numberOfSuitFiled{get;set;}
        public String amt{get;set;}
        
    }
    global class workingCapital{
        public NONSTDVec NONSTDVec{get;set;}
        public total total{get;set;}
        public STDVec STDVec{get;set;}
    }
    global class totalOutstanding{
        public String borrowerPercentage{get;set;}
        public String guarantorPercentage{get;set;}
        public String guarantor{get;set;}
        public String borrower{get;set;}
    }
    global class totalCF{
        public String guarantor{get;set;}
        public String borrowerPercentage{get;set;}
        public String guarantorPercentage{get;set;}
        public String borrower{get;set;}
    }
    global class total{
        public String dishonoredCheque{get;set;}
        public suitFilled suitFilled{get;set;}
        public settled settled{get;set;}
        public invoked invoked{get;set;}
        public writtenOff writtenOff{get;set;}
        public String wilfulDefault{get;set;}
        public String count{get;set;}
        public overdueCF overdueCF{get;set;}
        public NONSTDVec NONSTDVec{get;set;}
        public STDVec STDVec{get;set;}
        public String totalCF{get;set;}
        public String delinquentOutstanding{get;set;}
        public String openCF{get;set;}
        public String totalLenders{get;set;}
        public String value{get;set;}
        public String latestCFOpenedDate{get;set;}
        public String totalOutstanding{get;set;}
        public String delinquentCF{get;set;}
    }
    global class termLoan{
        public STDVec STDVec{get;set;}
        public total total{get;set;}
        public NONSTDVec NONSTDVec{get;set;}
    }
    global class summaryOfCreditFacilitiesVec{
        public list<String> summaryOfCreditFacilitiesRec{get;set;}
    }
    global class suitFilled{
        public String amt{get;set;}
        public String suitFilledBy{get;set;}
        public String suitRefNumber{get;set;}
        public String dateSuit{get;set;}
        public String suitAmt{get;set;}
        public String numberOfSuitFiled{get;set;}
        public String suitStatus{get;set;}
    }
    global class suitFiledVec{
        public String message{get;set;}
        public list<suitFilled> suitFilled{get;set;}
    }
    global class sub{
        public String count{get;set;}
        public String value{get;set;}
    }
    global class STDVec{
        public DPD0 DPD0{get;set;}
        public DPD1To30 DPD1To30{get;set;}
        public DPD31To60 DPD31To60{get;set;}
        public DPD61To90 DPD61To90{get;set;}

    }
    global class settled{
        public String amt{get;set;}
        public String numberOfSuitFiled{get;set;}
       
    }
    global class responseReport{
        public list<reportIssuesVec> reportIssuesVec{get;set;}
        public reportHeaderRec reportHeaderRec{get;set;}
        public productSec productSec{get;set;}
        public enquiryInformationRec enquiryInformationRec{get;set;}
    }
    global class reportIssuesVec{
        public String code{get;set;}
        public String description{get;set;}
        public String message{get;set;}
    }
    global class reportHeaderRec{
        public String daysPasswordToExpire{get;set;}
        public String reportOrderedBy{get;set;}
        public String inquiryPurpose{get;set;}
        public String memberDetails{get;set;}
        public String reportOrderNumber{get;set;}
        public String applicationReferenceNumber{get;set;}
        public String reportOrderDate{get;set;}
        public String memberReferenceNumber{get;set;}
    }
    global class relationshipInformation{
        public String dateOfBirth{get;set;}
        public String gender{get;set;}
        public String dateOfIncorporation{get;set;}
        public String name{get;set;}
        public String relatedType{get;set;}
        public String classOfActivity1{get;set;}
        public String businessCategory{get;set;}
        public String percentageOfControl{get;set;}
        public String businessIndustryType{get;set;}
        public String relationship{get;set;}
    }
    global class relationshipDetailsVec{
        public String message{get;set;}
        public list<relationshipDetails> relationshipDetails{get;set;}
    }
    global class relationshipDetails{
        public relationshipInformation relationshipInformation{get;set;}
        public borrwerAddressContactDetails borrwerAddressContactDetails{get;set;}
        public String relationshipHeader{get;set;}
        public borrwerIDDetailsVec borrwerIDDetailsVec{get;set;}
    }
    global class rankVec{
        public String rankValue{get;set;}
        public String rankName{get;set;}
        public String exclusionReason{get;set;}
    }
    global class rankSec{
        public String message{get;set;}
        public list<rankVec> rankVec{get;set;}
    }
    global class productSec{
        public oustandingBalanceByCFAndAssetClasificationSec oustandingBalanceByCFAndAssetClasificationSec{get;set;}
        public creditFacilitiesSummary creditFacilitiesSummary{get;set;}
        public creditProfileSummarySec creditProfileSummarySec{get;set;}
        public locationDetailsSec locationDetailsSec{get;set;}
        public enquiryDetailsInLast24MonthVec enquiryDetailsInLast24MonthVec{get;set;}
        public enquirySummarySec enquirySummarySec{get;set;}
        public creditFacilityDetailsasGuarantorVec creditFacilityDetailsasGuarantorVec{get;set;}
        public relationshipDetailsVec relationshipDetailsVec{get;set;}
        public derogatoryInformationSec derogatoryInformationSec{get;set;}
        public suitFiledVec suitFiledVec{get;set;}
        public creditRatingSummaryVec creditRatingSummaryVec{get;set;}
        public creditFacilitiesDetailsVec creditFacilitiesDetailsVec{get;set;}
        public rankSec rankSec{get;set;}
        public borrowerProfileSec borrowerProfileSec{get;set;}
        public creditFacilityDetailsasBorrowerSecVec creditFacilityDetailsasBorrowerSecVec{get;set;}
    }
    global class overdueCF{
        public String numberOfSuitFiled{get;set;}
        public String amt{get;set;}
       
    }
    global class outsideTotal{
        public String message{get;set;}
        public String totalLenders{get;set;}
        public String latestCFOpenedDate{get;set;}
        public String openCF{get;set;}
        public totalOutstanding totalOutstanding{get;set;}
        public delinquentOutstanding delinquentOutstanding{get;set;}
        public delinquentCF delinquentCF{get;set;}
        public totalCF totalCF{get;set;}
    }
    global class outsideInstitution{
        public String dishonoredCheque{get;set;}
        public outsideTotal outsideTotal{get;set;}
        public String message{get;set;}
        public NBFCOthers NBFCOthers{get;set;}
        public writtenOff writtenOff{get;set;}
        public overdueCF overdueCF{get;set;}
        public String wilfulDefault{get;set;}
        public suitFilled suitFilled{get;set;}
        public invoked invoked{get;set;}
        public otherPrivateForeignBanks otherPrivateForeignBanks{get;set;}
        public settled settled{get;set;}
        public workingCapital workingCapital{get;set;}
        public termLoan termLoan{get;set;}
        public otherPublicSectorBanks otherPublicSectorBanks{get;set;}
        public nonFunded nonFunded{get;set;}
        public String last24Months{get;set;}
        public forex forex{get;set;}
        public String current{get;set;}
        public total total{get;set;}
    }
    global class oustandingBalanceByCFAndAssetClasificationSec{
        public outsideInstitution outsideInstitution{get;set;}
        public yourInstitution yourInstitution{get;set;}
        public String message{get;set;}
    }
    global class otherPublicSectorBanks{
        public String message{get;set;}
        public String totalLenders{get;set;}
        public String latestCFOpenedDate{get;set;}
        public String openCF{get;set;}
        public totalOutstanding totalOutstanding{get;set;}
        public delinquentOutstanding delinquentOutstanding{get;set;}
        public delinquentCF delinquentCF{get;set;}
        public totalCF totalCF{get;set;}
    }
    global class otherPrivateForeignBanks{
        public String message{get;set;}
        public String totalLenders{get;set;}
        public String latestCFOpenedDate{get;set;}
        public String openCF{get;set;}
        public totalOutstanding totalOutstanding{get;set;}
        public delinquentOutstanding delinquentOutstanding{get;set;}
        public delinquentCF delinquentCF{get;set;}
        public totalCF totalCF{get;set;}
    }
    global class otherDetails{
        public String repaymentFrequency{get;set;}
        public String restructingReason{get;set;}
        public String assetBasedSecurityCoverage{get;set;}
        public String tenure{get;set;}
        public String guaranteeCoverage{get;set;}
        public String weightedAverageMaturityPeriodOfContracts{get;set;}
    }
    global class noOfEnquiries{
        public String month1{get;set;}
        public String month4To6{get;set;}
        public String mostRecentDate{get;set;}
        public String total{get;set;}
        public String month2To3{get;set;}
        public String month12To24{get;set;}
        public String greaterthan24Month{get;set;}
        public String month7To12{get;set;}
    }
    global class noOfCreditGrantors{
        public String yourBank{get;set;}
        public String others{get;set;}
        public String total{get;set;}
    }
    global class noOfCreditFacilities{
        public String yourBank{get;set;}
        public String others{get;set;}
        public String total{get;set;}
    }
    global class NONSTDVec{
        public dbt dbt{get;set;}
        public DPD91To180 DPD91To180{get;set;}
        public greaterThan180DPD greaterThan180DPD{get;set;}
        public loss loss{get;set;}
        public sub sub{get;set;}

    }
    global class nonFunded{
        public total total{get;set;}
        public NONSTDVec NONSTDVec{get;set;}
        public STDVec STDVec{get;set;}
    }
    global class NBFCOthers{
        public totalCF totalCF{get;set;}
        public String totalLenders{get;set;}
        public String latestCFOpenedDate{get;set;}
        public String openCF{get;set;}
        public totalOutstanding totalOutstanding{get;set;}
        public delinquentOutstanding delinquentOutstanding{get;set;}
        public delinquentCF delinquentCF{get;set;}
        public String message{get;set;}
    }
    global class loss{
        public String count{get;set;}
        public String value{get;set;}
    }
    global class locationInformationVec{
        public list<locationInformation> locationInformation{get;set;}
    }
    global class locationInformation{
        public String numberOfInstitutions{get;set;}
        public String firstReportedDate{get;set;}
        public String address{get;set;}
        public String lastReportedDate{get;set;}
        public String borrowerOfficeLocationType{get;set;}
    }
    global class locationDetailsSec{
        public String message{get;set;}
        public String faxNumber{get;set;}
        public locationInformationVec locationInformationVec{get;set;}
        public String contactNumber{get;set;}
    }
    global class invoked{
        public String numberOfSuitFiled{get;set;}
        public String amt{get;set;}

    }
    global class guarantorIDDetails{
        public String rationCard{get;set;}
        public String din{get;set;}
        public String registrationNumber{get;set;}
        public String drivingLicenseNumber{get;set;}
        public String uid{get;set;}
        public String passportNumber{get;set;}
        public String serviceTaxNumber{get;set;}
        public String voterID{get;set;}
        public String pan{get;set;}
        public String otherID{get;set;}
        public String tin{get;set;}
        public String cin{get;set;}
    }
    global class guarantorDetailsBorrwerIDDetailsVec{
        public list<guarantorIDDetails> guarantorIDDetails{get;set;}
        public String lastReportedDate{get;set;}
    }
    global class guarantorDetails{
        public String message{get;set;}
        public String dateOfIncorporation{get;set;}
        public String relatedType{get;set;}
        public String gender{get;set;}
        public String businessCategory{get;set;}
        public String name{get;set;}
        public String businessIndustryType{get;set;}
        public String dateOfBirth{get;set;}
    }
    global class guarantorAddressContactDetails{
        public String telephoneNumber{get;set;}
        public String address{get;set;}
        public String mobileNumber{get;set;}
        public String faxNumber{get;set;}
    }
    global class greaterThan180DPD{
        public String value{get;set;}
        public String count{get;set;}
    }
    global class forex{
        public total total{get;set;}
        public NONSTDVec NONSTDVec{get;set;}
        public STDVec STDVec{get;set;}
    }
    global class enquiryYourInstitution{
        public noOfEnquiries noOfEnquiries{get;set;}
    }
    global class enquiryTotal{
        public noOfEnquiries noOfEnquiries{get;set;}
    }
    global class enquirySummarySec{
        public enquiryOutsideInstitution enquiryOutsideInstitution{get;set;}
        public enquiryYourInstitution enquiryYourInstitution{get;set;}
        public enquiryTotal enquiryTotal{get;set;}
        public String message{get;set;}
    }
    global class enquiryOutsideInstitution{
        public noOfEnquiries noOfEnquiries{get;set;}
    }
    global class enquiryInformationRec{
        public String dateOfRegistration{get;set;}
        public String tin{get;set;}
        public String borrowerName{get;set;}
        public String cin{get;set;}
        public addressVec addressVec{get;set;}
        public String addressCount{get;set;}
        public String pan{get;set;}
        public String crn{get;set;}
    }
    global class enquiryDetailsInLast24MonthVec{
        public String message{get;set;}
        public list<enquiryDetailsInLast24Month> enquiryDetailsInLast24Month{get;set;}
    }
    global class enquiryDetailsInLast24Month{
        public String creditLender{get;set;}
        public String enquiryDt{get;set;}
        public String enquiryPurpose{get;set;}
        public String enquiryAmt{get;set;}
    }
    global class DPD91To180{
        public String value{get;set;}
        public String count{get;set;}
    }
    global class DPD61To90{
        public String count{get;set;}
        public String value{get;set;}
    }
    global class DPD31To60{
        public String value{get;set;}
        public String count{get;set;}
    }
    global class DPD1To30{
        public String count{get;set;}
        public String value{get;set;}
    }
    global class DPD0{
        public String count{get;set;}
        public String value{get;set;}
    }
    global class derogatoryInformationSec{
        public String messageOfBorrowerOutsideInstitution{get;set;}
        public String messageOfRelatedParties{get;set;}
        public String messageOfBorrower{get;set;}
        public derogatoryInformationBorrower derogatoryInformationBorrower{get;set;}
        public derogatoryInformationReportedOnGuarantedPartiesVec derogatoryInformationReportedOnGuarantedPartiesVec{get;set;}
        public String messageOfRelatedPartiesYourInstitution{get;set;}
        public String messageOfBorrowerYourInstitution{get;set;}
        public String messageOfRelatedPartiesOutsideInstitution{get;set;}
        public String messageOfGuarantedParties{get;set;}
        public derogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec derogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec{get;set;}
        public String message{get;set;}
    }
    global class derogatoryInformationReportedOnGuarantedPartiesVec{
        public list<String> derogatoryInformationReportedOnGuarantedParties{get;set;}
    }
    global class derogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec{
        public total total{get;set;}
        public outsideInstitution outsideInstitution{get;set;}
        public yourInstitution yourInstitution{get;set;}
    }
    global class derogatoryInformationBorrower{
        public total total{get;set;}
        public outsideInstitution outsideInstitution{get;set;}
        public yourInstitution yourInstitution{get;set;}
    }
    global class delinquentOutstanding{
        public String borrowerPercentage{get;set;}
        public String guarantor{get;set;}
        public String guarantorPercentage{get;set;}
        public String borrower{get;set;}
    }
    global class delinquentCF{
        public String borrower{get;set;}
        public String guarantor{get;set;}
        public String guarantorPercentage{get;set;}
        public String borrowerPercentage{get;set;}
    }
    global class dbt{
        public String value{get;set;}
        public String count{get;set;}
    }
    global class dates{
        public String sanctionedDt{get;set;}
        public String wilfulDefault{get;set;}
        public String loanRenewalDt{get;set;}
        public String loanExpiryDt{get;set;}
        public String suitFiledDt{get;set;}
    }
    global class creditRatingSummaryVec{
        public String message{get;set;}
        public list<creditRatingSummary> creditRatingSummary{get;set;}
    }
    global class creditRatingSummaryDetailsVec{
        public String lastReportedDt{get;set;}
        public String ratingExpiryDt{get;set;}
        public String ratingAsOn{get;set;}
        public String creditRating{get;set;}
    }
    global class creditRatingSummary{
        public String creditRatingAgency{get;set;}
        public list<creditRatingSummaryDetailsVec> creditRatingSummaryDetailsVec{get;set;}
    }
    global class creditProfileSummarySec{
        public yourInstitution yourInstitution{get;set;}
        public outsideInstitution outsideInstitution{get;set;}
        public total total{get;set;}
    }
    global class creditFacilitySecurityDetailsVec{
        public list<creditFacilitySecurityDetails> creditFacilitySecurityDetails{get;set;}
        public String message{get;set;}
    }
    global class creditFacilitySecurityDetails{
        public String validationDt{get;set;}
        public String relatedType{get;set;}
        public String currency1{get;set;}
        public String value{get;set;}
        public String classification{get;set;}
        public String lastReportedDt{get;set;}
    }
    global class creditFacilityOverdueDetailsVec{
        public String message{get;set;}
        public creditFacilityOverdueDetails creditFacilityOverdueDetails{get;set;}
    }
    global class creditFacilityOverdueDetails{
        public String DPD31To60Amt{get;set;}
        public String DPD91To180Amt{get;set;}
        public String DPDabove180Amt{get;set;}
        public String DPD61T090Amt{get;set;}
        public String DPD1To30Amt{get;set;}
    }
    global class creditFacilityGuarantorDetailsVec{
        public list<creditFacilityGuarantorDetails> creditFacilityGuarantorDetails{get;set;}
        public String message{get;set;}
    }
    global class creditFacilityGuarantorDetails{
        public guarantorAddressContactDetails guarantorAddressContactDetails{get;set;}
        public guarantorDetails guarantorDetails{get;set;}
        public guarantorDetailsBorrwerIDDetailsVec guarantorDetailsBorrwerIDDetailsVec{get;set;}
    }
    global class creditFacilityDetailsasGuarantorVec{
        public String message{get;set;}
        public list<creditFacilityDetailsasGuarantor> creditFacilityDetailsasGuarantor{get;set;}
    }
    global class creditFacilityDetailsasGuarantor{
        public borrwerInfo borrwerInfo{get;set;}
        public list<creditFacilityCurrentDetails> creditFacilityCurrentDetails{get;set;}
    }
    global class creditFacilityDetailsasBorrowerSecVec{
        public list<creditFacilityDetailsasBorrowerSec> creditFacilityDetailsasBorrowerSec{get;set;}
        public String message{get;set;}
    }
    global class creditFacilityDetailsasBorrowerSec{
        public creditFacilityOverdueDetailsVec creditFacilityOverdueDetailsVec{get;set;}
        public creditFacilityCurrentDetailsVec creditFacilityCurrentDetailsVec{get;set;}
        public creditFacilityGuarantorDetailsVec creditFacilityGuarantorDetailsVec{get;set;}
        public chequeDishounouredDuetoInsufficientFunds chequeDishounouredDuetoInsufficientFunds{get;set;}
        public creditFacilitySecurityDetailsVec creditFacilitySecurityDetailsVec{get;set;}
        public CFHistoryforACOrDPDupto24MonthsVec CFHistoryforACOrDPDupto24MonthsVec{get;set;}
        public String message{get;set;}
    }
    global class creditFacilityCurrentDetailsVec{
        public creditFacilityCurrentDetails creditFacilityCurrentDetails{get;set;}
    }
    global class creditFacilityCurrentDetails{
        public String accountNumber{get;set;}
        public String cfMember{get;set;}
        public dates dates{get;set;}
        public String lastReportedDate{get;set;}
        public String status{get;set;}
        public String statusDate{get;set;}
        public String derivative{get;set;}
        public String assetClassificationDaysPastDueDpd{get;set;}
        public String cfType{get;set;}
        public amount amount{get;set;}
        public String cfSerialNumber{get;set;}
        public otherDetails otherDetails{get;set;}
    }
    global class creditFacilitiesSummary{
        public summaryOfCreditFacilitiesVec summaryOfCreditFacilitiesVec{get;set;}
        public countOfCreditFacilities countOfCreditFacilities{get;set;}
    }
    global class creditFacilitiesDetailsVec{
        public String message{get;set;}
        public list<creditFacilitiesDetails> creditFacilitiesDetails{get;set;}
        public String total{get;set;}
    }
    global class creditFacilitiesDetails{
        public String reportedBy{get;set;}
        public String closedDate{get;set;}
        public String currentBalance{get;set;}
        public String ownership{get;set;}
        public String accountNo{get;set;}
        public String creditType{get;set;}
        public String lastReported{get;set;}
        public String group1{get;set;}
    }
    global class countOfCreditFacilities{
        public noOfCreditGrantors noOfCreditGrantors{get;set;}
        public noOfCreditFacilities noOfCreditFacilities{get;set;}
    }
    global class classOfActivityVec{
        public list<String> classOfActivity{get;set;}
    }
    global class chequeDishounouredDuetoInsufficientFunds{
        public String CD10To12Monthcount{get;set;}
        public String CD7To9Monthcount{get;set;}
        public String CD3Monthcount{get;set;}
        public String CD4To6Monthcount{get;set;}
        public String message{get;set;}
    }
    global class CFHistoryforACOrDPDupto24MonthsVec{
        public list<CFHistoryforACOrDPDupto24Months> CFHistoryforACOrDPDupto24Months{get;set;}
    }
    global class CFHistoryforACOrDPDupto24Months{
        public String ACorDPD{get;set;}
        public String OSAmount{get;set;}
        public String month{get;set;}
    }
    global class borrwerInfo{
        public borrwerDetails borrwerDetails{get;set;}
        public borrwerIDDetailsVec borrwerIDDetailsVec{get;set;}
        public borrwerAddressContactDetailsVec borrwerAddressContactDetailsVec{get;set;}
    }
    global class borrwerIDDetailsVec{
        public list<borrwerIDDetails> borrwerIDDetails{get;set;}
        public String lastReportedDate{get;set;}
    }
    global class borrwerIDDetails{
        public String serviceTaxNumber{get;set;}
        public String cin{get;set;}
        public String passportNumber{get;set;}
        public String tin{get;set;}
        public String rationCard{get;set;}
        public String voterID{get;set;}
        public String drivingLicenseNo{get;set;}
        public String din{get;set;}
        public String pan{get;set;}
        public String registrationNumber{get;set;}
        public String uid;
    }
    global class borrwerDetails{
        public String dateOfIncorporation{get;set;}
        public String borrowersLegalConstitution{get;set;}
        public String businessIndustryType{get;set;}
        public String name{get;set;}
        public String businessCategory{get;set;}
        public String salesFigure{get;set;}
        public String year{get;set;}
        global classOfActivityVec classOfActivityVec{get;set;}
        public String numberOfEmployees{get;set;}
    }
    global class borrwerAddressContactDetailsVec{
        public list<borrwerAddressContactDetails> borrwerAddressContactDetails{get;set;}
    }
    global class borrwerAddressContactDetails{
        public String telephoneNumber{get;set;}
        public String address{get;set;}
        public String mobileNumber{get;set;}
        public String faxNumber{get;set;}
    }
    global class borrowerProfileSec{
        public borrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec borrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec{get;set;}
        public borrowerDelinquencyReportedOnBorrower borrowerDelinquencyReportedOnBorrower{get;set;}
        public borrwerIDDetailsVec borrwerIDDetailsVec{get;set;}
        public borrwerAddressContactDetails borrwerAddressContactDetails{get;set;}
        public borrwerDetails borrwerDetails{get;set;}
    }
    global class borrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec{
        public list<String> borrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24Months{get;set;}
        public String message{get;set;}
    }
    global class borrowerDelinquencyReportedOnBorrower{
        public outsideInstitution outsideInstitution{get;set;}
        public yourInstitution yourInstitution{get;set;}
    }
    global class amount{
        public String naorc{get;set;}
        public String sanctionedAmt{get;set;}
        public String suitFiledAmt{get;set;}
        public String outstandingBalance{get;set;}
        public String highCredit{get;set;}
        public String overdue{get;set;}
        public String drawingPower{get;set;}
        public String contractsClassifiedAsNPA{get;set;}
        public String markToMarket{get;set;}
        public String writtenOFF{get;set;}
        public String notionalAmountOfContracts{get;set;}
        public String lastRepaid{get;set;}
        public String installmentAmt{get;set;}
        public String currency1{get;set;}
        public String settled{get;set;}
    }
    global class addressVec{
        public list<address> address{get;set;}
    }
    global class address{
        public String state{get;set;}
        public String pinCode{get;set;}
        public String city{get;set;}
        public String addressLine{get;set;}
    }
    global class CIBILResponse{
        public String status;
        public String errorMsg;
    }
    global class errorReport{
        public String bureau;
        public String trackingId;
        public String errors;
        public String product;
        public String status;
    }
    global class ErrorTrail{
        public String description;
        public String code;
       
    }
}