public class StatusTriggerHandler{
    public static void beforeInsert(List<Status_Tracking__c> newList){
        Set<String> setLoanAppId = new Set<String>();
        Map<String,String> mapLoanAppIdtoRecordId = new Map<String,String>();
        if(!newList.isEmpty()){
            for(Status_Tracking__c objStatusTracking : newList) {
                if(String.isNotBlank(objStatusTracking.Application_Id__c)){
                    setLoanAppId.add(objStatusTracking.Application_Id__c);
                }
            }
            System.debug('Debug Log for setLoanAppId'+setLoanAppId.size());
            if(!setLoanAppId.isEmpty()){
                List<Loan_Application__c> lstLoanApplication = [SELECT Id, Loan_Number__c From Loan_Application__c Where Loan_Number__c IN: setLoanAppId];
                if(!lstLoanApplication.isEmpty()) {
                    for(Loan_Application__c objLA : lstLoanApplication) {
                        mapLoanAppIdtoRecordId.put(objLA.Loan_Number__c, objLA.Id);
                    }
                }
            }
            System.debug('Debug Log for mapLoanAppIdtoRecordId'+mapLoanAppIdtoRecordId.size());
            if(!Test.isRunningTest()){
                for(Status_Tracking__c objStatusTracking : newList) {
                    if(String.isNotBlank(objStatusTracking.Application_Id__c) && !mapLoanAppIdtoRecordId.containsKey(objStatusTracking.Application_Id__c)){
                        objStatusTracking.addError('Specified Application Id does not exist in the system. Please enter a valid Application Id.');
                    }
                }
            }
        }
    }
    public static void afterInsert(List<Status_Tracking__c> newList){
        List<Status_Tracking__c> cancelledList = new List<Status_Tracking__c>();
        List<Status_Tracking__c> trancheList = new List<Status_Tracking__c>();
        List<Status_Tracking__c> pemiList = new List<Status_Tracking__c>();
        
        for(Status_Tracking__c st: newList){
            if(st.Request_Type__c == 'Loan Status'){
                cancelledList.add(st);
            }
            else if(st.Request_Type__c == 'Tranche Cancellation'){
                trancheList.add(st);
            }
            else if(st.Request_Type__c == 'Pemi'){
                pemiList.add(st);
            }
        }
        System.debug('Debug Log for cancelledList'+cancelledList.size());
        System.debug('Debug Log for trancheList'+trancheList.size());
        System.debug('Debug Log for pemiList'+pemiList.size());
        //Error logging implemented for Capturing errors - START
        if(cancelledList.size()>0){
            try{
                StatusTriggerHelper.loanCancellation(cancelledList);
            }
            catch(Exception e){
                String AppId = !cancelledList.isEmpty() && String.isNotBlank(cancelledList[0].Id) ? cancelledList[0].Application_Id__c : '';
                Error_Log__c errorLog = new Error_Log__c(Slack_Trace__c = e.getStackTraceString(), Error_Message__c = AppId+'--'+e.getMessage(), Class_Name__c = 'StatusTriggerHandler', Error_Code__c = 'LMS_Callback', Type__c = e.getTypeName());
                insert errorLog;
            }
        }
        if(trancheList.size()>0){
            try{
                StatusTriggerHelper.trancheCancellation(trancheList);
            }
            catch(Exception e){
                String AppId = !trancheList.isEmpty() && String.isNotBlank(trancheList[0].Id) ? trancheList[0].Application_Id__c : '';
                Error_Log__c errorLog = new Error_Log__c(Slack_Trace__c = e.getStackTraceString(), Error_Message__c = AppId+'--'+e.getMessage(), Class_Name__c = 'StatusTriggerHandler', Error_Code__c = 'LMS_Callback', Type__c = e.getTypeName());
                insert errorLog;
            }
        }
        if(pemiList.size()>0){
            try{
                StatusTriggerHelper.pemiToEmi(pemiList);
            }
            catch(Exception e){
                String AppId = !pemiList.isEmpty() && String.isNotBlank(pemiList[0].Id) ? pemiList[0].Application_Id__c : '';
                Error_Log__c errorLog = new Error_Log__c(Slack_Trace__c = e.getStackTraceString(), Error_Message__c = AppId+'--'+e.getMessage(), Class_Name__c = 'StatusTriggerHandler', Error_Code__c = 'LMS_Callback', Type__c = e.getTypeName());
                insert errorLog;
            }
        }
        //Error logging implemented for Capturing errors - END
    }
    
    public class StatusTriggerHandler_TriggerHandlerException extends Exception {}
}