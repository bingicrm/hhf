@isTest
public class TestSanctionConditionUtility{
    public static testMethod void getCreditDecisioningSanctionConditions(){
        
        Id loanConRecTypeId = Schema.SObjectType.loan_contact__c.getRecordTypeInfosByName().get('Co-Applicant').getRecordTypeId();
        
        Account custObj = new Account();
        custObj.Salutation='Mr';
        custObj.FirstName='Test';
        custObj.LastName = 'Test Customer';
        custObj.Phone = '9999999998';
        database.insert(custObj);
        
        scheme__c schemeObj = new scheme__c();
        schemeObj.Name = 'Scheme Name';
        schemeObj.Scheme_Group_ID__c = '12345';
        schemeObj.Scheme_Code__c = '12345';
        database.insert(schemeObj);
        
         
        Loan_Application__c loanAppObj = new Loan_Application__c();
        loanAppObj.customer__c = custObj.id;
        loanAppObj.Loan_Application_Number__c = 'LA00001';
        loanAppObj.Loan_Purpose__c = '11';
        loanAppObj.scheme__c = schemeObj.id;
        loanAppObj.Transaction_type__c = 'PB';
        loanAppObj.Requested_Amount__c = 50;        
        Database.insert(loanAppObj);
        
        Third_Party_Verification__c tpvObj = new Third_Party_Verification__c();
        tpvObj.Loan_Application__c = loanAppObj.id;
        tpvObj.Status__c = 'New';
        //Database.insert(tpvObj);   
        
        Test.startTest();
        loan_contact__c loanConObj = [select id, name, loan_applications__c from loan_contact__c where loan_applications__c =: loanAppObj.id];
        loanConObj.RecordTypeId = loanConRecTypeId;
        loanConObj.Borrower__c = '1';
        loanConObj.Constitution__c = '20';
        loanConobj.Applicant_Status__c = '1';
        loanConObj.Customer_segment__c = '1';
        loanConobj.Income_Program_Type__c = 'FLIP';
        loanConobj.Mobile__c='9876543210';
        loanConobj.Email__c='testemail@gmail.com';
        loanConobj.Category__c='1';
        loanConobj.Pan_Number__c = 'AXSPJ2345K';
        Database.update(loanConObj);
        
        Sanction_Condition_Master__c scmObj = new Sanction_Condition_Master__c();
        scmObj.Customer_Segment__c = '1';
        scmObj.Sanction_Condition__c = 'Test';
        Database.insert(scmObj); 
        
        system.assertNotEquals(null,loanAppObj.id);
        
        SanctionConditionUtility.getCreditDecisioningSanctionConditions(loanAppObj.id);
        //SanctionConditionUtility.getLoanSanction(loanAppObj.id,loanConObj.id,'Salaried');
        Test.stopTest();
    }
}