public class GroupExpViewController {
    
    @AuraEnabled 
    public static GroupExpWrapper getGroupExp(Id loanAppId){
		Id profileId = UserInfo.getProfileId();
        String profileName =[Select Id, Name from Profile where Id=:profileId].Name;
        system.debug('Profile Name'+profileName);					
        List<Loan_Application__c> lappLst = new List<Loan_Application__c>();
		Map<String, Boolean> mapOfApplicationWithCommonCDCheck = new Map<String, Boolean>();
        Map<String,List<Id>> mapOfApplicationWithCDs = new Map<String,List<Id>>();
        List<String> lstOfAppId = new List<String>();								 
        GroupExpWrapper groupExp = new GroupExpWrapper();
        lappLst = [SELECT Id,Approved_Loan_Amount__c,StageName__c,Loan_Number__c,Group_Exposure_Reviewed__c,Group_Exposure_Reviewed_By__c,(Select Id from Loan_Contacts__r) FROM Loan_Application__c WHERE Id =: loanAppId];// Added by Saumya Group_Exposure_Reviewed__c,Group_Exposure_Reviewed_By__c for BRE 
		
		For(Loan_Application__c objLA: lappLst){
			List<Id> lstofCd = new List<Id>(); 
            For(Loan_Contact__c objCD: objLA.Loan_Contacts__r){
            lstofCd.add(objCD.Id); 
            }
            mapOfApplicationWithCDs.put(objLA.Loan_Number__c,lstofCd);
        } 
        if(lappLst[0].Approved_Loan_Amount__c != null){
            List<Group_Exposure__c> lstGE = [SELECT Id, LoanApplication__r.StageName__c,
			Customer_Available_in_Current_Loan__c,
			Amount_Sanctioned_but_not_Disbursed__c,Product__c,ApplicationID__c,Customer_Detail__c,Primary_Applicant_Name__c,Principal_Outstanding__c,Remarks__c,WIP_Exposure__c,Status__c
                     FROM Group_Exposure__c
                     WHERE LoanApplication__c = :loanAppId];
			//Added by Saumya Customer_Available_in_Current_Loan__c for BRE 2							   
			system.debug('lstGE:::'+lstGE);
            for(Group_Exposure__c objGE: lstGE){
                lstOfAppId.add(objGE.ApplicationID__c);
            }
            groupExp.StageName = lappLst[0].StageName__c;
            groupExp.lstGroupExp = lstGE;
			groupExp.ExposureReviewed = lappLst[0].Group_Exposure_Reviewed__c;// Added by Saumya for BRE 2
            groupExp.loggedInUserProfile =profileName;// Added by Saumya for BRE 2
            groupExp.AppId = lappLst[0].Loan_Number__c;
            /*return [SELECT Id, LoanApplication__r.StageName__c,Amount_Sanctioned_but_not_Disbursed__c,Product__c,ApplicationID__c,Customer_Detail__c,Primary_Applicant_Name__c,Principal_Outstanding__c,Remarks__c,WIP_Exposure__c,Status__c
                     FROM Group_Exposure__c
                     WHERE LoanApplication__c = :loanAppId ]; */
            System.debug('groupExp'+groupExp);
            return groupExp;
        }
        else{
            return null;
        }
    }
    @AuraEnabled 
    public static void reviewedGroupExposure(Id loanAppId, Boolean reviewed){
    system.debug('reviewed::'+reviewed);
	Loan_Application__c objLA = new Loan_Application__c();
    objLA.Id = loanAppId;
    objLA.Group_Exposure_Reviewed__c =  reviewed;
    if(reviewed) {
    	objLA.Group_Exposure_Reviewed_By__c = UserInfo.getUserId();    
    }
    system.debug('objLA::'+objLA);
    update objLA;    
    }
    @AuraEnabled
    public static List<Integer> calculateSumPO(Id loanAppId) {
        system.debug('1234--->>');
        List<Loan_Application__c> laLst = new List<Loan_Application__c>();
        laLst = [SELECT Id, Name, Group_Exposure_Amount__c,Approved_Loan_Amount__c, Approved_cross_sell_amount__c, Group_Exposure_Adjustment__c FROM Loan_Application__c WHERE Id= :loanAppId];
        Integer sumPO = 0;
        Integer sumSUD = 0;
        Integer sumWIP = 0;
        Integer selectedSumPO = 0;
        Integer selectedSUD = 0;
        Integer selectedWIP = 0;
        Integer consideredExp = 0;
        Integer adjustAmt = 0;//Added by Saumya For Group Exposure
		Integer currentAmt = (laLst[0].Approved_Loan_Amount__c != null ? Integer.valueOf(laLst[0].Approved_Loan_Amount__c) : 0) + (laLst[0].Approved_cross_sell_amount__c != null? Integer.valueOf(laLst[0].Approved_cross_sell_amount__c) : 0); //Modified by Saumya For Group Exposure       
        Integer currentAmtwithAdjustment = (laLst[0].Approved_Loan_Amount__c != null ? Integer.valueOf(laLst[0].Approved_Loan_Amount__c) : 0) + (laLst[0].Approved_cross_sell_amount__c != null? Integer.valueOf(laLst[0].Approved_cross_sell_amount__c) : 0)+ (laLst[0].Group_Exposure_Adjustment__c != null? Integer.valueOf(laLst[0].Group_Exposure_Adjustment__c) : 0); //Modified by Saumya For Group Exposure
        system.debug('amount--->>'+laLst[0].Approved_Loan_Amount__c);
        List<Integer> sum = new List<Integer>();
        List<Group_Exposure__c> lstGE = [SELECT Id, Amount_Sanctioned_but_not_Disbursed__c,LoanApplication__r.Group_Exposure_Adjustment__c,ApplicationID__c,Customer_Detail__c,Primary_Applicant_Name__c,Principal_Outstanding__c,Remarks__c,WIP_Exposure__c,Status__c 
                                         FROM Group_Exposure__c
                                         WHERE LoanApplication__c = :loanAppId ]; //Added Group_Exposure_Adjustment__c by Saumya For Group Exposure
        List<Group_Exposure__c> lstGExp = [SELECT Id, Amount_Sanctioned_but_not_Disbursed__c,LoanApplication__r.Group_Exposure_Adjustment__c,ApplicationID__c,Customer_Detail__c,Primary_Applicant_Name__c,Principal_Outstanding__c,Remarks__c,WIP_Exposure__c,Status__c 
                                           FROM Group_Exposure__c
                                           WHERE LoanApplication__c = :loanAppId AND Status__c = 'Considered'];//Added Group_Exposure_Adjustment__c by Saumya For Group Exposure
        
        if(!lstGE.isEmpty()){
            for(Group_Exposure__c objGE : lstGE) {
                if(!String.isBlank(String.valueOf(objGE.Principal_Outstanding__c))) {
                    sumPO += Integer.valueOf(objGE.Principal_Outstanding__c);  
                }
                if(!String.isBlank(String.valueOf(objGE.Amount_Sanctioned_but_not_Disbursed__c))) {
                    sumSUD += Integer.valueOf(objGE.Amount_Sanctioned_but_not_Disbursed__c);        
                }
                if(!String.isBlank(String.valueOf(objGE.WIP_Exposure__c))) {
                    sumWIP += Integer.valueOf(objGE.WIP_Exposure__c);        
                }
                
                adjustAmt = objGE.LoanApplication__r.Group_Exposure_Adjustment__c != null ? Integer.valueOf(objGE.LoanApplication__r.Group_Exposure_Adjustment__c) : 0; //Added Group_Exposure_Adjustment__c by Saumya For Group Exposure
            }
        }
        if(!lstGExp.isEmpty()){
            for(Group_Exposure__c objGE : lstGExp) {
                if(!String.isBlank(String.valueOf(objGE.Principal_Outstanding__c))) {
                    selectedSumPO += Integer.valueOf(objGE.Principal_Outstanding__c);  
                }
                if(!String.isBlank(String.valueOf(objGE.Amount_Sanctioned_but_not_Disbursed__c))) {
                    selectedSUD += Integer.valueOf(objGE.Amount_Sanctioned_but_not_Disbursed__c);        
                }
                if(!String.isBlank(String.valueOf(objGE.WIP_Exposure__c))) {
                    selectedWIP += Integer.valueOf(objGE.WIP_Exposure__c);        
                }
                adjustAmt = objGE.LoanApplication__r.Group_Exposure_Adjustment__c != null ? Integer.valueOf(objGE.LoanApplication__r.Group_Exposure_Adjustment__c) : 0; //Added Group_Exposure_Adjustment__c by Saumya For Group Exposure
            }
        }
        consideredExp = selectedSUD + selectedSumPO + currentAmt;
        sum.add(sumPO);
        sum.add(sumSUD);
        sum.add(sumWIP);
        sum.add(selectedSumPO);
        sum.add(selectedSUD);
        sum.add(selectedWIP);
        sum.add(consideredExp);
        sum.add(adjustAmt); //Added Group_Exposure_Adjustment__c by Saumya For Group Exposure
        return sum;
    }    
    
    @AuraEnabled
    public static String saveGERecords(String GERecords, String UGERecords) {
        String errorMsg = null;
        String fieldName = 'Remarks__c';
        
        List<Group_Exposure__c> lstGrpExptoUpdate = (List<Group_Exposure__c>) System.JSON.deserialize(GERecords, List<Group_Exposure__c>.class);
        
        List<Group_Exposure__c> lstUGrpExptoUpdate = (List<Group_Exposure__c>) System.JSON.deserialize(UGERecords, List<Group_Exposure__c>.class);
        
        List<Group_Exposure__c> GEUpdate = new List<Group_Exposure__c>();
        
        if(lstGrpExptoUpdate.size() > 0){
            for(Group_Exposure__c ge: lstGrpExptoUpdate){
                ge.Status__c = 'Considered';
                GEUpdate.add(ge);
            }
            
        }
        
        if(lstUGrpExptoUpdate.size() > 0){
            for(Group_Exposure__c uge: lstUGrpExptoUpdate){
                if(!(uge.getSobjectType().getDescribe().fields.getMap().keySet().contains(fieldName.toLowerCase()))){
                    //uge.addError('Error');
                    errorMsg = 'Test';
                }
                else if(uge.Remarks__c == '' || uge.Remarks__c == null){
                    errorMsg = 'The Remarks cannot be blank.';   
                }
                else{
                    uge.Status__c = 'Not Considered';
                    GEUpdate.add(uge); 
                }
            }
        }
        
        if(GEUpdate.size() > 0 && (errorMsg == '' || errorMsg == null)){
            Update GEUpdate;
        }
        return errorMsg;
    }
    
    @AuraEnabled
    public static String saveGEAdjustment(Id loanId, Decimal adjustAmt, Decimal adjustAmtupdate){
        String errorMsg = null;
        List<Loan_Application__c> loanAppList = [Select Id, Group_Exposure_Amount__c from Loan_Application__c where Id =: loanId];
        List<Loan_Application__c> loanAppListUpdate = new List<Loan_Application__c>();
        if(loanAppList.size() > 0){
            for(Loan_Application__c la :loanAppList){
                la.Group_Exposure_Amount__c = adjustAmt;
                la.Group_Exposure_Adjustment__c = adjustAmtupdate; // Addded By Saumya For Group Exposure
                loanAppListUpdate.add(la);
                errorMsg = 'Success'; 
                
            }                
        }
        else{
            errorMsg = 'Error';
        }     
        if(errorMsg == 'Success'){
            Database.update(loanAppListUpdate);
            errorMsg = 'Updated';
        }
        System.debug('Errormsg>>>'+errorMsg);        
        return errorMsg;
    }
    
	@Auraenabled
    public static boolean GetIseditable(Id recId){
       
        Loan_Application__c loanAppDetails = new Loan_Application__c();
       
        loanAppDetails = [SELECT Name,StageName__c from Loan_Application__c where Id=:recId];
        
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        system.debug('ProfileName'+profileName);
        if(Label.Reviewed_Stage.contains(loanAppDetails.StageName__c) && Label.Reviewed_Profile.contains(profileName))
        { 
            return true;//edit mode
        } else {
            return false;//read only mode}   
        }
        
    }	

    public class GroupExpWrapper {
       @AuraEnabled public String StageName;
       @AuraEnabled public String AppId; 
		@AuraEnabled public Boolean ExposureReviewed;  //Added by Saumya for BRE2
       @AuraEnabled public String loggedInUserProfile;//Added by Saumya for BRE2
       @AuraEnabled public Map<String,Boolean> mapOfApplicationWithCommonCDCheck;//Added by Saumya for BRE2 
       @AuraEnabled public  List<Group_Exposure__c> lstGroupExp; 
    }
}