@isTest
public class TestTrancheTriggerHelper{
    
    @testSetup static void testData() {
    	Account acc=new Account();
        acc.name='TestDFGWSEFS AccoountASFSADFS for trigger';
        acc.phone='9666622222';
        acc.PAN__c='DBUIE8289A';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        Scheme__c sc=new Scheme__c();
        sc.name='Construction Finance';
        sc.Scheme_Code__c='CF';
        sc.Scheme_Group_ID__c='6';
        sc.Product_Code__c='HL';
        insert sc;
        
        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Transaction_type__c='DAA';
        lap.Scheme__c=sc.id;
        lap.recordtypeid= Schema.SObjectType.Loan_Application__c.getRecordTypeInfosByName().get('Readonly Record').getRecordTypeId();
        lap.Approved_Loan_Amount__c=5000000;
        lap.Requested_Amount__c=50000;
        //lap.Disbursed_Amount__c=40000;
        lap.StageName__c='Customer Onboarding';
        //lap.Sub_Stage__c='Docket Checker';
        lap.Property_Identified__c = True;
        lap.Property_Entered__c = FALSE;
        insert lap;
        List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
        
        Document_Master__c dm=new Document_Master__c();
        dm.name='test';
        dm.Doc_Id__c='asdfsdf';
        dm.Expiration_Possible__c=true;
        dm.FCU_Required__c=true;
        dm.OTC__c=true;
        insert dm;
        
        Document_Type__c dt=new Document_Type__c();
        dt.name='Any type';
        dt.Document_Type_Id__c='asfmsdkvns';
        dt.Expiration_Possible__c=true;
        dt.OTC__c=true;
        insert dt;
        
        Tranche__c tr=new Tranche__c();
        tr.Loan_Application__c=lap.id;
        tr.Disbursal_Amount__c=50100;
        tr.Request_Type__c= Constants.TRANCHEDISBURSAL;
        tr.Repayment_Start_Date__c = system.today();
        tr.Tranche_Stage__c = 'Tranche Disbursement Checker';
        insert tr;
        
        tr.Request_Type__c = 'Tranche Disbursal2';  
        update tr;
        
        Document_Checklist__c dc=new Document_Checklist__c();
        dc.Loan_Applications__c=lap.id;
        //dc.Loan_Contact__c=lstofcon[0].id;
        dc.Document_Type__c=dt.id;
        dc.Document_Master__c=dm.id;
        dc.REquest_Date_for_OTC__c=date.today();
        dc.Document_Collection_Mode__c='Photocopy';
        dc.Loan_Engine_Mandatory__c=true;
        dc.Express_Queue_Mandatory__c=true;
        dc.status__c='Pending';
        dc.File_Check_Completed__c=true;
        dc.Scan_Check_Completed__c=true;
        dc.Tranche__c=tr.id;
        dc.Original_Seen_and_Verified__c=true;
        insert dc;
        
        Loan_Repayment__c objLoanRp = new Loan_Repayment__c(Broken_Period_Interest_Handing__c = 'Return as charge',Loan_Application__c=lap.id,
                                                           dueDay__c=System.today().day());
        insert objLoanRp;    
    }   
    
    @isTest    
    public static void method1(){
        Loan_Application__c lap = [SELECT Id FROM Loan_Application__c LIMIT 1];
        Tranche__c tr = [SELECT Id FROM Tranche__c WHERE Loan_Application__c =:lap.Id LIMIT 1];        
        Loan_Repayment__c objLoanRp = [SELECT Id FROM Loan_Repayment__c WHERE Loan_Application__c =:lap.Id LIMIT 1];    
        Document_Checklist__c dc = [SELECT Id FROM Document_Checklist__c WHERE Loan_Applications__c =:lap.Id LIMIT 1];
            
        LMSDateUtility.lmsDateValue = system.today();  
        TrancheGapAmountController.makeAPICallout(tr.id);
        objLoanRp.Broken_Period_Interest_Handing__c = 'Add to Schedule';
        update objloanrp;
        TrancheGapAmountController.makeAPICallout(tr.id);
        TrancheGapAmountController.getTrancheRec(tr.id);
        TrancheGapAmountController.TrancheGapResponseWrapper objWrap = new TrancheGapAmountController.TrancheGapResponseWrapper();
        objWrap.o_error_code='';
        objWrap.o_error_msg ='';
        objWrap.o_gap_interest = '';
        objWrap.o_gapdays = '';
        objWrap.o_gapdtl ='';
        objWrap.o_success_flag = '';
        
        //Lookup.searchDB('tranche__c', 'Disbursal_Amount__c', 'Request_Type__c', 10, 'Request_Type__c', 'ra');
        
        List<Document_Checklist__c > ldc=new List<Document_Checklist__c >();
        ldc.add(dc);
        
        User u4=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
                 Title,Alias,TimeZoneSidKey,profile.name,isActive,
                 EmailEncodingKey,LanguageLocaleKey,
                 LocaleSidKey,UserRoleId 
                 from user where profile.Name='Third Party Vendor' and isActive=true limit 1];
        
        delete tr;  
        /* Third_Party_Verification__c tpv =new Third_Party_Verification__c();
tpv.Customer_Details__c=lstofcon[0].id;
tpv.Loan_Application__c=lap.id;
tpv.owner__c=u4.id;
tpv.recordtypeid='012p00000007NM6AAM';
tpv.Type_of_Verification__c='Full Profile Check';
insert tpv;

lap.Property_Entered__c = TRUE;
update lap;

Property__c prop=new Property__c();
prop.Loan_Application__c=lap.id;
prop.First_Property_Owner__c=lstofcon[0].id;
insert prop;*/
        
        
        
        TrancheTriggerHelper.propertyVerifications(lap.id,tr.id);
        //TrancheTriggerHelper.deleteDocs(ldc);
        
    }
    @isTest
    public static void method2(){
    	Tranche__c trObj = [SELECT Id,Tranche_Stage__c FROM Tranche__c LIMIT 1];
        trObj.Tranche_Stage__c = 'Tranche Disbursed'; 
        trObj.Approved_Disbursal_Amount__c = 50100;
        update trObj;
        Test.startTest();
		//trObj.Tranche_Stage__c = 'Tranche Disbursed';   
        //update trObj;
        Test.stopTest();
    }
    /*@isTest

public static void method2(){


Account acc=new Account();
acc.name='TestDFGWSEFS AccoountASFSADFS for trigger';
acc.phone='66666222222';
acc.PAN__c='DBUIE8289A';
acc.Date_of_Incorporation__c=date.today();
insert acc;

System.debug(acc.id);

Scheme__c sc=new Scheme__c();
sc.name='Construction Finance';
sc.Scheme_Code__c='CF';
sc.Scheme_Group_ID__c='6';
sc.Product_Code__c='HL';
insert sc;
System.debug(sc.id);

Loan_Application__c lap=new Loan_Application__c ();
lap.Customer__c=acc.id;
lap.Transaction_type__c='DAA';
lap.Scheme__c=sc.id;
lap.recordtypeid='012p00000007NLfAAM';
lap.Approved_Loan_Amount__c=5000000;
//lap.StageName__c='Tranche';
lap.Sub_Stage__c='Tranche File Check';
lap.Property_Identified__c = True;
insert lap;
System.debug('STAGE  '+lap.Sub_Stage__c);
System.debug(lap.Sub_Stage__c);
List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
Document_Master__c dm=new Document_Master__c();
dm.name='test';
dm.Doc_Id__c='asdfsdf';
dm.Expiration_Possible__c=true;
dm.FCU_Required__c=true;
dm.OTC__c=true;
insert dm;

Document_Type__c dt=new Document_Type__c();
dt.name='Any type';
dt.Document_Type_Id__c='asfmsdkvns';
dt.Expiration_Possible__c=true;
dt.OTC__c=true;
insert dt;

Tranche__c tr=new Tranche__c();
tr.Loan_Application__c=lap.id;
tr.Disbursal_Amount__c=16000;
insert tr;

Document_Checklist__c dc=new Document_Checklist__c();
dc.Loan_Applications__c=lap.id;
//dc.Loan_Contact__c=lstofcon[0].id;
dc.Document_Type__c=dt.id;
dc.Document_Master__c=dm.id;
dc.REquest_Date_for_OTC__c=date.today();
dc.Document_Collection_Mode__c='Photocopy';
dc.Loan_Engine_Mandatory__c=true;
dc.Express_Queue_Mandatory__c=true;
dc.status__c='Pending';
dc.File_Check_Completed__c=true;
dc.Scan_Check_Completed__c=true;
dc.Tranche__c=tr.id;
dc.Original_Seen_and_Verified__c=true;

insert dc;

List<Document_Checklist__c > ldc=new List<Document_Checklist__c >();
ldc.add(dc);

User u4=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
Title,Alias,TimeZoneSidKey,profile.name,isActive,
EmailEncodingKey,LanguageLocaleKey,
LocaleSidKey,UserRoleId 
from user where profile.Name='LIP' and isActive=true limit 1];

Third_Party_Verification__c tpv =new Third_Party_Verification__c();
tpv.Customer_Details__c=lstofcon[0].id;
tpv.Loan_Application__c=lap.id;
tpv.owner__c=u4.id;
tpv.recordtypeid='012p00000007NM6AAM';
tpv.Type_of_Verification__c='Full Profile Check';
insert tpv;


Property__c prop=new Property__c();
prop.Loan_Application__c=lap.id;
prop.First_Property_Owner__c=lstofcon[0].id;
insert prop;

lap.Property_Entered__c = TRUE;
update lap;

Database.delete(tr);
}

@isTest

public static void method3(){


Account acc=new Account();
acc.name='TestDFGWSEFS AccoountASFSADFS for trigger';
acc.phone='66666222222';
acc.PAN__c='DBUIE8289A';
acc.Date_of_Incorporation__c=date.today();
insert acc;

System.debug(acc.id);

Scheme__c sc=new Scheme__c();
sc.name='Construction Finance';
sc.Scheme_Code__c='CF';
sc.Scheme_Group_ID__c='6';
sc.Product_Code__c='HL';
insert sc;
System.debug(sc.id);

Loan_Application__c lap=new Loan_Application__c ();
lap.Customer__c=acc.id;
lap.Transaction_type__c='DAA';
lap.Scheme__c=sc.id;
lap.recordtypeid='012p00000007NLfAAM';
lap.Approved_Loan_Amount__c=5000000;
//lap.StageName__c='Tranche';
lap.Sub_Stage__c='Tranche File Check';
lap.Property_Identified__c = True;
insert lap;
System.debug('STAGE  '+lap.Sub_Stage__c);
System.debug(lap.Sub_Stage__c);
List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
Document_Master__c dm=new Document_Master__c();
dm.name='test';
dm.Doc_Id__c='asdfsdf';
dm.Expiration_Possible__c=true;
dm.FCU_Required__c=true;
dm.OTC__c=true;
insert dm;

Document_Type__c dt=new Document_Type__c();
dt.name='Any type';
dt.Document_Type_Id__c='asfmsdkvns';
dt.Expiration_Possible__c=true;
dt.OTC__c=true;
insert dt;

Tranche__c tr=new Tranche__c();
tr.Loan_Application__c=lap.id;
tr.Disbursal_Amount__c=16000;
insert tr;

Document_Checklist__c dc=new Document_Checklist__c();
dc.Loan_Applications__c=lap.id;
//dc.Loan_Contact__c=lstofcon[0].id;
dc.Document_Type__c=dt.id;
dc.Document_Master__c=dm.id;
dc.REquest_Date_for_OTC__c=date.today();
dc.Document_Collection_Mode__c='Photocopy';
dc.Loan_Engine_Mandatory__c=true;
dc.Express_Queue_Mandatory__c=true;
dc.status__c='Pending';
dc.File_Check_Completed__c=true;
dc.Scan_Check_Completed__c=true;
dc.Tranche__c=tr.id;
dc.Original_Seen_and_Verified__c=true;

insert dc;

List<Document_Checklist__c > ldc=new List<Document_Checklist__c >();
ldc.add(dc);

User u4=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
Title,Alias,TimeZoneSidKey,profile.name,isActive,
EmailEncodingKey,LanguageLocaleKey,
LocaleSidKey,UserRoleId 
from user where profile.Name='LIP' and isActive=true limit 1];

Third_Party_Verification__c tpv =new Third_Party_Verification__c();
tpv.Customer_Details__c=lstofcon[0].id;
tpv.Loan_Application__c=lap.id;
tpv.owner__c=u4.id;
tpv.recordtypeid='012p00000007NM6AAM';
tpv.Type_of_Verification__c='Full Profile Check';
insert tpv;


Property__c prop=new Property__c();
prop.Loan_Application__c=lap.id;
prop.First_Property_Owner__c=lstofcon[0].id;
insert prop;

tr.Disbursal_Amount__c=90000;
tr.Tranche_Stage__c='Tranche File Check';
Database.update(tr);


tr.Tranche_Stage__c='Tranche Credit Approval';
Database.update(tr);


tr.Tranche_Stage__c='Tranche Disbursement Maker';
Database.update(tr);


tr.Tranche_Stage__c='Tranche Disbursed';
Database.update(tr);


}

@isTest

public static void method4(){


Account acc=new Account();
acc.name='TestDFGWSEFS AccoountASFSADFS for trigger';
acc.phone='66666222222';
acc.PAN__c='DBUIE8289A';
acc.Date_of_Incorporation__c=date.today();
insert acc;

System.debug(acc.id);

Scheme__c sc=new Scheme__c();
sc.name='Construction Finance';
sc.Scheme_Code__c='CF';
sc.Scheme_Group_ID__c='6';
sc.Product_Code__c='HL';
insert sc;
System.debug(sc.id);

Loan_Application__c lap=new Loan_Application__c ();
lap.Customer__c=acc.id;
lap.Approved_Loan_Amount__c=5000;
//lap.Disbursed_Amount__c=4000;
lap.Transaction_type__c='DAA';
lap.Scheme__c=sc.id;
lap.recordtypeid='012p00000007NLfAAM';
lap.Approved_Loan_Amount__c=5000000;
//lap.StageName__c='Tranche';
lap.Sub_Stage__c='Tranche File Check';
lap.Property_Identified__c = True;
insert lap;
System.debug('STAGE  '+lap.Sub_Stage__c);
System.debug(lap.Sub_Stage__c);
List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
Document_Master__c dm=new Document_Master__c();
dm.name='test';
dm.Doc_Id__c='asdfsdf';
dm.Expiration_Possible__c=true;
dm.FCU_Required__c=true;
dm.OTC__c=true;
insert dm;

Document_Type__c dt=new Document_Type__c();
dt.name='Any type';
dt.Document_Type_Id__c='asfmsdkvns';
dt.Expiration_Possible__c=true;
dt.OTC__c=true;
insert dt;

Tranche__c tr=new Tranche__c();
tr.Disbursal_Amount__c=5000;
tr.Loan_Application__c=lap.id;
insert tr;

Document_Checklist__c dc=new Document_Checklist__c();
dc.Loan_Applications__c=lap.id;
//dc.Loan_Contact__c=lstofcon[0].id;
dc.Document_Type__c=dt.id;
dc.Document_Master__c=dm.id;
dc.REquest_Date_for_OTC__c=date.today();
dc.Document_Collection_Mode__c='Photocopy';
dc.Loan_Engine_Mandatory__c=true;
dc.Express_Queue_Mandatory__c=true;
dc.status__c='Pending';
dc.File_Check_Completed__c=true;
dc.Scan_Check_Completed__c=true;
dc.Tranche__c=tr.id;
dc.Original_Seen_and_Verified__c=true;

insert dc;

List<Document_Checklist__c > ldc=new List<Document_Checklist__c >();
ldc.add(dc);

User u4=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
Title,Alias,TimeZoneSidKey,profile.name,isActive,
EmailEncodingKey,LanguageLocaleKey,
LocaleSidKey,UserRoleId 
from user where profile.Name='LIP' and isActive=true limit 1];

Third_Party_Verification__c tpv =new Third_Party_Verification__c();
tpv.Customer_Details__c=lstofcon[0].id;
tpv.Loan_Application__c=lap.id;
tpv.owner__c=u4.id;
tpv.recordtypeid='012p00000007NM6AAM';
tpv.Type_of_Verification__c='Full Profile Check';
insert tpv;


Property__c prop=new Property__c();
prop.Loan_Application__c=lap.id;
prop.First_Property_Owner__c=lstofcon[0].id;
insert prop;

tr.Disbursal_Amount__c=90000;
tr.Tranche_Stage__c='Tranche File Check';
tr.Request_Type__c='Tranche Disbursal';
Database.update(tr);


tr.Tranche_Stage__c='Tranche Credit Approval';
Database.update(tr);


tr.Tranche_Stage__c='Tranche Disbursement Maker';
Database.update(tr);


tr.Tranche_Stage__c='Tranche Disbursed';
Database.update(tr);


}
@isTest

public static void method5(){

Account acc=new Account();
acc.name='TestDFGWSEFS AccoountASFSADFS for trigger';
acc.phone='66666222222';
acc.PAN__c='DBUIE8289A';
acc.Date_of_Incorporation__c=date.today();
insert acc;

System.debug(acc.id);

Scheme__c sc=new Scheme__c();
sc.name='Construction Finance';
sc.Scheme_Code__c='CF';
sc.Scheme_Group_ID__c='6';
sc.Product_Code__c='HL';
insert sc;
System.debug(sc.id);

Loan_Application__c lap=new Loan_Application__c ();
lap.Customer__c=acc.id;
lap.Transaction_type__c='DAA';
lap.Scheme__c=sc.id;
lap.recordtypeid='012p00000007NLfAAM';
lap.Approved_Loan_Amount__c=5000000;
//lap.StageName__c='Tranche';
lap.Sub_Stage__c='Tranche File Check';
lap.Property_Identified__c = True;
insert lap;
System.debug('STAGE  '+lap.Sub_Stage__c);
System.debug(lap.Sub_Stage__c);
List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
Document_Master__c dm=new Document_Master__c();
dm.name='test';
dm.Doc_Id__c='asdfsdf';
dm.Expiration_Possible__c=true;
dm.FCU_Required__c=true;
dm.OTC__c=true;
insert dm;

Document_Type__c dt=new Document_Type__c();
dt.name='Any type';
dt.Document_Type_Id__c='Tranche Disbursal';
dt.Expiration_Possible__c=true;
dt.OTC__c=true;
insert dt;

Loan_Application_Document_Checklist__c ladc=new Loan_Application_Document_Checklist__c();
ladc.Document_Master__c=dm.id;
ladc.Document_Type__c=dt.id;
insert ladc;

Tranche__c tr=new Tranche__c();
tr.Loan_Application__c=lap.id;
tr.Disbursal_Amount__c=16000;
tr.Request_Type__c='Tranche Disbursal';
insert tr;


List<Tranche__c> lst=new List<Tranche__c>();
lst.add(tr);
Document_Checklist__c dc=new Document_Checklist__c();
dc.Loan_Applications__c=lap.id;
//dc.Loan_Contact__c=lstofcon[0].id;
dc.Document_Type__c=dt.id;
dc.Document_Master__c=dm.id;
dc.REquest_Date_for_OTC__c=date.today();
dc.Document_Collection_Mode__c='Photocopy';
dc.Loan_Engine_Mandatory__c=true;
dc.Express_Queue_Mandatory__c=true;
dc.status__c='Pending';
dc.File_Check_Completed__c=true;
dc.Scan_Check_Completed__c=true;
dc.Tranche__c=tr.id;
dc.Original_Seen_and_Verified__c=true;

insert dc;

List<Document_Checklist__c > ldc=new List<Document_Checklist__c >();
ldc.add(dc);

User u4=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
Title,Alias,TimeZoneSidKey,profile.name,isActive,
EmailEncodingKey,LanguageLocaleKey,
LocaleSidKey,UserRoleId 
from user where profile.Name='LIP' and isActive=true limit 1];

Third_Party_Verification__c tpv =new Third_Party_Verification__c();
tpv.Customer_Details__c=lstofcon[0].id;
tpv.Loan_Application__c=lap.id;
tpv.owner__c=u4.id;
tpv.recordtypeid='012p00000007NM6AAM';
tpv.Type_of_Verification__c='Full Profile Check';
insert tpv;


Property__c prop=new Property__c();
prop.Loan_Application__c=lap.id;
prop.First_Property_Owner__c=lstofcon[0].id;
insert prop;


}

@isTest

public static void method6(){

Account acc=new Account();
acc.name='TestDFGWSEFS AccoountASFSADFS for trigger';
acc.phone='66666222222';
acc.PAN__c='DBUIE8289A';
acc.Date_of_Incorporation__c=date.today();
insert acc;

System.debug(acc.id);

Scheme__c sc=new Scheme__c();
sc.name='Construction Finance';
sc.Scheme_Code__c='CF';
sc.Scheme_Group_ID__c='6';
sc.Product_Code__c='HL';
insert sc;
System.debug(sc.id);

Loan_Application__c lap=new Loan_Application__c ();
lap.Customer__c=acc.id;
lap.Transaction_type__c='DAA';
lap.Scheme__c=sc.id;
lap.recordtypeid='012p00000007NLfAAM';
lap.Approved_Loan_Amount__c=4000;
//lap.StageName__c='Tranche';
lap.Sub_Stage__c='Tranche File Check';
lap.Property_Identified__c = True;
insert lap;

List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];

Disbursement__c db=new Disbursement__c();
db.Loan_Application__c=lap.id;
db.Customer_Detail__c=lstofcon[0].id;
insert db;

Disbursement_payment_details__c dpd=new Disbursement_payment_details__c();
dpd.Disbursement__c=db.id;
dpd.Customer_Detail__c=lstofcon[0].id;
dpd.Amount__c=5000;
insert dpd;

System.debug('STAGE  '+lap.Sub_Stage__c);
System.debug(lap.Sub_Stage__c);

Document_Master__c dm=new Document_Master__c();
dm.name='test';
dm.Doc_Id__c='asdfsdf';
dm.Expiration_Possible__c=true;
dm.FCU_Required__c=true;
dm.OTC__c=true;
insert dm;

Document_Type__c dt=new Document_Type__c();
dt.name='Any type';
dt.Document_Type_Id__c='Tranche Disbursal';
dt.Expiration_Possible__c=true;
dt.OTC__c=true;

try{
insert dt;
}
catch(DmlException e){
System.debug('Disbursal Amount cannot be greater than the Pending Amount');

}
Loan_Application_Document_Checklist__c ladc=new Loan_Application_Document_Checklist__c();
ladc.Document_Master__c=dm.id;
ladc.Document_Type__c=dt.id;
try{
insert ladc;
}

catch(DmlException e){
System.debug('Disbursal Amount cannot be greater than the Pending Amount');

}

Tranche__c tr=new Tranche__c();
tr.Loan_Application__c=lap.id;
tr.Disbursal_Amount__c=16000;
tr.Request_Type__c='Tranche Disbursal';

try{
insert tr;
}
catch(DmlException e){
System.debug('Disbursal Amount cannot be greater than the Pending Amount');

}

System.debug('Approved Amount'+lap.Approved_Loan_Amount__c );
System.debug('DisbursedAmount '+lap.Disbursed_Amount__c );

List<Tranche__c> lst=new List<Tranche__c>();
lst.add(tr);
Document_Checklist__c dc=new Document_Checklist__c();
dc.Loan_Applications__c=lap.id;
//dc.Loan_Contact__c=lstofcon[0].id;
dc.Document_Type__c=dt.id;
dc.Document_Master__c=dm.id;
dc.REquest_Date_for_OTC__c=date.today();
dc.Document_Collection_Mode__c='Photocopy';
dc.Loan_Engine_Mandatory__c=true;
dc.Express_Queue_Mandatory__c=true;
dc.status__c='Pending';
dc.File_Check_Completed__c=true;
dc.Scan_Check_Completed__c=true;
dc.Tranche__c=tr.id;
dc.Original_Seen_and_Verified__c=true;

insert dc;

List<Document_Checklist__c > ldc=new List<Document_Checklist__c >();
ldc.add(dc);

User u4=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
Title,Alias,TimeZoneSidKey,profile.name,isActive,
EmailEncodingKey,LanguageLocaleKey,
LocaleSidKey,UserRoleId 
from user where profile.Name='LIP' and isActive=true limit 1];

Third_Party_Verification__c tpv =new Third_Party_Verification__c();
tpv.Customer_Details__c=lstofcon[0].id;
tpv.Loan_Application__c=lap.id;
tpv.owner__c=u4.id;
tpv.recordtypeid='012p00000007NM6AAM';
tpv.Type_of_Verification__c='Full Profile Check';
insert tpv;


Property__c prop=new Property__c();
prop.Loan_Application__c=lap.id;
prop.First_Property_Owner__c=lstofcon[0].id;
insert prop;




}

@isTest

public static void method7(){

Account acc=new Account();
acc.name='TestDFGWSEFS AccoountASFSADFS for trigger';
acc.phone='66666222222';
acc.PAN__c='DBUIE8289A';
acc.Date_of_Incorporation__c=date.today();
insert acc;

System.debug(acc.id);

Scheme__c sc=new Scheme__c();
sc.name='Construction Finance';
sc.Scheme_Code__c='CF';
sc.Scheme_Group_ID__c='6';
sc.Product_Code__c='HL';
insert sc;
System.debug(sc.id);

Loan_Application__c lap=new Loan_Application__c ();
lap.Customer__c=acc.id;
lap.Transaction_type__c='DAA';
lap.Scheme__c=sc.id;
lap.recordtypeid='012p00000007NLfAAM';
lap.Approved_Loan_Amount__c=5000000;
//lap.StageName__c='Tranche';
lap.Sub_Stage__c='Tranche File Check';
lap.Property_Identified__c = True;
insert lap;
System.debug('STAGE  '+lap.Sub_Stage__c);
System.debug(lap.Sub_Stage__c);
List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
Document_Master__c dm=new Document_Master__c();
dm.name='test';
dm.Doc_Id__c='asdfsdf';
dm.Expiration_Possible__c=true;
dm.FCU_Required__c=true;
dm.OTC__c=true;
insert dm;

Document_Type__c dt=new Document_Type__c();
dt.name='Any type';
dt.Document_Type_Id__c='Tranche Disbursal';
dt.Expiration_Possible__c=true;
dt.OTC__c=true;
insert dt;

Loan_Application_Document_Checklist__c ladc=new Loan_Application_Document_Checklist__c();
ladc.Document_Master__c=dm.id;
ladc.Document_Type__c=dt.id;
insert ladc;

Tranche__c tr=new Tranche__c();
tr.Loan_Application__c=lap.id;
tr.Disbursal_Amount__c=16000;
tr.Request_Type__c='Tranche Disbursal';
insert tr;


List<Tranche__c> lst=new List<Tranche__c>();
lst.add(tr);
Document_Checklist__c dc=new Document_Checklist__c();
dc.Loan_Applications__c=lap.id;
//dc.Loan_Contact__c=lstofcon[0].id;
dc.Document_Type__c=dt.id;
dc.Document_Master__c=dm.id;
dc.REquest_Date_for_OTC__c=date.today();
dc.Document_Collection_Mode__c='Photocopy';
dc.Loan_Engine_Mandatory__c=true;
dc.Express_Queue_Mandatory__c=true;
dc.status__c='Pending';
dc.File_Check_Completed__c=true;
dc.Scan_Check_Completed__c=true;
dc.Tranche__c=tr.id;
dc.Original_Seen_and_Verified__c=true;

insert dc;

List<Document_Checklist__c > ldc=new List<Document_Checklist__c >();
ldc.add(dc);

User u4=[SELECT id,ProfileId,LastName,Email,Username, CompanyName,
Title,Alias,TimeZoneSidKey,profile.name,isActive,
EmailEncodingKey,LanguageLocaleKey,
LocaleSidKey,UserRoleId 
from user where profile.Name='LIP' and isActive=true limit 1];

Third_Party_Verification__c tpv =new Third_Party_Verification__c();
tpv.Customer_Details__c=lstofcon[0].id;
tpv.Loan_Application__c=lap.id;
tpv.owner__c=u4.id;
tpv.recordtypeid='012p00000007NM6AAM';
tpv.Type_of_Verification__c='Full Profile Check';
insert tpv;


Property__c prop=new Property__c();
prop.Loan_Application__c=lap.id;
prop.First_Property_Owner__c=lstofcon[0].id;
insert prop;

Set<id> trancheidset=new set<id>();
trancheidset.add(tr.id);

List<Tranche__c> lstr=new List<Tranche__c>();
lstr.add(tr); 

TrancheTriggerHelper.checkTrancheCreation(lstr);


}

*/
}