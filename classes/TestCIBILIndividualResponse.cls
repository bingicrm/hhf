@isTest
public class TestCIBILIndividualResponse {
    @isTest
    public static void createCIBILReportTest(){
        Account acc=new Account();
        acc.name='TestAccount1';
        acc.phone='8878799090';
        acc.PAN__c='fjauy0916u';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        System.debug(acc.id);
        
        Scheme__c sc=new Scheme__c();
        sc.name='TestScheme';
        sc.Scheme_Code__c='testcode';
        sc.Scheme_Group_ID__c='23131';
        sc.Product_Code__c='HL';
        insert sc;
        System.debug(sc.id);
        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Transaction_type__c='PB';
        lap.Scheme__c=sc.id;
        insert lap;
        System.debug(lap.id);
        
        City__c city=new city__c();
        city.City_ID__c='100000';
        city.Name='TEZPOOR';
        city.Country__c='1';
        city.State__c='5';
        insert city;
        Pincode__c pin=new Pincode__c();
        pin.City_LP__c=city.id;
        pin.ZIP_ID__c='46172';
        insert pin;
        list<Loan_Contact__c> lc=[select id, name from Loan_Contact__c where Loan_Applications__c=:lap.id limit 1];
        system.debug('loan contact'+lc[0]);
        Address__c address=new Address__c();
        address.Address_Line_1__c='abc Street';
        address.Address_Line_2__c='abc Area';
        address.Pincode_LP__c=pin.id;
        address.Type_of_address__c='PERMNENT';
        address.Loan_Contact__c=lc[0].id;
        //insert address;
        //list<customer_integration__c> ci=[select id, name from loan_A where loan_application__c=:lap.id limit 1 ];
        system.debug('loan contact'+lc[0]);
        customer_integration__c ci =new customer_integration__c();
        ci.Loan_Application__c  =lap.id;
        ci.Loan_Contact__c=lc[0].id;
        insert ci;
        CIBILIndividualResponse.Obligations o=new CIBILIndividualResponse.Obligations();
        o.loanType='testLoanType';
        o.currentStatus='testCurrentStatus';
        o.writeOffFlag='testwriteOffFlag';
        o.emiAmount=10.1;
        o.repaymentTenure=1;
        o.balance=10.1;
        o.countDPD3Months=3;
        o.maxPeakDPD3Months=1;
        o.countDPD12Months=2;
        o.maxPeakDPD12Months=3;
        o.accountType = 'abc';
        o.accountNumber = '123';
        o.amountOverdue = 123;
        o.currentBalance = 123;
        o.dateClosed = '30012010';
        o.dateOfLastPayment = '30012010';
        o.dateOpenedOrDisbursed = '30012010';
        o.dateReportedandCertified= '30012010';
        o.reportingMemberShortName = 'abc';
        o.highCreditOrSanctionedAmount = 123;
        o.ownershipIndicator = 'abc';
        o.paymentFrequency = 12;
        o.paymentHistory1 = 'abc';
        o.paymentHistory2= 'abc';
        o.paymentHistoryEndDate = '30012010';
        o.paymentHistoryStartDate = '30012010';
        o.rateOfInterest = 12;
        o.settlementAmount = 12;
        o.suitFiledorWilfulDefault = 'abc';
        o.typeOfCollateral = 'abc';
        o.valueOfCollateral = 12;
        o.writtenOffAmountPrincipal = 123;
        o.writtenOffAndSettledStatus = 'abc';
        o.creditLimit = 123;
        o.cashLimit = 123;
        o.writtenOffAmountTotal = 123;
        list<CIBILIndividualResponse.Obligations> cIR=new list <CIBILIndividualResponse.Obligations>();
        cIR.add(o);
        
        CIBILIndividualResponse.enquiryList enq = new CIBILIndividualResponse.enquiryList();
        enq.dateReported = '30012010';
        enq.enquiryPurpose = 'abc';
        enq.enquiryAmount = 123;
        enq.reportingMemberShortName = 'abc';
        list<CIBILIndividualResponse.enquiryList> enqList =new list <CIBILIndividualResponse.enquiryList>();
        enqList.add(enq);
        
        CIBILIndividualResponse.employmentList emp = new CIBILIndividualResponse.employmentList();
        emp.accountType = 'abc';
        emp.income = 123;
        emp.occupationCode = 'abc';
        emp.netGrossIndicator = 'abc';
        emp.monthlyAnnuallyIndicator = 'abc';
        emp.dateReported = '30012010';
        List<CIBILIndividualResponse.employmentList> empList = new List<CIBILIndividualResponse.employmentList>();
        empList.add(emp);
        
        CIBILIndividualResponse.addressList add = new CIBILIndividualResponse.addressList();
        add.addressCategory = '01';
        add.residenceCode = '01';
        add.addressLine1 = 'abc';
        add.addressLine2 = 'abc';
        add.addressLine3 = 'abc';
        add.addressLine4 = 'abc';
        add.addressLine5 = 'abc';
        add.stateCode = 'abc';
        add.pinCode = '';
        add.dateReported = '30012010';
        add.enrichedThroughtEnquiry = 'true';
        List<CIBILIndividualResponse.addressList> addList = new List<CIBILIndividualResponse.addressList>();
        addList.add(add);
        
        CIBILIndividualResponse.idList iddet =new CIBILIndividualResponse.idList();
        iddet.IdType = 'abc';
        iddet.IdValue = 'abc';
        iddet.issueDate = '30012010';
        iddet.expirationDate = '30012010';
        iddet.enrichedThroughtEnquiry = 'true';
        List<CIBILIndividualResponse.idList> iddetList =new List<CIBILIndividualResponse.idList>();
        iddetList.add(iddet);
        
        CIBILIndividualResponse.CibilRequest c=new CIBILIndividualResponse.CibilRequest();
        c.parentId=ci.id;
        c.accountWithOverdue=1;
        c.accountZeroBalance=1;
        c.addressMatch=true;
        c.bureauName='bureauName';
        c.cibilScore='cibilScore';
        c.creditSanctionAmount=10.2;
        c.currentBalance=11.1;
        c.errorMsg='errorMsg';
        c.mobileMatch=true;
        c.Obligations=cIR;
        c.enquiryList = enqList;
        c.employmentList = empList;
        c.addressList = addList;
        c.idList = iddetList;
        c.oldestOpenedDate=date.today();
        c.panMatch=true;
        c.recentEnquiryDate=date.today();
        c.recentOpenedDate=date.today();
        c.totalAccount=1;
        c.totalEnquiryCount=3;
        c.totalEnquiryCount2Y=4;
        c.totalEnquiryCountX1Y=2;
        c.totalEnquiryCountX30D=3;
        CIBILIndividualResponse.createCIBILReport(c);
        //CIBILIndividualResponse.getObligations(o,ci.id);
    }

}