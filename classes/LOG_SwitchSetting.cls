/**
 *    @author          : Deloitte Digital - SFDC
 *    @date            : 04/04/2016
 *    @description     : Class for custom setting 'Controller_Settings__c' records.
 *    Modification Log : 
 *    ------------------------------------------------------------------------------------     
 *    Developer                       Date                Description      
 *    ------------------------------------------------------------------------------------      
 *    Shilpa Menghani                04/04/2016            Original Version
 */
public without sharing class LOG_SwitchSetting 
{
    //DLOG Switch record list
    public List<Controller_Settings__c> lstSwitch {get;set;}
    
    //Paginated DLOG Switch record list
    public List<Controller_Settings__c> lstSetController{get;set;}
    
    // Custom Iteration instance
    //CustomIterable obj;
    
    /**
     * Constructor 
     */
    public LOG_SwitchSetting() 
    {
        lstSwitch =  new List<Controller_Settings__c>();
        lstSetController = new List<Controller_Settings__c>();
        /**
        *Get list of Profiles
        */
        List<Profile> profileList = [SELECT Id, Name, Description FROM Profile];
         for(Profile profObject : profileList){
            Controller_Settings__c logSwitch = new Controller_Settings__c();
            
            logSwitch  = Controller_Settings__c.getValues(profObject.Id);
                      
            if(logSwitch == null){
              logSwitch = new Controller_Settings__c();
              logSwitch.Name = profObject.Name;
              logSwitch.SetupOwnerId = profObject.Id;
              logSwitch.Logs_Enabled__c = true;
              logSwitch.Log_Integration_Errors__c = true;
                
            }
            else{
              logSwitch = [select SetupOwner.Name,Name,Logs_Enabled__c,Log_Integration_Errors__c,SetupOwnerId from Controller_Settings__c where Id =: logSwitch.Id limit 1];
              
            }
           
             lstSwitch.add(logSwitch);
        }
        //Pass list for pagintaion
       // obj = new CustomIterable(lstSwitch); 
       // obj.setPageSize = 8;
       // next();         
    }
    
    
        /*public Boolean hasNext {
            get 
            {
                return obj.hasNext();
            }
            set;
        }
        
        public Boolean hasPrevious {
            get 
            {
                return obj.hasPrevious();
            }
            set;
        }
        
        public void next() 
        {
            lstSetController = obj.next();
            
        }
        
        public void previous() 
        {
            lstSetController = obj.previous();
        }*/
        
        
     /** This method is called on Save button click
     * Add mapping to the database, if their datatype match
     * And also get the latest data for page 
     */
    public Pagereference Save(){
        try{
            if(lstSetController != null && !lstSetController.isEmpty()){
                upsert lstSetController;
            }
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,'Record Saved')); 
        }
        catch(Exception ex){
            apexpages.addMessages(ex);          
            return null;
        }
     return  null;   
    }
    
}