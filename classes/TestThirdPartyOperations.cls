@IsTest(isParallel=false)

private class TestThirdPartyOperations 
{
   
    static testmethod void test1(){
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
    system.debug('portalRole is ' + portalRole);
         User thisUser = [Select Id from User where Id = :UserInfo.getUserId()]; 
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
    User portalAccountOwner1 = new User(
    UserRoleId = portalRole.Id,
    ProfileId = profile1.Id,
    Username = System.now().millisecond() + 'test2123@test.com',
    Alias = 'batman',
    Email='bruce.wayne@wayneenterprises.com',
    EmailEncodingKey='UTF-8',
    Firstname='Bruce',
    Lastname='Wayne',
    LanguageLocaleKey='en_US',
    LocaleSidKey='en_US',
    TimeZoneSidKey='America/Chicago'
    );
    Database.insert(portalAccountOwner1);
    
    //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
    
    System.runAs ( portalAccountOwner1 ) {
    //Create account
    Account portalAccount1 = new Account(
    Name = 'TestAccount',
    OwnerId = portalAccountOwner1.Id
    );
    Database.insert(portalAccount1);
    
    //Create contact
    Contact contact1 = new Contact(
    FirstName = 'Test',
    Lastname = 'McTesty',
    AccountId = portalAccount1.Id,
    Email = System.now().millisecond() + 'test@test.com'
    );
    Database.insert(contact1);
        Branch_Master__c br=new Branch_Master__c(Name='DummyBranch',Pincode_Name__c='302001',Office_Code__c='123456');
            insert br; 
    //Create user
    Profile portalProfile = [SELECT Id FROM Profile where Name ='Third Party Vendor' Limit 1];
    User user1 = new User(
    Username = System.now().millisecond() + 'test12345testagain@test.com',
    ContactId = contact1.Id,
    ProfileId = portalProfile.Id,
    Alias = 'test123',
    Email = 'test12345@test.com',
    EmailEncodingKey = 'UTF-8',
    LastName = 'McTesty',
    CommunityNickname = 'test12345',
    TimeZoneSidKey = 'America/Los_Angeles',
    LocaleSidKey = 'en_US',
    LanguageLocaleKey = 'en_US',
    Technical__c = TRUE,
        Location__c = 'DummyBranch'
    );
    Database.insert(user1);
      
    }
    }
      public static testmethod void  Testcreate(){
          Profile portalProfile1 = [SELECT Id FROM Profile where Name ='Third Party Vendor' Limit 1];
         User thisUser = [Select Id from User where Id = :UserInfo.getUserId()]; 
       
        System.runAs(thisUser)
        {   
         UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
            Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
            User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test1212121212@test.com',
            Alias = 'batman',
            Email='dummy@dummy.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago');
            Database.insert(portalAccountOwner1);
        
        Id RecordTypeIdAccount1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
            
        List<Account> lstOfAccounts = new List<Account> ();
        
        Account acc=new Account();
        acc.Salutation='Mr';
        acc.FirstName='Test';
        acc.LastName='TestAccount1';
        acc.phone='9234567667';
        //acc.PAN__c='fjauy0916u';
        //acc.Gender__c = 'F';
        acc.Date_of_Incorporation__c=date.today();
        acc.RecordTypeId=RecordTypeIdAccount1;
        lstOfAccounts.add(acc);
        
        System.debug(acc.id);
        
        Account acc1=new Account();
        acc1.Salutation='Mr';
        acc1.FirstName='Test';
        acc1.LastName='TestAcc11';
        acc1.phone='9234567667';
        //acc1.PAN__c='fjauy0916u';
        acc1.Gender__c = 'F';
        acc1.Date_of_Incorporation__c=date.today();
        acc1.RecordTypeId=RecordTypeIdAccount1;
        lstOfAccounts.add(acc1);
        //insert lstOfAccounts;

            Branch_Master__c br=new Branch_Master__c(Name='DummyBranch',Pincode_Name__c='302001',Office_Code__c='123456');
            insert br;    
          

        Scheme__c sc=new Scheme__c();
        sc.name='TestScheme';
        sc.Scheme_Code__c='testcode';
        sc.Scheme_Group_ID__c='23131';
        sc.Product_Code__c='HL';
        insert sc;
        System.debug(sc.id);
        
        Branch_Master__c objBranchMaster = new Branch_Master__c(Name='Test Branch',Office_Code__c='TestOC');
        insert objBranchMaster;

        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Branch__c='test';
        lap.Branch_Lookup__c=objBranchMaster.Id;
        lap.Transaction_type__c='PB';
        lap.Scheme__c=sc.id;
        lap.StageName__c='Customer Onboarding';
        lap.Requested_Amount__c=50000;

        insert lap;
        System.debug(lap.id);
        


            //Create account
            Account portalAccount1 = new Account(
            Name = 'TestAccount',
            OwnerId = portalAccountOwner1.Id);
            
            Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
            
            Account acc4 = new Account(PAN__c='QWERT1567J',Name='Account1564',Date_of_Incorporation__c=System.Today(),
                                  Phone='9345678910',RecordTypeId=RecordTypeIdAccount);
            lstOfAccounts.add(acc4);
        insert lstOfAccounts;

            
            //Create contact
            Contact contact1 = new Contact(
            FirstName = 'Tt',
            Lastname = 'Mcy',
            AccountId = portalAccount1.Id,
            Email = System.now().millisecond() + 'tot@test.com');
            
                        Contact contact2 = new Contact(
            FirstName = 'Test1',
            Lastname = 'McTesty1',
            AccountId = portalAccount1.Id,
            Email = System.now().millisecond() + 'test1@test.com');
            
             Contact contact3 = new Contact(
            FirstName = 'Test3',
            Lastname = 'McTesty3',
            AccountId = portalAccount1.Id,
            Email = System.now().millisecond() + 'test1@test.com');
            
            list<contact> lstContact = new list<contact>();
            lstContact.add(contact1);
            lstContact.add(contact2);
            lstContact.add(contact3);
          
            Database.insert(lstContact);
  Profile portalProfile = [SELECT Id FROM Profile WHERE Name='Legal Vendor' Limit 1];
            Profile p2 = [SELECT Id FROM Profile WHERE Name='FCU Vendor' Limit 1];
            Profile p3 = [SELECT Id FROM Profile WHERE Name='Third Party Vendor' Limit 1];
        
         User unew1=new User(Username ='te4009@test.com',ContactId = contact2.Id,
                            ProfileId = p2.Id,Alias = 'te3',Email = 'te4009@test.com',EmailEncodingKey = 'UTF-8',
                            LastName = 'McTesty',CommunityNickname = 'tefst12345',TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US', IsActive = TRUE);
      
         
            Contact con = new Contact(LAstNAme='test677', Accountid=acc4.Id);
          insert con;
            
            Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Co-Applicant').getRecordTypeId();
            
            //Id RecordTypeIdLoanContact1 = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Guarantor').getRecordTypeId();
                                         test.startTest();

            Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=lap.Id,Customer__c=acc4.Id,
                                                             Applicant_Type__c='Applicant',Borrower__c='2',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             //Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='19',
                                                             Customer_segment__c='11',Applicant_Status__c='1',
                                                             Income_Program_Type__c='Liquid Income Program',
                                                             Contact__c=con.id,
                                                             Income_Considered__c=true,Mandatory_Fields_Completed__c=true
                                                             );
          insert objLoanContact;
                      test.stopTest();

            
            Loan_Contact__c objLC = [SELECT Id from Loan_Contact__c WHERE Applicant_Type__c='Applicant' AND Loan_Applications__c=:lap.Id LIMIT 1];
            
        List<User> lstUser = [SELECT Id from User WHERE Profile.Name='Sales Team' AND UserRole.Name LIKE '%SM%'];
        
        DSA_Master__c objDSAMaster = new DSA_Master__c(Name='A JAGAN MOHAN',Type__c='DSA', BrokerID__c = '1234',Manager__c=lstUser[0].Id);
        insert objDSAMaster;
        Id RecordTypeIdSourceDetail = Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('DSA').getRecordTypeId();
        Sourcing_Detail__c objSourceDetail = new Sourcing_Detail__c(Loan_Application__c=lap.Id,Sales_User_Name__c=lstUser[0].Id,
                                                                RecordTypeId=RecordTypeIdSourceDetail,DSA_Name__c=objDSAMaster.Id);
        System.debug('lstUser[0].Id::::'+lstUser[0].Id + 'DSA_Name__r.Manager__c'+objSourceDetail.DSA_Name__r.Manager__c);
        insert objSourceDetail;
        
        lap.StageName__c='Operation Control';
        lap.Property_Identified__c = true;
        //lap.Sub_Stage__c='Scan: Data Maker';
            
            update lap;
            
            
            
        City__c city=new city__c();
        city.City_ID__c='100000';
        city.Name='TEZPOOR';
        city.Country__c='1';
        city.State__c='5';
        insert city;
        Pincode__c pin=new Pincode__c();
        pin.City_LP__c=city.id;
        pin.ZIP_ID__c='46172';
        insert pin;
        list<Loan_Contact__c> lc=[select id, name from Loan_Contact__c where Loan_Applications__c=:lap.id limit 1];
        system.debug('loan contact'+lc[0]);
        List<Address__c> lstAdd= new List<Address__c>();
        Address__c address=new Address__c();
        address.Address_Line_1__c='abc Street';
        address.Address_Line_2__c='abc Area';
        address.Pincode_LP__c=pin.id;
        address.Type_of_address__c='PERMNENT';
        address.Loan_Contact__c=lc[0].id;
        insert address;
        lstAdd.add(address);
        
        // database.delete(acc.id);
        // list<Loan_Contact__c> lc=[select id, name from Loan_Contact__c where Loan_Applications__c=:lap.id limit 1];
        
        system.debug('loan contact'+lc[0]);
        lc[0].Borrower__c='2';
        lc[0].Date_of_Birth__c=Date.newInstance(2000, 11, 29); 
        lc[0].Mobile__c='9876543210';
        lc[0].Email__c='testemail@gmail.com';
        //lc[0].Recordtypeid = rtIndividual;    
        update lc[0];
        List<Property__c> lstProprty = new List<Property__c>();
        Property__c objProperty = new Property__c(Loan_Application__c=lap.id,Pincode_LP__c=pin.Id,
                                                 First_Property_Owner__c=lc[0].Id,Address_Line_1__c='Test Address Line 1',
                                                 Address_line_2__c='Test Address Line 2',Type_of_Property__c='7',Type_Property__c='Residential');
        lstProprty.add(objProperty) ;                                        
        insert lstProprty;
          List<User_Branch_Mapping__c> li=new List<User_Branch_Mapping__c>();
      
           /* User_Branch_Mapping__c ubrmap1= new User_Branch_Mapping__c(Name='DummyMap1',User__c=portalUser.id,Servicing_Branch__c=objBranchMaster.Id,Sourcing_Branch__c=objBranchMaster.Id);
            li.add(ubrmap1);
       
            insert li;*/
          
            
           List<FI_Applicability__c> lstFi = new List<FI_Applicability__c>();
            FI_Applicability__c fia= new FI_Applicability__c(FI__c=true, Scheme_Lookup__c=sc.id, Customer_Segment__c='11', Transaction_type__c='PB', Type_of_Address__c='PERMNENT', Loan_Contact_Type__c='Applicant');
            lstFi.add(fia);
            insert lstFi;
           List<Technical_Evaluation__c> lstTech = new List<Technical_Evaluation__c>();
            Technical_Evaluation__c tev= new Technical_Evaluation__c(Scheme_Lookup__c=sc.id, Transaction_type__c='PB', Type_of_Property__c='Residential', Threshold_Amount__c=10, Evaluation_Count__c=1);
            lstTech.add(tev);
            Technical_Evaluation__c tev1= new Technical_Evaluation__c(Scheme_Lookup__c=sc.id, Transaction_type__c='PB', Type_of_Property__c='Mixed', Threshold_Amount__c=1, Evaluation_Count__c=3);
            lstTech.add(tev1);
            insert lstTech;
        List<Vendor_PD_Matrix__c> lstVD = new LisT<Vendor_PD_Matrix__c>();
        Vendor_PD_Matrix__c vpd= new Vendor_PD_Matrix__c(Scheme_Lookup__c=sc.id, Branch_Lookup__c=br.id, Customer_Segment__c='9', Transaction_type__c='PB', Vendor_PD__c=true);
           lstVD.add(vpd) ;
            insert lstVD;
         Id rt = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('Legal').getRecordTypeId();
        Id rt1 = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('FCU').getRecordTypeId();
        Id rt2 = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('LIP').getRecordTypeId();
        Id rt3 = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('FI').getRecordTypeId();
        Id rt4 = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('Vendor PD').getRecordTypeId();
        Id rt5 = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('Technical').getRecordTypeId();
        
     /*       List<Third_Party_Verification__c> tpvList=new List<Third_Party_Verification__c>();
            Third_Party_Verification__c tpv11= new Third_Party_Verification__c(recordTypeId=rt1,Customer_Details__c=lc[0].id,Owner__c=unew1.id,Loan_Application__c=lap.id,Status__c='Initiated',Manually_Created_Record__c=true,Type_of_Verification__c='Full Profile Check');
          
            tpvlist.add(tpv11);
            
           insert tpvlist;*/
                       Map<Id, List<Address__c>> conAdd = new Map<Id, List<Address__c>>();
                       for(Address__c add: lstAdd){
                       conAdd.put(add.Loan_Contact__c,lstAdd);
                       }
                       Map<Id, List<Property__c>> loanProp = new Map<Id, List<Property__c>>();
                       for(Property__c add: lstProprty){
                       loanProp.put(add.Loan_Application__c,lstProprty);
                       }
                                              System.debug('conAdd:::'+conAdd);
                                              Decimal value;
                    for(Technical_Evaluation__c tech : lstTech){
                    if(tech.Scheme_Lookup__c == lap.Scheme__c && tech.Transaction_Type__c == lap.Transaction_Type__c && tech.Type_of_Property__c == lstProprty[0].Type_Property__c && lap.Requested_Amount__c >= tech.Threshold_Amount__c){
                    system.debug('---Evaluation Count---'+tech.Evaluation_Count__c);
                    value = tech.Evaluation_Count__c;
                    }
                    }
            Document_Type__c docType = new Document_Type__c(Name = Constants.PROPERTY_PAPERS);
            insert docType;
      Document_Checklist__c docs = new Document_Checklist__c(Loan_Applications__c = lap.Id,Document_Type__c = docType.Id);
            insert docs;
              ThirdPartyOperations.getMessage();
            ThirdPartyOperations.getLoanApp(lap.id);
            ThirdPartyOperations.thirdPartyCall(lap);
            ThirdPartyOperations.addressVerifications(lap,objLoanContact,conAdd,lstFi,li,lstVD,rt3,rt2,rt4,li);
            ThirdPartyOperations.fcuVerification(lap, objLoanContact, li, rt1, li);
            ThirdPartyOperations.propertyVerifications(lap, loanProp, li, li, lstTech,value , rt1, rt5, li);
      ThirdPartyOperations.propertyTrueOperations(lap);
        }
        
    }
    
     public static testmethod void  Testcreate1(){
         User thisUser = [Select Id from User where Id = :UserInfo.getUserId()]; 
          
        System.runAs(thisUser)
        {  

         UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
            Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
            User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'testasdfasfasasdfasdfasdf2@test.com',
            Alias = 'batman',
            Email='dummy@dummy.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago');
            Database.insert(portalAccountOwner1);
        
        List<Account> lstOfAccounts = new List<Account> ();
        
        Account acc=new Account();
        acc.name='TestAccount1';
        acc.phone='9234567667';
        acc.PAN__c='fjauy0916u';
        acc.Gender__c = 'F';
        acc.Date_of_Incorporation__c=date.today();
        lstOfAccounts.add(acc);
        
        System.debug(acc.id);
        
        Account acc1=new Account();
        acc1.name='TestAcc11';
        acc1.phone='9234567667';
        //acc1.PAN__c='fjauy0916u';
        acc1.Gender__c = 'F';
        acc1.Date_of_Incorporation__c=date.today();
        lstOfAccounts.add(acc1);
        //insert lstOfAccounts;

            Branch_Master__c br=new Branch_Master__c(Name='DummyBranch',Pincode_Name__c='302001',Office_Code__c='123456');
            insert br;    
          

        Scheme__c sc=new Scheme__c();
        sc.name='TestScheme';
        sc.Scheme_Code__c='testcode';
        sc.Scheme_Group_ID__c='23131';
        sc.Product_Code__c='HL';
        insert sc;
        System.debug(sc.id);
        
        Branch_Master__c objBranchMaster = new Branch_Master__c(Name='Test Branch',Office_Code__c='TestOC');
        insert objBranchMaster;

        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Branch__c='test';
        lap.Branch_Lookup__c=objBranchMaster.Id;
        lap.Transaction_type__c='PB';
        lap.Scheme__c=sc.id;
        lap.StageName__c='Customer Onboarding';
        lap.Requested_Amount__c=50000;

        insert lap;
        System.debug(lap.id);
        


            //Create account
            Account portalAccount1 = new Account(
            Name = 'TestAccount',
            OwnerId = portalAccountOwner1.Id);
            
            Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
            
            Account acc4 = new Account(PAN__c='QWERT1567J',Name='Account1564',Date_of_Incorporation__c=System.Today(),
                                  Phone='9345678910',RecordTypeId=RecordTypeIdAccount);
            lstOfAccounts.add(acc4);
        insert lstOfAccounts;

            
            //Create contact
            Contact contact1 = new Contact(
            FirstName = 'Tt',
            Lastname = 'Mcy',
            AccountId = portalAccount1.Id,
            Email = System.now().millisecond() + 'tot@test.com');
            
                        Contact contact2 = new Contact(
            FirstName = 'Test1',
            Lastname = 'McTesty1',
            AccountId = portalAccount1.Id,
            Email = System.now().millisecond() + 'test1@test.com');
            
       
            list<contact> lstContact = new list<contact>();
            lstContact.add(contact1);
            lstContact.add(contact2);

            Database.insert(lstContact);
  Profile portalProfile = [SELECT Id FROM Profile WHERE Name='Legal Vendor' Limit 1];
            Profile p2 = [SELECT Id FROM Profile WHERE Name='FCU Vendor' Limit 1];
        
         User unew1=new User(Username ='te4009@test.com',ContactId = contact2.Id,
                            ProfileId = p2.Id,Alias = 'te3',Email = 'te4009@test.com',EmailEncodingKey = 'UTF-8',
                            LastName = 'McTesty',CommunityNickname = 'tefst12345',TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US', IsActive = TRUE);
            
        
            
            
            Contact con = new Contact(LAstNAme='test677', Accountid=acc4.Id);
          insert con;
            
            Id RecordTypeIdLoanContact = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Co-Applicant').getRecordTypeId();
            
            //Id RecordTypeIdLoanContact1 = Schema.SObjectType.Loan_Contact__c.getRecordTypeInfosByName().get('Guarantor').getRecordTypeId();
                                         test.startTest();

            Loan_Contact__c objLoanContact = new Loan_Contact__c(Loan_Applications__c=lap.Id,Customer__c=acc4.Id,
                                                             Applicant_Type__c='Applicant',Borrower__c='2',
                                                             RecordTypeId=RecordTypeIdLoanContact,
                                                             //Relationship_with_Applicant__c='PTN',
                                                             Pan_Verification_status__c=true,Constitution__c='19',
                                                             Customer_segment__c='11',Applicant_Status__c='1',
                                                             Income_Program_Type__c='Liquid Income Program',
                                                             Contact__c=con.id,
                                                             Income_Considered__c=true,Mandatory_Fields_Completed__c=true
                                                             );
          insert objLoanContact;
                      test.stopTest();

            
            Loan_Contact__c objLC = [SELECT Id from Loan_Contact__c WHERE Applicant_Type__c='Applicant' AND Loan_Applications__c=:lap.Id LIMIT 1];
            
        List<User> lstUser = [SELECT Id from User WHERE Profile.Name='Sales Team' AND UserRole.Name LIKE '%SM%'];
        
        DSA_Master__c objDSAMaster = new DSA_Master__c(Name='A JAGAN MOHAN',Type__c='DSA', BrokerID__c = '1234',Manager__c=lstUser[0].Id);
        insert objDSAMaster;
        Id RecordTypeIdSourceDetail = Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('DSA').getRecordTypeId();
        Sourcing_Detail__c objSourceDetail = new Sourcing_Detail__c(Loan_Application__c=lap.Id,Sales_User_Name__c=lstUser[0].Id,
                                                                RecordTypeId=RecordTypeIdSourceDetail,DSA_Name__c=objDSAMaster.Id);
        System.debug('lstUser[0].Id::::'+lstUser[0].Id + 'DSA_Name__r.Manager__c'+objSourceDetail.DSA_Name__r.Manager__c);
        insert objSourceDetail;
        
        lap.StageName__c='Operation Control';
        //lap.Sub_Stage__c='Scan: Data Maker';
            
            update lap;
            
            
            
        City__c city=new city__c();
        city.City_ID__c='100000';
        city.Name='TEZPOOR';
        city.Country__c='1';
        city.State__c='5';
        insert city;
        Pincode__c pin=new Pincode__c();
        pin.City_LP__c=city.id;
        pin.ZIP_ID__c='46172';
        insert pin;
        list<Loan_Contact__c> lc=[select id, name from Loan_Contact__c where Loan_Applications__c=:lap.id limit 1];
        system.debug('loan contact'+lc[0]);
        List<Address__c> lstAdd= new List<Address__c>();
        Address__c address=new Address__c();
        address.Address_Line_1__c='abc Street';
        address.Address_Line_2__c='abc Area';
        address.Pincode_LP__c=pin.id;
        address.Type_of_address__c='CUROFF';
        address.Loan_Contact__c=lc[0].id;
        insert address;
            
        lstAdd = [select id,Address_Line_1__c,Address_Line_2__c,Pincode_LP__c,Type_of_address__c,Loan_Contact__c,Address_Type__c,TP_Address__c,Complete_Address__c from Address__c]; 
        system.debug('lstAdd'+lstAdd);
        // database.delete(acc.id);
        // list<Loan_Contact__c> lc=[select id, name from Loan_Contact__c where Loan_Applications__c=:lap.id limit 1];
        system.debug('loan contact'+lc[0]);
        lc[0].Borrower__c='2';
        lc[0].Date_of_Birth__c=Date.newInstance(2000, 11, 29); 
        update lc;
        List<Property__c> lstProprty = new List<Property__c>();
        Property__c objProperty = new Property__c(Loan_Application__c=lap.id,Pincode_LP__c=pin.Id,
                                                 First_Property_Owner__c=lc[0].Id,Address_Line_1__c='Test Address Line 1',
                                                 Address_line_2__c='Test Address Line 2',Type_of_Property__c='7',Type_Property__c='Residential');
        lstProprty.add(objProperty) ;                                        
        insert lstProprty;
        
          List<User_Branch_Mapping__c> li=new List<User_Branch_Mapping__c>();
        
            /*User_Branch_Mapping__c ubrmap1= new User_Branch_Mapping__c(Name='DummyMap1',User__c=unew1.id,Servicing_Branch__c=objBranchMaster.Id,Sourcing_Branch__c=objBranchMaster.Id);
            li.add(ubrmap1);
     
            insert li;*/
           
           List<FI_Applicability__c> lstFi = new List<FI_Applicability__c>();
            FI_Applicability__c fia= new FI_Applicability__c(FI__c=true, Scheme_Lookup__c=sc.id, Customer_Segment__c='11', Transaction_type__c='PB', Type_of_Address__c='CUROFF', Loan_Contact_Type__c='Applicant');
            lstFi.add(fia);
            insert lstFi;
           List<Technical_Evaluation__c> lstTech = new List<Technical_Evaluation__c>();
            Technical_Evaluation__c tev= new Technical_Evaluation__c(Scheme_Lookup__c=sc.id, Transaction_type__c='PB', Type_of_Property__c='Residential', Threshold_Amount__c=10, Evaluation_Count__c=1);
            lstTech.add(tev);
            Technical_Evaluation__c tev1= new Technical_Evaluation__c(Scheme_Lookup__c=sc.id, Transaction_type__c='PB', Type_of_Property__c='Mixed', Threshold_Amount__c=1, Evaluation_Count__c=3);
            lstTech.add(tev1);
            insert lstTech;
        List<Vendor_PD_Matrix__c> lstVD = new LisT<Vendor_PD_Matrix__c>();
        Vendor_PD_Matrix__c vpd= new Vendor_PD_Matrix__c(Scheme_Lookup__c=sc.id, Branch_Lookup__c=br.id, Customer_Segment__c='11', Transaction_type__c='PB', Vendor_PD__c=true);
           Vendor_PD_Matrix__c vpd1= new Vendor_PD_Matrix__c(Scheme_Lookup__c=sc.id, Branch_Lookup__c=objBranchMaster.id, Customer_Segment__c='11', Transaction_type__c='PB', Vendor_PD__c=true,Applicant_Type__c='Applicant',Income_Considered__c=true);
           lstVD.add(vpd) ;
             lstVD.add(vpd1) ;
            insert lstVD;
         Id rt = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('Legal').getRecordTypeId();
        Id rt1 = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('FCU').getRecordTypeId();
        Id rt2 = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('LIP').getRecordTypeId();
        Id rt3 = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('FI').getRecordTypeId();
        Id rt4 = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('Vendor PD').getRecordTypeId();
        Id rt5 = Schema.SObjectType.Third_Party_Verification__c.getRecordTypeInfosByName().get('Technical').getRecordTypeId();
        
          /*  List<Third_Party_Verification__c> tpvList=new List<Third_Party_Verification__c>();
            //Third_Party_Verification__c tpv= new Third_Party_Verification__c(recordTypeId=rt.Id,Customer_Details__c=objLoanContact.id,Owner__c=u.id,Loan_Application__c=objLoanApplication.id,Status__c='Initiated',Manually_Created_Record__c=true);
            Third_Party_Verification__c tpv11= new Third_Party_Verification__c(recordTypeId=rt1,Customer_Details__c=lc[0].id,Owner__c=unew1.id,Loan_Application__c=lap.id,Status__c='Initiated',Manually_Created_Record__c=true);
            Third_Party_Verification__c tpv12= new Third_Party_Verification__c(recordTypeId=rt2,Customer_Details__c=lc[0].id,Owner__c=unew1.id,Loan_Application__c=lap.id,Status__c='Initiated',Manually_Created_Record__c=true);
            Third_Party_Verification__c tpv13= new Third_Party_Verification__c(recordTypeId=rt,Customer_Details__c=lc[0].id,Owner__c=unew1.id,Loan_Application__c=lap.id,Status__c='Initiated',Manually_Created_Record__c=true);
        
           // tpvlist.add(tpv);
            tpvlist.add(tpv11);
            
            tpvlist.add(tpv12);
            Third_Party_Verification__c tpv2= new Third_Party_Verification__c(recordTypeId=rt,Loan_Application__c=lap.id,Owner__c=unew1.id,Customer_Details__c=lc[0].id,Status__c='New',Manually_Created_Record__c=true);
                        tpvlist.add(tpv2);
                       insert tpvlist;*/
                       Map<Id, List<Address__c>> conAdd = new Map<Id, List<Address__c>>();
                       for(Address__c add: lstAdd){
                       conAdd.put(add.Loan_Contact__c,lstAdd);
                       }
                       Map<Id, List<Property__c>> loanProp = new Map<Id, List<Property__c>>();
                       for(Property__c add: lstProprty){
                       loanProp.put(add.Loan_Application__c,lstProprty);
                       }
                                              System.debug('conAdd:::'+conAdd);
                                              Decimal value;
                    for(Technical_Evaluation__c tech : lstTech){
                    if(tech.Scheme_Lookup__c == lap.Scheme__c && tech.Transaction_Type__c == lap.Transaction_Type__c && tech.Type_of_Property__c == lstProprty[0].Type_Property__c && lap.Requested_Amount__c >= tech.Threshold_Amount__c){
                    system.debug('---Evaluation Count---'+tech.Evaluation_Count__c);
                    value = tech.Evaluation_Count__c;
                    }
                    }

            //  ThirdPartyOperations.getMessage();
           // ThirdPartyOperations.getLoanApp(lap.id);
           // ThirdPartyOperations.thirdPartyCall(lap);
            ThirdPartyOperations.addressVerifications(lap,objLoanContact,conAdd,lstFi,li,lstVD,rt3,rt2,rt4,li);
           // ThirdPartyOperations.fcuVerification(lap, objLoanContact, li, rt1, li);
            ThirdPartyOperations.propertyVerifications(lap, loanProp, li, li, lstTech,value , rt1, rt5, li);

        }
        
    }
}