@IsTest
public class TestCamSummary {
    
    public static testmethod void method1(){
        Account acc=new Account();
        acc.name='TesstMWDFSADF';
        acc.phone='9688467902';
        acc.PAN__c='YCJDP7834G';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        Document_Master__c objdoc = new Document_Master__c(Name = 'Sanction Letter', Doc_Id__c = '123');
        insert objdoc;
        
        
        System.debug(acc.id);
        
        Scheme__c sc=new Scheme__c();
        sc.name='Construction Finance';
        sc.Scheme_Code__c='CF';
        sc.Scheme_Group_ID__c='6';
        sc.Product_Code__c='HL';
        insert sc;
        System.debug(sc.id);
        
        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Transaction_type__c='DAA';
        lap.Scheme__c=sc.id;
        lap.Requested_Amount__c = 50000000;
        lap.Approved_Loan_Amount__c=5000000;
        lap.Transaction_Type__c='PB';
        
        insert lap;
        
        
        
        List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
        
        
        Customer_Obligations__c objcust = new Customer_Obligations__c(Customer_Detail__c = lstofcon[0].id, EMI_Amount__c= 1);
        insert objcust;
        Bank_Detail__c objbank = new Bank_Detail__c(Customer_Details__c = lstofcon[0].id,Loan_Application__c = lap.id);
        insert objbank;
        CamSummaryReportExtension obj = new CamSummaryReportExtension(new ApexPages.StandardController(lap));
        obj.propertyCost = 1000000;
        obj.marketValue = 800000;
        obj.propAddress = 'Sample Text';
        obj.getCustomerCategory();
        obj.getGender();
        obj.getLoanPurpose();
        obj.getPropertyType();
        obj.getConstitution();
        
    }
    
    
    public static testmethod void method2(){
        Account acc=new Account();
        acc.name='TesstMWDFSADF';
        acc.phone='9688467902';
        acc.PAN__c='YCJDP7834G';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        Document_Master__c objdoc = new Document_Master__c(Name = 'Sanction Letter', Doc_Id__c = '123');
        insert objdoc;
        
        
        System.debug(acc.id);
        
        Scheme__c sc=new Scheme__c();
        sc.name='Construction Finance';
        sc.Scheme_Code__c='CF';
        sc.Scheme_Group_ID__c='6';
        sc.Product_Code__c='HL';
        insert sc;
        System.debug(sc.id);
        
        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Transaction_type__c='DAA';
        lap.Scheme__c=sc.id;
        lap.Requested_Amount__c = 50000000;
        lap.Approved_Loan_Amount__c=5000000;
        lap.Transaction_Type__c='PB';
        lap.Property_Identified__c = true;   
        insert lap;
        
        User dstu=[SELECT id,Name,ProfileId,LastName,Email,Username, CompanyName,
                   Title,Alias,TimeZoneSidKey,ManagerId,
                   EmailEncodingKey,LanguageLocaleKey,
                   LocaleSidKey,UserRoleId,Line_Of_Business__c 
                   from user where profile.Name='DST' and Line_Of_Business__c = 'Open Market' and isActive=true limit 1];
        //==================================Apex Sharing=========================//
        Loan_Application__Share shareRecord = new Loan_Application__Share();
        shareRecord.AccessLevel = 'Edit';
        shareRecord.ParentId = lap.Id;
        shareRecord.UserOrGroupId = dstu.id;
        insert shareRecord;
        //=================================END==================================//
        
        
        List<User> lstUser = [SELECT Id from User WHERE Profile.Name='Sales Team' AND UserRole.Name LIKE '%SM%' AND Manager_Inspector_ID__c != null];
        
        Id RecordTypeIdSourceDetail = Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('DST').getRecordTypeId();
        
        DSA_Master__c objDSAMaster = new DSA_Master__c(Name='A JAGAN MOHAN',Type__c='DSA', BrokerID__c = '1234');
        insert objDSAMaster;
        
        DST_Master__c objDSTMaster = new DST_Master__c(Name=dstu.name,Inspector_ID__c='1234');
        insert objDSTMaster;
       
        System.runAs(dstu) {
            
            Sourcing_Detail__c objSourceDetail = new Sourcing_Detail__c(Loan_Application__c=lap.Id,Sales_User_Name__c=dstu.ManagerId,
                                                                        RecordTypeId=RecordTypeIdSourceDetail,DST_Name__c=objDSTMaster.Id,DST__c=dstu.Id
                                                                       );
                                                                        // DSA_Name__c=objDSAMaster.Id;
            insert objSourceDetail;
        }    
        lap.StageName__c = 'Operation Control';
        update lap;
        
        List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83');
        insert objPincode;
        
        Property__c objProperty = new Property__c(Loan_Application__c=lap.Id,Pincode_LP__c=objPincode.Id,
                                                  First_Property_Owner__c=lstofcon[0].Id,Address_Line_1__c='Test Address Line 1',
                                                  Address_line_2__c='Test Address Line 2');
        insert objProperty;
        
        
        
        
        
        Customer_Obligations__c objcust = new Customer_Obligations__c(Customer_Detail__c = lstofcon[0].id, EMI_Amount__c= 1);
        insert objcust;
        Bank_Detail__c objbank = new Bank_Detail__c(Customer_Details__c = lstofcon[0].id,Loan_Application__c = lap.id);
        insert objbank;
        
        callCamSummaryExtension(lap.id);
        
    }
    
    public static testmethod void method3(){
        Account acc=new Account();
        acc.name='TesstMWDFSADF';
        acc.phone='9688467902';
        acc.PAN__c='YCJDP7834G';
        acc.Date_of_Incorporation__c=date.today();
        insert acc;
        
        Document_Master__c objdoc = new Document_Master__c(Name = 'Sanction Letter', Doc_Id__c = '123');
        insert objdoc;
        
        
        System.debug(acc.id);
        
        Scheme__c sc=new Scheme__c();
        sc.name='Construction Finance';
        sc.Scheme_Code__c='CF';
        sc.Scheme_Group_ID__c='6';
        sc.Product_Code__c='HL';
        insert sc;
        System.debug(sc.id);
        
        Loan_Application__c lap=new Loan_Application__c ();
        lap.Customer__c=acc.id;
        lap.Transaction_type__c='DAA';
        lap.Scheme__c=sc.id;
        lap.Requested_Amount__c = 50000000;
        lap.Approved_Loan_Amount__c=5000000;
        lap.Transaction_Type__c='PB';
        lap.Property_Identified__c = true;
        insert lap;
        
        User dstu=[SELECT id,Name,ProfileId,LastName,Email,Username, CompanyName,
                   Title,Alias,TimeZoneSidKey,ManagerId,
                   EmailEncodingKey,LanguageLocaleKey,
                   LocaleSidKey,UserRoleId,Line_Of_Business__c 
                   from user where profile.Name='DST' and Line_Of_Business__c = 'Open Market' and isActive=true limit 1];
       //==================================Apex Sharing=========================//
        Loan_Application__Share shareRecord = new Loan_Application__Share();
        shareRecord.AccessLevel = 'Edit';
        shareRecord.ParentId = lap.Id;
        shareRecord.UserOrGroupId = dstu.id;
        insert shareRecord;
        //=================================END==================================//
        List<User> lstUser = [SELECT Id from User WHERE Profile.Name='Sales Team' AND UserRole.Name LIKE '%SM%' AND Manager_Inspector_ID__c != null];
        
        Id RecordTypeIdSourceDetail = Schema.SObjectType.Sourcing_Detail__c.getRecordTypeInfosByName().get('DST').getRecordTypeId();
        
        DSA_Master__c objDSAMaster = new DSA_Master__c(Name='A JAGAN MOHAN',Type__c='DSA', BrokerID__c = '1234');
        insert objDSAMaster;
        
        DST_Master__c objDSTMaster = new DST_Master__c(Name=dstu.name,Inspector_ID__c='1234');
        insert objDSTMaster;
        
       
        dstu.ManagerId = lstUser[0].Id;
        update dstu;
        System.runAs(dstu) {
            Sourcing_Detail__c objSourceDetail = new Sourcing_Detail__c(Loan_Application__c=lap.Id,Sales_User_Name__c=dstu.ManagerId,
                                                                        RecordTypeId=RecordTypeIdSourceDetail,DST_Name__c=objDSTMaster.Id,DST__c=dstu.Id
                                                                       );
                                                                        // DSA_Name__c=objDSAMaster.Id;
            insert objSourceDetail;
            system.debug('objSourceDetail' + objSourceDetail);
        }
        lap.StageName__c = 'Operation Control';
        update lap;
        
        List<Loan_Contact__c> lstofcon=[select id from Loan_Contact__c where Loan_Applications__c=:lap.id];
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83');
        insert objPincode;
        
        Property__c objProperty = new Property__c(Loan_Application__c=lap.Id,Pincode_LP__c=objPincode.Id,
                                                  First_Property_Owner__c=lstofcon[0].Id,
                                                  Address_line_2__c='Test Address Line 2');
        insert objProperty;
        
        
        
        
        
        Customer_Obligations__c objcust = new Customer_Obligations__c(Customer_Detail__c = lstofcon[0].id, EMI_Amount__c= 1);
        insert objcust;
        Bank_Detail__c objbank = new Bank_Detail__c(Customer_Details__c = lstofcon[0].id,Loan_Application__c = lap.id);
        insert objbank;
        
        callCamSummaryExtension(lap.id);
        
    }
    
    @future
    public static void callCamSummaryExtension(Id LoanApplicationId) {
        Loan_Application__c objLA = [Select l.Type_of_Repayment__c,l.Comments__c,l.Recommendation_by_Credit__c,l.LTV_amount__c,l.LTV__c,l.Property_Identified__c, l.Remarks__c, l.Loan_Number__c , l.Sub_Stage__c, l.StageName__c,l.Processing_Fee_Percentage__c,l.Approved_Processing_Fee__c ,l.Requested_Processing_Fees1__c , l.Scheme__r.Name,l.Scheme__r.Product_Code__c, l.Scheme__c, l.Requested_Amount__c, l.Loan_Purpose__c, l.Loan_Application_Number__c, l.FOIR__c, l.CreatedDate, l.Branch__c, l.Branch_Lookup__r.Name, l.Branch_Lookup__c, l.Approved_ROI__c, l.Approved_Loan_Tenure__c, l.Approved_Loan_Amount__c, l.Interest_Type__c, l.Final_Property_Valuation__c,
                                     (Select Comments__c, Status__c, Sanction_Condition__c , Customer_Details__c,Createdby.Name,CreatedDate From Loan_Sanction_Conditions__r), (Select Name, Category__c, Customer__c,Customer__r.Name, Customer_segment__c, Date_Of_Birth__c, Declared_income__c, Designation__c, Father_s_Husband_s_Name__c, Gender__c, Gross_Salary__c, KYC__c, Marital_Status__c, Net_Income__c, Net_Salary__c, Appraised_Obligation__c, Pan_Number__c, PAN_First_Name__c, PAN_Last_Name__c,Age__c,Total_Work_Experience__c, Organization_Name__c,  Year_in_Present_Occupation__c, Constitution__c From Loan_Contacts__r), 
                                     (Select Id, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, Cheque_Date__c, Bank_Name__c, Branch__c, Loan_Applications__c, Agreement_ID__c, Cheque_ID__c, Cheque_Status__c, Bounce_Reason_ID__c, Bounce_Reason_Description__c, Failure_Date__c,Amount__c , Realization_Date__c, Remarks__c From IMD__r),
                                     (Select Name, Loan_Application__c, Type__c, Insurance_Company__r.Name,Premium__c,Sum_Assured__c From Cross_sells__r),
                                     (select Name,Property_Type__c,Type_of_Property__c,Address_Line_1__c,Address_line_2__c from Properties__r) ,
                                     (Select Name,Customer_Details__r.Customer__r.Name, Credit_Deviation_Master__c,Credit_Deviation_Master__r.Description__c, Credit_Deviation_Master__r.Name,Credit_Deviation_Master__r.Approver_Level__c , Loan_Application__c, Remarks__c, Mitigant__r.Name,If_Others_please_specify__c From Applicable_Deviations__r) 
                                     From Loan_Application__c l 
                                     where l.Id = :LoanApplicationId];
        CamSummaryReportExtension obj = new CamSummaryReportExtension(new ApexPages.StandardController(objLA));
        obj.propertyCost = 1000000;
        obj.marketValue = 800000;
        obj.propAddress = 'Sample Text';
        obj.getCustomerCategory();
        obj.getGender();
        obj.getLoanPurpose();
        obj.getPropertyType();
        obj.getConstitution();
    }
   
}