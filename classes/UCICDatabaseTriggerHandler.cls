public class UCICDatabaseTriggerHandler {
    
    public static void beforeUpdate(List<UCIC_Database__c> newList,Map<Id,UCIC_Database__c> oldMap){
        
        for(UCIC_Database__c ud: newList){
            if(ud.Recommender_s_Decision__c != oldMap.get(ud.Id).Recommender_s_Decision__c){
                ud.Recommender__c = UserInfo.getUserId();
                ud.Recommender_s_Decision_DateTime__c = DateTime.Now();
            }
            if(ud.Approver_s_Decision__c != oldMap.get(ud.Id).Approver_s_Decision__c){
                ud.Approver__c = UserInfo.getUserId();
                ud.Approver_s_Decision_DateTime__c = DateTime.Now();
            }
        }
    }
    
    public static void afterInsert(List<UCIC_Database__c> newList){
        UCICDatabaseTriggerHelper.updateLA(newList);
        // UCICDatabaseTriggerHelper.updateDeviations(newList);
    }
    
    public static void afterUpdate(List<UCIC_Database__c> newList,Map<Id,UCIC_Database__c> oldMap){
        // UCICDatabaseTriggerHelper.updateDeviations(newList);
        Set<UCIC_Database__c> lstUC = new Set<UCIC_Database__c>();
        List<UCIC_Database__c> lstUCDB = new List<UCIC_Database__c>();
        
        for(UCIC_Database__c ud: newList){
            if(ud.Recommender_s_Decision__c != oldMap.get(ud.Id).Recommender_s_Decision__c){
                if(ud.Recommender_s_Decision__c == null || ud.Recommender_s_Decision__c == ''){
                    lstUC.add(ud); 
                }
            }
            /*if(ud.Approver_s_Decision__c != oldMap.get(ud.Id).Approver_s_Decision__c){
                if(ud.Approver_s_Decision__c == null || ud.Approver_s_Decision__c == ''){
                    lstUC.add(ud); 
                }
            }*/
        }
        
        if(lstUC.size() > 0){
            for(UCIC_Database__c obj:lstUC){
                lstUCDB.add(obj);
            }
        }
        if(lstUCDB.size() > 0){
            UCICDatabaseTriggerHelper.updateLA(lstUCDB);
        }
    }
}