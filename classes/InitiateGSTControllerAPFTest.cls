@isTest
private class InitiateGSTControllerAPFTest {

	private static testMethod void test() {
        
	    List<RecordType> lstRecordType = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Individual'];
        Id builderIndvRecTypeId;
        if(!lstRecordType.isEmpty()) {
            builderIndvRecTypeId = lstRecordType[0].Id;
        }
        
        Account acc = new Account();
            acc.RecordTypeId = builderIndvRecTypeId;
            acc.Name = 'Individual Test Acc';
            acc.PAN__c = 'AAAAA3333B';
            acc.GSTIN__c = '27AAHFG0551H1ZL';
        insert acc;
        
        List<RecordType> lstRecordType2 = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Corporate'];
        Id builderCorpRecTypeId;
        if(!lstRecordType2.isEmpty()) {
            builderCorpRecTypeId = lstRecordType2[0].Id;
        }
        
        List<RecordType> lstIndividualPBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Individual'];
        List<RecordType> lstCorporatePBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Corporate'];
        
        Account acc2 = new Account();
            acc2.RecordTypeId = builderCorpRecTypeId;
            acc2.Name = 'Test Corporate Account';
            acc2.PAN__c = 'AAAAB2222B';
            
        insert acc2;
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83123');
        insert objPincode;
        
        Project__c objProject = new Project__c();
        //objProject.Assigned_Sales_User__c = assignedSalesUser != null ? assignedSalesUser.Id : null;
        objProject.Account__c = acc.Id;
        objProject.Project_Name__c = 'Test Project';
        objProject.APF_Status__c = 'APF Initiated';
        objProject.Stage__c = 'APF Initiation';
        objProject.Sub_Stage__c = 'Project Data Capture';
        objProject.APF_Type__c = 'New';
        objProject.Address_Line_1__c = 'Test Address';
        objProject.Pincode__c = objPincode.Id;
        insert objProject;
        
        Project_Builder__c objProjectBuilderCorp = new Project_Builder__c();
        objProjectBuilderCorp.RecordTypeId = lstCorporatePBId[0].Id;
        objProjectBuilderCorp.Builder_Name__c = acc2.Id;
        objProjectBuilderCorp.Constitution__c = '24';
        objProjectBuilderCorp.Builder_Status__c = 'Resident';
        objProjectBuilderCorp.Contact_Number_Mobile__c = '9999444223';
        objProjectBuilderCorp.PAN_Number__c = 'EGNPS1111T';
        objProjectBuilderCorp.Project__c = objProject.Id; 
        objProjectBuilderCorp.Preferred_Communication_Address__c = 'CURRES'; 
        //objProjectBuilderCorp. =
        Database.insert(objProjectBuilderCorp,false);
        
        acc2.GSTIN__c = '27AAHFG0551H1ZL';
        update acc2;
        
        insert objProjectBuilderCorp;
        System.debug('Debug Log for GSTIN'+objProjectBuilderCorp);
        
        
        objProjectBuilderCorp.GSTIN_Number__c = '27AAHFG0551H1ZL';
        //objProjectBuilderCorp.Builder_Role__c = Constants.strPBConstructorMarketer;
        update objProjectBuilderCorp;
        
        Set<Id> setProjectId = new Set<Id>();
        setProjectId.add(objProject.Id);
        
        
        
        List<Project_Builder__c> lstProjectBuilder = [SELECT Id, Gender__c, RecordTypeId, Builder_Category__c, GSTIN_Available__c, Name, Date_Of_Birth__c, Builder_Name__r.RecordType.DeveloperName, All_Promoter_Count__c, Builder_Type__c, Class_Of_Activity__c, Builder_Name__c, Builder_Name__r.RecordTypeId, Builder_Status__c, Constitution__c, Builder_Role__c, Contact_Number_Mobile__c, Email__c, Preferred_Communication_Address__c, Address_Line_1__c, Address_Line_2__c, Pincode__c, Promoter_PAN_Validation_Count__c, Promoter_Cibil_Validation_Count__c, Project__c,(Select Id, Name, Name_of_Promoter__c, Email__c, Contact_No_Mobile__c, Preferred_Communication_Address__c, Promoter_Status__c, Address_Line_1__c, Address_Line_2__c, Pincode__c from Promoters__r) From Project_Builder__c Where Project__c =: objProject.Id];
        System.debug('Debug Log for lstProjectBuilder size'+lstProjectBuilder.size());
        String builderIndividualRecordId;
        List<Promoter__c> lstPromoter = new List<Promoter__c>();
        if(!lstProjectBuilder.isEmpty()) {
            for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                objProjectBuilder.GSTIN_Available__c = true;
                objProjectBuilder.GSTIN_Number__c = '27AAHFG0551H1ZL';
                if(objProjectBuilder.Promoters__r.isEmpty() && objProjectBuilder.Builder_Name__r.RecordType.DeveloperName != 'Builder_Individual'){
                    Promoter__c objPromoter = new Promoter__c(Project_Builder__c = objProjectBuilder.Id, Preferred_Communication_Address__c = 'CURRES', PAN_Number__c = 'AAARR1123L', GSTIN_Number__c = '27AAHFG0551H1ZL', GSTIN_Available__c = true);
                    lstPromoter.add(objPromoter);
                }
                else if(objProjectBuilder.Builder_Name__r.RecordType.DeveloperName == 'Builder_Individual') {
                    builderIndividualRecordId = objProjectBuilder.Id;
                    objProjectBuilder.Gender__c = 'M';
                }
            }
            update lstProjectBuilder;
            System.debug('Debug Log for lstPromoter'+lstPromoter.size());
            if(!lstPromoter.isEmpty()) {
                insert lstPromoter;
            }
            System.debug('Debug Log for lstPromoter post insertion from Address Check Method'+lstPromoter);
            update lstPromoter;
            
        }
        
        InitiateGSTControllerAPF.getCriteriaList(objProject.Id);
        String recordDetailsJson = '{"strBuilderName":"Test","strGSTIN":"'+lstPromoter[0].GSTIN_Number__c+'","strEmail":"test@xyz.com","strMobileNumber":"9999933333","strPanNumber":"'+lstPromoter[0].PAN_Number__c+'", "strProjectId":"'+objProject.Id+'"}';
        String paramjson='[{"strObjectType":"Promoter__c","isChecked": true, "strConstitution" : "20", "strRecordId" : "'+lstPromoter[0].id+'", "obj_RecordDetails" : '+recordDetailsJson+'}]';
        
        System.debug('Debug Log for paramjson'+paramjson);
        
        InitiateGSTControllerAPF.getErrorMessage(paramjson);
        
        String paramjson1 ='[{"strObjectType":"Project_Builder__c","isChecked": true, "strConstitution" : "20", "strRecordId" : "'+builderIndividualRecordId+'", "obj_RecordDetails" :'+recordDetailsJson+'}]';
        System.debug('Debug Log for paramjson1'+paramjson1);
        InitiateGSTControllerAPF.getErrorMessage(paramjson1);
        
        
        
            
        GstinAuthDetailsResponseBody.cls_mbr obj_mbr = new GstinAuthDetailsResponseBody.cls_mbr();
        List<GstinAuthDetailsResponseBody.cls_mbr> lst_mbr = new List<GstinAuthDetailsResponseBody.cls_mbr>();
        lst_mbr.add(obj_mbr);
        
        GstinAuthDetailsResponseBody.cls_pradr obj_pradr = new GstinAuthDetailsResponseBody.cls_pradr();
            obj_pradr.em = '';	//
    		obj_pradr.adr = '';	//
    		obj_pradr.addr = 'NA';	//NA
    		obj_pradr.mb = '';	//
    		obj_pradr.ntr = 'Service Provision';	//Service Provision
    		obj_pradr.lastUpdatedDate = '';	//NA
    		
		
    		
		GstinAuthDetailsResponseBody.cls_contacted obj_contacted = new GstinAuthDetailsResponseBody.cls_contacted();
		
		GstinAuthDetailsResponseBody.cls_adadr obj_adadr = new GstinAuthDetailsResponseBody.cls_adadr();
		List<GstinAuthDetailsResponseBody.cls_adadr> lst_adadr = new List<GstinAuthDetailsResponseBody.cls_adadr>();
		lst_adadr.add(obj_adadr);
        
        GstinAuthDetailsResponseBody.cls_result obj_Result = new GstinAuthDetailsResponseBody.cls_result();
            obj_Result.mbr = lst_mbr;
            obj_Result.canFlag = '';
            obj_Result.pradr = obj_pradr;
            obj_Result.tradeNam = '';
            obj_Result.lstupdt = '';
            obj_Result.contacted = obj_contacted;
            obj_Result.rgdt = '01/07/2017';	//01/07/2017
    		obj_Result.stjCd = 'WB093';	//WB093
    		obj_Result.stj = 'TALTALA';	//TALTALA
    		obj_Result.ctjCd = 'WA0606';	//WA0606
    		obj_Result.ppr = '';	//NA
    		obj_Result.dty = 'Regular';	//Regular
    		obj_Result.cmpRt = 'NA';	//NA
    		obj_Result.cxdt = '';	//
    		obj_Result.ctb = 'Public Sector Undertaking';	//Public Sector Undertaking
    		obj_Result.sts = 'Active';	//Active
    		obj_Result.gstin = '19AAFCS9264A1Z8';	//19AAFCS9264A1Z8
    		obj_Result.lgnm = 'SOFTAGE INFORMATION TECHNOLOGY LIMITED';	//SOFTAGE INFORMATION TECHNOLOGY LIMITED
    		List<String> lstString = new List<String>();
    		lstString.add('a');
    		lstString.add('b');
    		lstString.add('c');
    		obj_Result.nba = lstString; 
    		obj_Result.ctj = '';
    		obj_Result.adadr = lst_adadr;
        
        
        
        
        
        GstinAuthDetailsResponseBody.cls_error obj_error = new GstinAuthDetailsResponseBody.cls_error();
            obj_error.errorCode = '';
            obj_error.errorType = '';
            obj_error.errorMessage = '';
        
        GstinAuthDetailsResponseBody objResponseBodyGST = new GstinAuthDetailsResponseBody();
        objResponseBodyGST.requestId = '';
        objResponseBodyGST.statusCode = 101;
        objResponseBodyGST.status = '200';
        objResponseBodyGST.error = obj_error;
        objResponseBodyGST.result = obj_Result;
        
        Test.startTest();
            TestMockRequest req1=new TestMockRequest(200,'OK',JSON.serialize(objResponseBodyGST),null);
            Test.setMock(HttpCalloutMock.class, req1);
            InitiateGSTControllerAPF.makeGSTINAuthCallout(paramjson);
            
        Test.stopTest();    
            
            
        
	}
	
	
	private static testMethod void test2() {
	    
        
	    List<RecordType> lstRecordType = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Individual'];
        Id builderIndvRecTypeId;
        if(!lstRecordType.isEmpty()) {
            builderIndvRecTypeId = lstRecordType[0].Id;
        }
        
        Account acc = new Account();
            acc.RecordTypeId = builderIndvRecTypeId;
            acc.Name = 'Individual Test Acc';
            acc.PAN__c = 'AAAAA3333B';
            acc.GSTIN__c = '27AAHFG0551H1ZL';
        insert acc;
        
        List<RecordType> lstRecordType2 = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Corporate'];
        Id builderCorpRecTypeId;
        if(!lstRecordType2.isEmpty()) {
            builderCorpRecTypeId = lstRecordType2[0].Id;
        }
        
        List<RecordType> lstIndividualPBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Individual'];
        List<RecordType> lstCorporatePBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Corporate'];
        
        Account acc2 = new Account();
            acc2.RecordTypeId = builderCorpRecTypeId;
            acc2.Name = 'Test Corporate Account';
            acc2.PAN__c = 'AAAAB2222B';
            
        insert acc2;
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83123');
        insert objPincode;
        
        Project__c objProject = new Project__c();
        //objProject.Assigned_Sales_User__c = assignedSalesUser != null ? assignedSalesUser.Id : null;
        objProject.Account__c = acc.Id;
        objProject.Project_Name__c = 'Test Project';
        objProject.APF_Status__c = 'APF Initiated';
        objProject.Stage__c = 'APF Initiation';
        objProject.Sub_Stage__c = 'Project Data Capture';
        objProject.APF_Type__c = 'New';
        objProject.Address_Line_1__c = 'Test Address';
        objProject.Pincode__c = objPincode.Id;
        insert objProject;
        
        Project_Builder__c objProjectBuilderCorp = new Project_Builder__c();
        objProjectBuilderCorp.RecordTypeId = lstCorporatePBId[0].Id;
        objProjectBuilderCorp.Builder_Name__c = acc2.Id;
        objProjectBuilderCorp.Constitution__c = '24';
        objProjectBuilderCorp.Builder_Status__c = 'Resident';
        objProjectBuilderCorp.Contact_Number_Mobile__c = '9999444223';
        objProjectBuilderCorp.PAN_Number__c = 'EGNPS1111T';
        objProjectBuilderCorp.Project__c = objProject.Id; 
        objProjectBuilderCorp.Preferred_Communication_Address__c = 'CURRES'; 
        //objProjectBuilderCorp. =
        Database.insert(objProjectBuilderCorp,false);
        
        acc2.GSTIN__c = '27AAHFG0551H1ZL';
        update acc2;
        
        insert objProjectBuilderCorp;
        System.debug('Debug Log for GSTIN'+objProjectBuilderCorp);
        
        
        objProjectBuilderCorp.GSTIN_Number__c = '27AAHFG0551H1ZL';
        //objProjectBuilderCorp.Builder_Role__c = Constants.strPBConstructorMarketer;
        update objProjectBuilderCorp;
        
        Set<Id> setProjectId = new Set<Id>();
        setProjectId.add(objProject.Id);
        
        
        
        List<Project_Builder__c> lstProjectBuilder = [SELECT Id, Gender__c, RecordTypeId, Builder_Category__c, GSTIN_Available__c, Name, Date_Of_Birth__c, Builder_Name__r.RecordType.DeveloperName, All_Promoter_Count__c, Builder_Type__c, Class_Of_Activity__c, Builder_Name__c, Builder_Name__r.RecordTypeId, Builder_Status__c, Constitution__c, Builder_Role__c, Contact_Number_Mobile__c, Email__c, Preferred_Communication_Address__c, Address_Line_1__c, Address_Line_2__c, Pincode__c, Promoter_PAN_Validation_Count__c, Promoter_Cibil_Validation_Count__c, Project__c,(Select Id, Name, Name_of_Promoter__c, Email__c, Contact_No_Mobile__c, Preferred_Communication_Address__c, Promoter_Status__c, Address_Line_1__c, Address_Line_2__c, Pincode__c from Promoters__r) From Project_Builder__c Where Project__c =: objProject.Id];
        System.debug('Debug Log for lstProjectBuilder size'+lstProjectBuilder.size());
        String builderIndividualRecordId;
        List<Promoter__c> lstPromoter = new List<Promoter__c>();
        if(!lstProjectBuilder.isEmpty()) {
            for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                objProjectBuilder.GSTIN_Available__c = true;
                objProjectBuilder.GSTIN_Number__c = '27AAHFG0551H1ZL';
                if(objProjectBuilder.Promoters__r.isEmpty() && objProjectBuilder.Builder_Name__r.RecordType.DeveloperName != 'Builder_Individual'){
                    Promoter__c objPromoter = new Promoter__c(Project_Builder__c = objProjectBuilder.Id, Preferred_Communication_Address__c = 'CURRES', PAN_Number__c = 'AAARR1123L', GSTIN_Number__c = '27AAHFG0551H1ZL', GSTIN_Available__c = true);
                    lstPromoter.add(objPromoter);
                }
                else if(objProjectBuilder.Builder_Name__r.RecordType.DeveloperName == 'Builder_Individual') {
                    builderIndividualRecordId = objProjectBuilder.Id;
                    objProjectBuilder.Gender__c = 'M';
                }
            }
            update lstProjectBuilder;
            System.debug('Debug Log for lstPromoter'+lstPromoter.size());
            if(!lstPromoter.isEmpty()) {
                insert lstPromoter;
            }
            System.debug('Debug Log for lstPromoter post insertion from Address Check Method'+lstPromoter);
            update lstPromoter;
            
        }
        
        InitiateGSTControllerAPF.getCriteriaList(objProject.Id);
        String recordDetailsJson = '{"strBuilderName":"Test","strGSTIN":"'+lstPromoter[0].GSTIN_Number__c+'","strEmail":"test@xyz.com","strMobileNumber":"9999933333","strPanNumber":"'+lstPromoter[0].PAN_Number__c+'", "strProjectId":"'+objProject.Id+'"}';
        String paramjson='[{"strObjectType":"Promoter__c","isChecked": true, "strConstitution" : "20", "strRecordId" : "'+lstPromoter[0].id+'", "obj_RecordDetails" : '+recordDetailsJson+'}]';
        
        System.debug('Debug Log for paramjson'+paramjson);
        
        InitiateGSTControllerAPF.getErrorMessage(paramjson);
        
        String paramjson1 ='[{"strObjectType":"Project_Builder__c","isChecked": true, "strConstitution" : "20", "strRecordId" : "'+builderIndividualRecordId+'", "obj_RecordDetails" :'+recordDetailsJson+'}]';
        System.debug('Debug Log for paramjson1'+paramjson1);
        InitiateGSTControllerAPF.getErrorMessage(paramjson1);
        
        
        
            
        GstinAuthDetailsResponseBody.cls_mbr obj_mbr = new GstinAuthDetailsResponseBody.cls_mbr();
        List<GstinAuthDetailsResponseBody.cls_mbr> lst_mbr = new List<GstinAuthDetailsResponseBody.cls_mbr>();
        lst_mbr.add(obj_mbr);
        
        GstinAuthDetailsResponseBody.cls_pradr obj_pradr = new GstinAuthDetailsResponseBody.cls_pradr();
            obj_pradr.em = '';	//
    		obj_pradr.adr = '';	//
    		obj_pradr.addr = 'NA';	//NA
    		obj_pradr.mb = '';	//
    		obj_pradr.ntr = 'Service Provision';	//Service Provision
    		obj_pradr.lastUpdatedDate = '';	//NA
    		
		
    		
		GstinAuthDetailsResponseBody.cls_contacted obj_contacted = new GstinAuthDetailsResponseBody.cls_contacted();
		
		GstinAuthDetailsResponseBody.cls_adadr obj_adadr = new GstinAuthDetailsResponseBody.cls_adadr();
		List<GstinAuthDetailsResponseBody.cls_adadr> lst_adadr = new List<GstinAuthDetailsResponseBody.cls_adadr>();
		lst_adadr.add(obj_adadr);
        
        GstinAuthDetailsResponseBody.cls_result obj_Result = new GstinAuthDetailsResponseBody.cls_result();
            obj_Result.mbr = lst_mbr;
            obj_Result.canFlag = '';
            obj_Result.pradr = obj_pradr;
            obj_Result.tradeNam = 'Test';
            obj_Result.lstupdt = '';
            obj_Result.contacted = obj_contacted;
            obj_Result.rgdt = '01/07/2017';	//01/07/2017
    		obj_Result.stjCd = 'WB093';	//WB093
    		obj_Result.stj = 'TALTALA';	//TALTALA
    		obj_Result.ctjCd = 'WA0606';	//WA0606
    		obj_Result.ppr = '';	//NA
    		obj_Result.dty = 'Regular';	//Regular
    		obj_Result.cmpRt = 'NA';	//NA
    		obj_Result.cxdt = '';	//
    		obj_Result.ctb = 'Public Sector Undertaking';	//Public Sector Undertaking
    		obj_Result.sts = 'Active';	//Active
    		obj_Result.gstin = '19AAFCS9264A1Z8';	//19AAFCS9264A1Z8
    		obj_Result.lgnm = 'SOFTAGE INFORMATION TECHNOLOGY LIMITED';	//SOFTAGE INFORMATION TECHNOLOGY LIMITED
    		List<String> lstString = new List<String>();
    		lstString.add('a');
    		lstString.add('b');
    		lstString.add('c');
    		obj_Result.nba = lstString; 
    		obj_Result.ctj = '';
    		obj_Result.adadr = lst_adadr;
        
        
        
        
        
        GstinAuthDetailsResponseBody.cls_error obj_error = new GstinAuthDetailsResponseBody.cls_error();
            obj_error.errorCode = '';
            obj_error.errorType = '';
            obj_error.errorMessage = '';
        
        GstinAuthDetailsResponseBody objResponseBodyGST = new GstinAuthDetailsResponseBody();
        objResponseBodyGST.requestId = '';
        objResponseBodyGST.statusCode = 101;
        objResponseBodyGST.status = '200';
        objResponseBodyGST.error = obj_error;
        objResponseBodyGST.result = obj_Result;
        
        Test.startTest();
            TestMockRequest req1=new TestMockRequest(200,'OK',JSON.serialize(objResponseBodyGST),null);
            Test.setMock(HttpCalloutMock.class, req1);
            InitiateGSTControllerAPF.makeGSTINAuthCallout(paramjson1);
            
        Test.stopTest();    
            
            
        
	
	}
	
	private static testMethod void test3() {
        
	    List<RecordType> lstRecordType = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Individual'];
        Id builderIndvRecTypeId;
        if(!lstRecordType.isEmpty()) {
            builderIndvRecTypeId = lstRecordType[0].Id;
        }
        
        Account acc = new Account();
            acc.RecordTypeId = builderIndvRecTypeId;
            acc.Name = 'Individual Test Acc';
            acc.PAN__c = 'AAAAA3333B';
            acc.GSTIN__c = '27AAHFG0551H1ZL';
        insert acc;
        
        List<RecordType> lstRecordType2 = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Corporate'];
        Id builderCorpRecTypeId;
        if(!lstRecordType2.isEmpty()) {
            builderCorpRecTypeId = lstRecordType2[0].Id;
        }
        
        List<RecordType> lstIndividualPBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Individual'];
        List<RecordType> lstCorporatePBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Corporate'];
        
        Account acc2 = new Account();
            acc2.RecordTypeId = builderCorpRecTypeId;
            acc2.Name = 'Test Corporate Account';
            acc2.PAN__c = 'AAAAB2222B';
            
        insert acc2;
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83123');
        insert objPincode;
        
        Project__c objProject = new Project__c();
        //objProject.Assigned_Sales_User__c = assignedSalesUser != null ? assignedSalesUser.Id : null;
        objProject.Account__c = acc.Id;
        objProject.Project_Name__c = 'Test Project';
        objProject.APF_Status__c = 'APF Initiated';
        objProject.Stage__c = 'APF Initiation';
        objProject.Sub_Stage__c = 'Project Data Capture';
        objProject.APF_Type__c = 'New';
        objProject.Address_Line_1__c = 'Test Address';
        objProject.Pincode__c = objPincode.Id;
        insert objProject;
        
        Project_Builder__c objProjectBuilderCorp = new Project_Builder__c();
        objProjectBuilderCorp.RecordTypeId = lstCorporatePBId[0].Id;
        objProjectBuilderCorp.Builder_Name__c = acc2.Id;
        objProjectBuilderCorp.Constitution__c = '24';
        objProjectBuilderCorp.Builder_Status__c = 'Resident';
        objProjectBuilderCorp.Contact_Number_Mobile__c = '9999444223';
        objProjectBuilderCorp.PAN_Number__c = 'EGNPS1111T';
        objProjectBuilderCorp.Project__c = objProject.Id; 
        objProjectBuilderCorp.Preferred_Communication_Address__c = 'CURRES'; 
        //objProjectBuilderCorp. =
        Database.insert(objProjectBuilderCorp,false);
        
        acc2.GSTIN__c = '27AAHFG0551H1ZL';
        update acc2;
        
        insert objProjectBuilderCorp;
        System.debug('Debug Log for GSTIN'+objProjectBuilderCorp);
        
        
        objProjectBuilderCorp.GSTIN_Number__c = '27AAHFG0551H1ZL';
        //objProjectBuilderCorp.Builder_Role__c = Constants.strPBConstructorMarketer;
        update objProjectBuilderCorp;
        
        Set<Id> setProjectId = new Set<Id>();
        setProjectId.add(objProject.Id);
        
        
        
        List<Project_Builder__c> lstProjectBuilder = [SELECT Id, Gender__c, RecordTypeId, Builder_Category__c, GSTIN_Available__c, Name, Date_Of_Birth__c, Builder_Name__r.RecordType.DeveloperName, All_Promoter_Count__c, Builder_Type__c, Class_Of_Activity__c, Builder_Name__c, Builder_Name__r.RecordTypeId, Builder_Status__c, Constitution__c, Builder_Role__c, Contact_Number_Mobile__c, Email__c, Preferred_Communication_Address__c, Address_Line_1__c, Address_Line_2__c, Pincode__c, Promoter_PAN_Validation_Count__c, Promoter_Cibil_Validation_Count__c, Project__c,(Select Id, Name, Name_of_Promoter__c, Email__c, Contact_No_Mobile__c, Preferred_Communication_Address__c, Promoter_Status__c, Address_Line_1__c, Address_Line_2__c, Pincode__c from Promoters__r) From Project_Builder__c Where Project__c =: objProject.Id];
        System.debug('Debug Log for lstProjectBuilder size'+lstProjectBuilder.size());
        String builderIndividualRecordId;
        List<Promoter__c> lstPromoter = new List<Promoter__c>();
        if(!lstProjectBuilder.isEmpty()) {
            for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                objProjectBuilder.GSTIN_Available__c = true;
                objProjectBuilder.GSTIN_Number__c = '27AAHFG0551H1ZL';
                if(objProjectBuilder.Promoters__r.isEmpty() && objProjectBuilder.Builder_Name__r.RecordType.DeveloperName != 'Builder_Individual'){
                    Promoter__c objPromoter = new Promoter__c(Project_Builder__c = objProjectBuilder.Id, Preferred_Communication_Address__c = 'CURRES', PAN_Number__c = 'AAARR1123L', GSTIN_Number__c = '27AAHFG0551H1ZL', GSTIN_Available__c = true);
                    lstPromoter.add(objPromoter);
                }
                else if(objProjectBuilder.Builder_Name__r.RecordType.DeveloperName == 'Builder_Individual') {
                    builderIndividualRecordId = objProjectBuilder.Id;
                    objProjectBuilder.Gender__c = 'M';
                }
            }
            update lstProjectBuilder;
            System.debug('Debug Log for lstPromoter'+lstPromoter.size());
            if(!lstPromoter.isEmpty()) {
                insert lstPromoter;
            }
            System.debug('Debug Log for lstPromoter post insertion from Address Check Method'+lstPromoter);
            update lstPromoter;
            
        }
        
        InitiateGSTControllerAPF.getCriteriaList(objProject.Id);
        String recordDetailsJson = '{"strBuilderName":"Test","strGSTIN":"'+lstPromoter[0].GSTIN_Number__c+'","strEmail":"test@xyz.com","strMobileNumber":"9999933333","strPanNumber":"'+lstPromoter[0].PAN_Number__c+'", "strProjectId":"'+objProject.Id+'"}';
        String paramjson='[{"strObjectType":"Promoter__c","isChecked": true, "strConstitution" : "20", "strRecordId" : "'+lstPromoter[0].id+'", "obj_RecordDetails" : '+recordDetailsJson+'}]';
        
        System.debug('Debug Log for paramjson'+paramjson);
        
        InitiateGSTControllerAPF.getErrorMessage(paramjson);
        
        String paramjson1 ='[{"strObjectType":"Project_Builder__c","isChecked": true, "strConstitution" : "20", "strRecordId" : "'+builderIndividualRecordId+'", "obj_RecordDetails" :'+recordDetailsJson+'}]';
        System.debug('Debug Log for paramjson1'+paramjson1);
        InitiateGSTControllerAPF.getErrorMessage(paramjson1);
        
        
        
            
        GstinAuthDetailsResponseBody.cls_mbr obj_mbr = new GstinAuthDetailsResponseBody.cls_mbr();
        List<GstinAuthDetailsResponseBody.cls_mbr> lst_mbr = new List<GstinAuthDetailsResponseBody.cls_mbr>();
        lst_mbr.add(obj_mbr);
        
        GstinAuthDetailsResponseBody.cls_pradr obj_pradr = new GstinAuthDetailsResponseBody.cls_pradr();
            obj_pradr.em = '';	//
    		obj_pradr.adr = '';	//
    		obj_pradr.addr = 'NA';	//NA
    		obj_pradr.mb = '';	//
    		obj_pradr.ntr = 'Service Provision';	//Service Provision
    		obj_pradr.lastUpdatedDate = '';	//NA
    		
		
    		
		GstinAuthDetailsResponseBody.cls_contacted obj_contacted = new GstinAuthDetailsResponseBody.cls_contacted();
		
		GstinAuthDetailsResponseBody.cls_adadr obj_adadr = new GstinAuthDetailsResponseBody.cls_adadr();
		List<GstinAuthDetailsResponseBody.cls_adadr> lst_adadr = new List<GstinAuthDetailsResponseBody.cls_adadr>();
		lst_adadr.add(obj_adadr);
        
        GstinAuthDetailsResponseBody.cls_result obj_Result = new GstinAuthDetailsResponseBody.cls_result();
            obj_Result.mbr = lst_mbr;
            obj_Result.canFlag = '';
            obj_Result.pradr = obj_pradr;
            obj_Result.tradeNam = '';
            obj_Result.lstupdt = '';
            obj_Result.contacted = obj_contacted;
            obj_Result.rgdt = '01/07/2017';	//01/07/2017
    		obj_Result.stjCd = 'WB093';	//WB093
    		obj_Result.stj = 'TALTALA';	//TALTALA
    		obj_Result.ctjCd = 'WA0606';	//WA0606
    		obj_Result.ppr = '';	//NA
    		obj_Result.dty = 'Regular';	//Regular
    		obj_Result.cmpRt = 'NA';	//NA
    		obj_Result.cxdt = '';	//
    		obj_Result.ctb = 'Public Sector Undertaking';	//Public Sector Undertaking
    		obj_Result.sts = 'Active';	//Active
    		obj_Result.gstin = '19AAFCS9264A1Z8';	//19AAFCS9264A1Z8
    		obj_Result.lgnm = 'SOFTAGE INFORMATION TECHNOLOGY LIMITED';	//SOFTAGE INFORMATION TECHNOLOGY LIMITED
    		List<String> lstString = new List<String>();
    		lstString.add('a');
    		lstString.add('b');
    		lstString.add('c');
    		obj_Result.nba = lstString; 
    		obj_Result.ctj = '';
    		obj_Result.adadr = lst_adadr;
        
        
        
        
        
        GstinAuthDetailsResponseBody.cls_error obj_error = new GstinAuthDetailsResponseBody.cls_error();
            obj_error.errorCode = '';
            obj_error.errorType = '';
            obj_error.errorMessage = '';
        
        GstinAuthDetailsResponseBody objResponseBodyGST = new GstinAuthDetailsResponseBody();
        objResponseBodyGST.requestId = '';
        objResponseBodyGST.statusCode = 102;
        objResponseBodyGST.status = '200';
        objResponseBodyGST.error = obj_error;
        objResponseBodyGST.result = obj_Result;
        
        Test.startTest();
            TestMockRequest req1=new TestMockRequest(200,'OK',JSON.serialize(objResponseBodyGST),null);
            Test.setMock(HttpCalloutMock.class, req1);
            InitiateGSTControllerAPF.makeGSTINAuthCallout(paramjson);
            
        Test.stopTest();    
            
            
        
	}
	
	private static testMethod void test4() {
	    
        
	    List<RecordType> lstRecordType = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Individual'];
        Id builderIndvRecTypeId;
        if(!lstRecordType.isEmpty()) {
            builderIndvRecTypeId = lstRecordType[0].Id;
        }
        
        Account acc = new Account();
            acc.RecordTypeId = builderIndvRecTypeId;
            acc.Name = 'Individual Test Acc';
            acc.PAN__c = 'AAAAA3333B';
            acc.GSTIN__c = '27AAHFG0551H1ZL';
        insert acc;
        
        List<RecordType> lstRecordType2 = [Select Id, Name from RecordType Where sObjectType = 'Account' AND DeveloperName = 'Builder_Corporate'];
        Id builderCorpRecTypeId;
        if(!lstRecordType2.isEmpty()) {
            builderCorpRecTypeId = lstRecordType2[0].Id;
        }
        
        List<RecordType> lstIndividualPBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Individual'];
        List<RecordType> lstCorporatePBId = [Select Id, Name from RecordType Where sObjectType = 'Project_Builder__c' AND DeveloperName = 'Builder_Corporate'];
        
        Account acc2 = new Account();
            acc2.RecordTypeId = builderCorpRecTypeId;
            acc2.Name = 'Test Corporate Account';
            acc2.PAN__c = 'AAAAB2222B';
            
        insert acc2;
        
        Pincode__c objPincode = new Pincode__c(Name='110001',City__c='NEW DELHI',ZIP_ID__c='83123');
        insert objPincode;
        
        Project__c objProject = new Project__c();
        //objProject.Assigned_Sales_User__c = assignedSalesUser != null ? assignedSalesUser.Id : null;
        objProject.Account__c = acc.Id;
        objProject.Project_Name__c = 'Test Project';
        objProject.APF_Status__c = 'APF Initiated';
        objProject.Stage__c = 'APF Initiation';
        objProject.Sub_Stage__c = 'Project Data Capture';
        objProject.APF_Type__c = 'New';
        objProject.Address_Line_1__c = 'Test Address';
        objProject.Pincode__c = objPincode.Id;
        insert objProject;
        
        Project_Builder__c objProjectBuilderCorp = new Project_Builder__c();
        objProjectBuilderCorp.RecordTypeId = lstCorporatePBId[0].Id;
        objProjectBuilderCorp.Builder_Name__c = acc2.Id;
        objProjectBuilderCorp.Constitution__c = '24';
        objProjectBuilderCorp.Builder_Status__c = 'Resident';
        objProjectBuilderCorp.Contact_Number_Mobile__c = '9999444223';
        objProjectBuilderCorp.PAN_Number__c = 'EGNPS1111T';
        objProjectBuilderCorp.Project__c = objProject.Id; 
        objProjectBuilderCorp.Preferred_Communication_Address__c = 'CURRES'; 
        //objProjectBuilderCorp. =
        Database.insert(objProjectBuilderCorp,false);
        
        acc2.GSTIN__c = '27AAHFG0551H1ZL';
        update acc2;
        
        insert objProjectBuilderCorp;
        System.debug('Debug Log for GSTIN'+objProjectBuilderCorp);
        
        
        objProjectBuilderCorp.GSTIN_Number__c = '27AAHFG0551H1ZL';
        //objProjectBuilderCorp.Builder_Role__c = Constants.strPBConstructorMarketer;
        update objProjectBuilderCorp;
        
        Set<Id> setProjectId = new Set<Id>();
        setProjectId.add(objProject.Id);
        
        
        
        List<Project_Builder__c> lstProjectBuilder = [SELECT Id, Gender__c, RecordTypeId, Builder_Category__c, GSTIN_Available__c, Name, Date_Of_Birth__c, Builder_Name__r.RecordType.DeveloperName, All_Promoter_Count__c, Builder_Type__c, Class_Of_Activity__c, Builder_Name__c, Builder_Name__r.RecordTypeId, Builder_Status__c, Constitution__c, Builder_Role__c, Contact_Number_Mobile__c, Email__c, Preferred_Communication_Address__c, Address_Line_1__c, Address_Line_2__c, Pincode__c, Promoter_PAN_Validation_Count__c, Promoter_Cibil_Validation_Count__c, Project__c,(Select Id, Name, Name_of_Promoter__c, Email__c, Contact_No_Mobile__c, Preferred_Communication_Address__c, Promoter_Status__c, Address_Line_1__c, Address_Line_2__c, Pincode__c from Promoters__r) From Project_Builder__c Where Project__c =: objProject.Id];
        System.debug('Debug Log for lstProjectBuilder size'+lstProjectBuilder.size());
        String builderIndividualRecordId;
        List<Promoter__c> lstPromoter = new List<Promoter__c>();
        if(!lstProjectBuilder.isEmpty()) {
            for(Project_Builder__c objProjectBuilder : lstProjectBuilder) {
                objProjectBuilder.GSTIN_Available__c = true;
                objProjectBuilder.GSTIN_Number__c = '27AAHFG0551H1ZL';
                if(objProjectBuilder.Promoters__r.isEmpty() && objProjectBuilder.Builder_Name__r.RecordType.DeveloperName != 'Builder_Individual'){
                    Promoter__c objPromoter = new Promoter__c(Project_Builder__c = objProjectBuilder.Id, Preferred_Communication_Address__c = 'CURRES', PAN_Number__c = 'AAARR1123L', GSTIN_Number__c = '27AAHFG0551H1ZL', GSTIN_Available__c = true);
                    lstPromoter.add(objPromoter);
                }
                else if(objProjectBuilder.Builder_Name__r.RecordType.DeveloperName == 'Builder_Individual') {
                    builderIndividualRecordId = objProjectBuilder.Id;
                    objProjectBuilder.Gender__c = 'M';
                }
            }
            update lstProjectBuilder;
            System.debug('Debug Log for lstPromoter'+lstPromoter.size());
            if(!lstPromoter.isEmpty()) {
                insert lstPromoter;
            }
            System.debug('Debug Log for lstPromoter post insertion from Address Check Method'+lstPromoter);
            update lstPromoter;
            
        }
        
        InitiateGSTControllerAPF.getCriteriaList(objProject.Id);
        String recordDetailsJson = '{"strBuilderName":"Test","strGSTIN":"'+lstPromoter[0].GSTIN_Number__c+'","strEmail":"test@xyz.com","strMobileNumber":"9999933333","strPanNumber":"'+lstPromoter[0].PAN_Number__c+'", "strProjectId":"'+objProject.Id+'"}';
        String paramjson='[{"strObjectType":"Promoter__c","isChecked": true, "strConstitution" : "20", "strRecordId" : "'+lstPromoter[0].id+'", "obj_RecordDetails" : '+recordDetailsJson+'}]';
        
        System.debug('Debug Log for paramjson'+paramjson);
        
        InitiateGSTControllerAPF.getErrorMessage(paramjson);
        
        String paramjson1 ='[{"strObjectType":"Project_Builder__c","isChecked": true, "strConstitution" : "20", "strRecordId" : "'+builderIndividualRecordId+'", "obj_RecordDetails" :'+recordDetailsJson+'}]';
        System.debug('Debug Log for paramjson1'+paramjson1);
        InitiateGSTControllerAPF.getErrorMessage(paramjson1);
        
        
        
            
        GstinAuthDetailsResponseBody.cls_mbr obj_mbr = new GstinAuthDetailsResponseBody.cls_mbr();
        List<GstinAuthDetailsResponseBody.cls_mbr> lst_mbr = new List<GstinAuthDetailsResponseBody.cls_mbr>();
        lst_mbr.add(obj_mbr);
        
        GstinAuthDetailsResponseBody.cls_pradr obj_pradr = new GstinAuthDetailsResponseBody.cls_pradr();
            obj_pradr.em = '';	//
    		obj_pradr.adr = '';	//
    		obj_pradr.addr = 'NA';	//NA
    		obj_pradr.mb = '';	//
    		obj_pradr.ntr = 'Service Provision';	//Service Provision
    		obj_pradr.lastUpdatedDate = '';	//NA
    		
		
    		
		GstinAuthDetailsResponseBody.cls_contacted obj_contacted = new GstinAuthDetailsResponseBody.cls_contacted();
		
		GstinAuthDetailsResponseBody.cls_adadr obj_adadr = new GstinAuthDetailsResponseBody.cls_adadr();
		List<GstinAuthDetailsResponseBody.cls_adadr> lst_adadr = new List<GstinAuthDetailsResponseBody.cls_adadr>();
		lst_adadr.add(obj_adadr);
        
        GstinAuthDetailsResponseBody.cls_result obj_Result = new GstinAuthDetailsResponseBody.cls_result();
            obj_Result.mbr = lst_mbr;
            obj_Result.canFlag = '';
            obj_Result.pradr = obj_pradr;
            obj_Result.tradeNam = 'Test';
            obj_Result.lstupdt = '';
            obj_Result.contacted = obj_contacted;
            obj_Result.rgdt = '01/07/2017';	//01/07/2017
    		obj_Result.stjCd = 'WB093';	//WB093
    		obj_Result.stj = 'TALTALA';	//TALTALA
    		obj_Result.ctjCd = 'WA0606';	//WA0606
    		obj_Result.ppr = '';	//NA
    		obj_Result.dty = 'Regular';	//Regular
    		obj_Result.cmpRt = 'NA';	//NA
    		obj_Result.cxdt = '';	//
    		obj_Result.ctb = 'Public Sector Undertaking';	//Public Sector Undertaking
    		obj_Result.sts = 'Active';	//Active
    		obj_Result.gstin = '19AAFCS9264A1Z8';	//19AAFCS9264A1Z8
    		obj_Result.lgnm = 'SOFTAGE INFORMATION TECHNOLOGY LIMITED';	//SOFTAGE INFORMATION TECHNOLOGY LIMITED
    		List<String> lstString = new List<String>();
    		lstString.add('a');
    		lstString.add('b');
    		lstString.add('c');
    		obj_Result.nba = lstString; 
    		obj_Result.ctj = '';
    		obj_Result.adadr = lst_adadr;
        
        
        
        
        
        GstinAuthDetailsResponseBody.cls_error obj_error = new GstinAuthDetailsResponseBody.cls_error();
            obj_error.errorCode = '';
            obj_error.errorType = '';
            obj_error.errorMessage = '';
        
        GstinAuthDetailsResponseBody objResponseBodyGST = new GstinAuthDetailsResponseBody();
        objResponseBodyGST.requestId = '';
        objResponseBodyGST.statusCode = 102;
        objResponseBodyGST.status = '200';
        objResponseBodyGST.error = obj_error;
        objResponseBodyGST.result = obj_Result;
        
        Test.startTest();
            TestMockRequest req1=new TestMockRequest(200,'OK',JSON.serialize(objResponseBodyGST),null);
            Test.setMock(HttpCalloutMock.class, req1);
            InitiateGSTControllerAPF.makeGSTINAuthCallout(paramjson1);
            
        Test.stopTest();    
            
            
        
	
	}
	

}