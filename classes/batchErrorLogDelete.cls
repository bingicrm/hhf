global class batchErrorLogDelete implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {	
        if(!Test.isRunningTest()){
        	return Database.getQueryLocator('select Id FROM Error_Log__c WHERE createddate < LAST_N_DAYS:60');
    	} else
        {
            return Database.getQueryLocator('select Id FROM Error_Log__c');
        }
    }
        global void execute(Database.BatchableContext BC, List<Error_Log__c> scope)
        {
    	List<Error_Log__c> lstErrLog = new List<Error_Log__c>();
             for(Error_Log__c a : scope)
             {
                 lstErrLog.add(a);     
             }
              if(!lstErrLog.isEmpty())
                delete lstErrLog;
            } 
        
    global void finish(Database.BatchableContext BC)
    {
    }
}