<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <tabs>standard-Chatter</tabs>
    <tabs>standard-Workspace</tabs>
    <tabs>standard-ContentSearch</tabs>
    <tabs>standard-ContentSubscriptions</tabs>
    <tabs>ATS_Lead_Source__c</tabs>
    <tabs>Third_Party_Verification_APF__c</tabs>
    <tabs>Sub_Industry__c</tabs>
    <tabs>CRM_Survey_Question__c</tabs>
    <tabs>CRM_Survey_Answer__c</tabs>
</CustomApplication>
