<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <tabs>standard-Contact</tabs>
    <tabs>standard-Account</tabs>
    <tabs>standard-Lead</tabs>
    <tabs>standard-Opportunity</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>Error_Log__c</tabs>
    <tabs>standard-Product2</tabs>
    <tabs>standard-Forecasting3</tabs>
    <tabs>Loan_Application__c</tabs>
    <tabs>Local_Policies__c</tabs>
    <tabs>Lead_Owner_Mapping__c</tabs>
    <tabs>Targets__c</tabs>
    <tabs>Monthly_Target__c</tabs>
    <tabs>ATS_Lead_Source__c</tabs>
    <tabs>Third_Party_Verification_APF__c</tabs>
    <tabs>Sub_Industry__c</tabs>
    <tabs>PDC_Bank_City__c</tabs>
    <tabs>Hierarchy__c</tabs>
    <tabs>standard-WaveHome</tabs>
    <tabs>CRM_Survey_Question__c</tabs>
    <tabs>CRM_Survey_Answer__c</tabs>
</CustomApplication>
