<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>1</label>
    <protected>false</protected>
    <values>
        <field>Current_Stage__c</field>
        <value xsi:type="xsd:string">APF Initiation</value>
    </values>
    <values>
        <field>Current_Status__c</field>
        <value xsi:type="xsd:string">APF Initiated</value>
    </values>
    <values>
        <field>Current_Sub_Stage__c</field>
        <value xsi:type="xsd:string">Project Data Capture</value>
    </values>
    <values>
        <field>Is_Enabled__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Next_Stage__c</field>
        <value xsi:type="xsd:string">APF Initiation</value>
    </values>
    <values>
        <field>Next_Status__c</field>
        <value xsi:type="xsd:string">APF Initiated</value>
    </values>
    <values>
        <field>Next_Sub_Stage__c</field>
        <value xsi:type="xsd:string">File Checker</value>
    </values>
    <values>
        <field>Ownership_to__c</field>
        <value xsi:type="xsd:string">Individual</value>
    </values>
    <values>
        <field>Previous_Stage__c</field>
        <value xsi:type="xsd:string">APF Initiation</value>
    </values>
    <values>
        <field>Previous_Status__c</field>
        <value xsi:type="xsd:string">APF Initiated</value>
    </values>
    <values>
        <field>Previous_Sub_Stage__c</field>
        <value xsi:type="xsd:string">Project Data Capture</value>
    </values>
</CustomMetadata>
