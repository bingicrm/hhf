<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>COPS:Data Checker</label>
    <protected>false</protected>
    <values>
        <field>Fields_to_Display__c</field>
        <value xsi:type="xsd:string">Contact_Name__c,Document_Type__c,Document_Master__c,Status__c</value>
    </values>
    <values>
        <field>Permission_to_Edit__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Permission_to_Upload__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Permission_to_View_Attachments__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
