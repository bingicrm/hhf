<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NCH</label>
    <protected>false</protected>
    <values>
        <field>Group_Exposure_Amount__c</field>
        <value xsi:type="xsd:double">2.0000001E7</value>
    </values>
    <values>
        <field>Loan_Amount__c</field>
        <value xsi:type="xsd:double">2.0000001E7</value>
    </values>
    <values>
        <field>Rank__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>Role__c</field>
        <value xsi:type="xsd:string">NCH</value>
    </values>
    <values>
        <field>process__c</field>
        <value xsi:type="xsd:string">Credit</value>
    </values>
</CustomMetadata>
