<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>71</label>
    <protected>false</protected>
    <values>
        <field>Current_Stage__c</field>
        <value xsi:type="xsd:string">APF Inventory Maintenance</value>
    </values>
    <values>
        <field>Current_Status__c</field>
        <value xsi:type="xsd:string">APF Inventory Approved</value>
    </values>
    <values>
        <field>Current_Sub_Stage__c</field>
        <value xsi:type="xsd:string">APF Inventory Approval</value>
    </values>
    <values>
        <field>Is_Enabled__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Next_Stage__c</field>
        <value xsi:type="xsd:string">APF Inventory Maintenance</value>
    </values>
    <values>
        <field>Next_Status__c</field>
        <value xsi:type="xsd:string">APF Inventory Approved</value>
    </values>
    <values>
        <field>Next_Sub_Stage__c</field>
        <value xsi:type="xsd:string">APF Inventory Approval</value>
    </values>
    <values>
        <field>Ownership_to__c</field>
        <value xsi:type="xsd:string">Individual</value>
    </values>
    <values>
        <field>Previous_Stage__c</field>
        <value xsi:type="xsd:string">APF Inventory Maintenance</value>
    </values>
    <values>
        <field>Previous_Status__c</field>
        <value xsi:type="xsd:string">APF Approved</value>
    </values>
    <values>
        <field>Previous_Sub_Stage__c</field>
        <value xsi:type="xsd:string">APF Inventory Update</value>
    </values>
</CustomMetadata>
