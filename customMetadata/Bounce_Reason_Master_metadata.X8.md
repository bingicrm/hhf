<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>8</label>
    <protected>false</protected>
    <values>
        <field>BOUNCECHARGE__c</field>
        <value xsi:type="xsd:string">Y</value>
    </values>
    <values>
        <field>Bounce_Type__c</field>
        <value xsi:type="xsd:string">Non Technical</value>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">EXCEEDS ARRANGEMENT</value>
    </values>
</CustomMetadata>
