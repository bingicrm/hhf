<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Cibil Corporate</label>
    <protected>false</protected>
    <values>
        <field>Call_Back_url__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Client_ID__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Client_Password__c</field>
        <value xsi:type="xsd:string">mbuser@hh2FL</value>
    </values>
    <values>
        <field>Client_Secret_Key__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Client_Username__c</field>
        <value xsi:type="xsd:string">mbuser</value>
    </values>
    <values>
        <field>Content_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Is_Log_Enabled__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>JSONBody2__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>JSONBody__c</field>
        <value xsi:type="xsd:string">{
    &quot;Header&quot;: {
        &quot;SFDCRecordId&quot;: &quot;a0Z0w000000I74Z&quot;,
        &quot;RequestType&quot;: &quot;REQUEST&quot;,
        &quot;RequestTime&quot;: &quot;2019-04-04 15:40:49&quot;,
        &quot;CustId&quot;: &quot;a017F000019Z3PJQA0&quot;,
        &quot;ApplicationId&quot;: &quot;a017F000019Z3PJQA0&quot;
    },
    &quot;Request&quot;: {
        &quot;ProductType&quot;: &quot;CCIR&quot;,
        &quot;LoanType&quot;: &quot;2&quot;,
        &quot;LoanAmount&quot;: 846464,
        &quot;SourceSystemName&quot;: &quot;COMM&quot;,
        &quot;BureauRegion&quot;: &quot;QA/UAT&quot;,
        &quot;LoanPurposeDesc&quot;: &quot;Demand loan&quot;,
        &quot;Name&quot;: &quot;ANTARA SENIOR LIVING LTD&quot;,
        &quot;CompanyCategory&quot;: &quot;PRIVATE LIMITED&quot;,
        &quot;NatureOfBusiness&quot;: &quot;GROWING OF CEREALS&quot;,
        &quot;Address&quot;: [
            {
                &quot;AddressType&quot;: &quot;REGISTERED OFFICE&quot;,
                &quot;Address&quot;: &quot;1 DR JHA MARG, MAX HOUSE, OKHLA,&quot;,
                &quot;AddressCity&quot;: &quot;New Delhi&quot;,
                &quot;AddressPin&quot;: &quot;110020&quot;,
                &quot;AddressState&quot;: &quot;New Delhi&quot;
            }
        ],
        &quot;Id&quot;: {
            &quot;Pan&quot;: &quot;AAJCA4540R&quot;,
            &quot;Cin&quot;: &quot;&quot;
        },
        &quot;Phone&quot;: {
            &quot;PhoneNumber&quot;: &quot;9417978783&quot;
        },
        &quot;RelatedIndividuals&quot;: [
            {
                &quot;IndividualEntity&quot;: {
                    &quot;IndvEntityType&quot;: &quot;51&quot;,
                    &quot;IndvEntityRelation&quot;: {
                        &quot;FatherName&quot;: &quot;Mohan&quot;
                    },
                    &quot;IndvEntityName&quot;: {
                        &quot;Name1&quot;: &quot;ANTARA SENIOR LIVING LTD&quot;
                    },
                    &quot;Gender&quot;: &quot;M&quot;,
                    &quot;BirthDT&quot;: &quot;03031994&quot;,
                    &quot;IndvEntityAddress&quot;: {
                        &quot;AddressType&quot;: &quot;REGISTERED OFFICE&quot;,
                        &quot;Address&quot;: &quot;1 DR JHA MARG, MAX HOUSE, OKHLA,&quot;,
                        &quot;AddressPin&quot;: &quot;110020&quot;,
                        &quot;AddressCity&quot;: &quot;New Delhi&quot;,
                        &quot;AddressState&quot;: &quot;New Delhi&quot;
                    },
                    &quot;IndvEntityId&quot;: {
                        &quot;Pan&quot;: &quot;AAJCA4540R&quot;
                    },
                    &quot;IndvEntityPhone&quot;: {
                        &quot;PhoneNumber&quot;: &quot;9417978783&quot;
                    }
                }
            }
        ],
        &quot;RelatedOrganizations&quot;: [
            {
                &quot;OrganizationEntity&quot;: {
                    &quot;OrgnEntityType&quot;: &quot;51&quot;,
                    &quot;OrgnEntityName&quot;: &quot;ANTARA SENIOR LIVING LTD&quot;
                },
                &quot;OrgnEntityAddress&quot;: {
                    &quot;AddressType&quot;: &quot;REGISTERED OFFICE&quot;,
                    &quot;Address&quot;: &quot;1 DR JHA MARG, MAX HOUSE, OKHLA,&quot;,
                    &quot;AddressCity&quot;: &quot;New Delhi&quot;,
                    &quot;AddressPin&quot;: &quot;110020&quot;,
                    &quot;AddressState&quot;: &quot;New Delhi&quot;
                },
                &quot;OrgnentityId&quot;: {
                    &quot;Pan&quot;: &quot;AAJCA4540R&quot;
                },
                &quot;OrgnentityPhone&quot;: {
                    &quot;PhoneNumber&quot;: &quot;9417978783&quot;
                },
                &quot;ClassOfActivity&quot;: &quot;RE&quot;
            }
        ],
        &quot;BureauProductType&quot;: &quot;CIR&quot;
    }
}</value>
    </values>
    <values>
        <field>Product__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Request_Body__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Request_Method__c</field>
        <value xsi:type="xsd:string">POST</value>
    </values>
    <values>
        <field>Request_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_EndPoint__c</field>
        <value xsi:type="xsd:string">https://services.herofincorp.com/mb/api/multiBureauCommercial</value>
    </values>
    <values>
        <field>Service_End_Point_SMS__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Time_Out_Period__c</field>
        <value xsi:type="xsd:double">120000.0</value>
    </values>
    <values>
        <field>org_id__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
