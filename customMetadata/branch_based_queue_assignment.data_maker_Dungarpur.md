<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>data maker Dungarpur</label>
    <protected>false</protected>
    <values>
        <field>Branch__c</field>
        <value xsi:type="xsd:string">Dungarpur</value>
    </values>
    <values>
        <field>LOB__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Maximum_Amount__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Minimum_Amount__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Queue_Assigned__c</field>
        <value xsi:type="xsd:string">COPS Data Maker Dungarpur</value>
    </values>
    <values>
        <field>Stage__c</field>
        <value xsi:type="xsd:string">Operation Control</value>
    </values>
    <values>
        <field>Sub_Stage__c</field>
        <value xsi:type="xsd:string">COPS:Data Maker</value>
    </values>
</CustomMetadata>
