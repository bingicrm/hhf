<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Politically Exposed Persons</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Approval_Required__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Deviation__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Email__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Maximum_Match_Percentage__c</field>
        <value xsi:type="xsd:double">95.0</value>
    </values>
    <values>
        <field>Priority__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>Reject_Data_Match_Count__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Rejection_Reason__c</field>
        <value xsi:type="xsd:string">Negative Dedupe Match</value>
    </values>
    <values>
        <field>Severity_Level__c</field>
        <value xsi:type="xsd:string">1</value>
    </values>
    <values>
        <field>UCIC_Database_Name__c</field>
        <value xsi:type="xsd:string">Politically Exposed Persons</value>
    </values>
</CustomMetadata>
