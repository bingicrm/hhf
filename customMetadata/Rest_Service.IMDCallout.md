<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>IMDCallout</label>
    <protected>false</protected>
    <values>
        <field>Call_Back_url__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Client_ID__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Client_Password__c</field>
        <value xsi:type="xsd:string">hhfl@lMs12</value>
    </values>
    <values>
        <field>Client_Secret_Key__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Client_Username__c</field>
        <value xsi:type="xsd:string">lmsuser</value>
    </values>
    <values>
        <field>Content_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Is_Log_Enabled__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>JSONBody2__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>JSONBody__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Product__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Request_Body__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Request_Method__c</field>
        <value xsi:type="xsd:string">POST</value>
    </values>
    <values>
        <field>Request_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_EndPoint__c</field>
        <value xsi:type="xsd:string">https://services.herofincorp.com/homeloanService/api/onlineReceipt</value>
    </values>
    <values>
        <field>Service_End_Point_SMS__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Time_Out_Period__c</field>
        <value xsi:type="xsd:double">120000.0</value>
    </values>
    <values>
        <field>org_id__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
