<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Re-Look</label>
    <protected>false</protected>
    <values>
        <field>Cancel__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Cancelled_Reject_Stage__c</field>
        <value xsi:type="xsd:string">Re-Look</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Re_Open_Stage__c</field>
        <value xsi:type="xsd:string">Credit Decisioning</value>
    </values>
    <values>
        <field>Re_Open_Sub_Stage__c</field>
        <value xsi:type="xsd:string">Re Credit</value>
    </values>
    <values>
        <field>Reject__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Reopen__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Revive_Stage__c</field>
        <value xsi:type="xsd:string">Customer Onboarding</value>
    </values>
    <values>
        <field>Revive_Sub_Stage__c</field>
        <value xsi:type="xsd:string">Application Initiation</value>
    </values>
</CustomMetadata>
