<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Escrow_Bank_Details__c</label>
    <protected>false</protected>
    <values>
        <field>Enable_After_Delete__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Enable_After_Insert__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Enable_After_Undelete__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Enable_After_Update__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Enable_Before_Delete__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Enable_Before_Insert__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Enable_Before_Update__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Object_API_Name__c</field>
        <value xsi:type="xsd:string">Escrow_Bank_Details__c</value>
    </values>
    <values>
        <field>Trigger_Bypass__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>isTriggerActive__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
