<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>TRADING</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Activity_Type__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Industry__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Platform_Type__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Sector__c</field>
        <value xsi:type="xsd:string">T</value>
    </values>
    <values>
        <field>Service_Industry_Type__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Service__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Skill_Level__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Sub_Industry__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Trading_Type__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
