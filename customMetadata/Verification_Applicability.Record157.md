<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Record157</label>
    <protected>false</protected>
    <values>
        <field>Auto_Initiate__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Customer_Segment__c</field>
        <value xsi:type="xsd:string">LLP</value>
    </values>
    <values>
        <field>Customer_Type__c</field>
        <value xsi:type="xsd:string">Corporate</value>
    </values>
    <values>
        <field>Employee__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>FCU__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>FI__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>LIP__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Legal__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Loan_Contact_Type__c</field>
        <value xsi:type="xsd:string">Co- Applicant</value>
    </values>
    <values>
        <field>Product__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Scheme__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Technical__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Transaction_Type__c</field>
        <value xsi:type="xsd:string">Seller Balance Transfer</value>
    </values>
    <values>
        <field>Type_of_Address__c</field>
        <value xsi:type="xsd:string">Office/ Business</value>
    </values>
    <values>
        <field>Vendor_PD__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
