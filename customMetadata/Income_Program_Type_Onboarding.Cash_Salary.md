<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Cash Salary</label>
    <protected>false</protected>
    <values>
        <field>Income_Program_Type__c</field>
        <value xsi:type="xsd:string">Cash Salary</value>
    </values>
    <values>
        <field>LMS_Code__c</field>
        <value xsi:type="xsd:string">CP-CAS</value>
    </values>
</CustomMetadata>
