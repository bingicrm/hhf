<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>1</label>
    <protected>false</protected>
    <values>
        <field>Evaluation_Count__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>Scheme__c</field>
        <value xsi:type="xsd:string">EHL</value>
    </values>
    <values>
        <field>Threshold_Amount__c</field>
        <value xsi:type="xsd:double">5000000.0</value>
    </values>
    <values>
        <field>Transaction_Type__c</field>
        <value xsi:type="xsd:string">Normal</value>
    </values>
    <values>
        <field>Type_of_Property__c</field>
        <value xsi:type="xsd:string">Flat</value>
    </values>
</CustomMetadata>
