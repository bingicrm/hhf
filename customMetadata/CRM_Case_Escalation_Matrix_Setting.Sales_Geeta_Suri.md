<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Sales Geeta Suri</label>
    <protected>false</protected>
    <values>
        <field>Department__c</field>
        <value xsi:type="xsd:string">Sales</value>
    </values>
    <values>
        <field>L1_Email_Id__c</field>
        <value xsi:type="xsd:string">geeta.suri@herohfl.com</value>
    </values>
    <values>
        <field>L1_Name__c</field>
        <value xsi:type="xsd:string">Geeta Suri</value>
    </values>
    <values>
        <field>L2_Email_Id__c</field>
        <value xsi:type="xsd:string">viresh.kumar@herohfl.com</value>
    </values>
    <values>
        <field>L2_Name__c</field>
        <value xsi:type="xsd:string">Viresh Kumar</value>
    </values>
    <values>
        <field>L3_Email_Id__c</field>
        <value xsi:type="xsd:string">rahul.kishore@herohfl.com</value>
    </values>
    <values>
        <field>L3_Name__c</field>
        <value xsi:type="xsd:string">Rahul Kishore</value>
    </values>
    <values>
        <field>isActive__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
