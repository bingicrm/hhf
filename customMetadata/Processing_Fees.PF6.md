<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PF6</label>
    <protected>false</protected>
    <values>
        <field>Processing_Fees__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>Product__c</field>
        <value xsi:type="xsd:string">Construction Finance</value>
    </values>
    <values>
        <field>Property_Type__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
