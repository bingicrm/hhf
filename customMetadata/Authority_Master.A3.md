<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>A3</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Combination_Value__c</field>
        <value xsi:type="xsd:string">1000000010000000500</value>
    </values>
    <values>
        <field>Deviation_Level__c</field>
        <value xsi:type="xsd:double">500.0</value>
    </values>
    <values>
        <field>Group_Exposure_Amount__c</field>
        <value xsi:type="xsd:double">1.0E7</value>
    </values>
    <values>
        <field>Loan_Amount__c</field>
        <value xsi:type="xsd:double">1.0E7</value>
    </values>
</CustomMetadata>
