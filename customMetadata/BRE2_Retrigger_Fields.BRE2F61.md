<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BRE2F61</label>
    <protected>false</protected>
    <values>
        <field>Field__c</field>
        <value xsi:type="xsd:string">Vintage_of_Existing_Business__c</value>
    </values>
    <values>
        <field>Object__c</field>
        <value xsi:type="xsd:string">Personal_Discussion__c</value>
    </values>
    <values>
        <field>isActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
