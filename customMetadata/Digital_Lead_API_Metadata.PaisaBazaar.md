<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PaisaBazaar.com</label>
    <protected>false</protected>
    <values>
        <field>Source_Detail__c</field>
        <value xsi:type="xsd:string">PaisaBazaar.com</value>
    </values>
    <values>
        <field>User_Name__c</field>
        <value xsi:type="xsd:string">pb_delhi</value>
    </values>
    <values>
        <field>Vendor_Password__c</field>
        <value xsi:type="xsd:string">pb@hhfldigital</value>
    </values>
</CustomMetadata>
