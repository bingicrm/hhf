<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CS Pooja Trehan</label>
    <protected>false</protected>
    <values>
        <field>Department__c</field>
        <value xsi:type="xsd:string">CS</value>
    </values>
    <values>
        <field>L1_Email_Id__c</field>
        <value xsi:type="xsd:string">pooja.trehan@herohfl.com</value>
    </values>
    <values>
        <field>L1_Name__c</field>
        <value xsi:type="xsd:string">Pooja Trehan</value>
    </values>
    <values>
        <field>L2_Email_Id__c</field>
        <value xsi:type="xsd:string">gourav.pandey@herohfl.com/rajesh.kumar1@herohfl.com</value>
    </values>
    <values>
        <field>L2_Name__c</field>
        <value xsi:type="xsd:string">Gourav Pandey/Rajesh Kumar</value>
    </values>
    <values>
        <field>L3_Email_Id__c</field>
        <value xsi:type="xsd:string">gautam.munjal@herohfl.com</value>
    </values>
    <values>
        <field>L3_Name__c</field>
        <value xsi:type="xsd:string">Gautam Munjal</value>
    </values>
    <values>
        <field>isActive__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
