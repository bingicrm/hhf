<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PMAY1</label>
    <protected>false</protected>
    <values>
        <field>Recipient__c</field>
        <value xsi:type="xsd:string">Customer</value>
    </values>
    <values>
        <field>SMS_Template__c</field>
        <value xsi:type="xsd:string">Dear Sir / Madam, Thank you for considering Hero Housing Finance Ltd. (HHFL) for your Housing Loan requirement. As you have also applied for the PMAY subsidy scheme, please click https://bit.ly/2CAzjPJ  for eligibility requirements and process of availing the subsidy. After disbursal of the loan, HHFL will submit and coordinate with NHB  for your PMAY application. However, the decision of NHB shall be  final and binding  in all respects. Regards, Team Hero Housing Finance.</value>
    </values>
    <values>
        <field>Stage_Value__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
