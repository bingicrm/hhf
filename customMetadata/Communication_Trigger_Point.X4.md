<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>4</label>
    <protected>false</protected>
    <values>
        <field>Recipient__c</field>
        <value xsi:type="xsd:string">Customer</value>
    </values>
    <values>
        <field>SMS_Template__c</field>
        <value xsi:type="xsd:string">We regret to inform you that based on our internal policy norms, we are unable to sanction your loan request.</value>
    </values>
    <values>
        <field>Stage_Value__c</field>
        <value xsi:type="xsd:string">Loan Rejected</value>
    </values>
</CustomMetadata>
