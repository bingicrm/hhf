<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Notes and Attachments</label>
    <protected>false</protected>
    <values>
        <field>Delete_files__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Folder_Name__c</field>
        <value xsi:type="xsd:string">Notes &amp; Attachments for</value>
    </values>
</CustomMetadata>
