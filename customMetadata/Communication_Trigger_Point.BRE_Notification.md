<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BRE Notification</label>
    <protected>false</protected>
    <values>
        <field>Recipient__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SMS_Template__c</field>
        <value xsi:type="xsd:string">Dear Customer, Your App.No.&lt;LA&gt; for loan of Rs.&lt;Amount&gt; is approved In-Principle, subject to further evaluation.
Hero Housing Finance Ltd 
&lt;var&gt;</value>
    </values>
    <values>
        <field>Stage_Value__c</field>
        <value xsi:type="xsd:string">BRE Notification</value>
    </values>
</CustomMetadata>
