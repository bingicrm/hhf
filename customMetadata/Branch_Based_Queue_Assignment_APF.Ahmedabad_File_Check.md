<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Ahmedabad File Check</label>
    <protected>false</protected>
    <values>
        <field>Branch__c</field>
        <value xsi:type="xsd:string">Ahmedabad</value>
    </values>
    <values>
        <field>Is_Enabled__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Line_of_Business__c</field>
        <value xsi:type="xsd:string">Enter Values Here</value>
    </values>
    <values>
        <field>Maximum_Amount_Cap__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Minimum_Amount_Cap__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Queue_Assigned__c</field>
        <value xsi:type="xsd:string">Ahmedabad File Checker</value>
    </values>
    <values>
        <field>Stage__c</field>
        <value xsi:type="xsd:string">APF Initiation</value>
    </values>
    <values>
        <field>Sub_Stage__c</field>
        <value xsi:type="xsd:string">File Checker</value>
    </values>
</CustomMetadata>
