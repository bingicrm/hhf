<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CRM Case Closure</label>
    <protected>false</protected>
    <values>
        <field>Recipient__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SMS_Template__c</field>
        <value xsi:type="xsd:string">Dear Valued Customer,

Greetings from HHFL!!!

You Service Request number &lt;CaseNumber&gt; is resolved. We are closing your service request now.

In case of any further assistance, please write to us at Customer.care@herohfl.com or you can also reach us at 1800-212-8800, Monday to Friday- 10:00 AM to 6:00 PM. We will be delighted to assist you.

We value your relationship with us and assure you of our best services always.

Please provide your valuable feedback here.
&lt;URL&gt;

Warm Regards,
Team Hero Housing Finance Ltd.</value>
    </values>
    <values>
        <field>Stage_Value__c</field>
        <value xsi:type="xsd:string">CRM Case Closure Notification</value>
    </values>
</CustomMetadata>
