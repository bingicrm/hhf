<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Perfios NetBanking</label>
    <protected>false</protected>
    <values>
        <field>Recipient__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SMS_Template__c</field>
        <value xsi:type="xsd:string">Dear Customer, for your loan application &lt;LA&gt; with us, please use this link to complete bank statement analysis for &lt;bank&gt;:&lt;link&gt; HHFL</value>
    </values>
    <values>
        <field>Stage_Value__c</field>
        <value xsi:type="xsd:string">Perfios NetBanking</value>
    </values>
</CustomMetadata>
