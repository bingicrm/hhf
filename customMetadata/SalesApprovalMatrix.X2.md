<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>2</label>
    <protected>false</protected>
    <values>
        <field>Approving_authority__c</field>
        <value xsi:type="xsd:string">Business Head</value>
    </values>
    <values>
        <field>Max_Reduction__c</field>
        <value xsi:type="xsd:double">100.0</value>
    </values>
    <values>
        <field>Min_Reduction__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Vertical__c</field>
        <value xsi:type="xsd:string">Digital</value>
    </values>
    <values>
        <field>fieldname__c</field>
        <value xsi:type="xsd:string">ROI</value>
    </values>
</CustomMetadata>
