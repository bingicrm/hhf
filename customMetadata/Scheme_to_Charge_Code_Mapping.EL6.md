<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>EL6</label>
    <protected>false</protected>
    <values>
        <field>Charge_Code_ID__c</field>
        <value xsi:type="xsd:string">35</value>
    </values>
    <values>
        <field>Charge_ID__c</field>
        <value xsi:type="xsd:string">600835</value>
    </values>
    <values>
        <field>Scheme_ID__c</field>
        <value xsi:type="xsd:string">39</value>
    </values>
</CustomMetadata>
