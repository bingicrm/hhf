<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ANDAMAN AND NICOBAR ISLANDS</label>
    <protected>false</protected>
    <values>
        <field>LMS_Code__c</field>
        <value xsi:type="xsd:string">509</value>
    </values>
    <values>
        <field>State_Code__c</field>
        <value xsi:type="xsd:string">01</value>
    </values>
    <values>
        <field>State__c</field>
        <value xsi:type="xsd:string">ANDAMAN AND NICOBAR ISLANDS</value>
    </values>
</CustomMetadata>
