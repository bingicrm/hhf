<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Loan Against Property - Dombivali</label>
    <protected>false</protected>
    <values>
        <field>Branch__c</field>
        <value xsi:type="xsd:string">Dombivali</value>
    </values>
    <values>
        <field>Hunter_Product_Code__c</field>
        <value xsi:type="xsd:string">LAP_I_WES</value>
    </values>
    <values>
        <field>Product_Name__c</field>
        <value xsi:type="xsd:string">Loan Against Property</value>
    </values>
</CustomMetadata>
