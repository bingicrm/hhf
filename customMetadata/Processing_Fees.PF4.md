<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PF4</label>
    <protected>false</protected>
    <values>
        <field>Processing_Fees__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Product__c</field>
        <value xsi:type="xsd:string">Loan Against Property</value>
    </values>
    <values>
        <field>Property_Type__c</field>
        <value xsi:type="xsd:string">Industrial</value>
    </values>
</CustomMetadata>
