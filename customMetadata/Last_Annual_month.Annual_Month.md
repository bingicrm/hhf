<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Annual Month</label>
    <protected>false</protected>
    <values>
        <field>CutOff_Date__c</field>
        <value xsi:type="xsd:string">30</value>
    </values>
    <values>
        <field>Last_Annual_month__c</field>
        <value xsi:type="xsd:string">11</value>
    </values>
</CustomMetadata>
