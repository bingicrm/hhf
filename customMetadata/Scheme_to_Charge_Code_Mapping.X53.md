<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>53</label>
    <protected>false</protected>
    <values>
        <field>Charge_Code_ID__c</field>
        <value xsi:type="xsd:string">500547</value>
    </values>
    <values>
        <field>Charge_ID__c</field>
        <value xsi:type="xsd:string">600736</value>
    </values>
    <values>
        <field>Scheme_ID__c</field>
        <value xsi:type="xsd:string">11</value>
    </values>
</CustomMetadata>
