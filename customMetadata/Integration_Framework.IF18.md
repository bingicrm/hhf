<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>IF18</label>
    <protected>false</protected>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">Loan_Applications__r.Loan_Number__c</value>
    </values>
    <values>
        <field>Is_List__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Object__c</field>
        <value xsi:type="xsd:string">IMD__c</value>
    </values>
    <values>
        <field>Service_Name__c</field>
        <value xsi:type="xsd:string">IMDCallout</value>
    </values>
    <values>
        <field>Tag_Name__c</field>
        <value xsi:type="xsd:string">agreementId</value>
    </values>
</CustomMetadata>
