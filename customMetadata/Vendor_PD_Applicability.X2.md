<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>2</label>
    <protected>false</protected>
    <values>
        <field>Branch__c</field>
        <value xsi:type="xsd:string">Delhi</value>
    </values>
    <values>
        <field>Customer_Segment__c</field>
        <value xsi:type="xsd:string">1</value>
    </values>
    <values>
        <field>Scheme__c</field>
        <value xsi:type="xsd:string">HL</value>
    </values>
    <values>
        <field>Transaction_Type__c</field>
        <value xsi:type="xsd:string">Normal</value>
    </values>
    <values>
        <field>Vendor_PD__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
