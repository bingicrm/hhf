<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>HRF36</label>
    <protected>false</protected>
    <values>
        <field>Critical_Field__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Field__c</field>
        <value xsi:type="xsd:string">Land_Line_No__c</value>
    </values>
    <values>
        <field>Object__c</field>
        <value xsi:type="xsd:string">Reference__c</value>
    </values>
</CustomMetadata>
