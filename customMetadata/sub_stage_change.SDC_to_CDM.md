<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SDC to CDM</label>
    <protected>false</protected>
    <values>
        <field>AssignedTo__c</field>
        <value xsi:type="xsd:string">Queue</value>
    </values>
    <values>
        <field>Current_Stage__c</field>
        <value xsi:type="xsd:string">Operation Control</value>
    </values>
    <values>
        <field>Current_Sub_Stage__c</field>
        <value xsi:type="xsd:string">Scan: Data Checker</value>
    </values>
    <values>
        <field>Next_Stage__c</field>
        <value xsi:type="xsd:string">Operation Control</value>
    </values>
    <values>
        <field>Next_Sub_Stage__c</field>
        <value xsi:type="xsd:string">COPS:Data Maker</value>
    </values>
    <values>
        <field>Previous_Stage__c</field>
        <value xsi:type="xsd:string">Operation Control</value>
    </values>
    <values>
        <field>Previous_Sub_Stage__c</field>
        <value xsi:type="xsd:string">Scan: Data Maker</value>
    </values>
</CustomMetadata>
