<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ATS</label>
    <protected>false</protected>
    <values>
        <field>Source_Detail__c</field>
        <value xsi:type="xsd:string">SFDC</value>
    </values>
    <values>
        <field>User_Name__c</field>
        <value xsi:type="xsd:string">ATSLEAD</value>
    </values>
    <values>
        <field>Vendor_Password__c</field>
        <value xsi:type="xsd:string">ATS@HF@123</value>
    </values>
</CustomMetadata>
