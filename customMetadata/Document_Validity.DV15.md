<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DV15</label>
    <protected>false</protected>
    <values>
        <field>Document_Id__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Document_Name__c</field>
        <value xsi:type="xsd:string">Interim Sanction Letter</value>
    </values>
    <values>
        <field>Validity_in_days__c</field>
        <value xsi:type="xsd:string">30</value>
    </values>
</CustomMetadata>
