<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>16</label>
    <protected>false</protected>
    <values>
        <field>Controlling_field_API_Name__c</field>
        <value xsi:type="xsd:string">Customer_seegment__c</value>
    </values>
    <values>
        <field>Controlling_field_Value__c</field>
        <value xsi:type="xsd:string">Small Enterprise</value>
    </values>
    <values>
        <field>Dependent_field_API_Name__c</field>
        <value xsi:type="xsd:string">Income_Program_Type__c</value>
    </values>
    <values>
        <field>Dependent_field_value__c</field>
        <value xsi:type="xsd:string">Cash Profit Program</value>
    </values>
    <values>
        <field>Object_Name_for_Dependent_Picklist__c</field>
        <value xsi:type="xsd:string">Loan_Contact__c</value>
    </values>
</CustomMetadata>
