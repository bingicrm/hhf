<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>OTC Reviewed</label>
    <protected>false</protected>
    <values>
        <field>Recipient__c</field>
        <value xsi:type="xsd:string">Initiator</value>
    </values>
    <values>
        <field>SMS_Template__c</field>
        <value xsi:type="xsd:string">An application &lt;App id&gt; for &lt;customer&gt; has been reviewed for OTC request.</value>
    </values>
    <values>
        <field>Stage_Value__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
