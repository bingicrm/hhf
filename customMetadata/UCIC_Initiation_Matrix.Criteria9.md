<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Criteria9</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Direct_Callout__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>FieldType__c</field>
        <value xsi:type="xsd:string">SubStage</value>
    </values>
    <values>
        <field>Next_Stage__c</field>
        <value xsi:type="xsd:string">Loan Cancel</value>
    </values>
    <values>
        <field>Previous_Stage__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
