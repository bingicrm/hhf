<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Perfios ITR</label>
    <protected>false</protected>
    <values>
        <field>Recipient__c</field>
        <value xsi:type="xsd:string">Customer</value>
    </values>
    <values>
        <field>SMS_Template__c</field>
        <value xsi:type="xsd:string">Dear Customer, analysis for &lt;Type&gt; has been initiated for your loan &lt;LA&gt;  with HHFL. Please click on the link to proceed: &lt;var&gt; HHFL</value>
    </values>
    <values>
        <field>Stage_Value__c</field>
        <value xsi:type="xsd:string">Perfios ITR</value>
    </values>
</CustomMetadata>
