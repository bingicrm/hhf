<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BRE2F9</label>
    <protected>false</protected>
    <values>
        <field>Field__c</field>
        <value xsi:type="xsd:string">If_Other_please_specify__c</value>
    </values>
    <values>
        <field>Object__c</field>
        <value xsi:type="xsd:string">Loan_Application__c</value>
    </values>
    <values>
        <field>isActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
