<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Loan Reschedule request - Credit</label>
    <protected>false</protected>
    <values>
        <field>Category__c</field>
        <value xsi:type="xsd:string">REPAYMENT CHANGE/ RESCHEDULEMENT</value>
    </values>
    <values>
        <field>Department__c</field>
        <value xsi:type="xsd:string">Credit</value>
    </values>
    <values>
        <field>Dept_TAT__c</field>
        <value xsi:type="xsd:double">10.0</value>
    </values>
    <values>
        <field>Request_Type__c</field>
        <value xsi:type="xsd:string">Easy Kill</value>
    </values>
    <values>
        <field>Sequence__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Sub_Category__c</field>
        <value xsi:type="xsd:string">LOAN RESCHEDULEMENT REQUEST</value>
    </values>
    <values>
        <field>Total_TAT__c</field>
        <value xsi:type="xsd:double">13.0</value>
    </values>
    <values>
        <field>isActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
