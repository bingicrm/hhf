<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>LOAN AGAINST PROPERTY Fixedfor</label>
    <protected>false</protected>
    <values>
        <field>Consider_for_Split__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>IMGC_Premium_Fees__c</field>
        <value xsi:type="xsd:double">100.0</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>LTV__c</field>
        <value xsi:type="xsd:double">75.0</value>
    </values>
    <values>
        <field>Loan_Application_Scheme__c</field>
        <value xsi:type="xsd:string">HERO ASHRAY - LOAN AGAINST PROPERTY</value>
    </values>
    <values>
        <field>Max_Insurance_Amount__c</field>
        <value xsi:type="xsd:double">20.0</value>
    </values>
    <values>
        <field>Max_Loan_Amount__c</field>
        <value xsi:type="xsd:double">3000000.0</value>
    </values>
    <values>
        <field>Min_Loan_Amount__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Scheme__c</field>
        <value xsi:type="xsd:string">Personal Loan</value>
    </values>
</CustomMetadata>
