<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Delhi Credit</label>
    <protected>false</protected>
    <values>
        <field>Branch__c</field>
        <value xsi:type="xsd:string">Delhi</value>
    </values>
    <values>
        <field>LOB__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Maximum_Amount__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Minimum_Amount__c</field>
        <value xsi:type="xsd:double">1.0E8</value>
    </values>
    <values>
        <field>Queue_Assigned__c</field>
        <value xsi:type="xsd:string">Delhi Credit Evaluation</value>
    </values>
    <values>
        <field>Stage__c</field>
        <value xsi:type="xsd:string">Credit Decisioning</value>
    </values>
    <values>
        <field>Sub_Stage__c</field>
        <value xsi:type="xsd:string">Credit Review</value>
    </values>
</CustomMetadata>
