<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Low LTV Gross Margin</label>
    <protected>false</protected>
    <values>
        <field>Income_Program_Type__c</field>
        <value xsi:type="xsd:string">Low LTV Gross Margin</value>
    </values>
    <values>
        <field>LMS_Code__c</field>
        <value xsi:type="xsd:string">CP-LTV</value>
    </values>
</CustomMetadata>
