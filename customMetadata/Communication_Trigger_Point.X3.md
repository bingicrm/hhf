<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>3</label>
    <protected>false</protected>
    <values>
        <field>Recipient__c</field>
        <value xsi:type="xsd:string">Customer</value>
    </values>
    <values>
        <field>SMS_Template__c</field>
        <value xsi:type="xsd:string">The PD Details with app id &lt;app id&gt; are as follows:Date and time: &lt;PD Date&gt; &lt;PD Time&gt;;CM visiting: &lt;name&gt;;Visiting CM contact no.: &lt;num&gt;</value>
    </values>
    <values>
        <field>Stage_Value__c</field>
        <value xsi:type="xsd:string">Initiate PD</value>
    </values>
</CustomMetadata>
