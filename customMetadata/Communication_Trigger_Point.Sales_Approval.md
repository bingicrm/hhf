<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Sales Approval</label>
    <protected>false</protected>
    <values>
        <field>Recipient__c</field>
        <value xsi:type="xsd:string">Sales Manager</value>
    </values>
    <values>
        <field>SMS_Template__c</field>
        <value xsi:type="xsd:string">An application &lt;App id&gt; for &lt;Customer Name&gt; has been assigned to you for your review &amp; action from Customer acceptance.</value>
    </values>
    <values>
        <field>Stage_Value__c</field>
        <value xsi:type="xsd:string">Sales Approval</value>
    </values>
</CustomMetadata>
