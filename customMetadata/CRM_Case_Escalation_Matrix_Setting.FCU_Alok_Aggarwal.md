<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>FCU Alok Aggarwal</label>
    <protected>false</protected>
    <values>
        <field>Department__c</field>
        <value xsi:type="xsd:string">FCU</value>
    </values>
    <values>
        <field>L1_Email_Id__c</field>
        <value xsi:type="xsd:string">alok.aggarwal@herohfl.com</value>
    </values>
    <values>
        <field>L1_Name__c</field>
        <value xsi:type="xsd:string">Alok Aggarwal</value>
    </values>
    <values>
        <field>L2_Email_Id__c</field>
        <value xsi:type="xsd:string">akhileshwar.kumar@herofincorp.com</value>
    </values>
    <values>
        <field>L2_Name__c</field>
        <value xsi:type="xsd:string">Akhileshwar</value>
    </values>
    <values>
        <field>L3_Email_Id__c</field>
        <value xsi:type="xsd:string">rajneesh.sharma@herofincorp.com</value>
    </values>
    <values>
        <field>L3_Name__c</field>
        <value xsi:type="xsd:string">Rajnish</value>
    </values>
    <values>
        <field>isActive__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
