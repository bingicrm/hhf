<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PMAY2</label>
    <protected>false</protected>
    <values>
        <field>Recipient__c</field>
        <value xsi:type="xsd:string">Customer</value>
    </values>
    <values>
        <field>SMS_Template__c</field>
        <value xsi:type="xsd:string">Dear Sir / Madam, Your Loan Amount &lt;AppLoanAmt&gt; has been disbursed and your Loan Account Number is &lt;lanNumber&gt;. Please click on the following link for understanding the eligibility requirements and process with regard to your PMAY application: https://bit.ly/2CAzjPJ Please note that HHFL will now coordinate with the National Housing Bank (NHB) for processing of the subsidy, the decision of NHB shall be final and binding in all respects.</value>
    </values>
    <values>
        <field>Stage_Value__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
