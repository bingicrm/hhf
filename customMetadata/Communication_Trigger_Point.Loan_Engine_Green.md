<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Loan Engine Green</label>
    <protected>false</protected>
    <values>
        <field>Recipient__c</field>
        <value xsi:type="xsd:string">Customer</value>
    </values>
    <values>
        <field>SMS_Template__c</field>
        <value xsi:type="xsd:string">We are happy to share with you the In-principal approval details subjected to further evalution of your application &lt;app id&gt;.  Hero Housing Finance Ltd</value>
    </values>
    <values>
        <field>Stage_Value__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
