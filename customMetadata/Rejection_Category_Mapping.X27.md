<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>27</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Rejection_Category__c</field>
        <value xsi:type="xsd:string">Credit Reject</value>
    </values>
    <values>
        <field>Rejection_Reason__c</field>
        <value xsi:type="xsd:string">CHANGE IN SCHEME</value>
    </values>
</CustomMetadata>
