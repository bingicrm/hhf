<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CRF18</label>
    <protected>false</protected>
    <values>
        <field>Critical_Field__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Field__c</field>
        <value xsi:type="xsd:string">Requested_Loan_Tenure__c</value>
    </values>
    <values>
        <field>Object__c</field>
        <value xsi:type="xsd:string">Loan_Application__c</value>
    </values>
</CustomMetadata>
