<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SendUserSMS</label>
    <protected>false</protected>
    <values>
        <field>Call_Back_url__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Client_ID__c</field>
        <value xsi:type="xsd:string">herofcalt</value>
    </values>
    <values>
        <field>Client_Password__c</field>
        <value xsi:type="xsd:string">herofcalt6</value>
    </values>
    <values>
        <field>Client_Secret_Key__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Client_Username__c</field>
        <value xsi:type="xsd:string">HEROHO</value>
    </values>
    <values>
        <field>Content_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Is_Log_Enabled__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>JSONBody2__c</field>
        <value xsi:type="xsd:string">&lt;/multisms&gt;&lt;dlr&gt;true&lt;/dlr&gt;&lt;alert&gt;1&lt;/alert&gt;&lt;intflag&gt;false&lt;/intflag&gt;&lt;/push&gt;</value>
    </values>
    <values>
        <field>JSONBody__c</field>
        <value xsi:type="xsd:string">&lt;?xml version=&quot;1.0&quot;?&gt;&lt;push&gt;&lt;appid&gt;{appId}&lt;/appid&gt;&lt;subappid&gt;{appId}&lt;/subappid&gt;&lt;userid&gt;{appId}&lt;/userid&gt;&lt;pass&gt;{userPass}&lt;/pass&gt;&lt;acct-type&gt;1&lt;/acct-type&gt;&lt;msgid&gt;{msgId}&lt;/msgid&gt;&lt;content-type&gt;1&lt;/content-type&gt;&lt;priority&gt;true&lt;/priority&gt;&lt;masking&gt;true&lt;/masking&gt;&lt;from&gt;{sender}&lt;/from&gt;&lt;multisms&gt;</value>
    </values>
    <values>
        <field>Product__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Request_Body__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Request_Method__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Request_Type__c</field>
        <value xsi:type="xsd:string">POST</value>
    </values>
    <values>
        <field>Service_EndPoint__c</field>
        <value xsi:type="xsd:string">https://push3.maccesssmspush.com/servlet/com.aclwireless.pushconnectivity.listeners.XmlLangaugeAPIListener?xmlStr=</value>
    </values>
    <values>
        <field>Service_End_Point_SMS__c</field>
        <value xsi:type="xsd:string">https://push3.maccesssmspush.com/servlet/com.aclwireless.pushconnectivity.listeners.TextListener?appid=&lt;appId&gt;&amp;userId=&lt;userId&gt;&amp;pass=&lt;pass&gt;&amp;contenttype=1&amp;from=&lt;from&gt;&amp;to=&lt;to&gt;&amp;text=&lt;&lt;configuredtext&gt;&gt;&amp;alert=1&amp;selfid=true&amp;intflag=false</value>
    </values>
    <values>
        <field>Time_Out_Period__c</field>
        <value xsi:type="xsd:double">120000.0</value>
    </values>
    <values>
        <field>org_id__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
