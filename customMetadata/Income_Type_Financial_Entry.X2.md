<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>2</label>
    <protected>false</protected>
    <values>
        <field>AIP_Status__c</field>
        <value xsi:type="xsd:string">NA</value>
    </values>
    <values>
        <field>Banking_Status__c</field>
        <value xsi:type="xsd:string">Required</value>
    </values>
    <values>
        <field>Financial_Details_Status__c</field>
        <value xsi:type="xsd:string">Required</value>
    </values>
    <values>
        <field>Income_Program__c</field>
        <value xsi:type="xsd:string">Liquid Income Program</value>
    </values>
    <values>
        <field>LIP_Status__c</field>
        <value xsi:type="xsd:string">BCM Required</value>
    </values>
    <values>
        <field>Obligation_Status__c</field>
        <value xsi:type="xsd:string">Required</value>
    </values>
    <values>
        <field>Other_Income_Status__c</field>
        <value xsi:type="xsd:string">Required</value>
    </values>
    <values>
        <field>Salaried_Details_Status__c</field>
        <value xsi:type="xsd:string">NA</value>
    </values>
</CustomMetadata>
