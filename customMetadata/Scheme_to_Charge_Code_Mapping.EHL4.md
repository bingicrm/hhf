<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>EHL4</label>
    <protected>false</protected>
    <values>
        <field>Charge_Code_ID__c</field>
        <value xsi:type="xsd:string">500813</value>
    </values>
    <values>
        <field>Charge_ID__c</field>
        <value xsi:type="xsd:string">600848</value>
    </values>
    <values>
        <field>Scheme_ID__c</field>
        <value xsi:type="xsd:string">38</value>
    </values>
</CustomMetadata>
